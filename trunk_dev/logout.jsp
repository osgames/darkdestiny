<%@page import="at.viswars.*"%>

<%
System.out.println("LOGOUT!");

request.getSession();
session.setAttribute("userId",null);

try {
    session.invalidate();
} catch (IllegalStateException ise) {
    
}

// String forward = "http://www.thedarkdestiny.at/homepage/index.html";
String forward = (String)request.getParameter("forward");
if (forward == null) forward = GameConfig.getInstance().getStartURL();

if (request.getParameter("forward") != null) {
    forward = response.encodeRedirectURL(forward);
}

response.sendRedirect(forward);
%>
