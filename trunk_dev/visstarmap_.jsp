
<%@page import="at.viswars.model.User" %>
<%@page import="at.viswars.model.UserData" %>
<%@page import="at.viswars.service.ProfileService" %>
<%
// Determine correct connection path
    StringBuffer xmlUrlBuffer = request.getRequestURL();
    String xmlUrl = "";
    int slashCounter = 0;
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    User u = ProfileService.findUserByUserId(userId);
    UserData ud = ProfileService.findUserDataByUserId(userId);
    for (int i = 0; i < xmlUrlBuffer.length(); i++) {
        char currChar = xmlUrlBuffer.charAt(i);
        if (currChar == ("/".toCharArray())[0]) {
            slashCounter++;
        }

        xmlUrl += currChar;
        if (slashCounter == 4) {
            break;
        }
    }
%>

<body style="background-color:#000000">
<br><br><CENTER>
    <a href="main.jsp?page=visstarmapXT">Alte Sternenkarte</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.thedarkdestiny.at/board/viewtopic.php?f=5&t=2372" target="_new">Forums Thread</a>
    <BR>
    <applet id="starmap" code="at.viswars.vismap.gui.AppletMain" width="<%= ud.getStarmapWidth()%>" height="<%= ud.getStarmapHeight()%>" alt="" archive="applet/vismap__.jar">
        <param name="source" value="<%= xmlUrl%>createStarMapInfoXT.jsp"/>
        <param name="sessionId" value="<%= request.getSession().getId()%>"/>
        <param name="MODE" value="STARMAP">
        <PARAM NAME="cache_option" VALUE="NO">

    </applet>
</CENTER>
<script language="javascript" type="text/javascript">
    var disableHTMLScroll = false;

    document.getElementById("starmap").addEventListener('click',
    function(e){
        disableHTMLScroll = true;
    }
    , false);

    document.getElementById("starmap").addEventListener('mouseover',
    function(e){
        disableHTMLScroll = true;
    }
    , false);

    document.getElementById("starmap").addEventListener('mouseout',
    function(e){
        disableHTMLScroll = false;
    }
    , false);

    document.addEventListener('DOMMouseScroll', function(e){
        if (disableHTMLScroll) {
            e.stopPropagation();
            e.preventDefault();
            e.cancelBubble = false;
            return false;
        } else {
            return true;
        }
    }, false);
</script>
</BODY>