<%-- 
    Document   : accInDeletion
    Created on : 28.05.2012, 12:49:12
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.service.*"%>
<%@page import="at.viswars.Logger.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));

User u = MainService.findUserByUserId(userId);
int deleteDate = u.getDeleteDate();
int finalDeletion = (u.getDeleteDate() + GameConstants.DELETION_DELAY) - GameUtilities.getCurrentTick2();

int gracePeriod = u.getDeleteDate() + (int)(GameConstants.DELETION_DELAY / 2d);
int graceLeft = gracePeriod - GameUtilities.getCurrentTick2();

%>
<TABLE width="80%">
    <TR align="center">
        <TD align="middle" colspan=2>
            <BR><BR>
            You registered your account for deletion, you may still revoke that state for <%= graceLeft %> ticks, if you decide to revoke your deletion state,<BR>
            you will be able to enter the game again in <%= graceLeft %> ticks, otherwise deletion will be locked in and your account is deleted in <%= finalDeletion %> ticks.<BR><BR>
            <INPUT type="button" value="Löschung zurücknehmen" onclick="window.location.href = './createacc.jsp'">
        </TD>
    </TR>
</TABLE>
