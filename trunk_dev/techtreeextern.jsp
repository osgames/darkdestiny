
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.model.UserSettings"%>
<%@page import="at.viswars.model.User" %>
<%@page import="at.viswars.model.UserData" %>
<%@page import="at.viswars.service.ProfileService" %>
<%
// Determine correct connection path
StringBuffer xmlUrlBuffer = request.getRequestURL();
String xmlUrl = "";
int slashCounter = 0;
int userId = Integer.parseInt((String) session.getAttribute("userId"));
    UserSettings us = Service.userSettingsDAO.findByUserId(userId);
for (int i=0;i<xmlUrlBuffer.length();i++) {
    char currChar = xmlUrlBuffer.charAt(i);
    if (currChar == ("/".toCharArray())[0]) slashCounter++;
    
    xmlUrl += currChar;
    if (slashCounter == 4) break;
}
%>
<HTML>
    <TITLE>

    </TITLE>
    <BODY bgcolor="BLACK">

<br><br><CENTER>
     	<META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>

            <applet id="techtree" code="techtree.AppletMain" width="<%= us.getTtvWidth() %>" height="<%= us.getTtvHeight() %>" alt="" archive="applet/TechTreeViewer.jar">
                <param name="source" value="<%= xmlUrl %>createTechTreeInfo.jsp"/>
                <PARAM NAME="cache_option" VALUE="NO">

            </applet>
</CENTER>

    </BODY>
<script language="javascript" type="text/javascript">
var disableHTMLScroll = false;

    document.getElementById("techtree").addEventListener('click',
    function(e){
        disableHTMLScroll = true;
    }
    , false);

    document.getElementById("techtree").addEventListener('mouseover',
    function(e){
        disableHTMLScroll = true;
    }
    , false);

    document.getElementById("techtree").addEventListener('mouseout',
    function(e){
        disableHTMLScroll = false;
    }
    , false);

document.addEventListener('DOMMouseScroll', function(e){
    if (disableHTMLScroll) {
        e.stopPropagation();
        e.preventDefault();
        e.cancelBubble = false;
        return false;
    } else {
        return true;
    }
}, false);
</script>
</HTML>