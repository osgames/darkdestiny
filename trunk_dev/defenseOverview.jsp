<%@ page import="java.util.*" %>
<%@ page import="at.viswars.GameConfig" %>
<%@ page import="at.viswars.model.*" %>
<%@ page import="at.viswars.military.*" %>
<%@ page import="at.viswars.service.*" %>
<%@ page import="at.viswars.utilities.MilitaryUtilities" %>
<%@ page import="at.viswars.databuffer.fleet.RelativeCoordinate" %>
<%@ page import="at.viswars.ML" %>
<%
StringBuffer requestUrl = request.getRequestURL();
int userId = Integer.parseInt((String)session.getAttribute("userId"));   


String basePath = "";
int slashCounter = 0;

for (int i=0;i<requestUrl.length();i++) {
    char currChar = requestUrl.charAt(i);
    if (currChar == ("/".toCharArray())[0]) slashCounter++;
    
    basePath += currChar;
    if (slashCounter == 4) break;
}
%>
<script src="java/ajax.js" type="text/javascript"></script>
<script type="text/javascript">
var lastModified = 0;

function processAjaxData(defenseDataTotal) {
    // Create table
    var DetailTable = document.createElement("table");
    DetailTable.width = "100%";
    DetailTable.className = "blue";

    var tBodyOuter = document.createElement("tbody");
    DetailTable.appendChild(tBodyOuter);

    // Add shiprow
    var defenseData = null;
    var topic = null;
    var color = null;
    
    for (var x=0;x<3;x++) {
        switch (x) {
            case 0:
                topic = "<%= ML.getMLStr("defenseoverview_lbl_ownunits", userId) %>";
                color = "blue2";
                defenseData = defenseDataTotal.own;
                break;
            case 1:
                topic = "<%= ML.getMLStr("defenseoverview_lbl_alliedunits", userId) %>";
                color = "lightblue";
                defenseData = defenseDataTotal.allied;
                break;
            case 2:
                topic = "<%= ML.getMLStr("defenseoverview_lbl_enemyunits", userId) %>";
                color = "red";
                defenseData = defenseDataTotal.enemy;
                break;
        }
        
        if (defenseData == null) continue;
        
        var sideHeaderRow = document.createElement("tr");
        sideHeaderRow.className = color;
        var sideHeaderCell = document.createElement("td");
        sideHeaderCell.colSpan = "2";        
        var sideHeaderBoldNode = document.createElement("b");
        sideHeaderBoldNode.appendChild(document.createTextNode(topic));
        sideHeaderCell.appendChild(sideHeaderBoldNode);
        sideHeaderCell.width = "50%";
        sideHeaderCell.align = "center";
        sideHeaderRow.appendChild(sideHeaderCell);
        tBodyOuter.appendChild(sideHeaderRow);
        
        if ((defenseData.planetShips != null) || (defenseData.systemShips != null)) {                       
            var shipHeaderRow = document.createElement("tr");
            shipHeaderRow.className = color;
            var shipDetailRow = document.createElement("tr");

            var shipHeaderCell1 = document.createElement("td");
            shipHeaderCell1.width = "50%";
            var boldNodeH1 = document.createElement("b");
            boldNodeH1.appendChild(document.createTextNode("<%= ML.getMLStr("defenseoverview_lbl_orbitaldefense", userId) %>"));
            shipHeaderCell1.appendChild(boldNodeH1);
            var shipHeaderCell2 = document.createElement("td");
            shipHeaderCell2.width = "50%";                   
            var boldNodeH2 = document.createElement("b");
            boldNodeH2.appendChild(document.createTextNode("<%= ML.getMLStr("defenseoverview_lbl_systemwidedefense", userId) %>"));
            shipHeaderCell2.appendChild(boldNodeH2);

            shipHeaderRow.appendChild(shipHeaderCell1);
            shipHeaderRow.appendChild(shipHeaderCell2);

            tBodyOuter.appendChild(shipHeaderRow);

            // Add ship cells
            var shipPlanetCell = document.createElement("td");
            shipPlanetCell.setAttribute("valign", "top");
            var shipSystemCell = document.createElement("td");
            shipSystemCell.setAttribute("valign", "top");

            if (defenseData.planetShips != null) {
                for (i=0;i<defenseData.planetShips.length;i++) {
                    shipPlanetCell.appendChild(document.createTextNode(number_format(defenseData.planetShips[i].count,0) + "x " + defenseData.planetShips[i].name));
                    shipPlanetCell.appendChild(document.createElement("br"));
                }
            } else {
                shipPlanetCell.appendChild(document.createTextNode("-"));
            }
            if (defenseData.systemShips != null) {
                for (i=0;i<defenseData.systemShips.length;i++) {
                    shipSystemCell.appendChild(document.createTextNode(number_format(defenseData.systemShips[i].count,0) + "x " + defenseData.systemShips[i].name));
                    shipSystemCell.appendChild(document.createElement("br"));
                }                    
            } else {
                shipSystemCell.appendChild(document.createTextNode("-"));
            }                    

            // Add ship info
            shipDetailRow.appendChild(shipPlanetCell);
            shipDetailRow.appendChild(shipSystemCell);                                        

            tBodyOuter.appendChild(shipDetailRow);
        }

        // Add groundtroop row
        if (defenseData.groundTroops != null) {
            var troopHeaderRow = document.createElement("tr");
            troopHeaderRow.className = color;
            var troopHeaderCell = document.createElement("td");
            var boldNode = document.createElement("b");
            boldNode.appendChild(document.createTextNode("<%= ML.getMLStr("defenseoverview_lbl_groundtroops", userId) %>"));
            troopHeaderCell.appendChild(boldNode);
            troopHeaderCell.width = "50%";
            troopHeaderCell.colSpan = "2";
            troopHeaderRow.appendChild(troopHeaderCell);

            tBodyOuter.appendChild(troopHeaderRow);

            var troopDetailRow = document.createElement("tr");
            var troopDetailCell1 = document.createElement("td");
            troopDetailCell1.width = "50%";
            troopDetailCell1.setAttribute("valign", "top");
            var troopDetailCell2 = document.createElement("td");
            troopDetailCell2.width = "50%";
            troopDetailCell2.setAttribute("valign", "top");

            var left = true;

            for (i=0;i<defenseData.groundTroops.length;i++) {
                if (left) {
                    left = false;
                    troopDetailCell1.appendChild(document.createTextNode(number_format(defenseData.groundTroops[i].count,0) + "x " + defenseData.groundTroops[i].name));
                    troopDetailCell1.appendChild(document.createElement("br"));
                } else {
                    left = true;
                    troopDetailCell2.appendChild(document.createTextNode(number_format(defenseData.groundTroops[i].count,0) + "x " + defenseData.groundTroops[i].name));
                    troopDetailCell2.appendChild(document.createElement("br"));                            
                }
            }                    

            troopDetailRow.appendChild(troopDetailCell1);
            troopDetailRow.appendChild(troopDetailCell2);

            tBodyOuter.appendChild(troopDetailRow);
        }

        // Add groundtroop row
        if (defenseData.groundDefense != null) {
            var groundHeaderRow = document.createElement("tr");
            groundHeaderRow.className = color;
            var groundHeaderCell = document.createElement("td");
            var boldNodeGH = document.createElement("b");
            boldNodeGH.appendChild(document.createTextNode("<%= ML.getMLStr("defenseoverview_lbl_planetarydefense", userId) %>"));
            groundHeaderCell.appendChild(boldNodeGH);
            groundHeaderCell.colSpan = "2";
            groundHeaderRow.appendChild(groundHeaderCell);

            tBodyOuter.appendChild(groundHeaderRow);

            var groundDetailRow = document.createElement("tr");
            var groundDetailCell = document.createElement("td");
            groundDetailCell.colSpan = "2";

            for (i=0;i<defenseData.groundDefense.length;i++) {
                groundDetailCell.appendChild(document.createTextNode(number_format(defenseData.groundDefense[i].count,0) + "x " + defenseData.groundDefense[i].name));
                groundDetailCell.appendChild(document.createElement("br"));
            }         

            groundDetailRow.appendChild(groundDetailCell);
            tBodyOuter.appendChild(groundDetailRow);
        }
    }
    
    var toReplace = document.getElementById("detailContent").firstChild;
    document.getElementById("detailContent").replaceChild(DetailTable,toReplace);    
}

function showDetail(systemId,planetId){
    var table = document.getElementById("dataTable");
    var tRows = table.rows;
    
    if (lastModified != 0) {
        table.deleteRow(lastModified);
    }
    
    for (i=0; i<tRows.length; i++) { 
        if (tRows[i].id == ("row"+planetId)) {
            ajaxCall('<%= basePath %>pDefJSON.jsp?sId=' + systemId + '&pId=' + planetId,processAjaxData);
            // getData(systemId,planetId);
                       
            var TR = table.insertRow(i+1);
            lastModified = i + 1;
            
            var TDDataCell = document.createElement("td");
            TDDataCell.id = "detailContent";
            TDDataCell.colSpan = "3";                      
            TDDataCell.appendChild(document.createTextNode("<%= ML.getMLStr("defenseoverview_msg_loadingdata", userId) %>"));
            
            TR.appendChild(document.createElement("td"));
            TR.appendChild(TDDataCell);
        }
    }
}
</script>
<%
int showPlanet = 0;     
        
        if (request.getParameter("forward") != null) {
            if (request.getParameter("planetId") != null) {
                int planetId = Integer.parseInt(request.getParameter("planetId"));
                
                boolean valid = false;
                PlayerPlanet pp = MainService.findPlayerPlanetByPlanetId(planetId);
                if(pp != null){
                    if(pp.getUserId() == userId){
                            valid = true;
                        }
                }
                
                
                if (valid) {
                    RelativeCoordinate rc = new RelativeCoordinate(0,planetId);
                    int systemId = rc.getSystemId();

                    session.setAttribute("actPlanet",""+planetId);
                    session.setAttribute("actSystem",""+systemId);                  
%>                                
                    <jsp:include page="military.jsp">
                        <jsp:param name="page" value='military' />
                        <jsp:param name="type" value='8' />
                    </jsp:include>
<%
                    return;
                }
            }
        }
        
        if (request.getParameter("showPlanet") != null) {
            showPlanet = Integer.parseInt(request.getParameter("showPlanet"));
        }        
        
        if (showPlanet != 0) {            
%>
            
<%
        }
        
        MilitaryUtilities mu = new MilitaryUtilities();
        ArrayList<DefensePlanetEntry> dpEntries = mu.getDefensePlanetEntries(userId);
        %>
        <TABLE id="dataTable" class="blue">
            <TR style="font-weight:bold;" align="center">
                <TD class="blue"><%= ML.getMLStr("defenseoverview_lbl_id", userId)  %></TD>
                <TD class="blue" width="200"><%= ML.getMLStr("defenseoverview_lbl_name", userId) %></TD>
                <TD class="blue" width="150"><%= ML.getMLStr("defenseoverview_lbl_planetarydefense", userId) %></TD>
                <TD class="blue" width="150"><%= ML.getMLStr("defenseoverview_lbl_systemdefense", userId) %></TD>
                <TD class="blue"><%= ML.getMLStr("defenseoverview_lbl_actions", userId) %></TD>
            </TR>
            <%
            for (DefensePlanetEntry dpe : dpEntries) {                 
                boolean showDetail = false; %>              
            <TR id="row<%= dpe.getId() %>" align="center">
                <TD><%= dpe.getId() %></TD>
                <TD><%= dpe.getName() %></TD>
                <TD><% if (dpe.isHasMilitary()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/military.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasFleetDefense()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/spaceDef.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasFleetDefenseAllied()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/spaceDef_allied.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasEnemyFleet()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/spaceDef_enemy.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasOrbitalDefense()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/deploy.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasPlanetDefense()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/planetaryDef.jpg" + "\" \\>"); } %></TD>
                <TD><% if (dpe.isHasSystemFleet()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/spaceDef.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasSystemFleetAllied()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/spaceDef_allied.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasEnemySystemFleet()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/spaceDef_enemy.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasSystemDefense()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/deploy.jpg" + "\" \\>&nbsp"); } %>
                <% if (dpe.isHasScannerArray()) { showDetail = true; out.write("<IMG src=\"" + GameConfig.picPath() + "pic/icons/systemScanner.jpg" + "\" \\>&nbsp"); } %></TD>
                <TD><% if (showDetail) out.write("<IMG style=\"cursor:pointer;\" src=\"" + GameConfig.picPath() + "pic/view.jpg" + "\" onmouseover=\"doTooltip(event,' " + ML.getMLStr("defenseoverview_pop_showdefensedetails", userId)  + "')\" onmouseout=\"hideTip()\" onclick=\"showDetail("+dpe.getSystemId()+","+dpe.getId()+");\" \\>"); %>
                <% out.write("<IMG style=\"cursor:pointer;\" src=\"" + GameConfig.picPath() + "pic/goPlanet.jpg" + "\" onmouseover=\"doTooltip(event,' " + ML.getMLStr("defenseoverview_pop_planetarydefense", userId)  + "')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=switchPlanet&toLink=defensemenu&type=1&stype=4&switchPlanet="+dpe.getId()+"'\" \\>"); %></TD>
            </TR>
            <%
            }
            %>
        </TABLE>