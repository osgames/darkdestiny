<%@page import="at.viswars.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.service.AllianceService" %>
<%@page import="at.viswars.model.AllianceMember" %>
<%@page import="at.viswars.model.Alliance" %>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
boolean userHasAlliance = false;
int allianceId=0;
AllianceMember allianceMember = AllianceService.findAllianceMemberByUserId(userId);
       if(allianceMember != null){
            userHasAlliance = true;
        }

if (request.getParameter("allianceid") != null) { %>
    <script language="Javascript">        
        function setPic() {
            var availSpace = (Weite-355) * 0.8;

            document.getElementById("bild").setAttribute("width", availSpace, 0);
        }
    </script>    
<%    
    allianceId = Integer.parseInt(request.getParameter("allianceid"));    

    try {

        Alliance a = AllianceService.findAllianceByAllianceId(allianceId);
         if (a != null){
             ArrayList<AllianceMember> ams = AllianceService.findAllianceMemberByAllianceId(allianceId);
                    
%> 
            <TABLE class="bluetrans" width="80%" align="center">
            <TR bgcolor="#708090">
            <TD align="center" class="blue2"><B><%= a.getName() %>&nbsp;[<%= a.getTag() %>]</B></TD></TR>
<%                
            if (!a.getPicture().equalsIgnoreCase("")) {
%>                
                <TR><TD class="center" align="middle"><IMG id="bild" src="<%= a.getPicture() %>"/></TD></TR>
<%
            }
%>            
            <TR><TD align="center"><%= a.getDescription() %></TD></TR>
            <TR><TD>
                <TABLE style="font-size:12px" width="100%" cellpadding=0 cellspacing=0>
                <TR align="center">
                <TD width="50%"><B><%= ML.getMLStr("showAlliance_lbl_recruitingstate", userId)%></B></TD>
                <TD width="50%">
<%               if (a.getIsRecruiting()) { %>
                    <%= ML.getMLStr("showAlliance_lbl_allianceIsRecruiting", userId)%>
<%              } else { %>
                    <%= ML.getMLStr("showAlliance_lbl_allianceIsNotRecruiting", userId)%>
<%              } %>
                </TD></TR>
                <TR align="center">
                    <TD><B><%= ML.getMLStr("showAlliance_lbl_members", userId)%></B></TD>
                    <TD><%= ams.size() %></TD>
                    
                </TR>
<%
                if (ams.size() > 0) {
%>                                
                <TR>
                    <TD align="center" colspan=2>
                        <BR><B><%= ML.getMLStr("showAlliance_lbl_members", userId)%>:</B><BR>
<%
                        for (AllianceMember am : ams) {
                            out.write("<BR>");
                            out.write(AllianceService.findUserById(am.getUserId()).getGameName());
                        }
%>                        
                    </TD>
                </TR>
<%
                }
%>            
            </TABLE>
            </TD></TR>
            <TR><TD colspan=2 align="center"><A onClick="javascript:history.go(-1)"><BR><B><%= ML.getMLStr("global_link_back", userId)%></B></A></TD></TR>
            </TABLE>
<%
            } 
    } catch (Exception e) {
        DebugBuffer.addLine("Error in showAlliance: " + e);
    }
} else if (request.getParameter("type") != null) {
    int type = Integer.parseInt(request.getParameter("type"));

      %>
        <TABLE class="blue" width="80%" align="center">
            <TR align="center">
                <TD bgcolor="#708090" width="40%" class="blue"><B><%= ML.getMLStr("showAlliance_lbl_alliancename", userId)%></B></TD>
                <TD width="15%" class="blue"><B><%= ML.getMLStr("showAlliance_lbl_tag", userId)%></B></TD>
                <TD width="15%" class="blue"><B><%= ML.getMLStr("showAlliance_lbl_members", userId)%></B></TD>
                <TD width="15%" class="blue"><B><%= ML.getMLStr("showAlliance_lbl_abbr_recruiting", userId)%></B></TD>
                <TD width="15%" class="blue"><B><%= ML.getMLStr("showAlliance_lbl_join", userId)%></B></TD>
            </TR>
<%
        ArrayList<Alliance> as = AllianceService.findAllAlliances();
        for(Alliance a : as){
            if(!a.getIsRecruiting() && type == 1)continue;
            int memberCount = 0;


            
                memberCount = AllianceService.findAllianceMemberByAllianceId(a.getId()).size();

%>
            <TR align="center">
            <TD><A HREF="main.jsp?page=showAlliance&allianceid=<%= a.getId()%>" style="font-size:12px"><%= a.getName() %></A></TD>
            <TD align="center"><%= a.getTag() %></TD>
            <TD align="center"><%= memberCount %></TD>
            <TD>
<%
            if (a.getIsRecruiting()) { %>
                <%= ML.getMLStr("showAlliance_lbl_yes", userId)%>
<%          } else { %>
                <%= ML.getMLStr("showAlliance_lbl_no", userId)%>
<%          } %>
            </TD> 
<%          if (a.getIsRecruiting() && allianceMember == null) { %>
                <TD><A HREF="main.jsp?page=new/alliance&type=19&tag=<%= a.getTag() %>" style="font-size:12px"><%= ML.getMLStr("showAlliance_link_abbr_join", userId)%></A></TD>
<%          } else { %>
                <TD>-</TD>
<%          } %>
            </TR>
<%
        }
%>
        <TR><TD colspan="5" align="center"><A onClick="javascript:history.go(-1)"><BR><B><%= ML.getMLStr("global_link_back", userId)%></B></A></TD></TR>
        </TABLE>
<%
}
%>
