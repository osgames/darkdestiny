<%-- 
    Document   : researchNotification
    Created on : Dec 18, 2010, 4:15:10 PM
    Author     : Dreloc
--%>

<%@page import="java.util.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.model.Research" %>
<%@page import="at.viswars.ML" %>
<%@page import="at.viswars.notification.*" %>
<%@page import="at.viswars.GameConfig" %>
<%@page import="at.viswars.notification.ResearchNotification"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<SCRIPT TYPE="text/javascript">
    <!--
    function popup(mylink, windowname)
    {
        if (! window.focus)return true;
        var href;
        if (typeof(mylink) == 'string')
            href=mylink;
        else
            href=mylink.href;
        window.open(href, windowname, "resizable=no,scrollbars=yes,status=no");
        return false;
    }
    //-->
</SCRIPT>
<%




int researchId = 0;

            int userId = 0;
            if (session.getAttribute("userId") != null) {
                userId = Integer.parseInt((String) session.getAttribute("userId"));
            }
                    if (request.getParameter("researchId") != null) {
                        researchId = Integer.parseInt(request.getParameter("researchId"));
                    }
            Research r = Service.researchDAO.findById(researchId);

                   ResearchNotification rn = new ResearchNotification(r, userId);
              String msg =       rn.getMessage();
%>
<%= msg %>