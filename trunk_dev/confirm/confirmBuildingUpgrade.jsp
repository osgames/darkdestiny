<%@page import="at.viswars.buildable.ConstructionExt"%>
<%@page import="at.viswars.construction.ConstructionUpgradeCost"%>
<%@page import="at.viswars.model.Construction"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.result.ConstructionUpgradeResult"%>
<%@page import="at.viswars.FormatUtilities"%>
<%@page import="at.viswars.service.RessourceService"%>
<%@page import="at.viswars.model.Ressource"%>
<%@page import="at.viswars.model.Ressource"%>
<%@page import="at.viswars.ML"%>
<%@page import="at.viswars.construction.ConstructionImproveCost"%>
<%@page import="at.viswars.utilities.*"%>
<%@page import="at.viswars.result.ConstructionImproveResult"%>
<%@page import="at.viswars.service.ConstructionService"%>
<%@page import="at.viswars.requestbuffer.ConstructionUpgradeBuffer"%>
<%@page import="at.viswars.requestbuffer.ConstructionImproveBuffer"%>
<%@page import="at.viswars.requestbuffer.ParameterBuffer"%>
<%@page import="at.viswars.requestbuffer.BufferHandling"%>
<%@page import="java.util.*"%>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));

int bufferId = Integer.parseInt(request.getParameter("qi"));
ParameterBuffer pb = (ParameterBuffer)BufferHandling.getBuffer(userId, bufferId);

boolean upgrade = false;
boolean improve = false;
ConstructionUpgradeBuffer cub = null;
ConstructionImproveBuffer cib = null;

if (pb instanceof ConstructionUpgradeBuffer) {
    upgrade = true;
    cub = (ConstructionUpgradeBuffer)pb;
} else if (pb instanceof ConstructionImproveBuffer) {
    improve = true;
    cib = (ConstructionImproveBuffer)pb;
} else {
        String msg = "Invalid page call";
%>
        <jsp:forward page="/main.jsp?page=showerror&msg=<%= msg %>"/>
<%
}

int execute = 0;
if (request.getParameter("execute") != null) {
    execute = Integer.parseInt(request.getParameter("execute"));
}

if(execute == 1) {
        ArrayList<String> errors = new ArrayList<String>();

    if(improve){
        errors = ConstructionService.improveBuilding(cib.getUserId(), cib.getPlanetId(), cib.getConstructionId());
        cib.invalidate();
        }
    if(upgrade){
        
        errors = ConstructionService.upgradeBuilding(cub.getUserId(), cub.getPlanetId(), cub.getConstructionId(), cub.getConstructionTargetId(), cub.getCount());
        cub.invalidate();
        
       }
        // Call processing function and fill error array on errors

        if (errors.size() > 0) {
            String msg = null;
            for (String err : errors) {
                if (msg != null) msg += "<BR>";
                msg += err;
            }
        %>
        <jsp:forward page="/main.jsp?page=showerror&msg=<%= msg %>"/>
 <%
        } else {
        %>
        <jsp:forward page="/main.jsp?page=new/construction"/>
 <%
        }
 }

if (upgrade) { // Upgrade existing buildings to another type
    ConstructionExt ce = new ConstructionExt(cub.getConstructionId());
    ConstructionExt ceNew = new ConstructionExt(cub.getConstructionTargetId());
  ConstructionUpgradeCost cuc = new ConstructionUpgradeCost(ce, ceNew, cub.getPlanetId(), cub.getCount());
%>
<BR>
<FORM method="post" action='main.jsp?page=confirm/confirmBuildingUpgrade&qi=<%= bufferId %>&execute=1' name="confirm">

    <TABLE class="bluetrans" border="0" width="80%" style="font-size:13px">
        <TR>
            <TD align="center" colspan="2">
                <B><%= ML.getMLStr("construction_msg_constructionupgrade", userId) %><BR/></B></TD>
        </TR>
            <TR valign="middle">
            <TD width="200"><%= ML.getMLStr("construction_lbl_constructionType", userId) %>:</TD><TD><%= ML.getMLStr(Service.constructionDAO.findById(cub.getConstructionId()).getName(), userId) %></TD></TR>
        <TR valign="middle">
            <TD width="200"><%= ML.getMLStr("construction_lbl_constructiontargettype", userId) %>:</TD><TD><%= cub.getCount() %> x <%= ML.getMLStr(Service.constructionDAO.findById(cub.getConstructionTargetId()).getName(), userId) %></TD></TR>
        <TR>
            <TD><%= ML.getMLStr("construction_lbl_creditrefund", userId)%></TD>
             <TD><%= FormatUtilities.getFormattedNumber(cuc.getRess(Ressource.CREDITS) * cub.getCount()) %></TD>
        </TR>
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcecost", userId)%></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (cuc.getRess(r.getId()) > 0) {
            if(r.getId() == Ressource.CREDITS)continue;
            out.write("<TR><TD>"+ML.getMLStr(r.getName(), userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(cuc.getRess(r.getId()) * cub.getCount()) + "</SPAN></TD></TR>");
        }
    }
%>
        
                </TABLE><BR />

            <INPUT type="submit" name="Ok" value="Ok">
        </FORM> 
<%
} else if (improve) { // Level a current building

ConstructionImproveResult cir = new ConstructionImproveResult(cib.getConstructionId(), cib.getPlanetId());
ConstructionImproveCost cic = cir.getConstructionImproveCost();
%>
<BR>
      <FORM method="post" action='main.jsp?page=confirm/confirmBuildingUpgrade&qi=<%= bufferId %>&execute=1' name="confirm">

    <FONT size="-1"><%= ML.getMLStr("construction_msg_constructionimprovement",userId)%><BR/><BR><BR>
        <FONT color="yellow"><B><%= ML.getMLStr("construction_lbl_level",userId)%>  <%= cir.getLevel() %> => <%= (cir.getLevel() + 1) %></B></FONT><BR /><BR />
            <BR/></FONT>
        <TABLE class="bluetrans"  border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcecost", userId)%></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (cic.getRess(r.getId()) > 0) {
            if(r.getId() == Ressource.CREDITS)continue;
            out.write("<TR><TD>"+ML.getMLStr(r.getName(), userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(cic.getRess(r.getId())) + "</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />

            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%
}
%>

