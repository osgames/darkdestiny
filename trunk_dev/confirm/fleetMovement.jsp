<%@page import="at.viswars.FormatUtilities" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.movable.*" %>
<%@page import="at.viswars.construction.*" %>
<%@page import="at.viswars.requestbuffer.*" %>
<%@page import="at.viswars.model.*" %>
<%@page import="at.viswars.result.*" %>
<%@page import="at.viswars.databuffer.fleet.RelativeCoordinate" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.text.FormatTime"%>

<%        
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int systemId = Integer.parseInt((String)session.getAttribute("actSystem"));

int reqBufferId = 0;
if (request.getParameter("bufId") != null) {
    reqBufferId = Integer.parseInt(request.getParameter("bufId"));
}

FleetParameterBuffer fpb = FleetService.getRequestBuffer(userId, reqBufferId);

if (request.getParameter("c") != null) {
    BaseResult br = FleetService.processFlightRequest(userId, fpb);
    if (br.isError()) {
        fpb.invalidate();
        %><jsp:forward page="../main.jsp" >
        <jsp:param name="msg" value='<%=br.getMessage()%>' />
        <jsp:param name="page" value='showerror' />
        </jsp:forward><%
    } else {
        fpb.invalidate();
        if (fpb.getSourcePage() == 2) {
            RelativeCoordinate rc = null;

            if (fpb.targetIsSystem()) {
                rc = new RelativeCoordinate(fpb.getTarget(),0);
            } else {
                rc = new RelativeCoordinate(0,fpb.getTarget());
            }
 %>
        <jsp:forward page="../main.jsp" >
        <jsp:param name="page" value='new/system' />
        <jsp:param name="systemId" value='<%= rc.getSystemId() %>' />
        </jsp:forward>
     <% } else { %>
        <jsp:forward page="../main.jsp" >
        <jsp:param name="page" value='new/fleet' />
        </jsp:forward>
     <% }
    }
}

String destSystemName = "-";
String destPlanetName = "-";
String fleetName = "";

if (fpb.isFleetFormation()) {
    fleetName = ((FleetFormationExt)fpb.getParameter("fleet")).getBase().getName(); 
} else {
    fleetName = ((PlayerFleetExt)fpb.getParameter("fleet")).getBase().getName(); 
}


at.viswars.model.System destSystem = (at.viswars.model.System)fpb.getParameter("targetSystem");
Planet destPlanet = (Planet)fpb.getParameter("targetPlanet");

// Logger.getLogger().write("destSystem = " + destSystem.getName() + " destPlanet = " + destPlanet.getId());
// Logger.getLogger().write("distance = " + (fpb.getParameter("distance")));
// Logger.getLogger().write("distance = " + Double.parseDouble(fpb.getParameter("distance").toString()));
int systemId_ = destSystem.getId();
int planetId_ = 0;
if (destPlanet != null) {
    planetId_ = destPlanet.getId();
}

boolean hasEntry = FlightService.hasViewTableEntryFor(userId, systemId_, planetId_);
IdToNameService itn = new IdToNameService(userId);

if (hasEntry) {
    destSystemName = destSystem.getName();
    if (destPlanet != null) destPlanetName = itn.getPlanetName(destPlanet.getId());
} else {
    destSystemName = "System #" + destSystem.getId();
    if (destPlanet != null) destPlanetName = "Planet #" + destPlanet.getId();
}
%>
<FONT size=-1>Flotte versenden f&uuml;r Flotte <%= fleetName %> soll best&auml;tigt werden.<BR><BR>
<% if (fpb.isTrialWarning()) { %>
<FONT color="red"><B>ACHTUNG!!! Wenn du beim Anfliegen eines unbekannten oder nicht beobachteten Systems<BR>
auf einen Feind st&ouml;sst, wird der Trial-Schutz automatisch deaktiviert!</B></FONT><BR><BR>
<% } %>
<% if (fpb.isScanWarning()) { %>
<FONT color="red"><B>ACHTUNG!!! Diese Flotte scannt gerade. Wird diese Flotte bewegt wird der Scan automatisch abgebrochen!</B></FONT><BR><BR>
<% } %>
Gew&auml;hltes Ziel: <%= destSystemName %> / <%= destPlanetName %><BR>
Distanz: <%= FormatUtilities.getFormattedDecimal(Double.parseDouble(fpb.getParameter("distance").toString()),2) %> Lichtjahre<BR>
Gesch&auml;tzte Flugzeit: <FONT color=yellow><B><%= FormatTime.formatTimeSpan(((Integer)fpb.getParameter("timeToTarget")).longValue()) %></B></FONT><BR><BR></FONT>
<FORM method='post' action='main.jsp?page=confirm/fleetMovement&bufId=<%= fpb.getId() %>&c=1' name='confirm'>
    <INPUT type="submit" name="Ok" value="Ok" />
</FORM>
