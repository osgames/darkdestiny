<%@page import="at.viswars.Logger.Logger"%>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.construction.*" %>
<%@page import="at.viswars.databuffer.*" %>
<%@page import="at.viswars.model.Ressource" %>
<%@page import="at.viswars.service.RessourceService" %>
<%@page import="at.viswars.service.ConstructionService" %>
<%@page import="at.viswars.model.Construction" %>
<%@page import="at.viswars.requestbuffer.*" %>
<%@page import="at.viswars.result.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.database.access.DbConnect" %>
<%@page import="at.viswars.text.FormatTime"%>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));

int bufferId = Integer.parseInt(request.getParameter("qi"));
DeconstructionOrderAbortBuffer doab = (DeconstructionOrderAbortBuffer)BufferHandling.getBuffer(userId, bufferId);
int count = doab.getCount();

int execute = 0;
if (request.getParameter("execute") != null) {
    execute = Integer.parseInt(request.getParameter("execute"));
}
if(execute == 1) {
        ArrayList<String> errors = ConstructionService.abortDeconstruction(doab.getOrderId(), userId, count);
        session.setAttribute("actPage", "new/construction");
        
        /*
        try {
            request.removeAttribute("timeFinished");
        } catch(Exception e) {
            Logger.getLogger().write("Couldnt remove timeFinished");
        }
        */
        
        if (errors.size() > 0) {
            String msg = "";
            for (String err : errors) {
                if (msg.length() > 0) msg += "<BR>";
                msg += err;
            }

        String url = "/main.jsp?page=showerror&msg="+msg;
        %>                
        <jsp:forward page="<%= url %>"/>
 <%
        } else {
        %>
        <jsp:forward page="/main.jsp?page=new/construction"/>
 <%
        }
 }
        AbortDeconstructionResult adr = ConstructionService.getDeconstructionAbortCost(doab.getOrderId());
        ConstructionScrapAbortCost csac = adr.getConsScrapAbortCost();
%>
    
      <FORM method="post" action='main.jsp?page=confirm/deconstructionAbortion&qi=<%= doab.getId() %>&execute=1' name="confirm">
          <INPUT type="hidden" name="timeFinished" value="<%= doab.getOrderId() %>">
          <INPUT type="hidden" name="destroyCount" value="<%= count %>">
    <FONT size="-1"><%= ML.getMLStr("construction_msg_scrapabortionmsg1", userId)%><BR />
            <%= count %>x <%= ML.getMLStr(adr.getCons().getName(), userId) %><BR /><BR />
             <BR><%= ML.getMLStr("construction_msg_scrapabortionmsg2", userId)%><BR/><BR/></FONT>
        <TABLE border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcecost", userId)%></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (csac.getRess(r.getId()) > 0) {
            if(r.getId() == Ressource.CREDITS)continue;
            out.write("<TR><TD>"+ML.getMLStr(r.getName(), userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(csac.getRess(r.getId())*count) + "</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />

            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%

%>
