<%@page import="at.viswars.utilities.DiplomacyUtilities"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.DebugBuffer.DebugLevel"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="at.viswars.StarMapInfo"%>
<%@page import="at.viswars.StarMapInfoData"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="at.viswars.xml.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.OutputStreamWriter"%>
<%@page import="at.viswars.database.access.*" %>
<%
int userId = -1;
            try {
                Locale locale = request.getLocale();

                if (session.getAttribute("Language") != null) {
                    locale = (Locale) session.getAttribute("Language");
                }

                //session = request.getSession();
                System.out.println("session :" + session);
                System.out.println("session-Id :" + session.getId());
                
                try {
                    userId = Integer.parseInt((String) session.getAttribute("userId"));
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.WARNING, "Creating StarMapInfo-Data => No userId set");
                    return;
                }
                //    DebugBuffer.addLine(DebugLevel.WARNING, "Start creating Starmap-Data for User " + userId);
                long time = System.currentTimeMillis();

                //  DebugBuffer.addLine(DebugLevel.WARNING, "Done finding sharing users in " + (System.currentTimeMillis() - time) + " ms");

                XMLMemo starmap = new XMLMemo("Starmap");
                XMLMemo coding = new XMLMemo("Codierung");
                XMLMemo typeEntry = new XMLMemo("LandCode");
                typeEntry.addAttribute("A", "Gasriese, lebensfeindlich");
                typeEntry.addAttribute("B", "Gasriese, lebensfeindlich");
                typeEntry.addAttribute("C", "Kalt, bewohnbar");
                typeEntry.addAttribute("E", "Keine Atmosph\u00E4re, lebensfeindlich");
                typeEntry.addAttribute("G", "Heiss, bewohnbar");
                typeEntry.addAttribute("J", "Geologisch sehr aktiv, lebensfeindlich");
                typeEntry.addAttribute("L", "Fels und Eis, lebensfeindlich");
                typeEntry.addAttribute("M", "Erd\u00E4hnlich, bewohnbar");
                coding.addChild(typeEntry);
                starmap.addChild(coding);

                StarMapInfoData smid = new StarMapInfoData(userId);
                StarMapInfo smi = new StarMapInfo(userId);

                starmap = smi.addConstants(starmap);
                //  DebugBuffer.addLine(DebugLevel.WARNING, "Adding constants in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addObservatory(starmap, smid);
                //   DebugBuffer.addLine(DebugLevel.WARNING, "Adding observatories in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addSystems(starmap, smid);
                // DebugBuffer.addLine(DebugLevel.WARNING, "Adding systems in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addAllFleets(starmap, smid);
                // DebugBuffer.addLine(DebugLevel.WARNING, "Adding fleets in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addHyperScanner(starmap, smid);
                //  DebugBuffer.addLine(DebugLevel.WARNING, "Adding Hyperscanner in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addTradeRoutes(starmap);
                
                 smi.addSunTransmitter(starmap);
                //   DebugBuffer.addLine(DebugLevel.WARNING, "Adding traderoutes in " + (System.currentTimeMillis() - time) + " ms");
                 smi.addTerritories(starmap, smid);
//Hinzuf&uuml;gen der Handelsrouten Infos

                response.setCharacterEncoding("ISO-8859-1");
                // PrintWriter toJSP = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "ISO-8859-1"));
                PrintWriter toJSP = new PrintWriter(out);
                starmap.write(toJSP);
                //   DebugBuffer.addLine(DebugLevel.WARNING, "Done creating Starmap-Data for User " + userId + " in " + (System.currentTimeMillis() - time) + " ms");
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while generating StarmapInfo (for User "+userId+"): ", e);
            }
%>