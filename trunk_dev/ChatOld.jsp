<%@page import="at.viswars.chat.ServerListener"%>
<%@page import="at.viswars.*" %>    
<%@page import="at.viswars.Logger.*"%>
<%
session = request.getSession();
String userid = "";
String sessionId = "";

if (!ServerListener.serverConnected()) {
    try {
        ServerListener sl = new ServerListener();
        sl.start();
    } catch (Exception e) {

    }
}

DebugBuffer.addLine(DebugBuffer.DebugLevel.UNKNOWN,"Session ID on Chat.jsp: " + session.getId());

if (session.getAttribute("userId") != null) {
    userid = (String)session.getAttribute("userId");
    sessionId = session.getId();
}

StringBuffer xmlUrlBuffer = request.getRequestURL();

//Logger.getLogger().write("RequestURL->"+request.getRequestURL());

String xmlUrl = "";
int slashCounter = 0;

for (int i=0;i<xmlUrlBuffer.length();i++) {
    char currChar = xmlUrlBuffer.charAt(i);
    if (currChar == ("/".toCharArray())[0]) slashCounter++;
    
    xmlUrl += currChar;
    if (slashCounter == 4) break;
}
%>

<HTML>
    <TITLE>
    </TITLE>
    <BODY BGCOLOR="black">   
    <CENTER>
<applet code="Chat.AppletMain" width="640" height="600" alt="" archive="applet/DDChat.jar,applet/swing-layout-1.0.jar">
    <param name="userid" value="<%= userid %>">
    <param name="sessionId" value="<%= session.getId() %>">
    <param name="checkUrl" value="<%= xmlUrl %>checkSession.jsp"/>
</applet>
</CENTER>
    </BODY>
</HTML>