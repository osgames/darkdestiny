<%@page import="at.viswars.*" %>
<%@page import="at.viswars.databuffer.*" %>
<%@page import="at.viswars.construction.*" %>
<%
session = request.getSession();
String userId = (String)session.getAttribute("userId");
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int orderId = 0;
int fromType = 0;

if (request.getParameter("oId") != null) {
    orderId = Integer.parseInt(request.getParameter("oId"));
}

if (request.getParameter("type") != null) {
    fromType = Integer.parseInt(request.getParameter("type"));
}

// Check for permission
ProdOrderBufferEntry poe = ProdOrderBuffer.getOrderEntry(orderId);
if (poe.getUserId() != Integer.parseInt(userId)) {
    // Lead back to previous page
%>

<%
}

DeconstructionFunctions df = new DeconstructionFunctions(GameConstants.DESTROY_BUILDING_ORDER);

BuildingDeconstructionRessourceCost c = df.findForOrderDeconstruction(orderId, planetId, Integer.parseInt(userId));
int queueIdent = df.getQueueIdent(Integer.parseInt(userId)); 
ConfirmBuffer.getParameters(queueIdent).getParMap().put("fromType", fromType);
%>
        <script language="Javascript">
            var creditPer = <%= c.getCredits() %>;
<%
    for (RessourceEntry re : RessourceBuffer.getAllStorableRessources()) {
        if (c.getRess(re.getRessourceId()) > 0) {
            out.write("var " + re.getName() + "Per = " + c.getRess(re.getRessourceId()) + ";");
        }
    }
%>
            var maxCount = <%= c.getCount() %>;
            
            var actCount = 1;
            
            var totalCredit = creditPer;
<%
    for (RessourceEntry re : RessourceBuffer.getAllStorableRessources()) {
        if (c.getRess(re.getRessourceId()) > 0) {
            out.write("var total" + re.getName() + " = " + re.getName() + "Per;");
        }
    }
%>            
            
            function calcTotal() {
                totalCredit = actCount * creditPer;
<%
        for (RessourceEntry re : RessourceBuffer.getAllStorableRessources()) {
            if (c.getRess(re.getRessourceId()) > 0) {
                out.write("total" + re.getName() + " = actCount * " + re.getName() + "Per;");
            }
        }
%>                                   
        if (totalCredit < 0) {
            document.getElementById("credit").innerHTML = number_format(totalCredit*-1,0) + ' Credits';
        } else {
            document.getElementById("credit").innerHTML = number_format(totalCredit,0) + ' Credits';            
        }
<%
    for (RessourceEntry re : RessourceBuffer.getAllStorableRessources()) {
        if (c.getRess(re.getRessourceId()) > 0) {
            out.write("document.getElementById(\""+re.getName()+"\").innerHTML = number_format(total"+re.getName()+",0);");
        }
    }
%>
            }
        </script>
        <FORM method='post' action='main.jsp?page=confirmAction&qi=<%= queueIdent %>' name='confirm' >
        <TABLE width="80%" style="font-size:13px"><TR>
        <TD colspan=2 align="center">Abbrechen des Konstruktionsauftrags f&uuml;r dieses Geb&auml;ude muss best&auml;tigt werden.<BR><BR></TD></TR>
        <TR valign="middle">
        <TD width="200">Geb&auml;udetyp:</TD><TD><%= c.getName() %></TD></TR>
        <TR valign="middle"><TD>Planet:</TD><TD><%= planetId %></TD></TR>
        <TR valign="middle"><TD>Anzahl:</TD><TD><INPUT name="destroyCount" id="destroyCount" value="1" SIZE="1" MAXLENGTH="3" ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();"> / <%= c.getCount() %><BR></TD></TR>
        <TR valign="top"><%
            if (c.getCredits() < 0) { %>
            <TD>R&uuml;ckverg&uuml;tung:</TD><TD><DIV id="credit"> <%= FormatUtilities.getFormattedNumber(Math.abs(c.getCredits())) %></DIV><BR></TD>
         <% } else { %>
                <TD>Kosten:</TD><TD><DIV id="credit"> <%= FormatUtilities.getFormattedNumber(c.getCredits()) %></DIV><BR></TD>
         <% }
      %></TR>
        
            <TR>
                <TD align="center" colspan=2 class="blue2"><B>Ressourcenr&uuml;ckverg&uuml;tung</B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B>Ressource</B></TD>
                <TD class="blue2"><B>Menge</B></TD>                
            </TR>
<%
    for (RessourceEntry re : RessourceBuffer.getAllStorableRessources()) {
        if (c.getRess(re.getRessourceId()) > 0) {
            out.write("<TR><TD>"+re.getName()+"</TD><TD><DIV id=\""+re.getName()+"\">" + FormatUtilities.getFormattedNumber(c.getRess(re.getRessourceId())) + "</DIV></TD></TR>");
        }
    }
%>
        <TR><TD colspan=2 align="CENTER"><BR><INPUT type="submit" name="Ok" value="Ok"></TD></TR>
        </TABLE>                    
        </FORM>        
