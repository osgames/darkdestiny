package at.viswars.title;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.model.Chassis;
import at.viswars.model.ConditionToTitle;
import at.viswars.model.ShipDesign;
import at.viswars.utilities.TitleUtilities;
import java.util.ArrayList;


public class TheExplorer extends AbstractTitle {


    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
	public boolean check(int userId) {

        for (ShipDesign sd : (ArrayList<ShipDesign>) sdDAO.findByUserId(userId)) {
            if (sd.getChassis() == Chassis.ID_FRIGATE) {
            TitleUtilities.incrementCondition(ConditionToTitle.EXPLORER_FRIGATEDESIGN, userId);
                break;
            }
        }
		return true;
	}
}
