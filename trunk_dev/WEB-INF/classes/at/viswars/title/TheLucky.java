package at.viswars.title;

import at.viswars.DebugBuffer;
import at.viswars.model.PlayerPlanet;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.service.Service;

public class TheLucky extends AbstractTitle {

    public boolean check(int userId) {
        for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)) {


            PlanetCalculation pc = null;
            ExtPlanetCalcResult epcr = null;
            if (pp != null) {
                try {
                    pc = new PlanetCalculation(pp.getPlanetId());
                    epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
                } catch (Exception e) {
                    DebugBuffer.error("Error in Titlecondition " + e);

                }
                if (epcr.getMaxPopulation() > 15000000000l) {
                    return true;
                }
            }
        }
        return false;
    }
}
