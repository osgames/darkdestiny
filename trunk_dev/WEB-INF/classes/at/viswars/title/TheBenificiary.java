package at.viswars.title;

import at.viswars.model.CreditDonation;
import at.viswars.service.Service;


public class TheBenificiary extends AbstractTitle {

	public boolean check(int userId) {
            for(CreditDonation cd : Service.creditDonationDAO.findByToUserId(userId)){
                if(cd.getAmount() > 500000000l){
                    return true;
                }
            }
            return false;
	}
}
