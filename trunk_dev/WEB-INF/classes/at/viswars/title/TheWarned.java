package at.viswars.title;

import at.viswars.StarMapInfo;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.SystemDAO;
import at.viswars.model.Construction;
import at.viswars.model.PlayerPlanet;
import at.viswars.service.ResearchService;
import at.viswars.view.html.a;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

public class TheWarned extends AbstractTitle {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

    public boolean check(int userId) {
        if (!ResearchService.isResearched(userId, 42)) {
            return false;
        } else {
            ArrayList<at.viswars.model.System> hyperSystems = findHyperscanners(userId);
            Area hyperArea = new Area();
            for (at.viswars.model.System s : hyperSystems) {
                Ellipse2D currEllipse = new Ellipse2D.Double(s.getX() - Math.round(StarMapInfo.HYPERSCANNER_RANGE / 2d), s.getY() - Math.round(StarMapInfo.HYPERSCANNER_RANGE / 2d), StarMapInfo.HYPERSCANNER_RANGE * 2, StarMapInfo.HYPERSCANNER_RANGE * 2);

                hyperArea.add(new Area(currEllipse));
            }
            double surface = 0d;
            Rectangle rec = hyperArea.getBounds();
            for (int i = (int) rec.getMinX(); i <= (int) rec.getMaxX(); i++) {
                for (int j = (int) rec.getMinY(); j < (int) rec.getMaxY(); j++) {
                    if (hyperArea.contains(i, j)) {
                        surface += 1d;
                    }
                }

            }
            if (surface > 1000000000) {
                return true;
            }
            return false;
        }
    }

    private static ArrayList<at.viswars.model.System> findHyperscanners(int userId) {
        ArrayList<at.viswars.model.System> hyperscanners = new ArrayList<at.viswars.model.System>();
        ArrayList<PlayerPlanet> pps = ppDAO.findAll();

        for (PlayerPlanet pp : pps) {
            if (pp.getUserId() != userId) {

                continue;
            }
            if (pcDAO.findBy(pp.getPlanetId(), Construction.ID_HYPERSPACESCANNER) != null) {

                if (hyperscanners == null) {
                    hyperscanners = new ArrayList<at.viswars.model.System>();
                }
                hyperscanners.add(sDAO.findById(pDAO.findById(pp.getPlanetId()).getSystemId()));

            }

        }
        return hyperscanners;


    }
}
