package at.viswars.title;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.model.PlayerPlanet;


public class TheTyrant extends AbstractTitle {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
	public boolean check(int userId) {
            for(PlayerPlanet pp : ppDAO.findByUserId(userId)){
                if(pp.getTax() >= 60){
                    return true;
                }
            }
		return false;
	}
}
