package at.viswars.title;

import at.viswars.enumeration.EDiplomacyRelationType;
import at.viswars.model.DiplomacyRelation;
import at.viswars.model.DiplomacyType;
import at.viswars.service.Service;


public class ThePolitician extends AbstractTitle {

	public boolean check(int userId) {
            int count = 0;
            for(DiplomacyRelation dr : Service.diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_USER, false)){
                if(dr.getDiplomacyTypeId() == DiplomacyType.NAP || dr.getDiplomacyTypeId() == DiplomacyType.TREATY){
                    count++;
                }
            }
            for(DiplomacyRelation dr : Service.diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_ALLIANCE, false)){
                if(dr.getDiplomacyTypeId() == DiplomacyType.NAP || dr.getDiplomacyTypeId() == DiplomacyType.TREATY){
                    count++;
                }
            }
            if(count >= 3){
                return true;
            }else{
                return false;
            }
	}
}
