package at.viswars.title;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.model.Planet;
import at.viswars.viewbuffer.HomeSystemBuffer;


public class SystemLord extends AbstractTitle {


    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

	public boolean check(int userId) {
            at.viswars.model.System s = HomeSystemBuffer.getForUser(userId);
            if(s == null) return false;
            for(Planet p : pDAO.findBySystemId(s.getId())){
                if(ppDAO.findByPlanetId(p.getId()) == null || ppDAO.findByPlanetId(p.getId()).getUserId() != userId){
                    return false;
                }
            }
		return true;
	}

}
