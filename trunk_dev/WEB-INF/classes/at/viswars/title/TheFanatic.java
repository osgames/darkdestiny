package at.viswars.title;

import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import at.viswars.service.Service;
import java.util.HashSet;


public class TheFanatic extends AbstractTitle {

	public boolean check(int userId) {
            HashSet<Integer> systemCount = new HashSet<Integer>();
            for(PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)){
                Planet p = Service.planetDAO.findById(pp.getPlanetId());
                if(!systemCount.contains(p.getSystemId())){
                    systemCount.add(p.getSystemId());
                }
            }
            if(systemCount.size() > 100){
                return true;
            }else{
                return false;
            }
	}
}
