package at.viswars.title.condition;

import at.viswars.enumeration.EConditionType;
import at.viswars.enumeration.ETitleComparator;


public class CDBComparator extends AbstractCondition {

	private ParameterEntry value;
	private ParameterEntry comparator;

	public CDBComparator(ParameterEntry value, ParameterEntry comparator){
		this.value = value;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
          	return checkVsDatabase(userId, conditionToTitleId, value.getParamValue().getValue(), value.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));
	}
}
