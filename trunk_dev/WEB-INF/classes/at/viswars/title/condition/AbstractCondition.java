/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.title.condition;

import at.viswars.DebugBuffer;
import at.viswars.dao.ConditionToTitleDAO;
import at.viswars.dao.ConditionToUserDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.enumeration.EConditionType;
import at.viswars.enumeration.ETitleComparator;
import at.viswars.model.ConditionToUser;

/**
 *
 * @author Aion
 */
public abstract class AbstractCondition implements ITitleCondition {

    private static ConditionToTitleDAO ctDAO = (ConditionToTitleDAO) DAOFactory.get(ConditionToTitleDAO.class);
    private static ConditionToUserDAO cuDAO = (ConditionToUserDAO) DAOFactory.get(ConditionToUserDAO.class);

    public boolean checkVsDatabase(int userId, int conditionToTitleId, Object obj1, EConditionType type, ETitleComparator comparator) {

        ConditionToUser ctu = cuDAO.findBy(conditionToTitleId, userId);
        if (ctu != null) {
            if (!ctu.getValue().equals("")) {
                return compare(obj1, Integer.valueOf(ctu.getValue()), type, comparator);
            }
        }
        return false;
    }

    protected boolean compare(Object obj1, Object obj2, EConditionType type, ETitleComparator comparator) {
        int compareResult = 0;
        
        try {
            if (type == EConditionType.LONG) {
                Long value1 = Long.parseLong((String) obj1);
                Long value2 = (Long) (obj2);
                compareResult = value1.compareTo(value2);
            //   Logger.getLogger().write("Long value : " + value1 + " " + FilterUtilities.getComparatorString(comparator) + " " + value2);
            //   Logger.getLogger().write("Result value : " + compareResult);
            }
            if (type == EConditionType.INTEGER) {
                Integer value1 = Integer.parseInt((String) obj1);
                Integer value2 = (Integer) (obj2);
                compareResult = value1.compareTo(value2);
            }
            if (type == EConditionType.DOUBLE) {
                Double value1 = Double.parseDouble((String) obj1);
                Double value2 = (Double) (obj2);
                compareResult = value1.compareTo(value2);
            }
            if (type == EConditionType.FLOAT) {
                Float value1 = Float.parseFloat((String) obj1);
                Float value2 = (Float) (obj2);
                compareResult = value1.compareTo(value2);
            }
            if (type == EConditionType.BOOLEAN) {
                Boolean value1 = false;
                if (Integer.parseInt(((String) obj1)) == 1) {
                    value1 = true;
                }
                Boolean value2 = (Boolean) (obj2);
                compareResult = value1.compareTo(value2);
            //    Logger.getLogger().write("Boolean value : " + value1 + " " + FilterUtilities.getComparatorString(comparator) + " " + value2);
            //    Logger.getLogger().write("Result value : " + compareResult);

            }
            if ((comparator == ETitleComparator.EQUALS || comparator == ETitleComparator.BIGGEREQUALS || comparator == ETitleComparator.SMALLEREQUALS) && compareResult == 0) {
                return true;
            }
            if ((comparator == ETitleComparator.BIGGER || comparator == ETitleComparator.BIGGEREQUALS) && compareResult == -1) {
                return true;
            }
            if ((comparator == ETitleComparator.SMALLER || comparator == ETitleComparator.SMALLEREQUALS) && compareResult == 1) {
                return true;
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Could not compare " + obj1.toString() + " and " + obj2.toString() + " [type was " + type + "]", e);
        }

        return false;
    }
}
