package at.viswars.title.condition;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.enumeration.ETitleComparator;
import at.viswars.model.PlayerPlanet;

public class CPlanetCount extends AbstractCondition {

    private ParameterEntry planetCountValue;
    private ParameterEntry comparator;
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    public CPlanetCount(ParameterEntry planetCountValue, ParameterEntry comparator) {
        this.planetCountValue = planetCountValue;
        this.comparator = comparator;
    }

    public boolean checkCondition(int userId, int conditionToTitleId) {
        int planetCount = ppDAO.findByUserId(userId).size();

        return compare(planetCountValue.getParamValue().getValue(), planetCount, planetCountValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

    }
}
