package at.viswars.title.condition;

import at.viswars.enumeration.ETitleComparator;


public class CResourcesTransported extends AbstractCondition {

	private ParameterEntry resourceValue;
	private ParameterEntry comparatorValue;

	public CResourcesTransported(ParameterEntry resourceValue, ParameterEntry comparatorValue){
		this.resourceValue = resourceValue;
		this.comparatorValue = comparatorValue;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
	      	return checkVsDatabase(userId, conditionToTitleId, resourceValue.getParamValue().getValue(), resourceValue.getParam().getType(), ETitleComparator.valueOf(comparatorValue.getParamValue().getValue()));
}
}
