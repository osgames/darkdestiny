package at.viswars.title.condition;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.enumeration.ETitleComparator;
import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import java.util.ArrayList;
import java.util.HashMap;

public class CPlanetTypeCount extends AbstractCondition {

    private ParameterEntry typeValue;
    private ParameterEntry countValue;
    private ParameterEntry comparatorValue;
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

    public CPlanetTypeCount(ParameterEntry typeValue, ParameterEntry countValue, ParameterEntry comparatorValue) {
        this.typeValue = typeValue;
        this.countValue = countValue;
        this.comparatorValue = comparatorValue;
    }

    public boolean checkCondition(int userId, int conditionToTitleId) {
        HashMap<String, Integer> landTypes = new HashMap<String, Integer>();
        for(PlayerPlanet pp : (ArrayList<PlayerPlanet>)ppDAO.findByUserId(userId)){
            Planet p = pDAO.findById(pp.getPlanetId());
            if(!p.getLandType().equalsIgnoreCase(typeValue.getParamValue().getValue())){
                continue;
            }
            if(landTypes.containsKey(p.getLandType())){
                landTypes.put(p.getLandType(), landTypes.get(p.getLandType()) + 1);
            }else{
                landTypes.put(p.getLandType(), 1);
            }
        }
        if(!landTypes.containsKey(typeValue.getParamValue().getValue())){
            return false;
        }
        return compare(countValue.getParamValue().getValue(), landTypes.get(typeValue.getParamValue().getValue()), countValue.getParam().getType(), ETitleComparator.valueOf(comparatorValue.getParamValue().getValue()));
    }
}
