/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.title.condition;

import at.viswars.model.ConditionParameter;
import at.viswars.model.ParameterToTitle;

/**
 *
 * @author Aion
 */
public class ParameterEntry {

    public ParameterEntry(ParameterToTitle paramValue, ConditionParameter param) {
        this.paramValue = paramValue;
        this.param = param;
    }

    private ParameterToTitle paramValue;
    private ConditionParameter param;

    /**
     * @return the paramValue
     */
    public ParameterToTitle getParamValue() {
        return paramValue;
    }

    /**
     * @return the param
     */
    public ConditionParameter getParam() {
        return param;
    }

}
