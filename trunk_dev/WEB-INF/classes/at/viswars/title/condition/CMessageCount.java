package at.viswars.title.condition;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.MessageDAO;
import at.viswars.enumeration.ETitleComparator;


public class CMessageCount extends AbstractCondition {

	private ParameterEntry messageCountValue;
	private ParameterEntry comparator;
    private static MessageDAO mDAO = (MessageDAO) DAOFactory.get(MessageDAO.class);

	public CMessageCount(ParameterEntry messageCountValue, ParameterEntry comparator){
		this.messageCountValue = messageCountValue;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
            int msgCount = mDAO.findBySourceUserId(userId).size();

        return compare(messageCountValue.getParamValue().getValue(), msgCount, messageCountValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

	}
}
