package at.viswars.title.condition;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.PlayerTroopDAO;
import at.viswars.enumeration.ETitleComparator;
import at.viswars.model.PlayerTroop;


public class CGroundtroopCount extends AbstractCondition {

	private ParameterEntry groundTroopCountValue;
	private ParameterEntry groundTroopIdValue;
	private ParameterEntry comparator;
private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);

	public CGroundtroopCount(ParameterEntry groundTroopCountValue, ParameterEntry groundTroopIdValue, ParameterEntry comparator){
		this.groundTroopCountValue = groundTroopCountValue;
		this.groundTroopIdValue = groundTroopIdValue;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
            if(ppDAO.findHomePlanetByUserId(userId) == null){
                return false;
            }
            int pId = ppDAO.findHomePlanetByUserId(userId).getPlanetId();
            PlayerTroop pt = ptDAO.findBy(userId, pId, Integer.parseInt(groundTroopIdValue.getParamValue().getValue()));
            if(pt == null)return false;
            long groundTroopCount = pt.getNumber();
        return compare(groundTroopCountValue.getParamValue().getValue(), groundTroopCount, groundTroopCountValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

	}
}
