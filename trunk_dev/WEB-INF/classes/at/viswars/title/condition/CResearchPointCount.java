package at.viswars.title.condition;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerResearchDAO;
import at.viswars.dao.ResearchDAO;
import at.viswars.enumeration.ETitleComparator;
import at.viswars.model.PlayerResearch;
import at.viswars.model.Research;


public class CResearchPointCount extends AbstractCondition {

	private ParameterEntry researchPointValue;
	private ParameterEntry comparator;
 private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
private static ResearchDAO rDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);


	public CResearchPointCount(ParameterEntry researchPointValue, ParameterEntry comparator){
		this.researchPointValue = researchPointValue;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
               int researchPoints = 0;
               for(PlayerResearch pr : prDAO.findByUserId(userId)){
                   Research r = rDAO.findById(pr.getResearchId());
                   if(r == null){
                       continue;
                   }
                   researchPoints += r.getRp();
               }

               return compare(researchPointValue.getParamValue().getValue(), researchPoints, researchPointValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

	}
}
