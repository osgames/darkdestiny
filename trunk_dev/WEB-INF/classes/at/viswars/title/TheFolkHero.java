package at.viswars.title;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.enumeration.EShipType;
import at.viswars.model.Chassis;
import at.viswars.model.ConditionToTitle;
import at.viswars.model.ShipDesign;
import at.viswars.utilities.TitleUtilities;
import java.util.ArrayList;


public class TheFolkHero extends AbstractTitle {


    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
	public boolean check(int userId) {
        for (ShipDesign sd : (ArrayList<ShipDesign>) sdDAO.findByUserId(userId)) {
            if (sd.getChassis() == Chassis.ID_DESTROYER && sd.getType() == EShipType.COLONYSHIP) {
                TitleUtilities.incrementCondition(ConditionToTitle.COLONISATOR_COLONYSHIP, userId);
                break;
            }
        }
		return true;
	}
}
