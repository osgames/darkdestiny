package at.viswars.title;

import at.viswars.dao.ChassisDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.model.Chassis;
import at.viswars.model.ConditionToTitle;
import at.viswars.model.ShipDesign;
import at.viswars.utilities.TitleUtilities;
import java.util.ArrayList;

public class TheTinkerer extends AbstractTitle {

    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);

    public boolean check(int userId) {

        for (ShipDesign sd : (ArrayList<ShipDesign>) sdDAO.findByUserId(userId)) {
            if (sd.getChassis() == Chassis.ID_FIGHTER) {
                TitleUtilities.incrementCondition(ConditionToTitle.TINKERER_FIGHTERDESIGN, userId);
                break;
            }
        }
        return true;
    }
}
