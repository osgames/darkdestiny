package at.viswars.title;

import at.viswars.model.Construction;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.PlayerPlanet;
import at.viswars.service.Service;


public class TheCautious extends AbstractTitle {

	public boolean check(int userId) {
        for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)) {
            PlanetConstruction pc = Service.planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETARY_HUESHIELD);
            PlanetConstruction pc1 = Service.planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETARY_PARATRONSHIELD);
            if(pc != null || pc1 != null){
                return true;
            }
        }
        return false;
	}
}
