package at.viswars.title;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.model.PlayerPlanet;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.service.OverviewService;

public class TheGenerous extends AbstractTitle {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    public boolean check(int userId) {
        PlayerPlanet pp = ppDAO.findHomePlanetByUserId(userId);
        if(pp == null) return false;
        int planetId = pp.getPlanetId();
        PlanetCalculation pc = OverviewService.PlanetCalculation(planetId);
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        if(userId == 2){
            //Logger.getLogger().write((pp.getPopulation() - 10) + " > " + )
        }
        if (pp.getPopulation() > (epcr.getMaxPopulation() - 10000000)) {
            return true;
        } else {
            return false;
        }
    }
}
