package at.viswars.title;

import at.viswars.DebugBuffer;
import at.viswars.model.PlayerPlanet;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.service.Service;

public class TheBillionaire extends AbstractTitle {

    public boolean check(int userId) {
        long income = 0;
        for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)) {


            PlanetCalculation pc = null;
            ExtPlanetCalcResult epcr = null;
            if (pp != null) {
                try {
                    pc = new PlanetCalculation(pp.getPlanetId());
                    epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
                    income += epcr.getTaxIncome();
                } catch (Exception e) {
                    DebugBuffer.error("Error in Titlecondition " + e);
                }
            }
        }
        if (income > 25000000l) {
            return true;
        } else {
            return false;
        }
    }
}
