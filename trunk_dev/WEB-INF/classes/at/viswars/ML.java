/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars;

import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.model.Language;
import at.viswars.service.Service;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 *
 * @author Eobane
 */
public class ML extends Service {

    private static HashMap<Integer, Locale> locales;

    public static Language getLanguage(int userId) {
        return Service.languageDAO.findBy(Service.userDAO.findById(userId).getLocale());
    }

    public static String getQuestMLStr(String key, Locale l) {
        return getMLStr("Quest", key, l);
    }

    public static String getQuestMLStr(String key, int userId) {

        return getMLStr("Quest", key, userId);
    }

    public static String getAtlanMLStr(String key, Locale l) {
        return getMLStr("Atlan", key, l);
    }

    public static String getAtlanMLStr(String key, int userId) {

        return getMLStr("Atlan", key, userId);
    }

    public static String getMLStr(String propertiesFile, String key, final Locale l) {
        String data = key;
        try {

            if(propertiesFile == null){
                propertiesFile = "Bundle";
            }
            if (data.equals(key) || data == null) {
                try {

                    ResourceBundle rb = ResourceBundle.getBundle(propertiesFile,
                            new ResourceBundle.Control() {
                        @Override
                        public ResourceBundle newBundle(String baseName,
                                Locale locale,
                                String format,
                                ClassLoader loader,
                                boolean reload)
                                throws IllegalAccessException,
                                InstantiationException,
                                IOException {

                            if (baseName == null || locale == null
                                    || format == null || loader == null) {
                                throw new NullPointerException();
                            }
                            ResourceBundle bundle = null;
                            String bundleName = toBundleName(baseName, l);
                            format = "properties";
                            String resourceName = toResourceName(bundleName, format);
                            InputStream stream = null;
                            URL url = loader.getResource(resourceName);
                            if (url != null) {
                                URLConnection connection = url.openConnection();
                                if (connection != null) {
                                    // Disable caches to get fresh data for
                                    // reloading.
                                    connection.setUseCaches(false);
                                    stream = connection.getInputStream();
                                }
                            }

                            bundle = new PropertyResourceBundle(stream);

                            return bundle;
                        }
                    });
                    data = rb.getString(key);
                    return data;
                    // data = java.util.ResourceBundle.getBundle("Atlan", l).getString(key);
                } catch (Exception e) {
                }
            }
            if (data == null || data.equals(key)) {
                System.err.println("Autschi");
                data = java.util.ResourceBundle.getBundle("Bundle", l).getString(key);
            }
        } catch (Exception e) {
            DebugBuffer.trace("Error beim holen eines " + propertiesFile + "-Multilanguagekeys: " + e + " key : " + key);
            return "e:" + data;
        }
        return data;
    }

    public static String getMLStr(String propertiesFile, String key, int userId) {

        Locale l = locales.get(userId);
        String data = key;
        try {

            if (data.equals(key) || data == null) {
                try {

                    ResourceBundle rb = ResourceBundle.getBundle(propertiesFile,
                            new ResourceBundle.Control() {
                        @Override
                        public ResourceBundle newBundle(String baseName,
                                Locale locale,
                                String format,
                                ClassLoader loader,
                                boolean reload)
                                throws IllegalAccessException,
                                InstantiationException,
                                IOException {

                            if (baseName == null || locale == null
                                    || format == null || loader == null) {
                                throw new NullPointerException();
                            }
                            ResourceBundle bundle = null;
                            String bundleName = toBundleName(baseName, Locale.getDefault());
                            format = "properties";
                            String resourceName = toResourceName(bundleName, format);
                            InputStream stream = null;
                            URL url = loader.getResource(resourceName);
                            if (url != null) {
                                URLConnection connection = url.openConnection();
                                if (connection != null) {
                                    // Disable caches to get fresh data for
                                    // reloading.
                                    connection.setUseCaches(false);
                                    stream = connection.getInputStream();
                                }
                            }

                            bundle = new PropertyResourceBundle(stream);

                            return bundle;
                        }
                    });
                    data = rb.getString(key);
                    return data;
                    // data = java.util.ResourceBundle.getBundle("Atlan", l).getString(key);
                } catch (Exception e) {
                }
            }
            if (data.equals(key) || data == null) {
                data = java.util.ResourceBundle.getBundle("Bundle", l).getString(key);
            }
        } catch (Exception e) {
            DebugBuffer.trace("Error beim holen eines" + propertiesFile + "-Multilanguagekeys:" + e + " key : " + key);
            return "e:" + data;
        }
        // return "|"+data;
        return data;
    }

    public static String getMLStr(String key, Locale l) {
        String data = key;
        try {

            data = java.util.ResourceBundle.getBundle("Bundle", l).getString(key);
        } catch (Exception e) {
            DebugBuffer.warning("Error beim holen eines Multilanguagekeys: " + e + " key : " + key);
            return "e:" + data;
        }
        // return "|"+data;
        return data;
    }
    /*@Deprecated
     public static String getMLStr(String key, Locale locale) {
     String data = key;
     try {

     if (locale.toString().startsWith("de")) {
     data = java.util.ResourceBundle.getBundle("Bundle", locale).getString(key);
     } else {
     data = java.util.ResourceBundle.getBundle("Bundle", Locale.US).getString(key);
     }
     } catch (Exception e) {
     DebugBuffer.warning("Error beim holen eines Multilanguagekeys: " + e + " key : " + key);
     return "e:" + data;
     }
     return data;
     //return data;
     }*/

    public static void removeLocale(int userId) {
        try {
            locales.remove(userId);
        } catch (Exception e) {
        }
    }

    public static Locale getLocale(int userId) {
        if (locales == null) {

            locales = new HashMap<Integer, Locale>();

        }
        Locale l = locales.get(userId);
        if (l == null) {
            loadLocaleForUser(userId);
        }
        return locales.get(userId);

    }

    private static void loadLocaleForUser(int userId) {
        //Holen des Locales vom User
        String userLocale = userDAO.findById(userId).getLocale();
        //Suchen der dazugehörigen Sprache
        Language tempLang = Service.languageDAO.findBy(userLocale);
        //Wenn es eine MasterId gibt wird diese Sprache verwendet
        if (tempLang.getId() != tempLang.getMasterLanguageId()) {
            tempLang = Service.languageDAO.findById(tempLang.getMasterLanguageId());
            userLocale = tempLang.getLanguage() + "_" + tempLang.getCountry();
        }

        String language = userLocale.substring(0, userLocale.indexOf("_"));
        String country = userLocale.substring(userLocale.indexOf("_") + 1, userLocale.length());

        Locale l = new Locale(language, country);

        locales.put(userId, l);
    }

    public static String getLocaleString(int userId) {
        Locale l = locales.get(userId);

        return l.getLanguage() + "_" + l.getCountry();
    }

    public static String getMLStr(String key, int userId) {
        return getMLStr(key, userId, "");
    }

    public static String getMLStr(String key, int userId, Object... params) {
        String data = key;
        boolean debug = false;
        Locale l = null;
        try {
            if (locales == null) {
                locales = new HashMap<Integer, Locale>();
            }
            l = locales.get(userId);
            if (debug) {
                DebugBuffer.addLine(DebugLevel.WARNING, "MULTILANGUAGE - " + "l : " + l);
            }
            if (l == null) {
                loadLocaleForUser(userId);
                l = locales.get(userId);
            }

            if (debug) {
                DebugBuffer.addLine(DebugLevel.WARNING, "Data b4 - " + data);
            }
            data = java.util.ResourceBundle.getBundle("Bundle", l).getString(key);
            if (debug) {
                DebugBuffer.addLine(DebugLevel.WARNING, "Data after fetch - " + data);
            }
        } catch (Exception e) {
            DebugBuffer.warning("Error beim holen eines Multilanguagekeys: " + e + " key : " + key + " for User : " + userId + " Locale : " + l);
            if (l != null) {
                DebugBuffer.warning("locale-coutner" + l.getCountry() + " locale-language : " + l.getLanguage());
            }
            // e.printStackTrace();
            return "e:" + data;
        }
        // return "|"+data;

        int counter = 0;
        for (Object param : params) {
            counter++;
            data = data.replace("%" + counter, param.toString());
        }

        return data;
    }

    public static void generateJS(String path, String file) {

        URL u = ML.class.getResource("/db.properties");

        String loc = path;
        String loc2 = GameConfig.getInstance().getSrcPath() + "\\java\\";
        String jsFile = file;
        if (path == null) {
            loc = u.toString().replace("WEB-INF/classes/db.properties", "java").substring(6);
            loc = loc.replace("root/", "");
        }
        if (jsFile == null) {
            jsFile = "translate.js";
        }
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "[Javascript translations] Creating " + loc + "/" + jsFile);

            File f = new File(loc, "/" + jsFile);
            if (f.exists()) {
                f.delete();
                f = new File(loc, jsFile);
            }

            Logger.getLogger().write(Logger.LogLevel.INFO, "[Javascript translations] Creating " + loc2 + "/" + jsFile);

            File f2 = new File(loc2, jsFile);
            if (f2.exists()) {
                f2.delete();
                f2 = new File(loc2, jsFile);
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(f));
            BufferedWriter writer2 = new BufferedWriter(new FileWriter(f2));

            for (Language l : languageDAO.findAll()) {
                Properties properties = new Properties();
                writer.write("var ML_" + l.getLanguage() + "_" + l.getCountry() + " = [];");
                writer.newLine();
                properties.load(ML.class.getResourceAsStream("/" + l.getPropertiesfile()));
                for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                    String key = (String) entry.getKey();
                    String value = (String) entry.getValue();

                    if (key.startsWith("js_")) {
                        writer.write("ML_" + l.getLanguage() + "_" + l.getCountry() + "['" + key.substring(3) + "']= '" + value + "';");
                        writer.newLine();
                    }
                }
            }

            for (Language l : languageDAO.findAll()) {
                Properties properties = new Properties();
                writer2.write("var ML_" + l.getLanguage() + "_" + l.getCountry() + " = [];");
                writer2.newLine();
                properties.load(ML.class.getResourceAsStream("/" + l.getPropertiesfile()));
                for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                    String key = (String) entry.getKey();
                    String value = (String) entry.getValue();

                    if (key.startsWith("js_")) {
                        writer2.write("ML_" + l.getLanguage() + "_" + l.getCountry() + "['" + key.substring(3) + "']= '" + value + "';");
                        writer2.newLine();
                    }
                }
            }
            /*
             for(Language l : languageDAO.findAll()){
             Properties properties = new Properties();
             writer.write("data['" + l.getLanguage() + "'] = new Array();");
             writer.newLine();
             properties.load(ML.class.getResourceAsStream("/" + l.getPropertiesfile()));
             for(Map.Entry<Object, Object> entry : properties.entrySet()){
             String key = (String)entry.getKey();
             String value = (String)entry.getValue();

             if(key.startsWith("js_")){
             writer.write("data['" + l.getLanguage() + "']['" + key + "'] = '" + value + "';");
             writer.newLine();
             Logger.getLogger().write("key : " + key);
             Logger.getLogger().write("value : " + value);
             }
             }
             }*/
            writer.write("function ML(tag, language){");
            writer.newLine();
            writer.write("try {");
            writer.newLine();
            writer.write("var result = eval('ML_' + language + '[\"' + tag + '\"]');");
            writer.write("if (result == undefined) return \"e:\"+tag;");
            writer.write("return result;");
            writer.write("} catch (err) {");
            writer.newLine();
            writer.write("return \"e:\"+tag");
            writer.newLine();
            writer.write("}");
            writer.newLine();
            writer.newLine();
            writer.write("}");
            writer.close();

            writer2.write("function ML(tag, language){");
            writer2.newLine();
            writer2.write("try {");
            writer2.newLine();
            writer2.write("var result = eval('ML_' + language + '[\"' + tag + '\"]');");
            writer2.write("if (result == undefined) return \"e:\"+tag;");
            writer2.write("return result;");
            writer2.write("} catch (err) {");
            writer2.newLine();
            writer2.write("return \"e:\"+tag");
            writer2.newLine();
            writer2.write("}");
            writer2.newLine();
            writer2.newLine();
            writer2.write("}");
            writer2.close();

        } catch (FileNotFoundException fnfe) {
            DebugBuffer.error("Javascript Translation file cannot be found: " + fnfe.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
