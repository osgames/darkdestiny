/*
 * StartingArea.java
 *
 * Created on 28. April 2007, 13:06
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars;

import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.sql.ResultSet;
import java.sql.Statement;

import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.database.access.DbConnect;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;

/**
 *
 * @author Stefan
 */
public class StartingArea {       
    private static Area baseStarBlocking;
    private static Area dynamicBlocking;
    private static Area totalStartBlocking;        
    
    private static boolean initialized = false;
    
    private static final int BLOCKING_HABITATED = 130;
    private static final int BLOCKING_SYSTEM = 10;    
    
    private StartingArea() {
        
    }    
    
    public static synchronized void initialize() {
        if (!initialized) {
            try {
                baseStarBlocking = new Area();
                dynamicBlocking = new Area();
                totalStartBlocking = new Area();
                buildBase();
                buildDynamic();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while initializing of blocking Terrain",e);
                return;
            }
            merge();
            initialized = true;
            // Logger.getLogger().write("Initializing of blocking compelete");
        }
    }
    
    public static synchronized void initStaticData() {
        if (!initialized) {
            try {
                baseStarBlocking = new Area();
                dynamicBlocking = new Area();
                totalStartBlocking = new Area();
                buildBase();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while initializing of blocking Terrain",e);
                return;
            }
            
            initialized = true;
            // Logger.getLogger().write("Static Initializing of blocking compelete");
        }
    }
    
    public static synchronized void refreshStaticData() {
        try {
            baseStarBlocking = new Area();
            dynamicBlocking = new Area();
            totalStartBlocking = new Area();
            buildBase();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while initializing of blocking Terrain",e);
            return;
        }

        initialized = true;
        // Logger.getLogger().write("Static Initializing of blocking compelete");
    }    
    
    public synchronized static void refresh() {
        if (!initialized) initialize();
        
        try {
            dynamicBlocking = new Area();
            totalStartBlocking = new Area();
            buildDynamic();
            merge();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while initializing of blocking Terrain",e);
            return;            
        }
    }
    
    private static void buildBase() throws Exception {
        Statement stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT id, posx, posy FROM system"); 
        
        int count = 0;
        int partCount = 0;
            
        Area currTmpArea = new Area();        
        
        while (rs.next()) {
            count++;
            partCount++;                        
            
            if (partCount > 300) {
                baseStarBlocking.add(currTmpArea);
                currTmpArea = new Area();
                partCount = 0;
            } 
            
            int x = (int)(rs.getInt(2)-BLOCKING_SYSTEM);
            int y = (int)(rs.getInt(3)-BLOCKING_SYSTEM);

            Ellipse2D currEllipse = new Ellipse2D.Double(x,y,BLOCKING_SYSTEM*2,BLOCKING_SYSTEM*2);                

            DebugBuffer.addLine(DebugLevel.TRACE,"Adding system blocking Area at " + rs.getInt(2) + "/" + rs.getInt(3)+" CurrCount="+count);

            Area currBlocked = new Area(currEllipse);
            currTmpArea.add(currBlocked);                                
        }        
        
        baseStarBlocking.add(currTmpArea);  
    } 
    
    private static void buildDynamic() throws Exception {
        // Logger.getLogger().write("buildDynamic called");
        
        Statement stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT planetID FROM playerplanet WHERE population>=10000000"); 
        while (rs.next()) {
            // Logger.getLogger().write("Planet " + rs.getInt(1) + " found");

            RelativeCoordinate rc;
            AbsoluteCoordinate ac;
            
            try {
                rc = new RelativeCoordinate(0,rs.getInt(1));
                ac = rc.toAbsoluteCoordinate();  
                
                if (ac == null) throw new Exception("Converting Relative Coordinates failed!");
            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR,"Error occured while processing Coordinates for Planet " + rs.getInt(1) + " - Action taken: SKIPPED!");
                continue;
            }
            
            int x = (int)(ac.getX()-BLOCKING_HABITATED);
            int y = (int)(ac.getY()-BLOCKING_HABITATED);

            Ellipse2D currEllipse = new Ellipse2D.Double(x,y,BLOCKING_HABITATED*2,BLOCKING_HABITATED*2);                

            DebugBuffer.addLine(DebugLevel.TRACE,"Adding player blocking Area at ("+ac.getX()+"/"+ac.getY()+")");

            Area currBlocked = new Area(currEllipse);
            dynamicBlocking.add(currBlocked);                
        }        
    }
    
    private static void merge() {
        totalStartBlocking.add(baseStarBlocking);
        totalStartBlocking.add(dynamicBlocking);
    }
    
    public static boolean isBlocked(int x, int y) {        
        return totalStartBlocking.contains(x,y);
    }
    
    public static boolean isBlockedRect(int leftX, int topY, int rightX, int bottomY) {
        return totalStartBlocking.contains(leftX,topY,(rightX-leftX),(bottomY-topY));
    }
}

