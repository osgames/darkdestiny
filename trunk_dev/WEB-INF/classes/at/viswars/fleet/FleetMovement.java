/*
 * FleetMovement.java
 *
 * Created on 25. Juli 2004, 12:12
 */
package at.viswars.fleet;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.GameUtilities;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.dao.*;
import at.viswars.database.framework.exception.TransactionException;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.ELocationType;
import at.viswars.model.Action;
import at.viswars.model.ConditionToTitle;
import at.viswars.model.Planet;
import at.viswars.model.FleetDetail;
import at.viswars.model.FleetFormation;
import at.viswars.model.PlayerFleet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.User;
import at.viswars.model.ViewTable;
import at.viswars.movable.FleetFormationExt;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.requestbuffer.FleetParameterBuffer;
import at.viswars.result.BaseResult;
import at.viswars.service.FleetService;
import at.viswars.ships.ShipUtilitiesNonStatic;
import at.viswars.update.FlightProcessingEntry;
import at.viswars.utilities.TitleUtilities;
import at.viswars.utilities.ViewTableUtilities;
import at.viswars.viewbuffer.HomeSystemBuffer;
import java.util.*;

/**
 *
 * @author  Stefan
 */
public class FleetMovement {

    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static GalaxyDAO gDAO = (GalaxyDAO) DAOFactory.get(GalaxyDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static FleetFormationDAO ffDAO = (FleetFormationDAO) DAOFactory.get(FleetFormationDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static FleetOrderDAO foDAO = (FleetOrderDAO) DAOFactory.get(FleetOrderDAO.class);
    boolean error;
    String errorMess;
    private Map<String, Object> parMap;
    private FleetOrder fo = null;
    public final static int MOVE_FROM_SYSVIEW = 2;
    public final static int MOVE_FROM_FLEETVIEW = 1;
    // Constructor for generating a fleet movement
    //TODO This uses parameters from the JSP-File!
    /*
    public FleetMovement(Map<String, Object> pMap, int userId, int source) {
    this.parMap = pMap;
    this.userId = userId;
    }
    
    // Constructor for finishing a fleet movement
    public FleetMovement(int fleetId) {
    this.fleetId = fleetId;
    }
     */

    public static BaseResult checkFlightParameters(int userId, FleetParameterBuffer fpb) {
        int fleetId;
        boolean targetIsSystem;
        int targetId;
        int destsystem;
        int destplanet;
        int checkType;
        int actionType;
        int preFlightTime;
        double preDistance;
        int retreatFactor;
        boolean retreatLocIsSystem;
        int retreatTo;

        try {
            fleetId = fpb.getFleetId();
            targetIsSystem = fpb.targetIsSystem();
            targetId = fpb.getTarget();
            retreatFactor = fpb.getRetreatValue();
            retreatLocIsSystem = fpb.isRetreatLocSystem();
            retreatTo = fpb.getRetreatTo();
        } catch (Exception e) {
            // DebugBuffer.writeStackTrace("Paramter Error: ", e);      
            String msg = ML.getMLStr("fleetmovement_err_invalidparameter", userId);
            msg = msg.replace("%ERR%", e.getMessage());
            return new BaseResult(msg, true);
        }

        try {
            // CHECK FLEET ORDERS
            ShipUtilitiesNonStatic su = new ShipUtilitiesNonStatic(userId, 0);
            at.viswars.model.FleetOrder foTmp = new at.viswars.model.FleetOrder();
            foTmp.setRetreatFactor(retreatFactor);
            foTmp.setRetreatToType(fpb.isRetreatLocSystem() ? ELocationType.SYSTEM : ELocationType.PLANET);
            foTmp.setRetreatTo(retreatTo);
            
            su.checkFleetOrders(fleetId, fpb.isFleetFormation(), foTmp);
            
            if (su.outMsg != null) {
                return new BaseResult(su.outMsg, true);
            }             
        } catch (Exception e) {
            return new BaseResult(ML.getMLStr("fleetmovement_err_invalidvalues", userId), true);
        }

        at.viswars.model.System targetSystem = null;
        Planet targetPlanet = null;

        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Target is system = " + targetIsSystem + " targetId = " + targetId);

        if (targetIsSystem) {
            targetSystem = sDAO.findById(targetId);
        } else {
            targetPlanet = pDAO.findById(targetId);
            if (targetPlanet != null) {
                targetSystem = sDAO.findById(targetPlanet.getSystemId());
            }
        }
               
        /*
         *
         * @TODO Abfrage ob der User Im Trial Modus ist�// Allianz
         */
        if ((targetSystem == null)) {
            return new BaseResult(ML.getMLStr("fleetmovement_err_novalidsystem", userId), true);
        }
                
        for (Planet p : pDAO.findBySystemId(targetSystem.getId())) {
            PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());

            ArrayList<ViewTable> vtEntries = vtDAO.findByUserAndSystem(userId, targetSystem.getId());
            if (vtEntries.size() != 0) {
                if (pp != null) {
                    if (uDAO.findById(pp.getUserId()).getTrial() && pp.getUserId() != userId) {
                        return new BaseResult(ML.getMLStr("fleetmovement_err_novalidtargettrialmode", userId), true);
                    }
                }
            }
        }

        if (!targetIsSystem && (targetPlanet == null)) {
            return new BaseResult(ML.getMLStr("fleetmovement_err_novalidplanet", userId), true);
        }

        Movable fleet = null;

        if (fpb.isFleetFormation()) {
            try {
                fleet = new FleetFormationExt(fpb.getFleetId());
            } catch (Exception e) {
                return new BaseResult(e.getMessage(), true);
            }
        } else {
            fleet = new PlayerFleetExt(fpb.getFleetId());
        }

        if (fleet == null) {
            return new BaseResult(ML.getMLStr("fleetmovement_err_fleetnotexisting", userId), true);
        }

        PlayerFleetExt pfe = null;
        FleetFormationExt ffe = null;

        boolean invalidAccess = false;

        if (fpb.isFleetFormation()) {
            ffe = (FleetFormationExt) fleet;
            if (ffe.getBase().getUserId() != userId) {
                invalidAccess = true;
            }
        } else {
            pfe = (PlayerFleetExt) fleet;
            if (pfe.getBase().getUserId() != userId) {
                invalidAccess = true;
            }
        }

        if (invalidAccess) {
            return new BaseResult(ML.getMLStr("fleetmovement_err_invalidfleet", userId), true);
        }
        if (fleet.isMoving()) {
            return new BaseResult(ML.getMLStr("fleetmovement_err_invalidfleet", userId), true);
        }

        Logger.getLogger().write("Fleetspeed : " + fleet.getSpeed());
        if ((fleet.getRelativeCoordinate().getSystemId() != targetSystem.getId())
                && !fleet.canFlyInterstellar()) {

            Logger.getLogger().write("Part 1");
            return new BaseResult(ML.getMLStr("fleetmovement_err_interstellarflightnotpossible", userId), true);
        } else {
            if (fleet.getRelativeCoordinate().getSystemId() == targetSystem.getId()) {
                Logger.getLogger().write("Part 2");
                fpb.setParameter("timeToTarget", 1);
                fpb.setParameter("distance", 0);
            } else {
                Logger.getLogger().write("Part 3");
                AbsoluteCoordinate ac1 = fleet.getAbsoluteCoordinate();
                AbsoluteCoordinate ac2 = new AbsoluteCoordinate(targetSystem.getX(), targetSystem.getY());

                double distance = ac1.distanceTo(ac2);
                Logger.getLogger().write("Part 3.1");
                fpb.setParameter("timeToTarget", (int) Math.ceil(distance / fleet.getSpeed()));
                fpb.setParameter("distance", distance);
                Logger.getLogger().write("Part 3.2 (Distance is " + distance + ")");
            }
        }

        // Check if target and start are in valid galaxy
        int galaxyStart = fleet.getRelativeCoordinate().getGalaxyId();
        int galaxyTarget = new RelativeCoordinate(targetSystem.getId(),0).getGalaxyId();
        
        if (galaxyStart != galaxyTarget) {
            if (!gDAO.findById(galaxyStart).isIntergalacticFlight() || !gDAO.findById(galaxyTarget).isIntergalacticFlight()) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_galaxylocked", userId), true);
            }
        }
        
        fpb.setParameter("targetSystem", targetSystem);
        fpb.setParameter("targetPlanet", targetPlanet);
        Logger.getLogger().write("Fleet : " + fleet);
        fpb.setParameter("fleet", fleet);

        User u = uDAO.findById(userId);
        if (u.getTrial()) {
            fpb.setParameter(FleetParameterBuffer.TRIAL_WARNING, "true");
        }
        if (FleetService.findRunningScan(userId, fleetId) != null) {
            fpb.setParameter(FleetParameterBuffer.SCAN_WARNING, "true");
        }
        return new BaseResult(ML.getMLStr("fleetmovement_msg_allok", userId), false);
    }

    public static BaseResult generateFlight(int userId, FleetParameterBuffer fpb) throws Exception {
        PlayerFleetExt pfe = null;
        FleetFormationExt ffe = null;
        Movable fleet = null;

        ArrayList<PlayerFleetExt> fleetsToMove = new ArrayList<PlayerFleetExt>();
        float speed = 0;

        if (fpb.isFleetFormation()) {
            ffe = (FleetFormationExt) fpb.getParameter("fleet");
            fleet = ffe;
            speed = (float) ffe.getSpeed();
            fleetsToMove.addAll(ffe.getParticipatingFleets());
        } else {
            pfe = (PlayerFleetExt) fpb.getParameter("fleet");
            Logger.getLogger().write("pfe : " + pfe);
            fleet = pfe;
            speed = (float) pfe.getSpeed();
            fleetsToMove.add(pfe);
        }

        at.viswars.model.System targetSystem = (at.viswars.model.System) fpb.getParameter("targetSystem");
        Planet targetPlanet = (Planet) fpb.getParameter("targetPlanet");
        int timeToTarget = 0;

        Logger.getLogger().write("SYSTEM COORDINATE: " + fleet.getRelativeCoordinate().getSystemId() + " TARGETSYSTEM COORINATE: " + targetSystem.getId());
        if (fleet.getRelativeCoordinate().getSystemId() == targetSystem.getId()) {
            timeToTarget = 1;
        } else {
            AbsoluteCoordinate ac1 = fleet.getAbsoluteCoordinate();
            AbsoluteCoordinate ac2 = new AbsoluteCoordinate(targetSystem.getX(), targetSystem.getY());

            if ((ac1 == null) || (ac2 == null)) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Something was null for Fleet " + pfe.getName() + " (" + pfe.getId() + ") (AC1=" + ac1 + " AC2=" + ac2 + ")");
            }

            double distance = ac1.distanceTo(ac2);
            timeToTarget = (int) Math.ceil(distance / fleet.getSpeed());
        }

        TransactionHandler th = TransactionHandler.getTransactionHandler();
        th.startTransaction();

        String error = null;

        if (fpb.isScanWarning()) {
            try {
                ArrayList<Action> result = new ArrayList<Action>();

                Action a = new Action();
                a.setType(EActionType.SCAN_PLANET);
                a.setUserId(fpb.getUserId());
                a.setFleetId(fpb.getFleetId());
                result.addAll(aDAO.find(a));
                a.setType(EActionType.SCAN_SYSTEM);
                result.addAll(aDAO.find(a));

                if (result.size() != 1) {
                    DebugBuffer.error("No unique scanning entry found for aborting of scanning action for fleet " + fpb.getFleetId());
                } else {
                    aDAO.remove(result.get(0));
                }
            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.WARNING, "Konnte Scanaction nicht abbrechen f�r Flotte : " + fpb.getFleetId());
            }
        }

        // Get retreat information
        int retreatPercentage = fpb.getRetreatValue();
        boolean retreatLocIsSystem = fpb.isRetreatLocSystem();
        int retreatLoc = fpb.getRetreatTo();

        boolean retreatSet = false;

        try {
            for (PlayerFleetExt actFleet : fleetsToMove) {
                PlayerFleet pf = actFleet.getBase();

                at.viswars.model.FleetDetail fd = new at.viswars.model.FleetDetail();
                fd.setFleetId(pf.getId());
                fd.setStartSystem(pf.getSystemId());
                fd.setStartPlanet(pf.getPlanetId());
                fd.setStartX(actFleet.getAbsoluteCoordinate().getX());
                fd.setStartY(actFleet.getAbsoluteCoordinate().getY());
                fd.setDestSystem(targetSystem.getId());
                if (targetPlanet != null) {
                    fd.setDestPlanet(targetPlanet.getId());
                } else {
                    fd.setDestPlanet(0);
                }
                fd.setDestX(targetSystem.getX());
                fd.setDestY(targetSystem.getY());
                fd.setFlightTime(timeToTarget);
                fd.setSpeed(speed);
                fd.setCommandType(0);
                fd.setCommandTypeId(0);
                if (fpb.getParameter("retreating") != null) {
                    fd.setStartTime(GameUtilities.getCurrentTick2()-1);
                } else {
                    fd.setStartTime(GameUtilities.getCurrentTick2());
                }
                fd = fdDAO.add(fd);

                // Process retreat factor
                if (!retreatSet) {
                    if (retreatPercentage < 100) {
                        at.viswars.model.FleetOrder fo = null;
                        
                        if (fpb.isFleetFormation()) {
                            fo = foDAO.get(fpb.getFleetId(), FleetType.FLEET_FORMATION);
                        } else {
                            fo = foDAO.get(fpb.getFleetId(), FleetType.FLEET);
                        }                        
                        
                        boolean update = true;
                        if (fo == null) {    
                            update = false;
                            
                            fo = new at.viswars.model.FleetOrder();
                            
                            fo.setFleetId(fpb.getFleetId());

                            if (fpb.isFleetFormation()) {
                                fo.setFleetType(FleetType.FLEET_FORMATION);
                            } else {
                                fo.setFleetType(FleetType.FLEET);
                            }                            
                        }                       

                        fo.setRetreatFactor(retreatPercentage);
                        fo.setRetreatToType(retreatLocIsSystem ? ELocationType.SYSTEM : ELocationType.PLANET);
                        fo.setRetreatTo(retreatLoc);
                        
                        if (update) {
                            foDAO.update(fo);
                        } else {
                            foDAO.add(fo);
                        }
                    }
                    
                    retreatSet = true;
                }

                DebugBuffer.addLine(DebugLevel.UNKNOWN, "FleetDetail: " + fd);

                Action a = new Action();
                a.setType(EActionType.FLIGHT);
                a.setRefTableId(fd.getId());
                a.setNumber(0);
                a.setUserId(userId);
                a.setFleetId(fd.getFleetId());
                aDAO.add(a);

                ViewTableUtilities.addOrRefreshSystem(pf.getUserId(), pf.getSystemId(), GameUtilities.getCurrentTick2());

                pf.setLocation(ELocationType.TRANSIT);
                // pf.setStatus(1);
                // pf.setPlanetId(0);
                // pf.setSystemId(0);
                pfDAO.update(pf);

                /*
                 * TASKS/TITLE - START
                 */
                if (HomeSystemBuffer.getForUser(userId).getId() != targetSystem.getId()) {
                    TitleUtilities.incrementCondition(ConditionToTitle.EXPLORER_LEAVEYOURSYSTEM, userId);
                }
                /*
                 * TASKS/TITLE - END
                 */
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("FleetMovement Transaction failed", e);
            error += e.getMessage();
            try {
                th.rollback();
            } catch (TransactionException te2) {
                DebugBuffer.writeStackTrace("Rollback failed", te2);
                error += " >> " + te2.getMessage();
            }
        } finally {
            th.endTransaction();
            if (error != null) {
                return new BaseResult("ERROR: " + error, true);
            }
        }

                /*
                 * TASKS/TITLE - START
                 */
                if (HomeSystemBuffer.getForUser(userId).getId() != targetSystem.getId()) {
                    TitleUtilities.incrementCondition(ConditionToTitle.EXPLORER_LEAVEYOURSYSTEM, userId);
                }
                /*
                 * TASKS/TITLE - END
                 */
        return new BaseResult(ML.getMLStr("fleetmovement_msg_allok", userId), false);
    }

    public static void retreatFleet(FlightProcessingEntry fpe, int userId) {
        try {
            // Get basic data
            FleetDetail fd = fpe.getFleetDetail();
            int fleetId = fd.getFleetId();
            at.viswars.model.FleetOrder fo = foDAO.get(fleetId, FleetType.FLEET);

            // Write fleet to target system
            PlayerFleet pf = pfDAO.findById(fleetId);
            pf.setLocation(ELocationType.SYSTEM,fd.getDestSystem());
            // pf.setSystemId(fd.getDestSystem());
            // pf.setPlanetId(0);
            // pf.setStatus(0);
            pfDAO.update(pf);
            
            // Remove moving and order data
            fdDAO.remove(fd);
            aDAO.remove(fpe.getAction());

            if (fo != null) {
                foDAO.remove(fo);

                PlayerFleetExt pfe = new PlayerFleetExt(pf);

                // Create a FleetParameterBuffer object
                FleetParameterBuffer fpb = new FleetParameterBuffer(userId);
                fpb.setParameter(FleetParameterBuffer.FLEET_ID, ""+fleetId);
                fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, fo.getRetreatToType() == ELocationType.SYSTEM ? "true" : "false");
                fpb.setParameter(FleetParameterBuffer.TARGET_ID, ""+fo.getRetreatTo());
                fpb.setParameter(FleetParameterBuffer.RETREAT_FACTOR, ""+100);
                fpb.setParameter("fleet", pfe);

                RelativeCoordinate rc = null;
                at.viswars.model.System targetSys = null;
                Planet targetPlanet = null;

                if (fo.getRetreatToType() == ELocationType.SYSTEM) {
                    Logger.getLogger().write("GET RETREAT COORdINATES by System");
                    rc = new RelativeCoordinate(fo.getRetreatTo(),0);
                    targetSys = sDAO.findById(rc.getSystemId());
                } else {
                    Logger.getLogger().write("GET RETREAT COORdINATES by Planet");
                    rc = new RelativeCoordinate(0,fo.getRetreatTo());
                    targetSys = sDAO.findById(rc.getSystemId());
                    targetPlanet = pDAO.findById(rc.getPlanetId());
                }

                Logger.getLogger().write("RETREAT TARGETSYSTEM = " + targetSys + " TARGETPLANET = " + targetPlanet);

                fpb.setParameter("targetSystem", targetSys);
                fpb.setParameter("targetPlanet", targetPlanet);
                fpb.setParameter("retreating", true);

                // Generate new Flight Entry
                generateFlight(userId,fpb);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while retreat of Fleet: ", e);
        }
    }

    public static void retreatFleet2(FlightProcessingEntry fpe, int userId) {
        try {
            // Get basic data
            FleetDetail fd = fpe.getFleetDetail();
            int fleetId = fd.getFleetId();
            at.viswars.model.FleetOrder fo = foDAO.get(fleetId, FleetType.FLEET);

            // Write fleet to target system
            PlayerFleet pf = pfDAO.findById(fleetId);
            pf.setLocation(ELocationType.SYSTEM,fd.getDestSystem());
            // pf.setSystemId(fd.getDestSystem());
            // pf.setPlanetId(0);
            // pf.setStatus(0);
            pfDAO.update(pf);
            
            // Remove moving and order data
            fdDAO.remove(fd);
            aDAO.remove(fpe.getAction());

            if (fo != null) {
                foDAO.remove(fo);

                PlayerFleetExt pfe = new PlayerFleetExt(pf);

                // Create a FleetParameterBuffer object
                FleetParameterBuffer fpb = new FleetParameterBuffer(userId);
                fpb.setParameter(FleetParameterBuffer.FLEET_ID, ""+fleetId);
                fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, fo.getRetreatToType() == ELocationType.SYSTEM ? "true" : "false");
                fpb.setParameter(FleetParameterBuffer.TARGET_ID, ""+fo.getRetreatTo());
                fpb.setParameter(FleetParameterBuffer.RETREAT_FACTOR, ""+100);
                fpb.setParameter("fleet", pfe);

                RelativeCoordinate rc = null;
                at.viswars.model.System targetSys = null;
                Planet targetPlanet = null;

                if (fo.getRetreatToType() == ELocationType.SYSTEM) {
                    Logger.getLogger().write("GET RETREAT COORdINATES by System");
                    rc = new RelativeCoordinate(fo.getRetreatTo(),0);
                    targetSys = sDAO.findById(rc.getSystemId());
                } else {
                    Logger.getLogger().write("GET RETREAT COORdINATES by Planet");
                    rc = new RelativeCoordinate(0,fo.getRetreatTo());
                    targetSys = sDAO.findById(rc.getSystemId());
                    targetPlanet = pDAO.findById(rc.getPlanetId());
                }

                Logger.getLogger().write("RETREAT TARGETSYSTEM = " + targetSys + " TARGETPLANET = " + targetPlanet);

                fpb.setParameter("targetSystem", targetSys);
                fpb.setParameter("targetPlanet", targetPlanet);
                fpb.setParameter("retreating", true);

                // Generate new Flight Entry
                generateFlight(userId,fpb);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while retreat of Fleet: ", e);
        }
    }

    public static void retreatFleetFormation(int formationId, ArrayList<FlightProcessingEntry> fpeList) {
        try {
            // Get basic data
            at.viswars.model.FleetOrder fo = foDAO.get(formationId, FleetType.FLEET_FORMATION);
            foDAO.remove(fo);

            for (FlightProcessingEntry fpe : fpeList) {
                FleetDetail fd = fpe.getFleetDetail();
                int fleetId = fd.getFleetId();

                // Write fleet to target system
                PlayerFleet pf = pfDAO.findById(fleetId);
                pf.setLocation(ELocationType.SYSTEM,fd.getDestSystem());
                // pf.setSystemId(fd.getDestSystem());
                // pf.setPlanetId(0);
                // pf.setStatus(0);
                pfDAO.update(pf);

                // Remove moving and order data
                fdDAO.remove(fd);
                aDAO.remove(fpe.getAction());                
            }

            FleetFormation ff = ffDAO.findById(formationId);
            FleetFormationExt ffe = new FleetFormationExt(formationId);
            // PlayerFleetExt pfe = new PlayerFleetExt(pf);

            // Create a FleetParameterBuffer object
            FleetParameterBuffer fpb = new FleetParameterBuffer(ff.getUserId());
            fpb.setParameter(FleetParameterBuffer.FLEET_ID, "FF"+ff.getId());
            fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, fo.getRetreatToType() == ELocationType.SYSTEM ? "true" : "false");
            fpb.setParameter(FleetParameterBuffer.TARGET_ID, ""+fo.getRetreatTo());
            fpb.setParameter(FleetParameterBuffer.RETREAT_FACTOR, ""+100);
            fpb.setParameter("fleet", ffe);

            RelativeCoordinate rc = null;
            at.viswars.model.System targetSys = null;
            Planet targetPlanet = null;

            if (fo.getRetreatToType() == ELocationType.SYSTEM) {
                Logger.getLogger().write("GET RETREAT COORdINATES by System");
                rc = new RelativeCoordinate(fo.getRetreatTo(),0);
                targetSys = sDAO.findById(rc.getSystemId());
            } else {
                Logger.getLogger().write("GET RETREAT COORdINATES by Planet");
                rc = new RelativeCoordinate(0,fo.getRetreatTo());
                targetSys = sDAO.findById(rc.getSystemId());
                targetPlanet = pDAO.findById(rc.getPlanetId());
            }

            Logger.getLogger().write("RETREAT TARGETSYSTEM = " + targetSys + " TARGETPLANET = " + targetPlanet);

            fpb.setParameter("targetSystem", targetSys);
            fpb.setParameter("targetPlanet", targetPlanet);
            fpb.setParameter("retreating", true);

            // Generate new Flight Entry
            generateFlight(ff.getUserId(),fpb);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while retreat of Fleet: ", e);
        }
    }

    public static BaseResult callBackFleet(int fleetId, int userId) {
        try {
            // Check if fleet is callBackAble

            FleetDetail fd = fdDAO.findByFleetId(fleetId);
            if (fd == null) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_fleetnotflying", userId), true);
            }
            //ResultSet rs = stmt.executeQuery("SELECT startsystem, startplanet, startTime, flightTime, speed, startX, startY, destX, destY FROM fleetdetails WHERE fleetId=" + fleetId);
            if (fd.getStartPlanet() == 0 && fd.getStartSystem() == 0) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_fleetalreadycalledback", userId), true);
            }
            if (pfDAO.findById(fleetId).getUserId() != userId) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_fleetisnotyours", userId), true);
            }

            // Fleet was started this tick immediate return possible
            if (fd.getStartTime() == GameUtilities.getCurrentTick2()) {
                PlayerFleet pf = pfDAO.findById(fleetId);
                if (fd.getStartPlanet() != 0) {
                    pf.setLocation(ELocationType.PLANET,fd.getStartPlanet());
                } else {
                    pf.setLocation(ELocationType.SYSTEM,fd.getStartSystem());
                }
                // pf.setPlanetId(fd.getStartPlanet());
                // pf.setSystemId(fd.getStartSystem());
                // pf.setStatus(0);
                pfDAO.update(pf);
                fdDAO.remove(fd);
                Action a = new Action();
                a.setType(EActionType.FLIGHT);
                a.setFleetId(pf.getId());
                a.setRefTableId(fd.getId());
                ArrayList<Action> aList = aDAO.find(a);
                aDAO.remove(aList.get(0));
            } else {
                // Calc distance which fleet has already done
                int diffX = fd.getDestX() - fd.getStartX();
                int diffY = fd.getDestY() - fd.getStartY();

                double totalDistance = Math.sqrt(Math.pow(diffX, 2d) + Math.pow(diffY, 2d));
                double travelledDistance = Math.min(100d, 100d / fd.getFlightTime() * (GameUtilities.getCurrentTick2() - fd.getStartTime()));

                DebugBuffer.addLine(DebugLevel.TRACE, "Returning fleet " + fd.getFleetId());
                DebugBuffer.addLine(DebugLevel.TRACE, "Travelled distance (%): " + travelledDistance + " Total Distance: " + totalDistance);

                // Get current fleet position
                int currX = (int) (fd.getStartX() + ((diffX / 100d) * travelledDistance));
                int currY = (int) (fd.getStartY() + ((diffY / 100d) * travelledDistance));

                DebugBuffer.addLine(DebugLevel.TRACE, "OldStartLoc: X=" + fd.getStartX() + " Y=" + fd.getStartY());
                DebugBuffer.addLine(DebugLevel.TRACE, "CurrentLoc: X=" + currX + " Y=" + currY);
                DebugBuffer.addLine(DebugLevel.TRACE, "OldTargetLoc: X=" + fd.getDestX() + " Y=" + fd.getDestY());

                // Get distance and calculate flight time
                double newDistance = Math.sqrt(Math.pow(fd.getStartX() - currX, 2d) + Math.pow(fd.getStartY() - currY, 2d));

                float speed = 0.1f;
                if (fd.getSpeed() > 0) {
                    speed = fd.getSpeed();
                }

                DebugBuffer.addLine(DebugLevel.TRACE, "Fly back distance: " + newDistance + " Fleetspeed: " + speed);
                
                int newTravelTime = (int) (newDistance / speed);

                DebugBuffer.addLine(DebugLevel.TRACE, "Travel time: " + newTravelTime);
                
                // Set target = Source
                // delete Relative Coordinates for source
                // Calculate distance and traveltime due to speed
                // Update actions entry
                fd.setDestSystem(fd.getStartSystem());
                fd.setDestPlanet(fd.getStartPlanet());
                fd.setDestX(fd.getStartX());
                fd.setDestY(fd.getStartY());
                fd.setStartSystem(0);
                fd.setStartPlanet(0);
                fd.setStartX(currX);
                fd.setStartY(currY);
                fd.setStartTime(GameUtilities.getCurrentTick2());
                fd.setFlightTime(newTravelTime);
                //     stmt.executeUpdate("UPDATE fleetdetails SET destsystem=startsystem, destplanet=startplanet, destX=startX, destY=startY WHERE fleetId=" + fleetId);
                //    stmt.executeUpdate("UPDATE fleetdetails SET startsystem=0, startplanet=0, startX=" + currX + ", startY=" + currY + ", startTime=" + GameUtilities.getCurrentTick2() + ", flightTime=" + newTravelTime + " WHERE fleetId=" + fleetId);
                fdDAO.update(fd);
                //  stmt.executeUpdate("UPDATE actions SET timeFinished=" + (GameUtilities.getCurrentTick2() + newTravelTime) + " WHERE number=" + fleetId + " AND actionType=6");
                //  stmt.close();
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while callBack of Fleet: ", e);
            return new BaseResult("Error: " + e, true);
        }
        
        return new BaseResult(ML.getMLStr("fleetmovement_msg_allok", userId), false);
    }

    public static BaseResult callBackFleetFormation(int fleetFormationId, int userId) {
        try {
            //Search FleetFormation
            FleetFormation ff = ffDAO.findById(fleetFormationId);
            FleetFormationExt ffe = new FleetFormationExt(fleetFormationId);
            ArrayList<PlayerFleet> pfs = pfDAO.findByFleetFormation(fleetFormationId);

            if (ff.getUserId() != userId) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_notleaderoffleetformation", userId), true);
            }

            boolean calculateValues = true;
            //New Values
            int currX = 0;
            int currY = 0;
            int newTravelTime = 0;
            
            //Search Fleets
            for (PlayerFleet pf : pfs) {
                FleetDetail fd = fdDAO.findByFleetId(pf.getId());

                if (fd == null) {
                    return new BaseResult(ML.getMLStr("fleetmovement_err_fleetformationnotflying", userId), true);
                }
                if (fd.getStartPlanet() == 0 && fd.getStartSystem() == 0) {
                    return new BaseResult(ML.getMLStr("fleetmovement_err_fleetformationalreadycalledback", userId), true);
                }
                //No tick processed yet
                if (fd.getStartTime() == GameUtilities.getCurrentTick2()) {
                    if (fd.getStartPlanet() != 0) {
                        pf.setLocation(ELocationType.PLANET,fd.getStartPlanet());
                    } else {
                        pf.setLocation(ELocationType.SYSTEM,fd.getStartSystem());
                    }                    
                    // pf.setPlanetId(fd.getStartPlanet());
                    // pf.setSystemId(fd.getStartSystem());
                    // pf.setStatus(0);
                    pfDAO.update(pf);
                    fdDAO.remove(fd);

                    Action a = new Action();
                    a.setType(EActionType.FLIGHT);
                    a.setFleetId(pf.getId());
                    a.setRefTableId(fd.getId());
                    ArrayList<Action> aList = aDAO.find(a);
                    aDAO.remove(aList.get(0));
                } else {
                    //Calculate just once
                    if (calculateValues) {
                        int diffX = fd.getDestX() - fd.getStartX();
                        int diffY = fd.getDestY() - fd.getStartY();

                        double totalDistance = Math.sqrt(Math.pow(diffX, 2d) + Math.pow(diffY, 2d));
                        double travelledDistance = Math.min(100d, 100d / fd.getFlightTime() * (GameUtilities.getCurrentTick2() - fd.getStartTime()));

                        DebugBuffer.addLine(DebugLevel.TRACE, "Returning fleet " + fd.getFleetId());
                        DebugBuffer.addLine(DebugLevel.TRACE, "Travelled distance (%): " + travelledDistance + " Total Distance: " + totalDistance);

                        currX = (int) (fd.getStartX() + (diffX / 100d * travelledDistance));
                        currY = (int) (fd.getStartY() + (diffY / 100d * travelledDistance));

                        DebugBuffer.addLine(DebugLevel.TRACE, "OldStartLoc: X=" + fd.getStartX() + " Y=" + fd.getStartY());
                        DebugBuffer.addLine(DebugLevel.TRACE, "CurrentLoc: X=" + currX + " Y=" + currY);
                        DebugBuffer.addLine(DebugLevel.TRACE, "OldTargetLoc: X=" + fd.getDestX() + " Y=" + fd.getDestY());

                        double newDistance = Math.sqrt(Math.pow(fd.getStartX() - currX, 2d) + Math.pow(fd.getStartY() - currY, 2d));

                        float speed = 0.1f;
                        if (ffe.getSpeed() > 0) {
                            speed = (float) ffe.getSpeed();
                        }

                        DebugBuffer.addLine(DebugLevel.TRACE, "Fly back distance: " + newDistance + " Fleetspeed: " + speed);

                        newTravelTime = (int) (newDistance / speed);

                        DebugBuffer.addLine(DebugLevel.TRACE, "Travel time: " + newTravelTime);

                        calculateValues = false;
                    }


                    //Update to Database
                    fd.setDestSystem(fd.getStartSystem());
                    fd.setDestPlanet(fd.getStartPlanet());
                    fd.setDestX(fd.getStartX());
                    fd.setDestY(fd.getStartY());
                    fd.setStartSystem(0);
                    fd.setStartPlanet(0);
                    fd.setStartX(currX);
                    fd.setStartY(currY);
                    fd.setStartTime(GameUtilities.getCurrentTick2());
                    fd.setFlightTime(newTravelTime);
                    fdDAO.update(fd);
                }

            }
        } catch (Exception e) {
            Logger.getLogger().write("Error5:  " + e);
            DebugBuffer.writeStackTrace("Error while callBack of Fleet: ", e);
        }
        return new BaseResult(ML.getMLStr("fleetmovement_msg_allok", userId), false);
    }
}
