package at.viswars.fleet;

public class FleetOrder {
    private int commandType;
    private int retreatFactor;
    private int retreatToType;
    private int retreatTo;
    
    public int getCommandType() {
        return commandType;
    }

    public void setCommandType(int commandType) {
        this.commandType = commandType;
    }

    public int getRetreatFactor() {
        return retreatFactor;
    }

    public void setRetreatFactor(int retreatFactor) {
        this.retreatFactor = retreatFactor;
    }

    public int getRetreatToType() {
        return retreatToType;
    }

    public void setRetreatToType(int retreatToType) {
        this.retreatToType = retreatToType;
    }

    public int getRetreatTo() {
        return retreatTo;
    }

    public void setRetreatTo(int retreatTo) {
        this.retreatTo = retreatTo;
    }
}
