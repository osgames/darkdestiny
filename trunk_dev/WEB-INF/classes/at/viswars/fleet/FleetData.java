package at.viswars.fleet;

import at.viswars.DebugBuffer;
import at.viswars.ships.ShipData;
import at.viswars.ships.ShipStorage;
import at.viswars.ships.ShipUtilities;
import java.util.*;
import java.sql.*;

import at.viswars.database.access.DbConnect;

/**
 * Die Daten einer Flotte. 
 * (es werden nicht alle immer benutzt, wenn die Flotte im RAM bleibt, sollte
 * sichergestellt sein, dass bekannt ist, welche Daten gesetzt sind)
 * 
 * 
 * @author Rayden
 * @author martin
 */
public class FleetData {
    private int fleetId;
    private String name;
    private int userId;
    // Description for fleetAttitude
    // 1 = own
    // 2 = allied
    // 3 = neutral
    // 4 = enemy
    private int fleetAttitude;
    private int planetId;
    private int systemId;
    private int status;
    private int experience;
    private int troopSpace;
    private int ressSpace;
    private int smallHangarSpace;
    private int mediumHangarSpace;
    private int largeHangarSpace;
    private int currLoadedTroops;
    private int currLoadedPopulation;
    private int currLoadedRess;
    private int currLoadedSmallHangar;
    private int currLoadedMediumHangar;
    private int currLoadedLargeHangar;
    private boolean ableToCallBack;
    private ArrayList<ShipStorage> loadingList;
    private int ETA;
    private int order;
    private List<ShipData> shipList;    
    private FleetOrder fo = null;
    
    public FleetData()
    {
    	
    }
    
    public FleetData(int fleetId) {     
        
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT name, userId, planetId, systemId, status FROM playerfleet WHERE " +
                           "id="+fleetId);
            
            if (rs.next()) {
                this.fleetId = fleetId;
                name = rs.getString(1);
                userId = rs.getInt(2);
                planetId = rs.getInt(3);
                systemId = rs.getInt(4);
                status = rs.getInt(5);
            }
            
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Exception in Constructor of FleetData: ",e);
        }
    }
    
    public void save()
    {
    	Statement smt = null;
		try {
        smt = DbConnect.createStatement();
    	smt.execute("UPDATE `playerfleet` SET name='"+name+"', userId="+userId+
    				", planetId='"+planetId+"',	systemId="+systemId+", status="+status+" WHERE id="+fleetId);
		} catch (SQLException e) {
        	DebugBuffer.writeStackTrace("Error in FleetData.save(): ", e);
		} finally {
			try {
				smt.close();
			} catch (Exception e) {}
		}
    }
    
    public void loadShipList() {
        shipList = ShipUtilities.getShipList(fleetId);
    }
    
    public int getFleetId() {
        return fleetId;
    }

    public void setFleetId(int fleetId) {
        this.fleetId = fleetId;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPlanetId() {
        return planetId;
    }

    public void setPlanetId(int planetId) {
        this.planetId = planetId;
    }

    public int getSystemId() {
        return systemId;
    }

    public void setSystemId(int systemId) {
        this.systemId = systemId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }    

    public int getFleetAttitude() {
        return fleetAttitude;
    }

    public void setFleetAttitude(int fleetAttitude) {
        this.fleetAttitude = fleetAttitude;
    }

    public java.util.List<ShipData> getShipList() {
        return shipList;
    }

    public void setShipList(java.util.List<ShipData> shipList) {
        this.shipList = shipList;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getETA() {
        return ETA;
    }

    public void setETA(int ETA) {
        this.ETA = ETA;
    }   

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getTroopSpace() {
        return troopSpace;
    }

    public void setTroopSpace(int troopSpace) {
        this.troopSpace = troopSpace;
    }

    public int getRessSpace() {
        return ressSpace;
    }

    public void setRessSpace(int ressSpace) {
        this.ressSpace = ressSpace;
    }

    public ArrayList<ShipStorage> getLoadingList() {
        return loadingList;
    }

    public void setLoadingList(ArrayList<ShipStorage> loadingList) {
        this.loadingList = loadingList;
    }

    public int getCurrLoadedTroops() {
        return currLoadedTroops;
    }

    public void setCurrLoadedTroops(int currLoadedTroops) {
        this.currLoadedTroops = currLoadedTroops;
    }

    public int getCurrLoadedRess() {
        return currLoadedRess;
    }

    public void setCurrLoadedRess(int currLoadedRess) {
        this.currLoadedRess = currLoadedRess;
    }

    public int getCurrLoadedPopulation() {
        return currLoadedPopulation;
    }

    public void setCurrLoadedPopulation(int currLoadedPopulation) {
        this.currLoadedPopulation = currLoadedPopulation;
    }

    public int getCurrLoadedSmallHangar() {
        return currLoadedSmallHangar;
    }

    public void setCurrLoadedSmallHangar(int currLoadedSmallHangar) {
        this.currLoadedSmallHangar = currLoadedSmallHangar;
    }

    public int getCurrLoadedMediumHangar() {
        return currLoadedMediumHangar;
    }

    public void setCurrLoadedMediumHangar(int currLoadedMediumHangar) {
        this.currLoadedMediumHangar = currLoadedMediumHangar;
    }

    public int getCurrLoadedLargeHangar() {
        return currLoadedLargeHangar;
    }

    public void setCurrLoadedLargeHangar(int currLoadedLargeHangar) {
        this.currLoadedLargeHangar = currLoadedLargeHangar;
    }

    public int getSmallHangarSpace() {
        return smallHangarSpace;
    }

    public void setSmallHangarSpace(int smallHangarSpace) {
        this.smallHangarSpace = smallHangarSpace;
    }

    public int getMediumHangarSpace() {
        return mediumHangarSpace;
    }

    public void setMediumHangarSpace(int mediumHangarSpace) {
        this.mediumHangarSpace = mediumHangarSpace;
    }

    public int getLargeHangarSpace() {
        return largeHangarSpace;
    }

    public void setLargeHangarSpace(int largeHangarSpace) {
        this.largeHangarSpace = largeHangarSpace;
    }

    public boolean isAbleToCallBack() {
        return ableToCallBack;
    }

    public void setAbleToCallBack(boolean ableToCallBack) {
        this.ableToCallBack = ableToCallBack;
    }

    public FleetOrder getFo() {
        if (fo == null) {
            try { 
                Statement stmt = DbConnect.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT fleetorder, retreatFactor, retreatToType, retreatTo FROM fleetorders WHERE fleetId="+fleetId);
                if (rs.next()) {
                    FleetOrder foTmp = new FleetOrder();
                    foTmp.setCommandType(rs.getInt(1));
                    foTmp.setRetreatFactor(rs.getInt(2));
                    foTmp.setRetreatToType(rs.getInt(3));
                    foTmp.setRetreatTo(rs.getInt(4));
                    fo = foTmp;
                }
            } catch (Exception e) {
                DebugBuffer.error("Error on retrieving fleetorders: ",e);
            }
        }

        return fo;
    }

    public void setFo(FleetOrder fo) {
        this.fo = fo;
    }
}
