/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.fleet;

import at.viswars.DebugBuffer;
import at.viswars.GameConstants;
import at.viswars.ships.ShipStorage;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.FleetLoadingDAO;
import at.viswars.dao.GroundTroopDAO;
import at.viswars.dao.RessourceDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.hangar.ShipTypeEntry;
import at.viswars.model.FleetLoading;
import at.viswars.model.GroundTroop;
import at.viswars.model.HangarRelation;
import at.viswars.model.PlayerFleet;
import at.viswars.model.Ressource;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipFleet;
import at.viswars.utilities.HangarUtilities;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class LoadingInformation {    
    private final PlayerFleet fleet;
            
    private int troopSpace;
    private int ressSpace;
    private ArrayList<ShipStorage> loadedStorage;
    private int currLoadedRess;
    private int currLoadedTroops;
    private int currLoadedPopulation;
    private int smallHangarSpace;
    private int mediumHangarSpace;
    private int largeHangarSpace;
    private ArrayList<ShipTypeEntry> loadedShips;
    
    private static ShipFleetDAO sfDAO = (ShipFleetDAO)DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO)DAOFactory.get(ShipDesignDAO.class);    
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO)DAOFactory.get(FleetLoadingDAO.class);  
    private static RessourceDAO rDAO = (RessourceDAO)DAOFactory.get(RessourceDAO.class);  
    private static GroundTroopDAO gtDAO = (GroundTroopDAO)DAOFactory.get(GroundTroopDAO.class);  
    
    public LoadingInformation(PlayerFleet fleet) {
        this.fleet = fleet;
        init();
    }
    
    private void init() {
        // Loop through all Ship Designs and sum up
        try {
            ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(fleet.getId());
            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());
                
                troopSpace += sd.getTroopSpace() * sf.getCount();
                ressSpace += sd.getRessSpace() * sf.getCount();
                smallHangarSpace += sd.getSmallHangar() * sf.getCount();
                mediumHangarSpace += sd.getMediumHangar() * sf.getCount();
                largeHangarSpace += sd.getLargeHangar() * sf.getCount();                
            }

            // Calculate Loading
            ArrayList<FleetLoading> flList = flDAO.findByFleetId(fleet.getId());
            
            ArrayList<ShipStorage> loadingDetails = new ArrayList<ShipStorage>();
            for (FleetLoading fl : flList) {
                ShipStorage ss = new ShipStorage();
                ss.setLoadtype(fl.getLoadType());
                ss.setId(fl.getId());
                ss.setCount(fl.getCount());

                String name = "#" + ss.getLoadtype() + "-" + ss.getId();

                switch (ss.getLoadtype()) {
                    case (GameConstants.LOADING_RESS):
                        currLoadedRess += ss.getCount();
                        Ressource r = rDAO.findRessourceById(ss.getId());

                        if (r != null) {
                            name = r.getName();
                        }
                        break;
                    case (GameConstants.LOADING_TROOPS):
                        GroundTroop gt = gtDAO.findById(ss.getId());

                        if (gt != null) {
                            currLoadedTroops += ss.getCount() * gt.getSize();
                            name = gt.getName();
                        }
                        break;
                    case (GameConstants.LOADING_POPULATION):
                        currLoadedPopulation += ss.getCount();
                        name = "Arbeiter";
                        break;
                }

                ss.setName(name);
                loadingDetails.add(ss);
            }
            
            // Read HangarLoading
            HangarUtilities hu = new HangarUtilities();
            hu.initializeFleet(fleet.getId());
            loadedShips = hu.getNestedHangarStructure();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while preparing Loading Information ", e);
        }        
    }

    public int getTroopSpace() {
        return troopSpace;
    }

    public int getRessSpace() {
        return ressSpace;
    }

    public ArrayList<ShipStorage> getLoadedStorage() {
        if (loadedStorage == null) {
            loadedStorage = new ArrayList<ShipStorage>();
            
            ArrayList<FleetLoading> flList = flDAO.findByFleetId(fleet.getId());
            for (FleetLoading fl : flList) {
                ShipStorage ss = new ShipStorage();
                ss.setId(fl.getId());
                ss.setLoadtype(fl.getLoadType());
                ss.setCount(fl.getCount());
                
                if (fl.getLoadType() == 1) {
                    ss.setName(rDAO.findRessourceById(ss.getId()).getName());
                } else if (fl.getLoadType() == 2) {                    
                    ss.setName(gtDAO.findById(ss.getId()).getName());
                } else if (fl.getLoadType() == 4) {
                    ss.setName("Arbeiter");
                }
                
                loadedStorage.add(ss);                                
            }
        }
        
        return loadedStorage;
    }

    public int getCurrLoadedRess() {
        return currLoadedRess;
    }

    public int getCurrLoadedTroops() {
        return currLoadedTroops;
    }

    public int getCurrLoadedPopulation() {
        return currLoadedPopulation;
    }

    public int getSmallHangarSpace() {
        return smallHangarSpace;
    }

    public int getCurrLoadedSmallHangar() {
        return 0;
    }    
    
    public int getMediumHangarSpace() {
        return mediumHangarSpace;
    }

    public int getCurrLoadedMediumHangar() {
        return 0;
    }        
    
    public int getLargeHangarSpace() {
        return largeHangarSpace;
    }

    public int getCurrLoadedLargeHangar() {
        return 0;
    }        
    
    public ArrayList<ShipTypeEntry> getLoadedShips() {
        return loadedShips;
    }
    
    public boolean hasRessourceLoading() {
        return currLoadedRess > 0;
    }
    
    public boolean hasTroopLoading() {
        return currLoadedTroops > 0;
    }
    
    public boolean hasHangarLoading() {
        boolean hasLoading = false;
        
        for (ShipTypeEntry ste : loadedShips) {
            if ((ste.getSmallHangarLoaded().size() > 0) ||
                    (ste.getMediumHangarLoaded().size() > 0) ||
                    (ste.getLargeHangarLoaded().size() > 0)) {
                hasLoading = true;
            }
        }
        
        return hasLoading;
    }
}
