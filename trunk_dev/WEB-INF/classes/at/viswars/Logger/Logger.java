/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Stefan
 */
public class Logger {
    private static Logger logger = null;
    private static LogLevel logLevel = LogLevel.ERROR;

    /**
     * @param aLogLevel the logLevel to set
     */
    public static void setLogLevel(LogLevel aLogLevel) {
        logLevel = aLogLevel;
    }

    public static enum LogLevel {
        TRACE(-1),
        DEBUG(0), 
        INFO(1), 
        WARNING(2),
        ERROR(3);
        
        private final int value;

        private LogLevel(int value_){
            value = value_;
        }

        public int getIntValue()
        {
            return value;
        }        
    }    
    
    public static Logger getLogger() {
        if (logger == null) logger = new Logger();
        return logger;
    }
    
    public void write(String text) {                
        write(LogLevel.DEBUG,text);
    }

    public void write(LogLevel ll, String text) {    
        if (ll.getIntValue() < logLevel.getIntValue()) return;
        
        if (ll == LogLevel.ERROR) {
            System.out.println("["+getTime()+"] ERROR: " + text);
        } else if (ll == LogLevel.TRACE) {
            System.out.println("["+getTime()+"] TRACE: " + text);
        } else if (ll == LogLevel.WARNING) {
            System.out.println("["+getTime()+"] WARNING: " + text);
        } else if (ll == LogLevel.INFO) {
            System.out.println("["+getTime()+"] INFO: " + text);
        } else if (ll == LogLevel.DEBUG) {
            System.out.println("["+getTime()+"] DEBUG: " + text);
        }
    }    
    
    private static String getTime() {
        Date todaysDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = formatter.format(todaysDate);        
        
        return formattedDate;
    }    
}
