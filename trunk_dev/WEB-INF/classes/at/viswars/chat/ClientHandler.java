/*
 * ClientHandler.java
 *
 * Created on 04. Juli 2008, 00:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.chat;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 * 
 * FIXME: Does this require to be derived from Thread, or should it only 
 * implement runable?
 */
public class ClientHandler 
        extends Thread {
    private Socket clientSocket;
    
    private PrintWriter out;
    private BufferedReader in;
    
    private ChatUserEntry thisUser;
    
    // WARNING: PLEASE CHECK THESE VARIABLES ARE USED IN A THREAD SAFE WAY!!
    private boolean pingSent = false;
    private boolean noResponse = false;
    
    public int connState = 0; // TODO: WTF is this?!? Should be an enum, or some flags?
    
    public final long creationTime = System.currentTimeMillis();
    
    private boolean loggedIn = false;
    
    /** Creates a new instance of ClientHandler */
    public ClientHandler(Socket clientSocket) {
        // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Client Handler instance created");
        this.clientSocket = clientSocket;
        establishStreams();        
        // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Streams established");
        
        sendCommand("CONNECTION ESTABLISHED - WELCOME TO DD CHAT!");
        
        // DebugBuffer.addLine(DebugLevel.TRACE,"Set Client State to 1");
        connState = 1;
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                if (!loggedIn) {                    
                    String response = in.readLine();
                    
                    // DebugBuffer.trace("CHAT IN: " + response);
                    
                    ParameterEntry pe = CommandParser.parseCommand(response);
                    if (pe.command != CommandParser.LOGIN) continue;
                                                         
                    LoginResponse lr = ChatHandler.login(pe.pars.get(0),pe.pars.get(1));
                    loggedIn = lr.isSuccess();
                    
                    if (loggedIn) {
                        sendCommand("LOGINOK");
                    } else {
                        sendCommand("LOGINFAILED|"+lr.getReason());
                    }
                    
                    if (!loggedIn) {
                        continue;
                    } else {                                              
                        thisUser = ChatHandler.addUser(pe.pars.get(0)); 
                        
                        // Send Password for reconnect on Server reboot
                        sendCommand("PASSWORD|"+thisUser.getPassword());
                        
                        // Send Channel List and according Users
                        sendCommand("OPENCHAN|Globaler Chat");
                        
                        // FIXME: THIS IS NOT THREAD SAFE!!!!!!!
                        // YOU ARE HEADING FOR TROUBLE, IF SOME USERS ARE ACTUALLY
                        // USING THE CHAT!!!!
                        ArrayList<ChatUserEntry> userList = ChatHandler.getAllUsers();
                        // DebugBuffer.addLine(DebugLevel.TRACE,"Logged in Users = " + userList.size());
                        for (int i=0;i<userList.size();i++) {
                            ChatUserEntry cue = userList.get(i);
                                                        
                            // DebugBuffer.addLine(DebugLevel.TRACE,"Send ADDUSER for " + cue.getUserName());
                            
                            if (cue != null) {
                                sendCommand("ADDUSER|Globaler Chat|" + cue.getUserName());
                            }
                        }
                        
                        // FIXME: THIS IS NOT THREAD SAFE!!!!!!!
                        // YOU ARE HEADING FOR TROUBLE, IF SOME USERS ARE ACTUALLY
                        // USING THE CHAT!!!!
                        if (getThisUser().getAllianceId() != 0) {
                            sendCommand("OPENCHAN|Allianz Chat");
                            
                            userList = ChatHandler.getAllUserForAlliance(getThisUser().getAllianceId());
                            for (int i=0;i<userList.size();i++) {
                                ChatUserEntry cue = userList.get(i);
                                
                                if (cue != null) {
                                    sendCommand("ADDUSER|Allianz Chat|" + cue.getUserName());
                                }
                            }
                        }      
                        
                        ChatHandler.notifyAddUser("Globaler Chat",thisUser.getUserName());
                        ChatHandler.notifyAddUser(thisUser.getAllianceId(), thisUser.getUserName());
                    }
                }
                
                // DebugBuffer.addLine(DebugLevel.TRACE,"Set Client State to 2");
                connState = 2;                   
                
                // Login succeeded await incoming commands
                String response = in.readLine();
                ParameterEntry pe = CommandParser.parseCommand(response);
                                                
                switch(pe.command) {
                    case(CommandParser.MSG):
                        if (pe.pars.get(0).equalsIgnoreCase("Globaler Chat")) {
                            // DebugBuffer.addLine(DebugLevel.TRACE,"Global Chat Message -> " + thisUser.getUserName() + " // " + pe.pars.get(1));
                            ChatHandler.processGlobalMessage(thisUser.getUserName(),pe.pars.get(1));
                        } else if (pe.pars.get(0).equalsIgnoreCase("Allianz Chat")) {
                            ChatHandler.processAllianceMessage(thisUser.getAllianceId(),thisUser.getUserName(),pe.pars.get(1));
                        } else if (pe.pars.get(0).startsWith("\\")) {
                            ChatHandler.processCommand(thisUser, pe.pars.get(1));
                        }
                        break;
                    case(CommandParser.MSGPRIVATE):
                        break;
                    case(CommandParser.CLOSE):
                        sendCommand("GOODBYE");
                        closeStreams();
                        break;                        
                    case(CommandParser.PONG):
                        // DebugBuffer.addLine(DebugLevel.TRACE,"Incoming PONG keep alive!");
                        
                        if (pingSent) {
                            // DebugBuffer.addLine(DebugLevel.TRACE,"SET noResponse and pingSent to False");
                            noResponse = false;
                            pingSent = false;
                        }
                        break;
                }
            }
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.TRACE,"Client Disconnected! (Reason: " + e.getMessage()+")");
            
            if (thisUser != null) {
                DebugBuffer.trace("User " + thisUser.getUserName());
                ChatHandler.notifyRemoveUser(thisUser.getUserName());
                ChatHandler.removeUser(thisUser.getUserName());            
            } else {
                // DebugBuffer.trace("User was not created till now");
            }
            
            closeStreams();
        }
    }
    
    private void establishStreams() {
        try {
            out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(),"ISO-8859-1"), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(),"ISO-8859-1"));            
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR,"Encountered Connection Problem with Client: " + e);
        }
    }

    public void closeStreams() {
        try {
            out.close();
            in.close();
            clientSocket.close();
        } catch (IOException e) { 
            DebugBuffer.writeStackTrace("Client Handler: ", e);
        }        
    }
    
    public ChatUserEntry getThisUser() {
        return thisUser;
    }
    
    public void sendCommand(String command) {                
        synchronized(out) {
            // DebugBuffer.trace("CHAT OUT: " + command);
            
            out.println(command);
            out.flush();       
        }
    }

    public boolean checkConnAlive() {
        // DebugBuffer.addLine(DebugLevel.TRACE,"Check Conn Alive: " + pingSent + " " + noResponse);
        
        if (pingSent && noResponse) return false;
        
        if (!pingSent) {
            pingSent = true;
            noResponse = true;            
            sendCommand("PING");
        }
        
        return true;
    }
    
    public boolean isLoggedIn() {
        return loggedIn;
    }
}
