/*
 * ChatHandler.java
 *
 * Created on 04. Juli 2008, 00:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.chat;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.dao.AllianceMemberDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.UserDAO;
import at.viswars.model.AllianceMember;
import at.viswars.model.User;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ChatHandler {
    private static HashMap<String,ChatUserEntry> userList = new HashMap<String,ChatUserEntry>();
    private static HashMap<String,String> autoLoginList = new HashMap<String,String>();
    
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    
    /** Creates a new instance of ChatHandler */
    public ChatHandler() {

    }
    
    public static void allowAutoLogin(String user, String sessionId) {                
        sessionId = encryptPassword(sessionId);
        
        // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Adding autoLogin: " + user + " " + sessionId);
        autoLoginList.put(user, sessionId);
    }

    public static ChatUserEntry addUser(String name) {
        DebugBuffer.trace("Add Logged In User " + name);        
        
        // Load all data
        try {
            ChatUserEntry cue = new ChatUserEntry();
            
            // Statement stmt = DbConnect.createStatement();                                                
            // ResultSet rs = null;
            
            User u = null;
            AllianceMember am = null;
            
            try {
                Integer.parseInt(name);
                u = uDAO.findById(Integer.parseInt(name));
                am = amDAO.findByUserId(u.getUserId());
                // rs = stmt.executeQuery("SELECT u.id, u.Admin, u.gamename, a.masterAllianceId, u.password FROM user u LEFT OUTER JOIN alliancemembers am ON am.userId=u.id LEFT OUTER JOIN alliance a ON a.id=am.allianceId WHERE u.id="+Integer.parseInt(name)); 
            } catch (NumberFormatException nfe) {
                u = uDAO.findByGameName(name);
                am = amDAO.findByUserId(u.getUserId());
                // rs = stmt.executeQuery("SELECT u.id, u.Admin, u.gamename, a.masterAllianceId, u.password FROM user u LEFT OUTER JOIN alliancemembers am ON am.userId=u.id LEFT OUTER JOIN alliance a ON a.id=am.allianceId WHERE u.username='"+name+"'"); 
            }       
            
            if (u != null) {
                DebugBuffer.trace("Init UserId=" + u.getUserId() + " UserName="+u.getUserName() + " isAdmin="+u.getAdmin());
                
                cue.setUserId(u.getUserId());
                cue.setUserName(u.getGameName());
                cue.setIsAdmin(u.getAdmin());
                if (am != null) {
                    cue.setAllianceId(am.getAllianceId());
                } else {
                    cue.setAllianceId(0);
                }
                cue.setPassword(u.getPassword());
                
                userList.put(u.getGameName(),cue);
            } else {                
                DebugBuffer.trace("User " + name + " was not found!");
            }
            
            return cue;
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR,"Adding of user to chat failed: " + e);
        }
        
        return null;
    }
    
    public static void removeUser(String name) {
        userList.remove(name);
    }
    
    public static ArrayList<ChatUserEntry> getAllUserForAlliance(int id) {
        ArrayList<ChatUserEntry> userListTmp = new ArrayList<ChatUserEntry>();
        
        for (Map.Entry<String,ChatUserEntry> me : userList.entrySet()) {
            if (me.getValue().getAllianceId() == id) {
                userListTmp.add(me.getValue());
                Logger.getLogger().write("Returning allied User: " + me.getValue().getUserName());
            }
        }
        
        return userListTmp;
    }
    
    public static ArrayList<ChatUserEntry> getAllUsers() {
        ArrayList<ChatUserEntry> userListTmp = new ArrayList<ChatUserEntry>();
        
        for (Map.Entry<String,ChatUserEntry> me : userList.entrySet()) {
            Logger.getLogger().write("Returning normal User: " + me.getValue().getUserName());
            userListTmp.add(me.getValue());
        }
        
        return userListTmp;
    }
    
    public static ChatUserEntry getUser(String name) {
        return userList.get(name);
    }
    
    public static LoginResponse login(String name, String password) {
        LoginResponse lr = new LoginResponse();
        
        // DebugBuffer.addLine(DebugLevel.UNKNOWN,"LOGIN " + name + " " + password);
        
        try {
            // Statement stmt = DbConnect.createStatement();
            
            // Get password of user if session was allowed for AutoLogin
            // Remove session entry afterwards
            if (autoLoginList.containsKey(name)) {
                // DebugBuffer.addLine(DebugLevel.UNKNOWN,"AutoLogin contains " + name);
                
                if (autoLoginList.get(name).equalsIgnoreCase(password)) {
                    User u = uDAO.findById(Integer.parseInt(name));
                    // ResultSet rs = stmt.executeQuery("SELECT username, password FROM user WHERE id="+Integer.parseInt(name));
                    
                    if (u != null) {
                        name = u.getUserName();
                        password = u.getPassword();
                        
                        // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Found autoLoginEntry: " + name + " " + password);
                    }
                }
                
                autoLoginList.remove(name);
            }            
        
            try {
                int uId = Integer.parseInt(name);
                // ResultSet rs = stmt.executeQuery("SELECT username FROM user WHERE id="+uId);
                User u = uDAO.findById(uId);
                if (u != null) {
                    name = u.getUserName();
                }
            } catch (NumberFormatException nfe) {
                
            }
            
            // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Try to login with username " + name + " and passwort " + password);
            // ResultSet rs = stmt.executeQuery("SELECT password, gamename FROM user WHERE username='"+name+"' AND password='"+password+"'");
            User u = uDAO.findBy(name, password);
            
            if (u != null) {
                if (userList.containsKey(u.getGameName())) {
                    lr.setReason("User "+u.getGameName()+" bereits angemeldet");
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN,"LOGIN failed: " + lr.getReason());
                    lr.setSuccess(false);
                } else {
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN,"LOGIN succesful");
                    lr.setSuccess(true);
                }
                
            } else {
                lr.setReason("Ungueltiges Login!");
                // DebugBuffer.addLine(DebugLevel.UNKNOWN,"LOGIN failed: " + lr.getReason());
                lr.setSuccess(false);
            }                   
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR,"Adding of user to chat failed: " + e);
        }
        
        return lr;
    }
    
    public static void processGlobalMessage(String username, String msg) {
        List<ClientHandler> allClients = ServerListener.getClientList();
        
        for (int i=0;i<allClients.size();i++) {
            if (!allClients.get(i).isLoggedIn()) continue;
            DebugBuffer.addLine(DebugLevel.TRACE,"Processing Client " + allClients.get(i).getThisUser().getUserName());
            
            //ChatUserEntry cue = allClients.get(i).getThisUser();
            
            allClients.get(i).sendCommand("MSG|Globaler Chat|"+username+"|"+msg);
        }
    }
    
    public static void processAllianceMessage(int alliance, String username, String msg) {
        List<ClientHandler> allClients = ServerListener.getClientList();
        
        for (int i=0;i<allClients.size();i++) {
            if (!allClients.get(i).isLoggedIn()) continue;
            
            ChatUserEntry cue = allClients.get(i).getThisUser();
            
            if (cue.getAllianceId() == alliance) {
                allClients.get(i).sendCommand("MSG|Allianz Chat|"+username+"|"+msg);
            }
        }
    }
    
    public static void processPrivateMessage(String receiver, String msg) {
        List<ClientHandler> allClients = ServerListener.getClientList();
        
        for (int i=0;i<allClients.size();i++) {
            if (!allClients.get(i).isLoggedIn()) continue;
            
            ChatUserEntry cue = allClients.get(i).getThisUser();

            if (cue.getUserName().equalsIgnoreCase(receiver)) {
            	allClients.get(i).sendCommand("MSGPRIVATE|"+receiver+"|"+msg);
            }
        }
    }
    
    public static void processCommand(ChatUserEntry cue, String msg) {
        String command = msg.substring(1);
    }
    
    public static void notifyAddUser(int allianceId, String username) {
        List<ClientHandler> allClients = ServerListener.getClientList();
        
        for (int i=0;i<allClients.size();i++) {
            if (!allClients.get(i).isLoggedIn()) continue;
            
            ChatUserEntry cue = allClients.get(i).getThisUser();
            if ((cue.getAllianceId() != allianceId) || (allianceId == 0)) continue;
            
            if (!cue.getUserName().equalsIgnoreCase(username)) {
                allClients.get(i).sendCommand("ADDUSER|Allianz Chat|"+username);
            }
        }        
    }
    
    public static void notifyAddUser(String channel, String username) {
        // FIXME: THIS IS NOT THREAD SAFE!!!!!!!
        // YOU ARE HEADING FOR TROUBLE, IF SOME USERS ARE ACTUALLY
        // USING THE CHAT!!!!
        List<ClientHandler> allClients = ServerListener.getClientList();
        
        for (int i=0;i<allClients.size();i++) {
            if (!allClients.get(i).isLoggedIn()) continue;
            
            ChatUserEntry cue = allClients.get(i).getThisUser();
            
            if (!cue.getUserName().equalsIgnoreCase(username)) {
                allClients.get(i).sendCommand("ADDUSER|"+channel+"|"+username);
            }
        }
    }
    
    public static void notifyRemoveUser(String username) {
        // FIXME: THIS IS NOT THREAD SAFE!!!!!!!
        // YOU ARE HEADING FOR TROUBLE, IF SOME USERS ARE ACTUALLY
        // USING THE CHAT!!!!
        DebugBuffer.trace("Remove User " + username);
        
        List<ClientHandler> allClients = ServerListener.getClientList();
        
        for (int i=0;i<allClients.size();i++) {
            DebugBuffer.trace("Looping users - current: " + allClients.get(i).getName());
            
            if (!allClients.get(i).isLoggedIn()) continue;
            
            ChatUserEntry cue = allClients.get(i).getThisUser();
            
            if (!cue.getUserName().equalsIgnoreCase(username)) {
                allClients.get(i).sendCommand("REMOVEUSER|"+username);
            }
        }
    }
    
    private static String encryptPassword(String password) {
        String encrypted = "";
        
        byte[] orgPwd = password.getBytes();
        
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(orgPwd);
            byte messageDigest[] = algorithm.digest();
            
            // String s = new BASE64Encoder().encode(messageDigest);
            String s = "";
            for (int i=0;i<messageDigest.length;i++) {
                String currentHex = Integer.toHexString(0xFF & messageDigest[i]);
                if (currentHex.length() == 1) currentHex = "0" + currentHex;
                s += currentHex;
            }
            encrypted = s;
        } catch (Exception e) {
            
        }
        
        return encrypted;
    }            
}
