/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars;

import at.viswars.model.QuizAnswer;
import at.viswars.model.QuizEntry;
import at.viswars.model.QuizQuestion;
import at.viswars.service.Service;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class QuizTest {

    public static void main(String args[]) {


TreeMap<Integer, QuizEntry> sorted = new TreeMap<Integer, QuizEntry>();
        for(QuizEntry qe : Service.quizEntryDAO.findAll()){
           sorted.put(qe.getId(), qe);
        }
        for(Map.Entry<Integer, QuizEntry> entry : sorted.entrySet()){
            QuizEntry qe = entry.getValue();
            QuizQuestion qq = Service.quizQuestionDAO.findByQuizEntryId(qe.getId()).get(0);
            QuizAnswer qa = Service.quizAnswerDAO.findByQuizEntryId(qe.getId()).get(0);
System.out.println(qq.getQuestion() + " => " + qa.getAnswer());
        }

    }
}
