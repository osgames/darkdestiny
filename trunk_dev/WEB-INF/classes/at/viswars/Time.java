package at.viswars;

import at.viswars.DebugBuffer.DebugLevel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Bullet
 */
public class Time {

    private static long start;
    private static String msg;

    public static void start(String amsg) {
        start = System.currentTimeMillis();
        DebugBuffer.addLine(DebugLevel.TRACE, "Start - " + amsg);
        msg = amsg;
    }

    public static void stop() {
        DebugBuffer.addLine(DebugLevel.TRACE, "End - " + msg + " | In: " + (System.currentTimeMillis() - start) + " ms");
    }

    static void end() {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
