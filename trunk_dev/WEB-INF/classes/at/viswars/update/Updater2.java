package at.viswars.update;

import at.viswars.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.dao.*;
import at.viswars.database.framework.exception.TransactionException;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.databuffer.*;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EDiplomacyRelationType;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.exceptions.InvalidInstantiationException;
import at.viswars.groundcombat.MilizTraining;
import at.viswars.model.*;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.notification.NotificationBuffer;
import at.viswars.notification.OutOfRessourceNotification;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.result.ResearchResult;
import at.viswars.service.DiplomacyService;
import at.viswars.service.LoginService;
import at.viswars.service.Service;
import at.viswars.update.population.MigrationHandler;
import at.viswars.utilities.BonusUtilities;
import at.viswars.utilities.ResearchUtilities;
import at.viswars.utilities.TitleUtilities;
import at.viswars.utilities.GroundCombatUtilities;
import at.viswars.utilities.SpionageUtilities;
import at.viswars.voting.Vote;
import at.viswars.voting.VoteResult;
import at.viswars.voting.VoteUtilities;
import java.lang.System;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Updater2 implements Runnable {

    private final int startTick;
    private int currentTick;
    // private int noOfCurrRes;
    // private int accRP;
    private int currPlanetIdx = -1;
    // private List researchData;
    private PlayerPlanet pData;
    private UserData uData;
    // Buffers for update
    //private final NoticedChanges nc = new NoticedChanges();
    private static Map<Integer, ArrayList<ActionEntry>> globalActions;
    private static Map<Integer, ArrayList<Integer>> specialResearch;
    private static Set<UpdateListener> updateListeners;
    private static FlightProcessing fp;
    private static TradeRouteProcessing trp;
    private static TransportProcessing tp;
    private static ProdOrderBuffer pob;
    private PlanetCalculation pc;
    private static MigrationHandler mh;
    private static AbandonColonies ac;
    private static UpdaterDataSet uds;
    private static ArrayList<PlayerPlanet> ppList;
    private static ArrayList<PlayerPlanet> ppListRemovable = new ArrayList<PlayerPlanet>();
    private static ArrayList<PlanetLoyality> plListRemovable = new ArrayList<PlanetLoyality>();
    private static Map<Integer, Integer[]> resPoints;
    private static Map<Integer, ResearchProgress> resPSetUpdate;
    private static Map<Integer, Long> totalUserIncome;
    private static Map<Integer, Long> totalPopulation = Collections.synchronizedMap(new HashMap<Integer, Long>());
    private static Map<Integer, ArrayList<DevPointEntry>> devPointsPopPlanet = Collections.synchronizedMap(new HashMap<Integer, ArrayList<DevPointEntry>>());
    //DAOS
    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private ResearchDAO resDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    private UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private ResearchProgressDAO rpDAO = (ResearchProgressDAO) DAOFactory.get(ResearchProgressDAO.class);
    private PlayerResearchDAO pResDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);
    private NotificationToUserDAO ntuDAO = (NotificationToUserDAO) DAOFactory.get(NotificationToUserDAO.class);
    private DiplomacyRelationDAO drDAO = (DiplomacyRelationDAO) DAOFactory.get(DiplomacyRelationDAO.class);
    private PlanetLoyalityDAO plDAO = (PlanetLoyalityDAO) DAOFactory.get(PlanetLoyalityDAO.class);
    
    private VotingDAO vDAO = (VotingDAO) DAOFactory.get(VotingDAO.class);
    private boolean init = false;
    private ArrayList<Integer> planetIndices = new ArrayList<Integer>();
    protected boolean stop = false;
    protected boolean ready = false;
    protected boolean processing = false;
    protected boolean error = false;
    protected Exception errorCause = null;
    private Exception globalUpdateFailCause = null;

    public Updater2(int startTick) {
        this.startTick = startTick;
        currentTick = startTick;
    }

    public void run() {
        Logger.getLogger().write("Starting '" + Thread.currentThread().getName() + "' (" + planetIndices.size() + " assigned planets)");

        while (!stop) {
            ready = true;

            Logger.getLogger().write(Thread.currentThread().getName() + " waiting for coordinator");
            UpdateCoordinator.getInstance().waitForProcessingStart();
            if (stop) {
                Logger.getLogger().write(Thread.currentThread().getName() + " shutdown");
                return;
            }
            // Logger.getLogger().write("'" + Thread.currentThread().getName() + "' starts processing");

            Logger.getLogger().write(Thread.currentThread().getName() + " start processing");
            // Process all available planets
            try {
                for (Iterator<Integer> pIdxIt = planetIndices.iterator(); pIdxIt.hasNext();) {
                    int i = pIdxIt.next();
                    currPlanetIdx = i;

                    if (!loadPlanetData()) {
                        pIdxIt.remove();
                        continue;
                    }

                    uData = uds.getUserDataEntry(ppList.get(currPlanetIdx).getUserId());
                    if (uData == null) {
                        continue;
                    }

                    initializeSpecialResearch(uData.getUserId());

                    // Logger.getLogger().write(Thread.currentThread().getName() + " processing planet " + ppList.get(currPlanetIdx).getPlanetId());

                    calculateUpdates();
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("PlanetUpdate failed [PlanetId: " + pData.getPlanetId() + "]: ", e);
                errorCause = e;
                error = true;
            }

            // Logger.getLogger().write("'" + Thread.currentThread().getName() + "' finishes processing");

            processing = false;
        }
    }

    public void initUpdater() {
        if (init == true) {
            Logger.getLogger().write("Already initialized");
            return;
        }

        init = true;

        //  ppList = ppDAO.findAll();
        //Dont use Players on Vacation
        ppList = ppDAO.findAllToUpdate();
        uds = new UpdaterDataSet(
                ppList,
                prDAO.findAll(),
                udDAO.findAll(),
                aDAO.findAll());

        ac = new AbandonColonies(uds);
        trp = new TradeRouteProcessing(uds);
        tp = new TransportProcessing(uds);
        pob = new ProdOrderBuffer(uds);
        fp = new FlightProcessing(uds, startTick);
        mh = new MigrationHandler();

        updateListeners = new CopyOnWriteArraySet<UpdateListener>();

        resPoints = Collections.synchronizedMap(new HashMap<Integer, Integer[]>());
        resPSetUpdate = Collections.synchronizedMap(new HashMap<Integer, ResearchProgress>());
        totalUserIncome = Collections.synchronizedMap(new HashMap<Integer, Long>());
        specialResearch = Collections.synchronizedMap(new HashMap<Integer, ArrayList<Integer>>());
        currPlanetIdx = -1;
    }

    public void addPlanetJob(int idx) {
        planetIndices.add(idx);
    }

    public String getActPlanet() {
        return pData.getName();
    }

    private void addUpdateListener(UpdateListener listener) {
        updateListeners.add(listener);
    }

    public void cleanUp() {
        // DebugBuffer.addLine("clu1");
        // rb.destroyBuffer();
        pob.cleanUp();
        // globalActions.clear();
        // trh.storeData();
        // DebugBuffer.addLine("clu2");
    }

    public void calculateUpdates() {
        // DebugBuffer.addLine("cu1");
        // NEW calculatePopulation(pData.getPlanetId());
        //Time.start("Calculating Population");
        // Logger.getLogger().write(Thread.currentThread().getName() + " calculates Population");
        calculatePopulation();
        //Time.stop();
        // DebugBuffer.addLine("cu3");
        // at.viswars.Time.start("Calculating Ressources");
        // Logger.getLogger().write(Thread.currentThread().getName() + " calculates Ressources");
        calculateRessources();
        //Time.stop();

        // Logger.getLogger().write(Thread.currentThread().getName() + " sums Research Points");
        sumResearchPoints();

        // Logger.getLogger().write(Thread.currentThread().getName() + " builds stuff");
        pob.calcTickProduction(ppList.get(currPlanetIdx).getPlanetId(), pc);

        // Logger.getLogger().write(Thread.currentThread().getName() + " does migration stuff");
        mh.calculatePlanet(ppList.get(currPlanetIdx), pc.getPlanetCalcData().getExtPlanetCalcData());

        // Logger.getLogger().write(Thread.currentThread().getName() + " finished all that crap");
        // DebugBuffer.addLine("cu5");
        //   Time.start("Calculating Actions");
//        calculateActions();
        //   Time.stop();

        //    Time.start("Calculating TickProduction");
//        pob.calcTickProduction(pData.getPlanetId());
        //     Time.stop();
//        for (UpdateListener listener : updateListeners) {
//            listener.calculatePlanet(this, pData);
//        }

        //    Time.start("Calculating Traderoutes");
//        if (trh.containsIncomingPlanet(pData.getPlanetId())) {
//            trh.addMaxStock(pData.getPlanetId(), rc);
//        }
        //      Time.stop();        
        // DebugBuffer.addLine("cu8");
    }

    public void preTickInit() {
        resPoints.clear();
        mh.preTickInit();
        plListRemovable.clear();
        /*
        for (UpdateListener listener : updateListeners) {
        listener.preTickInit(this);
        }
         */
    }

    public void postTick() {
         plDAO.removeAll(plListRemovable);
         plDAO.updateAll(uds.getUpdatedLoyalityData());
    }
    
    public boolean calculateGlobalUpdates() {
        Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Global Updates for Tick " + currentTick);

        boolean errorState = false;

        DebugBuffer.trace("UPDATER: Start Global Updates ...");

        DebugBuffer.trace("UPDATER: Research ...");
        processResearches();

        DebugBuffer.trace("UPDATER: Income ...");
        calculateWeeklyIncome();
        totalUserIncome.clear();


        //##### SCANS
        DebugBuffer.trace("UPDATER: Processing Scans ...");
        try {
            calculateScans();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Processing Scans ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### TITLES
        DebugBuffer.trace("UPDATER: Processing Titles ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Title Updates");
            TitleUtilities.updateTitles();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("TitleUpdates failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### COMBAT-CHECK
        DebugBuffer.trace("UPDATER: Combat Check ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Creating new Combats");
            GroundCombatUtilities.checkForCombats(ppList);

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("GroundCombat failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### Decrement Locked-Value for Locked players
        DebugBuffer.trace("UPDATER: Decrement Locked Value ...");
        try {
            for (User u : (ArrayList<User>) Service.userDAO.findAllPlayers()) {
                if (u.getLocked() > 0) {
                    u.setLocked(u.getLocked() - 1);
                    Service.userDAO.update(u);
                }
            }

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Decrement Locked value failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### COMBAT
        DebugBuffer.trace("UPDATER: Ground Combat ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Ground Combat");
            ArrayList<Model> removedObjects = GroundCombatUtilities.processTick(ppList);
            for (Model m : removedObjects) {
                if (m instanceof ProductionOrder) {
                    pob.destroyProductionOrder(((ProductionOrder)m).getId());
                } else if (m instanceof TradeRoute) {
                    Logger.getLogger().write(Logger.LogLevel.INFO, "Updater2: attemptimg to destroy TradeRoute " + ((TradeRoute)m).getId());
                    trp.destroyRoute((TradeRoute)m);
                    tp.destroyRoute((TradeRoute)m);
                }                
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("GroundCombat failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### MILIZ
        DebugBuffer.trace("UPDATER: Miliz ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Miliz");
            MilizTraining.updateMiliz(ppList);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Miliztraining failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### VOTE-EXPIRATION CHECK
        DebugBuffer.trace("UPDATER: Vote Expiration ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Vote expiration check");
            processVoteExpirationCheck();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Processing Vote expiration check failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### TERRITORY Notifications
        DebugBuffer.trace("UPDATER: Territory Notifications ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Writing Territorynotification");
            NotificationBuffer.writeTerritoryNotifications();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Writing Territorynotification failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### Global Loyality / Riot Processing
        DebugBuffer.trace("UPDATER: Riots ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Riots");
            // PopulationUtilities.updateRiots(ppList);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("RiotCalculation failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### UPDATE BONUS
        DebugBuffer.trace("UPDATER: Bonus ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Update Bonus");
            BonusUtilities.updateBonus();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("BonusUpdate failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### Transport
        DebugBuffer.trace("UPDATER: Transport ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing TransportRoutes");
            tp.processRoutes(currentTick);

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("TradeRouteProcessing failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }        
        //##### TRADEROUTES
        DebugBuffer.trace("UPDATER: Traderoutes ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing TradeRoutes");
            trp.processRoutes(currentTick);

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("RouteProcessing failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### FLIGHTS
        DebugBuffer.trace("UPDATER: Flights ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Flights");
            fp.processTick(currentTick);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("FlightProcessing failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        //##### MIGRATION
        DebugBuffer.trace("UPDATER: Migration ...");
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Processing Population Migration");
            mh.globalUpdates();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("MigrationHandling failed: ", e);
            globalUpdateFailCause = e;
            errorState = true;
        }
        DebugBuffer.trace("UPDATER: Finished Global Updates ...");

        // calculateDevelopmentPointsGlobal();

        Logger.getLogger().write(Logger.LogLevel.INFO, "Finished Processing Global Updates for Tick " + currentTick);
        // calculateProdBuffer();
        /*
        for (UpdateListener listener : updateListeners) {
        listener.globalUpdates();
        }
        trh.process1Tick();
         */

        return !errorState;
    }

    protected ArrayList<PlayerPlanet> getPlanetList() {
        return ppList;
    }

    private boolean loadPlanetData() {
        try {
            pData = ppList.get(currPlanetIdx);

            // Check for abandoned colony
            if (pData.getDismantle() && (pData.getPopulation() <= 1000)) {
                if (ac.abandonColony(pData, currentTick)) {
                    ppListRemovable.add(pData);
                    return false;
                }
            }

            pc = new PlanetCalculation(ppList.get(currPlanetIdx), uds);
            // pob.setPlanetCalculation(pc);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Updater2.loadPlanetData(): ", e);
        }

        return true;
    }

    public int getStartTick() {
        return startTick;
    }

    public void calculateScans() {
        ArrayList<Action> actions = new ArrayList<Action>();
        actions.addAll(uds.getActionEntries(EActionType.SCAN_PLANET));
        actions.addAll(uds.getActionEntries(EActionType.SCAN_SYSTEM));
        for (Action a : actions) {
            try {
                if (a.getNumber() <= 1) {
                    PlayerFleetExt pfe;
                    try {
                        pfe = new PlayerFleetExt(a.getFleetId());
                    } catch (InvalidInstantiationException iie) {
                        DebugBuffer.warning("Tried to instantiate a non existing Fleet (" + a.getFleetId() + ") during a scan >> ACTION TAKEN: Deleting actions entry");
                        uds.removeActionEntry(a.getUserId(), a.getType(), a);
                        aDAO.remove(a);
                        continue;
                    }

                    if (a.getType().equals(EActionType.SCAN_PLANET)) {

                        if (pfe != null) {
                            SpionageUtilities.scanPlanet(pfe);
                        }
                    } else if (a.getType().equals(EActionType.SCAN_SYSTEM)) {

                        if (pfe != null) {
                            SpionageUtilities.scanSystem(pfe);
                        }
                    }

                    uds.removeActionEntry(a.getUserId(), a.getType(), a);
                    aDAO.remove(a);
                } else {
                    a.setNumber(a.getNumber() - 1);
                    aDAO.update(a);
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while finishing scan for fleet : " + a.getFleetId(), e);
                uds.removeActionEntry(a.getUserId(), a.getType(), a);
                aDAO.remove(a);
                continue;
            }
        }
    }

    private void calculateRessources() {
        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Calculate Ressources");

        ProductionResult prodRes = pc.getPlanetCalcData().getProductionData();
        ArrayList<at.viswars.model.Ressource> ressList = rDAO.findAll();

        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "RessList size is " + ressList.size());

        for (at.viswars.model.Ressource r : ressList) {
            if (r.getMineable()) {
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Ressource " + r.getName() + " is Mineable");
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Looking for (Ressource: " + r.getId() + " Type: " + PlanetRessource.TYPE_RESSOURCE_INSTORAGE + " PlanetId: " + ppList.get(currPlanetIdx).getPlanetId());
                PlanetRessource pr = uds.getPlanetRessEntry(ppList.get(currPlanetIdx).getPlanetId(), EPlanetRessourceType.PLANET, r.getId());
                if (pr == null) {
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN, "PR is null (RESSID: " + r.getName() + ")");
                    continue;
                }

                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Set new Qty");
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Set new Qty -> " + (pr.getQty() + prodRes.getRessProduction(r.getId()) - prodRes.getRessConsumption(r.getId())));
                long newQty = pr.getQty() + prodRes.getRessProduction(r.getId()) - prodRes.getRessConsumption(r.getId());

                if (newQty <= 0) {
                    if (newQty < 0) {
                        DebugBuffer.addLine(DebugLevel.ERROR, "A minable ressource [" + pr.getRessId() + "] on planet [" + pr.getPlanetId() + "]  got negative quantities [" + newQty + "]!");
                        DebugBuffer.addLine(DebugLevel.ERROR, "Values causing negative value >> Storage: " + pr.getQty() + " Production: " + prodRes.getRessProduction(r.getId()) + " Consumption: " + prodRes.getRessConsumption(r.getId()));
                        // TODO: Print out debug for calculating this ressource, to find the wrong calculations
                        if (prodRes.getCalcLog() != null) {
                            ArrayList<String> log = prodRes.getCalcLog().get((int) pr.getRessId());
                            if (log != null) {
                                for (String message : log) {
                                    DebugBuffer.addLine(DebugLevel.ERROR, "LOG: " + message);
                                }
                            }
                        }
                    }
                    uds.removePlanetRessEntry(ppList.get(currPlanetIdx).getPlanetId(), EPlanetRessourceType.PLANET, r.getId());
                    try {
                        new OutOfRessourceNotification(r.getId(), ppList.get(currPlanetIdx).getPlanetId(), ppList.get(currPlanetIdx).getUserId());
                    } catch (Exception e) {
                        DebugBuffer.writeStackTrace("Error while sending OutOfRessourceNotification about Planet " + currPlanetIdx + " ressource : " + r.getId() + "\n ",e);
                    }
                } else {
                    pr.setQty(newQty);
                }
            } else if (r.getTransportable()) {
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Ressource " + r.getName() + " is Transportable");
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Looking for (Ressource: " + r.getId() + " Type: " + PlanetRessource.TYPE_RESSOURCE_INSTORAGE + " PlanetId: " + ppList.get(currPlanetIdx).getPlanetId());
                uds.setMaxStorage(ppList.get(currPlanetIdx).getPlanetId(), r.getId(), (int) prodRes.getRessMaxStock(r.getId()));
                PlanetRessource pr = uds.getPlanetRessEntry(ppList.get(currPlanetIdx).getPlanetId(), EPlanetRessourceType.INSTORAGE, r.getId());
                if (pr == null) {
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN, "PR is null (RESSID: " + r.getName() + ")");
                    if (prodRes.getRessProduction(r.getId()) > 0) {
                        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Create new storage Entry (RESSID: " + r.getName() + ") with qty = " + prodRes.getRessProduction(r.getId()));
                        long newQty = prodRes.getRessProduction(r.getId()) - prodRes.getRessConsumption(r.getId());
                        if (newQty > 0) {
                            uds.createNewStorageRessEntry(ppList.get(currPlanetIdx).getPlanetId(), r.getId(), newQty);
                        }
                    }
                    continue;
                    // Logger.getLogger().write("PR="+pr+" for " + ppList.get(currPlanetIdx).getId() + ", " + PlanetRessource.TYPE_RESSOURCE_INSTORAGE + ", " + r.getId());
                }

                if (prodRes.getRessProduction(r.getId()) > 0) {
                    if (pr.getRessId() == at.viswars.model.Ressource.HOWALGONIUM) {
                        // TitleUtilities.incrementCondition(ConditionToTitle.TREASUREHUNTER_PRODUCEHOWALGONIUM, uData.getUserId(), (int) prodRes.getRessProduction(r.getId()));
                    }
                }
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Set new Qty -> " + (pr.getQty() + prodRes.getRessProduction(r.getId()) - prodRes.getRessConsumption(r.getId())));
                long newQty = pr.getQty() + prodRes.getRessProduction(r.getId()) - prodRes.getRessConsumption(r.getId());

                if (newQty <= 0) {
                    if (newQty < 0) {
                        DebugBuffer.addLine(DebugLevel.ERROR, "A ressource [" + pr.getRessId() + "] on planet [" + pr.getPlanetId() + "]  got negative quantities [" + newQty + "]!");
                        DebugBuffer.addLine(DebugLevel.ERROR, "Values causing negative value >> Storage: " + pr.getQty() + " Production: " + prodRes.getRessProduction(r.getId()) + " Consumption: " + prodRes.getRessConsumption(r.getId()));
                        // TODO: Print out debug for calculating this ressource, to find the wrong calculations
                        if (prodRes.getCalcLog() != null) {
                            ArrayList<String> log = prodRes.getCalcLog().get((int) pr.getRessId());
                            if (log != null) {
                                for (String message : log) {
                                    DebugBuffer.addLine(DebugLevel.ERROR, "LOG: " + message);
                                }
                            }
                        }
                    }
                    uds.removePlanetRessEntry(ppList.get(currPlanetIdx).getPlanetId(), EPlanetRessourceType.INSTORAGE, r.getId());
                } else {
                    if ((ppList.get(currPlanetIdx).getPlanetId() == 4086) && (r.getId() == 1)) {
                        Logger.getLogger().write("Change Iron on Planet 4086 by " + (prodRes.getRessProduction(r.getId()) - prodRes.getRessConsumption(r.getId())));
                    }

                    pr.setQty(newQty);
                }
            }
        }
    }

    private void calculatePopulation() {
        ExtPlanetCalcResult extRes = pc.getPlanetCalcData().getExtPlanetCalcData();
        PlayerPlanet pp = ppList.get(currPlanetIdx);

        int morale = pp.getMoral();
        if (extRes.getEstimatedMorale() > morale) {
            morale++;
        } else if (extRes.getEstimatedMorale() < morale) {
            morale -= (int) Math.ceil((morale - extRes.getEstimatedMorale()) / 10d);
        }

        // DebugBuffer.addLine("maxMorale="+maxMorale);
        if (morale > (int) extRes.getMaxMorale()) {
            morale = (int) extRes.getMaxMorale();
        }
        if (morale < 0) {
            morale = 0;
        }

        pp.setMoral(morale);

        // DebugBuffer.addLine("ChangeValueGrowth="+changevalueGrowth);
        float oldgrowth = extRes.getGrowth();
        float growth = extRes.getGrowth();

        growth += extRes.getChangevalueGrowth();
        // DebugBuffer.addLine("gcv="+changevalueGrowth);
        if (growth > extRes.getMaxGrowth()) {
            growth = extRes.getMaxGrowth();
        }
        if (growth < extRes.getMinGrowth()) {
            growth = oldgrowth + 1;
            if (growth > extRes.getMinGrowth()) {
                growth = extRes.getMinGrowth();
            }
        }

        if (pp.getDismantle()) {
            if (pp.getGrowth() > -4.9f) {
                growth = pp.getGrowth() - 0.1f;
            } else {
                growth = -5f;
            }
        }
        pp.setGrowth(growth);

        long population = pp.getPopulation();
        population += (int) Math.ceil((((float) population / (float) 100) * growth / 360));

        if (pp.getDismantle()) {
            // Logger.getLogger().write("CALCULATED GROWTH FOR " + pp.getPlanetId() + ": " + (int) Math.ceil((((float) population / (float) 100) * growth / 360)));
        }

        if (population < 0) {
            population = 0;
        }

        long maxPopulation = extRes.getMaxPopulation();
        if (population > maxPopulation) {
            population = maxPopulation;
        }

        pp.setPopulation(population);
        at.viswars.model.UserData ud = uds.getUserDataEntry(pp.getUserId());
        ud.setCredits(ud.getCredits() + extRes.getTaxIncome());

        if (!totalUserIncome.containsKey(ud.getUserId())) {
            totalUserIncome.put(ud.getUserId(), (long) extRes.getTaxIncome());
        } else {
            totalUserIncome.put(ud.getUserId(), totalUserIncome.get(ud.getUserId()) + extRes.getTaxIncome());
        }

        // Calculation Development Points
        ud.setDevelopementPoints(Math.max(ud.getDevelopementPoints() + extRes.getDevelopementPointsGlobal(), 0d));
        
        // Calculate loyality / riots
        double newValue = pp.getUnrest() + extRes.getRiotValueChange();
        double newRiot = Math.max(0d, Math.min(50d,newValue));
        
        pp.setUnrest(newRiot);
        
        newValue = extRes.getLoyalityValue().add(extRes.getLoyalityChange()).doubleValue();
        double newLoyality = Math.max(0d, Math.min(100d,newValue));
                        
        PlanetLoyality plOwn = uds.getLoyalityData(pp.getPlanetId(), pp.getUserId());
        if (plOwn == null) {
            plOwn = plDAO.getByPlanetAndUserId(pp.getPlanetId(), pp.getUserId());
            uds.addLoyalityEntry(plOwn);
        }
        
        BigDecimal actSum = new BigDecimal(newLoyality).setScale(2,RoundingMode.UP);
        plOwn.setValue(actSum.doubleValue());                
        
        BigDecimal largestDecimal = null;
        PlanetLoyality accEntry = null;
        
        // Adjust enemy loyalities
        HashMap<Integer,BigDecimal> enemyChange = extRes.getEnemyLoyalityChange();
        for (Map.Entry<Integer,BigDecimal> ecEntry : enemyChange.entrySet()) {
            PlanetLoyality plTmp = uds.getLoyalityData(pp.getPlanetId(), ecEntry.getKey());
            if (plTmp == null) {
                plTmp = plDAO.getByPlanetAndUserId(pp.getPlanetId(), ecEntry.getKey());                
                uds.addLoyalityEntry(plTmp);
            }
            
            newValue = plTmp.getValue() + ecEntry.getValue().doubleValue();
            newLoyality = Math.max(0d, Math.min(100d,newValue));          
            
            BigDecimal newLoyalityDec = new BigDecimal(newLoyality).setScale(2, RoundingMode.UP);
            if ((largestDecimal == null) || (largestDecimal.compareTo(newLoyalityDec) == -1)) {
                largestDecimal = newLoyalityDec;
                accEntry = plTmp;
            }
            
            actSum = actSum.add(newLoyalityDec);
            plTmp.setValue(newLoyalityDec.doubleValue());
            
            if (plTmp.getValue() <= 0) {
                plListRemovable.add(plTmp);
                uds.removeLoyalityEntry(plTmp);
            }
            // uds.addLoyalityEntry(plOwn);
        }
        
        if (largestDecimal != null) {
            // System.out.println("ACTSUM IS " + actSum.doubleValue());            
            
            BigDecimal diff = new BigDecimal(100).subtract(actSum);            
            // System.out.println("DIFF IS " + diff.doubleValue() + " HIGHEST VALUE IS " + largestDecimal.doubleValue());            
            accEntry.setValue(new BigDecimal(accEntry.getValue()).add(diff).doubleValue());
        }
    }

    private void sumResearchPoints() {
        ResearchResult rr = ResearchUtilities.getResearchPointsPlanet(ppList.get(currPlanetIdx).getPlanetId(), pc);
        int researchPoints = rr.getResearchPoints() + rr.getResearchPointsBonus();
        int cResearchPoints = rr.getCompResearchPoints() + rr.getCompResearchPointsBonus();
        if (resPoints.containsKey(uData.getUserId())) {
            Integer[] points = resPoints.get(uData.getUserId());
            points[0] += researchPoints;
            points[1] += cResearchPoints;

            // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Set FP="+points[0]+" and CFP="+points[1]+" for user " + uData.getUserId());
        } else {
            Integer[] points = new Integer[2];
            points[0] = researchPoints;
            points[1] = cResearchPoints;
            resPoints.put(uData.getUserId(), points);

            // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Set FP="+points[0]+" and CFP="+points[1]+" for user " + uData.getUserId());
        }
    }

    private void processResearches() {
        for (at.viswars.model.UserData ud : uds.getUpdatedUserData()) {
            //NOtification
            ArrayList<Research> finishedResearches = new ArrayList<Research>();

            int totalRP = 0;
            if (resPoints.get(ud.getUserId()) != null) {
                totalRP = resPoints.get(ud.getUserId())[0];
            }
            int totalRPComp = 0;
            if (resPoints.get(ud.getUserId()) != null) {
                totalRPComp = resPoints.get(ud.getUserId())[1];
            }

            /*
            if (ud.getUserId() == 72) {
            Logger.getLogger().write("Total FP="+totalRP+" CFP="+totalRPComp);
            }
             */

            HashMap<Action, ResearchProgress> aResMap = new HashMap<Action, ResearchProgress>();
            HashSet<Action> researches = uds.getActionEntries(ud.getUserId(), EActionType.RESEARCH);

            // Number of Researches which need RPs or CRPs
            int needsRPs = 0;
            int needsCRPs = 0;

            // Counting researches in research
            int rpPerResearch = 0;
            int rpPerResearchComp = 0;

            for (Iterator<Action> aIt = researches.iterator(); aIt.hasNext();) {
                Action a = aIt.next();

                if (a.getOnHold()) {
                    continue;
                }

                ResearchProgress rp;

                if (!resPSetUpdate.containsKey(a.getRefTableId())) {
                    rp = rpDAO.findById(a.getRefTableId());
                    resPSetUpdate.put(a.getRefTableId(), rp);
                } else {
                    rp = resPSetUpdate.get(a.getRefTableId());
                }

                if (rp == null) {
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Research " + a.getResearchId() + " was not found in researchProgress (user: " + ud.getUserId() + ")!");
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "ResearchProgress Id: " + a.getRefTableId() + "");
                    DebugBuffer.addLine(DebugLevel.WARNING, "Starting to repair in all my despair " + a.getRefTableId() + "");

                    if (pResDAO.findBy(a.getUserId(), a.getRefEntityId()) != null) {
                        DebugBuffer.addLine(DebugLevel.WARNING, "User has already researched this research, cleaning action entry");
                        //User has already researched this research
                        uds.removeActionEntry(ud.getUserId(), EActionType.RESEARCH, a);
                        aDAO.remove(a);
                    } else {
                        DebugBuffer.addLine(DebugLevel.WARNING, "User has not yet researched this research. Creating researchprogress entry");
                        Research research = resDAO.findById(a.getRefEntityId());
                        rp = new ResearchProgress();
                        rp.setUserId(a.getUserId());
                        rp.setResearchId(a.getRefEntityId());
                        rp.setRpLeft(research.getRp());
                        rp.setRpLeftComputer(research.getRpComputer());
                        rp = rpDAO.add(rp);
                        resPSetUpdate.put(a.getRefTableId(), rp);

                        aDAO.remove(a);
                        a.setRefTableId(rp.getId());
                        aDAO.add(a);
                    }
                }

                aResMap.put(a, rp);
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Add research " + rp.getResearchId() + " to user " + ud.getUserId());                

                if (rp.getRpLeft() > 0) {
                    needsRPs++;
                }
                if (rp.getRpLeftComputer() > 0) {
                    needsCRPs++;
                }
            }

            // if (ud.getUserId() == 72) Logger.getLogger().write("Needed RP="+needsRPs+" CRP="+needsCRPs);

            // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Researches needing FP " + needsRPs + " and CFP " + needsCRPs);

            boolean recalculate = true;
            boolean recalculateRp = true;
            boolean recalculateCrp = true;
            for (Map.Entry<Action, ResearchProgress> resMapEntry : aResMap.entrySet()) {
                Action a = resMapEntry.getKey();
                ResearchProgress rp = resMapEntry.getValue();

                if (a.getOnHold()) {
                    continue;
                }

                // Recalculate if a research needed less than the rpPerResearch
                if (recalculate) {
                    if (needsRPs > 0 && recalculateRp && recalculateRp) {
                        rpPerResearch = (int) Math.ceil((double) totalRP / needsRPs);
                        recalculateRp = false;
                    }
                    if (needsCRPs > 0 && recalculateCrp && recalculateCrp) {
                        rpPerResearchComp = (int) Math.ceil((double) totalRPComp / needsCRPs);
                        recalculateCrp = false;
                    }
                    recalculate = false;
                }

                if (rpPerResearch > totalRP) {
                    rpPerResearch = totalRP;
                }
                if (rpPerResearchComp > totalRPComp) {
                    rpPerResearchComp = totalRPComp;
                }

                Research r = resDAO.findById(rp.getResearchId());
                double bonusRatio = 1d + r.getBonus();

                //If ResearchProgress needs less points than points Per Research
                //DebugBuffer.addLine(DebugLevel.UNKNOWN, "Research " + rp.getResearchId() + " RPLEFT="+rp.getRpLeft() + 
                //        " CRPLEFT="+rp.getRpLeftComputer()); 
                //DebugBuffer.addLine(DebugLevel.UNKNOWN, "RP to subtract="+rpToSubtract + 
                //        " CRP to subract="+crpToSubtract); 

                boolean rpFinished = false;
                boolean crpFinished = false;
                if (rp.getRpLeft() > 0) {
                    needsRPs--;
                }
                if (rp.getRpLeftComputer() > 0) {
                    needsCRPs--;
                }
                if (rp.getRpLeft() <= ((int) (rpPerResearch * bonusRatio)) && rp.getRpLeft() > 0) {
                    totalRP -= (int) Math.floor((double) rp.getRpLeft() / bonusRatio);
                    rp.setRpLeft(0);
                    recalculate = true;
                    recalculateRp = true;
                }
                if (rp.getRpLeftComputer() <= ((int) (rpPerResearchComp * bonusRatio)) && rp.getRpLeftComputer() > 0) {
                    totalRPComp -= (int) Math.floor((double) rp.getRpLeftComputer() / bonusRatio);
                    rp.setRpLeftComputer(0);
                    recalculate = true;
                    recalculateCrp = true;
                }

                if (rp.getRpLeft() > 0 && rpPerResearch > 0) {
                    rp.setRpLeft(rp.getRpLeft() - (int) Math.floor((double) rpPerResearch * bonusRatio));
                    totalRP -= rpPerResearch;
                } else {
                    if (rp.getRpLeft() <= 0) {
                        rpFinished = true;
                    }
                }

                if (rpPerResearchComp > 0 && rp.getRpLeftComputer() > 0) {
                    // if (ud.getUserId() == 72) Logger.getLogger().write("Reduce research " + resDAO.findById(rp.getResearchId()).getName() + " by " + reduceBy + " CFP");
                    rp.setRpLeftComputer(rp.getRpLeftComputer() - (int) Math.floor((double) rpPerResearchComp * bonusRatio));
                    totalRPComp -= rpPerResearchComp;
                    // if (ud.getUserId() == 72) Logger.getLogger().write("RPC Left = " + totalRPComp);
                } else {
                    if (rp.getRpLeftComputer() <= 0) {
                        crpFinished = true;
                    }
                }

                //finished
                if (crpFinished && rpFinished) {
                    //Notification
                    finishedResearches.add(r);

                    //increment Bonus by 0,01%
                    TransactionHandler th = TransactionHandler.getTransactionHandler();
                    try {
                        th.startTransaction();
                        if (r.getBonus() < 500d) {
                            r.setBonus(r.getBonus() + 0.01d);
                            resDAO.update(r);
                        }
                        PlayerResearch pr = new PlayerResearch();
                        pr.setUserId(ud.getUserId());
                        pr.setResearchId(rp.getResearchId());
                        pr.setResTime(System.currentTimeMillis());
                        resPSetUpdate.remove(a.getRefTableId());

                        try {
                            pResDAO.add(pr);

                            if (pr.getResearchId() == 105) {
                                User u = uDAO.findById(ud.getUserId());
                                Logger.getLogger().write(LogLevel.INFO,"Remove user " + u.getGameName() + " [" + u.getUserId() + "] from Trial due to research of hyperspace technology");
                                LoginService.unlockTrialUser(u);
                            }
                        } catch (TransactionException teInner) {
                            DebugBuffer.warning("Recreated a Playerresearch Entry (" + pr.getUserId() + "/" + pr.getResearchId() + ")");
                            pResDAO.remove(pr);
                            pResDAO.add(pr);
                        }

                        uds.removeActionEntry(ud.getUserId(), EActionType.RESEARCH, a);
                        int timeFinished = a.getRefTableId();
                        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Research Done removing Action for RefTableId " + a.getRefTableId());
                        aDAO.remove(a);
                        if (aDAO.findByTimeFinished(timeFinished, EActionType.RESEARCH) != null) {
                            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "WTF action is still here though it should have been removed");
                        }
                        rpDAO.remove(rp);
                        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Research Done removing ResearchProgress with id " + rp.getId());
                        if (rpDAO.findById(timeFinished) != null) {
                            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "WTF researchProgress is still here");
                        }
                    } catch (Exception e) {
                        DebugBuffer.writeStackTrace("Error while finishing research: ", e);
                        DebugBuffer.addLine(DebugLevel.ERROR, "Detailed Information: ResearchID=" + r.getId() + " // PlayerResearch: Id=" + rp.getId() + " UserId=" + rp.getUserId() + " ResearchId=" + rp.getResearchId());
                        try {
                            th.rollback();
                        } catch (TransactionException te) {
                            DebugBuffer.writeStackTrace("Transaction Error", te);
                        }
                    } finally {
                        th.endTransaction();
                    }
                }
            }
            //Notification
            if (finishedResearches.size() > 0) {
                /***
                //Notification - START
                 */
                boolean logged = false;
                int userId = ud.getUserId();
                if (userId > 0) {
                    if (uDAO.findById(userId).getLastUpdate() >= (GameUtilities.getCurrentTick2() - 1)) {
                        logged = true;
                    }
                }
                NotificationToUser ntu = ntuDAO.getOrCreate(userId, NotificationType.ID_RESEARCH);
                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                    NotificationBuffer.addResearchNotification(userId, finishedResearches);
                }
            }
        }
    }

    //
    // Function to keep track of update influencing technologies
    //
    private void initializeSpecialResearch(int userId) {
        if (!specialResearch.containsKey(userId)) {
            specialResearch.put(userId, new ArrayList<Integer>());

            ArrayList<Integer> tmpList = specialResearch.get(userId);

            // Track Desintegrator
            if (ResearchUtilities.isResearchResearched(106, userId)) {
                tmpList.add(106);
            }
        }
    }

    private void calculateWeeklyIncome() {
        double weekDur = 144 * 7;

        for (User u : (ArrayList<User>) uDAO.findAll()) {
            UserData ud = uds.getUserDataEntry(u.getUserId());
            if (ud == null) {
                continue;
            }

            long totUserInc = 0;
            if (totalUserIncome.get(ud.getUserId()) != null) {
                totUserInc = totalUserIncome.get(ud.getUserId());
            }

            totUserInc = (long) ((double) totUserInc * weekDur);

            ud.setWeeklyIncome(
                    (long) Math.floor((double) ud.getWeeklyIncome() * (double) ((weekDur - 1d) / weekDur))
                    + (long) Math.floor((double) totUserInc * (double) (1d / weekDur)));
        }
    }

    public void addSpecialResearch(int userId, int researchId) {
        if (!specialResearch.containsKey(userId)) {
            initializeSpecialResearch(userId);
        }

        specialResearch.get(userId).add(researchId);
    }

    public void writeValuesToBuffer() {
        udDAO.updateAll(uds.getUpdatedUserData());
        prDAO.updateAll(uds.getUpdatedPlanetRess());

        ppList.removeAll(ppListRemovable);
        ppDAO.updateAll(ppList);       

        ArrayList<ResearchProgress> rpList = new ArrayList<ResearchProgress>();
        rpList.addAll(resPSetUpdate.values());
        rpDAO.updateAll(rpList);

        poDAO.updateAll(pob.getUpdatedOrders());
    }

    public int getCurrentTick() {
        return currentTick;
    }

    public void setCurrentTick(int currentTick) {
        this.currentTick = currentTick;
    }

    /**
     * @return the globalUpdateFailCause
     */
    public Exception getGlobalUpdateFailCause() {
        if (globalUpdateFailCause == null) {
            globalUpdateFailCause = new Exception("UNKNOWN CAUSE -- CHECK BUFFER");
        }

        return globalUpdateFailCause;
    }

    private void processVoteExpirationCheck() {
        for (Voting v : (ArrayList<Voting>) vDAO.findAll()) {
            if ((v.getExpire() < GameUtilities.getCurrentTick2()) && !v.getClosed()) {
                Vote vote = VoteUtilities.getVote(v.getVoteId());
                
                //If a DiplomacyVote => Find corresponding Diplomacyrelation and delete
                if (v.getType().equals(Voting.TYPE_ALLIANCERELATION)
                        || v.getType().equals(Voting.TYPE_ALLIANCEUSERRELATION)
                        || v.getType().equals(Voting.TYPE_USERRELATION)
                        || v.getType().equals(Voting.TYPE_USERALLIANCERELATION)) {

                    Integer from = (Integer) (vote.getMetaData(VoteMetadata.NAME_FROMID));
                    Integer to = (Integer) (vote.getMetaData(VoteMetadata.NAME_TOID));
                    EDiplomacyRelationType drt = null;
                    if (v.getType().equals(Voting.TYPE_ALLIANCERELATION)) {
                        drt = EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE;
                    } else if (v.getType().equals(Voting.TYPE_ALLIANCEUSERRELATION)) {
                        drt = EDiplomacyRelationType.ALLIANCE_TO_USER;
                    } else if (v.getType().equals(Voting.TYPE_USERRELATION)) {
                        drt = EDiplomacyRelationType.USER_TO_USER;
                    } else if (v.getType().equals(Voting.TYPE_USERALLIANCERELATION)) {
                        drt = EDiplomacyRelationType.USER_TO_ALLIANCE;
                    }
                    DiplomacyRelation dr = null;
                    try {
                        dr = drDAO.findBy(from, to, drt, true);
                    } catch (Exception e) {
                    }
                    if (dr != null) {
                        try {
                            DiplomacyService.deleteRelation(from, to, drt, true);
                        } catch (Exception e) {
                            DebugBuffer.addLine(DebugLevel.WARNING, "Couldnt delete relation when wanting to delete vote: " + e);
                        }
                    } else {
                        DebugBuffer.addLine(DebugLevel.WARNING, "Couldnt find relation when wanting to delete vote");
                    }
                    v.setClosed(true);
                    v.setResult(VoteResult.VOTE_NOT_EXPIRED);

                    vDAO.update(v);
                    //Normal Vote
                } else {
                    v.setClosed(true);
                    v.setResult(VoteResult.VOTE_NOT_EXPIRED);
                    vDAO.update(v);
                }
            }
        }
        for(DiplomacyRelation dr : (ArrayList<DiplomacyRelation>)Service.diplomacyRelationDAO.findAll()){
            if(dr.getPending()){
                int voteId = DiplomacyService.findCorrespondingVote(dr, 0);
                if(voteId > 0){
                    //Vote exists
                } else {
                    DebugBuffer.addLine(DebugLevel.WARNING, "Following Diplomacyrelation has no Vote and is Pending: " + dr.getFromId() + " => " + dr.getToId() + " | " + dr.getType() + " ACKWN : " + dr.getAcknowledged());
                    Service.diplomacyRelationDAO.remove(dr);
                    DebugBuffer.addLine(DebugLevel.WARNING, "Removed DR");                    
                }
            }
        }
    }
}
