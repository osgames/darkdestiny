/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.update;

import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.TradeRouteDetailDAO;
import at.viswars.model.TradeRouteDetail;
import at.viswars.trade.TradeRouteExt;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TradeRouteProcessingEntry {

    private TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    private final TradeRouteExt route;
    private final ArrayList<TradeRouteDetailProcessing> routeDetailsList =
            new ArrayList<TradeRouteDetailProcessing>();
    private final ArrayList<TradeRouteDetailProcessing> fromToList =
            new ArrayList<TradeRouteDetailProcessing>();
    private final ArrayList<TradeRouteDetailProcessing> toFromList =
            new ArrayList<TradeRouteDetailProcessing>();
    private boolean fromTo = false;
    private boolean toFrom = false;
    private int freeRouteCap;
    private boolean procClosed;
    private boolean deleted;

    public TradeRouteProcessingEntry(TradeRouteExt tre) {
        route = tre;

        // Load maxQty possible for every Detail
        ArrayList<TradeRouteDetail> routeDetails = trdDAO.getActiveByRouteId(tre.getBase().getId());
        int partRoutes = routeDetails.size();

        // Logger.getLogger().write(route.getBase().getType() + " - partROutes : " + partRoutes);
        // Logger.getLogger().write(route.getBase().getType() + " - tre.getCapacity() = " + tre.getCapacity());
        int maxCapPerRoute = 0;
        if (partRoutes > 0) {
            maxCapPerRoute = (int) Math.floor(tre.getCapacity() / partRoutes);
            // Logger.getLogger().write(route.getBase().getType() + " - Capacity per route = " + maxCapPerRoute);
        }

        for (TradeRouteDetail trd : routeDetails) {
            TradeRouteDetailProcessing trdp = new TradeRouteDetailProcessing(trd);
            routeDetailsList.add(trdp);
            if (trd.getReversed()) {
                toFrom = true;
                toFromList.add(trdp);
            } else {
                fromTo = true;
                fromToList.add(trdp);
            }
        }

        freeRouteCap = tre.getCapacity();
        // Logger.getLogger().write(route.getBase().getType() + " - freeRouteCap = " + maxCapPerRoute);
    }

    public TradeRouteExt getRoute() {
        return route;
    }

    public ArrayList<TradeRouteDetailProcessing> getRouteDetails() {
        return routeDetailsList;
    }

    public ArrayList<TradeRouteDetailProcessing> getFromToList() {
        return fromToList;
    }

    public ArrayList<TradeRouteDetailProcessing> getToFromList() {
        return toFromList;
    }

    public boolean isFromTo() {
        return fromTo;
    }

    public boolean isToFrom() {
        return toFrom;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
