/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.update;

import at.viswars.DebugBuffer;
import at.viswars.GameUtilities;
import at.viswars.Logger.Logger;
import at.viswars.dao.*;
import at.viswars.enumeration.ENoTransportReason;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.model.*;
import at.viswars.result.BaseResult;
import at.viswars.service.RessourceService;
import at.viswars.service.TradeService;
import at.viswars.trade.TradeUtilities;
import at.viswars.update.transport.DirectionalEdge;
import at.viswars.update.transport.PlanetNode;
import at.viswars.update.transport.TransportGraph;
import at.viswars.utilities.TitleUtilities;
import java.util.*;

/**
 *
 * @author Stefan
 */
public abstract class AbstractRouteProcessing {
    protected static TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    protected static TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    protected static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    protected static RessourceDAO rDAO = (RessourceDAO)DAOFactory.get(RessourceDAO.class);

    protected final UpdaterDataSet uds;

    protected HashMap<Integer, TradeRouteProcessingEntry> allEntries = new HashMap<Integer, TradeRouteProcessingEntry>();
    protected ArrayList<TradeRouteDetailProcessing> allDetailEntries = new ArrayList<TradeRouteDetailProcessing>();
    protected ArrayList<TradeRouteDetail> allRouteDetails = new ArrayList<TradeRouteDetail>();
    protected boolean debug = false;

    protected TransportGraph tg = new TransportGraph();    
    
    protected AbstractRouteProcessing(UpdaterDataSet uds) {
        this.uds = uds;
    }    
    
    public void processRoutes(int actTick) {
        // Clean temporary working values in Tranport Graph
        tg.cleanUp();
        
        // allDetailEntries.clear();
        // planetMaxIn.clear();
        // planetMaxOut.clear();
        ArrayList<TradeRouteDetailProcessing> markedForDeletion = new ArrayList<TradeRouteDetailProcessing>();

        // Process all planets and calculate maxIn and maxOut values
        ArrayList<PlanetNode> allPlanets = tg.getAllPlanets();

        if (debug) {
            Logger.getLogger().write("Start processing of transport routes - Found " + allPlanets.size() + " planet entries");
        }

        for (PlanetNode pn : allPlanets) {
            for (Ressource r : rDAO.findAllStoreableRessources()) {
                // -----------------------------
                // Check Source Planet for Stock
                // -----------------------------
                PlanetRessource pr = uds.getPlanetStorageEntry(pn.getPlanetId(), r.getId());
                long inStock = 0;
                if (pr != null) {
                    inStock = pr.getQty();
                }

                pr = uds.getPlanetRessEntry(pn.getPlanetId(),
                        EPlanetRessourceType.MINIMUM_TRANSPORT, r.getId().intValue());
                long minStock = 0;
                if (pr != null) {
                    minStock = pr.getQty();
                }

                long maxStock = uds.getMaxStorage(pn.getPlanetId(),
                        r.getId());

                if (debug) {
                    Logger.getLogger().write("["+pn.getPlanetId()+"/"+r.getId()+"] MAXOUT: " + inStock + " - " + minStock);
                }
                if (debug) {
                    Logger.getLogger().write("["+pn.getPlanetId()+"/"+r.getId()+"] MAXIN: " + maxStock + " - " + inStock);
                }

                long maxIn = Math.max(0, maxStock - inStock);
                long maxOut = Math.max(0, inStock - minStock);

                pn.setMaxOutgoingQty(r.getId(), (int)maxOut);
                pn.setMaxIncomingQty(r.getId(), (int)maxIn);
            }
        }

        // Max In and Max Out Quantities have been calculated at this point for every planet
        // Calculate outgoing quantities for each route
        // We do not have to calculate incoming quantities as each incoming is outgoing somewhere
        ArrayList<PlanetNode> outgoingOK = new ArrayList<PlanetNode>();
        ArrayList<PlanetNode> incomingOK = new ArrayList<PlanetNode>();

        // All routes transporting the same ressource outwards for a specific planet
        HashMap<PlanetNode,HashMap<Integer,ArrayList<TradeRouteDetailProcessing>>> sharedDelivery =
                new HashMap<PlanetNode,HashMap<Integer,ArrayList<TradeRouteDetailProcessing>>>();
        HashMap<PlanetNode,HashSet<Integer>> cappedDueToLowOutgoingCap =
                new HashMap<PlanetNode,HashSet<Integer>>();

        // Loop each planet and transport as much ressources as possible
        for (PlanetNode pn : allPlanets) {
            ArrayList<DirectionalEdge> allOutgoing = pn.getOutgoingTrades();
           
            // number of routes transporting the same ressource outgoing from current planet
            int sharedDeliveryCount = 0;
            
            for (Ressource r : rDAO.findAllStoreableRessources()) {                
                sharedDeliveryCount += pn.getOutgoingForRess(r.getId()).size();
                
                if (pn.getOutgoingForRess(r.getId()).size() > 1) {
                    if (!sharedDelivery.containsKey(pn)) {
                        HashMap<Integer,ArrayList<TradeRouteDetailProcessing>> shared =
                                new HashMap<Integer,ArrayList<TradeRouteDetailProcessing>>();
                        shared.put(r.getId(), pn.getOutgoingForRess(r.getId()));
                        sharedDelivery.put(pn,shared);
                    } else {
                        // TODO How can we run into this case, should not be possible?!?
                        DebugBuffer.warning("ENTERED STRANGE CONDITION 1: AbstractRouteProcessing");
                        HashMap<Integer,ArrayList<TradeRouteDetailProcessing>> shared = sharedDelivery.get(pn);
                        shared.put(r.getId(), pn.getOutgoingForRess(r.getId()));
                    }
                }
            }

            // Now we know what planets accept what quantities and whats their maximum they can deliver
            // We also have information which outgoing routes process the same ressource
            // The task now is to process this information and set all actual transport quantities
            HashMap<DirectionalEdge,Integer> freeCapacity = new HashMap<DirectionalEdge,Integer>();
            
            for (DirectionalEdge de : pn.getOutgoingTrades()) {
                int capPerRessource = (int)Math.ceil(de.getMaxCapacity() / de.getNoOfEntries());
                
                for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) { 
                    TradeRoute tr = trDAO.getById(trdp.getBase().getRouteId());
                    
                    int currRessId = trdp.getBase().getRessId();
                    
                    PlanetNode start = de.getSource();
                    PlanetNode target = de.getTarget();
                    
                    trdp.setNewMaximum(Math.min(start.getMaxOutgoingQty(currRessId),target.getMaxIncomingQty(currRessId)));
                    
                    // Check restriction for Transport Qty 
                    if (trdp.getBase().getMaxQty() > 0) {
                        long remainingQty = trdp.getBase().getMaxQty() - trdp.getBase().getQtyDelivered();
                        if (remainingQty < trdp.getMaxQty()) {
                            trdp.setNewMaximum((int)remainingQty);
                        }
                    }
                }
                
                // After this loop each detail route has maximum qty assigned, only restricted by planetary storage
                for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {           
                    // java.lang.System.out.println("Assigned Qty for " + trdp.getBase().getRessId() + " is " + capPerRessource + " MaxQty is " + trdp.getMaxQty());
                    if (capPerRessource > trdp.getMaxQty()) {
                        // java.lang.System.out.println("Set Assigned Qty of " + trdp.getMaxQty() + " for ressource " + trdp.getBase().getRessId());
                        trdp.setAssignedQty(trdp.getMaxQty());
                        if (trdp.getMaxQty() == 0) trdp.setClosed(true);
                        
                        // Add additional Capacity to free Capacity
                        int excess = capPerRessource - trdp.getMaxQty();
                        if (freeCapacity.containsKey(de)) {
                            freeCapacity.put(de, freeCapacity.get(de) + excess);
                        } else {
                            freeCapacity.put(de, excess);
                        }
                    } else {
                        trdp.setAssignedQty(capPerRessource);
                    }                
                }
            }
            
            // Check if some ressource is transported too much, adjust values and try to 
            // redistribute capacities to other ressources            
            do {
                HashMap<Integer,Integer> totalQty = new HashMap<Integer,Integer>();
                
                // Sum up outgoing quantities
                for (DirectionalEdge de : pn.getOutgoingTrades()) {
                    for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                        if (trdp.isClosed()) continue;
                        
                        int ressId = trdp.getBase().getRessId();
                        
                        // if (totalQty.containsKey(ressId)) {
                        //     totalQty.put(ressId, trdp.getAssignedQty());
                        // } else {
                        int actQty = 0;
                        if (totalQty.get(ressId) != null) {
                            actQty = totalQty.get(ressId);
                        }
                        
                        totalQty.put(ressId, actQty + trdp.getAssignedQty());
                        // java.lang.System.out.println("Total Qty out for " + ressId + " is " + totalQty.get(ressId));                        
                        // }
                    }
                }
                
                // Reduce excess qty from routes and increase free capacity
                for (Map.Entry<Integer,Integer> ressEntry : totalQty.entrySet()) {
                    // java.lang.System.out.println("Check Ressource " + ressEntry.getKey() + " with outgoing qty of " + ressEntry.getValue());
                    if (ressEntry.getValue() > pn.getMaxOutgoingQty(ressEntry.getKey())) {
                        // java.lang.System.out.println("Outgoing qty of " + ressEntry.getValue() + " is higher than " + pn.getMaxOutgoingQty(ressEntry.getKey()));
                        int shares = sharedDelivery.get(pn).get(ressEntry.getKey()).size();                        
                        int excess = ressEntry.getValue() - pn.getMaxOutgoingQty(ressEntry.getKey());
                        
                        // java.lang.System.out.println(shares + " shares and excess of " + excess);
                        
                        // Reduce qty of all shared outgoing     
                        for (DirectionalEdge de : pn.getOutgoingTrades()) {
                            TradeRouteDetailProcessing trdp = de.getEntryForRess(ressEntry.getKey());
                            if (trdp != null) {
                                int reduceBy = (int)Math.floor((double)excess / (double)ressEntry.getValue() * (double)trdp.getAssignedQty());
                                // java.lang.System.out.println("Reduce outgoing of " + trdp.getAssignedQty() + " by " + reduceBy);
                                
                                trdp.setAssignedQty(trdp.getAssignedQty() - reduceBy);
                                trdp.setNewMaximum(trdp.getAssignedQty());
                                trdp.setClosed(true);
                                
                                if (freeCapacity.containsKey(de)) {
                                    freeCapacity.put(de, freeCapacity.get(de) + reduceBy);
                                } else {
                                    freeCapacity.put(de, reduceBy);
                                }                                
                            }
                        }
                    }
                }
                
                // Excess quantities removed try to assign freeCapacity to other routes
                boolean couldRedistribute = false;
                for (DirectionalEdge de : pn.getOutgoingTrades()) {
                    if (freeCapacity.get(de) == null) continue;
                    boolean couldRedistributeCurrEdge = false;
                    
                    int currEdgeFreeCap = freeCapacity.get(de);
                    
                    // java.lang.System.out.println("Curr Edge free Cap is " + currEdgeFreeCap);
                    
                    int split = 0;
                    for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                        if (!trdp.isClosed()) split++;
                    }
                    
                    if (split == 0) {
                        continue;
                    }                                        
                    
                    int share = currEdgeFreeCap / split;
                    
                    // java.lang.System.out.println("Split count is " + split + " share is " + share);
                    
                    for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                        if (trdp.isClosed()) {
                            continue;
                        }

                        if (share > (trdp.getMaxQty() - trdp.getAssignedQty() )) {
                            share = trdp.getMaxQty() - trdp.getAssignedQty();
                        }                        
                        
                        if (share == 0) {                            
                            trdp.setClosed(true);
                            continue;
                        }
                        
                        couldRedistributeCurrEdge = true;    
                        couldRedistribute = true;
                        
                        // java.lang.System.out.println("Increase transport qty of " + trdp.getAssignedQty() + " by " + share);
                        
                        trdp.setAssignedQty(trdp.getAssignedQty() + share);                                                
                        freeCapacity.put(de,freeCapacity.get(de) - share);
                        
                        // java.lang.System.out.println("[2] Curr Edge free Cap is " + freeCapacity.get(de));
                    }                                        
                    
                    if (!couldRedistributeCurrEdge) freeCapacity.remove(de);
                }                               
                
                if (!couldRedistribute) freeCapacity.clear();
            } while (!freeCapacity.isEmpty());
        }
        
        // Quantities have been assigned - now store data to database
        ArrayList<TradeRouteDetail> toUpdate = new ArrayList<TradeRouteDetail>();
        
        for (TradeRouteDetailProcessing trdp : allDetailEntries) {
            TradeRouteDetail trd = trdp.getBase();
            
            if (trdp.getAssignedQty() > 0) {
                trdp.getBase().setLastTransport(trdp.getAssignedQty());
                trdDAO.update(trdp.getBase());
                
                TradeRoute tr = trDAO.getById(trdp.getBase().getRouteId());               
                    
                int sourcePlanet = tr.getStartPlanet();
                int targetPlanet = tr.getTargetPlanet();
                PlanetRessource source;
                PlanetRessource target;
                       
                if (trdp.getBase().getReversed()) {
                    sourcePlanet = tr.getTargetPlanet();
                    targetPlanet = tr.getStartPlanet();
                    target = uds.getPlanetStorageEntry(tr.getStartPlanet(), trdp.getBase().getRessId());
                    source = uds.getPlanetStorageEntry(tr.getTargetPlanet(), trdp.getBase().getRessId());                      
                } else {
                    sourcePlanet = tr.getStartPlanet();
                    targetPlanet = tr.getTargetPlanet();                    
                    source = uds.getPlanetStorageEntry(tr.getStartPlanet(), trdp.getBase().getRessId());
                    target = uds.getPlanetStorageEntry(tr.getTargetPlanet(), trdp.getBase().getRessId());                
                }
                
               // java.lang.System.out.println("Process route " + tr.getId() + " from " + sourcePlanet + " to " + targetPlanet + " RESSID: " + trdp.getBase().getRessId());                
                
                try {
                    TitleUtilities.incrementCondition(ConditionToTitle.MERCHANT_RESOURCES, ppDAO.findByPlanetId(targetPlanet).getUserId(), trdp.getAssignedQty());
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugBuffer.DebugLevel.DEBUG, "Error1 in traderouteprocessingNew with titles : " + e);
                }

                if (target == null) {
                    uds.createNewStorageRessEntry(targetPlanet, trdp.getBase().getRessId(), trdp.getAssignedQty());
                } else {
                    target.setQty(target.getQty() + trdp.getAssignedQty());
                }          
                
                source.setQty(source.getQty() - trdp.getAssignedQty());
                
                // Increase Qty already delivered                
                trd.setQtyDelivered(trd.getQtyDelivered() + trdp.getAssignedQty());
                
                // Do Qty check                      
                if (trdp.getBase().getMaxQty() > 0) {
                    if ((trd.getMaxQty() - trd.getQtyDelivered()) <= 0) {
                        markedForDeletion.add(trdp);
                        if (debug) {
                            Logger.getLogger().write("Mark route for deletion due to maxQty");
                        }                        
                        // trpe.setDeleted(true);
                    } else {
                        toUpdate.add(trd);
                    }
                } else {
                    toUpdate.add(trd);
                }
            } else {
                if (trd.getLastTransport() != 0) {
                    trd.setLastTransport(0);
                }
                
                toUpdate.add(trd);
            }
            
            if (trdp.getBase().getMaxDuration() > 0) {
                if (trdp.getBase().getRemainingTime(actTick) <= 0) {
                    markedForDeletion.add(trdp);
                    // trpe.setDeleted(true);
                    if (debug) {
                        Logger.getLogger().write("Mark route for deletion due to maxDur");
                    }
                }
            }            
        }
        
        trdDAO.updateAll(toUpdate);
        for (TradeRouteDetailProcessing trdp : markedForDeletion) {
            TradeRoute tr = trDAO.getById(trdp.getBase().getRouteId());
            TradeService.deleteRouteDetail(tr.getUserId(), trdp.getBase().getId());
        }
        
        
            /*
            // If in trade 
            // If we have routes which have reduced Quantity due to target, we can release this qty and increase other routes by that value
            // This step has to be repeated until no higher quantities can be assigned
            int newFreeCapacity = 0;
            HashSet<DirectionalEdge> mayIncrease = new HashSet<DirectionalEdge>();
            boolean firstRun = true;
            
            // Current assigned Quantity for each DetailRoute (= 1 Ressource, 1 Direction)
            HashMap<TradeRouteDetailProcessing,Integer> currAssignment = new HashMap<TradeRouteDetailProcessing,Integer>();
            
            do {
                // newFreeCapacity can only be larger 0 if TradeCalculation
                int routesToBalance = 0;
                
                if (newFreeCapacity > 0) {
                    int increaseBy = (int)Math.floor(newFreeCapacity / mayIncrease.size());
                    for (DirectionalEdge de : mayIncrease) {
                        de.increaseMaxCapacity((int)Math.floor(increaseBy / de.getDistance()));
                        for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                            routesToBalance++;
                            trdp.resetMaxQty();
                        }
                    }
                    
                    newFreeCapacity = 0;
                }
                
                for (DirectionalEdge de : allOutgoing) {
                    if (firstRun) {
                        for (TradeRouteDetailProcessing trpe : de.getDetailEntries()) {
                            trpe.getBase().setCapReason(ENoTransportReason.NONE);
                        }
                        
                        mayIncrease.add(de);
                    } else {
                        if (!mayIncrease.contains(de)) continue;
                    }

                    int unassignedQty = 0;
                    
                    if (firstRun) {
                        unassignedQty = de.getMaxCapacity();
                    } else {
                        unassignedQty = de.getAdditionalCapacity();
                    }

                    int capPerEntry = 0;

                    if (this instanceof TransportProcessing) {
                        if (de.getNoOfEntries() == 0) continue;
                        capPerEntry = (int)Math.ceil(de.getMaxCapacity() / de.getNoOfEntries());
                    } else if (this instanceof TradeRouteProcessing) {
                        if (firstRun) {
                            capPerEntry = (int)Math.ceil(de.getMaxCapacity() / sharedDeliveryCount);
                        } else {
                            if (de.getNoOfEntries() == 0) continue;
                            capPerEntry = (int)Math.floor(unassignedQty / de.getNoOfEntries());
                        }
                    }

                    boolean allAssigned = false;

                    while (!allAssigned) {                       
                        allAssigned = true;

                        if (unassignedQty == 0) {
                            continue;
                        }

                        for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {                                                        
                            if (trdp.getMaxQty() == 0) continue;
                            
                            if (this instanceof TransportProcessing) trdp.resetMaxQty();
                            
                            if (unassignedQty == 0) {
                                break;
                            }

                            int currentlyAssigned = 0;
                            
                            if (currAssignment.containsKey(trdp)) {
                                currentlyAssigned = currAssignment.get(trdp);
                                
                                if (currAssignment.get(trdp).equals(trdp.getMaxQty())) {
                                    continue;
                                }
                            }

                            int qtyToAssign = Integer.MAX_VALUE;

                            // Cap on route restrictions
                            if (trdp.getMaxQty() < capPerEntry) {
                                qtyToAssign = trdp.getMaxQty();
                            }

                            if (qtyToAssign > capPerEntry) {
                                qtyToAssign = capPerEntry;
                            }                            
                            
                            trdp.setNewMaximum(qtyToAssign);
                            
                            // If this is Trade we always work with maximum capacity
                            // In case of transport capPerEntry is calculated from unassigned Quantity as we only work in a local DirectionalEdge
                            // if (this instanceof TradeRouteProcessingNew) qtyToAssign -= currentlyAssigned;

                            // Cap on start and target
                            if (de.getSource().getMaxOutgoingQty(trdp.getBase().getRessId()) < qtyToAssign) {
                                qtyToAssign = de.getSource().getMaxOutgoingQty(trdp.getBase().getRessId());
                                trdp.setNewMaximum(qtyToAssign);

                                if (cappedDueToLowOutgoingCap.containsKey(pn)) {
                                    HashSet<Integer> ressList = cappedDueToLowOutgoingCap.get(pn);
                                    ressList.add(trdp.getBase().getRessId());
                                } else {
                                    HashSet<Integer> ressList = new HashSet<Integer>();
                                    ressList.add(trdp.getBase().getRessId());
                                    cappedDueToLowOutgoingCap.put(pn,ressList);
                                }
                                
                                if ((currentlyAssigned == 0) && (qtyToAssign == 0)) {
                                    trdp.getBase().setCapReason(ENoTransportReason.SOURCE_LOW);                                                                        
                                }                                
                                
                                trdp.setClosed(true);
                            }
                            if (de.getTarget().getMaxIncomingQty(trdp.getBase().getRessId()) < qtyToAssign) {
                                qtyToAssign = de.getTarget().getMaxIncomingQty(trdp.getBase().getRessId());
                                if (qtyToAssign < trdp.getMaxQty()) {                                
                                    if (this instanceof TradeRouteProcessing) newFreeCapacity += (int)Math.floor((double)(trdp.getMaxQty() - qtyToAssign) * de.getDistance());

                                    if ((currentlyAssigned == 0) && (qtyToAssign == 0)) {
                                        trdp.getBase().setCapReason(ENoTransportReason.TARGET_FULL);                                                                             
                                    }
    
                                    trdp.setClosed(true);
                                    mayIncrease.remove(de);
                                }

                                trdp.setNewMaximum(qtyToAssign);
                            }

                            // Cap on unassignedQty
                            if (currAssignment.get(trdp) != null) {
                                currAssignment.put(trdp, currAssignment.get(trdp) + qtyToAssign);
                            } else {
                                currAssignment.put(trdp, qtyToAssign);
                            }

                            de.getSource().incCurrentAssignedOutgoingQty(trdp.getBase().getRessId(), qtyToAssign);
                            de.getTarget().incCurrentAssignedIncomingQty(trdp.getBase().getRessId(), qtyToAssign);
                            unassignedQty -= qtyToAssign;

                            if ((((qtyToAssign == 0) && (unassignedQty == 0)) && (this instanceof TransportProcessing)) ||
                                   ((qtyToAssign == 0) && (this instanceof TradeRouteProcessing))) {
                                continue;
                            }
                            allAssigned = false;
                        }

                        if (this instanceof TransportProcessing) {
                            capPerEntry = (int)Math.ceil((double)unassignedQty / (double)de.getNoOfEntries());
                        } else if (this instanceof TradeRouteProcessing) {
                            if (firstRun) {
                                capPerEntry = (int)Math.ceil(de.getMaxCapacity() / sharedDeliveryCount);
                            } else {
                                capPerEntry = (int)Math.ceil(de.getMaxCapacity() / routesToBalance);
                            }
                        }                    
                    }

                    for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                        if (!currAssignment.containsKey(trdp)) {
                            trdp.setAssignedQty(0);
                        } else {
                            trdp.setAssignedQty(currAssignment.get(trdp));
                        }
                    }
                }            
                
                firstRun = false;
            } while ((this instanceof TradeRouteProcessing) && (newFreeCapacity > 0) && (mayIncrease.size() > 0));
        }

        if (cappedDueToLowOutgoingCap.size() > 0) {                        
            for (Map.Entry<PlanetNode,HashSet<Integer>> cappedEntry : cappedDueToLowOutgoingCap.entrySet()) {
                PlanetNode pn = cappedEntry.getKey();
                
                // if (this instanceof TradeRouteProcessingNew) if (pn.getPlanetId() == 11503) // java.lang.System.out.println("We have capping!");
                
                HashSet<Integer> cappedRess = cappedEntry.getValue();

                for (Iterator<Integer> it = cappedRess.iterator();it.hasNext();) {
                    int ressId = (int)it.next();                    

                    // Do some fucking complicated shit to balance this ;)
                    // Good approach would be reset all ressource entries for curent ressource
                    // and redistribute them according to allowed Qty of the directional edges
                    // to keep it somehow easy do not care about other routes if there is no qty left on a
                    // directional route skip and try to get the qty on other routes
                    int qtyToDistribute = 0;

                    if (sharedDelivery.get(pn) == null) continue;
                    if (debug) Logger.getLogger().write("Shared Ressource " + ressId + " was capped down on planet " + pn.getPlanetId());
                    if ((sharedDelivery.get(pn).get(ressId)) == null) {
                        if (debug) Logger.getLogger().write("Ressource " + ressId + " was not found in shared!");
                        continue;
                    }

                    ArrayList<TradeRouteDetailProcessing> trdpList = sharedDelivery.get(pn).get(ressId);
                    for (TradeRouteDetailProcessing trdp : trdpList) {
                        DirectionalEdge de = pn.getDirectionalEdgeOutFor(ressId, trdp);
                        qtyToDistribute += trdp.getAssignedQty();
                        
                        de.getSource().decCurrentAssignedOutgoingQty(ressId, trdp.getAssignedQty());
                        de.getTarget().decCurrentAssignedIncomingQty(ressId, trdp.getAssignedQty());
                        trdp.setAssignedQty(0);
                        trdp.resetMaxQty();
                        
                        trdp.setNewMaximum(de.getMaxCapacity());
                    }

                    if (debug) Logger.getLogger().write("Redistribute " + qtyToDistribute + " on " + trdpList.size() + " routes");
                    boolean allAssigned = false;
                    int capPerEntry = (int)Math.ceil((double)qtyToDistribute / (double)trdpList.size());
                    int unassignedEntries = trdpList.size();
                    if (debug) Logger.getLogger().write("capPerEntry = " + capPerEntry);

                    HashMap<TradeRouteDetailProcessing,Integer> currAssignment = new HashMap<TradeRouteDetailProcessing,Integer>();

                    while (!allAssigned) {
                        allAssigned = true;

                        if (qtyToDistribute == 0) continue;

                        for (TradeRouteDetailProcessing trdp : trdpList) {
                            if (qtyToDistribute == 0) break;
                            DirectionalEdge de = pn.getDirectionalEdgeOutFor(ressId, trdp);
                            trdp.resetMaxQty();
                            trdp.setNewMaximum(de.getMaxCapacity());                              

                            if (currAssignment.containsKey(trdp)) {
                                // Logger.getLogger().write("CA: " + currAssignment.get(trdp) + " vs. MaxQty: " + trdp.getMaxQty());

                                if (currAssignment.get(trdp).equals(trdp.getMaxQty())) {
                                    continue;
                                }
                            }

                            int qtyToAssign = Integer.MAX_VALUE;

                            // Cap on route restrictions                                                     
                            if (trdp.getMaxQty() < capPerEntry) {
                                qtyToAssign = trdp.getMaxQty();
                            }

                            if (qtyToAssign > capPerEntry) {
                                qtyToAssign = capPerEntry;
                            }                            
                            
                            trdp.setNewMaximum(qtyToAssign);
                            
                            // Cap on start and target
                            if (de.getSource().getMaxOutgoingQty(trdp.getBase().getRessId()) < qtyToAssign) {
                                // Logger.getLogger().write("Restrict by MAX Outgoing on " + de.getSource().getPlanetId()+ ": " + de.getSource().getMaxOutgoingQty(trdp.getBase().getRessId()));
                                qtyToAssign = de.getSource().getMaxOutgoingQty(trdp.getBase().getRessId());
                                trdp.setNewMaximum(qtyToAssign);
                            }
                            if (de.getTarget().getMaxIncomingQty(trdp.getBase().getRessId()) < qtyToAssign) {
                                // Logger.getLogger().write("Restrict by MAX Incoming on " + de.getTarget().getPlanetId() + ": " + de.getTarget().getMaxIncomingQty(trdp.getBase().getRessId()));
                                qtyToAssign = de.getTarget().getMaxIncomingQty(trdp.getBase().getRessId());
                                trdp.setNewMaximum(qtyToAssign);
                            }

                            // Cap on unassignedQty
                            // if (qtyToAssign == 0) trdp.setNewMaximum(0);

                            if (currAssignment.get(trdp) != null) {
                                currAssignment.put(trdp, currAssignment.get(trdp) + qtyToAssign);
                            } else {
                                currAssignment.put(trdp, qtyToAssign);
                            }

                            de.getSource().incCurrentAssignedOutgoingQty(trdp.getBase().getRessId(), qtyToAssign);
                            de.getTarget().incCurrentAssignedIncomingQty(trdp.getBase().getRessId(), qtyToAssign);
                            qtyToDistribute -= qtyToAssign;

                            // Logger.getLogger().write("Assigning " + currAssignment.get(trdp) + " for " + trdp + " [UNASSIGNED: " + qtyToDistribute + "]");

                            if (qtyToAssign == 0) continue;
                            allAssigned = false;
                        }

                        capPerEntry = (int)Math.ceil((double)qtyToDistribute / (double)trdpList.size());
                        if (debug) Logger.getLogger().write("capPerEntry = " + capPerEntry);
                    }

                    for (TradeRouteDetailProcessing trdp : trdpList) {
                        if (!currAssignment.containsKey(trdp)) {
                            trdp.setAssignedQty(0);
                        } else {
                            trdp.setAssignedQty(currAssignment.get(trdp));
                        }
                        
                        // trdp.getBase().setLastTransport(trdp.getAssignedQty());
                        // trdp.getBase().setQtyDelivered(trdp.getBase().getQtyDelivered() + trdp.getBase().getLastTransport());
                        // trdDAO.update(trdp.getBase());
                    }
                }
            }
            // TODO
        }

        // Quantities have been assigned
        // Now do the actual transaction
        ArrayList<TradeRouteDetail> toUpdate = new ArrayList<TradeRouteDetail>();

        for (TradeRouteDetailProcessing trdp : allDetailEntries) {
            toUpdate.add(trdp.getBase());
            
            if (trdp.getAssignedQty() == 0) {
                if ((trdp.getBase().getLastTransport() != 0) || (trdp.getBase().getCapReason().equals(ENoTransportReason.NONE))) {
                    trdp.getBase().setLastTransport(0);
                    trdDAO.update(trdp.getBase());
                }
                continue;
            }             
            
            TradeRouteProcessingEntry trpe = allEntries.get(trdp.getBase().getRouteId());

            // if ((trpe.getRoute().getBase().getStartPlanet() == 3545) &&
            //     (trpe.getRoute().getBase().getTargetPlanet() == 3540) &&
            //     (trdp.getBase().getRessId() == 1)) {
            //     debug = true;
            // }
            

            if (trdp.getBase().getReversed()) {
                PlanetRessource target = uds.getPlanetStorageEntry(trpe.getRoute().getBase().getStartPlanet(), trdp.getBase().getRessId());
                PlanetRessource source = uds.getPlanetStorageEntry(trpe.getRoute().getBase().getTargetPlanet(), trdp.getBase().getRessId());
                try {
                    TitleUtilities.incrementCondition(ConditionToTitle.MERCHANT_RESOURCES, ppDAO.findByPlanetId(target.getPlanetId()).getUserId(), trdp.getAssignedQty());
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugBuffer.DebugLevel.DEBUG, "Error1 in traderouteprocessingNew with titles : " + e);
                }

                if (target == null) {
                    uds.createNewStorageRessEntry(trpe.getRoute().getBase().getStartPlanet(), trdp.getBase().getRessId(), trdp.getAssignedQty());
                } else {
                    target.setQty(target.getQty() + trdp.getAssignedQty());
                }

                if (!(this instanceof TradeRouteProcessing)) {
                    if ((source == null) && (trdp.getAssignedQty() > 0)) {
                        if (debug) {
                            Logger.getLogger().write("Tried to transport a ressource (" + trdp.getBase().getRessId()
                                    + ") with qty (" + trdp.getAssignedQty()
                                    + ") from a planet " + trpe.getRoute().getBase().getTargetPlanet() + " where ressource does not exist!");
                        }
                        DebugBuffer.error("Tried to transport a ressource (" + trdp.getBase().getRessId()
                                + ") with qty (" + trdp.getAssignedQty()
                                + ") from a planet " + trpe.getRoute().getBase().getTargetPlanet() + " where ressource does not exist!");
                    } else {
                        if (source != null) {
                            source.setQty(source.getQty() - trdp.getAssignedQty());
                        }
                    }
                }
            } else {
                PlanetRessource source = uds.getPlanetStorageEntry(trpe.getRoute().getBase().getStartPlanet(), trdp.getBase().getRessId());
                PlanetRessource target = uds.getPlanetStorageEntry(trpe.getRoute().getBase().getTargetPlanet(), trdp.getBase().getRessId());
                try {
                    TitleUtilities.incrementCondition(ConditionToTitle.MERCHANT_RESOURCES, ppDAO.findByPlanetId(source.getPlanetId()).getUserId(), trdp.getAssignedQty());
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugBuffer.DebugLevel.DEBUG, "Error1 in Transptraderouteprocessing ortroute with titles : " + e);
                }
                if (target == null) {
                    uds.createNewStorageRessEntry(trpe.getRoute().getBase().getTargetPlanet(), trdp.getBase().getRessId(), trdp.getAssignedQty());
                } else {
                    if (debug) Logger.getLogger().write("Increase Quantity by " + trdp.getAssignedQty() + " on target");
                    target.setQty(target.getQty() + trdp.getAssignedQty());
                }

                if (!(this instanceof TradeRouteProcessing)) {
                    if ((source == null) && (trdp.getAssignedQty() > 0)) {
                        if (debug) {
                            Logger.getLogger().write("Tried to transport a ressource (" + trdp.getBase().getRessId()
                                    + ") with qty (" + trdp.getAssignedQty()
                                    + ") from a planet " + trpe.getRoute().getBase().getStartPlanet() + " where ressource does not exist!");
                        }
                        DebugBuffer.error("Tried to transport a ressource (" + trdp.getBase().getRessId()
                                + ") with qty (" + trdp.getAssignedQty()
                                + ") from a planet " + trpe.getRoute().getBase().getStartPlanet() + " where ressource does not exist!");
                    } else {
                        if (source != null) {
                            source.setQty(source.getQty() - trdp.getAssignedQty());
                        }
                    }
                }
            }

            trdp.getBase().setQtyDelivered(trdp.getBase().getQtyDelivered() + trdp.getAssignedQty());
            // Logger.getLogger().write("["+trdp+"] SET LAST TRANSPORT TO " + trdp.getAssignedQty());
            trdp.getBase().setLastTransport(trdp.getAssignedQty());

            // Do Qty check                      
            if (trdp.getBase().getMaxQty() > 0) {
                if ((trdp.getBase().getMaxQty() - trdp.getBase().getQtyDelivered()) <= 0) {
                    markedForDeletion.add(trdp);
                    trpe.setDeleted(true);
                } else {
                    toUpdate.add(trdp.getBase());
                }
            } else {
                toUpdate.add(trdp.getBase());
            }

            if (trdp.getBase().getMaxDuration() > 0) {
                if (trdp.getBase().getRemainingTime(actTick) <= 0) {
                    markedForDeletion.add(trdp);
                    trpe.setDeleted(true);
                    if (debug) Logger.getLogger().write("Mark route for deletion due to maxDur");
                }
            }
        }

        trdDAO.updateAll(toUpdate);
        allRouteDetails.removeAll(markedForDeletion);
        for (TradeRouteDetailProcessing trdp : markedForDeletion) {
            TradeRoute tr = trDAO.getById(trdp.getBase().getRouteId());
            TradeUtilities.deleteRouteDetail(tr, trdp.getBase());

            // Remove from graph
            allDetailEntries.remove(trdp);
            for (PlanetNode pn : tg.getAllPlanets()) {
                if (debug) Logger.getLogger().write("Cleaning up route for planet " + pn.getPlanetId() + " Ressource " + trdp.getBase().getRessId());

                Set<DirectionalEdge> remove = new HashSet<DirectionalEdge>();

                for (DirectionalEdge de : pn.getOutgoingTrades()) {
                    if (de.getDetailEntries().contains(trdp))
                        de.getDetailEntries().remove(trdp);

                    // if directionalEdge has no more entries remove it!
                    if (de.getDetailEntries().isEmpty()) {
                        remove.add(de);
                    }
                }

                for (DirectionalEdge de : pn.getIncomingTrades()) {
                    if (de.getDetailEntries().contains(trdp))
                        de.getDetailEntries().remove(trdp);

                    // if directionalEdge has no more entries remove it!
                    if (de.getDetailEntries().isEmpty()) {
                        remove.add(de);
                    }
                }

                for (DirectionalEdge de : remove) {
                    de.getSource().removeDirectionalEdgeOut(de);
                    de.getTarget().removeDirectionalEdgeIn(de);
                }
            }
        }
        */
    }    
    
    protected void destroyRoute(TradeRoute tr) {
        if (allEntries.containsKey(tr.getId())) {
            DebugBuffer.trace("Remove Traderoute Entry "+tr.getId()+" from RouteProcessing");
            
            for (Iterator<TradeRouteDetailProcessing> trdpIt = allDetailEntries.iterator();trdpIt.hasNext();) {
                TradeRouteDetailProcessing trdp = trdpIt.next();
                if (trdp.getBase().getRouteId().equals(tr.getId())) trdpIt.remove();
            }
            
            TradeRouteProcessingEntry trpe = allEntries.remove(tr.getId());
        }
        
        BaseResult br = TradeService.deleteRoute(tr.getUserId(), tr.getId());
        if (br.isError()) {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Error while destroyingRoute: " + br.getMessage());
        }        
    }    
    
    protected void clearAllSetRoutes() {
        ArrayList<TradeRouteDetail> toUpdate = new ArrayList<TradeRouteDetail>();

        for (TradeRouteDetail trd : ((ArrayList<TradeRouteDetail>)trdDAO.findAll())) {
            if (trd.getLastTransport() != 0) {
                trd.setLastTransport(0);
                toUpdate.add(trd);
            }
        }

        trdDAO.updateAll(toUpdate);
    }        
}
