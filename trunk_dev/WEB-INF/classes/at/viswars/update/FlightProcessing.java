/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.update;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.fleet.FleetMovement;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.dao.ActionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.model.Action;
import at.viswars.model.FleetDetail;
import java.util.Iterator;
import at.viswars.dao.FleetDetailDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.SystemDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.dao.UserSettingsDAO;
import at.viswars.dao.ViewTableDAO;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.diplomacy.combat.CombatGroupEntry;
import at.viswars.diplomacy.combat.CombatGroupResolver;
import at.viswars.diplomacy.combat.CombatGroupResult;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.ELocationType;
import at.viswars.model.Planet;
import at.viswars.model.PlanetDefense;
import at.viswars.model.PlayerFleet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.User;
import at.viswars.result.BaseResult;
import at.viswars.result.DiplomacyResult;
import at.viswars.service.LoginService;
import at.viswars.service.MainService;
import at.viswars.spacecombat.CombatHandler_NEW;
import at.viswars.spacecombat.CombatReportGenerator;
import at.viswars.utilities.DiplomacyUtilities;
import at.viswars.utilities.ViewTableUtilities;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class FlightProcessing {

    private final UpdaterDataSet uds;
    private final int startTick;
    private TreeMap<Integer, ArrayList<FlightProcessingEntry>> allFlights =
            new TreeMap<Integer, ArrayList<FlightProcessingEntry>>();
    private ArrayList<FlightProcessingEntry> checkForInterception =
            new ArrayList<FlightProcessingEntry>();
    private static FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static UserSettingsDAO usDAO = (UserSettingsDAO) DAOFactory.get(UserSettingsDAO.class);
    private int lowestTime = Integer.MAX_VALUE;
    private static boolean debug = true;

    protected FlightProcessing(UpdaterDataSet uds, int startTick) {
        this.startTick = startTick;
        this.uds = uds;
        init();
    }

    private void init() {
        if (debug) {
            Logger.getLogger().write("=== FLIGHT INITIALISATION START ===");
        }
        HashSet<Action> flightEntries = uds.getActionEntries(EActionType.FLIGHT);
        for (Iterator<Action> i = flightEntries.iterator(); i.hasNext();) {
            Action a = i.next();

            FleetDetail fd = fdDAO.findById(a.getFleetDetailId());
            if (fd == null) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Fleet " + a.getFleetDetailId() + " has no fleet details entry!");
                continue;
            }

            int timeFinished = fd.getStartTime() + fd.getFlightTime();

            lowestTime = Math.min(lowestTime, timeFinished);

            if (debug) {
                Logger.getLogger().write("Preprocessing Fleet " + a.getFleetId() + " => FlightTime=" + fd.getFlightTime() + " fd (Starttime)=" + fd.getStartTime() + " StartTick=" + startTick);
            }

            if (allFlights.containsKey(timeFinished)) {
                FlightProcessingEntry fpe = new FlightProcessingEntry(fd, a);
                if (debug) {
                    Logger.getLogger().write("Add Fleet " + a.getFleetId() + " with timeFinished " + timeFinished + " to allFlights");
                }
                allFlights.get(timeFinished).add(fpe);
                if ((fd.getStartTime() == startTick) && (fd.getFlightTime() > 1)) {
                    if (fd.getStartSystem() == 0) {
                        Logger.getLogger().write("Fleet " + a.getFleetId() + " was called back ignore interception check");
                    } else {                                      
                        Logger.getLogger().write("Add Fleet " + a.getFleetId() + " to interception check");
                        checkForInterception.add(fpe);
                    }                    
                }
            } else {
                ArrayList<FlightProcessingEntry> fdList = new ArrayList<FlightProcessingEntry>();
                FlightProcessingEntry fpe = new FlightProcessingEntry(fd, a);
                fdList.add(fpe);
                allFlights.put(timeFinished, fdList);
                if (debug) {
                    Logger.getLogger().write("Add Fleet " + a.getFleetId() + " with timeFinished " + timeFinished + " to allFlights");
                }
                if ((fd.getStartTime() == startTick) && (fd.getFlightTime() > 1)) {
                    if (debug) {
                        Logger.getLogger().write("Add Fleet " + a.getFleetId() + " to interception check");
                    }
                    checkForInterception.add(fpe);
                }
            }
        }

        if (debug) {
            Logger.getLogger().write("Marking " + checkForInterception.size() + " fleets for interception check");
        }

        for (FlightProcessingEntry fpe : checkForInterception) {
            if (fpe.getFleetDetail().getStartPlanet() == 0) {
                if (debug) {
                    Logger.getLogger().write("Mark " + fpe.getAction().getFleetId() + " (SYS_OUTSIDE)");
                }
                fpe.setDirection(FlightProcessingEntry.FlightDirection.SYS_OUTSIDE);
            } else {
                if (debug) {
                    Logger.getLogger().write("Mark " + fpe.getAction().getFleetId() + " (PLANET_OUTSIDE)");
                }
                fpe.setDirection(FlightProcessingEntry.FlightDirection.PLANET_OUTSIDE);
            }
        }

        if (debug) {
            Logger.getLogger().write("=== FLIGHT INITIALISATION END ===");
        }
    }

    private void addNewEntry(Action a) {
        FleetDetail fd = fdDAO.findById(a.getFleetDetailId());
        if (fd == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Fleet " + a.getFleetDetailId() + " has no fleet details entry!");
        }

        int timeFinished = fd.getStartTime() + fd.getFlightTime();

        lowestTime = Math.min(lowestTime, timeFinished);

        if (debug) {
            Logger.getLogger().write("FLIGHT DEBUG PRE 1: FlightTime=" + fd.getFlightTime() + " fd (Starttime)=" + fd.getStartTime() + " StartTick=" + startTick);
        }

        if (allFlights.containsKey(timeFinished)) {
            FlightProcessingEntry fpe = new FlightProcessingEntry(fd, a);
            allFlights.get(timeFinished).add(fpe);
            if ((fd.getStartTime() == startTick) && (fd.getFlightTime() > 1)) {
                checkForInterception.add(fpe);
            }
        } else {
            ArrayList<FlightProcessingEntry> fdList = new ArrayList<FlightProcessingEntry>();
            FlightProcessingEntry fpe = new FlightProcessingEntry(fd, a);
            fdList.add(fpe);
            allFlights.put(timeFinished, fdList);
            if ((fd.getStartTime() == startTick) && (fd.getFlightTime() > 1)) {
                if (debug) {
                    Logger.getLogger().write("FLIGHT DEBUG PRE 2: Add to check for interception");
                }
                checkForInterception.add(fpe);
            }
        }

        uds.addActionEntry(a);
    }

    protected void processTick(int tickTime) {
        if (debug) {
            Logger.getLogger().write("=== FLEET TICKUPDATE FOR TICK " + tickTime + " START ===");
        }
        boolean checkOutgoingFleets = (tickTime == (startTick + 1)) && (checkForInterception.size() > 0);

        if (!checkOutgoingFleets) {
            // if (debug) Logger.getLogger().write("Don't check outgoing");

            if (allFlights.size() == 0) {
                if (debug) {
                    Logger.getLogger().write("Abort Tick processing: Cause no flights available");
                }
                if (debug) {
                    Logger.getLogger().write("=== FLEET TICKUPDATE FOR TICK " + tickTime + " END ===");
                }
                return;
            }
            if (lowestTime > tickTime) {
                if (debug) {
                    Logger.getLogger().write("Abort Tick processing: Lowest time " + lowestTime + " is larger than current time " + tickTime);
                }
                if (debug) {
                    Logger.getLogger().write("=== FLEET TICKUPDATE FOR TICK " + tickTime + " END ===");
                }
                return;
            }
        } else {
            // if (debug) Logger.getLogger().write("Check outgoing");
        }

        boolean check = true;

        ArrayList<FlightProcessingEntry> fleetsToUpdate = new ArrayList<FlightProcessingEntry>();
        while (check && (allFlights.size() > 0)) {
            if (debug) {
                Logger.getLogger().write("Check Event at time " + allFlights.firstEntry().getKey() + " versus actual time " + tickTime + ")");
            }
            if (allFlights.firstEntry().getKey() <= tickTime) { // PROCESS THIS ENTRY                
                Map.Entry<Integer, ArrayList<FlightProcessingEntry>> actEntry = allFlights.firstEntry();

                for (FlightProcessingEntry fpe : actEntry.getValue()) {
                    if (debug) {
                        Logger.getLogger().write("Fleet " + fpe.getAction().getFleetId() + " needs to be processed set to INUPDATE");
                    }
                    PlayerFleet pf = pfDAO.findById(fpe.getFleetDetail().getFleetId());
                    // pf.setSystemId(fpe.getFleetDetail().getDestSystem());
                    // pf.setPlanetId(fpe.getFleetDetail().getDestPlanet());
                    pf.setStatus(PlayerFleet.INUPDATE);
                    fleetsToUpdate.add(fpe);
                }

                // fleetsToUpdate = pfDAO.updateAll(fleetsToUpdate);
                // for (FlightProcessingEntry fpe : actEntry.getValue()) {
                // fdDAO.remove(fpe.getFleetDetail());
                // aDAO.remove(fpe.getAction());
                // uds.removeActionEntry(fpe.getAction().getUserId(), EActionType.FLIGHT, fpe.getAction());
                // }

                if (debug) {
                    Logger.getLogger().write("Remove updated Fleets from allFlights list");
                }
                allFlights.remove(actEntry.getKey());
                if (allFlights.size() > 0) {
                    Map.Entry<Integer, ArrayList<FlightProcessingEntry>> nextEntry = allFlights.firstEntry();
                    lowestTime = nextEntry.getKey();
                    if (debug) {
                        Logger.getLogger().write("Set next update for tick " + lowestTime);
                    }
                }
            } else {
                check = false;
            }
        }

        // Get all fleets going out of systems
        for (FlightProcessingEntry fpeOutgoing : checkForInterception) {
            if (debug) {
                Logger.getLogger().write("Check outgoing fleet " + fpeOutgoing.getAction().getFleetId());
            }
            PlayerFleet pf = pfDAO.findById(fpeOutgoing.getFleetDetail().getFleetId());

            pf.setStatus(PlayerFleet.INUPDATE);
            if (!fleetsToUpdate.contains(fpeOutgoing)) {
                if (debug) {
                    Logger.getLogger().write("Add fleet " + fpeOutgoing.getAction().getFleetId() + " to INUPDATE");
                }
                fleetsToUpdate.add(fpeOutgoing);
            }
        }

        for (FlightProcessingEntry fpe : checkForInterception) {
            if (debug) {
                Logger.getLogger().write("Check outgoing fleet " + fpe.getAction().getFleetId());
            }
            if (fpe.getFleetDetail().getStartPlanet() == 0) {
                if (debug) {
                    Logger.getLogger().write("Set direction SYS_OUTSIDE");
                }
                fpe.setDirection(FlightProcessingEntry.FlightDirection.SYS_OUTSIDE);
            } else {
                if (debug) {
                    Logger.getLogger().write("Set direction PLANET_OUTSIDE");
                }
                fpe.setDirection(FlightProcessingEntry.FlightDirection.PLANET_OUTSIDE);
            }
            int passedTime = tickTime - fpe.getFleetDetail().getStartTime();
            if (debug) {
                Logger.getLogger().write("Time since update start " + passedTime);
            }

            if (passedTime > 1) {
                if (debug) {
                    Logger.getLogger().write("Fleet " + fpe.getAction().getFleetId() + " already left system -> Remove from UpdateList");
                }
                fleetsToUpdate.remove(fpe);
            }
        }
        checkForInterception.clear();

        // Check for possible combat events
        if (fleetsToUpdate.size() > 0) {
            prepareCombats(fleetsToUpdate, tickTime);
        }

        if (debug) {
            Logger.getLogger().write("=== FLEET TICKUPDATE FOR TICK " + tickTime + " END ===");
        }
    }

    private void prepareCombats(ArrayList<FlightProcessingEntry> fleetsToUpdate, int tickTime) {
        if (debug) {
            Logger.getLogger().write("Start processing Update List");
        }

        // Sort fleets by System
        HashMap<Integer, ArrayList<FlightProcessingEntry>> sysFleetMap = new HashMap<Integer, ArrayList<FlightProcessingEntry>>();

        if (debug) {
            Logger.getLogger().write(fleetsToUpdate.size() + " registered for updating");
        }
        // Assign directions to fleets
        for (FlightProcessingEntry fpe : fleetsToUpdate) {
            if (debug) {
                Logger.getLogger().write("Processing Fleet " + fpe.getAction().getFleetId());
            }
            if (fpe.getDirection() == null) {
                if (debug) {
                    Logger.getLogger().write("Checking Fleet " + fpe.getAction().getFleetId() + " with no Direction assigned");
                }
                if (fpe.getFleetDetail().getStartSystem() != fpe.getFleetDetail().getDestSystem()) {
                    if (fpe.getFleetDetail().getDestPlanet() != 0) {
                        if (debug) {
                            Logger.getLogger().write("Assign direction OUTSIDE_PLANET");
                        }
                        fpe.setDirection(FlightProcessingEntry.FlightDirection.OUTSIDE_PLANET);
                    } else {
                        if (debug) {
                            Logger.getLogger().write("Assign direction OUTSIDE_SYS");
                        }
                        fpe.setDirection(FlightProcessingEntry.FlightDirection.OUTSIDE_SYS);
                    }
                } else {
                    if (fpe.getFleetDetail().getStartPlanet() == 0) {
                        if (debug) {
                            Logger.getLogger().write("Assign direction SYS_PLANET");
                        }
                        if (fpe.getFleetDetail().getDestPlanet() == 0) {
                            fpe.setDirection(FlightProcessingEntry.FlightDirection.OUTSIDE_SYS);
                        } else {
                            fpe.setDirection(FlightProcessingEntry.FlightDirection.SYS_PLANET);
                        }
                    } else if (fpe.getFleetDetail().getDestPlanet() == 0) {
                        if (debug) {
                            Logger.getLogger().write("Assign direction PLANET_SYSTEM");
                        }
                        fpe.setDirection(FlightProcessingEntry.FlightDirection.PLANET_SYSTEM);
                    } else {
                        if (debug) {
                            Logger.getLogger().write("Assign direction PLANET_PLANET");
                        }
                        fpe.setDirection(FlightProcessingEntry.FlightDirection.PLANET_PLANET);
                    }
                }
            }

            // Sort fleets by System
            int sysId = 0;
            if ((fpe.getDirection() == FlightProcessingEntry.FlightDirection.PLANET_OUTSIDE) || (fpe.getDirection() == FlightProcessingEntry.FlightDirection.SYS_OUTSIDE)) {
                if (debug) {
                    Logger.getLogger().write("Set System Id for Fleet to " + fpe.getFleetDetail().getStartSystem());
                }
                sysId = fpe.getFleetDetail().getStartSystem();
            } else {
                if (debug) {
                    Logger.getLogger().write("Set System Id for Fleet to " + fpe.getFleetDetail().getDestSystem());
                }
                sysId = fpe.getFleetDetail().getDestSystem();
            }

            // Check if target is a trial, if yes turn fleet around
            if (fpe.getDirection().equals(FlightProcessingEntry.FlightDirection.OUTSIDE_SYS) || fpe.getDirection().equals(FlightProcessingEntry.FlightDirection.OUTSIDE_PLANET)) {
                if (debug) {
                    Logger.getLogger().write("Fleet is entering a system");
                }
                at.viswars.model.System sys = sDAO.findById(fpe.getFleetDetail().getDestSystem());
                if (debug) {
                    Logger.getLogger().write("[TC] SYSVIS: " + sys.getVisibility() + " FleetUserID: " + fpe.getAction().getUserId());
                }
                if ((sys.getVisibility() != 0) && !(sys.getVisibility().equals(fpe.getAction().getUserId()))
                        || isSystemFrozen(sys.getId())) {
                    if (debug) {
                        if ((sys.getVisibility() != 0) && (sys.getVisibility() != fpe.getAction().getUserId())) {
                            Logger.getLogger().write("Entered system is in Trial protection");
                        }
                        if (isSystemFrozen(sys.getId())) {
                            Logger.getLogger().write("Entered system is frozen");
                        }
                    }

                    // Return fleet to start Loc
                    returnFleet(fpe, tickTime);

                    continue;
                }
            }

            if (sysFleetMap.containsKey(sysId)) {
                if (debug) {
                    Logger.getLogger().write("Add Fleet " + fpe.getAction().getFleetId() + " to system map entry " + sysId);
                }
                ArrayList<FlightProcessingEntry> fpeList = sysFleetMap.get(sysId);
                fpeList.add(fpe);
            } else {
                if (debug) {
                    Logger.getLogger().write("Add Fleet " + fpe.getAction().getFleetId() + " to newly created system map entry " + sysId);
                }
                ArrayList<FlightProcessingEntry> fpeList = new ArrayList<FlightProcessingEntry>();
                fpeList.add(fpe);
                sysFleetMap.put(sysId, fpeList);
            }
        }

        CombatReportGenerator crg = new CombatReportGenerator();
        HashSet<Integer> destroyed = new HashSet<Integer>();
        HashSet<Integer> retreated = new HashSet<Integer>();

        // Process leaving fleets
        for (int sysIdTmp : sysFleetMap.keySet()) {
            if (debug) {
                Logger.getLogger().write("Processing System " + sysIdTmp);
            }
            PlayerFleet pf = new PlayerFleet();            
            // pf.setStatus(0);
            pf.setSystemId(sysIdTmp);
            pf.setPlanetId(0);
            ArrayList<PlayerFleet> stationarySystem = pfDAO.find(pf);
            boolean hasStationarySystem = !stationarySystem.isEmpty();

            ArrayList<FlightProcessingEntry> fleetsToProcess = sysFleetMap.get(sysIdTmp);

            // Check stationary fleets with outgoing
            HashSet<Integer> outAllowed = new HashSet<Integer>();
            ArrayList<FlightProcessingEntry> cleanUp = new ArrayList<FlightProcessingEntry>();

            for (FlightProcessingEntry actSysFleet : fleetsToProcess) {
                if (debug) {
                    Logger.getLogger().write("Processing Fleet " + actSysFleet.getFleetDetail().getFleetId() + " with direction: " + actSysFleet.getDirection());
                }

                if ((actSysFleet.getDirection() != FlightProcessingEntry.FlightDirection.SYS_OUTSIDE) && (actSysFleet.getDirection() != FlightProcessingEntry.FlightDirection.PLANET_OUTSIDE)) {
                    if (debug) {
                        Logger.getLogger().write("Skip fleet as the fleet is not outgoing");
                    }
                    continue;
                }

                for (PlayerFleet actStationary : stationarySystem) {
                    int uId1 = actSysFleet.getAction().getUserId();
                    int uId2 = actStationary.getUserId();
                    if (uId1 == uId2) {
                        cleanUp.add(actSysFleet);
                        if (debug) {
                            Logger.getLogger().write("Skip fleet as fleet has same userId as stationary fleets");
                        }
                        continue;
                    }

                    if (debug) {
                        Logger.getLogger().write("Check DiplomacyResult for " + uId1 + " vs " + uId2);
                    }

                    ArrayList<Integer> users = new ArrayList<Integer>();
                    users.add(uId1);
                    users.add(uId2);
                    CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(users, new RelativeCoordinate(sysIdTmp,0));
                    // DiplomacyResult dr = DiplomacyUtilities.getDiplomacyResult(uId1, uId2);
                    // if (!dr.isAttacks()) {
                    if (!cgr.hasConflictingParties()) {
                        cleanUp.add(actSysFleet);
                        if (debug) {
                            Logger.getLogger().write("Fleet " + actSysFleet.getAction().getFleetId() + " may leave system unharmed");
                        }
                        outAllowed.add(uId1);
                    }
                }

                if (stationarySystem.isEmpty()) {
                    if (debug) {
                        Logger.getLogger().write("No stationary Fleets => Fleet " + actSysFleet.getAction().getFleetId() + " may leave system unharmed");
                    }
                    outAllowed.add(actSysFleet.getAction().getUserId());
                }

                if (outAllowed.contains(actSysFleet.getAction().getUserId())) {
                    if (debug) {
                        Logger.getLogger().write("Fleet " + actSysFleet.getFleetDetail().getFleetId() + " may fly outside! -- Add to clean up Array");
                    }

                    cleanUp.add(actSysFleet);
                    continue;
                }
            }

            if (debug) {
                Logger.getLogger().write("Remove all cleanup fleets from processing list (" + cleanUp.size() + ")");
            }

            fleetsToProcess.removeAll(cleanUp);
            HashSet<Integer> allUsers = new HashSet<Integer>();

            // Logger.getLogger().write("============ PROCESSING SYSTEM " + sysIdTmp + " ================");

            for (FlightProcessingEntry actSysFleet : fleetsToProcess) {
                PlayerFleet pfTmp = pfDAO.findById(actSysFleet.getFleetDetail().getFleetId());
                // Logger.getLogger().write("Processing fleet " + actSysFleet.getFleetDetail().getFleetId() + " at location " + pfTmp.getSystemId() + ":" + pfTmp.getPlanetId() + " [State: "+pfTmp.getStatus()+"]");
                // Logger.getLogger().write("This fleet has target " + actSysFleet.getFleetDetail().getDestSystem());

                allUsers.add(actSysFleet.getAction().getUserId());
            }

            for (PlayerFleet statFleet : stationarySystem) {
                if (debug) {
                    Logger.getLogger().write("Processing fleet " + statFleet.getId() + " at location " + statFleet.getSystemId() + ":" + statFleet.getPlanetId() + " [State: " + statFleet.getStatus() + "]");
                }

                allUsers.add(statFleet.getUserId());
            }

            // Check for possible system defenses
            ArrayList<PlanetDefense> pdList = pdDAO.findBySystemId(sysIdTmp);
            for (PlanetDefense pd : pdList) {
                if (!allUsers.contains(pd.getUserId())) {
                    allUsers.add(pd.getUserId());
                }
            }

            // Debug all users
            if (debug) {
                for (Integer i : allUsers) {
                    Logger.getLogger().write("CG check on user " + i);
                }
            }

            ArrayList<Integer> allUsers_AL = new ArrayList<Integer>();
            allUsers_AL.addAll(allUsers);
            // HashMap<Integer, ArrayList<Integer>> groups = CombatUtilities.getCombatGroupedPlayers(allUsers_AL);
            RelativeCoordinate rcTmp = new RelativeCoordinate(sysIdTmp, 0);
            CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(allUsers_AL, rcTmp);

            if (!cgr.hasConflictingParties()) {
                // no combat
                if (debug) {
                    Logger.getLogger().write("NO COMBAT IN SYSTEM: " + sysIdTmp);

                    for (CombatGroupEntry cge : cgr.getCombatGroups()) {
                        for (Integer id : cge.getPlayers()) {
                            Logger.getLogger().write("CombatGroup " + cge.getGroupId() + " Player: " + id);
                        }
                    }
                }
            } else {
                // a omg fucking combat
                Logger.getLogger().write(LogLevel.INFO, "COMBAT IN SYSTEM: " + sysIdTmp);
                ArrayList<PlayerFleet> allFleets = new ArrayList<PlayerFleet>();
                for (FlightProcessingEntry fpEntry : fleetsToProcess) {
                    PlayerFleet pfTmp = pfDAO.findById(fpEntry.getFleetDetail().getFleetId());
                    allFleets.add(pfDAO.findById(fpEntry.getFleetDetail().getFleetId()));
                }

                allFleets.addAll(stationarySystem);

                RelativeCoordinate rc = new RelativeCoordinate(sysIdTmp, 0);
                AbsoluteCoordinate ac = rc.toAbsoluteCoordinate();

                CombatHandler_NEW ch = new CombatHandler_NEW(
                        CombatReportGenerator.CombatType.SYSTEM_COMBAT, allFleets,
                        rc, ac, crg);
                ch.executeBattle();

                if (debug) {
                    Logger.getLogger().write("Add destroyed fleets to destroyed list");
                }
                destroyed.addAll(ch.processCombatResults());
                retreated.addAll(ch.getRetreatedOrResignedFleets());
            }
        }

        // Probably some stuff got scrapped due to battle
        for (Iterator<Integer> sfmIt = sysFleetMap.keySet().iterator(); sfmIt.hasNext();) {
            Integer sysId = sfmIt.next();

            ArrayList<FlightProcessingEntry> fpeList = sysFleetMap.get(sysId);

            // Remove destroyed fleets from processing
            for (Iterator<FlightProcessingEntry> fpeIt = fpeList.iterator(); fpeIt.hasNext();) {
                FlightProcessingEntry fpe = fpeIt.next();

                if (destroyed.contains(fpe.getAction().getFleetId())) {
                    if (debug) {
                        Logger.getLogger().write("Remove fleet " + fpe.getAction().getFleetId() + " from processing Fleets (was destroyed in system combat [" + sysId + "])");
                    }
                    fpeIt.remove();
                }
            }

            // Send retreated Fleets to their retreat location
            HashMap<Integer, ArrayList<FlightProcessingEntry>> processedFormations = new HashMap<Integer, ArrayList<FlightProcessingEntry>>();

            for (Iterator<FlightProcessingEntry> fpeIt = fpeList.iterator(); fpeIt.hasNext();) {
                FlightProcessingEntry fpe = fpeIt.next();

                if (retreated.contains(fpe.getAction().getFleetId())) {
                    PlayerFleet pf = pfDAO.findById(fpe.getAction().getFleetId());

                    if (debug) {
                        Logger.getLogger().write("Remove fleet " + fpe.getAction().getFleetId() + " from processing Fleets (retreated in system combat [" + sysId + "])");
                    }

                    int formationId = pf.getFleetFormationId();
                    if (formationId != 0) {
                        Logger.getLogger().write("Processing Entry with FFID " + formationId);
                        if (processedFormations.containsKey(formationId)) {
                            Logger.getLogger().write("Already contained => ADD");
                            ArrayList<FlightProcessingEntry> fpeListTmp = processedFormations.get(formationId);
                            fpeListTmp.add(fpe);
                        } else {
                            Logger.getLogger().write("Create new Entry!");
                            ArrayList<FlightProcessingEntry> fpeListTmp = new ArrayList<FlightProcessingEntry>();
                            fpeListTmp.add(fpe);
                            processedFormations.put(formationId, fpeListTmp);
                        }
                    } else {
                        retreatFleet(fpe, tickTime);
                    }

                    fpeIt.remove();
                }
            }

            for (Map.Entry<Integer, ArrayList<FlightProcessingEntry>> me : processedFormations.entrySet()) {
                retreatFleetFormation(me.getKey(), me.getValue(), tickTime);
            }

            if (fpeList.isEmpty()) {
                // Remove sysEntry 
                sfmIt.remove();
            }
        }

        // Remove processing entries which have PLANET_OUTSIDE or SYSTEM_OUTSIDE and do not reach target this tick
        // As they have been processed now and already triggered a fight
        for (Iterator<Map.Entry<Integer,ArrayList<FlightProcessingEntry>>> sysListIt = sysFleetMap.entrySet().iterator();sysListIt.hasNext();) {
            Map.Entry<Integer,ArrayList<FlightProcessingEntry>> actSys = sysListIt.next();
            if (debug) Logger.getLogger().write("Looping system " + actSys.getKey());

            ArrayList<FlightProcessingEntry> actList = actSys.getValue();
            for (Iterator<FlightProcessingEntry> fpeIt = actList.iterator();fpeIt.hasNext();) {
                FlightProcessingEntry fpe = fpeIt.next();
                if (debug) Logger.getLogger().write("Looping fleet " + fpe.getFleetDetail().getFleetId() + " ( "+(fpe.getFleetDetail().getStartTime() + fpe.getFleetDetail().getFlightTime())+" -- "+tickTime+")");
                if (debug) Logger.getLogger().write("Direction is " + fpe.getDirection());

                if ((fpe.getDirection() == FlightProcessingEntry.FlightDirection.PLANET_OUTSIDE) ||
                    (fpe.getDirection() == FlightProcessingEntry.FlightDirection.SYS_OUTSIDE)) {
                    if ((fpe.getFleetDetail().getStartTime() + fpe.getFleetDetail().getFlightTime()) > tickTime) {
                        if (debug) Logger.getLogger().write("Removing fleet " + fpe.getFleetDetail().getFleetId());
                        fpeIt.remove();
                    }
                }
            }

            if (actList.isEmpty()) {
                sysListIt.remove();
            }
        }

        for (int sysIdTmp : sysFleetMap.keySet()) {
            ArrayList<FlightProcessingEntry> fleetsToProcess = sysFleetMap.get(sysIdTmp);
        }

        // If they aimed for system .. finish them
        // If they make a g� to a planet .. let them g��
        // If they went to outer void .. say goodbye
        for (int sysIdTmp : sysFleetMap.keySet()) {
            ArrayList<FlightProcessingEntry> fleetsToProcess = sysFleetMap.get(sysIdTmp);
            HashMap<Integer, ArrayList<FlightProcessingEntry>> planetMap =
                    new HashMap<Integer, ArrayList<FlightProcessingEntry>>();

            if (debug) {
                Logger.getLogger().write("After checking outgoing fleets - Process rest (" + fleetsToProcess.size() + ")");
            }
            // for (FlightProcessingEntry fpe : fleetsToProcess) {
            for (Iterator<FlightProcessingEntry> fpeIt = fleetsToProcess.iterator(); fpeIt.hasNext();) {
                FlightProcessingEntry fpe = fpeIt.next();

                if (debug) {
                    Logger.getLogger().write("Processing Fleet " + fpe.getAction().getFleetId() + " with direction " + fpe.getDirection());
                }
                if ((fpe.getDirection() == FlightProcessingEntry.FlightDirection.PLANET_OUTSIDE) || (fpe.getDirection() == FlightProcessingEntry.FlightDirection.SYS_OUTSIDE)) {
                    if (debug) {
                        Logger.getLogger().write("Skip outgoing fleet");
                    }
                    continue;
                } else if ((fpe.getDirection() == FlightProcessingEntry.FlightDirection.PLANET_SYSTEM) || (fpe.getDirection() == FlightProcessingEntry.FlightDirection.OUTSIDE_SYS)) {
                    if (debug) {
                        Logger.getLogger().write("Fleet " + fpe.getAction().getFleetId() + " is entering system >> Finish movement");
                    }
                    PlayerFleet pf = pfDAO.findById(fpe.getFleetDetail().getFleetId());
                    pf.setLocation(ELocationType.SYSTEM,fpe.getFleetDetail().getDestSystem());                    
                    
                    // pf.setSystemId(fpe.getFleetDetail().getDestSystem());
                    // pf.setPlanetId(fpe.getFleetDetail().getDestPlanet());
                    // pf.setStatus(PlayerFleet.STATIONARY);
                    // updFleets.add(pf);
                    Logger.getLogger().write("[1] FP - TLBUG DEBUG: Set fleet " + pf.getName() + " to position " + fpe.getFleetDetail().getDestSystem() + ":" + fpe.getFleetDetail().getDestPlanet());
                    pfDAO.update(pf);

                    ViewTableUtilities.addOrRefreshSystem(pf.getUserId(), pf.getSystemId(), tickTime);

                    DebugBuffer.addLine(DebugLevel.DEBUG, "DELETE MOVING INFORMATION (SYSTEM LEVEL) - " + fpe.getFleetDetail());
                    if (fpe.getFleetDetail() != null) {
                        DebugBuffer.addLine(DebugLevel.DEBUG, "FDE ID - " + fpe.getFleetDetail().getId());
                    }

                    // delFleetDetails.add(fpe.getFleetDetail());
                    // delActions.add(fpe.getAction());
                    Logger.getLogger().write("[1] FP - TLBUG DEBUG: Remove FleetDetail " + fpe.getFleetDetail().getId() + " and Action " + fpe.getAction().getRefTableId());
                    fdDAO.remove(fpe.getFleetDetail());
                    aDAO.remove(fpe.getAction());

                    uds.removeActionEntry(fpe.getAction().getUserId(), EActionType.FLIGHT, fpe.getAction());
                    fpeIt.remove();

                    // Check if sender was in trial, if he entered enemy system remove trial protection
                    User u = MainService.findUserByUserId(pf.getUserId());
                    if (u.getTrial()) {
                        int systemId = fpe.getFleetDetail().getDestSystem();
                        ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
                        for (Planet p : pList) {
                            PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
                            if (pp != null) {
                                if (!DiplomacyUtilities.hasAllianceRelation(u.getUserId(), pp.getUserId()) && !u.getUserId().equals(pp.getUserId())) {
                                    Logger.getLogger().write(LogLevel.INFO,"Remove Trial due to entering enemy system");
                                    LoginService.unlockTrialUser(u);
                                }
                            }
                        }
                    }

                    if (debug) {
                        Logger.getLogger().write("Deleted Movement information");
                    }
                } else {
                    if (debug) {
                        Logger.getLogger().write("Fleet is advancing to Planet");
                    }
                    if (planetMap.containsKey(fpe.getFleetDetail().getDestPlanet())) {
                        if (debug) {
                            Logger.getLogger().write("Adding fleet " + fpe.getAction().getFleetId() + " to list for planet " + fpe.getFleetDetail().getDestPlanet());
                        }
                        planetMap.get(fpe.getFleetDetail().getDestPlanet()).add(fpe);
                    } else {
                        if (debug) {
                            Logger.getLogger().write("Adding fleet " + fpe.getAction().getFleetId() + " to list for planet " + fpe.getFleetDetail().getDestPlanet());
                        }
                        ArrayList<FlightProcessingEntry> fpeList = new ArrayList<FlightProcessingEntry>();
                        fpeList.add(fpe);
                        planetMap.put(fpe.getFleetDetail().getDestPlanet(), fpeList);
                    }
                }
            }

            // If fleets was destroyed already in system delete Planet Flights
            for (Iterator<Integer> pmIt = planetMap.keySet().iterator(); pmIt.hasNext();) {
                ArrayList<FlightProcessingEntry> fpeList = planetMap.get(pmIt.next());

                for (Iterator<FlightProcessingEntry> fpeIt = fpeList.iterator(); fpeIt.hasNext();) {
                    FlightProcessingEntry fpe = fpeIt.next();
                    for (Iterator<Integer> dfIt = destroyed.iterator(); dfIt.hasNext();) {
                        if (fpe.getFleetDetail().getFleetId().equals(dfIt.next())) {
                            if (debug) {
                                Logger.getLogger().write("Remove destroyed Fleet " + fpe.getAction().getFleetId() + " from planetList");
                            }
                            fpeIt.remove();
                        }
                    }
                }

                if (fpeList.isEmpty()) {
                    pmIt.remove();
                }
            }

            // Process generated Planet flights
            for (int pIdTmp : planetMap.keySet()) {
                if (debug) {
                    Logger.getLogger().write("Processing Planet " + pIdTmp);
                }
                PlayerFleet pf = new PlayerFleet();
                pf.setStatus(0);
                pf.setPlanetId(pIdTmp);
                ArrayList<PlayerFleet> stationaryPlanet = pfDAO.find(pf);
                boolean hasStationaryPlanet = !stationaryPlanet.isEmpty();

                ArrayList<FlightProcessingEntry> fleetsToProcessInner = planetMap.get(pIdTmp);

                // Check stationary fleets with outgoing
                HashSet<Integer> outAllowed = new HashSet<Integer>();
                ArrayList<FlightProcessingEntry> cleanUp = new ArrayList<FlightProcessingEntry>();

                if (debug) {
                    Logger.getLogger().write("Process " + fleetsToProcessInner.size() + " planet flights [PlanetId: " + pIdTmp + "]");
                }

                if (debug) {
                    Logger.getLogger().write("=== Check for outgoing fleets ===");
                }
                for (FlightProcessingEntry actPlanetFleet : fleetsToProcessInner) {
                    if ((actPlanetFleet.getDirection() != FlightProcessingEntry.FlightDirection.SYS_OUTSIDE) && (actPlanetFleet.getDirection() != FlightProcessingEntry.FlightDirection.PLANET_OUTSIDE)) {
                        if (debug) {
                            Logger.getLogger().write("Skip fleet " + actPlanetFleet.getAction().getFleetId() + " as it is not outgoing");
                        }
                        continue;
                    }
                    if (actPlanetFleet.getFleetDetail().getDestPlanet() == 0) {
                        if (isSystemFrozen(actPlanetFleet.getFleetDetail().getDestSystem())) {
                            Logger.getLogger().write("Skip fleet cause system is frozen");

                            continue;
                        } else if (isPlanetFrozen(actPlanetFleet.getFleetDetail().getDestPlanet())) {
                            Logger.getLogger().write("Skip fleet cause planet is frozen");
                            continue;
                        }
                    }
                    if (outAllowed.contains(actPlanetFleet.getAction().getUserId())) {
                        if (debug) {
                            Logger.getLogger().write("Skip Fleet " + actPlanetFleet.getAction().getFleetId() + ": May leave!");
                        }
                        cleanUp.add(actPlanetFleet);
                        continue;
                    }

                    for (PlayerFleet actStationary : stationaryPlanet) {
                        int uId1 = actPlanetFleet.getAction().getUserId();
                        int uId2 = actStationary.getUserId();

                        boolean mayLeave = false;

                        if (uId1 == uId2) {
                            cleanUp.add(actPlanetFleet);
                            if (debug) {
                                Logger.getLogger().write("Skip Fleet " + actPlanetFleet.getAction().getFleetId() + ": Same userId as stationary!");
                            }
                            continue;
                        }

                        if (debug) {
                            Logger.getLogger().write("Check DiplomacyResult for " + uId1 + " vs " + uId2);
                        }
                        DiplomacyResult dr = DiplomacyUtilities.getDiplomacyResult(uId1, uId2);
                        if (!dr.isAttacks()) {
                            cleanUp.add(actPlanetFleet);
                            if (debug) {
                                Logger.getLogger().write("Add Fleet " + actPlanetFleet.getAction().getFleetId() + " to 'May leave!'");
                            }
                            outAllowed.add(uId1);
                        }
                    }
                }
                if (debug) {
                    Logger.getLogger().write("========================");
                }

                if (debug) {
                    Logger.getLogger().write("Remove all cleanUp fleets (" + cleanUp.size() + ")");
                }
                fleetsToProcess.removeAll(cleanUp);
                HashSet<Integer> allUsers = new HashSet<Integer>();

                for (FlightProcessingEntry actPlanetFleet : fleetsToProcessInner) {
                    allUsers.add(actPlanetFleet.getAction().getUserId());
                }
                for (PlayerFleet statFleet : stationaryPlanet) {
                    allUsers.add(statFleet.getUserId());
                }

                // Check for possible planetary defenses
                ArrayList<PlanetDefense> pdList = pdDAO.findByPlanetId(pIdTmp);
                for (PlanetDefense pd : pdList) {
                    if (!allUsers.contains(pd.getUserId())) {
                        allUsers.add(pd.getUserId());
                    }
                }

                ArrayList<Integer> allUsers_AL = new ArrayList<Integer>();
                allUsers_AL.addAll(allUsers);
                // HashMap<Integer, ArrayList<Integer>> groups = CombatUtilities.getCombatGroupedPlayers(allUsers_AL);
                RelativeCoordinate rcTmp = new RelativeCoordinate(0, pIdTmp);
                CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(allUsers_AL, rcTmp);

                if (!cgr.hasConflictingParties()) {
                    // no combat
                    if (debug) {
                        Logger.getLogger().write("NO COMBAT ON PLANET: " + pIdTmp);
                    }
                } else {
                    // a omg fucking combat
                    Logger.getLogger().write(LogLevel.INFO, "COMBAT ON PLANET: " + pIdTmp);

                    ArrayList<PlayerFleet> allFleets = new ArrayList<PlayerFleet>();
                    for (FlightProcessingEntry fpEntry : fleetsToProcessInner) {
                        PlayerFleet pfTmp = pfDAO.findById(fpEntry.getFleetDetail().getFleetId());
                        allFleets.add(pfDAO.findById(fpEntry.getFleetDetail().getFleetId()));
                    }

                    allFleets.addAll(stationaryPlanet);

                    RelativeCoordinate rc = new RelativeCoordinate(sysIdTmp, pIdTmp);
                    AbsoluteCoordinate ac = rc.toAbsoluteCoordinate();

                    CombatHandler_NEW ch = new CombatHandler_NEW(
                            CombatReportGenerator.CombatType.ORBITAL_COMBAT, allFleets,
                            rc, ac, crg);
                    ch.executeBattle();

                    destroyed.addAll(ch.processCombatResults());
                    retreated.addAll(ch.getRetreatedOrResignedFleets());
                }

                for (Integer destroyedFleet : destroyed) {
                    for (Iterator<FlightProcessingEntry> fpeIt = fleetsToProcess.iterator(); fpeIt.hasNext();) {
                        FlightProcessingEntry fpe = fpeIt.next();

                        if (fpe.getFleetDetail().getFleetId() == destroyedFleet) {
                            if (debug) {
                                Logger.getLogger().write("Remove fleet " + destroyedFleet + " from processing Fleets (was destroyed in combat)");
                            }
                            fpeIt.remove();
                            break;
                        }
                    }
                }

                // Send retreated Fleets to their retreat location
                HashMap<Integer, ArrayList<FlightProcessingEntry>> processedFormations = new HashMap<Integer, ArrayList<FlightProcessingEntry>>();

                for (Iterator<FlightProcessingEntry> fpeIt = fleetsToProcess.iterator(); fpeIt.hasNext();) {
                    FlightProcessingEntry fpe = fpeIt.next();

                    if (retreated.contains(fpe.getAction().getFleetId())) {
                        PlayerFleet pfTmp = pfDAO.findById(fpe.getAction().getFleetId());

                        if (debug) {
                            Logger.getLogger().write("Remove fleet " + fpe.getAction().getFleetId() + " from processing Fleets (retreated in planet combat [" + pIdTmp + "])");
                        }

                        int formationId = pfTmp.getFleetFormationId();
                        if (formationId != 0) {
                            Logger.getLogger().write("Processing Entry with FFID " + formationId);
                            if (processedFormations.containsKey(formationId)) {
                                Logger.getLogger().write("Already contained => ADD");
                                ArrayList<FlightProcessingEntry> fpeListTmp = processedFormations.get(formationId);
                                fpeListTmp.add(fpe);
                            } else {
                                Logger.getLogger().write("Create new Entry!");
                                ArrayList<FlightProcessingEntry> fpeListTmp = new ArrayList<FlightProcessingEntry>();
                                fpeListTmp.add(fpe);
                                processedFormations.put(formationId, fpeListTmp);
                            }
                        } else {
                            retreatFleet(fpe, tickTime);
                        }

                        fpeIt.remove();
                    }
                }

                for (Map.Entry<Integer, ArrayList<FlightProcessingEntry>> me : processedFormations.entrySet()) {
                    retreatFleetFormation(me.getKey(), me.getValue(), tickTime);
                }
            }

            // Finish all movements
            for (Iterator<FlightProcessingEntry> fpeIt = fleetsToProcess.iterator(); fpeIt.hasNext();) {
                FlightProcessingEntry fpe = fpeIt.next();
                if (debug) {
                    Logger.getLogger().write("Finish fleet movement for fleet " + fpe.getAction().getFleetId());
                }

                PlayerFleet pfPlanet = pfDAO.findById(fpe.getFleetDetail().getFleetId());

                FleetDetail fd = fpe.getFleetDetail();
                if (fd == null) {
                    DebugBuffer.error("Fleet " + pfPlanet.getId() + " had no fleetdetails entry in FlightProcessing");
                    fpeIt.remove();
                    continue;
                }

                int reachedDestAt = fd.getStartTime() + fd.getFlightTime();
                if (tickTime < reachedDestAt) {
                    DebugBuffer.error("Fleet " + pfPlanet.getId() + " tried to reach target too early! (User: " + pfPlanet.getUserId() + ") ["+tickTime+"<"+reachedDestAt+"]");
                    DebugBuffer.warning("Starting Debug Print for planetFleet");
                    debugPrintBrokenFleet(pfPlanet, fpe, tickTime);
                    continue;
                }

                if (pfPlanet == null) {
                    DebugBuffer.error("Fleet " + fpe.getFleetDetail().getFleetId() + " which doesn't exist anymore (due to a combat) was still in processing list");
                    continue;
                }

                pfPlanet.setLocation(ELocationType.PLANET,fd.getDestPlanet());
                // pfPlanet.setSystemId(fd.getDestSystem());
                // pfPlanet.setPlanetId(fd.getDestPlanet());
                // pfPlanet.setStatus(PlayerFleet.STATIONARY);
                ViewTableUtilities.addOrRefreshSystem(pfPlanet.getUserId(), pfPlanet.getSystemId(), tickTime);
                // updFleets.add(pfPlanet);
                Logger.getLogger().write("[2] FP - TLBUG DEBUG: Set fleet " + pfPlanet.getName() + " to position " + fd.getDestSystem() + ":" + fd.getDestPlanet());
                pfDAO.update(pfPlanet);

                DebugBuffer.addLine(DebugLevel.DEBUG, "DELETE MOVING INFORMATION (PLANET LEVEL) - " + fpe.getFleetDetail());
                if (fpe.getFleetDetail() != null) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "FDE ID - " + fpe.getFleetDetail().getId());
                }

                //delFleetDetails.add(fpe.getFleetDetail());
                //delActions.add(fpe.getAction());
                if (debug) {
                    Logger.getLogger().write("Process fpe " + fpe + " with direction " + fpe.getDirection());
                }
                if (debug) {
                    Logger.getLogger().write("Delete FleetDetail with ID " + fpe.getFleetDetail().getId());
                }

                Logger.getLogger().write("[2] FP - TLBUG DEBUG: Remove FleetDetail " + fpe.getFleetDetail().getId() + " and Action " + fpe.getAction().getRefTableId());
                fdDAO.remove(fpe.getFleetDetail());
                // if (debug) Logger.getLogger().write("Delete Action: Type="+fpe.getAction().getType()+" UserId="+fpe.getAction().getUserId()+" RefEntId="+fpe.getAction().getRefEntityId()+" RefTabId="+fpe.getAction().getRefTableId());
                aDAO.remove(fpe.getAction());
                uds.removeActionEntry(fpe.getAction().getUserId(), EActionType.FLIGHT, fpe.getAction());

                // Check if sender was in trial, if he entered enemy system remove trial protection
                User u = MainService.findUserByUserId(pfPlanet.getUserId());
                if (u.getTrial()) {
                    int systemId = fpe.getFleetDetail().getDestSystem();
                    ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
                    for (Planet p : pList) {
                        PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
                        if (pp != null) {
                            if (!DiplomacyUtilities.hasAllianceRelation(u.getUserId(), pp.getUserId()) && !u.getUserId().equals(pp.getUserId())) {
                                Logger.getLogger().write(LogLevel.INFO,"Remove Trial due to entering enemy system");
                                LoginService.unlockTrialUser(u);
                            }
                        }
                    }
                }
            }
        }

        if (debug) {
            Logger.getLogger().write("## Call WriteReportsToDatabase");
        }
        crg.writeReportsToDatabase();

        for (FlightProcessingEntry fpe : fleetsToUpdate) {
            if ((fpe.getDirection() == FlightProcessingEntry.FlightDirection.SYS_OUTSIDE)
                    || (fpe.getDirection() == FlightProcessingEntry.FlightDirection.PLANET_OUTSIDE)) {
                if (debug) {
                    Logger.getLogger().write("Set direction of Fleet " + fpe.getAction().getFleetId() + " to NULL");
                }
                fpe.setDirection(null);
            }
        }
    }

    // Retreat fleet formation
    private void retreatFleetFormation(int formationId, ArrayList<FlightProcessingEntry> fpeList, int tickTime) {
        for (FlightProcessingEntry fpe : fpeList) {
            PlayerFleet pf = pfDAO.findById(fpe.getFleetDetail().getFleetId());
            ViewTableUtilities.addOrRefreshSystem(pf.getUserId(), fpe.getFleetDetail().getDestSystem(), tickTime);
            uds.removeActionEntry(fpe.getAction().getUserId(), EActionType.FLIGHT, fpe.getAction());
        }

        FleetMovement.retreatFleetFormation(formationId, fpeList);

        if (debug) {
            Logger.getLogger().write("Retreat FleetFormation");
        }
    }

    // Retreat fleet
    private void retreatFleet(FlightProcessingEntry fpe, int tickTime) {
        PlayerFleet pf = pfDAO.findById(fpe.getFleetDetail().getFleetId());
        ViewTableUtilities.addOrRefreshSystem(pf.getUserId(), fpe.getFleetDetail().getDestSystem(), tickTime);

        FleetMovement.retreatFleet(fpe, fpe.getAction().getUserId());
        uds.removeActionEntry(fpe.getAction().getUserId(), EActionType.FLIGHT, fpe.getAction());

        if (debug) {
            Logger.getLogger().write("Retreat Fleet");
        }
    }

    //Return fleet
    private void returnFleet(FlightProcessingEntry fpe, int tickTime) {
        PlayerFleet pf = pfDAO.findById(fpe.getFleetDetail().getFleetId());
        ViewTableUtilities.addOrRefreshSystem(pf.getUserId(), fpe.getFleetDetail().getDestSystem(), tickTime);

        BaseResult br = FleetMovement.callBackFleet(fpe.getFleetDetail().getFleetId(), fpe.getAction().getUserId());
        if (br.isError()) {
            // Happens if fleet already got called back
            DebugBuffer.warning("Error occured while automatic fleet turn around " + br.getMessage() + " FleetId: " + pf.getId() + " UserId: " + pf.getUserId() + " FleetName: " + pf.getName());
            
            // We modify fleet entry now in a way so the fleet can retreat to home system
            FleetDetail fd = fpe.getFleetDetail();
            int fleetOwner = fpe.getAction().getUserId();
            
            PlayerPlanet pp = ppDAO.findHomePlanetByUserId(fleetOwner);
            RelativeCoordinate rc = new RelativeCoordinate(0,pp.getPlanetId());
            
            fd.setStartPlanet(rc.getPlanetId());
            fd.setStartSystem(rc.getSystemId());
            fd.setStartX(rc.toAbsoluteCoordinate().getX());
            fd.setStartY(rc.toAbsoluteCoordinate().getY());
            
            fdDAO.update(fd);
            br = FleetMovement.callBackFleet(fpe.getFleetDetail().getFleetId(), fpe.getAction().getUserId());
            
            if (br.isError()) {
                DebugBuffer.error("Retreat to homeplanet failed as well " + br.getMessage());
            } else {
                DebugBuffer.warning("Retreat to homeplanet was successful");
            }
        }

        uds.removeActionEntry(fpe.getAction().getUserId(), EActionType.FLIGHT, fpe.getAction());

        if (debug) {
            Logger.getLogger().write("Turn around fleet " + pf.getId());
        }

        Action aNew = new Action();
        aNew.setType(EActionType.FLIGHT);
        aNew.setFleetId(pf.getId());
        aNew.setUserId(pf.getUserId());
        
        ArrayList<Action> aList = aDAO.find(aNew);
        
        if (aList.size() > 0) {
            aNew = (Action) aDAO.find(aNew).get(0);
            addNewEntry(aNew);
        } else {
            // DebugBuffer.error("Could not find Flight Action for fleet " + pf.getId() + " and userHandle " + userHandle);
        }
    }

    private boolean isSystemFrozen(Integer systemId) {

        HashSet<Integer> users = new HashSet<Integer>();
        HashSet<Integer> usersFrozen = new HashSet<Integer>();

        for (Planet p : pDAO.findBySystemId(systemId)) {
            PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
            if (pp != null) {
                if (!users.contains(pp.getUserId())) {
                    users.add(pp.getUserId());
                }
                if (!usersFrozen.contains(pp.getUserId())) {
                    if (usDAO.findByUserId(pp.getUserId()).isFrozen()) {
                        usersFrozen.add(pp.getUserId());
                    }
                }
            }
        }
        if (!users.isEmpty()) {
            if (users.size() == usersFrozen.size()) {
                return true;
            }
        }
        return false;
    }

    private boolean isPlanetFrozen(Integer planetId) {

        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        if (pp == null) {
            return false;
        } else {
            return usDAO.findByUserId(pp.getUserId()).isFrozen();
        }
    }

    private void debugPrintBrokenFleet(PlayerFleet pf, FlightProcessingEntry fpe, int ticktime) {
        // Print fleet itself
        DebugBuffer.warning("Current Calculation Time: " + ticktime + " DEBUG PRINT FLEET: " + pf.getName() + " ("+pf.getId()+")");
        DebugBuffer.warning("Fleet Position > " + pf.getSystemId() + ":" + pf.getPlanetId());
        DebugBuffer.warning("Fleet Status > " + pf.getStatus());
        
        DebugBuffer.warning("Flight Processing Entry => FleetDetails");
        DebugBuffer.warning("FPE Direction > " + fpe.getDirection());
        DebugBuffer.warning("FD StartLoc > " + fpe.getFleetDetail().getStartSystem()+":"+fpe.getFleetDetail().getStartPlanet());
        DebugBuffer.warning("FD DestLoc > " + fpe.getFleetDetail().getDestSystem()+":"+fpe.getFleetDetail().getDestPlanet());
        DebugBuffer.warning("FD StartTime > " + fpe.getFleetDetail().getStartTime() + " FlightTime > " + fpe.getFleetDetail().getFlightTime());
              
        DebugBuffer.warning("Database => FleetDetails");
        FleetDetail fd = fdDAO.findByFleetId(pf.getId());
        DebugBuffer.warning("FD StartLoc > " + fd.getStartSystem()+":"+fd.getStartPlanet());
        DebugBuffer.warning("FD DestLoc > " + fd.getDestSystem()+":"+fd.getDestPlanet());
        DebugBuffer.warning("FD StartTime > " + fd.getStartTime() + " FlightTime > " + fd.getFlightTime());

        // Print all other other fleet approaching, leaving or residing in that system
    }
}
