/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.update;

import at.viswars.Logger.Logger;
import at.viswars.dao.ConstructionRestrictionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlanetLogDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.PlayerTroopDAO;
import at.viswars.dao.TradeRouteDAO;
import at.viswars.enumeration.EPlanetLogType;
import at.viswars.model.Planet;
import at.viswars.model.PlanetLog;
import at.viswars.model.PlayerPlanet;
import at.viswars.utilities.DestructionUtilities;
import at.viswars.utilities.ViewTableUtilities;
import at.viswars.viewbuffer.HeaderBuffer;

/**
 *
 * @author Stefan
 */
public class AbandonColonies {
    private PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private ConstructionRestrictionDAO crDAO = (ConstructionRestrictionDAO) DAOFactory.get(ConstructionRestrictionDAO.class);
    private PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    private PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

    private final UpdaterDataSet uds;
    
    protected AbandonColonies(UpdaterDataSet uds) {
        this.uds = uds;
    }
    
    protected boolean abandonColony(PlayerPlanet pp, int time) {
        // Check if there are PlayerTroops, if yes dont abandon
        // if (ptDAO.findByPlanetId(pp.getPlanetId()).size() > 0) return false;
        
        Logger.getLogger().write("DELETING COLONY: " + pp.getName() + " WITH ID " + pp.getPlanetId());

        /*
        // Delete all constructions
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("planetId",pp.getPlanetId()));
        
        // Check for buildings using constructionInterfaces and call destruction
        try {
            for (PlanetConstruction pc : pcDAO.findByPlanetId(pp.getPlanetId())) {
                ConstructionRestriction cr = null;
                cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, pc.getConstructionId());

                if (cr != null) {
                    Class rc = Class.forName("at.viswars.construction.restriction." + cr.getRestrictionClass());
                    ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                    cir.onDestruction(pp);
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on Construction Restriction: ", e);
        }

        pcDAO.removeAll(qks);
        ptDAO.removeAll(qks);
        
        // Delete ressources
        for (Ressource r : RessourceService.getAllStorableRessources()) {
            uds.removePlanetRessEntry(pp.getPlanetId(), EPlanetRessourceType.INSTORAGE, r.getId());
        }

        ppDAO.remove(pp);
        */

        DestructionUtilities.destroyPlayerPlanet(pp.getPlanetId());
        HeaderBuffer.reloadUser(pp.getUserId());

        // Create a deletion PlanetLog Entry
        PlanetLog pl = new PlanetLog();
        pl.setPlanetId(pp.getPlanetId());
        pl.setUserId(pp.getUserId());
        pl.setTime(time);
        pl.setType(EPlanetLogType.ABANDONED);
        plDAO.add(pl);

        Planet p = pDAO.findById(pp.getPlanetId());

        ViewTableUtilities.addOrRefreshSystem(pp.getUserId(), p.getSystemId(), time);


        return true;
    }
}
