/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.update.transport;

import at.viswars.update.TradeRouteDetailProcessing;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class PlanetNode {
    private final int planetId;

    private ArrayList<DirectionalEdge> outgoingTrades = new ArrayList<DirectionalEdge>();
    private ArrayList<DirectionalEdge> incomingTrades = new ArrayList<DirectionalEdge>();

    private HashMap<Integer,Integer> maxInQty = new HashMap<Integer,Integer>();
    private HashMap<Integer,Integer> maxOutQty = new HashMap<Integer,Integer>();

    private HashMap<Integer,Integer> currAssignedIn = new HashMap<Integer,Integer>();
    private HashMap<Integer,Integer> currAssignedOut = new HashMap<Integer,Integer>();

    public PlanetNode(int planetId) {
        this.planetId = planetId;
    }

    public void cleanUp() {
        currAssignedIn.clear();
        currAssignedOut.clear();
        
        for (DirectionalEdge de : outgoingTrades) {
            de.resetDetailEntries();
        }
    }

    /**
     * @return the planetId
     */
    public int getPlanetId() {
        return planetId;
    }

    public DirectionalEdge getDirectionalEdgeOutFor(int ressId, TradeRouteDetailProcessing trdp) {
        for (DirectionalEdge de : outgoingTrades) {
            TradeRouteDetailProcessing trdpTmp = de.getEntryForRess(ressId);
            if (trdpTmp != null) {
                if (trdpTmp.equals(trdp)) {
                    return de;
                }
            } else {
                // Logger.getLogger().write("Could not find " + trdp + " for ressource " + ressId + " in outgoing nodes for " + this.getPlanetId());
            }
        }

        return null;
    }

    public void addOutgoingTrade(DirectionalEdge outgoing) {
        if (outgoing.getNoOfEntries() == 0) return;
        getOutgoingTrades().add(outgoing);
    }

    public void addIncomingTrade(DirectionalEdge incoming) {
        if (incoming.getNoOfEntries() == 0) return;
        getIncomingTrades().add(incoming);
    }

    public void removeDirectionalEdgeIn(DirectionalEdge incoming) {
        incomingTrades.remove(incoming);
    }

    public void removeDirectionalEdgeOut(DirectionalEdge outgoing) {
        outgoingTrades.remove(outgoing);
    }

    public void setMaxOutgoingQty(int ressId, int qty) {
        maxOutQty.put(ressId, qty);
    }

    public void setMaxIncomingQty(int ressId, int qty) {
        maxInQty.put(ressId, qty);
    }

    public void setCurrentAssignedIncomingQty(int ressId, int qty) {
        currAssignedIn.put(ressId, qty);
    }

    public void setCurrentAssignedOutgoingQty(int ressId, int qty) {
        currAssignedOut.put(ressId, qty);
    }

    public void incCurrentAssignedIncomingQty(int ressId, int qty) {
        if (currAssignedIn.containsKey(ressId)) {
            currAssignedIn.put(ressId, currAssignedIn.get(ressId) + qty);
        } else {
            setCurrentAssignedIncomingQty(ressId,qty);
        }
    }

    public void decCurrentAssignedIncomingQty(int ressId, int qty) {
        if (!currAssignedIn.containsKey(ressId)) return;
        currAssignedIn.put(ressId, currAssignedIn.get(ressId) - qty);
    }

    public void incCurrentAssignedOutgoingQty(int ressId, int qty) {
        if (currAssignedOut.containsKey(ressId)) {
            currAssignedOut.put(ressId, currAssignedOut.get(ressId) + qty);
        } else {
            setCurrentAssignedOutgoingQty(ressId,qty);
        }
    }

    public void decCurrentAssignedOutgoingQty(int ressId, int qty) {
        if (!currAssignedOut.containsKey(ressId)) return;
        currAssignedOut.put(ressId, currAssignedOut.get(ressId) - qty);
    }
    
    public int getMaxOutgoingQty(int ressId) {
        int currAssignedOutValue = 0;
        if (currAssignedOut.containsKey(ressId)) {
            currAssignedOutValue = currAssignedOut.get(ressId);
        }

        return maxOutQty.get(ressId) - currAssignedOutValue;
    }

    public int getMaxIncomingQty(int ressId) {
        int currAssignedInValue = 0;
        if (currAssignedIn.containsKey(ressId)) {
            currAssignedInValue = currAssignedIn.get(ressId);
        }

        return maxInQty.get(ressId) - currAssignedInValue;
    }

    public ArrayList<TradeRouteDetailProcessing> getOutgoingForRess(int ressId) {
        ArrayList<TradeRouteDetailProcessing> allEntries = new ArrayList<TradeRouteDetailProcessing>();

        for (DirectionalEdge de : getOutgoingTrades()) {
            TradeRouteDetailProcessing trdp = de.getEntryForRess(ressId);
            if (trdp != null) allEntries.add(trdp);                
        }

        return allEntries;
    }

    /**
     * @return the outgoingTrades
     */
    public ArrayList<DirectionalEdge> getOutgoingTrades() {
        return outgoingTrades;
    }

    /**
     * @return the incomingTrades
     */
    public ArrayList<DirectionalEdge> getIncomingTrades() {
        return incomingTrades;
    }
}
