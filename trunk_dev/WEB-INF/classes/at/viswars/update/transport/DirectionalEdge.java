/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.update.transport;

import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.update.TradeRouteDetailProcessing;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class DirectionalEdge {
    private final PlanetNode source;
    private final PlanetNode target;

    private final int orgMaxCapacity;
    private int maxCapacity;
    private int additionalCapacity;
    private final double distance;

    private final HashMap<Integer,TradeRouteDetailProcessing> detailEntries =
            new HashMap<Integer,TradeRouteDetailProcessing>();
    private HashMap<Integer,Integer> assignedQuantities =
            new HashMap<Integer,Integer>();

    public DirectionalEdge(PlanetNode source, PlanetNode target, int maxCapacity) {
        this.source = source;
        this.target = target;
        this.maxCapacity = maxCapacity;
        this.orgMaxCapacity = maxCapacity;
        
        RelativeCoordinate rcSource = new RelativeCoordinate(0,source.getPlanetId());
        RelativeCoordinate rcTarget = new RelativeCoordinate(0,target.getPlanetId());
        
        distance = rcSource.distanceTo(rcTarget);
    }

    public void addDetail(TradeRouteDetailProcessing trdp) {
        detailEntries.put(trdp.getBase().getRessId(), trdp);
    }

    public void setAssignedQty(int ressId, int qty) {
        assignedQuantities.put(ressId,qty);
    }

    public void increaseAssignedQty(int ressId, int qty) {
        assignedQuantities.put(ressId,assignedQuantities.get(ressId) + qty);
    }

    public void decreaseAssignedQty(int ressId, int qty) {
        assignedQuantities.put(ressId,assignedQuantities.get(ressId) - qty);
    }

    public TradeRouteDetailProcessing getEntryForRess(int ressId) {
        return detailEntries.get(ressId);
    }

    public ArrayList<TradeRouteDetailProcessing> getDetailEntries() {
        ArrayList<TradeRouteDetailProcessing> trdpList = new ArrayList<TradeRouteDetailProcessing>();
        trdpList.addAll(detailEntries.values());

        return trdpList;
    }

    public int getNoOfEntries() {
        int number = 0;
        
        for (TradeRouteDetailProcessing trdp : detailEntries.values()) {
            if (!trdp.isClosed()) number++;
        }
        
        return number;
    }

    /**
     * @return the maxCapacity
     */
    public int getMaxCapacity() {
        return maxCapacity;
    }

    /**
     * @return the maxCapacity
     */
    public void increaseMaxCapacity(int value) {
        additionalCapacity = value;
        maxCapacity += value;
    }    
    
    /**
     * @return the source
     */
    public PlanetNode getSource() {
        return source;
    }

    /**
     * @return the target
     */
    public PlanetNode getTarget() {
        return target;
    }
    
    public void resetDetailEntries() {
        maxCapacity = orgMaxCapacity;
        additionalCapacity = 0;
        
        for (TradeRouteDetailProcessing trdp : detailEntries.values()) {            
            trdp.setClosed(false);
            trdp.resetMaxQty();
            trdp.setNewMaximum(getMaxCapacity());
        }
    }

    /**
     * @return the distance
     */
    public double getDistance() {
        return distance;
    }

    /**
     * @return the additionalCapacity
     */
    public int getAdditionalCapacity() {
        return additionalCapacity;
    }
}
