/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.update;

import at.viswars.model.TradeRouteDetail;

/**
 *
 * @author Stefan
 */
public class TradeRouteDetailProcessing {
    private final TradeRouteDetail base;
    
    private int maxQty = -1;
    private int maxByTargetSource = Integer.MAX_VALUE;
    private int assignedQty = 0;
    private boolean closed = false;
    
    public TradeRouteDetailProcessing(TradeRouteDetail trd) {
        this.base = trd;
    }      

    public void resetMaxQty() {
        if (closed) return;
        maxQty = -1;
    }

    public int getMaxQty() {
        if (maxQty == -1) {
            maxQty = Integer.MAX_VALUE;
            
            if (getBase().getMaxQty() > 0) {
                maxQty = Math.min(maxQty, (int)((long)getBase().getMaxQty() - getBase().getQtyDelivered()));
            }
            if (getBase().getMaxQtyPerTick() > 0) {
                maxQty = Math.min(maxQty, getBase().getMaxQtyPerTick());
            }
            
            maxQty = Math.min(maxQty,maxByTargetSource);
        }
        
        return maxQty;
    }

    public void setNewMaximum(int max) {
        maxQty = Math.min(getMaxQty(), max);
        maxQty = Math.max(0, maxQty);
    }
    
    public TradeRouteDetail getBase() {
        return base;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public int getAssignedQty() {
        return assignedQty;
    }

    public void setAssignedQty(int assignedQty) {
        this.assignedQty = assignedQty;
    }

    /**
     * @param maxByTargetSource the maxByTargetSource to set
     */
    protected void setMaxByTargetSource(int maxByTargetSource) {
        this.maxByTargetSource = maxByTargetSource;
    }
}
