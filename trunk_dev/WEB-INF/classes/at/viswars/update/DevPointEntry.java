/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.update;

/**
 *
 * @author Stefan
 */
public class DevPointEntry {
    private final int planetId;
    private final double devPointRating;
    private final long population;

    public DevPointEntry(int planetId, double devPointRating, long population) {
        this.planetId = planetId;
        this.devPointRating = devPointRating;
        this.population = population;
    }

    /**
     * @return the devPointRating
     */
    public double getDevPointRating() {
        return devPointRating;
    }

    /**
     * @return the population
     */
    public long getPopulation() {
        return population;
    }

    /**
     * @return the planetId
     */
    public int getPlanetId() {
        return planetId;
    }
}
