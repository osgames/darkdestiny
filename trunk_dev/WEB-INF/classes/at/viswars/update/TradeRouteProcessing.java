/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.update;

import at.viswars.Logger.Logger;
import at.viswars.enumeration.ETradeRouteStatus;
import at.viswars.enumeration.ETradeRouteType;
import at.viswars.model.TradeRoute;
import at.viswars.trade.TradeRouteExt;
import at.viswars.update.transport.DirectionalEdge;
import at.viswars.update.transport.PlanetNode;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TradeRouteProcessing extends AbstractRouteProcessing {
    public TradeRouteProcessing(UpdaterDataSet uds) {
        super(uds);
        initRoutes();
    }

    private void initRoutes() {
        if (debug) {
            Logger.getLogger().write("Start init of Traderoute Handler");
        }

        allDetailEntries.clear();
        clearAllSetRoutes();

        ArrayList<TradeRoute> allRoutes = trDAO.findAllByType(ETradeRouteType.TRADE);
        for (TradeRoute tr : allRoutes) {
            if (debug) {
                Logger.getLogger().write("Found route from " + tr.getStartPlanet() + " to " + tr.getTargetPlanet());
            }

            if (tr.getStatus().equals(ETradeRouteStatus.PENDING)) {
                continue;
            }

            TradeRouteExt tre = new TradeRouteExt(tr);
            TradeRouteProcessingEntry trpe = new TradeRouteProcessingEntry(tre);
            allEntries.put(tr.getId(), trpe);
            allDetailEntries.addAll(trpe.getFromToList());
            allDetailEntries.addAll(trpe.getToFromList());

            int actStart = tr.getStartPlanet();
            int actTarget = tr.getTargetPlanet();

            PlanetNode start = tg.getPlanet(actStart);
            PlanetNode target = tg.getPlanet(actTarget);

            if (start == null) {
                start = new PlanetNode(actStart);
                tg.addPlanet(start);
            }
            if (target == null) {
                target = new PlanetNode(actTarget);
                tg.addPlanet(target);
            }

            DirectionalEdge fromTo = new DirectionalEdge(start,target,tre.getCapacity());
            DirectionalEdge toFrom = new DirectionalEdge(target,start,tre.getCapacity());

            if (trpe.isFromTo()) {
                for (TradeRouteDetailProcessing trdp : trpe.getFromToList()) {
                    fromTo.addDetail(trdp);
                }
            }

            if (trpe.isToFrom()) {
                for (TradeRouteDetailProcessing trdp : trpe.getToFromList()) {
                    toFrom.addDetail(trdp);
                }
            }

            start.addIncomingTrade(toFrom);
            start.addOutgoingTrade(fromTo);

            target.addIncomingTrade(fromTo);
            target.addOutgoingTrade(toFrom);
        }
    }
}
