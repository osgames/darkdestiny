/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.update;

import at.viswars.Logger.Logger;
import at.viswars.enumeration.ETradeRouteStatus;
import at.viswars.enumeration.ETradeRouteType;
import at.viswars.model.TradeRoute;
import at.viswars.trade.TradeRouteExt;
import at.viswars.update.transport.DirectionalEdge;
import at.viswars.update.transport.PlanetNode;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TransportProcessing extends AbstractRouteProcessing {
    public TransportProcessing(UpdaterDataSet uds) {
        super(uds);
        initRoutes();
    }

    private void initRoutes() {
        if (debug) {
            Logger.getLogger().write("Start init of Traderoute Handler");
        }

        allDetailEntries.clear();
        clearAllSetRoutes();

        ArrayList<TradeRoute> allRoutes = trDAO.findAllByType(ETradeRouteType.TRANSPORT);
        for (TradeRoute tr : allRoutes) {
            if (debug) {
                Logger.getLogger().write("Found route from " + tr.getStartPlanet() + " to " + tr.getTargetPlanet());
            }

            if (tr.getStatus().equals(ETradeRouteStatus.PENDING)) {
                continue;
            }

            // Create Route Datastructure
            TradeRouteExt tre = new TradeRouteExt(tr);
            TradeRouteProcessingEntry trpe = new TradeRouteProcessingEntry(tre);
            allEntries.put(tr.getId(), trpe);
            allDetailEntries.addAll(trpe.getFromToList());
            allDetailEntries.addAll(trpe.getToFromList());

            int actStart = tr.getStartPlanet();
            int actTarget = tr.getTargetPlanet();

            // Set start and target Planet Node
            PlanetNode start = tg.getPlanet(actStart);
            PlanetNode target = tg.getPlanet(actTarget);

            if (start == null) {
                start = new PlanetNode(actStart);
                tg.addPlanet(start);
            }
            if (target == null) {
                target = new PlanetNode(actTarget);
                tg.addPlanet(target);
            }

            // Create Edges between planets
            DirectionalEdge outgoingStart = null;
            DirectionalEdge outgoingTarget = null;
            
            for (DirectionalEdge deTmp : start.getOutgoingTrades()) {
                if ((deTmp.getSource().getPlanetId() == start.getPlanetId()) && 
                    (deTmp.getTarget().getPlanetId() == target.getPlanetId())) {
                    outgoingStart = deTmp;
                }
            }
            for (DirectionalEdge deTmp : target.getOutgoingTrades()) {
                if ((deTmp.getSource().getPlanetId() == target.getPlanetId()) && 
                    (deTmp.getTarget().getPlanetId() == start.getPlanetId())) {
                    outgoingTarget = deTmp;
                }
            }
            
            if (outgoingStart == null) {
                outgoingStart = new DirectionalEdge(start,target,tre.getCapacity());
            }
            if (outgoingTarget == null) {
                outgoingTarget = new DirectionalEdge(target,start,tre.getCapacity());
            }

            // Add detail routes to Edges
            if (trpe.isFromTo()) {
                for (TradeRouteDetailProcessing trdp : trpe.getFromToList()) {
                    if (trdp.getBase().getDisabled()) continue;
                    // java.lang.System.out.println("Add trdp " + trdp.getBase().getId() + " [RessId: "+trdp.getBase().getRessId()+"] as outgoing to " + start.getPlanetId());
                    outgoingStart.addDetail(trdp);
                }
            }

            if (trpe.isToFrom()) {
                for (TradeRouteDetailProcessing trdp : trpe.getToFromList()) {
                    if (trdp.getBase().getDisabled()) continue;
                    outgoingTarget.addDetail(trdp);
                    // java.lang.System.out.println("Add trdp " + trdp.getBase().getId() + " [RessId: "+trdp.getBase().getRessId()+"] as outgoing to " + target.getPlanetId());
                }
            }            
            
            // Add directional edges to planets
            // start.addIncomingTrade(toFrom);
            start.addOutgoingTrade(outgoingStart);

            // target.addIncomingTrade(fromTo);
            target.addOutgoingTrade(outgoingTarget);
        }
    }
}
