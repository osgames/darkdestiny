/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.update;

import at.viswars.model.Action;
import at.viswars.model.FleetDetail;

/**
 *
 * @author Stefan
 */
public class FlightProcessingEntry {
    private final FleetDetail fd; 
    private final Action a;
    private FlightDirection direction;

    protected static enum FlightDirection {
        OUTSIDE_SYS, OUTSIDE_PLANET, PLANET_PLANET, PLANET_SYSTEM, PLANET_OUTSIDE,
        SYS_PLANET, SYS_OUTSIDE;
    }

    /**
     * @return the direction
     */
    public FlightDirection getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(FlightDirection direction) {
        this.direction = direction;
    }

    protected FlightProcessingEntry(FleetDetail fd, Action a) {
        this.fd = fd;
        this.a = a;
    }

    public FleetDetail getFleetDetail() {
        return fd;
    }

    public Action getAction() {
        return a;
    }
}
