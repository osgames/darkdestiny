/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.hangar;

import at.viswars.DebugBuffer;
import at.viswars.utilities.HangarUtilities;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author thaitzer
 */
public class Hangar {

    private int size;
    private int availableSize;
    private List<ShipTypeEntry> loaded;

    Hangar(int size) {
        setSize(size);
        loaded = new ArrayList<ShipTypeEntry>();
        // in order to calculate the available size, we need to know the containedInShipTypeEntry and the loadedShips
        updateAvailableSpace();

    }

    public final void updateAvailableSpace() {
        int consumedSpace = 0;
        for (ShipTypeEntry loadedShip : loaded) {
            consumedSpace += loadedShip.getSpaceCons() * loadedShip.getCount();
        }
        this.availableSize = getSize() - consumedSpace;
    }

    public int getAvailableSize() {
        return availableSize;
    }

    private void setAvailableSize(int availableSize) {
        this.availableSize = availableSize;
    }

    public boolean canNotLoadAll(ShipTypeEntry toLoad) {
        return toLoad.getCount() > this.numberOfShipsThatFitIntoHangar(toLoad);
    }

    private int numberOfShipsThatFitIntoHangar(ShipTypeEntry toLoad) {
        final Double numberOfShipsPerSingleHangar = Math.floor(getAvailableSize() / toLoad.getSpaceCons());
        int loadableShipsPerCarrier = numberOfShipsPerSingleHangar.intValue();

        return loadableShipsPerCarrier;
    }

    public List<ShipTypeEntry> getLoaded() {
        return loaded;
    }

    public void setLoadedAndUpdateAvailableSpace(List<ShipTypeEntry> loaded) {
        this.loaded = new ArrayList<ShipTypeEntry>(loaded);
        updateAvailableSpace();
    }

    public int getSize() {
        return size;
    }

    private void setSize(int size) {
        this.size = size;
    }

    public boolean loadAll(ShipTypeEntry toLoad) {
        loadAndUpdateAvailableSpace(toLoad, toLoad.getCount());
        return true;
    }

    public boolean loadPartly(ShipTypeEntry toLoad) {
        int count = (numberOfShipsThatFitIntoHangar(toLoad));
        if (count > 0) {
            ShipTypeEntry steSplit = toLoad.copyAndReduceSTE(count);
            loadAndUpdateAvailableSpace(steSplit, count);
            return true;
        }
        return false;
    }

    private void loadAndUpdateAvailableSpace(ShipTypeEntry steSplit, int count) {
        loaded.add(steSplit);
        this.setAvailableSize(this.getAvailableSize() - (steSplit.getSpaceCons() * count));
    }

    public void fillSpecificHangarRecursive(TreeMap<Float, TreeMap<Integer, ArrayList<ShipTypeEntry>>> availShips, int[] shipTypeIDs, Float carrierSpeed) {
        fillSpecificHangar(availShips, shipTypeIDs, carrierSpeed);
        if (loaded != null) {
            for (ShipTypeEntry loadedType : loaded) {

                loadedType.fillHangar(availShips);
            }
        }
    }

    private void fillSpecificHangar(TreeMap<Float, TreeMap<Integer, ArrayList<ShipTypeEntry>>> availShips, int[] shipTypeIDs, Float carrierSpeed) {
        boolean loadingProcessed = true;


        Float currentSpeed = availShips.firstKey();
        while (loadingProcessed) {
            loadingProcessed = false;


            TreeMap<Integer, ArrayList<ShipTypeEntry>> sizeMap = availShips.get(currentSpeed);


            if (sizeMap == null) {
                return;
            }

            for (int shipTypeID : shipTypeIDs) {
                if (sizeMap.containsKey(shipTypeID)) {
                    loadingProcessed = processShipType(sizeMap, shipTypeID, availShips);
                }
            }

            // if nothing was processed move to the next speed
            if (!loadingProcessed) {
                Float nextSpeed = availShips.higherKey(currentSpeed);
                if (nextSpeed != null) {
                    // do not load if the ship is slower than the carrier
                    if (nextSpeed < carrierSpeed) {
                        currentSpeed = nextSpeed;
                        loadingProcessed = true;
                    }
                }
            } else {
                currentSpeed = availShips.firstKey();
            }
        }
    }

    private boolean processShipType(TreeMap<Integer, ArrayList<ShipTypeEntry>> sizeMap, final int shipTypeID, TreeMap<Float, TreeMap<Integer, ArrayList<ShipTypeEntry>>> availShips) {
        boolean loadingProcessed;
        ShipTypeEntry toLoad = HangarUtilities.getShipWithBestHangar(sizeMap.get(shipTypeID));
        if (canNotLoadAll(toLoad)) {
            // Load partly and shutdown this hangar
            loadingProcessed = loadPartly(toLoad);
        } else {
            loadingProcessed = loadAll(toLoad);
            HangarUtilities.cleanTreeMaps(availShips, sizeMap, toLoad);
        }
        return loadingProcessed;
    }
}
