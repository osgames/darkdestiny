/*
 * ChassisSize.java
 *
 * Created on 13. November 2006, 23:41
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.hangar;

/**
 *
 * @author Stefan
 */
public interface ChassisSize {
    public static int FIGHTER = 300;
    public static int CORVETTE = 301;
    public static int FRIGATE = 351;
    public static int DESTROYER = 310;
    public static int CRUISER = 315;
    public static int BATTLESHIP = 320;
    public static int MONOLITH = 340;
    public static int TENDER = 350;        
    
    public static int FIGHTER_ID = 1;
    public static int CORVETTE_ID = 2;
    public static int FRIGATE_ID = 3;
    public static int DESTROYER_ID = 4;
    public static int CRUISER_ID = 5;
    public static int BATTLESHIP_ID = 6;
    public static int MONOLITH_ID = 8;
    public static int TENDER_ID = 10;      
}
