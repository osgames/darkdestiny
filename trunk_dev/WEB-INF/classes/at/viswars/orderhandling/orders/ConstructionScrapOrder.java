/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.orderhandling.orders;

import at.viswars.Buildable;

/**
 *
 * @author Stefan
 */
public class ConstructionScrapOrder extends ScrapOrder {
    public ConstructionScrapOrder(Buildable unit, int planetId, int userId, int count) {
        super(unit,planetId,userId,count);                
    }
}
