/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.orderhandling.orders;

import at.viswars.Buildable;

/**
 *
 * @author Stefan
 */
public class ConstructOrder {
    private final Buildable unit;
    private final int planetId;
    private final int userId;
    private final int count;    
    private final int priority;

    protected ConstructOrder(Buildable unit, int planetId, int userId, int count) {
        this.unit = unit;
        this.planetId = planetId;
        this.userId = userId;
        this.count = count;
        priority = -1;
    }
    protected ConstructOrder(Buildable unit, int planetId, int userId, int count, int priority) {
        this.unit = unit;
        this.planetId = planetId;
        this.userId = userId;
        this.count = count;
        this.priority = priority;
    }

    public Buildable getUnit() {
        return unit;
    }

    public int getPriority(){
        return priority;
    }
    public int getPlanetId() {
        return planetId;
    }

    public int getUserId() {
        return userId;
    }

    public int getCount() {
        return count;
    }

}
