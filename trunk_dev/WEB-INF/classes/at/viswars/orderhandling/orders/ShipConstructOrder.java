/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.orderhandling.orders;

import at.viswars.Buildable;

/**
 *
 * @author Stefan
 */
public class ShipConstructOrder extends ConstructOrder {
    public ShipConstructOrder(Buildable unit, int planetId, int userId, int count) {
        super(unit,planetId,userId,count);
    }
}
