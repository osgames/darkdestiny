/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.orderhandling.orders;

import at.viswars.Buildable;

/**
 *
 * @author Stefan
 */
public class ConstructionUpgradeOrder extends UpgradeOrder {
    private final int planetId;

    public ConstructionUpgradeOrder(Buildable fromUnit, Buildable toUnit, int count, int planetId, int userId) {
        super(fromUnit,toUnit,count);
        this.planetId = planetId;
        this.userId = userId;
    }

    /**
     * @return the planetId
     */
    public int getPlanetId() {
        return planetId;
    }
}
