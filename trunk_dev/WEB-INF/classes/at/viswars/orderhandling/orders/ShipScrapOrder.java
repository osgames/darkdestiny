/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.orderhandling.orders;

import at.viswars.Buildable;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.model.ShipDesign;

/**
 *
 * @author Stefan
 */
public class ShipScrapOrder extends ScrapOrder {
    private final int fleetId;

    public ShipScrapOrder(Buildable unit, int planetId, int userId, int count, int fleetId) {
        super(unit,planetId,userId,count);
        this.fleetId = fleetId;
    }

    /**
     * @return the fleetId
     */
    public int getFleetId() {
        return fleetId;
    }

    /**
     * @return the shipDesignId
     */
    public int getShipDesignId() {
        return ((ShipDesign)((ShipDesignExt)unit).getBase()).getId();
    }
}
