/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.orderhandling.result;

import at.viswars.result.BuildableResult;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class OrderResult {
    private final boolean success;
    private final ArrayList<String> errors;
    private final BuildableResult br;

    public OrderResult(boolean success) {
        this.success = success;
        this.errors = new ArrayList<String>();
        this.br = null;
    }    
    
    public OrderResult(boolean success, ArrayList<String> errors) {
        this.success = success;
        this.errors = errors;
        this.br = null;
    }

    public OrderResult(boolean success, BuildableResult br) {
        this.success = success;
        this.br = br;
        this.errors = br.getMissingRequirements();
    }    
    
    public OrderResult(boolean success, String error) {
        this.success = success;
        
        ArrayList<String> tmpErr = new ArrayList<String>();
        tmpErr.add(error);
        this.errors = tmpErr;
        this.br = null;
    }        
    
    public boolean isSuccess() {
        return success;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }

    /**
     * @return the br
     */
    public BuildableResult getBuildableResult() {
        return br;
    }
}
