/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.orderhandling;

import java.util.HashMap;

/**
 *
 * @author Stefan
 */
class OrderSync {
    private static HashMap<Integer,OrderSyncEntry> syncEntries = new HashMap<Integer,OrderSyncEntry>();
    
    protected static boolean addOrderProcess(OrderSyncEntry ose) {
        if (syncEntries.containsKey(ose.getUserId())) {
            return false;
        } else {
            syncEntries.put(ose.getUserId(), ose);
            return true;
        }
    }
    
    protected static void removeOrderProcess(int userId) {
        syncEntries.remove(userId);
    }
}
