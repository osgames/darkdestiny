/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.requestbuffer;

/**
 *
 * @author Stefan
 */
public class UpgradeStarbaseBuffer extends ParameterBuffer {
    public static final String DESIGN_ID = "designId";
    public static final String PLANET_ID = "planetId";
    public static final String SYSTEM_ID = "systemId";
    public static final String COUNT = "count";
    public static final String TO_DESIGN_ID = "toDesignId";
    
    public UpgradeStarbaseBuffer(int userId) {
        super(userId);
    }    
    
    public int getDesignId() {
        return (Integer)(getParameter(DESIGN_ID));
    }    
    
    public int getPlanetId() {
        return (Integer)(getParameter(PLANET_ID));
    }    
    
    public int getSystemId() {
        return (Integer)(getParameter(SYSTEM_ID));
    }      
    
    public int getCount() {
        return Integer.valueOf((String)getParameter(COUNT));
    }              

    public int getToDesignId() {
        return Integer.valueOf((String)getParameter(TO_DESIGN_ID));
    }        
}
