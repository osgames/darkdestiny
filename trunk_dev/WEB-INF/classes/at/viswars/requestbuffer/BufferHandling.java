/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.requestbuffer;

import at.viswars.Logger.Logger;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class BufferHandling {
    private static HashMap<Integer,ParameterBuffer> buffered = new HashMap<Integer,ParameterBuffer>();
    
    protected static int registerObject(ParameterBuffer pb) {
        int id = (int)(Math.random() * 99999d);
        while (buffered.keySet().contains(id)) {
            id = (int)(Math.random() * 99999d);
        }
        
        Logger.getLogger().write("REGISTERING BUFFER OBJECT " + id + " USERID: " + pb.getUserId() + " TYPE: " + pb.getClass().getName());
        buffered.put(id, pb);
        return id;
    }
    
    public static ParameterBuffer getBuffer(int userId, int id) {
        Logger.getLogger().write("QUERY BUFFER OBJECT " + id + " USERID: " + userId);
        
        ParameterBuffer pb = buffered.get(id);
        
        Logger.getLogger().write("PB IS" + pb);
        
        if (pb == null) return null;
        if (pb.getUserId() != userId) return null;
        
        return pb;
    }
    
    protected static void invalidate(ParameterBuffer pb) {
        Logger.getLogger().write("INVALIDATE BUFFER OBJECT " + pb.id);
        
        buffered.remove(pb.id);
    } 
}
