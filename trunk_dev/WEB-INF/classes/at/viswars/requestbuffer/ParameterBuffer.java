/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.requestbuffer;

import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public abstract class ParameterBuffer {
    protected final int id;
    private final int userId;
    public HashMap<String,Object> parameters = new HashMap<String,Object>();
    
    protected ParameterBuffer(int userId) {
        id = BufferHandling.registerObject(this);
        this.userId = userId;
    }
    
    public void setParameter(String key, Object value) {
        parameters.put(key, value);
    } 
    
    public Object getParameter(String key) {
        return parameters.get(key);
    }      
    
    public void invalidate() {
        BufferHandling.invalidate(this);        
    }
    
    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }
}
