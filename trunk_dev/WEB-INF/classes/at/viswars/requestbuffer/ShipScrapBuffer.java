/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.requestbuffer;

import at.viswars.Logger.Logger;

/**
 *
 * @author Stefan
 */
public class ShipScrapBuffer extends ParameterBuffer {
    public static final String FLEET_ID = "fleetId";
    public static final String DESIGN_ID = "sourceId";
    public static final String COUNT = "count";
    
    public ShipScrapBuffer(int userId) {
        super(userId);
    }    
    
    public int getFleetId() {
        Logger.getLogger().write("Par : " + getParameter(FLEET_ID));
        return (Integer)(getParameter(FLEET_ID));
    }
    
    public int getDesignId() {
        return (Integer)getParameter(DESIGN_ID);
    }

    public int getCount() {
        return Integer.parseInt((String)getParameter(COUNT));
    }
}
