/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.requestbuffer;

/**
 *
 * @author Stefan
 */
public class DeconstructionOrderAbortBuffer extends ParameterBuffer {
    public static final String CONSTRUCTION_ID = "consId";
    public static final String COUNT = "count";
    public static final String PLANET_ID = "planetId";
    public static final String ORDER_ID = "orderId";

    public DeconstructionOrderAbortBuffer(int userId) {
        super(userId);
    }

    public int getConstructionId() {
        return (Integer)getParameter(CONSTRUCTION_ID);
    }

    public int getCount() {
        return (Integer)getParameter(COUNT);
    }

    public int getPlanetId() {
        return (Integer)getParameter(PLANET_ID);
    }

    public int getOrderId() {
        return (Integer)getParameter(ORDER_ID);
    }
}
