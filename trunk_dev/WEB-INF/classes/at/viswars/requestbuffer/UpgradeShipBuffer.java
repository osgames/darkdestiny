/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.requestbuffer;

/**
 *
 * @author Stefan
 */
public class UpgradeShipBuffer extends ParameterBuffer {
    public static final String FLEET_ID = "fleetId";
    public static final String DESIGN_ID = "sourceId";
    public static final String TO_DESIGN_ID = "targetId";
    public static final String COUNT = "count";
    
    public UpgradeShipBuffer(int userId) {
        super(userId);
    }    
    
    public int getFleetId() {
        return (Integer)getParameter(FLEET_ID);
    }
    
    public int getDesignId() {
        return (Integer)getParameter(DESIGN_ID);
    }
    
    public int getToDesignId() {
        return Integer.parseInt((String)getParameter(TO_DESIGN_ID));
    }
    
    public int getCount() {
        return Integer.parseInt((String)getParameter(COUNT));
    }
}
