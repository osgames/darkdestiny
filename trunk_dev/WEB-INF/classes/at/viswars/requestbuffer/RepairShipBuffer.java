/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.requestbuffer;

import at.viswars.enumeration.EDamageLevel;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class RepairShipBuffer extends ParameterBuffer {
    public static final String FLEET_ID = "fleetId";
    public static final String DESIGN_ID = "sourceId";
    public static final String SHIPFLEET_ID = "shipFleetId";
    public static final String COUNT = "count";
    
    public RepairShipBuffer(int userId) {
        super(userId);
    }        
    
    public int getFleetId() {
        return (Integer)getParameter(FLEET_ID);
    }
    
    public int getDesignId() {
        return (Integer)getParameter(DESIGN_ID);
    }

    public int getShipFleetId() {
        return (Integer)getParameter(SHIPFLEET_ID);
    }    
    
    public HashMap<EDamageLevel,Integer> getCount() {
        return (HashMap<EDamageLevel,Integer>)getParameter(COUNT);
    }           
}
