/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.statistics;

import at.viswars.service.Service;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Aion
 */
public class BarChart extends Service implements IChart {

    int userId;
    public String title = "title";
    public String xTitle = "xTitle";
    public String yTitle = "yTitle";
    public PlotOrientation orientation = PlotOrientation.HORIZONTAL;
    public boolean legend = true;
    public boolean tooltips = false;
    public boolean urls = false;
    public String name = "name";
    public int width = 500;
    public int height = 500;
    public DefaultCategoryDataset dataset;

    public BarChart(DefaultCategoryDataset dataset, String xTitle, String yTitle, String title) {
        this.dataset = dataset;
        this.xTitle = xTitle;
        this.yTitle = yTitle;
        this.title = title;
        this.name = title;
    }

    public Chart getChart() {
        return new Chart(dataset, title, xTitle, yTitle, orientation, legend, tooltips, urls, name, width, height);
    }
}
