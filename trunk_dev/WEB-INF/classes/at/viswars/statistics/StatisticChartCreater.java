/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.statistics;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.database.access.DbConnect;
import at.viswars.model.StatisticEntry;
import at.viswars.service.Service;
import at.viswars.statistic.StatisticConstants;
import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.sql.ResultSet;
import java.sql.Statement;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Aion
 */
public class StatisticChartCreater extends Service {

    public static JFreeChart createChart(int statisticEntryId, int userId) {
        return createChart(statisticEntryDAO.findById(statisticEntryId), userId);

    }

    public static JFreeChart createChart(StatisticEntry se, Integer userId) {


        if (se.getRefType() == EStatisticRefType.CLAZZ) {
            try {
                Class clzz = Class.forName(se.getRef());
                //Get Constructor
                Constructor con = null;

                //Cast Relation
                IChart chart = null;
                try {
                    con = clzz.getConstructor(StatisticEntry.class, int.class);

                    //Cast Relation
                    chart = (IChart) con.newInstance(se, userId);
                } catch (Exception e) {
                    Logger.getLogger().write("E : " + e);
                    con = clzz.getConstructor();

                    //Cast Relation
                    chart = (IChart) con.newInstance();
                }
                if (se.getType() == EStatisticType.PIECHART) {
                    return createPieChart(chart, userId);
                } else if (se.getType() == EStatisticType.BARCHART) {
                    return createBarChart(chart, userId);
                }
            } catch (Exception e) {
                Logger.getLogger().write("Error : " + e);
            }
        } else if (se.getRefType() == EStatisticRefType.SELECTSTATEMENT) {
            try {
                Statement stmt = DbConnect.createStatement();
                ResultSet rs = stmt.executeQuery(se.getRef());

                if (se.getType() == EStatisticType.PIECHART) {
                    DefaultPieDataset dataset = new DefaultPieDataset();
                    while (rs.next()) {
                        dataset.setValue(rs.getString(2), rs.getLong(1));

                    }
                    PieChart chart = new PieChart(dataset,
                            ML.getMLStr(se.getName(), userId));
                    return createPieChart(chart, userId);

                } else if (se.getType() == EStatisticType.BARCHART) {
                    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
                    while (rs.next()) {
                        dataset.setValue(rs.getLong(1), ML.getMLStr(se.getName(), userId), rs.getString(2));

                    }
                    BarChart chart = new BarChart(dataset,
                            ML.getMLStr(se.getxTitle(), userId),
                            ML.getMLStr(se.getyTitle(), userId),
                            ML.getMLStr(se.getName(), userId));
                    return createBarChart(chart, userId);
                }
                rs.close();
            } catch (Exception e) {
                Logger.getLogger().write("Error : " + e);
            }
        }

        return null;
    }

    public static JFreeChart createPieChart(IChart ss, int userId) {

        Chart se = ss.getChart();

        DefaultPieDataset data = (DefaultPieDataset) se.getDataset();

        try {


            org.jfree.chart.JFreeChart chart = org.jfree.chart.ChartFactory.createPieChart(se.getTitle(), data, se.isLegend(), se.isTooltips(), se.isUrls());

            chart.setBackgroundPaint(Color.white);
            return chart;
            //   createHtmlFile(chart, se);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Fehler in Singlstatistic PIECHART - getData: ", e);
        } catch (Error ex) {
            DebugBuffer.writeStackTrace("ERROR: ", ex);
        }
        return null;
    }

    public static JFreeChart createBarChart(IChart ss, int userId) {

        Chart se = ss.getChart();
        DefaultCategoryDataset data = (DefaultCategoryDataset) se.getDataset();
      
        try {

            final JFreeChart chart = ChartFactory.createBarChart(se.getTitle(), se.getxTitle(), se.getyTitle(), data, se.getOrientation(), se.isLegend(), se.isTooltips(), se.isUrls());
            chart.setBackgroundPaint(Color.white);
            // createHtmlFile(chart, se);
            DebugBuffer.addLine(DebugLevel.TRACE, "Chart generated");
            return chart;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Fehler in SingleStatistic - createBarChart ", e);
        }
        return null;
    }

    public static void createHtmlFile(JFreeChart chart, Chart se) {
        try {
            File image = new File(StatisticConstants.CHART_DIRECTORY, se.getName() + ".jpg");
            ChartUtilities.saveChartAsJPEG(image, chart, se.getWidth(), se.getHeight());

            File test = new File(StatisticConstants.CHART_DIRECTORY, se.getName() + ".html");

            String tmpImageFilePath = image.getAbsolutePath();
            tmpImageFilePath = tmpImageFilePath.substring((tmpImageFilePath.length() - 4 - se.getName().length()), tmpImageFilePath.length());
            DebugBuffer.addLine(DebugLevel.TRACE, "BildPfad = " + tmpImageFilePath);

            FileWriter f1 = new FileWriter(test);

            f1.write("<html>\n\n");
            f1.write("\t <body bgcolor='BLACK'>\n\n");
            f1.write("\t\t <table align='CENTER'>\n");
            f1.write("\t\t\t <tr>\n");
            f1.write("\t\t\t\t <td align='CENTER'>");
            f1.write("<img src='" + tmpImageFilePath + "'></img>");
            f1.write("</td>\n");
            f1.write("\t\t\t </tr>\n");
            f1.write("\t\t </table>\n");

            f1.write("\t </body>\n\n");
            f1.write("</html>");

            f1.close();


        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Fehler in SingleStatistik.java", e);

            DebugBuffer.addLine(DebugLevel.TRACE, "Fehler in SingleStatistic - CreateHtmlFile " + e);
        }

    }
}
