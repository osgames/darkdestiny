/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.statistics.designs;

import at.viswars.model.StatisticEntry;
import at.viswars.statistics.Chart;
import at.viswars.statistics.IChart;

/**
 *
 * @author Bullet
 */
public class FighterChart implements IChart {

    Chart chart;

    public FighterChart(StatisticEntry se, int userId) {

        ShipDesignChart sdc = new ShipDesignChart(se, 1);
        this.chart = sdc.getChart(userId);
    }

    public Chart getChart() {
        return chart;
    }
}
