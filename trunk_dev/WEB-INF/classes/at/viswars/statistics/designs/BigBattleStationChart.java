/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.statistics.designs;

import at.viswars.model.StatisticEntry;
import at.viswars.statistics.Chart;
import at.viswars.statistics.IChart;

/**
 *
 * @author Bullet
 */
public class BigBattleStationChart implements IChart {

    Chart chart;

    public BigBattleStationChart(StatisticEntry se, int userId) {

        ShipDesignChart sdc = new ShipDesignChart(se, 9);
        this.chart = sdc.getChart(userId);
    }

    public Chart getChart() {
        return chart;
    }
}
