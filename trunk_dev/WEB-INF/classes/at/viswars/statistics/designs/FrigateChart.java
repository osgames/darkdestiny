/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.statistics.designs;

import at.viswars.model.StatisticEntry;
import at.viswars.statistics.Chart;
import at.viswars.statistics.IChart;

/**
 *
 * @author Bullet
 */
public class FrigateChart implements IChart {

    Chart chart;

    public FrigateChart(StatisticEntry se, int userId) {

        ShipDesignChart sdc = new ShipDesignChart(se, 3);
        this.chart = sdc.getChart(userId);
    }

    public Chart getChart() {
        return chart;
    }
}
