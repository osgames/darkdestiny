/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.statistics.designs;

import at.viswars.model.StatisticEntry;
import at.viswars.statistics.Chart;
import at.viswars.statistics.IChart;

/**
 *
 * @author Bullet
 */
public class TenderChart implements IChart{
   Chart chart;
 public TenderChart(StatisticEntry se, int userId) {

        ShipDesignChart sdc = new ShipDesignChart(se, 10);
        this.chart = sdc.getChart(userId);
    }

    public Chart getChart() {
        return chart;
    }
}
