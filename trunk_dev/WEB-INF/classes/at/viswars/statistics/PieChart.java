/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.statistics;

import at.viswars.model.StatisticEntry;
import at.viswars.service.Service;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Aion
 */
public class PieChart extends Service implements IChart {

    int userId;
    String title = "Title";
    String xTitle = null;
    String yTitle = null;
    PlotOrientation orientation = null;
    boolean legend = true;
    boolean tooltips = false;
    boolean urls = false;
    String name = "Name";
    int width = 500;
    int height = 500;
    DefaultPieDataset dataset;


    public PieChart(DefaultPieDataset dataset, String title) {
        this.dataset = dataset;
        this.title = title;
    this.name = title;
    }

    public Chart getChart() {
        return new Chart(dataset, title, xTitle, yTitle, orientation, legend, tooltips, urls, name, width, height);
    }
}
