/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.construction.BuildingAbortRessourceCost;
import at.viswars.construction.BuildingDeconstructionRessourceCost;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EBonusRange;
import at.viswars.model.ActiveBonus;
import at.viswars.model.Bonus;
import at.viswars.model.GroundTroop;
import at.viswars.model.Planet;
import at.viswars.model.PlanetRessource;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.PlayerTroop;
import at.viswars.model.ProductionOrder;
import at.viswars.model.Ressource;
import at.viswars.model.UserData;
import java.util.ArrayList;
import java.util.TreeMap;
import at.viswars.planetcalc.*;
import at.viswars.result.BaseResult;
import java.util.Map;

/**
 *
 * @author Bullet
 */
public class OverviewService extends Service {

    public static Planet findPlanetById(int planetId) {
        return planetDAO.findById(planetId);
    }

    public static PlayerPlanet findPlayerPlanetByPlanetId(int planetId) {
        return playerPlanetDAO.findByPlanetId(planetId);
    }

    public static ArrayList<PlanetRessource> findMineableRessources(int planetId) {
        return planetRessourceDAO.findMineableRessources(planetId);
    }

    public static Ressource findRessourceById(int ressourceId) {
        return ressourceDAO.findRessourceById(ressourceId);
    }

    public static PlanetCalculation PlanetCalculation(int planetId) {
        try {
            return new PlanetCalculation(planetId);

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Retrieval of Extended Planet Data failed for " + planetId + "!", e);
        }

        return null;
    }

    public static ArrayList<ActiveBonus> findActiveBonus(int planetId) {
        return activeBonusDAO.findByPlanetId(planetId);
    }

    public static Bonus findBonusById(int bonusId) {
        return bonusDAO.findById(bonusId);
    }

    public static ArrayList<Bonus> findAvailBonus(int planetId) {
        ArrayList<Bonus> result = new ArrayList<Bonus>();
        for (Bonus b : bonusDAO.findPlanetBonus()) {
            boolean add = true;
            for (ActiveBonus ab : activeBonusDAO.findByPlanetId(planetId)) {
                if (b.getId() == ab.getBonusId()) {
                    add = false;
                    break;
                }
            }
            if (add) {
                if (b.getReqResearch() > 0) {
                    if (!ResearchService.isResearched(playerPlanetDAO.findByPlanetId(planetId).getUserId(), b.getReqResearch())) {
                        continue;
                    }
                }

                PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
                int alreadyActive = getActiveBoniForUser(pp.getUserId(),b.getId());

                b.setCost(b.getCost() * (alreadyActive + 1));
                result.add(b);
            }
        }
        return result;
    }

    public static ExtPlanetCalcResult getCalculatedPlanetValues(int planetId) {
        try {
            PlanetCalculation pc = new PlanetCalculation(planetId);
            return pc.getPlanetCalcData().getExtPlanetCalcData();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Could not load extended planet calculations: ", e);
        }

        return null;
    }

    public static UserData findUserData(int userId) {
        return userDataDAO.findByUserId(userId);
    }

    private static int getActiveBoniForUser(int userId, int bonusId) {
        int activeBoni = 0;

        ArrayList<PlayerPlanet> ppList = playerPlanetDAO.findByUserId(userId);
        for (PlayerPlanet pp : ppList) {
            ArrayList<ActiveBonus> abList = activeBonusDAO.findByBonusId(bonusId);

            for (ActiveBonus ab : abList) {
                if (ab.getRefId().equals(pp.getPlanetId())) {
                    activeBoni++;
                }
            }
        }

        return activeBoni;
    }

    public static ArrayList<BaseResult> buyBonus(int userId, int planetId, Map<String, String[]> params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        if (playerPlanetDAO.findByPlanetId(planetId) == null) {
            result.add(new BaseResult("Planet does not belong to you", true));
            return result;
        }
        if (playerPlanetDAO.findByPlanetId(planetId).getUserId() != userId) {
            result.add(new BaseResult("Planet does not belong to you", true));
            return result;
        }

        if (params.get("bonusId") != null) {
            // We check how many bonuses of this type have already been consumed
            // and increase cost on multiple usage

            int bonusId = Integer.parseInt(params.get("bonusId")[0]);
            if (bonusId > 0) {
                for (Bonus b : findAvailBonus(planetId)) {
                    if (b.getId() == bonusId) {
                        int alreadyActive = getActiveBoniForUser(planetId,b.getId());

                        UserData ud = userDataDAO.findByUserId(userId);
                        if ((b.getCost() * (alreadyActive + 1)) > ud.getDevelopementPoints()) {
                            result.add(new BaseResult("Not enough points", true));
                            return result;
                        }

                        ud.setDevelopementPoints(ud.getDevelopementPoints() - (b.getCost() * (alreadyActive + 1)));
                        userDataDAO.update(ud);
                        ActiveBonus ab = new ActiveBonus();
                        ab.setBonusId(bonusId);
                        ab.setDuration(b.getDuration());
                        ab.setRefId(planetId);
                        ab.setBonusRange(EBonusRange.PLANET);
                        activeBonusDAO.add(ab);
                        break;
                    }
                }
            } else {

                result.add(new BaseResult(ML.getMLStr("overview_err_nobonusselected", userId), true));
                return result;
            }
        } else {
            result.add(new BaseResult(ML.getMLStr("overview_err_nobonusselected", userId), true));
            return result;
        }
        return result;
    }

    public static ArrayList<Ressource> findAllRessources() {

        ArrayList<Ressource> result = new ArrayList<Ressource>();
        TreeMap<Integer, Ressource> ressources = new TreeMap<Integer, Ressource>();

        for (Ressource r : ressourceDAO.findAllMineableRessources()) {
            ressources.put(r.getId(), r);
        }
        for (Ressource r : ressourceDAO.findAllStoreableRessources()) {
            ressources.put(r.getId(), r);
        }
        ressources.put(Ressource.ENERGY, ressourceDAO.findRessourceById(Ressource.ENERGY));

        result.addAll(ressources.values());

        return result;

    }

    public static long getPopulation(int planetId) {
        return playerPlanetDAO.findByPlanetId(planetId).getPopulation();
    }

    public static int getMilitaryPower(int planetId) {
        int militaryPower = 0;
        try {
            for (PlayerTroop pt : playerTroopDAO.findByPlanetId(planetId)) {
                GroundTroop gt = groundTroopDAO.findById(pt.getTroopId());
                militaryPower += gt.getHitpoints() * pt.getNumber();
            }
        } catch (Exception e) {
            Logger.getLogger().write("ex : " + e);

        }

        return militaryPower;
    }

    public static BuildingAbortRessourceCost findForAbortion(int timeFinished, int userId) {
        ProductionOrder po = productionOrderDAO.findById(timeFinished);
        int constructionId = actionDAO.findByTimeFinished(timeFinished, EActionType.BUILDING).getConstructionId();

        BuildingAbortRessourceCost barc = new BuildingAbortRessourceCost(constructionId, po);
        return barc;
    }

    public static BuildingDeconstructionRessourceCost findForDeconstruction(int buildingID, int planetId, int userID) {
        BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(buildingID, planetId);

        return bdrc;
    }

    public static BuildingDeconstructionRessourceCost findForDeconstruction(int buildingID, int userID) {
        BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(buildingID);

        return bdrc;
    }

    public static BuildingDeconstructionRessourceCost findForDeconstruction(int buildingID, int planetId, int userID, int count) {
        BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(buildingID, planetId);

        return bdrc;
    }

    @Deprecated
    public static ArrayList<Ressource> findAllStoreableRessources() {
        return ressourceDAO.findAllPayableRessources();
    }
}
