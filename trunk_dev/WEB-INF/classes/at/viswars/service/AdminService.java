/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.enumeration.ELocationType;
import at.viswars.model.BattleLogEntry;
import at.viswars.model.Planet;
import at.viswars.model.PlayerFleet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.ShipDesign;
import at.viswars.model.StatisticCategory;
import at.viswars.model.StatisticEntry;
import at.viswars.model.User;
import at.viswars.result.BaseResult;
import at.viswars.result.BattleLogRestoreEntry;
import at.viswars.result.BattleLogRestoreResult;
import at.viswars.statistics.EStatisticRefType;
import at.viswars.statistics.EStatisticType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Bullet
 */
public class AdminService extends Service {

    public static BattleLogRestoreResult getBattleLogRestore(int battleLogId) {
        BattleLogRestoreResult blrr = new BattleLogRestoreResult();
        blrr.setBatteLogId(battleLogId);
        for (BattleLogEntry ble : Service.battleLogEntryDAO.findById(battleLogId)) {
            ShipDesign sd = Service.shipDesignDAO.findById(ble.getDesignId());
            User user = Service.userDAO.findById(sd.getUserId());
            if (user == null) {
                DebugBuffer.warning("Could not find User while BattleLog-Restore : userId - " + sd.getUserId());
                continue;
            }
            PlayerPlanet pp = Service.playerPlanetDAO.findHomePlanetByUserId(user.getUserId());
            if (pp == null) {
                DebugBuffer.warning("Could not find HomePlanet while BattleLog-Restore : userId - " + sd.getUserId());
                continue;
            }
            Planet p = Service.planetDAO.findById(pp.getPlanetId());
            if (p == null) {
                DebugBuffer.warning("Could not find Planet while BattleLog-Restore : userId - " + pp.getPlanetId());
                continue;
            }
            BattleLogRestoreEntry blre = new BattleLogRestoreEntry(ble, user, pp, p, sd);
            blrr.addEntry(blre);
        }

        return blrr;
    }

    public static ArrayList<BaseResult> restoreFleets(int battleLogId) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        BattleLogRestoreResult blrr = getBattleLogRestore(battleLogId);
        //UserId <=> Restored-Fleet
        HashMap<Integer, PlayerFleet> fleets = new HashMap<Integer, PlayerFleet>();
        for (BattleLogRestoreEntry blre : blrr.getEntries()) {
            try {
                int userId = blre.getUser().getUserId();
                PlayerFleet fleet = fleets.get(userId);
                if (fleet == null) {
                    fleet = new PlayerFleet();
                    if (blre.getPlanet().getId() != 0) {
                        fleet.setLocation(ELocationType.PLANET, blre.getPlanet().getId());
                    } else {
                        fleet.setLocation(ELocationType.SYSTEM, blre.getPlanet().getSystemId());
                    }
                    // fleet.setSystemId(blre.getPlanet().getSystemId());
                    // fleet.setPlanetId(blre.getPlanet().getId());
                    fleet.setUserId(userId);
                    fleet.setName("Reconstruction-Fleet");
                    // fleet.setStatus(0);

                    fleet = Service.playerFleetDAO.add(fleet);
                }


                result.add(new BaseResult("Restored Design" + blre.getDesign().getName() + "(Count: " + blre.getBattleLogEntry().getCount() + ") of " + blre.getUser().getGameName() + " to fleet : " + fleet.getId() + " (" + fleet.getSystemId() + ":" + fleet.getPlanetId() + ")", false));
                Service.shipFleetDAO.updateOrCreate(fleet.getId(), blre.getDesign().getId(), blre.getBattleLogEntry().getCount());
            } catch (Exception e) {
                result.add(new BaseResult("Could not restore Fleet for : " + blre.getUser().getUserName(), true));
            }
        }

        return result;
    }

    public static void deleteCategory(int userId, Map params) {
        if (userDAO.findById(userId).getAdmin()) {
            int categoryId = Integer.parseInt(((String[]) params.get("categoryId"))[0]);
            StatisticCategory sc = new StatisticCategory();
            sc.setId(categoryId);
            statisticCategoryDAO.remove(sc);
        }
    }

    public static void deleteEntry(int userId, Map params) {
        if (userDAO.findById(userId).getAdmin()) {
            int entryId = Integer.parseInt(((String[]) params.get("entryId"))[0]);
            StatisticEntry se = new StatisticEntry();
            se.setId(entryId);
            statisticEntryDAO.remove(se);
        }
    }

    public static void createCategory(int userId, Map params) {
        if (userDAO.findById(userId).getAdmin()) {

            StatisticCategory se = new StatisticCategory();

            for (Object entry : params.entrySet()) {
                Map.Entry me = (Map.Entry) entry;
                String parName = (String) me.getKey();
            }
            String name = ((String[]) params.get("categoryName"))[0];
            se.setName(name);
            statisticCategoryDAO.add(se);
        }
    }

    public static void createEntry(int userId, Map params) {
        if (userDAO.findById(userId).getAdmin()) {

            StatisticEntry se = new StatisticEntry();

            String name = ((String[]) params.get("name"))[0];
            String ref = ((String[]) params.get("ref"))[0];
            String xTitle = ((String[]) params.get("xTitle"))[0];
            String yTitle = ((String[]) params.get("yTitle"))[0];
            int categoryId = Integer.parseInt(((String[]) params.get("categoryId"))[0]);
            EStatisticRefType srt = EStatisticRefType.valueOf(((String[]) params.get("refType"))[0]);
            EStatisticType st = EStatisticType.valueOf(((String[]) params.get("statType"))[0]);

            se.setName(name);
            se.setCategoryId(categoryId);
            se.setRef(ref);
            se.setRefType(srt);
            se.setType(st);
            se.setxTitle(xTitle);
            se.setyTitle(yTitle);


            statisticEntryDAO.add(se);
        }
    }
}
