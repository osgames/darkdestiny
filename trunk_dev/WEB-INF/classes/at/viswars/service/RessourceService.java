/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.service;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.RessourceDAO;
import at.viswars.model.Ressource;
import at.viswars.model.UserData;
import at.viswars.utilities.ResearchUtilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Stefan
 */
public class RessourceService {
    private static RessourceDAO rDAO = (RessourceDAO)DAOFactory.get(RessourceDAO.class);
    
    public static Ressource[] getAllTransportableRessources(UserData user) {                
        Ressource r = new Ressource();
        r.setTransportable(true);
        
        ArrayList<Ressource> result = rDAO.find(r);        
        ArrayList<Ressource> toRemove = new ArrayList<Ressource>();        
        
        for (Ressource rAct : result) {
            if (rAct.getResearchRequired() != 0) {
                 if (!ResearchUtilities.isResearchResearched(rAct.getResearchRequired(), user.getUserId())) {
                    toRemove.add(rAct);
                 }
            }
        }
        
        result.removeAll(toRemove);
        sortRessources(result);        
        return result.toArray(new Ressource[result.size()]);
    }

    public static Ressource[] getAllTransportableRessources() {
        Ressource r = new Ressource();
        r.setTransportable(true);
        
        ArrayList<Ressource> result = rDAO.find(r);        
        sortRessources(result);        
        
        return result.toArray(new Ressource[result.size()]);
    }    
    
    public static Ressource[] getAllMineableRessources() {
        ArrayList<Ressource> result = rDAO.findAllMineableRessources();  
        sortRessources(result);        
        
        return result.toArray(new Ressource[result.size()]);
    }

    public static Ressource[] getAllStorableRessources() {
        ArrayList<Ressource> result = rDAO.findAllStoreableRessources();        
        sortRessources(result);        
        
        return result.toArray(new Ressource[result.size()]);
    }

    public static Collection<Ressource> getAllDisplayableRessources() {
        ArrayList<Ressource> result = rDAO.findAllStoreableRessources();        
        sortRessources(result);        

        return result;
    }

    public static Ressource getRessourceById(int ressourceId) {
        Ressource r = rDAO.findRessourceById(ressourceId);
        return r;
    }
    
    private static synchronized void sortRessources(List input) {
        Collections.sort(input, new Comparator<Ressource>() {
            public int compare(Ressource o1, Ressource o2) {
                return o1.getDisplayLocation() - o2.getDisplayLocation();
            }
        });
    }    
}
