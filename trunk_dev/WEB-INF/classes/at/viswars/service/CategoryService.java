/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.service;

import at.viswars.model.PlanetCategory;
import at.viswars.model.PlayerCategory;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class CategoryService extends Service{


    /**
     * @param PlanetID
     * @return the Name of the Planet
     */
    public static String getCategoryNamefromCategoryID(int categoryId) {
        if(categoryId == 0)return "Keine Kategory";
        return playerCategoryDAO.findById(categoryId).getName();
    }

    public static int getCategoryIdFromPlanetId(int planetId) {
        PlanetCategory pc = planetCategoryDAO.findByPlanetId(planetId);
        if(pc == null)return 0;
        return pc.getCategoryId();
    }

    public static ArrayList<PlayerCategory> getAllCategorysByUserId(int userId) {

        return playerCategoryDAO.findByUserId(userId);
       
    }

    public static void deleteCategory(int userId, int categoryId) {
        PlayerCategory pCat = playerCategoryDAO.findBy(userId, categoryId);
        playerCategoryDAO.remove(pCat);
        ArrayList<PlanetCategory> pcs = planetCategoryDAO.findByCategoryId(categoryId);
        for(PlanetCategory pc : pcs){
            if(playerPlanetDAO.findByPlanetId(pc.getPlanetId()).getUserId() != userId)continue;
            planetCategoryDAO.remove(pc);
        }
    }

    public static void changeCategory(int userId, int categoryId, String newName) {
        PlayerCategory pCat = playerCategoryDAO.findBy(userId, categoryId);
        pCat.setName(newName);
        playerCategoryDAO.update(pCat);

    }

    public static void addCategory(int userId, String newName) {
        PlayerCategory pc = new PlayerCategory();
        pc.setUserId(userId);
        pc.setName(newName);
        playerCategoryDAO.add(pc);
    }
}
