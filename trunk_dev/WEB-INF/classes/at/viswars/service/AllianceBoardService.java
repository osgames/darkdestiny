/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.FormatUtilities;
import at.viswars.GenerateMessage;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.dao.AllianceDAO;
import at.viswars.dao.AllianceMemberDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.enumeration.EAllianceBoardPermissionType;
import at.viswars.enumeration.EAllianceBoardType;
import at.viswars.enumeration.EMessageRefType;
import at.viswars.enumeration.EMessageType;
import at.viswars.model.Alliance;
import at.viswars.model.AllianceBoard;
import at.viswars.model.AllianceBoardPermission;
import at.viswars.model.AllianceMember;
import at.viswars.model.AllianceMessage;
import at.viswars.model.AllianceMessageRead;
import at.viswars.model.User;
import at.viswars.result.AllianceBoardResult;
import at.viswars.result.AlliancePermissionResult;
import at.viswars.result.BaseResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class AllianceBoardService {

    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    public static final AllianceBoard alliance_board_internal;
    public static final AllianceBoard alliance_board_structure;
    
    static {
        alliance_board_internal = new AllianceBoard();
        alliance_board_internal.setAllianceId(0);
        alliance_board_internal.setDescription("");
        alliance_board_internal.setId(AllianceBoard.ALLIANCE_BOARD_ID);
        alliance_board_internal.setName("alliance_allianceboard_name");
        alliance_board_internal.setType(EAllianceBoardType.ALLIANCE_INTERNAL);
        alliance_board_structure = new AllianceBoard();
        alliance_board_structure.setAllianceId(0);
        alliance_board_structure.setDescription("");
        alliance_board_structure.setId(AllianceBoard.TREATY_BOARD_ID);
        alliance_board_structure.setType(EAllianceBoardType.ALLIANCE_STRUCTURE);
        alliance_board_structure.setName("alliance_treatyboard_name");
    }
    
    public static ArrayList<BaseResult> updatePermissions(int userId, int boardId, Map params) {
        
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        
        Logger.getLogger().write("Params:" + params.entrySet().size());
        HashMap<Integer, AllianceBoardPermission> userPerms = new HashMap<Integer, AllianceBoardPermission>();
        HashMap<Integer, AllianceBoardPermission> alliancePerms = new HashMap<Integer, AllianceBoardPermission>();
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            if (parName.startsWith("perm_")) {
                String parsed = parName.replace("perm_", "");
                String idString = parsed.substring(0, parsed.indexOf("-"));
                int id = Integer.parseInt(idString);
                parsed = parsed.substring(parsed.indexOf("-") + 1, parsed.length());
                String typeString = parsed.substring(0, parsed.indexOf("-"));
                EAllianceBoardPermissionType type = EAllianceBoardPermissionType.valueOf(typeString);
                
                parsed = parsed.substring(parsed.indexOf("-") + 1, parsed.length());
                String nameString = parsed;
                
                AllianceBoardPermission abp = null;
                
                if (type.equals(EAllianceBoardPermissionType.ALLIANCE)) {
                    abp = alliancePerms.get(id);
                } else if (type.equals(EAllianceBoardPermissionType.USER)) {
                    abp = userPerms.get(id);
                }
                
                if (abp == null) {
                    abp = new AllianceBoardPermission();
                    abp.setType(type);
                    abp.setRefId(id);
                    abp.setConfirmed(false);
                    abp.setAllianceBoardId(boardId);
                } else {
                    // if abp was not null there is nothing to do
                    continue;
                }
                
                Object o = params.get("perm_" + id + "-" + type + "-write");
                Logger.getLogger().write(o + " o");
                if (o != null) {
                    abp.setWrite(true);
                } else {
                    abp.setWrite(false);
                }
                o = params.get("perm_" + id + "-" + type + "-delete");
                Logger.getLogger().write(o + " o");
                if (o != null) {
                    abp.setDelete(true);
                } else {
                    abp.setDelete(false);
                }
                if (type.equals(EAllianceBoardPermissionType.USER)) {
                    userPerms.put(id, abp);
                }
                if (type.equals(EAllianceBoardPermissionType.ALLIANCE)) {
                    alliancePerms.put(id, abp);
                }
            }
            Logger.getLogger().write(me.getKey() + " => " + ((String[]) me.getValue())[0]);
        }
        savePerms(userPerms, boardId, EAllianceBoardPermissionType.USER);
        savePerms(alliancePerms, boardId, EAllianceBoardPermissionType.ALLIANCE);
        
        return result;
    }
    
    public static AllianceBoardResult getBoardViewData(int userId, int boardId) {
        
        AllianceMember am = Service.allianceMemberDAO.findByUserId(userId);
        if (am == null) {
            boardId = getFirstAllianceBoardId(userId);
        }
        
        AllianceBoardResult abr = new AllianceBoardResult();
        
        try {
            AllianceBoardPermission userPermission = null;
            HashMap<Integer, AllianceBoardPermission> availBoardPermissions = new HashMap<Integer, AllianceBoardPermission>();
            for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findByUserId(userId)) {
                availBoardPermissions.put(abp.getAllianceBoardId(), abp);
                AllianceBoard ab = Service.allianceBoardDAO.findById(abp.getAllianceBoardId());
                abr.getBoards().put(ab.getId(), ab);
                int count = AllianceBoardService.numberOfNewMessages(ab.getAllianceId(), userId, abp.getAllianceBoardId());
                abr.setUnreadMessageCount(abp.getAllianceBoardId(), count);
                if (boardId == abp.getAllianceBoardId()) {
                    userPermission = abp;
                }
            }
            int allianceId = 0;
            if (am != null) {
                
                allianceId = am.getAllianceId();
                //Add Allyinternal and Allystructure board
                //Create fake Permission for allyinternal and allystructure-boards
                {
                    AllianceBoardPermission abp = new AllianceBoardPermission();
                    abp.setAllianceBoardId(alliance_board_internal.getId());
                    abp.setWrite(true);
                    if (am.getIsAdmin() || am.getIsCouncil()) {
                        abp.setDelete(Boolean.TRUE);
                    } else {
                        abp.setDelete(false);
                    }
                    abp.setRefId(userId);
                    abp.setType(EAllianceBoardPermissionType.USER);
                    if (!availBoardPermissions.containsKey(abp.getAllianceBoardId())) {
                        availBoardPermissions.put(alliance_board_internal.getId(), abp);
                        abr.getBoards().put(alliance_board_internal.getId(), alliance_board_internal);
                        // If count is wrong replace alliance_board_internal.getAllianceId() by am.getAllianceId() - just to save you time
                        int count = AllianceBoardService.numberOfNewMessages(alliance_board_internal.getAllianceId(), userId, abp.getAllianceBoardId());
                        abr.setUnreadMessageCount(abp.getAllianceBoardId(), count);
                    }
                    if (boardId == AllianceBoard.ALLIANCE_BOARD_ID) {
                        userPermission = abp;
                    }
                }
                Alliance alliance = AllianceService.findAllianceByAllianceId(allianceId);
                {
                    AllianceBoardPermission abp = new AllianceBoardPermission();
                    abp.setAllianceBoardId(alliance_board_structure.getId());
                    abp.setWrite(true);
                    if (am.getIsAdmin() || am.getIsCouncil()) {
                        abp.setDelete(Boolean.TRUE);
                    } else {
                        abp.setDelete(false);
                    }
                    abp.setRefId(userId);
                    abp.setType(EAllianceBoardPermissionType.USER);
                    if (!availBoardPermissions.containsKey(abp.getAllianceBoardId())) {
                        availBoardPermissions.put(alliance_board_structure.getId(), abp);
                        
                        abr.getBoards().put(alliance_board_structure.getId(), alliance_board_structure);
                        int count = AllianceBoardService.numberOfNewMessages(am.getAllianceId(), userId, abp.getAllianceBoardId());
                        abr.setUnreadMessageCount(abp.getAllianceBoardId(), count);
                    }
                    if (boardId == AllianceBoard.TREATY_BOARD_ID) {
                        userPermission = abp;
                    }
                }
                
                
                for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findByAllianceId(allianceId)) {
                    if (!availBoardPermissions.containsKey(abp.getAllianceBoardId())) {
                        availBoardPermissions.put(abp.getAllianceBoardId(), abp);
                        AllianceBoard ab = Service.allianceBoardDAO.findById(abp.getAllianceBoardId());
                        abr.getBoards().put(ab.getId(), ab);
                        int count = AllianceBoardService.numberOfNewMessages(ab.getAllianceId(), userId, abp.getAllianceBoardId());
                        // System.out.println("[OTHER] Set unread for " + abp.getAllianceBoardId() + " to count of " + count);
                        abr.setUnreadMessageCount(abp.getAllianceBoardId(), count);
                        if (boardId == abp.getAllianceBoardId()) {
                            userPermission = abp;
                        }
                    }
                }
                
            }
            
            ArrayList<AllianceBoardPermission> abps = new ArrayList<AllianceBoardPermission>();
            abps.addAll(availBoardPermissions.values());
            abr.addPermissions(abps, userId);
            
            ArrayList<User> allowedUsers = new ArrayList<User>();
            for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findBy(boardId, EAllianceBoardPermissionType.USER)) {
                allowedUsers.add(Service.userDAO.findById(abp.getRefId()));
            }
            abr.setAllowedUsers(allowedUsers);
            
            ArrayList<Alliance> allowedAlliances = new ArrayList<Alliance>();
            for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findBy(boardId, EAllianceBoardPermissionType.ALLIANCE)) {
                allowedAlliances.add(Service.allianceDAO.findById(abp.getRefId()));
            }
            abr.setAllowedAlliances(allowedAlliances);
            
            if (boardId == alliance_board_internal.getId()) {
                
                abr.setMessages(getAllMessages(allianceId, boardId));
            } else if (boardId == alliance_board_structure.getId()) {
                
                abr.setMessages(getAllMessages(allianceId, boardId));
            } else {
                
                Logger.getLogger().write("boardIdx : " + boardId);
                abr.setMessages(getAllMessages(Service.allianceBoardDAO.findById(boardId).getAllianceId(), boardId));
            }

            //Creating permission of all users who have access to the board

            //Create fake permissions for all allianceUsers
            if (boardId == AllianceBoard.ALLIANCE_BOARD_ID) {
                for (AllianceMember amTmp : Service.allianceMemberDAO.findByAllianceId(allianceId)) {
                    AllianceBoardPermission abp = new AllianceBoardPermission();
                    abp.setAllianceBoardId(alliance_board_internal.getId());
                    abp.setWrite(true);
                    if (am.getIsAdmin() || am.getIsCouncil()) {
                        abp.setDelete(Boolean.TRUE);
                    } else {
                        abp.setDelete(false);
                    }
                    abp.setRefId(amTmp.getUserId());
                    abp.setType(EAllianceBoardPermissionType.USER);
                    abr.getBoardPermissions().add(abp);
                }
            } else if (boardId == AllianceBoard.TREATY_BOARD_ID) {
                ArrayList<Alliance> allys = Service.allianceDAO.findByMasterId(Service.allianceDAO.findById(allianceId).getMasterAlliance());
                for (AllianceMember amTmp : Service.allianceMemberDAO.findAllianceMembersOfAlliances(allys)) {
                    AllianceBoardPermission abp = new AllianceBoardPermission();
                    abp.setAllianceBoardId(alliance_board_internal.getId());
                    abp.setWrite(true);
                    if (am.getIsAdmin() || am.getIsCouncil()) {
                        abp.setDelete(Boolean.TRUE);
                    } else {
                        abp.setDelete(false);
                    }
                    abp.setRefId(amTmp.getUserId());
                    abp.setType(EAllianceBoardPermissionType.USER);
                    abr.getBoardPermissions().add(abp);
                }
            } else {
                AllianceBoard ab = Service.allianceBoardDAO.findById(boardId);
                if (ab.getType().equals(EAllianceBoardType.CUSTOM)) {
                    for (AllianceBoardPermission abpTmp : Service.allianceBoardPermissionDAO.findByAllianceBoardId(boardId)) {
                        if (abpTmp.getType().equals(EAllianceBoardPermissionType.ALLIANCE)) {
                            for (AllianceMember amTmp : Service.allianceMemberDAO.findByAllianceId(abpTmp.getRefId())) {
                                AllianceBoardPermission abp = new AllianceBoardPermission();
                                abp.setAllianceBoardId(alliance_board_internal.getId());
                                abp.setWrite(true);
                                if (!(am == null) && (am.getIsAdmin() || am.getIsCouncil())) {
                                    abp.setDelete(Boolean.TRUE);
                                } else {
                                    abp.setDelete(false);
                                }
                                abp.setRefId(amTmp.getUserId());
                                abp.setType(EAllianceBoardPermissionType.USER);
                                abr.getBoardPermissions().add(abp);
                            }
                        } else {
                            abr.getBoardPermissions().add(abpTmp);
                        }
                    }
                } else {
                    if (ab.getType().equals(EAllianceBoardType.ALLIANCE_INTERNAL)) {
                        for (AllianceMember amTmp : Service.allianceMemberDAO.findByAllianceId(allianceId)) {
                            AllianceBoardPermission abp = new AllianceBoardPermission();
                            abp.setAllianceBoardId(ab.getId());
                            abp.setWrite(true);
                            if (am.getIsAdmin() || am.getIsCouncil()) {
                                abp.setDelete(Boolean.TRUE);
                            } else {
                                abp.setDelete(false);
                            }
                            abp.setRefId(amTmp.getUserId());
                            abp.setType(EAllianceBoardPermissionType.USER);
                            abr.getBoardPermissions().add(abp);
                            if (boardId == abp.getAllianceBoardId() && userId == amTmp.getUserId()) {
                                userPermission = abp;
                            }
                        }
                    } else if (ab.getType().equals(EAllianceBoardType.ALLIANCE_STRUCTURE)) {
                        ArrayList<Alliance> allys = Service.allianceDAO.findByMasterId(Service.allianceDAO.findById(allianceId).getMasterAlliance());
                        for (AllianceMember amTmp : Service.allianceMemberDAO.findAllianceMembersOfAlliances(allys)) {
                            AllianceBoardPermission abp = new AllianceBoardPermission();
                            abp.setAllianceBoardId(ab.getId());
                            abp.setWrite(true);
                            if (am.getIsAdmin() || am.getIsCouncil()) {
                                abp.setDelete(Boolean.TRUE);
                            } else {
                                abp.setDelete(false);
                            }
                            abp.setRefId(amTmp.getUserId());
                            abp.setType(EAllianceBoardPermissionType.USER);
                            abr.getBoardPermissions().add(abp);
                            if (boardId == abp.getAllianceBoardId() && userId == amTmp.getUserId()) {
                                userPermission = abp;
                            }
                        }
                    }
                }
            }
            
            abr.setUserPermission(userPermission);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getAllianceBoardView", e);
        }
        
        return abr;
        
        
    }
    
    public static void savePerms(HashMap<Integer, AllianceBoardPermission> perms, int boardId, EAllianceBoardPermissionType type) {
        ArrayList<Integer> added = new ArrayList<Integer>();
        for (Map.Entry<Integer, AllianceBoardPermission> perm : perms.entrySet()) {
            AllianceBoardPermission abpSearch = new AllianceBoardPermission();
            abpSearch.setRefId(perm.getValue().getRefId());
            abpSearch.setAllianceBoardId(perm.getValue().getAllianceBoardId());
            abpSearch.setType(perm.getValue().getType());
            abpSearch = (AllianceBoardPermission) Service.allianceBoardPermissionDAO.get(abpSearch);
            added.add(perm.getValue().getRefId());
            if (abpSearch == null) {
                Service.allianceBoardPermissionDAO.add(perm.getValue());
                sendUserAddedMessages(perm.getValue().getRefId(), boardId, type);
            } else {
                abpSearch.setDelete(perm.getValue().getDelete());
                abpSearch.setWrite(perm.getValue().getWrite());
                Service.allianceBoardPermissionDAO.update(abpSearch);
            }
        }
        
        ArrayList<AllianceBoardPermission> toDelete = new ArrayList<AllianceBoardPermission>();
        for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findBy(boardId, type)) {
            if (!added.contains(abp.getRefId())) {
                toDelete.add(abp);
            }
        }
        for (AllianceBoardPermission abp : toDelete) {
            Service.allianceBoardPermissionDAO.remove(abp);
            sendUserRemovedMessages(abp.getRefId(), boardId, type);
        }
    }
    
    public static ArrayList<AllianceBoardPermission> canRead(int boardId) {
        return Service.allianceBoardPermissionDAO.findByAllianceBoardId(boardId);
        
        
    }
    
    public static AllianceBoard findAllianceBoardById(int boardId) {
        if (boardId == AllianceBoard.ALLIANCE_BOARD_ID) {
            return alliance_board_internal;
        } else if (boardId == AllianceBoard.TREATY_BOARD_ID) {
            return alliance_board_structure;
        } else {
            return Service.allianceBoardDAO.findById(boardId);
        }
        
    }
    
    public static ArrayList<BaseResult> createAllianceBoard(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        
        String boardName = "";
        String boardDescription = "";
        EAllianceBoardType type = EAllianceBoardType.ALLIANCE_INTERNAL;
        
        AllianceMember am = amDAO.findByUserId(userId);
        Alliance alliance = null;
        if (am != null) {
            alliance = aDAO.findById(am.getAllianceId());
        }
        
        if ((alliance == null) || (am == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }
        
        int allianceId = alliance.getId();        
        
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            
            if (parName.equalsIgnoreCase("boardName")) {
                boardName = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("boardDescription")) {
                boardDescription = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("boardType")) {
                type = EAllianceBoardType.valueOf(((String[]) me.getValue())[0]);
            }
        }
        
        if (am.getIsAdmin() || am.getIsCouncil()) {
            createAllianceBoard(boardName, boardDescription, allianceId, type);
        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));
        }
        
        return result;
    }
    
    public static AlliancePermissionResult getAlliancePermissions(int userId, int allianceId, int allianceBoardId) {
        AlliancePermissionResult apr = new AlliancePermissionResult();
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        AllianceMember userAm = Service.allianceMemberDAO.findByUserId(userId);
        if (!(userAm.getIsAdmin() || userAm.getIsCouncil()) || allianceId != userAm.getAllianceId()) {
            result.add(new BaseResult("NO PERMISSION", true));
            apr.setResult(result);
            return apr;
        }
        HashMap<Integer, User> users = new HashMap<Integer, User>();
        apr.setUsersSorted(Service.userDAO.findAllSortedByName());
        for (User u : apr.getUsersSorted()) {
            users.put(u.getUserId(), u);
        }
        apr.setUsers(users);
        HashMap<Integer, Alliance> alliance = new HashMap<Integer, Alliance>();
        apr.setAlliancesSorted(Service.allianceDAO.findAllSortedByName());
        for (Alliance a : apr.getAlliancesSorted()) {
            alliance.put(a.getId(), a);
        }
        apr.setAlliances(alliance);
        
        HashMap<Integer, ArrayList<Integer>> allianceMembers = new HashMap<Integer, ArrayList<Integer>>();
        for (AllianceMember am : (ArrayList<AllianceMember>) Service.allianceMemberDAO.findAll()) {
            ArrayList<Integer> members = allianceMembers.get(am.getAllianceId());
            if (members == null) {
                members = new ArrayList<Integer>();
            }
            members.add(am.getUserId());
            allianceMembers.put(am.getAllianceId(), members);
        }
        apr.setAllianceMembers(allianceMembers);
        apr.setPermissions(Service.allianceBoardPermissionDAO.findByAllianceBoardId(allianceBoardId));
        
        return apr;
    }
    
    public static void createAllianceBoard(String name, String description, int allianceId, EAllianceBoardType type) {
        AllianceBoard ab = new AllianceBoard();
        ab.setAllianceId(allianceId);
        ab.setName(name);
        ab.setType(type);
        ab.setDescription(description);
        // @TODO PrimaryKey Fixen
        Service.allianceBoardDAO.add(ab);
    }
    
    public static ArrayList<BaseResult> deleteAllianceBoard(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        int boardId = -2;
        
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            
            if (parName.equalsIgnoreCase("boardId")) {
                boardId = Integer.parseInt(((String[]) me.getValue())[0]);
            }
        }
        
        AllianceMember am = amDAO.findByUserId(userId);
        Alliance alliance = null;
        if (am != null) {
            alliance = aDAO.findById(am.getAllianceId());
        }
        
        if ((alliance == null) || (am == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }
        
        int allianceId = alliance.getId();        
        
        if (am.getIsAdmin() || am.getIsCouncil()) {
            deleteAllianceBoard(allianceId, boardId);
        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));
        }
        
        return result;
    }
    
    public static void deleteAllianceBoard(int allianceId, int boardId) {
        ArrayList<AllianceMessageRead> abrs = Service.allianceMessageReadDAO.findByAllianceIdBoardId(allianceId, boardId);
        
        for (AllianceMessageRead abr : abrs) {
            Service.allianceMessageReadDAO.remove(abr);
        }
        
        for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findByAllianceBoardId(boardId)) {
            Service.allianceBoardPermissionDAO.remove(abp);
        }
        ArrayList<AllianceMessage> ams = Service.allianceMessageDAO.findByAllyIdBoardId(allianceId, boardId);
        
        for (AllianceMessage am : ams) {
            Service.allianceMessageDAO.remove(am);
        }
        
        AllianceBoard ab = new AllianceBoard();
        ab.setAllianceId(allianceId);
        ab.setId(boardId);
        Service.allianceBoardDAO.remove(ab);
    }
    
    public static void updateLastRead(int userId, int boardId) {
        if (boardId == AllianceBoard.ALLIANCE_BOARD_ID || boardId == AllianceBoard.TREATY_BOARD_ID) {
            AllianceMember am = Service.allianceMemberDAO.findByUserId(userId);
            updateLastRead(am.getAllianceId(), userId, boardId);
        } else {
            
            AllianceBoard ab = Service.allianceBoardDAO.findById(boardId);
            updateLastRead(ab.getAllianceId(), userId, boardId);
        }
    }
    
    public static void updateLastRead(int allianceId, int userId, int boardId) {
        AllianceMessageRead amr = Service.allianceMessageReadDAO.findByUserIdBoardId(allianceId, boardId, userId);
        
        if (amr != null) {
            amr.setLastRead(java.lang.System.currentTimeMillis());
            Service.allianceMessageReadDAO.update(amr);
        } else {
            amr = new AllianceMessageRead();
            amr.setAllianceId(allianceId);
            amr.setBoardId(boardId);
            amr.setUserId(userId);
            amr.setLastRead(java.lang.System.currentTimeMillis());
            Service.allianceMessageReadDAO.add(amr);
        }
    }
    
    public static ArrayList<BaseResult> deleteAllianceMessage(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        
        AllianceMember am = amDAO.findByUserId(userId);
        Alliance alliance = null;
        if (am != null) {
            alliance = aDAO.findById(am.getAllianceId());
        }
        
        if ((alliance == null) || (am == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }
        
        int allianceId = alliance.getId();        
        
        try {
            
            long messageId = 0;
            int boardId = -2;
            
            for (Object entry : params.entrySet()) {
                Map.Entry me = (Map.Entry) entry;
                String parName = (String) me.getKey();
                
                if (parName.equalsIgnoreCase("messageId")) {
                    messageId = Long.parseLong(((String[]) me.getValue())[0]);
                } else if (parName.equalsIgnoreCase("boardId")) {
                    boardId = Integer.parseInt(((String[]) me.getValue())[0]);
                }
            }
            
            if (messageId == 0) {
                result.add(new BaseResult(ML.getMLStr("alliance_err_messagenotexisting", userId), true));
            } else {
                result.addAll(deleteAllianceMessage(userId, allianceId, boardId, messageId));
            }
            
            return result;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in deleteAllianceMessage: ", e);
            result.add(new BaseResult(e.getMessage(), true));
        } finally {
            return result;
        }
    }
    
    public static ArrayList<BaseResult> deleteAllianceMessage(int userId, int allianceId, int boardId, long messageId) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        
        AllianceBoardPermission abp = getPermission(boardId, userId);
        if (abp == null || !abp.getDelete()) {
            result.add(new BaseResult("Not allowed to delete", true));
            return result;
        }
        
        AllianceMember am = Service.allianceMemberDAO.findByUserId(userId);
        
        if (boardId == AllianceBoard.TREATY_BOARD_ID) {
            ArrayList<Alliance> allys = Service.allianceDAO.findByMasterId(Service.allianceDAO.findById(am.getAllianceId()).getMasterAlliance());
            
            for (Alliance a : allys) {
                AllianceMessage amsg = new AllianceMessage();
                amsg.setAllianceId(a.getId());
                amsg.setPostTime(messageId);
                amsg.setBoardId(boardId);
                
                if (amsg != null) {
                    try {
                        if (am.getIsAdmin() || am.getUserId() == amsg.getUserId()) {
                            Service.allianceMessageDAO.remove(amsg);
                        } else {
                            result.add(new BaseResult("alliance_err_notenoughrights", true));
                        }
                    } catch (Exception e) {
                        //Nachricht war in der anderen Allianz nicht vorhanden
                    }
                } else {
                    result.add(new BaseResult("alliance_err_messagenotexisting", true));
                }
            }
        } else {
            AllianceMessage amsg = Service.allianceMessageDAO.findByPostTime(messageId, boardId, am.getAllianceId());
            
            if (amsg != null) {
                if (am.getIsAdmin() || am.getUserId() == amsg.getUserId()) {
                    Service.allianceMessageDAO.remove(amsg);
                } else {
                    result.add(new BaseResult("alliance_err_notenoughrights", true));
                }
            } else {
                result.add(new BaseResult("alliance_err_messagenotexisting", true));
            }
        }
        
        return result;
    }
    
    public static boolean hasAllianceBoardPermissions(int userId) {
        if (!((ArrayList<AllianceBoardPermission>) Service.allianceBoardPermissionDAO.findByUserId(userId)).isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean newMessagesAvailable(int userId) {
        AllianceMember am = Service.allianceMemberDAO.findByUserId(userId);
        int count = 0;
        if (am != null) {
            for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findByAllianceId(am.getAllianceId())) {
                AllianceBoard ab = Service.allianceBoardDAO.findById(abp.getAllianceBoardId());
                count += numberOfNewMessages(ab.getAllianceId(), userId, ab.getId());
            }

            // if (numberOfNewMessages(am.getAllianceId(), userId, -1) > 0) Logger.getLogger().write(IdToNameService.getUserName(userId) + " has new messages in boardId -1");
            count += numberOfNewMessages(am.getAllianceId(), userId, -1);

            /*
             * ArrayList<AllianceBoard> abs =
             * Service.allianceBoardDAO.findByAllianceId(am.getAllianceId());
             *
             * for (AllianceBoard ab : abs) { if
             * (numberOfNewMessages(am.getAllianceId(), userId, ab.getId()) > 0)
             * Logger.getLogger().write(IdToNameService.getUserName(userId) + "
             * has new messages in boardId " + ab.getId() + "
             * ["+ab.getName()+"]"); count +=
             * numberOfNewMessages(am.getAllianceId(), userId, ab.getId()); }
             */

            // if (numberOfNewMessages(am.getAllianceId(), userId,  AllianceBoardService.alliance_board_structure.getId()) > 0) Logger.getLogger().write(IdToNameService.getUserName(userId) + " has new messages in boardId " + AllianceBoardService.alliance_board_structure.getId());
            count += numberOfNewMessages(am.getAllianceId(), userId, AllianceBoardService.alliance_board_structure.getId());
        }
        for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findByUserId(userId)) {
            AllianceBoard ab = Service.allianceBoardDAO.findById(abp.getAllianceBoardId());
            // if (numberOfNewMessages(am.getAllianceId(), userId, ab.getId()) > 0) Logger.getLogger().write(IdToNameService.getUserName(userId) + " has new messages in boardId " + ab.getId() + " ["+ab.getName()+"]");
            count += numberOfNewMessages(ab.getAllianceId(), userId, ab.getId());
        }
        
        return count > 0;
    }
    
    public static int numberOfNewMessages(int allianceId, int userId, int boardId) {        
        int count = 0;
        long lastRead = 0;
        AllianceMessageRead amr = Service.allianceMessageReadDAO.findByUserIdBoardId(allianceId, boardId, userId);
        
        if (amr != null) {
            lastRead = amr.getLastRead();
        }
        
        for (AllianceMessage am : Service.allianceMessageDAO.findByAllyIdBoardId(allianceId, boardId)) {
            // System.out.println("Board ID " + boardId + " COMPARE " + am.getPostTime() + ">" + lastRead + "?");
            if (am.getPostTime() > lastRead) {
                count++;
            }
        }

        // System.out.println("New message count for userId " + userId + " in boardId " + boardId + " allianceId " + allianceId + " = " + count);
        
        return count;
    }
    
    public static ArrayList<BaseResult> createAllianceMessage(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        String topic = "";
        String message = "";
        int tmpBoardId = -2;
        
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            
            if (parName.equalsIgnoreCase("topicAMessage")) {
                topic = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("message")) {
                message = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("boardId")) {
                tmpBoardId = Integer.parseInt(((String[]) me.getValue())[0]);
            }
        }
        
        if (message.equals("")) {
            result.add(new BaseResult(ML.getMLStr("alliance_err_insertmessage", userId), true));
        } else {
            topic = FormatUtilities.escapeChars(topic);
            message = FormatUtilities.escapeChars(message);
            createAllianceMessage(topic, userId, message, tmpBoardId);
        }
        
        return result;
    }
    
    public static String createAllianceMessage(String topic, int userId, String message, int boardId) {
        long postTime = System.currentTimeMillis();
        //Nachricht muss auf allen Allianzbrettern des B?ndniss hinzugef?gt werden
        AllianceBoardPermission abp = getPermission(boardId, userId);
        if (abp == null || !abp.getWrite()) {
            return "Not allowed";
        }
        if (boardId == AllianceBoard.TREATY_BOARD_ID) {
            AllianceMember amem = Service.allianceMemberDAO.findByUserId(userId);
            ArrayList<Alliance> allys = Service.allianceDAO.findByMasterId(Service.allianceDAO.findById(amem.getAllianceId()).getMasterAlliance());
            
            for (Alliance a : allys) {
                AllianceMessage am = new AllianceMessage();
                am.setAllianceId(a.getId());
                am.setUserId(userId);
                am.setPostTime(postTime);
                am.setTopic(topic);
                am.setMessage(message);
                am.setBoardId(boardId);
                
                Service.allianceMessageDAO.add(am);
            }
            //Allianzinterne Nachrichten
        } else if (boardId == AllianceBoard.ALLIANCE_BOARD_ID) {
            AllianceMember amem = Service.allianceMemberDAO.findByUserId(userId);
            AllianceMessage am = new AllianceMessage();
            am.setAllianceId(amem.getAllianceId());
            am.setUserId(userId);
            am.setTopic(topic);
            am.setMessage(message);
            am.setPostTime(postTime);
            am.setBoardId(boardId);
            Service.allianceMessageDAO.add(am);
            AllianceMessageRead amr = Service.allianceMessageReadDAO.findByUserIdBoardId(amem.getAllianceId(), boardId, userId);
            
            if (amr != null) {
                amr.setLastRead(postTime);
                Service.allianceMessageReadDAO.update(amr);
            } else {
                amr = new AllianceMessageRead();
                amr.setAllianceId(amem.getAllianceId());
                amr.setBoardId(boardId);
                amr.setUserId(userId);
                amr.setLastRead(postTime);
                Service.allianceMessageReadDAO.add(amr);
            }
        } else {
            AllianceBoard ab = Service.allianceBoardDAO.findById(boardId);
            AllianceMessage am = new AllianceMessage();
            
            am.setAllianceId(ab.getAllianceId());
            am.setUserId(userId);
            am.setTopic(topic);
            am.setMessage(message);
            am.setPostTime(postTime);
            am.setBoardId(boardId);
            Service.allianceMessageDAO.add(am);
            AllianceMessageRead amr = Service.allianceMessageReadDAO.findByUserIdBoardId(ab.getAllianceId(), boardId, userId);
            
            if (amr != null) {
                amr.setLastRead(postTime);
                Service.allianceMessageReadDAO.update(amr);
            } else {
                amr = new AllianceMessageRead();
                amr.setAllianceId(ab.getAllianceId());
                amr.setBoardId(boardId);
                amr.setUserId(userId);
                amr.setLastRead(postTime);
                Service.allianceMessageReadDAO.add(amr);
            }
        }
        
        return "Done";
    }
    
    public static ArrayList<AllianceMessage> getAllMessages(int allianceId, int boardId) {
        return getAllMessages(Service.allianceDAO.findById(allianceId), boardId);
    }
    
    public static AllianceBoardPermission getPermission(int boardId, int userId) {
        AllianceBoardPermission permission = new AllianceBoardPermission();
        permission.setDelete(false);
        permission.setWrite(false);
        
        if (boardId == AllianceBoard.TREATY_BOARD_ID) {
            AllianceMember amem = Service.allianceMemberDAO.findByUserId(userId);
            permission.setWrite(true);
            if (amem.getIsAdmin() || amem.getIsCouncil()) {
                
                permission.setDelete(true);
            }
            return permission;
            //Allianzinterne Nachrichten
        } else if (boardId == AllianceBoard.ALLIANCE_BOARD_ID) {
            AllianceMember amem = Service.allianceMemberDAO.findByUserId(userId);
            permission.setWrite(true);
            if (amem.getIsAdmin() || amem.getIsCouncil()) {
                
                permission.setDelete(true);
            }
            return permission;
            
        } else {
            AllianceBoard ab = Service.allianceBoardDAO.findById(boardId);
            AllianceBoardPermission abp = new AllianceBoardPermission();
            abp.setAllianceBoardId(ab.getId());
            abp.setRefId(userId);
            abp.setType(EAllianceBoardPermissionType.USER);
            
            abp = (AllianceBoardPermission) Service.allianceBoardPermissionDAO.get(abp);
            if (abp != null) {
                return abp;
            }
            
            AllianceMember amem = Service.allianceMemberDAO.findByUserId(userId);
            if (amem != null) {
                
                abp = new AllianceBoardPermission();
                abp.setAllianceBoardId(ab.getId());
                abp.setRefId(amem.getAllianceId());
                abp.setType(EAllianceBoardPermissionType.ALLIANCE);
                
                abp = (AllianceBoardPermission) Service.allianceBoardPermissionDAO.get(abp);
                if (abp != null) {
                    
                    return abp;
                }
            }
        }
        
        
        return permission;
    }
    
    public static ArrayList<AllianceMessage> getAllMessages(Alliance alliance, int boardId) {
        Logger.getLogger().write("Alliance : " + alliance);
        ArrayList<AllianceMessage> messages = new ArrayList<AllianceMessage>();
        // If boardId == -2 search in all allied alliances for messages with boardId -2

        if (boardId == AllianceBoard.TREATY_BOARD_ID) {
            ArrayList<Alliance> allAlliances = Service.allianceDAO.findByMasterId(alliance.getMasterAlliance());
            
            for (Alliance a : allAlliances) {
                messages.addAll(Service.allianceMessageDAO.findByAllyIdBoardId(a.getId(), boardId));
            }
            
            TreeMap<Long, AllianceMessage> mTree = new TreeMap<Long, AllianceMessage>();
            
            for (AllianceMessage am : messages) {
                mTree.put(am.getPostTime(), am);
            }
            
            messages.clear();
            messages.addAll(mTree.descendingMap().values());
        } else if (boardId == AllianceBoard.ALLIANCE_BOARD_ID) {
            messages = Service.allianceMessageDAO.findByAllyIdBoardId(alliance.getId(), boardId);
            TreeMap<Long, AllianceMessage> mTree = new TreeMap<Long, AllianceMessage>();
            
            for (AllianceMessage am : messages) {
                mTree.put(am.getPostTime(), am);
            }
            
            messages.clear();
            messages.addAll(mTree.descendingMap().values());
        } else {
            
            messages = Service.allianceMessageDAO.findByAllyIdBoardId(alliance.getId(), boardId);
            TreeMap<Long, AllianceMessage> mTree = new TreeMap<Long, AllianceMessage>();
            
            for (AllianceMessage am : messages) {
                mTree.put(am.getPostTime(), am);
            }
            
            messages.clear();
            messages.addAll(mTree.descendingMap().values());
        }
        
        return messages;
    }
    
    private static int getFirstAllianceBoardId(int userId) {
        for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findByUserId(userId)) {
            return abp.getAllianceBoardId();
        }
        return AllianceBoard.ALLIANCE_BOARD_ID;
    }
    
    private static void sendUserAddedMessages(Integer refId, int boardId, EAllianceBoardPermissionType eAllianceBoardPermissionType) {
        GenerateMessage gm = new GenerateMessage();
        AllianceBoard ab = Service.allianceBoardDAO.findById(boardId);
        if (eAllianceBoardPermissionType.equals(EAllianceBoardPermissionType.ALLIANCE)) {
            for (AllianceMember am : Service.allianceMemberDAO.findByAllianceId(refId)) {
                gm.setDestinationUserId(am.getUserId());
                String topic = ML.getMLStr("alliance_board_addmsg_alliance", am.getUserId()).replace("%NAME%", ab.getName());
                String message = ML.getMLStr("alliance_board_addmsg_alliance", am.getUserId()).replace("%NAME%", ab.getName());
                
                gm.setTopic(topic);
                gm.setMsg(message);
                gm.setSourceUserId(Service.userDAO.getSystemUser().getUserId());
                gm.setMessageType(EMessageType.SYSTEM);
                gm.setReference(EMessageRefType.ALLIANCEBOARD_ID_ADD, ab.getId());
                gm.writeMessageToUser();
            }
            
        } else if (eAllianceBoardPermissionType.equals(EAllianceBoardPermissionType.USER)) {
            gm.setDestinationUserId(refId);
            String topic = ML.getMLStr("alliance_board_addmsg_user", refId).replace("%NAME%", ab.getName());
            String message = ML.getMLStr("alliance_board_addmsg_user", refId).replace("%NAME%", ab.getName());
            
            gm.setTopic(topic);
            gm.setMsg(message);
            gm.setSourceUserId(Service.userDAO.getSystemUser().getUserId());
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setReference(EMessageRefType.ALLIANCEBOARD_ID_ADD, ab.getId());
            gm.writeMessageToUser();
        }
        
        
    }
    
    private static void sendUserRemovedMessages(Integer refId, int boardId, EAllianceBoardPermissionType eAllianceBoardPermissionType) {
        GenerateMessage gm = new GenerateMessage();
        AllianceBoard ab = Service.allianceBoardDAO.findById(boardId);
        if (eAllianceBoardPermissionType.equals(EAllianceBoardPermissionType.ALLIANCE)) {
            for (AllianceMember am : Service.allianceMemberDAO.findByAllianceId(refId)) {
                gm.setDestinationUserId(am.getUserId());
                String topic = ML.getMLStr("alliance_board_delmsg_alliance", am.getUserId()).replace("%NAME%", ab.getName());
                String message = ML.getMLStr("alliance_board_delmsg_alliance", am.getUserId()).replace("%NAME%", ab.getName());
                
                gm.setTopic(topic);
                gm.setMsg(message);
                gm.setSourceUserId(Service.userDAO.getSystemUser().getUserId());
                gm.setMessageType(EMessageType.SYSTEM);
                gm.writeMessageToUser();
                gm.setReference(EMessageRefType.ALLIANCEBOARD_ID_DELETE, ab.getId());
            }
            
        } else if (eAllianceBoardPermissionType.equals(EAllianceBoardPermissionType.USER)) {
            gm.setDestinationUserId(refId);
            String topic = ML.getMLStr("alliance_board_delmsg_user", refId).replace("%NAME%", ab.getName());
            String message = ML.getMLStr("alliance_board_delmsg_user", refId).replace("%NAME%", ab.getName());
            
            gm.setTopic(topic);
            gm.setMsg(message);
            gm.setSourceUserId(Service.userDAO.getSystemUser().getUserId());
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setReference(EMessageRefType.ALLIANCEBOARD_ID_DELETE, ab.getId());
            gm.writeMessageToUser();
        }
    }
}
