/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.service;

import at.viswars.ML;
import at.viswars.enumeration.ECombatReportStatus;
import at.viswars.model.CombatPlayer;
import at.viswars.result.BaseResult;
import java.util.Map;

/**
 *
 * @author Aion
 */
public class GroundCombatService extends Service{



    public static BaseResult archiveReport(int userId, Map<String, String[]> params){
        BaseResult br = new BaseResult(ML.getMLStr("global_done", userId), false);

        int groundCombatId = 0;
        try{
            groundCombatId = Integer.parseInt(params.get("gcId")[0]);
        }catch(Exception e){
            return new BaseResult("Ung�ltige Parameter", true);
        }
        if(groundCombatId > 0){
            CombatPlayer cp = combatPlayerDAO.findBy(groundCombatId, userId);
            cp.setReportStatus(ECombatReportStatus.ARCHIVED);
            combatPlayerDAO.update(cp);
        }
        return br;
    }
    public static BaseResult deleteReport(int userId, Map<String, String[]> params){
        BaseResult br = new BaseResult(ML.getMLStr("global_done", userId), false);

        int groundCombatId = 0;
        try{
            groundCombatId = Integer.parseInt(params.get("gcId")[0]);
        }catch(Exception e){
            return new BaseResult("Ung�ltige Parameter", true);
        }
        if(groundCombatId > 0){
            CombatPlayer cp = combatPlayerDAO.findBy(groundCombatId, userId);
            cp.setReportStatus(ECombatReportStatus.DELETED);
            combatPlayerDAO.update(cp);
        }
        return br;
    }
}
