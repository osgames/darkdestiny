/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.service;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.model.Construction;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.PlayerPlanet;

/**
 *
 * @author Stefan
 */
public class PlanetAdministrationService {
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    public static boolean showMigrationWarning(int userId, int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        if (pp.getUserId() != userId) return false;

        PlanetConstruction pc = pcDAO.findBy(planetId, Construction.ID_CIVILSPACEPORT);
        int maxMigration = 150000;
        if (pc != null) {
            maxMigration += (pc.getNumber() * 250000);
        }

        if (pp.getMigration() < 0) {
            if (Math.abs(pp.getMigration()) > ((double)maxMigration * 0.95d)) {
                return true;
            }
        }

        return false;
    }
}
