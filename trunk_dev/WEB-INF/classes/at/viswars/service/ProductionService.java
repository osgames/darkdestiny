/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.FormatUtilities;
import at.viswars.ML;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.buildable.GroundTroopExt;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.*;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EBonusType;
import at.viswars.model.*;
import at.viswars.result.BaseResult;
import at.viswars.result.BuildableResult;
import at.viswars.result.InConstructionResult;
import at.viswars.result.ProductionBuildResult;
import at.viswars.utilities.ConstructionUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import at.viswars.orderhandling.OrderHandling;
import at.viswars.orderhandling.orders.GroundTroopConstructOrder;
import at.viswars.orderhandling.orders.ShipConstructOrder;
import at.viswars.orderhandling.result.OrderResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.result.*;
import at.viswars.utilities.BonusUtilities;
import at.viswars.utilities.ProductionUtilities;
import at.viswars.view.ProductionPointsView;

/**
 *
 * @author Bullet
 */
public class ProductionService extends Service {

    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);    

    public static boolean isConstructed(int planetId, int constructionId) {
        if (planetConstructionDAO.findBy(planetId, constructionId) != null) {
            return true;
        } else {
            return false;
        }
    }
    public static ArrayList<Chassis> findAllChassisOrdered(){
        return chassisDAO.findAllOrdered();
    }

    public static void setNewPriority(int orderNo, Integer priority) {
        ProductionOrder po = productionOrderDAO.findById(orderNo);
        po.setPriority(priority);
        productionOrderDAO.update(po);
    }

    public static InConstructionResult getInConstructionData(int planetId) {
        ArrayList<Action> actionList = new ArrayList<Action>();
        actionList.addAll(actionDAO.findByPlanet(planetId));

        InConstructionResult icr = new InConstructionResult();

        for (Action a : actionList) {
            if ((a.getType() != EActionType.SHIP)
                    && (a.getType() != EActionType.GROUNDTROOPS)
                    && (a.getType() != EActionType.SPACESTATION)) {
                continue;
            }

            ProductionOrder po = productionOrderDAO.findByAction(a);
            Model m = null;
            if ((a.getType() == EActionType.SHIP) || (a.getType() == EActionType.SPACESTATION)) {
                m = shipDesignDAO.findById(a.getShipDesignId());
            } else {
                m = groundTroopDAO.findById(a.getGroundTroopId());
            }
            icr.addEntry(po, a, m);
        }

        return icr;
    }

    public static long findGroundsTroopBy(int groundTroopId, int planetId, int userId) {
        long number = 0l;
        PlayerTroop playerTroop = playerTroopDAO.findBy(userId, planetId, groundTroopId);
        if (playerTroop != null) {
            number = playerTroop.getNumber();
        }
        return number;
    }

    public static InConstructionResult getInConstructionDataOtherPlanets(int userId, int actPlanetId) {
        ArrayList<Action> actionList = actionDAO.findByUserAndType(userId, EActionType.SHIP);
        actionList.addAll(actionDAO.findByUserAndType(userId, EActionType.SPACESTATION));
        actionList.addAll(actionDAO.findByUserAndType(userId, EActionType.GROUNDTROOPS));

        InConstructionResult icr = new InConstructionResult();

        for (Action a : actionList) {
            if (a.getPlanetId() == actPlanetId) {
                continue;
            }
            ProductionOrder po = productionOrderDAO.findByAction(a);
            Model m = null;
            if ((a.getType() == EActionType.SHIP) || (a.getType() == EActionType.SPACESTATION)) {
                m = shipDesignDAO.findById(a.getShipDesignId());
            } else {
                m = groundTroopDAO.findById(a.getGroundTroopId());
            }
            icr.addEntry(po, a, m);
        }

        return icr;
    }

    public static ProductionBuildResult getProductionListShips(int userId, int planetId) {
        ProductionBuildResult pbr = new ProductionBuildResult();

        ArrayList<ShipDesign> sdList = shipDesignDAO.findByUserId(userId);
        for (ShipDesign sd : sdList) {
            ShipDesignExt sde = new ShipDesignExt(sd.getId());
            BuildableResult br = ConstructionUtilities.canBeBuilt(sd, userId, planetId, 1);
            pbr.addProduction(sde, br);
        }

        return pbr;
    }

    public static ProductionBuildResult getProductionListGroundTroops(int userId, int planetId) {
        ProductionBuildResult pbr = new ProductionBuildResult();

        ArrayList<GroundTroop> gtList = groundTroopDAO.findAll();
        TreeMap<String, GroundTroop> sorted = new TreeMap<String, GroundTroop>();
        for (GroundTroop gt : gtList) {
            sorted.put(gt.getName(), gt);
        }

        for (GroundTroop gt : gtList) {
            if (gt.getId() == 0) {
                continue;
            }
            GroundTroopExt gte = new GroundTroopExt(gt.getId());
            BuildableResult br = ConstructionUtilities.canBeBuilt(gt, userId, planetId, 1);
            pbr.addProduction(gte, br);
        }

        return pbr;
    }

    public static synchronized BaseResult scrapShip(int userId, int fleetId, int designId, int count) {
        BaseResult result = null;

        return result;
    }

    public static synchronized BaseResult upgradeShip(int userId, int fleetId, int designId, int targetDesign, int count) {
        BaseResult result = null;

        return result;
    }

    public static synchronized ArrayList<String> buildShip(int userId, int planetId, Map params) {
        ArrayList<String> errorList = new ArrayList<String>();

        if(ProductionUtilities.isBlockedByEnemy(planetId)){
            errorList.add(ML.getMLStr("production_msg_blockedByEnemy", userId));
            return errorList;
        }

        HashMap<Integer, Integer> unitsToBuild = convertParamsToUnitMap(params);
        MutableRessourcesEntry mre = new MutableRessourcesEntry(userId, planetId);

        for (Map.Entry<Integer, Integer> unitEntry : unitsToBuild.entrySet()) {
            int designId = unitEntry.getKey();
            int count = unitEntry.getValue();

            ShipDesignExt sdEntry = new ShipDesignExt(designId);
            mre.subtractRess(sdEntry.getRessCost(), count);
        }

        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getQty() < 0) {
                Ressource r = rDAO.findRessourceById(rae.getRessId());
                
                errorList.add(FormatUtilities.getFormattedNumber(Math.abs(rae.getQty())) + " mehr " + ML.getMLStr(r.getName(), userId) + " ben�tigt");
            }
        }

        if (errorList.size() > 0) {
            return errorList;
        }

        for (Map.Entry<Integer, Integer> unitEntry : unitsToBuild.entrySet()) {
            int designId = unitEntry.getKey();
            int count = unitEntry.getValue();

            ShipDesignExt sdEntry = new ShipDesignExt(designId);
            ShipConstructOrder sco = new ShipConstructOrder(sdEntry, planetId, userId, count);

            OrderResult oRes = OrderHandling.constructOrder(sco);

            if (!oRes.isSuccess()) {
                errorList.addAll(oRes.getErrors());
            }
        }

        return errorList;
    }

    public static synchronized ArrayList<String> buildGroundTroop(int userId, int planetId, Map params) {
        ArrayList<String> errorList = new ArrayList<String>();

        HashMap<Integer, Integer> unitsToBuild = convertParamsToUnitMap(params);
        MutableRessourcesEntry mre = new MutableRessourcesEntry(userId, planetId);

        for (Map.Entry<Integer, Integer> unitEntry : unitsToBuild.entrySet()) {
            int troopId = unitEntry.getKey();
            int count = unitEntry.getValue();

            GroundTroopExt gtEntry = new GroundTroopExt(troopId);
            mre.subtractRess(gtEntry.getRessCost(), count);
        }

        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getQty() < 0) {
                Ressource r = rDAO.findRessourceById(rae.getRessId());
                
                errorList.add(FormatUtilities.getFormattedNumber(Math.abs(rae.getQty())) + " mehr " + ML.getMLStr(r.getName(), userId) + " ben�tigt");
            }
        }

        if (errorList.size() > 0) {
            return errorList;
        }

        for (Map.Entry<Integer, Integer> unitEntry : unitsToBuild.entrySet()) {
            int troopId = unitEntry.getKey();
            int count = unitEntry.getValue();

            GroundTroopExt gtEntry = new GroundTroopExt(troopId);
            GroundTroopConstructOrder gco = new GroundTroopConstructOrder(gtEntry, planetId, userId, count);

            OrderResult oRes = OrderHandling.constructOrder(gco);

            if (!oRes.isSuccess()) {
                errorList.addAll(oRes.getErrors());
            }
        }

        return errorList;
    }

    public static synchronized String getNameForShip(int id) {
        ShipDesign sd = new ShipDesign();
        sd.setId(id);
        sd = (ShipDesign) shipDesignDAO.get(sd);

        if (sd == null) {
            return "N/A";
        } else {
            return sd.getName();
        }
    }

    public static synchronized String getNameForTroop(int id) {
        GroundTroop gt = new GroundTroop();
        gt.setId(id);
        gt = (GroundTroop) groundTroopDAO.get(gt);

        if (gt == null) {
            return "N/A";
        } else {
            return gt.getName();
        }
    }

    public static synchronized Module getModule(int id) {
        Module m = new Module();
        m.setId(id);
        return (Module) moduleDAO.get(m);
    }

    public static Ressource findRessource(int id) {
        return ressourceDAO.findRessourceById(id);
    }

    private static HashMap<Integer, Integer> convertParamsToUnitMap(Map<String, String[]> allPars) {
        HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();

        for (final Map.Entry<String, String[]> entry : allPars.entrySet()) {
            String parName = entry.getKey();

            if (parName.startsWith("build")) {
                String[] valueStr = entry.getValue();

                int value = 0;
                int designId = 0;

                try {
                    value = Integer.parseInt(valueStr[0]);
                    designId = Integer.parseInt(parName.substring(5));
                    if (value > 0) {
                        result.put(designId, value);
                    }
                } catch (NumberFormatException nfe) {
                    continue;
                }
            }
        }

        return result;
    }

    public static GTRelationResult findGTRelationsBy(int srcId) {
        double totalDamage = 0d;
        GTRelationResult gtrr = new GTRelationResult();

        HashMap<Integer, Double> result = new HashMap<Integer, Double>();
        HashMap<Integer, GTRelation> gtrelations = new HashMap<Integer, GTRelation>();
        int count = 0;
        for (GTRelation gtr : (ArrayList<GTRelation>) gtRelationDAO.findBySrcId(srcId)) {
            double damage = gtr.getAttackPoints() * gtr.getAttackProbab() * gtr.getHitProbab();
            totalDamage += damage;
            result.put(gtr.getTargetId(), damage);
            gtrelations.put(gtr.getTargetId(), gtr);
            count++;
        }
        gtrr.setGtRelations(gtrelations);
        gtrr.setDamage(result);
        gtrr.setMeanDamage(totalDamage / (double) count);
        return gtrr;
    }

    public static ArrayList<GroundTroop> findAllGroundTroops() {
        ArrayList<GroundTroop> result = new ArrayList<GroundTroop>();
        TreeMap<Integer, GroundTroop> sorted = new TreeMap<Integer, GroundTroop>();
        for (GroundTroop gt : groundTroopDAO.findAll()) {
            sorted.put(gt.getId(), gt);
        }
        result.clear();
        result.addAll(sorted.values());
        return result;
    }

    public static ProductionPointsView calcBonus(int planetId) {
        try {
            PlanetCalculation pc = new PlanetCalculation(planetId);
            ProductionResult pr = pc.getPlanetCalcData().getProductionData();

            int modulePoints = (int)pr.getRessProduction(at.viswars.model.Ressource.MODUL_AP);
            int dockPoints = (int)pr.getRessProduction(at.viswars.model.Ressource.PL_DOCK);
            int orbDockPoints = (int)pr.getRessProduction(at.viswars.model.Ressource.ORB_DOCK);
            int kasPoints = (int)pr.getRessProduction(at.viswars.model.Ressource.KAS);

            int[] bonusMod = BonusUtilities.calculateValue(planetId, (int) modulePoints, EBonusType.PRODUCTION);
            int[] bonusDock = BonusUtilities.calculateValue(planetId, (int) dockPoints, EBonusType.PRODUCTION);
            int[] bonusOrbDock = BonusUtilities.calculateValue(planetId, (int) orbDockPoints, EBonusType.PRODUCTION);
            modulePoints = bonusMod[0];
            dockPoints = bonusDock[0];
            orbDockPoints = bonusOrbDock[0];
            double modBonus = bonusMod[1];
            double dockBonus = bonusDock[1];
            double orbDockBonus = bonusOrbDock[1];
            double kasBonus = 0;


            return new ProductionPointsView(modulePoints, dockPoints, orbDockPoints, kasPoints,
                    modBonus, dockBonus, orbDockBonus, kasBonus);
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error in Bonuscalcculation of production");
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isBlockedByEnemy(int planetId){
        return ProductionUtilities.isBlockedByEnemy(planetId);
    }
    
    public static synchronized void compressOrders(int userId, int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        if ((pp == null) || (pp.getUserId() != userId)) return;
        
        InConstructionResult icr = getInConstructionData(planetId);
        TreeMap<Integer,TreeMap<Integer,InConstructionEntry>> sorted = new TreeMap<Integer,TreeMap<Integer,InConstructionEntry>>();
        
        for (InConstructionEntry ice : icr.getEntries()) {
            if (!sorted.containsKey(ice.getOrder().getPriority())) {
                sorted.put(ice.getOrder().getPriority(), new TreeMap<Integer,InConstructionEntry>());
            }
            
            sorted.get(ice.getOrder().getPriority()).put(ice.getOrder().getId(),ice);            
        }
        
        int priority = 0;
        ArrayList<ProductionOrder> poList = new ArrayList<ProductionOrder>();
        
        for (TreeMap<Integer,InConstructionEntry> pIdSorted : sorted.values()) {
            for (InConstructionEntry ice : pIdSorted.values()) {
                ice.getOrder().setPriority(priority);
                if (priority < 10) priority++;
                poList.add(ice.getOrder());
            }
        }                     
        
        poDAO.updateAll(poList);
    }    
}
