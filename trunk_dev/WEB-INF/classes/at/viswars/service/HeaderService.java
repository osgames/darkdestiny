/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetCategoryDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlayerCategoryDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.SystemDAO;
import at.viswars.model.Planet;
import at.viswars.model.System;
import at.viswars.model.PlanetCategory;
import at.viswars.model.PlayerCategory;
import at.viswars.model.PlayerPlanet;
import at.viswars.view.header.CategoryEntity;
import at.viswars.view.header.HeaderData;
import at.viswars.view.header.PlanetEntity;
import at.viswars.view.header.SystemEntity;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class HeaderService {

    private static SystemDAO systemDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO planetDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO playerPlanetDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlayerCategoryDAO playerCategoryDAO = (PlayerCategoryDAO) DAOFactory.get(PlayerCategoryDAO.class);
    private static PlanetCategoryDAO planetCategoryDAO = (PlanetCategoryDAO) DAOFactory.get(PlanetCategoryDAO.class);
    private HeaderData headerData;

    public HeaderService() {
        headerData = new HeaderData();
    }

    public void load(int userId) {
        // Erstellen des k&uuml;nstlichen <ALLE> Systems das alle Systeme und Planeten enth&auml;lt
        //dbg Logger.getLogger().write("User: "+userId);
        try {
            ArrayList<PlayerPlanet> pps = playerPlanetDAO.findByUserId(userId);
            TreeMap<String, System> systems = new TreeMap<String, System>();
            for (PlayerPlanet pp : pps) {
                if (pp.getUserId() == userId) {
                    Planet p = planetDAO.findById(pp.getPlanetId());
                    System s = systemDAO.findById(p.getSystemId());
                    if (systems.containsKey(s.getName() + s.getId())) {
                        continue;
                    }
                    systems.put(s.getName() + s.getId(), systemDAO.findById(p.getSystemId()));
                }
            }


            CategoryEntity all = new CategoryEntity();
            all.setCategoryId(0);
            all.setCategoryName(" < Alle > ");

            int lastCategoryId = 0;
            int lastSystemId = 0;
            int lastPlanetId = 0;
            int firstSystemId = 0;
            int firstPlanetId = 0;

            boolean firstSystem = true;
            for (Map.Entry<String, System> entry : systems.entrySet()) {

                SystemEntity system = new SystemEntity();
                System s = entry.getValue();
                system.setSystemId(s.getId());
                system.setSystemName(s.getName());
                if (firstSystem) {
                    firstSystem = false;
                    firstSystemId = s.getId();
                    all.setFirstSystemId(firstSystemId);
                } else {
                    system.setPreviousSystemId(lastSystemId);
                    all.getSystems().get(lastSystemId).setNextSystemId(s.getId());



                }
                ArrayList<Planet> planets = planetDAO.findBySystemId(s.getId());
                TreeMap<String, PlayerPlanet> ppsSystem = new TreeMap<String, PlayerPlanet>();
                for (Planet p : planets) {
                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
                    if (pp != null) {
                        if (pp.getUserId() == userId) {
                            ppsSystem.put(pp.getName() + p.getId(), pp);
                        }
                    }


                }

                boolean firstPlanet = true;
                for (Map.Entry<String, PlayerPlanet> entry2 : ppsSystem.entrySet()) {
                    PlayerPlanet pp = entry2.getValue();
                    PlanetEntity planet = new PlanetEntity();
                    planet.setPlanetId(pp.getPlanetId());
                    planet.setPlanetName(pp.getName());

                    if (firstPlanet) {
                        firstPlanetId = pp.getPlanetId();
                        system.setFirstPlanetId(firstPlanetId);
                        //dbg Logger.getLogger().write("Akt Planet = " + allPlanetsResultSet.getInt(1));
                        //dbg Logger.getLogger().write("Setting next To : " + lastPlanetId);

                        firstPlanet = false;
                    } else {
                        //dbg Logger.getLogger().write("Akt Planet = " + allPlanetsResultSet.getInt(1));
                        //dbg Logger.getLogger().write("Setting next To : " + lastPlanetId);
                        planet.setPreviousPlanetId(lastPlanetId);
                        //dbg Logger.getLogger().write("Setting from: " + lastPlanetId + " to : " + lastPlanetId);
                        system.getPlanets().get(lastPlanetId).setNextPlanetId(pp.getPlanetId());
                    }
                    lastPlanetId = pp.getPlanetId();

                    system.getPlanets().put(pp.getPlanetId(), planet);

                }
                //dbg Logger.getLogger().write("Setzen des ersten Planetens: "+firstPlanetId+ " auf : "+lastPlanetId);
                //dbg Logger.getLogger().write("Setzen des letzten Planetens: "+lastPlanetId+ " auf : "+firstPlanetId);
                system.getPlanets().get(firstPlanetId).setPreviousPlanetId(lastPlanetId);
                system.getPlanets().get(lastPlanetId).setNextPlanetId(firstPlanetId);

                all.getSystems().put(s.getId(), system);
                lastSystemId = s.getId();


            }
            //dbg Logger.getLogger().write("Setting From 1: " + lastSystemId + " next to : " + firstSystemId);
            try {
                if(all.getSystems().size() > 0){
                all.getSystems().get(firstSystemId).setPreviousSystemId(lastSystemId);
                all.getSystems().get(lastSystemId).setNextSystemId(firstSystemId);
                }
            } catch (Exception ex) {
                //Wenn er zwar in playerplanet einen user gefunden hat aber f&uuml;r diesen planeten kein system bestimmen konnte
                DebugBuffer.addLine(DebugLevel.ERROR, "Error in Header : Planet in playerplanet but not assigned to a system: " + "");
                return;
            }

            headerData.getCategorys().put(0, all);
            // Erstellen der Category spezifischen Ansichten
            ArrayList<PlayerCategory> playerCategorys = playerCategoryDAO.findByUserId(userId);
            //dbg Logger.getLogger().write("2");
            lastCategoryId = 0;
            lastSystemId = 0;
            lastPlanetId = 0;
            firstSystemId = 0;
            firstPlanetId = 0;

//Logger.getLogger().write("userId , " + userId);
            for (PlayerCategory pc : playerCategorys) {
     //           Logger.getLogger().write("PlayerCategory : " + pc.getId());
                if (Service.planetCategoryDAO.findByCategoryId(pc.getId()) == null) {
                    continue;
                }
                if (Service.planetCategoryDAO.findByCategoryId(pc.getId()).size() == 0) {
                    continue;
                }
                //dbg Logger.getLogger().write("Done3");
                CategoryEntity category = new CategoryEntity();
                category.setCategoryId(pc.getId());
                category.setCategoryName(pc.getName());
                category.setPreviousCategoryId(lastCategoryId);
                if (!headerData.getCategorys().containsKey(lastCategoryId)) {
                    DebugBuffer.addLine(DebugLevel.WARNING, "!headerData.getCategorys().containsKey(lastCategoryId) : " + lastCategoryId);
                }
                if (headerData.getCategorys().get(lastCategoryId) == null) {
                    DebugBuffer.addLine(DebugLevel.WARNING, "headerData.getCategorys().get(lastCategoryId) == null" + lastCategoryId);
                }
                headerData.getCategorys().get(lastCategoryId).setNextCategoryId(pc.getId());
                lastCategoryId = pc.getId();
                ArrayList<PlanetCategory> planetCategorys = planetCategoryDAO.findByCategoryId(pc.getId());
                ArrayList<System> sysTmp = new ArrayList<System>();
                for (PlanetCategory plc : planetCategorys) {
                    Planet p = planetDAO.findById(plc.getPlanetId());
                    System s = systemDAO.findById(p.getSystemId());
                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
                    if(pp == null || pp.getUserId() != userId){
                        planetCategoryDAO.remove(plc);
                    }else if (!sysTmp.contains(s)) {
                        sysTmp.add(s);
                    }
                }
                firstSystem = true;
                for (System s : sysTmp) {

                    SystemEntity system = new SystemEntity();

                    system.setSystemId(s.getId());
                    system.setSystemName(s.getName());
                    if (firstSystem) {
                        firstSystem = false;
                        firstSystemId = s.getId();
                        category.setFirstSystemId(firstSystemId);
                    } else {
                        system.setPreviousSystemId(lastSystemId);
                        category.getSystems().get(lastSystemId).setNextSystemId(s.getId());
                    }
                    lastSystemId = s.getId();

                    ArrayList<PlanetCategory> pcs = new ArrayList<PlanetCategory>();
                    for (Planet p : planetDAO.findBySystemId(s.getId())) {
                        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
                        if (pp == null) {
                            continue;
                        }
                        if (pp.getUserId() == userId) {
                            PlanetCategory pcTmp = planetCategoryDAO.findBy(p.getId(), pc.getId());
                            if (pcTmp != null) {
                                pcs.add(pcTmp);
                            }
                        }

                    }
                    //     rs3 = stmt3.executeQuery("SELECT pc.planetid, pc.planetname, pc.categoryId FROM planetcategory pc, planet p WHERE pc.planetid = p.id AND p.systemId =" + s.getId() + "  ORDER by p.systemId asc;");

                    //dbg Logger.getLogger().write("Done5");

                    boolean firstPlanet = true;

                    for (PlanetCategory pcTmp : pcs) {
                        if (category.getCategoryId() != pcTmp.getCategoryId()) {
                            continue;
                        }

                        PlanetEntity planet = new PlanetEntity();
                        planet.setPlanetId(pcTmp.getPlanetId());
                        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(pcTmp.getPlanetId());
                        planet.setPlanetName(pp.getName());
                        if (firstPlanet) {
                            firstPlanetId = pcTmp.getPlanetId();
                            system.setFirstPlanetId(firstPlanetId);
                            firstPlanet = false;
                        } else {
                            planet.setPreviousPlanetId(lastPlanetId);
                            system.getPlanets().get(lastPlanetId).setNextPlanetId(pcTmp.getPlanetId());
                        }
                        lastPlanetId = pcTmp.getPlanetId();
                        system.getPlanets().put(pcTmp.getPlanetId(), planet);

                    }
                    if (system.getPlanets().size() > 0) {
                        system.getPlanets().get(firstPlanetId).setPreviousPlanetId(lastPlanetId);
                        system.getPlanets().get(lastPlanetId).setNextPlanetId(firstPlanetId);
                        category.getSystems().put(s.getId(), system);
                    }
                }
                if (category.getSystems().size() > 0) {
                    category.getSystems().get(firstSystemId).setPreviousSystemId(lastSystemId);
                    category.getSystems().get(lastSystemId).setNextSystemId(firstSystemId);
                } else {
                    continue;
                }
                headerData.getCategorys().put(pc.getId(), category);
            }
            headerData.getCategorys().get(0).setPreviousCategoryId(lastCategoryId);
            if (!headerData.getCategorys().containsKey(lastCategoryId)) {
                DebugBuffer.addLine(DebugLevel.WARNING, "!yheaderData.getCategorys().containsKey(lastCategoryId) : " + lastCategoryId);
            }
            if (headerData.getCategorys().get(lastCategoryId) == null) {
                DebugBuffer.addLine(DebugLevel.WARNING, "yheaderData.getCategorys().get(lastCategoryId) == null" + lastCategoryId);
            }
            headerData.getCategorys().get(lastCategoryId).setNextCategoryId(0);



        } catch (Exception e) {
            //dbg Logger.getLogger().write("Error in here: ");
            DebugBuffer.addLine(e);
        }
    }

    public HeaderData getHeaderData() {
        return headerData;
    }

    public void setHeaderData(HeaderData headerData) {
        this.headerData = headerData;
    }

    public void test() {

        for (Map.Entry<Integer, CategoryEntity> me : headerData.getCategorys().entrySet()) {

            //dbg Logger.getLogger().write("Category: " + me.getValue().getPreviousCategoryId() + " - " + me.getValue().getCategoryId() + " - " + me.getValue().getNextCategoryId());
            //dbg Logger.getLogger().write("First System : "+me.getValue().getFirstSystemId());

            for (Map.Entry<Integer, SystemEntity> me2 : me.getValue().getSystems().entrySet()) {

                //dbg System.out.print("System: " + me2.getValue().getPreviousSystemId() + " - " + me2.getValue().getSystemId() + " - " + me2.getValue().getNextSystemId());
                //dbg Logger.getLogger().write("");
                //dbg Logger.getLogger().write(">>>>First Planet: "+me2.getValue().getFirstPlanetId());


                for (Map.Entry<Integer, PlanetEntity> me3 : me2.getValue().getPlanets().entrySet()) {
                    //dbg System.out.print("Planets: " + me3.getValue().getPreviousPlanetId() + " - " + +me3.getValue().getPlanetId() + " - " + me3.getValue().getNextPlanetId());
                    //dbg Logger.getLogger().write("");
                }
            }
        }

    }

    public void test2() {


        for (Map.Entry<Integer, CategoryEntity> me : headerData.getCategorys().entrySet()) {

            try {
                printCategory((CategoryEntity) me.getValue());
            } catch (Exception e) {
                Logger.getLogger().write("Error for Category: " + me.getKey());
            }

            for (Map.Entry<Integer, SystemEntity> me2 : me.getValue().getSystems().entrySet()) {

                try {
                    printSystem((SystemEntity) me2.getValue());
                } catch (Exception e) {
                    Logger.getLogger().write("Error for System: " + me2.getKey());
                }
                for (Map.Entry<Integer, PlanetEntity> me3 : me2.getValue().getPlanets().entrySet()) {

                    try {
                        printPlanet((PlanetEntity) me3.getValue());
                    } catch (Exception e) {
                        Logger.getLogger().write("Error for Planet: " + me3.getKey());
                    }
                }
            }
        }

    }

    public void printCategory(CategoryEntity ce) {
        Logger.getLogger().write("==========================================");
        Logger.getLogger().write("::: Category: " + ce.getCategoryId());
        Logger.getLogger().write("::: Name: " + ce.getCategoryName());
        Logger.getLogger().write("::: firstSystem: " + ce.getFirstSystemId());
        Logger.getLogger().write("::: nextCat: " + ce.getNextCategoryId());
        Logger.getLogger().write("::: Previous: " + ce.getPreviousCategoryId());
    }

    public void printSystem(SystemEntity se) {
        Logger.getLogger().write("==========================================");
        Logger.getLogger().write("::: System: " + se.getSystemName());
        Logger.getLogger().write("::: Id: " + se.getSystemId());
        Logger.getLogger().write("::: firstPlanet: " + se.getFirstPlanetId());
        Logger.getLogger().write("::: NXTSystem: " + se.getNextSystemId());
        Logger.getLogger().write("::: PRSystem: " + se.getPreviousSystemId());
    }

    public void printPlanet(PlanetEntity pe) {
        Logger.getLogger().write("==========================================");
        Logger.getLogger().write("::: Id: " + pe.getPlanetId());
        Logger.getLogger().write("::: NXTSystem: " + pe.getNextPlanetId());
        Logger.getLogger().write("::: PRSystem: " + pe.getPreviousPlanetId());
    }
}
