/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.enumeration.EAttribute;
import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.ModuleType;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.admin.module.AttributeTree;
import at.viswars.admin.module.ModuleAttributeResult;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.ActionDAO;
import at.viswars.dao.AttributeDAO;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DesignModuleDAO;
import at.viswars.dao.HangarRelationDAO;
import at.viswars.dao.ModuleAttributeDAO;
import at.viswars.dao.ModuleDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.dao.ProductionOrderDAO;
import at.viswars.dao.RessourceCostDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.dao.ShipTypeAdjustmentDAO;
import at.viswars.dao.TradeRouteShipDAO;
import at.viswars.database.framework.QueryKey;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EDamageLevel;
import at.viswars.enumeration.ERessourceCostType;
import at.viswars.enumeration.EShipType;
import at.viswars.model.Action;
import at.viswars.model.Chassis;
import at.viswars.model.ConditionToTitle;
import at.viswars.model.DesignModule;
import at.viswars.model.HangarRelation;
import at.viswars.model.Module;
import at.viswars.model.ModuleAttribute;
import at.viswars.model.PlanetDefense;
import at.viswars.model.ProductionOrder;
import at.viswars.model.Ressource;
import at.viswars.model.RessourceCost;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipFleet;
import at.viswars.model.ShipTypeAdjustment;
import at.viswars.model.TradeRouteShip;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.orderhandling.OrderHandling;
import at.viswars.orderhandling.orders.ShipRepairOrder;
import at.viswars.orderhandling.orders.ShipScrapOrder;
import at.viswars.orderhandling.orders.ShipUpgradeOrder;
import at.viswars.orderhandling.result.OrderResult;
import at.viswars.requestbuffer.RepairShipBuffer;
import at.viswars.requestbuffer.ShipScrapBuffer;
import at.viswars.requestbuffer.UpgradeShipBuffer;
import at.viswars.result.BaseResult;
import at.viswars.result.ShipDesignResult;
import at.viswars.utilities.ShipUtilities;
import at.viswars.utilities.TitleUtilities;
import at.viswars.view.ShipModifyEntry;
import at.viswars.view.ShipModifyView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class ShipDesignService {

    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    private static ModuleAttributeDAO maDAO = (ModuleAttributeDAO) DAOFactory.get(ModuleAttributeDAO.class);
    private static DesignModuleDAO dmDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);
    private static RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static AttributeDAO attDAO = (AttributeDAO) DAOFactory.get(AttributeDAO.class);
    private static ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);
    private static ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);
    private static TradeRouteShipDAO trsDAO = (TradeRouteShipDAO) DAOFactory.get(TradeRouteShipDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static HangarRelationDAO hrDAO = (HangarRelationDAO) DAOFactory.get(HangarRelationDAO.class);

    public enum State {
        SUCCESS, FAIL, NOTHING_DONE;
    }

    public static ArrayList<ShipDesign> getAllDesigns(int userId) {
        ArrayList<ShipDesign> designs = sdDAO.findByUserId(userId);

        if (designs == null) {
            designs = new ArrayList<ShipDesign>();
        }

        TreeMap<String, ArrayList<ShipDesign>> sorting = new TreeMap<String, ArrayList<ShipDesign>>();
        for (ShipDesign sd : designs) {
            if (sorting.containsKey(sd.getName())) {
                ArrayList<ShipDesign> designList = sorting.get(sd.getName());
                designList.add(sd);
                sorting.put(sd.getName(), designList);
            } else {
                ArrayList<ShipDesign> designList = new ArrayList<ShipDesign>();
                designList.add(sd);
                sorting.put(sd.getName(), designList);
            }
        }

        designs.clear();
        for (ArrayList<ShipDesign> designList : sorting.values()) {
            designs.addAll(designList);
        }

        return designs;
    }

    public static boolean isInProduction(int userId, int designId) {
        ArrayList<Action> as = aDAO.findByUserAndType(userId, EActionType.SHIP);
        as.addAll(aDAO.findByUserAndType(userId, EActionType.SPACESTATION));

        for (Action a : as) {
            if (a.getShipDesignId() == designId) {
                return true;
            }

            ProductionOrder po = poDAO.findById(a.getRefTableId());
            if (po.getToId() == designId) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBuilt(int userId, int designId) {
        // Check for fleets
        ShipFleet sf = new ShipFleet();
        sf.setDesignId(designId);
        ArrayList<ShipFleet> sfList = sfDAO.find(sf);

        if (sfList.size() > 0) {
            return true;
        }

        // Check for hangars
        ArrayList<HangarRelation> hrList = hrDAO.findByDesignId(designId);
        if (!hrList.isEmpty()) return true;

        // Check for traderoutes
        for (TradeRouteShip trs : (ArrayList<TradeRouteShip>) trsDAO.findAll()) {
            if (trs.getDesignId() == designId) {
                return true;
            }
        }

        // Check for starbases
        ArrayList<PlanetDefense> pdList = pdDAO.findByUserAndShipDesign(userId, designId);
        if (pdList.size() > 0) {
            return true;
        }

        return false;

    }

    public static BaseResult renameDesign(int userId, int designId, String newName) {
        ShipDesign sd = sdDAO.findById(designId);
        if (sd == null) {
            return new BaseResult(ML.getMLStr("shipdesign_err_noshipfound", userId), true);
        } else {
            if (sd.getUserId() != userId) {
                return new BaseResult(ML.getMLStr("shipdesign_err_doesnotbelongtoyou", userId), true);
            }

            if (newName != null) {
                if ((newName.length() < 5) || (newName.length() > 30)) {
                    return new BaseResult(ML.getMLStr("shipdesign_err_nametooshort", userId), true);
                } else {
                    sd.setName(newName);
                    sdDAO.update(sd);
                }
            } else {
                return new BaseResult(ML.getMLStr("shipdesign_err_noname", userId), true);
            }
        }

        return null;
    }

    public static BaseResult deleteDesign(int userId, int designId) {
        ShipDesign sd = sdDAO.findById(designId);

        if (sd.getUserId() != userId) {
            return new BaseResult(ML.getMLStr("shipdesign_err_doesnotbelongtoyou", userId), true);
        }

        if (sd == null) {
            return new BaseResult(ML.getMLStr("shipdesign_err_shipdesignunknown", userId), true);
        } else {
            // Check for ships in Fleets
            if (isBuilt(userId, designId)) {
                return new BaseResult(ML.getMLStr("shipdesign_err_shipexistsfordesign", userId), true);
            }
            // Check for ships in Production or Scraporders
            if (isInProduction(userId, designId)) {
                return new BaseResult("There exist ships in a productionorder for this design!", true);
            }

            QueryKeySet qks = new QueryKeySet();
            qks.addKey(new QueryKey("designId", sd.getId()));

            dmDAO.removeAll(qks);
            sdDAO.remove(sd);
        }

        return null;
    }

    public static BaseResult createDesign(int userId) {
        ShipDesign sd = new ShipDesign();
        sd.setName(ML.getMLStr("shipdesign_lbl_newdesign", userId));
        sd.setUserId(userId);
        sd.setChassis(1);
        sd.setDesignTime(System.currentTimeMillis());
        sd.setType(EShipType.BATTLE);
        sd.setBuildable(false);
        if (sdDAO.add(sd) == null) {
            return new BaseResult(ML.getMLStr("shipdesign_err_notabletocreatedesign", userId), true);
        }

        return null;
    }

    public static BaseResult copyDesign(int userId, int designId) {
        ShipDesign sd = sdDAO.findById(designId);
        
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        
        try {
            th.startTransaction();
            
            if (sd == null) {
                return new BaseResult(ML.getMLStr("shipdesign_err_noshipfound", userId), true);
            } else {
                ShipDesign sdNew = sd.clone();
                sdNew.setId(null);

                String newName = "Copy_" + sd.getName();
                if (newName.length() > 30) newName = newName.substring(0, 29);

                sdNew.setName(newName);
                sdNew.setDesignTime(System.currentTimeMillis());
                sdNew = sdDAO.add(sdNew);

                ArrayList<DesignModule> mOld = dmDAO.findByDesignId(sd.getId());

                for (DesignModule dm : mOld) {
                    dm.setDesignId(sdNew.getId());
                }
                dmDAO.insertAll(mOld);

                ArrayList<RessourceCost> rOld = rcDAO.find(ERessourceCostType.SHIP, sd.getId());
                for (RessourceCost rc : rOld) {
                    rc.setId(sdNew.getId());
                }
                rcDAO.insertAll(rOld);
            }
            
            th.execute();
        } catch (Exception e) {
            DebugBuffer.error("Error for CopyDesign: " + e);
            try {
                th.rollback();
            } catch (Exception e2) {
                DebugBuffer.error("Rollback failed for CopyDesign: " + e2);
            }
        } finally {
            th.endTransaction();
        }

        return null;
    }

    public static ShipDesign getShipDesign(int designId) {
        ShipDesign sd = sdDAO.findById(designId);

        return sd;
    }

    public static Chassis getChassis(int chassisId) {
        Chassis c = cDAO.findById(chassisId);

        if (c == null) {
            c = new Chassis();
            c.setId(999);
            c.setName("Unknown");
            c.setImg_design("");
        }

        return c;
    }

    public static String typeIdToName(EShipType type) {
        String result = "Unknown";

        if (type == EShipType.BATTLE) {
            result = "Kampfschiff";
        } else if (type == EShipType.TRANSPORT) {
            result = "Transporter";
        } else if (type == EShipType.CARRIER) {
            result = "Tr�ger";
        } else if (type == EShipType.COLONYSHIP) {
            result = "Kolonieschiff";
        } else if (type == EShipType.SPACEBASE) {
            result = "Raumbasis";
        } else {
            result = "Unbekannt";
        }

        return result;
    }

    public static String typeIdToNameML(EShipType type, int userId) {
        String result = "Unknown";

        if (type == EShipType.BATTLE) {
            result = ML.getMLStr("shipdesign_EShipType_BATTLE", userId);
        } else if (type == EShipType.TRANSPORT) {
            result = ML.getMLStr("shipdesign_EShipType_TRANSPORT", userId);
        } else if (type == EShipType.CARRIER) {
            result = ML.getMLStr("shipdesign_EShipType_CARRIER", userId);
        } else if (type == EShipType.COLONYSHIP) {
            result = ML.getMLStr("shipdesign_EShipType_COLONYSHIP", userId);
        } else if (type == EShipType.SPACEBASE) {
            result = ML.getMLStr("shipdesign_EShipType_SPACEBASE", userId);
        } else {
            result = "Unbekannt";
        }

        return result;
    }

    // Updates designs and ignores if the ship is already built
    // Internal use only for refreshing ship values
    public static void updateDesignCosts(int designId) {
        ShipDesignResult sdr = null;

        try {
            boolean hasColModule = false;
            boolean hasTransportModule = false;

            // Load all values from form
            HashMap<Integer, Integer> modules = new HashMap<Integer, Integer>();

            ShipDesign toUpdate = sdDAO.findById(designId);
            Chassis c = cDAO.findById(toUpdate.getChassis());
            EShipType shipTypeEnum = toUpdate.getType();
            ShipTypeAdjustment sta = staDAO.findByType(shipTypeEnum);

            modules.put(c.getRelModuleId(), 1);

            ArrayList<DesignModule> modList = dmDAO.findByDesignId(designId);
            for (DesignModule dm : modList) {
                modules.put(dm.getModuleId(), dm.getCount());
            }

            // All values are loaded
            // Sum up module values and determine attributes of ship
            boolean hasEngine = false;
            boolean hasComputer = false;

            int energyNeedBase = 0;
            int energyNeedAdd = 0;
            int totalEnergy = 0;

            int availSpace = 0;
            int usedSpace = 0;

            int ressSpace = 0;
            int popSpace = 0;

            int smallHangar = 0;
            int mediumHangar = 0;
            int largeHangar = 0;

            float hyperSpeed = 0f;
            float standardSpeed = 0f;

            MutableRessourcesEntry mre = new MutableRessourcesEntry();

            AttributeTree at = new AttributeTree(toUpdate.getUserId(), toUpdate.getDesignTime());
            for (Map.Entry<Integer, Integer> moduleEntry : modules.entrySet()) {
                int moduleId = moduleEntry.getKey();
                int moduleCount = moduleEntry.getValue();

                Module m = mDAO.findById(moduleId);
                ModuleAttributeResult mar = at.getAllAttributes(toUpdate.getChassis(), moduleId);

                if (m == null) {
                    continue;
                }
                if (m.getType() == ModuleType.ENGINE) {
                    if (mar.getAttributeValue(EAttribute.SPEED_ACCELERATION) != 0d) {
                        standardSpeed = (float) mar.getAttributeValue(EAttribute.SPEED_ACCELERATION);
                        hasEngine = true;
                    }

                    if (mar.getAttributeValue(EAttribute.SPEED_HYPER) != 0d) {
                        hyperSpeed = Math.max(hyperSpeed, (float) mar.getAttributeValue(EAttribute.SPEED_HYPER));
                    }
                } else if (m.getType() == ModuleType.COMPUTER) {
                    hasComputer = true;
                }

                if ((m.getType() == ModuleType.COMPUTER) || (m.getType() == ModuleType.SPECIAL) || (m.getType() == ModuleType.TRANSPORT_POPULATION) || (m.getType() == ModuleType.TRANSPORT_RESSOURCE) || (m.getType() == ModuleType.ENGINE)) {
                    if ((m.getType() == ModuleType.TRANSPORT_POPULATION) || (m.getType() == ModuleType.TRANSPORT_RESSOURCE)) {
                        hasTransportModule = true;
                        if (m.getType() == ModuleType.TRANSPORT_POPULATION) {
                            popSpace += (int) (mar.getAttributeValue(EAttribute.TROOP_STORAGE) * moduleCount);
                        } else if (m.getType() == ModuleType.TRANSPORT_RESSOURCE) {
                            ressSpace += (int) (mar.getAttributeValue(EAttribute.RESS_STORAGE) * moduleCount);
                        }
                    }
                    energyNeedBase += (int) (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * moduleCount);
                } else {
                    energyNeedAdd += (int) (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * moduleCount);
                }

                if (m.getType() == ModuleType.REACTOR) {
                    totalEnergy += (int) (mar.getAttributeValue(EAttribute.ENERGY_PRODUCTION) * moduleCount);
                }

                if (m.getType() == ModuleType.CHASSIS) {
                    availSpace += (int) (mar.getAttributeValue(EAttribute.SPACE_PROVIDING) * moduleCount);
                } else {
                    usedSpace += (int) (mar.getAttributeValue(EAttribute.SPACE_CONSUMPTION) * moduleCount);
                }

                if (m.getType() == ModuleType.COLONISATION) {
                    hasColModule = true;
                }

                if (m.getType() == ModuleType.HANGAR) {
                    if (m.getId() == 1030) {
                        smallHangar += (int) (mar.getAttributeValue(EAttribute.HANGAR_CAPACITY) * moduleCount);
                    } else if (m.getId() == 1031) {
                        mediumHangar += (int) (mar.getAttributeValue(EAttribute.HANGAR_CAPACITY) * moduleCount);
                    } else if (m.getId() == 1032) {
                        largeHangar += (int) (mar.getAttributeValue(EAttribute.HANGAR_CAPACITY) * moduleCount);
                    }
                }

                RessourcesEntry re = new RessourcesEntry(at.getRessourceCost(c.getId(), moduleId));


                mre.addRess(re, moduleCount * c.getMinBuildQty());
            }

            boolean success = true;

            if (usedSpace > availSpace) {
                success = false;
            }
            if (energyNeedBase > totalEnergy) {
                success = false;
            }

           if (!hasEngine && (shipTypeEnum != EShipType.SPACEBASE)) {
                success = false;
          }

            /*
            if (!hasComputer) {
            errors.add("Raumschiff besitzt kein Computersystem");
            success = false;
            }
             */

            if (shipTypeEnum == EShipType.TRANSPORT) {
                if (!hasTransportModule) {
                } else {
                    ressSpace *= sta.getRessFactor();
                    popSpace *= sta.getTroopFactor();
                }
            } else if (shipTypeEnum == EShipType.CARRIER) {
                smallHangar *= sta.getHangarFactor();
            }

            // Cleanup shipmodules and rebuild
            dmDAO.deleteDesign(designId);
            modules.remove(c.getRelModuleId());
            dmDAO.addModulesForDesign(designId, modules);

            ShipDesign sd = sdDAO.findById(designId);
            sd.setChassis(c.getId());
            sd.setHyperSpeed(hyperSpeed);
            sd.setStandardSpeed(standardSpeed);
            sd.setIsConstruct(false);
            sd.setType(shipTypeEnum);
            sd.setRessSpace(ressSpace);
            sd.setTroopSpace(popSpace);
            sd.setSmallHangar(smallHangar);
            sd.setMediumHangar(mediumHangar);
            sd.setLargeHangar(largeHangar);
            sd.setDesignTime(System.currentTimeMillis());

            if (success) {
            sd.setBuildable(true);
            } else {
            sd.setBuildable(false);
            }
            sdDAO.update(sd);

            // Add ressourcecost
            QueryKeySet qks = new QueryKeySet();
            qks.addKey(new QueryKey("type", ERessourceCostType.SHIP));
            qks.addKey(new QueryKey("id", sd.getId()));
            rcDAO.removeAll(qks);

            ArrayList<RessourceCost> cost = new ArrayList<RessourceCost>();
            for (RessAmountEntry rae : mre.getRessArray()) {
            RessourceCost rc = new RessourceCost();


            rc.setId(sd.getId());
            rc.setRessId(rae.getRessId());
            rc.setType(ERessourceCostType.SHIP);

            if (rae.getRessId() == Ressource.CREDITS) {
            rc.setQty((long) ((double) rae.getQty() * (double) sta.getCostFactor()));
            } else {
            rc.setQty((long) rae.getQty());
            }

            cost.add(rc);
            }

            rcDAO.insertAll(cost);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("ShipDesign save: ", e);
        }
    }

    private static State fixDesignModules(int designId, int spaceBalance, int energyBalance) {
        if (spaceBalance < 0) {
            Logger.getLogger().write("Ship " + designId + " uses more space than possible (" + (-spaceBalance) + ")");
        }
        /*
        if (energyBalance < 0) {
            Logger.getLogger().write("Ship " + designId + " has too few energy (" + (-energyBalance) + ")");
        }
         */

        // If there is too few space .. try to reduce weapon count
        // Loop all designs and check for modules which has to be adjusted
        ShipDesign sd = sdDAO.findById(designId);
        Chassis c = cDAO.findById(sd.getChassis());
        ArrayList<DesignModule> dmList = dmDAO.findByDesignId(sd.getId());

        for (DesignModule dm : dmList) {
            Module m = mDAO.findById(dm.getModuleId());
            ArrayList<ModuleAttribute> entries = maDAO.findBy(m.getId(), c.getId());

            if (m.getType() == ModuleType.WEAPON) {
                Logger.getLogger().write("Design " + sd.getName() + " [" + c.getName() + "] {" + sd.getUserId() + "} needs adjustment for weapon " + m.getName());

                int spaceConsumption = 0;
                for (ModuleAttribute ma : entries) {
                    at.viswars.model.Attribute a = attDAO.findById(ma.getAttributeId());
                    if (a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.SPACE_CONSUMPTION.name())) {
                        spaceConsumption = (int)Math.round(ma.getValue());
                    }
                }

                Logger.getLogger().write("Low space " + spaceBalance + " current consumption " + spaceConsumption);
                int redCount = (int)Math.ceil((double)-spaceBalance / (double)spaceConsumption);
                if (redCount >= dm.getCount()) redCount = dm.getCount() - 1;
                int newCount = dm.getCount() - redCount;
                Logger.getLogger().write("Reduce count of " + m.getName() + " from "+dm.getCount()+" to " + newCount + " (Freeing: " + (spaceConsumption * redCount) + ")");
                dm.setCount(newCount);
                dmDAO.update(dm);

                if ((spaceBalance + (redCount * spaceConsumption)) >= 0) {
                    spaceBalance += redCount * spaceConsumption;
                    Logger.getLogger().write("Problem solved");
                    Logger.getLogger().write("New space balance = " + spaceBalance);
                    return State.SUCCESS;
                } else {
                    spaceBalance += redCount * spaceConsumption;
                    Logger.getLogger().write("Further adjustment necessary");
                }

                Logger.getLogger().write("New space balance = " + spaceBalance);
            }

            // All weapon system couldnt satisfy need for space
            Logger.getLogger().write("Weapons couldn't satisfy need for space");
        }

        if (spaceBalance > 0) return State.SUCCESS;

        // If new weapon count is zero, try to reduce shield power
        for (DesignModule dm : dmList) {
            Module m = mDAO.findById(dm.getModuleId());
            ArrayList<ModuleAttribute> entries = maDAO.findBy(m.getId(), c.getId());

            if (m.getType() == ModuleType.SHIELD) {
                Logger.getLogger().write("Design " + sd.getName() + " [" + c.getName() + "] {" + sd.getUserId() + "} needs adjustment for shield " + m.getName());

                int spaceConsumption = 0;
                for (ModuleAttribute ma : entries) {
                    at.viswars.model.Attribute a = attDAO.findById(ma.getAttributeId());
                    if (a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.SPACE_CONSUMPTION.name())) {
                        spaceConsumption = (int)Math.round(ma.getValue());
                    }
                }

                int redCount = (int)Math.ceil((double)-spaceBalance / (double)spaceConsumption);
                if (redCount >= dm.getCount()) redCount = dm.getCount() - 1;
                int newCount = dm.getCount() - redCount;
                Logger.getLogger().write("Reduce count of " + m.getName() + " to " + newCount);

                dm.setCount(newCount);
                dmDAO.update(dm);

                if ((spaceBalance + (redCount * spaceConsumption)) >= 0) {
                    spaceBalance += redCount * spaceConsumption;
                    Logger.getLogger().write("Problem solved");
                    Logger.getLogger().write("New space balance = " + spaceBalance);
                    return State.SUCCESS;
                } else {
                    spaceBalance += redCount * spaceConsumption;
                    Logger.getLogger().write("Further adjustment necessary");
                }

                Logger.getLogger().write("New space balance = " + spaceBalance);
            }

            // All weapon system couldnt satisfy need for space
            Logger.getLogger().write("Ship couldnt be balanced");
            return State.FAIL;
        }

        return State.FAIL;
    }

    public static ShipDesignResult storeDesign(int userId, int designId, Map params) {
        ShipDesignResult sdr = null;
        ArrayList<String> errors = new ArrayList<String>();

        try {
            if (ShipDesignService.isBuilt(userId, designId)) {
                errors.add(ML.getMLStr("shipdesign_err_alreadybuilt", userId));
                return new ShipDesignResult(false, errors);
            }
            if (ShipDesignService.isInProduction(userId, designId)) {
                errors.add(ML.getMLStr("shipdesign_err_designinproduction", userId));
                return new ShipDesignResult(false, errors);
            }


            // Load all values from form
            HashMap<Integer, Integer> modules = new HashMap<Integer, Integer>();
            int chassisId = 0;
            int shipType = 0;

            shipType = Integer.parseInt(((String[]) params.get("shiptype"))[0]);
            boolean hasColModule = false;
            boolean hasTransportModule = false;
            boolean hasHangar = false;

            EShipType shipTypeEnum = EShipType.BATTLE;
            switch (shipType) {
                case (0):
                    shipTypeEnum = EShipType.BATTLE;
                    break;
                case (1):
                    shipTypeEnum = EShipType.TRANSPORT;
                    break;
                case (2):
                    shipTypeEnum = EShipType.CARRIER;
                    break;
                case (3):
                    shipTypeEnum = EShipType.COLONYSHIP;
                    break;
                case (4):
                    shipTypeEnum = EShipType.SPACEBASE;
                    break;
            }

            chassisId = Integer.parseInt(((String[]) params.get("cId"))[0]);
            Chassis c = cDAO.findById(chassisId);
            ShipTypeAdjustment sta = staDAO.findByType(shipTypeEnum);

            if ((shipTypeEnum != EShipType.SPACEBASE) && (c.isStarbase())) {
                shipTypeEnum = EShipType.SPACEBASE;
            }

            if ((shipTypeEnum == EShipType.SPACEBASE) && (!c.isStarbase())) {
                shipTypeEnum = EShipType.BATTLE;
                errors.add(ML.getMLStr("shipdesign_err_invalidtypebattleship", userId));
            }

            modules.put(c.getRelModuleId(), 1);

            for (Object entry : params.entrySet()) {
                Map.Entry me = (Map.Entry) entry;
                String key = (String) me.getKey();


                if (key.contains("_count")) {
                    String valueStr = ((String[]) me.getValue())[0];
                    if (valueStr.equalsIgnoreCase("")) {
                        valueStr = "0";
                    }

                    int count = 0;
                    int modId = 0;

                    try {
                        count = Integer.parseInt(valueStr);
                        if (count < 0) {
                            errors.add(ML.getMLStr("invalid_parameter", userId));
                            return new ShipDesignResult(false, errors);
                        }
                        modId = Integer.parseInt(((String[]) params.get(key.replace("_count", "")))[0]);
                    } catch (NumberFormatException nfe) {
                        errors.add(ML.getMLStr("invalid_parameter", userId));
                        return new ShipDesignResult(false, errors);
                    }

                    Logger.getLogger().write("Looping parameter " + key + " moduleId " + modId + " count " + count);

                    if (modId < 1) {
                        continue;
                    }

                    Module m = mDAO.findById(modId);

                    if (modules.containsKey(modId)) {
                        if (key.contains("engines") || m.getUniquePerShip()) {
                            continue;
                        }
                        modules.put(modId, modules.get(modId) + count);
                        // Logger.getLogger().write("Set count for " + modId + ": " + (modules.get(modId) + count));
                    } else {
                        if (m.getUniquePerShip()) count = 1;
                        modules.put(modId, count);
                        // Logger.getLogger().write("Set count for " + modId + ": " + count);
                    }
                }
            }

            // Check if a module is used where precondition is not satisfied            
            // TODO implement into module attributes
            boolean success = true;
            
            for (Map.Entry<Integer,Integer> modEntry : modules.entrySet()) {
                if ((modEntry.getKey() == 82) && !modules.keySet().contains(84)) {
                    String moduleName1 = ML.getMLStr(mDAO.findById(82).getName(),userId);
                    String moduleName2 = ML.getMLStr(mDAO.findById(84).getName(),userId);
                    
                    String error = ML.getMLStr("shipdesign_err_modulerequires", userId, moduleName1, moduleName2);
                    
                    errors.add(error);
                    success = false;
                    break;
                }
            }
            
            // All values are loaded
            // Sum up module values and determine attributes of ship
            boolean hasEngine = false;
            boolean hasComputer = false;

            int energyNeedBase = 0;
            int energyNeedAdd = 0;
            int totalEnergy = 0;

            int availSpace = 0;
            int usedSpace = 0;

            int ressSpace = 0;
            int popSpace = 0;

            int smallHangar = 0;
            int mediumHangar = 0;
            int largeHangar = 0;

            float hyperSpeed = 0f;
            float standardSpeed = 0f;

            MutableRessourcesEntry mre = new MutableRessourcesEntry();

            AttributeTree at = new AttributeTree(userId, System.currentTimeMillis());
            for (Map.Entry<Integer, Integer> moduleEntry : modules.entrySet()) {
                int moduleId = moduleEntry.getKey();
                int moduleCount = moduleEntry.getValue();

                Module m = mDAO.findById(moduleId);
                ModuleAttributeResult mar = at.getAllAttributes(chassisId, moduleId);

                if (m == null) {
                    continue;
                }
                if (m.getType() == ModuleType.ENGINE) {
                    if (mar.getAttributeValue(EAttribute.SPEED_ACCELERATION) != 0d) {
                        standardSpeed = (float) mar.getAttributeValue(EAttribute.SPEED_ACCELERATION);
                        hasEngine = true;
                    }

                    if (mar.getAttributeValue(EAttribute.SPEED_HYPER) != 0d) {
                        hyperSpeed = Math.max(hyperSpeed, (float) mar.getAttributeValue(EAttribute.SPEED_HYPER));
                    }
                } else if (m.getType() == ModuleType.COMPUTER) {
                    hasComputer = true;
                }

                if ((m.getType() == ModuleType.COMPUTER) || (m.getType() == ModuleType.COLONISATION) || (m.getType() == ModuleType.SPECIAL) || (m.getType() == ModuleType.TRANSPORT_POPULATION) || (m.getType() == ModuleType.TRANSPORT_RESSOURCE) || (m.getType() == ModuleType.ENGINE)) {
                    if ((m.getType() == ModuleType.TRANSPORT_POPULATION) || (m.getType() == ModuleType.TRANSPORT_RESSOURCE)) {
                        hasTransportModule = true;
                        if (m.getType() == ModuleType.TRANSPORT_POPULATION) {
                            popSpace += (int) (mar.getAttributeValue(EAttribute.TROOP_STORAGE) * moduleCount);
                        } else if (m.getType() == ModuleType.TRANSPORT_RESSOURCE) {
                            ressSpace += (int) (mar.getAttributeValue(EAttribute.RESS_STORAGE) * moduleCount);
                        }
                    }
                    energyNeedBase += (int) (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * moduleCount);
                } else {
                    energyNeedAdd += (int) (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * moduleCount);
                }

                if (m.getType() == ModuleType.REACTOR) {
                    totalEnergy += (int) (mar.getAttributeValue(EAttribute.ENERGY_PRODUCTION) * moduleCount);
                }

                if (m.getType() == ModuleType.CHASSIS) {
                    availSpace += (int) (mar.getAttributeValue(EAttribute.SPACE_PROVIDING) * moduleCount);
                } else {
                    usedSpace += (int) (mar.getAttributeValue(EAttribute.SPACE_CONSUMPTION) * moduleCount);
                }


                if (m.getType() == ModuleType.COLONISATION) {
                    hasColModule = true;
                }

                if (m.getType() == ModuleType.HANGAR) {
                    if (m.getId() == 1030) {
                        smallHangar += (int) (mar.getAttributeValue(EAttribute.HANGAR_CAPACITY) * moduleCount);
                    } else if (m.getId() == 1031) {
                        mediumHangar += (int) (mar.getAttributeValue(EAttribute.HANGAR_CAPACITY) * moduleCount);
                    } else if (m.getId() == 1032) {
                        largeHangar += (int) (mar.getAttributeValue(EAttribute.HANGAR_CAPACITY) * moduleCount);
                    }

                    hasHangar = true;
                }

                RessourcesEntry re = new RessourcesEntry(at.getRessourceCost(c.getId(), moduleId));


                mre.addRess(re, moduleCount * c.getMinBuildQty());
            }            

            if (usedSpace > availSpace) {
                errors.add(ML.getMLStr("shipdesign_err_notenoughspace", userId));
                success = false;
            }
            if (energyNeedBase > totalEnergy) {
                errors.add(ML.getMLStr("shipdesign_err_notenoughenergy", userId));
                success = false;
            } else if ((energyNeedBase + energyNeedAdd) > totalEnergy) {
                errors.add(ML.getMLStr("shipdesign_err_fewenergy", userId) + "!");
            }

            if (!hasEngine && (shipTypeEnum != EShipType.SPACEBASE)) {
                errors.add(ML.getMLStr("shipdesign_err_noengine", userId));
                success = false;
            }

            if (hasHangar && (shipTypeEnum != EShipType.BATTLE && shipTypeEnum != EShipType.CARRIER)) {
                errors.add(ML.getMLStr("shipdesign_err_hangarnotallowed", userId));
                success = false;
            }

            /*
            if (!hasComputer) {
            errors.add("Raumschiff besitzt kein Computersystem");
            success = false;
            }
             */

            if (shipTypeEnum != EShipType.COLONYSHIP) {
                if (hasColModule) {
                    errors.add(ML.getMLStr("shipdesign_err_invalidtypehastobecolony", userId));
                    shipTypeEnum = EShipType.COLONYSHIP;
                }
            }

            if (shipTypeEnum == EShipType.COLONYSHIP) {
                if (!hasColModule) {
                    errors.add(ML.getMLStr("shipdesign_err_invalidtypenocolonymodule", userId));
                    shipTypeEnum = EShipType.BATTLE;
                }
            } else if (shipTypeEnum == EShipType.TRANSPORT) {
                if (!hasTransportModule) {
                    errors.add(ML.getMLStr("shipdesign_err_invalidtypenotransportmodule", userId));
                } else {
                    ressSpace *= sta.getRessFactor();
                    popSpace *= sta.getTroopFactor();
                }
            } else if (shipTypeEnum == EShipType.CARRIER) {
                smallHangar *= sta.getHangarFactor();
            }

            if ((shipTypeEnum != EShipType.TRANSPORT) && hasTransportModule) {
                errors.add(ML.getMLStr("shipdesign_err_nottransporttypebutmodules", userId));
            }

            // Cleanup shipmodules and rebuild
            dmDAO.deleteDesign(designId);
            modules.remove(c.getRelModuleId());
            dmDAO.addModulesForDesign(designId, modules);

            ShipDesign sd = sdDAO.findById(designId);
            sd.setChassis(c.getId());
            sd.setHyperSpeed(hyperSpeed);
            sd.setStandardSpeed(standardSpeed);
            sd.setIsConstruct(false);
            sd.setType(shipTypeEnum);
            sd.setRessSpace(ressSpace);
            sd.setTroopSpace(popSpace);
            sd.setSmallHangar(smallHangar);
            sd.setMediumHangar(mediumHangar);
            sd.setLargeHangar(largeHangar);
            sd.setDesignTime(System.currentTimeMillis());

            if (success) {
                sd.setBuildable(true);
            } else {
                sd.setBuildable(false);
            }
            sdDAO.update(sd);

            /*
             * TASKS/TITLE - START
             */
            if (c.getId() == Chassis.ID_FIGHTER) {
                TitleUtilities.incrementCondition(ConditionToTitle.TINKERER_FIGHTERDESIGN, userId);
                
            }
            if (c.getId() == Chassis.ID_FRIGATE) {
                TitleUtilities.incrementCondition(ConditionToTitle.EXPLORER_FRIGATEDESIGN, userId);
            }
            if (c.getId() == Chassis.ID_FRIGATE && shipTypeEnum == EShipType.COLONYSHIP) {
                TitleUtilities.incrementCondition(ConditionToTitle.COLONISATOR_COLONYSHIP, userId);
            }
            /*
             * TASKS/TITLE - END
             */

            // Add ressourcecost
            QueryKeySet qks = new QueryKeySet();
            qks.addKey(new QueryKey("type", ERessourceCostType.SHIP));
            qks.addKey(new QueryKey("id", sd.getId()));
            rcDAO.removeAll(qks);

            ArrayList<RessourceCost> cost = new ArrayList<RessourceCost>();
            for (RessAmountEntry rae : mre.getRessArray()) {
                RessourceCost rc = new RessourceCost();


                rc.setId(sd.getId());
                rc.setRessId(rae.getRessId());
                rc.setType(ERessourceCostType.SHIP);

                if (rae.getRessId() == Ressource.CREDITS) {
                    rc.setQty((long) ((double) rae.getQty() * (double) sta.getCostFactor()));
                } else {
                    rc.setQty((long) rae.getQty());
                }

                cost.add(rc);
            }

            rcDAO.insertAll(cost);

            sdr = new ShipDesignResult(success, errors);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("ShipDesign save: ", e);
            errors.add("Interner Fehler");
            return new ShipDesignResult(false, errors);
        }
        return sdr;
    }

    public static BaseResult scrapShip(int userId, ShipScrapBuffer ssb) {
        ShipModifyView smv = ShipUtilities.getModifiableShips(ssb.getFleetId());

        boolean found = false;
        for (ShipModifyEntry sme : smv.getShipList()) {
            if (sme.getDesignId() == ssb.getDesignId()) {
                if (sme.getCount() >= ssb.getCount()) {
                    found = true;
                }
            }
        }

        if (!found || (ssb.getCount() < 0)) {
            return new BaseResult(ML.getMLStr("shipdesign_err_shipcannotbescrapped", userId), true);
        }

        ShipDesignExt sdEntry = new ShipDesignExt(ssb.getDesignId());
        ShipScrapOrder sco = new ShipScrapOrder(sdEntry, 0, userId, ssb.getCount(), ssb.getFleetId());

        OrderResult oRes = OrderHandling.scrapOrder(sco);

        ArrayList<String> errorList = new ArrayList<String>();
        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        StringBuffer errors = new StringBuffer();
        for (String error : errorList) {
            errors.append(error + "<BR>");
        }

        if (errors.length() == 0) {
            return new BaseResult(false);
        }
        return new BaseResult(errors.toString(), true);
    }

    public static BaseResult upgradeShip(int userId, UpgradeShipBuffer usb) {
        ShipModifyView smv = ShipUtilities.getModifiableShips(usb.getFleetId());

        ShipDesignExt sdEntry = new ShipDesignExt(usb.getDesignId());
        ShipDesignExt sdEntryNew = new ShipDesignExt(usb.getToDesignId());
        ShipDesign sd = (ShipDesign) sdEntry.getBase();
        ShipDesign sdNew = (ShipDesign) sdEntryNew.getBase();

        DebugBuffer.addLine(DebugLevel.TRACE, "UserId is " + userId);

        if (!ShipUtilities.canBeUpgraded(sd, sdNew, userId, usb.getFleetId(), usb.getCount())) {
            return new BaseResult(ML.getMLStr("shipdesign_err_cantbemodified", userId), true);
        }

        ShipUpgradeOrder suo = new ShipUpgradeOrder(sdEntry, sdEntryNew, usb.getCount(), usb.getFleetId());
        suo.setUserId(userId);

        if (usb.getCount() <= 0) {
            return new BaseResult("Invalid ship count",true);
        }
        OrderResult oRes = OrderHandling.upgradeOrder(suo);

        ArrayList<String> errorList = new ArrayList<String>();
        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        StringBuffer errors = new StringBuffer();
        for (String error : errorList) {
            errors.append(error + "<BR>");
        }

        if (errors.length() == 0) {
            return new BaseResult(false);
        }
        return new BaseResult(errors.toString(), true);
    }

    public static BaseResult repairShips(int userId, RepairShipBuffer rsb) {
        BaseResult check = ShipUtilities.canBeRepaired(userId, rsb.getShipFleetId(), rsb.getCount());

        if (check.isError()) {
            return new BaseResult(ML.getMLStr("shipdesign_err_cantbemodified", userId) + " ("+check.getMessage()+")", true);
        }

        for (Iterator<Map.Entry<EDamageLevel,Integer>> it = rsb.getCount().entrySet().iterator();it.hasNext();) {
            Map.Entry<EDamageLevel,Integer> entry = it.next();
            if (entry.getValue() <= 0) it.remove();
        }
        
        if (rsb.getCount().isEmpty()) {
            return new BaseResult("Nothing to do",true);
        }
        
        ShipDesignExt sde = new ShipDesignExt(rsb.getDesignId());
        Logger.getLogger().write("RSB FleetId = " + rsb.getFleetId());
        PlayerFleetExt pfExt = new PlayerFleetExt(rsb.getFleetId());

        ShipRepairOrder sro = new ShipRepairOrder(sde, pfExt.getRelativeCoordinate().getPlanetId(), userId, rsb.getShipFleetId(), rsb.getCount());   
        OrderResult oRes = OrderHandling.repairOrder(sro);

        ArrayList<String> errorList = new ArrayList<String>();
        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        StringBuffer errors = new StringBuffer();
        for (String error : errorList) {
            errors.append(error + "<BR>");
        }

        if (errors.length() == 0) {
            return new BaseResult(false);
        }

        return new BaseResult(errors.toString(), true);
    }
}
