/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.service;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.ViewTableDAO;
import at.viswars.model.ViewTable;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class FlightService {
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);

    public static boolean hasViewTableEntryFor(int userId, int systemId, int planetId) {
        ArrayList<ViewTable> vtEntries = vtDAO.findByUserAndSystem(userId, systemId);

        return (vtEntries.size() > 0);
    }
}
