/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.enumeration.EOwner;
import at.viswars.enumeration.ESystemSorting;
import at.viswars.GameConfig;
import at.viswars.GameConstants;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.PlanetExt;
import at.viswars.StarMapInfoData;
import at.viswars.SystemExt;
import at.viswars.dao.AllianceDAO;
import at.viswars.dao.AllianceMemberDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DiplomacyRelationDAO;
import at.viswars.dao.GalaxyDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlanetLogDAO;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.SystemDAO;
import at.viswars.dao.UserDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.dao.ViewTableDAO;
import at.viswars.model.Alliance;
import at.viswars.model.AllianceMember;
import at.viswars.model.Galaxy;
import at.viswars.model.Planet;
import at.viswars.model.PlanetLog;
import at.viswars.model.PlayerFleet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.User;
import at.viswars.model.UserData;
import at.viswars.model.ViewTable;
import at.viswars.result.SystemSearchResult;
import at.viswars.scanner.StarMapQuadTree;
import at.viswars.scanner.SystemDesc;
import at.viswars.utilities.AllianceUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @TODO Implementieren des Noobschutzes
 */
public class SystemService {

    private static SystemDAO systemDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO planetDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO playerPlanetDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static UserDAO userDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static UserDataDAO userDataDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static AllianceMemberDAO allianceMemberDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static AllianceDAO allianceDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static ViewTableDAO viewTableDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static DiplomacyRelationDAO diplomacyRelationDAO = (DiplomacyRelationDAO) DAOFactory.get(DiplomacyRelationDAO.class);
    private static PlayerFleetDAO playerFleetDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static PlanetLogDAO planetLogDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private static GalaxyDAO galaxyDAO = (GalaxyDAO) DAOFactory.get(GalaxyDAO.class);

    public static HashMap<Integer, Planet> findPlanetsBySystemId(int systemId) {
        HashMap<Integer, Planet> planets = new HashMap<Integer, Planet>();
        for (Planet p : planetDAO.findBySystemId(systemId)) {
            planets.put(p.getOrbitLevel(), p);
        }
        return planets;
    }

    @Deprecated
    public static boolean isSystemInViewTable(int userId, int systemId) {
        return viewTableDAO.isSystemInViewtable(userId, systemId);
    }

    public static String getGalaxy(int userId) {
        // Get lowest Galaxy Id so we dont have weird numbers
        int lowestId = 999999;
        ArrayList<Galaxy> gList = (ArrayList<Galaxy>)galaxyDAO.findAll();
        for (Galaxy g : gList) {
            if (g.getId() < lowestId) {
                lowestId = g.getId();
            }
        }

        lowestId -= 1;

        PlayerPlanet pp = playerPlanetDAO.findHomePlanetByUserId(userId);
        if (pp == null) {
            // Get random planet
            try {
                pp = playerPlanetDAO.findByUserId(userId).get(0);
            } catch (Exception e) {
                return "?";
            }
        }

        Planet p = planetDAO.findById(pp.getPlanetId());
        at.viswars.model.System s = systemDAO.findById(p.getSystemId());

        for (Galaxy g : (ArrayList<Galaxy>)galaxyDAO.findAll()) {
            if ((s.getId() >= g.getStartSystem()) && (s.getId() <= g.getEndSystem())) {
                return "" + (g.getId() - lowestId);
            }
        }

        return "?";
    }

    public static void addToAlliance() {
        int allianceId = 12;

        for (UserData ud : (ArrayList<UserData>) userDataDAO.findAll()) {

            if (allianceMemberDAO.findByUserId(ud.getUserId()) == null) {
                AllianceService.addUserToAlliance(allianceId, ud.getUserId());

                AllianceService.setUserRank(ud.getUserId(), 3, false);
            }
        }
        AllianceService.createTreaty(allianceId, 3);
        for (Alliance a : (ArrayList<Alliance>) allianceDAO.findAll()) {
            if (a.getIsSubAllianceOf() == 0 && a.getId() != 3) {
                AllianceService.createTreaty(a.getId(), allianceId);
            }
        }
    }

    // TODO 
    public static SystemSearchResult findSystemsByRange(int fromSystemId, int range, int reqUser) {
        try {
            ArrayList<at.viswars.model.System> result = getSystemsAround(fromSystemId, (double) range, ESystemSorting.SORT_BY_DISTANCE);
            at.viswars.model.System refSys = systemDAO.findById(fromSystemId);

            if (result.size() == 0) {
                return new SystemSearchResult("Keine Systeme gefunden");
            } else {
                HashMap<Integer, HashMap<Integer, ViewTable>> vtMap = viewTableDAO.findByUserCategorized(reqUser);
                vtMap = icludePlanetLogData(reqUser, vtMap);
                ArrayList<SystemExt> sysExtList = new ArrayList<SystemExt>();

                for (Iterator<at.viswars.model.System> sIt = result.iterator();sIt.hasNext();) {
                    at.viswars.model.System sys = sIt.next();
                    if (!vtMap.containsKey(sys.getId())) {
                        sys.setName("System " + sys.getId());
                    }

                    SystemExt se = new SystemExt(sys, refSys);
                    sysExtList.add(se);
                    se.setViewTable(vtMap.get(sys.getId()));
                }

                return new SystemSearchResult(sysExtList);
            }
        } catch (Exception e) {
            return new SystemSearchResult(e.getMessage());
        }
    }

    public static SystemSearchResult findSystemsByName(int fromSystemId, String name, int reqUser) {
        at.viswars.model.System system = systemDAO.findById(fromSystemId);
        if (system == null) {
            return new SystemSearchResult("Ung&uuml;ltiges Basissystem");
        }

        at.viswars.model.System sSearch = new at.viswars.model.System();
        sSearch.setName(name);
        ArrayList<at.viswars.model.System> systems = systemDAO.find(sSearch);

        // Filter results with viewable systems
        HashMap<Integer, HashMap<Integer, ViewTable>> vtMap = viewTableDAO.findByUserCategorized(reqUser);
        for (Iterator<at.viswars.model.System> sIt = systems.iterator();sIt.hasNext();) {
            at.viswars.model.System s = sIt.next();
            if (!vtMap.containsKey(s.getId())) {
                sIt.remove();
            }
        }

        if (systems.size() == 0) {
            return new SystemSearchResult("Keine Systeme mit Namen " + name + " gefunden");
        }

        ArrayList<SystemExt> sysExtList = new ArrayList<SystemExt>();

        for (at.viswars.model.System s : systems) {
            SystemExt se = new SystemExt(s, system);
            se.setViewTable(vtMap.get(se.getBase().getId()));
            sysExtList.add(se);
        }

        return new SystemSearchResult(sysExtList);
    }

    public static SystemSearchResult findOwnSystems(int fromSystemId, int userId) {
        ArrayList<PlayerPlanet> ppList = playerPlanetDAO.findByUserId(userId);
        at.viswars.model.System s = systemDAO.findById(fromSystemId);

        ArrayList<SystemExt> sysExtList = new ArrayList<SystemExt>();
        HashMap<Integer, HashMap<Integer, ViewTable>> vtMap = viewTableDAO.findByUserCategorized(userId);
        vtMap = icludePlanetLogData(userId, vtMap);

        for (PlayerPlanet pp : ppList) {
            Planet p = planetDAO.findById(pp.getPlanetId());
            at.viswars.model.System system = systemDAO.findById(p.getSystemId());

            SystemExt se = new SystemExt(system, s);
            sysExtList.add(se);
            se.setViewTable(vtMap.get(system.getId()));
        }

        return new SystemSearchResult(sysExtList);
    }

    /**
     * Creates a object containing systems and their planets, this function seems 
     * to be very expensive and probably should be used only very rare.
     * 
     * When fromSystemId and systemId are the same a different routine is run to 
     * consume not so many unnecessary ressources.
     *
     * @param fromSystemId A reference system used for distance calculation
     * @param systemId The actual system i guess
     * @param reqUser The user for who this list should be generated
     * 	rausgeholt werden soll, als drin ist, gibt es eine Exception
     * @returns A SystemSearchResult containing an extensive list with requested systems
     */
    public static SystemSearchResult findSystemsBySystemId(int fromSystemId, int systemId, int reqUser) {
        at.viswars.model.System system = systemDAO.findById(fromSystemId);
        at.viswars.model.System system2 = systemDAO.findById(systemId);
        if (system2 == null) {
            return new SystemSearchResult(ML.getMLStr("systemsearch_err_invalidsystemid", reqUser));
        }

        HashMap<Integer, HashMap<Integer, ViewTable>> vtMap = null;

        if (fromSystemId == systemId) {
            vtMap = viewTableDAO.findByUserAndSystemCategorized(reqUser, systemId);
            vtMap = icludePlanetLogData(reqUser, vtMap);
        } else {
            vtMap = viewTableDAO.findByUserCategorized(reqUser);
            if (!vtMap.containsKey(systemId)) system2.setName("System "+system2.getId());
            vtMap = icludePlanetLogData(reqUser, vtMap);
        }

        ArrayList<SystemExt> sysExtList = new ArrayList<SystemExt>();
        SystemExt se = new SystemExt(system2, system);
        sysExtList.add(se);
        se.setViewTable(vtMap.get(system2.getId()));
        return new SystemSearchResult(sysExtList);
    }

    public static HashMap<Integer, HashMap<Integer, ViewTable>> icludePlanetLogData(int userId, HashMap<Integer, HashMap<Integer, ViewTable>> vtMap) {
        boolean debug = false;
        StarMapInfoData smid = new StarMapInfoData(userId);
        HashMap<Integer, ArrayList<at.viswars.model.System>> observatories = smid.getObservatories();
        HashMap<Integer, PlayerPlanet> pps = new HashMap<Integer, PlayerPlanet>();
        for (PlayerPlanet ppTmp : (ArrayList<PlayerPlanet>) playerPlanetDAO.findAll()) {
            pps.put(ppTmp.getPlanetId(), ppTmp);
        }
        StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
        ArrayList<at.viswars.model.System> systems = systemDAO.findAll();
        int i = 0;

        for (at.viswars.model.System s : systems) {
            SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
            quadtree.addItemToTree(sd);
            i++;
        }

        //Building all Systems in Observatory reach
        HashSet<SystemDesc> tmpRes = new HashSet<SystemDesc>();
        HashSet<Integer> sysObs = new HashSet<Integer>();
        for (Map.Entry<Integer, ArrayList<at.viswars.model.System>> entry0 : observatories.entrySet()) {
            for (at.viswars.model.System entry : entry0.getValue()) {
                tmpRes.addAll(quadtree.getItemsAround(entry.getX(), entry.getY(), (int) 100));
            }
        }
        for (SystemDesc sd : tmpRes) {
            sysObs.add(sd.id);
        }


        HashMap<Integer, ArrayList<Integer>> fleets = new HashMap<Integer, ArrayList<Integer>>();
        for (PlayerFleet pf : (ArrayList<PlayerFleet>) playerFleetDAO.findAll()) {
            ArrayList<Integer> entry1 = fleets.get(pf.getSystemId());
            if (entry1 == null) {
                entry1 = new ArrayList<Integer>();
            }
            if ((!entry1.contains(pf.getPlanetId()) && (pf.getUserId() == userId || smid.getSharingUsers().contains(pf.getUserId())))) {
                entry1.add(pf.getPlanetId());
            }
            fleets.put(pf.getSystemId(), entry1);
        }
        //Planetlog
        HashMap<Integer, ArrayList<PlanetLog>> planetLogs = planetLogDAO.findAllSorted();
        //Systems
        HashMap<Integer, ArrayList<Planet>> planetList = planetDAO.findAllSortedBySystem();

        // **************************************************************
        HashSet<Integer> allUsers = new HashSet<Integer>();
        allUsers.add(userId);

        // Gather all visible planets for collecting users for DiplomacyRelations
        for (at.viswars.model.System s : systems) {
            if (vtMap.containsKey(s.getId())) {
                HashMap<Integer, ViewTable> planetEntries = vtMap.get(s.getId());
                for (Iterator<Integer> pIt = planetEntries.keySet().iterator(); pIt.hasNext();) {
                    Integer planetIdTmp = pIt.next();

                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetIdTmp);
                    if (pp != null) {
                        allUsers.add(pp.getUserId());

                        if (planetLogs.get(planetIdTmp) != null) {
                            for (PlanetLog pl : planetLogs.get(planetIdTmp)) {
                                allUsers.add(pl.getUserId());
                            }
                        }
                    }
                }
            }
        }

        ArrayList<Integer> arrAllUsers = new ArrayList<Integer>();
        arrAllUsers.addAll(allUsers);

        // **************************************************************
        for (at.viswars.model.System s : systems) {
            if (!vtMap.containsKey(s.getId())) {
                continue;
            }

            // Viewtableentries for map existing
            if (vtMap.containsKey(s.getId())) {
                boolean actual = false;
                if (debug) {
                    Logger.getLogger().write("1 - Actual : " + actual);
                }
                //Search if any player has a Planet in this System
                HashMap<Integer, ViewTable> planetEntries = vtMap.get(s.getId());

                for (Map.Entry<Integer, ViewTable> entry : planetEntries.entrySet()) {
                    //PP exists
                    if (pps.containsKey(entry.getKey())) {
                        PlayerPlanet pp = pps.get(entry.getKey());
                        // Whole system is actual
                        if (userId == pp.getUserId() || smid.getSharingUsers().contains(pp.getUserId())) {
                            actual = true;

                        }
                    }
                }
                if (debug) {
                    Logger.getLogger().write("2 - Actual : " + actual);
                }
                //Search in observatories
                if (!actual) {
                    if (sysObs.contains(s.getId())) {
                        actual = true;
                    }
                }
                if (debug) {
                    Logger.getLogger().write("3 - Actual : " + actual);
                }
                //Search in fleets

                if (fleets.containsKey(s.getId()) && fleets.get(s.getId()).contains(0)) {
                    actual = true;

                }
                if (debug) {
                    Logger.getLogger().write("4 - Actual : " + actual);
                }

                for (Map.Entry<Integer, ViewTable> entry : planetEntries.entrySet()) {
                    //System without planets
                    if (debug) {
                        Logger.getLogger().write("Pid :  : " + entry.getKey());
                    }

                    if (fleets.containsKey(s.getId()) && fleets.get(s.getId()).contains(entry.getKey())) {
                        actual = true;

                    }

                    Planet p = planetDAO.findById(entry.getKey());
                    if(p == null){
                        continue;
                    }
                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(entry.getKey());
                    ViewTable vt = entry.getValue();
                    int planetOwner = 0;

                    if (actual) {
                        if (pp != null) {
                            planetOwner = pp.getUserId();
                        }
                    } else {
                        if (planetLogs.get(p.getId()) != null) {
                            for (PlanetLog pl : planetLogs.get(p.getId())) {
                                if (pl.getTime() > vt.getLastTimeVisited()) {
                                    continue;
                                } else {
                                    if (pp != null) {
                                        planetOwner = pl.getUserId();
                                        break;
                                    }
                                }
                            }
                        }

                    }

                    vt.setUserId(planetOwner);
                }
            }
        }
        return vtMap;
    }

    public static SystemSearchResult findAllAlliedSystems(int fromSystemId, int userId) {
        ArrayList<Integer> allies = AllianceUtilities.getAllAlliedUsers(userId);
        ArrayList<SystemExt> sysExtList = new ArrayList<SystemExt>();
        HashMap<Integer, HashMap<Integer, ViewTable>> vtMap = viewTableDAO.findByUserCategorized(userId);
        at.viswars.model.System refSystem = systemDAO.findById(fromSystemId);

        for (int actUserId : allies) {
            if (actUserId == userId) {
                continue;
            }
            ArrayList<PlayerPlanet> ppList = playerPlanetDAO.findByUserId(actUserId);
            for (PlayerPlanet pp : ppList) {
                Planet p = planetDAO.findById(pp.getPlanetId());
                at.viswars.model.System sys = systemDAO.findById(p.getSystemId());

                SystemExt se = new SystemExt(sys, refSystem);
                se.setViewTable(vtMap.get(sys.getId()));
                sysExtList.add(se);
            }
        }

        return new SystemSearchResult(sysExtList);
    }

    public static SystemSearchResult findAllEnemySystems(int fromSystemId, int userId) {
        ArrayList<Integer> allies = AllianceUtilities.getAllAlliedUsers(userId);
        at.viswars.model.System refSystem = systemDAO.findById(fromSystemId);

        HashSet<Integer> alliedMap = new HashSet<Integer>();
        for (int actUserId : allies) {
            alliedMap.add(actUserId);
        }

        ArrayList<SystemExt> sysExtList = new ArrayList<SystemExt>();
        HashMap<Integer, HashMap<Integer, ViewTable>> vtMap = viewTableDAO.findByUserCategorized(userId);

        for (int systemId : vtMap.keySet()) {
            at.viswars.model.System sys = systemDAO.findById(systemId);
            if (sys == null) {
                DebugBuffer.addLine(DebugLevel.ERROR, "FindAllEnemySystem tried to load system " + systemId + " from viewtable but no system table entry found -> SKIP");
                continue;
            }

            SystemExt se = new SystemExt(sys, refSystem);
            se.setViewTable(vtMap.get(sys.getId()));

            for (PlanetExt pe : se.getPlanets()) {
                if ((pe.getOwnerAttitude(userId) == EOwner.WAR) ||
                    (pe.getOwnerAttitude(userId) == EOwner.AGGRESSIVE) ||
                    (pe.getOwnerAttitude(userId) == EOwner.NEUTRAL) ||
                    (pe.getOwnerAttitude(userId) == EOwner.NAP) ||
                    (pe.getOwnerAttitude(userId) == EOwner.TRADE))
                {
                    sysExtList.add(se);
                }
            }
        }

        return new SystemSearchResult(sysExtList);
    }

    public static SystemSearchResult findAllAlliedSystems(int fromSystemId, int userId, String targetUserName) {
        /*
        TreeMap<Double, at.viswars.model.System> sTree = new TreeMap<Double, at.viswars.model.System>();
        at.viswars.model.System system = systemDAO.findById(fromSystemId);
        User u = userDAO.findByGameName(targetUserName);
        Alliance a = allianceDAO.findById(allianceMemberDAO.findByUserId(userId).getAllianceId());
        // Get all allied Alliances
        ArrayList<Alliance> alliedAlliances = allianceDAO.findByMasterId(a.getMasterAlliance());
        // Get all allianceMembers of alliedAlliances and compare to user2
        ArrayList<AllianceMember> ams = allianceMemberDAO.findAllianceMembersOfAlliances(alliedAlliances);
        
        //Sicherheitscheck
        if (!areAllied(ams, u.getUserId())) {
        return null;
        }
        for (at.viswars.model.ViewTable sysView : viewTableDAO.findViewTableByUserId(userId)) {
        at.viswars.model.System s = systemDAO.findById(sysView.getSystemId());
        boolean foundAlliedPlanetInSystem = false;
        for (Planet p : planetDAO.findBySystemId(s.getId())) {
        if (playerPlanetDAO.findByPlanetId(p.getId()) == null || playerPlanetDAO.findByPlanetId(p.getId()).getUserId() == userId) {
        continue;
        } else {
        if (u.getUserId() == playerPlanetDAO.findByPlanetId(p.getId()).getUserId()) {
        
        foundAlliedPlanetInSystem = true;
        
        }
        }
        
        }
        if (foundAlliedPlanetInSystem) {
        double distance = Math.sqrt(Math.pow(Math.abs(system.getX() - s.getX()), 2) + Math.pow(Math.abs(system.getY() - s.getY()), 2));
        sTree.put(distance, s);
        }
        }
        return sTree;
         */
        return new SystemSearchResult("Not implemented");
    }

    public static SystemSearchResult findAllEnemySystems(int fromSystemId, int userId, String targetUserName) {
        /*
        TreeMap<Double, at.viswars.model.System> sTree = new TreeMap<Double, at.viswars.model.System>();
        at.viswars.model.System system = systemDAO.findById(fromSystemId);
        User u = userDAO.findByGameName(targetUserName);
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        ArrayList<AllianceMember> ams = new ArrayList<AllianceMember>();
        if (am != null) {
        Alliance a = allianceDAO.findById(allianceMemberDAO.findByUserId(userId).getAllianceId());
        // Get all allied Alliances
        ArrayList<Alliance> alliedAlliances = allianceDAO.findByMasterId(a.getMasterAlliance());
        // Get all allianceMembers of alliedAlliances and compare to user2
        ams = allianceMemberDAO.findAllianceMembersOfAlliances(alliedAlliances);
        }
        
        for (at.viswars.model.ViewTable sysView : viewTableDAO.findViewTableByUserId(userId)) {
        at.viswars.model.System s = systemDAO.findById(sysView.getSystemId());
        if (!viewTableDAO.isSystemInViewtable(userId, s.getId())) {
        continue;
        }
        boolean foundEnemyPlanetInSystem = false;
        for (Planet p : planetDAO.findBySystemId(s.getId())) {
        if (playerPlanetDAO.findByPlanetId(p.getId()) == null || playerPlanetDAO.findByPlanetId(p.getId()).getUserId() == userId) {
        continue;
        } else {
        if (!areAllied(ams, playerPlanetDAO.findByPlanetId(p.getId()).getUserId())) {
        if (u.getUserId() == playerPlanetDAO.findByPlanetId(p.getId()).getUserId()) {
        
        foundEnemyPlanetInSystem = true;
        }
        
        }
        }
        
        }
        if (foundEnemyPlanetInSystem) {
        double distance = Math.sqrt(Math.pow(Math.abs(system.getX() - s.getX()), 2) + Math.pow(Math.abs(system.getY() - s.getY()), 2));
        sTree.put(distance, s);
        }
        }
        return sTree;
         */
        return new SystemSearchResult("Not implemented");
    }

    @Deprecated
    public static String getPlanetString(at.viswars.model.System system, int userId) {
        if (!viewTableDAO.isSystemInViewtable(userId, system.getId())) {
            return "<TD ALIGN='CENTER' COLSPAN='13'> ??? </TD>";
        }
        String result = "";

        // PLANET TYPES#
        TreeMap<Integer, Planet> planetsOrbit = new TreeMap<Integer, Planet>();
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        ArrayList<AllianceMember> ams = new ArrayList<AllianceMember>();
        if (am != null) {
            Alliance a = allianceDAO.findById(am.getAllianceId());// Get all allied Alliances
            ArrayList<Alliance> alliedAlliances = allianceDAO.findByMasterId(a.getMasterAlliance());
            // Get all allianceMembers of alliedAlliances and compare to user2
            ams = allianceMemberDAO.findAllianceMembersOfAlliances(alliedAlliances);
        }

        ArrayList<Planet> planets = planetDAO.findBySystemId(system.getId());
        for (Planet p : planets) {

            planetsOrbit.put(p.getOrbitLevel(), p);
        }
        int orbitLevel = 0;
        result += "<TD ONMOUSEOVER=\"doTooltip(event,'" + ML.getMLStr("systemsearch_pop_sun", userId) + "')\" ONMOUSEOUT=\"hideTip()\"><B><CENTER>S</CENTER></B></TD>";
        orbitLevel++;
        for (Map.Entry<Integer, Planet> entry : planetsOrbit.entrySet()) {
            while (entry.getKey() != orbitLevel) {

                result += "<TD STYLE='width:25px; height:23px;'>&nbsp;</TD>";
                orbitLevel++;
            }
            String owner = "";
            if (entry.getValue() != null) {
                result += "<TD ";
                PlayerPlanet pp = playerPlanetDAO.findByPlanetId(entry.getValue().getId());
                if (pp != null) {
                    String allyTag = "";
                    if (allianceMemberDAO.findByUserId(pp.getUserId()) != null) {
                        allyTag = "  <B>[" + allianceDAO.findById(allianceMemberDAO.findByUserId(pp.getUserId()).getAllianceId()).getTag() + "]</B>";
                    }
                    owner += ML.getMLStr("systemsearch_lbl_owner", userId) + ": " + userDAO.findById(pp.getUserId()).getGameName() + allyTag + "<BR>";

                    if (pp.getUserId() == userId) {
                        result += "style=' width:25px; height:23px; background-image:url(" + GameConfig.picPath() + "pic/syssearchown.png)'";
                    } else if (areAllied(ams, pp.getUserId())) {
                        result += "style=' width:25px; height:23px; background-image:url(" + GameConfig.picPath() + "pic/syssearchallied.png)'";
                    } else {
                        result += "style=' width:25px; height:23px; background-image:url(" + GameConfig.picPath() + "pic/syssearchenemy.png)'";
                    }
                }
                /**
                 *
                 * @TODO Onklick Flotte Senden
                 */
                result += "ONMOUSEOVER=\"doTooltip(event,'" + owner + ML.getMLStr("systemsearch_pop_" + entry.getValue().getLandType(), userId) + "')\" ONMOUSEOUT=\"hideTip()\" ><CENTER><B><FONT color='";
                if (entry.getValue().getLandType().equals(Planet.LANDTYPE_A)) {
                    result += "#8c7f24'>";
                } else if (entry.getValue().getLandType().equals(Planet.LANDTYPE_B)) {
                    result += "#00b7e4'>";
                } else if (entry.getValue().getLandType().equals(Planet.LANDTYPE_C)) {
                    result += "#214237'>";
                } else if (entry.getValue().getLandType().equals(Planet.LANDTYPE_E)) {
                    result += "#FFFFFF'>";
                } else if (entry.getValue().getLandType().equals(Planet.LANDTYPE_G)) {
                    result += "#FDFD01'>";
                } else if (entry.getValue().getLandType().equals(Planet.LANDTYPE_J)) {
                    result += "#7e0000'>";
                } else if (entry.getValue().getLandType().equals(Planet.LANDTYPE_L)) {
                    result += "#FFFFFF'>";
                } else if (entry.getValue().getLandType().equals(Planet.LANDTYPE_M)) {
                    result += "#165000'>";
                }
                orbitLevel++;
            }


            result += entry.getValue().getLandType();
            result += "</FONT></B></CENTER></TD>";

            result += "";
        }
        while (orbitLevel <= Planet.MAX_ORBIT_LEVELS) {

            result += "<TD STYLE='width:25px; height:23px;'>&nbsp;</TD>";
            orbitLevel++;
        }
        return result;
    }

    @Deprecated
    public static String getOwnerString(at.viswars.model.System system, int userId) {
        if (!viewTableDAO.isSystemInViewtable(userId, system.getId())) {
            return "<TD ALIGN='CENTER' COLSPAN='3'> ??? </TD>";
        }
        // PLANET TYPES#
        String result = "";
        HashMap<String, Integer> ownerPlanets = new HashMap<String, Integer>();
        ownerPlanets.put("Own", 0);
        ownerPlanets.put("Allied", 0);
        ownerPlanets.put("Enemy", 0);
        for (Planet p : planetDAO.findBySystemId(system.getId())) {

            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
            if (pp != null) {
                if (pp.getUserId() == userId) {
                    ownerPlanets.put("Own", (ownerPlanets.get("Own") + 1));
                } else if (SystemService.areAllied(userId, pp.getUserId())) {
                    ownerPlanets.put("Allied", (ownerPlanets.get("Allied") + 1));
                } else {
                    ownerPlanets.put("Enemy", (ownerPlanets.get("Enemy") + 1));
                }

            }
        }
        for (Map.Entry<String, Integer> entry : ownerPlanets.entrySet()) {
            if (entry.getValue() == 0) {
                result += "<TD>&nbsp;</TD>";
                continue;
            }
            if (entry.getKey().equals("Own")) {
                result += "<TD ALIGN='CENTER'><B><FONT color='#00ff00'>" + entry.getValue() + "</FONT></B></TD>";
            } else if (entry.getKey().equals("Allied")) {
                result += "<TD ALIGN='CENTER'><B><FONT color='#00b7e4'>" + entry.getValue() + "</FONT></B></TD>";
            } else if (entry.getKey().equals("Enemy")) {
                result += "<TD ALIGN='CENTER'><B><FONT color='#ff0000'>" + entry.getValue() + "</FONT></B></TD>";

            }
        }

        return result;
    }

    public static HashMap<Integer, Integer> findSystemsPerUser(TreeMap<Double, at.viswars.model.System> systems) {
        HashMap<Integer, Integer> systemsPerUser = new HashMap<Integer, Integer>();
        for (Map.Entry<Double, at.viswars.model.System> entry : systems.entrySet()) {
            for (Planet p : planetDAO.findBySystemId(entry.getValue().getId())) {
                if (playerPlanetDAO.findByPlanetId(p.getId()) == null) {
                    continue;
                } else {
                    int tmpUserId = playerPlanetDAO.findByPlanetId(p.getId()).getUserId();
                    if (systemsPerUser.get(tmpUserId) == null) {
                        systemsPerUser.put(tmpUserId, 1);
                    } else {
                        systemsPerUser.put(tmpUserId, (systemsPerUser.get(tmpUserId) + 1));
                    }
                    break;
                }

            }

        }
        return systemsPerUser;
    }

    public static ArrayList<at.viswars.model.System> getSystemsAround(int systemId, double distance, ESystemSorting sort) {
        StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);

        at.viswars.model.System sSearch = systemDAO.findById(systemId);
        ArrayList<at.viswars.model.System> systems = systemDAO.findAll();

        int i = 0;

        for (at.viswars.model.System s : systems) {
            SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
            quadtree.addItemToTree(sd);
            i++;
        }

        ArrayList<at.viswars.model.System> result = new ArrayList<at.viswars.model.System>();
        ArrayList<SystemDesc> tmpRes = quadtree.getItemsAround(sSearch.getX(), sSearch.getY(), (int) distance);

        TreeMap<Object, ArrayList<at.viswars.model.System>> tm = new TreeMap<Object, ArrayList<at.viswars.model.System>>();

        for (SystemDesc sd : tmpRes) {
            at.viswars.model.System actSys = systemDAO.findById(sd.id);

            if (sort == ESystemSorting.SORT_BY_DISTANCE) {
                double actDistance = Math.sqrt(Math.pow((actSys.getX() - sSearch.getX()), 2d) + Math.pow((actSys.getY() - sSearch.getY()), 2d));
                if (tm.containsKey(actDistance)) {
                    tm.get(actDistance).add(actSys);
                } else {
                    ArrayList<at.viswars.model.System> sysList = new ArrayList<at.viswars.model.System>();
                    sysList.add(actSys);
                    tm.put(actDistance, sysList);
                }
            } else if ((sort == ESystemSorting.SORT_BY_ID) || (sort == ESystemSorting.NO_SORT)) {
                ArrayList<at.viswars.model.System> sysList = new ArrayList<at.viswars.model.System>();
                sysList.add(actSys);
                tm.put(actSys.getId(), sysList);
            } else if (sort == ESystemSorting.SORT_BY_NAME) {
                if (tm.containsKey(actSys.getName())) {
                    tm.get(actSys.getName()).add(actSys);
                } else {
                    ArrayList<at.viswars.model.System> sysList = new ArrayList<at.viswars.model.System>();
                    sysList.add(actSys);
                    tm.put(actSys.getName(), sysList);
                }
            }
        }

        for (Object key : tm.keySet()) {
            result.addAll(tm.get(key));
        }

        return result;
    }

    public static TreeMap<String, String> findAlliedUsers(TreeMap<Double, at.viswars.model.System> systems, int userId) {
        TreeMap<String, String> result = new TreeMap<String, String>();
        HashMap<Integer, Integer> systemsPerUser = findSystemsPerUser(systems);
        ArrayList<Alliance> alliedAllies = allianceDAO.findByMasterId(allianceDAO.findById(allianceMemberDAO.findByUserId(userId).getAllianceId()).getMasterAlliance());
        ArrayList<AllianceMember> ams = allianceMemberDAO.findAllianceMembersOfAlliances(alliedAllies);

        for (AllianceMember am : ams) {
            if (am.getUserId() == userId) {
                continue;
            }
            String planetCount = "";
            if (systemsPerUser.get(am.getUserId()) == null) {
                continue;
            } else {
                planetCount = String.valueOf(systemsPerUser.get(am.getUserId()));
            }
            result.put(userDAO.findById(am.getUserId()).getGameName(), userDAO.findById(am.getUserId()).getGameName()
                    + " [" + allianceDAO.findById(am.getAllianceId()).getTag() + "]"
                    + "  (" + planetCount + ")");

        }
        return result;
    }

    public static User findUserByGameName(String gameName) {

        return userDAO.findByGameName(gameName);
    }

    public static TreeMap<String, String> findEnemyUsers(TreeMap<Double, at.viswars.model.System> systems, int userId) {
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        ArrayList<AllianceMember> ams = new ArrayList<AllianceMember>();
        if (am != null) {
            Alliance a = allianceDAO.findById(am.getAllianceId());
            // Get all allied Alliances
            ArrayList<Alliance> alliedAlliances = allianceDAO.findByMasterId(a.getMasterAlliance());
            // Get all allianceMembers of alliedAlliances and compare to user2
            ams = allianceMemberDAO.findAllianceMembersOfAlliances(alliedAlliances);

        }

        TreeMap<String, String> result = new TreeMap<String, String>();
        HashMap<Integer, Integer> systemsPerUser = findSystemsPerUser(systems);
        ArrayList<User> users = userDAO.findAllPlayers();
        for (User u : users) {
            if (!areAllied(ams, u.getUserId())) {
                String planetCount;
                if (systemsPerUser.get(u.getUserId()) == null) {
                    continue;
                } else {
                    planetCount = String.valueOf(systemsPerUser.get(u.getUserId()));
                }
                String allyTag = "";
                if (allianceMemberDAO.findByUserId(u.getUserId()) != null) {
                    allyTag = "  [" + allianceDAO.findById(allianceMemberDAO.findByUserId(u.getUserId()).getAllianceId()).getTag() + "]";
                }
                result.put(u.getGameName(),
                        u.getGameName()
                        + allyTag + "  (" + planetCount + ")");
            }

        }
        return result;
    }

    public static boolean areAllied(int user1, int user2) {
        // Get alliance of user1
        AllianceMember am = allianceMemberDAO.findByUserId(user1);
        if (am == null) {
            return false;
        }
        Alliance a = allianceDAO.findById(allianceMemberDAO.findByUserId(user1).getAllianceId());
        // Get all allied Alliances
        ArrayList<Alliance> alliedAlliances = allianceDAO.findByMasterId(a.getMasterAlliance());
        // Get all allianceMembers of alliedAlliances and compare to user2
        ArrayList<AllianceMember> ams = allianceMemberDAO.findAllianceMembersOfAlliances(alliedAlliances);
        return allianceMemberDAO.areAllied(user2, ams);
    }

    public static boolean areAllied(ArrayList<AllianceMember> ams, int user2) {
        return allianceMemberDAO.areAllied(user2, ams);
    }

    public static Planet findPlanetById(int planetId) {
        return planetDAO.findById(planetId);

    }

    public static PlayerPlanet findPlayerPlanetByPlanetId(int planetId) {
        return playerPlanetDAO.findByPlanetId(planetId);
    }

    public static User findUserById(int userId) {
        return userDAO.findById(userId);
    }

    public static boolean isInAlliance(int userId) {
        if (allianceMemberDAO.findByUserId(userId) != null) {
            return true;
        } else {
            return false;
        }
    }
}
