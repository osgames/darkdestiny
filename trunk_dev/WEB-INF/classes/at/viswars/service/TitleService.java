/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.GameConfig;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.enumeration.EConditionType;
import at.viswars.enumeration.ETitleType;
import at.viswars.model.Condition;
import at.viswars.model.ConditionParameter;
import at.viswars.model.ConditionToTitle;
import at.viswars.model.Language;
import at.viswars.model.ParameterToTitle;
import at.viswars.model.Title;
import at.viswars.result.BaseResult;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 *
 * @author Aion
 */
public class TitleService extends Service {

    public static ArrayList<BaseResult> addTitle(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        Title title = new Title();
        Logger.getLogger().write("In here adding title : ");
        String titleName = allPars.get("titleName")[0];
        Logger.getLogger().write("titleName: " + titleName);
        title.setName("db_title_" + titleName);
        title.setDescription("db_title_" + titleName + "_desc");

        boolean visible = false;

        for (Map.Entry<String, String[]> entry : allPars.entrySet()) {

            if (entry.getKey().startsWith("lang_")) {
                String langKey = entry.getKey().replace("lang_", "");
                Integer languageId = Integer.parseInt(langKey.substring(0, 1));
                langKey = langKey.substring(2);
                Language l = languageDAO.findById(languageId);
                Properties properties = new Properties();
                String key = "null";

                if (langKey.startsWith("name")) {
                    key = title.getName() + langKey.substring(langKey.indexOf("_"));
                    Logger.getLogger().write("Key : " + key);
                } else if (langKey.equals("description")) {
                    key = title.getName() + "_desc";
                }
                try {
                    properties.load(new FileInputStream(GameConfig.getInstance().getSrcPath() + "/WEB-INF/classes/" + l.getPropertiesfile()));

                    Logger.getLogger().write("setting : " + key + " = " + entry.getValue()[0]);
                    properties.setProperty(key, entry.getValue()[0]);

                    FileOutputStream fos = new FileOutputStream(GameConfig.getInstance().getSrcPath() + "/WEB-INF/classes/" + l.getPropertiesfile());
                    properties.store(fos, "");
                    fos.flush();
                    fos.close();
                    Class type = ResourceBundle.class;
                    Field cacheList = type.getDeclaredField("cacheList");
                    cacheList.setAccessible(true);
                    ((Map) cacheList.get(ResourceBundle.class)).clear();
                } catch (Exception e) {
                    BaseResult br = new BaseResult("Error beim zugreifen auf das properties file<BR>"
                            + "sicher dass srcPath in db.properties richtig spezifiziert ist sucka?<BR>"
                            + "Pfad: <B>" + GameConfig.getInstance().getSrcPath() + "</B><BR>"
                            + "" + e, true);
                    result.add(br);
                    Logger.getLogger().write("error while savint to properties file : " + e);
                    return result;
                }
            }
            if (entry.getKey().equals("difficulty")) {
                int difficulty = Integer.parseInt(entry.getValue()[0]);
                title.setDifficulty(difficulty);
            }

            if (entry.getKey().equals("campaignId")) {
                int campaignId = Integer.parseInt(entry.getValue()[0].trim());
                title.setCampaignId(campaignId);
            }
            if (entry.getKey().equals("points")) {
                int points = Integer.parseInt(entry.getValue()[0]);
                title.setPoints(points);
            }
            if (entry.getKey().equals("titleType")) {
                ETitleType type = ETitleType.valueOf(entry.getValue()[0]);
                title.setType(type);
            }
            if (entry.getKey().equals("clazz")) {
                String clazz = "at.viswars.title." + entry.getValue()[0];
                title.setClazz(clazz);
            }
            if (entry.getKey().equals("visibility")) {
                visible = true;
            }


        }
        title.setVisible(visible);
        titleDAO.add(title);
        return result;

    }


    public static ArrayList<BaseResult> deleteTitle(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        int titleId = Integer.parseInt(allPars.get("titleId")[0]);
        for(ConditionToTitle ctt : conditionToTitleDAO.findByTitleId(titleId)){
            for(ParameterToTitle ptt : parameterToTitleDAO.findConditionToTitleId(ctt.getId())){
                parameterToTitleDAO.remove(ptt);
            }
            conditionToTitleDAO.remove(ctt);
        }
        Title t = titleDAO.findById(titleId);
        titleDAO.remove(t);
        result.add(new BaseResult("Bitte l�schen sie die Klasse : " + t.getClazz() + ".java h�ndisch", true));
        return result;
    }
    public static ArrayList<BaseResult> deleteCondition(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        int conditionId = Integer.parseInt(allPars.get("conditionId")[0]);
        Condition cond = conditionDAO.findById(conditionId);

        for(ConditionToTitle ctt : conditionToTitleDAO.findByTitleId(userId)){
            for(ParameterToTitle ptt : parameterToTitleDAO.findConditionToTitleId(ctt.getId())){
                parameterToTitleDAO.remove(ptt);
            }
            conditionToTitleDAO.remove(ctt);
        }

        for(ConditionParameter cp : conditionParameterDAO.findByConditionId(conditionId)){
            conditionParameterDAO.remove(cp);
        }

        conditionDAO.remove(cond);
        result.add(new BaseResult("Bitte l�schen sie die Klasse : " + cond.getClazz() + ".java h�ndisch", true));
        return result;
    }

    public static ArrayList<BaseResult> addParameter(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        ConditionParameter param = new ConditionParameter();
        Logger.getLogger().write("In here adding title : ");
        Integer conditionId = Integer.parseInt(allPars.get("conditionId")[0]);
        param.setConditionId(conditionId);
        for (ConditionParameter cp : conditionParameterDAO.findByConditionId(conditionId)) {
            if (cp.getOrder() == Integer.parseInt(allPars.get("order")[0])) {
                result.add(new BaseResult("F�r diese Stelle im Konstruktor gibt es schon einen Parameter", true));
                return result;
            }
        }
        param.setOrder(Integer.parseInt(allPars.get("order")[0]));
        param.setName(allPars.get("name")[0]);
        param.setType(EConditionType.valueOf(allPars.get("conditionType")[0]));

        conditionParameterDAO.add(param);



        return result;

    }

    public static ArrayList<BaseResult> deleteParameter(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        Integer conditionParameterId = Integer.parseInt(allPars.get("conditionParameterId")[0]);
        ConditionParameter param = conditionParameterDAO.findById(conditionParameterId);
        conditionParameterDAO.remove(param);
        return result;

    }

    public static ArrayList<BaseResult> createCondition(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        Condition c = new Condition();

        c.setDescription(allPars.get("description")[0]);
        c.setClazz("at.viswars.title.condition.C" + allPars.get("clazz")[0]);

        conditionDAO.add(c);

        return result;

    }

    public static ArrayList<BaseResult> assignCondition(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        try {
            int conditionId = Integer.parseInt(allPars.get("conditionId")[0]);
            int titleId = Integer.parseInt(allPars.get("titleId")[0]);


            ConditionToTitle ctt = new ConditionToTitle();
            ctt.setConditionId(conditionId);
            ctt.setTitleId(titleId);
            ctt.setOrder(Integer.parseInt(allPars.get("order")[0]));
            ctt.setDescription("");

            ctt = conditionToTitleDAO.add(ctt);

            String descKey = "db_title_condition_" + String.valueOf(ctt.getId()) + "_desc";
            ctt.setDescription(descKey);
            ctt = conditionToTitleDAO.update(ctt);

            for (Map.Entry<String, String[]> entry : allPars.entrySet()) {
                Logger.getLogger().write("key : " + entry.getKey() + " = " + entry.getValue()[0]);
            }
            for (Map.Entry<String, String[]> entry : allPars.entrySet()) {
                Logger.getLogger().write("key : " + entry.getKey());

                if (entry.getKey().startsWith("lang_")) {
                    String langKey = entry.getKey().replace("lang_", "");
                    Integer languageId = Integer.parseInt(langKey.substring(0, 1));
                    langKey = langKey.substring(2);
                    Language l = languageDAO.findById(languageId);
                    Properties properties = new Properties();

                    try {
                        properties.load(new FileInputStream(GameConfig.getInstance().getSrcPath() + "/WEB-INF/classes/" + l.getPropertiesfile()));

                        properties.setProperty(descKey, entry.getValue()[0]);

                        FileOutputStream fos = new FileOutputStream(GameConfig.getInstance().getSrcPath() + "/WEB-INF/classes/" + l.getPropertiesfile());
                        properties.store(fos, "");
                        fos.flush();
                        fos.close();
                        Class type = ResourceBundle.class;
                        Field cacheList = type.getDeclaredField("cacheList");
                        cacheList.setAccessible(true);
                        ((Map) cacheList.get(ResourceBundle.class)).clear();
                    } catch (Exception e) {
                        BaseResult br = new BaseResult("Error beim zugreifen auf das properties file<BR>"
                                + "sicher dass srcPath in db.properties richtig spezifiziert ist sucka?<BR>"
                                + "Pfad: <B>" + GameConfig.getInstance().getSrcPath() + "</B><BR>"
                                + "" + e, true);
                        result.add(br);
                        return result;
                    }
                }


            }
            for (ConditionParameter cp : conditionParameterDAO.findByConditionId(conditionId)) {
                String key = "condition_value_" + cp.getId();
                ParameterToTitle ptt = new ParameterToTitle();
                ptt.setConditionToTitleId(ctt.getId());
                ptt.setParameterId(cp.getId());
                ptt.setValue(allPars.get(key)[0]);
                parameterToTitleDAO.add(ptt);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.add(new BaseResult("Fehler bei der Parameter�bergabe", true));
        }
        result.add(new BaseResult("Done", false));
        return result;


    }

    public static ArrayList<BaseResult> generateJavaTitle(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        int titleId = Integer.parseInt(allPars.get("titleId")[0]);
        Title title = titleDAO.findById(titleId);
        String filePath = GameConfig.getInstance().getSrcPath() + "/WEB-INF/classes/" + title.getClazz().replace(".", "/") + ".java";
        Logger.getLogger().write("filePath :" + filePath);
        try {
            File f = new File(filePath);
            if (f.exists()) {
                result.add(new BaseResult("Dieses File gibt es schon bitte vorher l�schen", true));
                return result;
            } else {
                BufferedWriter out = new BufferedWriter(
                        new FileWriter(f));
                String outText = getJavaForTitle(titleId).toString();
                out.write(outText);
                out.close();
            }

        } catch (Exception e) {
            Logger.getLogger().write("e : " + e);
            e.printStackTrace();
        }
        result.add(new BaseResult("Klasse : " + title.getClazz() + ".java erstellt", false));
        return result;

    }

    public static ArrayList<BaseResult> generateJavaCondition(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        int conditionId = Integer.parseInt(allPars.get("conditionId")[0]);
        Condition condition = conditionDAO.findById(conditionId);
        String filePath = GameConfig.getInstance().getSrcPath() + "/WEB-INF/classes/" + condition.getClazz().replace(".", "/") + ".java";
        Logger.getLogger().write("filePath :" + filePath);
        try {
            File f = new File(filePath);
            if (f.exists()) {
                result.add(new BaseResult("Dieses File gibt es schon bitte vorher l�schen", true));
                return result;
            } else {
                BufferedWriter out = new BufferedWriter(
                        new FileWriter(f));
                String outText = getJavaForCondition(conditionId).toString();
                out.write(outText);
                out.close();
            }

        } catch (Exception e) {
            Logger.getLogger().write("e : " + e);
            e.printStackTrace();
        }
        result.add(new BaseResult("Klasse : " + condition.getClazz() + ".java erstellt", false));
        return result;

    }

    public static StringBuffer getJavaForTitle(int titleId) {

        StringBuffer string = new StringBuffer();

        Title title = titleDAO.findById(titleId);
        string.append("package at.viswars.title;\n");
        string.append("\n");
        string.append("\n");
        string.append("public class " + title.getClazz().replace("at.viswars.title.", "") + " extends AbstractTitle {\n");
        string.append("\n");
        string.append("\tpublic boolean check(int userId) {\n");
        string.append("\t\treturn true;\n");
        string.append("\t}\n");
        string.append("}\n");
        return string;
    }

    public static StringBuffer getJavaForCondition(int conditionId) {

        StringBuffer string = new StringBuffer();

        Condition cond = conditionDAO.findById(conditionId);
        string.append("package at.viswars.title.condition;\n");
        string.append("\n");
        string.append("\n");
        string.append("public class " + cond.getClazz().replace("at.viswars.title.condition.", "") + " extends AbstractCondition {\n");
        string.append("\n");
        for (ConditionParameter cp : conditionParameterDAO.findByConditionIdSortedByOrder(conditionId)) {
            string.append("\tprivate ParameterEntry " + cp.getName() + ";\n");

        }
        string.append("\n");
        string.append("\tpublic " + cond.getClazz().replace("at.viswars.title.condition.", "") + "(");
        boolean first = true;
        for (ConditionParameter cp : conditionParameterDAO.findByConditionIdSortedByOrder(conditionId)) {
            if (first) {
                first = false;
            } else {
                string.append(", ");
            }
            string.append("ParameterEntry " + cp.getName());

        }
        string.append("){\n");
        for (ConditionParameter cp : conditionParameterDAO.findByConditionIdSortedByOrder(conditionId)) {
            string.append("\t\tthis." + cp.getName() + " = " + cp.getName() + ";\n");

        }
        string.append("\t}\n");
        string.append("\n");
        string.append("\n");
        string.append("\n");
        string.append("\tpublic boolean checkCondition(int userId, int conditionToTitleId) {\n");
        string.append("\t\treturn true;\n");
        string.append("\t}\n");
        string.append("}\n");
        return string;
    }

    public static ArrayList<BaseResult> unAssignCondition(int userId, Map<String, String[]> allPars) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        try {
            int conditionToTitleId = Integer.parseInt(allPars.get("conditionToTitleId")[0]);

            ConditionToTitle ctt = conditionToTitleDAO.findById(conditionToTitleId);
            conditionToTitleDAO.remove(ctt);

            for (ParameterToTitle ptt : parameterToTitleDAO.findConditionToTitleId(conditionToTitleId)) {
                parameterToTitleDAO.remove(ptt);
            }
        } catch (Exception e) {
            result.add(new BaseResult("Fehler beim entfernen von Condition", true));
        }
        result.add(new BaseResult("Done", false));
        return result;


    }

    public static String findTitle(int userId, int loggedUser){
        int titleId = userDataDAO.findByUserId(userId).getTitleId();
        if(titleId > 0){
        return ML.getMLStr(titleDAO.findById(titleId).getName() + "_" + userDAO.findById(userId).getGender().toString(), loggedUser);
        }else{
            return ML.getMLStr("db_title_none", userId);
        }
    }

}
