/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.service;

import at.viswars.ML;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ModuleDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.RessourceDAO;
import at.viswars.dao.UserDAO;
import at.viswars.model.User;

/**
 *
 * @author HorstRabe
 */
public class IdToNameService {
    private final int userId;

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    public IdToNameService(int userId) {
        this.userId = userId;
    }

    public static String getPlanetName(int planetId) {
        if(ppDAO.findByPlanetId(planetId) != null){
        return ppDAO.findByPlanetId(planetId).getName();
        }else{
            return "Planet #" + planetId;
        }
    }

    public String getRessourceName(int ressId) {
        return ML.getMLStr(rDAO.findRessourceById(ressId).getName(),userId);
    }

    public String getModuleName(int moduleId) {
        return ML.getMLStr(mDAO.findById(moduleId).getName(),userId);
    }

    public static String getUserName(int userId) {
        User u = uDAO.findById(userId);

        if (u == null) {
            return "DELETED";
        }
        
        return u.getGameName();
    }
}
