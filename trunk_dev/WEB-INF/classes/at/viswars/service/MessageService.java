/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.FormatUtilities;
import at.viswars.GenerateMessage;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.MessageDAO;
import at.viswars.dao.UserDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.dao.VotingDAO;
import at.viswars.enumeration.EDoVoteRefType;
import at.viswars.enumeration.EMessageFolder;
import at.viswars.enumeration.EMessageRefType;
import at.viswars.enumeration.EMessageType;
import at.viswars.model.Message;
import at.viswars.model.User;
import at.viswars.model.UserData;
import at.viswars.model.Voting;
import at.viswars.result.BaseResult;
import at.viswars.utilities.LanguageUtilities;
import at.viswars.voting.EndVoteChart;
import at.viswars.voting.Vote;
import at.viswars.voting.VoteResult;
import at.viswars.voting.VoteUtilities;
import java.util.ArrayList;
import java.util.Iterator;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author Bullet
 */
public class MessageService {

    public static final int TYPE_SOURCE_USER = 1;
    public static final int TYPE_TARGET_USER = 2;
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static MessageDAO mDAO = (MessageDAO) DAOFactory.get(MessageDAO.class);
    private static VotingDAO vDAO = (VotingDAO) DAOFactory.get(VotingDAO.class);

    public static BaseResult writeMessageToUser(int sender, ArrayList<Integer> receiver, String topic, String message, boolean ignoreSmiley) {
        // Check values

        User sendingUser = uDAO.findById(sender);
        if (sendingUser == null) {
            return new BaseResult("Sender does not exist", true);
        }
        ArrayList<User> receiverList = new ArrayList<User>();

        for (Integer uId : receiver) {
            User receivingUser = uDAO.findById(uId);
            if (sendingUser == null) {
                return new BaseResult("Receiver (" + uId + ") does not exist", true);
            }
            receiverList.add(receivingUser);
        }

        GenerateMessage gm = new GenerateMessage();
        gm.setIgnoreSmiley(ignoreSmiley);
        
        topic = FormatUtilities.killJavaScript(topic);
        message = FormatUtilities.killJavaScript(message);
        
        gm.setTopic(topic);                        
        gm.setMsg(message);
        gm.setMasterEntry(true);
        gm.setMessageType(EMessageType.USER);
        gm.setSourceUserId(sender);

        for (Integer uId : receiver) {
            gm.setDestinationUserId(uId);
            gm.writeMessageToUser();
        }

        return new BaseResult("", false);
    }

    public static User findUserById(int userId) {
        return uDAO.findById(userId);
    }

    public static Message getMessageForView(int userId, int msgId, int type) {
        Message m = mDAO.findById(msgId);

        if (type == TYPE_TARGET_USER) {
            if (m.getTargetUserId() != userId) {
                return null;
            }
        } else if (type == TYPE_SOURCE_USER) {
            if (m.getSourceUserId() != userId) {
                return null;
            }
        }

        return m;
    }

    public static JFreeChart getChart(int userId, Message m) {
        if (m.getRefType() == EMessageRefType.VOTE) {
            int voteId = m.getRefId();

            Voting vTmp = vDAO.findById(voteId);
            Vote v = VoteUtilities.getVote(voteId);

            if (v.getType() == Voting.TYPE_VOTEROUNDEND) {
                Logger.getLogger().write(LogLevel.ERROR, "Calling Endvote Chart with parameters USERID: " + userId + " VOTEID: " + voteId);
                EndVoteChart evc = new EndVoteChart(userId, voteId);
                return evc.getChart();
            }
        }

        return null;
    }

    public static String getExtendedVoteText(int userId, Message m) {
        // Append Vote HTML
        String result = "";

        if (m.getRefType() == EMessageRefType.VOTE) {
            int voteId = m.getRefId();
            Voting vTmp = vDAO.findById(voteId);

            Vote v = VoteUtilities.getVote(voteId);
            try {
                VoteResult vr = VoteUtilities.currentState(userId, voteId);
                String voteStateText = VoteUtilities.getVoteStateHTML(vr, voteId, LanguageUtilities.getMasterLocaleForUser(userId));

                result += voteStateText;

                String voteText = VoteUtilities.generateVoteBody(v, voteId, userId, EDoVoteRefType.MESSAGE, m.getMessageId());

                Logger.getLogger().write("Append " + voteText);
                result += voteText;

                if (v.getType() == Voting.TYPE_VOTEROUNDEND) {
                    result += "<BR><BR>";
                    result += "<IMG src=\"ChartViewer\" usemap=\"#map\">";
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error on adding vote html to message: ", e);
            }
        }

        return result;
    }

    public static void deleteMessage(int userId, int msgId, EMessageFolder msgFolder) {
        try {
            if (msgFolder == null) {
                return;
            }
            Message m = mDAO.findById(msgId);
            if (m == null) {
                return;
            }

            if (msgFolder == EMessageFolder.OUTBOX) {
                if (m.getSourceUserId() == userId) {
                    m.setDelBySource(true);
                } else {
                    return;
                }
            } else if (msgFolder == EMessageFolder.INBOX) {
                if (m.getTargetUserId() == userId) {
                    m.setDelByTarget(true);
                    m.setViewed(true);
                } else {
                    return;
                }
            } else if (msgFolder == EMessageFolder.ARCHIVE) {
                if (m.getTargetUserId() == userId) {
                    m.setDelByTarget(true);
                    m.setViewed(true);
                    m.setArchiveByTarget(true);
                } else {
                    return;
                }
            }

            if ((m.getDelBySource() || (m.getSourceUserId() == 0)) && m.getDelByTarget()) {
                mDAO.remove(m);
            } else {
                mDAO.update(m);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in deleteMessage: ", e);
        }
    }

    public static void deleteAllMessages(int userId, EMessageFolder msgFolder, EMessageType msgType) {
        if (msgFolder == null) {
            return;
        }

        try {
            ArrayList<Message> mList = new ArrayList<Message>();

            mList = mDAO.findByUserId(userId, msgFolder);

            if (mList.size() == 0) {
                return;
            }
            for (Iterator<Message> mIt = mList.iterator(); mIt.hasNext();) {
                Message m = mIt.next();

                if (msgType != null) {
                    if (m.getType() != msgType) {
                        mIt.remove();
                        continue;
                    }
                }

                if ((msgFolder == EMessageFolder.INBOX) || (msgFolder == EMessageFolder.ARCHIVE)) {
                    m.setViewed(true);
                    m.setDelByTarget(true);
                } else if (msgFolder == EMessageFolder.OUTBOX) {
                    m.setDelBySource(true);
                }

                if ((m.getDelBySource() || (m.getSourceUserId() == 0)) && m.getDelByTarget()) {
                    mDAO.remove(m);
                    mIt.remove();
                }
            }

            if (mList.size() > 0) {
                mDAO.updateAll(mList);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in deleteMessages: ", e);
        }
    }

    @Deprecated
    public static Message findMessageBySourceId(int userId, int msgId) {
        return mDAO.findBySourceId(userId, msgId);
    }

    @Deprecated
    public static Message findMessageByTargetId(int userId, int msgId) {
        return mDAO.findByTargetId(userId, msgId);
    }

    public static void updateMessage(Message m) {
        mDAO.update(m);
    }

    public static ArrayList<Message> findInboxMessages(int userId) {
        return mDAO.findInboxMessages(userId);
    }

    public static ArrayList<Message> findOutboxMessages(int userId) {
        return mDAO.findOutboxMessages(userId);
    }

    public static ArrayList<Message> findArchivedMessages(int userId) {
        return mDAO.findArchivedMessages(userId);
    }

    public static ArrayList<Message> findMessagesByTargetUserId(int userId) {
        return mDAO.findByTargetUserId(userId);
    }

    public static ArrayList<Message> findMessagesBySourceUserId(int userId) {
        return mDAO.findBySourceUserId(userId);
    }

    public static ArrayList<Message> findMessagesByUserId(int userId, EMessageType messageType) {
        return mDAO.findByUserId(userId, messageType);
    }

    public static UserData findUserDataById(int userId) {
        return udDAO.findByUserId(userId);
    }

    public static ArrayList<User> findAllUsers() {
        return uDAO.findAll();
    }
}
