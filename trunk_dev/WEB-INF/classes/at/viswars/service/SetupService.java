/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.dao.*;
import at.viswars.database.access.DbConnect;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.database.framework.utilities.CreateFromModel;
import at.viswars.enumeration.*;
import at.viswars.model.*;
import at.viswars.result.BaseResult;
import at.viswars.result.GalaxyCreationResult;
import at.viswars.result.PropertiesCheckResult;
import at.viswars.setup.ConfigurationResult;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.StringReader;
import java.lang.System;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.*;

/**
 * g
 *
 * @author Aion
 */
public class SetupService {

    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private static PlanetLoyalityDAO pLoyalDAO = (PlanetLoyalityDAO) DAOFactory.get(PlanetLoyalityDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    public final static int BASE_POINTS = 10;
    public final static int BASE_SMALL_PLANET = 0;
    public final static int BASE_MEDIUM_PLANET = -6;
    public final static int BASE_LARGE_PLANET = -14;
    public final static int BASE_NO_RARE = 4;
    public final static int BASE_NORMAL_RARE = 0;
    public final static int BASE_MUCH_RARE = -6;
    public final static int BASE_LOW_IRON = 2;
    public final static int BASE_NORMAL_IRON = 0;
    public final static int BASE_HIGH_IRON = -4;
    public final static int BASE_ADDPOPULATION_PER = -2;
    private static String tickTime;

    public static ArrayList<BaseResult> generatePropertyFiles(Map<String, String[]> params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        //#### db.properties
        String classFolder = params.get("classFolder")[0];
        {
            String hostip = params.get("hostip")[0];
            String dbname = params.get("dbname")[0];
            String user = params.get("user")[0];
            String password = params.get("password")[0];
            String dbFile = classFolder + "/db.properties";
            try {
                File f = new File(dbFile);
                if (f.exists()) {
                    f.delete();

                } else {
                    Logger.getLogger().write(dbFile + " notfound");
                }
            } catch (Exception e) {
                Logger.getLogger().write("db.properties file not found: " + e);
            }
            Properties db = new Properties();
            db.setProperty("url", "jdbc" + ":" + "mysql" + ":" + "//" + hostip + "/" + dbname);
            db.setProperty("driverClass", "org.gjt.mm.mysql.Driver");
            db.setProperty("user", user);
            db.setProperty("pass", password);
            db.setProperty("logging", "INFO");
            db.setProperty(SetupProperties.DATABASE_MYSQLIP, "jdbc" + ":" + "mysql" + ":" + "//" + hostip + "");
            db.setProperty(SetupProperties.DATABASE_NAME, dbname);
            try {
                FileOutputStream fos = new FileOutputStream(dbFile);
                db.store(fos, user);
                fos.close();
            } catch (Exception e) {
                Logger.getLogger().write("Error writing db.properties: " + e);
            }
        }
        //#### game.properties
        {
            String gamehostURL = params.get("gamehostURL")[0];
            String startURL = params.get("startURL")[0];
            String srcPath = params.get("srcPath")[0];
            tickTime = params.get("tickTime")[0];
            String height = params.get("height")[0];
            String width = params.get("width")[0];
            String gameFile = classFolder + "/game.properties";
            try {
                File f = new File(gameFile);
                if (f.exists()) {
                    f.delete();

                } else {
                    Logger.getLogger().write(gameFile + " notfound");
                }
            } catch (Exception e) {
                Logger.getLogger().write("game.properties file not found: " + e);
            }
            Properties game = new Properties();
            game.setProperty("gamehostURL", gamehostURL);
            game.setProperty("startURL", startURL);
            game.setProperty("srcPath", srcPath);
            game.setProperty(SetupProperties.GAME_CLASSFOLDER, classFolder);
            game.setProperty(SetupProperties.GAME_TICKTIME, tickTime);
            game.setProperty(SetupProperties.GAME_UNIVERSE_HEIGHT, height);
            game.setProperty(SetupProperties.GAME_UNIVERSE_WIDTH, width);
            try {
                FileOutputStream fos = new FileOutputStream(gameFile);
                game.store(fos, null);
                fos.close();
            } catch (Exception e) {
                Logger.getLogger().write("Error writing game.properties: " + e);
            }
        }

        //Generate Database:


        result.add(new BaseResult("Game Values setted", false));
        result.add(new BaseResult("Database Values setted", false));
        return result;
    }

    public static void changePlayerStart(int galaxyId) {
        Galaxy g = Service.galaxyDAO.findById(galaxyId);
        g.setPlayerStart(!g.getPlayerStart());
        {
            Service.galaxyDAO.update(g);
        }
    }

    public static void generateGalaxy(Map<String, String[]> params) {


        String universetype = params.get("universetype")[0];
        String height = params.get("height")[0];
        String width = params.get("width")[0];
        int originX = Integer.parseInt(params.get("originX")[0]);
        int originY = Integer.parseInt(params.get("originY")[0]);
        String systemsPerUser = params.get("systemsPerUser")[0];
        String planetDensity = params.get("planetDensity")[0];

        GalaxyCreationResult gcr = UniverseCreation.createNewGalaxy(Integer.parseInt(height), Integer.parseInt(width), false, EPlanetDensity.valueOf(planetDensity), EGalaxyType.valueOf(universetype), Integer.parseInt(systemsPerUser), originX, originY);
        gcr.persist();
    }

    public static HashMap<String, ArrayList<String>> getInserts(String file) {
        HashMap<String, ArrayList<String>> inserts = new HashMap<String, ArrayList<String>>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(file)));
            String input = "";
            while ((input = br.readLine()) != null) {
                if (input.startsWith("INSERT INTO")) {
                    String table = input.substring(13);
                    table = table.substring(0, table.indexOf(" ") - 1);
                    if (table.equals("planetressource") || table.equals("system") || table.equals("planet") || table.equals("gamedata")) {
                        continue;
                    }
                    ArrayList<String> values = inserts.get(table);
                    if (values == null) {
                        values = new ArrayList<String>();
                    }
                    values.add(input);
                    inserts.put(table, values);
                }
            }
        } catch (Exception ex) {
            DebugBuffer.writeStackTrace("Error in " + SetupService.class.getName(), ex);
            ex.printStackTrace();
            // Logger.getLogger(SetupService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inserts;
    }

    public static ArrayList<BaseResult> generateDB() {


        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        try {
            Properties dbProperties = new Properties();
            dbProperties.load(SetupService.class.getResourceAsStream("/db.properties"));

            Properties universeProperties = new Properties();
            universeProperties.load(SetupService.class.getResourceAsStream("/universe.properties"));

            Properties gameProperties = new Properties();
            gameProperties.load(SetupService.class.getResourceAsStream("/game.properties"));

            GameConfig config = GameConfig.getInstance();

            Class.forName(config.getDriverClassName());
            String url = dbProperties.getProperty(SetupProperties.DATABASE_MYSQLIP).trim();
            String user = dbProperties.getProperty(SetupProperties.DATABASE_USER);
            String pass = dbProperties.getProperty(SetupProperties.DATABASE_PASS);
            String tmpTickTime = gameProperties.getProperty(SetupProperties.GAME_TICKTIME);
            String tmpHeight = gameProperties.getProperty(SetupProperties.GAME_UNIVERSE_HEIGHT);
            String tmpWidth = gameProperties.getProperty(SetupProperties.GAME_UNIVERSE_WIDTH);
            if (tmpTickTime == null) {
                tmpTickTime = tickTime;
            }
            if (tmpHeight == null) {
                tmpHeight = "1000";
            }
            if (tmpWidth == null) {
                tmpWidth = "1000";
            }

            try {
                Logger.getLogger().write("url : " + url);
                Connection conn = DriverManager.getConnection(url, user, pass);
                String sqlCreate = "CREATE DATABASE " + dbProperties.getProperty(SetupProperties.DATABASE_NAME + "");
                Logger.getLogger().write("sqlCreate : " + sqlCreate);
                if (sqlCreate != null) {
                    Statement state = conn.createStatement();
                    state.execute(sqlCreate);
                    state.close();
                    conn.close();
                }
            } catch (Exception e) {
                Logger.getLogger().write("Error creating database: " + e);
            }
            for (Class c : CreateFromModel.getClasses("at.viswars.model")) {
                try {
                    String sqlDrop = CreateFromModel.createDropSQLForClass(c);
                    if (sqlDrop != null) {
                        Statement state = DbConnect.getConnection().createStatement();
                        state.execute(sqlDrop);
                        state.close();
                    }
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.ERROR, e.toString());
                }

                String sql = CreateFromModel.createSQLForClass(c);
                if (sql != null) {
                    Statement state = DbConnect.getConnection().createStatement();
                    System.out.println("SQL : " + sql);
                    state.execute(sql);
                    state.close();
                }
            }
            try {

                int currTick = 0;
                Connection conn = DriverManager.getConnection(dbProperties.getProperty(SetupProperties.DATABASE_URL), user, pass);
                String sqlCreate = "INSERT INTO gamedata values('1','1','1','1','" + tmpTickTime + "','" + currTick + "','RUNNING','0','" + tmpHeight + "','" + tmpWidth + "')";
                Logger.getLogger().write("sqlCreate : " + sqlCreate);
                if (sqlCreate != null) {
                    Statement state = conn.createStatement();
                    state.execute(sqlCreate);
                    state.close();
                    conn.close();
                }
            } catch (Exception e) {
                Logger.getLogger().write("Error creating database: " + e);
            }
        } catch (Exception ex) {
            DebugBuffer.writeStackTrace("Error in " + SetupService.class.getName() + " Could not create table", ex);
        }


        result.add(new BaseResult("Database generated", false));
        result.add(new BaseResult("Please restart your server", false));


        return result;
    }

    public static ArrayList<BaseResult> generateConstants(Map<String, String[]> params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        try {
            DbConnect.getConnection().setAutoCommit(false);
        } catch (Exception e) {
            Logger.getLogger().write("Error while setting autocommit to false " + e);
        }
        // jo halt try catch machen :) scho kloa... owa es is afocher als i docht hob
        // dann k�nn ma am sonntag start0rn mit am klanan uni
        // mhm w�r gut ajo geh no ans ende der schleife :)

        for (Map.Entry<String, String[]> entry1 : params.entrySet()) {
            if (entry1.getKey().startsWith("table_")) {


                BufferedReader br = new BufferedReader(new StringReader(entry1.getValue()[0]));
                String token = "";
                try {
                    String table = "";
                    int i = 0;
                    while ((token = br.readLine()) != null) {
                        try {
                            String tmptable = token.substring(13);
                            tmptable = tmptable.substring(0, tmptable.indexOf(" ") - 1);
                            if (!tmptable.equals(table)) {
                                i = 0;
                                Logger.getLogger().write("Processing table : " + tmptable);
                            }
                            i++;
                            if (i % 50 == 0) {
                                Logger.getLogger().write("Processing entry - " + (i));
                            }
                            table = tmptable;
                            Statement state = DbConnect.getConnection().createStatement();
                            state.execute(token);
                            state.close();


                        } catch (Exception ex) {
                            DebugBuffer.writeStackTrace("Error in " + SetupService.class.getName(), ex);
                            break;
                        }
                    }
                } catch (Exception ex) {
                    DebugBuffer.writeStackTrace("Error in " + SetupService.class.getName(), ex);
                    break;
                }
            }



        }

        try {
            Statement state = DbConnect.getConnection().createStatement();
            state.execute("COMMIT");
            state.close();
            DbConnect.getConnection().setAutoCommit(true);
        } catch (Exception e) {
            Logger.getLogger().write("Error while commiting " + e);
        }
        Logger.getLogger().write("Finished inserted");
        result.add(new BaseResult("Constants inserted", false));
        result.add(new BaseResult("Please restart your server", false));
        result.add(new BaseResult("Create an account via login.jsp", false));
        return result;

    }

    public static ConfigurationResult checkServer() {
        boolean databaseConnectionOk = false;
        GameInit.initGame();

        try {
            Connection c = DbConnect.getConnection();
            databaseConnectionOk = true;
        } catch (Exception e) {
        }

        return new ConfigurationResult(databaseConnectionOk);
    }

    public static synchronized BaseResult buildParametrizedStartingSystem(int userId, Map<String, String[]> params) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        BaseResult br = new BaseResult("OK", false);

        try {
            th.startTransaction();

            Integer planetSizeValue = null;
            Integer rareRessValue = null;
            Integer addPopValue = null;
            Integer ironRessValue = null;

            for (final Map.Entry<String, String[]> entry : params.entrySet()) {
                String parName = entry.getKey();
                String valueStr = entry.getValue()[0];
                int value = 0;

                try {
                    if (parName.equalsIgnoreCase("homeplanet")) {
                        planetSizeValue = Integer.parseInt(valueStr);
                    } else if (parName.equalsIgnoreCase("rareress")) {
                        rareRessValue = Integer.parseInt(valueStr);
                    } else if (parName.equalsIgnoreCase("addPop")) {
                        addPopValue = Integer.parseInt(valueStr);
                    } else if (parName.equalsIgnoreCase("ironress")) {
                        ironRessValue = Integer.parseInt(valueStr);
                    }
                } catch (NumberFormatException nfe) {
                    DebugBuffer.error("Invalid parameter on start system selection: " + nfe.getMessage());
                    th.endTransaction();
                    return new BaseResult("Ung&uuml;tige Parameter f&uuml;r Systemerstellung", true);
                }

                System.out.println("PARAMETER: " + parName + " valueStr=" + valueStr);
            }

            // All values cast correctly calculate validity of selection
            int total = BASE_POINTS + planetSizeValue + rareRessValue + addPopValue + ironRessValue;
            if (total < 0) {
                th.endTransaction();
                return new BaseResult("Ung&uuml;tige Parameter f&uuml;r Systemerstellung", true);
            }
            // Check correctness of options
            if (((planetSizeValue != BASE_SMALL_PLANET) && (planetSizeValue != BASE_MEDIUM_PLANET) && (planetSizeValue != BASE_LARGE_PLANET))
                    || ((rareRessValue != BASE_NO_RARE) && (rareRessValue != BASE_NORMAL_RARE) && (rareRessValue != BASE_MUCH_RARE))
                    || ((ironRessValue != BASE_LOW_IRON) && (ironRessValue != BASE_NORMAL_IRON) && (ironRessValue != BASE_HIGH_IRON))
                    || (((addPopValue % 2) != 0) || (addPopValue > 0))) {
                th.endTransaction();
                return new BaseResult("Ung&uuml;tige Parameter f&uuml;r Systemerstellung", true);
            }

            // Selection was syntactically and semantic correct .. proceed
            double ironMultiplicator = 1d;
            double rareMultiplicator = 1d;
            long addPopulationToAssign = 0l;
            long mainPlanetPop = 0l;

            switch (planetSizeValue) {
                case (BASE_SMALL_PLANET):
                    mainPlanetPop = 7500000000l;
                    break;
                case (BASE_MEDIUM_PLANET):
                    mainPlanetPop = 8500000000l;
                    break;
                case (BASE_LARGE_PLANET):
                    mainPlanetPop = 9500000000l;
                    break;
            }

            switch (rareRessValue) {
                case (BASE_NO_RARE):
                    rareMultiplicator = 0d;
                    break;
                case (BASE_NORMAL_RARE):
                    rareMultiplicator = 1d;
                    break;
                case (BASE_MUCH_RARE):
                    rareMultiplicator = 3d;
                    break;
            }

            switch (ironRessValue) {
                case (BASE_LOW_IRON):
                    ironMultiplicator = 0.5d;
                    break;
                case (BASE_NORMAL_RARE):
                    ironMultiplicator = 1d;
                    break;
                case (BASE_HIGH_IRON):
                    ironMultiplicator = 2d;
                    break;
            }

            long ironGenerated = 0l;
            long ironToSpread = (long) (5000000000l * ironMultiplicator);
            long howaToSpread = (long) (20000 * rareMultiplicator);
            long ynkeToSpread = (long) ((15 * 175000) * rareMultiplicator);

            addPopulationToAssign = 1000000000l * (addPopValue / -2);

            HashMap<Planet, Long> rockPlanets = new HashMap<Planet, Long>();

            // Get system of user
            User u = uDAO.findById(userId);
            PlayerPlanet pp = ppDAO.findByUserId(userId).get(0);
            Planet p = pDAO.findById(pp.getPlanetId());
            at.viswars.model.System sys = sDAO.findById(p.getSystemId());

            // Cleaning the System
            ArrayList<Planet> pList = pDAO.findBySystemId(sys.getId());
            for (Planet pDel : pList) {
                pDAO.remove(pDel);

                ArrayList<PlanetRessource> prList = prDAO.findByPlanetId(pDel.getId());
                for (PlanetRessource pr : prList) {
                    prDAO.remove(pr);
                }
            }

            // Parameters have been set
            // Create M Planet first (Orbit level 5-7) (Temperature 14-16)
            Planet pMain = new Planet();

            int temperature = (int) (Math.floor(14d + Math.random() * 3d));
            int orbitLevel = (int) (Math.floor(5 + Math.random() * 3d));

            pMain.setId(getPlanetId(sys.getId(), orbitLevel));
            pMain.setSystemId(sys.getId());
            pMain.setAvgTemp(temperature);
            pMain.setOrbitLevel(orbitLevel);
            pMain.setLandType("M");
            pMain.setAtmosphereType("Z");
            pMain.setGrowthBonus(0f);
            pMain.setResearchBonus(0f);
            pMain.setProductionBonus(0f);

            // determine diameter
            for (int i = 10000; i < 20000; i += (int) Math.floor(Math.random() * 100)) {
                pMain.setDiameter(i);
                if (pMain.getMaxPopulation() > mainPlanetPop) {
                    break;
                }
            }

            System.out.println("SET ORBITLEVEL " + orbitLevel + " TEMPERATURE: " + temperature + " DIAMETER: " + pMain.getDiameter());

            long volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (pMain.getDiameter() / 2), 3)));
            long iron = 0l;

            int orbitLevelDiff = 6 - orbitLevel;
            if (orbitLevelDiff > 0) {
                iron = (long) (((float) volume / (float) 1000) * (1f + ((float) orbitLevelDiff / (float) 2.5)));
            } else if (orbitLevelDiff < 0) {
                iron = (long) (((float) volume / (float) 1000) / (1f + ((float) Math.abs(orbitLevelDiff) / (float) 2.5)));
            } else {
                iron = (long) ((float) volume / (float) 1000);
            }

            ironGenerated += iron;
            System.out.println("SET IRON " + iron);
            rockPlanets.put(pMain, iron);

            // Main Planet was created
            // Create other planets
            HashSet<Integer> occupiedOrbits = new HashSet<Integer>();
            occupiedOrbits.add(orbitLevel);

            // If additional Pop is != 0 create a new habitable Planet of type C with optimal temperature (3-5 degrees for C)
            // (25-27 degrees for G)
            addPopValue *= -1;
            if ((addPopValue > 0) && (addPopValue <= 6)) {
                // Create C only
                temperature = (int) (Math.floor(3d + Math.random() * 3d));
                do {
                    orbitLevel = (int) (Math.floor(5 + Math.random() * 3d));
                } while (occupiedOrbits.contains(orbitLevel));

                Planet pAdd1 = new Planet();
                pAdd1.setId(getPlanetId(sys.getId(), orbitLevel));
                pAdd1.setSystemId(sys.getId());
                pAdd1.setAvgTemp(temperature);
                pAdd1.setOrbitLevel(orbitLevel);
                pAdd1.setLandType("C");
                pAdd1.setAtmosphereType("Z");
                pAdd1.setGrowthBonus(0f);
                pAdd1.setResearchBonus(0f);
                pAdd1.setProductionBonus(0f);

                for (int i = 6000; i < 20000; i += (int) Math.floor(Math.random() * 100)) {
                    pAdd1.setDiameter(i);
                    if (pAdd1.getMaxPopulation() > addPopulationToAssign) {
                        break;
                    }
                }

                orbitLevelDiff = 6 - orbitLevel;
                if (orbitLevelDiff > 0) {
                    iron = (long) (((float) volume / (float) 1000) * (1f + ((float) orbitLevelDiff / (float) 2.5)));
                } else if (orbitLevelDiff < 0) {
                    iron = (long) (((float) volume / (float) 1000) / (1f + ((float) Math.abs(orbitLevelDiff) / (float) 2.5)));
                } else {
                    iron = (long) ((float) volume / (float) 1000);
                }

                ironGenerated += iron;
                rockPlanets.put(pAdd1, iron);

                System.out.println("[C] SET ORBITLEVEL " + orbitLevel + " TEMPERATURE: " + temperature + " DIAMETER: " + pAdd1.getDiameter());
                occupiedOrbits.add(orbitLevel);
            } else if ((addPopValue > 6) && (addPopValue <= 10)) {
                // Create C and G while C holds about 70% of population
                long cPop = (long) (addPopulationToAssign * 0.7d);
                long gPop = (long) (addPopulationToAssign * 0.3d);

                // Create C
                temperature = (int) (Math.floor(3d + Math.random() * 3d));
                do {
                    orbitLevel = (int) (Math.floor(5 + Math.random() * 3d));
                } while (occupiedOrbits.contains(orbitLevel));

                Planet pAdd1 = new Planet();
                pAdd1.setId(getPlanetId(sys.getId(), orbitLevel));
                pAdd1.setSystemId(sys.getId());
                pAdd1.setAvgTemp(temperature);
                pAdd1.setOrbitLevel(orbitLevel);
                pAdd1.setLandType("C");
                pAdd1.setAtmosphereType("Z");
                pAdd1.setGrowthBonus(0f);
                pAdd1.setResearchBonus(0f);
                pAdd1.setProductionBonus(0f);

                for (int i = 6000; i < 20000; i += (int) Math.floor(Math.random() * 100)) {
                    pAdd1.setDiameter(i);
                    if (pAdd1.getMaxPopulation() > cPop) {
                        break;
                    }
                }

                orbitLevelDiff = 6 - orbitLevel;
                if (orbitLevelDiff > 0) {
                    iron = (long) (((float) volume / (float) 1000) * (1f + ((float) orbitLevelDiff / (float) 2.5)));
                } else if (orbitLevelDiff < 0) {
                    iron = (long) (((float) volume / (float) 1000) / (1f + ((float) Math.abs(orbitLevelDiff) / (float) 2.5)));
                } else {
                    iron = (long) ((float) volume / (float) 1000);
                }

                ironGenerated += iron;
                rockPlanets.put(pAdd1, iron);

                System.out.println("[C] SET ORBITLEVEL " + orbitLevel + " TEMPERATURE: " + temperature + " DIAMETER: " + pAdd1.getDiameter());
                occupiedOrbits.add(orbitLevel);

                // Create G
                temperature = (int) (Math.floor(25d + Math.random() * 3d));
                do {
                    orbitLevel = (int) (Math.floor(5 + Math.random() * 3d));
                } while (occupiedOrbits.contains(orbitLevel));

                Planet pAdd2 = new Planet();
                pAdd2.setId(getPlanetId(sys.getId(), orbitLevel));
                pAdd2.setSystemId(sys.getId());
                pAdd2.setAvgTemp(temperature);
                pAdd2.setOrbitLevel(orbitLevel);
                pAdd2.setLandType("G");
                pAdd2.setAtmosphereType("Z");
                pAdd2.setGrowthBonus(0f);
                pAdd2.setResearchBonus(0f);
                pAdd2.setProductionBonus(0f);

                for (int i = 6000; i < 20000; i += (int) Math.floor(Math.random() * 100)) {
                    pAdd2.setDiameter(i);
                    if (pAdd2.getMaxPopulation() > cPop) {
                        break;
                    }
                }

                orbitLevelDiff = 6 - orbitLevel;
                if (orbitLevelDiff > 0) {
                    iron = (long) (((float) volume / (float) 1000) * (1f + ((float) orbitLevelDiff / (float) 2.5)));
                } else if (orbitLevelDiff < 0) {
                    iron = (long) (((float) volume / (float) 1000) / (1f + ((float) Math.abs(orbitLevelDiff) / (float) 2.5)));
                } else {
                    iron = (long) ((float) volume / (float) 1000);
                }

                ironGenerated += iron;
                rockPlanets.put(pAdd2, iron);

                System.out.println("[G] SET ORBITLEVEL " + orbitLevel + " TEMPERATURE: " + temperature + " DIAMETER: " + pAdd2.getDiameter());
                occupiedOrbits.add(orbitLevel);
            }

            // Create Gas giants
            int noOfGasGiants = 0;

            if (rareMultiplicator == 1d) {
                noOfGasGiants = 1;
            } else if (rareMultiplicator == 3d) {
                noOfGasGiants = 3;
            }

            int totalCreatedYnke = 0;
            HashMap<Planet, Integer> gasGiants = new HashMap<Planet, Integer>();

            while (noOfGasGiants > 0) {
                int diameter = 0;

                do {
                    if (Math.random() > 0.9d) {
                        diameter = 50000 + (int) (Math.random() * 150000);
                        orbitLevel = (int) Math.ceil(Math.random() * 7d);
                    } else {
                        diameter = 50000 + (int) (Math.random() * 250000);
                        orbitLevel = (int) Math.ceil(Math.random() * 5d) + 7;
                    }
                } while (occupiedOrbits.contains(orbitLevel));

                Planet ggPlanet = new Planet();
                ggPlanet.setId(getPlanetId(sys.getId(), orbitLevel));
                ggPlanet.setSystemId(sys.getId());
                ggPlanet.setAvgTemp(getTemperatureForOrbit(orbitLevel));
                ggPlanet.setOrbitLevel(orbitLevel);
                ggPlanet.setDiameter(diameter);

                if (ggPlanet.getDiameter() >= 50000 && ggPlanet.getDiameter() <= 150000) {
                    ggPlanet.setLandType("B");
                }
                if (ggPlanet.getDiameter() > 150000) {
                    ggPlanet.setLandType("A");
                }

                ggPlanet.setAtmosphereType("Z");
                ggPlanet.setGrowthBonus(0f);
                ggPlanet.setResearchBonus(0f);
                ggPlanet.setProductionBonus(0f);

                noOfGasGiants--;

                int currYnke = (diameter * 10) + (int) (Math.random() * (diameter * 10));
                totalCreatedYnke += currYnke;

                System.out.println("Create " + ggPlanet.getLandType() + "-Type gasgiant at orbitLevel " + orbitLevel + " DIAMETER: " + diameter);
                gasGiants.put(ggPlanet, currYnke);
                occupiedOrbits.add(orbitLevel);
            }

            System.out.println("Total Ynke generated " + totalCreatedYnke + " ynkeToSpread " + ynkeToSpread);
            int ynkeAdjust = (int) ((ynkeToSpread - totalCreatedYnke) / (double) gasGiants.size());
            System.out.println("Adjusting each giant by " + ynkeAdjust);

            for (Map.Entry<Planet, Integer> planetEntry : gasGiants.entrySet()) {
                planetEntry.setValue(planetEntry.getValue() + ynkeAdjust);
            }

            // Basic planets created now fill up with other planets
            ArrayList<Integer> freeOrbits = new ArrayList<Integer>();

            for (int i = 1; i < 12; i++) {
                freeOrbits.add(i);
            }
            for (Integer occ : occupiedOrbits) {
                freeOrbits.remove(occ);
            }

            // Select random Orbit and place a new random Planet (J, E or L)
            while (freeOrbits.size() > 0) {
                int freeOrbit = freeOrbits.get((int) Math.floor(Math.random() * freeOrbits.size()));

                if (Math.random() > 0.3d) {
                    Planet pNew = createRandomRockPlanetAtOrbit(freeOrbit);
                    pNew.setId(getPlanetId(sys.getId(), freeOrbit));
                    pNew.setSystemId(sys.getId());

                    volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (pNew.getDiameter() / 2), 3)));

                    orbitLevelDiff = 6 - freeOrbit;
                    if (orbitLevelDiff > 0) {
                        iron = (long) (((float) volume / (float) 1000) * (1f + ((float) orbitLevelDiff / (float) 2.5)));
                    } else if (orbitLevelDiff < 0) {
                        iron = (long) (((float) volume / (float) 1000) / (1f + ((float) Math.abs(orbitLevelDiff) / (float) 2.5)));
                    } else {
                        iron = (long) ((float) volume / (float) 1000);
                    }

                    ironGenerated += iron;
                    rockPlanets.put(pNew, iron);
                    System.out.println("Create " + pNew.getLandType() + " at orbitLevel " + freeOrbit + " DIAMETER: " + pNew.getDiameter());

                    if (ironGenerated > ironToSpread) {
                        break;
                    }
                }

                freeOrbits.remove(new Integer(freeOrbit));
            }

            System.out.println("Total Iron generated " + ironGenerated + " ironToSpread " + ironToSpread);
            long ironAdjust = (int) ((ironToSpread - ironGenerated) / (double) rockPlanets.size());
            System.out.println("Adjusting each planet by " + ironAdjust);

            if (ironAdjust < 0) {
                boolean ironTooHigh = true;
                while (ironTooHigh) {
                    ArrayList<Planet> pListTmp = new ArrayList<Planet>();
                    pListTmp.addAll(rockPlanets.keySet());

                    Planet pRand = pListTmp.get((int) Math.floor(Math.random() * pListTmp.size()));
                    long ironOnCurrent = rockPlanets.get(pRand);

                    if (!pRand.getLandType().equalsIgnoreCase("M")) {
                        long newIron = (long) (ironOnCurrent * 0.9d);
                        long diff = ironOnCurrent - newIron;

                        if (diff > (ironAdjust * -1l)) {
                            newIron = ironAdjust * -1l;
                            ironTooHigh = false;
                        } else {
                            ironAdjust += diff;
                        }

                        rockPlanets.put(pRand, newIron);
                    }
                }
            } else {
                for (Map.Entry<Planet, Long> planetEntry : rockPlanets.entrySet()) {
                    planetEntry.setValue(planetEntry.getValue() + ironAdjust);
                }
            }

            // Write planets
            int newMain = -1;

            for (Map.Entry<Planet, Long> planetEntry : rockPlanets.entrySet()) {
                Planet pTmp = planetEntry.getKey();

                boolean takeId = false;
                if (pTmp.equals(pMain)) {
                    takeId = true;
                }

                pTmp = pDAO.add(pTmp);

                if (takeId) {
                    newMain = pTmp.getId();
                }

                System.out.println("Adding resources for planet " + pTmp.getId());

                PlanetRessource pr = new PlanetRessource();
                pr.setPlanetId(pTmp.getId());
                pr.setRessId(Ressource.PLANET_IRON);
                pr.setType(EPlanetRessourceType.PLANET);
                pr.setQty(planetEntry.getValue());
                prDAO.add(pr);
            }

            if (newMain == -1) {
                throw new Exception("Invalid state");
            }

            for (Map.Entry<Planet, Integer> planetEntry : gasGiants.entrySet()) {
                Planet pTmp = planetEntry.getKey();
                pTmp = pDAO.add(pTmp);

                PlanetRessource pr = new PlanetRessource();
                pr.setPlanetId(pTmp.getId());
                pr.setRessId(Ressource.PLANET_YNKELONIUM);
                pr.setType(EPlanetRessourceType.PLANET);
                pr.setQty((long) planetEntry.getValue());
                prDAO.add(pr);
            }

            HashMap<Planet, Long> howaDistri = new HashMap<Planet, Long>();
            ArrayList<Planet> planets = new ArrayList<Planet>();
            planets.addAll(gasGiants.keySet());
            planets.addAll(rockPlanets.keySet());

            long howaToSpreadOrg = howaToSpread;
            if (howaToSpread > 0) {
                // Select random planet and place 20-40% of distributable howal
                Planet pTmp = planets.get((int) Math.floor(Math.random() * planets.size()));
                long qtyToAdd = (long) (howaToSpreadOrg * (0.2d + Math.random() * 0.2d));

                if (qtyToAdd > howaToSpread) {
                    qtyToAdd = howaToSpread;
                }

                if (howaDistri.containsKey(pTmp)) {
                    howaDistri.put(pTmp, howaDistri.get(pTmp) + qtyToAdd);
                } else {
                    howaDistri.put(pTmp, qtyToAdd);
                }

                howaToSpread -= qtyToAdd;
            }

            for (Map.Entry<Planet, Long> planetEntry : howaDistri.entrySet()) {
                Planet pTmp = planetEntry.getKey();

                PlanetRessource pr = new PlanetRessource();
                pr.setPlanetId(pTmp.getId());
                pr.setRessId(Ressource.PLANET_HOWALGONIUM);
                pr.setType(EPlanetRessourceType.PLANET);
                pr.setQty((long) planetEntry.getValue());
                prDAO.add(pr);
            }

            pp.setPlanetId(newMain);
            pp.setName("Planet #" + newMain);
            pp.setPopulation(5000000000l);
            pp.setMoral(100);
            pp.setTax(37);
            pp.setGrowth(5f);
            ppDAO.update(pp);

            // Finalize planet
            PlanetRessource food = new PlanetRessource();
            food.setPlanetId(newMain);
            food.setType(EPlanetRessourceType.INSTORAGE);
            food.setRessId(Ressource.FOOD);
            food.setQty(100000L);
            prDAO.add(food);

            PlanetRessource tmpIron = prDAO.findBy(p.getId(), Ressource.IRON, EPlanetRessourceType.INSTORAGE);
            if (tmpIron != null) {
                //Found Ressource which should NOT be there
                prDAO.remove(tmpIron);
            }
            PlanetRessource ironStore = new PlanetRessource();
            ironStore.setPlanetId(newMain);
            ironStore.setType(EPlanetRessourceType.INSTORAGE);
            ironStore.setRessId(Ressource.IRON);
            ironStore.setQty(2000000L);
            prDAO.add(ironStore);

            PlanetRessource tmpSteel = prDAO.findBy(p.getId(), Ressource.STEEL, EPlanetRessourceType.INSTORAGE);
            if (tmpSteel != null) {
                prDAO.remove(tmpSteel);
            }
            PlanetRessource steelStore = new PlanetRessource();
            steelStore.setPlanetId(newMain);
            steelStore.setType(EPlanetRessourceType.INSTORAGE);
            steelStore.setRessId(Ressource.STEEL);
            steelStore.setQty(1000000L);
            prDAO.add(steelStore);

            // Gebäude erstellen
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_AGROCOMPLEX, 51));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_BIGIRONMINE, 15));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_STEELCOMPLEX, 1));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_NUCLEARPOWERPLANT, 100));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_BIGRESSOURCESTORAGE, 4));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_BARRACKS, 2));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_LOCALADMINISTRATION, 1));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_PLANETADMINISTRATION, 1));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_PLANETGOVERNMENT, 1));
            pcDAO.add(new PlanetConstruction(newMain, Construction.ID_OBSERVATORY, 1));

            // Troops
            ptDAO.add(new PlayerTroop(newMain, userId, GroundTroop.ID_MILIZ, 10000000));
            ptDAO.add(new PlayerTroop(newMain, userId, GroundTroop.ID_LIGHTINFANTRY, 5000000));
            ptDAO.add(new PlayerTroop(newMain, userId, GroundTroop.ID_HEAVYINFANTRY, 2500000));
            ptDAO.add(new PlayerTroop(newMain, userId, GroundTroop.ID_LIGHTTANK, 50000));
            //     ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_ROCKETTANK, 25000));
            ptDAO.add(new PlayerTroop(newMain, userId, GroundTroop.ID_HEAVYTANK, 25000));
            ptDAO.add(new PlayerTroop(newMain, userId, GroundTroop.ID_INTERCEPTORPLANE, 10000));
            ptDAO.add(new PlayerTroop(newMain, userId, GroundTroop.ID_BOMBER, 5000));

            // Viewtable befüllen
            int currTick = GameUtilities.getCurrentTick2();

            sys.setVisibility(userId);
            sDAO.update(sys);
            ArrayList<at.viswars.model.System> systemList = SystemService.getSystemsAround(sys.getId(), 100d, ESystemSorting.NO_SORT);

            for (at.viswars.model.System sysTmp : systemList) {
                vtDAO.deleteSystem(userId, sysTmp.getId());
                vtDAO.addSystem(userId, sysTmp.getId(), currTick);
            }

            // Write user
            u.setUserConfigured(true);
            uDAO.update(u);

            UserData ud = udDAO.findByUserId(userId);
            ud.setCredits(100000000l);
            udDAO.update(ud);

            try {
                PlanetLoyality plOld = pLoyalDAO.getByPlanetAndUserId(p.getId(), userId);
                if (plOld != null) {
                    pLoyalDAO.remove(plOld);
                }
            } catch (Exception e) {
            }

            PlanetLoyality pLoyal = new PlanetLoyality();
            pLoyal.setPlanetId(newMain);
            pLoyal.setUserId(userId);

            PlanetLoyality plTmp = pLoyalDAO.getByPlanetAndUserId(newMain, userId);

            if (plTmp == null) {
                pLoyal.setValue(100d);
                pLoyalDAO.add(pLoyal);
            } else {
                plTmp.setValue(100d);
                pLoyalDAO.update(plTmp);
            }

            PlanetLog pl = new PlanetLog();
            pl.setUserId(userId);
            pl.setPlanetId(newMain);
            pl.setType(EPlanetLogType.COLONIZED);
            pl.setTime(GameUtilities.getCurrentTick2());
            plDAO.add(pl);

            th.execute();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while adjusting starting system: ", e);
            try {
                th.rollback();
                DebugBuffer.error(th.getTransactionLog());
            } catch (Exception te) {
                DebugBuffer.writeStackTrace("Transaction rollback failed: ", te);
            }

            br = new BaseResult("Unerwarteter Fehler bei Systemerstellung " + e.getMessage(), true);
        } finally {
            th.endTransaction();
        }

        return br;
    }

    private static Planet createRandomRockPlanetAtOrbit(int orbitLevel) {
        Planet pNew = new Planet();

        int diameter = 0;

        if (diameter == 0) {
            diameter = 6000 + (int) (Math.random() * 14000);
        }
        String landType = "";

        if (landType.equalsIgnoreCase("")) {
            if (orbitLevel < 5) {
                int zz = 1 + (int) (Math.random() * 4);
                if (zz <= 2) {
                    landType = "J";
                } else {
                    landType = "E";
                }
            }
            int zz = 1 + (int) (Math.random() * 100);
            if ((orbitLevel >= 5) && (orbitLevel <= 7)) {
                landType = "E";
            }

            if (orbitLevel > 7) {
                landType = "L";
            }
        }

        // Bis hierher ist definiert:
        // OrbitLevel
        // Diameter (Durchmesser)
        // Typ
        int avgTemp = 0;
        switch (orbitLevel) {
            case 1:
                avgTemp = 590 + (int) (Math.random() * 100);
                break;
            case 2:
                avgTemp = 280 + (int) (Math.random() * 80);
                break;
            case 3:
                avgTemp = 130 + (int) (Math.random() * 60);
                break;
            case 4:
                avgTemp = 60 + (int) (Math.random() * 40);
                break;
            case 5:
                avgTemp = 30 + (int) (Math.random() * 20);
                break;
            case 6:
                avgTemp = 10 + (int) (Math.random() * 10);
                break;
            case 7:
                avgTemp = 0 + (int) (Math.random() * 10);
                break;
            case 8:
                avgTemp = -50 + (int) (Math.random() * 20);
                break;
            case 9:
                avgTemp = -95 + (int) (Math.random() * 30);
                break;
            case 10:
                avgTemp = -140 + (int) (Math.random() * 40);
                break;
            case 11:
                avgTemp = -185 + (int) (Math.random() * 50);
                break;
            case 12:
                avgTemp = -230 + (int) (Math.random() * 60);
                break;
        }

        pNew.setOrbitLevel(orbitLevel);
        pNew.setAvgTemp(avgTemp);
        pNew.setDiameter(diameter);
        pNew.setLandType(landType);
        pNew.setGrowthBonus(0f);
        pNew.setProductionBonus(0f);
        pNew.setResearchBonus(0f);
        pNew.setAtmosphereType("Z");

        return pNew;
    }

    private static int getTemperatureForOrbit(int orbitLevel) {
        int avgTemp = 0;

        switch (orbitLevel) {
            case 1:
                avgTemp = 590 + (int) (Math.random() * 100);
                break;
            case 2:
                avgTemp = 280 + (int) (Math.random() * 80);
                break;
            case 3:
                avgTemp = 130 + (int) (Math.random() * 60);
                break;
            case 4:
                avgTemp = 60 + (int) (Math.random() * 40);
                break;
            case 5:
                avgTemp = 30 + (int) (Math.random() * 20);
                break;
            case 6:
                avgTemp = 10 + (int) (Math.random() * 10);
                break;
            case 7:
                avgTemp = 0 + (int) (Math.random() * 10);
                break;
            case 8:
                avgTemp = -50 + (int) (Math.random() * 20);
                break;
            case 9:
                avgTemp = -95 + (int) (Math.random() * 30);
                break;
            case 10:
                avgTemp = -140 + (int) (Math.random() * 40);
                break;
            case 11:
                avgTemp = -185 + (int) (Math.random() * 50);
                break;
            case 12:
                avgTemp = -230 + (int) (Math.random() * 60);
                break;
        }

        return avgTemp;
    }

    public static PropertiesCheckResult checkProperties() {

        PropertiesCheckResult pcr = new PropertiesCheckResult();

        try {
            Properties dbProperties = new Properties();
            dbProperties.load(SetupService.class.getResourceAsStream("/db.properties"));

            if (dbProperties == null) {
                pcr.getDbResult().add(new BaseResult("db.properties file at : " + SetupService.class.getResourceAsStream("/db.properties").toString() + " is null", true));
                pcr.setIsDbPropertiesFileOkay(false);
            } else {
                pcr.getDbResult().add(new BaseResult("Found db.properties with following values:", false));
                for (Map.Entry<Object, Object> entry : dbProperties.entrySet()) {
                    pcr.getDbResult().add(new BaseResult(entry.getKey() + " => " + entry.getValue(), false));
                }
            }

        } catch (Exception e) {
            pcr.setIsDbPropertiesFileOkay(false);
            pcr.getDbResult().add(new BaseResult("Error loading db.properties error : " + e, true));
        }

        try {

            Properties gameProperties = new Properties();
            gameProperties.load(SetupService.class.getResourceAsStream("/game.properties"));
            if (gameProperties == null) {
                pcr.setIsGamePropertiesFileOkay(false);
                pcr.getGameResult().add(new BaseResult("game.properties file at : " + SetupService.class.getResourceAsStream("/game.properties").toString() + " is null", true));
            } else {
                pcr.getGameResult().add(new BaseResult("Found game.properties with following values:", false));
                for (Map.Entry<Object, Object> entry : gameProperties.entrySet()) {
                    pcr.getGameResult().add(new BaseResult(entry.getKey() + " => " + entry.getValue(), false));
                }
            }
        } catch (Exception e) {
            pcr.setIsGamePropertiesFileOkay(false);
            pcr.getGameResult().add(new BaseResult("Error loading game.properties error : " + e, true));
        }



        return pcr;

    }

    private static int getPlanetId(int systemId, int orbitLevel) {
        return ((systemId - 1) * 12) + orbitLevel;
    }
}
