/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.GameUtilities;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.FleetDetailDAO;
import at.viswars.dao.GameDataDAO;
import at.viswars.model.FleetDetail;
import at.viswars.model.GameData;
import java.util.ArrayList;

/**
 *
 * @author LAZYPAD
 */
public class StartService {

    //Sets the tick to the actual time so no tickprocessing is done
    public static void updateTickToActualTime() {

        int currTick = GameUtilities.getCurrentTick2();

        GameDataDAO gdDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
        FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);

        GameData gd = (GameData) gdDAO.findAll().get(0);
        int lastUpdated = gd.getLastUpdatedTick();

        int timeWarp = currTick - lastUpdated;
        for (FleetDetail fd : (ArrayList<FleetDetail>) fdDAO.findAll()) {
            fd.setStartTime(fd.getStartTime() + timeWarp);
            fdDAO.update(fd);
        }

        gd.setLastUpdatedTick(currTick);
        gdDAO.update(gd);

        DAOFactory.dropAll();
    }
}
