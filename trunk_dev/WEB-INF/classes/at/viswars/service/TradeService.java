package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.GenerateMessage;
import at.viswars.ML;
import at.viswars.ships.ShipData;
import at.viswars.TradeShipData;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.TradeRouteDAO;
import at.viswars.dao.TradeRouteDetailDAO;
import at.viswars.dao.TradeRouteShipDAO;
import at.viswars.dao.UserDAO;
import at.viswars.enumeration.EAddLocation;
import at.viswars.enumeration.EMessageType;
import at.viswars.enumeration.ETradeRouteStatus;
import at.viswars.enumeration.ETradeRouteType;
import at.viswars.exceptions.InvalidInstantiationException;
import at.viswars.exceptions.PlanetNameNotUniqueException;
import at.viswars.fleet.LoadingInformation;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Ressource;
import at.viswars.model.TradePost;
import at.viswars.model.TradeRoute;
import at.viswars.model.TradeRouteDetail;
import at.viswars.model.TradeRouteShip;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.result.BaseResult;
import at.viswars.result.DiplomacyResult;
import at.viswars.result.TradeRouteResult;
import at.viswars.trade.ShipsToRouteEntry;
import at.viswars.trade.TradeRouteExt;
import at.viswars.trade.TradeRouteTransmitterInfo;
import at.viswars.trade.TradeUtilities;
import at.viswars.utilities.DiplomacyUtilities;
import at.viswars.utilities.ShippingUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

/**
 *
 * @author Bullet
 */
public class TradeService extends Service {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static TradeRouteShipDAO trsDAO = (TradeRouteShipDAO) DAOFactory.get(TradeRouteShipDAO.class);
    private static TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    private static TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    public static boolean hasTradePost(int planetId) {

        TradePost tr = tradePostDAO.findByPlanetId(planetId);
        if (tr != null) {
            return true;
        } else {
            return false;
        }
    }

    public static TradePost findTradePostByPlanetId(int planetId) {
        return tradePostDAO.findByPlanetId(planetId);
    }

    public static TradePost findTradePostById(int id) {
        return tradePostDAO.findById(id);
    }
    
    public static TradeRoute getRoute(int id) {
        return trDAO.getById(id);
    }
    
    // Prototype function

    @Deprecated
    public static ArrayList<Ressource> getRessources() {
        return ressourceDAO.findAllStoreableRessources();
    }

    // 
    // START NEW METHODS
    //
    public static TradeRouteResult getAllInternalTradeRoutes(int userId, int planetId) {
        return TradeUtilities.getInternalTradeRoutes(userId, planetId);
    }

    public static TradeRouteResult getAllInternalTradeRoutes(int userId) {
        return TradeUtilities.getInternalTradeRoutes(userId);
    }

    public static TradeRouteResult getAllExternalTradeRoutes(int userId, int planetId) {
        return TradeUtilities.getExternalTradeRoutes(userId, planetId);
    }

    public static TradeRouteResult getAllExternalTradeRoutes(int userId) {
        return TradeUtilities.getExternalTradeRoutes(userId);
    }

    public static BaseResult createTradeRoute_2(int userId, Map params) {
        try {
            return new BaseResult(ML.getMLStr("transport_msg_success", userId), false);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Unerwarteter Fehler in Handelsrouten", e);
            return new BaseResult("FEHLER: " + e.getMessage(), true);
        }
    }

    public static BaseResult createTradeRoute(int userId, Map params) {
        try {
            //At least one ressentry has to be available
            boolean ressEntryFound = false;

            //External if route to ally (Vote needed)
            boolean external = false;

            String returnMessage = (ML.getMLStr("transport_msg_success", userId));

            int sourcePlanetId = 0;
            int targetPlanetId = 0;

            HashMap<String, HashMap<String, Integer>> sortedPars = new HashMap<String, HashMap<String, Integer>>();

            for (Object entry : params.entrySet()) {
                Map.Entry me = (Map.Entry) entry;
                String key = (String) me.getKey();

                if (((!key.contains("fromTo"))
                        && (!key.contains("toFrom")))
                        && !(key.equalsIgnoreCase("targetId")
                        || key.equalsIgnoreCase("sourceId"))) {
                    continue;
                }

                Integer value = null;

                // Read out planet data
                if (key.equalsIgnoreCase("targetId") || key.equalsIgnoreCase("sourceId")) {
                    PlayerPlanet pp = null;

                    try {
                        pp = TradeUtilities.resolvePlanetNameId(((String[]) me.getValue())[0], userId, true);
                        if (pp == null) {
                            pp = TradeUtilities.resolvePlanetNameId(((String[]) me.getValue())[0], userId, false);
                        }
                    } catch (PlanetNameNotUniqueException pnnue) {
                        return new BaseResult(ML.getMLStr("transport_err_planetnamenotunique", userId), true);
                    }

                    /*
                    try {
                    value = Integer.parseInt(((String[]) me.getValue())[0]);
                    pp = ppDAO.findByPlanetId(value);
                    if (pp == null) {
                    }
                    } catch (NumberFormatException nfe) {
                    ArrayList<PlayerPlanet> ppList = ppDAO.findByName(((String[]) me.getValue())[0]);
                    if (ppList.size() > 0) {
                    pp = ppList.get(0);
                    }
                    }
                     */

                    if (pp == null) {
                        if (key.equalsIgnoreCase("targetId")) {
                            return new BaseResult(ML.getMLStr("transport_err_targetplanetnotfound", userId), true);
                        } else {
                            return new BaseResult(ML.getMLStr("transport_err_startplanetnotfound", userId), true);
                        }
                    }

                    if (key.equalsIgnoreCase("targetId")) {
                        targetPlanetId = pp.getPlanetId();

                        if (pp.getUserId() != userId) {
                            DiplomacyResult dr = DiplomacyUtilities.getDiplomacyResult(pp.getUserId(), userId);

                            if (dr.isAttackTradeFleets() == true) {
                                return new BaseResult(ML.getMLStr("transport_err_planetnotyoursorally", userId), true);
                            } else {
                                external = true;
                                returnMessage = ML.getMLStr("transport_msg_routecreatedtoplayer", userId);
                                returnMessage = returnMessage.replace("%USER%", userDAO.findById(pp.getUserId()).getGameName());
                            }
                        }
                    } else if (key.equalsIgnoreCase("sourceId")) {
                        sourcePlanetId = pp.getPlanetId();
                        if (pp.getUserId() != userId) {
                            return new BaseResult(ML.getMLStr("transport_err_startplanetnotyours", userId), true);
                        }
                    }

                    continue;
                } else {
                    // DebugBuffer.addLine(DebugLevel.ERROR, "Debug: key " + key + " value " + ((String[]) me.getValue())[0]);
                    try {
                        value = Integer.parseInt(((String[]) me.getValue())[0]);
                    } catch (NumberFormatException nfe) {
                        return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
                    }
                }

                if ((value < 0) || (value > Integer.MAX_VALUE)) {
                    return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
                }

                try {
                    if (key.contains("fromTo")) {
                        HashMap<String, Integer> parameter = null;

                        if (sortedPars.containsKey("fromTo")) {
                            parameter = sortedPars.get("fromTo");
                        } else {
                            parameter = new HashMap<String, Integer>();
                        }

                        parameter.put(key.replace("fromTo", ""), value);
                        sortedPars.put("fromTo", parameter);
                    } else if (key.contains("toFrom")) {
                        HashMap<String, Integer> parameter = null;

                        if (sortedPars.containsKey("toFrom")) {
                            parameter = sortedPars.get("toFrom");
                        } else {
                            parameter = new HashMap<String, Integer>();
                        }

                        parameter.put(key.replace("toFrom", ""), value);
                        sortedPars.put("toFrom", parameter);
                    }
                } catch (NumberFormatException nfe) {
                    return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
                }
            }

            if (((sortedPars.get("fromTo").size() % 4) != 0)
                    || ((sortedPars.get("toFrom").size() % 4) != 0)) {
                return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
            }

            int fromToCount = (int) Math.floor(sortedPars.get("fromTo").size() / 4);
            int toFromCount = (int) Math.floor(sortedPars.get("toFrom").size() / 4);

            // Create Routes
            HashMap<Integer, TradeRouteDetail> fromToRaw = new HashMap<Integer, TradeRouteDetail>();
            HashMap<Integer, TradeRouteDetail> toFromRaw = new HashMap<Integer, TradeRouteDetail>();

            ArrayList<Integer> delFromToRaw = new ArrayList<Integer>();
            ArrayList<Integer> delToFromRaw = new ArrayList<Integer>();

            HashSet<Integer> ressSet = new HashSet<Integer>();

            for (Map.Entry<String, Integer> fromToPar : sortedPars.get("fromTo").entrySet()) {
                String par = fromToPar.getKey();
                int id = 0;

                if (par.startsWith("RessSelect")) {
                    id = Integer.parseInt(par.replace("RessSelect", ""));
                    if (fromToPar.getValue() == 0) {
                        delFromToRaw.add(id);
                        continue;
                    } else {
                        if (!ressEntryFound) {
                            ressEntryFound = true;
                        }
                    }
                    if (ressSet.contains(fromToPar.getValue())) {
                        return new BaseResult(ML.getMLStr("transport_err_ressourcejustonce", userId), true);
                    }

                    ressSet.add(fromToPar.getValue());
                    getTradeRouteDetailEntry(fromToRaw, id).setRessId(fromToPar.getValue());
                } else if (par.startsWith("MaxQtyTick")) {
                    id = Integer.parseInt(par.replace("MaxQtyTick", ""));
                    getTradeRouteDetailEntry(fromToRaw, id).setMaxQtyPerTick(fromToPar.getValue());
                } else if (par.startsWith("MaxDur")) {
                    id = Integer.parseInt(par.replace("MaxDur", ""));
                    getTradeRouteDetailEntry(fromToRaw, id).setMaxDuration(fromToPar.getValue());
                } else if (par.startsWith("MaxQty")) {
                    id = Integer.parseInt(par.replace("MaxQty", ""));
                    getTradeRouteDetailEntry(fromToRaw, id).setMaxQty(fromToPar.getValue());
                }
            }

            ressSet.clear();
            for (Map.Entry<String, Integer> toFromPar : sortedPars.get("toFrom").entrySet()) {
                String par = toFromPar.getKey();
                int id = 0;

                if (par.startsWith("RessSelect")) {
                    id = Integer.parseInt(par.replace("RessSelect", ""));
                    if (toFromPar.getValue() == 0) {
                        delToFromRaw.add(id);
                        continue;
                    } else {
                        if (!ressEntryFound) {
                            ressEntryFound = true;
                        }
                    }

                    if (ressSet.contains(toFromPar.getValue())) {
                        return new BaseResult(ML.getMLStr("transport_err_ressourcejustonce", userId), true);
                    }

                    ressSet.add(toFromPar.getValue());
                    getTradeRouteDetailEntry(toFromRaw, id).setRessId(toFromPar.getValue());
                } else if (par.startsWith("MaxQtyTick")) {
                    id = Integer.parseInt(par.replace("MaxQtyTick", ""));
                    getTradeRouteDetailEntry(toFromRaw, id).setMaxQtyPerTick(toFromPar.getValue());
                } else if (par.startsWith("MaxDur")) {
                    id = Integer.parseInt(par.replace("MaxDur", ""));
                    getTradeRouteDetailEntry(toFromRaw, id).setMaxDuration(toFromPar.getValue());
                } else if (par.startsWith("MaxQty")) {
                    id = Integer.parseInt(par.replace("MaxQty", ""));
                    getTradeRouteDetailEntry(toFromRaw, id).setMaxQty(toFromPar.getValue());
                }

                getTradeRouteDetailEntry(toFromRaw, id).setReversed(true);
            }

            for (Integer i : delFromToRaw) {
                fromToRaw.remove(i);
            }

            for (Integer i : delToFromRaw) {
                toFromRaw.remove(i);
            }

            // Check routes for wrong values
            ArrayList<TradeRouteDetail> allRoutes = new ArrayList<TradeRouteDetail>();
            allRoutes.addAll(fromToRaw.values());
            allRoutes.addAll(toFromRaw.values());

            for (TradeRouteDetail trd : allRoutes) {
                if ((trd.getMaxQtyPerTick() > trd.getMaxQty())
                        && (trd.getMaxQty() > 0)) {
                    trd.setMaxQtyPerTick(trd.getMaxQty());
                }
            }

            if (sourcePlanetId == targetPlanetId) {
                    return new BaseResult("Start- und Zielplanet d�rfen nicht ident sein", true);
            } else {
                if (ressEntryFound) {
                    if (external) {
                        if (trDAO.exists(sourcePlanetId, targetPlanetId, ETradeRouteType.TRANSPORT) || trDAO.exists(targetPlanetId, sourcePlanetId, ETradeRouteType.TRANSPORT)) {
                            return new BaseResult(ML.getMLStr("transport_err_externalrouteaddressource", userId), true);
                        }
                        TradeUtilities.createExternalTradeRoute(userId, sourcePlanetId, targetPlanetId, allRoutes);
                    } else {
                        return TradeUtilities.createInternalTradeRoute(userId, sourcePlanetId, targetPlanetId, allRoutes);
                    }
                } else {
                    return new BaseResult(ML.getMLStr("transport_err_atleastoneressource", userId), true);
                }
            }
            return new BaseResult(returnMessage, false);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Unerwarteter Fehler in Handelsrouten", e);
            return new BaseResult("FEHLER: " + e.getMessage(), true);
        }
    }

    private static TradeRouteDetail getTradeRouteDetailEntry(HashMap<Integer, TradeRouteDetail> routeMap, int id) {
        if (routeMap.containsKey(id)) {
            return routeMap.get(id);
        } else {
            TradeRouteDetail trd = new TradeRouteDetail();
            routeMap.put(id, trd);
            return trd;
        }
    }

    public static BaseResult switchActiveRouteDetail(int userId, int routeDetailId) {
        TradeRouteDetail trd = trdDAO.getById(routeDetailId);
        if (trd == null) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }
        TradeRoute tr = trDAO.getById(trd.getRouteId());
        if (tr.getUserId() != userId) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }

        TradeUtilities.switchRouteDetailState(routeDetailId);

        return new BaseResult(ML.getMLStr("transport_msg_changedroutestatus", userId), false);
    }

    public static BaseResult deleteRouteDetail(int userId, int routeDetailId) {
        TradeRouteDetail trd = trdDAO.getById(routeDetailId);
        if (trd == null) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }
        TradeRoute tr = trDAO.getById(trd.getRouteId());
        if (tr == null) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }
        if ((tr.getUserId() != userId && tr.getTargetUserId() != userId)
                || (tr.getUserId() != userId && tr.getTargetUserId() == 0)) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }

        TradeUtilities.deleteRouteDetail(tr, trd);

        return new BaseResult(ML.getMLStr("transport_msg_routeentrydeleted", userId), false);
    }

    public static ArrayList<ShipData> getAllTransportShips(int userId, int routeId) {
        return TradeUtilities.getTradeShips(userId, routeId);
    }

    public static BaseResult setTransmitterCapacity(int userId, int routeId, int newCap) {
        // Logger.getLogger().write("GOT HERE TRANSCAP");

        BaseResult br = null;

        TradeRouteTransmitterInfo trti = TradeUtilities.getRouteTransmitterInfo(routeId);

        int maxFreeCap = Math.min(trti.getTotalCapStart() - trti.getUsedCapStart(), trti.getTotalCapTarget() - trti.getUsedCapTarget()) + trti.getUsageThisRoute();

        TradeRoute tr = trDAO.getById(routeId);
        if (tr.getUserId() != userId) {
            return new BaseResult("Diese Route geh&ouml;rt dir nicht", true);
        }

        if ((newCap < 0) || (newCap > maxFreeCap)) {
            return new BaseResult("Ung&uuml;ltige Menge", true);
        }

        try {
            TradeRouteShip trs = trsDAO.getTransmitterEntry(routeId);
            if (trs == null) {
                TradeRouteShip trsNew = new TradeRouteShip();
                trsNew.setTradeRouteId(routeId);
                trsNew.setShipType(TradeRouteShip.SHIPTYPE_CONSTRUCTION);
                trsNew.setDesignId(0);
                trsNew.setAddLocation(EAddLocation.IGNORE);
                trsNew.setNumber(newCap);
                // Logger.getLogger().write("CREATE NEW ENTRY WITH CAP " + newCap);
                trsDAO.add(trsNew);
            } else {
                if (newCap == 0) {
                    // Logger.getLogger().write("REMOVE ENTRY");
                    trsDAO.remove(trs);
                } else {
                    // Logger.getLogger().write("UPDATE TO CAP " + newCap);
                    trs.setNumber(newCap);
                    trsDAO.update(trs);
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in setTransmitterCapacity: ", e);
            return new BaseResult("Unexpected Error: " + e.getMessage(), true);
        }

        return new BaseResult("Kapazit&auml;t zugeteilt", false);
    }

    public static synchronized BaseResult addShipsToRoute(int userId, int routeId, Map<String, ?> params) {
        try {
            ArrayList<ShipData> possibleShips = TradeUtilities.getTradeShips(userId, routeId);
            ArrayList<ShipsToRouteEntry> streList = new ArrayList<ShipsToRouteEntry>();

            for (String parName : params.keySet()) {
                if (parName.startsWith("fid")) {
                    // Start splitting of parameter to retrieve fleet and designId

                    int[] fleetPos = {0, 0};
                    int[] designPos = {0, 0};

                    fleetPos[0] = 3;

                    for (int i = 0; i < parName.length(); i++) {
                        if (parName.substring(i, i + 3).equalsIgnoreCase("did")) {
                            fleetPos[1] = i;
                            designPos[0] = i + 3;
                            designPos[1] = parName.length();
                            break;
                        }
                    }

                    int fleetId = 0;
                    int designId = 0;
                    int count = 0;

                    try {
                        fleetId = Integer.parseInt(parName.substring(fleetPos[0], fleetPos[1]));
                        designId = Integer.parseInt(parName.substring(designPos[0], designPos[1]));
                        count = Integer.parseInt(((String[]) params.get(parName))[0]);
                        if (count <= 0) {
                            continue;
                        }
                    } catch (NumberFormatException nfe) {
                        return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
                    }

                    // Check if fleet has loading if yes loading is not allowed                    
                    PlayerFleetExt pfe;
                    
                    try {
                        pfe = new PlayerFleetExt(fleetId);
                    } catch (InvalidInstantiationException iie) {
                        return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
                    }
                    
                    if (pfe.getUserId() != userId) return new BaseResult(ML.getMLStr("transport_err_nopermission", userId), true);                 
                    
                    if (count > 0) {                    
                        LoadingInformation li = pfe.getLoadingInformation();
                                        
                        if (li.hasRessourceLoading()) {
                            return new BaseResult(ML.getMLStr("transport_err_ressloaded", userId, pfe.getName()), true);
                        }
                        if (li.hasTroopLoading()) {
                            return new BaseResult(ML.getMLStr("transport_err_troopsloaded", userId, pfe.getName()), true);
                        }
                        if (li.hasHangarLoading()) {
                            return new BaseResult(ML.getMLStr("transport_err_shipsloaded", userId, pfe.getName()), true);
                        }
                    }

                    boolean removeEntry = false;

                    ShipsToRouteEntry stre = new ShipsToRouteEntry(fleetId, null, designId, count);

                    ShipData foundEntry = null;
                    for (ShipData sd : possibleShips) {
                        if (sd.getFleetId() == fleetId) {
                            if (sd.getDesignId() == designId) {
                                if (count <= sd.getCount()) {
                                    foundEntry = sd;
                                }
                            }
                        }
                    }

                    if (foundEntry == null) {
                        return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
                    } else {
                        if (foundEntry.getCount() == count) {
                            possibleShips.remove(foundEntry);
                        } else {
                            foundEntry.setCount(foundEntry.getCount() - count);
                        }

                        streList.add(stre);
                    }
                }
            }

            BaseResult brInternal = TradeUtilities.addShipsToRoute(routeId, streList);
            if (brInternal.isError()) {
                return brInternal;
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while adding ships to traderoute: ", e);
            return new BaseResult("Unerwarteter Fehler: " + e.getMessage(), true);
        }

        return new BaseResult(ML.getMLStr("transport_msg_shipsassigned", userId), false);
    }

    public static ArrayList<TradeShipData> getShipsInRoute(int userId, int routeId) {
        ArrayList<TradeShipData> trsData = TradeUtilities.getShipsInRoute(routeId);

        TradeRoute tr = trDAO.getById(routeId);
        boolean userOwnsStart = ppDAO.findByPlanetId(tr.getStartPlanet()).getUserId() == userId;
        boolean userOwnsTarget = ppDAO.findByPlanetId(tr.getTargetPlanet()).getUserId() == userId;

        for (Iterator<TradeShipData> tsdIt = trsData.iterator(); tsdIt.hasNext();) {
            TradeShipData tsd = tsdIt.next();

            if ((tr.getStartPlanet() == tsd.getHome().getPlanetId()) && !userOwnsStart) {
                tsdIt.remove();
            }
            if ((tr.getTargetPlanet() == tsd.getHome().getPlanetId()) && !userOwnsTarget) {
                tsdIt.remove();
            }
        }

        return trsData;
    }

    public static synchronized BaseResult removeShipsFromRoute(int userId, int routeId, Map<String, ?> params) {
        try {
            ArrayList<TradeShipData> possibleShips = TradeService.getShipsInRoute(userId, routeId);
            ArrayList<ShipsToRouteEntry> streList = new ArrayList<ShipsToRouteEntry>();

            TradeRoute tr = trDAO.getById(routeId);

            for (String parName : params.keySet()) {
                if (parName.startsWith("did")) {
                    // Start splitting of parameter to retrieve fleet and designId                                                                             
                    int designId = 0;
                    int count = 0;
                    int homeId = 0;

                    try {
                        // Extract String data
                        String parNameCut = parName.replace("did", "");
                        StringTokenizer st = new StringTokenizer(parNameCut, "_");

                        designId = Integer.parseInt(st.nextToken());
                        homeId = Integer.parseInt(st.nextToken());
                        count = Integer.parseInt(((String[]) params.get(parName))[0]);
                    } catch (NumberFormatException nfe) {
                        return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
                    }

                    if (count <= 0) {
                        continue;
                    }

                    EAddLocation addLoc = null;
                    if (tr.getStartPlanet() == homeId) {
                        addLoc = EAddLocation.START;
                    } else if (tr.getTargetPlanet() == homeId) {
                        addLoc = EAddLocation.TARGET;
                    }

                    ShipsToRouteEntry stre = new ShipsToRouteEntry(0, addLoc, designId, count);

                    ShipData foundEntry = null;
                    for (ShipData sd : possibleShips) {
                        if (sd.getDesignId() == designId) {
                            if (count <= sd.getCount()) {
                                foundEntry = sd;
                            }
                        }
                    }

                    if (foundEntry == null) {
                        return new BaseResult(ML.getMLStr("transport_err_invalidparameters", userId), true);
                    } else {
                        if (foundEntry.getCount() == count) {
                            possibleShips.remove(foundEntry);
                        } else {
                            foundEntry.setCount(foundEntry.getCount() - count);
                        }

                        streList.add(stre);
                    }
                }
            }

            TradeUtilities.removeShipsFromRoute(routeId, streList);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while adding ships to traderoute: ", e);
            return new BaseResult("Unerwarteter Fehler: " + e.getMessage(), true);
        }

        return new BaseResult(ML.getMLStr("transport_msg_shipsremoved", userId), false);
    }

    //Calculates transport and tradebalance
    public static HashMap<Integer, Long> findTradeBalanceForPlanet(int planetId) {
        HashMap<Integer, Long> result = new HashMap<Integer, Long>();
        ArrayList<TradeRoute> routes1 = tradeRouteDAO.findByEndPlanet(planetId);
        ArrayList<TradeRoute> routes2 = tradeRouteDAO.findByStartPlanet(planetId, ETradeRouteType.TRANSPORT);
        for (TradeRoute r : routes1) {

            TradeRouteExt tre = new TradeRouteExt(r);
            ArrayList<TradeRouteDetail> tres = tre.getRouteDetails();
            for (TradeRouteDetail detail : tres) {
                Long balance = result.get(detail.getRessId());
                if (balance == null) {
                    balance = 0l;
                }
                if (detail.getReversed()) {
                    balance -= detail.getLastTransport();
                } else {
                    balance += detail.getLastTransport();
                }
                result.put(detail.getRessId(), balance);
            }

        }
        for (TradeRoute r : routes2) {

            TradeRouteExt tre = new TradeRouteExt(r);
            ArrayList<TradeRouteDetail> tres = tre.getRouteDetails();
            for (TradeRouteDetail detail : tres) {
                Long balance = result.get(detail.getRessId());
                if (balance == null) {
                    balance = 0l;
                }
                if (!detail.getReversed()) {
                    balance -= detail.getLastTransport();
                } else {
                    balance += detail.getLastTransport();
                }
                result.put(detail.getRessId(), balance);
            }

        }
        return result;
    }

    //Sorts trade by input and output
    public static HashMap<Integer, HashMap<Boolean, Long>> findTradeBalanceForPlanetSorted(int planetId) {
        return ShippingUtilities.findShippingBalanceForPlanetSorted(planetId);
    }

    public static BaseResult deleteRoute(int userId, int routeId) {
        TradeRoute tr = trDAO.getById(routeId);
        if (tr == null) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }
        if ((tr.getUserId() != userId && tr.getTargetUserId() != userId)
                || (tr.getUserId() != userId && tr.getTargetUserId() == 0)) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }

        TradeUtilities.deleteRoute(tr);

        return new BaseResult(ML.getMLStr("transport_msg_routedeleted", userId), false);
    }

    public static BaseResult acceptRoute(int userId, int routeId) {
        TradeRoute tr = trDAO.getById(routeId);
        if (tr == null) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }
        if (tr.getTargetUserId() != userId) {
            return new BaseResult(ML.getMLStr("transport_err_noaccess", userId), true);
        }
        tr.setStatus(ETradeRouteStatus.ACTIVE);
        trDAO.update(tr);
        GenerateMessage gm = new GenerateMessage();
        gm.setSourceUserId(tr.getTargetUserId());
        gm.setDestinationUserId(tr.getUserId());
        String topic = ML.getMLStr("transport_msg_tradeofferaccepted", tr.getUserId());
        topic = topic.replace("%USER%", uDAO.findById(userId).getGameName());
        gm.setTopic(topic);
        String msg = ML.getMLStr("transport_msg_tradeofferaccepted_msg", tr.getUserId());
        msg = msg.replace("%USER%", uDAO.findById(userId).getGameName());
        gm.setMsg(msg);

        gm.setMessageType(EMessageType.USER);
        gm.setEscape(false);
        gm.writeMessageToUser();

        return new BaseResult(ML.getMLStr("transport_msg_routeaccepted", userId), false);
    }

    public static ArrayList<Ressource> findAllRessources() {
        return ressourceDAO.findAllStoreableRessources();
    }

    public static String getPlanetName(int planetId) {
        return playerPlanetDAO.findByPlanetId(planetId).getName();
    }

    public static ArrayList<PlayerPlanet> getOwnPlanetIds(int userId) {
        return playerPlanetDAO.findByUserId(userId);
    }
}
