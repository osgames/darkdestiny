/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.FormatUtilities;
import at.viswars.GameConstants;
import at.viswars.GameUtilities;
import at.viswars.ML;
import at.viswars.StartingArea;
import at.viswars.UniverseCreation;
import at.viswars.dao.*;
import at.viswars.database.access.DbConnect;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.model.DeletedUser;
import at.viswars.model.Planet;
import at.viswars.model.PlanetRessource;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Ressource;
import at.viswars.model.Statistic;
import at.viswars.model.User;
import at.viswars.model.UserData;
import at.viswars.model.Language;
import at.viswars.model.LoginTracker;
import at.viswars.model.MultiLog;
import at.viswars.model.PlayerResearch;
import at.viswars.model.Research;
import at.viswars.model.UserSettings;
import at.viswars.result.BalancingResult;
import at.viswars.result.BaseResult;
import at.viswars.result.RegisterResult;
import at.viswars.utilities.MD5;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class LoginService {

    private static PlanetLoyalityDAO pLoyalDAO = (PlanetLoyalityDAO) DAOFactory.get(PlanetLoyalityDAO.class);
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static LanguageDAO lDAO = (LanguageDAO) DAOFactory.get(LanguageDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static UserSettingsDAO usDAO = (UserSettingsDAO) DAOFactory.get(UserSettingsDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private static LoginTrackerDAO ltDAO = (LoginTrackerDAO) DAOFactory.get(LoginTrackerDAO.class);
    private static StatisticDAO statDAO = (StatisticDAO) DAOFactory.get(StatisticDAO.class);
    private static MultiLogDAO mlDAO = (MultiLogDAO) DAOFactory.get(MultiLogDAO.class);
    private static PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private static ResearchDAO resDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    private static PlayerResearchDAO presDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static TechRelationDAO tDAO = (TechRelationDAO) DAOFactory.get(TechRelationDAO.class);
    private static boolean spaceBattleTest = false;

    public static ArrayList<Language> findAllLanguages() {
        return lDAO.findAll();
    }

    public static Language findLanguageById(int id) {
        return lDAO.findById(id);
    }

    private static void savePlayerPlanet(PlayerPlanet pp) {
        ppDAO.add(pp);
    }

    private static void savePlanetRessource(PlanetRessource pr) {
        PlanetRessource prTmp = (PlanetRessource) prDAO.findBy(pr.getPlanetId(), pr.getRessId(), pr.getType());
        if (prTmp != null) {
            prDAO.remove(prTmp);
        }

        prDAO.add(pr);
    }

    private static void saveUserData(UserData ud) {
        udDAO.add(ud);
    }

    public static PlayerPlanet findPlayerPlanetByPlanetId(int planetId) {
        return ppDAO.findByPlanetId(planetId);
    }

    public static PlanetRessource findPlanetRessourceBy(int planetId, int ressourceId, EPlanetRessourceType type) {
        return prDAO.findBy(planetId, ressourceId, type);
    }

    public static void setSystemAssignedForUser(int userId, boolean systemAssigned) {
        User u = uDAO.findById(userId);
        u.setSystemAssigned(systemAssigned);
        uDAO.update(u);
    }

    public static void checkForMulti(int userId, String address) {
        LoginTracker tmp = LoginService.findLatestLoginTrackerByIp(address);
        if (tmp == null) {
            tmp = new LoginTracker();
            tmp.setUserId(userId);
            tmp.setTime(java.lang.System.currentTimeMillis());
        }
        User u = uDAO.findById(userId);
        User u2 = uDAO.findById(tmp.getUserId());
        if (u == null || u2 == null) {
            return;
        }
        if (u.getAdmin() || u2.getAdmin() || (u.getTrusted() && u2.getTrusted())) {
            return;
        }

        UserSettings us1 = usDAO.findByUserId(userId);
        UserSettings us2 = usDAO.findByUserId(tmp.getUserId());
        if (us1 == null || us1.getSittedById() == tmp.getUserId()) {
            return;
        }
        if (us2 == null || us2.getSittedById() == userId) {
            return;
        }

        int max_login_min = 15;
        int minutes = max_login_min;
        int seconds = minutes * 60;
        int milliSeconds = seconds * 1000;

        if (((java.lang.System.currentTimeMillis() - tmp.getTime()) < milliSeconds) && (tmp.getUserId() != userId) && u.getAdmin() == false) {

            int tick = GameUtilities.getCurrentTick2();
            long date = java.lang.System.currentTimeMillis();
            MultiLog ml1 = mlDAO.findBy(address, tick, userId);
            MultiLog ml2 = mlDAO.findBy(address, tick, tmp.getUserId());
            if (ml1 != null) {
                ml1.incrementCount();
            } else {
                ml1 = new MultiLog();
                ml1.setTick(tick);
                ml1.setUserId(userId);
                ml1.setCount(1);
                ml1.setDate(date);
                ml1.setIp(address);
                mlDAO.add(ml1);
            }
            if (ml2 != null) {
                ml2.incrementCount();
            } else {

                ml2 = new MultiLog();
                ml2.setTick(tick);
                ml2.setUserId(tmp.getUserId());
                ml2.setCount(1);
                ml2.setDate(date);
                ml2.setIp(address);
                mlDAO.add(ml2);
            }

        }
    }

    public static void setLastUpdateForUserId(int tick, int userId) {
        User u = uDAO.findById(userId);
        if (tick != u.getLastUpdate()) {
            u.setLastUpdate(tick);
            uDAO.update(u);
        }
    }

    public static ArrayList<Planet> findAllPlanets() {
        return pDAO.findAll();
    }

    public static ArrayList<Statistic> findAllStatistics() {
        return statDAO.findAll();
    }

    public static ArrayList<at.viswars.model.System> findAllSystems() {
        return sDAO.findAll();
    }

    public static ArrayList<at.viswars.model.System> findAllSystemsOrdered() {
        ArrayList<at.viswars.model.System> result = sDAO.findAll();
        TreeMap<Integer, at.viswars.model.System> tmp = new TreeMap<Integer, at.viswars.model.System>();
        for (at.viswars.model.System s : result) {
            tmp.put(s.getId(), s);
        }
        result.clear();
        result.addAll(tmp.values());
        return result;
    }

    public static RegisterResult checkUserForCreation(String ip, String userName, String gameName, String email, String password, int joinDate, Locale locale, boolean portalRegistration) {
        boolean isAbleToRegister = true;
        ArrayList<BaseResult> results = new ArrayList<BaseResult>();

        try {
            boolean ableBySimilarity = true;
            boolean ableByExistingName = true;
            boolean ableByAccountProtection = true;


            //Namecheck

            if (Service.userDAO.findByGameName(gameName) != null) {
                ableByExistingName = false;
                results.add(new BaseResult(ML.getMLStr("registration_err_gamenameexisting", locale), true));
            }

            if (Service.userDAO.findByUserName(userName) != null) {
                ableByExistingName = false;
                results.add(new BaseResult(ML.getMLStr("registration_err_usernameexisting", locale), true));
            }

            //Account Protection Check

            //Check if round is still within beginning Phase
            if ((Service.gameDataDAO.findById(1).getLastUpdatedTick() - Service.gameDataDAO.findById(1).getStartTime()) <= GameConstants.NAME_PROTECTION_DURATION) {
                for (DeletedUser du : (ArrayList<DeletedUser>) Service.deletedUserDAO.findAll()) {
                    if (du.getUserName().equals(userName) && du.getGameName().equals(gameName)) {
                        System.out.println("Compare : " + du.getPassword() + " to : " + password);
                        if (du.getPassword().equals(MD5.encryptPassword(password))) {
                            //Do nothing
                        } else {
                            results.add(new BaseResult(ML.getMLStr("registration_err_namereserved_invalidpassword", locale), true));
                            ableByAccountProtection = false;
                        }
                    } else if (du.getUserName().equals(userName)) {
                        results.add(new BaseResult("Dieser Username ist bereits mit einem anderen Usernamen reserviert", true));
                        ableByAccountProtection = false;
                    } else if (du.getGameName().equals(gameName)) {
                        results.add(new BaseResult("Dieser Spielername ist bereits mit einem anderen Usernamen reserviert", true));
                        ableByAccountProtection = false;
                    }
                }
            }

            //SimilarityCheck
            int highestEquality = 0;
            int mostEqualUser = 0;
            int similarity = 0;

            try {
                ArrayList<User> users = uDAO.findAll();

                for (User user : users) {
                    similarity = 0;

                    if ((userName.length() < 5) || (gameName.length() < 5) || (email.length() < 5)
                            || (password.length() < 5)) {
                        similarity = 101;
                    }
                    String compUsername = user.getUserName();
                    String compGamename = user.getGameName();
                    //String compIP = rs.getString(4);
                    String compEmail = user.getEmail();
                    long compJoinDate = user.getJoinDate();

                    // equalLengthCheck
                    similarity += equalLengthCheck(userName, compUsername);
                    similarity += equalLengthCheck(gameName, compGamename);
                    similarity += equalLengthCheck(email, compEmail);

                    if (userName.equalsIgnoreCase(compUsername)) {
                        similarity += 101;
                    } else {
                        // Check for similarity of part Strings
                        similarity += simValue(userName, compUsername, 3);
                        similarity += simValue(userName, compUsername, 4);
                        similarity += simValue(userName, compUsername, 5);
                    }

                    if (gameName.equalsIgnoreCase(compGamename)) {
                        similarity += 101;
                    } else {
                        // Check for similarity of part Strings
                        similarity += simValue(gameName, compGamename, 3);
                        similarity += simValue(gameName, compGamename, 4);
                        similarity += simValue(gameName, compGamename, 5);
                    }

                    if (email.equalsIgnoreCase(compEmail)) {
                        similarity += 75;
                    } else {
                        similarity += simValue(email, compEmail, 3);
                        similarity += simValue(email, compEmail, 4);
                        similarity += simValue(email, compEmail, 5);
                    }

                    // If its a portal registration skip (same ip)
                    if (!portalRegistration) {
                        if (ip.equalsIgnoreCase(user.getIp())) {
                            similarity += 80;
                        } else {
                            // Part check on IP
                            similarity += detailIpCheck(user.getIp(), ip);
                        }
                    }

                    // Additional Check on Email validity
                    StringTokenizer stEmail = new StringTokenizer(email, "@");
                    StringTokenizer stEmail2 = new StringTokenizer(email, ".");

                    if ((stEmail.countTokens() < 2) || (stEmail2.countTokens() < 2)) {
                        similarity += 101;
                    }

                    if (joinDate == compJoinDate) {
                        similarity = similarity * 2;
                    } else {
                        long near = Math.abs(joinDate - compJoinDate);

                        if (near <= 3) {
                            similarity = (int) ((double) similarity * (1d + ((4d - (double) near) / 4d)));
                        }
                    }

                    if (highestEquality < similarity) {
                        highestEquality = similarity;
                        mostEqualUser = user.getUserId();
                    }

                    if (similarity > 200) {
                        DebugBuffer.addLine(DebugLevel.DEBUG, "REGISTERING DECLINED (" + similarity + "/" + mostEqualUser + ")");
                        results.add(new BaseResult(ML.getMLStr("registration_err_multiaccount", locale), true));
                        ableBySimilarity = false;
                    }
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in equality Check=", e);
            }

            isAbleToRegister = ableByAccountProtection && ableByExistingName && ableBySimilarity;
            if (!isAbleToRegister) {
                results.add(new BaseResult(ML.getMLStr("registration_err_minimumlength", locale).replace("%LENGTH%", "5"), false));
                results.add(new BaseResult(ML.getMLStr("registration_msg_reporttoforum", locale), false));
            }

            return new RegisterResult(isAbleToRegister, results);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on User Creation", e);
            results.add(new BaseResult("Error occured " + e.getMessage(), true));
            return new RegisterResult(isAbleToRegister, results);
        }
    }

    public static Planet findMPlanetBySystemId(int systemId) {
        ArrayList<Planet> planets = pDAO.findBySystemId(systemId);
        for (Planet p : planets) {
            if (p.getLandType().equals(Planet.LANDTYPE_M)) {
                return p;
            }
        }
        return null;
    }

    private static int equalLengthCheck(String value1, String value2) {
        int nearLength = Math.abs(value1.length() - value2.length());

        if (nearLength <= 3) {
            return ((4 - nearLength) * 2);
        }

        return 0;
    }

    private static int detailIpCheck(String ip1, String ip2) {
        try {
            int simValue = 0;

            StringTokenizer st1 = new StringTokenizer(ip1, ".");
            StringTokenizer st2 = new StringTokenizer(ip2, ".");

            String[] ip1tok = new String[4];
            String[] ip2tok = new String[4];

            int tokenPos = 0;
            while (st1.hasMoreTokens()) {
                ip1tok[tokenPos] = st1.nextToken();
                tokenPos++;
            }

            tokenPos = 0;
            while (st2.hasMoreTokens()) {
                ip2tok[tokenPos] = st2.nextToken();
                tokenPos++;
            }

            int simFound = 0;
            for (int i = 0; i < 4; i++) {
                if (ip1tok[i].equalsIgnoreCase(ip2tok[i])) {
                    simFound++;
                }
            }

            switch (simFound) {
                case (3):
                    simValue += 40;
                    break;
                case (2):
                    simValue += 25;
                    break;
                case (1):
                    simValue += 10;
                    break;
            }

            return simValue;
        } catch (Exception e) {
            DebugBuffer.error("IP1: " + ip1 + " -- IP2: " + ip2);
            DebugBuffer.error("Error in IP Check: ", e);
        }

        return 0;
    }

    private static int simValue(String value1, String value2, int chars) {
        int simValue = 0;

        if ((value1.length() >= chars) && (value2.length() > chars)) {
            ArrayList<String> value1Splits = new ArrayList<String>();
            ArrayList<String> value2Splits = new ArrayList<String>();

            // split value1
            int currPos = 0;
            while ((value1.length() + currPos) <= value1.length()) {
                value1Splits.add(value1.substring(currPos, currPos + chars));
                currPos++;
            }

            // split value2
            currPos = 0;
            while ((value2.length() + currPos) <= value2.length()) {
                value2Splits.add(value2.substring(currPos, currPos + chars));
                currPos++;
            }

            // Check all values
            for (int i = 0; i < value1Splits.size(); i++) {
                for (int j = 0; j < value2Splits.size(); j++) {
                    if (value1Splits.get(i).equalsIgnoreCase(value2Splits.get(j))) {
                        simValue += 5 * chars;
                    }
                }
            }
        } else {
            return 0;
        }

        return simValue;
    }

    public synchronized static String createNewUserEntries(int userId, int systemId, String language) {
        if (systemId <= 0) {
            return "Shit happened! - Falls du kein l33t Hacker bist .. melde dich bitte im Forum ;)";
        }
        TransactionHandler th = TransactionHandler.getTransactionHandler();

        try {
            th.startTransaction();

            if (spaceBattleTest) {

                User u = uDAO.findById(userId);
                u.setAdmin(true);
                uDAO.update(u);

                for (Research r : (ArrayList<Research>) resDAO.findAll()) {

                    if (r.getId() == 999) {
                        continue;
                    }
                    PlayerResearch pr = new PlayerResearch();
                    pr.setResTime(System.currentTimeMillis());
                    pr.setResearchId(r.getId());
                    pr.setUserId(userId);
                    presDAO.add(pr);
                }
            }

            ArrayList<Planet> planets = pDAO.findBySystemId(systemId);
            Planet p = null;
            for (Planet ptmp : planets) {
                if (ptmp.getLandType().equals(Planet.LANDTYPE_M)) {
                    p = ptmp;
                    break;
                }
            }

            if (p != null) {
                if (ppDAO.findByPlanetId(p.getId()) == null) {

                    PlayerPlanet pp = new PlayerPlanet();
                    pp.setName("Planet #" + p.getId());
                    pp.setUserId(userId);
                    pp.setPlanetId(p.getId());
                    pp.setHomeSystem(true);
                    pp.setPopulation(5000000000l);
                    pp.setMoral(100);
                    pp.setTax(37);
                    pp.setGrowth(5f);
                    pp.setSpecialPoints(0f);
                    pp.setSpecialGrowth(0f);
                    pp.setSpecialProduction(0f);
                    pp.setSpecialResearch(0f);
                    pp.setMigration(0);
                    pp.setPriorityAgriculture(PlayerPlanet.PRIORITY_HIGH);
                    pp.setPriorityIndustry(PlayerPlanet.PRIORITY_MEDIUM);
                    pp.setPriorityResearch(PlayerPlanet.PRIORITY_LOW);
                    pp.setUnrest(0d);

                    savePlayerPlanet(pp);

                    // Planet Loyality
                    /*
                     PlanetLoyality pLoyal = new PlanetLoyality();
                     pLoyal.setPlanetId(p.getId());
                     pLoyal.setUserId(userId);
                     pLoyal.setValue(100d);
                     pLoyalDAO.add(pLoyal);
                     */

                    UserData ud = new UserData();
                    ud.setUserId(userId);
                    ud.setCredits(100000000l);
                    ud.setRating(100);

                    UserSettings us = new UserSettings();
                    us.setUserId(userId);
                    us.setLeaveDate(0l);
                    us.setShowWelcomeSite(true);
                    us.setShowConstructionDetails(true);
                    us.setShowProductionDetails(true);
                    us.setStarmapHeight(600);
                    us.setStarmapWidth(800);
                    us.setSittedById(0);
                    us.setStarmapExternalBrowser(false);
                    us.setTtvHeight(600);
                    us.setTtvWidth(800);
                    us.setTtvExternalBrowser(false);
                    us.setLeaveDays(0);
                    us.setMaxLeaveTicks(0);

                    Service.userSettingsDAO.add(us);

                    saveUserData(ud);

                    /*
                     PlanetRessource food = new PlanetRessource();
                     food.setPlanetId(p.getId());
                     food.setType(EPlanetRessourceType.INSTORAGE);
                     food.setRessId(Ressource.FOOD);
                     food.setQty(100000L);
                     savePlanetRessource(food);

                     PlanetRessource tmpIron = prDAO.findBy(p.getId(), Ressource.IRON, EPlanetRessourceType.INSTORAGE);
                     if (tmpIron != null) {
                     //Found Ressource which should NOT be there
                     prDAO.remove(tmpIron);
                     }
                     PlanetRessource iron = new PlanetRessource();
                     iron.setPlanetId(p.getId());
                     iron.setType(EPlanetRessourceType.INSTORAGE);
                     iron.setRessId(Ressource.IRON);
                     iron.setQty(2000000L);
                     savePlanetRessource(iron);

                     PlanetRessource tmpSteel = prDAO.findBy(p.getId(), Ressource.STEEL, EPlanetRessourceType.INSTORAGE);
                     if (tmpSteel != null) {
                     prDAO.remove(tmpSteel);
                     }
                     PlanetRessource steel = new PlanetRessource();
                     steel.setPlanetId(p.getId());
                     steel.setType(EPlanetRessourceType.INSTORAGE);
                     steel.setRessId(Ressource.STEEL);
                     steel.setQty(1000000L);

                     savePlanetRessource(steel);
                     */
                } else {
                    //Planet geh�rt bereits einem SPieler
                }

                // Viewtable befüllen
                int currTick = GameUtilities.getCurrentTick2();

                at.viswars.model.System s = sDAO.findById(systemId);
                s.setVisibility(userId);
                sDAO.update(s);

                /*
                 ArrayList<at.viswars.model.System> systemList = SystemService.getSystemsAround(s.getId(), 100d, ESystemSorting.NO_SORT);

                 for (at.viswars.model.System sys : systemList) {
                 vtDAO.addSystem(userId, sys.getId(), currTick);
                 }

                 if (spaceBattleTest) {

                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_ORBITAL_SHIPYARD, 1));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_PLANETARYSHIPYARD, 1));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_MODULEFACTORY, 1));
                 }

                 // Gebäude erstellen
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_AGROCOMPLEX, 51));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_BIGIRONMINE, 15));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_STEELCOMPLEX, 1));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_NUCLEARPOWERPLANT, 100));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_BIGRESSOURCESTORAGE, 4));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_BARRACKS, 2));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_LOCALADMINISTRATION, 1));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_PLANETADMINISTRATION, 1));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_PLANETGOVERNMENT, 1));
                 pcDAO.add(new PlanetConstruction(p.getId(), Construction.ID_OBSERVATORY, 1));

                 // Troops
                 ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_MILIZ, 10000000));
                 ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_LIGHTINFANTRY, 5000000));
                 ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_HEAVYINFANTRY, 2500000));
                 ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_LIGHTTANK, 50000));
                 //     ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_ROCKETTANK, 25000));
                 ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_HEAVYTANK, 25000));
                 ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_INTERCEPTORPLANE, 10000));
                 ptDAO.add(new PlayerTroop(p.getId(), userId, GroundTroop.ID_BOMBER, 5000));


                 // Planetare Temperatur anpassen
                 DebugBuffer.addLine(DebugLevel.TRACE, "Try to adjust diameter and temperature for planetId=" + p.getId());
                 p = findPlanetById(p.getId());
                 p.setAvgTemp(15);
                 p.setDiameter(12500);
                 p = pDAO.update(p);

                 // Perform balancing of system
                 ArrayList<Planet> planetsToBalance = pDAO.findBySystemId(systemId);
                 HashMap<Integer, Planet> sortedPlanetsToBalance = new HashMap<Integer, Planet>();
                 for (Planet pTmp : planetsToBalance) {
                 sortedPlanetsToBalance.put(pTmp.getId(), pTmp);
                 }
                 HashMap<Integer, ArrayList<PlanetRessource>> ressources = new HashMap<Integer, ArrayList<PlanetRessource>>();
                 HashMap<Integer, PlayerPlanet> playerPlanets = new HashMap<Integer, PlayerPlanet>();
                 for (Planet pTmp : planetsToBalance) {
                 ressources.put(pTmp.getId(), prDAO.findByPlanetId(pTmp.getId()));
                 PlayerPlanet pp = ppDAO.findByPlanetId(pTmp.getId());
                 if (pp != null) {
                 playerPlanets.put(pTmp.getId(), pp);
                 }
                 }
                 BalancingResult br = new BalancingResult();
                 br.setActionTaken(true);
                 while (br.isActionTaken()) {
                 br = balanceStartingSystem(br, p.getId(), sortedPlanetsToBalance, ressources, playerPlanets, systemId);
                 Thread.sleep(100);
                 }
                 //Balancing finished save it!!
                 for (Planet pSave : sortedPlanetsToBalance.values()) {
                 pDAO.update(pSave);
                 }

                 long volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (12500 / 2), 3)));
                 long iron = (long) ((float) volume / (float) 1000);

                 PlanetRessource pr = prDAO.findBy(p.getId(), Ressource.PLANET_IRON, EPlanetRessourceType.PLANET);
                 if (pr == null) {
                 PlanetRessource prNew = new PlanetRessource();
                 prNew.setPlanetId(p.getId());
                 prNew.setType(EPlanetRessourceType.PLANET);
                 prNew.setRessId(Ressource.PLANET_IRON);
                 prNew.setQty(iron);

                 prDAO.add(prNew);
                 } else {
                 pr.setQty(iron);
                 prDAO.update(pr);
                 }
                 PlanetLog pl = new PlanetLog();
                 pl.setUserId(userId);
                 pl.setPlanetId(p.getId());
                 pl.setTime(GameUtilities.getCurrentTick2());
                 plDAO.add(pl);
                 // stmt3.executeUpdate("UPDATE planetressources SET qty=" + iron + " WHERE planetId=" + p.getId() + " AND type=" + PlanetTable.PLANET_RESSOURCE + " AND ressId=" + GameConstants.RES_PLANET_IRON);

                 DebugBuffer.addLine(DebugLevel.DEBUG, "Creation suceeded!");

                 //Update TerritoryEntries
                 try {
                 TerritoryUtilities.updateAllTerritoryPermissions();
                 } catch (Exception e) {

                 DebugBuffer.writeStackTrace("Error in create user: Could not update TerritoryEntries -> " + e.getMessage() + ":<BR>", e);
                 }
                 */
                setSystemAssignedForUser(userId, true);

            }
        } catch (Exception e) {
            e.printStackTrace();
            DebugBuffer.writeStackTrace("Error in create user -> " + e.getMessage() + ":<BR>", e);
            try {
                th.rollback();
            } catch (Exception te) {
                DebugBuffer.error(th.getTransactionLog());
                DebugBuffer.writeStackTrace("Error in create user -> " + te.getMessage() + ":<BR>", e);
            }

            th.endTransaction();
            return "Spielererstellung fehlgeschlagen! Bitte im Forum melden!<BR><BR>" + th.getTransactionLog();
        } finally {
            th.endTransaction();
        }

        return "";
    }

    public static BalancingResult balanceStartingSystem(BalancingResult result, int homeId, HashMap<Integer, Planet> planets, HashMap<Integer, ArrayList<PlanetRessource>> ressources, HashMap<Integer, PlayerPlanet> playerPlanets, int sysId) throws Exception {
        DebugBuffer.addLine(DebugLevel.UNKNOWN, "BALANCE STARTING SYSTEM " + sysId + " WITH HOMEPLANET " + homeId);

        result.setActionTaken(false);

        boolean hasYnke = false;
        int ynkeAmount = 0;
        boolean hasHowal = false;
        int howalAmount = 0;
        int habitableCount = 0;
        int mCount = 0;
        long totalAddPopulation = 0;

        HashMap<Integer, Long> colonies = new HashMap<Integer, Long>();

        for (Map.Entry<Integer, Planet> entry : planets.entrySet()) {
            Planet p = entry.getValue();
            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M)) {
                mCount++;
                habitableCount++;
            }

            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {
                habitableCount++;
            }

            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G)) {
                habitableCount++;
            }

            ArrayList<PlanetRessource> prs = ressources.get(p.getId());

            for (PlanetRessource pr : prs) {
                if (pr.getRessId() == Ressource.YNKELONIUM) {
                    hasYnke = true;
                    ynkeAmount += pr.getQty();
                }

                if (pr.getRessId() == Ressource.HOWALGONIUM) {
                    hasHowal = true;
                    howalAmount += pr.getQty();
                }
            }

            if (homeId != p.getId()) {
                totalAddPopulation += p.getMaxPopulation();
                if (p.getMaxPopulation() > 0) {
                    colonies.put(p.getId(), p.getMaxPopulation());
                }
            }
        }

        boolean reduceAddPop = false;
        boolean increasePopOnHP = false;
        boolean increasePopOnColony = false;
        boolean removeM = false;

        long increasePopBy = 0;
        long decreasePopBy = 0;

        int bonus = 0;

        if (!hasYnke) {
            bonus += 500000000;
        }
        if (!hasHowal) {
            bonus += 500000000;
        }
        if (totalAddPopulation > (3500000000l + bonus)) {
            long tmpDec = (3500000000l + bonus) - totalAddPopulation;
            long decValue = tmpDec - 100000000 + (int) (Math.random() * 200000000);
            if (decValue > 0) {
                decreasePopBy = decValue;
                reduceAddPop = true;
            }
        }
        if (mCount > 1) {
            removeM = true;
        }

        if (!hasYnke && !hasHowal) {
            if (totalAddPopulation < 4000000000l) {
                long tmpValue = 4000000000l - totalAddPopulation;
                if (habitableCount > 1) {
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);
                    if (incValue > 0) {
                        increasePopOnColony = true;
                        increasePopBy = incValue;
                    }
                } else {
                    if (tmpValue > 2500000000l) {
                        tmpValue = 2500000000l;
                    }
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);

                    if (incValue > 0) {
                        increasePopOnHP = true;
                        increasePopBy = incValue;
                    }
                }
            }
        } else if (!hasYnke) {
            if (totalAddPopulation < 3000000000l) {
                long tmpValue = 3000000000l - totalAddPopulation;
                if (habitableCount > 1) {
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);
                    if (incValue > 0) {
                        increasePopOnColony = true;
                        increasePopBy = incValue;
                    }
                } else {
                    if (tmpValue > 1700000000l) {
                        tmpValue = 1700000000l;
                    }
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);

                    if (incValue > 0) {
                        increasePopOnHP = true;
                        increasePopBy = incValue;
                    }
                }
            }
        } else if (!hasHowal) {
            if (totalAddPopulation < 3000000000l) {
                long tmpValue = 3000000000l - totalAddPopulation;
                if (habitableCount > 1) {
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);
                    if (incValue > 0) {
                        increasePopOnColony = true;
                        increasePopBy = incValue;
                    }
                } else {
                    if (tmpValue > 170000000l) {
                        tmpValue = 1700000000l;
                    }
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);

                    if (incValue > 0) {
                        increasePopOnHP = true;
                        increasePopBy = incValue;
                    }
                }
            }
        } else {
            if (totalAddPopulation < 2000000000l) {
                long tmpValue = 2000000000l - totalAddPopulation;
                if (habitableCount > 1) {
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);
                    if (incValue > 0) {
                        increasePopOnColony = true;
                        increasePopBy = incValue;
                    }
                } else {
                    if (tmpValue > 1400000000) {
                        tmpValue = 1400000000;
                    }
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);

                    if (incValue > 0) {
                        increasePopOnHP = true;
                        increasePopBy = incValue;
                    }
                }
            }
        }

        // Recheck homeworld
        if (habitableCount == 1) {
            boolean reduce = false;
            long reduceTo = 0;

            if (!(hasYnke ^ hasHowal)) {
                reduce = true;
                reduceTo = 9200000000l - (long) (Math.random() * 200000000);
            } else if (!hasYnke && !hasHowal) {
                reduce = true;
                reduceTo = 10100000000l - (long) (Math.random() * 200000000);
            } else {
                reduce = true;
                reduceTo = 8900000000l - (long) (Math.random() * 200000000);
            }
            Planet homeP = planets.get(homeId);
            if (homeP.getMaxPopulation() > reduceTo) {
                long targetPop = reduceTo;

                while (homeP.getMaxPopulation() > targetPop) {
                    homeP.setDiameter(homeP.getDiameter() - 100);
                }

            }
        }

        if (removeM) {
            result.setActionTaken(true);

            // rescan for planets and switch all planets not homeworld and M to C or G
            // Statement stmt3 = DbConnect.createStatement();
            //  ResultSet rs3 = stmt3.executeQuery("SELECT p.id, p.landType, pp.homesystem FROM planet p LEFT OUTER JOIN playerplanet pp ON pp.planetID=p.id WHERE p.systemId=" + sysId);

            for (Map.Entry<Integer, Planet> entry : planets.entrySet()) {
                Planet p = entry.getValue();
                PlayerPlanet pp = playerPlanets.get(p.getId());
                if ((pp == null) && (p.getLandType().equalsIgnoreCase("M"))) {
                    String newType = "";
                    int newTemp = 0;

                    if (Math.random() > 0.5d) {
                        newType = Planet.LANDTYPE_C;
                        newTemp = 0;
                    } else {
                        newType = Planet.LANDTYPE_G;
                        newTemp = 30;
                    }
                    result.addActionTaken("ACTION: TRANSFORM PLANET " + p.getId() + " FROM M TO " + newType + " (NEW TEMP=" + newTemp + ")");
                    DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: TRANSFORM PLANET " + p.getId() + " FROM M TO " + newType + " (NEW TEMP=" + newTemp + ")");

                    p.setLandType(newType);
                    p.setAvgTemp(newTemp);
                    break;
                }
            }
        } else if (reduceAddPop) {
            result.setActionTaken(true);

            // reduce diameter of largest colony till required pop reached
            int largestPlanet = 0;
            long largestPop = 0;

            for (Map.Entry<Integer, Long> colony : colonies.entrySet()) {
                if (largestPop < colony.getValue()) {
                    largestPop = colony.getValue();
                    largestPlanet = colony.getKey();
                }
            }

            result.addActionTaken("ACTION: REDUCE POPULATION ON " + largestPlanet + " BY " + FormatUtilities.getFormattedNumber(decreasePopBy));
            DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: REDUCE POPULATION ON " + largestPlanet + " BY " + FormatUtilities.getFormattedNumber(decreasePopBy));

            Planet pdTmp = planets.get(largestPlanet);
            long targetPop = pdTmp.getMaxPopulation() - decreasePopBy;
            int oldDiameter = pdTmp.getDiameter();

            while (pdTmp.getMaxPopulation() > targetPop) {
                pdTmp.setDiameter(pdTmp.getDiameter() - 100);
            }


            result.addActionTaken("ACTION: REDUCE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());
            DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: REDUCE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());
            // pdTmp.setDiameter(oldDiameter);
        } else if (increasePopOnHP || increasePopOnColony) {
            result.setActionTaken(true);

            if (increasePopOnHP) {
                result.setActionTaken(false);
                // Increase diameter of HP till required pop is reached
                result.addActionTaken("ACTION: INCREASE POPULATION ON HOMEPLANET" + homeId + " BY " + FormatUtilities.getFormattedNumber(increasePopBy));
                DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: INCREASE POPULATION ON HOMEPLANET " + homeId + " BY " + FormatUtilities.getFormattedNumber(increasePopBy));

                Planet pdTmp = planets.get(homeId);
                long targetPop = pdTmp.getMaxPopulation() + increasePopBy;
                int oldDiameter = pdTmp.getDiameter();

                while (pdTmp.getMaxPopulation() < targetPop) {
                    pdTmp.setDiameter(pdTmp.getDiameter() + 100);
                }

                result.addActionTaken("ACTION: INCREASE DIAMETER ON HOMEPLANET FROM " + oldDiameter + " TO " + pdTmp.getDiameter());
                DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: INCREASE DIAMETER ON HOMEPLANET FROM " + oldDiameter + " TO " + pdTmp.getDiameter());
                //  Statement stmt4 = DbConnect.createStatement();
                // stmt4.execute("UPDATE planet SET diameter=" + pdTmp.getDiameter() + " WHERE id=" + homeId);
                // stmt4.close();

            } else {
                // Increase diameter of smallest colony till required pop is reached
                int smallestPlanet = 0;
                long smallestPop = 99999999999l;

                for (Map.Entry<Integer, Long> colony : colonies.entrySet()) {
                    if (smallestPop > colony.getValue()) {
                        smallestPop = colony.getValue();
                        smallestPlanet = colony.getKey();
                    }
                }

                result.addActionTaken("ACTION: INCREASE POPULATION ON " + smallestPlanet + " BY " + FormatUtilities.getFormattedNumber(increasePopBy));
                DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: INCREASE POPULATION ON " + smallestPlanet + " BY " + FormatUtilities.getFormattedNumber(increasePopBy));

                Planet pdTmp = planets.get(smallestPlanet);
                long targetPop = pdTmp.getMaxPopulation() + increasePopBy;
                int oldDiameter = pdTmp.getDiameter();
                int oldTemp = pdTmp.getAvgTemp();

                while (pdTmp.getMaxPopulation() < targetPop) {
                    pdTmp.setDiameter(pdTmp.getDiameter() + 100);
                    if (pdTmp.getLandType().equalsIgnoreCase("C")) {
                        if (pdTmp.getAvgTemp() < 5) {
                            pdTmp.setAvgTemp(pdTmp.getAvgTemp() + 1);
                        }
                    } else {
                        if (pdTmp.getAvgTemp() > 25) {
                            pdTmp.setAvgTemp(pdTmp.getAvgTemp() - 1);
                        }
                    }

                }

                result.addActionTaken("ACTION: INCREASE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());
                DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: INCREASE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());

            }
        }
        return result;
    }

    @Deprecated
    private static boolean balanceStartingSystem(int homeId, int sysId) throws Exception {
        DebugBuffer.addLine(DebugLevel.UNKNOWN, "BALANCE STARTING SYSTEM " + sysId + " WITH HOMEPLANET " + homeId);

        boolean actionTaken = false;

        boolean hasYnke = false;
        int ynkeAmount = 0;
        boolean hasHowal = false;
        int howalAmount = 0;
        int habitableCount = 0;
        int mCount = 0;
        long totalAddPopulation = 0;

        HashMap<Integer, Long> colonies = new HashMap<Integer, Long>();
        ArrayList<Planet> planets = pDAO.findBySystemId(sysId);

        for (Planet p : planets) {
            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M)) {
                mCount++;
                habitableCount++;
            }

            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {
                habitableCount++;
            }

            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G)) {
                habitableCount++;
            }

            ArrayList<PlanetRessource> prs = prDAO.findByPlanetId(p.getId());

            for (PlanetRessource pr : prs) {
                if (pr.getRessId() == Ressource.YNKELONIUM) {
                    hasYnke = true;
                    ynkeAmount += pr.getQty();
                }

                if (pr.getRessId() == Ressource.HOWALGONIUM) {
                    hasHowal = true;
                    howalAmount += pr.getQty();
                }
            }

            if (homeId != p.getId()) {
                totalAddPopulation += p.getMaxPopulation();
                if (p.getMaxPopulation() > 0) {
                    colonies.put(p.getId(), p.getMaxPopulation());
                }
            }
        }

        boolean reduceAddPop = false;
        boolean increasePopOnHP = false;
        boolean increasePopOnColony = false;
        boolean removeM = false;

        long increasePopBy = 0;
        long decreasePopBy = 0;

        int bonus = 0;

        if (!hasYnke) {
            bonus += 500000000;
        }
        if (!hasHowal) {
            bonus += 500000000;
        }
        if (totalAddPopulation > (3500000000l + bonus)) {
            long tmpDec = (3500000000l + bonus) - totalAddPopulation;
            long decValue = tmpDec - 100000000 + (int) (Math.random() * 200000000);
            if (decValue > 0) {
                decreasePopBy = decValue;
                reduceAddPop = true;
            }
        }
        if (mCount > 1) {
            removeM = true;
        }

        if (!hasYnke && !hasHowal) {
            if (totalAddPopulation < 4000000000l) {
                long tmpValue = 4000000000l - totalAddPopulation;
                if (habitableCount > 1) {
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);
                    if (incValue > 0) {
                        increasePopOnColony = true;
                        increasePopBy = incValue;
                    }
                } else {
                    if (tmpValue > 2500000000l) {
                        tmpValue = 2500000000l;
                    }
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);

                    if (incValue > 0) {
                        increasePopOnHP = true;
                        increasePopBy = incValue;
                    }
                }
            }
        } else if (!hasYnke) {
            if (totalAddPopulation < 3000000000l) {
                long tmpValue = 3000000000l - totalAddPopulation;
                if (habitableCount > 1) {
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);
                    if (incValue > 0) {
                        increasePopOnColony = true;
                        increasePopBy = incValue;
                    }
                } else {
                    if (tmpValue > 1700000000l) {
                        tmpValue = 1700000000l;
                    }
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);

                    if (incValue > 0) {
                        increasePopOnHP = true;
                        increasePopBy = incValue;
                    }
                }
            }
        } else if (!hasHowal) {
            if (totalAddPopulation < 3000000000l) {
                long tmpValue = 3000000000l - totalAddPopulation;
                if (habitableCount > 1) {
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);
                    if (incValue > 0) {
                        increasePopOnColony = true;
                        increasePopBy = incValue;
                    }
                } else {
                    if (tmpValue > 170000000l) {
                        tmpValue = 1700000000l;
                    }
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);

                    if (incValue > 0) {
                        increasePopOnHP = true;
                        increasePopBy = incValue;
                    }
                }
            }
        } else {
            if (totalAddPopulation < 2000000000l) {
                long tmpValue = 2000000000l - totalAddPopulation;
                if (habitableCount > 1) {
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);
                    if (incValue > 0) {
                        increasePopOnColony = true;
                        increasePopBy = incValue;
                    }
                } else {
                    if (tmpValue > 1400000000) {
                        tmpValue = 1400000000;
                    }
                    long incValue = tmpValue - 100000000 + (int) (Math.random() * 200000000);

                    if (incValue > 0) {
                        increasePopOnHP = true;
                        increasePopBy = incValue;
                    }
                }
            }
        }

        // Recheck homeworld
        if (habitableCount == 1) {
            boolean reduce = false;
            long reduceTo = 0;

            if (!(hasYnke ^ hasHowal)) {
                reduce = true;
                reduceTo = 9200000000l - (long) (Math.random() * 200000000);
            } else if (!hasYnke && !hasHowal) {
                reduce = true;
                reduceTo = 10100000000l - (long) (Math.random() * 200000000);
            } else {
                reduce = true;
                reduceTo = 8900000000l - (long) (Math.random() * 200000000);
            }

            Planet homeP = pDAO.findById(homeId);

            if (homeP.getMaxPopulation() > reduceTo) {
                long targetPop = reduceTo;

                while (homeP.getMaxPopulation() > targetPop) {
                    homeP.setDiameter(homeP.getDiameter() - 100);
                }

                Planet p = pDAO.findById(homeId);
                p.setDiameter(homeP.getDiameter());
                pDAO.update(p);

            }
        }

        if (removeM) {
            actionTaken = true;

            // rescan for planets and switch all planets not homeworld and M to C or G
            // Statement stmt3 = DbConnect.createStatement();
            //  ResultSet rs3 = stmt3.executeQuery("SELECT p.id, p.landType, pp.homesystem FROM planet p LEFT OUTER JOIN playerplanet pp ON pp.planetID=p.id WHERE p.systemId=" + sysId);

            ArrayList<Planet> planets2 = pDAO.findBySystemId(sysId);
            for (Planet p : planets2) {
                PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
                if ((pp == null) && (p.getLandType().equalsIgnoreCase("M"))) {
                    String newType = "";
                    int newTemp = 0;

                    if (Math.random() > 0.5d) {
                        newType = Planet.LANDTYPE_C;
                        newTemp = 0;
                    } else {
                        newType = Planet.LANDTYPE_G;
                        newTemp = 30;
                    }

                    DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: TRANSFORM PLANET " + p.getId() + " FROM M TO " + newType + " (NEW TEMP=" + newTemp + ")");
                    //Statement stmt4 = DbConnect.createStatement();
                    //stmt4.execute("UPDATE planet SET landType='" + newType + "' AND avgTemp=" + newTemp + " WHERE id=" + rs3.getInt(1));
                    //stmt4.close();
                    p.setLandType(newType);
                    p.setAvgTemp(newTemp);
                    pDAO.update(p);
                }
            }
        } else if (reduceAddPop) {
            actionTaken = true;

            // reduce diameter of largest colony till required pop reached
            int largestPlanet = 0;
            long largestPop = 0;

            for (Map.Entry<Integer, Long> colony : colonies.entrySet()) {
                if (largestPop < colony.getValue()) {
                    largestPop = colony.getValue();
                    largestPlanet = colony.getKey();
                }
            }

            DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: REDUCE POPULATION ON " + largestPlanet + " BY " + decreasePopBy);

            Planet pdTmp = pDAO.findById(largestPlanet);
            long targetPop = pdTmp.getMaxPopulation() - decreasePopBy;
            int oldDiameter = pdTmp.getDiameter();

            while (pdTmp.getMaxPopulation() > targetPop) {
                pdTmp.setDiameter(pdTmp.getDiameter() - 100);
            }
            Planet p = pDAO.findById(largestPlanet);
            p.setDiameter(pdTmp.getDiameter());
            pDAO.update(p);
            // Statement stmt4 = DbConnect.createStatement();
            // stmt4.execute("UPDATE planet SET diameter=" + pdTmp.getDiameter() + " WHERE id=" + largestPlanet);
            // stmt4.close();

            DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: REDUCE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());
            // pdTmp.setDiameter(oldDiameter);
        } else if (increasePopOnHP || increasePopOnColony) {
            actionTaken = true;

            if (increasePopOnHP) {
                actionTaken = false;
                // Increase diameter of HP till required pop is reached
                DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: INCREASE POPULATION ON " + homeId + " BY " + increasePopBy);

                Planet pdTmp = pDAO.findById(homeId);
                long targetPop = pdTmp.getMaxPopulation() + increasePopBy;
                int oldDiameter = pdTmp.getDiameter();

                while (pdTmp.getMaxPopulation() < targetPop) {
                    pdTmp.setDiameter(pdTmp.getDiameter() + 100);
                }

                DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: INCREASE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());
                //  Statement stmt4 = DbConnect.createStatement();
                // stmt4.execute("UPDATE planet SET diameter=" + pdTmp.getDiameter() + " WHERE id=" + homeId);
                // stmt4.close();

                Planet p = pDAO.findById(homeId);
                p.setDiameter(pdTmp.getDiameter());
                pDAO.update(p);
            } else {
                // Increase diameter of smallest colony till required pop is reached
                int smallestPlanet = 0;
                long smallestPop = 99999999999l;

                for (Map.Entry<Integer, Long> colony : colonies.entrySet()) {
                    if (smallestPop > colony.getValue()) {
                        smallestPop = colony.getValue();
                        smallestPlanet = colony.getKey();
                    }
                }

                DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: INCREASE POPULATION ON " + smallestPlanet + " BY " + increasePopBy);

                Planet pdTmp = pDAO.findById(smallestPlanet);
                long targetPop = pdTmp.getMaxPopulation() + increasePopBy;
                int oldDiameter = pdTmp.getDiameter();
                int oldTemp = pdTmp.getAvgTemp();

                while (pdTmp.getMaxPopulation() < targetPop) {
                    pdTmp.setDiameter(pdTmp.getDiameter() + 100);
                    if (pdTmp.getLandType().equalsIgnoreCase("C")) {
                        if (pdTmp.getAvgTemp() < 5) {
                            pdTmp.setAvgTemp(pdTmp.getAvgTemp() + 1);
                        }
                    } else {
                        if (pdTmp.getAvgTemp() > 25) {
                            pdTmp.setAvgTemp(pdTmp.getAvgTemp() - 1);
                        }
                    }

                }

                DebugBuffer.addLine(DebugLevel.UNKNOWN, "ACTION: INCREASE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());
                //  Statement stmt4 = DbConnect.createStatement();
                //  stmt4.execute("UPDATE planet SET avgTemp=" + pdTmp.getAverageTemp() + ", diameter=" + pdTmp.getDiameter() + " WHERE id=" + smallestPlanet);
                //  stmt4.close();
                Planet p = pDAO.findById(smallestPlanet);
                p.setDiameter(pdTmp.getDiameter());
                p.setAvgTemp(pdTmp.getAvgTemp());
                pDAO.update(p);
            }
        }

        return actionTaken;
    }
    //FIXME Unused!

    private synchronized static int getStartingPosition(int userId) {
        int systemId = -1;
        int minX;
        int maxX;
        int minY;
        int maxY;
        int middleX = 0;
        int middleY = 0;


        // Determine dimensions of Universe
        Statement stmt = DbConnect.createStatement();

        try {
            ResultSet rs = stmt.executeQuery("SELECT MAX(id), MIN(posx), MIN(posy), MAX(posx), MAX(posy) FROM system");
            if (rs.next()) {
                systemId = rs.getInt(1) + 1;
                minX = rs.getInt(2);
                minY = rs.getInt(3);
                maxX = rs.getInt(4);
                maxY = rs.getInt(5);
                middleX = (int) (((float) maxX - (float) minX) / 2f);
                middleY = (int) (((float) maxY - (float) minY) / 2f);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while initialising of getStartPosition", e);
            try {
                stmt.close();
            } catch (Exception e2) {
            }
            return -1;
        }

        // Build blocking Area
        // - at least 130 Lightyears away from next planet with more than 10 Mio. inhabitants
        // - at least 10 Lightyears away from next system
        StartingArea.refresh();

        // Check blocking area
        int xSpan = 1;
        int currXMove = 1;
        boolean xRight = true;
        int ySpan = 1;
        int currYMove = 1;
        boolean yUp = true;
        int currXPos = middleX;
        int currYPos = middleY;
        boolean processingX = true;

        boolean blocked = StartingArea.isBlocked(currXPos, currYPos);

        // expand rectangle till partly outside of blocked area
        // to speed up searching of starting position
        if (blocked) {
            int leftX = middleX;
            int rightX = middleX + 1;
            int topY = middleY;
            int bottomY = middleY + 1;

            // Fast Scan
            int xScanStep = 20;
            int yScanStep = 20;

            while (StartingArea.isBlockedRect(leftX, topY, rightX, bottomY)) {
                leftX -= xScanStep;
                rightX += xScanStep;
                topY -= yScanStep;
                bottomY += yScanStep;
            }

            leftX += xScanStep;
            rightX -= xScanStep;
            topY += yScanStep;
            bottomY -= yScanStep;

            // Medium Scan
            xScanStep = 5;
            yScanStep = 5;

            while (StartingArea.isBlockedRect(leftX, topY, rightX, bottomY)) {
                leftX -= xScanStep;
                rightX += xScanStep;
                topY -= yScanStep;
                bottomY += yScanStep;
            }

            leftX += xScanStep;
            rightX -= xScanStep;
            topY += yScanStep;
            bottomY -= yScanStep;

            // Detail Scan
            xScanStep = 1;
            yScanStep = 1;

            while (StartingArea.isBlockedRect(leftX, topY, rightX, bottomY)) {
                leftX -= xScanStep;
                rightX += xScanStep;
                topY -= yScanStep;
                bottomY += yScanStep;
            }

            leftX += xScanStep;
            rightX -= xScanStep;
            topY += yScanStep;
            bottomY -= yScanStep;

            currXPos = leftX + xScanStep;
            currYPos = bottomY - yScanStep;
            rightX -= xScanStep;
            topY -= yScanStep;

            xSpan = rightX - leftX;
            currXMove = xSpan;
            ySpan = bottomY - topY;
            currYMove = ySpan;
        }

        while (blocked) {
            if (processingX) {
                if (xRight) {
                    currXPos++;
                    currXMove--;
                    if (currXMove == 0) {
                        xRight = false;
                        xSpan++;
                        currXMove = xSpan;
                        processingX = false;
                    }
                } else {
                    currXPos--;
                    currXMove--;
                    if (currXMove == 0) {
                        xRight = true;
                        xSpan++;
                        currXMove = xSpan;
                        processingX = false;
                    }
                }
            } else {
                if (yUp) {
                    currYPos--;
                    currYMove--;
                    if (currYMove == 0) {
                        yUp = false;
                        ySpan++;
                        currYMove = ySpan;
                        processingX = true;
                    }
                } else {
                    currYPos++;
                    currYMove--;
                    if (currYMove == 0) {
                        yUp = true;
                        ySpan++;
                        currYMove = ySpan;
                        processingX = true;
                    }
                }
            }

            blocked = StartingArea.isBlocked(currXPos, currYPos);
        }

        // if found -- create new system at this location with at least one M planet
        //Logger.getLogger().write("Optimal Position found at " + currXPos + "/" + currYPos);

        // Create system and return the systemId
        try {
            UniverseCreation.addSystemToUniverse(systemId, currXPos, currYPos, userId, false);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while creation of new system", e);
            try {
                stmt.close();
            } catch (Exception e2) {
            }
            return -1;
        }

        try {
            stmt.close();
        } catch (Exception e) {
        }

        return systemId;
    }

    public static void unlockTrialUser(int userId) {
        User u = uDAO.findById(userId);
        unlockTrialUser(u);
    }

    public static void unlockTrialUser(User u) {
        u.setTrial(false);

        uDAO.update(u);
        ArrayList<PlayerPlanet> pps = ppDAO.findByUserId(u.getUserId());
        for (PlayerPlanet pp : pps) {
            at.viswars.model.System s = sDAO.findById(pDAO.findById(pp.getPlanetId()).getSystemId());
            s.setVisibility(0);
            sDAO.update(s);
        }
    }

    public static User findUserById(int userId) {
        return uDAO.findById(userId);
    }

    public static int findHomePlanetByUserId(int userId) {
        PlayerPlanet pp = ppDAO.findHomePlanetByUserId(userId);
        if (pp != null) {
            return pp.getPlanetId();
        } else {
            return 0;
        }
    }

    public static void saveLoginTracker(LoginTracker loginTracker) {
        ltDAO.add(loginTracker);

    }

    public static void addTrackingEntry(int userId, String ip) {
        LoginTracker lt = new LoginTracker();
        lt.setUserId(userId);
        lt.setLoginIP(ip);
        lt.setTime(System.currentTimeMillis());
        try {
            ltDAO.add(lt);
        } catch (Exception e) {
        }
    }

    public static LoginTracker findLatestLoginTrackerByIp(String ip) {
        return ltDAO.findLatestByIp(ip);
    }

    public static PlayerPlanet findHomePlanet(int userId) {
        return ppDAO.findHomePlanetByUserId(userId);
    }

    public static Planet findPlanetById(int planetId) {
        return pDAO.findById(planetId);
    }

    public static UserData findUserDataByUserId(int userId) {
        return udDAO.findByUserId(userId);
    }

    public static User saveUser(User user) {
        if (uDAO.findByGameName(user.getGameName()) != null) {
            return null;
        }
        return uDAO.add(user);
    }

    public static User findUserBy(String userName) {
        return uDAO.findBy(userName);
    }

    public static User findPortalUser(String userName, String portalPasswird) {
        return uDAO.findPortalUserBy(userName, portalPasswird);
    }

    public static User findUserBy(String userName, String password) {
        return uDAO.findBy(userName, password);
    }
}
