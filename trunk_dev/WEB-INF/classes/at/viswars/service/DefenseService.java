/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.GroundTroopDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.dao.PlayerTroopDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.EDefenseType;
import at.viswars.model.GroundTroop;
import at.viswars.model.Planet;
import at.viswars.model.PlanetDefense;
import at.viswars.model.PlayerTroop;
import at.viswars.model.Ressource;
import at.viswars.model.ShipDesign;
import at.viswars.model.UserData;
import at.viswars.orderhandling.OrderHandling;
import at.viswars.orderhandling.orders.StarbaseScrapOrder;
import at.viswars.orderhandling.orders.StarbaseUpgradeOrder;
import at.viswars.orderhandling.result.OrderResult;
import at.viswars.requestbuffer.StarbaseScrapBuffer;
import at.viswars.requestbuffer.UpgradeStarbaseBuffer;
import at.viswars.result.BaseResult;
import at.viswars.utilities.DefenseUtilities;
import at.viswars.utilities.ShipUtilities;
import at.viswars.view.ShipModifyEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

/**
 *
 * @author HorstRabe
 */
public class DefenseService extends Service {

    private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

    public static ArrayList<Ressource> getAllStorableRessources() {
        return ressourceDAO.findAllStoreableRessources();
    }

    public static UserData findUserDataByUserId(int userId) {
        return userDataDAO.findByUserId(userId);
    }

    public static ArrayList<GroundTroop> findAllGroundTroops() {
        return groundTroopDAO.findAll();
    }

    public static ArrayList<PlayerTroop> findAllGroundTroopsOnPlanetUser(int planetId, int userId) {
        return ptDAO.findBy(userId, planetId);
    }

    public static GroundTroop getGroundTroop(int troopId) {
        return gtDAO.findById(troopId);
    }

    public static ShipModifyEntry getShipModifyEntry(int designId, int planetId) {
        return ShipUtilities.getShipModifyEntry(designId, planetId);
    }

    public static ArrayList<ShipDesign> getPossibleUpgradeDesigns(int userId, int designId, int chassisSize) {
        ArrayList<ShipDesign> sdList = new ArrayList<ShipDesign>();
        
        sdList.addAll(sdDAO.findByUserIdAndChassis(userId, chassisSize));
        for (Iterator<ShipDesign> sdIt = sdList.iterator(); sdIt.hasNext();) {
            ShipDesign sd = sdIt.next();
            if (sd.getId() == designId) {
                sdIt.remove();
            }
        }        
        
        return sdList;
    }
        
    public static int getCountFor(int systemId, int planetId, int userId, EDefenseType edt, int unitId) {
        PlanetDefense pd = pdDAO.get(systemId, planetId, userId, edt, unitId);
        
        if (pd != null) {
            return pd.getCount();
        }
        
        return 0;
    }
    
    public static BaseResult deployStation(Map<String, String[]> allPars, int srcSystemId, int srcPlanetId, int userId) {
        int planetId = 0;
        int count = 0;
        int designId = 0;
        int defSrcSystemId = 0;
        int defSrcPlanetId = 0;
        String infoStr = "";

        for (Map.Entry<String, String[]> me : allPars.entrySet()) {
            String key = me.getKey();

            DebugBuffer.addLine(DebugLevel.TRACE, "key=" + key + " value=" + ((String[]) me.getValue())[0]);

            try {
                if (key.equalsIgnoreCase("pId")) {
                    planetId = Integer.parseInt(me.getValue()[0]);
                }
                if (key.equalsIgnoreCase("count")) {
                    count = Integer.parseInt(me.getValue()[0]);
                }
                if (key.equalsIgnoreCase("relocate")) {
                    infoStr = me.getValue()[0];
                }
            } catch (NumberFormatException e) {
                return new BaseResult("Gib doch keinen Schwachsinn ein (1)!",true);
            }
        }

        try {
            StringTokenizer st = new StringTokenizer(infoStr, "_");
            String[] elem = new String[3];
            int elemCnt = 0;

            while (st.hasMoreElements()) {
                elem[elemCnt] = (String) st.nextElement();
                elemCnt++;
            }

            designId = Integer.parseInt(elem[0]);
            defSrcPlanetId = Integer.parseInt(elem[1]);
            defSrcSystemId = Integer.parseInt(elem[2]);
        } catch (Exception e) {
            return new BaseResult("Gib doch keinen Schwachsinn ein (3)!",true);
        }

        Logger.getLogger().write("PID: " + planetId + " C: " + count + " DID: " + designId + " UID: " + userId + " DSSID: " + defSrcSystemId + " SSID: " + srcSystemId);
        if ((planetId < 0) || (count <= 0) || (designId < 1) || (userId == 0) || (defSrcSystemId != 0) && (defSrcSystemId != srcSystemId)) {
            return new BaseResult("Gib doch keinen Schwachsinn ein (2)!",true);
        }

        try {
            // Check if destination exists
            Planet p = pDAO.findById(planetId);
            if ((p != null) && (p.getSystemId() != srcSystemId)) {
                return new BaseResult("Ung�ltiger Planet!",true);
            }

            // Check if enough stations available
            boolean removeEntry = false;

            DebugBuffer.addLine(DebugLevel.DEBUG, "designId=" + designId + ", srcPlanetId=" + defSrcPlanetId + ", srcSystemId=" + defSrcSystemId);
            int actCount = getCountFor(defSrcSystemId, defSrcPlanetId, userId, EDefenseType.SPACESTATION, designId);
            if (actCount < count) {
                return new BaseResult("Soviele Stationen sind nicht m�glich!",true);
            } else if (actCount == count) {
                removeEntry = true;
            }
            
            // Check for already deployed enemy defense structures
            // All checks OK -> deploy
            // Get Current Count
            int currentCount = getCountFor(srcSystemId, planetId, userId, EDefenseType.SPACESTATION, designId);

            if (currentCount == 0) {
                PlanetDefense pd = new PlanetDefense();
                pd.setSystemId(srcSystemId);
                pd.setPlanetId(planetId);
                pd.setUnitId(designId);
                pd.setUserId(userId);
                pd.setType(EDefenseType.SPACESTATION);
                pd.setCount(count);
                pdDAO.add(pd);
            } else {
                PlanetDefense pd = pdDAO.get(srcSystemId, planetId, userId, EDefenseType.SPACESTATION, designId);
                pd.setCount(pd.getCount() + count);
                pdDAO.update(pd);
            }

            if (removeEntry) {
                PlanetDefense pd = pdDAO.get(defSrcSystemId, defSrcPlanetId, userId, EDefenseType.SPACESTATION, designId);
                pdDAO.remove(pd);                
            } else {
                PlanetDefense pd = pdDAO.get(defSrcSystemId, defSrcPlanetId, userId, EDefenseType.SPACESTATION, designId);                
                pd.setCount(pd.getCount() - count);
                pdDAO.update(pd);                
            }

        } catch (Exception e) {
            return new BaseResult("Unexpected Error: " + e.getMessage(),true);
        }

        return new BaseResult("Stationen erfolgreich umpositioniert",false);
    }
    
    public static ArrayList<PlanetDefense> getDefensesForLoc(int userId, RelativeCoordinate rc) {
        ArrayList<PlanetDefense> pdList = new ArrayList<PlanetDefense>();

        if (rc.getPlanetId() == 0) {
            pdList.addAll(pdDAO.findBySystemAndUserId(rc.getSystemId(), userId));
        } else {
            pdList.addAll(pdDAO.findByPlanetAndUserId(rc.getPlanetId(), userId));
        }

        return pdList;
    }

    public static ArrayList<PlanetDefense> getDefensesForLocByType(int userId, RelativeCoordinate rc, EDefenseType edt) {
        ArrayList<PlanetDefense> pdList = new ArrayList<PlanetDefense>();

        if (rc.getPlanetId() == 0) {
            pdList.addAll(pdDAO.findBySystemAndUserId(rc.getSystemId(), userId));
        } else {
            pdList.addAll(pdDAO.findByPlanetAndUserId(rc.getPlanetId(), userId));
        }

        for (Iterator<PlanetDefense> pdIt = pdList.iterator(); pdIt.hasNext();) {
            PlanetDefense pd = pdIt.next();
            if (pd.getType() != edt) {
                pdIt.remove();
            }
        }

        return pdList;
    }

    public static ArrayList<PlanetDefense> getDefensesForSystemByType(int userId, int systemId, EDefenseType edt) {
        ArrayList<PlanetDefense> pdList = new ArrayList<PlanetDefense>();

        HashMap<Integer,Planet> pList = SystemService.findPlanetsBySystemId(systemId);
        
        pdList.addAll(pdDAO.findBySystemAndUserId(systemId, userId));
        for (Map.Entry<Integer,Planet> me : pList.entrySet()) {
            pdList.addAll(pdDAO.findByPlanetAndUserId(me.getValue().getId(), userId));
        }
        
        for (Iterator<PlanetDefense> pdIt = pdList.iterator(); pdIt.hasNext();) {
            PlanetDefense pd = pdIt.next();
            if (pd.getType() != edt) {
                pdIt.remove();
            }
        }

        return pdList;
    }
    
    public static BaseResult scrapStarbase(int userId, StarbaseScrapBuffer ssb) {
        ShipDesignExt sdEntry = new ShipDesignExt(ssb.getDesignId());
        ShipDesign sd = (ShipDesign) sdEntry.getBase();
        
        if (!DefenseUtilities.canBeScrapped(sd, userId, ssb.getPlanetId(), ssb.getCount())) {
            return new BaseResult(ML.getMLStr("shipdesign_err_cantbemodified", userId), true);
        }

        StarbaseScrapOrder sco = new StarbaseScrapOrder(sdEntry, ssb.getPlanetId(), userId, ssb.getCount());

        OrderResult oRes = OrderHandling.scrapOrder(sco);

        ArrayList<String> errorList = new ArrayList<String>();
        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        StringBuffer errors = new StringBuffer();
        for (String error : errorList) {
            errors.append(error + "<BR>");
        }

        if (errors.length() == 0) {
            return new BaseResult(false);
        }
        return new BaseResult(errors.toString(), true);
    }
    
    public static BaseResult upgradeStarbase(int userId, UpgradeStarbaseBuffer usb) {
       // ShipModifyEntry sme = DefenseService.getShipModifyEntry(usb.getDesignId(), usb.getPlanetId());

        ShipDesignExt sdEntry = new ShipDesignExt(usb.getDesignId());
        ShipDesignExt sdEntryNew = new ShipDesignExt(usb.getToDesignId());
        ShipDesign sd = (ShipDesign) sdEntry.getBase();
        ShipDesign sdNew = (ShipDesign) sdEntryNew.getBase();

        DebugBuffer.addLine(DebugLevel.TRACE, "UserId is " + userId);

        if (!DefenseUtilities.canBeUpgraded(sd, sdNew, userId, usb.getPlanetId(), usb.getCount())) {
            return new BaseResult(ML.getMLStr("shipdesign_err_cantbemodified", userId), true);
        }

        StarbaseUpgradeOrder suo = new StarbaseUpgradeOrder(sdEntry, sdEntryNew, usb.getCount(), usb.getPlanetId());
        suo.setUserId(userId);

        OrderResult oRes = OrderHandling.upgradeOrder(suo);

        ArrayList<String> errorList = new ArrayList<String>();
        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        StringBuffer errors = new StringBuffer();
        for (String error : errorList) {
            errors.append(error + "<BR>");
        }

        if (errors.length() == 0) {
            return new BaseResult(false);
        }
        return new BaseResult(errors.toString(), true);
    }    
}
