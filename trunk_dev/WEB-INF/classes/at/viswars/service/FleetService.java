/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.Colonizing;
import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.fleet.FleetData;
import at.viswars.fleet.FleetMovement;
import at.viswars.enumeration.EFleetViewType;
import at.viswars.GameConfig;
import at.viswars.ships.ShipData;
import at.viswars.ships.ShipStorage;
import at.viswars.ships.ShipUtilities;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.ActionDAO;
import at.viswars.dao.AllianceDAO;
import at.viswars.dao.AllianceMemberDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DamagedShipsDAO;
import at.viswars.dao.DiplomacyRelationDAO;
import at.viswars.dao.FleetDetailDAO;
import at.viswars.dao.FleetFormationDAO;
import at.viswars.dao.FleetLoadingDAO;
import at.viswars.dao.GroundTroopDAO;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.dao.UserDAO;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.diplomacy.combat.CombatGroupResolver;
import at.viswars.diplomacy.combat.CombatGroupResult;
import at.viswars.diplomacy.relations.EAttackType;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EConstructionType;
import at.viswars.enumeration.EDamageLevel;
import at.viswars.enumeration.EDamagedShipRefType;
import at.viswars.enumeration.ELocationType;
import at.viswars.fleet.LoadingInformation;
import at.viswars.model.Action;
import at.viswars.model.AllianceMember;
import at.viswars.model.Construction;
import at.viswars.model.DamagedShips;
import at.viswars.model.DiplomacyType;
import at.viswars.model.Embassy;
import at.viswars.model.FleetFormation;
import at.viswars.model.FleetLoading;
import at.viswars.model.GroundTroop;
import at.viswars.model.Planet;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.PlayerFleet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.PlayerTroop;
import at.viswars.model.Ressource;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipFleet;
import at.viswars.model.User;
import at.viswars.model.UserData;
import at.viswars.movable.FleetFormationExt;
import at.viswars.movable.IFleet;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.orderhandling.OrderHandling;
import at.viswars.orderhandling.orders.ShipScrapOrder;
import at.viswars.orderhandling.result.OrderResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.requestbuffer.BufferHandling;
import at.viswars.requestbuffer.FleetParameterBuffer;
import at.viswars.requestbuffer.RepairShipBuffer;
import at.viswars.result.AttackPossibleResult;
import at.viswars.result.BaseResult;
import at.viswars.result.DiplomacyResult;
import at.viswars.result.FleetListResult;
import at.viswars.result.RegroupResult;
import at.viswars.ships.ShipStatusEntry;
import at.viswars.spacecombat.CombatHandler_NEW;
import at.viswars.spacecombat.CombatReportGenerator;
import at.viswars.spacecombat.CombatReportGenerator.CombatType;
import at.viswars.utilities.DiplomacyUtilities;
import at.viswars.utilities.FleetUtilities;
import at.viswars.utilities.MilitaryUtilities;
import at.viswars.view.ShipModifyView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.jsp.JspWriter;

/**
 *
 * @author Stefan
 */
public class FleetService {

    private static FleetLoadingDAO flDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static AllianceDAO allyDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static DiplomacyRelationDAO drDAO = (DiplomacyRelationDAO) DAOFactory.get(DiplomacyRelationDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static FleetFormationDAO ffDAO = (FleetFormationDAO) DAOFactory.get(FleetFormationDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    public static final int FLEET_ACTION_LOAD_RESS = 21;
    public static final int FLEET_ACTION_UNLOAD_RESS = 22;
    public static final int FLEET_ACTION_LOAD_TROOPS = 23;
    public static final int FLEET_ACTION_UNLOAD_TROOPS = 24;
    public static final int FLEET_ACTION_LOAD_POPULATION = 25;
    public static final int FLEET_ACTION_UNLOAD_POPULATION = 26;
    private static final boolean DEBUG = true;

    public static StringBuffer getFleetLoadingJavaScript(int type, PlayerFleetExt pfe, int userId) {
        StringBuffer js = new StringBuffer("");

        if ((pfe == null) || (userId == 0)) {
            return js;
        }
        int planetId = pfe.getRelativeCoordinate().getPlanetId();
        LoadingInformation loadInfo = pfe.getLoadingInformation();
        UserData ud = MainService.findUserDataByUserId(userId);

        switch (type) {
            case (FLEET_ACTION_LOAD_RESS):
                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("var maxPlanet" + re.getName() + " = " + PlanetService.getStockRessourceOnPlanet(planetId, re.getId()) + ";");
                }

                js.append("var maxFleetStorage = " + loadInfo.getRessSpace() + ";");
                js.append("var currentFleetStorage = " + loadInfo.getCurrLoadedRess() + ";");

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("var " + re.getName() + "Loaded = 0;");
                }

                js.append("var currTotalTransfer = 0;");
                js.append("function sumUp() { currTotalTransfer = currentFleetStorage;");

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("if (maxPlanet" + re.getName() + " > 0) { currTotalTransfer = currTotalTransfer + document.getElementById(\"load" + re.getName() + "\").value * 1; }");
                }

                js.append("}");

                js.append("function endCheck() { sumUp(); if (currTotalTransfer > maxFleetStorage) { return false; } else { return true; }}");
                js.append("function maxRess(ressId) { sumUp(); var possibleAmount = 0;");


                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("if (ressId == " + re.getId() + ") { currTotalTransfer = currTotalTransfer - document.getElementById(\"load" + re.getName() + "\").value; }");
                }

                js.append("possibleAmount = maxFleetStorage - currTotalTransfer;");

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("if ((ressId == " + re.getId() + ") && (possibleAmount > maxPlanet" + re.getName() + ")) { possibleAmount = maxPlanet" + re.getName() + "; }");
                }

                js.append("if (possibleAmount > 0) {");

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("if (ressId == " + re.getId() + ") { document.getElementById(\"load" + re.getName() + "\").value = possibleAmount; }");
                }

                js.append("}}");

                break;

            case (FLEET_ACTION_UNLOAD_RESS):
                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("var maxRess" + re.getId() + "Unload = 0;");
                }

                if ((loadInfo.getLoadedStorage() != null) && !loadInfo.getLoadedStorage().isEmpty()) {
                    for (ShipStorage ss : loadInfo.getLoadedStorage()) {
                        if (ss.getLoadtype() == 1) {
                            js.append("maxRess" + ss.getId() + "Unload = " + ss.getCount() + ";");
                        }
                    }
                }

                ProductionResult pr = null;

                try {
                    PlanetCalculation pc = new PlanetCalculation(planetId);
                    pr = pc.getPlanetCalcData().getProductionData();
                } catch (Exception e) {
                    return new StringBuffer("");
                }

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("currPlanet" + re.getName() + " = " + pr.getRessStock(re.getId()) + ";");
                }

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("stockLimit" + re.getName() + " = " + pr.getRessMaxStock(re.getId()) + ";");
                }

                js.append("function lightCheck(ressId) {");

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("if (ressId == " + re.getId() + ") { if ((document.getElementById(\"unload" + re.getId() + "\").value*1 + currPlanet" + re.getName() + ") > stockLimit" + re.getName() + ") { document.getElementById(\"info" + re.getId() + "\").src = '" + GameConfig.picPath() + "pic/ress_low.jpg'; } else { document.getElementById(\"info" + re.getId() + "\").src = '" + GameConfig.picPath() + "pic/ress_ok.jpg'; } }");
                }

                js.append("}");
                js.append("function ressMax(ressId) {");

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("if (ressId == " + re.getId() + ") { document.getElementById(\"unload" + re.getId() + "\").value = maxRess" + re.getId() + "Unload; }");
                }

                js.append("lightCheck(ressId);");
                js.append("}");

                js.append("function ressOpt(ressId) {" + "optimalAmount = 0;");

                for (Ressource re : RessourceService.getAllTransportableRessources(ud)) {
                    js.append("if (ressId == " + re.getId() + ") { optimalAmount = stockLimit" + re.getName() + " - currPlanet" + re.getName() + "; if (optimalAmount > maxRess" + re.getId() + "Unload) { optimalAmount = maxRess" + re.getId() + "Unload; } }");
                }

                js.append("if (optimalAmount < 0) { optimalAmount = 0; }" + "document.getElementById(\"unload\"+ressId).value = optimalAmount;" + "lightCheck(ressId);" + "}");
                break;
            case (FLEET_ACTION_LOAD_TROOPS):
                js.append("var currentFleetStorage = " + (loadInfo.getCurrLoadedTroops() + loadInfo.getCurrLoadedPopulation()) + ";");
                js.append("var maxFleetStorage = " + loadInfo.getTroopSpace() + ";");

                for (GroundTroop gt : DefenseService.findAllGroundTroops()) {
                    if (gt.getId() == 0) {
                        continue;
                    }
                    js.append("maxTroop" + gt.getId() + "Load = 0;");
                }

                Collection<PlayerTroop> allAvailableTroops = DefenseService.findAllGroundTroopsOnPlanetUser(planetId, userId);
                for (PlayerTroop pt : allAvailableTroops) {
                    if (pt.getTroopId() == 0) {
                        continue;
                    }
                    js.append("maxTroop" + pt.getTroopId() + "Load = " + pt.getNumber() + ";");
                }

                js.append("function sumUp() { currTotalTransfer = currentFleetStorage;");

                for (PlayerTroop pt : allAvailableTroops) {
                    if (pt.getTroopId() == 0) {
                        continue;
                    }
                    GroundTroop gt = DefenseService.getGroundTroop(pt.getTroopId());

                    js.append("currTotalTransfer = currTotalTransfer + document.getElementById(\"load" + pt.getTroopId() + "\").value * " + gt.getSize() + ";");
                }
                js.append("}");

                js.append("function endCheck() { sumUp(); if (currTotalTransfer > maxFleetStorage) { return false; } else { return true; }} ");
                js.append("function maxTroop(troopId) { sumUp(); var possibleAmount = 0;");

                for (PlayerTroop pt : allAvailableTroops) {
                    if (pt.getTroopId() == 0) {
                        continue;
                    }
                    GroundTroop gt = DefenseService.getGroundTroop(pt.getTroopId());

                    js.append("if (troopId == " + pt.getTroopId() + ") { currTotalTransfer = currTotalTransfer - (document.getElementById(\"load" + pt.getTroopId() + "\").value * " + gt.getSize() + "); }");
                }

                js.append("possibleAmount = maxFleetStorage - currTotalTransfer;");

                for (PlayerTroop pt : allAvailableTroops) {
                    if (pt.getTroopId() == 0) {
                        continue;
                    }
                    GroundTroop gt = DefenseService.getGroundTroop(pt.getTroopId());
                    js.append("if ((troopId == " + pt.getTroopId() + ") && (possibleAmount > maxTroop" + pt.getTroopId() + "Load*" + gt.getSize() + ")) { possibleAmount = maxTroop" + gt.getId() + "Load*" + gt.getSize() + "; }");
                }

                js.append("if (possibleAmount > 0) {");

                for (PlayerTroop pt : allAvailableTroops) {
                    if (pt.getTroopId() == 0) {
                        continue;
                    }
                    GroundTroop gt = DefenseService.getGroundTroop(pt.getTroopId());
                    js.append("if (troopId == " + gt.getId() + ") {");
                    js.append("document.getElementById(\"load" + gt.getId() + "\").value = (possibleAmount - (possibleAmount % " + gt.getSize() + "))/" + gt.getSize() + ";");
                    js.append("}");
                }
                js.append("}}");
                break;
            case (FLEET_ACTION_UNLOAD_TROOPS):
                for (GroundTroop gt : DefenseService.findAllGroundTroops()) {
                    if (gt.getId() == 0) {
                        continue;
                    }
                    js.append("maxTroop" + gt.getId() + "Unload = 0;");
                }

                if ((loadInfo.getLoadedStorage() != null) && !loadInfo.getLoadedStorage().isEmpty()) {
                    for (ShipStorage ss : loadInfo.getLoadedStorage()) {
                        if (ss.getLoadtype() == 2) {
                            js.append("maxTroop" + ss.getId() + "Unload = " + ss.getCount() + ";");
                        }
                    }
                }

                js.append("function maxTroop(troopId) {");

                if ((loadInfo.getLoadedStorage() != null) && !loadInfo.getLoadedStorage().isEmpty()) {
                    for (ShipStorage ss : loadInfo.getLoadedStorage()) {
                        if (ss.getLoadtype() == 2) {
                            js.append("if (troopId == " + ss.getId() + ") {" + "document.getElementById(\"unload" + ss.getId() + "\").value = maxTroop" + ss.getId() + "Unload;" + "}");
                        }
                    }
                }

                js.append("}");
                break;
            case (FLEET_ACTION_LOAD_POPULATION):
                PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
                // long bev = pp.getPopulation();
                //Dann wieviel AP gebraucht werden
                PlanetCalculation pc = null;
                try {
                    pc = new PlanetCalculation(pp);
                } catch (Exception e) {
                    return new StringBuffer("");
                }

                // long apRequired = pc.getPlanetCalcData().getExtPlanetCalcData().getUsedPopulation();
                //Und das ergibt dann, wieviel Bev&ouml;lkerung gebraucht wird
                // long minPop = PlanetService.findMinPopPossible(apRequired);
                long freePop = PlanetService.getFreePopulation(pp.getPlanetId());

                js.append("maxPopulation = " + Math.max(freePop, 0) + ";");
                js.append("maxFleetStorage = " + loadInfo.getTroopSpace() + ";");
                js.append("currentFleetStorage = " + (loadInfo.getCurrLoadedPopulation() + loadInfo.getCurrLoadedTroops()) + ";");
                js.append("currTotalTransfer = 0;");

                js.append("function sumUp() {" + "currTotalTransfer = currentFleetStorage;" + "if (maxPopulation > 0) { currTotalTransfer = currTotalTransfer + document.getElementById(\"loadpop\").value * 1; }" + "}");

                js.append("function endCheck() {" + "sumUp();" + "if (currTotalTransfer > maxFleetStorage) { " + "return false; " + "} else { " + "return true; " + "}}");

                js.append("function maxPop() {" + "sumUp();" + "var possibleAmount = 0;" + "currTotalTransfer = currTotalTransfer - document.getElementById(\"loadpop\").value;" + "possibleAmount = maxFleetStorage - currTotalTransfer;" + "if (possibleAmount > maxPopulation) { " + "possibleAmount = maxPopulation; " + "}" + "if (possibleAmount > 0) {" + "document.getElementById(\"loadpop\").value = possibleAmount; " + "}}");
                break;
            case (FLEET_ACTION_UNLOAD_POPULATION):
                pp = ppDAO.findByPlanetId(planetId);
                pc = null;
                try {
                    pc = new PlanetCalculation(pp);
                } catch (Exception e) {
                    return new StringBuffer("");
                }

                js.append("var maxPopUnload = 0;");


                if ((loadInfo.getLoadedStorage() != null) && !loadInfo.getLoadedStorage().isEmpty()) {
                    for (ShipStorage ss : loadInfo.getLoadedStorage()) {
                        if (ss.getLoadtype() == 4) {

                            js.append("maxPopUnload = " + ss.getCount() + ";");

                        }
                    }
                }

                js.append("currPlanetPop = " + pp.getPopulation() + ";" + "var popLimit = " + pc.getPlanetCalcData().getExtPlanetCalcData().getMaxPopulation() + ";");

                js.append("function lightCheck() {" + "if ((document.getElementById(\"unloadPop\").value*1 + currPlanetPop) > popLimit) { document.getElementById(\"info\").src = '" + GameConfig.picPath() + "pic/ress_low.jpg'; } else { document.getElementById(\"info\").src = '" + GameConfig.picPath() + "pic/ress_ok.jpg'; }" + "}");

                js.append("function popMax() {" + "document.getElementById(\"unloadPop\").value = maxPopUnload;" + "lightCheck();" + "}");

                js.append("function popOpt() {" + "optimalAmount = 0;" + "optimalAmount = popLimit - currPlanetPop; if (optimalAmount > maxPopUnload) { optimalAmount = maxPopUnload; }" + "if (optimalAmount < 0) { optimalAmount = 0; }" + "document.getElementById(\"unloadPop\").value = optimalAmount;" + "lightCheck();" + "}");
                break;
        }

        return js;
    }

    public static boolean canFight(PlayerFleetExt pfe) {

        ArrayList<Integer> users = new ArrayList<Integer>();
        for (PlayerFleet pf : (ArrayList<PlayerFleet>) pfDAO.findBy(pfe.getRelativeCoordinate())) {
            if (!users.contains(pf.getUserId())) {
                users.add(pf.getUserId());
            }
        }
        boolean systemNeutral = true;
        boolean systemOwn = false;
        for (Planet p : pDAO.findBySystemId(pfe.getRelativeCoordinate().getSystemId())) {
            PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
            if (ppDAO.findByPlanetId(p.getId()) != null) {
                systemNeutral = false;
                if (pp.getUserId() == pfe.getUserId()) {
                    systemOwn = true;
                }
            }
        }
        for (Integer i : users) {
            if (i == pfe.getUserId()) {
                continue;
            }
            DiplomacyResult dr = DiplomacyUtilities.getDiplomacyResult(i, pfe.getUserId());
            if (dr.getBattleInNeutral().equals(EAttackType.OPTIONAL) && systemNeutral
                    || dr.getBattleInOwn().equals(EAttackType.OPTIONAL) && systemOwn
                    || dr.getBattleInNeutral().equals(EAttackType.YES) && systemNeutral) {
                return true;
            }

        }

        return false;

    }

    public static PlayerFleetExt getFleet(
            int fleetId) {
        try {
            PlayerFleetExt pfExt = new PlayerFleetExt(fleetId);
            return pfExt;
        } catch (Exception e) {
            return null;
        }

    }

    public static FleetFormationExt getFleetFormation(
            int fleetId) {
        try {
            FleetFormationExt ffExt = new FleetFormationExt(fleetId);
            return ffExt;
        } catch (Exception e) {
            return null;
        }

    }

    public static boolean scannerAvailable(int planetId, int userId) {
        for (Planet p : pDAO.findBySystemId(pDAO.findById(planetId).getSystemId())) {
            PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
            if (pp == null || pp.getUserId() != userId) {
                continue;
            }
            if (pcDAO.isConstructed(p.getId(), Construction.ID_SCANNERPHALANX)) {
                return true;
            }
        }
        return false;
    }

    public static int getCountForBuilding(int planetId, int constructionId) {
        PlanetConstruction pc = pcDAO.findBy(planetId, constructionId);
        if (pc == null) {
            return 0;
        } else {
            return pcDAO.findBy(planetId, constructionId).getNumber();
        }

    }

    public static FleetListResult getFleetList(
            int userId, int systemId, int planetId, EFleetViewType fvt) {
        FleetListResult result = new FleetListResult();

        // Get Fleet formations and single Fleets         
        ArrayList<PlayerFleet> pfList = new ArrayList<PlayerFleet>();

        PlayerFleet pfSearch = new PlayerFleet();


        HashMap<Integer, DiplomacyResult> diploBuffer = new HashMap<Integer, DiplomacyResult>();
        HashMap<Integer, HashMap<Integer, Boolean>> ownFleet = new HashMap<Integer, HashMap<Integer, Boolean>>();
        ArrayList<Integer> sharingUsers = new ArrayList<Integer>();

        if (fvt == EFleetViewType.ALL_PLANET) {
            pfSearch.setPlanetId(planetId);
        } else if (fvt == EFleetViewType.ALL_SYSTEM) {
            pfSearch.setSystemId(systemId);
        } else if (fvt == EFleetViewType.ALL_SYSTEM_OTHER_USERS) {
            pfSearch.setSystemId(systemId);
        } else if (fvt == EFleetViewType.ALL_SYSTEM_ALLIED) {
            sharingUsers = DiplomacyUtilities.findSharingUsers(userId);
            pfSearch.setSystemId(systemId);
        } else if (fvt == EFleetViewType.ALL_SYSTEM_ENEMY) {
            pfSearch.setSystemId(systemId);
        } else if (fvt == EFleetViewType.OWN_ALL) {
            pfSearch.setUserId(userId);
        } else if (fvt == EFleetViewType.OWN_ALL_PLANET) {
            pfSearch.setUserId(userId);
            pfSearch.setSystemId(systemId);
            pfSearch.setPlanetId(planetId);
        } else if ((fvt == EFleetViewType.OWN_SYSTEM) || (fvt == EFleetViewType.OWN_ALL_TRANSPORT_SYSTEM)) {
            pfSearch.setUserId(userId);
            pfSearch.setSystemId(systemId);
        } else if ((fvt == EFleetViewType.OWN_NOT_IN_SYSTEM) || (fvt == EFleetViewType.OWN_ALL_TRANSPORT_NOT_IN_SYSTEM)) {
            pfSearch.setUserId(userId);
        } else if (fvt == EFleetViewType.OWN_ALL_WITH_ACTION) {
            pfSearch.setUserId(userId);
        }

        if ((fvt == EFleetViewType.ENEMY_NOT_IN_SYSTEM) || (fvt == EFleetViewType.ALLIED_NOT_IN_SYSTEM)) {
            pfList = pfDAO.findAll();
            for (PlayerFleet pf : pfList) {
                HashMap<Integer, Boolean> entry1 = ownFleet.get(pf.getSystemId());
                if (entry1 == null) {
                    entry1 = new HashMap<Integer, Boolean>();
                }
                if (pf.getUserId() == userId) {
                    entry1.put(pf.getPlanetId(), true);
                }
                ownFleet.put(pf.getSystemId(), entry1);
            }
        } else {
            pfList = pfDAO.find(pfSearch);
        }
        //Check vor scannerphalanx

        boolean scannerPhalanxFound = false;
        if (fvt == EFleetViewType.ALL_SYSTEM_ENEMY) {
            for (Planet p : pDAO.findBySystemId(systemId)) {
                PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
                if (pp != null) {
                    if (diploBuffer.get(pp.getUserId()) == null) {
                        diploBuffer.put(pp.getUserId(), DiplomacyUtilities.getDiplomacyResult(userId, pp.getUserId()));
                    }
                    // if (!diploBuffer.get(pp.getUserId()).isSharingMap() && (pp.getUserId() != userId)) {
                    //    continue;
                    // } else {
                    if (pcDAO.findBy(p.getId(), Construction.ID_SCANNERPHALANX) != null && (pp.getUserId() == userId || diploBuffer.get(pp.getUserId()).isSharingMap())) {
                        scannerPhalanxFound = true;
                        break;
                    }
                    // }
                }
            }
        }



        // Filter Data        
        HashSet<Integer> skip = new HashSet<Integer>();

        for (PlayerFleet pf : pfList) {
            if (skip.contains(pf.getId())) {
                continue;
            }

            if (fvt != EFleetViewType.OWN_ALL_WITH_ACTION) {
                if (pf.getStatus() != 0) {
                    continue;
                }

            }
            if (fvt == EFleetViewType.ALL_SYSTEM_ALLIED) {
                if (pf.getUserId() == userId) {
                    continue;
                }

                // Logger.getLogger().write("Checking other fleet user " + pf.getUserId() + " vs current user " + userId + " = " + DiplomacyUtilities.getDiplomacyRel(pf.getUserId(), userId).getDiplomacyTypeId());
                if (DiplomacyUtilities.getDiplomacyRel(pf.getUserId(), userId).getDiplomacyTypeId() > DiplomacyType.TREATY) {
                    continue;
                }

            }

            if (fvt == EFleetViewType.ALLIED_NOT_IN_SYSTEM) {
                //Search for fleets on this position of allys or self
                boolean fleetFound = false;
                boolean planetFound = false;

                if (fvt == EFleetViewType.ALLIED_NOT_IN_SYSTEM) {
                    if (pf.getSystemId() == systemId) {
                        continue;
                    }
                }

                if (diploBuffer.get(pf.getUserId()) == null) {
                    diploBuffer.put(pf.getUserId(), DiplomacyUtilities.getDiplomacyResult(userId, pf.getUserId()));
                }
                if (!diploBuffer.get(pf.getUserId()).isHelps()) {
                    continue;

                }

                if (pf.getUserId() == userId) {
                    continue;
                }

                PlayerPlanet pp = ppDAO.findByPlanetId(pf.getPlanetId());
                if (pp != null && pp.getUserId() == userId) {
                    planetFound = true;
                }

                if (ownFleet.get(pf.getSystemId()) != null && ownFleet.get(pf.getSystemId()).get(pf.getPlanetId()) != null) {
                    fleetFound = true;
                }
                if (!fleetFound && !planetFound) {
                    continue;
                }
            }

            if (fvt == EFleetViewType.ALL_SYSTEM_ENEMY || fvt == EFleetViewType.ENEMY_NOT_IN_SYSTEM) {
                //Search for fleets on this position of allys or self
                boolean fleetFound = false;
                boolean planetFound = false;

                if (fvt == EFleetViewType.ENEMY_NOT_IN_SYSTEM) {
                    if (pf.getSystemId() == systemId) {
                        continue;
                    }
                }

                if (diploBuffer.get(pf.getUserId()) == null) {
                    diploBuffer.put(pf.getUserId(), DiplomacyUtilities.getDiplomacyResult(userId, pf.getUserId()));
                }
                if (diploBuffer.get(pf.getUserId()).isHelps()) {
                    continue;

                }

                if (pf.getUserId() == userId) {
                    continue;
                }

                PlayerPlanet pp = ppDAO.findByPlanetId(pf.getPlanetId());
                if (pp != null && pp.getUserId() == userId) {
                    planetFound = true;
                }

                if (ownFleet.get(pf.getSystemId()) != null && ownFleet.get(pf.getSystemId()).get(pf.getPlanetId()) != null) {
                    fleetFound = true;
                }
                if (!fleetFound && !scannerPhalanxFound && !planetFound) {
                    continue;
                }
            }

            if (fvt == EFleetViewType.ALL_SYSTEM_OTHER_USERS) {
                if (pf.getUserId() == userId) {
                    continue;
                }

            } else if ((fvt == EFleetViewType.OWN_NOT_IN_SYSTEM) || (fvt == EFleetViewType.OWN_ALL_TRANSPORT_NOT_IN_SYSTEM)) {
                if (pf.getSystemId() == systemId) {
                    continue;
                }

            } else if (fvt == EFleetViewType.OWN_ALL_WITH_ACTION) {
                if (pf.getStatus() == 0) {
                    continue;
                }

            }

            try {
                if (pf.getFleetFormationId() != 0) {
                    if ((fvt == EFleetViewType.OWN_ALL_TRANSPORT_SYSTEM) || (fvt == EFleetViewType.OWN_ALL_TRANSPORT_NOT_IN_SYSTEM)) {
                        continue;
                    }

                    FleetFormationExt ffe = new FleetFormationExt(pf.getFleetFormationId());

                    ArrayList<PlayerFleetExt> fleetsInFormation = ffe.getParticipatingFleets();
                    for (PlayerFleetExt pfe : fleetsInFormation) {
                        skip.add(pfe.getBase().getId());
                    }

                    result.addFleetFormation(ffe);
                    continue;

                }


            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, e.getMessage());
            }

            PlayerFleetExt pfExt = new PlayerFleetExt(pf.getId());
            if ((fvt == EFleetViewType.OWN_ALL_TRANSPORT_SYSTEM) || (fvt == EFleetViewType.OWN_ALL_TRANSPORT_NOT_IN_SYSTEM)) {
                LoadingInformation li = pfExt.getLoadingInformation();
                if ((li.getRessSpace() == 0) && (li.getTroopSpace() == 0) && ((li.getLargeHangarSpace() + li.getMediumHangarSpace() + li.getSmallHangarSpace()) == 0)) {
                    continue;
                }

            }

            result.addSingleFleet(pfExt);
        }


        return result;
    }

    public static void renameFleet(int fleetId, String newName) {
        PlayerFleet pf = pfDAO.findById(fleetId);
        if (pf == null) {
            return;
        }

        pf.setName(newName);
        pfDAO.update(pf);
    }

    public static void renameFleetFormation(int formationId, String newName) {
        FleetFormation ff = ffDAO.findById(formationId);
        if (ff == null) {
            return;
        }

        ff.setName(newName);
        ffDAO.update(ff);
    }

    public static void setRetreatFactor(int fleetId, int retreatFactor, int retreatToType, int retreatTo) {
    }

    public static void loadDetailInformation(ArrayList<FleetData> fleetList) {
    }

    public static PlayerFleetExt getDetailedFleetData(
            int fleetId) {
        PlayerFleetExt extInf = getFleet(fleetId);
        return extInf;
    }

    public static synchronized BaseResult disbandFormation(
            int formationId, int userId) {
        try {
            FleetFormationExt disbandFormation = new FleetFormationExt(formationId);
            if (disbandFormation == null) {
                return new BaseResult("Formation existiert nicht", true);
            }

            if (disbandFormation.getBase().getUserId() != userId) {
                return new BaseResult("Formation geh&ouml;rt dir nicht", true);
            }

            ArrayList<PlayerFleetExt> pfList = disbandFormation.getParticipatingFleets();
            for (PlayerFleetExt pfe : pfList) {
                pfe.getBase().setFleetFormationId(0);
                pfDAO.update(pfe.getBase());
            }

            ffDAO.remove(disbandFormation.getBase());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while creating FleetFormation", e);
            return new BaseResult(e.getMessage(), true);
        }

        return new BaseResult("Formation wurde erfolgreich aufgel&ouml;st", false);
    }

    public static synchronized BaseResult removeFromFormation(
            int fleetId, int userId) {
        try {
            PlayerFleetExt leavingFleet = null;
            FleetFormationExt formationToLeave = null;

            leavingFleet =
                    new PlayerFleetExt(fleetId);

            if (leavingFleet.getBase().getUserId() != userId) {
                return new BaseResult("Ung&uuml;ltige Flotte: Nicht in ihrem Besitz!", true);
            }

            if (!leavingFleet.isInFleetFormation()) {
                return new BaseResult("Flotte ist in keiner Flottenformation!", true);
            }

            formationToLeave = new FleetFormationExt(leavingFleet.getBase().getFleetFormationId());
            if (formationToLeave.getParticipatingFleets().size() == 1) {
                ffDAO.remove(formationToLeave.getBase());
            }

            PlayerFleet pf = leavingFleet.getBase();
            pf.setFleetFormationId(0);
            pfDAO.update(pf);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while creating FleetFormation", e);
            return new BaseResult(e.getMessage(), true);
        }

        return new BaseResult("Flotte wurde erfolgreich aus dem Verband entfernt", false);
    }

    public static synchronized BaseResult addToFormation(
            Map<String, String[]> allPars, int userId) {
        try {
            PlayerFleetExt joiningFleet = null;
            FleetFormationExt formationToJoin = null;

            int sourceSystem = -1;
            int sourcePlanet = -1;

            if (allPars.containsKey("fleetId")) {
                String tmp = allPars.get("fleetId")[0];
                if (tmp.equalsIgnoreCase("")) {
                    return new BaseResult("Keine Quellflotte vorhanden", true);
                }

                int sourceFleetId = -1;
                if (tmp != null) {
                    sourceFleetId = Integer.parseInt(tmp);
                }

                joiningFleet = new PlayerFleetExt(sourceFleetId);

                if (joiningFleet.getBase().getUserId() != userId) {
                    return new BaseResult("Ung&uuml;ltige Quellflotte: nicht in ihrem Besitz!", true);
                } else {
                    sourceSystem = joiningFleet.getBase().getSystemId();
                    sourcePlanet =
                            joiningFleet.getBase().getPlanetId();
                }

                if (joiningFleet.isInFleetFormation()) {
                    return new BaseResult("Flotte bereits in einer Flottenformation!", true);
                }

            }

            boolean createNewFormation = false;

            if (allPars.containsKey("formationId")) {
                String tmp = allPars.get("formationId")[0];
                if (tmp.equalsIgnoreCase("")) {
                    return new BaseResult("Ung&uuml;ltiger Zielverband", true);
                }

                int targetFormationId = -1;
                if (tmp != null) {
                    targetFormationId = Integer.parseInt(tmp);
                }

                if (targetFormationId == 0) {
                    createNewFormation = true;
                } else {
                    formationToJoin = new FleetFormationExt(targetFormationId);

                    if ((formationToJoin.getBase().getSystemId() != sourceSystem) || (formationToJoin.getBase().getPlanetId() != sourcePlanet)) {
                        return new BaseResult("Formation nicht im gleichen System wie Flotte", true);
                    }

                }
            }

            if (createNewFormation) {
                FleetFormation ff = new FleetFormation();
                ff.setName("Neue Formation");
                ff.setUserId(userId);
                ff =
                        ffDAO.add(ff);

                PlayerFleet pf = joiningFleet.getBase();
                pf.setFleetFormationId(ff.getId());
                pfDAO.update(pf);
            } else {
                PlayerFleet pf = joiningFleet.getBase();
                pf.setFleetFormationId(formationToJoin.getBase().getId());
                pfDAO.update(pf);
            }

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while creating FleetFormation", e);
            return new BaseResult(e.getMessage(), true);
        }

        return new BaseResult("Flotte wurde erfolgreich dem Verband hinzugef�gt", false);
    }

    /*
     * @TODO Storagehandling und Sicherheitsabfrage falls Flotte in Bewegung ist 
     */
    public static synchronized RegroupResult regroupFleet(
            Map<String, String[]> allMovePars, int userId, int systemId, boolean damagedFirst) {
        //boolean success = false;
        boolean validValues = false;
        int sourceFleetId = 0;
        int targetFleetId = 0;
        int sourceSystem = 0;
        int sourcePlanet = 0;
        boolean deleteSourceFleet = false;

        try {
            FleetData srcFleet = null;
            FleetData tgtFleet = null;

            // userId und systemId checken
            if ((userId == 0) || (systemId == 0)) {
                return new RegroupResult("Ung&uuml;tige Parameter", true);
            }

            if (allMovePars.containsKey("fleetId")) {
                String tmp = allMovePars.get("fleetId")[0];
                if (tmp.equalsIgnoreCase("")) {
                    return new RegroupResult("Keine Quellflotte vorhanden", true);
                }

                if (tmp != null) {
                    sourceFleetId = Integer.parseInt(tmp);
                }

                srcFleet = new FleetData(sourceFleetId);

                if (srcFleet.getUserId() != userId) {
                    return new RegroupResult("Ung&uuml;ltige Quellflotte: nicht in ihrem Besitz!", true);
                } else {
                    sourceSystem = srcFleet.getSystemId();
                    sourcePlanet =
                            srcFleet.getPlanetId();
                }

            }

            if (allMovePars.containsKey("targetfleet")) {
                String tmp = ((String[]) allMovePars.get("targetfleet"))[0];
                if (tmp.equalsIgnoreCase("")) {
                    return new RegroupResult("Keine Zielflotte vorhanden", true);
                }

                if (tmp != null) {
                    targetFleetId = Integer.parseInt(tmp);
                }

                if (targetFleetId != 0) {
                    tgtFleet = new FleetData(targetFleetId);

                    if (tgtFleet == null) {
                        return new RegroupResult("Zielflotte irgendwie net da", true);
                    }

                    if ((tgtFleet.getSystemId() != srcFleet.getSystemId()) || (tgtFleet.getPlanetId() != srcFleet.getPlanetId()) || (tgtFleet.getUserId() != srcFleet.getUserId())) {
                        return new RegroupResult("Ung&uuml;ltige Zielflotte: nicht am selben Standort wie Quellflotte!", true);
                    }

                }
            }

            if (tgtFleet != null) {
                if (srcFleet.getFleetId() == tgtFleet.getFleetId()) {
                    return new RegroupResult("Ziel- und Quellflotte sind identisch!", true);
                }

            }

            // Check if source or target is scanning
            if ((FleetService.findRunningScan(userId, srcFleet.getFleetId()) != null) ||
                ((tgtFleet != null) && (FleetService.findRunningScan(userId, tgtFleet.getFleetId()) != null))) {          
                return new RegroupResult("Quell- oder Zielflotte f�hren einen Scan durch", true);
            }
            
            // Alle Bauparameter durchgehen und ?berpr?fen
            for (final Map.Entry<String, String[]> entry : allMovePars.entrySet()) {
                String parName = entry.getKey();
                String[] valueStr = entry.getValue();

                if (parName.startsWith("stype")) {
                    int designId = Integer.parseInt(parName.substring(5));
                    int count = Integer.parseInt(valueStr[0]);
                    if (count > 0) {
                        ShipFleet sf = sfDAO.getByFleetDesign(sourceFleetId, designId);
                        if ((sf == null) || (sf.getCount() < count)) {
                            return new RegroupResult("Ung&uuml;ltige(r) Schiffstyp/-anzahl in Anfrage", true);
                        }

                        validValues = true;
                    }
                }
            }

            if (!validValues) {
                return new RegroupResult("Ung&uuml;tige Parameter", false);
            }

            srcFleet = ShipUtilities.loadLoadingInformation(srcFleet);
            if ((srcFleet.getCurrLoadedLargeHangar() > 0) || (srcFleet.getCurrLoadedMediumHangar() > 0) || (srcFleet.getCurrLoadedSmallHangar() > 0)) {
                return new RegroupResult("Es d&uuml;rfen sich keine Schiffe in den Hangars befinden, wenn die Flotte umgruppiert wird. Erst entladen, dann gehts weiter", true);
            }

            if (targetFleetId != 0) {
                tgtFleet = ShipUtilities.loadLoadingInformation(tgtFleet);
                if ((tgtFleet.getCurrLoadedLargeHangar() > 0) || (tgtFleet.getCurrLoadedMediumHangar() > 0) || (tgtFleet.getCurrLoadedSmallHangar() > 0)) {
                    return new RegroupResult("Es d&uuml;rfen sich keine Schiffe in den Hangars befinden (auch nicht in der Zielflotte), wenn die Flotte umgruppiert wird. Erst entladen, dann gehts weiter", true);
                }

            }


            // Flotte verschieben
            boolean createNewFleet = (targetFleetId == 0);
            boolean intoNewFleet = createNewFleet;

            for (final Map.Entry<String, String[]> entry : allMovePars.entrySet()) {
                String parName = entry.getKey();
                String[] valueStr = entry.getValue();

                if (parName.startsWith("stype")) {
                    int designId = Integer.parseInt(parName.substring(5));
                    int count = Integer.parseInt(valueStr[0]);
                    if (count > 0) {
                        ShipFleet sf = sfDAO.getByFleetDesign(sourceFleetId, designId);
                        ArrayList<DamagedShips> dsList = dsDAO.findById(sf.getId());

                        int available = sf.getCount();
                        boolean moveAll = available == count;

                        if (createNewFleet) {
                            if (targetFleetId == 0) {
                                PlayerFleet newFleet = pfDAO.createNewFleet("Neue Flotte %ID%", userId, sourceSystem, sourcePlanet);
                                targetFleetId = newFleet.getId();
                                createNewFleet = false;
                            }
                        }

                        ShipFleet sfNew = sfDAO.updateOrCreate(targetFleetId, designId, count);

                        transferDamagedShips(sf.getId(), sfNew.getId(), count, damagedFirst);

                        if (moveAll) {
                            sfDAO.remove(sf);
                        } else {
                            sfDAO.updateOrCreate(sourceFleetId, designId, -count);
                        }

                        // Delete Source Fleet falls leer
                        ArrayList<ShipFleet> shipsInSource = sfDAO.findByFleetId(sourceFleetId);


                        if (shipsInSource.isEmpty()) {
                            // Ladung komplett verschieben
                            FleetUtilities.moveFleetLoadingTotal(sourceFleetId, targetFleetId);

                            PlayerFleet pf = new PlayerFleet();
                            pf.setId(sourceFleetId);
                            pfDAO.remove(pf);
                            deleteSourceFleet = true;
                        } else {
                            // Ladung teilweise verschieben
                            FleetUtilities.moveExcessFleetLoading(sourceFleetId, targetFleetId);
                        }
                    }
                }
            }
        } catch (NumberFormatException nfe) {
            return new RegroupResult("Ung&uuml;tige Werte", true);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in FleetService - regroupFleet: ", e);
            return new RegroupResult(e.getMessage(), true);
        }

        return new RegroupResult("Die Flotte wurde erfolgreich umgruppiert", false, deleteSourceFleet);
    }

    public static FleetParameterBuffer createRequestBuffer(
            int userId) {
        return new FleetParameterBuffer(userId);
    }

    public static FleetParameterBuffer getRequestBuffer(
            int userId, int id) {
        return (FleetParameterBuffer) BufferHandling.getBuffer(userId, id);
    }

    public static BaseResult checkFlightRequest(
            int userId, FleetParameterBuffer fpb) {
        return FleetMovement.checkFlightParameters(userId, fpb);
    }

    public static synchronized BaseResult processFlightRequest(
            int userId, FleetParameterBuffer fpb) {
        BaseResult br = null;

        try {
            IFleet fleetObj;

            if (fpb.isFleetFormation()) {
                fleetObj = new FleetFormationExt(fpb.getFleetId());
            } else {
                fleetObj = new PlayerFleetExt(fpb.getFleetId());
            }

             
            if (fleetObj.isMoving()) {
                DebugBuffer.addLine(DebugLevel.WARNING, "A moving fleet " + fpb.getFleetId() + " wanted to be processed for flight movment!");
                return new BaseResult("Flotte ist bereits unterwegs", true);
            }

            br = FleetMovement.generateFlight(userId, fpb);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in FleetMovement (" + fpb.getFleetId() + "): ", e);
            br = new BaseResult(e.getMessage(), true);
        }

        return br;
    }

    public static synchronized BaseResult callBackFleet(int fleetId, int userId) {

        BaseResult br = null;

        try {
            br = FleetMovement.callBackFleet(fleetId, userId);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in FleetMovement", e);
            br = new BaseResult(e.getMessage(), true);
        }

        return br;
    }

    public static synchronized BaseResult callBackFleetFormation(int fleetFormationId, int userId) {

        BaseResult br = null;
        try {
            br = FleetMovement.callBackFleetFormation(fleetFormationId, userId);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in FleetMovement", e);
            br = new BaseResult(e.getMessage(), true);
        }

        return br;
    }

    public static Integer findRunningScan(int userId, int fleetId) {
        Action a = new Action();
        a.setUserId(userId);
        a.setFleetId(fleetId);
        a.setType(EActionType.SCAN_PLANET);
        ArrayList<Action> actions = aDAO.find(a);
        a = new Action();
        a.setUserId(userId);
        a.setFleetId(fleetId);
        a.setType(EActionType.SCAN_SYSTEM);
        actions.addAll(aDAO.find(a));
        if (actions.size() > 0) {
            return actions.get(0).getNumber();
        } else {
            return null;
        }

    }

    public static boolean canCreateEmbassy(PlayerFleetExt pfe) {
        PlayerPlanet pp = ppDAO.findByPlanetId(pfe.getRelativeCoordinate().getPlanetId());
        if (pp == null) {
            return false;
        }
        if (pp.getUserId() == pfe.getUserId()) {
            return false;
        }
        if (!pp.getHomeSystem()) {
            return false;
        }
        Embassy e = Service.embassyDAO.findBy(pfe.getUserId(), pp.getUserId());
        if (e != null) {
            return false;
        }
        return true;
    }

    public static boolean canScanPlanet(IFleet fleet) {
        if (fleet.isMoving() || fleet.getRelativeCoordinate().getPlanetId() == 0 || findRunningScan(fleet.getUserId(), fleet.getId()) != null) {
            
            return false;
        }

        PlayerPlanet pp = ppDAO.findByPlanetId(fleet.getRelativeCoordinate().getPlanetId());

        ArrayList<ShipData> shipList = fleet.getShipList();
        for (ShipData sd : shipList) {
            if ((sd.getChassisSize() >= 3) && ((pp == null) || (pp.getUserId() != fleet.getUserId()))) {
                return true;
            }

        }

        return false;
    }

    public static boolean canScanSystem(IFleet fleet) {
        if (fleet.isMoving() || fleet.getRelativeCoordinate().getPlanetId() != 0 || findRunningScan(fleet.getUserId(), fleet.getId()) != null) {
    
            return false;
        }
        ArrayList<ShipData> shipList = fleet.getShipList();
        for (ShipData sd : shipList) {
            if ((sd.getChassisSize() >= 3)) {
                return true;
            }

        }

        return false;
    }

    public static boolean canColonize(PlayerFleetExt pf) {
        return Colonizing.calcColonizeableByFleet(pf);
    }

    public static BaseResult colonize(
            int fleetId) {
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);
        return Colonizing.colonize(pfe);
    }

    public static synchronized void scanPlanet(IFleet fleet) {
        if (canScanPlanet(fleet)) {
            Action a = new Action();

            a.setUserId(fleet.getUserId());
            a.setFleetId(fleet.getId());
            a.setType(EActionType.SCAN_PLANET);
            a.setNumber(1);
            aDAO.add(a);
        }

    }
    
    public static ArrayList<Integer> fleetsInSystemZeroWithStatus0(){
        ArrayList<Integer> fleets = new ArrayList<Integer>();
        for(PlayerFleet pf : (ArrayList<PlayerFleet>)Service.playerFleetDAO.findAll()){
            if(pf.getSystemId() == 0 && pf.getStatus() == 0){
                fleets.add(pf.getId());
            }
        }
        return fleets;
    } 
    public static ArrayList<Integer> fleetsInSystemZeroWithoutFleetDetails(){
        ArrayList<Integer> fleets = new ArrayList<Integer>();
        for(PlayerFleet pf : (ArrayList<PlayerFleet>)Service.playerFleetDAO.findAll()){
            PlayerFleetExt pfe = new PlayerFleetExt(pf);
            if(pf.getSystemId() == 0 && pf.getStatus() == 1 && Service.fleetDetailDAO.findByFleetId(pf.getId()) == null){
                fleets.add(pf.getId());
            }
        }
        return fleets;
    }


    public static synchronized BaseResult scanSystem(IFleet fleet) {
        BaseResult br = null;
        if (canScanSystem(fleet)) {
            Action a = new Action();

            a.setUserId(fleet.getUserId());
            a.setFleetId(fleet.getId());
            a.setType(EActionType.SCAN_SYSTEM);
            int duration = 0;
            int systemId = fleet.getRelativeCoordinate().getSystemId();
            ArrayList<Planet> planets = pDAO.findBySystemId(systemId);
            //Fleetcheck => Battle if an enemy fleet found
            boolean enemyFleetFound = false;
            boolean enemyDefenseFound = false;
            for (Planet p_list : planets) {
                ArrayList<PlayerFleet> playerFleets = pfDAO.findByPlanetId(p_list.getId());
                for (PlayerFleet pf : playerFleets) {
                    if (pf.getUserId() == fleet.getUserId()) {
                        continue;
                    }
                    if (DiplomacyUtilities.getDiplomacyResult(fleet.getUserId(), pf.getUserId()).isAttacks()) {
                        enemyFleetFound = true;
                        break;
                    }
                }
                //If no fleet search for planetary defense
                if (!enemyFleetFound) {
                    for (PlanetConstruction pc : Service.planetConstructionDAO.findByPlanetId(p_list.getId())) {
                        Construction c = Service.constructionDAO.findById(pc.getConstructionId());
                        if (c.getType().equals(EConstructionType.PLANETARY_DEFENSE) && pc.getNumber() > 0) {
                            enemyDefenseFound = true;
                            break;
                        }
                    }
                }
                //Stop scan => Set Fleet to position
                if (enemyFleetFound || enemyDefenseFound) {
                    PlayerFleet pf = pfDAO.findById(fleet.getId());
                    pf.setLocation(ELocationType.PLANET, p_list.getId());
                    // pf.setPlanetId(p_list.getId());
                    pfDAO.update(pf);
                    playerFleets = pfDAO.findByPlanetId(pf.getPlanetId());
                    fleet = new PlayerFleetExt(pf.getId());

                    HashSet<Integer> retreatedFleets;
                    if (fleet.getRelativeCoordinate().getSystemId() == 0) {
                        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Invoking Combat in GroundCombatUtilities while Scanning with System 0");
                        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "fleetId : " + fleet.getId());

                    } else {
                        CombatReportGenerator crg = new CombatReportGenerator();
                        CombatHandler_NEW ch;
                        if (fleet.getRelativeCoordinate().getPlanetId() == 0) {
                            ch = new CombatHandler_NEW(CombatType.SYSTEM_COMBAT, playerFleets, fleet.getRelativeCoordinate(), fleet.getAbsoluteCoordinate(), crg);
                        } else {
                            ch = new CombatHandler_NEW(CombatType.ORBITAL_COMBAT, playerFleets, fleet.getRelativeCoordinate(), fleet.getAbsoluteCoordinate(), crg);

                        }

                        ch.executeBattle();
                        ch.processCombatResults();

                        retreatedFleets = ch.getRetreatedOrResignedFleets();
                        Iterator<Integer> fleets = retreatedFleets.iterator();
                        while (fleets.hasNext()) {
                            Integer fleetId = fleets.next();
                            //The scanning fleet retreated => set it to system 
                            PlayerFleet pfTmp = pfDAO.findById(fleetId);
                            if (fleetId == pf.getId()) {
                                pfTmp.setLocation(ELocationType.SYSTEM, systemId);
                                // pfTmp.setSystemId(systemId);
                                // pfTmp.setPlanetId(0);
                                pfDAO.update(pfTmp);
                                //Enemy Fleet retreats
                            } else {
                                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Function not yet programmed, ray is a bad boy ! : D ");
                                //FleetMovement.retreatFleet2(null, pf.getUserId());
                            }


                        }
                        crg.writeReportsToDatabase();
                    }
                    return new BaseResult("Sie sind einer feindlichen Flotte oder planetarer Verteidigung bei Planet#" + p_list.getId() + " begegnet", true);
                }

            }
            if (!enemyFleetFound && !enemyDefenseFound) {

                for (Planet p : planets) {
                    PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
                    //@comment if active he cant scan enemy plantes
                    /*if (pp != null && DiplomacyUtilities.getDiplomacyResult(pfe.getUserId(), pp.getUserId()).isAttacks()) {
                    continue;
                    }*/
                    duration += 2;
                }

                a.setNumber(duration);
                aDAO.add(a);
            }
        }

        return br;
    }

    public static ArrayList<BaseResult> showColonizationConstraints(
            int userId, int fleetId) {
        PlayerFleet pf = pfDAO.findById(fleetId);

        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        User u = uDAO.findById(userId);
        if (!u.getTrial()) {
            return new ArrayList<BaseResult>();
        }

        ArrayList<PlayerPlanet> ppList = ppDAO.findByUserId(userId);
        
        if (ppList.size() >= Colonizing.MAX_PLANET_COUNT_TRIAL) {
            result.add(new BaseResult("Sie haben bereits 10 Planeten. Wenn sie diesen Planeten besiedeln verlieren sie den Trial Status", true));
        } else {
            HashSet<Integer> systems = new HashSet<Integer>();
            for (PlayerPlanet pp : ppList) {
                Planet p = pDAO.findById(pp.getPlanetId());
                systems.add(p.getSystemId());                
            }
            
            systems.add(pDAO.findById(pf.getPlanetId()).getSystemId());
            if (systems.size() > Colonizing.MAX_SYSTEM_COUNT_TRIAL) {
                result.add(new BaseResult("Sie haben bereits 3 Systeme. Wenn sie diesen Planeten besiedeln verlieren sie den Trial Status", true));
            }
        }

        for (Planet p : pDAO.findBySystemId(pDAO.findById(pf.getPlanetId()).getSystemId())) {
            PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
            if (pp != null) {
                if (pp.getUserId() != userId) {
                    result.add(new BaseResult("Es befindet sich bereits ein anderer Spieler in diesem System. <BR> Wenn sie diesen Planeten besiedeln verlieren sie den Trial Status", true));
                    break;
                }
            }
        }
        return result;
    }

    // ===========================================================================
    // This function is only used for checking if details of a fleet can be viewed
    // ===========================================================================
    public static void checkValidUserView(String fleetId, boolean isFormation, int userId, int systemId, boolean scanner) throws Exception {
        if ((fleetId == null) || (fleetId.equalsIgnoreCase("0"))) {
            return;
        }
        int fleet = Integer.parseInt(fleetId);
        int fleetUserId = 0;
        int fleetPlanetId = 0;
        int fleetSystemId = 0;

        if (isFormation) {
            FleetFormationExt ffe = new FleetFormationExt(fleet);
            // FleetFormation ff = new FleetFormation();
            // ff.setId(fleet);
            // ff = (FleetFormation) ffDAO.get(ff);
            if (ffe != null) {
                fleetUserId = ffe.getUserId();
                if (ffe.getRelativeCoordinate() != null) {
                    fleetSystemId = ffe.getRelativeCoordinate().getSystemId();
                    fleetPlanetId = ffe.getRelativeCoordinate().getPlanetId();
                }
            }

        } else {
            PlayerFleet pf = new PlayerFleet();
            pf.setId(fleet);
            pf = (PlayerFleet) pfDAO.get(pf);
            if (pf != null) {
                fleetUserId = pf.getUserId();
                fleetSystemId = pf.getSystemId();
                fleetPlanetId = pf.getPlanetId();
            }

        }

        if (fleetUserId != 0) {
            boolean alliedView = DiplomacyUtilities.getDiplomacyRel(fleetUserId, userId).getDiplomacyTypeId() <= 1;

            if (alliedView || (fleetUserId == userId)) {
                return;
            }
            if ((fleetSystemId == systemId) && scanner) {
                return;
            }
            PlayerPlanet pp = ppDAO.findByPlanetId(fleetPlanetId);
            if (pp != null && pp.getUserId() == userId) {
                return;
            }

            if (pfDAO.findBy(fleetPlanetId, fleetSystemId, userId).size() > 0) {
                return;
            }

        }

        DebugBuffer.error("[1] Spieler " + userId + " hat keine Berechtigung auf Flotte " + fleetId + " zuzugreifen!");

        try {
            throw new Exception("[1] Spieler " + userId + " hat keine Berechtigung auf Flotte " + fleetId + " zuzugreifen! (SYSTEM: " + fleetSystemId + "<>" + systemId + " SCANNER: " + scanner + ")");
        } catch (Exception e) {
            throw e;
        }

    }

    public static ArrayList<AttackPossibleResult> getPossibleToAttack(int userId, int systemId, int planetId) {
        ArrayList<AttackPossibleResult> result = new ArrayList<AttackPossibleResult>();
        ArrayList<Integer> processed = new ArrayList<Integer>();
        for (PlayerFleet pf : pfDAO.findBy(planetId, systemId)) {
            if (pf.getUserId() == userId) {
                continue;
            }
            if (!processed.contains(pf.getUserId())) {
                String name = uDAO.findById(pf.getUserId()).getGameName();
                AllianceMember am = amDAO.findByUserId(pf.getUserId());
                if (am != null) {
                    name += "[" + allyDAO.findById(am.getAllianceId()).getTag() + "]";
                }
                result.add(new AttackPossibleResult(pf.getUserId(), DiplomacyUtilities.getDiplomacyResult(userId, pf.getUserId()).getColor(), name));
                processed.add(pf.getUserId());
            }
        }
        return result;
    }

    public static void checkValidUser(String fleetId, boolean isFormation, int userId, JspWriter out) throws Exception {
        // Logger.getLogger().write("FleetId " + fleetId + " userId " + userId);

        if ((fleetId == null) || (fleetId.equalsIgnoreCase("0"))) {
            return;
        }

        int fleet = Integer.parseInt(fleetId);
        int fleetUserId = 0;

        if (isFormation) {
            FleetFormation ff = new FleetFormation();
            ff.setId(fleet);
            ff = (FleetFormation) ffDAO.get(ff);
            if (ff != null) {
                fleetUserId = ff.getUserId();
            }

        } else {
            PlayerFleet pf = new PlayerFleet();
            pf.setId(fleet);
            pf = (PlayerFleet) pfDAO.get(pf);
            if (pf != null) {
                fleetUserId = pf.getUserId();
            }

        }

        if (fleetUserId != 0) {
            if (fleetUserId != userId) {
                //B�se manipulation gefunden !!!
                out.write("Diese Flotte geh�rt dir nicht, also kannst du sie auch nicht manipulieren !!!");
                //        	out.close();
                DebugBuffer.error("[2] Spieler " + userId + " hat keine Berechtigung auf Flotte " + fleetId + " zuzugreifen!");
                try {
                    throw new Exception("[2] Spieler " + userId + " hat keine Berechtigung auf Flotte " + fleetId + " zuzugreifen!");
                } catch (Exception e) {
                    throw e;
                }

            }
        }
    }

    public static ArrayList<ShipDesign> getPossibleRefitShips(ShipDesign sde, int userId) {
        ArrayList<ShipDesign> sdResult = new ArrayList<ShipDesign>();
        if (sde.getUserId() == userId) {
            ArrayList<ShipDesign> sdList = sdDAO.findByUserIdAndChassis(userId, sde.getChassis());
            for (ShipDesign sd : sdList) {
                if (!sd.getBuildable()) {
                    continue;
                }
                if (sd.getId() != sde.getId()) {
                    sdResult.add(sd);
                }

            }
        }
        return sdResult;
    }

    public static ShipModifyView getModifiableShips(
            int fleetId, int userId) {
        return at.viswars.utilities.ShipUtilities.getModifiableShips(fleetId);
    }

    public static ArrayList<BaseResult> canBeScrapped(int userId, int fleetId, int shipDesignId, int destroyCount) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        PlayerFleet pf = pfDAO.findById(fleetId);
        if (pf.getUserId() != userId) {
            result.add(new BaseResult("Dise Flotte geh�rt ihnen nicht", true));
            return result;
        }

        if (ppDAO.findByPlanetId(pf.getPlanetId()).getUserId() != userId && !DiplomacyUtilities.getDiplomacyResult(pf.getUserId(), userId).isAbleToUseShipyard()) {
            result.add(new BaseResult("Diser Planet geh�rt nicht ihnen", true));
            return result;
        }
        boolean shipyard = false;
        boolean orbitalShipyard = false;
        if (pcDAO.isConstructed(pf.getPlanetId(), Construction.ID_PLANETARYSHIPYARD)) {
            shipyard = true;
        }

        if (pcDAO.isConstructed(pf.getPlanetId(), Construction.ID_ORBITAL_SHIPYARD)) {
            orbitalShipyard = true;
        }

        if (!shipyard || !orbitalShipyard) {
            if (!shipyard) {
                result.add(new BaseResult("Dieser Planet verf&uuml;gt nicht &uuml;ber eine planetare Schiffswerft", true));
            }

            if (!orbitalShipyard) {
                result.add(new BaseResult("Dieser Planet verf&uuml;gt nicht &uuml;ber eine orbitale Schiffswerft", true));
            }

            return result;

        }

        for (ShipFleet sf : sfDAO.findByFleetId(fleetId)) {
            if (sf.getDesignId() == shipDesignId) {
                if (sf.getCount() > destroyCount) {
                    sf.setCount(sf.getCount() - destroyCount);
                    destroyCount = 0;
                    break;
                } else if (sf.getCount() == destroyCount) {
                    destroyCount = 0;
                    break;
                } else if (sf.getCount() < destroyCount) {
                    destroyCount -= sf.getCount();
                }

            }
        }

        if (destroyCount > 0) {
            result.add(new BaseResult("So viele Schiffe sind nicht vorhanden", true));
            return result;
        }

        ArrayList<FleetLoading> flList = flDAO.findByFleetId(fleetId);
        if (!flList.isEmpty()) {
            result.add(new BaseResult("Flotte ist noch beladen - Bitte Schiffe auslagern oder Ladung entfernen", true));
            return result;
        }

        return result;
    }

    public static ArrayList<BaseResult> scrapShips(Map<String, String[]> allPars, int userId) {
        ArrayList<BaseResult> brs = new ArrayList<BaseResult>();
        if (allPars.get("destroyCount") == null || allPars.get("shipDesignId") == null || allPars.get("fleetId") == null) {
            brs.add(new BaseResult("Parameter ung�ltig", true));
            return brs;
        }

        int destroyCount = -1;
        int shipDesignId = -1;
        int fleetId = -1;
        try {
            destroyCount = Integer.parseInt(allPars.get("destroyCount")[0]);
            shipDesignId = Integer.parseInt(allPars.get("shipDesignId")[0]);
            fleetId = Integer.parseInt(allPars.get("fleetId")[0]);
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.DEBUG, "Der Spieler " + userId + " versucht ung&uuml;ltige Parameter beim l&ouml;schen von Schiffen zu verwenden WUAHAHAHAHA. YOUR ASS IS MINE");
        }

        if (destroyCount > -1 && shipDesignId > -1 && fleetId > -1) {
            ArrayList<BaseResult> tmp = canBeScrapped(userId, fleetId, shipDesignId, destroyCount);
            if (tmp.size() > 0) {
                return tmp;
            }

            ShipDesignExt sdEntry = new ShipDesignExt(shipDesignId);
            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "fleetId : " + fleetId);
            ShipScrapOrder sso = new ShipScrapOrder(sdEntry, pfDAO.findById(fleetId).getPlanetId(), userId, destroyCount, fleetId);

            OrderResult oRes = OrderHandling.scrapOrder(sso);
            if (!oRes.isSuccess()) {
                for (String s : oRes.getErrors()) {
                    brs.add(new BaseResult(s, true));




                }

            }
            /* Action a = new Action();
            a.setType(EActionType.SHIP);
            a.setNumber(destroyCount);
            a.setShipDesignId(shipDesignId);
            
            
            ProductionOrder po = new ProductionOrder();
            po.setType(EProductionOrderType.SCRAP);
            po.setToId(shipDesignId);
            po.setPriority(1);
            po.setPlanetId(pf.getPlanetId());
            poDAO.add(po);
            
            a.setRefTableId(po.getId());*/
        }

        return brs;




    }

    public static synchronized void addRepairShipsToBuffer(Map<String, String[]> allPars, RepairShipBuffer rsb) {
        // Retrieve data for damage level 
        String[] minor = allPars.get("count_" + EDamageLevel.MINORDAMAGE.toString());
        String[] light = allPars.get("count_" + EDamageLevel.LIGHTDAMAGE.toString());
        String[] medium = allPars.get("count_" + EDamageLevel.MEDIUMDAMAGE.toString());
        String[] heavy = allPars.get("count_" + EDamageLevel.HEAVYDAMAGE.toString());

        int minorInt = 0;
        int lightInt = 0;
        int mediumInt = 0;
        int heavyInt = 0;

        try {
            HashMap<EDamageLevel, Integer> toRepair = new HashMap<EDamageLevel, Integer>();

            if (minor != null) {
                minorInt = Integer.parseInt(minor[0]);
                toRepair.put(EDamageLevel.MINORDAMAGE, minorInt);
            }

            if (light != null) {
                lightInt = Integer.parseInt(light[0]);
                toRepair.put(EDamageLevel.LIGHTDAMAGE, lightInt);
            }

            if (medium != null) {
                mediumInt = Integer.parseInt(medium[0]);
                toRepair.put(EDamageLevel.MEDIUMDAMAGE, mediumInt);
            }

            if (heavy != null) {
                heavyInt = Integer.parseInt(heavy[0]);
                toRepair.put(EDamageLevel.HEAVYDAMAGE, heavyInt);
            }

            rsb.setParameter(RepairShipBuffer.COUNT, toRepair);

        } catch (NumberFormatException nfe) {
        }

        // Check if values are valid
    }

    private static void transferDamagedShips(int shipFleetId, int shipFleetIdTo, int count, boolean damagedFirst) {
        ShipData sd = new ShipData(shipFleetId);

        if (DEBUG) {
            DebugBuffer.addLine(DebugLevel.TRACE, "Processing ShipFleet Entry with id " + sd.getId() + " COUNT: " + sd.getCount());
        }
        ArrayList<ShipStatusEntry> sseList = sd.getShipDataStatus();

        int countLeft = count;
        if (damagedFirst) {
            for (int i = (sseList.size() - 1); i
                    >= 0; i--) {
                ShipStatusEntry sse = sseList.get(i);
                if (sse.getCount() == 0) {
                    continue;
                }
                if (sse.getStatus() == EDamageLevel.NODAMAGE) {
                    // Nothing to do
                    if (DEBUG) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "DamagedFirst already at non damaged -> Nothing to do");
                    }
                    return;
                } else {
                    if (sse.getCount() <= countLeft) {
                        // Convert this damageLevel completly to Hangar
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damage level " + sse.getStatus() + " completly");
                        }
                        DamagedShips ds = dsDAO.getByIdAndDamage(shipFleetId, sse.getStatus());
                        dsDAO.remove(ds);

                        DamagedShips dsNew = dsDAO.getByIdAndDamage(shipFleetIdTo, ds.getDamageLevel());

                        if (dsNew == null) {
                            dsNew = new DamagedShips();
                            dsNew.setShipFleetId(shipFleetIdTo);
                            dsNew.setRefTable(EDamagedShipRefType.SHIPFLEET);
                            dsNew.setCount(ds.getCount());
                            dsNew.setDamageLevel(ds.getDamageLevel());
                            dsDAO.add(dsNew);
                        } else {
                            dsNew.setCount(dsNew.getCount() + ds.getCount());
                            dsDAO.update(dsNew);
                        }

                        countLeft -= sse.getCount();
                    } else {
                        // Split up damageLevel
                        int sfLeft = sse.getCount() - countLeft;

                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damage level " + sse.getStatus() + " partially (LEFT: " + sfLeft + ")");
                        }

                        DamagedShips ds = dsDAO.getByIdAndDamage(shipFleetId, sse.getStatus());
                        ds.setCount(sfLeft);
                        dsDAO.update(ds);

                        DamagedShips dsNew = dsDAO.getByIdAndDamage(shipFleetIdTo, ds.getDamageLevel());

                        if (dsNew == null) {
                            dsNew = new DamagedShips();
                            dsNew.setShipFleetId(shipFleetIdTo);
                            dsNew.setRefTable(EDamagedShipRefType.SHIPFLEET);
                            dsNew.setCount(countLeft);
                            dsNew.setDamageLevel(sse.getStatus());
                            dsDAO.add(dsNew);
                        } else {
                            dsNew.setCount(dsNew.getCount() + countLeft);
                            dsDAO.update(dsNew);
                        }

                        countLeft = 0;
                    }
                }

                if (countLeft == 0) {
                    return;
                }
            }
        } else {
            for (int i = 0; i
                    < sseList.size(); i++) {
                ShipStatusEntry sse = sseList.get(i);

                if (sse.getCount() == 0) {
                    continue;
                }
                if (sse.getStatus() == EDamageLevel.NODAMAGE) {
                    if (DEBUG) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "UndamagedFirst -> Reduce count left and continue (CountLeft=" + countLeft + " to transfer " + sse.getCount() + ")");
                    }
                    countLeft -= Math.min(count, sse.getCount());
                } else {
                    if (sse.getCount() <= countLeft) {
                        // Convert this damageLevel completly to Hangar
                        if (DEBUG) {
                        }
                        DamagedShips ds = dsDAO.getByIdAndDamage(shipFleetId, sse.getStatus());
                        dsDAO.remove(ds);

                        DamagedShips dsNew = dsDAO.getByIdAndDamage(shipFleetIdTo, ds.getDamageLevel());
                        if (dsNew == null) {
                            dsNew = new DamagedShips();
                            dsNew.setShipFleetId(shipFleetIdTo);
                            dsNew.setRefTable(EDamagedShipRefType.SHIPFLEET);
                            dsNew.setCount(ds.getCount());
                            dsNew.setDamageLevel(ds.getDamageLevel());
                            dsDAO.add(dsNew);
                        } else {
                            dsNew.setCount(dsNew.getCount() + ds.getCount());
                            dsDAO.update(dsNew);
                        }

                        countLeft -= sse.getCount();

                    } else {
                        // Split up damageLevel
                        int sfLeft = sse.getCount() - countLeft;

                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damage level " + sse.getStatus() + " partially (LEFT: " + sfLeft + ")");
                        }
                        DamagedShips ds = dsDAO.getByIdAndDamage(shipFleetId, sse.getStatus());
                        ds.setCount(sfLeft);
                        dsDAO.update(ds);

                        DamagedShips dsNew = dsDAO.getByIdAndDamage(shipFleetIdTo, ds.getDamageLevel());

                        if (dsNew == null) {
                            dsNew = new DamagedShips();
                            dsNew.setShipFleetId(shipFleetIdTo);
                            dsNew.setRefTable(EDamagedShipRefType.SHIPFLEET);
                            dsNew.setCount(countLeft);
                            dsNew.setDamageLevel(sse.getStatus());
                            dsDAO.add(dsNew);
                        } else {
                            dsNew.setCount(dsNew.getCount() + countLeft);
                            dsDAO.update(dsNew);
                        }

                        countLeft = 0;
                    }
                }

                if (countLeft == 0) {
                    return;
                }
            }
        }
    }

    public static ArrayList<BaseResult> attack(int userId, int fleetId, Map<String, String[]> params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        String[] users = params.get("users");
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);
        for (String s : users) {
            int toUserId = Integer.parseInt(s);
            DiplomacyService.createUserRelation(userId, toUserId, DiplomacyType.WAR, false);
        }
        if (pfe.getRelativeCoordinate().getSystemId() == 0) {
            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Invoking Combat in FleetService - attack with System 0 : userId - " + userId + " | fleetId - " + fleetId);

        } else {
            CombatReportGenerator crg = new CombatReportGenerator();
            if (pfe.getRelativeCoordinate().getPlanetId() == 0) {
                CombatHandler_NEW ch = new CombatHandler_NEW(CombatType.SYSTEM_COMBAT, pfDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
                ch.executeBattle();
                ch.processCombatResults();
            } else {
                CombatHandler_NEW ch = new CombatHandler_NEW(CombatType.ORBITAL_COMBAT, pfDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
                ch.executeBattle();
                ch.processCombatResults();
            }
        crg.writeReportsToDatabase();
        }

        return result;
    }

    public static boolean canUseShipyard(int userId, int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        if (pp == null) {
            return false;
        }

        int count = 0;
        count += FleetService.getCountForBuilding(planetId, Construction.ID_ORBITAL_SHIPYARD);
        count += FleetService.getCountForBuilding(planetId, Construction.ID_PLANETARYSHIPYARD);

        if (count == 0) {
            return false;
        }

        if (pp.getUserId() == userId) {
            return true;
        }
        if (DiplomacyUtilities.getDiplomacyResult(pp.getUserId(), userId).isAbleToUseShipyard()) {
            return true;
        }

        return false;
    }

    public static boolean invasionPossible(int userId, PlayerFleetExt pfExt) {
        int planetId = pfExt.getBase().getPlanetId();
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);

        if (pp == null) {
            return false;
        }

        if (pp.getUserId() == userId) {
            return false;
        }

        if (pfExt.isMoving()) {
            return false;
        }

        LoadingInformation li = pfExt.getLoadingInformation();

        if ((li.getTroopSpace() == 0) || (li.getCurrLoadedTroops() == 0)) {
            return false;
        }

        if (MilitaryUtilities.getAttackerStrength(planetId, userId) < (MilitaryUtilities.getPlanetaryShieldStrength(planetId) / 2d)) {
            return false;
        }

        ArrayList<Integer> users = new ArrayList<Integer>();
        users.add(pp.getUserId());
        users.add(userId);

        CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(users, new RelativeCoordinate(0, planetId));
        if (!cgr.hasConflictingParties()) {
            return false;
        }

        return true;
    }
    
    public static void abortScan(int userId, int fleetId) {
        try {
            PlayerFleet pf = pfDAO.findById(fleetId);
            if ((pf == null) || (pf.getUserId() != userId)) return;
            
            ArrayList<Action> result = new ArrayList<Action>();

            Action a = new Action();
            a.setType(EActionType.SCAN_PLANET);
            a.setUserId(userId);
            a.setFleetId(fleetId);
            result.addAll(aDAO.find(a));
            a.setType(EActionType.SCAN_SYSTEM);
            result.addAll(aDAO.find(a));

            if (result.size() != 1) {
                DebugBuffer.error("No unique scanning entry found for aborting of scanning action for fleet " + fleetId);
            } else {
                aDAO.remove(result.get(0));
            }
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.WARNING, "Konnte Scanaction nicht abbrechen f�r Flotte : " + fleetId);
        }        
    }
}
