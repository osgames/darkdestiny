/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.service;

import at.viswars.model.Alliance;
import at.viswars.model.AllianceMember;
import at.viswars.model.Chassis;
import at.viswars.model.Construction;
import at.viswars.model.GroundTroop;
import at.viswars.model.Ressource;
import at.viswars.model.Statistic;
import at.viswars.model.StatisticEntry;
import at.viswars.model.User;
import at.viswars.model.UserData;
import at.viswars.result.PlayerStatisticResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class StatisticService extends Service{

    public static Alliance findAllianceByUserId(int userId){
        
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        if(am == null){
            return null;
        }else{
            return allianceDAO.findById(am.getAllianceId());
        }
    }

    public static ArrayList<Ressource> findAllRessources(){
        return ressourceDAO.findAllStoreableRessources();
    }
    public static ArrayList<GroundTroop> findAllGroundTroops(){
        return groundTroopDAO.findAll();
    }

    public static ArrayList<Chassis> findAllChassis(){
        return chassisDAO.findAll();
    }

    public static Construction findConstructionById(int constructionId){
        return constructionDAO.findById(constructionId);
    }

    public static User findUserByUserId(int userId){
        return userDAO.findById(userId);
    }
    public static UserData findUserDataByUserId(int userId){
        return userDataDAO.findByUserId(userId);
    }

    public static PlayerStatisticResult getPlayerStatisticResult(){
        PlayerStatisticResult psr = new PlayerStatisticResult();
        psr.setPlayerStats(findAllStatistics());
        HashMap<Integer, String> userNames = new HashMap<Integer, String>();
        for(User u : (ArrayList<User>)userDAO.findAllPlayers()){
            userNames.put(u.getUserId(), u.getGameName());
        }
        psr.setUserNames(userNames);
        return psr;
    }

    public static TreeMap<Integer, ArrayList<Statistic>> findAllStatistics(){
        return statisticDAO.findAllOrdered();
    }

     public static String getAllianceTag(int userId){
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        if(am == null){
            return "";
        }else{
            return allianceDAO.findById(am.getAllianceId()).getTag();
        }
    }

     public static ArrayList<StatisticEntry> getStaticEntriesByCategoryId(int categoryId){
         return statisticEntryDAO.findByCategoryId(categoryId);
     }
}
