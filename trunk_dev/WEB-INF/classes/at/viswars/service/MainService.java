/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.service;

import at.viswars.ML;
import at.viswars.model.User;
import at.viswars.model.Language;
import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.UserData;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class MainService extends Service{


    public static void changeLanguage(int userId, String language) {
        User u = userDAO.findById(userId);
        u.setLocale(language);
        ML.removeLocale(userId);
        userDAO.update(u);
    }

    public static ArrayList<User> getAllPlayers() {
        return userDAO.findAllSorted(false);
    }
    
    public static ArrayList<User> getAllUsers() {
        return userDAO.findAllSorted(true);
    }
    
    public static UserData findUserDataByUserId(int userId) {
        return userDataDAO.findByUserId(userId);
    }

    public static User findUserByUserId(int userId) {
        return userDAO.findById(userId);
    }
    
    public static ArrayList<Language> findAllLanguages() {
        return languageDAO.findAll();
    }
    
    public static Planet findPlanetById(int planetId) {
        return planetDAO.findById(planetId);
    }

    public static PlayerPlanet findPlayerPlanetByPlanetId(int planetId){
        return playerPlanetDAO.findByPlanetId(planetId);
    }
}
