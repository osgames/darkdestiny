/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ressources;

import at.viswars.enumeration.EConstructionProdType;
import at.viswars.model.PlanetConstruction;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class RessProdNode {
    public final int ressource;    
    public final int type;
    
    private RessProdNode previous;
    private RessProdNode next;
    
    private ArrayList<PlanetConstruction> pc;
    private long maxStorage;
    private long minStorage;
    private long actStorage;
    
    private long baseProduction;
    private long baseConsumption;
    private long maxProductionByPrevious;
    
    private long actProduction;
    private long actConsumption;
    
    public final static int NODE_TYPE_INDUSTRY = 1; 
    public final static int NODE_TYPE_AGRICULTURE = 2; 
    public final static int NODE_TYPE_RESEARCH = 4;     
    public final static int NODE_TYPE_ENERGY = 3; 
    public final static int NODE_TYPE_CONSTRUCTION = 5;
    
    public RessProdNode(int r, int type) {
        ressource = r;
        this.type = type;
    } 
    
    public RessProdNode getPrevious() {
        return previous;
    }

    public void setPrevious(RessProdNode previous) {
        this.previous = previous;
        previous.setNext(this);        
    }

    public RessProdNode getNext() {
        return next;
    }

    public void setNext(RessProdNode next) {
        this.next = next;
    }

    public long getMaxStorage() {
        return maxStorage;
    }

    public void setMaxStorage(long maxStorage) {
        this.maxStorage = maxStorage;
    }

    public long getMinStorage() {
        return minStorage;
    }

    public void setMinStorage(long minStorage) {
        this.minStorage = minStorage;
    }

    public long getActStorage() {
        return actStorage;
    }

    public void setActStorage(long actStorage) {
        this.actStorage = actStorage;
    }

    public long getBaseProduction() {
        return baseProduction;
    }

    public void setBaseProduction(long baseProduction) {
        this.baseProduction = baseProduction;
    }

    public long getBaseConsumption() {
        return baseConsumption;
    }

    public void setBaseConsumption(long baseConsumption) {
        this.baseConsumption = baseConsumption;
    }

    public long getActProduction() {
        return actProduction;
    }

    public void setActProduction(long actProduction) {
        this.actProduction = actProduction;
    }

    public long getActConsumption() {
        return actConsumption;
    }

    public void setActConsumption(long actConsumption) {  
        this.actConsumption = actConsumption;
    }

    public long getMaxProductionByPrevious() {
        return maxProductionByPrevious;
    }

    public void setMaxProductionByPrevious(long maxProductionByPrevious) {
        this.maxProductionByPrevious = maxProductionByPrevious;
    }
}