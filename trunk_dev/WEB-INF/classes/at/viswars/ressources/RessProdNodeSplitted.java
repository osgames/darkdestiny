/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ressources;

import at.viswars.Logger.Logger;
import at.viswars.enumeration.EConstructionProdType;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class RessProdNodeSplitted extends RessProdNode {
    private HashMap<EConstructionProdType,Long> consumptionSource = new HashMap<EConstructionProdType,Long>();

    public RessProdNodeSplitted(int r, int type) {
        super(r,type);
    } 
    
    public long getBaseConsumption(EConstructionProdType prodType) {
        if (consumptionSource.containsKey(prodType)) {
            return consumptionSource.get(prodType);
        } else {
            return 0;
        }
    }

    public void addBaseConsumption(long baseConsumption, EConstructionProdType prodType) {
        if (consumptionSource.containsKey(prodType)) {
            consumptionSource.put(prodType, consumptionSource.get(prodType) + baseConsumption);
        } else {
            consumptionSource.put(prodType, baseConsumption);
        }
    }
    
    public void printNode() {
        Logger.getLogger().write("DEBUG NODE");
        for (Map.Entry<EConstructionProdType,Long> mapEntry : consumptionSource.entrySet()) {
            Logger.getLogger().write("Consumption for type " + mapEntry.getKey() + " is " + mapEntry.getValue());
        }
    }
}
