/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.viewbuffer;

import at.viswars.result.FleetListResult.ESortType;
import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class FleetSortBuffer {

    private static HashMap<Integer, ESortType> sorts;


    public static ESortType getByUserId(int userId){
        if(sorts == null){
            sorts = new HashMap<Integer, ESortType>();
        }
        ESortType result = sorts.get(userId);
        if(result == null){
            set(ESortType.ID, userId);
            return(ESortType.ID);
        }else{
            return result;
        }
    }

    public static void set(ESortType sort, int userId){
        sorts.put(userId, sort);
    }

}
