/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.viewbuffer;

import at.viswars.service.Service;
import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class HomeSystemBuffer extends Service{

    public static HashMap<Integer,  at.viswars.model.System> homeSystem;

    public static void init(){
        homeSystem = new HashMap<Integer, at.viswars.model.System>();

        for(PlayerPlanet pp :  (ArrayList<PlayerPlanet>)playerPlanetDAO.findAll()){
            if(pp.getHomeSystem()){
                Planet p = planetDAO.findById(pp.getPlanetId());
                homeSystem.put(pp.getUserId(), systemDAO.findById(p.getSystemId()));
            }
        }
    }

    public static at.viswars.model.System getForUser(int userId){
        
        if(homeSystem == null){
            init();
        }
        return homeSystem.get(userId);
    }

    public static void setForUser(int userId, at.viswars.model.System system){
            homeSystem.put(userId, system);
    }
}
