 package at.viswars.viewbuffer;

import java.util.ArrayList;
import java.util.HashMap;

import at.viswars.service.Service;
import at.viswars.model.MenuImage;
import at.viswars.model.MenuLink;
import java.util.TreeMap;
/**
 *
 * MenuBuffer zum Aufbau des Linken Men?s
 * 
 * @author Dreloc
 */
public class MenuBuffer extends Service{


    /**
     * @return the menuLinks
     */
    public static ArrayList<MenuLink> getMenuLinks() {
        ArrayList<MenuLink> result =  menuLinkDAO.findAll();
        TreeMap<Integer, MenuLink> tmp = new TreeMap<Integer, MenuLink>();
        for(MenuLink ml : result){
            tmp.put(ml.getLinkOrder(), ml);
        }
        result =  new ArrayList<MenuLink>();
        result.addAll(tmp.values());
        return result;
    }


 
    public static HashMap<Integer, MenuImage> getMenuImages() {

        ArrayList<MenuImage> menuImages = menuImageDAO.findAll();
        HashMap<Integer, MenuImage> result = new HashMap<Integer, MenuImage>();
        for(MenuImage mi : menuImages){
            result.put(mi.getId(), mi);
        }
        return result;
    }

}
