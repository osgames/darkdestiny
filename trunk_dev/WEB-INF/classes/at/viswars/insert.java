package at.viswars;
import java.sql.*;
import java.util.*;

import at.viswars.database.access.DbConnect;

/**
 *
 * @author The White Rabbit
 */
public class insert {
    
    /** Creates a new instance of test */
    public insert() {
    }
    public static void main(String[] args) {
        // TODO code application logic here
        Map<Integer, String> planetupd = new HashMap<Integer, String>();
        try{                        
            Class.forName("org.gjt.mm.mysql.Driver");            
            Statement pstmt = DbConnect.createStatement();
            ResultSet rs = pstmt.executeQuery("SELECT id, diameter, landType, file FROM planet");
                                    
            while (rs.next() ){
                
                String landType;
                String file;
                int diameter;
                int id;
                int fileint;
                id = rs.getInt(1);
                diameter = rs.getInt(2);
                landType = rs.getString(3);
                
                // Zuweisen des Namens von Gasriesen 150000 300000
                if (landType.equalsIgnoreCase("A")){
                    if (diameter >= 150000 && diameter <= 200000){                                                
                        fileint = 1 + (int)(Math.random()*10);}                    
                    else if (diameter > 200000 && diameter <= 250000){                        
                        fileint = 11 + (int)(Math.random()*10);}                    
                    else {fileint = 21 + (int)(Math.random()*10);}
                    
                    String filetmp = Integer.toString(fileint);
                    file = filetmp;
                                        
                    // Zuweisen des Namens von blauen Gasriesen 50000 150000
                }else if (landType.equalsIgnoreCase("B")){
                    if (diameter >= 50000 && diameter <= 83000){                                                
                        fileint = 31 + (int)(Math.random()*10);}                    
                    else if (diameter > 83000 && diameter <= 116000){                        
                        fileint = 41 + (int)(Math.random()*10);}                    
                    else {fileint = 51 + (int)(Math.random()*10);}
                                        
                    String filetmp = Integer.toString(fileint);
                    file = filetmp;                                                                                
                }
                // Zuweisen des Namens von kalten Erd&auml;hnlichen 5000 20000
                else if (landType.equalsIgnoreCase("C")){
                    if (diameter >= 5000 && diameter <= 10000){                        
                        fileint = 61 + (int)(Math.random()*10);}                    
                    else if (diameter > 10000 && diameter <= 15000){                        
                        fileint = 71 + (int)(Math.random()*10);}                    
                    else {fileint = 81 + (int)(Math.random()*10);}
                                        
                    String filetmp = Integer.toString(fileint);
                    file = filetmp;
                    
                } //Zuweisen von W&uuml;stenplaneten
                else if (landType.equalsIgnoreCase("G")){
                    if (diameter >= 5000 && diameter <= 10000){                                                
                        fileint = 91 + (int)(Math.random()*10);}                    
                    else if (diameter > 10000 && diameter <= 15000){                        
                        fileint = 101 + (int)(Math.random()*10);}                    
                    else {fileint = 111 + (int)(Math.random()*10);}                    
                    
                    String filetmp = Integer.toString(fileint);
                    file = filetmp;                                        
                }
                //Zuweisen von vulkanaktiven Planeten
                else if (landType.equalsIgnoreCase("J")){
                    if (diameter >= 5000 && diameter <= 10000){                                                
                        fileint = 121 + (int)(Math.random()*10);}                    
                    else if (diameter > 10000 && diameter <= 15000){                        
                        fileint = 131 + (int)(Math.random()*10);}                    
                    else {fileint = 141 + (int)(Math.random()*10);}
                                        
                    String filetmp = Integer.toString(fileint);
                    file = filetmp;                    
                }
                //Zuweisen von Eisplaneten
                else if (landType.equalsIgnoreCase("L")){
                    if (diameter >= 5000 && diameter <= 10000){                                                
                        fileint = 151 + (int)(Math.random()*10);}                    
                    else if (diameter > 10000 && diameter <= 15000){                        
                        fileint = 161 + (int)(Math.random()*10);}                    
                    else {fileint = 171 + (int)(Math.random()*10);}
                                        
                    String filetmp = Integer.toString(fileint);
                    file = filetmp;                                                            
                }
                //Zuweisen von erd&auml;hnlichen
                else if (landType.equalsIgnoreCase("M")){
                    if (diameter >= 5000 && diameter <= 10000){                                                
                        fileint = 181 + (int)(Math.random()*10);}                    
                    else if (diameter > 10000 && diameter <= 15000){                        
                        fileint = 191 + (int)(Math.random()*10);}                    
                    else {fileint = 201 + (int)(Math.random()*10);}                    
                    
                    String filetmp = Integer.toString(fileint);
                    file = filetmp;                                                                                                                        
                }
                //Zuweisen von mond&auml;hnlichen
                else if (landType.equalsIgnoreCase("E")){
                    if (diameter >= 5000 && diameter <= 10000){                                                
                        fileint = 211 + (int)(Math.random()*10);}                    
                    else if (diameter > 10000 && diameter <= 15000){                        
                        fileint = 221 + (int)(Math.random()*10);}                    
                    else {fileint = 231 + (int)(Math.random()*10);}
                                        
                    String filetmp = Integer.toString(fileint);
                    file = filetmp;                                                                                                                      
                }
                
                else file="";
                
                /*DebugBuffer.addLine("Neuer Planet");
                DebugBuffer.addLine("id" +id);
                DebugBuffer.addLine("Durchmesser" +diameter);
                DebugBuffer.addLine(landType);
                DebugBuffer.addLine(file);          
                String filenr=file+"k.jpg";
                DebugBuffer.addLine(filenr);
                */
                 
                 planetupd.put(Integer.valueOf(id), file);                                                
            }
                                                
            for (Map.Entry<Integer, String> entry : planetupd.entrySet()) {
             /* DebugBuffer.addLine(
                        (Integer)entry.getKey() + " -->" +
                        (String)entry.getValue()
                        );
              */
                int p = ((Integer)entry.getKey()).intValue();
                String q = (String)entry.getValue();

                Statement stmt2 = DbConnect.createStatement();
                stmt2.executeUpdate("Update planet set file= '"+ q + "' WHERE id =" + p);
            }
            //
            //  stmt2.executeUpdate("Update test set file="+(String)entry.getValue()+"where id="+(Integer)entry.getKey());                                                                                    
        }
        
        catch(Exception e) {
            DebugBuffer.writeStackTrace("Fehler: ", e);            
        }                                        
    }                        
}