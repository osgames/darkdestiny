/*
 * DefensePlanetEntry.java
 *
 * Created on 03. Dezember 2007, 23:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.military;

/**
 *
 * @author Stefan
 */
public class DefensePlanetEntry {
    private int id;
    private int systemId;
    private String name;
    private boolean hasPlanetDefense;
    private boolean hasOrbitalDefense;
    private boolean hasFleetDefense;
    private boolean hasFleetDefenseAllied;
    private boolean hasEnemyFleet;    
    private boolean hasShield;
    private boolean hasMilitary;
    private boolean hasSystemDefense;
    private boolean hasSystemFleet;
    private boolean hasSystemFleetAllied;
    private boolean hasEnemySystemFleet;    
    private boolean hasSystemShield;
    private boolean hasMineField;
    private boolean hasScannerArray;
    private boolean hasHyperspaceScanner;
    
    /** Creates a new instance of DefensePlanetEntry */
    public DefensePlanetEntry() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasPlanetDefense() {
        return hasPlanetDefense;
    }

    public void setHasPlanetDefense(boolean hasPlanetDefense) {
        this.hasPlanetDefense = hasPlanetDefense;
    }

    public boolean isHasOrbitalDefense() {
        return hasOrbitalDefense;
    }

    public void setHasOrbitalDefense(boolean hasOrbitalDefense) {
        this.hasOrbitalDefense = hasOrbitalDefense;
    }

    public boolean isHasFleetDefense() {
        return hasFleetDefense;
    }

    public void setHasFleetDefense(boolean hasFleetDefense) {
        this.hasFleetDefense = hasFleetDefense;
    }

    public boolean isHasShield() {
        return hasShield;
    }

    public void setHasShield(boolean hasShield) {
        this.hasShield = hasShield;
    }

    public boolean isHasMilitary() {
        return hasMilitary;
    }

    public void setHasMilitary(boolean hasMilitary) {
        this.hasMilitary = hasMilitary;
    }

    public boolean isHasSystemDefense() {
        return hasSystemDefense;
    }

    public void setHasSystemDefense(boolean hasSystemDefense) {
        this.hasSystemDefense = hasSystemDefense;
    }

    public boolean isHasSystemFleet() {
        return hasSystemFleet;
    }

    public void setHasSystemFleet(boolean hasSystemFleet) {
        this.hasSystemFleet = hasSystemFleet;
    }

    public boolean isHasSystemShield() {
        return hasSystemShield;
    }

    public void setHasSystemShield(boolean hasSystemShield) {
        this.hasSystemShield = hasSystemShield;
    }

    public boolean isHasMineField() {
        return hasMineField;
    }

    public void setHasMineField(boolean hasMineField) {
        this.hasMineField = hasMineField;
    }

    public int getSystemId() {
        return systemId;
    }

    public void setSystemId(int systemId) {
        this.systemId = systemId;
    }

    public boolean isHasFleetDefenseAllied() {
        return hasFleetDefenseAllied;
    }

    public void setHasFleetDefenseAllied(boolean hasFleetDefenseAllied) {
        this.hasFleetDefenseAllied = hasFleetDefenseAllied;
    }

    public boolean isHasEnemyFleet() {
        return hasEnemyFleet;
    }

    public void setHasEnemyFleet(boolean hasEnemyFleet) {
        this.hasEnemyFleet = hasEnemyFleet;
    }

    public boolean isHasSystemFleetAllied() {
        return hasSystemFleetAllied;
    }

    public void setHasSystemFleetAllied(boolean hasSystemFleetAllied) {
        this.hasSystemFleetAllied = hasSystemFleetAllied;
    }

    public boolean isHasEnemySystemFleet() {
        return hasEnemySystemFleet;
    }

    public void setHasEnemySystemFleet(boolean hasEnemySystemFleet) {
        this.hasEnemySystemFleet = hasEnemySystemFleet;
    }

    /**
     * @return the hasScannerArray
     */
    public boolean isHasScannerArray() {
        return hasScannerArray;
    }

    /**
     * @param hasScannerArray the hasScannerArray to set
     */
    public void setHasScannerArray(boolean hasScannerArray) {
        this.hasScannerArray = hasScannerArray;
    }

    /**
     * @return the hasHyperspaceScanner
     */
    public boolean isHasHyperspaceScanner() {
        return hasHyperspaceScanner;
    }

    /**
     * @param hasHyperspaceScanner the hasHyperspaceScanner to set
     */
    public void setHasHyperspaceScanner(boolean hasHyperspaceScanner) {
        this.hasHyperspaceScanner = hasHyperspaceScanner;
    }
    
}
