/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.access;

import java.sql.Connection;

/**
 *
 * @author Stefan
 */
public class TimedConnection {
    private long lastCheck = System.currentTimeMillis();
    private final Connection conn;
    private String name = "[unnamed]";

    public TimedConnection(Connection c) {
        this.conn = c;
    }

    /**
     * @return the lastCheck
     */
    public long getLastCheck() {
        return lastCheck;
    }

    /**
     * @param lastCheck the lastCheck to set
     */
    public void setLastCheck(long lastCheck) {
        this.lastCheck = lastCheck;
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
