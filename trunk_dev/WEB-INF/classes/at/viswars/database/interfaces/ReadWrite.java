/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.interfaces;

import at.viswars.database.framework.QueryKeySet;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface ReadWrite<T> extends ReadOnly<T> {
    public T insertOrUpdate(T entry);
    public ArrayList<T> insertOrUpdate(ArrayList<T> entries);
    public T add(T entry);
    public T update(T entry);
    public void remove(T entry);
    public ArrayList<T> updateAll(T entry, QueryKeySet qks);
}
