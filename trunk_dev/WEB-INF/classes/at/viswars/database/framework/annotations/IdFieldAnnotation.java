/**
 * 
 */
package at.viswars.database.framework.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This is a marker annotation indicating an Id-Field in the database. It is
 * used to find the entries that need to be set to remove an entry from the
 * database.
 * 
 * @author martin
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface IdFieldAnnotation {
	// This marker annotation indicates an Id field in the database.

}
