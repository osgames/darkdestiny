/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author Stefan
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface TableNameAnnotation {
    public String value();
}
