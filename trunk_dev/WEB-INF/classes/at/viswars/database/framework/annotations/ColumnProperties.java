/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author Stefan
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnProperties {
    public boolean nullable() default(false);
    public boolean unsigned() default(false);
    public int length() default(0);
    public boolean unique() default(false);
    public boolean autoIncrement() default(false);
}
