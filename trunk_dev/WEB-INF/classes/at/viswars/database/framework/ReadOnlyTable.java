/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.database.framework;

import at.viswars.DebugBuffer;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.database.interfaces.ReadOnly;
import at.viswars.model.Model;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ReadOnlyTable<T extends Model> implements ReadOnly<T> {
    protected at.viswars.database.framework.core.DatabaseTable datasource;
    private boolean debug = true;

    protected ReadOnlyTable() {
        initTable();
    }

    protected void reloadDataSource() {
        initTable();
    }

    private void initTable() {
        boolean readOnlyClass = false;
        if (getClass().getSuperclass().equals(ReadOnlyTable.class)) readOnlyClass = true;
        
        // DBCoreUtils.inspect(this);
        Class<T> dataClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        Logger.getLogger().write("Loading = " + dataClass.getName());

        String srcClass = DataSourceConfig.getInstance().getClassForModel(dataClass);
        try {
            Class rc = Class.forName(srcClass);
            Constructor<at.viswars.database.framework.core.DatabaseTable> constructor = rc.getConstructor(Class.class);
            constructor.setAccessible(true);
            datasource = constructor.newInstance(dataClass);
            // Logger.getLogger().write("Set datasource to " + datasource);
        } catch (Exception e) {
            Logger.getLogger().write(LogLevel.ERROR,"Loading of " + dataClass.getName() + " failed");
        }
    }

    @Override
    public T get(QueryKeySet qks) {
        try {
            T result = (T) datasource.getEntry(qks);

            return result;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in get: ", e);
            return null;
        }
    }

    public ArrayList<T> get(ArrayList<T> entries) {
        try {
            ArrayList<T> result = datasource.getEntries(entries);
            return result;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in get: ", e);
            return null;
        }
    }

    @Override
    public boolean contains(QueryKeySet qks) {
        try {
            long startTime = System.currentTimeMillis();
            T result = (T) datasource.getEntry(qks);
            //// Logger.getLogger().write("Contains time was " + (System.currentTimeMillis() - startTime) + "ms");

            return (result != null);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in contains: ", e);
            return false;
        }
    }

    public T get(T entry) {
        try {
            // long startTime = System.currentTimeMillis();
            T result = (T) datasource.getEntry(entry);
            // Logger.getLogger().write("GetQuery time was " + (System.currentTimeMillis() - startTime) + "ms");

            return result;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in get: ", e);
            return null;
        }
    }

    public ArrayList<T> find(T entry) {
        ArrayList<T> result = null;

        try {
            long startTime = System.currentTimeMillis();
            result = datasource.getEntries(entry);
            // Logger.getLogger().write("FindQuery time was " + (System.currentTimeMillis() - startTime) + "ms");
        } catch (Exception iie) {
            DebugBuffer.writeStackTrace("Error in find: ", iie);
            return null;
        }

        return result;
    }

    @Override
    public ArrayList<T> find(String indexName, QueryKeySet qks) {
        ArrayList<T> result = null;

        try {
            long startTime = System.currentTimeMillis();
            result = datasource.getEntries(qks, indexName);
            // // Logger.getLogger().write("FindQuery time was " + (System.currentTimeMillis() - startTime) + "ms");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in find: ", e);
            return null;
        }

        return result;
    }

    @Override
    public ArrayList<T> findAll() {
        ArrayList<T> result = null;

        try {
            long startTime = System.currentTimeMillis();
            result = datasource.getAllEntries();
            //  // Logger.getLogger().write("FindAllQuery time was " + (System.currentTimeMillis() - startTime) + "ms");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in findAll: ", e);
            return null;
        }

        return result;
    }
    
    public Exception getWriteLock() {
        return datasource.getWriteLock();
    }
}
