/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.database.framework.utilities;

import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DataScope.EScopeType;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.StringType;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.model.Model;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Stefan
 */
public class CreateFromModel {

    /**
     * Scans all classes accessible from the context class loader which belong
     * to the given package and subpackages.
     *
     * @param packageName The base package
     * @return The classes
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Recursive method used to find all classes in a given directory and
     * subdirs.
     *
     * @param directory The base directory
     * @param packageName The package name for classes found inside the base
     * directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

    public static String createDropSQLForClass(Class c) {

        TableNameAnnotation ta = (TableNameAnnotation) c.getAnnotation(TableNameAnnotation.class);
        if (ta == null) {
            return null;
        }
        String sql = "DROP TABLE `" + ta.value() + "`";
        return sql;
    }

    public static String createSQLForClass(Class c) {
        String sql = "";

        TableNameAnnotation ta = (TableNameAnnotation) c.getAnnotation(TableNameAnnotation.class);
        if (ta == null) {
            return null;
        }

        sql += "CREATE TABLE `" + ta.value() + "` (";

        Field[] fields = c.getDeclaredFields();
        ArrayList<String> primary = new ArrayList<String>();

        boolean firstField = true;
        for (Field f : fields) {
            FieldMappingAnnotation fma = (FieldMappingAnnotation) f.getAnnotation(FieldMappingAnnotation.class);

            if (fma != null) {
                ColumnProperties cpa = (ColumnProperties) f.getAnnotation(ColumnProperties.class);

                if (f.getAnnotation(IdFieldAnnotation.class) != null) {
                    primary.add(fma.value());
                }

                if (!firstField) {
                    sql += ", ";
                } else {
                    firstField = false;
                }
                sql += "`" + fma.value() + "`";

                if (f.getType() == String.class) {
                    StringType sta = (StringType) f.getAnnotation(StringType.class);
                    if (sta != null) {
                        if (sta.value().equalsIgnoreCase("varchar")) {
                            int length = 0;
                            if ((cpa == null) || (cpa.length() == 0)) {
                                length = 50;
                            } else {
                                length = cpa.length();
                            }
                            sql += " varchar(" + length + ")";
                        } else if (sta.value().equalsIgnoreCase("text")) {
                            int length = 0;
                            if ((cpa == null) || (cpa.length() == 0)) {
                                length = 50;
                            } else {
                                length = cpa.length();
                            }
                            sql += sta.value() + " (" + length + ")";
                        } else {
                            sql += " " + sta.value() + " CHARACTER SET utf8";
                        }
                    } else {
                        sql += getTypeDef(f);
                    }
                } else {
                    sql += getTypeDef(f);
                }

                if (cpa != null) {
                    Logger.getLogger().write("Found ColumnProperties Annotation");

                    if (cpa.unsigned()) {
                        sql += " unsigned";
                    }
                    if (!cpa.nullable()) {
                        sql += " NOT NULL";
                    }
                    if (cpa.autoIncrement()) {
                        sql += " AUTO_INCREMENT";
                    }
                }

                DefaultValue dv = (DefaultValue) f.getAnnotation(DefaultValue.class);
                if (dv != null) {
                    if (f.getType() == Boolean.class) {
                        sql += " DEFAULT '" + (Boolean.getBoolean(dv.value()) ? 1 : 0) + "'";
                    } else {
                        sql += " DEFAULT '" + dv.value() + "'";
                    }
                }
            }
        }

        // Add primary
        if (!primary.isEmpty()) {
            boolean firstPrimary = true;
            sql += ", PRIMARY KEY (";
            for (String prim : primary) {
                if (!firstPrimary) {
                    sql += ",";
                } else {
                    firstPrimary = false;
                }

                sql += "`" + prim + "`";
            }
            sql += ") ";
        }

        sql += ") ENGINE=InnoDB DEFAULT CHARSET=utf8";

        return sql;
    }

    public static String createInsertStatementsFromDBForDAOClass(Class c) {
        StringBuffer sb = new StringBuffer();

        System.out.println("Class name: " + c.getName());



        ReadOnlyTable rot = null;
        try {
            rot = DAOFactory.get(c);
        } catch (Exception e) {
            System.out.println(e);
        }

        Class dataClass = (Class) ((ParameterizedType) rot.getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        System.out.println("dataClass " + dataClass);
        if (rot != null) {
            System.out.println("rot class " + rot.getClass());
            for (Model m : (ArrayList<Model>) rot.findAll()) {

                TableNameAnnotation ta = (TableNameAnnotation) m.getClass().getAnnotation(TableNameAnnotation.class);
                if (ta == null) {
                    return null;
                }

                DataScope ds = (DataScope) m.getClass().getAnnotation(DataScope.class);
                if (ds == null || ds.type().equals(EScopeType.USER_GENERATED)) {
                    return null;
                }
                System.out.println("Datascope: " + ds.type());
                StringBuffer insert = new StringBuffer();

                Field[] fields = m.getClass().getDeclaredFields();
                //INSERT INTO `allianceboardpermission` VALUES ('116', 'USER', '8', '1', '0', '0');

                insert.append("INSERT INTO '" + ta.value() + "' VALUES ('");
                boolean first = true;
                for (Field f : fields) {
                    f.setAccessible(true);
                    FieldMappingAnnotation fma = (FieldMappingAnnotation) f.getAnnotation(FieldMappingAnnotation.class);
                    if (fma != null) {
                        try {

                            Class ec = f.getClass();
                            Object value;
                            if ((ec == boolean.class) || (ec == Boolean.class)) {
                                value = (Boolean) f.get(m) ? (int) 1 : (int) 0;
                            } else {
                                value = f.get(m);
                            }
                            if (first) {
                                first = false;
                                insert.append(value);
                            } else {
                                insert.append("','");
                                insert.append(value);
                            }
                        } catch (IllegalArgumentException ex) {
                            java.util.logging.Logger.getLogger(CreateFromModel.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IllegalAccessException ex) {
                            java.util.logging.Logger.getLogger(CreateFromModel.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }
                insert.append("')");
                insert.append("<BR>");
                sb.append(insert);

            }
        }
        return sb.toString();
    }

    private static String getTypeDef(Field f) {
        String typeDef = "";

        Class c = f.getType();

        if ((c == int.class) || (c == Integer.class)) {
            typeDef = " int(11)";
        } else if ((c == double.class) || (c == Double.class)) {
            typeDef = " double";
        } else if ((c == long.class) || (c == Long.class)) {
            typeDef = " bigint(20)";
        } else if ((c == float.class) || (c == Float.class)) {
            typeDef = " float";
        } else if ((c == boolean.class) || (c == Boolean.class)) {
            typeDef = " tinyint(3)";
        } else if (c == char.class) {
            typeDef = " varchar(1)";
        } else if (c == String.class) {
            typeDef = " varchar(50)";
        } else if (c.isEnum()) {
            Class fEnumType = f.getType();
            ArrayList<String> eValues = new ArrayList<String>();  // enum constants

            Enum[] eConstants = (Enum[]) fEnumType.getEnumConstants();
            for (Enum e : eConstants) {
                eValues.add(e.toString());
            }

            typeDef = " enum(";
            boolean firstenum = true;

            for (String name : eValues) {
                if (!firstenum) {
                    typeDef += ",";
                } else {
                    firstenum = false;
                }

                typeDef += "'" + name + "'";
            }

            typeDef += ") COLLATE utf8_general_ci";
        }

        return typeDef;
    }
}
