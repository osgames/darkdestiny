/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework;

import at.viswars.DebugBuffer;
import at.viswars.database.access.DbConnect;
import at.viswars.database.framework.exception.InternalFrameworkException;
import at.viswars.database.framework.exception.TransactionException;
import at.viswars.database.framework.transaction.Transaction;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.database.framework.transaction.TransactionMultiple;
import at.viswars.database.interfaces.ReadWrite;
import at.viswars.model.Model;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public abstract class ReadWriteTable<T extends Model> extends ReadOnlyTable<T> implements ReadWrite<T> {
    public ReadWriteTable() {              
        super();
    }    
    
    public T insertOrUpdate(T entry) {
        return null;
    }

    public synchronized ArrayList<T> insertOrUpdate(ArrayList<T> entries) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            return datasource.updateOrInsert(entries);
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
        
        return null;
    }    

    public synchronized T add(T entry) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            if ((getTransaction() != null) && (!getTransaction().isError())) {
                try {
                    return (T)getTransaction().executeTransaction(new Transaction(this,(Model)entry,Transaction.TRANSACTION_TYPE_INSERT));
                } catch (TransactionException e) {
                    e.printStackTrace();
                    throw new TransactionException("Add could not be executed");
                }                 
            } else {
                return (T)datasource.insert(entry);
            }            
        } catch (TransactionException te) {
            DbConnect.releaseWriteConnection();
            throw te;
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
        
        return null;
    }

    public synchronized T update(T entry) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            if ((getTransaction() != null) && (!getTransaction().isError())) {
                try {
                    return (T)getTransaction().executeTransaction(new Transaction(this,(Model)entry,Transaction.TRANSACTION_TYPE_UPDATE));
                } catch (TransactionException e) {
                    e.printStackTrace();
                    throw new TransactionException("Update could not be executed");
                }              
            } else {
                return (T)datasource.merge(entry);
            }
        } catch (TransactionException te) {
            DbConnect.releaseWriteConnection();  
            throw te;            
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
        
        return null;
    }

    public synchronized ArrayList<T> updateAll(ArrayList<T> entries) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            if ((getTransaction() != null) && (!getTransaction().isError())) {
                try {
                    return (ArrayList<T>)getTransaction().executeTransactionMultiple(new TransactionMultiple(this,(ArrayList<Model>)entries,Transaction.TRANSACTION_TYPE_UPDATE));
                } catch (TransactionException e) {
                    e.printStackTrace();
                    throw new TransactionException("UpdateAll could not be executed");
                }                      
            } else {
                return (ArrayList<T>)datasource.mergeAll(entries);
            }
        } catch (TransactionException te) {
            DbConnect.releaseWriteConnection();
            throw te;            
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
        
        return null;
    }    

    public synchronized ArrayList<T> insertAll(ArrayList<T> entries) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            if ((getTransaction() != null) && (!getTransaction().isError())) {
                try {
                    return (ArrayList<T>)getTransaction().executeTransactionMultiple(new TransactionMultiple(this,(ArrayList<Model>)entries,Transaction.TRANSACTION_TYPE_INSERT));
                } catch (TransactionException e) {
                    e.printStackTrace();
                    throw new TransactionException("InsertAll could not be executed");
                }                     
            } else {
                return (ArrayList<T>)datasource.insertAll(entries);
            }
        } catch (TransactionException te) {
            DbConnect.releaseWriteConnection();
            throw te;            
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
        
        return null;
    }        
    
    public synchronized void remove(T entry) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            if ((getTransaction() != null) && (!getTransaction().isError())) {
                try {
                    getTransaction().executeTransaction(new Transaction(this,(Model)entry,Transaction.TRANSACTION_TYPE_DELETE));
                } catch (TransactionException e) {
                    e.printStackTrace();
                    throw new TransactionException("Remove could not be executed");
                }                        
            } else {
                datasource.delete(entry);
            }
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
    }
    
    public synchronized int removeAll(ArrayList<T> entries) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            if ((getTransaction() != null) && (!getTransaction().isError())) {
                try {
                    getTransaction().executeTransactionMultiple(new TransactionMultiple(this,(ArrayList<Model>)entries,Transaction.TRANSACTION_TYPE_DELETE));
                } catch (TransactionException e) {
                    e.printStackTrace();
                    throw new TransactionException("RemoveAll could not be executed");
                }                
            } else {
                return datasource.deleteAll(entries);
            }
        } catch (TransactionException te) {
            DbConnect.releaseWriteConnection();
            throw te;            
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
        
        return -1;
    }        
    
    public synchronized int removeAll(QueryKeySet qks) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            return datasource.deleteAll(qks);
        } catch (Exception e) {
            e.printStackTrace();
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
        
        return 0;
    }    
    
    public synchronized int removeAll() throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            return datasource.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }
        
        return 0;
    }        
    
    public synchronized ArrayList<T> updateAll(T entry, QueryKeySet qks) throws TransactionException {
        try {
            DbConnect.assignWriteConnection();
            return (ArrayList<T>)datasource.mergeAll(entry, qks);
        } catch (Exception e) {
            e.printStackTrace();
            printUnderlyingException(e);
            if (getTransaction() != null) getTransaction().setTransactionError(new TransactionException(e.getMessage()));
        } finally {
            DbConnect.releaseWriteConnection();
        }

        return null;
    }
    
    private TransactionHandler getTransaction() {
        return TransactionHandler.getCurrentTransaction();
    }

    private void printUnderlyingException(Exception e) {
        if (e instanceof InternalFrameworkException) {
            InternalFrameworkException ife = (InternalFrameworkException)e;

            if (ife.getUnderlyingException() == null) return;
            if (ife.getUnderlyingException() instanceof SQLException) {
                SQLException sqle = (SQLException)ife.getUnderlyingException();

                DebugBuffer.error("SQL ERROR " + sqle.getErrorCode() + ": " + sqle.getMessage());
                DebugBuffer.writeStackTrace("STACKTRACE: ", sqle);
            }
        }
    }
}
