/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.database.framework.core;

import at.viswars.Logger.Logger;

/**
 *
 * @author Stefan
 */
public class AccessControl {

    private int givenLocks;
    private int waitingWriters;
    public static boolean TRACE = true;
    private Object mutex;

    private Exception lastObtainedWriteLock = null;
    private final DatabaseTable owner;
    
    public AccessControl(DatabaseTable dt) {        
        owner = dt;
        mutex = new Object();
        givenLocks = 0;
        waitingWriters = 0;
    }

    public Exception getLocks() {
        if (givenLocks == -1) {
            return lastObtainedWriteLock;
        } else {
            return null;
        }
    }
    
    public void getReadLock() {
        synchronized (mutex) {

            try {
                while ((givenLocks == -1) || (waitingWriters != 0)) {
                    if (TRACE) {
                        Logger.getLogger().write(Thread.currentThread().toString() + "waiting for readlock");
                    }
                    mutex.wait();
                }
            } catch (java.lang.InterruptedException e) {
                Logger.getLogger().write(e.getMessage());
            }

            givenLocks++;

            if (TRACE) {
                // Logger.getLogger().write(Thread.currentThread().toString() + " got readlock, GivenLocks = " + givenLocks);
            }
        }
    }

    public void getWriteLock() {
        synchronized (mutex) {
            waitingWriters++;
            try {
                while (givenLocks != 0) {
                    if (TRACE) {
                        String tableName = "";
                        
                        if (owner instanceof DatabaseTableBuffered) {
                            tableName = ((DatabaseTableBuffered)owner).tableName;
                        } else if (owner instanceof DatabaseTablePrimitive) {
                            tableName = ((DatabaseTablePrimitive)owner).tableName;
                        }
                        Logger.getLogger().write(Logger.LogLevel.INFO,Thread.currentThread().toString() + "waiting for writelock @ " + tableName);                        
                    }
                    mutex.wait();
                }
            } catch (java.lang.InterruptedException e) {
                Logger.getLogger().write(e.getMessage());
            }

            waitingWriters--;
            givenLocks = -1;

            if (TRACE) {
                lastObtainedWriteLock = new Exception("This function caused another thread to wait for writelock");
                // Logger.getLogger().write(Thread.currentThread().toString() + " got writelock, GivenLocks = " + givenLocks);
            }
        }
    }

    public void releaseLock() {
        synchronized (mutex) {
            if (givenLocks == 0) {
                return;
            }
            if (givenLocks == -1) {
                // Logger.getLogger().write("Release writelock");
                givenLocks = 0;
            } else {
                givenLocks--;
            }
            if (TRACE) {
                // Logger.getLogger().write(Thread.currentThread().toString() + " released lock, GivenLocks = " + givenLocks);
            }
            mutex.notifyAll();
        }
    }
}
