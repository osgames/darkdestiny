/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.database.framework.core;

/**
 *
 * @author Stefan
 */
public class ForeignKey {
    private final String baseFieldName;
    private final String mappingTable;
    private final String childTableName;
    private final String childFieldName;
    
    public ForeignKey(String baseFieldName, String mappingTable, String childTableName, String childFieldName) {
        this.baseFieldName = baseFieldName;
        this.mappingTable = mappingTable;
        this.childTableName = childTableName;
        this.childFieldName = childFieldName;
    }

    /**
     * @return the baseFieldName
     */
    public String getBaseFieldName() {
        return baseFieldName;
    }

    /**
     * @return the childTableName
     */
    public String getChildTableName() {
        return childTableName;
    }

    /**
     * @return the childFieldName
     */
    public String getChildFieldName() {
        return childFieldName;
    }

    /**
     * @return the mappingTable
     */
    public String getMappingTable() {
        return mappingTable;
    }
}
