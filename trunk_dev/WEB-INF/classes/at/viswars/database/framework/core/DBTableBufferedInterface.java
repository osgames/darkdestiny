/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework.core;

import at.viswars.database.framework.QueryKeySet;
import at.viswars.database.framework.exception.InternalFrameworkException;
import at.viswars.database.framework.exception.InvalidIndexException;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
interface DBTableBufferedInterface<T> extends DBTableInterface<T> {
    public ArrayList<T> getEntries(QueryKeySet qks, String indexName) throws InternalFrameworkException, InvalidIndexException;
}
