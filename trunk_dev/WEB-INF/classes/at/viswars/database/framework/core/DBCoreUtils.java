/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.database.framework.core;

import at.viswars.DebugBuffer;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.database.framework.QueryKey;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.database.framework.QueryLogging;
import at.viswars.database.framework.TableIndex;
import at.viswars.database.framework.TableIndexField;
import at.viswars.database.framework.exception.InternalFrameworkException;
import at.viswars.database.framework.exception.PrimaryKeyInvalidException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class DBCoreUtils {

    protected static <T> QueryKeySet buildQueryKeySetFromEntry(T entry, HashSet<String> mappedFields) throws InternalFrameworkException {
        try {
            QueryKeySet qks = new QueryKeySet();

            for (Field f : entry.getClass().getDeclaredFields()) {
                f.setAccessible(true);

                if (f.get(entry) == null) {
                    continue;
                }

                if (!mappedFields.contains(f.getName())) {
                    continue;
                }
                // Logger.getLogger().write("Add Key " + f.getName() + " with Value " + f.get(entry));
                qks.addKey(new QueryKey(f.getName(), f.get(entry)));
            }

            return qks;
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    // -------------------------------------------
    // Replacement for buildQueryKeySetFromPrimary
    // -------------------------------------------
    protected static <T> Object[][] getPrimaryQueryFields(T entry, TableIndex ti) throws InternalFrameworkException, PrimaryKeyInvalidException {
        try {
            Object[][] searchFields = new Object[ti.getTableIndexFields().size()][2];
            int counter = 0;
            
            for (TableIndexField tif : ti.getTableIndexFields()) {
                String fieldName = tif.getFieldName();
                Field f = entry.getClass().getDeclaredField(fieldName);
                
                Object fieldValue = getFieldValue(
                        entry,
                        f,
                        f.getType());
                
                if (fieldValue == null) throw new PrimaryKeyInvalidException("Primary Key not completly specified");
                
                searchFields[counter][0] = fieldName;
                searchFields[counter][1] = fieldValue;
                counter++;
            }

            return searchFields;
        } catch (NoSuchFieldException nsfe) {
            throw new InternalFrameworkException(nsfe.getMessage());
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }        
    }
    
    @Deprecated
    protected static <T> QueryKeySet buildQueryKeySetFromPrimary(T entry, HashMap<String, TableIndex> availIndices) throws InternalFrameworkException, PrimaryKeyInvalidException {
        try {
            TableIndex primary = availIndices.get("primary");

            QueryKeySet qks = new QueryKeySet();
            for (TableIndexField tif : primary.getTableIndexFields()) {
                Object fieldValue = getFieldValue(
                        entry,
                        entry.getClass().getDeclaredField(tif.getFieldName()),
                        entry.getClass().getDeclaredField(tif.getFieldName()).getType());
                
                if (fieldValue == null) throw new PrimaryKeyInvalidException("Primary Key not completly specified");
                
                qks.addKey(new QueryKey(
                        tif.getFieldName(),
                        fieldValue));
            }

            return qks;
        } catch (NoSuchFieldException nsfe) {
            throw new InternalFrameworkException(nsfe.getMessage());
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    protected static void checkNotPrimary(QueryKeySet qks, HashMap<String, TableIndex> availIndices) throws InternalFrameworkException {
        TableIndex ti = availIndices.get("primary");

        for (TableIndexField tif : ti.getTableIndexFields()) {
            for (QueryKey qk : qks.getKeys()) {
                if (qk.getFieldName().equals(tif.getFieldName())) {
                    throw new InternalFrameworkException("Primary fields must not be changed in update operations!");
                }
            }
        }
    }

    protected static void checkPrimaryKey(QueryKeySet qks, HashMap<String, TableIndex> availIndices) throws PrimaryKeyInvalidException {
        TableIndex ti = availIndices.get("primary");

        for (TableIndexField tif : ti.getTableIndexFields()) {
            boolean found = false;
            boolean typeValid = false;

            for (QueryKey qk : qks.getKeys()) {
                if (qk.getFieldName().equals(tif.getFieldName())) {
                    found = true;
                    if (qk.getClass() != tif.getDataType()) {
                        typeValid = true;
                    }
                }
            }

            if (!found) {
                throw new PrimaryKeyInvalidException("Not all fields specified");
            }
            if (!typeValid) {
                throw new PrimaryKeyInvalidException("Field " + tif.getFieldName() + " has invalid type should be " + tif.getDataType());
            }
        }
    }

    protected static <T> Object getFieldValue(T entry, Field f, Class c) throws IllegalAccessException {
        Object obj = null;

        f.setAccessible(true);

        if ((c == boolean.class) || (c == Boolean.class)) {
            obj = (Boolean) f.get(entry) ? (int) 1 : (int) 0;
        } else {
            return f.get(entry);
        }
        
        /*
        if ((c == int.class) || (c == Integer.class)) {
            obj = f.get(entry);
        } else if ((c == double.class) || (c == Double.class)) {
            obj = f.get(entry);
        } else if ((c == long.class) || (c == Long.class)) {
            obj = f.get(entry);
        } else if ((c == float.class) || (c == Float.class)) {
            obj = f.get(entry);
        } else if ((c == boolean.class) || (c == Boolean.class)) {
            obj = (Boolean) f.get(entry) ? (int) 1 : (int) 0;
        } else if (c == char.class) {
            obj = f.get(entry);
        } else if (c == Enum.class) {
            obj = f.get(entry);
        }
        */
        
        return obj;
    }

    protected static QueryMode getQueryMode(QueryKeySet qks, HashMap<String, TableIndex> availIndices) {
        // Logger.getLogger().write("--------------------- START ANALYZE ------------------------");
        QueryMode qm = null;

        TableIndex index = null;

        HashMap<String, Object> queryFields = new HashMap<String, Object>();

        int queryValue = 0;
        for (QueryKey qk : qks.getKeys()) {
            // Logger.getLogger().write("Add field " + qk.getFieldName() + " with value " + qk.getFieldValue());
            queryFields.put(qk.getFieldName(), qk.getFieldValue());
            queryValue++;
        }

        TreeMap<Integer, ArrayList<TableIndex>> ratedIndices = new TreeMap<Integer, ArrayList<TableIndex>>();

        for (Map.Entry<String, TableIndex> _index : availIndices.entrySet()) {
            TableIndex ti = _index.getValue();
            // Logger.getLogger().write("Checking index " + ti.getName());
            HashSet<String> indexFields = new HashSet<String>();

            int indexValue = 0;
            for (TableIndexField tif : ti.getTableIndexFields()) {
                // Logger.getLogger().write("Index contains field " + tif.getFieldName());
                indexFields.add(tif.getFieldName());

                // Logger.getLogger().write("Check if queryFields contains " + tif.getFieldName() + " for index " + ti.getName());
                if (queryFields.keySet().contains(tif.getFieldName())) {
                    indexValue++;
                }
            }

            if (indexValue == 0) {
                continue;
            }

            // Logger.getLogger().write("Found indexValue " + indexValue + " for index " + ti.getName());

            if (ratedIndices.containsKey(indexValue)) {
                ArrayList<TableIndex> tiList = ratedIndices.get(indexValue);
                tiList.add(ti);
            } else {
                ArrayList<TableIndex> tiList = new ArrayList<TableIndex>();
                tiList.add(ti);
                ratedIndices.put(indexValue, tiList);
            }

            // Optimal index was found!
            if (indexFields.containsAll(queryFields.keySet())) {
                // Logger.getLogger().write("Index " + ti.getName() + " is optimal");
                index = ti;
                break;
            }
        }

        int queryMode = -1;
        HashSet<TableIndex> choosenIndices = new HashSet<TableIndex>();

        if (index == null) {
            // try to find indices for all fields 
            // DESCRIPTION: Start with best rated indices and try to combine 
            // with next matching indices to find a optimal solution
            HashSet<String> coveredFields = new HashSet<String>();            

            if ((ratedIndices.size() > 0) && (index == null)) {
                // Logger.getLogger().write("Try to build indexed merged!");
                for (ArrayList<TableIndex> tiList : ratedIndices.descendingMap().values()) {
                    for (TableIndex ti : tiList) {
                        // Logger.getLogger().write("Check index " + ti.getName());
                        for (TableIndexField tif : ti.getTableIndexFields()) {
                            if (coveredFields.contains(tif.getFieldName())) {
                                continue;
                            }
                            // Logger.getLogger().write("Check if queryFields contains field " + tif.getFieldName() + " for index " + ti.getName());
                            if (queryFields.keySet().contains(tif.getFieldName())) {
                                // Logger.getLogger().write("Found field " + tif.getFieldName() + " in index " + ti.getName());
                                coveredFields.add(tif.getFieldName());
                                queryFields.remove(tif.getFieldName());
                                choosenIndices.add(ti);
                            }
                        }
                    }

                    if (queryFields.size() == 0) {
                        queryMode = QueryMode.QUERYMODE_INDEXED_MERGED;
                    }
                }
            }
        }

        if (index != null) {
            queryMode = QueryMode.QUERYMODE_INDEXED;
            choosenIndices.add(index);
        } else {
            if (ratedIndices.size() > 0) {
                if (queryFields.size() == 0) {
                    queryMode = QueryMode.QUERYMODE_INDEXED_MERGED;
                } else {
                    queryMode = QueryMode.QUERYMODE_INDEXED_PARTIAL;
                    choosenIndices.add(ratedIndices.lastEntry().getValue().get(0));
                }
            } else {
                queryMode = QueryMode.QUERYMODE_DIRECT;
            }
        }

        if (queryMode == QueryMode.QUERYMODE_DIRECT) {
            // System.out.println("Using query mode direct");
            // for (QueryKey qk : qks.getKeys()) {
                // System.out.println(qk.getFieldName() + " >> " + qk.getFieldValue());
            // }

            QueryLogging.directQueries++;
        } else if (queryMode == QueryMode.QUERYMODE_INDEXED) {
            QueryLogging.indexedQueries++;
        } else if (queryMode == QueryMode.QUERYMODE_INDEXED_MERGED) {
            QueryLogging.mergedQueries++;
        } else if (queryMode == QueryMode.QUERYMODE_INDEXED_PARTIAL) {
            QueryLogging.partialQueries++;
        }

        qm = new QueryMode(queryMode, choosenIndices, queryFields);

        return qm;
    }

    protected static String QKSToSQL_SETSTR(QueryKeySet qks) {
        StringBuffer where = new StringBuffer();

        boolean firstEntry = true;

        for (QueryKey qk : qks.getKeys()) {
            if (!firstEntry) {
                where.append(", ");
            }

            firstEntry = false;
            if ((qk.getFieldValue() == String.class) || qk.getFieldValue() instanceof Enum) {
                if (qk.getFieldValue() == null) {
                    where.append(qk.getFieldName() + "=null");
                } else {
                    where.append(qk.getFieldName() + "='" + saveSQL(qk.getFieldValue()) + "'");
                }
            } else {
                where.append(qk.getFieldName() + "=" + qk.getFieldValue());
            }
        }

        return where.toString();
    }

    protected static String QKSToSQL_WHERESTR(QueryKeySet qks) {
        StringBuffer where = new StringBuffer();

        boolean firstEntry = true;

        for (QueryKey qk : qks.getKeys()) {
            if (!firstEntry) {
                where.append(" AND ");
            }

            firstEntry = false;
            if (qk.getFieldValue() == String.class) {
                where.append(qk.getFieldName() + "='" + saveSQL(qk.getFieldValue()) + "'");
            } else {
                where.append(qk.getFieldName() + "=" + qk.getFieldValue());
            }
        }

        return where.toString();
    }

    protected static String QKSToSQL_DELETESTR(QueryKeySet qks) {
        StringBuffer where = new StringBuffer();

        boolean firstEntry = true;

        for (QueryKey qk : qks.getKeys()) {
            if (!firstEntry) {
                where.append(" AND ");
            }

            firstEntry = false;
            if ((qk.getFieldValue() == String.class) ||
                (qk.getFieldValue() instanceof Enum)) {
                where.append(qk.getFieldName() + "='" + saveSQL(qk.getFieldValue()) + "'");
            } else {
                where.append(qk.getFieldName() + "=" + qk.getFieldValue());
            }
        }

        return where.toString();
    }

    protected static <T> String EntryToSQL_DELETESTR(T entry, HashMap<String,String> fieldToDBName, HashSet<String> primaryFields) throws IllegalAccessException {
        StringBuffer delete = new StringBuffer();

        boolean firstEntry = true;

        for (Field f : entry.getClass().getDeclaredFields()) {
            if (!fieldToDBName.containsKey(f.getName())) {
                continue;
            }
            if (!primaryFields.contains(f.getName())) continue;
                        
            f.setAccessible(true);
            if (f.get(entry) == null) continue;            

            if (!firstEntry) {
                delete.append(" AND ");
            }

            firstEntry = false;
            delete.append(fieldToDBName.get(f.getName()) + "=");

            if ((f.getType() == String.class) || (f.getType().isEnum())) {
                delete.append("'" + saveSQL(f.get(entry)) + "'");
            } else {
                delete.append(f.get(entry));
            }
        }

        return delete.toString();        
    }
    
    protected static <T> String EntryToSQL_INSERTSTR(T entry, HashMap<String, Object> defValues, HashMap<String,String> fieldToDBName) throws 
            IllegalAccessException, InternalFrameworkException {
        StringBuffer insert = new StringBuffer();

        StringBuffer fields = new StringBuffer();
        StringBuffer values = new StringBuffer();

        boolean firstEntry = true;

        for (Field f : entry.getClass().getDeclaredFields()) {                                  
            if (!fieldToDBName.containsKey(f.getName())) {
                continue;
            }
            f.setAccessible(true);

            if (f.get(entry) == null) {
                String fieldName = fieldToDBName.get(f.getName());

                if (defValues.get(fieldName) != null) {           
                    try {
                        f.set(entry, convertValue(f, defValues.get(fieldName)));
                    } catch (Exception e) {
                        DebugBuffer.error("Default value '"+defValues.get(fieldName)+"' for field " + fieldName + " count not be set: ", e);
                        throw new InternalFrameworkException("Default value '"+defValues.get(fieldName)+"' for field " + fieldName + " could not be set!");
                    }
                } else {
                    Logger.getLogger().write(LogLevel.WARNING,"FIELD " + fieldName + " has no default value but was null! Please add autoincrement=true in ColumnProperties if this is intended! ["+entry+"]");
                    continue;
                }
            }

            if (!firstEntry) {
                fields.append(", ");
                values.append(", ");
            }

            firstEntry = false;
            fields.append(fieldToDBName.get(f.getName()));

            if ((f.getType() == String.class) || (f.getType().isEnum())) {
                values.append("'" + saveSQL(f.get(entry)) + "'");
            } else {
                values.append(f.get(entry));
            }
        }

        insert.append("(");
        insert.append(fields);
        insert.append(") VALUES (");
        insert.append(values);
        insert.append(")");

        return insert.toString();
    }

    protected static String QKSToSQL_INSERTSTR(QueryKeySet qks) {
        StringBuffer insert = new StringBuffer();

        StringBuffer fields = new StringBuffer();
        StringBuffer values = new StringBuffer();

        boolean firstEntry = true;

        for (QueryKey qk : qks.getKeys()) {
            if (!firstEntry) {
                fields.append(", ");
                values.append(", ");
            }

            firstEntry = false;
            fields.append(qk.getFieldName());

            // Logger.getLogger().write("FIELD=" + qk.getFieldName() + " VALUE=>" + qk.getFieldValue() + "< TYPE=" + qk.getFieldValue().getClass());

            if (qk.getFieldValue() == String.class) {
                values.append("'" + saveSQL(qk.getFieldValue()) + "'");
            } else {
                if (qk.getFieldValue().equals("")) {
                    values.append("''");
                } else {
                    values.append(qk.getFieldValue());
                }
            }
        }

        insert.append("(");
        insert.append(fields);
        insert.append(") VALUES (");
        insert.append(values);
        insert.append(")");

        return insert.toString();
    }

    protected static Object convertValue(Field f, Object v) throws IllegalAccessException {
        Object obj = null;

        f.setAccessible(true);
        Class c = f.getType();

        if ((c == int.class) || (c == Integer.class)) {
            obj = Integer.parseInt((String) v);
        } else if ((c == double.class) || (c == Double.class)) {
            obj = Double.parseDouble((String) v);
        } else if ((c == long.class) || (c == Long.class)) {
            obj = Long.parseLong((String) v);
        } else if ((c == float.class) || (c == Float.class)) {
            obj = Float.parseFloat((String) v);
        } else if ((c == boolean.class) || (c == Boolean.class)) {
            obj = null;
            Integer i = Integer.parseInt((String) v);
            if (i != null) {
                if (i == 0) {
                    obj = false;
                } else {
                    obj = true;
                }
            }
        } else if (c == char.class) {
            obj = null;
        } else if (f.getType().isEnum()) {
            obj = Enum.valueOf(c, (String)v);
        }

        return obj;
    }
    
    protected static Object saveSQL(Object input) {
        if (input == null) return null;
        
        if (input.getClass() == String.class) {
            String output = (String)input;
            output = output.replace("'", "\\'");  
            return output;
        } else {
            return input;
        }  
    }
    
    public static void inspect(Object o) {
        Type type = o.getClass();
        while (type != null) {
            System.out.print(type + " implements");
            Class<?> rawType =
                    (type instanceof ParameterizedType)
                    ? (Class<?>) ((ParameterizedType) type).getRawType()
                    : (Class<?>) type;
            Type[] interfaceTypes = rawType.getGenericInterfaces();
            if (interfaceTypes.length > 0) {
                System.out.println(":");
                for (Type interfaceType : interfaceTypes) {
                    if (interfaceType instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) interfaceType;
                        System.out.print("  " + parameterizedType.getRawType() + " with type args: ");
                        Type[] actualTypeArgs = parameterizedType.getActualTypeArguments();
                        System.out.println(Arrays.toString(actualTypeArgs));
                    } else {
                        System.out.println("  " + interfaceType);
                    }
                }
            } else {
                System.out.println(" nothing");
            }
            type = rawType.getGenericSuperclass();
        }
    }    
}
