/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.database.framework.core;

import at.viswars.database.framework.OrderKey;
import at.viswars.database.framework.QueryKey;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.database.framework.TableIndex;
import at.viswars.database.framework.TableIndexField;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.exception.InternalFrameworkException;
import at.viswars.database.framework.exception.InvalidIndexException;
import at.viswars.model.User;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
class IndexedTableData<T> {

    private final String indexName;
    private HashMap<String, HashMap<Object, HashSet<T>>> indexedTable = new HashMap<String, HashMap<Object, HashSet<T>>>();
    private HashMap<String, String> fieldNameMapping = new HashMap<String, String>();

    protected IndexedTableData(TableIndex tblIdx, Class<T> dataClass) {
        // // Logger.getLogger().write("Creating Index " + tblIdx.getName() + " for table " + tableName);
        indexName = tblIdx.getName();

        for (TableIndexField idxField : tblIdx.getTableIndexFields()) {
            HashMap<Object, HashSet<T>> values = new HashMap<Object, HashSet<T>>();
            indexedTable.put(idxField.getFieldName(), values);

            // // Logger.getLogger().write("Added field " + idxField.getFieldName() + " to indexTableData");
        }

        for (Field f : dataClass.getDeclaredFields()) {
            if (f.isAnnotationPresent(FieldMappingAnnotation.class)) {
                // Logger.getLogger().write(dataClass.getName() + ": Mapping " + f.getAnnotation(FieldMappingAnnotation.class).value() + " to " + f.getName());
                fieldNameMapping.put(f.getAnnotation(FieldMappingAnnotation.class).value(), f.getName());
            }
        }
    }

    protected void deleteEntry(T entry, Field f) {
        HashMap<Object, HashSet<T>> indexEntry = indexedTable.get(f.getName());
        for (HashSet<T> content : indexEntry.values()) {
            if (content.remove(entry)) {
                break;
            }
        }
    }

    protected void deleteEntry(T entry) {
        for (HashMap<Object, HashSet<T>> indexEntries : indexedTable.values()) {
            // // Logger.getLogger().write("Looping values");

            for (HashSet<T> content : indexEntries.values()) {
                // // Logger.getLogger().write("HashSet contains?" + content.contains(entry));
                if (content.remove(entry)) {
                    break;
                }
            }
        }
    }

    protected void deleteAll(HashSet<T> entries) {
        for (HashMap<Object, HashSet<T>> indexEntries : indexedTable.values()) {
            for (Map.Entry<Object, HashSet<T>> indexEntry : indexEntries.entrySet()) {
                // // Logger.getLogger().write("HashSet contains?" + content.contains(entry));
                Object key = indexEntry.getKey();
                HashSet<T> content = indexEntry.getValue();

                // Logger.getLogger().write("Clean index " + indexName + " for value " + key);

                content.removeAll(entries);
            }
        }
    }

    protected void addEntries(HashSet<T> entries, Field fIn, Class c) throws IllegalAccessException, NoSuchFieldException {
        // HashMap<Object, HashSet<T>> values = indexedTable.get(fIn.getAnnotation(FieldMappingAnnotation.class).value());
        HashMap<Object, HashSet<T>> values = indexedTable.get(fIn.getName());
        if (values == null) {
            return;
        }

        Object obj = null;

        for (T entry : entries) {
            Field f = entry.getClass().getDeclaredField(fIn.getName());
            f.setAccessible(true);

            if ((c == int.class) || (c == Integer.class)) {
                obj = f.get(entry);
            } else if ((c == double.class) || (c == Double.class)) {
                obj = f.get(entry);
            } else if ((c == long.class) || (c == Long.class)) {
                obj = f.get(entry);
            } else if ((c == float.class) || (c == Float.class)) {
                obj = f.get(entry);
            } else if ((c == boolean.class) || (c == Boolean.class)) {
                // obj = f.getBoolean(entry) ? (int) 1 : (int) 0;              
                obj = f.get(entry);
            } else if (c == char.class) {
                obj = f.get(entry);
            } else if (c == String.class) {
                obj = (String) f.get(entry);
            } else if (c.isEnum()) {
                obj = Enum.valueOf(c, f.get(entry).toString());
            }

            if (values.containsKey(obj)) {
                // Logger.getLogger().write("VALUE " + obj + " EXISTS, ADDING " + entry + " TO INDEX " + indexName);
                values.get(obj).add(entry);
            } else {
                // Logger.getLogger().write("VALUE " + obj + " NOT EXISTS, CREATING " + entry + " IN " + indexName);
                HashSet<T> entriesNew = new HashSet<T>();
                entriesNew.add(entry);
                values.put(obj, entriesNew);
            }
        }
    }

    protected void addEntry(T entry, Field f, Class c) throws IllegalAccessException {
        // HashMap<Object, HashSet<T>> values = indexedTable.get(f.getAnnotation(FieldMappingAnnotation.class).value());
        HashMap<Object, HashSet<T>> values = indexedTable.get(f.getName());
        if (values == null) {
            return;
        }
        Object obj = null;

        if ((c == int.class) || (c == Integer.class)) {
            obj = f.get(entry);
        } else if ((c == double.class) || (c == Double.class)) {
            obj = f.get(entry);
        } else if ((c == long.class) || (c == Long.class)) {
            obj = f.get(entry);
        } else if ((c == float.class) || (c == Float.class)) {
            obj = f.get(entry);
        } else if ((c == boolean.class) || (c == Boolean.class)) {
            // obj = f.getBoolean(entry) ? (int) 1 : (int) 0;              
            obj = f.get(entry);
        } else if (c == char.class) {
            obj = f.get(entry);
        } else if (c == String.class) {
            obj = (String) f.get(entry);
        } else if (c.isEnum()) {
            obj = Enum.valueOf(c, f.get(entry).toString());
        }

        if (values.containsKey(obj)) {
            values.get(obj).add(entry);
        } else {
            HashSet<T> entries = new HashSet<T>();
            entries.add(entry);
            values.put(obj, entries);
        }
    }

    protected void moveEntry(String fieldName, T entry, Object oldValue, Object newValue) {
        if (indexedTable.get(fieldName).get(newValue) == null) {
            HashSet<T> newSet = new HashSet<T>();
            newSet.add(entry);
            indexedTable.get(fieldName).put(newValue, newSet);
        } else {
            indexedTable.get(fieldName).get(oldValue).remove(entry);
            indexedTable.get(fieldName).get(newValue).add(entry);
        }
    }

    protected HashSet<T> getEntryFast(Map<String, Object> keys) {
        HashSet<T> result = null;

        for (Map.Entry<String, Object> key : keys.entrySet()) {
            if (result == null) {
                result = new HashSet<T>();
                result.addAll(indexedTable.get(key.getKey()).get(key.getValue()));
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "getEntryFast: Checking KEY "+key.getKey()+" with value " + key.getValue() + " RESULTS->"+result.size());
            } else {
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "getEntryFast: Checking KEY "+key.getKey()+" with value " + key.getValue() + " RESULTS FOUND->"+indexedTable.get(key.getKey()).get(key.getValue()).size());
                // indexedTable.get(key.getKey()).get(key.getValue()).retainAll(result); 
                result.retainAll(indexedTable.get(key.getKey()).get(key.getValue()));
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "getEntryFast: Checking KEY "+key.getKey()+" with value " + key.getValue() + " RESULTS->"+result.size());
            }
        }

        return result;
    }

    protected T getEntryRaw2(Object[][] queryFields) throws InternalFrameworkException, InvalidIndexException {
        HashSet<T> resBuffer = null;

        for (Object[] actField : queryFields) {
            String fieldName = fieldNameMapping.get(actField[0]);
            if (fieldName == null) {
                fieldName = (String) actField[0];
            }

            HashMap<Object, HashSet<T>> entries = indexedTable.get(fieldName);

            if (entries.containsKey(actField[1])) {
                // // Logger.getLogger().write("Found " + entries.get(qk.getFieldValue()).size() + " matches for " + qk.getFieldName());
                if (resBuffer == null) {
                    resBuffer = new HashSet<T>();

                    resBuffer.addAll(entries.get(actField[1]));
                } else {
                    resBuffer.retainAll(entries.get(actField[1]));
                    if (resBuffer.size() == 0) {
                        return null;
                    }
                }
            } else {
                return null;
            }
        }


        if (!resBuffer.iterator().hasNext()) {
            return null;
        }
        return resBuffer.iterator().next();
    }

    protected HashSet<T> getEntryRaw(QueryKeySet keyset) throws InternalFrameworkException, InvalidIndexException {
        HashSet<T> resultBuffer = new HashSet<T>();
        HashSet<T> resBuffer = null;

        for (QueryKey qk : keyset.getKeys()) {
            String fieldName = fieldNameMapping.get(qk.getFieldName());
            if (fieldName == null) {
                fieldName = qk.getFieldName();
            }

            if (!indexedTable.containsKey(fieldName)) {
                // System.err.println("[WARNING] Field " + fieldName + " could not be found in index " + indexName);
                continue;
            }
            HashMap<Object, HashSet<T>> entries = indexedTable.get(fieldName);

            // // Logger.getLogger().write("Check for fieldValue " + qk.getFieldValue() + " in field " + qk.getFieldName());

            if (entries.containsKey(qk.getFieldValue())) {
                // // Logger.getLogger().write("Found " + entries.get(qk.getFieldValue()).size() + " matches for " + qk.getFieldName());

                if (resBuffer == null) {
                    resBuffer = new HashSet<T>();

                    /*
                    for (T resClass : entries.get(qk.getFieldValue())) {
                    // Logger.getLogger().write("Found Result Ref: " + resClass);
                    }
                     */

                    resBuffer.addAll(entries.get(qk.getFieldValue()));
                } else {
                    resBuffer.retainAll(entries.get(qk.getFieldValue()));
                }
            } else {
                // // Logger.getLogger().write("Clear");
                resBuffer = null;
                break;
            }
        }

        if (resBuffer == null) {
            return new HashSet<T>();
        }

        Collection<T> medResult = null;

        try {
            if (!keyset.getOrderKeys().isEmpty()) {
                for (OrderKey ok : keyset.getOrderKeys()) {
                    String oKeyName = fieldNameMapping.get(ok.getFieldName());

                    TreeMap<Object, LinkedList<T>> tm = new TreeMap<Object, LinkedList<T>>();

                    if (medResult == null) {
                        for (T entry : resBuffer) {
                            Field f = entry.getClass().getDeclaredField(oKeyName);
                            f.setAccessible(true);

                            // // Logger.getLogger().write("Order Field " + f.getName());

                            if (tm.containsKey(f.get(entry))) {
                                tm.get(f.get(entry)).add(entry);
                            } else {
                                LinkedList<T> ll = new LinkedList<T>();
                                ll.add(entry);
                                tm.put(f.get(entry), ll);
                            }
                        }

                        medResult = new LinkedList<T>();

                        if (ok.getOrder() == OrderKey.ORDER_ASC) {
                            // // Logger.getLogger().write("Get Ascending result");
                            for (LinkedList<T> list : tm.values()) {
                                medResult.addAll(list);
                            }
                        } else if (ok.getOrder() == OrderKey.ORDER_DESC) {
                            // // Logger.getLogger().write("Get Descending result");
                            for (LinkedList<T> list : tm.descendingMap().values()) {
                                medResult.addAll(list);
                            }
                        }
                    } else {
                        for (T entry : medResult) {
                            Field f = entry.getClass().getDeclaredField(oKeyName);
                            f.setAccessible(true);

                            if (tm.containsKey(f.get(entry))) {
                                tm.get(f.get(entry)).add(entry);
                            } else {
                                LinkedList<T> ll = new LinkedList<T>();
                                ll.add(entry);

                                tm.put(f.get(entry), ll);
                            }
                        }

                        if (ok.getOrder() == OrderKey.ORDER_ASC) {
                            // // Logger.getLogger().write("Get Ascending result");
                            for (LinkedList<T> list : tm.values()) {
                                medResult.addAll(list);
                            }
                        } else if (ok.getOrder() == OrderKey.ORDER_DESC) {
                            // // Logger.getLogger().write("Get Descending result");
                            for (LinkedList<T> list : tm.descendingMap().values()) {
                                medResult.addAll(list);
                            }
                        }
                    }
                }
            }
        } catch (NoSuchFieldException nsfe) {
            throw new InternalFrameworkException(nsfe.getMessage());
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }

        if (medResult != null) {
            resultBuffer.addAll(medResult);
        } else {
            resultBuffer.addAll(resBuffer);
        }

        return resultBuffer;
    }

    protected ArrayList<T> getEntry(QueryKeySet keyset) throws InternalFrameworkException, InvalidIndexException {
        ArrayList<T> result = new ArrayList<T>();
        HashSet<T> resBuffer = getEntryRaw(keyset);

        result.addAll(resBuffer);

        return result;
    }

    protected HashSet<T> getAllEntriesRaw() {
        HashSet<T> result = new HashSet<T>();

        for (HashMap<Object, HashSet<T>> indexEntry : indexedTable.values()) {
            for (HashSet<T> indexContent : indexEntry.values()) {
                result.addAll(indexContent);
            }
            break;
        }

        return result;
    }

    protected ArrayList<T> getAllEntries() {
        ArrayList<T> result = new ArrayList<T>();

        for (HashMap<Object, HashSet<T>> indexEntry : indexedTable.values()) {
            for (HashSet<T> indexContent : indexEntry.values()) {
                result.addAll(indexContent);
            }
            break;
        }

        return result;
    }
}
