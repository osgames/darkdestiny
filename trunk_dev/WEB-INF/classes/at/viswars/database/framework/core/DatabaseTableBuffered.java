/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.database.framework.core;

import at.viswars.DebugBuffer;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.database.access.DbConnect;
import at.viswars.database.framework.DatabaseHelper;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.IndexType;
import at.viswars.database.framework.annotations.OrderByAnnotation;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.database.framework.TableIndex;
import at.viswars.database.framework.TableIndexField;
import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.OneToMany;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.database.framework.exception.InternalFrameworkException;
import at.viswars.database.framework.exception.InvalidIndexException;
import at.viswars.database.framework.exception.PrimaryKeyInvalidException;
import at.viswars.model.DataChangeAware;
import at.viswars.model.Model;
import at.viswars.model.PlayerFleet;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 *
 * @author Stefan
 */
public class DatabaseTableBuffered<T> extends DatabaseTable<T> implements DBTableBufferedInterface<T> {

    private AccessControl ac;
    protected final Class<T> dataClass;
    protected final String tableName;
    private HashMap<TableIndex, IndexedTableData<T>> indices = new HashMap<TableIndex, IndexedTableData<T>>();
    protected HashMap<String, TableIndex> availIndices = new HashMap<String, TableIndex>();
    private HashMap<Integer, ArrayList<T>> buffer = new HashMap<Integer, ArrayList<T>>();
    private HashMap<String, Object> defaultValues = new HashMap<String, Object>();   
   
    private final static int QUERYMODE_INDEXED = 0;
    private final static int QUERYMODE_INDEXED_MERGED = 1;
    private final static int QUERYMODE_INDEXED_PARTIAL = 2;
    private final static int QUERYMODE_DIRECT = 3;

    public DatabaseTableBuffered(Class<T> dataClass) {
        ac = new AccessControl(this);

        this.dataClass = dataClass;
        tableName = ((TableNameAnnotation) dataClass.getAnnotation(TableNameAnnotation.class)).value();

        indices.clear();
        availIndices.clear();
        buffer.clear();
        defaultValues.clear();

        initTable();
    }

    private void initTable() {
        // load default Values
        try {
            Statement defStmt = DbConnect.createStatement();
            /*
            ResultSet rs = defStmt.executeQuery("SELECT COLUMN_NAME, COLUMN_DEFAULT " +
            "FROM INFORMATION_SCHEMA.COLUMNS " +
            "WHERE table_name = '" + tableName + "' AND table_schema= '" + DbConnect.getDbName() + "'");
             */
            ResultSet rs = defStmt.executeQuery("SHOW COLUMNS FROM " + tableName);


            while (rs.next()) {
                defaultValues.put(rs.getString(1), rs.getObject(5));
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }

        String sqlSelect = "SELECT ";

        boolean firstField = true;

        HashMap<String, ArrayList<TableIndexField>> tmpIndices = new HashMap<String, ArrayList<TableIndexField>>();

        for (Field f : dataClass.getDeclaredFields()) {
            for (Annotation a : f.getDeclaredAnnotations()) {
                if (a instanceof FieldMappingAnnotation) { // Build select statement
                    mappedFields.add(f.getName());
                    fieldToDBName.put(f.getName(), ((FieldMappingAnnotation) a).value());

                    if (firstField) {
                        sqlSelect += ((FieldMappingAnnotation) a).value();
                        firstField = false;
                    } else {
                        sqlSelect += ", " + ((FieldMappingAnnotation) a).value();
                    }
                } else if (a instanceof IdFieldAnnotation) { // Create primary key
                    primaryFields.add(f.getName());

                    if (tmpIndices.containsKey("primary")) {
                        tmpIndices.get("primary").add(new TableIndexField(f.getName(), f.getType()));
                    } else {
                        ArrayList<TableIndexField> idxFieldList = new ArrayList<TableIndexField>();
                        // idxFieldList.add(new TableIndexField(f.getAnnotation(FieldMappingAnnotation.class).value(), f.getType()));
                        idxFieldList.add(new TableIndexField(f.getName(), f.getType()));

                        tmpIndices.put("primary", idxFieldList);
                    }
                } else if (a instanceof IndexAnnotation) { // Create indices                                        
                    indexedFields.add(f.getName());

                    IndexAnnotation ifa = (IndexAnnotation) a;

                    if (tmpIndices.containsKey(ifa.indexName())) {
                        tmpIndices.get(ifa.indexName()).add(new TableIndexField(f.getName(), f.getType()));
                    } else {
                        ArrayList<TableIndexField> idxFieldList = new ArrayList<TableIndexField>();
                        // idxFieldList.add(new TableIndexField(f.getAnnotation(FieldMappingAnnotation.class).value(), f.getType()));
                        idxFieldList.add(new TableIndexField(f.getName(), f.getType()));

                        tmpIndices.put(ifa.indexName(), idxFieldList);
                    }
                } else if (a instanceof OrderByAnnotation) { // Create Order By String
                    FieldMappingAnnotation fma = f.getAnnotation(FieldMappingAnnotation.class);
                } else if (a instanceof OneToMany) { // Create Foreign Key Mapping
                    OneToMany otm = f.getAnnotation(OneToMany.class);
                    String refTable = otm.refTable();
                    String variableName = refTable.substring(0, 1).toLowerCase() + refTable.substring(1) + "List";
                    String fieldName = otm.baseField();
                    
                    try {
                        Field baseField = dataClass.getDeclaredField(fieldName);

                        ForeignKey fk = new ForeignKey(baseField.getName(),otm.mappingTable(),otm.refTable(),otm.refField());
                        foreignKeys.put(variableName, fk);                                       
                        ForeignKeyHandler.getInstance().addForeignKey(dataClass, variableName, fk);

                        Logger.getLogger().write("Created ForeignKey for parentField " + f.getName() + " to table " + otm.refTable() + "/" + otm.refField() + " DataArray: " + variableName);
                    } catch (NoSuchFieldException nsfe) {
                        Logger.getLogger().write(LogLevel.ERROR, "Field " + fieldName + " does not exist in model " + dataClass);
                    }   
                }
            }
        }

        Logger.getLogger().write("Build indices");
        
        // Build indices               
        for (Map.Entry<String, ArrayList<TableIndexField>> me : tmpIndices.entrySet()) {
            if (me.getKey().equalsIgnoreCase("primary")) {
                TableIndex primaryTI = new TableIndex("primary", IndexType.PRIMARY_INDEX, me.getValue());
                availIndices.put(primaryTI.getName(), primaryTI);
                indices.put(primaryTI, new IndexedTableData<T>(primaryTI, dataClass));
            } else {
                TableIndex index = new TableIndex(me.getKey(), IndexType.PRIMARY_INDEX, me.getValue());
                availIndices.put(index.getName(), index);
                indices.put(index, new IndexedTableData<T>(index, dataClass));
            }
        }

        Logger.getLogger().write("Load data from " + tableName);
        
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery(sqlSelect + " FROM " + tableName);

            int i = 0;

            while (rs.next()) {
                if (tableName.equalsIgnoreCase("fk_testtable")) Logger.getLogger().write("Loading ... ");
                
                Constructor<T> constructor = dataClass.getConstructor();
                constructor.setAccessible(true);
                T dataEntry = constructor.newInstance();

                // TableIndex ti2 = availIndices.get("primary");
                Collection<TableIndex> tblIndices = availIndices.values();

                for (Field field : dataClass.getDeclaredFields()) {
                    if (tableName.equalsIgnoreCase("fk_testtable")) Logger.getLogger().write("Check field ... " + field.getName() + " Type: " + field.getType());
                    
                    field.setAccessible(true);

                    String dbFieldName = fieldToDBName.get(field.getName());
                    if ((dbFieldName == null) && !(field.getType() == ArrayList.class)) {
                        continue;
                    }

                    /* FieldMappingAnnotation fma = field.getAnnotation(FieldMappingAnnotation.class);

                    if (fma == null) {
                    continue;
                    }
                     */

                    if ((field.getType() == Integer.class) || (field.getType() == int.class)) {
                        field.set(dataEntry, rs.getInt(dbFieldName));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == Long.class) || (field.getType() == long.class)) {
                        field.set(dataEntry, rs.getLong(dbFieldName));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == Double.class) || (field.getType() == double.class)) {
                        field.set(dataEntry, rs.getDouble(dbFieldName));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == Float.class) || (field.getType() == float.class)) {
                        field.set(dataEntry, rs.getFloat(dbFieldName));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == Boolean.class) || (field.getType() == boolean.class)) {
                        field.set(dataEntry, rs.getInt(dbFieldName) == 1);
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == String.class)) {
                        field.set(dataEntry, rs.getString(dbFieldName));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if (field.getType().isEnum()) {
                        Class fEnumType = field.getType();
                        HashSet<String> eValues = new HashSet<String>();  // enum constants

                        Enum[] eConstants = (Enum[]) fEnumType.getEnumConstants();
                        for (Enum e : eConstants) {
                            eValues.add(null);
                            eValues.add(e.toString());
                        }

                        if (eValues.contains(rs.getString(dbFieldName))) {
                            if (rs.getString(dbFieldName) == null) {
                                field.set(dataEntry, null);
                            } else {
                                field.set(dataEntry, Enum.valueOf(fEnumType, rs.getString(dbFieldName)));
                            }

                            for (TableIndex ti : tblIndices) {
                                indices.get(ti).addEntry(dataEntry, field, field.getType());
                            }
                        } else {
                            throw new InternalFrameworkException("Database Enumeration " + rs.getString(dbFieldName) + " does not exist in Enumeration "
                                    + field.getType().getName());
                        }
                    } else if (field.getType() == ArrayList.class) {
                        Logger.getLogger().write("Found Array "+field.getName()+" in " + dataClass);
                        
                        if (foreignKeys.containsKey(field.getName())) {
                            Logger.getLogger().write("Found name in fk array");
                            ForeignKey fk = foreignKeys.get(field.getName());
                            Class c = Class.forName("at.viswars.database.framework.core.LazyArrayList");
                            Constructor constructorArray = c.getConstructor(Model.class,String.class);
                            constructorArray.setAccessible(true);    
                            LazyArrayList lazyList = (LazyArrayList)constructorArray.newInstance(dataEntry,field.getName());
                            field.set(dataEntry, lazyList);
                        }
                    }

                    i++;

                    if ((i % 500) == 0) {
                        // Logger.getLogger().write("Indexed " + i + " entries!");
                    }
                }
            }

            Logger.getLogger().write("Loading finished successfully");
            stmt.close();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        } catch (NoSuchMethodException nsme) {
            nsme.printStackTrace();          
        } catch (InstantiationException ie) {
            ie.printStackTrace();
        } catch (InvocationTargetException ite) {
            ite.printStackTrace();
        } catch (InternalFrameworkException ife) {
            ife.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }

    @Override
    public ArrayList<T> getEntries(T entry) throws InternalFrameworkException {
        // Build query key set        
        QueryKeySet qks = DBCoreUtils.buildQueryKeySetFromEntry(entry, mappedFields);
        
        QueryMode qm = DBCoreUtils.getQueryMode(qks, availIndices);
        /*
        if (qm.getQueryMode() == QueryMode.QUERYMODE_DIRECT) {
            System.out.println("This querymode was sponsered by " +tableName);
        }
        */
        // Logger.getLogger().write("Choosen QueryMode: " + qm.getQueryMode() + " usedIndices: " + qm.getIndices().size() + " uncoveredFields: " + qm.getUncovered().size());

        HashSet<T> resultHash = null;

        ac.getReadLock();
        resultHash = getEntriesInternal(qm, qks);
        ArrayList<T> resultTmp = new ArrayList<T>();
        resultTmp.addAll(resultHash);
        ac.releaseLock();

        ArrayList<T> result = cloneResult(resultTmp);

        // Logger.getLogger().write("Return " + result);                
        return result;
    }

    @Override
    public ArrayList<T> getEntries(QueryKeySet qks, String indexName) throws InternalFrameworkException, InvalidIndexException {
        TableIndex ti = availIndices.get(indexName);

        // Logger.getLogger().write("TABLENAME: " + tableName + " INDEX COUNT: " + availIndices.size());

        if (ti == null) {
            throw new InvalidIndexException("Index " + indexName + " unknown");
        }
        IndexedTableData<T> tableData = indices.get(ti);

        ac.getReadLock();
        ArrayList<T> result = cloneResult(tableData.getEntry(qks));
        ac.releaseLock();

        return result;
    }

    @Override
    public T getEntry(T entry) throws PrimaryKeyInvalidException, InvalidIndexException, InternalFrameworkException {
        TableIndex ti = availIndices.get("primary");
        IndexedTableData<T> tableData = indices.get(ti);

        // Check if all primary fields was specified                
        Object[][] queryFields = DBCoreUtils.getPrimaryQueryFields(entry, ti);
        // DBCoreUtils.checkPrimaryKey(qks, availIndices);

        ac.getReadLock();
        T res = cloneResult(tableData.getEntryRaw2(queryFields));
        ac.releaseLock();

        return res;
    }

    @Override
    public T getEntry(QueryKeySet qks) throws PrimaryKeyInvalidException, InvalidIndexException, InternalFrameworkException {
        IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));

        // Check if all primary fields was specified                
        DBCoreUtils.checkPrimaryKey(qks, availIndices);

        ac.getReadLock();
        ArrayList<T> res = cloneResult(tableData.getEntry(qks));
        ac.releaseLock();

        if (res.size() == 0) {
            return null;
        } else {
            return res.get(0);
        }
    }

    @Override
    public ArrayList<T> getAllEntries() {
        TableIndex ti = availIndices.get("primary");

        IndexedTableData<T> tableData = indices.get(ti);

        ac.getReadLock();
        ArrayList<T> res = cloneResult(tableData.getAllEntries());
        ac.releaseLock();

        return res;
    }

    @Override
    public T merge(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        try {
            ac.getWriteLock();
            QueryKeySet qks = DBCoreUtils.buildQueryKeySetFromPrimary(entry, availIndices);
            DBCoreUtils.checkPrimaryKey(qks, availIndices);

            IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));
            ArrayList<T> res = tableData.getEntry(qks);

            if (res.size() == 0) {
                ac.releaseLock();
                throw new PrimaryKeyInvalidException("Primary Key not available");
            }

            T result = res.get(0);

            // Build statement and update 
            Statement stmt = DbConnect.createStatement();
            String stmtStr = "UPDATE " + tableName + " SET";
            String whereStr = " WHERE";

            // ************************************************
            // **      CHECK MESSED UP SYSTEM 0 ENTRIES      **
            // ************************************************
            if (entry instanceof PlayerFleet) {
                PlayerFleet pf = (PlayerFleet) entry;

                if ((pf.getSystemId() == 0) && (pf.getStatus() == 0)) {
                    DebugBuffer.writeStackTrace("Setting fleet to invalid position", new Exception("SYSTEM 0 ON STATUS 0 WAS SET ON FLEET " + pf.getId() + " (" + pf.getName() + ")"));
                    throw new RuntimeException("Fleet in system 0 bug");
                }
            }

            // ************************************************
            // ** END  CHECK MESSED UP SYSTEM 0 ENTRIES  END **
            // ************************************************

            try {
                boolean firstEntry = true;
                boolean firstWhereEntry = true;

                for (Field f : entry.getClass().getDeclaredFields()) {
                    f.setAccessible(true);

                    if (!fieldToDBName.containsKey(f.getName())) {
                        continue;
                    }

                    Field targetField = result.getClass().getDeclaredField(f.getName());

                    targetField.setAccessible(true);
                    if (qks.getKeyFields().contains(f.getName())) {
                        if (firstWhereEntry) {
                            firstWhereEntry = false;
                        } else {
                            whereStr += " AND";
                        }

                        if ((f.getType() == String.class) || f.getType().isEnum()) {
                            if (f.get(entry) == null) {
                                whereStr += " " + fieldToDBName.get(f.getName()) + "=null";
                            } else {
                                whereStr += " " + fieldToDBName.get(f.getName()) + "='" + DBCoreUtils.saveSQL(f.get(entry)) + "'";
                            }
                        } else {
                            whereStr += " " + fieldToDBName.get(f.getName()) + "=" + f.get(entry);
                        }

                        continue;
                    }

                    if (firstEntry) {
                        firstEntry = false;
                    } else {
                        stmtStr += ",";
                    }

                    if ((f.getType() == String.class) || f.getType().isEnum()) {
                        if (f.get(entry) == null) {
                            stmtStr += " " + fieldToDBName.get(f.getName()) + "=null";
                        } else {
                            stmtStr += " " + fieldToDBName.get(f.getName()) + "='" + DBCoreUtils.saveSQL(f.get(entry)) + "'";
                        }
                    } else {
                        stmtStr += " " + fieldToDBName.get(f.getName()) + "=" + f.get(entry);
                    }
                }

                stmt.executeUpdate(stmtStr + whereStr);
                stmt.close();
            } catch (SQLException sqle) {
                ac.releaseLock();

                try {
                    stmt.close();
                } catch (Exception e) {
                    // Logger.getLogger().write("Error in Update: " + e);
                }

                throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtStr + whereStr, sqle);
            }

            for (Field f : entry.getClass().getDeclaredFields()) {
                Field targetField = result.getClass().getDeclaredField(f.getName());

                targetField.setAccessible(true);
                if (!fieldToDBName.containsKey(f.getName())) {
                    continue;
                }
                f.setAccessible(true);

                TableIndex ti = null;

                if (targetField.get(result) != targetField.get(entry)) {
                    // If this field is an index sorting needs to be done 
                    if (indexedFields.contains(targetField.getName())) {
                        IndexAnnotation ia = (IndexAnnotation) targetField.getAnnotation(IndexAnnotation.class);
                        ti = availIndices.get(ia.indexName());

                        indices.get(ti).deleteEntry(result, f);
                    }
                }

                targetField.set(result, f.get(entry));

                if (ti != null) {
                    indices.get(ti).addEntry(result, f, f.getType());
                }
            }

            ac.releaseLock();
            return cloneResult(result);
        } catch (NoSuchFieldException nsfe) {
            ac.releaseLock();
            throw new InternalFrameworkException(nsfe.getMessage());
        } catch (IllegalAccessException iae) {
            ac.releaseLock();
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    @Override
    public ArrayList<T> mergeAll(T entry, QueryKeySet qksWhere) throws InternalFrameworkException {
        try {
            ac.getWriteLock();
            QueryKeySet qksValues = DBCoreUtils.buildQueryKeySetFromEntry(entry, mappedFields);
            DBCoreUtils.checkNotPrimary(qksValues, availIndices);
            QueryMode qm = DBCoreUtils.getQueryMode(qksWhere, availIndices);

            HashSet<T> dataToChange = getEntriesInternal(qm, qksWhere);

            // Build statement and update 
            String setStr = DBCoreUtils.QKSToSQL_SETSTR(qksValues);
            String whereStr = DBCoreUtils.QKSToSQL_WHERESTR(qksWhere);

            if (setStr.length() == 0) {
                ac.releaseLock();
                throw new InternalFrameworkException("No 'set' parameters defined!");
            }

            Statement stmt = DbConnect.createStatement();
            String stmtStr = "UPDATE " + tableName + " SET " + setStr;
            if (whereStr.length() > 0) {
                stmtStr += " WHERE " + whereStr;
            }

            // Logger.getLogger().write("Determined Update Command is " + stmtStr);
            // Logger.getLogger().write("Affected entries: " + dataToChange.size());

            try {
                stmt.executeUpdate(stmtStr);
                stmt.close();
            } catch (SQLException sqle) {
                ac.releaseLock();

                try {
                    stmt.close();
                } catch (Exception e) {
                    // Logger.getLogger().write("Error in Update: " + e);
                }

                throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtStr + whereStr, sqle);
            }

            for (Field f : entry.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                if (f.get(entry) == null) {
                    // Logger.getLogger().write("Skip field " + f.getName());
                    continue;
                }

                for (T actEntry : dataToChange) {
                    Field targetField = actEntry.getClass().getDeclaredField(f.getName());
                    targetField.setAccessible(true);

                    if (!fieldToDBName.containsKey(f.getName())) {
                        continue;
                    }

                    TableIndex ti = null;

                    if (targetField.get(actEntry) != targetField.get(entry)) {
                        // If this field is an index sorting needs to be done 
                        if (indexedFields.contains(targetField.getName())) {
                            IndexAnnotation ia = (IndexAnnotation) targetField.getAnnotation(IndexAnnotation.class);
                            ti = availIndices.get(ia.indexName());
                            indices.get(ti).deleteEntry(actEntry);
                        }
                    }

                    targetField.set(actEntry, f.get(entry));

                    if (ti != null) {
                        indices.get(ti).addEntry(actEntry, f, f.getType());
                    }
                }
            }

            ArrayList<T> result = new ArrayList<T>();
            result.addAll(dataToChange);

            ac.releaseLock();
            return cloneResult(result);
        } catch (NoSuchFieldException nsfe) {
            ac.releaseLock();
            throw new InternalFrameworkException(nsfe.getMessage());
        } catch (IllegalAccessException iae) {
            ac.releaseLock();
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    @Override
    public ArrayList<T> mergeAll(ArrayList<T> entries) throws InternalFrameworkException {
        TableIndex primary = availIndices.get("primary");
        IndexedTableData<T> tableData = indices.get(primary);

        ArrayList<String> statements = new ArrayList<String>();

        long start = System.currentTimeMillis();

        try {
            ac.getWriteLock();
            HashMap<T, T> dataToChange = new HashMap<T, T>();

            // Create prepared Statment String
            String pStmtStr1 = "";
            String pStmtStr2 = "";

            boolean firstSet = true;
            boolean firstWhere = true;

            int setBaseCount = 0;

            for (Field f : dataClass.getDeclaredFields()) {
                if (!mappedFields.contains(f.getName())) {
                    continue;
                }

                if (primaryFields.contains(f.getName())) {
                    if (!firstWhere) {
                        pStmtStr2 += " AND ";
                    }
                    firstWhere = false;
                    pStmtStr2 += fieldToDBName.get(f.getName()) + "=?";
                } else {
                    if (!firstSet) {
                        pStmtStr1 += ", ";
                    }
                    firstSet = false;
                    pStmtStr1 += fieldToDBName.get(f.getName()) + "=?";

                    setBaseCount++;
                }
            }

            try {
                DbConnect.getConnection().setAutoCommit(false);

                PreparedStatement pStmt = DbConnect.prepareStatement("UPDATE " + tableName + " SET " + pStmtStr1 + " WHERE " + pStmtStr2,false);
                // Logger.getLogger().write("Prepared Statement = UPDATE " + tableName + " SET " + pStmtStr1 + " WHERE " + pStmtStr2);
                Map<String, Object> fastKey = new HashMap<String, Object>();

                int skipped = 0;

                for (T entry : entries) {
                    // ************************************************
                    // **      CHECK MESSED UP SYSTEM 0 ENTRIES      **
                    // ************************************************
                    if (entry instanceof PlayerFleet) {
                        PlayerFleet pf = (PlayerFleet) entry;

                        if ((pf.getSystemId() == 0) && (pf.getStatus() == 0)) {
                            DebugBuffer.writeStackTrace("Setting fleet to invalid position", new Exception("SYSTEM 0 ON STATUS 0 WAS SET ON FLEET " + pf.getId() + " (" + pf.getName() + ")"));
                            throw new RuntimeException("Fleet in system 0 bug");
                        }
                    }

                    // ************************************************
                    // ** END  CHECK MESSED UP SYSTEM 0 ENTRIES  END **
                    // ************************************************     

                    if (entry instanceof DataChangeAware) {
                        if (!((DataChangeAware) entry).isModified()) {
                            skipped++;
                            continue;
                        }
                    }

                    fastKey.clear();
                    // Logger.getLogger().write("Found 2 entries to be updated!");

                    // Create SQL commands                     
                    int primaryCount = 0;
                    int setCount = 0;

                    for (Field f : entry.getClass().getDeclaredFields()) {
                        if (!mappedFields.contains(f.getName())) {
                            continue;
                        }

                        /*
                        if (f.getAnnotation(FieldMappingAnnotation.class) == null) {
                        continue;
                        }
                         */

                        f.setAccessible(true);
                        if (primaryFields.contains(f.getName())) {
                            // if (f.isAnnotationPresent(IdFieldAnnotation.class)) {
                            primaryCount++;
                            if (f.getType().isEnum()) {
                                pStmt.setObject(setBaseCount + primaryCount, f.get(entry).toString());
                            } else {
                                pStmt.setObject(setBaseCount + primaryCount, f.get(entry));
                            }
                            // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding field "+f.getName()+ " with value " + f.get(entry) + " to fastKey");
                            fastKey.put(f.getName(), f.get(entry));
                        } else {
                            setCount++;
                            if (f.getType().isEnum()) {
                                pStmt.setObject(setCount, f.get(entry).toString());
                            } else {
                                pStmt.setObject(setCount, f.get(entry));
                            }
                        }
                    }

                    pStmt.addBatch();

                    // Extract primary of each entry
                    /*
                    QueryKeySet qksPrim = DBCoreUtils.buildQueryKeySetFromPrimary(entry, availIndices);
                    DBCoreUtils.checkPrimaryKey(qksPrim, availIndices);
                     */
                    T res = null;

                    try {
                        res = (T) tableData.getEntryFast(fastKey).iterator().next();
                        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Retrieved Entry "+res);
                    } catch (NoSuchElementException nsee) {
                        ac.releaseLock();
                        nsee.printStackTrace();
                        
                        try {
                            DbConnect.getConnection().setAutoCommit(true);
                        } catch (SQLException e) {
                        }                        
                        
                        DebugBuffer.error("--- START --- Debugging fastkey for table " + tableName);
                        for (Map.Entry<String, Object> key : fastKey.entrySet()) {
                            DebugBuffer.error("Key field: " + key.getKey() + " value: " + key.getValue().toString());
                        }                        
                        DebugBuffer.error("--- END ---");
                        
                        throw new InternalFrameworkException(nsee.getMessage() + " PARAMS: " + fastKey.toString());
                    }

                    dataToChange.put(res, entry);
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Size of DataToChange "+dataToChange.size());
                }

                Logger.getLogger().write("[" + tableName + "] Mass Update of " + entries.size() + " entries (Skipped: " + skipped + ")");

                // Change data in DB            
                long start2 = System.currentTimeMillis();

                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Executing Update Batch in "+tableName+": " + DbConnect.getConnection().getAutoCommit());
                int[] returned = pStmt.executeBatch();

                for (int i = 0; i < returned.length; i++) {
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN, " Result " + i + " was " + returned[i]);
                }

                Statement stmt = DbConnect.createStatement();
                stmt.execute("COMMIT");
                stmt.close();

                DbConnect.getConnection().setAutoCommit(true);

                pStmt.close();
                /*
                DbConnect.getConnection().setAutoCommit(false);
                //Statement update = DbConnect.createStatement();
                
                for (String stmtStr : statements) {
                update.addBatch(stmtStr);
                }                

                update.executeBatch();                
                update.execute("COMMIT");
                

                DbConnect.getConnection().setAutoCommit(true);
                 */

                long end2 = System.currentTimeMillis();
                // Logger.getLogger().write("Time for updating table " + (end2 - start2) + "ms");
            } catch (SQLException sqle) {
                ac.releaseLock();
                sqle.printStackTrace();

                try {
                    DbConnect.getConnection().setAutoCommit(true);
                } catch (SQLException e) {
                }

                throw new InternalFrameworkException(sqle.getMessage(), sqle);
            }

            long end = System.currentTimeMillis();
            // Logger.getLogger().write("Time for SQL processing " + (end - start) + "ms");

            // Change data in buffer
            // Clean scondary indices
            // HashSet<T> toClean = new HashSet<T>();
            // toClean.addAll(dataToChange.keySet());            

            /*
            HashSet<String> excludeIndexCheck = new HashSet<String>();
            ArrayList<TableIndex> refreshIndices = new ArrayList<TableIndex>();
             */
            start = System.currentTimeMillis();

            for (Map.Entry<T, T> changeEntry : dataToChange.entrySet()) {
                T base = changeEntry.getKey();
                T newBase = changeEntry.getValue();

                for (Field f : newBase.getClass().getDeclaredFields()) {
                    if ((!mappedFields.contains(f.getName()))
                            || (primaryFields.contains(f.getName()))) {
                        // if ((f.getAnnotation(FieldMappingAnnotation.class) == null) ||
                        //        (f.isAnnotationPresent(IdFieldAnnotation.class))) {
                        continue;
                    }

                    String fName = f.getName();

                    f.setAccessible(true);
                    Field fOrg = base.getClass().getDeclaredField(fName);
                    fOrg.setAccessible(true);

                    Object newValue = f.get(newBase);
                    Object baseValue = fOrg.get(base);

                    if (!((baseValue == null) && (newValue == null))) {
                        if ((baseValue == null && newValue != null) || (!(baseValue.equals(newValue)))) {
                            if (indexedFields.contains(f.getName())) {
                                // if (f.getAnnotation(IndexAnnotation.class) != null) {
                                // TODO Inperformant query on Annotation!!
                                IndexAnnotation ia = (IndexAnnotation) f.getAnnotation(IndexAnnotation.class);
                                indices.get(availIndices.get(ia.indexName())).moveEntry(fName, base, baseValue, newValue);
                                /*
                                indices.get(availIndices.get(ia.indexName())).deleteEntry(base);
                                refreshIndices.add(availIndices.get(ia.indexName()));
                                excludeIndexCheck.add(fOrg.getName());
                                 */
                            }

                            fOrg.set(base, newValue);
                        }
                    }
                }
            }

            end = System.currentTimeMillis();

            // Logger.getLogger().write("Time for buffer update " + (end - start) + "ms");

            // Clear indices
            /*
            HashSet<T> data = new HashSet<T>();
            data.addAll(dataToChange.keySet());
            
            for (TableIndex ti : refreshIndices) {
            tableData = indices.get(ti);
            for (TableIndexField tif : ti.getTableIndexFields()) {
            tableData.addEntries(data, dataClass.getDeclaredField(tif.getFieldName()), tif.getDataType());                    
            }
            }
             */
            ArrayList<T> changedData = new ArrayList<T>();
            changedData.addAll(dataToChange.keySet());
            ac.releaseLock();
            return cloneResult(changedData);
            /*
            } catch (PrimaryKeyInvalidException pkie) {
            throw new InternalFrameworkException(pkie.getMessage());
            } catch (InvalidIndexException iie) {
            throw new InternalFrameworkException(iie.getMessage());
             */
        } catch (IllegalAccessException iae) {
            ac.releaseLock();
            throw new InternalFrameworkException(iae.getMessage());
        } catch (NoSuchFieldException nsfe) {
            ac.releaseLock();
            nsfe.printStackTrace();
            throw new InternalFrameworkException(nsfe.getMessage());
        }
    }

    @Override
    public ArrayList<T> updateOrInsert(ArrayList<T> entries) throws InternalFrameworkException {
        throw new InternalFrameworkException("Not implemented");
    }

    @Override
    public T insert(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        // ************************************************
        // **      CHECK MESSED UP SYSTEM 0 ENTRIES      **
        // ************************************************
        if (entry instanceof PlayerFleet) {
            PlayerFleet pf = (PlayerFleet) entry;

            if ((pf.getSystemId() == 0) && (pf.getStatus() == 0)) {
                DebugBuffer.writeStackTrace("Setting fleet to invalid position", new Exception("SYSTEM 0 ON STATUS 0 WAS SET ON FLEET " + pf.getId() + " (" + pf.getName() + ")"));
                throw new RuntimeException("Fleet in system 0 bug");
            }
        }

        // ************************************************
        // ** END  CHECK MESSED UP SYSTEM 0 ENTRIES  END **
        // ************************************************             
        
        try {
            ac.getWriteLock();
            String stmtStr = "INSERT INTO " + tableName + " "
                    + DBCoreUtils.EntryToSQL_INSERTSTR(entry, defaultValues, fieldToDBName);
            Statement stmt = DbConnect.createStatement();

            Integer generatedKey = null;

            try {
                stmt.execute(stmtStr, Statement.RETURN_GENERATED_KEYS);

                ResultSet keys = stmt.getGeneratedKeys();
                if (keys.next()) {
                    generatedKey = keys.getInt(1);
                    // Logger.getLogger().write("Generated Key 1 = " + generatedKey);
                }

                stmt.close();
            } catch (SQLException sqle) {
                ac.releaseLock();

                try {
                    stmt.close();
                } catch (Exception e) {
                }

                throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtStr, sqle);
            }

            T newEntry = cloneResult(entry);

            Collection<TableIndex> tblIndices = availIndices.values();
            for (Field f : entry.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                String fieldName = f.getName();

                if (!mappedFields.contains(fieldName)) {
                    // Logger.getLogger().write("Continuing field : " + f.getName() + " Reason : No FieldMappingAnnotation");
                    continue;
                } else {
                    //  // Logger.getLogger().write("Not Continuing field : " + f.getAnnotation(FieldMappingAnnotation.class) + " annotation :" + f.getAnnotation(FieldMappingAnnotation.class).value());
                }

                if ((f.get(entry) == null) && !(indexedFields.contains(fieldName) || primaryFields.contains(fieldName))) {
                    continue;
                }

                // Logger.getLogger().write("Field name: " + f.getName() + " Annoation: " + f.getAnnotation(IdFieldAnnotation.class) + " Generated Key: " + generatedKey);
                if (primaryFields.contains(fieldName) && (generatedKey != null)) {
                    // Logger.getLogger().write("Generated Key = " + generatedKey + " ("+tableName+")");
                    f.set(newEntry, generatedKey);
                    f.set(entry, generatedKey);
                }

                if (!(indexedFields.contains(fieldName) || primaryFields.contains(fieldName))) {
                    continue;
                }
                for (TableIndex ti : tblIndices) {
                    // Logger.getLogger().write("Add new Entry into buffer for field " + f.getName() + " with type " + f.getType() + " and value " + f.get(newEntry));
                    indices.get(ti).addEntry(newEntry, f, f.getType());
                }
            }

            ac.releaseLock();
            return cloneResult(newEntry);           
        } catch (IllegalAccessException iae) {
            ac.releaseLock();
            throw new InternalFrameworkException(iae.getMessage());
        } catch (Exception e) {
            ac.releaseLock();
            throw new InternalFrameworkException("UNEXPECTED ERROR: " + e.getMessage());            
        }
    }

    @Override
    public ArrayList<T> insertAll(ArrayList<T> entries) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        try {
            ac.getWriteLock();
            ArrayList<Integer> generatedKeys = new ArrayList<Integer>();

            // Generate Prepared Statement String
            String fieldStr = "";
            String valueStr = "";

            boolean firstField = true;
            boolean firstValue = true;

            for (Field f : dataClass.getDeclaredFields()) {
                if (!mappedFields.contains(f.getName())) {
                    continue;
                }

                ColumnProperties cpAnnotation = f.getAnnotation(ColumnProperties.class);
                if (cpAnnotation != null) {
                    if (cpAnnotation.autoIncrement()) {
                        continue;
                    }
                }

                if (!firstField) {
                    fieldStr += ", ";
                }
                firstField = false;
                fieldStr += fieldToDBName.get(f.getName());

                if (!firstValue) {
                    valueStr += ", ";
                }
                firstValue = false;
                valueStr += "?";
            }

            String pStmtStr = "INSERT INTO " + tableName + " (" + fieldStr + ") VALUES (" + valueStr + ")";
            // DebugBuffer.warning("Inserting " + entries.size() + " entries into table " + this.tableName);

            try {
                DbConnect.getConnection().setAutoCommit(false);

                PreparedStatement pStmt = DbConnect.prepareStatement(pStmtStr, true);

                for (T entry : entries) {
                    // ************************************************
                    // **      CHECK MESSED UP SYSTEM 0 ENTRIES      **
                    // ************************************************
                    if (entry instanceof PlayerFleet) {
                        PlayerFleet pf = (PlayerFleet) entry;

                        if ((pf.getSystemId() == 0) && (pf.getStatus() == 0)) {
                            DebugBuffer.writeStackTrace("Setting fleet to invalid position", new Exception("SYSTEM 0 ON STATUS 0 WAS SET ON FLEET " + pf.getId() + " (" + pf.getName() + ")"));
                            throw new RuntimeException("Fleet in system 0 bug");
                        }
                    }

                    // ************************************************
                    // ** END  CHECK MESSED UP SYSTEM 0 ENTRIES  END **
                    // ************************************************                         
                    
                    int fldCount = 0;

                    for (Field f : entry.getClass().getDeclaredFields()) {
                        if (!mappedFields.contains(f.getName())) {
                            continue;
                        }

                        ColumnProperties cpAnnotation = f.getAnnotation(ColumnProperties.class);
                        if (cpAnnotation != null) {
                            if (cpAnnotation.autoIncrement()) {
                                continue;
                            }
                        }

                        fldCount++;

                        f.setAccessible(true);
                        if (f.get(entry) == null) {
                            String fieldName = f.getAnnotation(FieldMappingAnnotation.class).value();

                            if (defaultValues.get(fieldName) != null) {
                                f.set(entry, DBCoreUtils.convertValue(f, defaultValues.get(fieldName)));
                            }
                        }

                        if (f.getType().isEnum()) {
                            pStmt.setObject(fldCount, f.get(entry).toString());
                        } else {
                            pStmt.setObject(fldCount, f.get(entry));
                        }
                    }

                    pStmt.addBatch();
                }

                pStmt.executeBatch();

                ResultSet keys = pStmt.getGeneratedKeys();
                while (keys.next()) {
                    // DebugBuffer.warning("Adding key " + keys.getInt(1));
                    generatedKeys.add(keys.getInt(1));
                }

                Statement stmt = DbConnect.createStatement();
                stmt.execute("COMMIT");
                DbConnect.getConnection().setAutoCommit(true);

                stmt.close();
                pStmt.close();
            } catch (SQLException sqle) {
                ac.releaseLock();

                try {
                    DbConnect.getConnection().setAutoCommit(true);
                } catch (SQLException e) {
                }

                Logger.getLogger().write(LogLevel.ERROR, pStmtStr);
                throw new InternalFrameworkException(sqle.getMessage(), sqle);
            }

            ArrayList<T> newEntries = cloneResult(entries);
            Collection<TableIndex> tblIndices = availIndices.values();

            int counter = 0;

            for (T entry : newEntries) {
                for (Field f : entry.getClass().getDeclaredFields()) {
                    f.setAccessible(true);
                    if (!fieldToDBName.containsKey(f.getName())) {
                        continue;
                    }
                    if ((f.get(entry) == null) && !primaryFields.contains(f.getName())) {
                        continue;
                    }

                    // // Logger.getLogger().write("Field name: " + f.getName() + " Annoation: " + f.getAnnotation(IdFieldAnnotation.class) + " Generated Key: " + generatedKey);
                    if (primaryFields.contains(f.getName()) && (generatedKeys.size() > 0)) {
                        f.set(entry, generatedKeys.get(counter));
                    }

                    for (TableIndex ti : tblIndices) {
                        indices.get(ti).addEntry(entry, f, f.getType());
                    }
                }

                counter++;
            }

            // QueryKeySet qks2 = DBCoreUtils.buildQueryKeySetFromPrimary(entry,availIndices);
            // IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));
            // Check if all primary fields was specified                
            // DBCoreUtils.checkPrimaryKey(qks2,availIndices);
            // ArrayList<T> res = tableData.getEntry(qks2);

            ac.releaseLock();
            return cloneResult(newEntries);
        } catch (IllegalAccessException iae) {
            ac.releaseLock();
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    @Override
    public void delete(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        QueryKeySet qks = DBCoreUtils.buildQueryKeySetFromPrimary(entry, availIndices);

        IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));

        // Check if all primary fields was specified                
        DBCoreUtils.checkPrimaryKey(qks, availIndices);

        ac.getWriteLock();
        ArrayList<T> res = tableData.getEntry(qks);

        if (res.size() == 0) {
            ac.releaseLock();
            throw new PrimaryKeyInvalidException("Primary Key not available");
        }
        T result = res.get(0);

        // // Logger.getLogger().write("RESULT FOUND?: " + result);

        Statement stmt = DbConnect.createStatement();
        String stmtStr = "DELETE FROM " + this.tableName + " WHERE ";

        try {
            stmtStr += DBCoreUtils.EntryToSQL_DELETESTR(entry, fieldToDBName, primaryFields);
            stmt.execute(stmtStr);
        } catch (IllegalAccessException iae) {
            ac.releaseLock();
            throw new InternalFrameworkException(iae.getMessage() + " >> Statement: " + stmtStr);
        } catch (SQLException sqle) {
            ac.releaseLock();
            throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtStr, sqle);
        }

        for (Map.Entry<TableIndex, IndexedTableData<T>> idxSet : indices.entrySet()) {
            TableIndex tblIdx = idxSet.getKey();
            IndexedTableData itd = idxSet.getValue();

            //// Logger.getLogger().write("Cleaning up Index " + tblIdx.getName());
            itd.deleteEntry(result);
        }

        ac.releaseLock();
    }

    public int deleteAll(T entry) throws InternalFrameworkException {
        QueryKeySet qks = DBCoreUtils.buildQueryKeySetFromEntry(entry, mappedFields);
        QueryMode qm = DBCoreUtils.getQueryMode(qks, availIndices);
        // Logger.getLogger().write("Choosen QueryMode: " + qm.getQueryMode() + " usedIndices: " + qm.getIndices().size() + " uncoveredFields: " + qm.getUncovered().size());
        ac.getWriteLock();
        HashSet<T> result = getEntriesInternal(qm, qks);
        String stmtString = "DELETE FROM " + tableName + " WHERE " + DBCoreUtils.QKSToSQL_DELETESTR(qks);

        try {
            Statement stmt = DbConnect.createStatement();
            stmt.execute(stmtString);
        } catch (SQLException sqle) {
            ac.releaseLock();
            throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtString, sqle);
        }

        for (TableIndex ti : availIndices.values()) {
            indices.get(ti).deleteAll(result);
        }

        ac.releaseLock();
        return result.size();
    }

    /*
     * Deletes a specific set of entries in the table, this is expensive, if possible use 
     * deleteAll(T entry) and specify the most common values which specify the entries which 
     * should be deleted!
     */
    public int deleteAll(ArrayList<T> entries) throws InternalFrameworkException, PrimaryKeyInvalidException {
        TableIndex primary = availIndices.get("primary");
        IndexedTableData<T> tableData = indices.get(primary);
        HashSet<T> toDelete = new HashSet<T>();

        ac.getWriteLock();

        try {
            StringBuilder condString = new StringBuilder();
            boolean firstWhere = true;

            int setBaseCount = 0;

            for (Field f : dataClass.getDeclaredFields()) {
                if (!mappedFields.contains(f.getName())) {
                    continue;
                }

                if (primaryFields.contains(f.getName())) {
                    if (!firstWhere) {
                        condString.append(" AND ");
                    }
                    firstWhere = false;
                    condString.append(fieldToDBName.get(f.getName()) + "=?");
                }
            }

            try {
                DbConnect.getConnection().setAutoCommit(false);

                PreparedStatement pStmt = DbConnect.prepareStatement("DELETE FROM " + tableName + " WHERE " + condString,false);
                // Logger.getLogger().write("Prepared Statement = UPDATE " + tableName + " SET " + pStmtStr1 + " WHERE " + pStmtStr2);
                Map<String, Object> fastKey = new HashMap<String, Object>();

                for (T entry : entries) {
                    fastKey.clear();
                    // Logger.getLogger().write("Found 2 entries to be updated!");

                    // Create SQL commands                     
                    int primaryCount = 0;

                    for (Field f : entry.getClass().getDeclaredFields()) {
                        if (!mappedFields.contains(f.getName())) {
                            continue;
                        }

                        f.setAccessible(true);
                        if (primaryFields.contains(f.getName())) {
                            // if (f.isAnnotationPresent(IdFieldAnnotation.class)) {
                            primaryCount++;
                            if (f.getType().isEnum()) {
                                pStmt.setObject(setBaseCount + primaryCount, f.get(entry).toString());
                            } else {
                                pStmt.setObject(setBaseCount + primaryCount, f.get(entry));
                            }
                            // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding field "+f.getName()+ " with value " + f.get(entry) + " to fastKey");
                            fastKey.put(f.getName(), f.get(entry));
                        }
                    }

                    pStmt.addBatch();

                    T res = null;

                    try {
                        res = (T) tableData.getEntryFast(fastKey).iterator().next();
                        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Retrieved Entry "+res);
                    } catch (NoSuchElementException nsee) {
                        ac.releaseLock();
                        nsee.printStackTrace();
                        throw new InternalFrameworkException(nsee.getMessage() + " PARAMS: " + fastKey.toString());
                    }

                    toDelete.add(res);
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Size of DataToChange "+dataToChange.size());
                }

                // Change data in DB            
                long start2 = System.currentTimeMillis();

                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Executing Update Batch in "+tableName+": " + DbConnect.getConnection().getAutoCommit());
                int[] returned = pStmt.executeBatch();

                Statement stmt = DbConnect.createStatement();
                stmt.execute("COMMIT");
                stmt.close();

                DbConnect.getConnection().setAutoCommit(true);

                pStmt.close();
                long end2 = System.currentTimeMillis();
                // Logger.getLogger().write("Time for updating table " + (end2 - start2) + "ms");
            } catch (SQLException sqle) {
                ac.releaseLock();
                sqle.printStackTrace();

                try {
                    DbConnect.getConnection().setAutoCommit(true);
                } catch (SQLException e) {
                }

                throw new InternalFrameworkException(sqle.getMessage(), sqle);
            }

            long end = System.currentTimeMillis();

            long start = System.currentTimeMillis();

            for (TableIndex ti : availIndices.values()) {
                indices.get(ti).deleteAll(toDelete);
            }

            end = System.currentTimeMillis();

            ac.releaseLock();
            return toDelete.size();
        } catch (IllegalAccessException iae) {
            ac.releaseLock();
        }

        ac.releaseLock();
        return -1;
    }

    @Override
    public int deleteAll(QueryKeySet qks) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        QueryMode qm = DBCoreUtils.getQueryMode(qks, availIndices);
        // Logger.getLogger().write("Choosen QueryMode: " + qm.getQueryMode() + " usedIndices: " + qm.getIndices().size() + " uncoveredFields: " + qm.getUncovered().size());
        ac.getWriteLock();
        HashSet<T> result = getEntriesInternal(qm, qks);
        String stmtString = "DELETE FROM " + tableName + " WHERE " + DBCoreUtils.QKSToSQL_DELETESTR(qks);

        try {
            Statement stmt = DbConnect.createStatement();
            stmt.execute(stmtString);
        } catch (SQLException sqle) {
            ac.releaseLock();
            throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtString, sqle);
        }

        for (TableIndex ti : availIndices.values()) {
            indices.get(ti).deleteAll(result);
        }

        ac.releaseLock();
        return result.size();
    }

    @Override
    public int deleteAll() throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        HashSet<T> result = indices.get(availIndices.get("primary")).getAllEntriesRaw();

        try {
            Statement stmt = DbConnect.createStatement();
            stmt.execute("TRUNCATE TABLE " + tableName);
        } catch (SQLException sqle) {
            throw new InternalFrameworkException(sqle.getMessage(), sqle);
        }

        ac.getWriteLock();
        for (TableIndex ti : availIndices.values()) {
            indices.get(ti).deleteAll(result);
        }
        ac.releaseLock();

        return result.size();
    }

    // ----------------------------------------------------------------------------------
    //                               TRANSACTION STUFF
    // ----------------------------------------------------------------------------------    
    public ArrayList<T> getEntries(ArrayList<T> entries) throws PrimaryKeyInvalidException, InvalidIndexException, InternalFrameworkException {
        ArrayList<T> affectedEntries = new ArrayList<T>();

        for (T entry : entries) {
            affectedEntries.add(this.getEntry(entry));
        }

        return affectedEntries;
    }

    // ----------------------------------------------------------------------------------
    //                               INTERNAL METHODS
    // ----------------------------------------------------------------------------------
    private void cleanIndices(ArrayList<TableIndex> indicesToClear, HashSet<T> entries) {
        for (TableIndex ti : indicesToClear) {
            // Logger.getLogger().write("Clear index " + ti.getName() + " for " + entries.size() + " entries");
            IndexedTableData<T> dataTable = indices.get(ti);

            dataTable.deleteAll(entries);
        }
    }

    private HashSet<T> getEntriesInternal(QueryMode qm, QueryKeySet qks) throws InternalFrameworkException {
        HashSet<T> result = new HashSet<T>();

        switch (qm.getQueryMode()) {
            case (QUERYMODE_INDEXED): // Optimal Access by one Index
                try {
                    IndexedTableData<T> tableData = indices.get(qm.getIndices().iterator().next());
                    // IndexedTableData<T> tableData = indices.get(((TableIndex[]) qm.getIndices().toArray())[0]);
                    result = tableData.getEntryRaw(qks);
                } catch (InvalidIndexException iie) {
                    throw new InternalFrameworkException("Determined index was invalid (" + iie.getMessage() + ")");
                }
                break;
            case (QUERYMODE_INDEXED_MERGED): // Second best Access by serveral Indices
                HashSet<T> tmpResult = null;

                try {
                    for (TableIndex ti : qm.getIndices()) {
                        IndexedTableData<T> tableData = indices.get(ti);
                        if (tmpResult == null) {
                            tmpResult = tableData.getEntryRaw(qks);
                        } else {
                            tmpResult.retainAll(tableData.getEntryRaw(qks));
                        }
                    }
                } catch (InvalidIndexException iie) {
                    throw new InternalFrameworkException("Determined index was invalid (" + iie.getMessage() + ")");
                }

                result.addAll(tmpResult);
                break;
            case (QUERYMODE_INDEXED_PARTIAL): // Third best Access by partially Indexed result
            case (QUERYMODE_DIRECT):
                try {
                    tmpResult = null;

                    for (TableIndex ti : qm.getIndices()) {
                        IndexedTableData<T> tableData = indices.get(ti);
                        if (tmpResult == null) {
                            tmpResult = tableData.getEntryRaw(qks);
                        } else {
                            tmpResult.retainAll(tableData.getEntryRaw(qks));
                        }
                    }

                    if (tmpResult != null) {
                        result = tmpResult;
                    } else {
                        IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));
                        result = tableData.getAllEntriesRaw();
                    }

                    HashSet<T> toDelete = new HashSet<T>();

                    for (T resEntry : result) {
                        for (Map.Entry<String, Object> condField : qm.getUncovered().entrySet()) {
                            // Logger.getLogger().write("Check field " + condField.getKey() + " with Value " + condField.getValue());

                            Field f = dataClass.getDeclaredField(condField.getKey());
                            f.setAccessible(true);

                            // Logger.getLogger().write("Compare " + f.get(resEntry) + " to " + condField.getValue());
                            if (DatabaseHelper.compareFields(f, resEntry, condField.getValue()) != 0) {
                                // // Logger.getLogger().write("REMOVE");
                                toDelete.add(resEntry);
                            } else {
                                // // Logger.getLogger().write("KEEP");
                            }
                        }
                    }

                    // Logger.getLogger().write("RESULT SIZE = " + result.size() + " TODELETE SIZE = " + toDelete.size());
                    result.removeAll(toDelete);
                } catch (InvalidIndexException iie) {
                    iie.printStackTrace();
                } catch (NoSuchFieldException nsfe) {
                    nsfe.printStackTrace();
                    /*
                    } catch (IllegalAccessException iae) {
                    iae.printStackTrace();
                     */
                }
                break;
        }

        return result;
    }

    private T cloneResult(T in) {
        if (in == null) {
            return null;
        }

        try {
            Constructor<T> constructor = dataClass.getConstructor();
            constructor.setAccessible(true);
            T out = constructor.newInstance();

            Class<T> clazz = (Class<T>) in.getClass();
            Method mtd = clazz.getMethod("clone", new Class[0]);
            T clone = (T) mtd.invoke(in, new Object[0]);
            out = clone;

            return out;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private ArrayList<T> cloneResult(ArrayList<T> in) {
        ArrayList<T> out = new ArrayList<T>();

        try {
            for (T item : in) {
                Class<T> clazz = (Class<T>) item.getClass();
                Method mtd = clazz.getMethod("clone", new Class[0]);
                T clone = (T) mtd.invoke(item, new Object[0]);
                out.add(clone);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return out;
    }

    @Override
    public Exception getWriteLock() {
        Logger.getLogger().write(LogLevel.DEBUG, "Got called for WriteLock " + tableName);
        return ac.getLocks();
    }
}
