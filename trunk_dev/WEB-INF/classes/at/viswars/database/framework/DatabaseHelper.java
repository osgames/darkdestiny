/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework;

import at.viswars.database.framework.exception.InternalFrameworkException;
import java.lang.reflect.Field;

/**
 *
 * @author Stefan
 */
public class DatabaseHelper {
    public static int compareFields(Field f, Object dataEntry, Object compareValue) throws InternalFrameworkException {
        try {
            if (f.get(dataEntry) == null) {
                if (compareValue == null) {
                    return 0;
                } else {
                    return 1;
                }
            }

            if ((f.getType() == Integer.class) ||
                    (f.getType() == int.class)) {
                    Integer value1 = (Integer)f.get(dataEntry);
                    Integer value2 = (Integer)compareValue;
                return value1.compareTo(value2);
            } else if ((f.getType() == Double.class) ||
                    (f.getType() == double.class)) {
                    Double value1 = (Double)f.get(dataEntry);
                    Double value2 = (Double)compareValue;
                return value1.compareTo(value2);                
            } else if ((f.getType() == Float.class) ||
                    (f.getType() == float.class)) {
                    Float value1 = (Float)f.get(dataEntry);
                    Float value2 = (Float)compareValue;
                return value1.compareTo(value2);
            } else if ((f.getType() == Long.class) ||
                    (f.getType() == long.class)) {
                    Long value1 = (Long)f.get(dataEntry);
                    Long value2 = (Long)compareValue;
                return value1.compareTo(value2);
            } else if (f.getType() == String.class) {
                return f.get(dataEntry).toString().compareTo(compareValue.toString());
            } else if ((f.getType() == Boolean.class) ||
                    (f.getType() == boolean.class)) {
                return (f.get(dataEntry) == compareValue) ? 0 : 1;
            } else if (f.getType().isEnum()) {
                return (f.get(dataEntry).toString().compareTo(compareValue.toString()));
            } else {
                throw new InternalFrameworkException("Invalid datatype encountered! (Type found: "+f.getType()+")");
            }            
        } catch (Exception e) {
            throw new InternalFrameworkException("Compare failed! [Fieldname: "+f.getName()+" Value: "+compareValue+"] (Reason: " + e.getMessage() + ")");
        }
    }
}
