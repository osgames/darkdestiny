/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class QueryKeySet {
    private ArrayList<QueryKey> keys = new ArrayList<QueryKey>();
    private ArrayList<OrderKey> orderKeys = new ArrayList<OrderKey>();
    private HashSet<String> keyFields = new HashSet<String>();
    
    public QueryKeySet() {
        
    }
    
    public void addKey(QueryKey qk) {
        keyFields.add(qk.getFieldName());
        getKeys().add(qk);
    }

    public ArrayList<QueryKey> getKeys() {
        return keys;
    }

    public void addOrderKey(OrderKey ok) {
        getOrderKeys().add(ok);       
    }

    public ArrayList<OrderKey> getOrderKeys() {
        return orderKeys;
    }
    
    public HashSet<String> getKeyFields() {
        return keyFields;
    }
    
    public String getHashBase() {
       String hashStr = "";
       
       for (QueryKey qk : keys) {
           hashStr += qk.getFieldName();
           hashStr += qk.getFieldValue();
       }
       
       return hashStr;
    }
    
    @Override
    public int hashCode() {
       int hashCode = 0;
        
        // Build hasCode from QueryKey 
       String hashStr = "";
       
       for (QueryKey qk : keys) {
           hashStr += qk.getFieldName();
           hashStr += qk.getFieldValue();
       }

       return hashStr.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final QueryKeySet other = (QueryKeySet)obj;

        if (other.hashCode() != this.hashCode()) {
            return false;
        }
        
        return true;
    }
}
