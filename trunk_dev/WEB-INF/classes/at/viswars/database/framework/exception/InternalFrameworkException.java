/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework.exception;

/**
 *
 * @author Stefan
 */
public class InternalFrameworkException extends Exception {
    private final Exception underlyingException;

    public InternalFrameworkException(String msg, Exception underlyingException) {
        super(msg);
        this.underlyingException = underlyingException;
    }

    public InternalFrameworkException(String msg) {
        super(msg);
        this.underlyingException = null;
    }

    /**
     * @return the underlyingException
     */
    public Exception getUnderlyingException() {
        return underlyingException;
    }
}
