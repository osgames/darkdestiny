/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework.exception;

/**
 *
 * @author Stefan
 */
public class InvalidIndexException extends Exception {
    public InvalidIndexException(String msg) {
        super(msg);
    }
}
