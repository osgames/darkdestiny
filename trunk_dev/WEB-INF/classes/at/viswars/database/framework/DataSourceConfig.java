/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework;

import at.viswars.database.framework.core.DatabaseTableBuffered;
import at.viswars.database.framework.core.DatabaseTablePrimitive;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 *
 * @author Stefan
 */
public class DataSourceConfig {    
    private static DataSourceConfig instance;
    
    private HashMap<String,String> tableType = new HashMap<String,String>();
    
    public static DataSourceConfig getInstance() {
        if (instance == null) {
            instance = new DataSourceConfig();
        }
        return instance;
    }    
    
    private DataSourceConfig() {
        ResourceBundle rb = ResourceBundle.getBundle("tableconfig");

        for (String key : rb.keySet()) {
            TableType tt = TableType.valueOf(rb.getString(key));
            
            if (tt == TableType.SOURCE_BUFFERED) {
                tableType.put(key, DatabaseTableBuffered.class.getName());    
            } else if (tt == TableType.SOURCE_DATABASE) {
                tableType.put(key, DatabaseTablePrimitive.class.getName()); 
            }          
        }   
    }
    
    public String getClassForModel(Class m) {
        String mName = m.getName().replace("at.viswars.model.", "");        
        return tableType.get(mName);
    }
}
