/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.database.framework.test;

import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.TestTableDAO;
import at.viswars.database.framework.QueryKey;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.model.TestTable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class TestDBFramework {

    private static TestTableDAO ttDAO = (TestTableDAO) DAOFactory.get(TestTableDAO.class);
    private static HashMap<Integer, Integer> field2 = new HashMap<Integer, Integer>();
    private static HashMap<Integer, Integer> field3 = new HashMap<Integer, Integer>();
    private static HashMap<Integer, Integer> field4 = new HashMap<Integer, Integer>();

    public static void startTest() {
        Logger.getLogger().write("START FRAMEWORK TEST");

        generateTestData();
        prepareReferenceData();
        testUpdate();
        checkIntegrity();
        insertDelete();
        checkIntegrity();
        clearTestData();
        checkNoDataLeft();
    }

    private static void generateTestData() {
        Logger.getLogger().write("GENERATE TEST DATA");
        ArrayList<TestTable> testData = new ArrayList<TestTable>();

        for (int i = 0; i < 10000; i++) {
            TestTable tt = new TestTable();
            tt.setField2((double) (i % 4));
            tt.setField3(i % 8);
            tt.setField4(i % 7);

            testData.add(tt);
        }

        long startTime = System.currentTimeMillis();
        ttDAO.insertAll(testData);
        long endTime = System.currentTimeMillis();
        Logger.getLogger().write("Generating " + testData.size() + " entries took " + (endTime - startTime) + "ms (" + ((int) (((double) (endTime - startTime) / (double) testData.size()) * 100d) / 100d) + "ms per Entry)");
    }

    private static void checkIntegrity() {
        boolean allChecksOk = true;

        for (int i = 0; i < 3; i++) {
            TestTable tt = new TestTable();
            tt.setField2((double) i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            if (field2.get(i) != results.size()) {
                allChecksOk = false;
            }
        }

        for (int i = 0; i < 7; i++) {
            TestTable tt = new TestTable();
            tt.setField3(i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            if (field3.get(i) != results.size()) {
                allChecksOk = false;
            }
        }

        for (int i = 0; i < 6; i++) {
            TestTable tt = new TestTable();
            tt.setField4(i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            if (field4.get(i) != results.size()) {
                allChecksOk = false;
            }
        }

        String resultStr = allChecksOk ? "SUCCESS" : "FAILED";
        Logger.getLogger().write("INTEGRITY CHECK: " + resultStr);
    }

    private static void prepareReferenceData() {
        for (int i = 0; i < 3; i++) {
            TestTable tt = new TestTable();
            tt.setField2((double) i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            Logger.getLogger().write("Found " + results.size() + " entries with field2=" + i + " in " + (endTime - startTime) + "ms");
            field2.put(i, results.size());
        }

        for (int i = 0; i < 7; i++) {
            TestTable tt = new TestTable();
            tt.setField3(i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            Logger.getLogger().write("Found " + results.size() + " entries with field3=" + i + " in " + (endTime - startTime) + "ms");
            field3.put(i, results.size());
        }

        for (int i = 0; i < 6; i++) {
            TestTable tt = new TestTable();
            tt.setField4(i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            Logger.getLogger().write("Found " + results.size() + " entries with field4=" + i + " in " + (endTime - startTime) + "ms");
            field4.put(i, results.size());
        }
    }

    private static void testUpdate() {
        TestTable ttSel = new TestTable();
        ttSel.setField2(3d);
        ttSel.setField3(7);

        ArrayList<TestTable> resultOrg = ttDAO.find(ttSel);
        Logger.getLogger().write("Found " + resultOrg.size() + " entries for field2=3 and field3=7");
        Logger.getLogger().write("Starting to update field4 in this entries to 999 ...");

        TestTable tt = new TestTable();
        tt.setField4(999);

        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("field2", 3d));
        qks.addKey(new QueryKey("field3", 7));

        long startTime = System.currentTimeMillis();
        ArrayList<TestTable> updResult = ttDAO.updateAll(tt, qks);
        long endTime = System.currentTimeMillis();
        Logger.getLogger().write("Update of " + updResult.size() + " entries took " + (endTime - startTime) + "ms (" + ((int) (((double) (endTime - startTime) / (double) updResult.size()) * 100d) / 100d) + "ms per Entry)");

        ttSel = new TestTable();
        ttSel.setField4(999);

        ArrayList<TestTable> result = ttDAO.find(ttSel);
        Logger.getLogger().write("Found " + result.size() + " entries for field4=999");

        Logger.getLogger().write("Restore data");
        startTime = System.currentTimeMillis();
        updResult = ttDAO.updateAll(resultOrg);
        endTime = System.currentTimeMillis();
        Logger.getLogger().write("Update of " + updResult.size() + " entries took " + (endTime - startTime) + "ms (" + ((int) (((double) (endTime - startTime) / (double) updResult.size()) * 100d) / 100d) + "ms per Entry)");
    }

    private static void insertDelete() {
        Logger.getLogger().write("Inserting additional 2000 entries into table");
        ArrayList<TestTable> testData = new ArrayList<TestTable>();

        for (int i = 0; i < 2000; i++) {
            TestTable tt = new TestTable();
            tt.setField2((double) (i % 4));
            tt.setField3(i % 8);
            tt.setField4(8);

            testData.add(tt);
        }

        long startTime = System.currentTimeMillis();
        ttDAO.insertAll(testData);
        long endTime = System.currentTimeMillis();
        Logger.getLogger().write("Generating " + testData.size() + " entries took " + (endTime - startTime) + "ms (" + ((int) (((double) (endTime - startTime) / (double) testData.size()) * 100d) / 100d) + "ms per Entry)");

        TestTable tt = new TestTable();
        tt.setField4(8);
        startTime = System.currentTimeMillis();
        ArrayList<TestTable> results = ttDAO.find(tt);
        endTime = System.currentTimeMillis();
        Logger.getLogger().write("Found " + results.size() + " entries with field4=8 in " + (endTime - startTime) + "ms");

        Logger.getLogger().write("Delete data to restore previous state ...");

        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("field4",8));
        startTime = System.currentTimeMillis();         
        int no = ttDAO.removeAll(qks);
        endTime = System.currentTimeMillis();        
        Logger.getLogger().write("Deletion of "+no+" entries took " + (endTime - startTime) + "ms ("+((int)(((double)(endTime - startTime) / (double)no) * 100d) / 100d)+"ms per Entry)");  
     
    }
    
    private static void clearTestData() {
        long startTime = System.currentTimeMillis();
        int no = ttDAO.removeAll();
        long endTime = System.currentTimeMillis();
        Logger.getLogger().write("Deletion of "+no+" entries took " + (endTime - startTime) + "ms ("+((int)(((double)(endTime - startTime) / (double)no) * 100d) / 100d)+"ms per Entry)");  
    }
    
    private static void checkNoDataLeft() {
        boolean allChecksOk = true;

        for (int i = 0; i < 3; i++) {
            TestTable tt = new TestTable();
            tt.setField2((double) i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            if (0 != results.size()) {
                allChecksOk = false;
            }
        }

        for (int i = 0; i < 7; i++) {
            TestTable tt = new TestTable();
            tt.setField3(i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            if (0 != results.size()) {
                allChecksOk = false;
            }
        }

        for (int i = 0; i < 6; i++) {
            TestTable tt = new TestTable();
            tt.setField4(i);
            long startTime = System.currentTimeMillis();
            ArrayList<TestTable> results = ttDAO.find(tt);
            long endTime = System.currentTimeMillis();
            if (0 != results.size()) {
                allChecksOk = false;
            }
        }

        String resultStr = allChecksOk ? "SUCCESS" : "FAILED";
        Logger.getLogger().write("INTEGRITY CHECK: " + resultStr);        
    }
}
