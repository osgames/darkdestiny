/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.database.framework;

/**
 *
 * @author Stefan
 */
public class OrderKey {
    public static final int ORDER_ASC = 0;
    public static final int ORDER_DESC = 1;
    
    private final String fieldName;
    private final int order;

    public OrderKey(String fieldName, int order) {
        this.fieldName = fieldName;
        this.order = order;
    }
    
    public String getFieldName() {
        return fieldName;
    }

    public int getOrder() {
        return order;
    }
}
