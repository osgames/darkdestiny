/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dominationmap;

import at.viswars.Logger.Logger;
import at.viswars.scanner.StarMapQuadTree;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class DominationMap {
    public static void createDominationMap() {
        int width = 2000;
        int height = 2000;
        int distance = 200;
        
        ArrayList<FactionPoint> testData = new ArrayList<FactionPoint>();

        // Generate Test Data Seed
        for (int i=0;i<100;i++) {
            int x = (int)Math.floor(Math.random() * width);
            int y = (int)Math.floor(Math.random() * width);
            int faction = (int)Math.ceil(Math.random() * 10d);
            
            testData.add(new FactionPoint(faction,x,y));
        }
        
        // Fill Quadtree
        StarMapQuadTree<FactionPoint> qt = new StarMapQuadTree<FactionPoint>(width,height,4);
        for (FactionPoint fp : testData) {
            qt.addItemToTree(fp);
        }
        
        // Find intersecting influence spheres of different users
        for (FactionPoint fp : testData) {
            ArrayList<FactionPoint> fpList = qt.getItemsAround(fp.x, fp.y, distance * 2);
            
            for (FactionPoint fp2 : fpList) {
                if (fp2.getFaction() != fp.getFaction()) {
                    Logger.getLogger().write("Found influence between user " + fp.getFaction() + " and user " + fp2.getFaction());                        
                }
            }
        }
    } 
}
