/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dominationmap;

import java.awt.Rectangle;

/**
 *
 * @author Stefan
 */
public class FactionRect extends Rectangle {
    private final int faction;
    
    public FactionRect(int faction, int x, int y, int width, int height) {
        super(x,y,width,height);
        this.faction = faction;
    }
    
    public FactionRect(FactionPoint fp, int borderSize) {
        super(fp.x - (int)(borderSize / 2d),
              fp.y - (int)(borderSize / 2d),
              borderSize,
              borderSize);
        
        this.faction = fp.getFaction();
    }

    public int getFaction() {
        return faction;
    }
}
