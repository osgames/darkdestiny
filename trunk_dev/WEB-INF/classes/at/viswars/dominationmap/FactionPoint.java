/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dominationmap;

import at.viswars.scanner.AbstractCoordinate;

/**
 *
 * @author Stefan
 */
public class FactionPoint extends AbstractCoordinate {
    private final int faction;
    
    public FactionPoint(int faction, int x, int y) {
        super(x,y);   
        this.faction = faction;             
    }

    public int getFaction() {
        return faction;
    }
    
    public double getDistanceTo(FactionPoint fp) {
        return Math.sqrt(Math.pow(this.x - fp.x, 2d) + Math.pow(this.y - fp.y,2d));
    }
}
