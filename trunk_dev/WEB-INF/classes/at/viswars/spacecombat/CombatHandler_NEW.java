/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.spacecombat;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.ships.ShipData;
import at.viswars.dao.ActionDAO;
import at.viswars.dao.BattleLogDAO;
import at.viswars.dao.BattleLogEntryDAO;
import at.viswars.dao.ConstructionModuleDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DamagedShipsDAO;
import at.viswars.dao.FleetDetailDAO;
import at.viswars.dao.FleetLoadingDAO;
import at.viswars.dao.FleetOrderDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.database.framework.QueryKey;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.diplomacy.combat.CombatGroupEntry;
import at.viswars.diplomacy.combat.CombatGroupResolver;
import at.viswars.diplomacy.combat.CombatGroupResult;
import at.viswars.enumeration.ECombatHandlerState;
import at.viswars.enumeration.EDamageLevel;
import at.viswars.enumeration.EDamagedShipRefType;
import at.viswars.enumeration.EDefenseType;
import at.viswars.enumeration.EShipType;
import at.viswars.fleet.FleetType;
import at.viswars.utilities.HangarUtilities;
import at.viswars.interfaces.ISimulationSet;
import at.viswars.model.Action;
import at.viswars.model.BattleLog;
import at.viswars.model.BattleLogEntry;
import at.viswars.model.FleetDetail;
import at.viswars.model.FleetOrder;
import at.viswars.model.PlanetDefense;
import at.viswars.model.PlayerFleet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipFleet;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.service.ConstructionService;
import at.viswars.service.ShipDesignService;
import at.viswars.spacecombat.CombatGroupFleet.UnitTypeEnum;
import at.viswars.spacecombat.CombatReportGenerator.CombatType;
import at.viswars.spacecombat.combatcontroller.CombatControllerFactory;
import at.viswars.utilities.ShipUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Dreloc
 */
public class CombatHandler_NEW {
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    private static BattleLogDAO blDAO = (BattleLogDAO) DAOFactory.get(BattleLogDAO.class);
    private static BattleLogEntryDAO bleDAO = (BattleLogEntryDAO) DAOFactory.get(BattleLogEntryDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static ConstructionModuleDAO cmDAO = (ConstructionModuleDAO) DAOFactory.get(ConstructionModuleDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static FleetOrderDAO foDAO = (FleetOrderDAO) DAOFactory.get(FleetOrderDAO.class);

    // Constructor variables
    private final ArrayList<ICombatParticipent> combatFleets = new ArrayList<ICombatParticipent>();
    private final RelativeCoordinate relCombatLocation;
    private final AbsoluteCoordinate absCombatLocation; // May be useful in future
    private final CombatReportGenerator crg;
    private final CombatReportGenerator.CombatType combatType;
    private ArrayList<ICombatParticipent> orgFleetList;
    private final HashMap<Integer, ArrayList<Integer>> groupPlayerMap = new HashMap<Integer, ArrayList<Integer>>();
    private CombatGroupResult cgr;
    private ArrayList<CombatGroup> combatGroups = new ArrayList<CombatGroup>();
    private HashMap<Integer, ArrayList<CombatResultEntry>> results = new HashMap<Integer, ArrayList<CombatResultEntry>>();
    private SpaceCombatDataLogger sc_DataLogger = null;

    // Simulation and Testing
    private String battleClass = "at.viswars.spacecombat.combatcontroller.SpaceCombatNew";

    private boolean simulation = false;
    private ISimulationSet iss = null;
    private boolean hasPlanetaryHU_sim = false;
    private boolean hasPlanetaryPA_sim = false;
    private int planetOwner_sim = 0;
    private HashMap<Integer,Integer> planetaryDefenses_sim = null;
    private HashMap<Integer,Integer> mobileDefenseTransferMap_sim = new HashMap<Integer,Integer>();

    private int planet_sim = 0;

    // Handling state of CombatHandler
    private ECombatHandlerState state = null;

    public CombatHandler_NEW(ISimulationSet css) {
        this(css.getCombatType(), css.getFleets(), new RelativeCoordinate(0, 0), css.getAbsLocation(), css.getCombatReportGenerator());
        this.iss = css;

        ArrayList<PlayerPlanet> ppList = ppDAO.findByUserId(css.getPlanetOwner());
        planet_sim = ppList.get(0).getPlanetId();

        if (css.getBattleClass() != null) {
            this.battleClass = css.getBattleClass();
            Logger.getLogger().write("Set BattleClass version to: " + battleClass);
        }

        this.hasPlanetaryHU_sim = css.hasHUShield();
        this.hasPlanetaryPA_sim = css.hasPAShield();
        this.planetOwner_sim = css.getPlanetOwner();
        this.planetaryDefenses_sim = css.getGroundDefense();

        simulation = true;
        state = ECombatHandlerState.INITIALIZED;
    }

    public CombatHandler_NEW(CombatReportGenerator.CombatType type, ArrayList<PlayerFleet> involvedFleets, RelativeCoordinate relLocation, AbsoluteCoordinate absLocation, CombatReportGenerator crg) {
        combatFleets.addAll(involvedFleets);
        absCombatLocation = absLocation;
        relCombatLocation = relLocation;
        this.crg = crg;
        combatType = type;
        state = ECombatHandlerState.INITIALIZED;
    }

    public void executeBattle() {
        if (state != ECombatHandlerState.INITIALIZED) return;

        Logger.getLogger().write(Logger.LogLevel.INFO, "Combat location is " + relCombatLocation.getSystemId() + ":" + relCombatLocation.getPlanetId());

        if (relCombatLocation.getSystemId() == 0) {
            DebugBuffer.writeStackTrace("Preventing combat in System 0", new Exception("COMBAT IN SYS 0"));
            throw new RuntimeException("ERROR COMBAT IN SYS 0");
        }

        if (!simulation) {
        // Save participating shipdesigns to the battlelog
            try {
                BattleLog bl = new BattleLog();
                bl.setComment("");
                bl.setAccuracy(0f);
                bl = blDAO.add(bl);
                int id = bl.getId();
                for (ICombatParticipent cp : combatFleets) {
                    PlayerFleetExt pfe = new PlayerFleetExt(cp.getId());
                    for (ShipData sd : pfe.getShipList()) {
                        BattleLogEntry battleLogEntry = bleDAO.findBy(id, sd.getDesignId());
                        if (battleLogEntry != null) {
                            battleLogEntry.setCount(battleLogEntry.getCount() + sd.getCount());
                            bleDAO.update(battleLogEntry);
                        } else {
                            battleLogEntry = new BattleLogEntry();
                            battleLogEntry.setDesignId(sd.getDesignId());
                            if (id != 0) {
                                battleLogEntry.setId(id);
                            }
                            battleLogEntry.setCount(sd.getCount());
                            bleDAO.add(battleLogEntry);
                        }
                    }
                }
            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Error while creating additional Data for BattleLog");
                DebugBuffer.writeStackTrace("Errror", e);
            }
        } else {
            // Remove space stations from PlayerFleet Data of owning player
            for (ICombatParticipent cp : combatFleets) {
                if (cp.getUserId() == planetOwner_sim) {
                    PlayerFleetExt pfe = new PlayerFleetExt(cp.getId());
                    for (ShipData sd : pfe.getShipList()) {
                        int designId = sd.getDesignId();

                        ShipDesign sdTmp = sdDAO.findById(designId);
                        if (sdTmp.getType() == EShipType.SPACEBASE) {
                            mobileDefenseTransferMap_sim.put(designId, sd.getCount());

                            ShipFleet sfDel = new ShipFleet();
                            sfDel.setFleetId(cp.getId());
                            sfDel.setDesignId(designId);
                            sfDel = (ShipFleet)sfDAO.find(sfDel).get(0);
                            sfDAO.remove(sfDel);
                        }
                    }
                }
            }
        }


        ArrayList<Integer> playerList = new ArrayList<Integer>();
        for (ICombatParticipent cp : combatFleets) {
            if (!playerList.contains(cp.getUserId())) {
                playerList.add(cp.getUserId());
            }
        }

        // Find stationary defense at current combat location
        ArrayList<Integer> dUsers = hasPlayerDefenses(relCombatLocation);
        for (Integer uId : dUsers) {
            if (!playerList.contains(uId)) {
                playerList.add(uId);
            }

            if (relCombatLocation.getPlanetId() != 0) {
                combatFleets.add(new ActiveDefenseSystem(new Integer(0), uId, "Planetary Defense for Planet #" + relCombatLocation.getPlanetId()));
            } else {
                combatFleets.add(new ActiveDefenseSystem(new Integer(0), uId, "System Defense for System #" + relCombatLocation.getSystemId()));
            }
        }

        // groupPlayerMap = CombatUtilities.getCombatGroupedPlayers(playerList);
        if (simulation) {
            cgr = CombatGroupResolver.getCombatGroupsSimulation(playerList, relCombatLocation, iss.getGroups());
        } else {
            cgr = CombatGroupResolver.getCombatGroups(playerList, relCombatLocation);
        }

        for (CombatGroupEntry cge : cgr.getCombatGroups()) {
            groupPlayerMap.put(cge.getGroupId(), cge.getPlayers());
        }

        orgFleetList = new ArrayList<ICombatParticipent>();
        for (ICombatParticipent cp : combatFleets) {
            if (cp instanceof PlayerFleet) {
                orgFleetList.add(((PlayerFleet) cp).clone());
            } else if (cp instanceof ActiveDefenseSystem) {
                orgFleetList.add(((ActiveDefenseSystem) cp).clone());
            }
        }

        buildCombatGroups();

        try {
            startCombat();

            crg.addCombatResult(combatType, relCombatLocation, absCombatLocation, orgFleetList, combatGroups);
            compareCombatResultToFleets();
            storeDamageLevels();
            state = ECombatHandlerState.FIGHT_PROCESSED;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("ERROR IN SPACE COMBAT: ", e);
            state = ECombatHandlerState.ERROR;
        }
    }

    private ArrayList<Integer> hasPlayerDefenses(RelativeCoordinate rc) {
        ArrayList<PlanetDefense> pdList = new ArrayList<PlanetDefense>();
        if (rc == null) {
            return new ArrayList<Integer>();
        }
        if (rc.getPlanetId() != 0) {
            pdList = pdDAO.findByPlanetId(rc.getPlanetId());
        } else {
            pdList = pdDAO.findBySystemId(rc.getSystemId());
        }

        if (simulation) {
            Logger.getLogger().write("====== LOAD SIMULATION DEFENSE ======");

            for (Map.Entry<Integer,Integer> pDefList : planetaryDefenses_sim.entrySet()) {
                PlanetDefense pd = new PlanetDefense();
                pd.setFleetId(0);
                pd.setPlanetId(relCombatLocation.getPlanetId());
                pd.setSystemId(relCombatLocation.getSystemId());
                pd.setCount(pDefList.getValue());
                pd.setType(EDefenseType.TURRET);
                pd.setUserId(planetOwner_sim);
                pd.setUnitId(pDefList.getKey());
                pdList.add(pd);
            }

            for (Map.Entry<Integer,Integer> oDefList : mobileDefenseTransferMap_sim.entrySet()) {
                PlanetDefense pd = new PlanetDefense();
                pd.setFleetId(0);
                pd.setPlanetId(relCombatLocation.getPlanetId());
                pd.setSystemId(relCombatLocation.getSystemId());
                pd.setCount(oDefList.getValue());
                pd.setType(EDefenseType.SPACESTATION);
                pd.setUserId(planetOwner_sim);
                pd.setUnitId(oDefList.getKey());
                pdList.add(pd);
            }
        }

        ArrayList<Integer> players = new ArrayList<Integer>();

        Logger.getLogger().write("LOCATION IS="+rc.getPlanetId());
        PlayerPlanet pp = ppDAO.findByPlanetId(rc.getPlanetId());

        for (PlanetDefense pd : pdList) {
            if ((pd.getType() == EDefenseType.TURRET) && (pd.getUserId() != pp.getUserId())) {
                DebugBuffer.warning("Defense of some other player ("+pd.getUserId()+") still exists on this planet which belongs to " + pp.getUserId() + " (ACTION TAKEN: DELETED)");
                pdDAO.remove(pd);
                continue;
            }

            if (!players.contains(pd.getUserId())) {
                players.add(pd.getUserId());
            }
        }

        return players;
    }

    private ArrayList<ICombatParticipent> findDefenses(RelativeCoordinate rc) {
        ArrayList<ICombatParticipent> defenses = new ArrayList<ICombatParticipent>();

        ArrayList<PlanetDefense> pdList = null;
        if (rc.getPlanetId() != 0) {
            pdList = pdDAO.findByPlanetId(rc.getPlanetId());
        } else {
            pdList = pdDAO.findBySystemId(rc.getSystemId());
        }

        return defenses;
    }

    private void buildCombatGroups() {
        Logger.getLogger().write("======== Build Combat Groups =========");

        for (Map.Entry<Integer, ArrayList<Integer>> gpe : groupPlayerMap.entrySet()) {
            Logger.getLogger().write("Preparing Combat Group " + gpe.getKey());

            CombatGroup cg = new CombatGroup();
            cg.setId(gpe.getKey());

            // Tell each group what to attack
            for (CombatGroupEntry cge : cgr.getCombatGroups()) {
                if (cge.getGroupId() == cg.getId()) {
                    for (Integer enemyGrp : cge.getAttackingGroups()) {
                        for (CombatGroupEntry cgeEnemy : cgr.getCombatGroups()) {
                            if (enemyGrp == cgeEnemy.getGroupId()) {
                                for (Integer eUser : cgeEnemy.getPlayers()) {
                                    cg.addToAttack(eUser);
                                }
                            }
                        }
                    }
                }
            }

            ArrayList<Integer> pList = gpe.getValue();

            int globalShipCount = 0;
            int fleetCount = 0;

            Logger.getLogger().write("Active Group: " + gpe.getKey());
            for (Integer i : gpe.getValue()) {
                Logger.getLogger().write("Associated player " + i);
            }

            for (Iterator<ICombatParticipent> cpIt = combatFleets.iterator(); cpIt.hasNext();) {
                ICombatParticipent cp = cpIt.next();
                if (!pList.contains((int)cp.getUserId())) {
                    continue;
                }

                fleetCount++;

                CombatGroupFleet cgf = new CombatGroupFleet();
                cgf.setUserId(cp.getUserId());
                cgf.setGroupId(cg.getId());
                cgf.setFleetName(cp.getName());
                cgf.setFleetId(cp.getId());

                Logger.getLogger().write("Created CGF for user " + cgf.getUserId() + " with name " + cgf.getFleetName());

                if (cp instanceof PlayerFleet) {
                    Logger.getLogger().write("Found Fleet " + cp.getId() + " for Combat Group " + gpe.getKey());

                    // UNLOAD HANGAR FOR COMBAT
                    HangarUtilities hu = new HangarUtilities();
                    hu.reconstructUnloadedFleetInDB(cp.getId());

                    // LOAD FLEETS
                    Logger.getLogger().write("Load fleet");
                    PlayerFleetExt pfExt = new PlayerFleetExt(cp.getId());
                    at.viswars.model.FleetOrder fo = null;

                    if (pfExt.isInFleetFormation()) {
                        // READ RETREAT INFO IF AVAILABLE
                        fo = foDAO.get(pfExt.getBase().getFleetFormationId(), FleetType.FLEET_FORMATION);
                        Logger.getLogger().write("Check retreat factor for fleetformation " + cp.getId());

                        cgf.setFleetFormationId(pfExt.getBase().getFleetFormationId());
                    } else {
                        // READ RETREAT INFO IF AVAILABLE
                        fo = foDAO.get(cp.getId(), FleetType.FLEET);
                        Logger.getLogger().write("Check retreat factor for fleet " + cp.getId());
                    }

                    if (fo != null) {
                        Logger.getLogger().write("Set RetreatFactor to " + fo.getRetreatFactor());
                        cgf.setRetreatFactor(fo.getRetreatFactor());
                        cgf.setRetreatTo(fo.getRetreatTo());
                        cgf.setRetreatToType(fo.getRetreatToType());
                    } else {
                        Logger.getLogger().write("Nothing found");
                        cgf.setRetreatFactor(100);
                    }

                    int localShipCount = 0;
                    Map<CombatGroupFleet.DesignDescriptor, CombatUnit> shipDesignCount = new HashMap<CombatGroupFleet.DesignDescriptor, CombatUnit>();

                    for (ShipData sd : pfExt.getShipList()) {
                        CombatGroupShipDesign cgsd = new CombatGroupShipDesign();
                        cgsd.setDesignId(sd.getId());
                        cgsd.setCount(sd.getCount());

                        if (sd.getCount() <= 0) {
                            DebugBuffer.warning("Found Ship ["+sd.getDesignId()+"] with count " + sd.getCount() + " in fleet " +
                                    pfExt.getName() + "("+pfExt.getName()+")");
                            continue;
                        }

                        localShipCount += sd.getCount();

                        shipDesignCount.put(
                                new CombatGroupFleet.DesignDescriptor(
                                sd.getDesignId(), CombatGroupFleet.UnitTypeEnum.SHIP),
                                new CombatUnit(sd.getDesignId(), sd.getId(), sd.getCount(), CombatGroupFleet.UnitTypeEnum.SHIP));
                    }

                    globalShipCount += localShipCount;
                    cgf.setShipDesign(shipDesignCount);

                    Logger.getLogger().write("Putting " + shipDesignCount.size() + " designs into fleet " + cp.getId());
                } else if (cp instanceof ActiveDefenseSystem) {
                    // LOAD DEFENSE SYSTEMS              
                    Logger.getLogger().write("Load Defense");
                    ArrayList<PlanetDefense> pdList = null;

                    if (relCombatLocation.getPlanetId() != 0) {
                        pdList = pdDAO.findByPlanetAndUserId(relCombatLocation.getPlanetId(), cp.getUserId());
                        cgf.setPlanetDefense(true);
                    } else {
                        pdList = pdDAO.findBySystemAndUserId(relCombatLocation.getSystemId(), cp.getUserId());
                        cgf.setSystemDefense(true);
                    }

                    int localShipCount = 0;
                    Map<CombatGroupFleet.DesignDescriptor, CombatUnit> shipDesignCount = new HashMap<CombatGroupFleet.DesignDescriptor, CombatUnit>();

                    // ADD HERE - SIMULATED BUILDINGS
                    // if we run a simulation we need to simulate the defense entries
                    if (simulation) {
                        Logger.getLogger().write("====== LOAD SIMULATION DEFENSE ======");

                        for (Map.Entry<Integer,Integer> pDefList : planetaryDefenses_sim.entrySet()) {
                            PlanetDefense pd = new PlanetDefense();
                            pd.setFleetId(0);
                            pd.setPlanetId(relCombatLocation.getPlanetId());
                            pd.setSystemId(relCombatLocation.getSystemId());
                            pd.setCount(pDefList.getValue());
                            pd.setType(EDefenseType.TURRET);
                            pd.setUserId(planetOwner_sim);
                            pd.setUnitId(pDefList.getKey());
                            pdList.add(pd);
                        }

                        for (Map.Entry<Integer,Integer> oDefList : mobileDefenseTransferMap_sim.entrySet()) {
                            PlanetDefense pd = new PlanetDefense();
                            pd.setFleetId(0);
                            pd.setPlanetId(relCombatLocation.getPlanetId());
                            pd.setSystemId(relCombatLocation.getSystemId());
                            pd.setCount(oDefList.getValue());
                            pd.setType(EDefenseType.SPACESTATION);
                            pd.setUserId(planetOwner_sim);
                            pd.setUnitId(oDefList.getKey());
                            pdList.add(pd);
                        }
                    }

                    for (PlanetDefense pd : pdList) {
                        CombatGroupShipDesign cgsd = new CombatGroupShipDesign();
                        cgsd.setDesignId(pd.getUnitId());
                        cgsd.setCount(pd.getCount());
                        localShipCount += pd.getCount();

                        if (pd.getCount() <= 0) {
                            DebugBuffer.warning("Found Planetdefense ["+pd.getType().name()+"/"+pd.getUnitId()+"] with count " + pd.getCount() + " for location " +
                                    relCombatLocation.getSystemId() + ":" + relCombatLocation.getPlanetId());
                            continue;
                        }

                        CombatGroupFleet.UnitTypeEnum unitType = null;
                        if (pd.getType() == EDefenseType.TURRET) {
                            Logger.getLogger().write("Load Surface Defense");
                            unitType = CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE;
                        } else if (pd.getType() == EDefenseType.SPACESTATION) {
                            Logger.getLogger().write("Load Plattform Defense");
                            unitType = CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE;
                        }

                        shipDesignCount.put(
                                new CombatGroupFleet.DesignDescriptor(
                                pd.getUnitId(), unitType),
                                new CombatUnit(pd.getUnitId(), pd.getCount(), unitType));
                    }

                    globalShipCount += localShipCount;
                    cgf.setShipDesign(shipDesignCount);
                }

                cg.addFleet(cgf);
                cpIt.remove();
            }

            cg.setShipCount(globalShipCount);
            cg.setFleetCount(fleetCount);
            combatGroups.add(cg);
        }
    }

    private void startCombat() {
        ISpaceCombat sc = null;

        if (simulation) {
            sc = CombatControllerFactory.getCombatController(battleClass, CombatHandler.ORBITAL_COMBAT, planet_sim);
        } else {
            int combatNumeric = 0;
            if (combatType.equals(CombatType.ORBITAL_COMBAT)) {
                combatNumeric = CombatHandler.ORBITAL_COMBAT;
                sc = CombatControllerFactory.getCombatController(battleClass, combatNumeric, relCombatLocation.getPlanetId());
            } else if (combatType.equals(CombatType.SYSTEM_COMBAT)) {
                combatNumeric = CombatHandler.SYSTEM_COMBAT;
                sc = CombatControllerFactory.getCombatController(battleClass, combatNumeric, relCombatLocation.getSystemId());
            }
        }

        sc.setISimulationSet(iss);
        CombatContext.createCombatContext(sc);
        // sc.setSimulationParameters(speedFactor,probabilityPA,minDamagePA,maxDamagePA,simultanAttack);

        sc.setCombatGroups(combatGroups);
        sc.setCombatGroupResult(cgr);

        Logger.getLogger().write(Logger.LogLevel.INFO, "Starting combat calculation");
        sc.startFightCalc();
        sc_DataLogger = sc.getLoggingData();

        CombatContext.destroyCombatContext();
        Logger.getLogger().write(Logger.LogLevel.INFO, "Starting combat calculation finished");
    }

    public SpaceCombatDataLogger getLoggingData() {
        if (state != ECombatHandlerState.FIGHT_PROCESSED) return null;
        return sc_DataLogger;
    }

    private void storeDamageLevels() {
        for (CombatGroup cg : combatGroups) {
            Logger.getLogger().write("CGF Size: " + cg.getFleetList().size());
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                if (cgf.isPlanetDefense() || cgf.isSystemDefense()) {
                    continue;
                }

                PlayerFleetExt refFleet = null;

                // Logger.getLogger().write("CF Size: " + orgFleetList.size());
                for (ICombatParticipent cp : orgFleetList) {
                   // Logger.getLogger().write("Compare " + cp.getId() + " to " + cgf.getFleetId());
                    if (cp.getId() == cgf.getFleetId()) {
                        refFleet = new PlayerFleetExt(cp.getId());
                        break;
                    }
                }

                ArrayList<ShipData> ships = refFleet.getShipList();

                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                    for (ShipData sd : ships) {
                        if (sd.getDesignId() == entry.getKey().getId()) {
                            Logger.getLogger().write("Ship " + sd.getDesignName() + " had " + (sd.getCount() - entry.getValue().getSurvivors()) + "/" + sd.getCount() + " losses");

                            QueryKeySet qks = new QueryKeySet();
                            qks.addKey(new QueryKey("shipFleetId", sd.getId()));
                            dsDAO.removeAll(qks);
                            Logger.getLogger().write("Cleaned id " + sd.getId());
                            Logger.getLogger().write("MINOR: " + entry.getValue().getMinorDamage());
                            Logger.getLogger().write("LIGHT: " + entry.getValue().getLightDamage());
                            Logger.getLogger().write("MEDIUM: " + entry.getValue().getMediumDamage());
                            Logger.getLogger().write("HEAVY: " + entry.getValue().getHeavyDamage());

                            ArrayList<at.viswars.model.DamagedShips> newEntries = new ArrayList<at.viswars.model.DamagedShips>();
                            if (entry.getValue().getMinorDamage() > 0) {
                                at.viswars.model.DamagedShips newEntry = new at.viswars.model.DamagedShips();
                                newEntry.setCount(entry.getValue().getMinorDamage());
                                newEntry.setShipFleetId(sd.getId());
                                newEntry.setDamageLevel(EDamageLevel.MINORDAMAGE);
                                newEntry.setRefTable(EDamagedShipRefType.SHIPFLEET);
                                newEntries.add(newEntry);
                            }
                            if (entry.getValue().getLightDamage() > 0) {
                                at.viswars.model.DamagedShips newEntry = new at.viswars.model.DamagedShips();
                                newEntry.setCount(entry.getValue().getLightDamage());
                                newEntry.setShipFleetId(sd.getId());
                                newEntry.setDamageLevel(EDamageLevel.LIGHTDAMAGE);
                                newEntry.setRefTable(EDamagedShipRefType.SHIPFLEET);
                                newEntries.add(newEntry);
                            }
                            if (entry.getValue().getMediumDamage() > 0) {
                                at.viswars.model.DamagedShips newEntry = new at.viswars.model.DamagedShips();
                                newEntry.setCount(entry.getValue().getMediumDamage());
                                newEntry.setShipFleetId(sd.getId());
                                newEntry.setDamageLevel(EDamageLevel.MEDIUMDAMAGE);
                                newEntry.setRefTable(EDamagedShipRefType.SHIPFLEET);
                                newEntries.add(newEntry);
                            }
                            if (entry.getValue().getHeavyDamage() > 0) {
                                at.viswars.model.DamagedShips newEntry = new at.viswars.model.DamagedShips();
                                newEntry.setCount(entry.getValue().getHeavyDamage());
                                newEntry.setShipFleetId(sd.getId());
                                newEntry.setDamageLevel(EDamageLevel.HEAVYDAMAGE);
                                newEntry.setRefTable(EDamagedShipRefType.SHIPFLEET);
                                newEntries.add(newEntry);
                            }

                            dsDAO.insertAll(newEntries);
                        }
                    }
                }
            }
        }
    }

    private void compareCombatResultToFleets() {
        for (CombatGroup cg : combatGroups) {
            Logger.getLogger().write(LogLevel.INFO, "CGF Size: " + cg.getFleetList().size());
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                PlayerFleetExt refFleet = null;

                Logger.getLogger().write(LogLevel.INFO, "CF Size: " + orgFleetList.size());

                for (ICombatParticipent cp : orgFleetList) {
                   // Logger.getLogger().write(LogLevel.INFO, "Compare " + cp.getId() + " to " + cgf.getFleetId());

                    if (cp.getId() == cgf.getFleetId()) {
                        if (cgf.isPlanetDefense() || cgf.isSystemDefense()) {
                            ArrayList<PlanetDefense> pdList = null;

                            if (cgf.isPlanetDefense()) {
                                pdList = pdDAO.findByPlanetAndUserId(relCombatLocation.getPlanetId(), cp.getUserId());
                            } else {
                                pdList = pdDAO.findBySystemAndUserId(relCombatLocation.getSystemId(), cp.getUserId());
                            }

                            for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                                for (PlanetDefense pd : pdList) {
                                    if (pd.getUnitId() == entry.getKey().getId()) {
                                        String name = "Unknown";

                                        if (entry.getKey().getType() == UnitTypeEnum.SURFACE_DEFENSE) {
                                            name = ConstructionService.findConstructionById(entry.getKey().getId()).getName();
                                        } else if (entry.getKey().getType() == UnitTypeEnum.PLATTFORM_DEFENSE) {
                                            name = ShipDesignService.getShipDesign(entry.getKey().getId()).getName();
                                        }

                                        Logger.getLogger().write(LogLevel.INFO, "Defense " + name + " had " + (pd.getCount() - entry.getValue().getSurvivors()) + "/" + pd.getCount() + " losses");

                                        if (results.containsKey(cgf.getFleetId())) {
                                            ArrayList<CombatResultEntry> creList = results.get(cgf.getFleetId());
                                            creList.add(new CombatResultEntry(pd.getUnitId(), pd.getCount(), entry.getValue().getSurvivors(), entry.getKey().getType()));
                                        } else {
                                            ArrayList<CombatResultEntry> creList = new ArrayList<CombatResultEntry>();
                                            creList.add(new CombatResultEntry(pd.getUnitId(), pd.getCount(), entry.getValue().getSurvivors(), entry.getKey().getType()));
                                            results.put(cgf.getFleetId(), creList);
                                        }
                                    }
                                }
                            }
                        } else {
                            refFleet = new PlayerFleetExt(cp.getId());
                            ArrayList<ShipData> ships = refFleet.getShipList();

                            for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                                for (ShipData sd : ships) {
                                    if (sd.getDesignId() == entry.getKey().getId()) {
                                        Logger.getLogger().write(LogLevel.INFO, "Ship " + sd.getDesignName() + " had " + (sd.getCount() - entry.getValue().getSurvivors()) + "/" + sd.getCount() + " losses");

                                        if (results.containsKey(cgf.getFleetId())) {
                                            ArrayList<CombatResultEntry> creList = results.get(cgf.getFleetId());
                                            creList.add(new CombatResultEntry(sd.getDesignId(), sd.getCount(), entry.getValue().getSurvivors(), entry.getKey().getType()));
                                        } else {
                                            ArrayList<CombatResultEntry> creList = new ArrayList<CombatResultEntry>();
                                            creList.add(new CombatResultEntry(sd.getDesignId(), sd.getCount(), entry.getValue().getSurvivors(), entry.getKey().getType()));
                                            results.put(cgf.getFleetId(), creList);
                                        }
                                    }
                                }
                            }
                        }

                        break;
                    }
                }
            }
        }
    }

    /** This method returns fleets which have retreated or resigned during combat
     * @return A Hashset of fleet id's which have retreated or resigned during combat
     */
    public HashSet<Integer> getRetreatedOrResignedFleets() {
        Logger.getLogger().write("Get Retreated Fleets");

        if (state != ECombatHandlerState.RESULTS_PROCESSED) return null;

        HashSet<Integer> retreatedFleets = new HashSet<Integer>();

        for (CombatGroup cg : combatGroups) {
            Logger.getLogger().write(LogLevel.INFO, "Looping Combat group " + cg.getId());
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                Logger.getLogger().write(LogLevel.INFO, "Looping CombatGroupFleet " + cgf.getFleetId());
                if (!cgf.isPlanetDefense() &&
                    !cgf.isSystemDefense()) {
                    if (cgf.isResigned() || cgf.isRetreated()) {
                        Logger.getLogger().write(LogLevel.INFO, "Fleet resigned!");
                        retreatedFleets.add(cgf.getFleetId());
                    } else {
                        Logger.getLogger().write(LogLevel.INFO, "Fleet not resigned -> IGNORE");
                    }
                } else {
                    Logger.getLogger().write(LogLevel.INFO, "Fleet is defense -> SKIP");
                }
            }
        }

        return retreatedFleets;
    }

    /** This method writes losses of combat participents back to Database
     * @return A Hashset of fleet id's which have been destroyed due to combat
     */
    public HashSet<Integer> processCombatResults() {
        if (state != ECombatHandlerState.FIGHT_PROCESSED) return null;

        Logger.getLogger().write(">>>> PROCESSING COMBAT RESULTS");

        HashSet<Integer> destroyedFleets = new HashSet<Integer>();

        for (Map.Entry<Integer, ArrayList<CombatResultEntry>> resEntry : results.entrySet()) {
            ArrayList<CombatResultEntry> creList = resEntry.getValue();

            boolean survivors = false;

            for (CombatResultEntry cre : creList) {
                if (cre.getSurvivors() > 0) {
                    survivors = true;
                    break;
                }
            }

            DebugBuffer.addLine(DebugLevel.DEBUG, "FLEET " + resEntry.getKey() + " is survivor? " + survivors);

            // Destroy fleet and all related data
            if (!survivors) {
                if (resEntry.getKey() != 0) { // SHIP HANDLING
                    PlayerFleet pf = pfDAO.findById(resEntry.getKey());
                    DebugBuffer.trace("TLBUG DEBUG:" + pf.getName() + " was destroyed in combat");

                    pfDAO.remove(pf);

                    FleetOrder fo = foDAO.get(pf.getId(), FleetType.FLEET);
                    if (fo != null) foDAO.remove(fo);                    

                    DebugBuffer.trace("TLBUG DEBUG: Removed Fleet Orders");

                    // DebugBuffer.addLine(DebugLevel.DEBUG, "Remove Fleet " + pf.getId());

                    // Fleet Details
                    FleetDetail fd = fdDAO.findByFleetId(pf.getId());
                    if (fd != null) {
                        DebugBuffer.trace("TLBUG DEBUG: Removed Fleet Detail " + fd.getId());
                        // DebugBuffer.addLine(DebugLevel.DEBUG, "Delete Fleetdetail " + fd);
                        fdDAO.remove(fd);

                        // Action Entry
                        Action a = aDAO.findFlightEntryByFleetDetail(fd.getId());
                        DebugBuffer.trace("TLBUG DEBUG: Removed Action " + a.getRefTableId());
                        // DebugBuffer.addLine(DebugLevel.DEBUG, "Delete Action " + a);
                        aDAO.remove(a);
                    }

                    // Fleet Loading
                    QueryKeySet qks = new QueryKeySet();
                    qks.addKey(new QueryKey("fleetId", pf.getId()));
                    int flDel = flDAO.removeAll(qks);
                    // DebugBuffer.addLine(DebugLevel.DEBUG, "Remove all fleetloading (" + flDel + ")");

                    // Ship Fleet
                    int sfDel = sfDAO.removeAll(qks);
                    DebugBuffer.trace("TLBUG DEBUG: Removed all shipfleets");
                    // DebugBuffer.addLine(DebugLevel.DEBUG, "Remove all shipfleet (" + sfDel + ")");

                    Logger.getLogger().write("Adding " + pf.getId() + " to destroyed fleets");
                    DebugBuffer.trace("TLBUG DEBUG: Adding " + pf.getName() + " to destroyed fleets");
                    // DebugBuffer.addLine(DebugLevel.DEBUG, "Adding " + pf.getId() + " to destroyed fleets");
                    destroyedFleets.add(pf.getId());
                } else { // DEFENSE HANDLING
                    if (relCombatLocation.getPlanetId() != 0) {
                        QueryKeySet qks = new QueryKeySet();
                        qks.addKey(new QueryKey("planetId", relCombatLocation.getPlanetId()));
                        pdDAO.removeAll(qks);
                    } else {
                        QueryKeySet qks = new QueryKeySet();
                        qks.addKey(new QueryKey("systemId", relCombatLocation.getSystemId()));
                        pdDAO.removeAll(qks);
                    }
                }
            } else {
                Logger.getLogger().write(">>>> WE HAVE SURVIVORS HERE");

                if (resEntry.getKey() != 0) { // SHIP HANDLING
                    Logger.getLogger().write(">>>> WE HAVE EVEN SHIPS HERE");

                    for (CombatResultEntry cre : creList) {
                        ShipFleet sf = sfDAO.getByFleetDesign(resEntry.getKey(), cre.getDesignId());
                        if (sf == null) {
                            DebugBuffer.addLine(DebugLevel.ERROR, "Try to get non-existant shipfleet entry for fleet " + resEntry.getKey() + " and design " + cre.getDesignId());
                            continue;
                        }

                        if (cre.getSurvivors() > 0) {
                            DebugBuffer.addLine(DebugLevel.DEBUG, "Set count for design " + sf.getDesignId() + " to " + cre.getSurvivors());
                            sf.setCount(cre.getSurvivors());
                            sfDAO.update(sf);
                        } else {
                            DebugBuffer.addLine(DebugLevel.DEBUG, "Delete design " + sf.getDesignId() + " from Fleet");
                            sfDAO.remove(sf);
                        }
                    }

                    // Check if we have to destroy fleet loading
                    ShipUtilities.checkExcessFleetLoading(resEntry.getKey());

                    // Load all ships in this fleet back into hangars
                    // Applies only to fleet formations to avoid problems
                    PlayerFleet pf = pfDAO.findById(resEntry.getKey());

                    Logger.getLogger().write("TRY LOADING HANGAR FOR FLEET " + resEntry.getKey() + " " + pf.getName() + " FORMATION: " + pf.getFleetFormationId());

                    if (pf.getFleetFormationId() != 0) {
                        Logger.getLogger().write("DO IT FAGGOT");
                        HangarUtilities hu = new HangarUtilities();
                        hu.reconstructUnloadedFleetInDB(pf.getId());
                        hu.initializeFleet(pf.getId());
                        hu.reconstructFleetInDB(pf.getId(), 0);
                    }
                } else { // DEFENSE HANDLING
                    if (relCombatLocation.getPlanetId() != 0) { // ORBITAL
                        ArrayList<PlanetDefense> pdList = pdDAO.findByPlanet(relCombatLocation.getPlanetId(), EDefenseType.SPACESTATION);
                        updateOrDeleteDefense(pdList, creList, UnitTypeEnum.PLATTFORM_DEFENSE);

                        pdList = pdDAO.findByPlanet(relCombatLocation.getPlanetId(), EDefenseType.TURRET);
                        updateOrDeleteDefense(pdList, creList, UnitTypeEnum.SURFACE_DEFENSE);
                    } else { // SYSTEM
                        ArrayList<PlanetDefense> pdList = pdDAO.findBySystem(relCombatLocation.getSystemId(), EDefenseType.SPACESTATION);
                        updateOrDeleteDefense(pdList, creList, UnitTypeEnum.PLATTFORM_DEFENSE);

                        pdList = pdDAO.findBySystem(relCombatLocation.getSystemId(), EDefenseType.TURRET);
                        updateOrDeleteDefense(pdList, creList, UnitTypeEnum.SURFACE_DEFENSE);
                    }
                }
            }
        }

        state = ECombatHandlerState.RESULTS_PROCESSED;
        return destroyedFleets;
    }

    private void updateOrDeleteDefense(ArrayList<PlanetDefense> pdList, ArrayList<CombatResultEntry> creList, UnitTypeEnum unitType) {
        for (CombatResultEntry cre : creList) {
            if (cre.getType() != unitType) {
                continue;
            }

            for (PlanetDefense pd : pdList) {
                if (cre.getDesignId() == pd.getUnitId()) {
                    if (cre.getSurvivors() > 0) {
                        pd.setCount(cre.getSurvivors());
                        pdDAO.update(pd);
                    } else {
                        pdDAO.remove(pd);
                    }
                }
            }
        }
    }

    public void printCombatGroups() {
        Logger.getLogger().write("Generated " + combatGroups.size());

        for (CombatGroup cg : combatGroups) {
            Logger.getLogger().write("Checking Group " + cg.getId());
            Logger.getLogger().write("This group has " + cg.getFleetList().size() + " fleets and " + cg.getShipCount() + " ships");

            for (CombatGroupFleet cgf : cg.getFleetList()) {
                Logger.getLogger().write("Checking fleet " + cgf.getFleetName() + "[" + cgf.getFleetIdStr() + "]");
                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                    Logger.getLogger().write("Found design " + entry.getKey().getId() + " [" + entry.getKey().getType() + "] with count " + entry.getValue().getSurvivors());
                }
            }
        }
    }
}
