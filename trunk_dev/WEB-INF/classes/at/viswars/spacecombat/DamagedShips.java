/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DamagedShipsDAO;
import at.viswars.enumeration.EDamageLevel;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class DamagedShips {
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    
    public final static int NO_DAMAGE = 0;
    // < 80%
    public final static int LIGHT_DAMAGE = 1; 
    // < 60%
    // -10% firepower    
    public final static int SERIOUS_DAMAGE = 2; 
    // < 40%
    // -20% shield damage absorbtion
    // -30% firepower
    // -10% speed
    public final static int HEAVY_DAMAGE = 3; 
    // < 20%   
    // Shield malfunction
    // -70% firepower
    // -50% speed
    public final static int CRITICAL_DAMAGE = 4;
    public final static int DESTROYED = 5;
    
    private BattleShip ship;
    private int[] typeDist = new int[]{0,0,0,0,0,0};
    private double[] hpStack = new double[]{0d,0d,0d,0d,0d,0d};
    
    private double NO_DAMAGE_MULTIPLIER = 1d;
    private double LIGHT_DAMAGE_MULTIPLIER = 2d;
    private double SERIOUS_DAMAGE_MULTIPLIER = 3d;
    private double HEAVY_DAMAGE_MULTIPLIER = 4d;
    private double CRITICAL_DAMAGE_MULTIPLIER = 5d;
    
    public DamagedShips(BattleShip bs) {
        int countLeft = bs.getCount();
        
        ArrayList<at.viswars.model.DamagedShips> damageList = dsDAO.findById(bs.getShipFleetId());
        
        ship = bs;
        
        // Logger.getLogger().write("Looking for damageentries for shipfleetId: " + bs.getShipFleetId());
        for (at.viswars.model.DamagedShips damageEntry : damageList) {
            // Logger.getLogger().write("Entries found for " + damageEntry.getShipFleetId() + "!");
            if (damageEntry.getDamageLevel() == EDamageLevel.MINORDAMAGE) {
                typeDist[1] = damageEntry.getCount();
                hpStack[1] = ((int)bs.getHp() * 0.8d) * damageEntry.getCount();                
            } else if (damageEntry.getDamageLevel() == EDamageLevel.LIGHTDAMAGE) {
                typeDist[2] = damageEntry.getCount();
                hpStack[2] = ((int)bs.getHp() * 0.6d) * damageEntry.getCount();                                
            } else if (damageEntry.getDamageLevel() == EDamageLevel.MEDIUMDAMAGE) {
                typeDist[3] = damageEntry.getCount();
                hpStack[3] = ((int)bs.getHp() * 0.4d) * damageEntry.getCount();                                
            } else if (damageEntry.getDamageLevel() == EDamageLevel.HEAVYDAMAGE) {
                typeDist[4] = damageEntry.getCount();
                hpStack[4] = ((int)bs.getHp() * 0.2d) * damageEntry.getCount();                               
            }
            
            countLeft -= damageEntry.getCount();
        }
        
        if (countLeft > 0) {
            typeDist[0] = countLeft;
            hpStack[0] = bs.getHp() * countLeft;
        }
    }

    public double getAverageCount() {
        double count = 0d;

        count += typeDist[0];
        count += typeDist[1] * 0.8d;
        count += typeDist[2] * 0.6d;
        count += typeDist[3] * 0.4d;
        count += typeDist[4] * 0.2d;

        return count;
    }

    public double getHpForLevel(int damageLevel) {        
        // Logger.getLogger().write("Request damageLevel " + damageLevel + " for ship " + ship.getName() + " which count is " + ship.getCount()+" TYPEDIST >> " + getTypeDist()[damageLevel]);
        
        if (getTypeDist()[damageLevel] == 0) {
            //Logger.getLogger().write("Total count was originally " + ship.getOrgCount());
            printDistribution();
            //Logger.getLogger().write("Exiting System.exit(0)");
            throw new RuntimeException("Major crap happened ["+damageLevel+"]");
        }

        // NEW
        double averageHpInLevel = hpStack[damageLevel] / getTypeDist()[damageLevel];
        double baseStackHp = ship.getOrgHp() * getTypeDist()[damageLevel];

        double damageInLevel = baseStackHp - hpStack[damageLevel];

        return ship.getOrgHp() - damageInLevel;
        // NEW
        // return hpStack[damageLevel] / getTypeDist()[damageLevel];
    }
    
    public void updateStack(int damageLevelOrg, double startHp, double endHp) {                               
        // double hpInStack = hpStack[damageLevelOrg];
        // double hpInStack_New = hpInStack - (startHp - endHp);
        // double hpInStack_New = endHp;
        
        double damagePerc = 100d - (100d / ship.getOrgHp() * endHp);
        int newLevel = (int)Math.floor(damagePerc / 20d);

        // Logger.getLogger().write(ship.getName() + " damagePerc: " + damagePerc + " (ORGHP: " + ship.getOrgHp() + " ENDHP: " + endHp + ")");

        // double baseHpActLevel = ship.getOrgHp() * (1d - (damageLevelOrg / 5d));
        // double maxHpInStack = getTypeDist()[damageLevelOrg] * baseHpActLevel;
        // double maxAllowedDiff = ship.getOrgHp() / 5d;        
        // double currentDiff = maxHpInStack - hpInStack_New;
        
        if (newLevel > damageLevelOrg) {
            typeDist[damageLevelOrg] -= 1;
            typeDist[newLevel] += 1;  
            
            hpStack[damageLevelOrg] -= startHp;
            hpStack[newLevel] += endHp;
        /*
        if ((currentDiff > maxAllowedDiff) && (damageLevelOrg < 4)) {
            int levelsToMove = (int)Math.floor(currentDiff / maxAllowedDiff);
            
            if ((damageLevelOrg + levelsToMove) > 4) {
                // Logger.getLogger().write("["+damageLevelOrg+"] CurrentDiff = " + currentDiff + " maxAllowedDiff = " + maxAllowedDiff);
            }
            
            int newLevel = damageLevelOrg + levelsToMove;
            double hpToTransfer = baseHpActLevel - currentDiff;
            
            // if (damageLevelOrg == 0) Logger.getLogger().write("Move " + ship.getName() + " from damage level " + damageLevelOrg + " to " + newLevel);
            
            typeDist[damageLevelOrg] -= 1;
            if (newLevel == 5) {
                Logger.getLogger().write("hpInStack = " + hpInStack + " hpInStack_New = " + hpInStack_New);
                // Logger.getLogger().write("DLO: " + damageLevelOrg + " ENDHP="+endHp+ " HPDIFF="+hpDiff);
                Logger.getLogger().write("New Level is " + newLevel);
            }
            typeDist[newLevel] += 1;                        
          
            // if (damageLevelOrg == 0) Logger.getLogger().write("Ships in Level " + damageLevelOrg + ": " + typeDist[damageLevelOrg] + " Ships in Level " + newLevel + ": " + typeDist[newLevel]);
            
            // if (damageLevelOrg == 0) Logger.getLogger().write("Reduce original Hitpoints from " + hpStack[damageLevelOrg] + " by " + hpToTransfer);
            hpStack[damageLevelOrg] -= hpToTransfer;
                       
            if (getTypeDist()[damageLevelOrg] == 0) hpStack[damageLevelOrg] = 0;
                     
            hpStack[newLevel] += hpToTransfer;            
            
            // if (damageLevelOrg == 0) Logger.getLogger().write("Hp in Level " + damageLevelOrg + ": " + hpStack[damageLevelOrg] + " Hp in Level " + newLevel + ": " + hpStack[newLevel]);            
        */
        } else {
            // if (ship.getChassisSize() == 8) Logger.getLogger().write("Reduce hp stack for ship "+ship.getName()+" from "+hpStack[damageLevelOrg]+" to "+(hpStack[damageLevelOrg]-hpDiff));
            hpStack[damageLevelOrg] -= (startHp - endHp);
        }                
    }
    
    public void destroyShip(int damageLevelOrg, double hpDiff) {
        typeDist[damageLevelOrg] -= 1;
        hpStack[damageLevelOrg] -= hpDiff;
        
        if (getTypeDist()[damageLevelOrg] == 0) hpStack[damageLevelOrg] = 0;
        
        // Logger.getLogger().write("Destroy " + ship.getName() + " (HP destroyed: " + hpDiff + ")");
        
        getTypeDist()[DESTROYED]++;
    }
    
    public int getRandomDamageLevel() {
        /*
        double totalCount = getTypeDist()[0] * NO_DAMAGE_MULTIPLIER + 
                getTypeDist()[1] * LIGHT_DAMAGE_MULTIPLIER + 
                getTypeDist()[2] * SERIOUS_DAMAGE_MULTIPLIER + 
                getTypeDist()[3] * HEAVY_DAMAGE_MULTIPLIER + 
                getTypeDist()[4] * CRITICAL_DAMAGE_MULTIPLIER;
        
        double[] borderValues = new double[5];
        
        borderValues[0] = getTypeDist()[0] * NO_DAMAGE_MULTIPLIER;
        borderValues[1] = borderValues[0] + getTypeDist()[1] * LIGHT_DAMAGE_MULTIPLIER;
        borderValues[2] = borderValues[1] + getTypeDist()[2] * SERIOUS_DAMAGE_MULTIPLIER;
        borderValues[3] = borderValues[2] + getTypeDist()[3] * HEAVY_DAMAGE_MULTIPLIER;
        borderValues[4] = borderValues[3] + getTypeDist()[4] * CRITICAL_DAMAGE_MULTIPLIER;

        double randomValue = Math.random() * totalCount;
        if (randomValue > totalCount) randomValue = totalCount;
        
        int actValue = 0;
        
        for (int i=0;i<=4;i++) {
            if (randomValue < borderValues[i]) {
                if (getTypeDist()[i] == 0) Logger.getLogger().write("Seriously ... WTF!");
                return i;
            }
        }
                        
        Logger.getLogger().write("No value found: " + randomValue + " -> borderValue[4]="+borderValues[4]);
        */

        double highestPercentage = -1;
        int takeMe = -1;

        for (int i=0;i<5;i++) {
            double actPerc = (100d / ship.getCount() * getTypeDist()[i]);
            switch (i) {
                case (0):
                    actPerc = actPerc * NO_DAMAGE_MULTIPLIER;
                    break;
                case (1):
                    actPerc = actPerc * LIGHT_DAMAGE_MULTIPLIER;
                    break;
                case (2):
                    actPerc = actPerc * SERIOUS_DAMAGE_MULTIPLIER;
                    break;
                case (3):
                    actPerc = actPerc * HEAVY_DAMAGE_MULTIPLIER;
                    break;
                case (4):
                    actPerc = actPerc * CRITICAL_DAMAGE_MULTIPLIER;
                    break;
            }

            if (actPerc > highestPercentage) {
                highestPercentage = actPerc;
                takeMe = i;
            }
        }

        if (takeMe != -1) return takeMe;

        if (getTypeDist()[5] == ship.getOrgCount()) {
            // All ships have been destroyed
            return -1;
        }
        
        Logger.getLogger().write("Invalid state entered");
        throw new RuntimeException("Major crap happened");
    }
    
    public void printDistribution() {
        // Logger.getLogger().write("Damage distribution for " + ship.getName() + ":");
        
        for (int i=0;i<6;i++) {
            if (getTypeDist()[i] > 0) {
                Logger.getLogger().write("Damage Level " + i + " -> " + getTypeDist()[i] + " ships (Avg. Hp: " + hpStack[i] + ")");
                // Logger.getLogger().write("Damage Level " + i + " -> " + getTypeDist()[i] + " ships (Avg. Hp: " + hpStack[i] + ")");
            }
        }
    }

    public int[] getTypeDist() {
        return typeDist;
    }
}
