/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.spacecombat.CombatGroupFleet.UnitTypeEnum;

/**
 *
 * @author Stefan
 */
class CombatResultEntry {
    private final int designId;
    private final int orgCount;
    private final int survivors;
    private final UnitTypeEnum type;
    
    public CombatResultEntry(int designId, int orgCount, int survivors, UnitTypeEnum type) {
        this.designId = designId;
        this.orgCount = orgCount;
        this.survivors = survivors;
        this.type = type;
    }

    public int getDesignId() {
        return designId;
    }

    public int getOrgCount() {
        return orgCount;
    }

    public int getSurvivors() {
        return survivors;
    }

    public UnitTypeEnum getType() {
        return type;
    }
}
