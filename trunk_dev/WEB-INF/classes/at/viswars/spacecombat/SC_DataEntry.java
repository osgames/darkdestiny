/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.enumeration.EDamageLevel;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class SC_DataEntry {
    public final int designId;

    // Datafields
    // Basic
    private double damageTaken = 0;
    private double effDamageTaken = 0;

    // Armor
    private double ST_DamageAbsorbed = 0;
    private double TE_DamageAbsorbed = 0;
    private double YN_DamageAbsorbed = 0;

    // Targeting
    private HashMap<SC_DataEntry,Integer> chooseAsTarget = new HashMap<SC_DataEntry,Integer>();
    private HashMap<EDamageLevel,Integer> damageDistribution = new HashMap<EDamageLevel,Integer>();
    
    // Shields
    private double PS_DamageAbsorbed = 0;
    private double PS_DamagePenetrated = 0;
    private double PS_DamagePenetrated_Fail = 0;
    private double PS_ShieldPointsRestored = 0;

    private double HU_DamageAbsorbed = 0;
    private double HU_DamagePenetrated = 0;
    private double HU_DamagePenetrated_Fail = 0;
    private double HU_ShieldPointsRestored = 0;

    private double PA_DamageAbsorbed = 0;
    private double PA_DamagePenetrated = 0;
    private double PA_DamagePenetrated_Fail = 0;
    private double PA_ShieldPointsRestored = 0;

    public SC_DataEntry(int designId) {
        this.designId = designId;
    }

    /**
     * @return the damageTaken
     */
    public double getDamageTaken() {
        return damageTaken;
    }

    /**
     * @param damageTaken the damageTaken to set
     */
    public void incDamageTaken(double value) {
        this.damageTaken += value;
    }

    /**
     * @return the effDamageTaken
     */
    public double getEffDamageTaken() {
        return effDamageTaken;
    }

    /**
     * @param effDamageTaken the effDamageTaken to set
     */
    public void incEffDamageTaken(double value) {
        this.effDamageTaken += value;
    }

    /**
     * @return the ST_DamageAbsorbed
     */
    public double getST_DamageAbsorbed() {
        return ST_DamageAbsorbed;
    }

    /**
     * @param ST_DamageAbsorbed the ST_DamageAbsorbed to set
     */
    public void incST_DamageAbsorbed(double value) {
        this.ST_DamageAbsorbed += value;
    }

    /**
     * @return the TE_DamageAbsorbed
     */
    public double getTE_DamageAbsorbed() {
        return TE_DamageAbsorbed;
    }

    /**
     * @param TE_DamageAbsorbed the TE_DamageAbsorbed to set
     */
    public void incTE_DamageAbsorbed(double value) {
        this.TE_DamageAbsorbed += value;
    }

    /**
     * @return the YN_DamageAbsorbed
     */
    public double getYN_DamageAbsorbed() {
        return YN_DamageAbsorbed;
    }

    /**
     * @param YN_DamageAbsorbed the YN_DamageAbsorbed to set
     */
    public void incYN_DamageAbsorbed(double value) {
        this.YN_DamageAbsorbed += value;
    }

    /**
     * @return the chooseAsTarget
     */
    public HashMap<SC_DataEntry, Integer> getChooseAsTarget() {
        return chooseAsTarget;
    }

    /**
     * @param chooseAsTarget the chooseAsTarget to set
     */
    public void assChoosenTarget(SC_DataEntry sc_DEntry) {
        if (chooseAsTarget.containsKey(sc_DEntry)) {
            chooseAsTarget.put(sc_DEntry, chooseAsTarget.get(sc_DEntry) + 1);
        } else {
            chooseAsTarget.put(sc_DEntry, 1);
        }
    }

    /**
     * @return the PS_DamageAbsorbed
     */
    public double getPS_DamageAbsorbed() {
        return PS_DamageAbsorbed;
    }

    /**
     * @param PS_DamageAbsorbed the PS_DamageAbsorbed to set
     */
    public void incPS_DamageAbsorbed(double value) {
        this.PS_DamageAbsorbed += value;
    }

    /**
     * @return the PS_DamagePenetrated
     */
    public double getPS_DamagePenetrated() {
        return PS_DamagePenetrated;
    }

    /**
     * @param PS_DamagePenetrated the PS_DamagePenetrated to set
     */
    public void incPS_DamagePenetrated(double value) {
        this.PS_DamagePenetrated += value;
    }

    /**
     * @return the PS_DamagePenetrated_Fail
     */
    public double getPS_DamagePenetrated_Fail() {
        return PS_DamagePenetrated_Fail;
    }

    /**
     * @param PS_DamagePenetrated_Fail the PS_DamagePenetrated_Fail to set
     */
    public void incPS_DamagePenetrated_Fail(double value) {
        this.PS_DamagePenetrated_Fail += value;
    }

    /**
     * @return the PS_ShieldPointsRestored
     */
    public double getPS_ShieldPointsRestored() {
        return PS_ShieldPointsRestored;
    }

    /**
     * @param PS_ShieldPointsRestored the PS_ShieldPointsRestored to set
     */
    public void incPS_ShieldPointsRestored(double value) {
        this.PS_ShieldPointsRestored += value;
    }

    /**
     * @return the HU_DamageAbsorbed
     */
    public double getHU_DamageAbsorbed() {
        return HU_DamageAbsorbed;
    }

    /**
     * @param HU_DamageAbsorbed the HU_DamageAbsorbed to set
     */
    public void incHU_DamageAbsorbed(double value) {
        this.HU_DamageAbsorbed += value;
    }

    /**
     * @return the HU_DamagePenetrated
     */
    public double getHU_DamagePenetrated() {
        return HU_DamagePenetrated;
    }

    /**
     * @param HU_DamagePenetrated the HU_DamagePenetrated to set
     */
    public void incHU_DamagePenetrated(double value) {
        this.HU_DamagePenetrated += value;
    }

    /**
     * @return the HU_DamagePenetrated_Fail
     */
    public double getHU_DamagePenetrated_Fail() {
        return HU_DamagePenetrated_Fail;
    }

    /**
     * @param HU_DamagePenetrated_Fail the HU_DamagePenetrated_Fail to set
     */
    public void incHU_DamagePenetrated_Fail(double value) {
        this.HU_DamagePenetrated_Fail += value;
    }

    /**
     * @return the HU_ShieldPointsRestored
     */
    public double getHU_ShieldPointsRestored() {
        return HU_ShieldPointsRestored;
    }

    /**
     * @param HU_ShieldPointsRestored the HU_ShieldPointsRestored to set
     */
    public void incHU_ShieldPointsRestored(double value) {
        this.HU_ShieldPointsRestored += value;
    }

    /**
     * @return the PA_DamageAbsorbed
     */
    public double getPA_DamageAbsorbed() {
        return PA_DamageAbsorbed;
    }

    /**
     * @param PA_DamageAbsorbed the PA_DamageAbsorbed to set
     */
    public void incPA_DamageAbsorbed(double value) {
        this.PA_DamageAbsorbed += value;
    }

    /**
     * @return the PA_DamagePenetrated
     */
    public double getPA_DamagePenetrated() {
        return PA_DamagePenetrated;
    }

    /**
     * @param PA_DamagePenetrated the PA_DamagePenetrated to set
     */
    public void incPA_DamagePenetrated(double value) {
        this.PA_DamagePenetrated += value;
    }

    /**
     * @return the PA_DamagePenetrated_Fail
     */
    public double getPA_DamagePenetrated_Fail() {
        return PA_DamagePenetrated_Fail;
    }

    /**
     * @param PA_DamagePenetrated_Fail the PA_DamagePenetrated_Fail to set
     */
    public void incPA_DamagePenetrated_Fail(double value) {
        this.PA_DamagePenetrated_Fail += value;
    }

    /**
     * @return the PA_ShieldPointsRestored
     */
    public double getPA_ShieldPointsRestored() {
        return PA_ShieldPointsRestored;
    }

    /**
     * @param PA_ShieldPointsRestored the PA_ShieldPointsRestored to set
     */
    public void incPA_ShieldPointsRestored(double value) {
        this.PA_ShieldPointsRestored += value;
    }

    /**
     * @return the damageDistribution
     */
    public HashMap<EDamageLevel, Integer> getDamageDistribution() {
        return damageDistribution;
    }

    public int getCountForDamageLevel(EDamageLevel damageLevel) {
        if (damageDistribution.containsKey(damageLevel)) {
            return damageDistribution.get(damageLevel);
        } else {
            return 0;
        }
    }

    /**
     * @param damageDistribution the damageDistribution to set
     */
    public void addDamagedShip(EDamageLevel damageLevel, int count) {
        if (damageDistribution.containsKey(damageLevel)) {
            damageDistribution.put(damageLevel, damageDistribution.get(damageLevel) + count);
        } else {
            damageDistribution.put(damageLevel, count);
        }
    }
    public SC_DataEntry subtract(SC_DataEntry toSubtract){

        damageTaken -= toSubtract.getDamageTaken();
        effDamageTaken -= toSubtract.getEffDamageTaken();

        ST_DamageAbsorbed -= toSubtract.getST_DamageAbsorbed();
        TE_DamageAbsorbed -= toSubtract.getTE_DamageAbsorbed();
        YN_DamageAbsorbed -= toSubtract.getYN_DamageAbsorbed();

        HU_DamageAbsorbed -= toSubtract.getHU_DamageAbsorbed();
        HU_DamagePenetrated -= toSubtract.getHU_DamagePenetrated();
        HU_DamagePenetrated_Fail -= toSubtract.getHU_DamagePenetrated_Fail();
        HU_ShieldPointsRestored -= toSubtract.getHU_ShieldPointsRestored();

        PA_DamageAbsorbed -= toSubtract.getPA_DamageAbsorbed();
        PA_DamagePenetrated -= toSubtract.getPA_DamagePenetrated();
        PA_DamagePenetrated_Fail -= toSubtract.getPA_DamagePenetrated_Fail();
        PA_ShieldPointsRestored -= toSubtract.getPA_ShieldPointsRestored();

        PS_DamageAbsorbed -= toSubtract.getPS_DamageAbsorbed();
        PS_DamagePenetrated -= toSubtract.getPS_DamagePenetrated();
        PS_DamagePenetrated_Fail -= toSubtract.getPS_DamagePenetrated_Fail();
        PS_ShieldPointsRestored -= toSubtract.getPS_ShieldPointsRestored();

        for(Map.Entry<EDamageLevel, Integer> damage : getDamageDistribution().entrySet()){
            damage.setValue(damage.getValue() - toSubtract.getDamageDistribution().get(damage.getKey()));
        }
        return this;
    }
}
