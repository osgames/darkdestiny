/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.spacecombat;

import at.viswars.WeaponModule;
import at.viswars.spacecombat.helper.ShieldDefinition;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public interface IDefenseBuilding extends ICombatUnit {
    public ArrayList<WeaponModule> getWeapons();
    public ShieldDefinition getShields();

    public void setSc_LoggingEntry(SC_DataEntry scde);
}
