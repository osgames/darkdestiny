/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.spacecombat.helper.ShieldDefinition;
import at.viswars.WeaponModule;
import at.viswars.model.ShipDesign;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface IBattleShip extends ICombatUnit {
    public void reloadShieldsAndWeapons();
    
    public boolean isDefenseStation();
    public boolean isPlanetaryDefense();
    public boolean hasTargetFound();
    
    public ArrayList<WeaponModule> getWeapons();
    public ShieldDefinition getShields();
    public int getArmor();
    public DamagedShips getDs();
    
    public void setParentCombatGroupId(int cgId);
    
    public void setSc_LoggingEntry(SC_DataEntry scde);
    
    public void setShipFleetId(int shipFleetId);
    public ShipDesign getShipDesign();
}
