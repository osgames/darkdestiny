/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.databuffer.fleet.ICoordinate;
import at.viswars.spacecombat.CombatReportGenerator.CombatType;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class UserReportKey {
    private final int user;
    private EnumMap<CombatType,ArrayList<ICoordinate>> locations = new EnumMap<CombatType,ArrayList<ICoordinate>>(CombatType.class);

    protected UserReportKey(int user) {
        this.user = user;
    }

    protected void addCombatLocation(CombatType ct, ICoordinate coord) {
        if (getLocations().containsKey(ct)) {
            getLocations().get(ct).add(coord);
        } else {
            ArrayList<ICoordinate> locList = new ArrayList<ICoordinate>();
            locList.add(coord);
            getLocations().put(ct,locList);
        }
    }

    /**
     * @return the user
     */
    public int getUser() {
        return user;
    }

    /**
     * @return the locations
     */
    public EnumMap<CombatType, ArrayList<ICoordinate>> getLocations() {
        return locations;
    }
}
