/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.spacecombat;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.GenerateMessage;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.ML;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.*;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.ICoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.EMessageType;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Ressource;
import at.viswars.model.User;
import at.viswars.spacecombat.CombatGroupFleet.UnitTypeEnum;
import at.viswars.utilities.DiplomacyUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Dreloc
 */
public class CombatReportGenerator {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static ProductionDAO pDAO = (ProductionDAO) DAOFactory.get(ProductionDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static ResearchProgressDAO rpDAO = (ResearchProgressDAO) DAOFactory.get(ResearchProgressDAO.class);
    private static TechRelationDAO trDAO = (TechRelationDAO) DAOFactory.get(TechRelationDAO.class);
    private static ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private static ChassisDAO chDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    
    // private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    // private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    ArrayList<CombatReportEntry> combats = new ArrayList<CombatReportEntry>();
    HashMap<UserReportKey, StringBuilder> userReports;

    public enum CombatType {

        SPACE, SYSTEM_COMBAT, ORBITAL_COMBAT;
    }

    public void addCombatResult(CombatType ct, RelativeCoordinate relLocation, AbsoluteCoordinate absLocation, ArrayList<ICombatParticipent> orgFleets, ArrayList<CombatGroup> combatResult) {
        Logger.getLogger().write(LogLevel.INFO, "## Add Combat Result to Report Generator");
        combats.add(new CombatReportEntry(ct, relLocation, absLocation, orgFleets, combatResult));
    }

    public void generateReports() {
        Logger.getLogger().write(LogLevel.INFO, "##############################");
        Logger.getLogger().write(LogLevel.INFO, "## Start generating Reports ##");
        Logger.getLogger().write(LogLevel.INFO, "##############################");
        TreeMap<CombatType, ArrayList<SingleCombatReportEntry>> combat = new TreeMap<CombatType, ArrayList<SingleCombatReportEntry>>();

        for (CombatReportEntry cre : combats) {
            ArrayList<SingleCombatReportEntry> SingleCombats = combat.get(cre.getType());
            if (SingleCombats == null) {
                SingleCombats = new ArrayList<SingleCombatReportEntry>();
            }

            Logger.getLogger().write(LogLevel.INFO, "## Add Single Combat Entry");
            SingleCombats.add(generateReport(cre));
            combat.put(cre.getType(), SingleCombats);
        }

        Logger.getLogger().write(LogLevel.INFO, "## Generate User Reports");
        generateUserReports(combat);
        // generateHtml(combat);
        Logger.getLogger().write(LogLevel.INFO, "#############################");
        Logger.getLogger().write(LogLevel.INFO, "## Stop generating Reports ##");
        Logger.getLogger().write(LogLevel.INFO, "#############################");
    }

    public HashMap<UserReportKey, StringBuilder> getUserReports() {
        return userReports;
    }

    private void generateUserReports(TreeMap<CombatType, ArrayList<SingleCombatReportEntry>> combat) {
        String combatString = "Die Flottenzentrale hat soeben einen Kampfbericht �bermittelt. " + "<BR>\n";

        HashMap<Integer, StringBuilder> userBattleReport = new HashMap<Integer, StringBuilder>();
        HashMap<Integer, ArrayList<CombatType>> alreadyProcessed = new HashMap<Integer, ArrayList<CombatType>>();

        HashMap<Integer,UserReportKey> urkList = new HashMap<Integer,UserReportKey>();

        for (Map.Entry<CombatType, ArrayList<SingleCombatReportEntry>> entry1 : combat.entrySet()) {
            ArrayList<SingleCombatReportEntry> singleCombats = entry1.getValue();

            //Kreire individuelle Msg f�r jeden Spieler f�r jeden kampf
            for (SingleCombatReportEntry scre : singleCombats) {
                for (Integer i : scre.getParticipants()) {
                    String combatLocation = "";

                    StringBuilder userMsg = userBattleReport.get(i);
                    if (userMsg == null) {
                        userMsg = new StringBuilder();
                    }

                    UserReportKey urk;
                    if (!urkList.containsKey(i)) {
                        urk = new UserReportKey(i);
                    } else {
                        urk = urkList.get(i);
                    }

                    //Startmessage f�r jeden Spieler
                    if (entry1.getKey() == CombatType.SPACE) {
                        combatLocation += "<H2>Weltraumk�mpfe</H2> " + "\n";
                        urk.addCombatLocation(CombatType.SPACE, scre.absLocation);
                    } else if (entry1.getKey() == CombatType.SYSTEM_COMBAT) {
                        combatLocation += "<H2>Systemk�mpfe</H2> " + "\n";
                        urk.addCombatLocation(CombatType.SYSTEM_COMBAT, scre.relLocation);
                    } else if (entry1.getKey() == CombatType.ORBITAL_COMBAT) {
                        combatLocation += "<H2>Orbitalk�mpfe</H2> " + "\n";
                        urk.addCombatLocation(CombatType.ORBITAL_COMBAT, scre.relLocation);
                    }

                    urkList.put(i, urk);

                    StringBuilder combatMsg = new StringBuilder();
                    StringBuilder genMsg = generateHtmlForUser(scre, i, entry1.getKey());

                    boolean add = false;

                    if (!alreadyProcessed.containsKey(i)) {
                        add = true;

                        ArrayList<CombatType> userProc = new ArrayList<CombatType>();
                        userProc.add(entry1.getKey());
                        alreadyProcessed.put(i, userProc);
                    } else {
                        ArrayList<CombatType> userProc = alreadyProcessed.get(i);
                        if (userProc == null) {
                            userProc = new ArrayList<CombatType>();
                        }
                        if (!userProc.contains(entry1.getKey())) {
                            add = true;
                            userProc.add(entry1.getKey());
                            alreadyProcessed.put(i, userProc);
                        }
                    }

                    if (add) {
                        // ---------------------------------------------
                        // Build information about damageLevels of enemy
                        // ---------------------------------------------
                        StringBuilder damageReport = new StringBuilder();

                        int minorDamageCount = 0;
                        int lightDamageCount = 0;
                        int mediumDamageCount = 0;
                        int heavyDamageCount = 0;

                        TreeMap<Status, HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>> usersByStatus = sortFleetList(scre.getShipList(), i);
                        for (Map.Entry<Status, HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>> entryTmp : usersByStatus.entrySet()) {
                            if (entryTmp.getKey() == Status.ENEMY) {
                                HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>> enemies = entryTmp.getValue();

                                for (Integer enemy : enemies.keySet()) {
                                    minorDamageCount += scre.getMinorDamageCountFor(enemy);
                                    lightDamageCount += scre.getLightDamageCountFor(enemy);
                                    mediumDamageCount += scre.getMediumDamageCountFor(enemy);
                                    heavyDamageCount += scre.getHeavyDamageCountFor(enemy);
                                }
                            }
                        }

                        if (minorDamageCount > 0) {
                            damageReport.append(minorDamageCount);
                            damageReport.append(" " + ML.getMLStr("combat_gen_lbl_minor_damage", urk.getUser()) + "<BR>");
                            // damageReport.append(" Schiffe des Gegners sind geringf�gig besch�digt<BR>");
                        }
                        if (lightDamageCount > 0) {
                            damageReport.append(lightDamageCount);
                            damageReport.append(" " + ML.getMLStr("combat_gen_lbl_light_damage", urk.getUser()) + "<BR>");
                            // damageReport.append(" Schiffe des Gegners sind leicht besch�digt<BR>");
                        }
                        if (mediumDamageCount > 0) {
                            damageReport.append(mediumDamageCount);
                            damageReport.append(" " + ML.getMLStr("combat_gen_lbl_medium_damage", urk.getUser()) + "<BR>");
                            // damageReport.append(" Schiffe des Gegners sind betr�chtlich besch�digt<BR>");
                        }
                        if (heavyDamageCount > 0) {
                            damageReport.append(heavyDamageCount);
                            damageReport.append(" " + ML.getMLStr("combat_gen_lbl_heavy_damage", urk.getUser()) + "<BR>");
                            // damageReport.append(" Schiffe des Gegners sind schwer besch�digt<BR>");
                        }
                        // ----------------------------------------------
                        // END
                        // ----------------------------------------------

                        genMsg.append("<BR>");
                        combatMsg.append(combatLocation);
                        if (damageReport.length() > 0) {
                            combatMsg.append(damageReport);
                            combatMsg.append("<BR>");
                        }
                        combatMsg.append(genMsg);
                    }

                    userMsg.append(combatMsg);
                    userBattleReport.put(i, userMsg);
                }
            }

        }

        if (userReports == null) {
            userReports = new HashMap<UserReportKey, StringBuilder>();
        }

        for (Map.Entry<Integer, StringBuilder> userMsg : userBattleReport.entrySet()) {
            StringBuilder msg = userMsg.getValue();
            msg.insert(0, combatString);
            userReports.put(urkList.get(userMsg.getKey()), msg);
        }
    }

    private String generateTopic(UserReportKey urk) {
        StringBuffer sb = new StringBuffer();
        sb.append(ML.getMLStr("combat_gen_lbl_battle_notification", urk.getUser()) + " (");
        boolean first = true;

        for (Map.Entry<CombatType,ArrayList<ICoordinate>> locEntry : urk.getLocations().entrySet()) {
            if (locEntry.getKey() == CombatType.SPACE) {
                for (ICoordinate coord : locEntry.getValue()) {
                    AbsoluteCoordinate ac = (AbsoluteCoordinate)coord;

                    if (!first) {
                        sb.append(", ");
                    } else {
                        first = false;
                    }
                    sb.append(ML.getMLStr("combat_gen_lbl_coordinate", urk.getUser()) + " " + ac.getX() + ":" + ac.getY());
                }
            } else if (locEntry.getKey() == CombatType.SYSTEM_COMBAT) {
                for (ICoordinate coord : locEntry.getValue()) {
                    RelativeCoordinate rc = (RelativeCoordinate)coord;

                    if (!first) {
                        sb.append(", ");
                    } else {
                        first = false;
                    }
                    sb.append("System " + rc.getSystemId());
                }
            } else if (locEntry.getKey() == CombatType.ORBITAL_COMBAT) {
                for (ICoordinate coord : locEntry.getValue()) {
                    RelativeCoordinate rc = (RelativeCoordinate)coord;

                    if (!first) {
                        sb.append(", ");
                    } else {
                        first = false;
                    }
                    sb.append("Planet " + rc.getPlanetId());
                }
            }
        }

        sb.append(")");

        return sb.toString();
    }

    public void writeReportsToDatabase() {

        boolean sendAdminReport = false;

        Logger.getLogger().write(LogLevel.INFO, "## Write Reports to Database");


        if (userReports == null) {
            Logger.getLogger().write(LogLevel.INFO, "## User Reports == null -> GENERATE");
            generateReports();
        }

        for (Map.Entry<UserReportKey, StringBuilder> report : userReports.entrySet()) {
            GenerateMessage gm = new GenerateMessage();
            gm.setTopic(generateTopic(report.getKey()));
            gm.setDestinationUserId(report.getKey().getUser());
            gm.setSourceUserId(uDAO.getSystemUser().getUserId());
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setMsg(report.getValue().toString());
            gm.setEscape(false);
            gm.writeMessageToUser();
        }
        if (sendAdminReport) {
            try{
            ArrayList<Integer> admins = new ArrayList<Integer>();
            for (User u : (ArrayList<User>) uDAO.findAll()) {
                if (u.getAdmin()) {
                    admins.add(u.getUserId());
                }
            }
            for (Integer i : admins) {
                for (Map.Entry<UserReportKey, StringBuilder> report : userReports.entrySet()) {
                    GenerateMessage gm = new GenerateMessage();
                    gm.setTopic("Kampfnachricht - Spieler: " + uDAO.findById(report.getKey().getUser()).getGameName());
                    gm.setDestinationUserId(i);
                    gm.setSourceUserId(uDAO.getSystemUser().getUserId());
                    gm.setMessageType(EMessageType.SYSTEM);
                    gm.setMsg(report.getValue().toString());
                    gm.setEscape(false);
                    gm.writeMessageToUser();
                }
            }
            }catch(Exception e){
                DebugBuffer.addLine(DebugLevel.WARNING, "Error while generating battlereport for admins :  " + e);
            }
        }
    }

    private StringBuilder generateHtmlForUser(SingleCombatReportEntry scre, int userId, CombatType type) {
        Logger.getLogger().write(LogLevel.INFO, "Create Combat report for user " + userId);

        StringBuilder msg = new StringBuilder();
        scre.getShipList();

        String locString = "Kampf irgendwo im nirgendwo";

        if (type == CombatType.SPACE) {
            locString = "<BR>" + ML.getMLStr("combat_gen_lbl_battle_at_coord", userId) + " x:" + scre.getAbsLocation().getX() + "/y:" + scre.getAbsLocation().getY() + "</B></BR>";
        } else if (type == CombatType.SYSTEM_COMBAT) {
            String systemName;
            
            if (vtDAO.containsSystem(userId, scre.getRelLocation().getSystemId())) {
                systemName = sDAO.findById(scre.getRelLocation().getSystemId()).getName();
            } else {
                systemName = "System " + scre.getRelLocation().getSystemId();
            }
            
            locString = "<B>" + ML.getMLStr("combat_gen_lbl_battle_at_system", userId) + " " + systemName + " (" + scre.getRelLocation().getSystemId() + ")</B><BR>";
        } else if (type == CombatType.ORBITAL_COMBAT) {
            PlayerPlanet pp = ppDAO.findByPlanetId(scre.getRelLocation().getPlanetId());
            if (pp != null) {
                locString = "<B>" + ML.getMLStr("combat_gen_lbl_orbital_battle_at", userId) + " " + pp.getName() + " (" + scre.getRelLocation().getSystemId() + ":" + scre.getRelLocation().getPlanetId() + ")</B><BR>";
            } else {
                locString = "<B>" + ML.getMLStr("combat_gen_lbl_orbital_battle_at_noname", userId) + " #" + scre.getRelLocation().getPlanetId() + " (" + scre.getRelLocation().getSystemId() + ":" + scre.getRelLocation().getPlanetId() + ")</B><BR>";
            }
        }

        msg.append(locString);

        TreeMap<Status, HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>> usersByStatus = sortFleetList(scre.getShipList(), userId);

        for (Map.Entry<Status, HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>> entry1 : usersByStatus.entrySet()) {

            String color = "#FFFFFF";
            if (entry1.getKey() == Status.OWN) {
                color = "#66CC00";
            } else if (entry1.getKey() == Status.ALLIED) {
                color = "#6666FF";
            } else if (entry1.getKey() == Status.ENEMY) {
                color = "#FF0000";
            }
            for (Map.Entry<Integer, HashMap<Integer, CombatReportFleetEntry>> users : entry1.getValue().entrySet()) {
                int currUserId = users.getKey();
                msg.append("\n");
                msg.append("\t<TABLE style=\"font-size:12px; width:450px\">\n");
                msg.append("\t\t<TR style=\"background-color:");
                msg.append(color);
                msg.append("\">\n");
                msg.append("\t\t\t<TD colspan=\"4\">\n");
                msg.append("\t\t\t\t<B>");
                msg.append(uDAO.findById(currUserId).getGameName());
                msg.append("</B>\n");
                msg.append("\t\t\t</TD>\n");
                msg.append("\t\t</TR>\n");
                for (Map.Entry<Integer, CombatReportFleetEntry> fleets : users.getValue().entrySet()) {
                    long cost = 0l;

                    if (fleets.getKey() != 0) {
                        for (ReportShipEntry rse : fleets.getValue().getFleetShips()) {
                            ShipDesignExt sde = new ShipDesignExt(rse.getDesignId());
                            long tmpCost = (long) sde.getRessCost().getRess(Ressource.CREDITS);
                            cost += tmpCost * rse.getBeforeCount();
                        }
                    }

                    msg.append("\t\t<TR>\n");
                    msg.append("\t\t\t<TD colspan=\"4\">\n");

                    if (fleets.getKey() == 0) {
                        if (type == CombatType.SYSTEM_COMBAT) {
                            msg.append("\t\t\t\t<B>");
                            msg.append(ML.getMLStr("combat_gen_lbl_system_defense", currUserId));
                            msg.append("</B>\n");
                        } else if (type == CombatType.ORBITAL_COMBAT) {
                            msg.append("\t\t\t\t<B>");
                            msg.append(ML.getMLStr("combat_gen_lbl_orbital_defense", currUserId));
                            msg.append("</B>\n");
                        }
                    } else {
                        msg.append("\t\t\t\t<B>Flotte: ");
                        msg.append(scre.getNameForId(fleets.getKey()));
                        if (fleets.getValue().isRetreated()) {
                            msg.append("<BR><FONT color=\"yellow\">Die Flotte hat sich vorzeitig aus dem Kampf zur&uuml;ckgezogen.</FONT>");
                        } else if (fleets.getValue().isResigned()) {
                            msg.append("<BR><FONT color=\"yellow\">Die Flotte konnten den planetaren Schutzschirm nicht durchdringen und zog sich zur&uuml;ck.</FONT>");
                        }
                        msg.append("</B><BR>\n");
                        // msg.append("\t\t\t\t<B>Fleet cost: " + FormatUtilities.getFormattedNumber(cost) + "</B>\n";
                    }
                    msg.append("\t\t\t</TD>\n");
                    msg.append("\t\t</TR>\n");

                    msg.append("\t\t<TR>\n");
                    //Design
                    msg.append("\t\t\t<TD width=\"165\"><B>\n");
                    msg.append("\t\t\t\t Design \n");
                    msg.append("\t\t\t</TD></B>\n");
                    //Typ
                    msg.append("\t\t\t<TD width=\"165\"><B>\n");
                    msg.append("\t\t\t\t Typ \n");
                    msg.append("\t\t\t</TD></B>\n");
                    //Anzahl
                    msg.append("\t\t\t<TD width=\"60\"><B>\n");
                    msg.append("\t\t\t\t Anzahl \n");
                    msg.append("\t\t\t</TD></B>\n");
                    //Verluste
                    msg.append("\t\t\t<TD width=\"60\"><B>\n");
                    msg.append("\t\t\t\t Verluste \n");
                    msg.append("\t\t\t</TD></B>\n");
                    //TR ENDE
                    msg.append("\t\t</TR>\n");
                    for (ReportShipEntry rse : fleets.getValue().getFleetShips()) {
                        msg.append("\t\t<TR>\n");
                        //DesignName
                        String designName = "Unknown";
                        String chassis = "Unknown";

                        if ((rse.getType() == UnitTypeEnum.SHIP) || (rse.getType() == UnitTypeEnum.PLATTFORM_DEFENSE)) {
                            designName = sdDAO.findById(rse.getDesignId()).getName();
                            chassis = chDAO.findById(sdDAO.findById(rse.getDesignId()).getChassis()).getName();
                        } else if (rse.getType() == UnitTypeEnum.SURFACE_DEFENSE) {
                            designName = ML.getMLStr(cDAO.findById(rse.getDesignId()).getName(), currUserId);
                            chassis = "combat_gen_lbl_construct";
                        }
                        msg.append("\t\t\t<TD>\n");
                        msg.append("\t\t\t\t");
                        msg.append(designName);
                        msg.append("\n");
                        msg.append("\t\t\t</TD>\n");
                        //Typ
                        msg.append("\t\t\t<TD>\n");
                        msg.append("\t\t\t\t");
                        msg.append(ML.getMLStr(chassis, userId));
                        msg.append("\n");
                        msg.append("\t\t\t</TD>\n");
                        //Anzahl
                        msg.append("\t\t\t<TD align=\"center\">\n");
                        msg.append("\t\t\t\t");
                        msg.append(rse.getBeforeCount());
                        msg.append("\n");
                        msg.append("\t\t\t</TD>\n");
                        //Verluste
                        msg.append("\t\t\t<TD align=\"center\">\n");
                        msg.append("\t\t\t\t");
                        msg.append(rse.getLoss());
                        msg.append("\n");
                        msg.append("\t\t\t</TD>\n");

                        msg.append("\t\t</TR>\n");
                    }
                }
                msg.append("\t</TABLE>\n");
                msg.append("\t<BR>\n");
            }
        }

        // msg = locString + msg.toString();
        return msg;


    }

    private TreeMap<Status, HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>> sortFleetList(HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>> singleCombat, int userId) {

        TreeMap<Status, HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>> sorted = new TreeMap<Status, HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>>();

        for (Map.Entry<Integer, HashMap<Integer, CombatReportFleetEntry>> entry : singleCombat.entrySet()) {
            Logger.getLogger().write("UserId : " + entry.getKey());
            HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>> userEntry = null;

            if (entry.getKey() == userId) {
                userEntry = sorted.get(Status.OWN);
                if (userEntry == null) {
                    userEntry = new HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>();
                }
                userEntry.put(entry.getKey(), entry.getValue());
                sorted.put(Status.OWN, userEntry);
            } else if (DiplomacyUtilities.getDiplomacyResult(entry.getKey(), userId).isHelps()) {
                userEntry = sorted.get(Status.ALLIED);
                if (userEntry == null) {
                    userEntry = new HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>();
                }
                userEntry.put(entry.getKey(), entry.getValue());
                sorted.put(Status.ALLIED, userEntry);
            } else {
                userEntry = sorted.get(Status.ENEMY);
                if (userEntry == null) {
                    userEntry = new HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>();
                }
                userEntry.put(entry.getKey(), entry.getValue());
                sorted.put(Status.ENEMY, userEntry);
            }
        }
        return sorted;
    }

    private SingleCombatReportEntry generateReport(CombatReportEntry cre) {

        ArrayList<Integer> participants = new ArrayList<Integer>();

        HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>> shipList = new HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>>();
        HashMap<Integer, String> nameMapping = new HashMap<Integer, String>();
        HashMap<Integer, ArrayList<CombatUnit>> shipDetailData = new HashMap<Integer, ArrayList<CombatUnit>>();

        for (CombatGroup cg : cre.getCombatResult()) {
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                ICombatParticipent activeCP = null;
                nameMapping.put(cgf.getFleetId(), cgf.getFleetName());

                for (ICombatParticipent cp : cre.getOriginalFleets()) {
                    if (cp.getId() == cgf.getFleetId()) {
                        activeCP = cp;
                        if (!participants.contains(cp.getUserId())) {
                            participants.add(cp.getUserId());
                        }
                        break;
                    }
                }

                ArrayList<ReportShipEntry> fleetShips = new ArrayList<ReportShipEntry>();

                // ArrayList<ShipData> ships = null; 
                // ArrayList<PlanetDefense> defense = null; 

                /*
                if (activeCP instanceof PlayerFleet) {
                ships = new PlayerFleetExt(activeCP.getId()).getShipList();
                } else if (activeCP instanceof ActiveDefenseSystem) {                    
                if (cgf.isPlanetDefense()) {                       
                defense = pdDAO.findByPlanetAndUserId(cre.getRelLocation().getPlanetId(), activeCP.getUserId());
                } else if (cgf.isSystemDefense()) {
                defense = pdDAO.findBySystemAndUserId(cre.getRelLocation().getSystemId(), activeCP.getUserId());
                }                    
                }
                 */

                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                    CombatGroupFleet.DesignDescriptor desc = entry.getKey();

                    if (desc.getType() == UnitTypeEnum.SHIP) {
                        // for (ShipData sd : ships) {
                        //     if (sd.getDesignId() == desc.getId()) {
                        ReportShipEntry rse = new ReportShipEntry(desc.getType(), desc.getId(), entry.getValue().getOrgCount(), entry.getValue().getSurvivors());
                        fleetShips.add(rse);
                        //     }
                        // }                        
                    } else {
                        ReportShipEntry rse = new ReportShipEntry(desc.getType(), desc.getId(), entry.getValue().getOrgCount(), entry.getValue().getSurvivors());
                        fleetShips.add(rse);
                    }
                }

                HashMap<Integer, CombatReportFleetEntry> fleets = shipList.get(activeCP.getUserId());
                if (fleets == null) {
                    fleets = new HashMap<Integer, CombatReportFleetEntry>();
                }

                CombatReportFleetEntry crfe = new CombatReportFleetEntry();
                crfe.addShips(fleetShips);

                Logger.getLogger().write("[CRG] CGF IS = " + cgf + " Name = " + cgf.getFleetName() +  " RESIGNED = " + cgf.isResigned());

                crfe.setResigned(cgf.isResigned());
                crfe.setRetreated(cgf.isRetreated());

                fleets.put(activeCP.getId(), crfe);
                shipList.put(activeCP.getUserId(), fleets);

                if (shipDetailData.containsKey(activeCP.getUserId())) {
                    ArrayList<CombatUnit> ddList = shipDetailData.get(activeCP.getUserId());
                    ddList.addAll(cgf.getShipDesign().values());
                } else {
                    ArrayList<CombatUnit> ddList = new ArrayList<CombatUnit>();
                    ddList.addAll(cgf.getShipDesign().values());
                    shipDetailData.put(activeCP.getUserId(), ddList);
                }
            }
        }
        //   printShipList(shipList);


        return new SingleCombatReportEntry(cre.getAbsLocation(), cre.getRelLocation(), shipList, participants, nameMapping, shipDetailData);
    }

    private void printShipList(HashMap<Integer, HashMap<Integer, ArrayList<ReportShipEntry>>> shipList) {
        for (Map.Entry<Integer, HashMap<Integer, ArrayList<ReportShipEntry>>> entry : shipList.entrySet()) {
            Logger.getLogger().write("||| UserId : " + entry.getKey());
            for (Map.Entry<Integer, ArrayList<ReportShipEntry>> entry2 : entry.getValue().entrySet()) {
                Logger.getLogger().write("|||||| FleetId : " + entry2.getKey());
                for (ReportShipEntry rse : entry2.getValue()) {
                    Logger.getLogger().write("||||||||| ShipDesign " + sdDAO.findById(rse.getDesignId()).getName() + " had " + (rse.getLoss()) + "/" + rse.getBeforeCount() + " losses");
                }
            }
        }
    }

    private enum Status {

        OWN, ALLIED, ENEMY
    }

    public class SingleCombatReportEntry {
        private AbsoluteCoordinate absLocation;
        private RelativeCoordinate relLocation;
        private HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>> shipList;
        private ArrayList<Integer> participants;
        private HashMap<Integer, String> fleetIdNameMapping;
        private HashMap<Integer, ArrayList<CombatUnit>> shipDamageData;

        public SingleCombatReportEntry(AbsoluteCoordinate absLocation, RelativeCoordinate relLocation, HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>> shipList, ArrayList<Integer> participants, HashMap<Integer, String> nameMapping, HashMap<Integer, ArrayList<CombatUnit>> shipDamageData) {
            this.absLocation = absLocation;
            this.relLocation = relLocation;
            this.shipList = shipList;
            this.participants = participants;
            this.fleetIdNameMapping = nameMapping;
            this.shipDamageData = shipDamageData;
        }

        public int getMinorDamageCountFor(int userId) {
            ArrayList<CombatUnit> cuList = shipDamageData.get(userId);

            int totalCount = 0;
            for (CombatUnit cu : cuList) {
                totalCount += cu.getMinorDamage();
            }

            return totalCount;
        }

        public int getLightDamageCountFor(int userId) {
            ArrayList<CombatUnit> cuList = shipDamageData.get(userId);

            int totalCount = 0;
            for (CombatUnit cu : cuList) {
                totalCount += cu.getLightDamage();
            }

            return totalCount;
        }

        public int getMediumDamageCountFor(int userId) {
            ArrayList<CombatUnit> cuList = shipDamageData.get(userId);

            int totalCount = 0;
            for (CombatUnit cu : cuList) {
                totalCount += cu.getMediumDamage();
            }

            return totalCount;
        }

        public int getHeavyDamageCountFor(int userId) {
            ArrayList<CombatUnit> cuList = shipDamageData.get(userId);

            int totalCount = 0;
            for (CombatUnit cu : cuList) {
                totalCount += cu.getHeavyDamage();
            }

            return totalCount;
        }

        public String getNameForId(int id) {
            return fleetIdNameMapping.get(id);
        }

        /**
         * @return the shipList
         */
        public HashMap<Integer, HashMap<Integer, CombatReportFleetEntry>> getShipList() {
            return shipList;
        }

        /**
         * @return the participants
         */
        public ArrayList<Integer> getParticipants() {
            return participants;
        }

        /**
         * @return the absLocation
         */
        public AbsoluteCoordinate getAbsLocation() {
            return absLocation;
        }

        /**
         * @return the relLocation
         */
        public RelativeCoordinate getRelLocation() {
            return relLocation;
        }
    }

    public class ReportShipEntry {

        private final UnitTypeEnum type;
        private final int designId;
        private final int beforeCount;
        private final int afterCount;

        public ReportShipEntry(UnitTypeEnum type, int designId, int beforeCount, int afterCount) {
            this.type = type;
            this.designId = designId;
            this.beforeCount = beforeCount;
            this.afterCount = afterCount;
        }

        public int getLoss() {
            return getBeforeCount() - getAfterCount();
        }

        /**
         * @return the designId
         */
        public int getDesignId() {
            return designId;
        }

        /**
         * @return the previousCount
         */
        public int getBeforeCount() {
            return beforeCount;
        }

        /**
         * @return the afterCount
         */
        public int getAfterCount() {
            return afterCount;
        }

        public UnitTypeEnum getType() {
            return type;
        }
    }
}
