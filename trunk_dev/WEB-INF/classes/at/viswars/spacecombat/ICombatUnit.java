/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.spacecombat;

/**
 *
 * @author Aion
 */
public interface ICombatUnit {
    public String getName();
    public int getCount();
    public float getHp();
    public int getDesignId();
    
    public boolean fire(boolean allowReturnFire);

    public void setParentCombatGroupId(int id);
    public int getParentCombatGroupId();
    public void setParentCombatGroupFleet(CombatGroupFleet cgf);
    public CombatGroupFleet getParentCombatGroupFleet();
    public double getDistraction();
    public int[] getDamageDistribution(int count, double temp_destroyed);
}
