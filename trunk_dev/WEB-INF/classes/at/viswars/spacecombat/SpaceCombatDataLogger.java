/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.Logger.Logger;
import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class SpaceCombatDataLogger implements Serializable {
    private HashMap<Integer,HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>>> allLoggedShips =
            new HashMap<Integer,HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>>>();

    public SC_DataEntry addNewShip(int userId, CombatGroupFleet.UnitTypeEnum ute, int designId) {
        Logger.getLogger().write("ADDING: USERID: " + userId + " UTE: " + ute.name() + " DESIGNID: " + designId);

        if (getAllLoggedShips().containsKey(userId)) {
            HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>> unitTypes = allLoggedShips.get(userId);

            if (unitTypes.containsKey(ute)) {
                HashMap<Integer,SC_DataEntry> shipDesigns = unitTypes.get(ute);

                if (shipDesigns.containsKey(designId)) {
                    // Ship is already Logged
                    return shipDesigns.get(designId);
                } else {
                    SC_DataEntry sc_DEntry = new SC_DataEntry(designId);
                    shipDesigns.put(designId, sc_DEntry);
                    return sc_DEntry;
                }
            } else {
                HashMap<Integer,SC_DataEntry> shipDesigns = new HashMap<Integer,SC_DataEntry>();
                SC_DataEntry sc_DEntry = new SC_DataEntry(designId);
                shipDesigns.put(designId, sc_DEntry);
                unitTypes.put(ute,shipDesigns);

                return sc_DEntry;
            }
        } else {
            SC_DataEntry sc_DEntry = new SC_DataEntry(designId);
            HashMap<Integer,SC_DataEntry> shipDesigns = new HashMap<Integer,SC_DataEntry>();
            shipDesigns.put(designId,sc_DEntry);

            HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>> unitTypes =
                new HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>>();

            unitTypes.put(ute, shipDesigns);
            allLoggedShips.put(userId,unitTypes);
            return sc_DEntry;
        }
    }

    public SC_DataEntry getShipLogger(int userId, CombatGroupFleet.UnitTypeEnum ute, int designId) {

        /*
        Logger.getLogger().write("USERID: " + userId + " UTE: " + ute.name() + " DESIGNID: " + designId);
        Logger.getLogger().write("1: " + allLoggedShips.get(userId));
        Logger.getLogger().write("2: " + allLoggedShips.get(userId).get(ute));
        */
        
        return allLoggedShips.get(userId).get(ute).get(designId);
    }

    /**
     * @return the allLoggedShips
     */
    public HashMap<Integer,HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>>> getAllLoggedShips() {
        return allLoggedShips;
    }
}
