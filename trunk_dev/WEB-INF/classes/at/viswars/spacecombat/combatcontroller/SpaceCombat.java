/*
 * SpaceCombat.java
 *
 * Created on 01. Mai 2004, 19:05
 */
package at.viswars.spacecombat.combatcontroller;

import at.viswars.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.admin.module.AttributeTree;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.UserDAO;
import java.sql.*;
import java.util.*;

import at.viswars.database.access.DbConnect;
import at.viswars.databuffer.RangeSizePenalty;
import at.viswars.diplomacy.combat.CombatGroupEntry;
import at.viswars.enumeration.EDamageLevel;
import at.viswars.enumeration.EShipType;
import at.viswars.model.Construction;
import at.viswars.model.Ressource;
import at.viswars.model.ShipDesign;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.spacecombat.helper.ArmorShieldFactor;
import at.viswars.spacecombat.BattleShip;
import at.viswars.spacecombat.CombatContext;
import at.viswars.spacecombat.CombatGroup;
import at.viswars.spacecombat.CombatGroupFleet;
import at.viswars.spacecombat.CombatGroupFleet.UnitTypeEnum;
import at.viswars.spacecombat.CombatHandler;
import at.viswars.spacecombat.CombatLogger;
import at.viswars.spacecombat.CombatUnit;
import at.viswars.spacecombat.DamagedShips;
import at.viswars.spacecombat.helper.FleetTracker;
import at.viswars.spacecombat.IBattleShip;
import at.viswars.spacecombat.ICombatUnit;
import at.viswars.spacecombat.IDefenseBuilding;
import at.viswars.spacecombat.SC_DataEntry;
import at.viswars.spacecombat.helper.ShieldDefinition;
import at.viswars.spacecombat.SpaceCombatDataLogger;
import at.viswars.spacecombat.helper.TargetList;
import at.viswars.utilities.DiplomacyUtilities;

/**
 *
 * @author  Rayd3n
 *
 * Generell werden pro Seite nur 10000 Raumschiffe zugelassen.
 * Wird diese Anzahl ?berschritten, wird die Zusammensetzung der Flotte analysiert und
 * eine "Reserve"-Liste erstellt. Die am Kampf beteiligte Flotte wird skaliert und ein
 * Skalierungsfaktor wird festgelegt, der sich auf die Zielerfassung der gegnerischen Schiffe
 * auswirkt. Ist zudem der Skalierungsfaktor einer Flotte kleiner als der der anderen, wird gr??erer
 * Schaden bei der unterlegenen Flotte erzeugt.
 * Wird ein Schiff aus der aktiven Liste zerst?rt, kommt ein Schiff aus der Reserveliste
 * nach.
 *
 */
class SpaceCombat extends AbstractSpaceCombat {
    private ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    /**
     * Wie lange darf ein Kampf h?chstens dauern (sprich: wie oft darf jedes Schiff
     * seine Waffen neu laden und abfeuern, bevor der Kampf zu ende ist)
     */
    private static final int MAX_SHOOT_ROUNDS = 1000;
    private int roundsDone = 0;        
    private List<FleetTracker> fleetTrackers = new ArrayList<FleetTracker>();
    private int noDamageCounter = 0;
    private final int PLANETARY_HU = 0;
    private final int PLANETARY_PA = 1;
    private ShieldDefinition planetaryShield;
    private int planetaryDefGroup = -1;
    private int processedCombatRounds = 0;
    // private static final HashMap<BattleShip, TargetList> targetMap = new HashMap<BattleShip, TargetList>();
    private HashMap<String, TargetList> targetMap = new HashMap<String, TargetList>();
    private HashMap<Integer,Integer> groupCount = new HashMap<Integer,Integer>();    

    private IBattleShip[][][] combatArray;

    /** Creates a new instance of SpaceCombat */
    public SpaceCombat(int combatLocation, int combatLocationId) {
        super(combatLocation,combatLocationId);

        // Logger.getLogger().write("SpaceCombat contructed!");
        targetMap.clear();
        atMap.clear();
        asfMap.clear();

        CombatContext.registerObject("ASFMAP", asfMap);
        RangeSizePenalty.initialize();
       
        // DebugBuffer.addLine("Construction finished successfully");
    }

    /*
    private void setGroupCount() {
        getGroupCount().clear();

        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                if (combatArray[i][j][0] == null) {
                    continue;

                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    if (combatArray[i][j][k] == null) {
                        continue;
                    }

                    if (getGroupCount().containsKey(i)) {
                        getGroupCount().put(i,getGroupCount().get(i) + (combatArray[i][j][k].getCount() * combatArray[i][j][k].getChassisSize()));
                    } else {
                        getGroupCount().put(i,combatArray[i][j][k].getCount() * combatArray[i][j][k].getChassisSize());
                    }
                }
            }
        }
    }    
    */

    @Override
    public void startFightCalc() {
        // Logger.getLogger().write("Starting Fight calculation!");
        long startTime = 0;
        long stopTime = 0;

        // Initialize Combat Array
        startTime = System.currentTimeMillis();
        initalizeCombatArray();
        stopTime = System.currentTimeMillis();
        Logger.getLogger().write(LogLevel.INFO, "Init Combat Array! [" + FormatUtilities.getFormattedNumber(stopTime - startTime) + " ms]");

        // Create shared Data for designs
        startTime = System.currentTimeMillis();
        initializeSharedData();
        stopTime = System.currentTimeMillis();
        Logger.getLogger().write(LogLevel.INFO, "Create shared data! [" + FormatUtilities.getFormattedNumber(stopTime - startTime) + " ms]");

        // Fill Combat Array
        startTime = System.currentTimeMillis();
        fillCombatArray();
        stopTime = System.currentTimeMillis();
        Logger.getLogger().write(LogLevel.INFO, "Fill Combat Array! [" + FormatUtilities.getFormattedNumber(stopTime - startTime) + " ms]");

        // Calculate Damage Shares
        startTime = System.currentTimeMillis();
        calculateDamageShares();
        stopTime = System.currentTimeMillis();
        Logger.getLogger().write(LogLevel.INFO, "Calculate Damage Shares! [" + FormatUtilities.getFormattedNumber(stopTime - startTime) + " ms]");
        // DebugBuffer.addLine("Schadensverteilung erfolgreich berechnet");

        fight();

        // Copy damage levels into LoggingData
        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                if (combatArray[i][j][0] == null) {
                    continue;

                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    if (combatArray[i][j][k] == null) {
                        continue;
                    }

                    BattleShip bs = (BattleShip)combatArray[i][j][k];
                    DamagedShips ds = bs.getDs();
                    int[] damageDistribution = ds.getTypeDist();

                    ShipDesign sd = sdDAO.findById(bs.getDesignId());
                    SC_DataEntry sc_DataEntry = sc_DataLogger.getShipLogger(sd.getUserId(),CombatGroupFleet.UnitTypeEnum.SHIP,bs.getDesignId());

                    sc_DataEntry.addDamagedShip(EDamageLevel.NODAMAGE, damageDistribution[0]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.MINORDAMAGE, damageDistribution[1]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.LIGHTDAMAGE, damageDistribution[2]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.MEDIUMDAMAGE, damageDistribution[3]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.HEAVYDAMAGE, damageDistribution[4]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.DESTROYED, damageDistribution[5]);
                }
            }
        }

        // Print out logged result
        Logger.getLogger().write("========== START OF DETAIL REPORT ==========");
        HashMap<Integer,HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>>> loggedShips = sc_DataLogger.getAllLoggedShips();
        for (Map.Entry<Integer,HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>>> meUser : loggedShips.entrySet()) {
            Logger.getLogger().write("========== DATA FOR USER " + uDAO.findById(meUser.getKey()).getGameName() + " (" + meUser.getKey() + ") ===========");

            HashMap<CombatGroupFleet.UnitTypeEnum,HashMap<Integer,SC_DataEntry>> unitTypeMap = meUser.getValue();
            HashMap<Integer, SC_DataEntry> designMap = unitTypeMap.get(CombatGroupFleet.UnitTypeEnum.SHIP);

            for (Map.Entry<Integer, SC_DataEntry> meDesign : designMap.entrySet()) {
                Logger.getLogger().write("=== DETAILS FOR SHIP: " + sdDAO.findById(meDesign.getKey()).getName() + " (" + meDesign.getKey() + ")");

                Logger.getLogger().write("Total damage received: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getDamageTaken(), 0));
                Logger.getLogger().write("Effective damage received: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getEffDamageTaken(), 0));
                Logger.getLogger().write(" ");
                Logger.getLogger().write("Damage blocked by Paratron: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getPA_DamageAbsorbed(), 0));
                Logger.getLogger().write("Damage blocked by HU: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getHU_DamageAbsorbed(), 0));
                Logger.getLogger().write("Damage blocked by Prallschirm: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getPS_DamageAbsorbed(), 0));
                Logger.getLogger().write(" ");
                Logger.getLogger().write("Damage going through Paratron: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getPA_DamagePenetrated(), 0));
                Logger.getLogger().write("Damage going through HU: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getHU_DamagePenetrated(), 0));
                Logger.getLogger().write("Damage going through Prallschirm: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getPS_DamagePenetrated(), 0));
                Logger.getLogger().write(" ");
                Logger.getLogger().write("Recharged Power for Paratron: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getPA_ShieldPointsRestored(), 0));
                Logger.getLogger().write("Recharged Power for HU: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getHU_ShieldPointsRestored(), 0));
                Logger.getLogger().write("Recharged Power Prallschirm: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getPS_ShieldPointsRestored(), 0));
                Logger.getLogger().write(" ");
                Logger.getLogger().write("Damage blocked by Steelarmor: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getST_DamageAbsorbed(), 0));
                Logger.getLogger().write("Damage blocked by Terkonitarmor: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getTE_DamageAbsorbed(), 0));
                Logger.getLogger().write("Damage blocked by Ynkenitarmor: " + FormatUtilities.getFormattedDecimal(meDesign.getValue().getYN_DamageAbsorbed(), 0));
                Logger.getLogger().write(" ");
                Logger.getLogger().write("NODAMAGE = " + meDesign.getValue().getCountForDamageLevel(EDamageLevel.NODAMAGE));
                Logger.getLogger().write("MINORDAMAGE = " + meDesign.getValue().getCountForDamageLevel(EDamageLevel.MINORDAMAGE));
                Logger.getLogger().write("LIGHTDAMAGE = " + meDesign.getValue().getCountForDamageLevel(EDamageLevel.LIGHTDAMAGE));
                Logger.getLogger().write("MEDIUMDAMAGE = " + meDesign.getValue().getCountForDamageLevel(EDamageLevel.MEDIUMDAMAGE));
                Logger.getLogger().write("HEAVYDAMAGE = " + meDesign.getValue().getCountForDamageLevel(EDamageLevel.HEAVYDAMAGE));
                Logger.getLogger().write("DESTROYED = " + meDesign.getValue().getCountForDamageLevel(EDamageLevel.DESTROYED));
            }
        }
        Logger.getLogger().write("========== END OF DETAIL REPORT ==========");

        updateFigures();
    }

    @Override
    public SpaceCombatDataLogger getLoggingData() {
        return sc_DataLogger;
    }

    private void fight() {
        Logger.getLogger().write(LogLevel.DEBUG, "Starting combat");
        // Loop through all ships and calculate Damage Values
        // - Loop runs till only one CombatGroup left
        // - if a Design looses 20% since last check re-calculate Damage allocation
        // - after all ships have fired all their weapons reset everything and reload shields
        // - in case of unarmed ships or no further progress -> send attacking fleet back
        // - and limit the Shooting-rounds to 1000, so that this
        // Logger.getLogger().write("--------------------------------------");    
        // Logger.getLogger().write("SPACECOMBAT");    
        // Logger.getLogger().write("--------------------------------------");    
        // Logger.getLogger().write("Fight started!");        

        int rounds = MAX_SHOOT_ROUNDS;
        boolean combatFinished = false;
        while ((!combatFinished) && (rounds > 0)) {
            Logger.getLogger().write(LogLevel.WARNING, "Processing combat round " + ((MAX_SHOOT_ROUNDS - rounds) + 1));
            Logger.getLogger().write(LogLevel.INFO, "Combat Round " + (processedCombatRounds + 1));
            // Logger.getLogger().write("Combat Round " + (processedCombatRounds + 1));
            boolean weaponFired = false;
            boolean allWeaponsFired = false;

            CombatLogger cl = CombatLogger.getInstance();

            // setGroupCount();

            for (int i = 0; i < noOfCombatGroups; i++) {
                for (int j = 0; j < highestNoOfFleets; j++) {
                    if (combatArray[i][j][0] == null) {
                        continue;

                    }
                    for (int k = 0; k < highestNoOfDesigns; k++) {
                        if (combatArray[i][j][k] == null) {
                            continue;

                        }
                        BattleShip bs = (BattleShip) combatArray[i][j][k];

                        cl.addLoggedShip(bs, 1001 - rounds);
                        if (bs.getDs() != null) {
                            // bs.getDs().printDistribution();
                        }
                    }
                }
            }

            while (!allWeaponsFired) {
                allWeaponsFired = true;

                for (int i = 0; i < noOfCombatGroups; i++) {
                    for (int j = 0; j < highestNoOfFleets; j++) {
                        if (combatArray[i][j][0] == null) {
                            continue;


                        }
                        boolean fleetCheckDone = false;

                        for (int k = 0; k < highestNoOfDesigns; k++) {
                            if (combatArray[i][j][k] == null) {
                                continue;


                            }
                            if (!fleetCheckDone) {
                                if (skip((BattleShip)combatArray[i][j][k])) {
                                    break;

                                }
                                fleetCheckDone = true;
                            }

                            IBattleShip bs = (BattleShip) combatArray[i][j][k];

                            bs.getShields().setReloaded(false);

                            if (roundsDone == 0) {
                                Logger.getLogger().write("Check if " + bs.getName() + " is Planetary Defense");
                                if (!bs.isPlanetaryDefense()) {
                                    Logger.getLogger().write("Check failed");
                                    continue;
                                }
                            }

                            if (roundsDone == 1) {
                                Logger.getLogger().write("Check if " + bs.getName() + " is Planetary Defense or Starbase");
                                if (!bs.isPlanetaryDefense() && !bs.isDefenseStation()) {
                                    Logger.getLogger().write("Check failed");
                                    continue;
                                }
                            }

                            // Logger.getLogger().write(LogLevel.INFO,"Currently Processing: ["+i+"]["+j+"]["+k+"]="+bs.getName());
                            if (bs.getCount() > 0) {
                                // Logger.getLogger().write("Count is ok - "+bs.getName()+" may fire weapon");
                                weaponFired = bs.fire(true) || weaponFired;

                                // DebugBuffer.addLine("Target Found? " + bs.isTargetFound());
                                if (!bs.hasTargetFound()) {
                                    // Logger.getLogger().write(LogLevel.INFO, "Check only 1 group left => " + only1GroupLeft());

                                    if (only1GroupLeft()) {
                                        cl.printLog();
                                        DebugBuffer.addLine(DebugLevel.DEBUG, "Return due to only one group left");
                                        printDamageLogs();
                                        Logger.getLogger().write(LogLevel.WARNING, "Finish combat due to only one group left");
                                        return;
                                    }

                                    for (WeaponModule wm : bs.getWeapons()) {
                                        wm.setFired(wm.getTotalCount());
                                    }
                                } else {
                                }

                                for (WeaponModule wm : bs.getWeapons()) {
                                    if (wm.getFired() < wm.getTotalCount()) {
                                        allWeaponsFired = false;
                                    }
                                }
                            } else {
                                // DebugBuffer.addLine("Count is 0 - continue");
                            }
                        }
                    }
                }
            }

            cl.printLog();

            processedCombatRounds++;
            rounds--;
            roundsDone++;

            Logger.getLogger().write(LogLevel.INFO, "Check retreat or resign");
            checkRetreatOrResign();

            if (!weaponFired) {
                Logger.getLogger().write(LogLevel.INFO, "(!weaponFired) Check only 1 group left => " + only1GroupLeft());

                if (only1GroupLeft()) {
                    // Logger.getLogger().write("Only 1 group left!");
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Return due to only one group left");
                    printDamageLogs();
                    Logger.getLogger().write(LogLevel.WARNING, "Finish combat due to only one group left");
                    return;
                }

//                Logger.getLogger().write("NEXT ROUND");

                if (!resetAllShips()) {
                    Logger.getLogger().write(LogLevel.INFO, "Check for return due to no damage");

                    noDamageCounter++;
                    // Logger.getLogger().write("NoDamageCounter = " + noDamageCounter);
                    if (noDamageCounter > 10) {
                        Logger.getLogger().write(LogLevel.WARNING, "Finish combat due to no damage");
                        DebugBuffer.addLine(DebugLevel.DEBUG, "Return due to no damage");
                        return;
                    }
                } else {
                    noDamageCounter = 0;
                }
            } else {
                if (!resetAllShips()) {
                    Logger.getLogger().write(LogLevel.INFO, "Check for return due to no damage 2");

                    noDamageCounter++;
                    // Logger.getLogger().write("NoDamageCounter = " + noDamageCounter);
                    if (noDamageCounter > 10) {
                        DebugBuffer.addLine(DebugLevel.DEBUG, "Return due to no damage");
                        Logger.getLogger().write(LogLevel.WARNING, "Finish combat due to no damage");
                        return;
                    }
                } else {
                    noDamageCounter = 0;
                }
            }
        }

        Logger.getLogger().write(LogLevel.WARNING, "Finish combat due to round limit reached");
        DebugBuffer.addLine(DebugLevel.DEBUG, "Round limit reached");
    }

    private void initializeSharedData() {
        for (int i = 0; i < combatGroups.size(); i++) {
            CombatGroup cg = (CombatGroup) combatGroups.get(i);
            // DebugBuffer.addLine("CG="+i+" NoOfFleets="+cg.getFleetCount()+"="+((ArrayList)cg.getFleetList()).size());

            for (int j = 0; j < cg.getFleetList().size(); j++) {
                CombatGroupFleet cgf = cg.getFleetList().get(j);

                // DebugBuffer.addLine("CGF="+j+" NoOfDesigns="+cgf.getShipDesign().size());
                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> me : cgf.getShipDesign().entrySet()) {
                    CombatGroupFleet.DesignDescriptor key = me.getKey();
                    int designId = key.getId();
                    Logger.getLogger().write(LogLevel.INFO, "Processing fleet: " + cgf.getFleetName() + " (" + cgf.getFleetId() + ") -- User: " + cgf.getUserIdStr());

                    ShipDesign sd = sdDAO.findById(designId);

                    if (cgf.isPlanetDefense() || cgf.isSystemDefense()) {
                        if (key.getType() == UnitTypeEnum.SURFACE_DEFENSE) {
                            if (!atMap.containsKey(0)) {
                                Logger.getLogger().write(LogLevel.INFO, "Create AT for " + 0);
                                atMap.put(0, new AttributeTree(cgf.getUserId(), 0l));
                                asfMap.put(0, new ArmorShieldFactor(7, atMap.get(0)));
                            } else {
                                Logger.getLogger().write(LogLevel.INFO, "Skipped AT creation for " + 0);
                            }
                        } else {
                            if (!atMap.containsKey(designId)) {
                                Logger.getLogger().write(LogLevel.INFO, "Create AT for " + designId + " TYPE: " + key.getType());
                                atMap.put(designId, new AttributeTree(cgf.getUserId(), sd.getDesignTime()));
                                asfMap.put(designId, new ArmorShieldFactor(sd.getChassis(), atMap.get(designId)));
                            } else {
                                Logger.getLogger().write(LogLevel.INFO, "Skipped AT creation for " + designId);
                            }
                        }

                        continue;
                    } else {
                        if (!atMap.containsKey(designId)) {
                            Logger.getLogger().write(LogLevel.INFO, "Create AT for " + designId);
                            atMap.put(designId, new AttributeTree(cgf.getUserId(), sd.getDesignTime()));
                            asfMap.put(designId, new ArmorShieldFactor(sd.getChassis(), atMap.get(designId)));
                        } else {
                            Logger.getLogger().write(LogLevel.INFO, "Skipped AT creation for " + designId);
                        }

                        continue;
                    }
                }
            }
        }
    }

    private void printDamageLogs() {
        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                if (combatArray[i][j][0] == null) {
                    continue;

                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    if (combatArray[i][j][k] == null) {
                        continue;

                    }
                    BattleShip bs = (BattleShip) combatArray[i][j][k];
                    if (bs.getDs() != null) {
                        Logger.getLogger().write("------------ DAMAGE LOG FOR " + bs.getName() + "--------------");
                        bs.getDs().printDistribution();
                    }
                }
            }
        }
    }

    protected int getProcessedCombatRounds() {
        return processedCombatRounds;
    }

    @Override
    public boolean skip(ICombatUnit icu) {
        /*
        if (bs.getFleetTracker() == null) {
            return false;

        }
        CombatGroupFleet cgf = bs.getFleetTracker().getCGF();

        // Logger.getLogger().write("Check fleet " + cgf.getFleetName() + " for resigned");
        if (cgf.isResigned() || cgf.isRetreated()) {
            // Logger.getLogger().write("Result: true");
            return true;
        }

        // Logger.getLogger().write("Result: false");
        */
        return false;        
    }

    // PROBLEM !!!! if a fleet retreats this has to be handled first
    // Otherwise calculation of attack power will not be done if only 
    // planetary defense is left
    private void checkRetreatOrResign() {
        // Logger.getLogger().write("CheckRetreatOrResign");

        boolean onlyPlanetDefLeft = onlyPlanetaryDefenseLeft();
        HashMap<Integer, Integer> accStrengthPerGroup = new HashMap<Integer, Integer>();

        for (FleetTracker ft : fleetTrackers) {
            CombatGroupFleet tmpCGF = ft.getCGFs().get(0);

            if (tmpCGF.isRetreated() || tmpCGF.isResigned()) {
                continue;
            }
            if ((ft.getDestroyedPercentage() > tmpCGF.getRetreatFactor()) && (tmpCGF.getRetreatFactor() < 100)) {
                // Set fleet retreated
                // Logger.getLogger().write("Fleet " + ft.getRelatedCGF().getFleetName() + " retreats! ("+ft.getDestroyedPercentage()+">"+tmpCGF.getRetreatFactor()+")");
                tmpCGF.setRetreated(true);
                checkRetreatOrResign();
                return;
            }

            // Summarize total Attack Power of CombatGroups 
            if (onlyPlanetDefLeft) {
                ft.calculateAttackPower();

                if (accStrengthPerGroup.containsKey(tmpCGF.getGroupId() - 1)) {
                    accStrengthPerGroup.put(tmpCGF.getGroupId(), accStrengthPerGroup.get(tmpCGF.getGroupId() - 1) + ft.getAttackPower());
                } else {
                    // Logger.getLogger().write("Adding combatGroup "+(tmpCGF.getGroupId()-1)+" To Strength Map (Initial Fleet: " + tmpCGF.getFleetName()+")");
                    accStrengthPerGroup.put(tmpCGF.getGroupId() - 1, ft.getAttackPower());
                }
            }
        }

        if (onlyPlanetDefLeft) {
            for (int i = 0; i < noOfCombatGroups; i++) {
                if (i != planetaryDefGroup) {
                    if (!accStrengthPerGroup.containsKey(i)) {
                        continue;


                    }
                    if (accStrengthPerGroup.get(i) < ((planetaryShield.getShield_HU() + planetaryShield.getShield_PA()) / 2)) {
                        // Set all fleets in this group to resigned!
                        CombatGroup cg = combatGroups.get(i);
                        List<CombatGroupFleet> fleets = cg.getFleetList();
                        for (CombatGroupFleet cgf : fleets) {
                            // Logger.getLogger().write("Set " + cgf.getFleetName() + " resigns!");
                            cgf.setResigned(true);
                        }
                    }
                }
            }
        }
    }

    /**
     * Alle Schiffe wieder in den Ursprungszustand zur?cksetzen
     * @return ob ?berhaupt ein Schiff besch?digt wurde (wenn nicht, hat das weitere Rechnen
     * 	keinen Sinn mehr)
     */
    private boolean resetAllShips() {
        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Reset Ships");
        boolean foundDamage = false;

        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                boolean fleetCheckDone = false;

                if (combatArray[i][j][0] == null) {
                    continue;

                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    if (combatArray[i][j][k] == null) {
                        continue;


                    }
                    if (!fleetCheckDone) {
                        if (skip((BattleShip)combatArray[i][j][k])) {
                            break;

                        }
                        fleetCheckDone = true;
                    }

                    BattleShip bs = (BattleShip) combatArray[i][j][k];
                    foundDamage |= bs.isTookDamage();
                    bs.reloadShieldsAndWeapons();
                    for (int l = 0; l < bs.getWeapons().size(); l++) {
                        ((WeaponModule) bs.getWeapons().get(l)).setTotalCount(((WeaponModule) bs.getWeapons().get(l)).getCount() * bs.getCount());
                        ((WeaponModule) bs.getWeapons().get(l)).setFired(0);
                    }
                }
            }
        }

        return foundDamage;
    }

    /**
     * Ermittelt ob auf der Verteidigerseite nur noch Planetare Verteidigung existiert  
     * bzw. ob nur noch das Schild existiert. Falls kein Schildvorhanden ist, wird immer
     * false retourniert     
     * @return Nur noch Verteidigungsanlagen oder Schild (zwingend) vorhanden
     */
    private boolean onlyPlanetaryDefenseLeft() {
        // Logger.getLogger().write("Check only Plantary Def Left");

        if (combatLocation == CombatHandler.SYSTEM_COMBAT) {
            return false;

        }
        if (planetaryShield == null) {
            return false;

        }
        if (planetaryShield.getShieldEnergy() == 0) {
            return false;

            // Find planetary surface Defense

        }
        boolean dataFound = false;

        // planetaryDefGroup = -1;
        ArrayList<Integer> groupHasNonSurface = new ArrayList<Integer>();

        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                boolean fleetCheckDone = false;

                if (combatArray[i][j][0] == null) {
                    continue;

                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    if (combatArray[i][j][k] == null) {
                        continue;

                    }
                    BattleShip bs = (BattleShip)combatArray[i][j][k];
                    if (bs.getCount() == 0) {
                        continue;


                    }
                    if (!fleetCheckDone) {
                        if (skip(bs)) {
                            break;

                        }
                        fleetCheckDone = true;
                    }

                    if (groupHasNonSurface.contains(bs.getFleetTracker().getCGFs().get(0).getGroupId() - 1) && planetaryDefGroup != -1) {
                        dataFound = true;
                        break;
                    }

                    if (!bs.isPlanetaryDefense()) {
                        // Logger.getLogger().write("Non Planet Def Unit Found for Group "+(bs.getFleetTracker().getRelatedCGF().getGroupId()-1)+"!");
                        groupHasNonSurface.add(bs.getFleetTracker().getCGFs().get(0).getGroupId() - 1);
                    } else {
                        // Logger.getLogger().write("Planet Def Group found for Group "+(bs.getFleetTracker().getRelatedCGF().getGroupId()-1)+"!");
                        planetaryDefGroup = bs.getFleetTracker().getCGFs().get(0).getGroupId() - 1;
                    }

                    if (planetaryDefGroup == -1) {

                        //##CHANGED
                        // if (AllianceUtilities.areAllied(bs.getFleetTracker().getRelatedCGF().getUserId(), pp.getUserId())) {
                        if (DiplomacyUtilities.getDiplomacyResult(bs.getFleetTracker().getCGFs().get(0).getUserId(), pp.getUserId()).isHelps()) {
                            // Logger.getLogger().write("Planet Def Group found for Group "+(bs.getFleetTracker().getRelatedCGF().getGroupId()-1)+"!");
                            planetaryDefGroup = bs.getFleetTracker().getCGFs().get(0).getGroupId() - 1;
                        }
                    }
                }
                if (dataFound) {
                    break;

                }
            }
            if (dataFound) {
                break;

            }
        }

        if (groupHasNonSurface.contains(planetaryDefGroup)) {
            // Logger.getLogger().write("RESULT: false");
            return false;
        } else {
            // Logger.getLogger().write("RESULT: true");
            return true;
        }

        /*
        for (FleetTracker ft : fleetTrackers) {
        CombatGroupFleet cgf = ft.getRelatedCGF();

        Map<DesignDescriptor,Integer> designs = cgf.getShipDesign();

        for (Map.Entry<DesignDescriptor,Integer> designEntry : designs.entrySet()) {
        if (designEntry.getKey().getType() != UnitTypeEnum.SURFACE_DEFENSE) {
        onlyPlanetaryDefense = false;
        break;
        } else {
        planetaryDefGroup = cgf.getGroupId();
        }
        }

        if (!onlyPlanetaryDefense) break;
        }
         */
    }

    private boolean only1GroupLeft() {
        // Check ob nur noch eine Kampfgruppe Schiffe enth?lt
        int CGWithShips = 0;

        for (int i = 0; i < noOfCombatGroups; i++) {
            int tempCount = 0;

            for (int j = 0; j < highestNoOfFleets; j++) {
                if (combatArray[i][j][0] == null) {
                    continue;

                }
                boolean fleetCheckDone = false;

                for (int k = 0; k < highestNoOfDesigns; k++) {
                    if (combatArray[i][j][k] == null) {
                        continue;


                    }
                    if (!fleetCheckDone) {
                        if (skip((BattleShip)combatArray[i][j][k])) {
                            break;

                        }
                        fleetCheckDone = true;
                    }

                    BattleShip bs = (BattleShip) combatArray[i][j][k];
                    // Logger.getLogger().write("Adding " + bs.getName() + " ("+bs.getCount()+") to survivors");
                    tempCount += bs.getCount();
                }
            }

            if (tempCount > 0) {
                CGWithShips++;
            } else {
                cgr.removeCombatGroupFromCompare(i + 1);
            }
        }

        // Check if existing combatgroups fight each other
        if (!cgr.hasConflictingParties()) {
            return true;
        }

        if (CGWithShips <= 1) {
            return true;
        }

        return false;
    }

    private void updateFigures() {
        int currCombatGroup = -1;

        for (int i = 0; i < combatGroups.size(); i++) {
            currCombatGroup++;
            int currFleet = -1;
            CombatGroup cg = (CombatGroup) combatGroups.get(i);
            for (int j = 0; j < cg.getFleetList().size(); j++) {
                currFleet++;
                CombatGroupFleet cgf = (CombatGroupFleet) cg.getFleetList().get(j);
                Map<CombatGroupFleet.DesignDescriptor, CombatUnit> sd = cgf.getShipDesign();

                // Loop through List and search for BattleShip
                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> me : sd.entrySet()) {
                    for (int k = 0; k < highestNoOfDesigns; k++) {
                        if (combatArray[currCombatGroup][currFleet][k] == null) {
                            continue;
                        }

                        BattleShip bs = (BattleShip) combatArray[currCombatGroup][currFleet][k];
                        if (bs.getDesignId() == me.getKey().getId()) {
                            me.getValue().setBattleShip(bs);
                            // sd.put(me.getKey(), bs.getCount());
                        }
                    }
                }
            }
        }
    }

    private void initalizeCombatArray() {
        noOfCombatGroups = combatGroups.size();
        highestNoOfFleets = 0;
        highestNoOfDesigns = 0;

        for (int i = 0; i < noOfCombatGroups; i++) {
            Logger.getLogger().write("I: " + i);

            CombatGroup cg = (CombatGroup) combatGroups.get(i);
            Logger.getLogger().write("CG FleetCount: " + cg.getFleetCount());
            if (cg.getFleetCount() > highestNoOfFleets) {
                highestNoOfFleets = cg.getFleetCount();
            }

            Logger.getLogger().write("CG FleetList: " + cg.getFleetList());
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                int designs = cgf.getShipDesign().size();
                if (designs > highestNoOfDesigns) {
                    highestNoOfDesigns = designs;
                }
            }
        }

        combatArray = new BattleShip[noOfCombatGroups][highestNoOfFleets][highestNoOfDesigns];
    }

    private void setPlanetaryShield() {
        planetaryShield = new ShieldDefinition();

        if (ppDAO.findById(combatLocationId) == null) {
            return;
        }

        // Load free Energy from Planet
        ProductionResult productionResult = null;
        try {
            PlanetCalculation pc = new PlanetCalculation(combatLocationId);
            productionResult = pc.getPlanetCalcData().getProductionData();
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, " Error while loading free energy" + e);
        }
        long freeEnergy = productionResult.getRessProduction(Ressource.ENERGY) - productionResult.getRessConsumption(Ressource.ENERGY);
        long shieldPower = 0;

        if (freeEnergy < 0) {
            freeEnergy = 0;


        }

        int shieldType = -1;

        if (pcDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETARY_HUESHIELD) != null) {
            shieldType = PLANETARY_HU;
            shieldPower = freeEnergy * 10;
        }
        if (pcDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETARY_PARATRONSHIELD) != null) {
            shieldType = PLANETARY_PA;
            shieldPower = freeEnergy * 15;
        }

        if (freeEnergy > 0) {
            if (shieldType == PLANETARY_HU) {
                planetaryShield.setShield_HU((int) shieldPower);
                planetaryShield.setSharedShield_HU((int) shieldPower);
                planetaryShield.setEnergyNeed_HU((int) freeEnergy);
                planetaryShield.setShieldEnergy((int) freeEnergy);
            } else if (shieldType == PLANETARY_PA) {
                planetaryShield.setShield_PA((int) shieldPower);
                planetaryShield.setSharedShield_PA((int) shieldPower);
                planetaryShield.setEnergyNeed_PA((int) freeEnergy);
                planetaryShield.setShieldEnergy((int) freeEnergy);
            }

            // Logger.getLogger().write("Planetary shield strength: " + planetaryShield.getShield_HU());
            // planetaryShield.setShieldEnergyPerShip(shieldEnergyConsumption);
            // planetaryShield.setShieldEnergy(shieldEnergyConsumption * count);
        }
    }

    private void fillCombatArray() {
        if (combatLocation == CombatHandler.ORBITAL_COMBAT) {
            setPlanetaryShield();
        }

        for (int i = 0; i < combatGroups.size(); i++) {
            CombatGroup cg = (CombatGroup) combatGroups.get(i);
            // DebugBuffer.addLine("CG="+i+" NoOfFleets="+cg.getFleetCount()+"="+((ArrayList)cg.getFleetList()).size());

            for (int j = 0; j < cg.getFleetList().size(); j++) {
                CombatGroupFleet cgf = cg.getFleetList().get(j);
                FleetTracker ft = FleetTracker.getFleetTracker(cgf);                

                int currDesign = 0;
                // DebugBuffer.addLine("CGF="+j+" NoOfDesigns="+cgf.getShipDesign().size());
                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> me : cgf.getShipDesign().entrySet()) {
                    if (cgf.isPlanetDefense() || cgf.isSystemDefense()) {
                        CombatGroupFleet.DesignDescriptor key = me.getKey();
                        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "type is " + key.getType());

                        int designId = key.getId();
                        int count = me.getValue().getOrgCount();
                        // int count = ((Integer) me.getValue()).intValue();

                        if (key.getType() == CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE) {
                            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Add Defense [" + designId + "] (on surface) to [" + i + "][" + j + "][" + currDesign + "]");
                            IDefenseBuilding idb = (IDefenseBuilding)mu.buildDefense(key, count, cgf.getUserId());
                            
                            if (planetaryShield != null) {
                                // TODO FIX SOMETHING !!!!!
                                // idb.setShields(planetaryShield);
                            }

                            // combatArray[i][j][currDesign].setFleetTracker(ft);
                            idb.setParentCombatGroupFleet(cgf);
                            // combatArray[i][j][currDesign] = idb;
                            // Logger.getLogger().write("Created planetary defense ("+combatArray[i][j][currDesign].getName()+") with primary: " + combatArray[i][j][currDesign].getPrimaryTarget());

                            currDesign++;
                        } else if (key.getType() == CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE) {
                            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Add Defense [" + designId + "] (orbital) to [" + i + "][" + j + "][" + currDesign + "]");
                            IBattleShip ibs = (IBattleShip)mu.buildDefense(key, count, cgf.getUserId());
                            ibs.setParentCombatGroupFleet(cgf);
                            combatArray[i][j][currDesign] = ibs;
                            // combatArray[i][j][currDesign].setFleetTracker(ft);                            
                            currDesign++;
                        }
                    } else {
                        int designId = me.getKey().getId();
                        int count = me.getValue().getOrgCount();
                        // int count = me.getValue();

                        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Add Ship to [" + i + "][" + j + "][" + currDesign + "]");
                        IBattleShip ibs = mu.buildBattleShip(designId, count, me.getValue().getShipFleetId());                        
                        ibs.setShipFleetId(me.getValue().getShipFleetId());
                        ibs.setSc_LoggingEntry(sc_DataLogger.addNewShip(cgf.getUserId(), CombatGroupFleet.UnitTypeEnum.SHIP, designId));
                        ibs.setParentCombatGroupId(i);
                        ibs.setParentCombatGroupFleet(cgf);
                        combatArray[i][j][currDesign] = ibs;

                        ft.setOrgCount(ft.getOrgCount() + combatArray[i][j][currDesign].getCount());
                        ft.setActCount(ft.getOrgCount());

                        ft.setOrgHp((int)(ft.getOrgHp() + (combatArray[i][j][currDesign].getHp() * combatArray[i][j][currDesign].getCount())));
                        ft.setActHp(ft.getOrgHp());

                        // combatArray[i][j][currDesign].setFleetTracker(ft);
                        fleetTrackers.add(ft);

                        currDesign++;
                    }
                }
            }
        }

        for (FleetTracker ft : fleetTrackers) {
            ft.setOrgAttackPower();
        }
    }

    private void calculateDamageShares() {
        Logger.getLogger().write(LogLevel.INFO, "NOCG: " + noOfCombatGroups + " HNOF: " + highestNoOfFleets + " HNOSD: " + highestNoOfDesigns);

        WeaponModule weapon = new WeaponModule();
        for (int i = 0; i < noOfCombatGroups; i++) {
            // DebugBuffer.addLine("Scan Battlegroup: " + i);
            for (int j = 0; j < highestNoOfFleets; j++) {
                // DebugBuffer.addLine("Scan Fleet: " + j);
                if (combatArray[i][j][0] == null) {
                    continue;
                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    // DebugBuffer.addLine("Scan ShipDesign: " + k);
                    if (combatArray[i][j][k] == null) {
                        continue;
                        // Current Attacker Found
                    }
                    BattleShip bs_att = (BattleShip) combatArray[i][j][k];
                    if (bs_att.getCount() == 0) {
                        continue;
                    }

                    int maxEnemyShipCount = noOfCombatGroups * highestNoOfFleets * highestNoOfDesigns;

                    String targetMapIdentifier = null;
                    if (bs_att.isPlanetaryDefense()) {
                        targetMapIdentifier = bs_att.getDesignId() + "a";
                    } else {
                        targetMapIdentifier = String.valueOf(bs_att.getDesignId());
                    }

                    if (!targetMap.containsKey(targetMapIdentifier)) {
                        TargetList tl = new TargetList();

                        tl.setTargetShip(new BattleShip[maxEnemyShipCount]);
                        tl.setTargetValue(new int[maxEnemyShipCount]);
                        tl.setTargetValuePerCount(new float[maxEnemyShipCount]);

                        // DebugBuffer.addLine("Found Attacker: " + bs_att.getName());
                        // DebugBuffer.addLine("Attached Modules: " + bs_att.getWeapons() + " COUNT: " + bs_att.getWeapons().size());

                        int totalAttackValue = 0;
                        int noOfTarget = -1;

                        // Loop through enemy ships and calculate targeting values
                        for (int i2 = 0; i2 < noOfCombatGroups; i2++) {
                            // DebugBuffer.addLine("Scan Battlegroup2: " + i2);
                            if (i2 == i) {
                                continue;
                            }
                            for (int j2 = 0; j2 < highestNoOfFleets; j2++) {
                                // DebugBuffer.addLine("Scan Fleet2: " + j2);
                                if (combatArray[i2][j2][0] == null) {
                                    continue;
                                }
                                for (int k2 = 0; k2 < highestNoOfDesigns; k2++) {
                                    // DebugBuffer.addLine("Scan ShipDesign2: " + k2);
                                    if (combatArray[i2][j2][k2] == null) {
                                        continue;
                                        // Found a defender Found - calculate SharePoints
                                    }

                                    // ****************** NEW *********************
                                    // Check if the current ship may attack target
                                    boolean doNotAttack = false;

                                    for (CombatGroupEntry cge : cgr.getCombatGroups()) {
                                        if (cge.getGroupId() == (i + 1)) {
                                            for (CombatGroupEntry cgeInner : cgr.getCombatGroups()) {
                                                if (cgeInner.getGroupId() == (i2 + 1)) {
                                                    if (!cge.getAttackingGroups().contains(i2 + 1)) {
                                                        doNotAttack = true;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (doNotAttack) {
                                                break;
                                            }
                                        }
                                    }

                                    if (doNotAttack) {
                                        continue;
                                    }
                                    // ********************************************

                                    BattleShip bs_def = (BattleShip) combatArray[i2][j2][k2];
                                    if (bs_def.getCount() == 0) {
                                        continue;
                                    }
                                    // DebugBuffer.addLine("Found Defender: " + bs_def.getName());

                                    // Base Figures
                                    noOfTarget++;
                                    float hitChance = weapon.getAccuracy();
                                    //Unused
//                                float hitBonus = 1;
                                    int countEnemy = bs_def.getCount();
                                    // DebugBuffer.addLine("DEF: "+bs_def.getChassisSize()+" ATT: " + bs_att.getPrimaryTarget());
                                    boolean isPrimary = bs_def.getChassisSize() == bs_att.getPrimaryTarget();
                                    //Unused
//                                boolean isSecondary = ((bs_att.getPrimaryTarget() - 1) == bs_def.getChassisSize()) || ((bs_att.getPrimaryTarget() + 1) == bs_def.getChassisSize());

                                    // Calculated Speed Difference
                                    float speedDiff = (100f / bs_def.getSpeed() * bs_att.getSpeed()) / 100f;

                                    // Bonus to primary due to fast speed or good scanning systems

                                    // Implementation of Damage Status of enemy Ship

                                    // Add Count of Ships to AttackValue
                                    // int attackValue = (countEnemy / (1 + (int)Math.pow(Math.log(countEnemy),2))) * getSizeFactor(bs_def.getChassisSize());
                                    /*
                                    int attackValue = countEnemy * getSizeFactor(bs_def.getChassisSize());

                                    // Calculate Primary Target Value
                                    float primaryBoost = (100 * getSizeFactor(bs_def.getChassisSize()));
                                    float primaryValue = 1;
                                    if (isPrimary) {
                                        primaryValue = 100 * primaryBoost;
                                    } else {
                                        primaryValue = (modFactorSecTargets(bs_def.getChassisSize(), bs_att.getPrimaryTarget()) * 100f) * primaryBoost;
                                        if (primaryValue < 1f) {
                                            primaryValue = 1;
                                        }
                                    }                                     
                                    */

                                    int attackValue = 100;
                                    // attackValue = (int) ((float) attackValue + primaryValue);

                                    ShipDesign sd = sdDAO.findById(bs_def.getDesignId());
                                    if (sd != null) {
                                        if ((sd.getType() == EShipType.TRANSPORT) || (sd.getType() == EShipType.COLONYSHIP)) {
                                            attackValue = (int) Math.ceil((double) attackValue / 45d);
                                        } else if (sd.getType() == EShipType.CARRIER) {
                                            attackValue = (int) Math.ceil((double) attackValue / 15d);
                                        }
                                    }

                                    totalAttackValue += attackValue;

                                    // Adjust hitChance
                                    hitChance = (int) ((float) hitChance * speedDiff);

                                    DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Add target " + bs_def.getName() + " for " + bs_att.getName() + " (" + attackValue + ")");

                                    tl.getTargetShip()[noOfTarget] = bs_def;
                                    tl.getTargetValue()[noOfTarget] = attackValue;
                                    tl.getTargetValuePerCount()[noOfTarget] = (float) attackValue / countEnemy;
                                }
                            }
                        }

                        tl.setTotalTargetValue(totalAttackValue);
                        bs_att.setTargetList(tl);
                        targetMap.put(targetMapIdentifier, tl);
                        Logger.getLogger().write(LogLevel.INFO, "Created new TargetIdentifier");
                    } else {
                        Logger.getLogger().write(LogLevel.INFO, "Use shared Identifier");
                        bs_att.setTargetList(targetMap.get(targetMapIdentifier));
                    }
                }
            }
        }

        Logger.getLogger().write(LogLevel.INFO, "Created " + targetMap.size() + " targeting lists");
    }

    @Deprecated
    private int getSizeFactor(int id) {
        double bonusValue = 0;

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT sr.engineFactor FROM sizerelation sr, module m WHERE sr.chassisId=m.id AND m.chassisSize=" + id);
            if (rs.next()) {
                bonusValue = Math.pow(1 + Math.log((double) rs.getInt(1)), 2);
            }
        } catch (Exception e) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.ERROR, "ERROR: " + e);
        }

        return (int) bonusValue;
    }

    @Deprecated
    private float modFactorSecTargets(int defId, int optId) {
        // Logger.getLogger().write("Called with " + defId + " and " + optId);

        float modFactor = 1;

        float optimalValue = 0;
        float defenderValue = 0;

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT sr.engineFactor FROM sizerelation sr, module m WHERE sr.chassisId=m.id AND m.chassisSize=" + optId);
            if (rs.next()) {
                optimalValue = rs.getInt(1);
            }
            rs = stmt.executeQuery("SELECT sr.engineFactor FROM sizerelation sr, module m WHERE sr.chassisId=m.id AND m.chassisSize=" + defId);
            if (rs.next()) {
                defenderValue = rs.getInt(1);
            }
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.ERROR, "ERROR: " + e);
        }

        if ((optimalValue != 0) && (defenderValue != 0)) {
            if (optimalValue > defenderValue) {
                modFactor = defenderValue / optimalValue;
            } else {
                modFactor = optimalValue / defenderValue;
            }
        } else {
            // DebugBuffer.addLine(DebugBuffer.DebugLevel.ERROR, "A value wasn't found!");
        }

        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "ModFactor of " + modFactor + " calculated");
        return modFactor;
    }

    @Override
    public void updateTargetLists() {
        for (TargetList tl : targetMap.values()) {
            tl.buildTotalTargetValue();
        }
    }

    @Override
    public TargetList getMyTargetList(BattleShip bs) {
        return targetMap.get(bs.toString());
    }

    /**
     * @return the groupCount
     */
    @Override
    public HashMap<Integer, Integer> getGroupCount() {
        return groupCount;
    }
}
