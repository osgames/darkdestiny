/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.ModuleDAO;
import at.viswars.enumeration.EShipType;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public abstract class AbstractBattleShip extends CombatUnit implements IBattleShip {
    // Basic Data
    protected int shipFleetId;
    protected float speed;
    protected EShipType purpose;    

    protected boolean isDefenseStation;
    protected boolean isPlanetaryDefense;

    // Combat Logging    
    protected ModuleDAO mDAO = (ModuleDAO)DAOFactory.get(ModuleDAO.class);

    public AbstractBattleShip() {
        super(0,0,0,CombatGroupFleet.UnitTypeEnum.SHIP);
        throw new UnsupportedOperationException("WTF MAN .. this is old stuff");
    }

    public AbstractBattleShip(int designId, int shipFleetId, int count) {
        super(designId,shipFleetId,count,CombatGroupFleet.UnitTypeEnum.SHIP);
    }

    public AbstractBattleShip(int designId, int count) {
        super(designId,count,CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE);
    }
}
