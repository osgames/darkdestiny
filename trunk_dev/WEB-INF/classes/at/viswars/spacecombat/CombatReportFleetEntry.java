/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.spacecombat.CombatReportGenerator.ReportShipEntry;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class CombatReportFleetEntry {
    private boolean resigned = false;
    private boolean retreated = false;
    private ArrayList<ReportShipEntry> fleetShips = new ArrayList<ReportShipEntry>();

    public void addShips(ArrayList<ReportShipEntry> entries) {
        getFleetShips().addAll(entries);
    }

    public void addShip(ReportShipEntry entry) {
        getFleetShips().add(entry);
    }

    /**
     * @return the resigned
     */
    public boolean isResigned() {
        return resigned;
    }

    /**
     * @param resigned the resigned to set
     */
    public void setResigned(boolean resigned) {
        this.resigned = resigned;
    }

    /**
     * @return the retreated
     */
    public boolean isRetreated() {
        return retreated;
    }

    /**
     * @param retreated the retreated to set
     */
    public void setRetreated(boolean retreated) {
        this.retreated = retreated;
    }

    /**
     * @return the fleetShips
     */
    public ArrayList<ReportShipEntry> getFleetShips() {
        return fleetShips;
    }
}
