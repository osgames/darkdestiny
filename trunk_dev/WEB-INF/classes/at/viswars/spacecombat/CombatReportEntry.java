/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.model.PlayerFleet;
import java.util.ArrayList;

/**
 *
 * @author Dreloc
 */
public class CombatReportEntry {
    private final CombatReportGenerator.CombatType type;
    private final RelativeCoordinate relLocation;
    private final AbsoluteCoordinate absLocation;
    private final ArrayList<ICombatParticipent> originalFleets;
    private final ArrayList<CombatGroup> combatResult;

    public CombatReportEntry(CombatReportGenerator.CombatType type,
            RelativeCoordinate relLocation,
            AbsoluteCoordinate absLocation,
            ArrayList<ICombatParticipent> originalFleets,
            ArrayList<CombatGroup> combatResult) {
        this.type = type;
        this.relLocation = relLocation;
        this.absLocation = absLocation;
        this.originalFleets = originalFleets;
        this.combatResult = combatResult;
    }

    /**
     * @return the type
     */
    public CombatReportGenerator.CombatType getType() {
        return type;
    }

    /**
     * @return the originalFleets
     */
    public ArrayList<ICombatParticipent> getOriginalFleets() {
        return originalFleets;
    }

    /**
     * @return the combatResult
     */
    public ArrayList<CombatGroup> getCombatResult() {
        return combatResult;
    }
    /**
     * @return the relLocation
     */
    public RelativeCoordinate getRelLocation() {
        return relLocation;
    }

    /**
     * @return the absLocation
     */
    public AbsoluteCoordinate getAbsLocation() {
        return absLocation;
    }
}
