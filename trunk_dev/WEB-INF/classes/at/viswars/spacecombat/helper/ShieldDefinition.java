/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat.helper;

/**
 *
 * @author Stefan
 */
public class ShieldDefinition implements Cloneable {
    private int shield_PS;
    private int shield_HU;
    private int shield_PA;    
    private int shieldEnergyPerShip;
    private int shieldEnergy;
    private int maxShieldEnergy;    
    private float sharedShield_PS;
    private float sharedShield_HU;
    private float sharedShield_PA;
    private float energyNeed_PA;
    private float energyNeed_HU;
    private float energyNeed_PS;
    private float sharedEnergyLevel;
    private float ps_EnergyLevel = 100f;
    private float hu_EnergyLevel = 100f;
    private float pa_EnergyLevel = 100f;

    private boolean reloaded = true;
    
    public int getShield_PS() {
        return shield_PS;
    }

    public void setShield_PS(int shield_PS) {
        this.shield_PS = shield_PS;
    }

    public int getShield_HU() {
        return shield_HU;
    }

    public void setShield_HU(int shield_HU) {
        this.shield_HU = shield_HU;
    }

    public int getShield_PA() {
        return shield_PA;
    }

    public void setShield_PA(int shield_PA) {
        this.shield_PA = shield_PA;
    }

    public int getShieldEnergyPerShip() {
        return shieldEnergyPerShip;
    }

    public void setShieldEnergyPerShip(int shieldEnergyPerShip) {
        this.shieldEnergyPerShip = shieldEnergyPerShip;
    }

    public int getShieldEnergy() {
        return shieldEnergy;
    }

    public void setShieldEnergy(int shieldEnergy) {
        this.shieldEnergy = shieldEnergy;
    }

    public int getMaxShieldEnergy() {
        return maxShieldEnergy;
    }

    public void setMaxShieldEnergy(int maxShieldEnergy) {
        this.maxShieldEnergy = maxShieldEnergy;
    }

    public float getSharedShield_PS() {
        return sharedShield_PS;
    }

    public void setSharedShield_PS(float sharedShield_PS) {
        this.sharedShield_PS = sharedShield_PS;
    }

    public float getSharedShield_HU() {
        return sharedShield_HU;
    }

    public void setSharedShield_HU(float sharedShield_HU) {
        this.sharedShield_HU = sharedShield_HU;
    }

    public float getSharedShield_PA() {
        return sharedShield_PA;
    }

    public void setSharedShield_PA(float sharedShield_PA) {
        this.sharedShield_PA = sharedShield_PA;
    }

    public float getEnergyNeed_PA() {
        return energyNeed_PA;
    }

    public void setEnergyNeed_PA(float energyNeed_PA) {
        this.energyNeed_PA = energyNeed_PA;
    }

    public float getEnergyNeed_HU() {
        return energyNeed_HU;
    }

    public void setEnergyNeed_HU(float energyNeed_HU) {
        this.energyNeed_HU = energyNeed_HU;
    }

    public float getEnergyNeed_PS() {
        return energyNeed_PS;
    }

    public void setEnergyNeed_PS(float energyNeed_PS) {
        this.energyNeed_PS = energyNeed_PS;
    }

    public float getSharedEnergyLevel() {
        return sharedEnergyLevel;
    }

    public void setSharedEnergyLevel(float sharedEnergyLevel) {
        this.sharedEnergyLevel = sharedEnergyLevel;
    }

    public float getPs_EnergyLevel() {
        return ps_EnergyLevel;
    }

    public void setPs_EnergyLevel(float ps_EnergyLevel) {
        this.ps_EnergyLevel = ps_EnergyLevel;
    }

    public float getHu_EnergyLevel() {
        return hu_EnergyLevel;
    }

    public void setHu_EnergyLevel(float hu_EnergyLevel) {
        this.hu_EnergyLevel = hu_EnergyLevel;
    }

    public float getPa_EnergyLevel() {
        return pa_EnergyLevel;
    }

    public void setPa_EnergyLevel(float pa_EnergyLevel) {
        this.pa_EnergyLevel = pa_EnergyLevel;
    }

    public boolean isReloaded() {
        return reloaded;
    }

    public void setReloaded(boolean reloaded) {
        this.reloaded = reloaded;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ShieldDefinition clone() {
        try {
            ShieldDefinition tmp = (ShieldDefinition)super.clone();

            // Deep clone Maps and Lists

            return tmp;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
