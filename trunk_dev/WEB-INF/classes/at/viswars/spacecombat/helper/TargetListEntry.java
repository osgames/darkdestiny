/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.spacecombat.helper;

import at.viswars.Logger.Logger;
import at.viswars.spacecombat.ICombatUnit;

/**
 *
 * @author Aion
 */
public class TargetListEntry {
    private ICombatUnit target;
    private double baseAttackProbability;
    private double relativeAttackProbability;
    private double targetFireIndicator;

    /**
     * @return the target
     */
    public ICombatUnit getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(ICombatUnit target) {
        this.target = target;
    }

    /**
     * @return the relativeAttackProbability
     */
    public double getRelativeAttackProbability() {
        return relativeAttackProbability;
    }

    /**
     * @param relativeAttackProbability the relativeAttackProbability to set
     */
    public void setRelativeAttackProbability(double relativeAttackProbability) {
        Logger.getLogger().write(this + " SET RAP: " + relativeAttackProbability);
        this.relativeAttackProbability = relativeAttackProbability;
    }

    /**
     * @return the targetFireIndicator
     */
    public double getTargetFireIndicator() {
        return targetFireIndicator;
    }

    /**
     * @param targetFireIndicator the targetFireIndicator to set
     */
    public void setTargetFireIndicator(double targetFireIndicator) {
        this.targetFireIndicator = targetFireIndicator;
    }

    /**
     * @return the baseAttackProbability
     */
    public double getBaseAttackProbability() {
        return baseAttackProbability;
    }

    /**
     * @param baseAttackProbability the baseAttackProbability to set
     */
    public void setBaseAttackProbability(double baseAttackProbability) {
        this.baseAttackProbability = baseAttackProbability;
    }
}
