/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat.helper;

import at.viswars.Logger.Logger;
import at.viswars.spacecombat.BattleShip;
import at.viswars.spacecombat.CombatContext;
import at.viswars.spacecombat.ISpaceCombat;

/**
 *
 * @author Stefan
 */
public class TargetList {
    private BattleShip[] targetShip;
    private int[] targetValue;
    private float[] targetValuePerCount;
    private int totalTargetValue;
    
    public BattleShip[] getTargetShip() {
        return targetShip;
    }

    public void setTargetShip(BattleShip[] targetShip) {
        this.targetShip = targetShip;
    }

    public int[] getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(int[] targetValue) {
        this.targetValue = targetValue;
    }

    public int getTotalTargetValue() {
        return totalTargetValue;
    }

    public void setTotalTargetValue(int totalTargetValue) {
        this.totalTargetValue = totalTargetValue;
    }

    public float[] getTargetValuePerCount() {
        return targetValuePerCount;
    }

    public void setTargetValuePerCount(float[] targetValuePerCount) {
        this.targetValuePerCount = targetValuePerCount;
    }
    
    public void buildTotalTargetValue() {
        totalTargetValue = 0;

        Logger.getLogger().write("Build TotalTargetValue");

        for (int i = 0; i < targetShip.length; i++) {
            Logger.getLogger().write("Looping ship " + targetShip[i].getName());

            if (((BattleShip) targetShip[i]) == null) {
                continue;
            }
            BattleShip tmpShp = (BattleShip) targetShip[i];

            ISpaceCombat isc = CombatContext.getCombatController();
            if (isc.skip(tmpShp)) {
                continue;
            }

            Logger.getLogger().write("Calculating target value for this ship TVpC: " + targetValuePerCount[i] + " * Count: " + tmpShp.getCount());;
            targetValue[i] = (int) (targetValuePerCount[i] * (float) tmpShp.getCount());
            if ((tmpShp.getCount() > 0) && (targetValue[i] == 0)) {
                 Logger.getLogger().write("Assign new targetValue: 1 (Something must have gone missing)");
                targetValue[i] = 1;
            }
            totalTargetValue += targetValue[i];
        }

        Logger.getLogger().write("Total Targetvalue = " + totalTargetValue);
    }    
}
