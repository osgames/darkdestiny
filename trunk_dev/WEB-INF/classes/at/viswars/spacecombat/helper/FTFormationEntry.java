/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat.helper;

/**
 *
 * @author Stefan
 */
public class FTFormationEntry extends FTEntry {
    private final int formationId;

    protected FTFormationEntry(int formationId) {
        this.formationId = formationId;
    }

    /**
     * @return the formationId
     */
    public int getFormationId() {
        return formationId;
    }
}
