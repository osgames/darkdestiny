/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.spacecombat.helper;

import at.viswars.Logger.Logger;
import at.viswars.spacecombat.AbstractCombatUnit;
import at.viswars.spacecombat.ICombatUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Aion
 */
public class TargetListController {
    private HashMap<ICombatUnit,ArrayList<TargetListEntry>> targetLists = new HashMap<ICombatUnit,ArrayList<TargetListEntry>>();
    
    public void register(ICombatUnit icu) {
        targetLists.put(icu, new ArrayList<TargetListEntry>());
    }
    
    public ArrayList<TargetListEntry> getList(ICombatUnit icu) {
        return targetLists.get(icu);
    }
    
    public void addTarget(ICombatUnit icu, ICombatUnit target, double probability, double targetFireIndicator) {
        TargetListEntry tle = new TargetListEntry();
        tle.setTarget(target);
        tle.setBaseAttackProbability(probability);
        tle.setRelativeAttackProbability(probability);
        tle.setTargetFireIndicator(targetFireIndicator);        
        
        targetLists.get(icu).add(tle);
    }

    public void updateTargetLists(ICombatUnit icu) {
        for (Iterator tleIt = targetLists.get(icu).iterator();tleIt.hasNext();) {
            TargetListEntry tle = (TargetListEntry)tleIt.next();
            // Logger.getLogger().write("["+tle+"] Looping a target: " + ((BattleShipNew)tle.getTarget()).getName());

            AbstractCombatUnit acu = (AbstractCombatUnit)tle.getTarget();
            if (acu.getParentCombatGroupFleet().isRetreated() ||
                acu.getParentCombatGroupFleet().isResigned()) {
                tleIt.remove();
                Logger.getLogger().write("Target was a coward");
                continue;
            }

            if (acu.getTempDestroyed() >= 1d) {
                tleIt.remove();
                Logger.getLogger().write("Target?? Which target?");
                continue;
            } else {
                Logger.getLogger().write("Setting a value: " + (acu.getCount() * (Math.max(0d,1d - acu.getTempDestroyed()))));
                // tle.setRelativeAttackProbability(acu.getCount() * (Math.max(0d,1d - acu.getTempDestroyed())));
                tle.setRelativeAttackProbability(tle.getBaseAttackProbability() * (Math.max(0d,1d - acu.getTempDestroyed())));
            }
        }
    }
}
