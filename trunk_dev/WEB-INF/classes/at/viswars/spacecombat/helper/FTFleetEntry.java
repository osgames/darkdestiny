/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat.helper;

/**
 *
 * @author Stefan
 */
public class FTFleetEntry extends FTEntry {
    private final int fleetId;

    protected FTFleetEntry(int fleetId) {
        this.fleetId = fleetId;
    }

    /**
     * @return the formationId
     */
    public int getFleetId() {
        return fleetId;
    }
}
