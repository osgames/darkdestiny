/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat.helper;

import at.viswars.spacecombat.BattleShip;

/**
 *
 * @author Stefan
 */
public class TargetIdentifier {
    private final BattleShip target;
    private final int targetValueId;
    
    public TargetIdentifier(BattleShip target, int targetValueId) {
        this.target = target;
        this.targetValueId = targetValueId;
    }

    public BattleShip getTarget() {
        return target;
    }

    public int getTargetValueId() {
        return targetValueId;
    }
}
