/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.spacecombat.helper;

import at.viswars.Logger.Logger;
import at.viswars.WeaponModule;
import at.viswars.spacecombat.AbstractCombatUnit;
import at.viswars.spacecombat.BattleShipNew;
import at.viswars.spacecombat.CombatGroupFleet;
import at.viswars.spacecombat.CombatGroupFleet.DesignDescriptor;
import at.viswars.spacecombat.CombatUnit;
import at.viswars.utilities.MilitaryUtilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class FleetTracker {
    private static HashMap<String,HashMap<FTEntry,FleetTracker>> threadSafeInstances =
            new HashMap<String,HashMap<FTEntry,FleetTracker>>();
    private int orgHp;
    private int orgCount;
    private float actHp;
    private int actCount;
    private int attackPower;
    private int orgAttackPower;
    private final int fleetFormation;
    
    private final ArrayList<CombatGroupFleet> cgfList = new ArrayList<CombatGroupFleet>();

    private FleetTracker(CombatGroupFleet cgf) {
        cgfList.add(cgf);
        fleetFormation = cgf.getFleetFormationId();
    }

    protected void addFleet(CombatGroupFleet cgf) {
        cgfList.add(cgf);
    }

    private boolean contains(CombatGroupFleet cgf) {
        return cgfList.contains(cgf);
    }

    public static Collection<FleetTracker> getAllFleetTrackers() {
        return threadSafeInstances.get(Thread.currentThread().toString()).values();
    }

    public static FleetTracker getFleetTracker(CombatGroupFleet cgf) {
        HashMap<FTEntry,FleetTracker> instances = threadSafeInstances.get(Thread.currentThread().toString());
        if (instances == null) {
            instances = new HashMap<FTEntry,FleetTracker>();

            FleetTracker ft = new FleetTracker(cgf);
            if (cgf.getFleetFormationId() != 0) {                                
                instances.put(new FTFormationEntry(cgf.getFleetFormationId()), ft);                
            } else {
                instances.put(new FTFleetEntry(cgf.getFleetId()), ft);    
            }

            threadSafeInstances.put(Thread.currentThread().toString(), instances);
            return ft;
        } else {
            if (cgf.getFleetFormationId() == 0) {
                for (Map.Entry<FTEntry,FleetTracker> me : instances.entrySet()) {
                    if (me.getKey() instanceof FTFleetEntry) {
                        if (((FTFleetEntry)me.getKey()).getFleetId() == cgf.getFleetId()) {
                            return instances.get(me.getKey());
                        }
                    }
                }

                // Nothing found so lets create a new Fleet Tracker
                FleetTracker ft = new FleetTracker(cgf);
                instances.put(new FTFleetEntry(cgf.getFleetId()), ft);
                return ft;
            } else {
                for (Map.Entry<FTEntry,FleetTracker> me : instances.entrySet()) {
                    if (me.getKey() instanceof FTFormationEntry) {
                        if (((FTFormationEntry)me.getKey()).getFormationId() == cgf.getFleetFormationId()) {
                            if (!me.getValue().contains(cgf)) {
                                me.getValue().addFleet(cgf);
                            }
                            return instances.get(me.getKey());
                        }
                    }
                }

                // Nothing found so lets create a new Fleet Tracker
                FleetTracker ft = new FleetTracker(cgf);
                instances.put(new FTFormationEntry(cgf.getFleetFormationId()), ft);
                return ft;
            }
        }
    }
    
    public float getDestroyedPercentage() {
        float perc = 0;

        // Get the percentage of destroyed Hitpoints
        float totalHp = 0;
        float hpLeft = 0;

        Logger.getLogger().write("Starting to sum up hitpoints");
        for (CombatGroupFleet cgf : cgfList) {
            for (Map.Entry<DesignDescriptor,CombatUnit> me : cgf.getShipDesign().entrySet()) {
                AbstractCombatUnit acu = (AbstractCombatUnit)me.getValue();
                totalHp += acu.getOrgCount() * acu.getHp();
                hpLeft += (1d - acu.getTempDestroyed()) * acu.getHp() * acu.getOrgCount();

                Logger.getLogger().write("totalHP = " + totalHp + " hpLeft = " + hpLeft + " after ship " + me.getValue().getName());
            }                        
        }

        float destroyedPerc = 100f - (100f / totalHp * hpLeft);
        Logger.getLogger().write("Destroyed Percentage for " + this + " is " + destroyedPerc);
        return destroyedPerc;
        
        /*
        Logger.getLogger().write("Calculate destroyed percentage");
        Logger.getLogger().write("OrgCount="+orgCount+" ActCount="+actCount);
        Logger.getLogger().write("OrgHp="+orgHp+" ActHp="+actHp);
        
        
        float percCount = 100f - (100f / (float) orgCount * (float) actCount);
        float percHp = 100f - (100f / (float) orgHp * actHp);
        float attackPowerPerc = 0;
        
        if (orgAttackPower != 0) {
            attackPowerPerc = 100f - (100f / (float)orgAttackPower * attackPower);
        }

        // Logger.getLogger().write("Values: Count="+percCount+" Hp="+percHp);
        if (orgAttackPower == 0) {
            perc = (percCount + percHp) / 2f;
        } else {
            perc = (((percCount + attackPowerPerc) / 2f) + percHp) / 2f;
        }
        
        if (percCount < 0.1f) return 0;
        
        return perc;
         */
    }

    public int getOrgHp() {
        return orgHp;
    }

    public void setOrgHp(int orgHp) {
        this.orgHp = orgHp;
    }

    public int getOrgCount() {
        return orgCount;
    }

    public void setOrgCount(int orgCount) {
        this.orgCount = orgCount;
    }

    @Deprecated
    public float getActHp() {
        return actHp;
    }

    @Deprecated
    public void setActHp(float actHp) {
        this.actHp = actHp;
    }

    @Deprecated
    public int getActCount() {
        return actCount;
    }

    @Deprecated
    public void setActCount(int actCount) {
        this.actCount = actCount;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void calculateAttackPower() {
        MilitaryUtilities mu = new MilitaryUtilities();

        int totalAttackPower = 0;

        for (CombatGroupFleet cgf : cgfList) {            
            Map<DesignDescriptor, CombatUnit> designs = cgf.getShipDesign();
            Logger.getLogger().write("Get attackpower for " + cgf.getFleetName() + " DesignCount = " + cgf.getShipDesign().size());
            for (Map.Entry<DesignDescriptor, CombatUnit> designEntry : designs.entrySet()) {
                // IBattleShip ibs = mu.buildBattleShipNew(designEntry.getKey().getId(), designEntry.getValue().getOrgCount(), 0);
                // BattleShip bs = mu.buildBattleShip(designEntry.getKey().getId(), designEntry.getValue().getOrgCount(),0);
                if (designEntry.getValue() instanceof BattleShipNew) {
                    BattleShipNew bsn = (BattleShipNew)designEntry.getValue();
                    ArrayList<WeaponModule> weaponList = bsn.getWeapons();

                    Logger.getLogger().write("BSN is " + bsn.getName() + " weaponListSize is " + bsn.getWeapons().size());

                    for (WeaponModule wm : weaponList) {
                        Logger.getLogger().write("Power of " + cgf.getFleetName() + " with weapon " + wm.getWeaponId() + " is " + ((double)wm.getAttackPower() * (double)wm.getCount() * (1d - bsn.getTempDestroyed())));
                        totalAttackPower += (int)((double)wm.getAttackPower() * (double)wm.getCount() * (1d - bsn.getTempDestroyed()));
                    }
                }
            }
        }

        Logger.getLogger().write("Total Attackpower is " + totalAttackPower);
        attackPower = totalAttackPower;
    }

    public ArrayList<CombatGroupFleet> getCGFs() {
        return cgfList;
    }

    public void setOrgAttackPower() {
        calculateAttackPower();
        orgAttackPower = attackPower;
    }

    public static void clearFleetTracker() {
        threadSafeInstances.remove(Thread.currentThread().toString());
    }
}
