/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat.helper;

import at.viswars.admin.module.AttributeTree;
import at.viswars.admin.module.ModuleAttributeResult;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.model.ShipDesign;

/**
 *
 * @author Stefan
 */
public class ArmorShieldFactor {
    private final AttributeTree at;

    private ModuleAttributeResult armYnke;
    private ModuleAttributeResult armTerk;
    private ModuleAttributeResult armSteel;
    private ModuleAttributeResult marPara;
    private ModuleAttributeResult marHU;
    private ModuleAttributeResult marPrall;

    private ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);

    public ArmorShieldFactor(int chassisSize, AttributeTree at) {
        this.at = at;

        armYnke = at.getAllAttributes(chassisSize, 22);
        armTerk = at.getAllAttributes(chassisSize, 21);
        armSteel = at.getAllAttributes(chassisSize, 2);

        marPara = at.getAllAttributes(chassisSize, 52);
        marHU = at.getAllAttributes(chassisSize, 51);
        marPrall = at.getAllAttributes(chassisSize, 50);
    }

    public ModuleAttributeResult getPSShield() {
        return marPrall;
    }

    public ModuleAttributeResult getHUShield() {
        return marHU;
    }

    public ModuleAttributeResult getPTShield() {
        return marPara;
    }

    public ModuleAttributeResult getSteelArmor() {
        return armSteel;
    }

    public ModuleAttributeResult getTerkArmor() {
        return armTerk;
    }

    public ModuleAttributeResult getYnkeArmor() {
        return armYnke;
    }
}
