/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat.helper;

import at.viswars.Logger.Logger;
import at.viswars.spacecombat.SC_DataEntry;

/**
 *
 * @author Stefan
 */
public class ShieldDefenseDetail {
    private double absPA = 0d;
    private double penPA = 0d;

    private double absHU = 0d;
    private double penHU = 0d;

    private double absPS = 0d;
    private double penPS = 0d;

    /**
     * @return the absPA
     */
    public double getAbsPA() {
        return absPA;
    }

    /**
     * @param absPA the absPA to set
     */
    public void setAbsPA(double absPA) {
        this.absPA = absPA;
    }

    /**
     * @return the penPA
     */
    public double getPenPA() {
        return penPA;
    }

    /**
     * @param penPA the penPA to set
     */
    public void setPenPA(double penPA) {
        this.penPA = penPA;
    }

    /**
     * @return the absHU
     */
    public double getAbsHU() {
        return absHU;
    }

    /**
     * @param absHU the absHU to set
     */
    public void setAbsHU(double absHU) {
        this.absHU = absHU;
    }

    /**
     * @return the penHU
     */
    public double getPenHU() {
        return penHU;
    }

    /**
     * @param penHU the penHU to set
     */
    public void setPenHU(double penHU) {
        this.penHU = penHU;
    }

    /**
     * @return the absPS
     */
    public double getAbsPS() {
        return absPS;
    }

    /**
     * @param absPS the absPS to set
     */
    public void setAbsPS(double absPS) {
        this.absPS = absPS;
    }

    public void scaleToTotalDamageReduction(double value, SC_DataEntry sc_LoggingEntry) {
        double totalAbsorbtion = absPA + absHU + absPS;
        double totalPenetration = penPA + penHU + penPS;

        Logger.getLogger().write("ABSPA: " + absPA + " ABSHU: " + absHU + " for " + sc_LoggingEntry.designId);

        if (totalAbsorbtion > 0d) sc_LoggingEntry.incPA_DamageAbsorbed(value * (1d / totalAbsorbtion * absPA));
        if (totalAbsorbtion > 0d) sc_LoggingEntry.incHU_DamageAbsorbed(value * (1d / totalAbsorbtion * absHU));
        if (totalAbsorbtion > 0d) sc_LoggingEntry.incPS_DamageAbsorbed(value * (1d / totalAbsorbtion * absPS));

        if (totalPenetration > 0d) sc_LoggingEntry.incPA_DamagePenetrated(value * (1d / totalPenetration * penPA));
        if (totalPenetration > 0d) sc_LoggingEntry.incHU_DamagePenetrated(value * (1d / totalPenetration * penHU));
        if (totalPenetration > 0d) sc_LoggingEntry.incPS_DamagePenetrated(value * (1d / totalPenetration * penPS));
    }

    /**
     * @return the penPS
     */
    public double getPenPS() {
        return penPS;
    }

    /**
     * @param penPS the penPS to set
     */
    public void setPenPS(double penPS) {
        this.penPS = penPS;
    }
}
