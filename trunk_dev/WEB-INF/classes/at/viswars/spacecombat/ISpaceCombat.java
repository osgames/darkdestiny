/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.spacecombat.helper.TargetList;
import at.viswars.diplomacy.combat.CombatGroupResult;
import at.viswars.interfaces.ISimulationSet;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Stefan
 */
public interface ISpaceCombat {
    public void startFightCalc();
    public void setCombatGroups(List<CombatGroup> combatGroups);
    public void setCombatGroupResult(CombatGroupResult cgr);
    public SpaceCombatDataLogger getLoggingData();

    public TargetList getMyTargetList(BattleShip bs);
    public void updateTargetLists();
    public HashMap<Integer, Integer> getGroupCount();
    public boolean skip(ICombatUnit icu);

    public void setISimulationSet(ISimulationSet iss);
    public ISimulationSet getISimulationSet();
}
