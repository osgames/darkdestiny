/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.spacecombat;

import at.viswars.Logger.Logger;
import at.viswars.test.AttackResolution;
import at.viswars.test.EnemyConnector;
import at.viswars.test.HelpResolution;
import at.viswars.test.PlayerNode;
import java.util.ArrayList;
import java.util.HashMap;
import at.viswars.test.Connector;
import at.viswars.utilities.DiplomacyUtilities;

/**
 *
 * @author Dreloc
 */
public class CombatUtilities {
    public static HashMap<Integer,ArrayList<Integer>> getCombatGroupedPlayers(ArrayList<Integer> players) {
        HashMap<Integer,ArrayList<Integer>> groupedPlayers = new HashMap<Integer,ArrayList<Integer>>();

        ArrayList<PlayerNode> pNodes = new ArrayList<PlayerNode>();
        ArrayList<PlayerNode> assignedNodes = new ArrayList<PlayerNode>();

        for (int player : players) {
            pNodes.add(new PlayerNode(player));
        }

        for (int i=0;i<pNodes.size();i++) {
            for (int j=i;j<pNodes.size();j++) {
                if (pNodes.get(i).getUserId() == pNodes.get(j).getUserId()) continue;
                setRelation(pNodes.get(i),pNodes.get(j),
                        DiplomacyUtilities.getDiplomacyRelation(
                        pNodes.get(i).getUserId(),
                        pNodes.get(j).getUserId()));
            }
        }

        int currGroup = 1;

        // Process relations
        while (assignedNodes.size() < pNodes.size()) {
            for (PlayerNode pn : pNodes) {
                Logger.getLogger().write("Masterprocess " + pn.getUserId());

                if (assignedNodes.contains(pn)) continue;

                if (pn.getGroup() == -1) {
                    assignedNodes.add(pn);
                    pn.setGroup(currGroup);
                    currGroup++;
                }

                ArrayList<PlayerNode> stepProcNodes = new ArrayList<PlayerNode>();
                stepProcNodes.add(pn);

                for (PlayerNode pnInner : pn.getRelationMap().keySet()) {
                    if (stepProcNodes.contains(pnInner)) continue;
                    if (pn.getRelationMap().get(pnInner) instanceof EnemyConnector) continue;
                    processNode(pnInner,pn,stepProcNodes,assignedNodes);
                }
            }
        }

        for (PlayerNode pn : pNodes) {
            if (groupedPlayers.containsKey(pn.getGroup())) {
                groupedPlayers.get(pn.getGroup()).add(pn.getUserId());
            } else {
                ArrayList<Integer> pList = new ArrayList<Integer>();
                pList.add(pn.getUserId());
                groupedPlayers.put(pn.getGroup(), pList);
            }
        }

        return groupedPlayers;
    }

    private static void processNode(PlayerNode currNode, PlayerNode masterNode, ArrayList<PlayerNode> stepProcNodes, ArrayList<PlayerNode> assignedNodes) {
        stepProcNodes.add(currNode);

        if (!contradicts(currNode,masterNode)) {
            /*
            if (hasSameEnemy(currNode,masterNode)) {
                Logger.getLogger().write(currNode.getUserId() + " shares " + masterNode.getUserId());
                currNode.setGroup(masterNode.getGroup());
                assignedNodes.add(currNode);
            }
            */
            Logger.getLogger().write(currNode.getUserId() + " shares " + masterNode.getUserId());
            currNode.setGroup(masterNode.getGroup());
            assignedNodes.add(currNode);
        } else {
            Logger.getLogger().write(currNode.getUserId() + " contradicts " + masterNode.getUserId());
        }

        for (PlayerNode pnInner : currNode.getRelationMap().keySet()) {
            if (stepProcNodes.contains(pnInner)) continue;
            if (currNode.getRelationMap().get(pnInner) instanceof EnemyConnector) continue;
            processNode(pnInner,masterNode,stepProcNodes,assignedNodes);
        }
    }

    private static boolean contradicts(PlayerNode pn1, PlayerNode pn2) {
        for (AttackResolution ar : pn1.getAttacks()) {
            if (ar.getUserId() == pn2.getUserId()) return true;

            for (HelpResolution hr : pn2.getHelps()) {
                if (ar.getUserId() == hr.getUserId()) return true;
            }
        }

        for (HelpResolution hr : pn1.getHelps()) {
            for (AttackResolution ar : pn2.getAttacks()) {
                if (ar.getUserId() == hr.getUserId()) return true;
            }
        }

        return false;
    }

    private static boolean hasSameEnemy(PlayerNode pn1, PlayerNode pn2) {
        for (AttackResolution ar : pn1.getAttacks()) {
            for (AttackResolution ar2 : pn2.getAttacks()) {
                if (ar.getUserId() == ar2.getUserId()) return true;
            }
        }

        return false;
    }

    private static void setRelation(PlayerNode side1, PlayerNode side2, Connector c) {
        side1.addRelation(side2, c);
        side2.addRelation(side1, c);
    }
    
    public static void buildArmorShieldFaktorMap() {
        
    }
}
