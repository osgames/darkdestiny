/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.spacecombat;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.ModuleType;
import at.viswars.WeaponModule;
import at.viswars.admin.module.AttributeTree;
import at.viswars.admin.module.ModuleAttributeResult;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.ConstructionDAO;
import at.viswars.dao.ConstructionModuleDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ModuleDAO;
import at.viswars.dao.ShipTypeAdjustmentDAO;
import at.viswars.enumeration.EAttribute;
import at.viswars.model.Chassis;
import at.viswars.model.Construction;
import at.viswars.model.ConstructionModule;
import at.viswars.model.Module;
import at.viswars.ships.Shield;
import at.viswars.spacecombat.helper.ShieldDefenseDetail;
import at.viswars.spacecombat.helper.ShieldDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Aion
 */
public class DefenseBuilding extends AbstractDefenseBuilding implements IDefenseBuilding {

    private ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private ChassisDAO chDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    public static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    public static ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);
    private static ConstructionModuleDAO cmDAO = (ConstructionModuleDAO) DAOFactory.get(ConstructionModuleDAO.class);
    private final Construction c;
    private final Chassis ch;
    private final int userId;

    public DefenseBuilding(int designId, int count, int userId) {
        super(designId, count);

        c = cDAO.findById(designId);
        ch = chDAO.findById(7);

        this.userId = userId;
        this.name = c.getName();
        this.count = count;
        this.chassisSize = 7;
        this.designId = designId;
        // this.purpose = sd.getType(); ?

        if (c.getId() == Construction.ID_PLANETARYLASERCANNON) {
            this.primaryTarget = 5;
        } else if (c.getId() == Construction.ID_PLANETARYMISSLEBASE) {
            this.primaryTarget = 2;
        } else if (c.getId() == Construction.ID_PLANETARY_INTERVALCANNON) {
            this.primaryTarget = 5;
        }

        this.restCount = this.orgCount;

        initializeAllModules();
    }

    private void initializeAllModules() {
        int groupingMultiplier = 1;

        // ShipTypeAdjustment sta = null; // staDAO.findByType(sd.getType());

        // Check trough modules
        ArrayList<WeaponModule> weaponArray = new ArrayList<WeaponModule>();
        int weaponEnergyConsumption = 0;
        int shieldEnergyConsumption = 0;
        int internalEnergyConsumption = 0;
        int armorValue = 0;
        int shieldHpPS = 0;
        int shieldHpHU = 0;
        int shieldHpPA = 0;
        int energyPS = 0;
        int energyHU = 0;
        int energyPA = 0;

        setHp(c.getHp());

        HashMap<Integer, Integer> modules = new HashMap<Integer, Integer>();
        ArrayList<ConstructionModule> cmList = cmDAO.findByConstruction(designId);
        for (ConstructionModule cm : cmList) {
            modules.put(cm.getModuleId(), cm.getCount());
        }

        //  rs = stmt.executeQuery("SELECT m.name, m.type, m.value, m.energy, m.chassisSize, m.isConstruct, dm.count, m.id FROM designmodule dm, module m WHERE dm.designId=" + designId + " AND m.id=dm.moduleId");

        int totalRange = 0;
        int weaponCount = 0;
        int maxTargetAllocation = 0;
        int fireSpeed = 0;

        for (Map.Entry<Integer, Integer> moduleEntry : modules.entrySet()) {
            Module m = mDAO.findById(moduleEntry.getKey());
            int modCount = moduleEntry.getValue();

            ModuleType type = m.getType();

            DebugBuffer.addLine(DebugLevel.DEBUG, "Check moduel " + m.getName() + " with id " + moduleEntry.getKey() + " and type " + m.getType());
            DebugBuffer.addLine(DebugLevel.DEBUG, "Type of corresponding moduleObjekt is null");
            if (type == ModuleType.ARMOR) {
                switch (moduleEntry.getKey()) {
                    case (2):
                        armorType = 1;
                        break;
                    case (21):
                        armorType = 2;
                        break;
                    case (22):
                        armorType = 3;
                        break;
                    case (23):
                        armorType = 4;
                        break;
                }
            } else if (type == ModuleType.WEAPON) {
                WeaponModule wm = new WeaponModule();
                Module mod = mDAO.findById(moduleEntry.getKey());

                AttributeTree at = new AttributeTree(userId, 0l);
                ModuleAttributeResult mar = at.getAllAttributes(7, m.getId());

                wm.setWeaponId(m.getId());
                wm.setMultifire((int) mar.getAttributeValue(EAttribute.MULTIFIRE));
                wm.setMultifired(0);

                fireSpeed += mar.getAttributeValue(EAttribute.MULTIFIRE) * modCount;

                maxTargetAllocation += wm.getMultifire() * modCount;

                wm.setRange((int) mar.getAttributeValue(EAttribute.RANGE));

                totalRange += wm.getRange() * modCount;
                weaponCount += modCount;

                DebugBuffer.addLine(DebugLevel.DEBUG, "Multifire = " + wm.getMultifire() + " for weapon " + m.getName());

                wm.setAttackPower((int) ((float) mar.getAttributeValue(EAttribute.ATTACK_STRENGTH) / wm.getMultifire()) * groupingMultiplier);
                wm.setAccuracy((float) mar.getAttributeValue(EAttribute.ACCURACY));
                wm.setCount(modCount * wm.getMultifire());
                wm.setTotalCount(modCount * count * wm.getMultifire());
                wm.setEnergyConsumption(0);
                weaponEnergyConsumption += wm.getEnergyConsumption() * wm.getCount();
                // DebugBuffer.addLine("Add weapons to " + bs.getName());

                weaponArray.add(wm);
            } else if (type == ModuleType.SHIELD) {
                // TODO
                Shield s = null; // sdd.getShield(moduleEntry.getKey());

                if (s != null) {
                    switch (moduleEntry.getKey()) {
                        case (50):
                            shieldEnergyConsumption += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                            shieldHpPS += (int) ((float) s.getDefenseStrength() * (float) modCount) * groupingMultiplier;
                            energyPS += (s.getEnergyConsumption() * modCount);
                            break;
                        case (51):
                            shieldEnergyConsumption += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                            shieldHpHU += (int) ((float) s.getDefenseStrength() * (float) modCount) * groupingMultiplier;
                            energyHU += (s.getEnergyConsumption() * modCount);
                            break;
                        case (52):
                            shieldEnergyConsumption += (s.getEnergyConsumption() * modCount) * groupingMultiplier;
                            shieldHpPA += (int) ((float) s.getDefenseStrength() * (float) modCount) * groupingMultiplier;
                            energyPA += (s.getEnergyConsumption() * modCount);
                            break;
                    }
                }
            } else if (false) { // 9??
                // FREE
            }
        }

        // setMaxTargetAllocation(maxTargetAllocation);
        weapons = weaponArray;
        // setAverageRange((float) totalRange / weaponCount);
        // setFireSpeed(fireSpeed);
        // setSizeLevel((float) Math.log10(sdd.getBase().getStructure()));
        // bs.setHp(bs.getHp() + (bs.getHp() / 100 * armorValue));

        ShieldDefinition sDef = new ShieldDefinition();

        sDef.setShield_PS(shieldHpPS);
        sDef.setShield_HU(shieldHpHU);
        sDef.setShield_PA(shieldHpPA);
        sDef.setSharedShield_PS(sDef.getShield_PS() * count);
        sDef.setSharedShield_HU(sDef.getShield_HU() * count);
        sDef.setSharedShield_PA(sDef.getShield_PA() * count);
        sDef.setEnergyNeed_PS(energyPS);
        sDef.setEnergyNeed_HU(energyHU);
        sDef.setEnergyNeed_PA(energyPA);

        totalHp = c.getHp() * count;

        sDef.setShieldEnergyPerShip(shieldEnergyConsumption);
        sDef.setShieldEnergy(shieldEnergyConsumption * count);

        shields = sDef;
    }

    @Override
    public String getName() {
        return c.getName();
    }

    @Override
    public int getCount() {
        return count;
    }
    
    @Override
    protected double getReductionThroughShieldsNew(AbstractCombatUnit abs, AbstractCombatUnit absAtt, WeaponModule wm, int maxHitable, HashMap<Integer, Double> detailDamageAtt, ShieldDefenseDetail sdd) {
        double reduction = 0d;
        double damageAttacker = detailDamageAtt.get(wm.getWeaponId());
        double damageAttOrg = damageAttacker;

        HashMap<Double, Double> weightedReductionMap = new HashMap<Double, Double>();

        // float psShieldStr = abs.getShields().getSharedShield_PS();
        float huShieldStr = abs.getShields().getSharedShield_HU();
        float paShieldStr = abs.getShields().getSharedShield_PA();           

        Logger.getLogger().write("["+abs.getName()+"] PA Value is " + abs.getShields().getShield_PA() + " Shared Shield Value is " + abs.getShields().getSharedShield_PA());
        Logger.getLogger().write("["+abs.getName()+"] HU Value is " + abs.getShields().getShield_HU() + " Shared Shield Value is " + abs.getShields().getSharedShield_HU());        
        
        double relShieldStrPA = paShieldStr;
        double relShieldStrHU = huShieldStr;
        // double relShieldStrPS = psShieldStr * concentrationModifier;

        // System.out.println("RelPS: " + relShieldStrPS);                    
        double currReduction = 0d;

        // double absorbPS = abs.prallPenetration.get(wm.getWeaponId());
        double absorbHU = 1d * Math.min(1d,((relShieldStrHU * 1.2d) / abs.getShields().getShield_HU()) * abs.huPenetration.get(wm.getWeaponId())); // abs.huPenetration.get(wm.getWeaponId());
        double absorbPA = 1d * Math.min(1d,((relShieldStrPA * 1.2d) / abs.getShields().getShield_PA()) * abs.paraPenetration.get(wm.getWeaponId())); // abs.paraPenetration.get(wm.getWeaponId());

        double wastedDamage = 0d;

        if (paShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbPA;

            if (absorbedDamage > relShieldStrPA) {
                // abs.sc_LoggingEntry.incPA_DamagePenetrated(absorbedDamage - relShieldStrPA);
                absorbedDamage = relShieldStrPA;
            }

            // abs.sc_LoggingEntry.incPA_DamageAbsorbed(absorbedDamage);
            Logger.getLogger().write("[PA] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrPA + " (Abs. Damage: " + absorbedDamage + " / absorbPA: " + absorbPA + ")");


            relShieldStrPA -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsPA(sdd.getAbsPA() + damageAttacker);
                abs.getShields().setSharedShield_PA((float) abs.getShields().getSharedShield_PA() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, currReduction + adjust * absorbPA);
                Logger.getLogger().write("CurrReduction: " + currReduction + " Adjust: " + adjust);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                abs.getShields().setSharedShield_PA((float) abs.getShields().getSharedShield_PA() - effAbsorbedDamage);
                // sdd.setAbsPA(sdd.getAbsPA() + effAbsorbedDamage);
                sdd.setAbsPA(sdd.getAbsPA() + absorbedDamage);
                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenPA(sdd.getPenPA() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // abs.sc_LoggingEntry.incPA_DamagePenetrated(damageAttacker);
            }
        }

        if (huShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbHU;

            if (absorbedDamage > relShieldStrHU) {
                // abs.sc_LoggingEntry.incHU_DamagePenetrated(absorbedDamage - relShieldStrHU);
                absorbedDamage = relShieldStrHU;
            }

            Logger.getLogger().write("[HU] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrHU + " (Abs. Damage: " + absorbedDamage + " / absorbHU: " + absorbHU + ")");


            // abs.sc_LoggingEntry.incHU_DamageAbsorbed(absorbedDamage);
            relShieldStrHU -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsHU(sdd.getAbsHU() + damageAttacker);
                abs.getShields().setSharedShield_HU((float) abs.getShields().getSharedShield_HU() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, currReduction + adjust * absorbHU);
                // currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbHU);
                Logger.getLogger().write("CurrReduction: " + currReduction + " Adjust: " + adjust);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                abs.getShields().setSharedShield_HU((float) abs.getShields().getSharedShield_HU() - effAbsorbedDamage);
                // sdd.setAbsHU(sdd.getAbsHU() + effAbsorbedDamage);
                sdd.setAbsHU(sdd.getAbsHU() + absorbedDamage);
                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenHU(sdd.getPenHU() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // sdd.setPenHU(sdd.getPenHU() + damageAttacker);

                // abs.sc_LoggingEntry.incHU_DamagePenetrated(damageAttacker);
            }
        }

        /*
        if (psShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbPS;

            if (absorbedDamage > relShieldStrPS) {
                // abs.sc_LoggingEntry.incPS_DamagePenetrated(absorbedDamage - relShieldStrPS);
                absorbedDamage = relShieldStrPS;
            }

            // abs.sc_LoggingEntry.incPS_DamageAbsorbed(absorbedDamage);
            relShieldStrPS -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsPS(sdd.getAbsPS() + damageAttacker);
                abs.getShields().setSharedShield_PS((float) abs.getShields().getSharedShield_PS() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, adjust * absorbHU);
                // currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbPS);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                abs.getShields().setSharedShield_PS((float) abs.getShields().getSharedShield_PS() - effAbsorbedDamage);
                // sdd.setAbsPS(sdd.getAbsPS() + effAbsorbedDamage);
                sdd.setAbsPS(sdd.getAbsPS() + absorbedDamage);
                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenPS(sdd.getPenPS() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // sdd.setPenPS(sdd.getPenPS() + damageAttacker);                
                // abs.sc_LoggingEntry.incPS_DamagePenetrated(damageAttacker);
            }
        }
        */
        System.out.println("---------------------------------------- END ---------------------------------------");
        
        return currReduction;
    }
    
    /*
    private double getReductionThroughShields(AbstractCombatUnit abs, AbstractCombatUnit absAtt, double damageAttackerOrg, HashMap<Integer, Double> detailDamageAtt, ShieldDefenseDetail sdd) {                                               
        double reduction = 0d;
        double damageAttacker = damageAttackerOrg;
        HashMap<Double, Double> weightedReductionMap = new HashMap<Double, Double>();

        double attackerDistraction = absAtt.getDistraction();
        double concentrationModifier = 0.2d + (0.8d / 1d * attackerDistraction);

        for (WeaponModule wm : absAtt.getWeapons()) {
            float psShieldStr = abs.getShields().getSharedShield_PS();
            float huShieldStr = abs.getShields().getSharedShield_HU();
            float paShieldStr = abs.getShields().getSharedShield_PA();

            System.out.println("Processing attacked unit " + abs.getName());
            System.out.println("PS: " + psShieldStr);
            System.out.println("HU: " + huShieldStr);
            System.out.println("PA: " + paShieldStr);            
            
            double relShieldStrPA = paShieldStr / abs.getCount() * concentrationModifier;
            double relShieldStrHU = huShieldStr / abs.getCount() * concentrationModifier;
            double relShieldStrPS = psShieldStr / abs.getCount() * concentrationModifier;

            System.out.println("RelPS: " + relShieldStrPS);
            System.out.println("RelHU: " + relShieldStrHU);
            System.out.println("RelPA: " + relShieldStrPA);               
            
            double currReduction = 0d;

            double absorbPS = abs.prallPenetration.get(wm.getWeaponId());
            double absorbHU = abs.huPenetration.get(wm.getWeaponId());
            double absorbPA = abs.paraPenetration.get(wm.getWeaponId());

            double wastedDamage = 0d;

            if (paShieldStr > 0) {
                double absorbedDamage = damageAttacker * absorbPA;

                if (absorbedDamage > relShieldStrPA) {
                    // abs.sc_LoggingEntry.incPA_DamagePenetrated(absorbedDamage - relShieldStrPA);
                    absorbedDamage = relShieldStrPA;
                }

                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(absorbedDamage);
                Logger.getLogger().write("[PA] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrPA + " (Abs. Damage: " + absorbedDamage + " / absorbPA: " + absorbPA + ")");


                relShieldStrPA -= absorbedDamage;
                double adjust = 1d / damageAttacker * absorbedDamage;

                if (damageAttacker <= absorbedDamage) {
                    currReduction = 1d;
                    sdd.setAbsPA(sdd.getAbsPA() + damageAttacker);
                    abs.getShields().setSharedShield_PA((float) abs.getShields().getSharedShield_PA() - (float) damageAttacker);
                    // abs.sc_LoggingEntry.incPA_DamageAbsorbed(damageAttacker);
                    damageAttacker = 0;
                } else {
                    currReduction = Math.max(currReduction, (1d - ((1d - adjust) / 2d)) * absorbPA);
                    Logger.getLogger().write("CurrReduction: " + currReduction + " Adjust: " + adjust);
                    float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                    abs.getShields().setSharedShield_PA((float) abs.getShields().getSharedShield_PA() - effAbsorbedDamage);
                    sdd.setAbsPA(sdd.getAbsPA() + effAbsorbedDamage);
                    // abs.sc_LoggingEntry.incPA_DamageAbsorbed(effAbsorbedDamage);
                    damageAttacker -= effAbsorbedDamage;
                    sdd.setPenPA(sdd.getPenPA() + damageAttacker);
                    // abs.sc_LoggingEntry.incPA_DamagePenetrated(damageAttacker);
                }
            }

            if (huShieldStr > 0) {
                double absorbedDamage = damageAttacker * absorbHU;

                if (absorbedDamage > relShieldStrHU) {
                    // abs.sc_LoggingEntry.incHU_DamagePenetrated(absorbedDamage - relShieldStrHU);
                    absorbedDamage = relShieldStrHU;
                }

                Logger.getLogger().write("[HU] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrHU + " (Abs. Damage: " + absorbedDamage + " / absorbHU: " + absorbHU + ")");


                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(absorbedDamage);
                relShieldStrHU -= absorbedDamage;
                double adjust = 1d / damageAttacker * absorbedDamage;

                if (damageAttacker <= absorbedDamage) {
                    currReduction = 1d;
                    sdd.setAbsHU(sdd.getAbsHU() + damageAttacker);
                    abs.getShields().setSharedShield_HU((float) abs.getShields().getSharedShield_HU() - (float) damageAttacker);
                    // abs.sc_LoggingEntry.incHU_DamageAbsorbed(damageAttacker);
                    damageAttacker = 0;
                } else {
                    currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbHU);
                    Logger.getLogger().write("CurrReduction: " + currReduction + " Adjust: " + adjust);
                    float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                    abs.getShields().setSharedShield_HU((float) abs.getShields().getSharedShield_HU() - effAbsorbedDamage);
                    sdd.setAbsHU(sdd.getAbsHU() + effAbsorbedDamage);
                    // abs.sc_LoggingEntry.incHU_DamageAbsorbed(effAbsorbedDamage);
                    damageAttacker -= effAbsorbedDamage;
                    sdd.setPenHU(sdd.getPenHU() + damageAttacker);
                    // abs.sc_LoggingEntry.incHU_DamagePenetrated(damageAttacker);
                }
            }

            if (psShieldStr > 0) {
                double absorbedDamage = damageAttacker * absorbPS;

                if (absorbedDamage > relShieldStrPS) {
                    // abs.sc_LoggingEntry.incPS_DamagePenetrated(absorbedDamage - relShieldStrPS);
                    absorbedDamage = relShieldStrPS;
                }

                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(absorbedDamage);
                relShieldStrPS -= absorbedDamage;
                double adjust = 1d / damageAttacker * absorbedDamage;

                if (damageAttacker <= absorbedDamage) {
                    currReduction = 1d;
                    sdd.setAbsPS(sdd.getAbsPS() + damageAttacker);
                    abs.getShields().setSharedShield_PS((float) abs.getShields().getSharedShield_PS() - (float) damageAttacker);
                    // abs.sc_LoggingEntry.incPS_DamageAbsorbed(damageAttacker);
                    damageAttacker = 0;
                } else {
                    currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbPS);
                    float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                    abs.getShields().setSharedShield_PS((float) abs.getShields().getSharedShield_PS() - effAbsorbedDamage);
                    sdd.setAbsPS(sdd.getAbsPS() + effAbsorbedDamage);
                    // abs.sc_LoggingEntry.incPS_DamageAbsorbed(effAbsorbedDamage);
                    damageAttacker -= effAbsorbedDamage;
                    sdd.setPenPS(sdd.getPenPS() + damageAttacker);
                    // abs.sc_LoggingEntry.incPS_DamagePenetrated(damageAttacker);
                }
            }

            double share = 1d / damageAttackerOrg * detailDamageAtt.get(wm.getWeaponId());
            Logger.getLogger().write("Adding weapon " + wm.getWeaponId() + " with share: " + share + " as DA=" + damageAttackerOrg + " and DDA=" + detailDamageAtt.get(wm.getWeaponId()));
            weightedReductionMap.put(share, currReduction);
        }

        // Calculate Reduction from weighted reduction
        for (Map.Entry<Double, Double> entry : weightedReductionMap.entrySet()) {
            Logger.getLogger().write("Weighted Entry found: " + entry.getValue() + " Share: " + entry.getKey());
            reduction += entry.getValue() * entry.getKey();
        }

        return reduction;
    }   
    */

    public int getArmor() {
        return armorType;
    }

    @Override
    public double getDistraction() {
        return 0d;
    }

    @Override
    public int[] getDamageDistribution(int count, double temp_destroyed) {
        temp_destroyed = Math.min(temp_destroyed, 1d);

        int[] distribution = new int[6];
        if (temp_destroyed >= 1d) {
            distribution[5] = count;
        } else {
            int destroyed = (int) Math.floor(count * temp_destroyed);
            int survivor = count - destroyed;

            distribution[0] = survivor;
            distribution[5] = destroyed;
        }

        for (int i = 0; i < 5; i++) {
            this.damageDistribution[i] = distribution[i];
        }

        return distribution;
    }

    public void setShields(ShieldDefinition sDef) {
        Logger.getLogger().write("["+this.getName()+"] Set PA shield to " + sDef.getShield_PA());
        Logger.getLogger().write("["+this.getName()+"] Set HU shield to " + sDef.getShield_HU());
        
        shields = sDef;
    }

    /**
     * @return the c
     */
    public Construction getConstruction() {
        return c;
    }
}
