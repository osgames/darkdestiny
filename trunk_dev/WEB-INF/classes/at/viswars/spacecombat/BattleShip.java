/*
 * BattleShip.java
 *
 * Created on 01. Februar 2006, 19:29
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package at.viswars.spacecombat;

import at.viswars.model.ShipDesign;
import at.viswars.spacecombat.helper.ArmorShieldFactor;
import at.viswars.spacecombat.helper.ShieldDefinition;
import at.viswars.spacecombat.helper.TargetIdentifier;
import at.viswars.spacecombat.helper.FleetTracker;
import at.viswars.spacecombat.helper.TargetList;
import at.viswars.enumeration.EAttribute;
import at.viswars.*;
import java.util.*;

import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.admin.module.ReferenceType;
import at.viswars.enumeration.EShipType;

/**
 *
 * @author Stefan
 */
public class BattleShip extends AbstractBattleShip implements IBattleShip {
    private FleetTracker ft = null;
    private boolean tookDamage = false;    
    private int armorType;
    private int groupingMultiplier;
    private int restCount;
    private int orgHp;
    
    private int weaponEnergyPerShip;
    private int weaponEnergy;
    private int maxWeaponEnergy;
    private int energy;
    
    private boolean targetFound;
    private boolean maxFired;
    private int activeWeaponSystem;
    private float sharedHp;
    private float averageRange;
    private int fireSpeed;
    private float sizeLevel;
    private int maxTargetAllocation;
    private int destroyChance = 0;

    private DamagedShips ds = null;   
    private ArmorShieldFactor asf = null;
    private TargetList targetList = null;

    public BattleShip(int designId) {                
        activeWeaponSystem = -1;                
    }

    public void initDamageStack() {
        if (isPlanetaryDefense || isDefenseStation) {
            return;
        }
        ds = new DamagedShips(this);
    }

    /*
    private void buildTotalTargetValue() {
        totalTargetValue = 0;

        // DebugBuffer.addLine("Found " + targetShip.length + " targets for " + name);
        for (int i = 0; i < targetShip.length; i++) {
            if (((BattleShip) targetShip[i]) == null) {
                continue;
            }
            BattleShip tmpShp = (BattleShip) targetShip[i];
            if (SpaceCombat.skip(tmpShp)) {
                continue;
            }
            targetValue[i] = (int) (targetValuePerCount[i] * (float) tmpShp.getCount());
            if ((tmpShp.getCount() > 0) && (targetValue[i] == 0)) {
                targetValue[i] = 1;
            }
            totalTargetValue += targetValue[i];
        }

        // DebugBuffer.addLine("totalTargetValue is " + totalTargetValue);
    }
    */

    public boolean fire() {
        return fire(true);
    }

    @Override
    public boolean fire(boolean allowReturnFire) {
        // Return if no more Targets assigned
        setTargetFound(false);

        if (targetList.getTotalTargetValue() == 0) {
            // DebugBuffer.addLine("No targets");
            targetFound = false;
            // Logger.getLogger().write("Return no target found");
            return false;
        }

        activeWeaponSystem = 0;
        // DebugBuffer.addLine("---------------------------------------");
        // DebugBuffer.addLine("Attacking Ship is " + name);
        // DebugBuffer.addLine("Select weapon system (total systems="+weapons.size()+")");
        if (weapons.isEmpty()) {
            // Logger.getLogger().write("Return no weapons found");
            return false;
        }

        boolean validWeaponFound = false;

        while (true) {
            // DebugBuffer.addLine((activeWeaponSystem + 1)+">"+weapons.size());
            // Logger.getLogger().write("Active weapon system " + activeWeaponSystem);
            if (activeWeaponSystem == weapons.size()) {
                // Logger.getLogger().write("Break fire routine due to everything fired: " + (activeWeaponSystem + 1)+">"+weapons.size());
                break;
                //DebugBuffer.addLine("1Select weapon number " + (activeWeaponSystem + 1));
            }

            WeaponModule wm = weapons.get(activeWeaponSystem);
            // Logger.getLogger().write("Active Weapon System Multifire " + wm.getMultifire() + " - Damage " + wm.getAttackPower() + " - Range " + wm.getRange());

            int attackerDamageLevel = DamagedShips.NO_DAMAGE;
            if (getDs() != null) {
                attackerDamageLevel = getDs().getRandomDamageLevel();
            }
               
            // If == null return true
            // Logger.getLogger().write("Retrieve Target 1");
            TargetIdentifier ti = getTarget();
            if (ti == null) {
                // Logger.getLogger().write("Return due to no target found 2");
                return false;
            }
            
            if (attackerDamageLevel == -1) {
                Logger.getLogger().write("Attacker "+getName()+" has count of " + getCount());
                Logger.getLogger().write("Attacker does not exist");
                throw new RuntimeException("Major crap happened");
                // System.exit(0);
            }
            
            for (int i = 0; i < wm.getMultifire(); i++) {
                boolean weaponValid = wm.getTotalCount() > wm.getFired();

                // Logger.getLogger().write("Firing " + wm.getWeaponId() + " ("+(wm.getFired()+1)+"/"+wm.getTotalCount()+")");

                if (!weaponValid) {
                    // DebugBuffer.addLine("No not fired weapon found!");
                    // Logger.getLogger().write("No not fired weapon found");
                    break;
                } else {
                    validWeaponFound = true;
                }

                /*
                if (weaponValid) {
                //DebugBuffer.addLine("Found weapon " + wm.getWeaponId());
                break;
                }
                 */

                sc_LoggingEntry.assChoosenTarget(ti.getTarget().getSc_LoggingEntry());
                boolean defenderDestroyed = dealDamage(attackerDamageLevel, ti);
                setTargetFound(true);

                if (defenderDestroyed) {
                    // Logger.getLogger().write("Retrieve Target 2");
                    ti = getTarget();
                    if (ti == null) {
                        // Logger.getLogger().write("No target found 3");
                        setTargetFound(false);
                        return false;
                    }                    
                }
            }

            activeWeaponSystem++;
        }

        // DebugBuffer.addLine("ATTACK");

        return validWeaponFound;
    }

    private TargetIdentifier getTarget() {
        int attackValue = GameUtilities.getRandomNo2(targetList.getTotalTargetValue()) - 1;
        BattleShip toAttack = null;

        int accTargetValue = 0;
        int currSelectedShip = 0;

        ISpaceCombat isc = CombatContext.getCombatController();
        TargetList tl = isc.getMyTargetList(this);
        
        for (int i = 0; i < targetList.getTargetShip().length; i++) {
            if (targetList.getTargetShip()[i] == null) {
                continue;
            }
            accTargetValue += targetList.getTargetValue()[i];
            // DebugBuffer.addLine("Adding targetingValue of " + i + " which is " + targetValue[i]);
            // DebugBuffer.addLine("Increased accTargetValue to " + accTargetValue + " compare with " + attackValue);
            if ((accTargetValue >= attackValue) && (accTargetValue != 0)) {
                // DebugBuffer.addLine("Select Ship " + i);
                currSelectedShip = i;
                toAttack = (BattleShip) targetList.getTargetShip()[i];
                // DebugBuffer.addLine("Selected target for " + name + " is " + toAttack.getName());
                break;
            }
        }

        if (toAttack == null) {
            // DebugBuffer.addLine("NO target found!");
            return null;
        }               
        
        // Logger.getLogger().write("Returning ship " + toAttack.getName() + " with count " + toAttack.getCount());
        
        return new TargetIdentifier(toAttack,currSelectedShip);
    }
    
    private boolean dealDamage(int attackerDamageLevel, TargetIdentifier ti) {
        if (targetList.getTotalTargetValue() == 0) {
            // DebugBuffer.addLine("No target");
            // Logger.getLogger().write("Return due to no Target");
            targetFound = false;
        }

        BattleShip toAttack = ti.getTarget();
        int currSelectedShip = ti.getTargetValueId();
        
        WeaponModule wm = (WeaponModule) weapons.get(activeWeaponSystem);

        weaponEnergy -= wm.getEnergyConsumption();
        if (weaponEnergy < 0) {
            DebugBuffer.addLine(DebugLevel.TRACE, "Running low on Energy");
            weaponEnergy = 0;
            wm.setFired(wm.getTotalCount());
        }

        // DebugBuffer.addLine("wmFired="+wm.getFired()+" Energy left="+weaponEnergy);
        ParSet parSet = new ParSet();

        double attackerSpeed = this.speed;

        if (isPlanetaryDefense || isDefenseStation) {
            parSet.attackerDamageLevel = 0;
        } else {
            parSet.attackerDamageLevel = attackerDamageLevel;

            if (attackerDamageLevel == DamagedShips.HEAVY_DAMAGE) {
                attackerSpeed *= 0.9d;
            } else if (attackerDamageLevel == DamagedShips.CRITICAL_DAMAGE) {
                attackerSpeed *= 0.5d;
            }
        }

        if (toAttack.isPlanetaryDefense || toAttack.isDefenseStation) {
            parSet.damageLevel = 0;
        } else {
            if (toAttack.getDs() != null) {
                parSet.damageLevel = toAttack.getDs().getRandomDamageLevel();
                if (parSet.damageLevel == -1) {
                    Logger.getLogger().write("Active Weapon = " + mDAO.findById(wm.getWeaponId()).getName());
                    Logger.getLogger().write("Defender does not exist");
                    throw new RuntimeException("Major crap happened");
                }
            }
        }

        if (isPlanetaryDefense) {
            parSet.damageReduction = 1d;
        } else {
            double defenderSpeed = toAttack.speed;

            if (parSet.damageLevel == DamagedShips.HEAVY_DAMAGE) {
                defenderSpeed *= 0.9d;
            } else if (parSet.damageLevel == DamagedShips.CRITICAL_DAMAGE) {
                defenderSpeed *= 0.5d;
            }

            double speedFactor = this.getSpeed() / toAttack.getSpeed();
            // Logger.getLogger().write("SpeedFactor1: " + speedFactor); 
            double rangeFactor = wm.getRange() / toAttack.getAverageRange();

            if (rangeFactor == Double.NaN) {
                rangeFactor = 1d;
            }

            // Logger.getLogger().write("Speed A = " + speedFactor);
            
            parSet.superiority = 1d;
            /*
            // Attacker is faster 
            if (speedFactor > 1d) {
                speedFactor = speedFactor / Math.min(1d, ((double) toAttack.getMaxTargetAllocation() / (double) count));
                if ((speedFactor == Double.NaN) || Double.isInfinite(speedFactor)) {
                    speedFactor = 1d;
                }
                
                Logger.getLogger().write("Speed A1 = " + speedFactor);
                // Logger.getLogger().write("SpeedFactor2: " + speedFactor + " " + getMaxTargetAllocation() + " " + count); 
            // Attacker is slower
            } else if (speedFactor < 1d) {
                double value1 = 1d - speedFactor;
                double value2 = Math.min(1d, ((double) getMaxTargetAllocation() / (double) toAttack.getCount()));
                speedFactor = value1 / value2;
                Logger.getLogger().write("V1: " + value1);
                Logger.getLogger().write("V2: " + value2);
                
                if ((speedFactor == Double.NaN) || Double.isInfinite(speedFactor)) {
                    speedFactor = 1d;
                }
                
                Logger.getLogger().write("Speed A2 = " + speedFactor);
                // Logger.getLogger().write("SpeedFactor3: " + speedFactor + " " + getMaxTargetAllocation() + " " + toAttack.getCount()); 
            // Attacker has same speed    
            } else if (speedFactor == 1d) {
                
            }
            */

            int attackerSup = count * (int)Math.ceil((groupingMultiplier + 1) / 10d) * Math.max(1,fireSpeed);
            int defenderSup = toAttack.count * (int) Math.ceil((toAttack.getGroupingMultiplier() + 1) / 10d) * Math.max(1,toAttack.getFireSpeed());

            // Logger.getLogger().write("AttackerSup: " + attackerSup + " DefenderSup: " + defenderSup);

            if ((attackerSup > defenderSup) && (toAttack.getAverageRange() < getAverageRange())) {
                
                //Filled in by DRE to avoid division by zero
                if (defenderSup > 0) {
                    parSet.superiority = (attackerSup / defenderSup);
                } else {
                    DebugBuffer.addLine(DebugLevel.WARNING, toAttack.getName() + " COUNT: " + (toAttack.count) + " GM+1: " + Math.ceil(Math.log(toAttack.getGroupingMultiplier() + 1)) + " FS: " + Math.max(1,toAttack.getFireSpeed()));
                    DebugBuffer.addLine(DebugLevel.WARNING, "Wanted to divide by zero. " + attackerSup + " / " + defenderSup + " (attackerSup/defenderSup)");
                    parSet.superiority = attackerSup;
                }
                
                if (rangeFactor < 1d) {
                    double oldFactor = rangeFactor;
                    rangeFactor = 1d - ((1d - rangeFactor) / (parSet.superiority));
                    // Logger.getLogger().write(getName() + " increased rangeFactor from " + oldFactor + " to " + rangeFactor);
                    // Logger.getLogger().write("Range Factor adjusted from " + oldFactor +" to " + rangeFactor + " [TARGET: " + toAttack.name + "]");
                }

                if (speedFactor < 1d) {
                    double oldFactor = speedFactor;
                    speedFactor = 1d - ((1d - speedFactor) / (parSet.superiority));
                    // Logger.getLogger().write(getName() + " increased speedFactor from " + oldFactor + " to " + speedFactor);
                    // Logger.getLogger().write("Speed Factor adjusted from " + oldFactor +" to " + speedFactor + " [TARGET: " + toAttack.name + "]");
                }
            }

            /*
            if (rangeFactor < 0) {
            double subOne = 1d - rangeFactor;
            int countDiff = this.getCount() - toAttack.getCount();
            if (countDiff > 0) {
            subOne /= Math.log10(countDiff);
            }
            rangeFactor = 1d - subOne;
            } 
             */

            if ((rangeFactor == Double.NaN) || Double.isNaN(rangeFactor)) {
                rangeFactor = 1d;
            }

            double accuracy = wm.getAccuracy();
            if (toAttack.getAverageRange() < wm.getRange()) {
                accuracy = 1d;
            } else {
                accuracy = 1d - ((1d - wm.getAccuracy()) / toAttack.getSizeLevel());
            }

            // Logger.getLogger().write("ACC: " + accuracy + " SF: " + speedFactor + " RF: " + rangeFactor);
            /*
            if (name.equalsIgnoreCase("baller_003")) {
                Logger.getLogger().write("SF="+speedFactor+" ACC="+accuracy+" RF="+rangeFactor);
            }
            */
                        
            parSet.damageReduction = Math.min(1d, speedFactor * accuracy * rangeFactor);
            // Logger.getLogger().write("Final Factor = " + parSet.damageReduction);
        }

        // Logger.getLogger().write(this.getName() + " attacks " + toAttack.getName() + " with attackpower of " + wm.getAttackPower());
        sc_LoggingEntry.assChoosenTarget(ti.getTarget().getSc_LoggingEntry());
        
        ISpaceCombat isc = CombatContext.getCombatController();
        /*
        Logger.getLogger().write("ISC="+isc);
        Logger.getLogger().write("ISC GC="+isc.getGroupCount());
        Logger.getLogger().write("assignedBattleGroup: " + assignedBattleGroup);
        Logger.getLogger().write("ISC GC ASSBG: " + isc.getGroupCount().get(assignedBattleGroup));

        double ratio = Math.min(1d, isc.getGroupCount().get(assignedBattleGroup) /
                isc.getGroupCount().get(toAttack.getParentCombatGroupId()));         
         */
        
        double ratio = 1d;
        boolean defenderDestroyed = toAttack.takeDamage(wm, ratio, parSet);
        
        if (defenderDestroyed) {            
            // Logger.getLogger().write("defender was destroyed");
            // totalTargetValue -= targetValuePerCount[currSelectedShip];
            // targetValue[currSelectedShip] -= targetValuePerCount[currSelectedShip];
            isc.updateTargetLists();
            // DebugBuffer.addLine("New Targeting Value for " + toAttack.getName() + " is "+targetValue[currSelectedShip]);
            // Adjust targeting Values
        } else {
            // Something else about firing multiple shots
        }        

        if (targetList.getTotalTargetValue() == 0) {
            targetFound = false;
        } else {
            targetFound = true;
        }
        
        return defenderDestroyed;
    }

    public boolean takeDamage(WeaponModule wm, double attackerRatio, ParSet parSet) {
        CombatLogger cl = CombatLogger.getInstance();

        double startHp = hp;

        if (getDs() != null) {
            startHp = getDs().getHpForLevel(parSet.damageLevel);
            // Logger.getLogger().write("Retrieve " + startHp + " hitpoints [DamageLevel "+parSet.damageLevel+"] for " + this.getName());
        } else {
            // Logger.getLogger().write("No Damagelevels for " + this.getName());
        }

        double endHp = startHp;

        // Loop through all assigned DamagePackets and reduce hp and shields
        // - reduce hp regarding shield power %age
        //   this should do a realistic reduction on losses for shield covered ships
        // Logger.getLogger().write("PARSET: rangePenalty="+parSet.rangePenalty+" rangeSizePenalty="+parSet.rangeSizePenalty+" speedFactor="+parSet.speedFactor);        
        float orgDamage = wm.getAttackPower();
        sc_LoggingEntry.incDamageTaken(orgDamage);

        float damage = wm.getAttackPower();

        if (parSet.attackerDamageLevel == DamagedShips.SERIOUS_DAMAGE) {
            damage *= 0.9f;
        } else if (parSet.attackerDamageLevel == DamagedShips.HEAVY_DAMAGE) {
            damage *= 0.7f;
        } else if (parSet.attackerDamageLevel == DamagedShips.CRITICAL_DAMAGE) {
            damage *= 0.3f;
        }

        /*
        ArmorShieldFactor asf = (ArmorShieldFactor) this.asf.get(Integer.valueOf(wm.getWeaponId()));

        if (isPlanetaryDefense) { // Hard shields for planetary shields
        asf.setHuschirmpenentration((asf.getHuschirmpenentration() + (asf.getHuschirmpenentration() / 2f)) / 2f);
        asf.setParatronpenetration((asf.getParatronpenetration() + (asf.getParatronpenetration() / 2f)) / 2f);
        }

        if (asf.getSplashdamage() != 0) {
        damage = damage * (float) Math.pow(asf.getSplashdamage(), Math.ceil((wm.getTotalCount() - wm.getFired()) / wm.getCount()));
        }
         */

        // Logger.getLogger().write("ORGDAMAGE=" + damage);
        // Logger.getLogger().write("parSet.rangePenalty="+parSet.rangePenalty+" // parSet.speedFactor="+parSet.speedFactor);

        // Logger.getLogger().write("Org damage of " + damage + " to target ["+this.name+"]");

        damage *= parSet.damageReduction;

        //Logger.getLogger().write("Reduced damage of " + damage + " to target ["+this.name+"]");

        // damage = damage - (float)((double)damage * parSet.rangePenalty);
        // damage = damage * (float)(parSet.rangeSizePenalty * parSet.speedFactor);

        // Logger.getLogger().write("Final damage: " + damage);

        wm.setFired(wm.getFired() + 1);
        // Logger.getLogger().write(this.name + " taking " + damage + " from enemy (shot " + wm.getFired() + " of " + wm.getTotalCount() + ")");

        // Should be more dynamic .. consider for future            
        if (!paraPenetration.containsKey(wm.getWeaponId())) {
            paraPenetration.put(wm.getWeaponId(), asf.getPTShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
        }
        if (!huPenetration.containsKey(wm.getWeaponId())) {
            huPenetration.put(wm.getWeaponId(), asf.getHUShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
        }
        if (!prallPenetration.containsKey(wm.getWeaponId())) {
            prallPenetration.put(wm.getWeaponId(), asf.getPSShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
        }

        double paraPene = paraPenetration.get(wm.getWeaponId());
        double huPene = huPenetration.get(wm.getWeaponId());
        double prallPene = prallPenetration.get(wm.getWeaponId());

        double damageBeforeShields = damage;
        
        // Lead damage into shield
        // if (asf != null) {
        double tmpCount = count;
        /*
        if (ds != null) {
            tmpCount = ds.getAverageCount();
        }
         */

        if ((shields.getSharedShield_PA() > 0) && (parSet.damageLevel != DamagedShips.CRITICAL_DAMAGE)) {
            if (paraPene > 0d) {
                if (this.isPlanetaryDefense) {
                    tmpCount = 1;                    // if ((GameUtilities.getRandomNo2(100) + modifier < shields.getPa_EnergyLevel()) || (((shields.getShield_PA() * count) - shields.getSharedShield_PA()) < shields.getShield_PA())) {
                }

                double rcvdDamage = ((shields.getShield_PA() * tmpCount) - shields.getSharedShield_PA());
                double hitOneFactor = rcvdDamage / shields.getShield_PA();
                if (hitOneFactor > 1d) hitOneFactor = hitOneFactor * attackerRatio;
                double overOne = Math.max(1d,hitOneFactor * (1d - (shields.getPa_EnergyLevel() / 100f)));

                if (GameUtilities.getRandomNo2(100) < (shields.getPa_EnergyLevel() / overOne)) {
                    // Logger.getLogger().write("HIT BLOCKED --> " + ((shields.getShield_PA() * count) - shields.getSharedShield_PA()));
                    float damageOnShield = Math.min(damage * (float) paraPene,shields.getSharedShield_PA());

                    sc_LoggingEntry.incPA_DamagePenetrated(damage - damageOnShield);
                    sc_LoggingEntry.incPA_DamageAbsorbed(damageOnShield);

                    shields.setSharedShield_PA(shields.getSharedShield_PA() - damageOnShield);

                    try {
                        cl.addShieldDamage(this.getDesignId(), (int) damageOnShield);
                    } catch (Exception e) {
                        Logger.getLogger().write("Logging fault");
                    }

                    damage -= damageOnShield;

                    if (shields.getSharedShield_PA() <= 0) {
                        shields.setSharedShield_PA(0);
                        shields.setPa_EnergyLevel(0);
                    } else {
                        shields.setPa_EnergyLevel(100f / (float) (shields.getShield_PA() * count) * shields.getSharedShield_PA());
                    }
                } else {
                    sc_LoggingEntry.incPA_DamagePenetrated_Fail(damage);
                }
            } else {
                sc_LoggingEntry.incPA_DamagePenetrated_Fail(damage);
            }
        } else {
            sc_LoggingEntry.incPA_DamagePenetrated_Fail(damage);
        }

        boolean huCheck = false;
        boolean huCheck2 = false;

        if ((shields.getSharedShield_HU() > 0) && (parSet.damageLevel != DamagedShips.CRITICAL_DAMAGE)) {
            huCheck = true;
            if (huPene > 0d) {
                if (this.isPlanetaryDefense) {
                    tmpCount = 1;
                }

                double rcvdDamage = ((shields.getShield_HU() * tmpCount) - shields.getSharedShield_HU());
                // double overOne = Math.max(1d,rcvdDamage / shields.getShield_HU() * (1d - (shields.getHu_EnergyLevel() / 100f)));

                double hitOneFactor = rcvdDamage / shields.getShield_HU();
                if (hitOneFactor > 1d) hitOneFactor = hitOneFactor * attackerRatio;
                double overOne = Math.max(1d,hitOneFactor * (1d - (shields.getHu_EnergyLevel() / 100f)));

                int randomNo = GameUtilities.getRandomNo2(100);
                if (randomNo < (shields.getHu_EnergyLevel() / overOne)) {
                // if ((randomNo < shields.getHu_EnergyLevel()) && (((shields.getShield_HU() * tmpCount) - shields.getSharedShield_HU()) < shields.getShield_HU())) {
                    // Logger.getLogger().write("BLOCKED --> " + ((shields.getShield_HU() * count) - shields.getSharedShield_HU()));
                    huCheck2 = true;
                    float damageOnShield = Math.min(damage * (float) huPene,shields.getSharedShield_HU());

                    sc_LoggingEntry.incHU_DamagePenetrated(damage - damageOnShield);
                    sc_LoggingEntry.incHU_DamageAbsorbed(damageOnShield);

                    if (this.isPlanetaryDefense) {
                        //Logger.getLogger().write("["+this.getName()+"] Planetary shield taking damage: " + damageOnShield + " damage on target: " + (damage - damageOnShield));
                    }

                    shields.setSharedShield_HU(shields.getSharedShield_HU() - damageOnShield);

                    try {
                        cl.addShieldDamage(this.getDesignId(), (int) damageOnShield);
                    } catch (Exception e) {
                        Logger.getLogger().write("Logging fault");
                    }

                    damage -= damageOnShield;

                    if (shields.getSharedShield_HU() <= 0) {
                        shields.setSharedShield_HU(0);
                        shields.setHu_EnergyLevel(0);
                    } else {
                        shields.setHu_EnergyLevel(100f / (float) (shields.getShield_HU() * count) * shields.getSharedShield_HU());
                    }
                } else {
                    sc_LoggingEntry.incHU_DamagePenetrated_Fail(damage);
                }
            } else {
                sc_LoggingEntry.incHU_DamagePenetrated_Fail(damage);
            }
        } else {
            sc_LoggingEntry.incHU_DamagePenetrated_Fail(damage);
        }


        if ((shields.getSharedShield_PS() > 0) && (parSet.damageLevel != DamagedShips.CRITICAL_DAMAGE)) {
            if (prallPene > 0d) {
                double rcvdDamage = ((shields.getShield_PS() * tmpCount) - shields.getSharedShield_PS());
                double overOne = Math.max(1d,rcvdDamage / shields.getShield_PS() * (1d - (shields.getPs_EnergyLevel() / 100f)));

                // if ((GameUtilities.getRandomNo2(100) + modifier < shields.getPs_EnergyLevel()) || (((shields.getShield_PS() * count) - shields.getSharedShield_PS()) <
                if (GameUtilities.getRandomNo2(100) < (shields.getPs_EnergyLevel() / overOne)) {
                // if ((GameUtilities.getRandomNo2(100) < shields.getPs_EnergyLevel()) && (((shields.getShield_PS() * tmpCount) - shields.getSharedShield_PS()) < shields.getShield_PS())) {
                    float damageOnShield = Math.min(damage * (float) prallPene,shields.getSharedShield_PS());
                    shields.setSharedShield_PS(shields.getSharedShield_PS() - damageOnShield);

                    sc_LoggingEntry.incPS_DamagePenetrated(damage - damageOnShield);
                    sc_LoggingEntry.incPS_DamageAbsorbed(damageOnShield);

                    try {
                        cl.addShieldDamage(this.getDesignId(), (int) damageOnShield);
                    } catch (Exception e) {
                        Logger.getLogger().write("Logging fault");
                    }

                    damage -= damageOnShield;

                    if (shields.getSharedShield_PS() <= 0) {
                        shields.setSharedShield_PS(0);
                        shields.setPs_EnergyLevel(0);
                    } else {
                        shields.setPs_EnergyLevel(100f / (float) (shields.getShield_PS() * count) * shields.getSharedShield_PS());
                    }
                } else {
                    sc_LoggingEntry.incPS_DamagePenetrated_Fail(damage);
                }
            } else {
                sc_LoggingEntry.incPS_DamagePenetrated_Fail(damage);
            }
        } else {
            sc_LoggingEntry.incPS_DamagePenetrated_Fail(damage);
        }
        // }

        if (damage == 0) {
            Logger.getLogger().write(LogLevel.INFO,"(Shot: "+wm.getFired()+"/"+wm.getTotalCount()+") ["+this.name+"] Blocked by shield (Shieldpoints: " + shields.getSharedShield_HU() + ") {DR: " + parSet.damageReduction + "} {HC1: "+huCheck+"} {HC2: "+huCheck2+"}");
            return false;
        }

        if (!ynkeAbsorb.containsKey(wm.getWeaponId())) {
            ynkeAbsorb.put(wm.getWeaponId(), asf.getYnkeArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
        }
        if (!terkAbsorb.containsKey(wm.getWeaponId())) {
            terkAbsorb.put(wm.getWeaponId(), asf.getTerkArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
        }
        if (!steelAbsorb.containsKey(wm.getWeaponId())) {
            steelAbsorb.put(wm.getWeaponId(), asf.getSteelArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
        }

        double damageBeforeArmor = damage;

        // Reduce Damage due to Armortype
        switch (armorType) {
            case (1):
                damage = damage * (1f - (float) steelAbsorb.get(wm.getWeaponId()).doubleValue());
                sc_LoggingEntry.incST_DamageAbsorbed(damageBeforeArmor - damage);
                break;
            case (2):
                damage = damage * (1f - (float) terkAbsorb.get(wm.getWeaponId()).doubleValue());
                sc_LoggingEntry.incTE_DamageAbsorbed(damageBeforeArmor - damage);
                break;
            case (3):
                damage = damage * (1f - (float) ynkeAbsorb.get(wm.getWeaponId()).doubleValue());
                sc_LoggingEntry.incYN_DamageAbsorbed(damageBeforeArmor - damage);
                break;
        }

        double critialChance = damageBeforeArmor / (double) hp * Math.log(parSet.superiority);
        if (Math.random() < critialChance) {
        //    Logger.getLogger().write(this.getName() + " received a critical hit with chance of " + critialChance);
            damage = orgDamage * 2;
            
            //    Logger.getLogger().write(name + " takes " + damage + " critical Damage");
           
        }

        if (damage > 0) {
            tookDamage = true;
            Logger.getLogger().write(LogLevel.INFO,"(Shot: "+wm.getFired()+"/"+wm.getTotalCount()+") ["+this.name+"] Took damage: " + damage + " {DR: " + parSet.damageReduction + "} {"+parSet.damageLevel+"} {HC1: "+huCheck+"} {HC2: "+huCheck2+"}");
        }

    //    Logger.getLogger().write("Compare " + damage + " damage  to " + orgHp + " Hitpoints of target [" + this.name + "]");
        if (damage > (orgHp / this.groupingMultiplier)) {
        //    Logger.getLogger().write("Reduce damage to " + orgHp);
            damage = (orgHp / this.groupingMultiplier);
        }

        sc_LoggingEntry.incEffDamageTaken(damage);
        endHp -= damage;
        
        //hp -= damage;

        try {
            cl.addHpDamage(this.getDesignId(), (int) damage);
        } catch (Exception e) {
            Logger.getLogger().write("Logging fault");
        }

        if (ft != null) {
            //Logger.getLogger().write("Set Act Hp to " + (ft.getActHp() - damage) + " ACTHP="+ft.getActHp()+" DAMAGE="+damage);
            ft.setActHp(ft.getActHp() - damage);
        }

        // Logger.getLogger().write(this.name + " took " + damage + " damage. (HP left: " + endHp + " // Shield Level: " + (100d / (shields.getShield_HU() * count) * shields.getSharedShield_HU()) + ")");

        // Logger.getLogger().write("HP LEFT " + hp);
        // DebugBuffer.addLine("Damage=" + damage+ " count="+count+" hp="+hp+"/"+orgHp+" sharedPS="+shields.getSharedShield_PS());

        // Calculate Chance to destroy Ship
        // This value is calculated by relation between total hitpoints and count of ships
        /*
        boolean destroyShip = false;
        
        if ((damage > hp) || ((count == 1) && (sharedHp < 0))) {
        destroyShip = true;
        } else {
        float destroyChance = 100f - (float)Math.pow(Math.log10(100f / (float)(hp * count) * (float)sharedHp), 7f);
        if (GameUtilities.getRandomNo2(100) < destroyChance) {
        accDamage += damage;
        if (accDamage > (sharedHp / count)) {
        destroyShip = true;
        accDamage = 0;
        }
        }
        }
         */

        // Logger.getLogger().write("End hp = " + endHp + " ("+startHp+")");
        if (endHp <= 0) {
            // if (hp < 0) {
            if (getDs() != null) {
          //      Logger.getLogger().write("Destroy ship");
                // Logger.getLogger().write("Destroy ship");
                getDs().destroyShip(parSet.damageLevel, startHp);
            }
            if (ft != null) {
                ft.setActHp(ft.getActHp() + (float) Math.abs(endHp));
                // ft.setActHp(ft.getActHp() + Math.abs(hp));
            }
            try {
                cl.incDestroyed(this.getDesignId(), count);
            } catch (Exception e) {
                Logger.getLogger().write("Logging fault");
            }
            
            // Logger.getLogger().write("Reduce Count");
            count--;
            hp = getOrgHp();

            // Temporary Fix to solve Problem with negative ships
            if (count <= 0) {
                count = 0;
                startHp = 0;
                endHp = 0;
                hp = 0;
                if (ft != null) {
                    ft.setActHp(0);
                }
            }

            if (ft != null) {
                ft.setActCount(ft.getActCount() - 1);            // Rescale Accumulated shields
            }
            return true;
        } else {
            if (getDs() != null) {
                // Logger.getLogger().write("Update Stack");
                getDs().updateStack(parSet.damageLevel, startHp, endHp);
                // getDs().updateStack(parSet.damageLevel, hp, startHp - hp);
            } else {
                hp -= (startHp - endHp);
            }
            return false;
        }
    }

    @Override
    public void reloadShieldsAndWeapons() {
        double oldPSValue = shields.getSharedShield_PS();
        double oldHUValue = shields.getSharedShield_HU();
        double oldPAValue = shields.getSharedShield_PA();


        // If there is enough Energy for all Systems complete reload
        DebugBuffer.addLine(DebugLevel.TRACE, "Reset Energy settings for " + this.getName());
        tookDamage = false;

        maxWeaponEnergy = weaponEnergyPerShip * count;
        int ps_Energy = 0;
        int hu_Energy = 0;
        int pa_Energy = 0;

        if (this.isPlanetaryDefense) { // Always full restore
            // Always full restore
            if (shields.isReloaded()) {
                return;            //Logger.getLogger().write("["+this.getName()+"] Old shield value: " + shields.getSharedShield_HU() + " / " + shields.getShield_HU());
            }
            shields.setMaxShieldEnergy(shields.getShieldEnergy());

            shields.setSharedShield_PA(shields.getShield_PA());
            shields.setSharedShield_HU(shields.getShield_HU());
            shields.setSharedShield_PS(shields.getShield_PS());
            shields.setPs_EnergyLevel(100f);
            shields.setHu_EnergyLevel(100f);
            shields.setPa_EnergyLevel(100f);

            weaponEnergy = maxWeaponEnergy;

            // Logger.getLogger().write("Reload planetary shield to " + shields.getSharedShield_HU());
        } else { // Combined shield weapon calculation
            shields.setMaxShieldEnergy(shields.getShieldEnergyPerShip() * count);

            if ((energy * count) >= (shields.getMaxShieldEnergy() + maxWeaponEnergy)) {
                DebugBuffer.addLine(DebugLevel.TRACE, "Full restore!");
                shields.setShieldEnergy(shields.getMaxShieldEnergy());
                weaponEnergy = maxWeaponEnergy;

                // Logger.getLogger().write("Set Shield Energy to "+shields.getShieldEnergy()+" and weaponEnergy to"+weaponEnergy+" for " + this.name);

                // Reset Shield loading Values
                shields.setSharedShield_PA(shields.getShield_PA() * count);
                shields.setSharedShield_HU(shields.getShield_HU() * count);
                shields.setSharedShield_PS(shields.getShield_PS() * count);
                shields.setPs_EnergyLevel(100f);
                shields.setHu_EnergyLevel(100f);
                shields.setPa_EnergyLevel(100f);
                
                sc_LoggingEntry.incPA_ShieldPointsRestored(shields.getSharedShield_PA() - oldPAValue);
                sc_LoggingEntry.incHU_ShieldPointsRestored(shields.getSharedShield_HU() - oldHUValue);
                sc_LoggingEntry.incPS_ShieldPointsRestored(shields.getSharedShield_PS() - oldPSValue);
                return;
            }

            ps_Energy = (int) ((float) (shields.getEnergyNeed_PS() * count) / 100f * shields.getPs_EnergyLevel());
            hu_Energy = (int) ((float) (shields.getEnergyNeed_HU() * count) / 100f * shields.getHu_EnergyLevel());
            pa_Energy = (int) ((float) (shields.getEnergyNeed_PA() * count) / 100f * shields.getPa_EnergyLevel());

            // If energy lackage support different systems with available energy
            DebugBuffer.addLine(DebugLevel.TRACE, "Not sufficient Energy available!");

            int availEnergyShields = (int) ((float) energy / 100f * 30f) * count;
            int availEnergyWeapon = (energy * count) - availEnergyShields;

            DebugBuffer.addLine(DebugLevel.TRACE, "Assigned availEnergy to shield=" + availEnergyShields);
            DebugBuffer.addLine(DebugLevel.TRACE, "Assigned availEnergy to weapons=" + availEnergyWeapon);

            if (availEnergyShields > shields.getMaxShieldEnergy()) {
                shields.setShieldEnergy(shields.getMaxShieldEnergy());
                availEnergyShields -= shields.getShieldEnergy();
            } else {
                shields.setShieldEnergy(availEnergyShields);
                availEnergyShields = 0;
            }

            if (availEnergyWeapon > maxWeaponEnergy) {
                weaponEnergy = maxWeaponEnergy;
                availEnergyWeapon -= weaponEnergy;
            } else {
                weaponEnergy = availEnergyWeapon;
                availEnergyWeapon = 0;
            }

            DebugBuffer.addLine(DebugLevel.TRACE, "Assigned energy to shields(1)=" + shields.getShieldEnergy());
            DebugBuffer.addLine(DebugLevel.TRACE, "Assigned energy to weapons(1)=" + weaponEnergy);

            if (availEnergyShields > 0) {
                weaponEnergy += availEnergyShields;
                availEnergyShields = 0;
            }

            if (availEnergyWeapon > 0) {
                shields.setShieldEnergy(shields.getShieldEnergy() + availEnergyWeapon);
                availEnergyWeapon = 0;
            }
        }

        DebugBuffer.addLine(DebugLevel.TRACE, "Assigned energy to shields(2)=" + shields.getShieldEnergy());
        DebugBuffer.addLine(DebugLevel.TRACE, "Assigned energy to weapons(2)=" + weaponEnergy);

        DebugBuffer.addLine(DebugLevel.TRACE, "Weapon Energy set to " + weaponEnergy);

        // Reset Shield Loading Values
        /*
        int ps_Energy = (int)((float)(shields.getEnergyNeed_PA() * count) / 100f * shields.getPs_EnergyLevel());
        int hu_Energy = (int)((float)(shields.getEnergyNeed_HU() * count) / 100f * shields.getHu_EnergyLevel());
        int pa_Energy = (int)((float)(shields.getEnergyNeed_PA() * count) / 100f * shields.getPa_EnergyLevel());
         */
        // Load Prallschirm
        if (!this.isPlanetaryDefense) { // Skip shield reloading if a shared shield (ie. planetary defense)
            if ((shields.getEnergyNeed_PS() != 0) && (shields.getPs_EnergyLevel() < 100f) && (shields.getShieldEnergy() > 0)) {
                DebugBuffer.addLine(DebugLevel.TRACE, "Energy need for Prallschirm found");
                int addEnergyNeeded = (int) (shields.getEnergyNeed_PS() * count) - ps_Energy;
                DebugBuffer.addLine(DebugLevel.TRACE, addEnergyNeeded + " Energy needed");
                if (shields.getShieldEnergy() > addEnergyNeeded) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "Full reload");
                    shields.setShieldEnergy(shields.getShieldEnergy() - addEnergyNeeded);
                    shields.setSharedShield_PS(shields.getShield_PS() * count);
                    shields.setPs_EnergyLevel(100);
                } else {
                    ps_Energy += shields.getShieldEnergy();
                    shields.setShieldEnergy(0);
                    shields.setPs_EnergyLevel(100f / (shields.getEnergyNeed_PS() * (float) count) * (float) ps_Energy);
                    DebugBuffer.addLine(DebugLevel.TRACE, "Load shield to " + shields.getPs_EnergyLevel() + "%");
                    shields.setSharedShield_PS((int) ((float) (shields.getShield_PS() * count) / 100f * shields.getPs_EnergyLevel()));
                }
            }

            // Load Hï¿½-Schirm
            if ((shields.getEnergyNeed_HU() != 0) && (shields.getHu_EnergyLevel() < 100f) && (shields.getShieldEnergy() > 0)) {
                int addEnergyNeeded = (int) (shields.getEnergyNeed_HU() * count) - hu_Energy;
                if (shields.getShieldEnergy() > addEnergyNeeded) {
                    shields.setShieldEnergy(shields.getShieldEnergy() - addEnergyNeeded);
                    shields.setSharedShield_HU(shields.getShield_HU() * count);
                    shields.setHu_EnergyLevel(100);
                } else {
                    hu_Energy += shields.getShieldEnergy();
                    shields.setShieldEnergy(0);
                    shields.setHu_EnergyLevel(100f / (shields.getEnergyNeed_HU() * (float) count) * (float) hu_Energy);
                    shields.setSharedShield_HU((int) ((float) (shields.getShield_HU() * count) / 100f * shields.getHu_EnergyLevel()));
                }
            }

            // Load Paratron
            if ((shields.getEnergyNeed_PA() != 0) && (shields.getPa_EnergyLevel() < 100f) && (shields.getShieldEnergy() > 0)) {
                int addEnergyNeeded = (int) (shields.getEnergyNeed_PA() * count) - pa_Energy;
                if (shields.getShieldEnergy() > addEnergyNeeded) {
                    shields.setShieldEnergy(shields.getShieldEnergy() - addEnergyNeeded);
                    shields.setSharedShield_PA(shields.getShield_PA() * count);
                    shields.setPa_EnergyLevel(100);
                } else {
                    pa_Energy += shields.getShieldEnergy();
                    shields.setShieldEnergy(0);
                    shields.setHu_EnergyLevel(100f / (shields.getEnergyNeed_PA() * (float) count) * (float) pa_Energy);
                    shields.setSharedShield_PA((int) ((float) (shields.getShield_PA() * count) / 100f * shields.getPa_EnergyLevel()));
                }
            }

            sc_LoggingEntry.incPA_ShieldPointsRestored(shields.getSharedShield_PA() - oldPAValue);
            sc_LoggingEntry.incHU_ShieldPointsRestored(shields.getSharedShield_HU() - oldHUValue);
            sc_LoggingEntry.incPS_ShieldPointsRestored(shields.getSharedShield_PS() - oldPSValue);
        }

        shields.setReloaded(true);
    }

    @Override
    public ShieldDefinition getShields() {
        return shields;
    }

    public void setShields(ShieldDefinition shields) {
        Logger.getLogger().write("Set shields for " + name);
        this.shields = shields;
    }

    public FleetTracker getFleetTracker() {
        return ft;
    }

    public void setFleetTracker(FleetTracker ft) {
        this.ft = ft;
    }

    public int getOrgHp() {
        return orgHp;
    }

    public DamagedShips getDs() {
        return ds;
    }

    public float getAverageRange() {
        return averageRange;
    }

    public void setAverageRange(float averageRange) {
        this.averageRange = averageRange;
    }

    public float getSizeLevel() {
        return sizeLevel;
    }

    public void setSizeLevel(float sizeLevel) {
        this.sizeLevel = sizeLevel;
    }

    public int getMaxTargetAllocation() {
        return maxTargetAllocation;
    }

    public void setMaxTargetAllocation(int maxTargetAllocation) {
        this.maxTargetAllocation = maxTargetAllocation;
    }

    public int getGroupingMultiplier() {
        return groupingMultiplier;
    }

    public void setGroupingMultiplier(int groupingMultiplier) {
        this.groupingMultiplier = groupingMultiplier;
    }

    public int getShipFleetId() {
        return shipFleetId;
    }

    @Override
    public void setShipFleetId(int shipFleetId) {
        this.shipFleetId = shipFleetId;
    }

    public int getRestCount() {
        return restCount;
    }

    public void setRestCount(int restCount) {
        this.restCount = restCount;
    }

    public int getFireSpeed() {
        return fireSpeed;
    }

    public void setFireSpeed(int fireSpeed) {
        this.fireSpeed = fireSpeed;
    }

    public void setTargetList(TargetList targetList) {
        this.targetList = targetList;
    }

    public int getOrgCount() {
        return orgCount;
    }

    public int getArmor() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ShipDesign getShipDesign() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    class ParSet {

        public double superiority;
        public double damageReduction;
        public int damageLevel;
        public int attackerDamageLevel;
    }

    public int getDesignId() {
        return designId;
    }

    public void setDesignId(int designId) {
        this.designId = designId;                      
        asf = ((HashMap<Integer, ArmorShieldFactor>)CombatContext.getRegisteredObject("ASFMAP")).get(this.getDesignId());
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        // this.orgCount = count;
        this.count = count;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public float getHp() {
        return (int) hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
        this.orgHp = hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
        this.orgHp = (int) hp;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    @Override
    public ArrayList<WeaponModule> getWeapons() {
        return weapons;
    }

    public void setWeapons(ArrayList<WeaponModule> weapons) {
        this.weapons = weapons;
    }

    public int getPrimaryTarget() {
        return primaryTarget;
    }

    public void setPrimaryTarget(int primaryTarget) {
        this.primaryTarget = primaryTarget;
    }

    public EShipType getPurpose() {
        return purpose;
    }

    public void setPurpose(EShipType purpose) {
        this.purpose = purpose;
    }

    public int getChassisSize() {
        return chassisSize;
    }

    public void setChassisSize(int chassisSize) {
        this.chassisSize = chassisSize;
    }

    public boolean isMaxFired() {
        return maxFired;
    }

    public void setMaxFired(boolean maxFired) {
        this.maxFired = maxFired;
    }

    /*
    public BattleShip[] getTargetShip() {
        return targetShip;
    }

    public void setTargetShip(BattleShip[] targetShip) {
        this.targetShip = targetShip;
    }

    public int[] getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(int[] targetValue) {
        this.targetValue = targetValue;
    }

    public int getTotalTargetValue() {
        return totalTargetValue;
    }

    public void setTotalTargetValue(int totalTargetValue) {
        this.totalTargetValue = totalTargetValue;
    }

    public float[] getTargetValuePerCount() {
        return targetValuePerCount;
    }

    public void setTargetValuePerCount(float[] targetValuePerCount) {
        this.targetValuePerCount = targetValuePerCount;
    }
    */ 

    @Override
    public boolean hasTargetFound() {
        return targetFound;
    }

    public void setTargetFound(boolean targetFound) {
        this.targetFound = targetFound;
    }

    public int getArmorType() {
        return armorType;
    }

    public void setArmorType(int armorType) {
        this.armorType = armorType;
    }

    @Override
    public boolean isDefenseStation() {
        return isDefenseStation;
    }

    public void setIsDefenseStation(boolean isDefenseStation) {
        this.isDefenseStation = isDefenseStation;
    }

    @Override
    public boolean isPlanetaryDefense() {
        return isPlanetaryDefense;
    }

    public void setIsPlanetaryDefense(boolean isPlanetaryDefense) {
        this.isPlanetaryDefense = isPlanetaryDefense;
    }

    public float getSharedHp() {
        return sharedHp;
    }

    public void setSharedHp(float sharedHp) {
        this.sharedHp = sharedHp;
    }

    public int getDestroyChance() {
        return destroyChance;
    }

    public void setDestroyChance(int destroyChance) {
        this.destroyChance = destroyChance;
    }

    public int getWeaponEnergy() {
        return weaponEnergy;
    }

    public void setWeaponEnergy(int weaponEnergy) {
        this.weaponEnergy = weaponEnergy;
        this.maxWeaponEnergy = weaponEnergy;
    }

    public int getWeaponEnergyPerShip() {
        return weaponEnergyPerShip;
    }

    public void setWeaponEnergyPerShip(int weaponEnergyPerShip) {
        this.weaponEnergyPerShip = weaponEnergyPerShip;
    }

    public boolean isTookDamage() {
        return tookDamage;
    }

    @Override
    @SuppressWarnings("unchecked")
    public BattleShip clone() {
        try {
            BattleShip tmp = (BattleShip)super.clone();

            // Deep clone Maps and Lists
            tmp.shields = this.shields.clone();

            return tmp;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public double getDistraction() {
        return 0d;
    }

    @Override
    public int[] getDamageDistribution(int count, double temp_destroyed) {
        return null;
    }
}
