/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.bean;

import at.viswars.ai.AIDebugBuffer;
import at.viswars.ai.AIDebugEntry;
import at.viswars.ai.AIThread;
import at.viswars.ai.AIThreadHandler;
import at.viswars.ai.AIUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "AIBean")
@ApplicationScoped
public class AIBean implements Serializable {

    
    public boolean dosome;
    public TreeMap<Long, AIDebugEntry> getEntries() {

        return AIDebugBuffer.getEntries();
    }

    public List<Integer> getItemKeys() {
        List keys = new ArrayList();
        keys.addAll(AIDebugBuffer.getEntries().keySet());
        return keys;
    }

    public int getSize() {
        return AIDebugBuffer.getEntries().size();
    }
    
    public void createAI(){
        AIUtilities.createAI();
    }
    
    public ArrayList<AIThread> getThreads(){
        return AIThreadHandler.getThreads();
    }

    /**
     * @return the dosome
     */
    public boolean isDosome() {
        return dosome;
    }

    /**
     * @param dosome the dosome to set
     */
    public void setDosome(boolean dosome) {
        this.dosome = dosome;
        AIUtilities.createAI();
    }

}
