/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;


/**
 *
 * @author Admin
 */
@ManagedBean(name = "ConversionBean")
@ApplicationScoped
public class ConversionBean implements Serializable {

   
  
    public String getDateFromLong(Long date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.mm.yy HH:mm:ss a");
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(date);
       
        return  sdf.format(gc.getTime());
    }
}
