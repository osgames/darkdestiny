/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.battlelog;

import at.viswars.enumeration.EDamageLevel;
import at.viswars.result.DesignResult;
import java.util.HashMap;

/**
 *
 * @author Orlov
 */
public class BattleReport {

    HashMap<Integer, DesignResult> designs;
    public void setCount(int designId, int count){
        DesignResult dr = designs.get(designId);
        if(dr == null){
            dr = new DesignResult(designId);
            dr.setCount(count);
        }else{
            dr.setCount(dr.getCount() + count);
        }
        designs.put(designId, dr);

    }
    public void setLost(int designId, int count){

        DesignResult dr = designs.get(designId);
        if(dr == null){
            dr = new DesignResult(designId);
            dr.setLost(count);
        }else{
            dr.setLost(dr.getLost() + count);
        }
        designs.put(designId, dr);

    }

    public void setDamage(int designId, float damage){

        DesignResult dr = designs.get(designId);
        if(dr == null){
            dr = new DesignResult(designId);
            dr.setDamage(damage);
        }else{
            dr.setDamage(dr.getDamage() + damage);
        }
        designs.put(designId, dr);
    }

    public void setDamageAbsorbedShield(int designId, float damage){

        DesignResult dr = designs.get(designId);
        if(dr == null){
            dr = new DesignResult(designId);
            dr.setDamageAbsorbedShield(damage);
        }else{
            dr.setDamageAbsorbedShield(dr.getDamageAbsorbedShield() + damage);
        }
        designs.put(designId, dr);
    }
    public void setDamageAbsorbedArmor(int designId, float damage){

        DesignResult dr = designs.get(designId);
        if(dr == null){
            dr = new DesignResult(designId);
            dr.setDamageAbsorbedArmor(damage);
        }else{
            dr.setDamageAbsorbedArmor(dr.getDamageAbsorbedArmor() + damage);
        }
        designs.put(designId, dr);
    }
    public void setDesignDamage(int designId, float damage){

        DesignResult dr = designs.get(designId);
        if(dr == null){
            dr = new DesignResult(designId);
            dr.setDesignDamage(damage);
        }else{
            dr.setDesignDamage(dr.getDesignDamage() + damage);
        }
        designs.put(designId, dr);
    }

    public void setDamagedShip(int designId, EDamageLevel damageLevel, int count){

        DesignResult dr = designs.get(designId);
        if(dr == null){
            dr = new DesignResult(designId);
        }
        if(dr.getDamaged().get(damageLevel) == null){
            dr.getDamaged().put(damageLevel, count);
        }else{
            dr.getDamaged().put(damageLevel, dr.getDamaged().get(damageLevel) + count);
        }
        designs.put(designId, dr);
    }
}
