/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.battlelog;

import java.util.ArrayList;

/**
 *
 * @author Dreloc
 */
public class TempConfig {
    private ArrayList<String> players;
    private long timeStamp;
    private BattleLogSimulationSet blss;

    public TempConfig(ArrayList<String> players, long timeStamp, BattleLogSimulationSet blss) {
        this.players = players;
        this.timeStamp = timeStamp;
        this.blss = blss;
    }

    /**
     * @return the players
     */
    public ArrayList<String> getPlayers() {
        return players;
    }

    /**
     * @return the timeStamp
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * @return the blss
     */
    public BattleLogSimulationSet getBlss() {
        return blss;
    }

}
