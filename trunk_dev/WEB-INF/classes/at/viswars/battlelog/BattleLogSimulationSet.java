/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.battlelog;

import at.viswars.spacecombat.*;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.interfaces.ISimulationSet;
import at.viswars.model.PlayerFleet;
import at.viswars.model.ShipFleet;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class BattleLogSimulationSet implements ISimulationSet, Serializable {

    PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    String battleClass;
    private final CombatReportGenerator.CombatType combatType;
    private final RelativeCoordinate relLocation;
    private final AbsoluteCoordinate absLocation;
    private final CombatReportGenerator crg;
    private HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants;
    private ArrayList<PlayerFleet> fleets = new ArrayList<PlayerFleet>();
    private ArrayList<ShipFleet> ships = new ArrayList<ShipFleet>();
    HashSet<Integer> genFleetIds = new HashSet<Integer>();
    HashMap<Integer, ArrayList<Integer>> groups = new HashMap<Integer, ArrayList<Integer>>();

    private final boolean HUShield;
    private final boolean PAShield;
    private final Integer planetOwner;
    private final HashMap<Integer, Integer> groundDefense;


    public BattleLogSimulationSet(HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants, HashMap<Integer, ArrayList<Integer>> groups, String battleClass, Integer planetOwner, boolean HUShield, boolean PAShield, HashMap<Integer, Integer> groundDefense) {
        this.participants = participants;
        combatType = CombatReportGenerator.CombatType.SPACE;
        crg = new CombatReportGenerator();
        relLocation = new RelativeCoordinate(0, 2600);
        absLocation = new AbsoluteCoordinate(0, 0);
        this.battleClass = battleClass;
        this.groups = groups;
        this.HUShield = HUShield;
        this.PAShield = PAShield;
        this.planetOwner = planetOwner;
        this.groundDefense = groundDefense;
        generateFleets();
    }

    private void generateFleets() {
        for (Map.Entry<Integer, HashMap<Integer, HashMap<Integer, Integer>>> user : participants.entrySet()) {
            int userId = user.getKey();
            for (Map.Entry<Integer, HashMap<Integer, Integer>> fleet : user.getValue().entrySet()) {
                PlayerFleet pf = new PlayerFleet();
                pf = new PlayerFleet();
                pf.setUserId(userId);
                pf.setName("Flotte : " + fleet.getKey());
                pf = pfDAO.add(pf);
                genFleetIds.add(pf.getId());

                for (Map.Entry<Integer, Integer> designs : fleet.getValue().entrySet()) {
                    ShipFleet sf = new ShipFleet();
                    sf.setDesignId(designs.getKey());
                    sf.setFleetId(pf.getId());
                    sf.setCount(designs.getValue());
                    sf = sfDAO.add(sf);
                    ships.add(sf);
                }
                fleets.add(pf);
            }
        }
    }

    @Override
    public void cleanUp() {
        for (Integer i : genFleetIds) {
            PlayerFleet pf = new PlayerFleet();
            pf.setId(i);
            pfDAO.remove(pf);
        }
        for (ShipFleet sf : ships) {
            sfDAO.remove(sf);
        }
    }

    public CombatReportGenerator.CombatType getCombatType() {
        return combatType;
    }

    public RelativeCoordinate getRelLocation() {
        return relLocation;
    }

    public AbsoluteCoordinate getAbsLocation() {
        return absLocation;
    }

    public CombatReportGenerator getCombatReportGenerator() {
        return crg;
    }

    public ArrayList<PlayerFleet> getFleets() {
        return fleets;
    }

    public void addPlayer(int groupId, int userId){
        ArrayList<Integer> users = groups.get(groupId);
        if(users == null){
            users = new ArrayList<Integer>();
        }
        users.add(userId);
        groups.put(groupId, users);
    }

    @Override
    public HashMap<Integer, ArrayList<Integer>> getGroups() {
        return groups;
    }

    @Override
    public String getBattleClass() {
        return battleClass;
    }

    @Override
    public int getPlanetOwner() {
        return planetOwner;
    }

    @Override
    public boolean hasHUShield() {
        return HUShield;
    }

    @Override
    public boolean hasPAShield() {
        return PAShield;
    }

    @Override
    public HashMap<Integer, Integer> getGroundDefense() {
        return groundDefense;
    }
}
