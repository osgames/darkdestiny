/*
 * GameInit.java
 *
 * Created on 29. April 2007, 19:57
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.viswars;

import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.GameDataDAO;
import at.viswars.dao.PlayerResearchDAO;
import at.viswars.dao.ResearchDAO;
import at.viswars.databuffer.*;
import at.viswars.model.Research;
import at.viswars.service.StartService;
import at.viswars.viewbuffer.HeaderBuffer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;

/**
 *
 * @author Stefan
 */
public class GameInit {

    private static ResearchDAO researchDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    private static GameDataDAO gameDataDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
    private static PlayerResearchDAO playerResearchDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static boolean init = false;

    /**
     * Creates a new instance of GameInit
     */
    private GameInit() {
        // This constructor of the utility class should not be called!
    }

    public static synchronized void initGame() {
        if (init) {
            return;
        }

        GameUtilities.setLastRestart(System.currentTimeMillis());
        try {

            Logger.getLogger().write("Resetting and updating Researchbonusd");
            for (Research r : (ArrayList<Research>) researchDAO.findAll()) {
                int size = playerResearchDAO.findByResearchId(r.getId()).size();
                double bonus = (double) size * 0.01d;
                if (bonus > 5) {
                    bonus = 5d;
                }
                r.setBonus(bonus);
                researchDAO.update(r);
            }
            try {
                GameConstants.UNIVERSE_HEIGHT = gameDataDAO.findById(1).getHeight();
                GameConstants.UNIVERSE_WIDTH = gameDataDAO.findById(1).getWidth();

            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Error while setting Height,Width from GameData to GameConstants");
            }

            try {
                ML.generateJS(null, null);
            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.WARNING, "Error creating ML-Javscript file");
            }

            // StartingArea.initStaticData();

            // Logger.getLogger().write("Initializing RessourceCostBuffer");
            //     RessourceCostBuffer.initialize();
            //  Logger.getLogger().write("Initializing TechRelationBuffer");
            //     TechRelationBuffer.initialize();
            // Logger.getLogger().write("Initializing ConstructionTable");
            //    ConstructionTable.initialize();
            // Logger.getLogger().write("Initializing ResearchTable");
            //    ResearchTable.initialize();
            Logger.getLogger().write("Initializing HeaderBuffer");
            HeaderBuffer.initialize();
            Logger.getLogger().write("Initializing RangeSizePenalty");
            RangeSizePenalty.initialize();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Initialization failed: ", e);
        }

        init = true;

        /*
         try {
         // ServerListener sl = new ServerListener();
         // sl.start();
         } catch (Exception e) {

         }       
         */
    }
}
