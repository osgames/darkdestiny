/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.setup;

import at.viswars.DebugBuffer;
import at.viswars.model.Galaxy;
import at.viswars.result.GalaxyCreationResult;
import at.viswars.service.Service;
import at.viswars.utilities.img.IModifyImageFunction;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;

/**
 *
 * @author Admin
 */
public class GalaxyRenderer implements IModifyImageFunction {

    private Galaxy galaxy;
    private int width;
    private int height;
    private final String path;
    private GalaxyCreationResult galaxyCreationResult;
    boolean byDatabase = true;

    public GalaxyRenderer(int galaxyId, String path) {

        galaxy = Service.galaxyDAO.findById(galaxyId);
        this.path = path;
    }

    public GalaxyRenderer(GalaxyCreationResult gcr, String path) {
        galaxyCreationResult = gcr;
        byDatabase = false;
        this.path = path;
    }
    /* (non-Javadoc)
     * @see at.viswars.img.IModifyImageFunction#run(java.awt.Graphics)
     */

    public void run(Graphics graphics) {
        Image img = null; //Toolkit.getDefaultToolkit().createImage(path + GetSystemView.getLargePic(pd.getPlanetId()));
        try {

            img = ImageIO.read(new File(path + "gstar.png"));



            if (byDatabase) {
                for (at.viswars.model.System s : (ArrayList<at.viswars.model.System>) Service.systemDAO.findAll()) {
                    if (s.getGalaxyId().equals(galaxy.getId())) {

                        graphics.drawImage(img, s.getX() - (galaxy.getOriginX() - (galaxy.getWidth() / 2)), s.getY() - (galaxy.getOriginY() - (galaxy.getHeight() / 2)), null);
                    }
                }
            } else {
                galaxy = galaxyCreationResult.getGalaxy();
                for (at.viswars.model.System s : (ArrayList<at.viswars.model.System>) galaxyCreationResult.getSystems()) {
                    graphics.drawImage(img, s.getX() - (galaxy.getOriginX() - (galaxy.getWidth() / 2)), s.getY() - (galaxy.getOriginY() - (galaxy.getHeight() / 2)), null);

                }
            }

        } catch (Exception e) {
            DebugBuffer.error("Picture not found " + e + "piclink " + path + "gstar.png");
        }
    }

    /* (non-Javadoc)
     * @see at.viswars.img.IModifyImageFunction#setSize(int, int)
     */
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public String createMapEntry() {
        return "";
    }
}
