/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.setup;

import at.viswars.enumeration.EPlanetDensity;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.model.Planet;
import at.viswars.model.PlanetRessource;
import at.viswars.model.Ressource;
import at.viswars.model.System;
import at.viswars.result.SystemGenerationResult;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public abstract class AbstractGalaxy implements IGalaxy {

    protected int width;
    protected int height;
    protected boolean destroyDBEntries;
    protected EPlanetDensity pDensity;
    protected int sysPerUser;
    protected int originX;
    protected int originY;

    public AbstractGalaxy(int width, int height, boolean destroyDBEntries, EPlanetDensity pDensity, int sysPerUser, int originX, int originY) {
        this.width = width;
        this.height = height;
        this.destroyDBEntries = destroyDBEntries;
        this.pDensity = pDensity;
        this.sysPerUser = sysPerUser;
        this.originX = originX;
        this.originY = originY;
    }

    public static SystemGenerationResult generateSystem(int posx, int posy, double distanceToCenter, int actSystemId, ArrayList<System> systemList, ArrayList<Planet> planetList, ArrayList<PlanetRessource> ressList, boolean startSystemOnly) {


        int systemsGenerated = 1;
        int planetsGenerated = 0;
        
        ArrayList<Planet> newPlanets = new ArrayList<Planet>();
        ArrayList<PlanetRessource> newRessources = new ArrayList<PlanetRessource>();

        double failChance = 0d;
        if (distanceToCenter <= 25d) { // Max fail 30%
            failChance = (30d / 25d * (25d - distanceToCenter)) / 100d;
        } else if (distanceToCenter >= 75d) { // Max fail 15%
            failChance = (15d / 25d * (100d - distanceToCenter)) / 100d;
        }

        boolean foundMPlanet = false;

        at.viswars.model.System s = new at.viswars.model.System();
        s.setId(actSystemId);
        s.setName("System " + (actSystemId));
        s.setX(posx);
        s.setY(posy);

        int planetCount = 1 + (int) (Math.random() * 12);

        // Reduce planets due to fail chance
        for (int k = 0; k < planetCount; k++) {
            if (Math.random() < failChance) {
                planetCount--;
            }
        }

        int actualPlanetCount = 0;
        boolean[] map = new boolean[13];
        while (actualPlanetCount < planetCount) {
            int orbitLevel = 1 + (int) (Math.random() * 12);
            if (!map[orbitLevel]) {
                actualPlanetCount++;
                map[orbitLevel] = true;
                int diameter = 0;
                if (orbitLevel <= 7) {
                    int zz = 1 + (int) (Math.random() * 20);
                    if (zz == 5) {
                        diameter = 50000 + (int) (Math.random() * 150000);
                    }
                } else {
                    int zz = 1 + (int) (Math.random() * 2);
                    if (zz == 1) {
                        diameter = 50000 + (int) (Math.random() * 250000);
                    }
                }
                if (diameter == 0) {
                    diameter = 6000 + (int) (Math.random() * 14000);
                }
                String landType = "";
                if (diameter >= 50000 && diameter <= 150000) {
                    landType = "B";
                }
                if (diameter > 150000) {
                    landType = "A";
                }
                if (landType.equalsIgnoreCase("")) {
                    if (orbitLevel < 5) {
                        int zz = 1 + (int) (Math.random() * 4);
                        if (zz <= 2) {
                            landType = "J";
                        } else {
                            landType = "E";
                        }
                    }
                    int zz = 1 + (int) (Math.random() * 100);
                    if (orbitLevel == 5) {
                        if (zz < 50) {
                            landType = "G";
                        }
                        if (zz >= 50 && zz < 60) {
                            landType = "M";
                        }
                        if (zz >= 60 && zz < 75) {
                            landType = "C";
                        }
                        if (zz >= 75) {
                            landType = "E";
                        }
                    }

                    if (orbitLevel == 6) {
                        if (zz < 25) {
                            landType = "G";
                        }
                        if (zz >= 25 && zz < 50) {
                            landType = "M";
                        }
                        if (zz >= 50 && zz < 75) {
                            landType = "C";
                        }
                        if (zz >= 75) {
                            landType = "E";
                        }
                    }

                    if (orbitLevel == 7) {
                        if (zz < 15) {
                            landType = "G";
                        }
                        if (zz >= 15 && zz < 25) {
                            landType = "M";
                        }
                        if (zz >= 25 && zz < 75) {
                            landType = "C";
                        }
                        if (zz >= 75) {
                            landType = "E";
                        }
                    }

                    if (orbitLevel > 7) {
                        landType = "L";
                    }
                }

                // Bis hierher ist definiert:
                // OrbitLevel
                // Diameter (Durchmesser)
                // Typ

                int avgTemp = 0;
                switch (orbitLevel) {
                    case 1:
                        avgTemp = 590 + (int) (Math.random() * 100);
                        break;
                    case 2:
                        avgTemp = 280 + (int) (Math.random() * 80);
                        break;
                    case 3:
                        avgTemp = 130 + (int) (Math.random() * 60);
                        break;
                    case 4:
                        avgTemp = 60 + (int) (Math.random() * 40);
                        break;
                    case 5:
                        avgTemp = 30 + (int) (Math.random() * 20);
                        break;
                    case 6:
                        avgTemp = 10 + (int) (Math.random() * 10);
                        break;
                    case 7:
                        avgTemp = 0 + (int) (Math.random() * 10);
                        break;
                    case 8:
                        avgTemp = -50 + (int) (Math.random() * 20);
                        break;
                    case 9:
                        avgTemp = -95 + (int) (Math.random() * 30);
                        break;
                    case 10:
                        avgTemp = -140 + (int) (Math.random() * 40);
                        break;
                    case 11:
                        avgTemp = -185 + (int) (Math.random() * 50);
                        break;
                    case 12:
                        avgTemp = -230 + (int) (Math.random() * 60);
                        break;
                }

                if (landType.equalsIgnoreCase("M")) {
                    avgTemp = 6 + (int) (Math.random() * 24);
                } else if (landType.equalsIgnoreCase("C")) {
                    avgTemp = -5 + (int) (Math.random() * 10);
                } else if (landType.equalsIgnoreCase("G")) {
                    avgTemp = 25 + (int) (Math.random() * 10);
                }

                long iron = 0;
                long ynkel = 0;
                long howal = 0;
                long cvEmb = 0;

                if (landType.equalsIgnoreCase("A")) {
                    int randVal = (diameter * 10) + (int) (Math.random() * (diameter * 10));
                    ynkel = randVal;
                } else if (landType.equalsIgnoreCase("B")) {
                    int randVal = (diameter * 10) + (int) (Math.random() * (diameter * 10));
                    ynkel = randVal;
                } else {
                    long volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (diameter / 2), 3)));

                    int orbitLevelDiff = 6 - orbitLevel;
                    if (orbitLevelDiff > 0) {
                        iron = (long) (((float) volume / (float) 1000) * (1f + ((float) orbitLevelDiff / (float) 2.5)));
                    } else if (orbitLevelDiff < 0) {
                        iron = (long) (((float) volume / (float) 1000) / (1f + ((float) Math.abs(orbitLevelDiff) / (float) 2.5)));
                    } else {
                        iron = (long) ((float) volume / (float) 1000);
                    }
                }

                boolean hasHowal = Math.random() < 0.1d;

                if (hasHowal) {
                    boolean hasMuchHowal = Math.random() < 0.05d;

                    if (hasMuchHowal) {
                        howal = (int) (60000 + Math.random() * 250000);
                    } else {
                        howal = (int) (Math.random() * 30000);
                    }
                }
                if(landType.equals(Planet.LANDTYPE_M)){
                    foundMPlanet = true;
                }

                planetsGenerated++;
                int planetId =  ((actSystemId - 1) * 12) + orbitLevel;

                Planet p = new Planet();
                p.setId(planetId);
                p.setSystemId(actSystemId);
                p.setLandType(landType);
                p.setAvgTemp(avgTemp);
                p.setDiameter(diameter);
                p.setOrbitLevel(orbitLevel);
                p.setAtmosphereType("Z");
                newPlanets.add(p);
                
                


                if (iron != 0) {
                    at.viswars.model.PlanetRessource pr = new at.viswars.model.PlanetRessource();
                    pr.setPlanetId(planetId);
                    pr.setType(EPlanetRessourceType.PLANET);
                    pr.setRessId(Ressource.PLANET_IRON);
                    pr.setQty(iron);
                    newRessources.add(pr);

                }
                if (ynkel != 0) {
                    at.viswars.model.PlanetRessource pr = new at.viswars.model.PlanetRessource();
                    pr.setPlanetId(planetId);
                    pr.setType(EPlanetRessourceType.PLANET);
                    pr.setRessId(Ressource.PLANET_YNKELONIUM);
                    pr.setQty(ynkel);
                    newRessources.add(pr);

                }
                if (howal != 0) {
                    at.viswars.model.PlanetRessource pr = new at.viswars.model.PlanetRessource();
                    pr.setPlanetId(planetId);
                    pr.setType(EPlanetRessourceType.PLANET);
                    pr.setRessId(Ressource.PLANET_HOWALGONIUM);
                    pr.setQty(howal);
                    newRessources.add(pr);

                }
            }
        }

        //If startsystem with m planet and no m-planet found dont save
        if (startSystemOnly && !foundMPlanet) {
            //do nothing
            
            return new SystemGenerationResult(systemsGenerated, planetsGenerated, false);
        } else {
            systemList.add(s);
            planetList.addAll(newPlanets);
            ressList.addAll(newRessources);
            return new SystemGenerationResult(systemsGenerated, planetsGenerated, true);

        }
    }
}
