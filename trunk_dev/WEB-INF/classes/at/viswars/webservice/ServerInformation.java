/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.webservice;

import at.viswars.GameConfig;
import at.viswars.GameUtilities;
import at.viswars.ML;
import at.viswars.enumeration.EGameDataStatus;
import at.viswars.enumeration.EPassword;
import at.viswars.enumeration.EUserGender;
import at.viswars.model.GameData;
import at.viswars.model.Language;
import at.viswars.model.PlayerStatistic;
import at.viswars.model.Statistic;
import at.viswars.model.User;
import at.viswars.service.Service;
import at.viswars.servlets.portal.RegistrationServlet.RegistrationValue;
import at.viswars.view.html.form;
import at.viswars.view.html.option;
import at.viswars.view.html.select;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.webservice.dto.PlayerStatisticList;
import at.viswars.webservice.dto.PlayerStatisticListEntry;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import org.richfaces.component.util.Strings;

/**
 * wsimport -d c:\daten\darkdestiny-portal\trunk\WEB-INF\classes
 * http://127.0.0.1:9090/serverinformation?wsdl -keep -Xnocompile
 *
 * @author Admin
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class ServerInformation {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "isUserRegistered")
    public Boolean isUserRegistered(@WebParam(name = "userName") String userName) {
        User u = Service.userDAO.findByUserName(userName);
        if (u != null) {
            return true;
        } else {
            return false;
        }
    }

    @WebMethod(operationName = "isPortalPasswordValid")
    public boolean isPortalPasswordValid(@WebParam(name = "userName") String userName, @WebParam(name = "webServicePassword") String webServicePassword, @WebParam(name = "portalPassword") String portalPassword) {
        if (!checkWebServicePassword(webServicePassword)) {
            return false;
        }
        User u = Service.userDAO.findByUserName(userName);
        if (u != null && u.getPortalPassword().equals(portalPassword)) {
            return true;
        } else {
            return false;
        }
    }

    @WebMethod(operationName = "getPortalPassword")
    public String getPortalPassword(@WebParam(name = "userName") String userName, @WebParam(name = "webServicePassword") String webServicePassword, @WebParam(name = "registrationToken") String registrationToken) {
        if (!checkWebServicePassword(webServicePassword)) {
            return "";
        }
        User u = Service.userDAO.findByUserName(userName);
        if (u != null && u.getRegistrationToken().equals(registrationToken)) {
            return u.getPortalPassword();
        } else {
            return "";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getRank")
    public Integer getRank(@WebParam(name = "gamename") String gamename) {
        User u = Service.userDAO.findByGameName(gamename);
        if (u != null) {
            Statistic s = Service.statisticDAO.findByUserId(u.getUserId());
            if (s != null) {
                return s.getRank();
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getPoints")
    public Integer getPoints(@WebParam(name = "gamename") String gamename) {
        User u = Service.userDAO.findByGameName(gamename);
        if (u != null) {
            Statistic s = Service.statisticDAO.findByUserId(u.getUserId());
            if (s != null) {
                return s.getPoints();
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    @WebMethod(operationName = "incrementStatus")
    public void incrementStatus(@WebParam(name = "webServicePassword") String webServicePassword) {

        if (!checkWebServicePassword(webServicePassword)) {
            return;
        }
        GameData gameData = Service.gameDataDAO.findAll().get(0);
        boolean found = false;
        for (EGameDataStatus s : EGameDataStatus.values()) {
            if (found) {
                gameData.setStatus(s);
                Service.gameDataDAO.update(gameData);
                break;
            }
            if (s.equals(gameData.getStatus())) {
                found = true;
            }
        }
    }

    @WebMethod(operationName = "decrementStatus")
    public void decrementStatus(@WebParam(name = "webServicePassword") String webServicePassword) {
        System.out.println("Received decrement Status action");
        System.out.println("compare : " + Service.passwordDAO.findByType(EPassword.WEBSERVICE).getPassword() + " to " + webServicePassword);
        if (!checkWebServicePassword(webServicePassword)) {
            return;
        }
        EGameDataStatus tmpPrio = null;
        GameData gameData = Service.gameDataDAO.findAll().get(0);
        for (EGameDataStatus s : EGameDataStatus.values()) {
            if (s.equals(gameData.getStatus()) && tmpPrio != null) {
                gameData.setStatus(tmpPrio);
                Service.gameDataDAO.update(gameData);
                break;
            }
            tmpPrio = s;
        }

    }

    @WebMethod(operationName = "getLoginForm")
    public String getLoginForm(@WebParam(name = "webServicePassword") String webServicePassword, @WebParam(name = "locale") String l) {
        if (!checkWebServicePassword(webServicePassword)) {
            return "Error, wrong webservice password";
        }
        Locale locale = new Locale("de", "DE");
        if (l != null && !l.equals("")) {
            locale = new Locale(l);
        }

        StringBuffer stringBuffer = new StringBuffer();

        form f = new form();
        f.setProperties("name='anmelden' method='post' action='" + GameConfig.getInstance().getHostURL() + "/RegistrationServlet'");

        table t = new table();

        tr tr = new tr();
        tr.addTd("<input type='hidden' name='" + RegistrationValue.NAME_USERNAME + "' value='%USERNAME_VALUE%'/>");
        tr.addTd("<input type='hidden' name='" + RegistrationValue.NAME_REGISTRATIONTOKEN + "' value='%REGISTRATIONTOKEN_VALUE%'/>");
        tr.addTd("<input type='hidden' name='" + RegistrationValue.NAME_SOURCE_URL + "' value='%NAME_SOURCE_URL_VALUE%'/>");
        t.addTr(tr);

        tr = new tr();
        tr.addTd("Spielername");
        tr.addTd("<input name='" + RegistrationValue.NAME_GAMENAME + "'/>");
        t.addTr(tr);

        tr = new tr();
        tr.addTd("Password (L�nge min. 5)");
        tr.addTd("<input type='password' name='" + RegistrationValue.NAME_PASS1 + "'/>");
        t.addTr(tr);



        tr = new tr();
        tr.addTd("Password Confirmation");
        tr.addTd("<input type='password' name='" + RegistrationValue.NAME_PASS2 + "'/>");
        t.addTr(tr);


        tr = new tr();
        tr.addTd("Email");
        tr.addTd("<input name='" + RegistrationValue.NAME_EMAIL + "'/>");
        t.addTr(tr);


        tr = new tr();
        tr.addTd("Sprache");
        select s = new select();
        s.setProperties(" name='" + RegistrationValue.NAME_LOCALE + "'");
        for (Language language : Service.languageDAO.findAll()) {
            option o = new option();
            o.setData(ML.getMLStr(language.getName(), locale));
            o.setProperties("value=" + language.getId());
            s.addOption(o);
        }
        td td = new td(s.toString());
        tr.addTd(td);
        t.addTr(tr);


        tr = new tr();
        tr.addTd("Geschlecht (ihres Charakters)");
        s = new select();
        s.setProperties(" name='" + RegistrationValue.NAME_GENDER + "'");
        for (EUserGender gender : EUserGender.values()) {
            option o = new option();
            o.setData(ML.getMLStr("login_opt_" + gender.toString(), locale));
            o.setProperties("value=" + gender.toString());
            s.addOption(o);
        }
        td = new td(s.toString());

        tr.addTd(td);
        t.addTr(tr);

        tr = new tr();
        tr.addTd("<input type='submit' action='submit' value='Abschicken'/>", " colspan='2' style='text-align:center'");
        t.addTr(tr);

        f.addDiv(t);
        stringBuffer.append(f.toString());
        return stringBuffer.toString();
    }

    public boolean checkWebServicePassword(String webServicePassword) {
        if (Service.passwordDAO.findByType(EPassword.WEBSERVICE) == null) {

            return false;
        }
        return ((Service.passwordDAO.findByType(EPassword.WEBSERVICE).getValue().equals(webServicePassword)));
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getStatus")
    public String getStatus() {
        return Service.gameDataDAO.findAll().get(0).getStatus().toString();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUserOnlineCount")
    public Integer getUserOnlineCount() {

        return GameUtilities.getUsersOnlineSince(2);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUserCount")
    public Integer getUserCount() {

        return Service.userDAO.findAllPlayers().size();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getStatistics")
    public PlayerStatisticList getStatistics(@WebParam(name = "date") final long date) {
        PlayerStatisticList psl = new PlayerStatisticList();
        System.out.println("Serverside statistics call!");

        Date d = new Date(date);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        System.out.println(sdf.format(d));
        ArrayList<PlayerStatisticListEntry> psle = convertPlayerStatistics(Service.playerStatisticDAO.findByDay(date), date);
        System.out.println("Found : " + psle.size() + " entries");
        psl.setPlayerStatistics(psle);
        return psl;
    }

    private ArrayList<PlayerStatisticListEntry> convertPlayerStatistics(ArrayList<PlayerStatistic> statistics, long date) {
        ArrayList<PlayerStatisticListEntry> result = new ArrayList<PlayerStatisticListEntry>();
        for (PlayerStatistic ps : statistics) {
            PlayerStatisticListEntry plse = new PlayerStatisticListEntry();
            plse.setDate(date);
            String gameName = "Unbekannt";
            System.out.println("UserId : " + ps.getUserId());
            if (Service.userDAO.findById(ps.getUserId()) != null) {
                gameName = Service.userDAO.findById(ps.getUserId()).getGameName();
            }
            plse.setGameName(gameName);
            plse.setRank(ps.getRank());
            plse.setMilitaryPoints(ps.getMilitaryPoints());
            plse.setPopulationPoints(ps.getPopulationPoints());
            plse.setResearchPoints(ps.getResearchPoints());
            plse.setRessourcePoints(ps.getRessourcePoints());
            result.add(plse);
        }
        return result;
    }
}
