/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.webservice;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.model.Quiz;
import at.viswars.model.QuizAnswer;
import at.viswars.model.QuizEntry;
import at.viswars.model.QuizQuestion;
import at.viswars.model.Statistic;
import at.viswars.model.User;
import at.viswars.service.Service;
import at.viswars.utilities.MD5;
import at.viswars.webservice.dto.QuestionList;
import at.viswars.webservice.dto.QuizResult;
import java.util.ArrayList;
import java.util.HashSet;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 *
 * @author Admin
 */
@WebService(name="IRC")
@SOAPBinding(style = Style.RPC)
public class IRC {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "setUserCount")
    @Oneway
    public void setUserCount(@WebParam(name = "count") int count) {
        DebugBuffer.addLine(DebugLevel.DEBUG, "[WEBSERVICE] Set Chat User Count to : " + count);
        WebData.setChatUserCount(count);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "isUserRegistered")
    public Boolean isUserRegistered(@WebParam(name = "gamename") String gamename) {
        //TODO write your implementation code here:
        User u = Service.userDAO.findByGameName(gamename);
        if (u != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "isUserIdentified")
    public Boolean isUserIdentified(@WebParam(name = "gamename") String gamename, @WebParam(name = "password") String password) {
        //TODO write your implementation code here:
        User u = Service.userDAO.findByGameName(gamename);
        if (u != null && u.getPassword().equals(MD5.encryptPassword(password))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getRank")
    public Integer getRank(@WebParam(name = "gamename") String gamename) {
        //TODO write your implementation code here:
        User u = Service.userDAO.findByGameName(gamename);
        if (u != null) {
            Statistic s = Service.statisticDAO.findByUserId(u.getUserId());
            if (s != null) {
                return s.getRank();
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getPoints")
    public Integer getPoints(@WebParam(name = "gamename") String gamename) {
        //TODO write your implementation code here:
        User u = Service.userDAO.findByGameName(gamename);
        if (u != null) {
            Statistic s = Service.statisticDAO.findByUserId(u.getUserId());
            if (s != null) {
                return s.getPoints();
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "addQuiz")
    @Oneway
    public void addQuiz(@WebParam(name = "name") String name, @WebParam(name = "topic") String topic) {
        Quiz q = new Quiz();
        q.setName(name);
        q.setTopic(topic);
        Service.quizDAO.add(q);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getQuizList")
    public at.viswars.webservice.dto.QuizResult getQuizList() {
        //TODO write your implementation code here:
        ArrayList<at.viswars.webservice.dto.Quiz> entries = new ArrayList<at.viswars.webservice.dto.Quiz>();
        for (Quiz q : (ArrayList<Quiz>) Service.quizDAO.findAll()) {
            at.viswars.webservice.dto.Quiz quiz = new at.viswars.webservice.dto.Quiz();
            quiz.setId(q.getId());
            quiz.setName(q.getName());
            quiz.setTopic(q.getTopic());
            entries.add(quiz);
        }
        QuizResult qr = new QuizResult();
        qr.setQuizList(entries);
        return qr;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "removeQuiz")
    public Integer removeQuiz(Integer id) {
        //TODO write your implementation code here:
        Quiz q = Service.quizDAO.findById(id);

        if (q != null) {
            Service.quizDAO.remove(q);
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getQuiz")
    public at.viswars.webservice.dto.Quiz getQuiz(Integer id) {
        //TODO write your implementation code here:
        at.viswars.webservice.dto.Quiz quiz = new at.viswars.webservice.dto.Quiz();
        Quiz q = Service.quizDAO.findById(id);

        quiz.setId(q.getId());
        quiz.setName(q.getName());
        quiz.setTopic(q.getTopic());
        return quiz;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "addQuestion")
    public Integer addQuestion(@WebParam(name = "quizId") Integer quizId, @WebParam(name = "difficulty") Integer difficulty, @WebParam(name = "question") String question, @WebParam(name = "answer") String answer, @WebParam(name = "author") String author) {

        Quiz q = Service.quizDAO.findById(quizId);
        if (q == null) {
            return -1;
        } else {
            QuizEntry qe = new QuizEntry();
            qe.setQuizId(quizId);
            qe.setDifficulty(difficulty);
            qe.setAuthor(author);

            qe = Service.quizEntryDAO.add(qe);

            QuizQuestion qq = new QuizQuestion();
            qq.setQuizEntryId(qe.getId());
            qq.setQuestion(question);

            qq = Service.quizQuestionDAO.add(qq);



            QuizAnswer qa = new QuizAnswer();
            qa.setQuizEntryId(qe.getId());
            qa.setAnswer(answer);

            qa = Service.quizAnswerDAO.add(qa);
            answer = null;
            question = null;
        }


        return 1;

    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "deleteQuestion")
    public Integer deleteQuestion(@WebParam(name = "id") Integer id) {
        //TODO write your implementation code here:

        QuizEntry qe = Service.quizEntryDAO.findById(id);
        if (qe == null) {
            return -1;
        } else {
            Service.quizEntryDAO.remove(qe);
            return 1;
        }
    }


    /**
     * Web service operation
     */
    @WebMethod(operationName = "getQuestionList")
    public at.viswars.webservice.dto.QuestionList getQuestionList(@WebParam(name = "quizId") Integer quizId, @WebParam(name = "amount") Integer amount, @WebParam(name = "startId") Integer startId, @WebParam(name = "random") Boolean random) {
        ArrayList<at.viswars.webservice.dto.Question> entries = new ArrayList<at.viswars.webservice.dto.Question>();
        ArrayList<QuizEntry> questions = new ArrayList<QuizEntry>();
        if (random) {
            questions = (ArrayList<QuizEntry>) Service.quizEntryDAO.findByQuizId(quizId);
            ArrayList<QuizEntry> questionsTmp = new ArrayList<QuizEntry>();
            int size = questions.size();
            if (amount > size) {
                amount = size;
            }
            HashSet<Integer> randomValues = new HashSet<Integer>();

            while (randomValues.size() < amount) {
                int rnd = (int) (Math.random() * size);

                while (randomValues.contains(rnd)) {
                    rnd++;
                    if (rnd > size) {
                        rnd = 1;
                    }
                }
                randomValues.add(rnd);
            }
            for (Integer value : randomValues) {
                questionsTmp.add(questions.get(value));
            }
            questions = questionsTmp;

        } else {
            questions = (ArrayList<QuizEntry>) Service.quizEntryDAO.findByQuizId(quizId);
        }
        for (QuizEntry q : questions) {
            at.viswars.webservice.dto.Question question = new at.viswars.webservice.dto.Question();
            QuizQuestion qq = Service.quizQuestionDAO.findByQuizEntryId(q.getId()).get(0);
            QuizAnswer qa = Service.quizAnswerDAO.findByQuizEntryId(q.getId()).get(0);

            question.setQuestion(qq.getQuestion());
            question.setAnswer(qa.getAnswer());
            question.setId(q.getId());

            entries.add(question);
        }
        QuestionList ql = new QuestionList();
        ql.setQuestions(entries);
        return ql;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getQuestionCount")
    public Integer getQuestionCount(@WebParam(name = "quizId") Integer quizId) {
        ArrayList<QuizEntry> qqs = (ArrayList<QuizEntry>) Service.quizEntryDAO.findByQuizId(quizId);
        return qqs.size();
    }
}
