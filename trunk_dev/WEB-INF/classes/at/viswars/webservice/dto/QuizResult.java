/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.webservice.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "quizResult")
public class QuizResult {

    @XmlElements({
        @XmlElement(name = "quizList", type = at.viswars.webservice.dto.Quiz.class)})
    private List<Quiz> quizList;

    /**
     * @return the quizList
     */
    public List<Quiz> getQuizList() {
        return quizList;
    }

    /**
     * @param quizList the quizList to set
     */
    public void setQuizList(List<Quiz> quizList) {
        this.quizList = quizList;
    }
}
