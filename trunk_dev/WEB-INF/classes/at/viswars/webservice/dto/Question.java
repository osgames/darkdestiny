/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.webservice.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author Admin
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="question")
public class Question {
    @XmlElement(name="id")
    private Integer id;    
    @XmlElement(name="question")
    private String question;
    @XmlElement(name="answer")
    private String answer;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * @param question the question to set
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     * @return the answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * @param answer the answer to set
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

}
