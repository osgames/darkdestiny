/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.webservice.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "questionList")
public class QuestionList {

    @XmlElements({
        @XmlElement(name = "questions", type = at.viswars.webservice.dto.Question.class)})
    private List<Question> questions;

    /**
     * @return the questions
     */
    public List<Question> getQuestions() {
        return questions;
    }

    /**
     * @param questions the questions to set
     */
    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

}
