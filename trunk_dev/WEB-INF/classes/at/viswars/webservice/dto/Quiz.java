/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.webservice.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author Admin
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="quiz")
public class Quiz {
    @XmlElement(name="id")
    private Integer id;    
    @XmlElement(name="name")
    private String name;
    @XmlElement(name="topic")
    private String topic;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }
}
