/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.databuffer;

import at.viswars.DebugBuffer;
import java.util.*;
import at.viswars.database.access.DbConnect;
import java.sql.*;

/**
 *
 * @author Stefan
 */
public class RessourceCostBuffer {
    private static HashMap<Integer,HashMap<Integer,ArrayList<RessAmountEntry>>> typeMap;
    
    public final static int COST_CONSTRUCTION = 0;
    public final static int COST_MODULE = 1;
    public final static int COST_GROUPTROOPS = 2;
    public final static int COST_SHIP = 3;
    
    private static boolean init = false;
    
    public static synchronized void initialize() 
            throws SQLException {
        if (init) return;
        
        typeMap = new HashMap<Integer,HashMap<Integer,ArrayList<RessAmountEntry>>>();
        
        Statement stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT type, id, ressId, qty FROM ressourcecost");
        
        while (rs.next()) {
            if (typeMap.containsKey(rs.getInt(1))) {
                HashMap<Integer,ArrayList<RessAmountEntry>> tmpIdMap = typeMap.get(rs.getInt(1));
                
                if (tmpIdMap.containsKey(rs.getInt(2))) {
                    ArrayList<RessAmountEntry> tmpList = tmpIdMap.get(rs.getInt(2));
                    
                    tmpList.add(new RessAmountEntry(rs.getInt(3), rs.getInt(4)));
                } else {
                    ArrayList<RessAmountEntry> tmpList = new ArrayList<RessAmountEntry>();
                    tmpList.add(new RessAmountEntry(rs.getInt(3), rs.getInt(4)));
                    tmpIdMap.put(rs.getInt(2), tmpList);
                }
            } else {
                ArrayList<RessAmountEntry> tmpList = new ArrayList<RessAmountEntry>();
                tmpList.add(new RessAmountEntry(rs.getInt(3), rs.getInt(4)));
                
                HashMap<Integer,ArrayList<RessAmountEntry>> tmpIdMap = new HashMap<Integer,ArrayList<RessAmountEntry>>();
                tmpIdMap.put(rs.getInt(2), tmpList);                
                
                DebugBuffer.trace("Adding " + rs.getInt(1) + " to typeMap");
                typeMap.put(rs.getInt(1),tmpIdMap);
            }                        
        }
        
        if (!typeMap.containsKey(COST_CONSTRUCTION)) typeMap.put(COST_CONSTRUCTION, new HashMap<Integer,ArrayList<RessAmountEntry>>());
        if (!typeMap.containsKey(COST_MODULE)) typeMap.put(COST_MODULE, new HashMap<Integer,ArrayList<RessAmountEntry>>());
        if (!typeMap.containsKey(COST_GROUPTROOPS)) typeMap.put(COST_GROUPTROOPS, new HashMap<Integer,ArrayList<RessAmountEntry>>());
        if (!typeMap.containsKey(COST_SHIP)) typeMap.put(COST_SHIP, new HashMap<Integer,ArrayList<RessAmountEntry>>());
        
        init = true;
    }
    
    public static ArrayList<RessAmountEntry> getRessourceCost(int type, int id) {
        if (!init) {
            try {
                initialize();
            } catch (SQLException e) {
                DebugBuffer.writeStackTrace("Ressource Cost error: ", e);
                throw new RuntimeException(e);
            }
        }
        
        if (typeMap.containsKey(type)) {
            HashMap<Integer,ArrayList<RessAmountEntry>> tmpIdMap = typeMap.get(type);
            if (tmpIdMap.containsKey(id)) {
                return tmpIdMap.get(id);
            } else {
                // This building has no Ressource Consumption on construction
                ArrayList<RessAmountEntry> emptyRess = new ArrayList<RessAmountEntry>();
                return emptyRess;
            }
        } else {
            DebugBuffer.error("TypeMap for construction cost not initialized!");
        }
        
        return new ArrayList<RessAmountEntry>();
    }
    
    public static void setRessourceCost(int type, int id, ArrayList<RessAmountEntry> ress) {
            // Check Array for empty entries 
            ArrayList<RessAmountEntry> markedForDel = new ArrayList<RessAmountEntry>();
        
            for (RessAmountEntry rae : ress) {                
                if (rae.getQty() <= 0) {
                    DebugBuffer.debug("TYPE " + type + " with ID " + id + " was cleaned by a zero ress entry for ressource ");
                    markedForDel.add(rae);
                }                
            }
            
            for (RessAmountEntry rae : markedForDel) {
                ress.remove(rae);
            }   
            markedForDel.clear();
        
            if (typeMap.containsKey(type)) {
                HashMap<Integer,ArrayList<RessAmountEntry>> tmpIdMap = typeMap.get(type);                
                tmpIdMap.put(id, ress);
            } else {
                HashMap<Integer,ArrayList<RessAmountEntry>> tmpIdMap = new HashMap<Integer,ArrayList<RessAmountEntry>>();
                tmpIdMap.put(id, ress);                
                
                typeMap.put(type,tmpIdMap);
            }        
    }
    
    public static void storeEntry(int type, int id) {
        try {
            Statement stmt = DbConnect.createStatement();
            stmt.execute("DELETE FROM ressourcecost WHERE type="+type+" AND id="+id);
            
            for (RessAmountEntry rae : getRessourceCost(type,id)) {
                stmt.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES ("+type+","+id+","+rae.getRessId()+","+rae.getQty()+")");
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on storing cost data: ",e);
        }
    }
}
