/*
 * LockHandler.java
 *
 * Created on 18. Oktober 2006, 11:43
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.databuffer;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
/**
 *
 * @author Stefan
 */
public class LockHandler {
    
    private static final int MAX_WAIT_NUMERS = 400;
    boolean lock = false;
    Exception e = null;
    
    /** Creates a new instance of LockHandler */
    public LockHandler() {
        
    }
    
    public void CheckLockPP() {
        int waited = MAX_WAIT_NUMERS;
        while (lock) {
            try {
                DebugBuffer.addLine(DebugLevel.TRACE,"WAITING FOR LOCK RELEASE");
                waitForLock();
                waited --;
                if (waited == 0) {
                    throw new RuntimeException("Waiting too long for Lock", e);
                }
            } catch (InterruptedException e) {
                
            }
        }
    }
    
    private synchronized void waitForLock() throws InterruptedException {
        wait(100);
    }
    
    public synchronized void unlock() {
        if (!lock)
            return;
        lock = false;
        e = null;
        notifyAll();
    }
    
    public synchronized void lock() {
        int waited = MAX_WAIT_NUMERS;
        while (lock) {
            try {
                DebugBuffer.addLine(DebugLevel.TRACE,"WAITING FOR LOCK RELEASE");
                waitForLock();
                waited --;
                if (waited == 0) {
                    throw new RuntimeException("Waiting too long for Lock", e);
                }
            } catch (InterruptedException e) {
                
            }
        }
        lock = true;
        e = new Exception("Locking the Table");
    }
}
