/*
 * ConfirmEntry.java
 *
 * Created on 04. Oktober 2006, 17:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.databuffer;

import java.util.*;

/**
 *
 * @author Stefan
 */
public class ConfirmEntry {
    private long entryTime;
    private int userId;
    private int type;
    private Map<String, Object> parMap;
    
    /** Creates a new instance of ConfirmEntry */
    public ConfirmEntry() {
        
    }

    public long getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(long entryTime) {
        this.entryTime = entryTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Map<String, Object> getParMap() {
        return parMap;
    }

    public void setParMap(Map<String, Object> parMap) {
        this.parMap = parMap;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    
}
