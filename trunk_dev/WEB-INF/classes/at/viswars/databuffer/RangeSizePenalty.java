/*
 * RangeSizePenalty.java
 *
 * Created on 29. J�nner 2008, 15:17
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.databuffer;

import at.viswars.Logger.Logger;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class RangeSizePenalty {
    private static HashMap<Integer,HashMap<Integer,Double>> rangeMap;
    private static HashMap<Integer,HashMap<Integer,Double>> rangeSizePenaltyMap;
    private static boolean init = false;
    
    public RangeSizePenalty() {

    }    
    
    public static void initialize() {
        if (init) return;
        
        rangeMap = new HashMap<Integer,HashMap<Integer,Double>>();
        rangeSizePenaltyMap = new HashMap<Integer,HashMap<Integer,Double>>();
        
        // Init rangeMap
        double base = 0.4d;
        double realBase = 0.4d;        
        
        for (int i=1;i<=10;i++) {
            double actBase = realBase;
            HashMap<Integer,Double> rangeI = new HashMap<Integer,Double>();            
            
            for (int j=1;j<=6;j++) {                
                double value = actBase;
                if (value < 0) value = 0;
                
                rangeI.put(j,value);
                
                actBase += 0.05d;
            }
            
            rangeMap.put(i,rangeI);
            
            if (base > 0) base -= 0.05d;
            realBase -= 0.05d;
        }
        
        // Init rangeSizePenaltyMap
        for (int i=1;i<=10;i++) {
            HashMap<Integer,Double> oneMap = new HashMap<Integer,Double>();
            for (int j=1;j<=6;j++) { 
                oneMap.put(j,1d);
            }
            
            rangeSizePenaltyMap.put(i,oneMap);
        }
        
        rangeSizePenaltyMap.get(3).put(1,0.9d);
        
        rangeSizePenaltyMap.get(4).put(1,0.8d);
        rangeSizePenaltyMap.get(4).put(1,0.9d);
        
        rangeSizePenaltyMap.get(5).put(1,0.75d);
        rangeSizePenaltyMap.get(5).put(1,0.8d);
        rangeSizePenaltyMap.get(5).put(1,0.9d);
        
        rangeSizePenaltyMap.get(6).put(1,0.7d);
        rangeSizePenaltyMap.get(6).put(1,0.75d);
        rangeSizePenaltyMap.get(6).put(1,0.8d);
        rangeSizePenaltyMap.get(6).put(1,0.9d);
        
        rangeSizePenaltyMap.get(7).put(1,0.65d);
        rangeSizePenaltyMap.get(7).put(1,0.7d);
        rangeSizePenaltyMap.get(7).put(1,0.75d);
        rangeSizePenaltyMap.get(7).put(1,0.8d);
        rangeSizePenaltyMap.get(7).put(1,0.9d);
        
        rangeSizePenaltyMap.get(8).put(1,0.6d);
        rangeSizePenaltyMap.get(8).put(1,0.65d);
        rangeSizePenaltyMap.get(8).put(1,0.7d);
        rangeSizePenaltyMap.get(8).put(1,0.75d);
        rangeSizePenaltyMap.get(8).put(1,0.8d);
        rangeSizePenaltyMap.get(8).put(1,0.9d);
        
        rangeSizePenaltyMap.get(9).put(1,0.55d);
        rangeSizePenaltyMap.get(9).put(1,0.6d);
        rangeSizePenaltyMap.get(9).put(1,0.65d);
        rangeSizePenaltyMap.get(9).put(1,0.7d);
        rangeSizePenaltyMap.get(9).put(1,0.75d);
        rangeSizePenaltyMap.get(9).put(1,0.8d);
        
        rangeSizePenaltyMap.get(10).put(1,0.5d);
        rangeSizePenaltyMap.get(10).put(1,0.55d);
        rangeSizePenaltyMap.get(10).put(1,0.6d);
        rangeSizePenaltyMap.get(10).put(1,0.65d);
        rangeSizePenaltyMap.get(10).put(1,0.7d);
        rangeSizePenaltyMap.get(10).put(1,0.75d);
        
        checkPenaltyMap();
        
        init = true;
    }
    
    public static Double getRangePenalty(int range, int chassisSize) {
        Logger.getLogger().write("Looking for RangePenalty " + range + "/" + chassisSize);
        if (!rangeMap.containsKey(range)) return null;
        
        switch (chassisSize) {
            case(3):
                return rangeMap.get(range).get(chassisSize - 2);
            case(4):
                return rangeMap.get(range).get(chassisSize - 2);
            case(5):
                return rangeMap.get(range).get(chassisSize - 2);
            case(6):
                return rangeMap.get(range).get(chassisSize - 2);
            case(7): // Kampfstation klein 
                return rangeMap.get(range).get(chassisSize - 4);
            case(8):
                return rangeMap.get(range).get(chassisSize - 3);
            case(9): // Kampfstation gro� 
                return rangeMap.get(range).get(chassisSize - 4);
            case(10):
                return rangeMap.get(range).get(chassisSize - 4);     
        }
        
        return null;
    }
    
    public static Double getRangeSizePenalty(int range, int chassisSize) {
        if (!rangeMap.containsKey(range)) return null;
        
        switch (chassisSize) {
            case(3):
                return rangeSizePenaltyMap.get(range).get(chassisSize - 2);
            case(4):
                return rangeSizePenaltyMap.get(range).get(chassisSize - 2);
            case(5):
                return rangeSizePenaltyMap.get(range).get(chassisSize - 2);
            case(6):
                return rangeSizePenaltyMap.get(range).get(chassisSize - 2);
            case(7): // Kampfstation klein 
                return rangeSizePenaltyMap.get(range).get(chassisSize - 4);
            case(8):
                return rangeSizePenaltyMap.get(range).get(chassisSize - 2);
            case(9): // Kampfstation gro� 
                return rangeSizePenaltyMap.get(range).get(chassisSize - 4);
            case(10):
                return rangeSizePenaltyMap.get(range).get(chassisSize - 2);     
        }
        
        return null;        
    }
    
    public static void checkPenaltyMap() {
        for (int i=1;i<=10;i++) {
            // Logger.getLogger().write("RANGEMAP " + i);
            for (int j=1;j<=6;j++) {
               // Logger.getLogger().write("Range penalty on size " + j + " = " + rangeMap.get(i).get(j));
            }
        }
        /*
        for (Integer i : rangeMap.keySet()) {
            //Logger.getLogger().write("RANGEMAP " + i);
            for (Double d : rangeMap.get(i).values()) {
                //Logger.getLogger().write("VALUE = " + d);
            }
        }*/
    }
}
