/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.databuffer;

/**
 *
 * @author Stefan
 */
public class RessAmountEntry {
    protected int ressId;
    protected double qty;

    protected RessAmountEntry() {
        
    }
    
    public RessAmountEntry(int ressId, double qty) {
        this.ressId = ressId;
        this.qty = qty;
    }   

    public int getRessId() {
        return ressId;
    }

    public double getQty() {
        return qty;
    }
    
}
