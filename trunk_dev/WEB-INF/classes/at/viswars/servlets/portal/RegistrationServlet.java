/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.servlets.portal;

import at.viswars.GameConfig;
import at.viswars.GameUtilities;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.UserDAO;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.enumeration.EUserGender;
import at.viswars.model.Language;
import at.viswars.model.LoginTracker;
import at.viswars.model.User;
import at.viswars.result.BaseResult;
import at.viswars.result.RegisterResult;
import at.viswars.service.LoginService;
import at.viswars.service.Service;
import at.viswars.utilities.MD5;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Stefan
 */
public class RegistrationServlet extends HttpServlet {

    public enum RegistrationValue {

        NAME_USERNAME, NAME_GAMENAME, NAME_REGISTRATIONTOKEN, NAME_PASS1, NAME_PASS2, NAME_LOCALE, NAME_GENDER, NAME_EMAIL, NAME_SOURCE_URL
    }
    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        TransactionHandler th = TransactionHandler.getTransactionHandler();

        String registrationUrl = "";
        try {

            for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
                System.out.println(entry.getKey() + " => " + entry.getValue()[0]);
            }

            String userName = request.getParameter(RegistrationValue.NAME_USERNAME.toString());
            String gameName = request.getParameter(RegistrationValue.NAME_GAMENAME.toString());
            String email = request.getParameter(RegistrationValue.NAME_EMAIL.toString());
            String registrationToken = request.getParameter(RegistrationValue.NAME_REGISTRATIONTOKEN.toString());
            String gender = request.getParameter(RegistrationValue.NAME_GENDER.toString());
            String language = request.getParameter(RegistrationValue.NAME_LOCALE.toString());

            if (request.getParameter(RegistrationValue.NAME_SOURCE_URL.toString()) != null) {
                registrationUrl = request.getParameter(RegistrationValue.NAME_SOURCE_URL.toString());
            }


            String password1 = request.getParameter(RegistrationValue.NAME_PASS1.toString());
            String password2 = request.getParameter(RegistrationValue.NAME_PASS2.toString());

            String orgPassword = password1;

            RegisterResult rr = LoginService.checkUserForCreation(request.getRemoteAddr(), userName, gameName, email, password1, GameUtilities.getCurrentTick2(), request.getLocale(), true);

            User user = new User();


            String portalPassword = RandomStringUtils.randomAlphabetic(20);
            if (rr.isAbleToRegister()) {
                user.setUserName(userName);
                user.setGender(EUserGender.valueOf(gender));
                user.setPassword(MD5.encryptPassword(password1));
                user.setGameName(gameName);
                user.setLastUpdate(GameUtilities.getCurrentTick2());
                user.setIp(request.getRemoteAddr());
                user.setEmail(email.trim());
                user.setSystemAssigned(false);
                user.setActive(true);
                user.setAdmin(false);
                user.setTrial(true);
                user.setBetaAdmin(false);
                user.setLocked(0);
                user.setPortalPassword(portalPassword);
                user.setActivationCode("");
                user.setDeleteDate(0);
                user.setMulti(false);
                user.setRegistrationToken(registrationToken);
                user.setAcceptedUserAgreement(true);
                user.setRegistrationToken(registrationToken);
                Language l = Service.languageDAO.findById(Integer.parseInt(language));
                user.setLocale(l.getLanguage() + "_" + l.getCountry());
                user.setJoinDate(java.lang.System.currentTimeMillis());

                user = LoginService.saveUser(user);



                LoginTracker loginTracker = new LoginTracker();
                loginTracker.setUserId(user.getUserId());
                loginTracker.setTime(java.lang.System.currentTimeMillis());
                loginTracker.setLoginIP(request.getRemoteAddr());

                LoginService.saveLoginTracker(loginTracker);

                response.sendRedirect(GameConfig.getInstance().getHostURL() + "/enter.jsp?user=" + userName + "&password=" + orgPassword);
            } else {
                System.out.println("Error on login");
                String errors = "Error on login!";
                for (BaseResult br : rr.getResults()) {
                    System.out.println(br.getMessage());
                    errors += ";" + br.getMessage();

                }
                registrationUrl += "?errors=" + URLEncoder.encode(errors, "UTF-8");
                System.out.println("Trying to redirect to : " + registrationUrl);

                response.sendRedirect(registrationUrl);
            }

        } catch (Exception e) {
            th.rollback();
            System.out.println("error : " + e);
            e.printStackTrace();
        } finally {
            out.close();
            th.endTransaction();

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
