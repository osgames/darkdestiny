/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.servlets;

import at.viswars.DebugBuffer;
import com.keypoint.PngEncoder;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author Aion
 */
public class StatChartViewer extends HttpServlet {

    public void init() throws ServletException {
    }

    //Process the HTTP Get request
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // get the chart from session
        try {
            HttpSession session = request.getSession();
            JFreeChart chart = (JFreeChart) session.getAttribute("chart");
            BufferedImage chartImage = chart.createBufferedImage(500, 500, null);

            // set the content type so the browser can see this as a picture
            response.setContentType("image/png");

            // send the picture
            PngEncoder encoder = new PngEncoder(chartImage, false, 0, 9);
            response.getOutputStream().write(encoder.pngEncode());
            session.removeAttribute("chart");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Chart Viewer - Servlet", e);
        }

    }

    //Process the HTTP Post request
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    //Process the HTTP Put request
    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    //Clean up resources
    public void destroy() {
    }
}
