/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.servlets;

import at.viswars.Logger.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class XhabitForwarder extends HttpServlet {

    public static int[] buff = new int[1000000];
    public static String actCookie = null;

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Logger.getLogger().write("GOT CALLED");
        PrintWriter out = response.getWriter();

        // Cookies
        Cookie[] inCookies = request.getCookies();
        Cookie activeCookie = null;
        if (inCookies != null) {
            activeCookie = inCookies[0];
        }

        try {
            String forwardURL = null;

            if (request.getParameter("fParam") != null) {
                forwardURL = request.getParameter("fParam");
            }

            // Parameter processing 
            String urlParams = "";
            Enumeration params = request.getParameterNames();

            boolean firstParameter = true;
            // Url already contains parameters?
            if (forwardURL != null) {
                firstParameter = !(forwardURL.contains("?"));
            }

            while (params.hasMoreElements()) {
                String paraName = (String) params.nextElement();
                Logger.getLogger().write(paraName + "=" + request.getParameter(paraName));

                if (!paraName.equalsIgnoreCase("fParam")) {
                    if (firstParameter) {
                        urlParams += "?";
                        firstParameter = false;
                    } else {
                        urlParams += "&";
                    }

                    String pValue = request.getParameter(paraName);
                    pValue = pValue.replace(" ", "%20");

                    urlParams += paraName + "=" + pValue;
                }
            }

            // URL processing
            String urlStr = null;
            if (forwardURL == null) {
                urlStr = "http://univers.port-x.de/dd/login.jsp";
            } else if (forwardURL.startsWith("http://")) {
                urlStr = forwardURL + urlParams;
            } else {
                urlStr = "http://univers.port-x.de/dd/" + forwardURL + urlParams;
            }

            Logger.getLogger().write("Connecting to " + urlStr);

            URL url = new URL(urlStr);
            URLConnection uconn = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uconn;



            conn.setConnectTimeout(100000);
            conn.setReadTimeout(100000);
            conn.setInstanceFollowRedirects(true);

            if (actCookie != null) {
                // String cookie = activeCookie.getName() + "=" + activeCookie.getValue() + ";";
                Logger.getLogger().write("Sent cookie: " + actCookie);
                conn.setRequestProperty("Cookie", actCookie);
            }

            conn.connect();

            String cookies = conn.getHeaderField("Set-Cookie");

            Map<String, Map<String, String>> cookieMap = new HashMap<String, Map<String, String>>();
            if (cookies != null) {
                Logger.getLogger().write("Cookie = " + cookies);
                actCookie = cookies;

                StringTokenizer st = new StringTokenizer(cookies, ";");
                Map<String, String> cookie = new HashMap<String, String>();
                if (st.hasMoreTokens()) {
                    String token = st.nextToken();
                    String name = token.substring(0, token.indexOf("=")).trim();
                    String value = token.substring(token.indexOf("=") + 1, token.length()).trim();

                    Logger.getLogger().write(name + "=" + value);

                    cookie.put(name, value);
                    cookieMap.put(name, cookie);
                }
            }

            // String forwarderURL = "http://www.thedarkdestiny.at/dd/XhabitForwarder";
            String forwarderURL = "http://localhost:8084/DarkDestiny/XhabitForwarder";

            response.setContentType(conn.getContentType());
            response.setCharacterEncoding(conn.getContentEncoding());
            response.setHeader("Location", forwarderURL);
            // response.setHeader("Location", "http://localhost:8084/DarkDestiny/XhabitForwarder");
            response.setHeader("Set-Cookie", cookies);

            Logger.getLogger().write("TEST " + conn.getResponseCode());
            Logger.getLogger().write(conn.getContentType());
            // Object content = conn.getContent();
            InputStream is2 = conn.getInputStream();

            /*
            if (conn.getContentType().equalsIgnoreCase("image/png")) {
            URLImageSource uis = (URLImageSource)content;
            response.
            } else {
            is2 = (InputStream) content;
            }
             */
            // Logger.getLogger().write("TEST " + is2.available());

            int actByte = -1;

            int i = 0;
            while ((actByte = is2.read()) != -1) {
                // Logger.getLogger().write("Reading " + actByte);
                buff[i] = actByte;
                i++;
            }

            buff[i] = -1;


            char[] output = new char[i + 1];

            for (int j = 0; j < buff.length; j++) {
                if (buff[j] == -1) {
                    break;
                }

                output[j] = (char) buff[j];
            }

            String s = new String(output);
            s = s.replaceAll("action=\"", "action=\"" + forwarderURL + "?fParam=");
            s = s.replaceAll("href='", "href='" + forwarderURL + "?fParam=");
            s = s.replaceAll("href=\"", "href=\"" + forwarderURL + "?fParam=");
            s = s.replaceAll("src=\"GetPlanetPic", "src=\"" + forwarderURL + "?fParam=GetPlanetPic");
            s = s.replaceAll("value='main.jsp", "value='"+forwarderURL+"?fParam=main.jsp");
            s = s.replaceAll("href = 'main.jsp", "href = '"+forwarderURL+"?fParam=main.jsp");
            s = s.replaceAll("ajaxCall\\('http", "ajaxCall\\('"+forwarderURL+"?fParam=http");
            
            byte[] bytes = s.getBytes();

            out.write(s);

        /*
        for (int j = 0;j<bytes.length;j++) {
        out.w
        } 
         */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
