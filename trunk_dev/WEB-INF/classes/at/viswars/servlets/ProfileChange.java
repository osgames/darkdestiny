/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.servlets;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.UserDAO;
import at.viswars.model.User;
import at.viswars.result.ProfileResult;
import at.viswars.service.ProfileService;
import at.viswars.utilities.DestructionUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class ProfileChange extends HttpServlet {
    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();        

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            int userId = Integer.parseInt((String)session.getAttribute("userId"));
            ProfileResult pr = ProfileService.updateProfileData(userId, request.getParameterMap());
            
            User u = uDAO.findById(userId);
            if (u.getTrial() && u.getDeleteDate() > 0) {
                DestructionUtilities.destroyPlayer(userId);
            }
            // result.addAll(pr.getResult());
            // userData = ProfileService.findUserDataByUserId(userId);
            // user = ProfileService.findUserByUserId(userId);

            if (pr.isAutoLogOut()) {
                // if this player was a trial user, instantly destroy his account

                response.sendRedirect("logout.jsp");
            } else {
                response.sendRedirect("main.jsp?page=new/profile&subpage=2");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
