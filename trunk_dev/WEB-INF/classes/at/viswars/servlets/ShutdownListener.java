/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.servlets;

import at.viswars.CheckForUpdate;
import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.GameConfig;
import at.viswars.Logger.Logger;
import at.viswars.SetupProperties;
import at.viswars.Threading.ThreadController;
import at.viswars.ai.AIDebugBuffer;
import at.viswars.ai.AIDebugLevel;
import at.viswars.ai.AIThread;
import at.viswars.ai.AIThreadHandler;
import at.viswars.chat.ServerListener;
import at.viswars.enumeration.EUserType;
import at.viswars.model.User;
import at.viswars.notification.NotificationBuffer;
import at.viswars.service.Service;
import at.viswars.service.StartService;
import at.viswars.webservice.ServerInformation;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.ws.Endpoint;

/**
 *
 * @author Stefan
 */
public class ShutdownListener implements ServletContextListener {
    // private static FkTestDAO fkTestDAO = (FkTestDAO) DAOFactory.get(FkTestDAO.class);
    // private static FkChildTestDAO fkChildTestDAO = (FkChildTestDAO) DAOFactory.get(FkChildTestDAO.class);

    Endpoint serverInformationEndpoint;

    @Override
    public void contextInitialized(ServletContextEvent sce) {


        boolean updateTickOnStartup = false;
        Properties gameProperties = new Properties();

        try {
            gameProperties.load(GameConfig.class.getResourceAsStream("/game.properties"));
            updateTickOnStartup = Boolean.valueOf(gameProperties.getProperty(SetupProperties.GAME_UPDATETICKONSTARTUP));

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(GameConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (updateTickOnStartup) {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Update On Startup is true invoking StartService.updateTickToActualTime()");
            StartService.updateTickToActualTime();
        } else {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Update On Startup is false");
        }


        // FkTest fkTest = fkTestDAO.findById(1);
        // for (FkChildTest fkct : fkTest.getFkChildTest()) {
        //     System.out.println(fkct.getId() + " -- " + fkct.getRefId() + " -- " + fkct.getName());
        // }


        Logger.getLogger().write(Logger.LogLevel.INFO, "STARTUP: " + sce.getServletContext().getServletContextName());
        Logger.getLogger().write(Logger.LogLevel.INFO, "Try to initialize webservice!");
        try {


            String address = GameConfig.getInstance().getWebserviceURL();
            System.out.println("Init ServerInformation");
            ServerInformation si = new ServerInformation();
            System.out.println("Publishing ServerInformation on : " + address);
            try {
                serverInformationEndpoint = Endpoint.publish(address, si);
            } catch (IllegalArgumentException iae) {
                DebugBuffer.error("Cannot instantiate Endpoint at address '"+address+"': " + iae.getMessage());
            } catch (Exception e) {
                DebugBuffer.error("Cannot instantiate Endpoint at address '"+address+"': " + e.getMessage());
            }

            try {
                URL u = new URL(address);
                HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                huc.setRequestMethod("GET");  //OR  huc.setRequestMethod ("HEAD");
                huc.connect();
                int code = huc.getResponseCode();
                DebugBuffer.addLine(DebugLevel.DEBUG, "ServerInformation Service answered with Code : " + code);

            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, "ServerInformation Service answered with err : " + e.getMessage());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            AIDebugBuffer.add(AIDebugLevel.INFO, "Trying to start AI threads");
            /*
            for (User u : Service.userDAO.findAll()) {
                if (u.getUserType().equals(EUserType.AI)) {

                    AIThreadHandler.addThread(u.getUserId());
                }
            }
            */
        } catch (Exception e) {
            Logger.getLogger().write(Logger.LogLevel.ERROR, "ERROR STARTING AI THREADS " + e);
            e.printStackTrace();
        }
        // GameConfig.getInstance();
        // GameInit.initGame();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Logger.getLogger().write(Logger.LogLevel.INFO, "DESTROY: " + sce.getServletContext().getServletContextName());

        // Stop chat
        ServerListener.stop();
        ThreadController.stopAllThreads();


        try {
            serverInformationEndpoint.stop();
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error stopping IRC Endpoint . " + e.getMessage());
        }
        if (CheckForUpdate.getCheckForUpdateThread() != null) {
            CheckForUpdate.getCheckForUpdateThread().interrupt();
        }

        //Write notifications to User
        Logger.getLogger().write(Logger.LogLevel.INFO, "WRITING NOTIFICATIONS: " + sce.getServletContext().getServletContextName());
        try {
            NotificationBuffer.writeAll();
        } catch (Exception e) {
            Logger.getLogger().write(Logger.LogLevel.INFO, "ERROR WRITING NOTIFICATIONS " + e);
        }
        try {
            Logger.getLogger().write(Logger.LogLevel.INFO, "Trying to stop AI threads");
            for (AIThread thread : AIThreadHandler.getThreads()) {
                thread.setRun(false);
            }

            Logger.getLogger().write(Logger.LogLevel.INFO, "Threads stopped");
        } catch (Exception e) {
            Logger.getLogger().write(Logger.LogLevel.INFO, "ERROR Stopping AI THREADS " + e);

        }


        /*
         ArrayList<DebugLine> lines = DebugBuffer.readBuffer();
         Logger.getLogger().write(Logger.LogLevel.INFO, "Printing current state of debugBuffer");
         Logger.getLogger().write(Logger.LogLevel.INFO, "=====================================");
         for (DebugLine dl : lines) {
         Logger.getLogger().write(Logger.LogLevel.INFO, "<" + dl.getTimeStamp() + "> " + dl.getMessage());
         }

         Logger.getLogger().write(Logger.LogLevel.INFO, "=====================================");
         Logger.getLogger().write(Logger.LogLevel.INFO, "Gather all Database Locks");
         Logger.getLogger().write(Logger.LogLevel.INFO, "=====================================");
         for (Exception e : DAOFactory.gatherAllLocks()) {
         Logger.getLogger().write(Logger.LogLevel.INFO, "Lock data found");
         e.printStackTrace();
         }
         */

        /*
         Logger.getLogger().write(Logger.LogLevel.INFO, "=====================================");
         Logger.getLogger().write(Logger.LogLevel.INFO, "Printing current state of Threads");
         Logger.getLogger().write(Logger.LogLevel.INFO, "=====================================");
         ArrayList<ThreadEntry> teList = ThreadUtilities.getAllThreads();
         for (ThreadEntry te : teList) {
         Logger.getLogger().write(Logger.LogLevel.INFO, te.getThreadName() + " CPUTime > " + FormatUtilities.getFormattedDecimalCutEndZero(te.getCpuTime() / 1000000000d, 2) + " sek.");
         }
         */
    }
}
