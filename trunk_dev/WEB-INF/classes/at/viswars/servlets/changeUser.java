/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.servlets;

import at.viswars.DebugBuffer;
import at.viswars.Logger.Logger;
import at.viswars.model.PlayerPlanet;
import at.viswars.service.LoginService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class changeUser extends HttpServlet {
    /** 
    * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
    * @param request servlet request
    * @param response servlet response
    */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpSession session = request.getSession();

            Logger.getLogger().write("CHANGE="+request.getParameter("change"));

            session.setAttribute("userId", (String)request.getParameter("change"));
            PlayerPlanet pp = LoginService.findHomePlanet(Integer.parseInt((String) request.getParameter("change")));
            int systemId = LoginService.findPlanetById(pp.getPlanetId()).getSystemId();

            session.setAttribute("actPlanet", "" + pp.getPlanetId());
            session.setAttribute("actSystem", "" + systemId);   

            response.sendRedirect("main.jsp?page=new/overview");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on changeUser: ", e);
        } finally { 
            out.close();
        }        
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
    * Handles the HTTP <code>GET</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        // response.sendRedirect("http://www.google.de");
        processRequest(request, response);
    } 

    /** 
    * Handles the HTTP <code>POST</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
    * Returns a short description of the servlet.
    */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
