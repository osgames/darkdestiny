/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.notification;

import at.viswars.ML;
import at.viswars.enumeration.EMessageRefType;
import at.viswars.enumeration.EMessageType;
import at.viswars.model.TerritoryMap;
import at.viswars.model.TerritoryMapShare;
import at.viswars.service.Service;
import java.util.Locale;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class TerritoryNotification extends Notification {

    TerritoryMapShare tms;
    ETerritoryNotificationType type;

    public TerritoryNotification(int userId, TerritoryMapShare tms, ETerritoryNotificationType type) {
        super(userId);
        this.tms = tms;
        this.type = type;
    }

    public void generateMessage() {
        String msg = "";
        String topic = "";
        TerritoryMap tm = Service.territoryMapDAO.findById(tms.getTerritoryId());
        if (tm == null || Service.userDAO.findById(tm.getRefId()) == null) {
            return;
        }
        topic += ML.getMLStr("notification_territory_topic", userId).replace("%TERRITORYNAME%", tm.getName()).replace("%USERNAME%", Service.userDAO.findById(tm.getRefId()).getGameName());

        if (type.equals(ETerritoryNotificationType.DELETED)) {
            msg += ML.getMLStr("notification_territory_msg_delete", userId).replace("%TERRITORYNAME%", tm.getName()).replace("%USERNAME%", Service.userDAO.findById(tm.getRefId()).getGameName());

        } else {
            msg += ML.getMLStr("notification_territory_msg_added", userId).replace("%TERRITORYNAME%", tm.getName()).replace("%USERNAME%", Service.userDAO.findById(tm.getRefId()).getGameName());
        }
        gm.setTopic(topic);
        gm.setMsg(msg);
        if (type.equals(ETerritoryNotificationType.ADDED)) {
            this.gm.setReference(EMessageRefType.TERRITORY_ID_ADD, tms.getTerritoryId());
        }
        if (type.equals(ETerritoryNotificationType.DELETED)) {
            this.gm.setReference(EMessageRefType.TERRITORY_ID_DELETE, tms.getTerritoryId());
        }

        persistMessage();

    }

    public enum ETerritoryNotificationType {

        ADDED, DELETED
    }
}
