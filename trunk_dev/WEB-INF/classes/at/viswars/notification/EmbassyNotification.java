/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.notification;

import at.viswars.ML;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.UserDAO;
import at.viswars.utilities.DiplomacyUtilities;
import java.util.ArrayList;

/**
 *
 * @author Dreloc
 */
public class EmbassyNotification extends Notification {

    public static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private int remUserId;
    private ArrayList<Integer> remUsers;
    public EmbassyNotification(int userId, int remUserId, ArrayList<Integer> remUsers){
        super(userId);
        this.remUserId = remUserId;
        this.remUsers = remUsers;
            generateMessage();
            persistMessage();
    }

    public void generateMessage() {

        String msg = ML.getMLStr("notification_msg_embassy", userId);
        msg = msg.replace("%REMUSER%", uDAO.findById(remUserId).getGameName());
        String users = " ";
        boolean first = true;
        for(int i : remUsers){
            if(first){
                first = false;
            }else{
                users += ", ";
            }
            users += uDAO.findById(i).getGameName();
        }
        msg = msg.replace("%USERS%", users);

        gm.setMsg(msg);

        String topic = ML.getMLStr("notification_lbl_embassy", userId);
        gm.setTopic(topic);
    }

}
