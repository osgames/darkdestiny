/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.notification;

import at.viswars.GenerateMessage;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.MessageDAO;
import at.viswars.dao.UserDAO;
import at.viswars.enumeration.EMessageType;

/**
 *
 * @author Bullet
 */
public abstract class Notification implements INotification {
    public static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    public static MessageDAO messageDAO = (MessageDAO) DAOFactory.get(MessageDAO.class);
    
    protected int userId;
    protected GenerateMessage gm = new GenerateMessage();

    public Notification(int userId) {
        this.userId = userId;

        gm.setIgnoreSmiley(true);
        gm.setMessageType(EMessageType.SYSTEM);

        gm.setSourceUserId(uDAO.getSystemUser().getUserId());
        gm.setDestinationUserId(userId);
    }

    @Override
    public void persistMessage() {
        gm.writeMessageToUser();
    }
}
