/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.notification;

import at.viswars.ML;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.RessourceDAO;
import java.util.Locale;

/**
 *
 * @author Aion
 */
public class OutOfRessourceNotification extends Notification {

    int ressourceId;
    int planetId;

    public static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    public static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    public OutOfRessourceNotification(int ressourceId, int planetId, int userId) {

        super(userId);
        this.ressourceId = ressourceId;
        this.planetId = planetId;
            generateMessage();
            persistMessage();
    }

    @Override
    public void generateMessage() {

        String msg = ML.getMLStr("notification_outofressource_msg", userId);
        msg = msg.replace("%RESNAME%", ML.getMLStr(rDAO.findRessourceById(ressourceId).getName(), userId));
        msg = msg.replace("%PLANETID%", String.valueOf(planetId));
        msg = msg.replace("%PLANETNAME%", ppDAO.findByPlanetId(planetId).getName());

        gm.setMsg(msg);

        String topic = ML.getMLStr("notification_outofressource_topic", userId);
        topic = topic.replace("%RESNAME%", ML.getMLStr(rDAO.findRessourceById(ressourceId).getName(), userId));
        gm.setTopic(topic);
    }
}
