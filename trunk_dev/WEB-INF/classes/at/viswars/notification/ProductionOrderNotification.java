/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.notification;

import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.ML;
import at.viswars.dao.ConstructionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.GroundTroopDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EProductionOrderType;
import at.viswars.model.Construction;
import at.viswars.model.GroundTroop;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.ShipDesign;
import at.viswars.update.ProdOrderBufferEntry;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class ProductionOrderNotification extends Notification {

    public static PlayerPlanetDAO playerPlanetDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    public static ConstructionDAO constructionDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    public static ShipDesignDAO shipDesignDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    public static GroundTroopDAO groundTroopDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> planetEntries;

    public ProductionOrderNotification(TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> planetEntries, int userId) {
        super(userId);
        this.planetEntries = planetEntries;
        generateMessage();
        persistMessage();
    }

    /*
    public void persistMessage() {

        Message m = new Message();

        m.setArchiveBySource(this.getArchiveBySource());
        m.setArchiveByTarget(this.getArchiveByTarget());
        m.setDelBySource(this.getDelBySource());
        m.setDelByTarget(this.getDelByTarget());
        m.setType(this.getType());
        m.setOutMaster(this.getOutMaster());
        m.setSourceUserId(this.getSourceUserId());
        m.setTargetUserId(this.getTargetUserId());
        m.setTimeSent(this.getTimeSent());
        m.setViewed(this.getViewed());
        m.setTopic(this.getTopic());
        m.setText(this.getText());

        messageDAO.add(m);
    }
    */

    public void generateMessage() {

        String topic = ML.getMLStr("notification_msg_topic", userId);
        String msg = "";
        for (Map.Entry<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> entry : planetEntries.entrySet()) {
            int pId = entry.getKey();
            EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>> not = entry.getValue();
            msg += ML.getMLStr("notification_msg_finishedonplanet1", userId) + " ";
            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(pId);
            if (pp == null) {
                Logger.getLogger().write(LogLevel.ERROR, "PP was null where it should not have been [generateMessage/ProductionOrderNotification] on planet Id " + pId);
                continue;
            }

            msg += "<A href=\"selectview.jsp?showPlanet=" + pId + "\">";
            msg += pp.getName() + " ";
            msg += "</A>";
            msg += ML.getMLStr("notification_msg_finishedonplanet2", userId) + "<BR>";
            msg += addMessages(not, userId);
            msg = msg.substring(0, msg.length() - 4);
            msg += "_______________________" + "<BR>";
        }
        gm.setTopic(topic);
        gm.setMsg(msg);
    }

    public static String addMessages(EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>> finishedOrders, int userId) {

        String msg = "";

        ArrayList<ProdOrderBufferEntry> tmp = new ArrayList<ProdOrderBufferEntry>();
        // Constructions
        tmp = finishedOrders.get(EProductionOrderType.C_BUILD);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_con_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                Construction c = constructionDAO.findById(pboe.getAction().getConstructionId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += ML.getMLStr(c.getName(), userId);
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Scrap Construction
        tmp = finishedOrders.get(EProductionOrderType.C_SCRAP);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_scrap_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                Construction c = constructionDAO.findById(pboe.getAction().getConstructionId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += ML.getMLStr(c.getName(), userId);
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Produce Groundtroops
        tmp = finishedOrders.get(EProductionOrderType.GROUNDTROOPS);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_grnd_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                GroundTroop gt = groundTroopDAO.findById(pboe.getAction().getRefEntityId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += ML.getMLStr(gt.getName(), userId);
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Produce Order
        tmp = finishedOrders.get(EProductionOrderType.PRODUCE);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_prod_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                ShipDesign sd = shipDesignDAO.findById(pboe.getAction().getRefEntityId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += sd.getName();
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Repair Order
        tmp = finishedOrders.get(EProductionOrderType.REPAIR);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_repair_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                ShipDesign sd = shipDesignDAO.findById(pboe.getAction().getShipDesignId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += sd.getName();
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // SCRAP Order
        tmp = finishedOrders.get(EProductionOrderType.SCRAP);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_prodscrap_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                ShipDesign sd = shipDesignDAO.findById(pboe.getAction().getShipDesignId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += sd.getName();
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // IMPROVE Order
        tmp = finishedOrders.get(EProductionOrderType.C_IMPROVE);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_prodcimprove_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                Construction c = constructionDAO.findById(pboe.getAction().getConstructionId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += ML.getMLStr(c.getName(), userId);
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Upgrade Order
        tmp = finishedOrders.get(EProductionOrderType.UPGRADE);

        if (tmp != null) {
            Logger.getLogger().write("z1");
            msg += ML.getMLStr("notification_prod_upg_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
            Logger.getLogger().write("z2");
                if (pboe.getActionType() == EActionType.BUILDING) {
            Logger.getLogger().write("z3");
                    Construction c = constructionDAO.findById(pboe.getAction().getConstructionId());
                    msg += "- ";
                    msg += pboe.getAction().getNumber() + " ";
                    msg += ML.getMLStr(c.getName(), userId);
                    msg += "<BR>";
                } else if (pboe.getActionType() == EActionType.SHIP) {
            Logger.getLogger().write("z4");
                    ShipDesign sd = shipDesignDAO.findById(pboe.getProductionOrder().getToId());
                    msg += "- ";
                    msg += pboe.getAction().getNumber() + " ";
                    msg += sd.getName();
                    msg += "<BR>";
                } else if (pboe.getActionType() == EActionType.SPACESTATION) {
            Logger.getLogger().write("z5");
                    ShipDesign sd = shipDesignDAO.findById(pboe.getProductionOrder().getToId());
                    msg += "- ";
                    msg += pboe.getAction().getNumber() + " ";
                    msg += sd.getName();
                    msg += "<BR>";
                }
            }
            msg += "<BR>";
        }

        return msg;
    }
}
