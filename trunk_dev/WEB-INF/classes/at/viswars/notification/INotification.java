/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.notification;

import at.viswars.service.Service;
import at.viswars.update.ProdOrderBufferEntry;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public interface INotification{


    abstract void generateMessage();
    abstract void persistMessage();

}
