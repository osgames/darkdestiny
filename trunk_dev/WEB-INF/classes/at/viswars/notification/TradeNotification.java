/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.notification;

import at.viswars.FormatUtilities;
import at.viswars.ML;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.RessourceDAO;

/**
 *
 * @author Admin
 */
public class TradeNotification extends Notification {

    public static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    public static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    String msg = "";
    private int ressId;
    private int planetId;
    private long amount;
    private double price;

    public TradeNotification(int userId, int ressId, int planetId, long amount, double price) {
        super(userId);
        this.ressId = ressId;
        this.planetId = planetId;
        this.amount = amount;
        this.price = price;
        generateMessage();
        persistMessage();
    }

    public void generateMessage() {


        msg = ML.getMLStr("notification_trade_msg", userId).replace("%PLANET%", ppDAO.findByPlanetId(planetId).getName()).replace("%AMOUNT%", FormatUtilities.getFormattedNumber(amount)).replace("%RESSOURCE%", ML.getMLStr(rDAO.findRessourceById(ressId).getName(), userId));
        String topic = ML.getMLStr("notification_trade_topic", userId).replace("%AMOUNT%", FormatUtilities.getFormattedNumber(amount)).replace("%RESSOURCE%", ML.getMLStr(rDAO.findRessourceById(ressId).getName(), userId));

        gm.setTopic(topic);
        gm.setMsg(msg);
    }

    public String getMessage() {
        return msg;
    }
}
