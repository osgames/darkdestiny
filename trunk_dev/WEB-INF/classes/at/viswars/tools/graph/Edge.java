/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.tools.graph;

/**
 *
 * @author Stefan
 */
public class Edge {
    private final Node node1;
    private final Node node2;
    private double distance = 0d;
    
    public Edge(Node node1, Node node2) {
        this.node1 = node1;
        this.node2 = node2;
    }

    /**
     * @return the distance
     */
    public double getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }

    /**
     * @return the node1
     */
    public Node getNode1() {
        return node1;
    }

    /**
     * @return the node2
     */
    public Node getNode2() {
        return node2;
    }
}
