/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.tools.graph;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class Node {
    private final String name;
    private final Object load;
    private ArrayList<Edge> edges = new ArrayList<Edge>();
    
    public Node(String name) {
        this.name = name;
        this.load = null;
    }
    
    public Node(String name, Object load) {
        this.name = name;
        this.load = load;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the o
     */
    public Object getLoad() {
        return load;
    }

    /**
     * @return the edges
     */
    public ArrayList<Edge> getEdges() {
        return edges;
    }
    
    protected void addEdge(Edge e) {
        edges.add(e);
    }
}
