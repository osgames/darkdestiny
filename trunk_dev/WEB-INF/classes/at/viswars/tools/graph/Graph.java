/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.tools.graph;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class Graph {
    private ArrayList<Node> nodes = new ArrayList<Node>();
    private ArrayList<Edge> edges = new ArrayList<Edge>();
    
    public void addNode(Node n) {
        getNodes().add(n);
    }
    
    public void addEdge(Node n1, Node n2) {
        Edge newEdge = new Edge(n1,n2);
        n1.addEdge(newEdge);
        n2.addEdge(newEdge);
        getEdges().add(newEdge);
    }
    
    public void addEdge(Edge e) {       
        e.getNode1().addEdge(e);
        e.getNode2().addEdge(e);
        getEdges().add(e);
    }

    /**
     * @return the nodes
     */
    public ArrayList<Node> getNodes() {
        return nodes;
    }

    /**
     * @return the edges
     */
    public ArrayList<Edge> getEdges() {
        return edges;
    }
    
    public boolean reachable(Node nStart, Node nEnd) {
        return false;
    }   
    
    public ArrayList<Graph> getPartGraphs() {
        return new ArrayList<Graph>();
    }       
}
