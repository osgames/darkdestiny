/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.trade;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.TradeShipData;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.PlayerResearchDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.TradeOfferDAO;
import at.viswars.dao.TradePostDAO;
import at.viswars.dao.TradeRouteDetailDAO;
import at.viswars.dao.TradeRouteShipDAO;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.ETradeRouteType;
import at.viswars.model.Construction;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.ShipDesign;
import at.viswars.model.TradePost;
import at.viswars.model.TradeRoute;
import at.viswars.model.TradeRouteDetail;
import at.viswars.model.TradeRouteShip;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class TradeRouteExt implements Comparable {

    private final TradeRoute base;
    private static TradeOfferDAO toDAO = (TradeOfferDAO) DAOFactory.get(TradeOfferDAO.class);
    private static TradePostDAO tpDAO = (TradePostDAO) DAOFactory.get(TradePostDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    private static TradeRouteShipDAO trsDAO = (TradeRouteShipDAO) DAOFactory.get(TradeRouteShipDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);

    private ArrayList<TradeRouteDetail> routeDetails = null;
    private HashMap<ShipDesign, Integer> assignedShips = null;
    private int assignedTransmitterCap = 0;
    private boolean addingTransmitterPossible = false;
    private boolean removingTransmitterPossible = false;
    private boolean addingShipsPossible = false;
    private boolean removingShipsPossible = false;
    private int capacity = -1;
    private final double routeLength;
    private final RelativeCoordinate rc1;
    private final RelativeCoordinate rc2;
    boolean debug = false;

    public TradeRouteExt(TradeRoute tr) {
        this.base = tr;

        rc1 = new RelativeCoordinate(0, tr.getStartPlanet());
        rc2 = new RelativeCoordinate(0, tr.getTargetPlanet());

        routeLength = Math.max(1d, rc1.distanceTo(rc2));
    }

    public int getCapacity() {
        if (capacity == -1) {
            capacity = 0;

            if (base.getType().equals(ETradeRouteType.TRANSPORT)) {
                for (ShipDesign sd : getAssignedShips().keySet()) {
                    if (debug) {
                        Logger.getLogger().write("Check capacity for ship " + sd);
                        Logger.getLogger().write("Count " + assignedShips.get(sd));
                    }
                    double capTmp = 0d;

                    if (rc1.getSystemId() == rc2.getSystemId()) {
                        capTmp = (sd.getRessSpace() * assignedShips.get(sd)) / 2d;
                    } else {
                        capTmp = (sd.getRessSpace() * assignedShips.get(sd)) / 2d / routeLength * sd.getHyperSpeed();
                    }

                    capacity += capTmp;
                }

                capacity += getAssignedTransmitterCap();
            } else if (base.getType().equals((ETradeRouteType.TRADE))) {
                TradePost tp = tpDAO.findById(base.getTradePostId());
                if(tp == null){
                   return 0;
                }
                PlanetConstruction pc = pcDAO.findBy(tp.getPlanetId(), Construction.ID_INTERSTELLARTRADINGPOST);
                if(pc == null){
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Try to trade from tradepost which has no planetconstruction :(");
                    return 0;
                }

                int level = pc.getLevel();
                if(level <= 0){
                    level = 1;
                }

               
                double capBoost = tp.getCapBoost();

                capacity += ((double)tp.getTransportPerLJ() * (double)level * capBoost)  / 2d / routeLength;
               
            }
        }

        return capacity;
    }

    public ArrayList<TradeRouteDetail> getRouteDetails() {
        if (routeDetails == null) {
            routeDetails = trdDAO.getByRouteId(base.getId());
        }

        return routeDetails;
    }

    public void setRouteDetails(ArrayList<TradeRouteDetail> routeDetails) {
        this.routeDetails = routeDetails;
    }

    public HashMap<ShipDesign, Integer> getAssignedShips() {
        if (assignedShips == null) {
            assignedShips = new HashMap<ShipDesign, Integer>();

            ArrayList<TradeShipData> resList = TradeUtilities.getShipsInRoute(base.getId());
            for (TradeShipData sData : resList) {
                if (debug) {
                    Logger.getLogger().write("SD = " + sdDAO.findById(sData.getDesignId()));
                }

                ShipDesign sd = sdDAO.findById(sData.getDesignId());
                if (sd == null) {
                    continue;
                }
                assignedShips.put(sd, sData.getCount());
            }
        }
        if (debug) {
            Logger.getLogger().write("assignedShips Size is " + assignedShips.size());
        }
        return assignedShips;
    }

    public void setAssignedShips(HashMap<ShipDesign, Integer> assignedShips) {
        this.assignedShips = assignedShips;
    }

    public int getAssignedTransmitterCap() {
        TradeRouteShip trs = trsDAO.getTransmitterEntry(base.getId());

        if (trs == null) {
            assignedTransmitterCap = 0;
        } else {
            assignedTransmitterCap = trs.getNumber();
        }

        return assignedTransmitterCap;
    }

    public void setAssignedTransmitterCap(int assignedTransmitterCap) {
        this.assignedTransmitterCap = assignedTransmitterCap;
    }

    public boolean isAddingTransmitterPossible() {
        return addingTransmitterPossible;
    }

    public void setAddingTransmitterPossible(boolean addingTransmitterPossible) {
        this.addingTransmitterPossible = addingTransmitterPossible;
    }

    public boolean isRemovingTransmitterPossible() {
        return removingTransmitterPossible;
    }

    public void setRemovingTransmitterPossible(boolean removingTransmitterPossible) {
        this.removingTransmitterPossible = removingTransmitterPossible;
    }

    public boolean isAddingShipsPossible() {
        return addingShipsPossible;
    }

    public void setAddingShipsPossible(boolean addingShipsPossible) {
        this.addingShipsPossible = addingShipsPossible;
    }

    public boolean isRemovingShipsPossible() {
        return removingShipsPossible;
    }

    public void setRemovingShipsPossible(boolean removingShipsPossible) {
        this.removingShipsPossible = removingShipsPossible;
    }

    public TradeRoute getBase() {
        return base;
    }

    @Override
    public int compareTo(Object o) {
        TradeRouteExt tre = (TradeRouteExt) o;
        return this.getBase().getId().compareTo(tre.getBase().getId());
    }

    public double getRouteLength() {
        return routeLength;
    }
    
    public int getAssignedShipCount(){
         int shipCount = 0;
                                for(Map.Entry<ShipDesign, Integer> entry : getAssignedShips().entrySet()){
                                    shipCount += entry.getValue();
                                }
                                return shipCount;
    }
}
