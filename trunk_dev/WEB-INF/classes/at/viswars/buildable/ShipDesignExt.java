/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.buildable;

import at.viswars.Buildable;
import at.viswars.admin.module.AttributeTree;
import at.viswars.admin.module.ModuleAttributeResult;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DesignModuleDAO;
import at.viswars.dao.RessourceCostDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.enumeration.ERessourceCostType;
import at.viswars.model.Chassis;
import at.viswars.model.DesignModule;
import at.viswars.model.Model;
import at.viswars.model.RessourceCost;
import at.viswars.model.ShipDesign;
import at.viswars.ships.BuildTime;
import at.viswars.ships.ShipRepairCost;
import at.viswars.ships.ShipScrapCost;
import at.viswars.ships.ShipUpgradeCost;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ShipDesignExt implements Buildable {
    DesignModuleDAO dmDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);
    ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    
    private final ShipDesign ship;
    private final RessourcesEntry re;
    private final BuildTime bt;    
    private final Chassis c;

    public ShipDesignExt(int id) {
        ship = sdDAO.findById(id);   
        
        ArrayList<RessAmountEntry> raeList = new ArrayList<RessAmountEntry>();
        ArrayList<RessourceCost> rcList = rcDAO.find(ERessourceCostType.SHIP,id);
        
        for (RessourceCost rc : rcList) {
            RessAmountEntry rae = new RessAmountEntry(rc.getRessId(),rc.getQty());
            raeList.add(rae);
        }
        
        re = new RessourcesEntry(raeList);
        
        c = cDAO.findById(ship.getChassis());
        AttributeTree aTree = new AttributeTree(ship.getUserId(),0l);
        ModuleAttributeResult mar = aTree.getAllAttributes(c.getId(), c.getRelModuleId());
        
        bt = new BuildTime();
        
        int orbDockPoints = 0;
        int dockPoints = 0;
        int modPoints = 0;
        
        dockPoints = (int)mar.getAttributeValue(at.viswars.enumeration.EAttribute.DOCK_CONS_POINTS) * c.getMinBuildQty();
        orbDockPoints = (int)mar.getAttributeValue(at.viswars.enumeration.EAttribute.ORB_DOCK_CONS_POINTS) * c.getMinBuildQty();
        // bt.setDockPoints((int)mar.getAttributeValue(at.viswars.Attribute.DOCK_CONS_POINTS) * c.getMinBuildQty());
        // bt.setOrbDockPoints((int)mar.getAttributeValue(at.viswars.Attribute.ORB_DOCK_CONS_POINTS) * c.getMinBuildQty());                
        
        for (DesignModule dm : dmDAO.findByDesignId(ship.getId())) {
            mar = aTree.getAllAttributes(c.getId(), dm.getModuleId());
            modPoints += (int)mar.getAttributeValue(at.viswars.enumeration.EAttribute.MOD_CONS_POINTS) * c.getMinBuildQty();
            dockPoints += (int)mar.getAttributeValue(at.viswars.enumeration.EAttribute.DOCK_CONS_POINTS) * c.getMinBuildQty();
            orbDockPoints += (int)mar.getAttributeValue(at.viswars.enumeration.EAttribute.ORB_DOCK_CONS_POINTS) * c.getMinBuildQty();
        }
                
        bt.setModulePoints(modPoints);
        bt.setDockPoints(dockPoints);
        bt.setOrbDockPoints(orbDockPoints);
    }

    @Override
    public RessourcesEntry getRessCost() {        
        return re;
    }
    
    @Override
    public Model getBase() {
        return ship;
    }

    @Override
    public BuildTime getBuildTime() {
        return bt;
    }

    public ShipScrapCost getShipScrapCost() {
        return new ShipScrapCost(this);
    }    
    
    public ShipScrapCost getShipScrapCost(int count) {
        return new ShipScrapCost(this, count);
    }
    
    public ShipUpgradeCost getShipUpgradeCost(ShipDesignExt sdeTo) {
        return new ShipUpgradeCost(this,sdeTo);
    }    
    
    public ShipUpgradeCost getShipUpgradeCost(ShipDesignExt sdeTo, int count) {
        return new ShipUpgradeCost(this, sdeTo, count);
    }

    public ShipRepairCost getShipRepairCost() {
        return new ShipRepairCost(this);
    }
    
    public Chassis getChassis() {
        return c;
    }
}
