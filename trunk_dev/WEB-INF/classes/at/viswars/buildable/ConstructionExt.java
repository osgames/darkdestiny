/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.buildable;

import at.viswars.Buildable;
import at.viswars.construction.ConstructionAbortRefund;
import at.viswars.construction.ConstructionScrapAbortCost;
import at.viswars.construction.ConstructionScrapCost;
import at.viswars.dao.ConstructionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.RessourceCostDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.enumeration.ERessourceCostType;
import at.viswars.model.Construction;
import at.viswars.model.Model;
import at.viswars.model.RessourceCost;
import at.viswars.ships.BuildTime;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ConstructionExt implements Buildable {
    ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    
    private final Construction cons;
    private final RessourcesEntry re;

    public ConstructionExt(int id) {
        cons = cDAO.findById(id);   
        
        ArrayList<RessAmountEntry> raeList = new ArrayList<RessAmountEntry>();
        ArrayList<RessourceCost> rcList = rcDAO.find(ERessourceCostType.CONSTRUCTION,id);
        
        for (RessourceCost rc : rcList) {
            RessAmountEntry rae = new RessAmountEntry(rc.getRessId(),rc.getQty());
            raeList.add(rae);
        }
        
        re = new RessourcesEntry(raeList);
    }

    public RessourcesEntry getRessCost() {        
        return re;
    }
    
    public Model getBase() {
        return cons;
    }

    //@TODO
    @Override
    public BuildTime getBuildTime() {
        return null;
    }

    public ConstructionAbortRefund getAbortRefund(int orderId) {
        return new ConstructionAbortRefund(this,orderId);
    }
    
    public ConstructionScrapAbortCost getScrapAbortCost(int orderId) {
        return new ConstructionScrapAbortCost(this,orderId);
    }    

    public ConstructionScrapCost getScrapCost(int planetId) {
        return new ConstructionScrapCost(this,planetId);
    }
    
    /*
    public ConstructionScrapAbortCost getScrapAbortCost(int orderId) {
        
    }    
    */
} 
