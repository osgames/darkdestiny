/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.buildable;

import at.viswars.Buildable;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.GroundTroopDAO;
import at.viswars.dao.RessourceCostDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.enumeration.ERessourceCostType;
import at.viswars.model.GroundTroop;
import at.viswars.model.Model;
import at.viswars.model.RessourceCost;
import at.viswars.ships.BuildTime;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class GroundTroopExt implements Buildable {
    GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    
    private final GroundTroop troop;
    private final RessourcesEntry re;
    private final BuildTime bt;

    public GroundTroopExt(int id) {
        troop = gtDAO.findById(id);   
        
        ArrayList<RessAmountEntry> raeList = new ArrayList<RessAmountEntry>();
        ArrayList<RessourceCost> rcList = rcDAO.find(ERessourceCostType.GROUNDTROOPS,id);
        
        for (RessourceCost rc : rcList) {
            RessAmountEntry rae = new RessAmountEntry(rc.getRessId(),rc.getQty());
            raeList.add(rae);
        }
        
        re = new RessourcesEntry(raeList);
        
        bt = new BuildTime();
        bt.setModulePoints(troop.getModulPoints());
        bt.setKasPoints(troop.getCrew());
    }

    @Override
    public RessourcesEntry getRessCost() {        
        return re;
    }
    
    @Override
    public Model getBase() {
        return troop;
    }
    
    @Override
    public BuildTime getBuildTime() {
        return bt;
    }    
}
