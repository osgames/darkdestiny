/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars;

import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.model.Quiz;
import at.viswars.model.QuizAnswer;
import at.viswars.model.QuizEntry;
import at.viswars.model.QuizQuestion;
import at.viswars.service.Service;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class QuizLoader {

    public static void main(String args[]) {



        File file = new File("quizfragen.txt");
        System.out.println("F : " + file.getAbsolutePath());
        StringBuffer contents = new StringBuffer();
        BufferedReader reader = null;

        TransactionHandler th = TransactionHandler.getTransactionHandler();
        try {
            th.startTransaction();

		reader = new BufferedReader(
		   new InputStreamReader(
                      new FileInputStream(file), "ISO-8859-1"));
            String text = null;

            Quiz q = new Quiz();
            q.setName("Perry Rhoden Quizz");
            q.setTopic("Neue Fragen? Einfach hier hinzufügen https://docs.google.com/document/d/1v18sSA5MLkBU2zHwjWfEgZOdGedG6lXwcaKMUc4qaw0/edit");

            q = Service.quizDAO.add(q);

            // repeat until all lines is read
            String answer = null;
            String question = null;
            ArrayList<QuizAnswer> qas = new ArrayList<QuizAnswer>();
            ArrayList<QuizQuestion> qqs = new ArrayList<QuizQuestion>();
            
            boolean newEntry = false;
            while ((text = reader.readLine()) != null) {
                if (!text.equals("")) {
                    if (question == null) {
                        question = text.trim();
                    } else if (answer == null) {
                        answer = text.trim();
                     // System.out.println(question + " => " + answer);

                        QuizEntry qe = new QuizEntry();
                        qe.setQuizId(q.getId());
                        qe.setDifficulty(5);
                        qe.setAuthor("Kazzenkatt");

                        qe = Service.quizEntryDAO.add(qe);

                        QuizQuestion qq = new QuizQuestion();
                        qq.setQuizEntryId(qe.getId());
                        qq.setQuestion(question);

                        qq = Service.quizQuestionDAO.add(qq);

                        qqs.add(qq);

                        QuizAnswer qa = new QuizAnswer();
                        qa.setQuizEntryId(qe.getId());
                        qa.setAnswer(answer);
                        
                        qa = Service.quizAnswerDAO.add(qa);
                        qas.add(qa);
                        answer = null;
                        question = null;
                    }
                }
            }
            Service.quizQuestionDAO.insertAll(qqs);
            Service.quizAnswerDAO.insertAll(qas);
            th.execute();
        } catch (Exception e) {
            e.printStackTrace();
            try {
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } finally {
            th.endTransaction();
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
