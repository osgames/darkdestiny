/*
 * BetaTools.java
 *
 * Created on 13. Juni 2004, 14:58
 */

package at.viswars;

import at.viswars.ships.ShipData;
import at.viswars.Logger.Logger;
import at.viswars.database.access.DbConnect;
import at.viswars.model.ShipDesign;
import at.viswars.result.TestCombatResult;
import java.util.*;

import at.viswars.service.Service;
import at.viswars.spacecombat.CombatHandler_NEW;
import at.viswars.spacecombat.CombatSimulationSet;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author  Stefan
 */
public class BetaTools {

    public static ArrayList<ShipData> getShipDesignsForUser(int userId){

        ArrayList<ShipData> designs = new ArrayList<ShipData>();
        HashMap<String, ShipData> sortedDesigns = new HashMap<String, ShipData>();
        ArrayList<ShipDesign> sds =  Service.shipDesignDAO.findByUserId(userId);
          for(ShipDesign sd : sds){
                ShipData sdata = new ShipData();

                sdata.setDesignId(sd.getId());
                sdata.setDesignName(sd.getName() + " [" + sd.getType().toString() + "][" + ML.getMLStr(Service.chassisDAO.findById(sd.getChassis()).getName(), userId) + "]");

                sortedDesigns.put(sd.getName(), sdata);
            }
          designs.addAll(sortedDesigns.values());

        return designs;
    }

    public static ArrayList<ShipData> getAllShipDesigns() {
        ArrayList<ShipData> designs = new ArrayList<ShipData>();
        
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id, name FROM shipdesigns");
            
            while (rs.next()) {
                ShipData sd = new ShipData();
                
                sd.setDesignId(rs.getInt(1));
                sd.setDesignName(rs.getString(2));
                
                designs.add(sd);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Beta-Tools - getAllShipDesigns: ",e);
        }
        
        return designs;
    }
    
    public static ArrayList<ShipData> getAllTroopTypes() {
        ArrayList<ShipData> troops = new ArrayList<ShipData>();
        
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id, name FROM groundtroops");
            
            while (rs.next()) {
                ShipData sd = new ShipData();
                
                sd.setDesignId(rs.getInt(1));
                sd.setDesignName(rs.getString(2));
                
                troops.add(sd);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Beta-Tools - getAllShipDesigns: ", e);
        }
        
        return troops;
    }
    
    public static ArrayList<ShipData> getAllPDTypes() {
        ArrayList<ShipData> planetDef = new ArrayList<ShipData>();
        
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id, name FROM construction WHERE designId<>0");
            
            while (rs.next()) {
                ShipData sd = new ShipData();
                
                sd.setDesignId(rs.getInt(1));
                sd.setDesignName(rs.getString(2));
                
                planetDef.add(sd);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Beta-Tools - getAllShipDesigns: ", e);
        }
        
        return planetDef;
    }
    
    public static TestCombatResult generateUnits(Map<String, Object> allPars) {
        TestCombatResult result = null;
        CombatSimulationSet css = null;        
        
        HashMap<Integer,Integer> attackerShips = new HashMap<Integer,Integer>();
        HashMap<Integer,Integer> defenderShips = new HashMap<Integer,Integer>();
        
        try {
            for(String parName : allPars.keySet()) {

                Logger.getLogger().write("Checking parameter " + parName);
                
                if (parName.startsWith("attacker")) {                    
                    Logger.getLogger().write("Attacker found");
                    
                    String[] valueStr = (String[])allPars.get(parName);
                    String desc = valueStr[0];                                        
                    
                    Logger.getLogger().write("DESC="+desc);
                    if (desc.equalsIgnoreCase("0")) continue;
                    
                    StringTokenizer tok = new StringTokenizer(desc,"_");
                    
                    Logger.getLogger().write("TokenCount="+tok.countTokens());
                    
                    int designId = Integer.parseInt(tok.nextToken());
                    int position = Integer.parseInt(tok.nextToken());

                    Logger.getLogger().write("DesignId="+designId+" position="+position);
                    
                    int value = Integer.parseInt(((String[])allPars.get("att" + position + "Count"))[0]);
                    
                    Logger.getLogger().write("Value="+value);
                    
                    
                    if (value != 0) {        
                        attackerShips.put(designId,value);
                        // stmt.execute("INSERT shipfleet SET fleetId=99997, designId="+designId+", count="+value);                        
                    }                    
                } else if (parName.startsWith("defender")) {
                    Logger.getLogger().write("defender found");
                    
                    String[] valueStr = (String[])allPars.get(parName);
                    String desc = valueStr[0];
                    
                    Logger.getLogger().write("DESC="+desc);
                    if (desc.equalsIgnoreCase("0")) continue;
                    
                    StringTokenizer tok = new StringTokenizer(desc,"_");
                    
                    Logger.getLogger().write("TokenCount="+tok.countTokens());
                    
                    int designId = Integer.parseInt(tok.nextToken());
                    int position = Integer.parseInt(tok.nextToken());

                    Logger.getLogger().write("DesignId="+designId+" position="+position);
                    
                    int value = Integer.parseInt(((String[])allPars.get("def" + position + "Count"))[0]);
                    
                    Logger.getLogger().write("Value="+value);
                    
                    if (value != 0) {
                        defenderShips.put(designId,value);
                        // stmt.execute("INSERT shipfleet SET fleetId=99998, designId="+designId+", count="+value);                         
                    }       
                }
            }
            int attackerId = Integer.parseInt(((String[])allPars.get("attId"))[0]);
            int defenderId = Integer.parseInt(((String[])allPars.get("defId"))[0]);
            css = new CombatSimulationSet(attackerId,defenderId,attackerShips,defenderShips);
            // CombatHandler ch = new CombatHandler(9998,99997,74);
            try {
                CombatHandler_NEW ch = new CombatHandler_NEW(css);
                ch.executeBattle();
                css.getCombatReportGenerator().generateReports();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Battle failed due to error: ",e);
            }
            css.cleanUp();
             
            return new TestCombatResult(css.getCombatReportGenerator().getUserReports(),css.getAttackerCost(),css.getDefenderCost());            
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Combatsimulator: ",e);
            return new TestCombatResult(e.getMessage(),true);          
        }                
    }
}
