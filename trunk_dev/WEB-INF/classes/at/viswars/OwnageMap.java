/*
 * OwnageMap.java
 *
 * Created on 06. April 2008, 16:22
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars;

import at.viswars.database.access.DbConnect;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.model.PlayerPlanet;
import at.viswars.service.Service;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import at.viswars.model.System;

/**
 *
 * @author Stefan
 */
public class OwnageMap extends Service{
    // 0 influence / 1 user
    private short[][][] totalMap = new short[200][200][2];
    private short[][] tmpMap = new short[200][200];
    private short userId = 0;
    private ArrayList<AbsoluteCoordinate> systemList = new ArrayList<AbsoluteCoordinate>();
    private HashMap<Short,Color> colorMap = new HashMap<Short,Color>();
    
    /** Creates a new instance of OwnageMap */
    public OwnageMap() {
        try {
            colorMap.put((short)-1,Color.GRAY);

            ArrayList<System> allSys = systemDAO.findAll();
            for (System se : allSys) {
                totalMap[(short)Math.ceil(se.getX()/10d)-1][(short)Math.ceil(se.getY()/10d)-1][1] = -1;
            }
            
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id FROM user");
            while (rs.next()) {
                // Logger.getLogger().write("Processing player " + rs.getInt(1));
                tmpMap = new short[200][200];
                
                systemList.clear();
                userId = rs.getShort(1);
                
                int r = (int)Math.floor(Math.random() * 256);
                int g = (int)Math.floor(Math.random() * 256);
                int b = (int)Math.floor(Math.random() * 256);
                
                colorMap.put(userId,new Color(r,g,b));
                
                // Load all planets
                Statement stmt2 = DbConnect.createStatement();
                ResultSet rs2 = stmt2.executeQuery("SELECT planetID FROM playerplanet WHERE userID="+userId);
                while (rs2.next()) {
                    // Logger.getLogger().write("Processing planet " + rs2.getInt(1));
                    
                    RelativeCoordinate rc = new RelativeCoordinate(0,rs2.getInt(1));
                    AbsoluteCoordinate acTmp = rc.toAbsoluteCoordinate();
                    
                    AbsoluteCoordinate ac = new AbsoluteCoordinate((int)Math.ceil(acTmp.getX()/10d)-1,(int)Math.ceil(acTmp.getY()/10d)-1);
                    
                    PlayerPlanet pd = playerPlanetDAO.findByPlanetId(rs2.getInt(1));
                    if (pd.getPopulation() < (long)5000000) {
                       writeToTmp(ac.getX(), ac.getY(),20); 
                    } else if (pd.getPopulation() < (long)50000000) {
                       writeToTmp(ac.getX(), ac.getY(),40); 
                    } else if (pd.getPopulation() < (long)500000000) {
                       writeToTmp(ac.getX(), ac.getY(),60);  
                    } else if (pd.getPopulation() < (long)2000000000) {
                       writeToTmp(ac.getX(), ac.getY(),80);  
                    } else if (pd.getPopulation() < (long)(5 * 1000000000)) {
                       writeToTmp(ac.getX(), ac.getY(),100);  
                    } else if (pd.getPopulation() >= (long)(5 * 1000000000)) {
                       writeToTmp(ac.getX(), ac.getY(),120);  
                    }
                                        
                    systemList.add(ac);
                }
                
                // fillTmpMap();
                mergeMap();
                
                stmt2.close();
            }
            
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.error("Error while creating Map: " + e);
        }
    }
    
    // FIXME Unused!
    private void fillTmpMap() {
        tmpMap = new short[200][200];
        
        /*for (AbsoluteCoordinate ac : systemList) {
            // writeToTmp(ac.getX(), ac.getY());
        }*/
    }
    
    private void mergeMap() {
        for (AbsoluteCoordinate ac : systemList) {
            writePattern(ac.getX(), ac.getY());
        }
    }
    
    private void writeToTmp(int x_in, int y_in, int influence) {        
        int span = (int)Math.floor(influence / 20d);
        
        for (int x=x_in-span;x<x_in+span;x++) {
            for (int y=y_in-span;y<y_in+span;y++) {
                if ((x >= 0) && (y >= 0) && (x < 200) && (y < 200)) {
                    // int actInfluence = (int)Math.floor(influence - Math.abs(x - x_in) * 20d - Math.abs(y - y_in) * 20d);
                    
                    
                    int actInfluence = (int)Math.floor(influence - Math.sqrt(Math.pow(x - x_in,2d) + Math.pow(y - y_in,2d)) * 20d);                    
                    if (actInfluence <= 0) continue;
                    
                    if (actInfluence > tmpMap[x][y]) {
                        // Logger.getLogger().write("Write influence of " + actInfluence + " on " + x + "/" + y);
                        tmpMap[x][y] = (short)actInfluence;
                    }
                }
            }
        }
    }
    
    private void writePattern(int x_in, int y_in) {
        for (int x=x_in-6;x<x_in+6;x++) {
            for (int y=y_in-6;y<y_in+6;y++) {
                if ((x >= 0) && (y >= 0) && (x < 200) && (y < 200)) {
                    short actInfluence = tmpMap[x][y];
                    
                    // Logger.getLogger().write("actInfluence of "+x+"/"+y+" = " + actInfluence);
                    if (actInfluence <= 0) continue;
                    
                    // Logger.getLogger().write("TEST3");
                    if (actInfluence > totalMap[x][y][0]) {
                        // Logger.getLogger().write("TEST4");
                        // Logger.getLogger().write("Copy influence of " + actInfluence + " on " + x + "/" + y + " for player " + userId);
                        totalMap[x][y][0] = actInfluence;
                        totalMap[x][y][1] = userId;
                    }
                }
            }
        }
    }
    
    public String buildMap() {
        StringBuffer mapHTML = new StringBuffer();
        mapHTML.append("<TABLE width=\"800\" height=\"400\" style=\"table-layout:fixed\" bgcolor=\"#000000\" cellspacing=0 border=0 cellpadding=0>");
        for (int x=0;x<200;x++) {
            mapHTML.append("<TR style=\"height:2px; overflow:hidden; max-height:2px;\">");
            for (int y=0;y<200;y++) {
                if (totalMap[x][y][1] == 0) {
                    mapHTML.append("<TD style=\"font-size:2px; width:4px; height:2px; overflow:hidden; max-height:2px;\">&nbsp</TD>");
                } else {                    
                    mapHTML.append("<TD bgcolor=\"#"+Integer.toHexString( colorMap.get(totalMap[x][y][1]).getRGB() & 0x00ffffff)+"\" style=\"font-size:2px; width:4px; height:2px; overflow:hidden; max-height:2px;\">&nbsp</TD>");
                }
            }
            mapHTML.append("</TR>");
        }
        mapHTML.append("</TABLE>");
        
        return mapHTML.toString();
    }   
}
