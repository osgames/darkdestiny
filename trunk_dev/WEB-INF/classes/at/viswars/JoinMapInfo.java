/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars;

import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.model.Galaxy;
import at.viswars.service.Service;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Planet;
import at.viswars.model.Statistic;
import at.viswars.scanner.StarMapQuadTree;
import at.viswars.scanner.SystemDesc;
import at.viswars.service.LoginService;
import at.viswars.xml.XMLMemo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 *
 * @author HorstRabe
 */
public class JoinMapInfo extends Service {

    public static final int FLEET_ENEMY = 3;
    public static final int FLEET_OWN = 1;
    public static final int FLEET_ALLIED = 2;
    public static final int HyperScannerId = 42;
    public static final int FLEET_MAXIMUM_VISIBLE_RANGE = 300;
    public static final int HYPERSCANNER_RANGE = 200;
    private Locale l;

    public JoinMapInfo(Locale l) {
        this.l = l;
    }

    public XMLMemo addSystems(XMLMemo starMap) {
        int tileSize = 100;
        int factor = (int) Math.round(GameConstants.UNIVERSE_HEIGHT / tileSize);
        long then = System.currentTimeMillis();
        DebugBuffer.addLine(DebugLevel.DEBUG, "Quadtree initialize!");
        StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);

        DebugBuffer.addLine(DebugLevel.DEBUG, "Quadtree initialized! MS " + (System.currentTimeMillis() - then));

        HashMap<Integer, HashMap<Integer, String>> landMapping = new HashMap<Integer, HashMap<Integer, String>>();



        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Loading Land mapping!");

        for (Planet p : LoginService.findAllPlanets()) {
            if (landMapping.containsKey(p.getSystemId())) {
                landMapping.get(p.getSystemId()).put(p.getId(), p.getLandType());
            } else {
                HashMap<Integer, String> planetMapping = new HashMap<Integer, String>();
                planetMapping.put(p.getId(), p.getLandType());
                landMapping.put(p.getSystemId(), planetMapping);
            }
        }

        DebugBuffer.addLine(DebugLevel.UNKNOWN, landMapping.size() + " mappings created");

        HashMap<Integer, Integer> pointsMap = new HashMap<Integer, Integer>();

        for (Statistic s : LoginService.findAllStatistics()) {
            pointsMap.put(s.getUserId(), s.getPoints());
        }



        XMLMemo constants = new XMLMemo("Constants");

        XMLMemo language = new XMLMemo("Language");
        language.addAttribute("Locale", l.toString());

        constants.addChild(language);
        XMLMemo uniConstants = new XMLMemo("UniverseConstants");
        uniConstants.addAttribute("height", GameConstants.UNIVERSE_HEIGHT);
        uniConstants.addAttribute("width", GameConstants.UNIVERSE_WIDTH);
        constants.addChild(uniConstants);

        starMap.addChild(constants);




        XMLMemo galaxys = new XMLMemo("Galaxys");
        for (Galaxy g : (ArrayList<Galaxy>) Service.galaxyDAO.findAll()) {
            XMLMemo galaxy = new XMLMemo("Galaxy");
            galaxy.addAttribute("id", g.getId());
            galaxy.addAttribute("height", g.getHeight());
            galaxy.addAttribute("width", g.getWidth());
            galaxy.addAttribute("originX", g.getOriginX());
            galaxy.addAttribute("originY", g.getOriginY());
            galaxy.addAttribute("playerStart", g.getPlayerStart().toString());
            galaxy.addAttribute("startSystem", g.getStartSystem());
            galaxy.addAttribute("endSystem", g.getEndSystem());
            galaxys.addChild(galaxy);
        }
        starMap.addChild(galaxys);

        XMLMemo tiles = new XMLMemo("Tiles");
        for (Galaxy g : (ArrayList<Galaxy>) Service.galaxyDAO.findAll()) {
            addGalaxyTiles(tiles, g);
        }

        starMap.addChild(tiles);
        XMLMemo mainTag = new XMLMemo("Systems");
        starMap.addChild(mainTag);
        try {
            for (at.viswars.model.System s : LoginService.findAllSystemsOrdered()) {

                if (!landMapping.containsKey(s.getId())) {
                    continue;
                }

                int points = 0;
                long population = 0;
                boolean occupied = false;
                boolean habitable = false;

                HashMap<Integer, String> planetMapping = landMapping.get(s.getId());

                for (Integer planetId : planetMapping.keySet()) {
                    PlayerPlanet pp = LoginService.findPlayerPlanetByPlanetId(planetId);

                    if (pp != null) {
                        population += pp.getPopulation();
                        int userId = pp.getUserId();

                        if (pointsMap.containsKey(userId)) {
                            if (points < pointsMap.get(userId)) {
                                points = pointsMap.get(userId);
                            }
                        }

                        occupied = true;
                    }
                }

                if (occupied) {
                    SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), population, points);
                    // if (rs.getInt(1) == 2405) DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding system 2405 with population: " + (int)Math.log10(population) + " points: " + points + " at ("+rs.getInt(3)+","+rs.getInt(4)+")");
                    quadtree.addItemToTree(sd);
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while adding systems to QuadTree: ", e);
        }




        try {
            for (at.viswars.model.System s : LoginService.findAllSystemsOrdered()) {
                if (!landMapping.containsKey(s.getId())) {
                    XMLMemo actSystem = new XMLMemo("System");
                    actSystem.addAttribute("id", s.getId());
                    actSystem.addAttribute("name", "System #" + s.getId());
                    actSystem.addAttribute("x", s.getX());
                    actSystem.addAttribute("y", s.getY());
                    actSystem.addAttribute("sysstatus", -1);
                    mainTag.addChild(actSystem);
                    continue;
                }

                XMLMemo actSystem = new XMLMemo("System");
                actSystem.addAttribute("id", s.getId());

                String sysName = "System #" + s.getId();

                actSystem.addAttribute("name", sysName);
                actSystem.addAttribute("x", s.getX());
                actSystem.addAttribute("y", s.getY());

                boolean occupied = false;
                boolean habitable = false;
                boolean startable = true;
                
                if(!s.getGalaxy().getPlayerStart()){
                    startable = false;
                }

                int systemStatus = -1;

                HashMap<Integer, String> planetMapping = landMapping.get(s.getId());

                int points = 0;
                long population = 0;

                for (Integer planetId : planetMapping.keySet()) {
                    PlayerPlanet pp = LoginService.findPlayerPlanetByPlanetId(planetId);


                    if (pp != null) {
                        occupied = true;

                        population += pp.getPopulation();
                        int userId = pp.getUserId();

                        if (pointsMap.containsKey(userId)) {
                            if (points < pointsMap.get(userId)) {
                                points = pointsMap.get(userId);
                            }
                        }
                    }
                    if (planetMapping.get(planetId).equalsIgnoreCase(Planet.LANDTYPE_M)) {
                        habitable = true;
                    }
                }

                if (occupied || !startable) {
                    systemStatus = 4;
                } else if (habitable) {
                    ArrayList<SystemDesc> systems = quadtree.getItemsAround(s.getX(), s.getY(), 200);

                    // DebugBuffer.addLine(DebugLevel.UNKNOWN, "CHECKING SYS: " + rs.getInt(1) + " Found neighbours: " + systems.size() + " at ("+rs.getInt(3)+","+rs.getInt(4)+")");

                    long popValue = 0;
                    int popScore = 0;
                    int userScore = 0;

                    for (SystemDesc sd : systems) {
                        popValue += sd.popscore;
                        /*
                        if (popScore < sd.popscore) {
                            popScore = sd.popscore;
                        }
                        */
                        if (userScore < sd.statscore) {
                            userScore = sd.statscore;
                        }
                    }

                    systemStatus = 1;

                    XMLMemo actScore = new XMLMemo("Score");

                    actScore.addAttribute("popPoints", popScore);
                    actScore.addAttribute("statPoints", userScore);

                    actSystem.addChild(actScore);
                }

                if ((systemStatus != -1) && (systemStatus != 4)) {
                    actSystem.addAttribute("sysstatus", systemStatus);
                }
                mainTag.addChild(actSystem);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in creating of JoinMap: ", e);
        }

        return starMap;
    }

    public XMLMemo addGalaxyTiles(XMLMemo tiles, Galaxy g) {
        int tileSize = 100;
        int factor = 6;
        long then = System.currentTimeMillis();

        int xMin = (int) Math.round(g.getOriginX() - g.getWidth() / 2d);
        int yMin = (int) Math.round(g.getOriginY() - g.getHeight() / 2d);
        int xMax = (int) Math.round(g.getOriginX() + g.getWidth() / 2d);
        int yMax = (int) Math.round(g.getOriginY() + g.getHeight() / 2d);

        DebugBuffer.addLine(DebugLevel.DEBUG, "Quadtree initialize with factor : " + factor + " x : " + xMin + " y : " + yMin + " height : " + xMax + " width: " + yMax);
        StarMapQuadTree quadtree = new StarMapQuadTree(xMin, yMin, xMax, yMax, factor);

        DebugBuffer.addLine(DebugLevel.DEBUG, "Quadtree initialized! MS " + (System.currentTimeMillis() - then));

        HashMap<Integer, HashMap<Integer, String>> landMapping = new HashMap<Integer, HashMap<Integer, String>>();



        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Loading Land mapping!");

        for (Planet p : LoginService.findAllPlanets()) {
            if (p.getSystemId() > g.getEndSystem() || p.getSystemId() < g.getStartSystem()) {
                continue;
            }
            if (landMapping.containsKey(p.getSystemId())) {
                landMapping.get(p.getSystemId()).put(p.getId(), p.getLandType());
            } else {
                HashMap<Integer, String> planetMapping = new HashMap<Integer, String>();
                planetMapping.put(p.getId(), p.getLandType());
                landMapping.put(p.getSystemId(), planetMapping);
            }
        }

        DebugBuffer.addLine(DebugLevel.UNKNOWN, landMapping.size() + " mappings created");

        HashMap<Integer, Integer> pointsMap = new HashMap<Integer, Integer>();

        for (Statistic s : LoginService.findAllStatistics()) {
            pointsMap.put(s.getUserId(), s.getPoints());
        }





        try {
            for (at.viswars.model.System s : LoginService.findAllSystemsOrdered()) {
                if (s.getId() > g.getEndSystem() || s.getId() < g.getStartSystem()) {
                    continue;
                }
                if (!landMapping.containsKey(s.getId())) {
                    continue;
                }

                int points = 0;
                long population = 0;
                boolean occupied = false;
                boolean habitable = false;

                HashMap<Integer, String> planetMapping = landMapping.get(s.getId());

                for (Integer planetId : planetMapping.keySet()) {
                    PlayerPlanet pp = LoginService.findPlayerPlanetByPlanetId(planetId);

                    if (pp != null) {
                        population += pp.getPopulation();
                        int userId = pp.getUserId();

                        if (pointsMap.containsKey(userId)) {
                            if (points < pointsMap.get(userId)) {
                                points = pointsMap.get(userId);
                            }
                        }

                        occupied = true;
                    }
                }

                if (occupied) {
                    SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), population, points);
                    // if (rs.getInt(1) == 2405) DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding system 2405 with population: " + (int)Math.log10(population) + " points: " + points + " at ("+rs.getInt(3)+","+rs.getInt(4)+")");
                    quadtree.addItemToTree(sd);
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while adding systems to QuadTree: ", e);
        }



        for (int i = xMin; i < xMax; i = i + tileSize) {
            for (int j = yMin; j < yMax; j = j + tileSize) {

                XMLMemo tile = new XMLMemo("Tile");
                tile.addAttribute("x", i);
                tile.addAttribute("y", j);

                int x = i + tileSize / 2;
                int y = j + tileSize / 2;

                ArrayList<SystemDesc> systems = quadtree.getItemsAround(x, y, tileSize * 3);

                tile.addAttribute("systems", systems.size());
                long population = 0;
                int popScore = 1;
                int userScore = 0;
                int intensity = 128;

                for (SystemDesc sd : systems) {
                    double distanceToCore = Math.sqrt(Math.pow(x - sd.x, 2d) + Math.pow(y - sd.y, 2d));

                    /*
                    int popScoreTmp = sd.popscore; //  / (int)Math.ceil((distanceToCore / (double)tileSize) + 0.01d);
                    popScore += popScoreTmp;
                     */

                    population += (int)((double)sd.popscore / Math.pow(10d,Math.max(1d, Math.ceil(distanceToCore / (double)tileSize))));

                    /*
                    if (popScore < sd.popscore) {
                        popScore = sd.popscore;
                    }
                    */

                    if (userScore < sd.statscore) {
                        userScore = sd.statscore;
                    }
                }

             //   Logger.getLogger().write("POPULATION LOG IS " + Math.log10(population));
                intensity = (int)Math.max(0d,Math.min(255d, Math.ceil(255d / 121d * Math.pow(Math.log10(population),2d))));
             //   Logger.getLogger().write("Intensity = " + intensity);

                tile.addAttribute("popScore", popScore);
                tile.addAttribute("userScore", userScore);
                tile.addAttribute("intensity", intensity);
                tiles.addChild(tile);
            }


        }




        return tiles;
    }

    public static String secString(String input) {
        char[] inChars = input.toCharArray();
        char[] outChars = new char[inChars.length];

        for (int i = 0; i
                < inChars.length; i++) {
            if ((inChars[i] >= 65) && (inChars[i] <= 90)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 97) && (inChars[i] <= 122)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 35) && (inChars[i] <= 62)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 192) && (inChars[i] <= 255)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] == 45) || (inChars[i] == 46) || (inChars[i] == 95) || (inChars[i] == 32)) {
                outChars[i] = inChars[i];
                continue;
            }

            outChars[i] = 95;
        }

        StringBuffer sb = new StringBuffer();
        sb.append(outChars);

        return sb.toString();
    }
}
