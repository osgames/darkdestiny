package at.viswars.exceptions;

public class MissingDataException 
        extends Exception {

    private static final long serialVersionUID = -8603578375339751496L;

    public MissingDataException() {
    }

    public MissingDataException(String message) {
        super(message);
    }

    public MissingDataException(Throwable cause) {
        super(cause);
    }

    public MissingDataException(String message, Throwable cause) {
        super(message, cause);
    }

}
