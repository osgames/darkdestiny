/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.exceptions;

/**
 *
 * @author Stefan
 */
public class SystemNameNotUniqueException extends Exception {
    private static final long serialVersionUID = -8603578311334751496L;

    public SystemNameNotUniqueException() {
    }

    public SystemNameNotUniqueException(String message) {
        super(message);
    }

    public SystemNameNotUniqueException(Throwable cause) {
        super(cause);
    }

    public SystemNameNotUniqueException(String message, Throwable cause) {
        super(message, cause);
    }
}
