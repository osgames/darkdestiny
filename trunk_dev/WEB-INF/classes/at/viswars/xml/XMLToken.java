/**
 * 
 */
package at.viswars.xml;

/**
 * @author martin
 *
 */
public class XMLToken {

	private String text;

	public XMLToken(String text)
	{
		this.text = text;
	}
	
	public String getText() {
		return text;
	}

}
