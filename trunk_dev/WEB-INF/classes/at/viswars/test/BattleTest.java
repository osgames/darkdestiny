/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.test;

import at.viswars.Logger.Logger;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class BattleTest {
    public static void runTest() {
        PlayerNode pn1 = new PlayerNode(1);
        PlayerNode pn2 = new PlayerNode(2);
        PlayerNode pn3 = new PlayerNode(3);
        PlayerNode pn4 = new PlayerNode(4);
        PlayerNode pn5 = new PlayerNode(5);
        PlayerNode pn6 = new PlayerNode(6);
        
        setRelation(pn1,pn2,new AlliedConnector());
        setRelation(pn2,pn3, new AlliedConnector());                
        setRelation(pn2,pn6, new EnemyConnector());
        setRelation(pn3,pn4, new EnemyConnector());
        setRelation(pn3,pn5, new EnemyConnector());
        setRelation(pn4,pn5, new AlliedConnector());
        setRelation(pn4,pn6, new EnemyConnector());
        setRelation(pn5,pn6, new AlliedConnector());
        
        ArrayList<PlayerNode> nodes = new ArrayList<PlayerNode>();
        ArrayList<PlayerNode> assignedNodes = new ArrayList<PlayerNode>();     
        
        nodes.add(pn1);
        nodes.add(pn2);
        nodes.add(pn3);
        nodes.add(pn4);
        nodes.add(pn5);
        nodes.add(pn6);
        
        int currGroup = 1;
        
        // Process relations
        while (assignedNodes.size() < nodes.size()) {
            for (PlayerNode pn : nodes) {                    
                Logger.getLogger().write("Masterprocess " + pn.getUserId());
                
                if (assignedNodes.contains(pn)) continue;
                
                if (pn.getGroup() == -1) {
                    assignedNodes.add(pn);
                    pn.setGroup(currGroup);
                    currGroup++;
                }
                
                ArrayList<PlayerNode> stepProcNodes = new ArrayList<PlayerNode>();
                stepProcNodes.add(pn);
                
                for (PlayerNode pnInner : pn.getRelationMap().keySet()) {
                    if (stepProcNodes.contains(pnInner)) continue;
                    if (pn.getRelationMap().get(pnInner) instanceof EnemyConnector) continue;
                    processNode(pnInner,pn,stepProcNodes,assignedNodes);
                }
            }
        }
        
        Logger.getLogger().write("Determined " + (currGroup - 1) + " groups!");
    }
    
    private static void processNode(PlayerNode currNode, PlayerNode masterNode, ArrayList<PlayerNode> stepProcNodes, ArrayList<PlayerNode> assignedNodes) {
        stepProcNodes.add(currNode);
        
        if (!contradicts(currNode,masterNode)) {
            /*
            if (hasSameEnemy(currNode,masterNode)) {
                Logger.getLogger().write(currNode.getUserId() + " shares " + masterNode.getUserId());
                currNode.setGroup(masterNode.getGroup());
                assignedNodes.add(currNode);
            }
            */                        
            Logger.getLogger().write(currNode.getUserId() + " shares " + masterNode.getUserId());
            currNode.setGroup(masterNode.getGroup());
            assignedNodes.add(currNode);            
        } else {
            Logger.getLogger().write(currNode.getUserId() + " contradicts " + masterNode.getUserId());
        }
        
        for (PlayerNode pnInner : currNode.getRelationMap().keySet()) {
            if (stepProcNodes.contains(pnInner)) continue;
            if (currNode.getRelationMap().get(pnInner) instanceof EnemyConnector) continue;
            processNode(pnInner,masterNode,stepProcNodes,assignedNodes);
        }        
    }
    
    private static boolean contradicts(PlayerNode pn1, PlayerNode pn2) {
        for (AttackResolution ar : pn1.getAttacks()) {     
            if (ar.getUserId() == pn2.getUserId()) return true;
            
            for (HelpResolution hr : pn2.getHelps()) {
                if (ar.getUserId() == hr.getUserId()) return true;
            }
        }
        
        for (HelpResolution hr : pn1.getHelps()) {
            for (AttackResolution ar : pn2.getAttacks()) {
                if (ar.getUserId() == hr.getUserId()) return true;
            }
        }
        
        return false;
    }
    
    private static boolean hasSameEnemy(PlayerNode pn1, PlayerNode pn2) {
        for (AttackResolution ar : pn1.getAttacks()) {
            for (AttackResolution ar2 : pn2.getAttacks()) {
                if (ar.getUserId() == ar2.getUserId()) return true;
            }
        }
        
        return false;
    }
    
    private static void setRelation(PlayerNode side1, PlayerNode side2, Connector c) {
        side1.addRelation(side2, c);
        side2.addRelation(side1, c);
    }
}
