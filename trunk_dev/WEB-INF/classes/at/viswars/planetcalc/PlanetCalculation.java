/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.planetcalc;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.model.PlayerPlanet;
import at.viswars.update.UpdaterDataSet;

/**
 *
 * @author Stefan
 */
public class PlanetCalculation {
    private PlayerPlanetDAO ppDao = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    
    private PlayerPlanet pp;
    private ExtPlanetCalculation extPlanetCalc;
    private ProdCalculation prodCalc;
    private final PlanetCalcResult planetCalcResult = new PlanetCalcResult();
    private ProductionResult prodResultFuture = null;
    private final UpdaterDataSet uds;

    public PlanetCalculation(int planetId) throws Exception {
        PlayerPlanet pp = ppDao.findByPlanetId(planetId);                
        if (pp == null) throw new Exception();

        uds = null;
        init(pp);
    }
    
    public PlanetCalculation(PlayerPlanet pp) throws Exception {
        if (pp == null) throw new Exception();                

        uds = null;
        init(pp);
    }

    public PlanetCalculation(PlayerPlanet pp, UpdaterDataSet pcDataSet) throws Exception {
        if (pp == null) throw new Exception();    

        uds = pcDataSet;
        init(pp);
    }    
    
    private void init(PlayerPlanet pp) throws Exception {
        this.pp = pp;
        
        extPlanetCalc = new ExtPlanetCalculation(pp);
        if (uds != null) {
            prodCalc = new ProdCalculation(pp,uds);
        } else {
            prodCalc = new ProdCalculation(pp);
        }

        extPlanetCalc.adjustBaseProdCalculation(prodCalc);                        
        extPlanetCalc.calculateRessDependentValues(prodCalc.getProduction());
    }    
    
    public PlanetCalcResult getPlanetCalcData() {
        planetCalcResult.setExtPlanetCalcData(extPlanetCalc.getData());
        planetCalcResult.setProductionData(prodCalc.getProduction());
        
        return planetCalcResult;
    }
    
    public ProductionResult getProductionDataFuture() {       
        if (prodResultFuture == null) {
            ProdCalculation pc = new ProdCalculation(pp,true);      
            prodResultFuture = pc.getProduction();
        }        
        
        return prodResultFuture;
    }    
    
    public int getPlanetId() {
        if (pp != null) {
            return pp.getPlanetId();
        }
        
        return -1;
    }
    
    public PlayerPlanet getPlayerPlanet() {
        return pp.clone();
    }    
}
