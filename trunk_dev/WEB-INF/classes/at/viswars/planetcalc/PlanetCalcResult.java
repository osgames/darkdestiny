/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.planetcalc;

/**
 *
 * @author Stefan
 */
public class PlanetCalcResult {
    private ExtPlanetCalcResult extPlanetCalcData;
    private ProductionResult productionData;

    public ExtPlanetCalcResult getExtPlanetCalcData() {
        return extPlanetCalcData;
    }

    protected void setExtPlanetCalcData(ExtPlanetCalcResult extPlanetCalcData) {
        this.extPlanetCalcData = extPlanetCalcData;
    }

    public ProductionResult getProductionData() {
        return productionData;
    }

    protected void setProductionData(ProductionResult productionData) {
        this.productionData = productionData;
    }
}
