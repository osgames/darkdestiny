/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.planetcalc;

import at.viswars.Logger.Logger;
import at.viswars.dao.ActionDAO;
import at.viswars.dao.ConstructionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlanetRessourceDAO;
import at.viswars.dao.PlayerResearchDAO;
import at.viswars.dao.ProductionDAO;
import at.viswars.dao.RessourceDAO;
import at.viswars.enumeration.EBonusType;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.model.Action;
import at.viswars.model.Construction;
import at.viswars.model.Planet;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Production;
import at.viswars.model.Ressource;
import at.viswars.ressources.RessProdNode;
import at.viswars.ressources.RessProdNodeSplitted;
import at.viswars.ressources.RessProdTree;
import at.viswars.result.BonusResult;
import at.viswars.update.UpdaterDataSet;
import at.viswars.utilities.BonusUtilities;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class ProdCalculation {

    private RessProdTree rpt;
    private PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private ProductionDAO pDAO = (ProductionDAO) DAOFactory.get(ProductionDAO.class);
    private PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private PlanetDAO plDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private PlayerResearchDAO pResDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    
    private final PlayerPlanet pp;
    private final Planet planet;
    
    private boolean hasDesintegratorBonus = false;
    private double moraleAdjust = 1d;
    
    private boolean calcFuture = false;
    
    public static final int VALUETYPE_PROD = 1;
    public static final int VALUETYPE_CONS = 2;
    public static final int VALUETYPE_STOCK = 3;
    public static final int VALUETYPE_MAXSTOCK = 4;
    public static final int VALUETYPE_MINSTOCK = 5;
    
    private boolean debug = false;
    private HashMap<Integer,ArrayList<String>> calcLog = new HashMap<Integer,ArrayList<String>>();

    private final UpdaterDataSet uds;
    private final boolean updateProcessing;

    public ProdCalculation(PlayerPlanet pp, boolean calcFuture) {
        this.pp = pp;
        planet = plDAO.findById(pp.getPlanetId());
        hasDesintegratorBonus = (pResDAO.findBy(pp.getUserId(), 106) != null);
        
        this.calcFuture = calcFuture;
        this.updateProcessing = false;
        uds = null;

        initMoraleFactor();
        buildTree();
        
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProdCalculation(PlayerPlanet pp) {
        this.pp = pp;
        this.updateProcessing = false;
        uds = null;

        planet = plDAO.findById(pp.getPlanetId());
        hasDesintegratorBonus = (pResDAO.findBy(pp.getUserId(), 106) != null);
        
        initMoraleFactor();
        buildTree();
        
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProdCalculation(PlayerPlanet pp, UpdaterDataSet uds) {
        this.pp = pp;
        this.uds = uds;
        this.updateProcessing = true;

        planet = plDAO.findById(pp.getPlanetId());
        hasDesintegratorBonus = (pResDAO.findBy(pp.getUserId(), 106) != null);

        initMoraleFactor();
        buildTree();

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initMoraleFactor() {
        moraleAdjust = 1d;
        
        if (pp.getMoral() < 70) {
            moraleAdjust = 1d / 70d * (double)pp.getMoral();
        }
    }
    
    private void buildTree() {
        rpt = new RessProdTree();
        RessProdNode rpnIronOre = new RessProdNode(Ressource.PLANET_IRON, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnYnkeloniumOre = new RessProdNode(Ressource.PLANET_YNKELONIUM, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnHowalgoniumOre = new RessProdNode(Ressource.PLANET_HOWALGONIUM, RessProdNode.NODE_TYPE_INDUSTRY);

        RessProdNode rpnIron = new RessProdNode(Ressource.IRON, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnSteel = new RessProdNode(Ressource.STEEL, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnTerkonit = new RessProdNode(Ressource.TERKONIT, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnYnkelonium = new RessProdNode(Ressource.YNKELONIUM, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnHowalgonium = new RessProdNode(Ressource.HOWALGONIUM, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnCVEminium = new RessProdNode(Ressource.CVEMBINIUM, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnEnergy = new RessProdNodeSplitted(Ressource.ENERGY, RessProdNode.NODE_TYPE_ENERGY);
        RessProdNode rpnFood = new RessProdNode(Ressource.FOOD, RessProdNode.NODE_TYPE_AGRICULTURE);

        RessProdNode rpnResearchPoints = new RessProdNode(Ressource.RP, RessProdNode.NODE_TYPE_RESEARCH);
        RessProdNode rpnCompResearchPoints = new RessProdNode(Ressource.RP_COMP, RessProdNode.NODE_TYPE_RESEARCH);

        RessProdNode rpnKasProd = new RessProdNode(Ressource.KAS, RessProdNode.NODE_TYPE_CONSTRUCTION);
        RessProdNode rpnModulProd = new RessProdNode(Ressource.MODUL_AP, RessProdNode.NODE_TYPE_CONSTRUCTION);        
        RessProdNode rpnDockProd = new RessProdNode(Ressource.PL_DOCK, RessProdNode.NODE_TYPE_CONSTRUCTION); 
        RessProdNode rpnOrbDockProd = new RessProdNode(Ressource.ORB_DOCK, RessProdNode.NODE_TYPE_CONSTRUCTION); 
        
        rpnFood.setBaseConsumption((long) (pp.getPopulation() / 1000));
        rpnFood.setActConsumption(rpnFood.getBaseConsumption());        
        addLogEntry(Ressource.FOOD,"Initialize BaseConsumption: " + rpnFood.getBaseConsumption());
        addLogEntry(Ressource.FOOD,"Initialize ActConsumption: " + rpnFood.getActConsumption());

        rpnIron.setPrevious(rpnIronOre);
        rpnYnkelonium.setPrevious(rpnYnkeloniumOre);
        rpnHowalgonium.setPrevious(rpnHowalgoniumOre);

        rpnSteel.setPrevious(rpnIron);
        rpnTerkonit.setPrevious(rpnSteel);

        rpt.setNode(rpnIronOre);
        rpt.setNode(rpnYnkeloniumOre);
        rpt.setNode(rpnHowalgoniumOre);
        rpt.setNode(rpnEnergy);

        rpt.addRootNode(rpnIronOre);
        rpt.addRootNode(rpnYnkeloniumOre);
        rpt.addRootNode(rpnHowalgoniumOre);

        rpt.setNode(rpnIron);
        rpt.setNode(rpnSteel);
        rpt.setNode(rpnTerkonit);
        rpt.setNode(rpnYnkelonium);
        rpt.setNode(rpnHowalgonium);
        rpt.setNode(rpnCVEminium);
        
        rpt.addRootNode(rpnFood);
        rpt.setNode(rpnFood);
        
        rpt.addRootNode(rpnResearchPoints);
        rpt.addRootNode(rpnCompResearchPoints);
        rpt.setNode(rpnResearchPoints);
        rpt.setNode(rpnCompResearchPoints);
        
        rpt.addRootNode(rpnKasProd);
        rpt.addRootNode(rpnModulProd);
        rpt.addRootNode(rpnDockProd);
        rpt.addRootNode(rpnOrbDockProd);
        rpt.setNode(rpnKasProd);
        rpt.setNode(rpnModulProd);
        rpt.setNode(rpnDockProd);
        rpt.setNode(rpnOrbDockProd);        
    }

    private void init() throws Exception {        
        if (debug) {
            Logger.getLogger().write("--------------------------------------------------");
        }
        ArrayList<at.viswars.model.PlanetRessource> prList = null;
        
        if (updateProcessing) {
            prList = uds.getRessEntriesForPlanet(pp.getPlanetId());
        } else {
            prList = prDAO.findByPlanetId(pp.getPlanetId());
        }

        for (at.viswars.model.PlanetRessource pr : prList) {
            if ((pr.getType() == EPlanetRessourceType.INSTORAGE) || (pr.getType() == EPlanetRessourceType.PLANET)) {
                rpt.getNode(pr.getRessId()).setActStorage(pr.getQty());
                addLogEntry(pr.getRessId(),"Set ActStorage to " + pr.getQty());
            }else if (pr.getType() == EPlanetRessourceType.MINIMUM_PRODUCTION){
                rpt.getNode(pr.getRessId()).setMinStorage(pr.getQty());
                addLogEntry(pr.getRessId(),"Set MinStorage to " + pr.getQty());
            }
        }

        if (calcFuture) {
            ArrayList<Action> aList = aDAO.findRunningConstructions(pp.getPlanetId());
            for (Action a : aList) {
                addConstructionToTree(a.getConstructionId(),a.getNumber(),a.getNumber());
            }
        }

        ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(pp.getPlanetId());
        for (PlanetConstruction pc : pcList) {
            addConstructionToTree(pc.getConstructionId(),pc.getActiveCount(),pc.getNumber());
        }

        // When tree is filled go down from root and set values to maxmimum
        for (RessProdNode rpn : rpt.getRootNodes()) {
            RessProdNode rpnTmp = rpn;
            boolean firstLoop = true;

            do {
                if (firstLoop) {
                    firstLoop = false;
                } else {
                    rpnTmp = rpnTmp.getNext();
                }
                                
                at.viswars.model.Ressource r = rDAO.findRessourceById(rpnTmp.ressource);

                if (debug) {
                    Logger.getLogger().write("Process Ressource Node " + rpnTmp.ressource);                // Handle mineable ressources as they get ressource values from Planet
                }
                if (r.getMineable()) {
                    at.viswars.model.PlanetRessource pr = null;                    
                    if (updateProcessing) {
                        pr = uds.getPlanetRessEntry(pp.getPlanetId(), EPlanetRessourceType.PLANET, r.getId());
                    } else {
                        pr = prDAO.findBy(pp.getPlanetId(), r.getId(), EPlanetRessourceType.PLANET);
                    }

                    // Set consumption to zero as there are no further ore deposits
                    // Decrease Consumption and Production of following node                   
                    if (pr == null) {
                        rpnTmp.setActConsumption(0);
                        addLogEntry(rpnTmp.ressource,"Set ActConsumption to 0 (due to no ore available)");
                        if (rpnTmp.getNext() != null) {
                            if (debug) Logger.getLogger().write("Set Production for " + rpnTmp.getNext().ressource + " ["+pp.getPlanetId()+"] to 0");
                            rpnTmp.getNext().setActProduction(0);
                            rpnTmp.getNext().setMaxProductionByPrevious(0);
                            addLogEntry(rpnTmp.getNext().ressource,"Set ActProduction to 0 (due to no ore on previous)");
                            addLogEntry(rpnTmp.getNext().ressource,"Set MaxProductionByPrevious to 0 (due to no ore on previous)");
                            // rpnTmp.getNext().setBaseProduction(VALUETYPE_PROD);
                        }

                        continue;
                    }

                    // Set consumption to new limit due to low ore deposits
                    // Decrease Consumption and Production of following node
                    if (pr.getQty() < rpnTmp.getActConsumption()) {
                        double maxEff = 1d / rpnTmp.getActConsumption() * pr.getQty();
                        rpnTmp.setActConsumption(Math.max(1,pr.getQty()));

                        addLogEntry(rpnTmp.ressource,"Set ActConsumption to "+pr.getQty()+" (due to low ore)");

                        if (rpnTmp.getNext() != null) {
                            long actProduction = (long)Math.max(1l,(long)Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                            rpnTmp.getNext().setActProduction(actProduction);
                            addLogEntry(rpnTmp.getNext().ressource,"Set ActProduction to "+rpnTmp.getNext().getActProduction()+" (due to low ore on previous)");
                            rpnTmp.getNext().setMaxProductionByPrevious(actProduction);
                            addLogEntry(rpnTmp.getNext().ressource,"Set MaxProductionByPrevious to "+rpnTmp.getNext().getMaxProductionByPrevious()+" (due to low ore on previous)");
                        }
                    }
                } else {
                    if (!((rpnTmp.type == RessProdNode.NODE_TYPE_RESEARCH) || 
                          (rpnTmp.type == RessProdNode.NODE_TYPE_CONSTRUCTION))) {  
                        // Limit production due to full stock
                        if (debug) {
                            Logger.getLogger().write("[" + rpnTmp.ressource + "] ACTSTORAGE=" + rpnTmp.getActStorage() + " ACTPROD=" + rpnTmp.getActProduction() + " ACTCONS=" + rpnTmp.getActConsumption() + " MAXSTORAGE=" + rpnTmp.getMaxStorage());
                        }
                        
                        if ((rpnTmp.getActStorage() + rpnTmp.getActProduction() - rpnTmp.getActConsumption()) > rpnTmp.getMaxStorage()) {
                            if (debug) {
                                Logger.getLogger().write("Limit production due to stock for ress " + rpnTmp.ressource);
                            }
                            // double maxEff = 100d / rpnTmp.getActProduction() * (rpnTmp.getMaxStorage() - rpnTmp.getActStorage());
                            rpnTmp.setActProduction(rpnTmp.getMaxStorage() - rpnTmp.getActStorage() + rpnTmp.getActConsumption());
                            addLogEntry(rpnTmp.ressource,"Reduce ActProduction to " + rpnTmp.getActProduction() + " cause it exceeds maximum storage");
                        }                        
                        
                        if ((rpnTmp.getMinStorage() > 0) && (rpnTmp.getNext() != null)) {
                            if (debug) {
                                Logger.getLogger().write("MINSTOCK >> ");
                                Logger.getLogger().write("STORAGE: " + rpnTmp.getActStorage() + " PROD: " + rpnTmp.getActProduction() + " CONS: " + rpnTmp.getActConsumption());
                                Logger.getLogger().write("COMPARE: " + (rpnTmp.getActStorage() + rpnTmp.getActProduction() - rpnTmp.getActConsumption()) + " <=> " + rpnTmp.getMinStorage());
                            }
                            if (rpnTmp.getActStorage() + rpnTmp.getActProduction() - rpnTmp.getActConsumption() < rpnTmp.getMinStorage()) {
                                double maxCons = Math.max(0, rpnTmp.getActStorage() + rpnTmp.getActProduction() - rpnTmp.getMinStorage());
                                if (debug) Logger.getLogger().write("MAXCONS: " + maxCons);
                                double maxEff = 1d / rpnTmp.getActConsumption() * maxCons;
                                
                                if (maxEff < 1d) {          
                                    if (debug) {
                                        Logger.getLogger().write("Set Act Cons of " + r.getName() + " to " + maxCons);
                                        Logger.getLogger().write("Max Eff of next node = " + maxEff);
                                        Logger.getLogger().write("Set Production of next node to " + (long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                                    }
                                    
                                    rpnTmp.setActConsumption((long)maxCons);
                                    rpnTmp.getNext().setActProduction((long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                                    rpnTmp.getNext().setMaxProductionByPrevious((long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                                    addLogEntry(rpnTmp.ressource,"Set ActConsumption to " + maxCons + " due to minStorage");
                                    addLogEntry(rpnTmp.getNext().ressource,"Set ActProduction to " + rpnTmp.getNext().getActProduction() + " due to minStorage limitation on previous node");
                                    addLogEntry(rpnTmp.getNext().ressource,"Set MaxProductionByPrevious to " + rpnTmp.getNext().getMaxProductionByPrevious() + " due to minStorage limitation on previous node");
                                }
                            }
                        }
                        
                        if (rpnTmp.type != RessProdNode.NODE_TYPE_AGRICULTURE) {
                            if (rpnTmp.getActConsumption() > (rpnTmp.getActProduction() + rpnTmp.getActStorage())) {
                                if (debug) {
                                    Logger.getLogger().write("Check if " + r.getName() + " Consumption ("+rpnTmp.getActConsumption()+") is larger than PROD + STORAGE ("+(rpnTmp.getActProduction() + rpnTmp.getActStorage())+")");
                                }                              
                                double maxEff = 1d / rpnTmp.getActConsumption() * (rpnTmp.getActProduction() + rpnTmp.getActStorage());
                                if (rpnTmp.getNext() != null) {
                                    rpnTmp.getNext().setActProduction((long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                                    addLogEntry(rpnTmp.getNext().ressource,"Set ActProduction to " + rpnTmp.getNext().getActProduction() + " as production and storage of previous node is lower than consumption");
                                }
                                rpnTmp.setActConsumption((rpnTmp.getActProduction() + rpnTmp.getActStorage()));
                                addLogEntry(rpnTmp.ressource,"Set ActConsumption to " + rpnTmp.getActConsumption() + " cause Consumption higher than production + storage");
                            }
                        } else {
                            double maxConsByProdAndStock = rpnTmp.getActProduction() + rpnTmp.getActStorage();
                            
                            rpnTmp.setActConsumption(rpnTmp.getBaseConsumption());
                            addLogEntry(rpnTmp.ressource,"Set ActConsumption to " + rpnTmp.getActConsumption() + " (BaseConsumption) as we have enough food to meet demands");
                            rpnTmp.setActConsumption((long)Math.min(maxConsByProdAndStock, rpnTmp.getActConsumption()));
                            addLogEntry(rpnTmp.ressource,"Limit ActConsumption to " + rpnTmp.getActConsumption() + " which is either BaseConsumption or available Qty in Stock + production");
                        }
                    }
                }
            } while (rpnTmp.getNext() != null);

            // If the last node is reached we check the production of this node and update backwards
            firstLoop = true;
            do {
                if (firstLoop) {
                    firstLoop = false;
                } else {
                    rpnTmp = rpnTmp.getPrevious();
                }

                if (debug) {
                    Logger.getLogger().write("Backward Check " + rpnTmp.ressource);                                       
                }

                RessProdNode rpnPrev = rpnTmp.getPrevious();
                if (rpnPrev == null) break;
                
                // Scale consumption of previous node if this node has decreased production                
                if (rpnTmp.getActProduction() > 0) {                    
                    if (rpnTmp.getActProduction() < rpnTmp.getBaseProduction()) {                        
                        double maxEff = 1d / rpnTmp.getBaseProduction() * rpnTmp.getActProduction();
                        
                        if (rpnTmp.type != RessProdNode.NODE_TYPE_AGRICULTURE) {
                            rpnPrev.setActConsumption((int)Math.floor(rpnPrev.getBaseConsumption() * maxEff));
                            addLogEntry(rpnPrev.ressource,"Set ActConsumption to " + rpnPrev.getActConsumption() + " (BaseConsumption * maxEff)");
                        }
                        
                        if (!rDAO.findRessourceById(rpnPrev.ressource).getMineable()) {
                            long freeCapacity = rpnPrev.getMaxStorage() - (rpnPrev.getActStorage() - rpnPrev.getActConsumption());
                            long toProduce = Math.min(freeCapacity, rpnPrev.getMaxProductionByPrevious());
                            if (debug) {
                                Logger.getLogger().write("Set new production for " + rpnPrev.ressource + " -- FREECAP: " + freeCapacity + " TOPROD: " + toProduce);
                            }
                            rpnPrev.setActProduction(toProduce);
                            addLogEntry(rpnPrev.ressource,"Set ActProduction to " + rpnPrev.getActProduction() + ", which is maxQty in respect to storage and production of previous node");
                        }                        
                    }
                } else {
                    if (rpnTmp.type != RessProdNode.NODE_TYPE_AGRICULTURE) {
                        rpnPrev.setActConsumption(0);
                        addLogEntry(rpnPrev.ressource,"Set ActConsumption to 0 as next node has no production");
                    }
                }
                
                if (!rDAO.findRessourceById(rpnPrev.ressource).getMineable()) {
                    if ((rpnPrev.getActStorage() + rpnPrev.getActProduction() - rpnPrev.getActConsumption()) > rpnPrev.getMaxStorage()) {
                        if (debug) {
                            Logger.getLogger().write("Limit production due to stock for ress " + rpnPrev.ressource);
                        }
                        rpnPrev.setActProduction((long)Math.min(rpnPrev.getMaxStorage() - rpnPrev.getActStorage() + rpnPrev.getActConsumption(),rpnPrev.getMaxProductionByPrevious()));
                        addLogEntry(rpnPrev.ressource,"Set ActProduction to " + rpnPrev.getActConsumption() + " which in respect to storage of current node and production of previous");
                    }   
                }
            } while (rpnTmp.getPrevious() != null);            
        }        
    }

    private void addConstructionToTree(int cId, int count, int totalCount) {
        Construction c = cDAO.findById(cId);
        ArrayList<Production> pList = pDAO.findProductionForConstructionId(cId);
        for (Production p : pList) {
            // prDAO.findBy(pp.getPlanetId(), p.getRessourceId(), EPlanetRessourceType.MINIMUM);

            if (p.getDecinc() == Production.PROD_STORAGE) {
                RessProdNode rpn = rpt.getNode(p.getRessourceId());

                if (rpn != null) {
                    rpn.setMaxStorage(rpn.getMaxStorage() + (p.getQuantity() * totalCount));
                    addLogEntry(rpn.ressource,"(Storage) SetMaxStorage to " + rpn.getMaxStorage());
                }
            } else if (p.getDecinc() == Production.PROD_INCREASE) {
                RessProdNode rpn = rpt.getNode(p.getRessourceId());

                if (rpn != null) {
                    long qty = p.getQuantity();
                    
                    if (hasDesintegratorBonus && 
                       ((p.getRessourceId() == Ressource.IRON) ||
                        (p.getRessourceId() == Ressource.YNKELONIUM))) {
                        qty = (long)(qty * 1.2d);
                    }
                    
                    if ((planet.getLandType().equalsIgnoreCase(Planet.LANDTYPE_J)) &&
                        (p.getRessourceId() == Ressource.IRON)) {
                        qty = (long)(qty * 1.25d);
                    }            

                    // Solar planet
                    if (cId == Construction.ID_SOLARPLANT) {
                        if (p.getRessourceId() == Ressource.ENERGY) {
                            int orbitLevel = planet.getOrbitLevel();
                            float factor = 1f;

                            if (orbitLevel < 6) {
                                factor += (6f - orbitLevel) * 0.1f;
                            } else if (orbitLevel > 6) {
                                factor -= (orbitLevel - 6f) * 0.1f;
                            }

                            qty = (long)Math.floor((float)qty * factor);
                        }
                    }

                    // ResearchBonus
                    if ((p.getRessourceId() == Ressource.RP) || (p.getRessourceId() == Ressource.RP_COMP)) {
                        BonusResult br = BonusUtilities.findBonus(planet.getId(), EBonusType.RESEARCH);
                        if (br != null) {
                            long inc = (long)(qty * br.getPercBonus());
                            inc += br.getNumericBonus();
                            
                            qty += inc;
                        }
                    } else if ((p.getRessourceId() == Ressource.MODUL_AP) ||
                               (p.getRessourceId() == Ressource.ORB_DOCK) ||
                               (p.getRessourceId() == Ressource.PL_DOCK)) {
                        BonusResult br = BonusUtilities.findBonus(planet.getId(), EBonusType.PRODUCTION);
                        if (br != null) {
                            long inc = (long)(qty * br.getPercBonus());
                            inc += br.getNumericBonus();
                            
                            qty += inc;
                        }                        
                    }
                    
                    rpn.setBaseProduction((long)(rpn.getBaseProduction() + qty * count * moraleAdjust));
                    rpn.setActProduction(rpn.getBaseProduction());
                    rpn.setMaxProductionByPrevious(rpn.getBaseProduction());
                    addLogEntry(rpn.ressource,"(AddConstruction/INCREASE) Set BaseProduction to " + rpn.getBaseProduction());
                    addLogEntry(rpn.ressource,"(AddConstruction/INCREASE) Set ActProduction to " + rpn.getActProduction());
                    addLogEntry(rpn.ressource,"(AddConstruction/INCREASE) Set MaxProductionByPrevious to " + rpn.getMaxProductionByPrevious());
                }
            } else if (p.getDecinc() == Production.PROD_DECREASE) {
                RessProdNode rpn = rpt.getNode(p.getRessourceId());

                if (rpn != null) {
                    // This only matches for Energy Node
                    if (rpn instanceof RessProdNodeSplitted) {
                        RessProdNodeSplitted rpns = (RessProdNodeSplitted) rpn;
                        // Logger.getLogger().write("Add energy consumption of " + (p.getQuantity() * count) + " for ProdType " + c.getProdType() + "["+c.getName()+"]");
                        rpns.addBaseConsumption(p.getQuantity() * count, c.getProdType());
                        addLogEntry(rpns.ressource,"(Energy) Add baseConsumption of " + (p.getQuantity() * count) + " for ProdType " + c.getProdType());
                    }

                    long qty = p.getQuantity();
                    
                    if (hasDesintegratorBonus && 
                       ((p.getRessourceId() == Ressource.PLANET_IRON) ||
                        (p.getRessourceId() == Ressource.PLANET_YNKELONIUM))) {
                        qty = (long)(qty * 1.2d);
                    }
                    
                    if ((planet.getLandType().equalsIgnoreCase(Planet.LANDTYPE_J)) &&
                        (p.getRessourceId() == Ressource.PLANET_IRON)) {
                        qty = (long)(qty * 1.25d);
                    }                    
                    
                    // Do not decrease Energy consumption if morale is low
                    if (!(rpn instanceof RessProdNodeSplitted)) {
                        rpn.setBaseConsumption((long)(rpn.getBaseConsumption() + qty * count * moraleAdjust));
                    } else {
                        rpn.setBaseConsumption((long)(rpn.getBaseConsumption() + qty * count));
                    }
                    rpn.setActConsumption(rpn.getBaseConsumption());
                    addLogEntry(rpn.ressource,"(AddConstruction/DECREASE) Set BaseConsumption to " + rpn.getBaseConsumption());
                    addLogEntry(rpn.ressource,"(AddConstruction/DECREASE) Set ActConsumption to " + rpn.getActConsumption());
                }
            }
        }
    }

    protected long getValue(int ressId, int valueType) {
        RessProdNode rpn = rpt.getNode(ressId);

        if (rpn == null) {
            return 0;
        }
        switch (valueType) {
            case (VALUETYPE_PROD):
                return rpn.getActProduction();
            case (VALUETYPE_CONS):
                return rpn.getActConsumption();
            case (VALUETYPE_STOCK):
                return rpn.getActStorage();
            case (VALUETYPE_MAXSTOCK):
                return rpn.getMaxStorage();
            case (VALUETYPE_MINSTOCK):
                return rpn.getMinStorage();
            default:
                return 0;
        }
    }

    protected RessProdNode getNode(int ressId) {
        return rpt.getNode(ressId);
    }

    protected RessProdTree getProdTree() {
        return rpt;
    }

    protected ProductionResult getProduction() {
        ProductionResult pr = new ProductionResult(rpt);
        pr.setCalcLog(calcLog);
        return pr;
    }

    private void addLogEntry(int ressId, String message) {
        Ressource r = rDAO.findRessourceById(ressId);
        message = "[" + r.getName() + "] " + message;

        if (calcLog.containsKey(ressId)) {
            calcLog.get(ressId).add(message);
        } else {
            ArrayList<String> messageList = new ArrayList<String>();
            messageList.add(message);
            calcLog.put(ressId, messageList);
        }
    }
    
    private void printLogEntry(int ressId) {
        ArrayList<String> logEntries = calcLog.get(ressId);
        
        if (logEntries != null) {
            for (String entry : logEntries) {
                System.out.println("RESSLOG: " + entry);
            }
        }
    }
}
