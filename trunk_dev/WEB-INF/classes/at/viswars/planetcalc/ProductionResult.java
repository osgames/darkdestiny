/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.planetcalc;

import at.viswars.ressources.RessProdNode;
import at.viswars.ressources.RessProdTree;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ProductionResult {
    private HashMap<Integer,Long> production = new HashMap<Integer,Long>();
    private HashMap<Integer,Long> baseProduction = new HashMap<Integer,Long>();
    private HashMap<Integer,Long> baseConsumption = new HashMap<Integer,Long>();
    private HashMap<Integer,Long> consumption = new HashMap<Integer,Long>();
    private HashMap<Integer,Long> stock = new HashMap<Integer,Long>();
    private HashMap<Integer,Long> maxStock = new HashMap<Integer,Long>();
    private HashMap<Integer,Long> minStock = new HashMap<Integer,Long>();

    private HashMap<Integer,ArrayList<String>> calcLog = null;
    
    public ProductionResult(RessProdTree rpt) {
        for (Map.Entry<Integer,RessProdNode> rpnEntry : rpt.getAllNodes().entrySet()) {
            int key = rpnEntry.getKey();
            RessProdNode rpn = rpnEntry.getValue();
            
            production.put(key, rpn.getActProduction());
            baseProduction.put(key, rpn.getBaseProduction());
            baseConsumption.put(key, rpn.getBaseConsumption());
            consumption.put(key, rpn.getActConsumption());
            stock.put(key, rpn.getActStorage());
            maxStock.put(key, rpn.getMaxStorage());
            minStock.put(key, rpn.getMinStorage());
        }
    }
    
    public long getRessProduction(int ressId) {
        if (production.containsKey(ressId)) {
            return production.get(ressId);
        } else {
            return 0;
        }
    }
    
    public long getRessBaseProduction(int ressId) {
        if (baseProduction.containsKey(ressId)) {
            return baseProduction.get(ressId);
        } else {
            return 0;
        }
    }    

    public long getRessBaseConsumption(int ressId) {
        if (baseConsumption.containsKey(ressId)) {
            return baseConsumption.get(ressId);
        } else {
            return 0;
        }
    }        
    
    public long getRessConsumption(int ressId) {
        if (consumption.containsKey(ressId)) {
            return consumption.get(ressId);
        } else {
            return 0;
        }        
    }
    
    public long getRessStock(int ressId) {
        if (stock.containsKey(ressId)) {
            return stock.get(ressId);
        } else {
            return 0;
        }        
    }
    
    public long getRessMaxStock(int ressId) {
        if (maxStock.containsKey(ressId)) {
            return maxStock.get(ressId);
        } else {
            return 0;
        }        
    }
    
    public long getRessMinStock(int ressId) {
        if (minStock.containsKey(ressId)) {
            return minStock.get(ressId);
        } else {
            return 0;
        }        
    }

    /**
     * @return the calcLog
     */
    public HashMap<Integer, ArrayList<String>> getCalcLog() {
        return calcLog;
    }

    /**
     * @param calcLog the calcLog to set
     */
    public void setCalcLog(HashMap<Integer, ArrayList<String>> calcLog) {
        this.calcLog = calcLog;
    }
}
