/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.planetcalc;

import at.viswars.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.construction.InConstructionData;
import at.viswars.dao.*;
import at.viswars.enumeration.EBonusType;
import at.viswars.enumeration.EConstructionProdType;
import at.viswars.enumeration.EProductionOrderType;
import at.viswars.model.*;
import at.viswars.ressources.RessProdNode;
import at.viswars.ressources.RessProdNodeSplitted;
import at.viswars.result.BonusResult;
import at.viswars.utilities.BonusUtilities;
import at.viswars.utilities.PlanetUtilities;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 *
 * This class offers methods to calculate additional values for a certain
 * planet, like taxes, production efficiency, population growths an so on
 */
public class ExtPlanetCalculation {

    private PlayerPlanetDAO ppDao = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private PlanetLoyalityDAO plDAO = (PlanetLoyalityDAO) DAOFactory.get(PlanetLoyalityDAO.class);
    private final PlayerPlanet pp;
    private final ExtPlanetCalcResult epcResult = new ExtPlanetCalcResult();
    private ArrayList<PlanetConstruction> pcList = null;    // static stuff
    private float baseEfficiency = 0f;
    private final Planet p;
    private boolean debug = false;
    private int militaryPower = -1;
    private static final int MINPOP_MAXCRIME = 500000;

    public ExtPlanetCalculation(int planetId) throws Exception {
        pp = ppDao.findByPlanetId(planetId);
        if (pp != null) {
            p = pDAO.findById(pp.getPlanetId());
            init();
        } else {
            throw new Exception("Initialisation of ExtPlanetCalculation failed");
        }
    }

    public ExtPlanetCalculation(PlayerPlanet pp) throws Exception {
        this.pp = pp;
        if (pp != null) {
            p = pDAO.findById(pp.getPlanetId());
            init();
        } else {
            throw new Exception("Initialisation of ExtPlanetCalculation failed");
        }
    }

    private void init() {
        if (pcList == null) {
            pcList = pcDAO.findByPlanetId(pp.getPlanetId());
        }
        int populationNeeded = 0;
        int addPopulation = 0;
        int taxIncome = (int) (((float) pp.getPopulation() * (float) pp.getTax()) / (float) GameConstants.GLOBAL_MONEY_MODIFICATOR);
        float taxFactor = 1f;

        int inConsUsedPop = 0;
        int inConsUsedSpace = 0;

        LinkedList<InConstructionData> inConstruction = at.viswars.utilities.ConstructionUtilities.getInConstructionData(pp.getPlanetId());
        for (InConstructionData icd : inConstruction) {
            // Ignore IMPROVE and UPGRADE currently
            if (icd.getConstructionData().getProdOrderType() == EProductionOrderType.C_IMPROVE
                    || icd.getConstructionData().getProdOrderType() == EProductionOrderType.C_UPGRADE) {
                continue;
            }

            inConsUsedPop += icd.getConstructionData().getConstruct().getWorkers() * icd.getConstructionData().getCount();
            inConsUsedSpace += icd.getConstructionData().getConstruct().getSurface() * icd.getConstructionData().getCount();
        }

        epcResult.setInConsUsedPopulation(inConsUsedPop);
        epcResult.setInConsUsedSpace(inConsUsedSpace);

        for (PlanetConstruction pc : pcList) {
            Construction c = cDAO.findById(pc.getConstructionId());
            populationNeeded += c.getWorkers() * pc.getNumber();
            addPopulation += c.getPopulation() * pc.getNumber();

            if (c.getId() == Construction.ID_LOCALADMINISTRATION) {
                taxFactor = Math.max(taxFactor, 1.05f);
            }
            if (c.getId() == Construction.ID_PLANETADMINISTRATION) {
                taxFactor = Math.max(taxFactor, 1.15f);
            }
            if (c.getId() == Construction.ID_PLANETGOVERNMENT) {
                taxFactor = Math.max(taxFactor, 1.3f);
            }
        }

        if (pp.getHomeSystem()) {
            taxFactor += 0.2f;
        }

        taxIncome = (int) ((float) taxIncome * taxFactor);


        epcResult.setUsedPopulation(populationNeeded);
        epcResult.setBaseTaxIncome(taxIncome);
        epcResult.setNeededPopulation(populationNeeded);
        epcResult.setMaxPopulation(p.getMaxPopulation() + addPopulation);

        // Calculate Population efficiency first 
        // Depending on that we can determine efficiency on energy production
        float popEfficiency = Math.min(1f, 1f / populationNeeded * pp.getPopActionPoints());
        float moraleEfficiency = 0.5f + Math.min(0.5f, 0.5f / 100f * pp.getMoral());

        baseEfficiency = popEfficiency * moraleEfficiency;
        epcResult.setEffEnergy(baseEfficiency);
        epcResult.setEffAgriculture(taxFactor);
    }

    protected void calculateRessDependentValues(ProductionResult pr) {
        calcFoodCoverage(pr);
        calcCriminality();
        calcMorale();
        calcRiots();
        //Morale, Riots and Criminality has to be processed before invoking dev calc
        calcDevelopementPoints();
        calcGrowth();
        adjustTaxes();
    }

    private void calcFoodCoverage(ProductionResult pr) {
        float foodCoverage = 0;

        long foodIncrease = 0;
        long foodDecrease = 0;
        long storage = 0;

        foodIncrease = pr.getRessProduction(Ressource.FOOD);
        foodDecrease = pr.getRessBaseConsumption(Ressource.FOOD);
        storage = pr.getRessStock(Ressource.FOOD);

        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "FoodProduction="+foodIncrease+" FoodConsumption="+foodDecrease+" FoodStock="+storage);

        float coverageByStock = (float) storage / (float) Math.max(0,foodDecrease - foodIncrease);

        if (Float.isInfinite(coverageByStock) || Float.isNaN(coverageByStock)) {
            coverageByStock = (float) storage / 1f;
        }

        if (coverageByStock <= 0) {
            if (foodDecrease > 0) {
                foodCoverage = ((float) 100 / foodDecrease * foodIncrease) + Math.abs(storage / (float) foodDecrease);
            }
        } else {
            if (coverageByStock < 1) {
                if (foodDecrease > 0) {
                    foodCoverage = (float) 100 / foodDecrease * (float) (foodIncrease + coverageByStock);
                }
            } else {
                foodCoverage = 100f + coverageByStock;
            }
        }

        // if (p.getId() == 11046) java.lang.System.out.println("FoodCoverageStock is " + coverageByStock);
        
        float foodCoverageNoStock = (float) 100 / (float) foodDecrease * (float) foodIncrease;

        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "FoodCoverage="+foodCoverage+" FoodCoverageNoStock="+foodCoverageNoStock);

        epcResult.setFoodcoverage(foodCoverage);
        epcResult.setFoodcoverageNoStock(foodCoverageNoStock);
    }

    private void calcMorale() {
        double lowFoodFactor = 1d;

        int maxMorale = 100;
        int moraleBonus = 0;
        int estimatedMorale = 100;

        float foodCoverage = epcResult.getFoodcoverage();
        float foodCoverageNoStock = epcResult.getFoodcoverageNoStock();
        
        int foodMorale = 0;

        // if (p.getId() == 11046) java.lang.System.out.println("FoodCoverage is " + foodCoverage + " FoodCoverageNoStock is " + epcResult.getFoodcoverageNoStock());
        
        if (foodCoverage >= 100) {
            float foodBonus = 0f;
            
            if (foodCoverageNoStock > 100) {
                // We produce more than we need, lets check with stock to get full bonus
                // 1. For each percent abot 100 we receive 1 Bonus Point
                foodBonus += foodCoverageNoStock - 100f;
                
                // 2. For every 100 ticks Storage coverage we receive 1 Bonus Points
                foodBonus += (foodCoverage - foodCoverageNoStock) / 100f;
            } else {
                // Coverage is good but we live from stock ... more harsh calculation
                // 1. For every 100 ticks Storage coverate we receive 1 Bonus Point
                foodBonus += (int)Math.floor((foodCoverage - 100f) / 100f);
            }
            
            // 3. Cap to 5 points
            if (foodBonus > 5) {
                foodBonus = 5f;
            }
            
            estimatedMorale += foodBonus;
            maxMorale += foodBonus;       
            
            foodMorale = (int) Math.ceil(foodBonus); 
            
            /*
            float foodBonus = (epcResult.getFoodcoverage() - 100) / 2;
            if (foodBonus > 5) {
                foodBonus = 5;
            }
            estimatedMorale += foodBonus;
            maxMorale += foodBonus;

            foodMorale = (int) Math.ceil(foodBonus);            
            */
        } else if (foodCoverage <= 100) {
            float foodMalus = (int) (100f - foodCoverage);
            estimatedMorale -= foodMalus;
            foodMorale -= foodMalus;

            if (foodMalus > 25) {
                lowFoodFactor = 0d;
            } else {
                lowFoodFactor = 1d - (1d / 25d * foodMalus);
            }
        }
        
        // if (p.getId() == 11046) java.lang.System.out.println("FoodMorale: " + foodMorale + " lowFoodFactor: " + lowFoodFactor);

        if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M)
                || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G)
                || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {
            moraleBonus = 30;

            if (!(p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_A) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_B))) {
                int freeSurface = (int) (((float) 100 / (float) p.getSurfaceSquares()) * (float) p.getSurface().getFreeSurface());

                if (freeSurface < 30) {
                    moraleBonus = (int) (moraleBonus * (1d / 30d * freeSurface));
                }

                if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {

                    PlanetConstruction pc = pcDAO.findBy(pp.getPlanetId(), Construction.ID_RECREATIONAL_PARK);
                    if (pc != null) {
                        int parkBuildings = pc.getNumber();
                        if (parkBuildings > 0) {
                            double effPark = 100d / (double) pp.getPopulation() * ((double) parkBuildings * 250000000d);
                            if (effPark > 200) {
                                effPark = 200;
                            }

                            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M)) {
                                moraleBonus += 50d / 100d * effPark;
                            } else {
                                moraleBonus += 25d / 100d * effPark;
                            }
                        }
                    }
                }

                if (moraleBonus > 50) {
                    moraleBonus = 50;
                }
            }

            maxMorale += moraleBonus;

            float orgMoraleBonus = moraleBonus;
            estimatedMorale += moraleBonus * lowFoodFactor;

            if (foodCoverage <= 100) {
                foodMorale -= (int) Math.ceil(orgMoraleBonus - (moraleBonus * lowFoodFactor));
            }

            // DebugBuffer.addLine("ADD BONUS BY SURFACE = " + moraleBonus);
        }

        if (pp.getTax() < 25) {
            estimatedMorale += 25 - pp.getTax();
            epcResult.setMoraleBonusTax(25 - pp.getTax());
        } else if (pp.getTax() > 25) {
            estimatedMorale -= (pp.getTax() - 25) * 2;
            epcResult.setMoraleBonusTax((25 - pp.getTax()) * 2);
        }

        epcResult.setMoralFood(foodMorale);
        epcResult.setMorale(pp.getMoral());
        epcResult.setMoraleBonus(moraleBonus);
        epcResult.setMaxMorale(maxMorale);
        epcResult.setEstimatedMorale(estimatedMorale);
    }

    private void calcGrowth() {
        float changevalueGrowth = 0;
        float maxGrowth = 5;

        int morale = pp.getMoral();
        // Increase Maximum Population Growth by 2 % per 30 extra Morale
        if (morale > 100) {
            maxGrowth += (float) 0.02 * ((float) (morale - 100) * ((float) 100 / (float) 30));
        }

        if (morale < 50) {
            maxGrowth -= (50 - morale) * 0.15;
        }

        double maxGrowthBonus = 0;
        BonusResult br = BonusUtilities.findBonus(pp.getPlanetId(), EBonusType.POPULATION_GROWTH);
        if (br != null) {
            maxGrowthBonus += br.getNumericBonus();
        }

        maxGrowth += maxGrowthBonus;
        epcResult.setMaxGrowthBonus((float) maxGrowthBonus);

        if (epcResult.getFoodcoverage() < 100) {
            changevalueGrowth -= (100 - epcResult.getFoodcoverage()) * (float) 0.01;
        } else {
            changevalueGrowth += 1;
        }

        // if (pp.getPlanetId() == 15014) Logger.getLogger().write("CV Growth="+changevalueGrowth);        
        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "CV Growth="+changevalueGrowth);

        float minGrowth = (-100 + epcResult.getFoodcoverage());

        if (minGrowth > 0) {
            minGrowth = 0;
        }
        if (minGrowth > maxGrowth) {
            minGrowth = maxGrowth;
        }

        epcResult.setGrowth(pp.getGrowth());
        epcResult.setMinGrowth(minGrowth);
        epcResult.setMaxGrowth(maxGrowth);
        epcResult.setChangevalueGrowth(changevalueGrowth);
    }

    protected void adjustBaseProdCalculation(ProdCalculation pc) {
        if (debug) {
            Logger.getLogger().write("-------------- START CALCULATING ENERGY (" + p.getId() + ") ---------------");
        }

        // Get Energy production from Ressource Calculation
        // and adjust with base efficiency
        RessProdNodeSplitted energyNode = (RessProdNodeSplitted) pc.getNode(8);
        // energyNode.printNode();        

        // long energyProd = (long) (energyNode.getBaseProduction() * baseEfficiency);
        long energyProd = (long) (energyNode.getBaseProduction());
        long energyCons = energyNode.getBaseConsumption();

        if (debug) {
            Logger.getLogger().write("ENERGY PRODUCTION " + energyProd + " ENERGY CONSUMPTION " + energyCons);
        }

        float energyEfficiency = Math.min(1f, 1f / energyCons * energyProd);

        if (debug) {
            Logger.getLogger().write("TOTAL BALANCE " + (energyProd - energyCons) + " EFF " + energyEfficiency);
        }
        long availEnergy = energyProd - energyNode.getBaseConsumption(EConstructionProdType.COMMON);
        if (debug) {
            Logger.getLogger().write("ENERGY PROD AFTER GENERAL " + availEnergy);
            Logger.getLogger().write("ENERGY CONSUMPTION AGRAR = " + energyNode.getBaseConsumption(EConstructionProdType.AGRAR));
            Logger.getLogger().write("ENERGY CONSUMPTION INDUSTRY = " + energyNode.getBaseConsumption(EConstructionProdType.INDUSTRY));
            Logger.getLogger().write("ENERGY CONSUMPTION RESEARCH = " + energyNode.getBaseConsumption(EConstructionProdType.RESEARCH));
        }
        if (availEnergy < 0) {
            availEnergy = 0;
        }

        // Set eff common
        epcResult.setEffCommon(energyEfficiency);

        // Adjust calculated efficiency values to Ressource Calculation
        TreeMap<Integer, HashMap<EConstructionProdType, Long>> distriMap =
                new TreeMap<Integer, HashMap<EConstructionProdType, Long>>();

        distriMap.put(PlayerPlanet.PRIORITY_LOW, new HashMap<EConstructionProdType, Long>());
        distriMap.put(PlayerPlanet.PRIORITY_MEDIUM, new HashMap<EConstructionProdType, Long>());
        distriMap.put(PlayerPlanet.PRIORITY_HIGH, new HashMap<EConstructionProdType, Long>());

        HashMap<EConstructionProdType, ArrayList<RessProdNode>> categoryNodes =
                new HashMap<EConstructionProdType, ArrayList<RessProdNode>>();

        categoryNodes.put(EConstructionProdType.AGRAR, new ArrayList<RessProdNode>());
        categoryNodes.put(EConstructionProdType.INDUSTRY, new ArrayList<RessProdNode>());
        categoryNodes.put(EConstructionProdType.RESEARCH, new ArrayList<RessProdNode>());

        for (RessProdNode rpn : pc.getProdTree().getAllNodes().values()) {
            if (rpn.type == RessProdNode.NODE_TYPE_AGRICULTURE) {
                categoryNodes.get(EConstructionProdType.AGRAR).add(rpn);
            } else if (rpn.type == RessProdNode.NODE_TYPE_INDUSTRY) {
                categoryNodes.get(EConstructionProdType.INDUSTRY).add(rpn);
            } else if (rpn.type == RessProdNode.NODE_TYPE_RESEARCH) {
                categoryNodes.get(EConstructionProdType.RESEARCH).add(rpn);
            }
        }

        EConstructionProdType currType = EConstructionProdType.RESEARCH;
        int currPrio = PlayerPlanet.PRIORITY_LOW;

        switch (pp.getPriorityResearch()) {
            case (PlayerPlanet.PRIORITY_LOW):
                currPrio = PlayerPlanet.PRIORITY_LOW;
                break;
            case (PlayerPlanet.PRIORITY_MEDIUM):
                currPrio = PlayerPlanet.PRIORITY_MEDIUM;
                break;
            case (PlayerPlanet.PRIORITY_HIGH):
                currPrio = PlayerPlanet.PRIORITY_HIGH;
        }

        distriMap.get(currPrio).put(currType, energyNode.getBaseConsumption(currType));

        currType = EConstructionProdType.AGRAR;
        currPrio = PlayerPlanet.PRIORITY_LOW;

        switch (pp.getPriorityAgriculture()) {
            case (PlayerPlanet.PRIORITY_LOW):
                currPrio = PlayerPlanet.PRIORITY_LOW;
                break;
            case (PlayerPlanet.PRIORITY_MEDIUM):
                currPrio = PlayerPlanet.PRIORITY_MEDIUM;
                break;
            case (PlayerPlanet.PRIORITY_HIGH):
                currPrio = PlayerPlanet.PRIORITY_HIGH;
        }

        distriMap.get(currPrio).put(currType, energyNode.getBaseConsumption(currType));

        currType = EConstructionProdType.INDUSTRY;
        currPrio = PlayerPlanet.PRIORITY_LOW;

        switch (pp.getPriorityIndustry()) {
            case (PlayerPlanet.PRIORITY_LOW):
                currPrio = PlayerPlanet.PRIORITY_LOW;
                break;
            case (PlayerPlanet.PRIORITY_MEDIUM):
                currPrio = PlayerPlanet.PRIORITY_MEDIUM;
                break;
            case (PlayerPlanet.PRIORITY_HIGH):
                currPrio = PlayerPlanet.PRIORITY_HIGH;
        }

        distriMap.get(currPrio).put(currType, energyNode.getBaseConsumption(currType));

        for (HashMap<EConstructionProdType, Long> prioEntry : distriMap.values()) {
            if (prioEntry.isEmpty()) {
                continue;
            }

            long totalConsumption = 0l;

            for (Long cons : prioEntry.values()) {
                totalConsumption += cons;
            }

            float actEff = 0f;
            if (totalConsumption == 0l) {
                actEff = 1f;
            } else {
                if (totalConsumption <= availEnergy) {
                    availEnergy -= totalConsumption;
                    actEff = 1f;
                } else if (availEnergy > 0) {
                    actEff = (float) availEnergy / (float) totalConsumption;
                    availEnergy = 0;
                } else {
                    actEff = 0f;
                }
            }

            for (EConstructionProdType actType : prioEntry.keySet()) {
                if (actType == EConstructionProdType.AGRAR) {
                    epcResult.setEffAgriculture(actEff);
                    for (RessProdNode rpn : categoryNodes.get(EConstructionProdType.AGRAR)) {                   
                        rpn.setActProduction((long) ((float) Math.min(rpn.getBaseProduction() * actEff, rpn.getActProduction())));

                        if (rpn.getActStorage() - rpn.getActConsumption() + rpn.getActProduction() < 0) {
                            rpn.setActConsumption(rpn.getActStorage() + rpn.getActProduction());
                        }
                    }
                } else if (actType == EConstructionProdType.INDUSTRY) {
                    epcResult.setEffIndustry(actEff);
                    for (RessProdNode rpn : categoryNodes.get(EConstructionProdType.INDUSTRY)) {
                        rpn.setActProduction((long) ((float) Math.min(rpn.getBaseProduction() * actEff, rpn.getActProduction())));
                        
                        if (rpn.getActStorage() - rpn.getActConsumption() + rpn.getActProduction() < 0) {
                            rpn.setActConsumption(rpn.getActStorage() + rpn.getActProduction());
                        }
                        
                        if (rpn.getPrevious() != null) {
                            rpn.getPrevious().setActConsumption((long) ((float) rpn.getPrevious().getActConsumption() * actEff));
                        }
                    }
                } else if (actType == EConstructionProdType.RESEARCH) {
                    epcResult.setEffResearch(actEff);
                    for (RessProdNode rpn : categoryNodes.get(EConstructionProdType.RESEARCH)) {
                        rpn.setActProduction((long) ((float) Math.min(rpn.getBaseProduction() * actEff, rpn.getActProduction())));

                        if (rpn.getActStorage() - rpn.getActConsumption() + rpn.getActProduction() < 0) {
                            rpn.setActConsumption(rpn.getActStorage() + rpn.getActProduction());
                        }
                    }
                }
            }
        }

        if (debug) {
            Logger.getLogger().write("-------------- END CALCULATING ENERGY ---------------");
        }
    }

    private int castEnumerationToRPNType(EConstructionProdType eType) {
        if (eType == EConstructionProdType.AGRAR) {
            return RessProdNode.NODE_TYPE_AGRICULTURE;
        } else if (eType == EConstructionProdType.INDUSTRY) {
            return RessProdNode.NODE_TYPE_INDUSTRY;
        } else if (eType == EConstructionProdType.RESEARCH) {
            return RessProdNode.NODE_TYPE_RESEARCH;
        } else if (eType == EConstructionProdType.ENERGY) {
            return RessProdNode.NODE_TYPE_ENERGY;
        }

        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "No cast possible for " + eType);
        return -1;
    }

    private EConstructionProdType castRPNTypeToEnumeration(int rpnType) {
        if (rpnType == RessProdNode.NODE_TYPE_AGRICULTURE) {
            return EConstructionProdType.AGRAR;
        } else if (rpnType == RessProdNode.NODE_TYPE_INDUSTRY) {
            return EConstructionProdType.INDUSTRY;
        } else if (rpnType == RessProdNode.NODE_TYPE_RESEARCH) {
            return EConstructionProdType.RESEARCH;
        } else if (rpnType == RessProdNode.NODE_TYPE_ENERGY) {
            return EConstructionProdType.ENERGY;
        }

        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "No cast possible for " + rpnType);
        return null;
    }

    protected ExtPlanetCalcResult getData() {
        return epcResult;
    }

    private void calcCriminality() {
        double criminalityValue = pp.getPopulation() * 0.005d - getMilitaryPower(pp.getPlanetId());
        double baseCriminality = pp.getPopulation() * 0.005d;

        if (pp.getPopulation() < MINPOP_MAXCRIME) {
            criminalityValue = (criminalityValue / MINPOP_MAXCRIME) * (double) pp.getPopulation();
        }

        double criminalityPercent = Math.round((criminalityValue / baseCriminality) * 100d);
        if (criminalityPercent < 0) {
            criminalityPercent = 0d;
        }
        epcResult.setCriminalityValue(criminalityPercent);

    }

    public int getMilitaryPower(int planetId) {
        if (militaryPower > -1) {
            return militaryPower;
        }

        try {
            for (PlayerTroop pt : ptDAO.findByPlanetId(planetId)) {
                GroundTroop gt = gtDAO.findById(pt.getTroopId());
                militaryPower += gt.getHitpoints() * pt.getNumber();
            }
        } catch (Exception e) {
            Logger.getLogger().write("ex : " + e);
        }

        return militaryPower;
    }

    private void calcRiots() {
        double riotValue = pp.getUnrest();
        double riotValueChange = 0d;

        // if no entry for current owner exists create one
        PlanetLoyality pLoy = plDAO.getByPlanetAndUserId(pp.getPlanetId(), pp.getUserId());
        if (pLoy == null) {
            pLoy = new PlanetLoyality();
            pLoy.setPlanetId(pp.getPlanetId());
            pLoy.setUserId(pp.getUserId());
            pLoy.setValue(0d);
            plDAO.add(pLoy);
        }

        // determine loyality for this planet
        HashMap<Integer, PlanetLoyality> loyalMap = plDAO.findByPlanetIdMap(pp.getPlanetId());
        PlanetLoyality pl = loyalMap.get(pp.getUserId());

        // Base Loyality gain for each loyality
        HashMap<Integer, BigDecimal> userLoyalityGain = new HashMap<Integer, BigDecimal>();
        double totalEnemyGain = 0d;
        double noLoyality = 100d;

        for (Map.Entry<Integer, PlanetLoyality> loyEntry : loyalMap.entrySet()) {
            int userId = loyEntry.getKey();
            PlanetLoyality plTmp = loyEntry.getValue();
            noLoyality -= plTmp.getValue();

            double distance = PlanetUtilities.getDistanceToHome(plTmp.getUserId(), plTmp.getPlanetId());
            double baseGain = Math.min(1d, (200d / Math.max(1d, distance) + 0.05d));

            userLoyalityGain.put(userId, new BigDecimal(baseGain));
            if (userId != pp.getUserId()) {
                totalEnemyGain += baseGain;
                // java.lang.System.out.println("EnemyGain for "+userId+" = " + baseGain);
            }
        }

        noLoyality = Math.max(0d, noLoyality); // prevent value getting negative

        double cGain = userLoyalityGain.get(pp.getUserId()).doubleValue();
        double currUserGain = cGain;

        if (userLoyalityGain.size() > 1) {
            currUserGain = Math.min(cGain, cGain / (1d + (totalEnemyGain / (userLoyalityGain.size() - 1))));
        }

        // java.lang.System.out.println("NoLoyality = " + noLoyality);
        // java.lang.System.out.println("MyGain = " + currUserGain + " TotalEnemyGain = " + totalEnemyGain + " UserLoyalityGain SIZE="+userLoyalityGain.size());

        BigDecimal currUserGainDec = new BigDecimal(currUserGain);
        BigDecimal enemyGainTotal = new BigDecimal(0);

        currUserGainDec.setScale(2, RoundingMode.UP);

        epcResult.setLoyalityChange(currUserGainDec);
        for (Map.Entry<Integer, BigDecimal> ulgEntry : userLoyalityGain.entrySet()) {
            if (!ulgEntry.getKey().equals(pp.getUserId())) {
                // java.lang.System.out.println("[Pre] Gain for " + ulgEntry.getKey() + " = " + ulgEntry.getValue());
                if (userLoyalityGain.size() >= 2) {
                    ulgEntry.setValue(new BigDecimal(-(currUserGain - currUserGain / totalEnemyGain * ulgEntry.getValue().doubleValue())));
                } else {
                    ulgEntry.setValue(new BigDecimal(-currUserGain));
                }
                // java.lang.System.out.println("Gain loss for " + ulgEntry.getKey() + " = " + ulgEntry.getValue());
            }
        }

        userLoyalityGain.remove(pp.getUserId());
        epcResult.setEnemyLoyalityChange(userLoyalityGain);

        double calmingValue = 0d;
        if (getMilitaryPower(pp.getPlanetId()) > 0) {
            calmingValue = Math.min(1d, 100d / (pp.getPopulation() / getMilitaryPower(pp.getPlanetId())));
        }

        if (pl == null) {
            epcResult.setLoyalityValue(new BigDecimal(0));
            if (pp.getUnrest() < 50d) {
                riotValueChange = 1d;
            }
        } else {
            epcResult.setLoyalityValue(new BigDecimal(pl.getValue()).setScale(2, RoundingMode.UP));

            double targetUnrest = (50d - (50d / 80d * pl.getValue()));
            targetUnrest -= targetUnrest * (calmingValue * 0.9d);
            if (pl.getValue() >= 80d) {
                targetUnrest = 0;
            }

            if (pp.getUnrest() < targetUnrest) {
                riotValueChange = Math.pow(80d - pl.getValue(), 1.05d) / 100d;
            } else {
                riotValueChange = -Math.min(((pp.getUnrest() - targetUnrest) / 100d), 0.1d);
            }
        }

        // Recalculate Loyality
        // Low morale will cause loyality loss
        // High morale will cause faster loyality gain
        // if planet is closer to another loyality gain will be slowed down and vice versa

        epcResult.setRiotValue(riotValue);
        // epcResult.setRiotValueChange(PopulationUtilities.getGrowthRiots(PopulationUtilities.getDistanceToHomeSystem(pp.getPlanetId()), pp.getPlanetId()));
        epcResult.setRiotValueChange(riotValueChange);

    }

    private void adjustTaxes() {

        int baseTaxIncome = epcResult.getBaseTaxIncome();
        int taxIncome = baseTaxIncome;

        int riotBase = (int) (((float) pp.getPopulation() * 50f) / (float) GameConstants.GLOBAL_MONEY_MODIFICATOR);
        int taxRiotDiscount = (int) Math.round(riotBase * (epcResult.getRiotValue() / 35l));

        taxIncome -= taxRiotDiscount;
        epcResult.setTaxRiotDiscount(taxRiotDiscount);

        double criminalityPercent = epcResult.getCriminalityValue();
        int taxCrimDiscount = (int) Math.round((baseTaxIncome * ((criminalityPercent) / 200l)));
        taxIncome -= taxCrimDiscount;
        epcResult.setTaxCrimDiscount(taxCrimDiscount);
        epcResult.setTaxIncome(taxIncome);
    }

    private void calcDevelopementPoints() {

        double devRiots = (Math.round((int) Math.floor((GameConstants.RIOTS_TRESHOLD - epcResult.getRiotValue()) * 10d)) / 100d);
        double devCriminality = Math.round((int) Math.floor((GameConstants.CRIMINALITY_THRESHOLD - epcResult.getCriminalityValue()) * 10d)) / 100d;
        double devMorale = Math.round((int) Math.floor((epcResult.getMorale() - GameConstants.MORALE_THRESHOLD) * 10d)) / 100d;

        double developementPoints = devRiots + devCriminality + devMorale;

        epcResult.setDevCriminality(devCriminality);
        epcResult.setDevMorale(devMorale);
        epcResult.setDevRiots(devRiots);
        epcResult.setDevelopementPoints(developementPoints);
        epcResult.setDevelopementPointsGlobal(developementPoints * (double) pp.getPopulation() / GameConstants.DEVELOPEMENT_POPULATION);
    }
}
