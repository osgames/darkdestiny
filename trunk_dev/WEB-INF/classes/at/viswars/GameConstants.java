
/*
 * Constants.java
 *
 * Created on 11. M�rz 2003, 20:47
 */

package at.viswars;

/**
 *
 * @author  Rayden
 */
public class GameConstants {
    public static final boolean LOGIN_ONLYADMIN = false;


    public static int UNIVERSE_HEIGHT = 10000;
    public static int UNIVERSE_WIDTH = 10000;
 /*  
    // MILITARY SUPPORT BUILDINGS
    public static final int MILITARY_SPACEPORT_FIGHTER = 50000;
    public static final int MILITARY_SPACEPORT_CORVETTE = 10000;
    */
    // GLOBAL VALUES
    public static final int TRIAL_END_TECH = 31;
    public static final int GLOBAL_MONEY_MODIFICATOR = 200000;
    public static final double GLOBAL_MAX_MILIZ_PERC = 0.005d;
    public static final int GLOBAL_MILIZ_TRAIN_UNIT = 2780;
 
    // ACTION TYPES
    public static final int ACT_TYPE_BUILDING = 1;
    public static final int ACT_TYPE_RESEARCH = 2;
    public static final int ACT_TYPE_MODULE = 3;
    public static final int ACT_TYPE_SHIP = 4;
    public static final int ACT_TYPE_GROUNDTROOPS = 5;
    public static final int ACT_TYPE_FLIGHT = 6;
    public static final int ACT_TYPE_DEFENSE = 7;
    public static final int ACT_TYPE_SPYRELATED = 8;
    public static final int ACT_TYPE_TRADE = 9;
    public static final int ACT_TYPE_DECONSTRUCT = 10;
    public static final int ACT_TYPE_PROBE = 11;
    
    // PLANET SWITCH 
    public static final int SWITCH_HOMESYSTEM = 1;
    public static final int SWITCH_NEXT_HIGHER = 2;
    public static final int SWITCH_NEXT_LOWER = 3;
    public static final int SWITCH_BY_PARAMETER = 4;
    
    // TYPES OF VALUES
    public static final int VALUE_TYPE_NORMAL = 1;
    public static final int VALUE_TYPE_ENERGY = 2;
    public static final int VALUE_TYPE_INTONS = 3;
    
    // TYPES OF PLANET
     public static final int OWN_PLANET = 1;
    public static final int ENEMY_PLANET = 2;
    public static final int NEUTRAL_PLANET = 3;
       /*
    // TYPES OF RESSOURCES
    
    public static final int RES_INCREASE = 0;
    public static final int RES_DECREASE = 1;
    public static final int RES_STORAGE = 2;
    
    public static final int RES_IRON = 1;
    public static final int RES_STEEL = 2;
    public static final int RES_TERKONIT = 3;
    public static final int RES_YNKELONIUM = 4;
    public static final int RES_HOWALGONIUM = 5;
    public static final int RES_CVEMBINIUM = 6;
    public static final int RES_WASSERSTOFF = 7;
    public static final int RES_ENERGY = 8;
    public static final int RES_FOOD = 9;
    public static final int RES_RP = 10;
    public static final int RES_RP_COMP = 14;
    public static final int RES_PLANET_IRON = 11;
    public static final int RES_PLANET_YNKELONIUM = 12;
    public static final int RES_PLANET_HOWALGONIUM = 13;    
    public static final int RES_POPULATION = 20;
    public static final int RES_MODUL_AP = 30;
    public static final int RES_PL_DOCK = 31;
    public static final int RES_ORB_DOCK = 32;
    public static final int RES_KAS = 33;
    */    
    // TYPES OF SYSTEMVIEWS
    public static final int SYS_OWN = 1;
    public static final int SYS_GENERAL = 2;
    public static final int SYS_BY_ID = 3;
    public static final int SYS_BY_NAME = 4;
    /*
    // TYPES OF TECHTREE
    public static final int TECH_BUILD = 0;
    public static final int TECH_RESEARCH = 1;
    public static final int TECH_MODULE = 2;
    public static final int TECH_GROUNDTROOPS = 3;
    
    // TYPES OF COMMANDS
    public static final int FLEET_MOVE = 1;
    public static final int FLEET_ATTACK = 2;
    public static final int FLEET_SPY = 3;
    public static final int FLEET_COLONIZE = 4;
    public static final int FLEET_TRADE = 5;
    public static final int FLEET_EXPLORE = 6;
     
     */
      
    // TYPES OF MODULES
    public static final int MODULE_HYPER_ENGINE = 1;
    public static final int MODULE_ARMOR = 2;
    public static final int MODULE_WEAPON = 3;
    public static final int MODULE_ENGINE = 4;
    public static final int MODULE_CHASSIS = 5;
    public static final int MODULE_ROCKET = 6;
    public static final int MODULE_FREE_2 = 7;
    public static final int MODULE_SHIELD = 8;    
    public static final int MODULE_WEAPON_SUPPORT = 9;
    public static final int MODULE_REACTOR = 10;
    public static final int MODULE_SPECIAL = 11;
    public static final int MODULE_HANGAR = 12;
    /*
    // CHANCE TO TARGET
    public static final int PRIORITY_PRIMARY = 3;
    public static final int PRIORITY_SECONDARY = 2;
    public static final int PRIORITY_LIGHTLY_DAMAGED = 1;
    public static final int PRIORITY_HEAVILY_DAMAGED = 2;
    public static final int PRIORITY_SLOW = 1;
    
    // UNIT TYPE
    public static final int UNIT_SHIP = 1;
    public static final int UNIT_STATION = 2;
    public static final int UNIT_BUILDING = 3;
    
    // TYPE OF ORDERS
    public static final int ORDER_ATTACK = 1;
    public static final int ORDER_BOMBARDMENT = 2;
    public static final int ORDER_INVASION = 3;
    public static final int ORDER_SIEGE = 4;
    
    // TYPE OF COMMANDS
    public static final int COMMAND_MOVE = 1;
    public static final int COMMAND_ATTACK = 2;
    public static final int COMMAND_COLONIZE = 3;
    public static final int COMMAND_SPY = 4;
    public static final int COMMAND_TRADE = 5;
    
    // COMBAT LEVEL
    public static final int GROUND_COMBAT = 1;
    public static final int ORBITAL_COMBAT = 2;
    public static final int SYSTEM_COMBAT = 3;
    
    // RESS CALCULATION MODES
    public static final int OUTPUT_PLANET_RESS = 1;
    public static final int OUTPUT_PLANET_STOCK = 2;
    public static final int OUTPUT_PLANET_STOCKLIMIT = 3;
    public static final int OUTPUT_PLANET_STOCKLIMITMAX = 4;
    */
    // LOADING TYPES
    public static final int LOADING_RESS = 1;
    public static final int LOADING_TROOPS = 2;
    public static final int LOADING_SHIPS = 3;
    public static final int LOADING_POPULATION = 4;
 
    
    // CONFIRM TYPES
    public static final int CONFIRM_FLEET_MOVEMENT = 1;
    public static final int DESTROY_BUILDING = 2;
    public static final int DESTROY_BUILDING_ORDER = 3;
    public static final int DESTROY_SHIP = 4;
    public static final int DESTROY_SHIP_BUILD_ORDER = 5;   
    public static final int CONFIRM_SHIP_UPGRADE = 6;
    public static final int CONFIRM_INCONSTRUCTION_CANCEL = 7;
    public static final int CONFIRM_PROBE = 8;
    public static final int CONFIRM_DESTROY_STARBASE = 9;
    public static final int CONFIRM_UPGRADE_STARBASE = 10;
    public static final int CONFIRM_TRADE_TREATY = 11;
  
    // PLANET UPGRADES
    public static final int UPDATE_LOCAL_ADMINISTRATION = 1;
    public static final int UPDATE_PLANETARY_ADMINISTRATION = 2;
    public static final int UPDATE_PLANETARY_GOVERNMENT = 3;
    public static final int UPDATE_MINING_DESINTEGRATOR = 4;
    public static final int UPDATE_VULCAN_ACTIVITY = 5;
    public static final int UPDATE_HOMEWORLD = 6;
    public static final int UPDATE_SUPER_COMPUTER = 7;
      /*
    // VOTING 
    public static final int VOTE_DEGRADE = 10;
    public static final int VOTE_PROMOTE = 11;
        
    */
    public final static int STATISTIC_INCOME = 0;
    public final static int STATISTIC_POPULATION = 1;
    public final static int STATISTIC_PLANETS = 2;
    public final static int STATISTIC_SYSTEMS = 3;
        /*
    public final static int INVADE = 99;  
     */
    // Links
    public final static String LINK_ADMIN = "subCategory=99&page=admin";
    //Zeit bis der Account gel�scht wird
    //nachdem der Benutzer die L�schung beantragt hat
	public static final int DELETION_DELAY = 1008;

	//Hier wird beschrieben, wie ein Password, in der
	//Datenbank verschl�sselt wird
    public final static String ENCRYPT_PWD_START = "MD5('"; 
    public final static String ENCRYPT_PWD_END = "')";

    //Invalidierung der Session eines Users:
    public static final int INVALIDATE_USER_AFTER= 3600;


    public static boolean LOGIN_AVAILABLE = true;

    //Hier wird beschrieben, nach welcher Zeit in Ticks ein
    //Benutzer als inaktive betrachtet wird
	public static final int USER_DEAD_2 = 6*24*14; //6 Ticks / Stunde * 24 Stunden/Tag * 14 Tage


        //Management => Happiness

       public static final int MORALE_THRESHOLD = 80;
       public static final double CRIMINALITY_THRESHOLD = 25d;
       public static final double RIOTS_TRESHOLD = 25d;
       public static final double DEVELOPEMENT_POPULATION = 7000000000d;
       public static final double DEVELOPEMENT_POINTS_LIMIT_GLOBAL = 20;

    public final static Object appletSync = new Object();
    public static long NAME_PROTECTION_DURATION = 6 * 24 * 3;
}
