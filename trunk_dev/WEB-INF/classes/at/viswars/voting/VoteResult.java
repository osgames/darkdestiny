/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.voting;

import java.util.HashMap;
import at.viswars.model.VoteOption;

/**
 *
 * @author Stefan
 */
public class VoteResult {
    public static final int VOTED_ACCEPT = 1;
    public static final int VOTED_DENIED = 2;
    public static final int VOTE_CLOSED_ACCEPT = 3;
    public static final int VOTE_CLOSED_DENIED = 4;
    public static final int VOTE_CLOSED_OPTION = 5;
    public static final int VOTED = 6;
    public static final int VOTE_ALREADY_VOTED = 7;
    public static final int VOTE_NOT_EXISTS = 8;
    public static final int VOTE_NOT_EXPIRED = 9;
    public static final int VOTE_STATE = 999;
    
    private int voteCode;
    private int choosenOption;
    private HashMap<Integer,Integer> userVotes;
    private HashMap<Integer,VoteOption> options;

    public VoteResult(final int voteCode, final HashMap<Integer,Integer> userVotes, final HashMap<Integer,VoteOption> options) throws Exception {
        if (voteCode == VOTE_CLOSED_OPTION) throw new Exception("Invalid voteCode");
        
        this.voteCode = voteCode;
        this.choosenOption = 0;
        this.userVotes = userVotes;
        this.options = options;
    }    
    
    public VoteResult(final int voteCode, final int choosenOption, final HashMap userVotes, final HashMap<Integer,VoteOption> options) throws Exception {
        if ((voteCode == VOTE_CLOSED_OPTION) && (choosenOption < 1)) throw new Exception("Invalid voteCode");
        
        this.voteCode = voteCode;
        this.choosenOption = choosenOption;
        this.userVotes = userVotes;
        this.options = options;
    }
    
    public HashMap<Integer,Integer> getUserVotes() {
        return (HashMap<Integer,Integer>)userVotes.clone();
    }

    public HashMap<Integer,VoteOption> getOptions() {
        return (HashMap<Integer,VoteOption>)options.clone();
    }    
    
    public int getVoteCode() {
        return voteCode;
    }
}
