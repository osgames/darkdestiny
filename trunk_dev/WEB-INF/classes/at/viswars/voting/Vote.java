/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.voting;

import at.viswars.DebugBuffer;
import at.viswars.model.VoteOption;
import at.viswars.model.VotePermission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 *
 * @author Stefan
 */
public class Vote {

    private final int voteStarter;
    private final String voteTopic;
    private final String voteMessage;

    private final VotingTextMLSecure votingTextML;

    private int minVotes;
    private int totalVotes;
    private final int type;
    private int expires = 0;
    private boolean closed = false;
    private int closeOption = 0;
    private ArrayList<VotePermission> perms = new ArrayList<VotePermission>();
    private ArrayList<VoteOption> options = new ArrayList<VoteOption>();
    private ArrayList<Integer> receivers = new ArrayList<Integer>();
    private HashMap<String, DataEntry> metadata = new HashMap<String, DataEntry>();

    private final boolean msgTranslation;

    public Vote(final int voteStarter, final String voteTopic, final String voteMessage, final int type) throws Exception {
        this.voteStarter = voteStarter;
        this.voteTopic = voteTopic;
        this.voteMessage = voteMessage;
        this.type = type;

        this.votingTextML = null;
        this.msgTranslation = false;

        if ((type < 0) || (type > 3 && type != 6 && type != 7 && type != 8 && type != 9 && type != 18 && type != 19 && type != 20 && type != 21 && type != 22 && type != 100 && type != 30)) {
            throw new Exception("Invalid vote type!");
        }
    }

    public Vote(final int voteStarter, final VotingTextMLSecure votingTextML, final int type) throws Exception {
        this.voteStarter = voteStarter;
        this.voteTopic = "";
        this.voteMessage = "";
        this.type = type;

        this.votingTextML = votingTextML;
        this.msgTranslation = true;

        if ((type < 0) || (type > 3 && type != 6 && type != 7 && type != 8 && type != 9 && type != 18 && type != 19 && type != 20 && type != 21 && type != 22 && type != 100 && type != 30)) {
            throw new Exception("Invalid vote type!");
        }
    }

    public int getVoteStarter() {
        return voteStarter;
    }

    public String getVoteTopic() {
        return voteTopic;
    }

    public int getMinVotes() {
        return minVotes;
    }

    public void setMinVotes(int minVotes) {
        this.minVotes = minVotes;
    }

    public int getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }

    public int getType() {
        return type;
    }

    public int getExpires() {
        return expires;
    }

    public void setExpires(int expires) {
        this.expires = expires;
    }

    public ArrayList<VotePermission> getPerms() {
        ArrayList<VotePermission> clone = new ArrayList<VotePermission>();

            for (VotePermission vp : perms) {
                clone.add(vp.clone());
            }

        return clone;
    }

    public void addPerm(VotePermission perm) throws Exception {
        for (VotePermission vp : perms) {
            if (vp.getType() != perm.getType()) {
                throw new Exception("Vote type alliance and single user may not be mixed!");
            }
        }

        perms.add(perm);
    }

    public void removePerm(VotePermission perm) {
        perms.remove(perm);
    }

    public ArrayList<VoteOption> getOptions() {
        ArrayList<VoteOption> clone = new ArrayList<VoteOption>();

        for (VoteOption o : options) {
            clone.add(o.clone());
        }
        return clone;
    }

    public void addOption(VoteOption option) throws Exception {
        for (VoteOption o : options) {
            if (option.getPreSelect() && o.getPreSelect()) {
                throw new Exception("Only 1 option my be preselected!");
            }
            if ((option.getType() == VoteOption.OPTION_TYPE_ACCEPT) && (o.getType() == VoteOption.OPTION_TYPE_ACCEPT)) {
                throw new Exception("Only 1 option may be TYPE_ACCEPT");
            }
            if ((option.getType() == VoteOption.OPTION_TYPE_DENY) && (o.getType() == VoteOption.OPTION_TYPE_DENY)) {
                throw new Exception("Only 1 option may be TYPE_DENY");
            }
        }

        options.add(option);
    }

    public void removeOption(VoteOption option) {
        options.remove(option);
    }

    public void addMetaData(String key, Object value, MetaDataDataType dataType) {
        metadata.put(key, new DataEntry(value, dataType));
    }

    public void removeMetaData(String key) {
        metadata.remove(key);
    }

    protected HashMap<String, DataEntry> getMetaData() {
        return metadata;
    }

    public Object getMetaData(String key) {
        DataEntry de = metadata.get(key);
        if (de == null) {
            return null;
        }

        if (de.getDataType() == MetaDataDataType.STRING) {
            return de.getValue();
        } else if (de.getDataType() == MetaDataDataType.INTEGER) {
            return Integer.parseInt(de.getValue().toString());
        } else if (de.getDataType() == MetaDataDataType.DOUBLE) {
            return Double.parseDouble(de.getValue().toString());
        }

        return key;
    }

    public ArrayList<Integer> getReceivers() {
        return receivers;
    }

    public void setReceivers(ArrayList<Integer> receivers) {
        this.receivers = receivers;
    }

    public String getVoteMessage() {
        return voteMessage;
    }

    public void closeVote(int closeOption) {
        this.closed = true;
        this.closeOption = closeOption;
    }

    public boolean isClosed() {
        return closed;
    }

    public int getCloseOption() {
        return closeOption;
    }

    /**
     * @return the msgTranslation
     */
    public boolean isMsgTranslation() {
        return msgTranslation;
    }

    /**
     * @return the votingTextML
     */
    public VotingTextMLSecure getVotingTextML() {
        return votingTextML;
    }


}
