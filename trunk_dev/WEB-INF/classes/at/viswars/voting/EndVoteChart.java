/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.voting;

import at.viswars.DebugBuffer;
import at.viswars.ML;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.VoteOptionDAO;
import at.viswars.model.VoteOption;
import at.viswars.utilities.LanguageUtilities;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Stefan
 */
public class EndVoteChart {
    private JFreeChart chart;

    private VoteOptionDAO voDAO = (VoteOptionDAO) DAOFactory.get(VoteOptionDAO.class);

    public EndVoteChart(int userId, int voteId) {
        try {
            HashMap<Integer, Integer> votePowerMap = new HashMap<Integer, Integer>();
            TreeMap<Integer, Integer> optionPower = new TreeMap<Integer, Integer>();

            int totalNotVoted = 0;
            int totalVotePower = 0;
            int noOfBars = 0;

            // Get totalVotePower
            Vote v = VoteUtilities.getVote(voteId);
            HashMap<String, DataEntry> metaDataMap = v.getMetaData();

            for (Map.Entry<String, DataEntry> metaDataEntry : metaDataMap.entrySet()) {
                String key = metaDataEntry.getKey();
                DataEntry de = metaDataEntry.getValue();

                if (key.startsWith("VOTEPOWER")) {
                    int userIdTmp = Integer.parseInt(key.replace("VOTEPOWER", ""));
                    int power = Integer.parseInt((String)de.getValue());

                    totalVotePower += power;
                    totalNotVoted += power;
                    votePowerMap.put(userIdTmp, power);
                }
            }

            // Get Number of options
            ArrayList<VoteOption> voList = v.getOptions();
            noOfBars = voList.size() + 1;

            // Create map with option and voted power
            VoteResult vr = VoteUtilities.currentState(userId, voteId);
            HashMap<Integer, Integer> userVotes = vr.getUserVotes();

            for (Map.Entry<Integer, Integer> userVote : userVotes.entrySet()) {
                int userIdTmp = userVote.getKey();
                int optionId = userVote.getValue();

                int userPower = votePowerMap.get(userIdTmp);
                totalNotVoted -= userPower;

                if (optionPower.containsKey(optionId)) {
                    optionPower.put(optionId,optionPower.get(optionId) + userPower);
                } else {
                    optionPower.put(optionId,userPower);
                }
            }

            // Now we have all necessary data and can fill the chart!
            DefaultPieDataset dataset = new DefaultPieDataset();

            for (Map.Entry<Integer, Integer> opEntry : optionPower.entrySet()) {
                VoteOption vo = voDAO.findById(opEntry.getKey());

                String voteOptionText = vo.getVoteOption();
                if (vo.isConstant()) {
                    voteOptionText = ML.getMLStr(voteOptionText, userId);
                } else {
                    Locale l = LanguageUtilities.getMasterLocaleForUser(userId);
                    voteOptionText = VoteUtilities.translateParametrizedString(vo.getVoteOption(),l);
                }

                dataset.setValue(voteOptionText, opEntry.getValue());
            }

            dataset.setValue(ML.getMLStr("vote_option_notvoted",userId), totalNotVoted);

            JFreeChart chart = ChartFactory.createPieChart(
                "Abstimmungsergebnis",  // chart title
                dataset,             // data
                true,               // include legend
                true,
                false
            );

            PieSectionLabelGenerator labels = new
            StandardPieSectionLabelGenerator("{0} ({2})");
            
            PiePlot plot = (PiePlot) chart.getPlot();
            plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
            plot.setNoDataMessage("No data available");
            plot.setCircular(false);
            plot.setLabelGap(0.02);
            plot.setLabelGenerator(labels);

            this.chart = chart;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in creating vote chart: ", e);
        }
    }

    /**
     * @return the chart
     */
    public JFreeChart getChart() {
        return chart;
    }
}
