/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.voting;

/**
 *
 * @author Stefan
 */
public class DataEntry {
    private final Object value;
    private final MetaDataDataType dataType;
    
    public DataEntry(Object value, MetaDataDataType dataType) {
        this.value = value;
        this.dataType = dataType;
    }

    public Object getValue() {
        return value;
    }

    public MetaDataDataType getDataType() {
        return dataType;
    }
}
