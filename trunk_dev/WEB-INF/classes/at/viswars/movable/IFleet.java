/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.movable;

import at.viswars.fleet.Movable;
import at.viswars.ships.ShipData;
import at.viswars.model.FleetOrder;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface IFleet extends Movable {
    public int getId();
    public String getName();
    public int getUserId();
    public FleetOrder getFleetOrder();
    public ArrayList<ShipData> getShipList();
}
