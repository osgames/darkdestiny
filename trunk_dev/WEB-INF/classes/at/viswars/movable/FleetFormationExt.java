/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.movable;

import at.viswars.DebugBuffer;
import at.viswars.GameUtilities;
import at.viswars.ships.ShipData;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.FleetDetailDAO;
import at.viswars.dao.FleetFormationDAO;
import at.viswars.dao.FleetOrderDAO;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.fleet.FleetType;
import at.viswars.fleet.LoadingInformation;
import at.viswars.model.Chassis;
import at.viswars.model.FleetDetail;
import at.viswars.model.FleetFormation;
import at.viswars.model.FleetOrder;
import at.viswars.model.PlayerFleet;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipFleet;
import at.viswars.service.FleetService;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class FleetFormationExt implements IFleet {
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO)DAOFactory.get(PlayerFleetDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO)DAOFactory.get(FleetDetailDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO)DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO)DAOFactory.get(ShipDesignDAO.class);
    private static FleetOrderDAO foDAO = (FleetOrderDAO)DAOFactory.get(FleetOrderDAO.class);
    private static FleetFormationDAO ffDAO = (FleetFormationDAO)DAOFactory.get(FleetFormationDAO.class);
    
    private final FleetFormation base;
    private final FleetDetail moveData;
    
    private int ETA = 0;
    private boolean ableToCallBack = true;
    
    private Double speed = null;
    private LoadingInformation loadInfo = null;
    private ArrayList<ShipData> shipList = null;
    private at.viswars.model.FleetOrder fo = null;
    
    private final ArrayList<PlayerFleetExt> participatingFleets = new ArrayList<PlayerFleetExt>();
    
    public FleetFormationExt(int id) throws Exception {        
        base = ffDAO.findById(id);
        ArrayList<PlayerFleet> pfList = pfDAO.getParticipatingFleets(base);
        
        for (PlayerFleet pf : pfList) {
            participatingFleets.add(new PlayerFleetExt(pf.getId()));            
        }        
        
        // Check consistency of fleet formation
        FleetDetail fdLast = null;
        FleetDetail fdAct = null;
        PlayerFleetExt pfeLast = null;
                
        for (PlayerFleetExt pfe : participatingFleets) {
            fdAct = fdDAO.findByFleetId(pfe.getBase().getId());
            if (fdLast == null) {
                fdLast = fdAct;
            } else {
                if ((fdAct.getStartX().compareTo(fdLast.getStartX()) != 0) ||
                    (fdAct.getStartY().compareTo(fdLast.getStartY()) != 0) ||
                    (fdAct.getDestX().compareTo(fdLast.getDestX()) != 0) ||
                    (fdAct.getDestY().compareTo(fdLast.getDestY()) != 0) ||
                    (fdAct.getStartTime().compareTo(fdLast.getStartTime()) != 0) ||
                    (fdAct.getFlightTime().compareTo(fdLast.getFlightTime()) != 0)) {
                    throw new Exception("Inconsistent FleetFormation [FleetDetails] ("+id+")");
                }
            }
            
            if (pfeLast == null) {
                pfeLast = pfe;
            } else {
                if ((pfe.getBase().getPlanetId().compareTo(pfeLast.getBase().getPlanetId()) != 0) ||
                    (pfe.getBase().getSystemId().compareTo(pfeLast.getBase().getSystemId()) != 0) ||
                    (pfe.getBase().getStatus().compareTo(pfeLast.getBase().getStatus()) != 0)) {
                    throw new Exception("Inconsistent FleetFormation [PlayerFleet] ("+id+") >> " + 
                            pfe.getBase().getPlanetId() + "<>" + pfeLast.getBase().getPlanetId() + " " + 
                            pfe.getBase().getSystemId() + "<>" + pfeLast.getBase().getSystemId() + " " +
                            pfe.getBase().getStatus() + "<>" + pfeLast.getBase().getStatus());
                }
            }
        }
        
        moveData = fdLast;
        
        if (moveData != null) {
            getBase().setStatus(0);
        } else {
            getBase().setStatus(1);
        }
        
        getBase().setPlanetId(pfeLast.getBase().getPlanetId());
        getBase().setSystemId(pfeLast.getBase().getSystemId());
        
        if (moveData != null) {
            ETA = moveData.getFlightTime() - (GameUtilities.getCurrentTick2() - moveData.getStartTime());
            if ((moveData.getStartPlanet() == 0) && (moveData.getStartSystem() == 0)) {
                ableToCallBack = false;
            }
        } else {
            ableToCallBack = false;
        }
    }
      
    /*
    public LoadingInformation getLoadingInformation() {
        if (loadInfo == null) {
            loadInfo = new LoadingInformation(getBase());
        }
        
        return loadInfo;
    }
    */
    
    @Override
    public boolean isScanning() {
        return (getScanDuration() > 0);
    }
    
    @Override
    public int getScanDuration() {
        Integer duration =  FleetService.findRunningScan(base.getUserId(), base.getId());
        if(duration == null){
            return 0;
        }else{
            return duration;
        }
    }
    @Override
    public boolean canScanSystem() {
        return FleetService.canScanSystem(this);
    }

    @Override
    public boolean canScanPlanet() {
        return FleetService.canScanPlanet(this);
    }
    public AbsoluteCoordinate getAbsoluteCoordinate() {
        if (isMoving()) {
            int currTime = GameUtilities.getCurrentTick2();
            double distanceDone = (int)Math.floor(100d / moveData.getFlightTime() * (currTime - moveData.getStartTime()));
                        
            int diffX = moveData.getDestX() - moveData.getStartX();
            int diffY = moveData.getDestY() - moveData.getStartY();
            
            int currX = moveData.getStartX() + (int)Math.floor(diffX / 100d * distanceDone);
            int currY = moveData.getStartY() + (int)Math.floor(diffY / 100d * distanceDone);
            
            return new AbsoluteCoordinate(currX,currY);
        } else {
            RelativeCoordinate rc = new RelativeCoordinate(getBase().getSystemId(),getBase().getPlanetId());
            return rc.toAbsoluteCoordinate();
        }
    }
    
    public RelativeCoordinate getRelativeCoordinate() {
        if (isMoving()) {
            return null;
        }
        
        return new RelativeCoordinate(getBase().getSystemId(),getBase().getPlanetId());
    }
    
    public double getSpeed() {                
        if (speed == null) {
            double speedTmp = 999d;
            
            ArrayList<ShipFleet> sfList = new ArrayList<ShipFleet>();;
            for (PlayerFleetExt pf : getParticipatingFleets()) {
                sfList.addAll(sfDAO.findByFleetId(pf.getBase().getId()));
            }
            if (sfList.size() == 0) speedTmp = 0d;
            
            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());
                if (sd == null) {
                    DebugBuffer.error("SD was null for Formation " + this.getBase().getId() + " Shipfleet Entry " + sf.getId() + " and Design Entry " + sf.getDesignId());
                } else {
                    speedTmp = Math.min(speedTmp,sd.getHyperSpeed());
                }
            }
            
            speed = speedTmp;
        }

        return speed;
    }
    
    public boolean isMoving() {
        return (moveData != null);
    }
    
    public AbsoluteCoordinate getTargetAbsoluteCoordinate() {
        if (!isMoving()) return null;
        
        return new AbsoluteCoordinate(moveData.getDestX(),moveData.getDestY());
    }
    
    public RelativeCoordinate getTargetRelativeCoordinate() {
        if (!isMoving()) return null;
        
        return new RelativeCoordinate(moveData.getDestSystem(),moveData.getDestPlanet());
    }    
    
    public AbsoluteCoordinate getSourceAbsoluteCoordinate() {
        if (!isMoving()) return null;
        
        return new AbsoluteCoordinate(moveData.getStartX(),moveData.getStartY());
    }

    public RelativeCoordinate getSourceRelativeCoordinate() {
        if (!isMoving()) return null;
        
        return new RelativeCoordinate(moveData.getStartSystem(),moveData.getStartPlanet());
    }    
    
    public FleetFormation getBase() {
        return base;
    }

    public int getETA() {
        return ETA;
    }

    public boolean isAbleToCallBack() {
        return ableToCallBack;
    }

    @Override
    public ArrayList<ShipData> getShipList() {
        if (shipList == null) {
            shipList = new ArrayList<ShipData>();

            for (PlayerFleetExt pf : getParticipatingFleets()) {
                ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(pf.getId());
                for (ShipFleet sf : sfList) {
                    ShipDesign sd = sdDAO.findById(sf.getDesignId());
                    if (sd == null) {
                        DebugBuffer.error("SD was null for Formation " + this.getBase().getId() + " Shipfleet Entry " + sf.getId() + " and Design Entry " + sf.getDesignId());
                        continue;
                    }

                    ShipData sda = new ShipData();
                    sda.setId(sf.getId());
                    sda.setDesignId(sd.getId());
                    sda.setDesignName(sd.getName());
                    sda.setFleetId(base.getId());
                    sda.setFleetName(base.getName());
                    sda.setCount(sf.getCount());
                    sda.setChassisSize(sd.getChassis());
                    sda.setLoaded(false);

                    shipList.add(sda);
                }
            }
        }
        
        return shipList;
    }

    public FleetOrder getFleetOrder() {
        if (fo == null) {
            fo = foDAO.get(base.getId(),FleetType.FLEET_FORMATION);
        }
        
        return fo;
    }

    public ArrayList<PlayerFleetExt> getParticipatingFleets() {
        return participatingFleets;
    }

    public int getId() {
        return base.getId();
    }    
    
    public String getName() {
        return base.getName();
    }
    
    public int getUserId() {
        return base.getUserId();
    }

    public boolean canHyperJump() {
        throw new UnsupportedOperationException("please implement");
    }
    public boolean canFlyInterstellar() {
        for(ShipData sd : getShipList()){
            if(sd.getChassisSize() == Chassis.ID_FIGHTER ||
                    sd.getChassisSize() == Chassis.ID_CORVETTE){
                return false;
            }
        }
        return true;
    }
}
