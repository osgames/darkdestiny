/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.dao.AllianceBoardPermissionDAO;
import at.viswars.dao.AllianceDAO;
import at.viswars.dao.AllianceMemberDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.model.Alliance;
import at.viswars.model.AllianceBoardPermission;
import at.viswars.model.AllianceMember;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class AllianceUtilities {

    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static AllianceBoardPermissionDAO abpDAO = (AllianceBoardPermissionDAO) DAOFactory.get(AllianceBoardPermissionDAO.class);

    static boolean areAllied(int user1, int user2) {
        
        // Get alliance of user1
        AllianceMember am = amDAO.findByUserId(user1);
        AllianceMember am2 = amDAO.findByUserId(user2);

        if ((am == null) || (am2 == null)) {
            return false;
        }
        Alliance a = aDAO.findById(am.getAllianceId());
        Alliance a2 = aDAO.findById(am2.getAllianceId());

        if (a.getMasterAlliance().equals(a2.getMasterAlliance())) {
            return true;
        }
        return false;
    }

    public static ArrayList<Integer> getAllAlliedUsers(int user) {
        AllianceMember am = amDAO.findByUserId(user);
        if (am == null) return new ArrayList<Integer>();
        
        Alliance a = aDAO.findById(am.getAllianceId());
        
        Alliance aSearch = new Alliance();
        aSearch.setMasterAlliance(a.getMasterAlliance());
        ArrayList<Alliance> aList = aDAO.find(aSearch);
        
        ArrayList<Integer> userList = new ArrayList<Integer>();
        
        for (Alliance aEntry : aList) {
            AllianceMember amSearch = new AllianceMember();
            amSearch.setAllianceId(aEntry.getId());
            ArrayList<AllianceMember> amList = amDAO.find(amSearch);
            for (AllianceMember amEntry : amList) {
                userList.add(amEntry.getUserId());
            }
        }        
        
        return userList;
    }

    public static boolean hasAlliance(int userId) {
        if(amDAO.findByUserId(userId) != null){
            return true;
        }else{
            return false;
        }
    }

    public static boolean isTrial(int userId) {
        AllianceMember am = amDAO.findByUserId(userId);
        if(am == null){
            return false;
        }else if(am.getIsTrial()){
            return true;
        }else{
            return false;
        }
    }
    
    public static boolean userIsInAllyOrHasBoards(int userId) {
        AllianceMember am = amDAO.findByUserId(userId);
        if (am != null) return true;
        
        Iterable<AllianceBoardPermission> abpIt = abpDAO.findByUserId(userId);
        for (AllianceBoardPermission abp : abpIt) {
            return true;
        }
        
        return false;
    }
    
    public static Integer getDefaultBoard(int userId) {
        // If user is in alliance return 0 as default board
        AllianceMember am = amDAO.findByUserId(userId);
        if (am != null) return -1;
        
        Iterable<AllianceBoardPermission> abpIt = abpDAO.findByUserId(userId);
        for (AllianceBoardPermission abp : abpIt) {
            return abp.getAllianceBoardId();
        }        
        
        return null;
    }
}
