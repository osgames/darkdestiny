/*
 * HangarUtilities.java
 *
 * Created on 13. November 2006, 18:59
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.ships.ShipData;
import at.viswars.enumeration.EAttribute;
import java.util.*;
import at.viswars.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.admin.module.AttributeTree;
import at.viswars.admin.module.ModuleAttributeResult;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DamagedShipsDAO;
import at.viswars.dao.HangarRelationDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.enumeration.EDamageLevel;
import at.viswars.enumeration.EDamagedShipRefType;
import at.viswars.hangar.ChassisSize;
import at.viswars.hangar.ShipTypeEntry;
import at.viswars.model.Chassis;
import at.viswars.model.DamagedShips;
import at.viswars.model.HangarRelation;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipFleet;
import at.viswars.ships.ShipStatusEntry;

public class HangarUtilities implements ChassisSize {

    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static HangarRelationDAO hrDAO = (HangarRelationDAO) DAOFactory.get(HangarRelationDAO.class);
    private static ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
  
    private HashMap<Float, HashMap<Integer, ArrayList<ShipTypeEntry>>> speedMap;
    private ArrayList<ShipTypeEntry> newFleetStructure;
    public final static boolean DEBUG = false;
    
    public HangarUtilities() {
        speedMap = new HashMap<Float, HashMap<Integer, ArrayList<ShipTypeEntry>>>();
    }
    
    public boolean initializeFleet(int fleetId) {
        if (DEBUG) {
            DebugBuffer.addLine(DebugLevel.TRACE, "initializeFleet - HangarUtilities: " + fleetId);
        }
        // Es sollte eine HashMap mit Schiffen sortiert nach Hyperraumgeschwindingkeit aufgebaut werden
        // Zus&auml;tzlich Listen mit Schiffen die andere Schiffe einladen k&ouml;nnen
        // Zuerst wird versucht Hangars mit langsamen Schiffen zu bef&uuml;llen
        try {
            List<ShipFleet> sfList = sfDAO.findByFleetId(fleetId);

            // Statement stmt = DbConnect.createStatement();
            // ResultSet rs = stmt.executeQuery("SELECT 
            /*
             * sf.designId, sf.count, sd.chassis, sd.hyperspeed, sd.smallHangar,
             * sd.mediumHangar, sd.largeHangar
             */            
            
            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());
                Chassis c = cDAO.findById(sd.getChassis());
                AttributeTree at = new AttributeTree(sd.getUserId(), sd.getDesignTime());
                
                ModuleAttributeResult marChassis = at.getAllAttributes(sd.getChassis(), c.getRelModuleId());

                // Speed Entry available
                if (speedMap.containsKey(sd.getHyperSpeed())) {
                    ShipTypeEntry ste = ShipTypeEntry.initializeShipTypeEntry(marChassis, c, sd, sf);
                    
                    HashMap<Integer, ArrayList<ShipTypeEntry>> sizeMap = speedMap.get(sd.getHyperSpeed());
                    
                    if (sizeMap.containsKey(ste.getSizeType())) {
                        ArrayList<ShipTypeEntry> detailArray = sizeMap.get(ste.getSizeType());
                        
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Adding ship (to an existing detail Entry) " + ste.getId());
                        }
                        detailArray.add(ste);                        
                    } else {
                        ArrayList<ShipTypeEntry> detailArray = new ArrayList<ShipTypeEntry>();
                        
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Adding ship " + ste.getId());
                        }
                        detailArray.add(ste);
                        
                        sizeMap.put(ste.getSizeType(), detailArray);
                    }
                } else {
                    HashMap<Integer, ArrayList<ShipTypeEntry>> sizeMap = new HashMap<Integer, ArrayList<ShipTypeEntry>>();
                    ShipTypeEntry ste = ShipTypeEntry.initializeShipTypeEntry(marChassis, c, sd, sf);
                    
                    ArrayList<ShipTypeEntry> detailArray = new ArrayList<ShipTypeEntry>();
                    
                    if (DEBUG) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "Adding ship " + ste.getId());
                    }
                    detailArray.add(ste);
                    
                    sizeMap.put(ste.getSizeType(), detailArray);                    
                    speedMap.put(ste.getHyperSpeed(), sizeMap);
                }
            }
        } catch (Exception e) {
            if (DEBUG) {
                DebugBuffer.addLine(DebugLevel.TRACE, "Error in initializeFleet - HangarUtilities: " + e);
            }
        }

        // Sortierung durchf&uuml;hren
        
        TreeMap<Float, TreeMap<Integer, ArrayList<ShipTypeEntry>>> tmSpeed = new TreeMap<Float, TreeMap<Integer, ArrayList<ShipTypeEntry>>>();        
        for (Map.Entry<Float, HashMap<Integer, ArrayList<ShipTypeEntry>>> me : speedMap.entrySet()) {
            
            float key = me.getKey();
            HashMap<Integer, ArrayList<ShipTypeEntry>> sizeMap = me.getValue();            
            
            TreeMap<Integer, ArrayList<ShipTypeEntry>> tmSize = new TreeMap<Integer, ArrayList<ShipTypeEntry>>();
            
            for (Map.Entry<Integer, ArrayList<ShipTypeEntry>> me2 : sizeMap.entrySet()) {
                
                int key2 = me2.getKey();
                ArrayList<ShipTypeEntry> ste = me2.getValue();
                
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "Adding to size treeMap");
                }
                tmSize.put(key2, ste);
            }
            
            if (DEBUG) {
                DebugBuffer.addLine(DebugLevel.TRACE, "Adding to speed treeMap");
            }
            tmSpeed.put(key, tmSize);
        }

        // ArrayList mit neuer Flottenstruktur
        newFleetStructure = new ArrayList<ShipTypeEntry>();

        // Sortiere Listen durchgehen und beladen
        // !!! Hier sollten alle Eintr&auml;ge der TreeMap genau 1x durchlaufen werden !!!!      
        // PROBLEM: Was ist wenn Eintr&auml;ge in der Rekursion gel&ouml;scht werden
        boolean finished = false;
        
        if (tmSpeed.size() == 0) {
            return false;
        }
        
        while (!finished) {
            Float lastKey = tmSpeed.lastKey();
            
            if (DEBUG) {
                DebugBuffer.addLine(DebugLevel.TRACE, "Checking speed key " + lastKey + " map size = " + tmSpeed.size());
            }
            
            TreeMap<Integer, ArrayList<ShipTypeEntry>> tmSize = tmSpeed.get(lastKey);            
            
            Integer lastKeySize = (Integer) tmSize.lastKey();
            
            if (DEBUG) {
                DebugBuffer.addLine(DebugLevel.TRACE, "Checking size key " + lastKeySize + " map size = " + tmSize.size());
            }
            
            ArrayList<ShipTypeEntry> steArray = tmSize.get(lastKeySize);

            // Falls der aktuelle Schiffstyp keine Hangarkapazit&auml;ten hat, normal in Flottenzusammensetzung aufnehmen
            while (steArray.size() > 0) {
                ShipTypeEntry ste = steArray.get(0);
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "CHECKING DESIGN " + ste.getId() + " TOTAL SIZE=" + steArray.size());
                }
                
                if (ste.shipTypeDoesNotContainHangar()) {
                    if (DEBUG) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "ADDING DESIGN " + ste.getId());
                    }
                    getNewFleetStructure().add(ste);
                    steArray.remove(ste);
                    if (steArray.isEmpty()) {
                        tmSize.remove(lastKeySize);
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Removing " + lastKeySize + " from size Map, remaining size = " + tmSize.size());
                        }
                        if (tmSize.size() == 0) {                            
                            tmSpeed.remove(lastKey);
                            if (DEBUG) {
                                DebugBuffer.addLine(DebugLevel.TRACE, "Removing " + lastKey + " from speed Map, remaining size = " + tmSpeed.size());
                            }
                        }
                        continue;
                    }
                    continue;
                }
                
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "ADDING DESIGN " + ste.getId());
                }
                getNewFleetStructure().add(ste);                
                ste.fillHangar(tmSpeed);
                steArray.remove(ste);
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "Removing ship " + ste.getId() + " from Array, remaining size = " + steArray.size());
                }
            }

            // Eintrag aus TreeMap entfernen             
            tmSize.remove(lastKeySize);
            if (DEBUG) {
                DebugBuffer.addLine(DebugLevel.TRACE, "Removing " + lastKeySize + " from size Map, remaining size = " + tmSize.size());
            }
            if (tmSize.size() == 0) {                
                tmSpeed.remove(lastKey);
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "Removing " + lastKey + " from speed Map, remaining size = " + tmSpeed.size());
                }
            }
            if (tmSpeed.size() == 0) {
                finished = true;
            }
            
        }
        
        return true;
    }
    
  


  
    public static void cleanTreeMaps(TreeMap<Float, TreeMap<Integer, ArrayList<ShipTypeEntry>>> speedMap, TreeMap<Integer, ArrayList<ShipTypeEntry>> sizeMap, ShipTypeEntry ste) {
        sizeMap.get(ste.getSizeType()).remove(ste);
        if (DEBUG) {
            DebugBuffer.addLine(DebugLevel.TRACE, "Cleaned up ship array entry " + ste.getId() + " - Remaining = " + sizeMap.get(ste.getSizeType()).size());
        }
        if (sizeMap.get(ste.getSizeType()).isEmpty()) {
            sizeMap.remove(ste.getSizeType());            
            if (DEBUG) {
                DebugBuffer.addLine(DebugLevel.TRACE, "Cleaned up sizeMap Entry " + ste.getSizeType() + " - Remaining = " + sizeMap.size());
            }
            if (sizeMap.isEmpty()) {
                speedMap.remove(ste.getHyperSpeed());
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "Cleaned up speedMap " + ste.getHyperSpeed());
                }
            }            
        }        
    }

    
    public static ShipTypeEntry getShipWithBestHangar(ArrayList<ShipTypeEntry> shipList) {
        ShipTypeEntry ste = shipList.get(0);        
        
        int highestCap = 0;
        
        for (int i = 0; i < shipList.size(); i++) {
            ShipTypeEntry steTmp = shipList.get(i);
            
            int tmpCap = steTmp.getHangarSizeCumulated();
            if (tmpCap > highestCap) {
                highestCap = tmpCap;
                ste = shipList.get(i);
            }
        }
        
        return ste;
    }
    
    public ArrayList<ShipTypeEntry> getNestedHangarStructure() {
        ArrayList<ShipTypeEntry> nestedOutputList = new ArrayList<ShipTypeEntry>();
        
        rekSearchHangar(newFleetStructure, nestedOutputList, 0);
        
        return nestedOutputList;
    }
    
    public static ArrayList<ShipTypeEntry> getNestedHangarStructure(int fleetId, int designId) {
        ArrayList<ShipTypeEntry> nestedOutputList = new ArrayList<ShipTypeEntry>();
        
        try {
            ShipFleet sf = sfDAO.getByFleetDesign(fleetId, designId);

            // Statement stmt = DbConnect.createStatement();
            // ResultSet rs = stmt.executeQuery("SELECT designId, count FROM shipfleet WHERE fleetId="+fleetId+" AND designId="+designId);
            if (sf != null) {
                ShipTypeEntry tmpEntry = new ShipTypeEntry();
                tmpEntry.setId(sf.getDesignId());
                tmpEntry.setNestedLevel(0);
                tmpEntry.setCount(sf.getCount());                
                
                nestedOutputList.add(tmpEntry);
                
                rekSearchHangar(nestedOutputList, 1, fleetId, tmpEntry);
            }
            
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getNestedHangarStructure: ", e);
        }
        
        return nestedOutputList;
    }    
    
    public static ArrayList<ShipTypeEntry> getNestedHangarStructure(int fleetId) {
        ArrayList<ShipTypeEntry> nestedOutputList = new ArrayList<ShipTypeEntry>();
        
        try {
            ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(fleetId);

            // Statement stmt = DbConnect.createStatement();
            // ResultSet rs = stmt.executeQuery("SELECT designId, count FROM shipfleet WHERE fleetId="+fleetId);
            for (ShipFleet sf : sfList) {
                ShipTypeEntry tmpEntry = new ShipTypeEntry();
                tmpEntry.setId(sf.getDesignId());
                tmpEntry.setNestedLevel(0);
                tmpEntry.setCount(sf.getCount());                
                
                nestedOutputList.add(tmpEntry);
                
                rekSearchHangar(nestedOutputList, 1, fleetId, tmpEntry);
            }            
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getNestedHangarStructure: ", e);
        }
        
        return nestedOutputList;
    }
    
    public static void rekSearchHangar(ArrayList<ShipTypeEntry> nestedOutputList, int level, int fleetId, ShipTypeEntry lastSTE) {
        try {
            // Statement stmt = DbConnect.createStatement();
            // ResultSet rs = null;
            ArrayList<HangarRelation> hrList;
            
            if (level == 1) {
                hrList = hrDAO.findByFleetAndRelToDesignInternal(fleetId, lastSTE.getId());
                // rs = stmt.executeQuery("SELECT designId, count FROM hangarrelation WHERE fleetId="+fleetId+" AND relationToDesign="+lastSTE.getId());
            } else {
                hrList = hrDAO.findByFleetAndRelToDesign(fleetId, lastSTE.getId());
                // rs = stmt.executeQuery("SELECT designId, count FROM hangarrelation WHERE fleetId="+fleetId+" AND relationToDesign="+lastSTE.getId()+" AND relationToSF=1");
            }
            
            for (HangarRelation hr : hrList) {
                ShipTypeEntry tmpEntry = new ShipTypeEntry();
                tmpEntry.setId(hr.getDesignId());
                tmpEntry.setHangarEntryId(hr.getId());
                tmpEntry.setNestedLevel(level);
                tmpEntry.setCount(hr.getCount());
                
                nestedOutputList.add(tmpEntry);
                
                rekSearchHangar(nestedOutputList, level + 1, fleetId, tmpEntry);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Exception in rekSearchHangar= ", e);
        }
    }
    
    public void rekSearchHangar(List<ShipTypeEntry> levelStructure, List<ShipTypeEntry> nestedOutputList, int level) {
        for (int i = 0; i < levelStructure.size(); i++) {
            ShipTypeEntry tmpEntry = levelStructure.get(i);
            tmpEntry.setNestedLevel(level);
            nestedOutputList.add(tmpEntry);
            
            if (tmpEntry.getLargeHangarLoaded() != null) {
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "CHECK LARGE HANGAR LIST (LEVEL: " + level + ")");
                }
                rekSearchHangar(tmpEntry.getLargeHangarLoaded(), nestedOutputList, level + 1);
            }
            if (tmpEntry.getMediumHangarLoaded() != null) {
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "CHECK MEDIUM HANGAR LIST (LEVEL: " + level + ")");
                }
                rekSearchHangar(tmpEntry.getMediumHangarLoaded(), nestedOutputList, level + 1);
            }
            if (tmpEntry.getSmallHangarLoaded() != null) {
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "CHECK SMALL HANGAR LIST (LEVEL: " + level + ")");
                }
                rekSearchHangar(tmpEntry.getSmallHangarLoaded(), nestedOutputList, level + 1);
            }
        }
    }
    
    public void reconstructUnloadedFleetInDB(int fleetId) {
        try {
            // Statement stmt = DbConnect.createStatement();
            // Statement stmt2 = DbConnect.createStatement();
            // Statement stmt3 = DbConnect.createStatement();
            // ResultSet rs = stmt.executeQuery("SELECT * FROM hangarrelation WHERE fleetId="+fleetId);
            ArrayList<HangarRelation> hrList = hrDAO.findByFleetId(fleetId);
            for (HangarRelation hr : hrList) {
                Logger.getLogger().write("Check RELATION " + hr.getId());
                ShipFleet sf = sfDAO.getByFleetDesign(fleetId, hr.getDesignId());
                
                if (sf == null) {
                    Logger.getLogger().write("Shipfleet not available!");
                    ShipFleet sfNew = new ShipFleet();
                    sfNew.setFleetId(fleetId);
                    sfNew.setDesignId(hr.getDesignId());
                    sfNew.setCount(hr.getCount());
                    sfNew = sfDAO.add(sfNew);
                    
                    ArrayList<DamagedShips> dsList = dsDAO.findByHangarId(hr.getId());
                    for (DamagedShips ds : dsList) {
                        Logger.getLogger().write("Transferring damage entry");
                        dsDAO.remove(ds);
                        ds.setShipFleetId(sfNew.getId());
                        ds.setRefTable(EDamagedShipRefType.SHIPFLEET);                        
                        dsDAO.add(ds);                        
                    }                    
                } else {
                    Logger.getLogger().write("Shipfleet available!");
                    sf.setCount(sf.getCount() + hr.getCount());
                    sfDAO.update(sf);
                    
                    ArrayList<DamagedShips> dsList = dsDAO.findByHangarId(hr.getId());
                    for (DamagedShips ds : dsList) {
                        DamagedShips sfDs = dsDAO.getByIdAndDamage(sf.getId(), ds.getDamageLevel());
                        if (sfDs == null) {
                            Logger.getLogger().write("No Damage Entries found!");
                            dsDAO.remove(ds);
                            ds.setShipFleetId(sf.getId());
                            ds.setRefTable(EDamagedShipRefType.SHIPFLEET);
                            dsDAO.add(ds);
                        } else {
                            Logger.getLogger().write("Damage Entries found!");
                            dsDAO.remove(ds);
                            sfDs.setCount(sfDs.getCount() + ds.getCount());
                            dsDAO.update(sfDs);
                        }
                    }                    
                }
                
                hrDAO.remove(hr);
            }

            // Delete all hangarrelation entries            
            // stmt.execute("DELETE FROM hangarrelation WHERE fleetId="+fleetId);
        } catch (Exception e) {
            if (DEBUG) {
                DebugBuffer.writeStackTrace("Error while unloading all Ships = ", e);
            }
        }
    }
    
    public void reconstructFleetInDB(int fleetId, int firsttransfer) {
        ArrayList<ShipTypeEntry> newStructure = getNestedHangarStructure();
        boolean heavyDamageFirst = (firsttransfer == 1);
        
        try {
            // Delete shipFleet entries 
            // Statement stmt = DbConnect.createStatement();
            if (DEBUG) {
                DebugBuffer.addLine(DebugLevel.TRACE, "DELETE shipfleet entries");
            }
            // stmt.execute("DELETE FROM shipfleet WHERE fleetId="+fleetId);
            // QueryKeySet qks = new QueryKeySet();
            // qks.addKey(new QueryKey("fleetId",fleetId));
            // sfDAO.removeAll(qks);

            // Reconstruct from newStructure           
            int[] rekursionDesignArray = new int[5];
            
            for (int i = 0; i < newStructure.size(); i++) {
                ShipTypeEntry ste = newStructure.get(i);
                ShipFleet sf = sfDAO.getByFleetDesign(fleetId, ste.getId());
                
                if (sf == null) {
                    DebugBuffer.error("Shipfleet for was null for FleetId=" + fleetId + " and designId=" + ste.getId());
                }
                
                if (DEBUG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "Checking " + sf.getId());
                }
                
                if (ste.getNestedLevel() == 0) {
                    if (DEBUG) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "Insert into shipfleet entries");
                    }
                    // sfDAO.remove(sf);
                    // sfDAO.updateOrCreate(fleetId, ste.getId(), ste.getCount());
                    // stmt.execute("INSERT INTO shipfleet (fleetId, designId, count) VALUES ("+fleetId+","+ste.getId()+","+ste.getCount()+")");
                } else {
                    if (ste.getNestedLevel() > 1) {
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Insert into hangarrelation nested");
                        }
                        // stmt.execute("INSERT INTO hangarrelation (fleetId, designId, count, relationToDesign, relationToSF) VALUES ("+fleetId+","+ste.getId()+","+ste.getCount()+","+rekursionDesignArray[ste.getNestedLevel()-1]+",1)");
                        HangarRelation hr = new HangarRelation();
                        hr.setFleetId(fleetId);
                        hr.setDesignId(ste.getId());
                        hr.setCount(ste.getCount());
                        hr.setRelationToDesign(rekursionDesignArray[ste.getNestedLevel() - 1]);
                        hr.setRelationToSF(1);
                        hr = hrDAO.add(hr);

                        // Adjust damaged ships entries                                                
                        if (sf.getCount() == ste.getCount()) {
                            sfDAO.remove(sf);
                            
                            if (DEBUG) {
                                DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damaged Ships totally for id " + sf.getId());
                            }
                            for (DamagedShips ds : dsDAO.findById(sf.getId())) {
                                dsDAO.remove(ds);                                
                                
                                if (DEBUG) {
                                    DebugBuffer.addLine(DebugLevel.TRACE, "Transferring ...");
                                }
                                DamagedShips dsNew = new DamagedShips();
                                
                                dsNew.setDamageLevel(ds.getDamageLevel());
                                dsNew.setRefTable(EDamagedShipRefType.HANGARRELATION);
                                dsNew.setShipFleetId(hr.getId());
                                
                                DamagedShips existingEntry = (DamagedShips)dsDAO.get(dsNew);
                                if (existingEntry == null) {
                                    dsNew.setCount(ds.getCount());
                                    dsDAO.add(dsNew);
                                } else {
                                    existingEntry.setCount(existingEntry.getCount() + ds.getCount());
                                    dsDAO.update(existingEntry);    
                                }                                                
                            }
                        } else {
                            if (DEBUG) {
                                DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damaged Ships partially for id " + sf.getId());
                            }
                            transferDamagedShips(sf.getId(), hr, ste.getCount(), heavyDamageFirst);
                            sf.setCount(sf.getCount() - ste.getCount());
                            sfDAO.update(sf);                            
                        }
                    } else {
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Insert into hangarrelation");
                        }
                        // stmt.execute("INSERT INTO hangarrelation (fleetId, designId, count, relationToDesign, relationToSF) VALUES ("+fleetId+","+ste.getId()+","+ste.getCount()+","+rekursionDesignArray[ste.getNestedLevel()-1]+",0)");
                        HangarRelation hr = new HangarRelation();
                        hr.setFleetId(fleetId);
                        hr.setDesignId(ste.getId());
                        hr.setCount(ste.getCount());
                        hr.setRelationToDesign(rekursionDesignArray[ste.getNestedLevel() - 1]);
                        hr.setRelationToSF(0);
                        hr = hrDAO.add(hr);

                        // Adjust damaged ships entries                                                
                        if (sf.getCount() == ste.getCount()) {
                            sfDAO.remove(sf);
                            
                            if (DEBUG) {
                                DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damaged Ships totally for id " + sf.getId());
                            }
                            for (DamagedShips ds : dsDAO.findById(sf.getId())) {
                                dsDAO.remove(ds);
                                
                                if (DEBUG) {
                                    DebugBuffer.addLine(DebugLevel.TRACE, "Transferring ...");
                                }
                                DamagedShips dsNew = new DamagedShips();                                
                                
                                dsNew.setDamageLevel(ds.getDamageLevel());
                                dsNew.setRefTable(EDamagedShipRefType.HANGARRELATION);
                                dsNew.setShipFleetId(hr.getId());
                                
                                DamagedShips existingEntry = (DamagedShips)dsDAO.get(dsNew);
                                if (existingEntry == null) {
                                    dsNew.setCount(ds.getCount());
                                    dsDAO.add(dsNew);
                                } else {
                                    existingEntry.setCount(existingEntry.getCount() + ds.getCount());
                                    dsDAO.update(existingEntry);    
                                }                                
                                
                                // dsDAO.update(dsNew);
                            }
                        } else {
                            if (DEBUG) {
                                DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damaged Ships partially for id " + sf.getId());
                            }                            
                            transferDamagedShips(sf.getId(), hr, ste.getCount(), heavyDamageFirst);                            
                            sf.setCount(sf.getCount() - ste.getCount());
                            sfDAO.update(sf);                            
                        }                        
                    }                    
                }
                
                rekursionDesignArray[ste.getNestedLevel()] = ste.getId();
            }
        } catch (Exception e) {
            if (DEBUG) {
                DebugBuffer.writeStackTrace("Error while reconstrucing Fleet entries=", e);
            }
        }
    }
    
    private void transferDamagedShips(int shipFleetId, HangarRelation hr, int count, boolean damagedFirst) {
        ShipData sd = new ShipData(shipFleetId);        
        if (DEBUG) {
            DebugBuffer.addLine(DebugLevel.TRACE, "Processing ShipFleet Entry with id " + sd.getId() + " COUNT: " + sd.getCount());
        }        
        
        ArrayList<ShipStatusEntry> sseList = sd.getShipDataStatus();
        
        int countLeft = count;
        
        if (damagedFirst) {
            for (int i = (sseList.size() - 1); i >= 0; i--) {
                ShipStatusEntry sse = sseList.get(i);
                
                if (sse.getCount() == 0) {
                    continue;
                }
                
                if (sse.getStatus() == EDamageLevel.NODAMAGE) {
                    // Nothing to do
                    if (DEBUG) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "DamagedFirst already at non damaged -> Nothing to do");
                    }                    
                    return;
                } else {
                    if (sse.getCount() <= countLeft) {
                        // Convert this damageLevel completly to Hangar
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damage level " + sse.getStatus() + " completly");
                        }                        
                        DamagedShips ds = dsDAO.getByIdAndDamage(shipFleetId, sse.getStatus());
                        dsDAO.remove(ds);
                        
                        DamagedShips dsNew = new DamagedShips();
                        dsNew.setShipFleetId(hr.getId());
                        dsNew.setRefTable(EDamagedShipRefType.HANGARRELATION);
                        dsNew.setCount(ds.getCount());
                        dsNew.setDamageLevel(ds.getDamageLevel());
                        dsDAO.add(dsNew);
                        countLeft -= sse.getCount();
                    } else {
                        // Split up damageLevel
                        int sfLeft = sse.getCount() - countLeft;                        
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damage level " + sse.getStatus() + " partially (LEFT: " + sfLeft + ")");
                        }                        
                        
                        DamagedShips ds = dsDAO.getByIdAndDamage(shipFleetId, sse.getStatus());
                        ds.setCount(sfLeft);
                        dsDAO.update(ds);
                        
                        DamagedShips dsNew = new DamagedShips();
                        dsNew.setShipFleetId(hr.getId());
                        dsNew.setRefTable(EDamagedShipRefType.HANGARRELATION);
                        dsNew.setCount(countLeft);
                        dsNew.setDamageLevel(sse.getStatus());
                        dsDAO.add(dsNew);
                        
                        countLeft = 0;
                    }
                }
                
                if (countLeft == 0) {
                    return;
                }
            }            
        } else {
            for (int i = 0; i < sseList.size(); i++) {
                ShipStatusEntry sse = sseList.get(i);
                if (sse.getCount() == 0) {
                    continue;
                }
                
                if (sse.getStatus() == EDamageLevel.NODAMAGE) {
                    if (DEBUG) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "UndamagedFirst -> Reduce count left and continue (CountLeft=" + countLeft + " to transfer " + sse.getCount() + ")");
                    }                    
                    countLeft -= Math.min(count, sse.getCount());
                } else {
                    if (sse.getCount() <= countLeft) {
                        // Convert this damageLevel completly to Hangar
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damage level " + sse.getStatus() + " completly (CountLeft=" + countLeft + " to transfer " + sse.getCount() + ")");
                        }                        
                        DamagedShips ds = dsDAO.getByIdAndDamage(shipFleetId, sse.getStatus());
                        dsDAO.remove(ds);
                        
                        DamagedShips dsNew = new DamagedShips();
                        dsNew.setShipFleetId(hr.getId());
                        dsNew.setRefTable(EDamagedShipRefType.HANGARRELATION);
                        dsNew.setCount(ds.getCount());
                        dsNew.setDamageLevel(ds.getDamageLevel());
                        dsDAO.add(dsNew);
                        countLeft -= sse.getCount();
                    } else {
                        // Split up damageLevel
                        int sfLeft = sse.getCount() - countLeft;
                        if (DEBUG) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Transfer damage level " + sse.getStatus() + " partially (LEFT: " + sfLeft + ")");
                        }                        
                        
                        DamagedShips ds = dsDAO.getByIdAndDamage(shipFleetId, sse.getStatus());
                        ds.setCount(sfLeft);
                        dsDAO.update(ds);
                        
                        DamagedShips dsNew = new DamagedShips();
                        dsNew.setShipFleetId(hr.getId());
                        dsNew.setRefTable(EDamagedShipRefType.HANGARRELATION);
                        dsNew.setCount(countLeft);
                        dsNew.setDamageLevel(sse.getStatus());
                        dsDAO.add(dsNew);
                        
                        countLeft = 0;
                    }                    
                }                
                
                if (countLeft == 0) {
                    return;
                }
            }
        }
    }
    
    public static ArrayList<ShipStatusEntry> getShipDataStatus(ShipTypeEntry ste) {
        ArrayList<ShipStatusEntry> sdsList = new ArrayList<ShipStatusEntry>();
        
        ArrayList<DamagedShips> damageLevels = dsDAO.findByHangarId(ste.getHangarEntryId());
        
        if (damageLevels.size() == 0) {
            sdsList.add(new ShipStatusEntry(EDamageLevel.NODAMAGE, ste.getCount()));
            return sdsList;
        }
        
        int countLeft = ste.getCount();
        
        ShipStatusEntry noDamage = new ShipStatusEntry(EDamageLevel.NODAMAGE, 0);
        ShipStatusEntry minorDamage = new ShipStatusEntry(EDamageLevel.MINORDAMAGE, 0);
        ShipStatusEntry lightDamage = new ShipStatusEntry(EDamageLevel.LIGHTDAMAGE, 0);
        ShipStatusEntry mediumDamage = new ShipStatusEntry(EDamageLevel.MEDIUMDAMAGE, 0);
        ShipStatusEntry heavyDamage = new ShipStatusEntry(EDamageLevel.HEAVYDAMAGE, 0);
        
        for (DamagedShips ds : damageLevels) {
            if (ds.getDamageLevel() == EDamageLevel.NODAMAGE) {
                noDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            } else if (ds.getDamageLevel() == EDamageLevel.MINORDAMAGE) {
                minorDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            } else if (ds.getDamageLevel() == EDamageLevel.LIGHTDAMAGE) {
                lightDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            } else if (ds.getDamageLevel() == EDamageLevel.MEDIUMDAMAGE) {
                mediumDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            } else if (ds.getDamageLevel() == EDamageLevel.HEAVYDAMAGE) {
                heavyDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            }
        }
        
        noDamage.increaseCount(countLeft);
        
        sdsList.add(noDamage);
        sdsList.add(minorDamage);
        sdsList.add(lightDamage);
        sdsList.add(mediumDamage);
        sdsList.add(heavyDamage);
        
        return sdsList;
    }    
    
    public ArrayList<ShipTypeEntry> getNewFleetStructure() {
        return newFleetStructure;
    }
}