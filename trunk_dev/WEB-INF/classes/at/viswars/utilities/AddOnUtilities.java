/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.addon.AddOnProperties;
import at.viswars.addon.IAddOn;
import at.viswars.dao.AddOnDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.enumeration.EAddOnProperty;
import at.viswars.model.AddOn;
import java.io.File;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class AddOnUtilities {

    public static AddOnDAO addOnDAO = (AddOnDAO) DAOFactory.get(AddOnDAO.class);
    public static HashMap<String, IAddOn> addOns = new HashMap<String, IAddOn>();

    public static void loadAddOns() {
        
        DebugBuffer.addLine(DebugLevel.TRACE, "Loading AddOns");
        for (AddOn addOn : addOnDAO.findAllActive()) {
            try {
                URL u = AddOnUtilities.class.getResource("/db.properties");

                String loc;
                loc = u.toString().replace("classes/db.properties", "addon").substring(6);
                loc = loc.replace("root/", "");

                File file = new File(loc + "/" + addOn.getPackageName());
                Logger.getLogger().write("File : " + file.getAbsolutePath());
                URL jarfile = new URL("jar", "", "file:" + file.getAbsolutePath() + "!/");
                URLClassLoader cl = URLClassLoader.newInstance(new URL[]{jarfile}, AddOnUtilities.class.getClassLoader());


                Class loadedClass = cl.loadClass("at.viswars.addon." + addOn.getInterfaceName().trim());



                Constructor c = loadedClass.getConstructor();

                IAddOn loadedAddOn = (IAddOn) c.newInstance();
                loadedAddOn.print();
                AddOnProperties aop = loadedAddOn.getProperties();
                
                loadedAddOn.install();

                register(addOn.getName(), loadedAddOn);

            } catch (Exception e) {
                e.printStackTrace();
                DebugBuffer.addLine(DebugLevel.ERROR, "Could not load addon : name =" + addOn.getName());
            }
        }
    }

    public static void register(String addOnName, IAddOn addOnClass) {
        addOns.put(addOnName, addOnClass);

    }
    
    public static void loadPages(AddOnProperties properties){
        
        Logger.getLogger().write("Webpagefolder : " + properties.getProperty(EAddOnProperty.WEBPAGE_FOLDER));
        Logger.getLogger().write("Webpagefolder_ADMIN : " + properties.getProperty(EAddOnProperty.WEBPAGE_FOLDER_ADMIN));
        Logger.getLogger().write("Webpagefolder_ADDON : " + properties.getProperty(EAddOnProperty.WEBPAGE_FOLDER_ADDON));
    }
/*
    private static AddOnProperties defineAddOnProperties() {
        AddOnProperties aop = new AddOnProperties();

        URL u = AddOnUtilities.class.getResource("/db.properties");

        String loc;
        loc = u.toString().replace("/WEB-INF/classes/db.properties", "").substring(6);

        File file = new File(loc);
        aop.setProperty(EAddOnProperty.WEBPAGE_FOLDER, file.getAbsolutePath());

        loc = u.toString().replace("/WEB-INF/classes/db.properties", "/addon").substring(6);
        file = new File(loc);
        aop.setProperty(EAddOnProperty.WEBPAGE_FOLDER_ADDON, file.getAbsolutePath());
        loc = u.toString().replace("/WEB-INF/classes/db.properties", "/admin").substring(6);
        file = new File(loc);
        aop.setProperty(EAddOnProperty.WEBPAGE_FOLDER_ADMIN, file.getAbsolutePath());



        return aop;
    }*/
}
