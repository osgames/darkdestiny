/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.utilities;

import at.viswars.enumeration.EFilterComparator;

/**
 *
 * @author Aion
 */
public class FilterUtilities {

    public static EFilterComparator getComparator(String comparator){
        if(comparator.equals(">")){
            return EFilterComparator.BIGGER;
        }else if(comparator.equals("&#8805;")){
            return EFilterComparator.BIGGEREQUALS;
        }else if(comparator.equals("=")){
            return EFilterComparator.EQUALS;
        }else if(comparator.equals("&#8804;")){
            return EFilterComparator.SMALLEREQUALS;
        }else if(comparator.equals("<")){
            return EFilterComparator.SMALLER;
        }

        return null;
    }

    public static String getComparatorString(EFilterComparator comparator) {
        if(comparator == EFilterComparator.BIGGER){
            return ">";
        }else if(comparator == EFilterComparator.BIGGEREQUALS){
            return "&#8805;";
        }else if(comparator == EFilterComparator.EQUALS){
            return "=";
        }else if(comparator == EFilterComparator.SMALLEREQUALS){
            return "&#8804;";
        }else if(comparator == EFilterComparator.SMALLER){

            return "<";
        }
        return "Comparator not found";
    }
}
