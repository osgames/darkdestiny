/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.FormatUtilities;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.ships.ShipData;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DamagedShipsDAO;
import at.viswars.dao.FleetLoadingDAO;
import at.viswars.dao.GroundTroopDAO;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.RessourceDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.enumeration.EDamageLevel;
import at.viswars.enumeration.EDefenseType;
import at.viswars.fleet.LoadingInformation;
import at.viswars.model.Construction;
import at.viswars.model.DamagedShips;
import at.viswars.model.FleetLoading;
import at.viswars.model.GroundTroop;
import at.viswars.model.Model;
import at.viswars.model.PlanetDefense;
import at.viswars.model.PlayerFleet;
import at.viswars.model.Ressource;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipFleet;
import at.viswars.model.UserData;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.result.BaseResult;
import at.viswars.service.FleetService;
import at.viswars.ships.ShipRepairCost;
import at.viswars.ships.ShipScrapCost;
import at.viswars.ships.ShipStatusEntry;
import at.viswars.ships.ShipStorage;
import at.viswars.ships.ShipUpgradeCost;
import at.viswars.view.ShipModifyEntry;
import at.viswars.view.ShipModifyView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ShipUtilities {

    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);

    public static boolean canBeScrapped(Model ship, int userId, int fleetId, int count) {
        ShipDesign sd = (ShipDesign)ship;
        
        if (sd.getUserId() != userId) {
            return false;
        }
        
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);
        boolean found = false;
        
        for (ShipData sData : pfe.getShipList()) {
            if (sData.getDesignId() == sd.getId()) {
                if (sData.getCount() >= count) {
                    found = true;
                }
            }
        }
        
        if (!found) return false;
        
        UserData ud = udDAO.findByUserId(userId);        
        ShipDesignExt sde = new ShipDesignExt(sd.getId());
        ShipScrapCost ssc = sde.getShipScrapCost(count);
        
        if (ud.getCredits() < ssc.getRess(Ressource.CREDITS)) {
            return false;
        }
        
        return true;
    }
    
    public static BaseResult canBeRepaired(int userId, int shipFleetId, HashMap<EDamageLevel,Integer> toRepair) {
        BaseResult result = null;

        Logger.getLogger().write("SFID="+shipFleetId);
        ShipFleet sfEntry = sfDAO.getById(shipFleetId);
        PlayerFleet pf = pfDAO.findById(sfEntry.getFleetId());
        PlayerFleetExt pfe = new PlayerFleetExt(pf.getId());
        
        if (pf.getUserId() != userId) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 1 ("+userId+"<>"+pf.getId()+")");
            return new BaseResult("Ung&uuml;ltiger Benutzer",true);
        }
        
        ShipModifyView smv = getModifiableShips(pf.getId());
        boolean found = false;
        
        for (ShipModifyEntry sme : smv.getShipList()) {
            if (sme.getDesignId() == sfEntry.getDesignId()) {
                found = true;
                break;                
            }
        }
        
        if (!found) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 2");
            return new BaseResult("Schiff nicht gefunden",true);
        }        
        
        ArrayList<DamagedShips> damagedShips = dsDAO.findById(shipFleetId);
        
        for (Map.Entry<EDamageLevel,Integer> dEntry : toRepair.entrySet()) {
            found = false;
            for (DamagedShips ds : damagedShips) {
                if (ds.getDamageLevel() == dEntry.getKey()) {
                    if (ds.getCount() >= dEntry.getValue()) {
                        found = true;
                        break;
                    }
                }
            }
            
            if (!found) {
                DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 3 (Ship not available for repair)");
                return new BaseResult("Schiff nicht gefunden",true);
            }
        }
        
        ShipDesignExt sde = new ShipDesignExt(sfEntry.getDesignId());
        ShipDesign sd = (ShipDesign)sde.getBase();
        
        ShipRepairCost src = new ShipRepairCost(sde);        
        RessourcesEntry re = src.getTotalCosts(toRepair);        
        
        MutableRessourcesEntry mre = new MutableRessourcesEntry(userId,pfe.getRelativeCoordinate().getPlanetId());
        mre.subtractRess(re, 1);        
        
        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getQty() < 0) {
                DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 4 With ress " + rae.getRessId() + " >> "+ rae.getQty());
                Ressource r = rDAO.findRessourceById(rae.getRessId());
                return new BaseResult(FormatUtilities.getFormattedNumber(Math.abs(rae.getQty())) + " zuwenig " + ML.getMLStr(r.getName(), userId),true);
            }
        }        
        
        return new BaseResult("",false);
    }
    
    public static boolean canBeUpgraded(Model from, Model to, int userId, int fleetId, int count) {
        ShipDesign SDFrom = (ShipDesign)from;
        ShipDesign SDTo = (ShipDesign)to;
               
        if (SDFrom.getUserId() != userId) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 1 ("+userId+"<>"+SDFrom.getUserId()+")");
            return false;
        }
        
        if (SDFrom.getUserId() != SDTo.getUserId()) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 2");
            return false;
        }
        
        ShipModifyView smv = getModifiableShips(fleetId);
        boolean found = false;
        
        for (ShipModifyEntry sme : smv.getShipList()) {
            if (sme.getDesignId() == SDFrom.getId()) {
                if ((sme.getCount() - sme.getDamagedCount()) >= count) {
                    found = true;
                    break;
                }
            }
        }
        
        if (!found) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 3");
            return false;
        }
        
        ShipDesignExt SDEFrom = new ShipDesignExt(SDFrom.getId());
        ShipDesignExt SDETo = new ShipDesignExt(SDTo.getId());
        
        ShipUpgradeCost suc = SDEFrom.getShipUpgradeCost(SDETo, count);
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);
        
        MutableRessourcesEntry mre = new MutableRessourcesEntry(userId,pfe.getRelativeCoordinate().getPlanetId());
        RessourcesEntry re = new RessourcesEntry(suc.getRess());
        mre.subtractRess(re,1d);
        
        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getQty() < 0) {
                DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 4 With ress " + rae.getRessId() + " >> "+ rae.getQty());
                return false;
            }
        }
        
        return true;
    }

    public static ShipModifyView getModifiableShips(int fleetId) {
        return getModifiableShipsInternal(fleetId,-1);
    }

    public static ShipModifyView getModifiableShips(int fleetId, int designId) {
        return getModifiableShipsInternal(fleetId,designId);
    }

    private static ShipModifyView getModifiableShipsInternal(int fleetId, int designId) {
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);
        int planetId = pfe.getRelativeCoordinate().getPlanetId();

        boolean hasOrbDock = (pcDAO.findBy(planetId, Construction.ID_ORBITAL_SHIPYARD) != null);
        boolean hasDock = (pcDAO.findBy(planetId, Construction.ID_PLANETARYSHIPYARD) != null);
        boolean hasModule = (pcDAO.findBy(planetId, Construction.ID_MODULEFACTORY) != null);
        boolean canUseShipyard = FleetService.canUseShipyard(pfe.getUserId(), pfe.getRelativeCoordinate().getPlanetId());

        ShipModifyView smv = new ShipModifyView(fleetId);
        ArrayList<ShipData> sdList = pfe.getShipList();
        
        if (designId != -1) {
            for (Iterator<ShipData> sdIt = sdList.iterator();sdIt.hasNext();) {
                ShipData sd = sdIt.next();
                if (sd.getDesignId() != designId) sdIt.remove();
            }
        }

        ArrayList<FleetLoading> flList = flDAO.findByFleetId(fleetId);
        boolean checkForCap = !flList.isEmpty();

        boolean hasTroops = false;
        boolean hasResources = false;

        if (checkForCap) {
            for (FleetLoading fl : flList) {
                if ((fl.getLoadType() == 2) || (fl.getLoadType() == 4)) {
                    hasTroops = true;
                }
                if (fl.getLoadType() == 1) {
                    hasResources = true;
                }
            }
        }

        for (ShipData sd : sdList) {
            ShipModifyEntry sme = new ShipModifyEntry();
            sme.setShipData(sd);

            boolean canBeModified = true;
            boolean canBeScrapped = true;

            if (!hasModule) {
                canBeModified = false;
            }

            if (sd.getChassisSize() > 5) {
                if (!hasOrbDock) {
                    canBeModified = false;
                    canBeScrapped = false;
                }
            } else if (!hasDock) {
                canBeModified = false;
                canBeScrapped = false;
            }
            //If other players shipyard
            if(!canUseShipyard){
                canBeModified = false;
                canBeScrapped = false;
            }

            sme.setCanBeModified(canBeModified);
            sme.setCanBeScrapped(canBeScrapped);
            
            for (ShipStatusEntry sse : sd.getShipDataStatus()) {
                if ((sse.getCount() > 0) && (sse.getStatus() != EDamageLevel.NODAMAGE)) {
                    sme.setDamagedCount(sme.getDamagedCount() + sse.getCount());
                    sme.setCanBeRepaired(true);
                    continue;
                }
            }

            if ((sme.getCount() - sme.getDamagedCount()) <= 0) {
                sme.setCanBeModified(false);
            }

            // Check if ships have loading
            if (checkForCap) {
                ShipDesign sdTmp = sdDAO.findById(sd.getDesignId());
                if ((sdTmp.getRessSpace() > 0) && hasResources) {
                    sme.setBlockedByLoading(true);
                    sme.setCanBeModified(false);
                    sme.setCanBeRepaired(false);
                    sme.setCanBeScrapped(false);
                }
                if ((sdTmp.getTroopSpace() > 0) && hasTroops) {
                    sme.setBlockedByLoading(true);
                    sme.setCanBeModified(false);
                    sme.setCanBeRepaired(false);
                    sme.setCanBeScrapped(false);
                }
            }

            smv.addEntry(sme);
        }

        return smv;
    }

    public static ShipModifyEntry getShipModifyEntry(int designId, int planetId) {
        ShipDesign sd = sdDAO.findById(designId);
        return getShipModifyEntry(sd, planetId);
    }

    public static ShipModifyEntry getShipModifyEntry(ShipDesign sd, int planetId) {
        boolean hasOrbDock = (pcDAO.findBy(planetId, Construction.ID_ORBITAL_SHIPYARD) != null);
        boolean hasDock = (pcDAO.findBy(planetId, Construction.ID_PLANETARYSHIPYARD) != null);
        boolean hasModule = (pcDAO.findBy(planetId, Construction.ID_MODULEFACTORY) != null);

        ShipModifyEntry sme = new ShipModifyEntry();
        ArrayList<ShipData> shipList = new ArrayList<ShipData>();

        PlanetDefense pd = pdDAO.findBy(planetId, sd.getId(), EDefenseType.SPACESTATION);

        ShipData sda = new ShipData();
        sda.setDesignId(sd.getId());
        sda.setDesignName(sd.getName());
        sda.setCount(pd.getCount());
        sda.setChassisSize(sd.getChassis());
        sda.setLoaded(false);

        shipList.add(sda);

        sme.setShipData(sda);

        boolean canBeModified = true;
        boolean canBeScrapped = true;

        if (!hasModule) {
            canBeModified = false;
        }

        if (sda.getChassisSize() > 5) {
            if (!hasOrbDock) {
                canBeModified = false;
                canBeScrapped = false;
            }
        } else if (!hasDock) {
            canBeModified = false;
            canBeScrapped = false;
        }

        sme.setCanBeModified(canBeModified);
        sme.setCanBeScrapped(canBeScrapped);
        return sme;
    }

    public static void checkExcessFleetLoading(int fleetId) {
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);

        LoadingInformation li = pfe.getLoadingInformation();
        if ((li.getCurrLoadedTroops() + li.getCurrLoadedPopulation()) > li.getTroopSpace()) {
            // Rescale Troops
            ArrayList<ShipStorage> ssList = li.getLoadedStorage();

            for (Iterator<ShipStorage> ssIt = ssList.iterator();ssIt.hasNext();) {
                ShipStorage ss = ssIt.next();
                if (!((ss.getLoadtype() == ShipStorage.LOADTYPE_GROUNDTROOP) || (ss.getLoadtype() == ShipStorage.LOADTYPE_POPULATION))) {
                    ssIt.remove();
                }
            }

            // Now that we have cleaned the list of unnecessary stuff
            // lets come to the fun part and destroy random stuff
            int stuffToDestroy = (li.getCurrLoadedTroops() + li.getCurrLoadedPopulation()) - li.getTroopSpace();

            while (stuffToDestroy > 0) {
                // Pick a random element
                ShipStorage ss = ssList.get((int)Math.floor(ssList.size() * Math.random()));

                int loadingSize = 1;
                if (ss.getLoadtype() == ShipStorage.LOADTYPE_GROUNDTROOP) { // Get the size of this
                    GroundTroop gt = gtDAO.findById(ss.getId());
                    loadingSize = gt.getSize();
                }

                int maxToDestroy = Math.min((int)Math.ceil(ss.getCount() / loadingSize),(int)Math.ceil(stuffToDestroy / loadingSize));
                if (maxToDestroy < (int)Math.ceil(ss.getCount() / loadingSize)) {
                    maxToDestroy += 1;
                }

                if ((maxToDestroy < 1) && (ss.getCount() > 0)) maxToDestroy = 1;

                int destroyCountTmp = (int)Math.ceil(Math.random() * maxToDestroy);
                int destroyCount = Math.min(maxToDestroy,destroyCountTmp);

                stuffToDestroy -= destroyCount * loadingSize;
                ss.setCount(ss.getCount() - destroyCount);

                Logger.getLogger().write("Destroy " + destroyCount + " " + ss.getName());
            }

            // Store changes
            for (ShipStorage ss : ssList) {
                flDAO.setNewLoadingAmount(fleetId, ss.getLoadtype(), ss.getId(), ss.getCount());
            }
        }

        pfe = new PlayerFleetExt(fleetId);
        li = pfe.getLoadingInformation();
        if (li.getCurrLoadedRess() > li.getRessSpace()) {
            // Rescale Ress
            ArrayList<ShipStorage> ssList = li.getLoadedStorage();

            for (Iterator<ShipStorage> ssIt = ssList.iterator();ssIt.hasNext();) {
                ShipStorage ss = ssIt.next();
                if (ss.getLoadtype() != ShipStorage.LOADTYPE_RESSOURCE) {
                    ssIt.remove();
                }
            }

            int stuffToDestroy = li.getCurrLoadedRess() - li.getRessSpace();

            while (stuffToDestroy > 0) {
                // Pick a random element
                ShipStorage ss = ssList.get((int)Math.floor(ssList.size() * Math.random()));

                int maxToDestroy = Math.min(ss.getCount(),stuffToDestroy);
                int destroyCountTmp = (int)Math.ceil(Math.random() * maxToDestroy);
                int destroyCount = Math.min(maxToDestroy,destroyCountTmp);

                stuffToDestroy -= destroyCount;
                ss.setCount(ss.getCount() - destroyCount);

                Logger.getLogger().write("Destroy " + destroyCount + " " + ss.getName());
            }

            // Store changes
            for (ShipStorage ss : ssList) {
                flDAO.setNewLoadingAmount(fleetId, ss.getLoadtype(), ss.getId(), ss.getCount());
            }     
        }
    }
}
