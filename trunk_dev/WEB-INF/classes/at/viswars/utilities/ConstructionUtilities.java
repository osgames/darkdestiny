/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.ML;
import at.viswars.buildable.ConstructionExt;
import at.viswars.construction.*;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.model.PlanetRessource;
import at.viswars.dao.ActionDAO;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.ConstructionDAO;
import at.viswars.dao.ConstructionRestrictionDAO;
import at.viswars.dao.ConstructionUpgradeDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.GroundTroopDAO;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlanetRessourceDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.PlayerResearchDAO;
import at.viswars.dao.ProductionDAO;
import at.viswars.dao.ProductionOrderDAO;
import at.viswars.dao.ResearchDAO;
import at.viswars.dao.RessourceCostDAO;
import at.viswars.dao.RessourceDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.TechRelationDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.enumeration.EProductionOrderType;
import at.viswars.enumeration.ERessourceCostType;
import at.viswars.enumeration.ERestrictionReason;
import at.viswars.model.GroundTroop;
import at.viswars.model.Action;
import at.viswars.model.Construction;
import at.viswars.model.ConstructionRestriction;
import at.viswars.model.ConstructionUpgrade;
import at.viswars.model.Model;
import at.viswars.model.Planet;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Production;
import at.viswars.model.ProductionOrder;
import at.viswars.model.Research;
import at.viswars.model.Ressource;
import at.viswars.model.RessourceCost;
import at.viswars.model.ShipDesign;
import at.viswars.model.TechRelation;
import at.viswars.model.UserData;
import at.viswars.result.AbortableResult;
import at.viswars.result.BuildableResult;
import at.viswars.result.DestructableResult;
import at.viswars.orderhandling.result.OrderResult;
import at.viswars.result.SurfaceResult;
import at.viswars.service.OverviewService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import at.viswars.orderhandling.orders.AbortOrder;
import at.viswars.orderhandling.orders.ConstructionScrapAbortOrder;
import at.viswars.orderhandling.orders.ScrapOrder;
import at.viswars.result.*;
import at.viswars.service.ConstructionService;
import at.viswars.service.PlanetService;
import at.viswars.service.ResearchService;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ConstructionUtilities {

    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private static TechRelationDAO trDAO = (TechRelationDAO) DAOFactory.get(TechRelationDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static ProductionDAO prodDAO = (ProductionDAO) DAOFactory.get(ProductionDAO.class);
    private static PlayerResearchDAO pResDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static ResearchDAO rDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    private static ConstructionRestrictionDAO crDAO = (ConstructionRestrictionDAO) DAOFactory.get(ConstructionRestrictionDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static ChassisDAO chDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private static RessourceDAO ressDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);
    private static ConstructionUpgradeDAO cuDAO = (ConstructionUpgradeDAO) DAOFactory.get(ConstructionUpgradeDAO.class);

    public static LinkedList<InConstructionData> getInConstructionData(int planetId) {                
        LinkedList<InConstructionData> inCons = new LinkedList<InConstructionData>();

        InConstructionResult icd = ConstructionService.findRunnigConstructions(planetId);
        
        /*
        ArrayList<Action> rCons = aDAO.findRunningConstructions(planetId);

        for (Action a : rCons) {
            ProductionOrder po = poDAO.findByAction(a);

            ConstructCount tmpC = new ConstructCount(cDAO.findById(a.getConstructionId()), a.getType(), po.getType(), a.getNumber());
            inCons.add(new InConstructionData(tmpC));
        }        
        */

        for (InConstructionEntry ice : icd.getEntries()) {
            ConstructCount tmpC = new ConstructCount(cDAO.findById(ice.getAction().getConstructionId()), ice.getAction().getType(), ice.getOrder().getType(), ice.getAction().getNumber());
            inCons.add(new InConstructionData(tmpC));
        }
        
        return inCons;
    }
 

    public static HashMap<Integer, LinkedList<InConstructionData>> getInConstructionDataGlobal(int userId) {
        HashMap<Integer, LinkedList<InConstructionData>> inConsGlobal = new HashMap<Integer, LinkedList<InConstructionData>>();

        ArrayList<Action> rCons = aDAO.findRunningConstructionsGlobal(userId);

        for (Action a : rCons) {
            ProductionOrder po = poDAO.findByAction(a);
            ConstructCount tmpC = new ConstructCount(cDAO.findById(a.getConstructionId()), a.getType(), po.getType(), a.getNumber());

            if (inConsGlobal.containsKey(a.getPlanetId())) {
                inConsGlobal.get(a.getPlanetId()).add(new InConstructionData(tmpC));
            } else {
                LinkedList<InConstructionData> inCons = new LinkedList<InConstructionData>();
                inCons.add(new InConstructionData(tmpC));
                inConsGlobal.put(a.getPlanetId(), inCons);
            }
        }

        return inConsGlobal;
    }

    public static OrderResult buildConstruction(int userId, int planetId, int cId, int count) {
        if (count <= 0) {
            return new OrderResult(false, "Anzahl muss gr��er 0 sein!");
        }
        Construction c = cDAO.findById(cId);
        if (c == null) {
            return new OrderResult(false, "Ung�ltiges Geb�ude!");
        }

        BuildableResult br = canBeBuilt(c, userId, planetId, count);
        if (br.isBuildable()) {
        } else {
            return new OrderResult(false, br.getMissingRequirements());
        }

        return null;
    }

    public static OrderResult buildGroundtroop(int userId, int planetId, int uId, int count) {
        if (count <= 0) {
            return new OrderResult(false, "Anzahl muss gr��er 0 sein!");
        }
        GroundTroop gt = gtDAO.findById(uId);
        if (gt == null) {
            return new OrderResult(false, "Ung�ltiger Einheitentyp!");
        }

        BuildableResult br = canBeBuilt(gt, userId, planetId, count);
        if (br.isBuildable()) {
        } else {
            return new OrderResult(false, br.getMissingRequirements());
        }

        return null;
    }

    public static OrderResult buildShip(int userId, int planetId, int sId, int count) {
        if (count <= 0) {
            return new OrderResult(false, "Anzahl muss gr��er 0 sein!");
        }
        ShipDesign sd = sdDAO.findById(sId);
        if (sd == null) {
            return new OrderResult(false, "Ung�ltiges Schiffsdesign!");
        }
        if (sd.getUserId() != userId) {
            return new OrderResult(false, "Ung�ltiges Schiffsdesign!");
        }

        BuildableResult br = canBeBuilt(sd, userId, planetId, count);
        if (br.isBuildable()) {
        } else {
            return new OrderResult(false, br.getMissingRequirements());
        }

        return null;
    }

    @Deprecated
    private static void createProductionOrder(Model m, int planetId, int unitId, int count) throws Exception {
        EProductionOrderType prodOrderType = null;
        EActionType actionType = null;

        if (m instanceof Construction) {
            prodOrderType = EProductionOrderType.C_BUILD;
            actionType = EActionType.BUILDING;
        } else if (m instanceof ShipDesign) {
            prodOrderType = EProductionOrderType.PRODUCE;
            actionType = EActionType.SHIP;
        } else if (m instanceof GroundTroop) {
            prodOrderType = EProductionOrderType.GROUNDTROOPS;
            actionType = EActionType.GROUNDTROOPS;
        }

        /*
        ProductionOrder po = new ProductionOrder();
        po.setIndNeed(number * c.getBuildtime());
        po.setModuleNeed(0);
        po.setOrbDockNeed(0);
        po.setDockNeed(0);
        po.setKasNeed(0);
        po.setIndProc(0);
        po.setModuleProc(0);
        po.setOrbDockProc(0);
        po.setDockProc(0);
        po.setKasProc(0);
        po.setPlanetId(planetId);
        po.setPriority(1);
        po.setType(ProductionOrder.PROD_C_ORDER_BUILD);
        po = poDAO.add(po);
        
        Action a = new Action();
        a.setActionType(Action.TYPE_BUILDING);
        a.setUserId(userId);
        a.setNumber(number);
        a.setBuildingId(c.getId());
        a.setPlanetId(planetId);
        a.setSystemId(p.getSystemId());
        Logger.getLogger().write("poId " + po.getId());
        a.setTimeFinished(po.getId());
        a.setOnHold(false);
        a = aDAO.add(a);   
         */
    }

    public static boolean isBeingImproved(int constructionId, int userId, int planetId) {
        Action a = aDAO.findByConstructionId(userId, constructionId, planetId);
        if (a != null) {
            return true;
        } else {
            return false;
        }

    }

    public static BuildableResult canBeImproved(Construction c, int userId, int planetId) {
        ArrayList<String> errorList = new ArrayList<String>();
        BuildableResult br = new BuildableResult(true, 1, errorList);

        BuildableResult brImproveable = isImproveable(c, userId, planetId);
        //Do resourcecheck
        if (br.isBuildable()) {

            ConstructionImproveResult cir = new ConstructionImproveResult(c.getId(), planetId);
            ConstructionImproveCost cic = cir.getConstructionImproveCost();

            ArrayList<RessAmountEntry> ressCost = cic.getRessArray();

            UserData ud = udDAO.findByUserId(userId);
            if (ressCost.size() > 0) {
                for (RessAmountEntry rae : ressCost) {
                    if (rae.getRessId() == Ressource.CREDITS) {
                        if (ud.getCredits() < (rae.getQty())) {
                            errorList.add("Not enough Credits");
                            return new BuildableResult(false, 0, errorList);
                        }
                    } else {
                        PlanetRessource pr = prDAO.findBy(planetId, rae.getRessId(), EPlanetRessourceType.INSTORAGE);

                        if (pr == null) {
                            errorList.add("Not enough Resources");
                            return new BuildableResult(false, 0, errorList);
                        } else if (pr.getQty() < (rae.getQty())) {
                            errorList.add("Not enough Resources");
                            return new BuildableResult(false, 0, errorList);
                        }
                    }
                }
            }
            return br;
        } else {
            return brImproveable;
        }
    }

    public static BuildableResult isImproveable(Construction c, int userId, int planetId) {
        ArrayList<String> errorList = new ArrayList<String>();
        PlanetConstruction pc = pcDAO.findBy(planetId, c.getId());
        BuildableResult br = new BuildableResult(true, 1, errorList);
        if (pc == null) {
            errorList.add("Construction does not exist on this planet");
            return new BuildableResult(false, 0, errorList);
        }
        if (isBeingImproved(c.getId(), userId, planetId)) {

            errorList.add("Already Upgrading");
            return new BuildableResult(false, 0, errorList);
        }
        if (!c.getLevelable()) {
            errorList.add("Not levelable construction");
            return new BuildableResult(false, 0, errorList);
        }

        return br;
    }

    public static boolean isDeactivated(Construction c, int userId, int planetId) {
        boolean isDeactivated = false;
        try {
            ConstructionRestriction cr =
                    cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, c.getId());

            if (cr != null) {
                Class rc = Class.forName("at.viswars.construction.restriction." + cr.getRestrictionClass());
                ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                if (cir.isDeactivated(ppDAO.findByPlanetId(planetId))) {
                    isDeactivated = true;
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on Construction Restriction: ", e);
        }
        return isDeactivated;
    }

    //Find out if a construction can be upgraded
    public static ConstructionUpgradeResult canBeUpgraded(Construction c, int userId, int planetId) {

        ArrayList<String> errorList = new ArrayList<String>();
        ArrayList<ConstructionUpgrade> cus = cuDAO.findBySourceId(c.getId());
        int targetConstructionId = c.getId();
        //No follow-up construction
        if (cus == null) {
            errorList.add("There is no upgrade option for this buildings");
            return new ConstructionUpgradeResult(false, c.getId(), 0, planetId, 0, errorList);
        } else {
            boolean canBeUpgraded = false;
            //Loop if its possible to upgrade to one of the following targetcons
            for (ConstructionUpgrade cu : cus) {
                Construction targetCon = cDAO.findById(cu.getTargetConstruction());
                canBeUpgraded = canBeUpgraded || canBeUpgraded(c, userId, planetId, 1, targetCon).isBuildable();
                if (canBeUpgraded) {
                    targetConstructionId = cu.getTargetConstruction();
                    break;
                }
            }
            return new ConstructionUpgradeResult(canBeUpgraded, c.getId(), targetConstructionId, planetId, 1, errorList);

        }


    }

    public static ConstructionUpgradeResult canBeUpgraded(Construction c, int userId, int planetId, int count, Construction cNew) {
        ArrayList<String> errorList = new ArrayList<String>();
        ConstructionUpgradeResult cur = null;

        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        UserData ud = udDAO.findByUserId(userId);

        if ((pp == null) || (ud == null)) {
            ArrayList<String> error = new ArrayList<String>();
            error.add("Invalid input data");
            return new ConstructionUpgradeResult(false, c.getId(), cNew.getId(), planetId, 0, errorList);
        }


        ArrayList<TechRelation> neededTech = new ArrayList<TechRelation>();
        neededTech = trDAO.findByConstructionId(cNew.getId());


        boolean isFinished = true;
        boolean possibleByTech = true;
        boolean possibleByLockedTech = true;
        boolean possibleByCredits = true;
        boolean possibleByRess = true;
        boolean possibleByUnique = true;
        boolean possibleByPlanet = true;
        boolean possibleByRestriction = true;
        boolean possibleByPopActionPoints = true;
        boolean possibleBySurface = true;
        boolean isOutdated = false;


        // Check Techtree
        if (neededTech.size() > 0) {
            for (TechRelation tr : neededTech) {
                if (tr.getReqResearchId() != 0) {
                    if (pResDAO.findBy(userId, tr.getReqResearchId()) == null) {
                        if (tr.getReqResearchId() == 999) {
                            possibleByLockedTech = false;
                        }
                        possibleByTech = false;
                        try {
                            errorList.add(ML.getMLStr("construction_msg_missingtechnology", userId) + " " + ML.getMLStr(rDAO.findById(tr.getReqResearchId()).getName(), userId));

                        } catch (Exception e) {
                            errorList.add("F�r diese Forschung ist zwar eine Vorforschung in Techrelation vorhanden die Forschung selbst existiert aber nicht");
                        }
                    }
                }
                if (tr.getReqConstructionId() != 0) {
                    if (pcDAO.findBy(planetId, tr.getReqConstructionId()) == null) {
                        possibleByTech = false;
                        errorList.add(ML.getMLStr("construction_msg_missingconstruction", userId) + ML.getMLStr(cDAO.findById(tr.getReqConstructionId()).getName(), userId));
                    }
                }
            }
        }

        int maxCount = Integer.MAX_VALUE;

        // Check Ressources
        ArrayList<RessAmountEntry> ressCost = new ConstructionUpgradeCost(new ConstructionExt(c.getId()), new ConstructionExt(cNew.getId()), planetId, count).getRessArray();


        if (ressCost.size() > 0) {
            for (RessAmountEntry rae : ressCost) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    ud = udDAO.findByUserId(userId);
                    if (ud.getCredits() < (rae.getQty() * count)) {
                        possibleByCredits = false;
                    } else {
                        if (rae.getQty() > 0) {
                            maxCount = Math.min(maxCount, (int) Math.floor(ud.getCredits() / rae.getQty()));
                        }
                    }
                } else {
                    PlanetRessource pr = prDAO.findBy(planetId, rae.getRessId(), EPlanetRessourceType.INSTORAGE);

                    if (pr == null) {
                        maxCount = 0;
                        possibleByRess = false;
                    } else if (pr.getQty() < (rae.getQty() * count)) {
                        maxCount = 0;
                        possibleByRess = false;
                    } else {
                        if (rae.getQty() == 0) {
                            maxCount = 0;
                        } else {
                            maxCount = Math.min(maxCount, (int) Math.floor(pr.getQty() / rae.getQty()));
                        }
                    }
                }
            }
        }
        if (!possibleByRess || !possibleByCredits) {
            errorList.add(ML.getMLStr("construction_err_notenoughressources", userId));
        }

        // Check Unique  
        pp = ppDAO.findByPlanetId(planetId);

        boolean mineableRessExists = true;

        //If Construction
        if (cNew != null) {
            LinkedList<InConstructionData> inCons = getInConstructionData(planetId);
            boolean isBeeingBuiltLocal = false;
            boolean isBeeingBuiltSystem = false;
            boolean isBeeingBuiltGlobal = false;

            int inConsUsedSpace = 0;
            int inConsUsedWorkers = 0;

            for (InConstructionData icd : inCons) {
                inConsUsedSpace += icd.getConstructionData().getConstruct().getSurface() * icd.getConstructionData().getCount();
                inConsUsedWorkers += icd.getConstructionData().getConstruct().getWorkers() * icd.getConstructionData().getCount();
            }

            if (cNew.isUniquePerPlanet()) {
                for (InConstructionData icd : inCons) {
                    if (icd.getConstructionData().getConstruct().getId() == cNew.getId()) {
                        isBeeingBuiltLocal = true;
                        break;
                    }
                }
            }

            Planet p = pDAO.findById(planetId);

            if (cNew.isUniquePerSystem()) {
                ArrayList<Planet> pList = pDAO.findBySystemId(p.getSystemId());
                for (Planet pTmp : pList) {
                    PlayerPlanet ppTmp = ppDAO.findByPlanetId(pTmp.getId());

                    if (ppTmp != null) {
                        inCons = getInConstructionData(ppTmp.getPlanetId());
                        for (InConstructionData icd : inCons) {
                            if (icd.getConstructionData().getConstruct().getId() == cNew.getId()) {
                                isBeeingBuiltSystem = true;
                                break;
                            }
                        }
                    }

                    if (isBeeingBuiltSystem) {
                        break;
                    }
                }
            }

            if (cNew.isUniquePerPlanet()) {
                if ((pcDAO.findBy(planetId, cNew.getId()) != null) || isBeeingBuiltLocal) {
                    possibleByUnique = false;
                } else {
                    maxCount = 1;
                }
            }

            if (cNew.isUniquePerSystem()) {
                ArrayList<Planet> pList = pDAO.findBySystemId(p.getSystemId());
                for (Planet pTmp : pList) {
                    if ((pcDAO.findBy(pTmp.getId(), cNew.getId()) != null) || isBeeingBuiltSystem) {
                        possibleByUnique = false;
                    } else {
                        maxCount = 1;
                    }
                }
            }
            //Check if there is the rawmaterial on the planet the construciton needs
            for (Production prod : prodDAO.findProductionForConstructionId(cNew.getId())) {
                if (ressDAO.findRessourceById(prod.getRessourceId()).getMineable()) {
                    if (prDAO.findBy(planetId, prod.getRessourceId(), EPlanetRessourceType.PLANET) == null) {
                        mineableRessExists = false;
                        break;
                    }
                    //Abfrage auf Howalgoniummine... Wenn zwar Howalgoniu vorhanden Scanner aber nicht erforscht =>
                    //Konstruktion im Men� nicht sichtbar
                    if ((cNew.getId() == Construction.ID_HOWALGONIUMMINE || cNew.getId() == Construction.ID_ORBITAL_HOWALGONIUMMINE)
                            && !ResearchService.isResearched(userId, Research.HOWALGONIUMSCANNER)) {
                        mineableRessExists = false;
                        break;
                    }
                    //Abfrage auf Ynkeloniummine... Wenn zwar Ynkelonium vorhanden Ynkelonium aber nicht erforscht =>
                    //Konstruktion im Men� nicht sichtbar
                    if ((cNew.getId() == Construction.ID_ORBITAL_YNKELONIUMMINE)
                            && !ResearchService.isResearched(userId, Research.YNKELONIUM)) {
                        mineableRessExists = false;
                        break;
                    }
                }
            }

            if (!possibleByUnique) {
                errorList.add(ML.getMLStr("construction_err_alreadybuilt", userId));
            }

            if (!cNew.getTypplanet().contains(p.getLandType())) {
                possibleByPlanet = false;
                errorList.add("Geb&auml;ude kann auf diesem Planet nicht gebaut werden");
                // errorList.add(ML.getMLStr("construction_err_notenoughworkers", userId));
            }


            //TODO calculate difference
            SurfaceResult sr = p.getSurface();
            if ((sr.getFreeSurface() - inConsUsedSpace) < cNew.getSurface()) {
                possibleBySurface = false;
                errorList.add(ML.getMLStr("construction_err_notenoughspace", userId));
            } else {
                if (cNew.getSurface() > 0) {
                    maxCount = Math.min(maxCount, (int) Math.floor((sr.getFreeSurface() - inConsUsedSpace) / cNew.getSurface()));
                }
            }
            long popActionUsed = 0;
            for (PlanetConstruction pc : pcDAO.findByPlanetId(planetId)) {
                popActionUsed += pc.getNumber() * cDAO.findById(pc.getConstructionId()).getWorkers();
            }

            if ((pp.getPopActionPoints() - inConsUsedWorkers - popActionUsed) < cNew.getWorkers()) {
                possibleByPopActionPoints = false;
                errorList.add(ML.getMLStr("construction_err_notenoughworkers", userId));
            } else {
                if (cNew.getWorkers() > 0) {
                    maxCount = Math.min(maxCount, (int) Math.floor((pp.getPopActionPoints() - popActionUsed - inConsUsedWorkers) / cNew.getWorkers()));
                }
            }
        }

        //Restrict by existing buildings (There cant be more upgraded than existing)        
        maxCount = Math.min(maxCount, pcDAO.findBy(planetId, c.getId()).getNumber());

        try {
            ConstructionRestriction cr = null;
            if (cNew != null) {
                cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, cNew.getId());
            }

            if (cr != null) {
                Class rc = Class.forName("at.viswars.construction.restriction." + cr.getRestrictionClass());
                ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                if (!cir.onConstructionCheck(pp)) {
                    possibleByRestriction = false;
                    for (Map.Entry<String, ERestrictionReason> entry : cir.getReasons().entrySet()) {
                        errorList.add(entry.getKey());
                        if (entry.getValue().equals(ERestrictionReason.OUTDATED)) {
                            isOutdated = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on Construction Restriction: ", e);
        }

        boolean buildable = possibleByTech && possibleByRess && possibleByCredits && mineableRessExists
                && possibleByUnique && possibleByPlanet && possibleByRestriction && possibleByPopActionPoints && possibleBySurface && possibleByLockedTech;

        cur = new ConstructionUpgradeResult(buildable, c.getId(), cNew.getId(), planetId, maxCount, errorList);
        cur.setDeniedByTech(!possibleByTech);
        cur.setDeniedByOutdated(isOutdated);
        cur.setDeniedByUnique(!possibleByUnique);
        cur.setDeniedByLandType(!possibleByPlanet);
        cur.setPossibleByPopActionPoints(possibleByPopActionPoints);
        cur.setDeniedBySurface(!possibleBySurface);
        cur.setDeniedByMineableRessource(!mineableRessExists);
        cur.setNotFinished(!isFinished);
        cur.setDeniedByLockedTech(!isFinished);
        return cur;
    }

    public static BuildableResult canBeBuilt(Model unit, int userId, int planetId, int count) {
        ArrayList<String> errorList = new ArrayList<String>();
        BuildableResult br = null;

        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        UserData ud = udDAO.findByUserId(userId);

        if ((pp == null) || (ud == null)) {
            ArrayList<String> error = new ArrayList<String>();
            error.add("Invalid input data");
            return new BuildableResult(false, 0, error);
        }

        Construction c = null;
        ShipDesign sd = null;
        GroundTroop gt = null;

        ArrayList<TechRelation> neededTech = new ArrayList<TechRelation>();
        if (unit instanceof Construction) {
            c = (Construction) unit;
            neededTech = trDAO.findByConstructionId(c.getId());
        } else if (unit instanceof ShipDesign) {
            sd = (ShipDesign) unit;
        } else if (unit instanceof GroundTroop) {
            gt = (GroundTroop) unit;
            neededTech = trDAO.findByGroundTroopId(gt.getId());
        }

        boolean isFinished = true;
        boolean possibleByTech = true;
        boolean possibleByLockedTech = true;
        boolean possibleByCredits = true;
        boolean possibleByRess = true;
        boolean possibleByUnique = true;
        boolean possibleByPlanet = true;
        boolean possibleByRestriction = true;
        boolean possibleByPopActionPoints = true;
        boolean possibleBySurface = true;
        boolean isOutdated = false;


        // Check Techtree
        if (neededTech.size() > 0) {
            for (TechRelation tr : neededTech) {
                if (tr.getReqResearchId() != 0) {
                    if (pResDAO.findBy(userId, tr.getReqResearchId()) == null) {
                        if (tr.getReqResearchId() == 999) {
                            possibleByLockedTech = true;
                        }
                        possibleByTech = false;
                        try {
                            errorList.add(ML.getMLStr("construction_msg_missingtechnology", userId) + " " + ML.getMLStr(rDAO.findById(tr.getReqResearchId()).getName(), userId));

                        } catch (Exception e) {
                            errorList.add("F�r diese Forschung ist zwar eine Vorforschung in Techrelation vorhanden die Forschung selbst existiert aber nicht");
                        }
                    }
                }
                if (tr.getReqConstructionId() != 0) {
                    if (pcDAO.findBy(planetId, tr.getReqConstructionId()) == null) {
                        possibleByTech = false;
                        errorList.add(ML.getMLStr("construction_msg_missingconstruction", userId) + ML.getMLStr(cDAO.findById(tr.getReqConstructionId()).getName(), userId));
                    }
                }
            }
        }

        int maxCount = Integer.MAX_VALUE;
        // DebugBuffer.warning("MAXCOUNT SET TO INFINITE");

        // Check Ressources
        ArrayList<RessourceCost> ressCost = new ArrayList<RessourceCost>();

        if (c != null) {
            ressCost = rcDAO.find(ERessourceCostType.CONSTRUCTION, c.getId());
        } else if (sd != null) {
            ressCost = rcDAO.find(ERessourceCostType.SHIP, sd.getId());
        } else {
            ressCost = rcDAO.find(ERessourceCostType.GROUNDTROOPS, gt.getId());
        }

        if (ressCost.size() > 0) {
            for (RessourceCost rc : ressCost) {
                if (rc.getRessId() == Ressource.CREDITS) {
                    ud = udDAO.findByUserId(userId);

                    if (ud.getCredits() < (rc.getQty() * count)) {
                        possibleByCredits = false;
                        maxCount = 0;
                        // DebugBuffer.warning("MAXCOUNT SET TO 0 DUE TO LOW CREDITS");
                    } else {
                        if (rc.getQty() > 0) {
                            maxCount = Math.min(maxCount, (int)Math.floor(ud.getCredits() / rc.getQty()));
                            // DebugBuffer.warning("MAXCOUNT SET TO "+maxCount+" DUE TO CREDITS");
                        }
                    }
                } else {
                    PlanetRessource pr = prDAO.findBy(planetId, rc.getRessId(), EPlanetRessourceType.INSTORAGE);

                    if (pr == null) {
                        maxCount = 0;
                        // DebugBuffer.warning("MAXCOUNT SET TO 0 DUE TO RESS IS NULL" + rc.getRessId());
                        possibleByRess = false;
                    } else if (pr.getQty() < (rc.getQty() * count)) {
                        maxCount = 0;
                        // DebugBuffer.warning("MAXCOUNT SET TO 0 DUE LOW RESS " + pr.getRessId());
                        possibleByRess = false;
                    } else {
                        if (rc.getQty() == 0) {
                            maxCount = 0;
                            // DebugBuffer.warning("MAXCOUNT SET TO 0 DUE TO SOME WEIRD IF STATEMENT " + rc.getRessId());
                        } else {
                            maxCount = Math.min(maxCount, (int) Math.floor(pr.getQty() / rc.getQty()));
                            // DebugBuffer.warning("MAXCOUNT SET TO "+maxCount+" DUE TO RESS " + pr.getRessId());
                        }
                    }
                }
            }
        }

        if (!possibleByRess || !possibleByCredits) {
            errorList.add(ML.getMLStr("construction_err_notenoughressources", userId));
        }

        // Check Unique  
        pp = ppDAO.findByPlanetId(planetId);

        boolean mineableRessExists = true;

        //If Groundtroop check if enough pps available
        if (gt != null && gt.getCrew() > 0) {
            // DebugBuffer.warning("MAXCOUNT PREV: " + maxCount + " MAXCOUNT FREEPOP: " + PlanetService.getFreePopulation(planetId));
            maxCount = Math.min(maxCount, (int) Math.min(PlanetService.getFreePopulation(planetId), (long) ((maxCount * gt.getCrew()))));
            // DebugBuffer.warning("MAXCOUNT SET TO " + maxCount + " DUE TO FREEPOP");
            errorList.add(ML.getMLStr("construction_err_notenoughworkers", userId));
        }

        //If Construction
        if (c != null) {
            LinkedList<InConstructionData> inCons = getInConstructionData(planetId);
            boolean isBeeingBuiltLocal = false;
            boolean isBeeingBuiltSystem = false;
            boolean isBeeingBuiltGlobal = false;

            int inConsUsedSpace = 0;
            int inConsUsedWorkers = 0;

            for (InConstructionData icd : inCons) {
                inConsUsedSpace += icd.getConstructionData().getConstruct().getSurface() * icd.getConstructionData().getCount();
                inConsUsedWorkers += icd.getConstructionData().getConstruct().getWorkers() * icd.getConstructionData().getCount();
            }

            if (c.isUniquePerPlanet()) {
                for (InConstructionData icd : inCons) {
                    if (icd.getConstructionData().getConstruct().getId() == c.getId()) {
                        isBeeingBuiltLocal = true;
                        break;
                    }
                }
            }

            Planet p = pDAO.findById(planetId);

            if (c.isUniquePerSystem()) {
                ArrayList<Planet> pList = pDAO.findBySystemId(p.getSystemId());
                for (Planet pTmp : pList) {
                    PlayerPlanet ppTmp = ppDAO.findByPlanetId(pTmp.getId());

                    if (ppTmp != null) {
                        inCons = getInConstructionData(ppTmp.getPlanetId());
                        for (InConstructionData icd : inCons) {
                            if (icd.getConstructionData().getConstruct().getId() == c.getId()) {
                                isBeeingBuiltSystem = true;
                                break;
                            }
                        }
                    }

                    if (isBeeingBuiltSystem) {
                        break;
                    }
                }
            }

            if (c.isUniquePerPlanet()) {
                if ((pcDAO.findBy(planetId, c.getId()) != null) || isBeeingBuiltLocal || (count > 1)) {
                    possibleByUnique = false;
                } else {
                    maxCount = 1;
                }
            }

            if (c.isUniquePerSystem()) {
                ArrayList<Planet> pList = pDAO.findBySystemId(p.getSystemId());
                for (Planet pTmp : pList) {
                    if ((pcDAO.findBy(pTmp.getId(), c.getId()) != null) || isBeeingBuiltSystem || (count > 1)) {
                        possibleByUnique = false;
                    } else {
                        maxCount = 1;
                    }
                }
            }
            //Check if there is the rawmaterial on the planet the construciton needs
            for (Production prod : prodDAO.findProductionForConstructionId(c.getId())) {
                if (ressDAO.findRessourceById(prod.getRessourceId()).getMineable()) {
                    if (prDAO.findBy(planetId, prod.getRessourceId(), EPlanetRessourceType.PLANET) == null) {
                        mineableRessExists = false;
                        break;
                    }
                    //Abfrage auf Howalgoniummine... Wenn zwar Howalgoniu vorhanden Scanner aber nicht erforscht =>
                    //Konstruktion im Men� nicht sichtbar
                    if ((c.getId() == Construction.ID_HOWALGONIUMMINE || c.getId() == Construction.ID_ORBITAL_HOWALGONIUMMINE)
                            && !ResearchService.isResearched(userId, Research.HOWALGONIUMSCANNER)) {
                        mineableRessExists = false;
                        break;
                    }
                    //Abfrage auf Ynkeloniummine... Wenn zwar Ynkelonium vorhanden Ynkelonium aber nicht erforscht =>
                    //Konstruktion im Men� nicht sichtbar
                    if ((c.getId() == Construction.ID_ORBITAL_YNKELONIUMMINE)
                            && !ResearchService.isResearched(userId, Research.YNKELONIUM)) {
                        mineableRessExists = false;
                        break;
                    }
                }
            }

            if (!possibleByUnique) {
                errorList.add(ML.getMLStr("construction_err_alreadybuilt", userId));
            }

            if (!c.getTypplanet().contains(p.getLandType())) {
                possibleByPlanet = false;
                errorList.add("Geb&auml;ude kann auf diesem Planet nicht gebaut werden");
                // errorList.add(ML.getMLStr("construction_err_notenoughworkers", userId));
            }

            SurfaceResult sr = p.getSurface();
            if ((sr.getFreeSurface() - inConsUsedSpace) < c.getSurface()) {
                possibleBySurface = false;
                errorList.add(ML.getMLStr("construction_err_notenoughspace", userId));
            } else {
                if (c.getSurface() > 0) {
                    maxCount = Math.min(maxCount, (int) Math.floor((sr.getFreeSurface() - inConsUsedSpace) / c.getSurface()));
                }
            }
            long popActionUsed = 0;
            for (PlanetConstruction pc : pcDAO.findByPlanetId(planetId)) {
                popActionUsed += pc.getNumber() * cDAO.findById(pc.getConstructionId()).getWorkers();
            }

            if (((pp.getPopActionPoints() - inConsUsedWorkers - popActionUsed) < c.getWorkers()) && (c.getWorkers() > 0)) {
                possibleByPopActionPoints = false;
                errorList.add(ML.getMLStr("construction_err_notenoughworkers", userId));
            } else {
                if (c.getWorkers() > 0) {
                    maxCount = Math.min(maxCount, (int) Math.floor((pp.getPopActionPoints() - popActionUsed - inConsUsedWorkers) / c.getWorkers()));
                }
            }
        }

        try {
            ConstructionRestriction cr = null;
            if (c != null) {
                cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, c.getId());
            }

            if (cr != null) {
                Class rc = Class.forName("at.viswars.construction.restriction." + cr.getRestrictionClass());
                ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                if (!cir.onConstructionCheck(pp)) {
                    possibleByRestriction = false;
                    for (Map.Entry<String, ERestrictionReason> entry : cir.getReasons().entrySet()) {
                        errorList.add(entry.getKey());
                        if (entry.getValue().equals(ERestrictionReason.OUTDATED)) {
                            isOutdated = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on Construction Restriction: ", e);
        }

        boolean buildable = possibleByTech && possibleByRess && possibleByCredits && mineableRessExists
                && possibleByUnique && possibleByPlanet && possibleByRestriction && possibleByPopActionPoints && possibleBySurface;

        if (sd != null) {
            buildable = buildable && sd.getBuildable();
            isFinished = sd.getBuildable();
        }

        br = new BuildableResult(buildable, maxCount, errorList);
        br.setDeniedByTech(!possibleByTech);
        br.setDeniedByOutdated(isOutdated);
        br.setDeniedByUnique(!possibleByUnique);
        br.setDeniedByLockedTech(!possibleByLockedTech);
        br.setDeniedByLandType(!possibleByPlanet);
        br.setPossibleByPopActionPoints(possibleByPopActionPoints);
        br.setDeniedBySurface(!possibleBySurface);
        br.setDeniedByMineableRessource(!mineableRessExists);
        br.setNotFinished(!isFinished);

        // DebugBuffer.warning(unit + ": "  + maxCount + " " + possibleByTech + " " + isOutdated + " " + possibleByUnique + " " + possibleByLockedTech + " " + possibleByPlanet + " " + possibleByPopActionPoints +
        //         " " + possibleBySurface + " " + mineableRessExists + " " + isFinished);

        return br;
    }

    public static AbortableResult canBeAborted(AbortOrder ao) {
        ArrayList<String> errors = new ArrayList<String>();
        
        if (ao instanceof ConstructionScrapAbortOrder) {
            ConstructionScrapAbortOrder csao = (ConstructionScrapAbortOrder)ao;
            
            ProductionOrder po = csao.getPo();
            Action a = aDAO.findByTimeFinished(po.getId(), EActionType.DECONSTRUCT);
            int count = csao.getCount();          
            
            // When aborting this order we have no ensure that there is enough space on the planet to build 
            // the count of this building 
            LinkedList<InConstructionData> inConsDataLst = getInConstructionData(csao.getPlanetId());
            Planet p = pDAO.findById(csao.getPlanetId());
            PlayerPlanet pp = ppDAO.findByPlanetId(csao.getPlanetId());
            
            if (pp.getUserId() != csao.getUserId()) {
                errors.add(ML.getMLStr("construction_err_notpossible", csao.getUserId()));
            }
            
            SurfaceResult sr = p.getSurface();
            int freeSurface = sr.getFreeSurface();
            
            for (InConstructionData inConsData : inConsDataLst) {
                freeSurface -= inConsData.getConstructionData().getConstruct().getSurface() * inConsData.getConstructionData().getCount();
            }            
            
            freeSurface -= ((Construction)csao.getUnit().getBase()).getSurface() * csao.getCount();
            if (freeSurface < 0) {
                errors.add(ML.getMLStr("construction_err_notenoughspace", csao.getUserId()));
            }                                    
            
            if ((csao.getCount() < 0) || (csao.getCount() > a.getNumber())) {
                errors.add(ML.getMLStr("construction_err_notenoughconstructionsexisting", csao.getUserId()));
            }       
            
            // Check for resources
            ConstructionExt ce = (ConstructionExt)csao.getUnit();
            ConstructionScrapAbortCost csac = ce.getScrapAbortCost(po.getId());
            
            boolean notEnoughRess = false;
            
            for (RessAmountEntry rae : csac.getRessArray()) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    if (rae.getQty() < 0) continue;
                    
                    UserData ud = udDAO.findByUserId(csao.getUserId());
                    if (ud.getCredits() < rae.getQty()) {
                        notEnoughRess = true;
                        break;
                    }
                } else {
                    PlanetRessource pr = prDAO.findBy(csao.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                    if (pr == null) {
                        notEnoughRess = true;
                        break;
                    } else {
                        if (pr.getQty() < rae.getQty()) {
                            notEnoughRess = true;
                            break;
                        }
                    }
                }
            }
            
            if (notEnoughRess) {
                errors.add(ML.getMLStr("construction_err_notenoughressources", csao.getUserId()));
            }
        }
        
        return new AbortableResult(errors.isEmpty(), errors);
    }
    
    public static ArrayList<Integer> getPossibleUpgradeConstructions(int constructionId) {
        ArrayList<Integer> constructionIds = new ArrayList<Integer>();
        for (ConstructionUpgrade cu : cuDAO.findBySourceId(constructionId)) {
            constructionIds.add(cu.getTargetConstruction());
        }
        return constructionIds;
    }

    public static DestructableResult canBeDestructed(ScrapOrder so) {
        Model unit = so.getUnit().getBase();

        Construction c = null;
        if (unit instanceof Construction) {
            c = (Construction) unit;
        }
        BuildingDeconstructionRessourceCost bdrc = OverviewService.findForDeconstruction(c.getId(), so.getPlanetId(), so.getUserId());

        MutableRessourcesEntry mre = new MutableRessourcesEntry(so.getUserId(), so.getPlanetId());
        mre.subtractRess(bdrc.getRessCost(), so.getCount());
        if (mre.getRess(Ressource.CREDITS) < 0) {
            return new DestructableResult(false, ML.getMLStr("construction_err_notenoughcredits", so.getUserId()));
        }


        return new DestructableResult(true, new ArrayList<String>());
    }
}
