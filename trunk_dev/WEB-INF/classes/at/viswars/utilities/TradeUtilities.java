/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.utilities;

import at.viswars.Logger.Logger;
import at.viswars.dao.AllianceDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PriceListDAO;
import at.viswars.dao.PriceListEntryDAO;
import at.viswars.dao.UserDAO;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.enumeration.TradeOfferType;
import at.viswars.model.PriceList;
import at.viswars.model.PriceListEntry;
import at.viswars.model.Ressource;
import at.viswars.service.RessourceService;
import at.viswars.trade.TradeGoodPriceList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class TradeUtilities {
    private static PriceListDAO plDAO = (PriceListDAO)DAOFactory.get(PriceListDAO.class);
    private static PriceListEntryDAO pleDAO = (PriceListEntryDAO)DAOFactory.get(PriceListEntryDAO.class);
    private static UserDAO uDAO = (UserDAO)DAOFactory.get(UserDAO.class);
    private static AllianceDAO aDAO = (AllianceDAO)DAOFactory.get(AllianceDAO.class);

    public static PriceList createTradePriceList(int userId, TradeOfferType tot, int refId) {
        ArrayList<PriceList> plList = plDAO.findByUserId(userId);

        for (PriceList pl : plList) {
            if (pl.getTradeOfferType() == tot) {
                if (pl.getTradeOfferType() == TradeOfferType.PUBLIC || pl.getTradeOfferType() == TradeOfferType.ALLIED) {
                    return null;
                } else {
                    if (pl.getTradeOfferType() == TradeOfferType.ALLY) {
                        if (pl.getRefId() == refId) {
                            return null;
                        }
                    } else if (pl.getTradeOfferType() == TradeOfferType.USER) {
                        if (pl.getRefId() == refId) {
                            return null;
                        }
                    }
                }
            }
        }

        if (tot == TradeOfferType.PUBLIC || tot == TradeOfferType.ALLIED) {
            refId = 0;
        }

        PriceList plNew = new PriceList();
        plNew.setUserId(userId);
        plNew.setTradeOfferType(tot);
        plNew.setRefId(refId);
        plNew = plDAO.add(plNew);

        for (Ressource r : RessourceService.getAllTransportableRessources()) {
            PriceListEntry ple = new PriceListEntry();
            ple.setPriceListId(plNew.getId());
            ple.setRessId(r.getId());
            ple.setPrice(0);
            pleDAO.add(ple);
        }

        return plNew;
    }

    public static ArrayList<TradeGoodPriceList> getPriceLists(int userId) {
        ArrayList<TradeGoodPriceList> tgplList = new ArrayList<TradeGoodPriceList>();

        for (PriceList pl : plDAO.findByUserId(userId)) {
            TradeGoodPriceList tgpl = new TradeGoodPriceList(pl.getUserId(),pl.getTradeOfferType(),pl.getRefId());
            tgpl.setId(pl.getId());
            tgplList.add(tgpl);
        }

        return tgplList;
    }

    public static TradeGoodPriceList getTradePriceList(int id) {
        Logger.getLogger().write("Get TradePrice");

        PriceList pl = plDAO.getById(id);
        if (pl != null) {
            TradeGoodPriceList tgpl = new TradeGoodPriceList(pl.getUserId(),pl.getTradeOfferType(),pl.getRefId());
            tgpl.setId(pl.getId());
            return tgpl;
        } else {
            return null;
        }
    }

    // PARAMS: TARGETGROUP: ALL, ALLIED, ALLIANCE, PLAYER & ID
    public static TradeGoodPriceList getTradePriceList(int userId, TradeOfferType tot, int refId) {
        Logger.getLogger().write("Get TradePrice");

        PriceList pl = new PriceList();
        pl.setUserId(userId);
        pl.setTradeOfferType(tot);
        pl.setRefId(refId);
        ArrayList<PriceList> plList = plDAO.find(pl);

        if (plList.isEmpty()) {
            return null;
        } else {
            TradeGoodPriceList tgpl = new TradeGoodPriceList(userId,tot,refId);
            tgpl.setId(plList.get(0).getId());
            return tgpl;
        }
    }

    public static void deletePriceList(int listId) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();

        try {
            th.startTransaction();

            PriceList pl = plDAO.getById(listId);
            plDAO.remove(pl);

            ArrayList<PriceListEntry> pleList = pleDAO.findByPriceList(listId);
            for (PriceListEntry ple : pleList) {
                pleDAO.remove(ple);
            }

            th.execute();
        } catch (Exception e) {
            th.rollback();
        } finally {
            th.endTransaction();
        }
    }

    public static void setPrices(int listId, HashMap<Integer,Integer> prices) {
        for (Map.Entry<Integer,Integer> values : prices.entrySet()) {
            int ressId = values.getKey();
            int price = values.getValue();

            PriceListEntry ple = pleDAO.findByPriceListAndRess(listId, ressId);
            ple.setPrice(price);
            pleDAO.update(ple);
        }
    }
}
