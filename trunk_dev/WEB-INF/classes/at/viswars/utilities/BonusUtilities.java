/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.utilities;

import at.viswars.Time;
import at.viswars.admin.module.AttributeType;
import at.viswars.enumeration.EBonusType;
import at.viswars.model.ActiveBonus;
import at.viswars.model.Bonus;
import at.viswars.result.BonusResult;
import at.viswars.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Dreloc
 */
public class BonusUtilities extends Service{


    public static void updateBonus(){
        ArrayList<ActiveBonus> updateList = new ArrayList<ActiveBonus>();

        for(ActiveBonus ab : (ArrayList<ActiveBonus>)activeBonusDAO.findAll()){
            Bonus b = bonusDAO.findById(ab.getBonusId());
            if(b.getDuration() == 0)continue;

            if((ab.getDuration() - 1) <= 0){
                activeBonusDAO.remove(ab);
            }else{
                ab.setDuration(ab.getDuration() - 1);
                updateList.add(ab);
                // activeBonusDAO.update(ab);
            }
        }

        activeBonusDAO.updateAll(updateList);
    }

    public static BonusResult findBonus(int planet, EBonusType type){
        double numericBonus = 0d;
        double percentageBonus = 0d;

        for(ActiveBonus ab : activeBonusDAO.findByPlanetId(planet)){
            Bonus b = bonusDAO.findById(ab.getBonusId());
            if(b.getBonusType() == type){
                if(b.getValueType() == AttributeType.INCREASE){
                    numericBonus += b.getValue();
                }else if(b.getValueType() == AttributeType.DECREASE){
                    numericBonus -= b.getValue();
                }else if(b.getValueType() == AttributeType.INCREASE_PERC){
                    percentageBonus += b.getValue();
                }else if(b.getValueType() == AttributeType.DECREASE_PERC){
                    percentageBonus -= b.getValue();
                }else if(b.getValueType() == AttributeType.DEFINE){
                    numericBonus = b.getValue();
                    break;
                }
            }
        }


        return new BonusResult(numericBonus, percentageBonus);
    }

    public static int[] calculateValue(int planetId, int value, EBonusType bonusType){

        int baseValue = value;
        int[] result = new int[2];
        BonusResult br = findBonus(planetId, bonusType);
        baseValue -= br.getNumericBonus();
        baseValue = (int)Math.round(baseValue / (1d + br.getPercBonus()));

        result[0] = baseValue;
        result[1] = value - baseValue;


        return result;
    }
}
