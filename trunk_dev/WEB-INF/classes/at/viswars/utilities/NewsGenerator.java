/*
 * NewsGenerator.java
 *
 * Created on 10. Mai 2007, 13:08
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.view.html.td;
import at.viswars.view.html.body;
import at.viswars.view.html.html;
import at.viswars.view.html.tr;
import at.viswars.view.html.table;
import at.viswars.database.access.DbConnect;
import java.util.*;
import java.io.*;
import at.viswars.DebugBuffer;
import java.sql.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.FormatUtilities;
import at.viswars.GameConfig;
import at.viswars.GenerateMessage;

/**
 *
 * @author Horst
 */
public class NewsGenerator {

    File htmlFile;
    FileWriter f1;
    Map<String, String[]> parMap;
    String fileName = "/webs/web495/html/homepage/subSites/News.htm";
    //Setzen des Pfades der News.htm
    String newsHtmlPath = "/webs/web495/html/homepage/subSites/News.htm";
    //Setzen der maximalen News
    private int maxNews = 5;
    final int languageId;

    /** Creates a new instance of NewsGenerator */
    public NewsGenerator(int languageId) {
        this.languageId = languageId;
    }

    public void makeFile(String fileName) {

        htmlFile = new File(fileName);
        DebugBuffer.addLine(DebugLevel.DEBUG, "Filename der News.html = " + htmlFile.getAbsolutePath());
        try {
            f1 = new FileWriter(htmlFile);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in NewsGenerator - NewsGenerator ", e);
        }
    }

    public void makeFile() {

        htmlFile = new File(fileName);
        DebugBuffer.addLine(DebugLevel.DEBUG, "Filename der News.html = " + htmlFile.getAbsolutePath());
        try {
            f1 = new FileWriter(htmlFile);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in NewsGenerator - NewsGenerator ", e);
        }
    }

    public void setParameterMap(Map<String, String[]> Parameters) {

        this.parMap = Parameters;

        for (final Map.Entry<String, String[]> entry : parMap.entrySet()) {
            String parName = entry.getKey();

            if (parName.startsWith("newsname")) {
                fileName = parMap.get("newsname")[0];
            }
        }
        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Name in Feld: " + fileName);
        //this.fileName = fileName;
    }

    public static void main(String args[]) {

        NewsGenerator NG = new NewsGenerator(1);
        NG.makeFile();
        NG.generateNews(0);
        NG.makeFile("/webs/web495/html/homepage/subSites/ArchivedNews.htm");
        NG.generateNews(1);
    }

    public String generateAllNews() {
        if (languageId == 1) {
            try {;
                makeFile("/webs/web495/html/homepage/subSites/News.htm");
                //Generieren der nicht archivierten news
                generateNews(0);

                makeFile("/webs/web495/html/homepage/subSites/ArchivedNews.htm");
                //Generieren der archivierten news
                generateNews(1);
            } catch (Exception e) {
            }
        }
        if (languageId == 0) {
            try {
                makeFile("/webs/web495/html/homepage_en/subSites/News.htm");
                //Generieren der nicht archivierten news
                generateNews(0);

                makeFile("/webs/web495/html/homepage_en/subSites/ArchivedNews.htm");
                //Generieren der archivierten news
                generateNews(1);
            } catch (Exception e) {
            }
        }
        return "done";

    }

    public String generateNews(int isArchived) {
        int newsCounter = 0;

        html html = new html();

        body body = new body();

                String imageLink = "images";
                if(languageId == 0){
                    imageLink = "http://www.thedarkdestiny.at/homepage/subSites/images";
                }
        body.setProperties("text='White' style='background-image:url("+ imageLink + "/RightMidBackground.jpg); background-attachment:fixed;'");
        if (isArchived == 0) {
            body.setData("<center><img src='"+ imageLink + "/news.gif'></img><br><br>");
            body.setEndData("<a href='http://www.thedarkdestiny.at/homepage/subSites/ArchivedNews.htm'><font color='WHITE'><b>-&auml;ltere News-</b></font></a><br><br></center>");
        } else {

            body.setData("<center><img src='"+ imageLink + "/news.gif'></img><br><br><a href='http://www.thedarkdestiny.at/homepage/subSites/News.htm'><font color='WHITE'><b>-Zur&uuml;ck-</b></font></a><br><br></center>");
        }
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM news WHERE isArchived=" + isArchived + " AND languageId=" + languageId + " ORDER BY date DESC");
            while (rs.next()) {
                /*Logger.getLogger().write("Username");
                //Logger.getLogger().write("UsernameID= "+rs.getInt(6));
                //Logger.getLogger().write("Username= "+getUserName(rs.getInt(6)));*/
                newsCounter++;
                if (isArchived == 0) {
                    if (newsCounter <= maxNews) {
                        table table1 = new table();
                        table1.setProperties("cellspacing='0' cellpadding='0' align='center' WIDTH='90%' border='0'");

                        tr tr1 = new tr();

                        td td11 = new td();
                        td11.setProperties("height='41' width='100' style='background-image: url("+ imageLink + "/NewsTable/HeaderLeft.jpg); background-repeat: repeat-x;' border='0'");
                        java.util.Date msgDate = new java.util.Date(rs.getLong(5));
                        String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate);

                        td11.setData("<FONT COLOR='3399FF' ><center>    " + msgDateString + "</FONT></center>");
                        tr1.addTd(td11);
                        td td12 = new td();
                        td12.setProperties("height='41' width='27' align='left'  style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectLeft.jpg); background-repeat: repeat-x;'");
                        tr1.addTd(td12);
                        td td13 = new td();
                        td13.setProperties("height='41' width='20'               style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectBackground.jpg); background-repeat: repeat-x;'");
                        tr1.addTd(td13);
                        td td14 = new td();
                        td14.setProperties("height='41' width='*'  align='left'  style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectSpacerLeft.jpg); background-repeat: repeat-x;'");
                        td14.setData("<FONT COLOR='3399FF' ><center><b> " + FormatUtilities.toISO(rs.getString(2)) + "</b></center> </FONT>");
                        tr1.addTd(td14);
                        td td15 = new td();
                        td15.setProperties("height='41' width='30' 		     style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectSpacerLeft.jpg); background-repeat: repeat-x;'");
                        tr1.addTd(td15);
                        td td16 = new td();
                        td16.setProperties("height='41' width='31' align='right' style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectRight.jpg); background-repeat: repeat-x;'");
                        tr1.addTd(td16);

                        table1.addTr(tr1);

                        body.addTable(table1);

                        // NEXT TABLE
                        table table2 = new table();
                        table2.setProperties("cellspacing='0' cellpadding='0' align='center' WIDTH='90%' border='0'");

                        tr tr2 = new tr();

                        td td21 = new td();
                        td21.setProperties("width='46' height='23' style='background-image: url("+ imageLink + "/NewsTable/TextTopLeftAge.jpg); background-repeat: none;'");
                        tr2.addTd(td21);
                        td td22 = new td();
                        td22.setProperties("width='*' height='23' style='background-image: url("+ imageLink + "/NewsTable/TextTopSpacer.jpg); background-repeat: repeat-x; text-align: center;'");
                        //td22.setData("<img src='images/NewsTable/TextTop.jpg' border='0'></img>");
                        tr2.addTd(td22);
                        td td23 = new td();
                        td23.setProperties("width='46' height='23' style='background-image: url("+ imageLink + "/NewsTable/TextTopRightAge.jpg); background-repeat: none;'");
                        tr2.addTd(td23);

                        table2.addTr(tr2);

                        //next ROW

                        tr tr3 = new tr();

                        td td31 = new td();
                        td31.setProperties("background='"+ imageLink + "/NewsTable/TextLeftSpacer.jpg'");
                        tr3.addTd(td31);
                        td td32 = new td();
                        td32.setProperties("td background='"+ imageLink + "/NewsTable/TextMid.jpg'");

                        // Parsing
                        td32.setData("<FONT COLOR='BLACK'> " + GenerateMessage.parseText(FormatUtilities.toISO(rs.getString(3))) + "<br><br><p align='right'><b>-" + getUserName(rs.getInt(6)) + "-</b></p> </FONT>");
                        tr3.addTd(td32);
                        td td33 = new td();
                        td33.setProperties("background='"+ imageLink + "/NewsTable/TextRightSpacer.jpg'");
                        tr3.addTd(td33);

                        table2.addTr(tr3);

                        //next ROW
        /*          <td width="46" height="37"><img src="images/NewsTable/TextBottomLeftAge.jpg" border="0"></img></td>
                        <td witdh="*" align="left" background="images/NewsTable/TextBottomSpacerLeft.jpg">&nbsp</td>
                        <td width="*" height="37" background="images/NewsTable/TextBottomSpacerRight.jpg"><img src="images/NewsTable/TextBottom.jpg" border="0"></img></td>
                        <td witdh="*" align="right" background="images/NewsTable/TextBottomSpacerRight.jpg">&nbsp</td>
                        <td width="46" height="37"><img src="images/NewsTable/TextBottomRightAge.jpg" border="0"></img></td>*/
                        tr tr4 = new tr();

                        td td41 = new td();
                        td41.setProperties("width='46' height='37' style='background-image: url("+ imageLink + "/NewsTable/TextBottomLeftAge.jpg); background-repeat: none;'");
                        tr4.addTd(td41);
                        td td42 = new td();
                        td42.setProperties("width='*' height='37' style='background-image: url("+ imageLink + "/NewsTable/TextBottomSpacerLeft.jpg); background-repeat: repeat-x; text-align: center;'");
                        // td42.setData("<img src='images/NewsTable/TextBottom.jpg' border='0'>");
                        tr4.addTd(td42);
                        td td43 = new td();
                        td43.setProperties("width='46' height='37' style='background-image: url("+ imageLink + "/NewsTable/TextBottomRightAge.jpg); background-repeat: none;'");
                        tr4.addTd(td43);

                        table2.addTr(tr4);
                        table1.addTable(table2);
                        //Jede News �ber MaxNews wird als archiviert
                    } else {
                        //Logger.getLogger().write("mehr als "+maxNews+" message");
                        String query = "UPDATE news SET isArchived = 1 WHERE newsId = " + rs.getInt(1);

                        try {
                            Statement stmt2 = DbConnect.createStatement();


                            stmt2.execute(query);
                            stmt2.close();
                        } catch (Exception e) {
                            DebugBuffer.writeStackTrace("Error in News - setToArchive: ", e);
                        }
                    }
                } else {
                    table table1 = new table();
                    table1.setProperties("cellspacing='0' cellpadding='0' align='center' WIDTH='90%' border='0'");

                    tr tr1 = new tr();

                    td td11 = new td();
                    td11.setProperties("height='41' width='100'                 style='background-image: url("+ imageLink + "/NewsTable/HeaderLeft.jpg); background-repeat: repeat-x;' border='0'");
                    java.util.Date msgDate = new java.util.Date(rs.getLong(5));
                    String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate);

                    td11.setData("<FONT COLOR='3399FF' ><center>    " + msgDateString + "</FONT></center>");
                    tr1.addTd(td11);
                    td td12 = new td();
                    td12.setProperties("height='41' width='27' align='left'  style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectLeft.jpg); background-repeat: repeat-x;'");
                    tr1.addTd(td12);
                    td td13 = new td();
                    td13.setProperties("height='41' width='20'               style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectBackground.jpg); background-repeat: repeat-x;'");
                    tr1.addTd(td13);
                    td td14 = new td();
                    td14.setProperties("height='41' width='*'  align='left'  style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectSpacerLeft.jpg); background-repeat: repeat-x;'");
                    td14.setData("<FONT COLOR='3399FF' ><center><b> " + FormatUtilities.toISO(rs.getString(2)) + "</b></center> </FONT>");
                    tr1.addTd(td14);
                    td td15 = new td();
                    td15.setProperties("height='41' width='30' 		     style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectSpacerLeft.jpg); background-repeat: repeat-x;'");
                    tr1.addTd(td15);
                    td td16 = new td();
                    td16.setProperties("height='41' width='31' align='right' style='background-image: url("+ imageLink + "/NewsTable/HeaderSubjectRight.jpg); background-repeat: repeat-x;'");
                    tr1.addTd(td16);

                    table1.addTr(tr1);

                    body.addTable(table1);

                    // NEXT TABLE
                    table table2 = new table();
                    table2.setProperties("cellspacing='0' cellpadding='0' align='center' WIDTH='90%' border='0'");

                    tr tr2 = new tr();

                    td td21 = new td();
                    td21.setProperties("width='46' height='23' style='background-image: url("+ imageLink + "/NewsTable/TextTopLeftAge.jpg); background-repeat: none;'");
                    tr2.addTd(td21);
                    td td22 = new td();
                    td22.setProperties("width='*' height='23' style='background-image: url("+ imageLink + "/NewsTable/TextTopSpacer.jpg); background-repeat: repeat-x; text-align: center;'");
                    //td22.setData("<img src='images/NewsTable/TextTop.jpg' border='0'></img>");
                    tr2.addTd(td22);
                    td td23 = new td();
                    td23.setProperties("width='46' height='23' style='background-image: url("+ imageLink + "/NewsTable/TextTopRightAge.jpg); background-repeat: none;'");
                    tr2.addTd(td23);

                    table2.addTr(tr2);

                    //next ROW

                    tr tr3 = new tr();

                    td td31 = new td();
                    td31.setProperties("background='"+ imageLink + "/NewsTable/TextLeftSpacer.jpg'");
                    tr3.addTd(td31);
                    td td32 = new td();
                    td32.setProperties("td background='"+ imageLink + "/NewsTable/TextMid.jpg'");
                    td32.setData("<FONT COLOR='BLACK'> " + GenerateMessage.parseText(FormatUtilities.toISO(rs.getString(3))) + "<br><br><p align='right'><b>-" + getUserName(rs.getInt(6)) + "-</b></p> </FONT>");
                    tr3.addTd(td32);
                    td td33 = new td();
                    td33.setProperties("background='"+ imageLink + "/NewsTable/TextRightSpacer.jpg'");
                    tr3.addTd(td33);

                    table2.addTr(tr3);

                    //next ROW
        /*          <td width="46" height="37"><img src="images/NewsTable/TextBottomLeftAge.jpg" border="0"></img></td>
                    <td witdh="*" align="left" background="images/NewsTable/TextBottomSpacerLeft.jpg">&nbsp</td>
                    <td width="*" height="37" background="images/NewsTable/TextBottomSpacerRight.jpg"><img src="images/NewsTable/TextBottom.jpg" border="0"></img></td>
                    <td witdh="*" align="right" background="images/NewsTable/TextBottomSpacerRight.jpg">&nbsp</td>
                    <td width="46" height="37"><img src="images/NewsTable/TextBottomRightAge.jpg" border="0"></img></td>*/
                    tr tr4 = new tr();

                    td td41 = new td();
                    td41.setProperties("width='46' height='37' style='background-image: url("+ imageLink + "/NewsTable/TextBottomLeftAge.jpg); background-repeat: none;'");
                    tr4.addTd(td41);
                    td td42 = new td();
                    td42.setProperties("width='*' height='37' style='background-image: url("+ imageLink + "/NewsTable/TextBottomSpacerLeft.jpg); background-repeat: repeat-x; text-align: center;'");
                    // td42.setData("<img src='images/NewsTable/TextBottom.jpg' border='0'>");
                    tr4.addTd(td42);
                    td td43 = new td();
                    td43.setProperties("width='46' height='37' style='background-image: url("+ imageLink + "/NewsTable/TextBottomRightAge.jpg); background-repeat: none;'");
                    tr4.addTd(td43);

                    table2.addTr(tr4);
                    table1.addTable(table2);
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in NewsGenerator - generateNews ", e);
        }

        html.setBody(body);

        writeHtmlFile(html);
        return htmlFile.getAbsolutePath();
    }

    public void writeHtmlFile(html html) {

        ArrayList<table> tables = new ArrayList<table>();
        ArrayList<table> innerTables = new ArrayList<table>();


        try {


            f1.write("<html>\n\n");

            body body = html.getBody();
            tables = body.getTables();
            f1.write("<HEAD> \n \t<META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>\n<META http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />\n</HEAD>\n");
            f1.write("\t <body");
            if (!body.getProperties().equals("")) {

                f1.write(" " + body.getProperties() + " >\n\n");

            } else {

                f1.write(">\n\n");
            }

            if (body.getData() != null) {
                f1.write("\t" + body.getData() + "\n\n");
            }

            //TABLES
            for (int i = 0; i < tables.size(); i++) {
                table table = tables.get(i);
                writeTable(table);
                innerTables = table.getTables();
                for (int j = 0; j < innerTables.size(); j++) {
                    table innerTable = innerTables.get(j);
                    writeTable(innerTable);

                }
                //TABLE END


                f1.write("<br><br><br>\n");
            }
            if (body.getEndData() != null) {
                f1.write("\t" + body.getEndData() + "\n\n");
            }



            f1.write("\t </body>\n\n");

            f1.write("</html>\n");

            f1.close();

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in NewsGenerator - writeHtmlFile ", e);

        }


    }

    public void writeTable(table table) {

        ArrayList<tr> trs = new ArrayList<tr>();
        ArrayList<td> tds = new ArrayList<td>();
        try {
            f1.write("\t\t<table");
            if (!table.getProperties().equals("")) {
                f1.write(" " + table.getProperties() + ">\n\n");

            } else {
                f1.write(">\n\n");
            }

            //TRS

            trs = table.getTrs();

            for (int j = 0; j < trs.size(); j++) {

                tr tr = trs.get(j);

                f1.write("\t\t\t<tr");

                if (!tr.getProperties().equals("")) {

                    f1.write(" " + tr.getProperties() + ">\n\n");

                } else {

                    f1.write(">\n\n");
                }

                //TDS

                tds = tr.getTds();

                for (int k = 0; k < tds.size(); k++) {

                    td td = tds.get(k);

                    f1.write("\t\t\t\t<td");

                    if (!td.getProperties().equals("")) {

                        f1.write(" " + td.getProperties() + ">");

                    } else {

                        f1.write(">");
                    }
                    //TD Data

                    if (!td.getData().equals("")) {

                        f1.write(" " + td.getData() + " ");

                    }

                    f1.write(" </td>\n");
                }



                f1.write("\t\t\t </tr>\n\n");
            }

            f1.write("\t\t</table>\n\n");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in NewsGenerator - writeTable ", e);

        }
    }

    public String getUserName(int userId) {

        return "Euer DarkDestiny - Team";
    }
}
