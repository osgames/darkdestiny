/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.utilities;

import at.viswars.diplomacy.relations.EAttackType;
import at.viswars.model.PlayerFleet;
import at.viswars.service.DiplomacyService;
import at.viswars.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class ProductionUtilities extends Service{


    public static boolean isBlockedByEnemy(int planetId){
            boolean blockedByEnemy = false;

            int ownerId = playerPlanetDAO.findByPlanetId(planetId).getUserId();

            PlayerFleet searchPf = new PlayerFleet();
            searchPf.setPlanetId(planetId);

            boolean foundEnemyFleet = false;
            boolean foundOwnerFleet = false;

            ArrayList<PlayerFleet> pfs = playerFleetDAO.find(searchPf);
            for(PlayerFleet pf : pfs){
                if(!foundOwnerFleet){
                    if(pf.getUserId() == ownerId){
                        foundOwnerFleet = true;
                    }
                }

                if(!foundEnemyFleet){
                    if(pf.getUserId() != ownerId){
                        if(DiplomacyUtilities.getDiplomacyResult(pf.getUserId(), ownerId).getBattleInOwn().equals(EAttackType.YES)){
                            foundEnemyFleet = true;
                        }
                    }
                }
            }
            if(foundEnemyFleet && !foundOwnerFleet){
                blockedByEnemy = true;
            }

            return blockedByEnemy;

    }

}
