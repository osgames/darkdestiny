/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.GameConstants;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.PlayerResearchDAO;
import at.viswars.dao.ProductionDAO;
import at.viswars.dao.TechRelationDAO;
import at.viswars.enumeration.EBonusType;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.PlayerResearch;
import at.viswars.model.Production;
import at.viswars.model.Ressource;
import at.viswars.model.TechRelation;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.result.BonusResult;
import at.viswars.result.ResearchResult;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ResearchUtilities {

    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static ProductionDAO pDAO = (ProductionDAO) DAOFactory.get(ProductionDAO.class);
    private static TechRelationDAO trDAO = (TechRelationDAO) DAOFactory.get(TechRelationDAO.class);

    public static boolean isModuleResearched(int moduleId, int userId) {
        ArrayList<TechRelation> trList = trDAO.findByModuleId(moduleId);
        if (trList.size() == 0) {
            return true;
        }

        for (TechRelation tr : trList) {
            PlayerResearch pr = prDAO.findBy(userId, tr.getReqResearchId());
            if (pr == null) {
                return false;
            }
        }

        return true;
    }

    public static ResearchResult getResearchPointsGlobal(int userId) {
        ArrayList<PlayerPlanet> ppList = ppDAO.findByUserId(userId);

        int totalResPoints = 0;
        int totalCompResPoints = 0;

        int totalRPBonus = 0;
        int totalCRPBonus = 0;

        for (PlayerPlanet pp : ppList) {
            int resPoints = 0;
            int compResPoints = 0;

            ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(pp.getPlanetId());

            boolean planetHasResearch = false;

            // TODO unify rp calculation of epcr
            for (PlanetConstruction pc : pcList) {
                ArrayList<Production> pList = pDAO.findProductionForConstructionId(pc.getConstructionId());
                for (Production p : pList) {
                    if (p.getRessourceId() == Ressource.RP) {
                        resPoints += p.getQuantity() * pc.getActiveCount();
                        planetHasResearch = true;
                    } else if (p.getRessourceId() == Ressource.RP_COMP) {
                        compResPoints += p.getQuantity() * pc.getActiveCount();
                        planetHasResearch = true;
                    }
                }
            }

            if (planetHasResearch) {
                // Logger.getLogger().write("Total research for Planet " + pp.getPlanetId() + " RP: " + resPoints + " CRP: " + compResPoints);

                try {
                    PlanetCalculation pc = new PlanetCalculation(pp.getPlanetId());
                    ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

                    // if (pp.getPlanetId() == 7864) Logger.getLogger().write("RESS EFF IS " + epcr.getEffResearch());
                    // if (pp.getPlanetId() == 7864) Logger.getLogger().write("RESSPOINTS = " + resPoints);
                    // if (pp.getPlanetId() == 7864) Logger.getLogger().write("CRESSPOINTS = " + compResPoints);

                    // Logger.getLogger().write("Research Efficiency on currPlanet is " + epcr.getEffResearch());
                    BonusResult br = BonusUtilities.findBonus(pp.getPlanetId(), EBonusType.RESEARCH);

                    int actRP = (int) (resPoints * epcr.getEffResearch());
                    int actCRP = (int) (compResPoints * epcr.getEffResearch());

                    if (br != null) {
                        totalRPBonus += (actRP * br.getPercBonus()) + br.getNumericBonus();
                        totalCRPBonus += (actCRP * br.getPercBonus()) + br.getNumericBonus();
                    }

                    totalResPoints += (int) (resPoints * epcr.getEffResearch());
                    totalCompResPoints += (int) (compResPoints * epcr.getEffResearch());
                } catch (Exception e) {
                    DebugBuffer.writeStackTrace("Research Points Global Error: ", e);
                }
            }
        }

        return new ResearchResult(totalResPoints, totalCompResPoints, totalRPBonus, totalCRPBonus);
    }

    public static ResearchResult getResearchPointsPlanet(int planetId, PlanetCalculation pc) {

        int totalResPoints = 0;
        int totalCompResPoints = 0;

        int totalRPBonus = 0;
        int totalCRPBonus = 0;


        int resPoints = 0;
        int compResPoints = 0;

        // TODO use ProdCalculation
        ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(planetId);

        boolean planetHasResearch = false;

        for (PlanetConstruction pCon : pcList) {
            ArrayList<Production> pList = pDAO.findProductionForConstructionId(pCon.getConstructionId());
            for (Production p : pList) {
                if (p.getRessourceId() == Ressource.RP) {
                    resPoints += p.getQuantity() * pCon.getActiveCount();
                    planetHasResearch = true;
                } else if (p.getRessourceId() == Ressource.RP_COMP) {
                    compResPoints += p.getQuantity() * pCon.getActiveCount();
                    planetHasResearch = true;
                }
            }
        }

        if (planetHasResearch) {
            // Logger.getLogger().write("Total research for Planet " + pp.getPlanetId() + " RP: " + resPoints + " CRP: " + compResPoints);

            try {
                ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

                // if (pp.getPlanetId() == 7864) Logger.getLogger().write("RESS EFF IS " + epcr.getEffResearch());
                // if (pp.getPlanetId() == 7864) Logger.getLogger().write("RESSPOINTS = " + resPoints);
                // if (pp.getPlanetId() == 7864) Logger.getLogger().write("CRESSPOINTS = " + compResPoints);

                // Logger.getLogger().write("Research Efficiency on currPlanet is " + epcr.getEffResearch());
                BonusResult br = BonusUtilities.findBonus(planetId, EBonusType.RESEARCH);

                int actRP = (int) (resPoints * epcr.getEffResearch());
                int actCRP = (int) (compResPoints * epcr.getEffResearch());

                if (br != null) {
                    totalRPBonus += (actRP * br.getPercBonus()) + br.getNumericBonus();
                    totalCRPBonus += (actCRP * br.getPercBonus()) + br.getNumericBonus();
                }

                totalResPoints += (int) (resPoints * epcr.getEffResearch());
                totalCompResPoints += (int) (compResPoints * epcr.getEffResearch());
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Research Points Planet Error: ",e);
            }

        }

        return new ResearchResult(totalResPoints, totalCompResPoints, totalRPBonus, totalCRPBonus);
    }

    public static boolean isResearchResearched(int resId, int userId) {
        PlayerResearch pr = new PlayerResearch();
        pr.setResearchId(resId);
        pr.setUserId(userId);
        pr = (PlayerResearch) prDAO.get(pr);

        return (pr != null);
    }
}
