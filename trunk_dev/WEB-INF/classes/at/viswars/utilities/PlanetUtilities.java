/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.GameUtilities;
import at.viswars.construction.restriction.Observatory;
import at.viswars.dao.*;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EPlanetLogType;
import at.viswars.model.*;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.notification.NotificationBuffer;
import at.viswars.result.BaseResult;
import at.viswars.service.FleetService;
import at.viswars.service.IdToNameService;
import at.viswars.service.Service;
import at.viswars.service.TransportService;
import at.viswars.spacecombat.CombatHandler_NEW;
import at.viswars.spacecombat.CombatReportGenerator;
import at.viswars.viewbuffer.HeaderBuffer;
import at.viswars.voting.MetaDataDataType;
import at.viswars.voting.Vote;
import at.viswars.voting.VoteUtilities;
import at.viswars.voting.VotingTextMLSecure;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class PlanetUtilities extends Service {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetLogDAO plDAO = (PlanetLogDAO)DAOFactory.get(PlanetLogDAO.class);
    private static UserDAO uDAO = (UserDAO)DAOFactory.get(UserDAO.class);
    private static SystemDAO sDAO = (SystemDAO)DAOFactory.get(SystemDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO)DAOFactory.get(PlanetDAO.class);
    
    public static void movePlanetToPlayer(int newOwner, int planetId, PlayerPlanet pp, boolean peaceful) {
        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "The planet " + planetId + " is taken over by " + userDAO.findById(newOwner).getGameName());
        DebugBuffer.addLine(DebugBuffer.DebugLevel.GROUNDCOMBAT, "The planet " + planetId + " is taken over by " + userDAO.findById(newOwner).getGameName());
        if (newOwner == pp.getUserId()) {
            return;
        }
        try {
            int oldUser = pp.getUserId();

            //Delete possible Notifications
            try {
                NotificationBuffer.removePONotification(oldUser, planetId);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while deleting possible Notifications : userId " + oldUser + " planetId : " + planetId,e);
            }

            //Deleting TradepostEntry
            try {
                PlanetConstruction pc = planetConstructionDAO.findBy(planetId, Construction.ID_INTERSTELLARTRADINGPOST);
                if (pc != null) {
                    TradePost tp = Service.tradePostDAO.findByPlanetId(planetId);
                    if (tp != null) {
                        TradePost tpNew = new TradePost();
                        tpNew.setName(tp.getName());
                        tpNew.setPlanetId(tp.getPlanetId());
                        tpNew.setTransportPerLJ(tp.getTransportPerLJ());
                        Service.tradePostDAO.remove(tp);
                        Service.tradePostDAO.add(tp);
                    }
                }
            } catch (Exception e) {
                DebugBuffer.error("Error while deleting possible TradepostEntry : userId " + oldUser + " planetId : " + planetId);

            }

            //Deleting Traderoutes
            ArrayList<TradeRoute> tradeRoutes = tradeRouteDAO.findByStartPlanet(planetId);
            tradeRoutes.addAll(tradeRouteDAO.findByEndPlanet(planetId));
            for (TradeRoute tr : tradeRoutes) {
                TransportService.deleteRoute(tr.getUserId(), tr.getId());
            }

            //Updating Planets in Observatory range
            boolean hasObservatory = planetConstructionDAO.isConstructed(planetId, Construction.ID_OBSERVATORY);
            Observatory o = new Observatory();
            if (hasObservatory) {
                //Delete for existing User
                o.onDeconstruction(pp);
            }
            
            //Change planet
            pp.setHomeSystem(false);
            pp.setUserId(newOwner);
            
            if (!peaceful) {
                pp.setTax(0);
            }
            
            playerPlanetDAO.update(pp);            
            for (PlanetCategory pc : planetCategoryDAO.findAllByPlanetId(planetId)) {
                planetCategoryDAO.remove(pc);
            }
            
            //Observatory after planet changed
            if (hasObservatory) {
                //Establish Entries for new Owner
                o.onFinishing(pp);
            }

            HeaderBuffer.reloadUser(newOwner);
            HeaderBuffer.reloadUser(oldUser);

            //Remove Groundtroop Orders
            if (!peaceful) {
                for (Action a : actionDAO.findByPlanet(planetId)) {
                    if (a.getType().equals(EActionType.GROUNDTROOPS)) {
                        ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                        productionOrderDAO.remove(po);
                        actionDAO.remove(a);
                    }                    
                }
            }
            
            // Transfer all groundtroops to new user
            for (PlayerTroop pt : playerTroopDAO.findBy(oldUser, planetId)) {
                playerTroopDAO.remove(pt);                
                pt.setUserId(newOwner);

                PlayerTroop existing = playerTroopDAO.findBy(newOwner, planetId, pt.getTroopId());
                if (existing == null) {
                    playerTroopDAO.add(pt);
                } else {
                    existing.setNumber(existing.getNumber() + pt.getNumber());
                    playerTroopDAO.update(existing);
                }
            }                       
            
            //Create PlanetLog Entry
            {
                PlanetLog pl = new PlanetLog();
                pl.setPlanetId(planetId);
                pl.setTime(GameUtilities.getCurrentTick2());
                pl.setUserId(newOwner);
                pl.setType(EPlanetLogType.TRANSFER);

                planetLogDAO.add(pl);
            }
            
            // GenerateMessage looser = new GenerateMessage();
            // GenerateMessage winner = new GenerateMessage();

            int currTick = GameUtilities.getCurrentTick2();
            RelativeCoordinate rc = new RelativeCoordinate(0, pp.getPlanetId());
            ViewTableUtilities.addOrRefreshSystem(oldUser, rc.getSystemId(), currTick);
            ViewTableUtilities.addOrRefreshSystem(newOwner, rc.getSystemId(), currTick);

            /*
            ArrayList<Integer> participants = new ArrayList<Integer>();
            participants.add(oldUser);
            participants.add(newOwner);            
            */

            /*
            CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(participants, new RelativeCoordinate(0, planetId));

            for (Map.Entry<Integer, ArrayList<Integer>> entry : cgr.getAsMap().entrySet()) {
                for (Integer i : entry.getValue()) {
                    //Loosers
                    if (entry.getValue().contains(oldUser)) {
                        looser.setDestinationUserId(i);
                        looser.setSourceUserId(uDAO.getSystemUser().getUserId());
                        looser.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
                        looser.setMsg(ML.getMLStr("fleetmanagement_msg_invasionlostplanet", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(newOwner).getGameName()));
                        looser.setMessageType(EMessageType.SYSTEM);
                        looser.writeMessageToUser();
                        //Winners
                    } else {
                        winner.setDestinationUserId(i);
                        winner.setSourceUserId(uDAO.getSystemUser().getUserId());
                        winner.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
                        winner.setMsg(ML.getMLStr("fleetmanagement_msg_invasionwon", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(oldUser).getGameName()));
                        winner.setMessageType(EMessageType.SYSTEM);
                        winner.writeMessageToUser();
                    }
                }
            }            
            */
        } catch (Exception e) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.ERROR, "Groundcombat - Error while fight after moving planet : " + e);
            DebugBuffer.produceStackTrace(e);
        }

        //Attack Tradefleets if new are available
        try {
            ArrayList<PlayerFleet> pfs = playerFleetDAO.findByPlanetId(planetId);
            for (PlayerFleet pf : pfs) {
                PlayerFleetExt pfe = new PlayerFleetExt(pf);
                if (FleetService.canFight(pfe)) {

                    if (pfe.getRelativeCoordinate().getSystemId() == 0) {
                        DebugBuffer.addLine(DebugBuffer.DebugLevel.FATAL_ERROR, "Invoking Combat in GroundCombatUtilities with System 0");

                    } else {
                        CombatReportGenerator crg = new CombatReportGenerator();
                        if (pfe.getRelativeCoordinate().getPlanetId() == 0) {
                            CombatHandler_NEW ch = new CombatHandler_NEW(CombatReportGenerator.CombatType.SYSTEM_COMBAT, playerFleetDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
                            ch.processCombatResults();
                        } else {
                            CombatHandler_NEW ch = new CombatHandler_NEW(CombatReportGenerator.CombatType.ORBITAL_COMBAT, playerFleetDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
                            ch.processCombatResults();
                        }

                        crg.writeReportsToDatabase();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while setting new owner : ", e);
        }
    }    
    
    public static double getDistanceToHome(int userId, int planetId) {
        PlayerPlanet home = ppDAO.findHomePlanetByUserId(userId);
        // Yeah not optimal but just return something so we have a value to calculate
        if (home == null) return 0d;
        
        RelativeCoordinate homeCoord = new RelativeCoordinate(0,home.getPlanetId());
        RelativeCoordinate compareCoord = new RelativeCoordinate(0,planetId);
        
        return homeCoord.distanceTo(compareCoord);
    }
    
    public static void transferPlanet(int newUser, int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        
        int currPlanetOwner = pp.getUserId();
        
        ArrayList<User> uList = PlanetUtilities.getAllFormerPlanetOwners(currPlanetOwner, planetId);
        boolean allowed = false;
        
        for (User u : uList) {
            if (u.getUserId() == newUser) {
                allowed = true;
            }
        }        
        
        if (allowed) {
            movePlanetToPlayer(newUser,planetId,pp,true);
        }
    }
    
    public static BaseResult createTransferPlanetVote(int userId, int targetUserId, int planetId) {
        // Basic checks on parameters
        ArrayList<User> uList = PlanetUtilities.getAllFormerPlanetOwners(userId, planetId);
        boolean allowed = false;
        
        // java.lang.System.out.println("Received transfer request from " + userId + " to " + targetUserId + " for planet " + planetId);
        
        for (User u : uList) {
            if (u.getUserId() == targetUserId) {
                allowed = true;
            }
        }
        
        if (!allowed) {
            return new BaseResult("No way!",true);
        }
        
        try {
            Vote v = null;
            
            PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
            Planet p = pDAO.findById(planetId);
            at.viswars.model.System sys = sDAO.findById(p.getSystemId());
            
            if (true) {
                HashMap<String,CharSequence> msgReplacements = new HashMap<String,CharSequence>();
                HashMap<String,CharSequence> sbjReplacements = new HashMap<String,CharSequence>();

                sbjReplacements.put("%USER%",IdToNameService.getUserName(userId));
                sbjReplacements.put("%PLANETID%",p.getId().toString()); 
                sbjReplacements.put("%PLANETNAME%",pp.getName());

                msgReplacements.put("%USER%",IdToNameService.getUserName(userId));
                msgReplacements.put("%PLANETID%",p.getId().toString());                
                msgReplacements.put("%PLANETNAME%",pp.getName());
                msgReplacements.put("%SYSTEMID%",sys.getId().toString());
                msgReplacements.put("%SYSTEMNAME%",sys.getName());

                VotingTextMLSecure vtML = new VotingTextMLSecure("vote_transferplanet","vote_transferplanet_sbj",msgReplacements,sbjReplacements);
                v = new Vote(userId, vtML, Voting.TYPE_TRANSFERPLANET);
            }

            ArrayList<Integer> receivers = new ArrayList<Integer>();
            receivers.add(targetUserId);
            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, targetUserId));

            v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));
            v.setMinVotes((int) Math.ceil((double) receivers.size() * (2d / 3d)));
            v.setTotalVotes(receivers.size());

            v.addOption(new VoteOption("alliance_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
            v.addOption(new VoteOption("alliance_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

            v.addMetaData("PLANETID", planetId, MetaDataDataType.INTEGER);
            v.addMetaData("RECEIVERID", targetUserId, MetaDataDataType.INTEGER);

            v.setReceivers(receivers);

            VoteUtilities.createVote(v);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on transferPlanet vote: ", e);
            return new BaseResult(e.getMessage(),true);
        }        
        
        return new BaseResult("‹bergabeangebot an %1 wurde gesendet.",false);
    }
    
    public static ArrayList<User> getAllFormerPlanetOwners(int userId, int planetId) {
        ArrayList<User> userList = new ArrayList<User>();
        
        ArrayList<PlanetLog> allLogEntries = plDAO.findByPlanetIdSorted(planetId);
        PlanetLog lastInvasion = null;
        HashMap<Integer,ArrayList<Integer>> timeUserHelp = new HashMap<Integer,ArrayList<Integer>>();
        
        HashSet<Integer> processedUsers = new HashSet<Integer>();
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        
        boolean firstEntryInvasion = false;
        int currElement = 0;
        
        for (PlanetLog pl : allLogEntries) {
            // java.lang.System.out.println("Looping entry type " + pl.getType() + " and time " + pl.getTime());
            currElement++;
            
            if (!firstEntryInvasion && currElement == 1 && (pl.getType() == EPlanetLogType.INVADED || pl.getType() == EPlanetLogType.INVADED_HELP)) {
                // java.lang.System.out.println("Set first invasion to true");
                firstEntryInvasion = true;
            }
            
            if (firstEntryInvasion && lastInvasion == null && pl.getType() == EPlanetLogType.INVADED) {
                // java.lang.System.out.println("Set first invasion Entry to entry " + pl.getType() + " with time " + pl.getTime());
                lastInvasion = pl;
            }
            
            if (pl.getUserId() == userId) continue;
            
            if (pl.getType() == EPlanetLogType.COLONIZED) {
                // java.lang.System.out.println("Adding colonization entry " + pl.getType() + " with time " + pl.getTime());
                User u = uDAO.findById(pl.getUserId());
                userList.add(uDAO.findById(pl.getUserId()));
                processedUsers.add(u.getUserId());                
            }
            
            if (pl.getType() == EPlanetLogType.INVADED_HELP) {     
                // java.lang.System.out.println("Adding help entry " + pl.getType() + " with time " + pl.getTime() + " of user " + pl.getUserId());
                
                ArrayList<Integer> users = timeUserHelp.get(pl.getTime());
                if (users == null) {
                    users = new ArrayList<Integer>();
                    users.add(pl.getUserId());
                    timeUserHelp.put(pl.getTime(), users);
                } else {
                    users.add(pl.getUserId());
                }
            }
        }
        
        if (lastInvasion != null) {
            // java.lang.System.out.println("There was an invasion at " + lastInvasion.getTime());
            
            ArrayList<Integer> users = timeUserHelp.get(lastInvasion.getTime());
            if (users != null) {
                for (Integer uId : users) {
                    // java.lang.System.out.println("Found help by " + uId);
                    
                    if (processedUsers.contains(uId)) continue;
                    if (uId == userId) continue;

                    User u = uDAO.findById(uId);
                    if (u != null) {          
                        userList.add(u);
                        processedUsers.add(u.getUserId());
                    }      
                }
            } else {
                // java.lang.System.out.println("Found no help");
            }
        }               
        
        // If there is a groundcombat we dont care 
        if (Service.groundCombatDAO.findRunningCombat(planetId) != null) {
            userList.clear();
        }
        
        return userList;
    }
}
