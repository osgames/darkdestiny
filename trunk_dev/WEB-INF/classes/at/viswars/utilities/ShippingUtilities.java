/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.DamagedShipsDAO;
import at.viswars.dao.FleetLoadingDAO;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.RessourceDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.dao.TradeRouteDAO;
import at.viswars.dao.TradeRouteDetailDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.enumeration.ETradeRouteType;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.TradeRoute;
import at.viswars.model.TradeRouteDetail;
import at.viswars.service.Service;
import at.viswars.trade.TradeRouteExt;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author LensiPensi
 */
public class ShippingUtilities {

    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    private static TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    public static HashMap<Integer, HashMap<Boolean, Long>> findShippingBalanceForPlanetSorted(int planetId) {
        HashMap<Integer, HashMap<Boolean, Long>> result = new HashMap<Integer, HashMap<Boolean, Long>>();
        //Search all routes where planetId is Endplanet
        ArrayList<TradeRoute> routes1 = trDAO.findByEndPlanet(planetId);
        //Search all Transport-routes where planetId is Startplanet
        //If a planet has outgoing Traderoutes the ressources has already been decremented from the stock
        //Therefore the outgoing amount should not be added to the tradebalance of the ressource
        ArrayList<TradeRoute> routes2 = trDAO.findByStartPlanet(planetId, ETradeRouteType.TRANSPORT);
        // ArrayList<TradeRoute> routes2 = trDAO.findByStartPlanet(planetId);
        for (TradeRoute r : routes1) {

            TradeRouteExt tre = new TradeRouteExt(r);
            ArrayList<TradeRouteDetail> tres = tre.getRouteDetails();
            for (TradeRouteDetail detail : tres) {
                HashMap<Boolean, Long> balances = result.get(detail.getRessId());
                if (balances == null) {
                    balances = new HashMap<Boolean, Long>();
                }
                Long balance = balances.get(detail.getReversed());

                if (balance == null) {
                    balance = 0l;
                }
                balance += detail.getLastTransport();
                balances.put(detail.getReversed(), balance);
                result.put(detail.getRessId(), balances);
            }

        }
        for (TradeRoute r : routes2) {

            TradeRouteExt tre = new TradeRouteExt(r);
            ArrayList<TradeRouteDetail> tres = tre.getRouteDetails();
            for (TradeRouteDetail detail : tres) {
                HashMap<Boolean, Long> balances = result.get(detail.getRessId());
                if (balances == null) {
                    balances = new HashMap<Boolean, Long>();
                }
                Long balance = balances.get(!detail.getReversed());

                if (balance == null) {
                    balance = 0l;
                }
                balance += detail.getLastTransport();

                balances.put(!detail.getReversed(), balance);

                result.put(detail.getRessId(), balances);
            }

        }
        return result;
    }

    public static HashMap<Integer, HashMap<Boolean, Long>> findShippingBalanceForPlanetSorted(int planetId, ETradeRouteType type) {
        HashMap<Integer, HashMap<Boolean, Long>> result = new HashMap<Integer, HashMap<Boolean, Long>>();
        // ArrayList<TradeRoute> routes2 = trDAO.findByStartPlanet(planetId);
        //Search all routes where planetId is Endplanet
        ArrayList<TradeRoute> routes1 = trDAO.findByEndPlanet(planetId, type);
        for (TradeRoute r : routes1) {

            ArrayList<TradeRouteDetail> tres = Service.tradeRouteDetailDAO.getByRouteId(r.getId());
            for (TradeRouteDetail detail : tres) {
                HashMap<Boolean, Long> balances = result.get(detail.getRessId());
                if (balances == null) {
                    balances = new HashMap<Boolean, Long>();
                }
                Long balance = balances.get(detail.getReversed());

                if (balance == null) {
                    balance = 0l;
                }
                balance += detail.getLastTransport();
                balances.put(detail.getReversed(), balance);
                result.put(detail.getRessId(), balances);
            }

        }

        //Search all Transport-routes where planetId is Startplanet
        //If a planet has outgoing Traderoutes the ressources has already been decremented from the stock
        //Therefore the outgoing amount should not be added to the tradebalance of the ressource
        if (type.equals(ETradeRouteType.TRANSPORT)) {
            ArrayList<TradeRoute> routes2 = trDAO.findByStartPlanet(planetId, ETradeRouteType.TRANSPORT);
            for (TradeRoute r : routes2) {

                TradeRouteExt tre = new TradeRouteExt(r);
                ArrayList<TradeRouteDetail> tres = tre.getRouteDetails();
                for (TradeRouteDetail detail : tres) {
                    HashMap<Boolean, Long> balances = result.get(detail.getRessId());
                    if (balances == null) {
                        balances = new HashMap<Boolean, Long>();
                    }
                    Long balance = balances.get(!detail.getReversed());

                    if (balance == null) {
                        balance = 0l;
                    }
                    balance += detail.getLastTransport();

                    balances.put(!detail.getReversed(), balance);

                    result.put(detail.getRessId(), balances);
                }

            }
        }
        return result;
    }

    public static HashMap<Integer, HashMap<Integer, HashMap<Boolean, Long>>> findTradeBalanceForUserSorted(int userId) {
        ArrayList<Integer> planets = new ArrayList<Integer>();
        for (PlayerPlanet pp : ppDAO.findByUserId(userId)) {
            planets.add(pp.getPlanetId());
        }
        HashMap<Integer, HashMap<Integer, HashMap<Boolean, Long>>> result = new HashMap<Integer, HashMap<Integer, HashMap<Boolean, Long>>>();

        for (TradeRoute tr : (ArrayList<TradeRoute>) trDAO.findAll()) {
            TradeRouteExt tre = new TradeRouteExt(tr);
            if (!planets.contains(tr.getStartPlanet()) && !planets.contains(tr.getTargetPlanet())) {
                continue;
            }
            if (planets.contains(tr.getTargetPlanet())) {
                HashMap<Integer, HashMap<Boolean, Long>> planetBalance = result.get(tr.getTargetPlanet());
                if (planetBalance == null) {
                    planetBalance = new HashMap<Integer, HashMap<Boolean, Long>>();
                }
                for (TradeRouteDetail trd : tre.getRouteDetails()) {
                    HashMap<Boolean, Long> balances = planetBalance.get(trd.getRessId());
                    if (balances == null) {
                        balances = new HashMap<Boolean, Long>();
                    }
                    Long balance = balances.get(trd.getReversed());

                    if (balance == null) {
                        balance = 0l;
                    }
                    balance += trd.getLastTransport();
                    balances.put(trd.getReversed(), balance);
                    planetBalance.put(trd.getRessId(), balances);
                }
                result.put(tr.getTargetPlanet(), planetBalance);
            }
            if (planets.contains(tr.getStartPlanet())) {
                HashMap<Integer, HashMap<Boolean, Long>> planetBalance = result.get(tr.getStartPlanet());
                if (planetBalance == null) {
                    planetBalance = new HashMap<Integer, HashMap<Boolean, Long>>();
                }
                ArrayList<TradeRouteDetail> tres = tre.getRouteDetails();
                for (TradeRouteDetail trd : tres) {
                    //If its a Trade it has already been subtracted from the stock
                    //So it should not be shown in the balance
                    /*
                     *  !Planet A! => Planet B 
                     * 
                     */
                    if (tr.getType().equals(ETradeRouteType.TRADE) && !trd.getReversed()) {
                        continue;
                    }
                    HashMap<Boolean, Long> balances = planetBalance.get(trd.getRessId());
                    if (balances == null) {
                        balances = new HashMap<Boolean, Long>();
                    }
                    Long balance = balances.get(!trd.getReversed());

                    if (balance == null) {
                        balance = 0l;
                    }
                    balance += trd.getLastTransport();

                    balances.put(!trd.getReversed(), balance);

                    planetBalance.put(trd.getRessId(), balances);
                }
                result.put(tr.getStartPlanet(), planetBalance);
            }


        }
        return result;
    }
}
