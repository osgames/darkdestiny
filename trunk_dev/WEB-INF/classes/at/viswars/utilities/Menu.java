package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.view.html.body;
import at.viswars.view.html.html;
import at.viswars.view.html.table;
import at.viswars.view.menu.LeftMenu;
import at.viswars.view.menu.RessourceEntity;
import at.viswars.view.menu.StatusBar;
import at.viswars.view.menu.StatusBarData;
import at.viswars.service.Service;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author Dreloc
 */
public class Menu extends Service {
    //Testfile

    File htmlFile;
    FileWriter f1;
    //Hyperlink Table und Status die der JSP Seite hinzgef&uuml;gt werden
    private table leftMenuTable;
    private table statusBarTable;
    body menuBody = new body();
    //Die Htmlseite
    private html menuHtml;
    // Global variables need for data retrieval
    private int userId;
    private int planetId;
    private ProductionResult pr;
    private ProductionResult prFuture;
    private ExtPlanetCalcResult epcr;
    boolean isAdmin;

    public Menu(int subCategory, int userId, int planetId, boolean isAdmin) {
        try {
            this.userId = userId;
            this.planetId = planetId;


            /**
             *
             * @TODO Laden der Daten aus der MenuUtilities.java
             *
             */
            // sbd = buildTestData(rmd);
            menuHtml = new html();

            /**
             *
             *  Erstellen der Html-Tabelle des Linken Men&uuml;s und der Statusbar
             *
             */
            LeftMenu leftMenu = new LeftMenu(subCategory, userId, isAdmin);
            leftMenuTable = leftMenu.getLeftMenuTable();


            /**
             *
             * Hinzuf&uuml;gen der Tabelle zum Body und zur Html
             *
             *
             */
            menuBody.addTable(leftMenu.getLeftMenuTable());
            menuHtml.setBody(menuBody);

            //   makeFile();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Menu: ", e);
        }

    }

    public void buildStatusBar() {

        PlanetCalculation pc = null;
        try {
            pc = new PlanetCalculation(planetId);
        } catch (Exception ex) {
            DebugBuffer.writeStackTrace("Error in Menu while building Statusbar: ",ex);
        }

        pr = pc.getPlanetCalcData().getProductionData();
        prFuture = pc.getProductionDataFuture();
        epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

        MenuUtilities mu = new MenuUtilities(this);
        StatusBarData sbd = new StatusBarData();

        // Logger.getLogger().write("In buildStatusBar: "+planetId);

        MenuUtilities.addRessources(sbd);
        sbd.setPlanetId(planetId);
        mu.loadStatusBarData(sbd);
        StatusBar statusBar = new StatusBar(sbd, userId);
        statusBarTable = statusBar.getStatusBarTable();


        menuBody.addTable(statusBar.getStatusBarTable());
    }

    /**
     * 
     * Diese Funktion wird nicht wirklich verwendet, da in der 
     * newMenu.jsp 'nur' die Tables hinzugef&uuml;gt werden und nicht die Ganze
     * htmlSeite
     * 
     * @return
     */
    public html getMenuHtml() {
        return menuHtml;
    }

    public void setMenuHtml(html menuHtml) {
        this.menuHtml = menuHtml;
    }

    /**
     * Testklasse
     */
    // FIXME unused!
    private StatusBarData buildTestData(StatusBarData rmd) {
        rmd = new StatusBarData();


        rmd = MenuUtilities.addRessources(rmd);

        rmd.getMessages().setAMessageReceived(true);

        RessourceEntity ressource = rmd.getRessources().get(0);
        ressource.getStorage().setStorageCapicity(200000);
        ressource.getStorage().setStorageStock(100000);

        RessourceEntity ressource1 = rmd.getRessources().get(1);
        ressource1.getStorage().setStorageCapicity(400000);
        ressource1.getStorage().setStorageStock(50000);
        ressource1.getProductionUsage().setProductionNow(400);
        ressource1.getProductionUsage().setUsageNow(4000);


        RessourceEntity ressource2 = rmd.getRessources().get(2);
        ressource2.getStorage().setStorageCapicity(2400000);
        ressource2.getStorage().setStorageStock(2000000);

        RessourceEntity ressource3 = rmd.getPopulationFood().getFood();
        ressource3.getStorage().setStorageCapicity(2400000);
        ressource3.getStorage().setStorageStock(2000000);
        rmd.getPopulationFood().getFood().getProductionUsage().setProductionNow(2000000);
        rmd.getPopulationFood().getFood().getProductionUsage().setUsageNow(1000000);
        rmd.getPopulationFood().setPlanetPopulation((long) 238012389);

        rmd.getEnergy().getEnergy().getProductionUsage().setProductionNow(1000);
        rmd.getEnergy().getEnergy().getProductionUsage().setProductionLater(1000);
        rmd.getEnergy().getEnergy().getProductionUsage().setUsageNow(500);
        rmd.getEnergy().getEnergy().getProductionUsage().setUsageLater(1500);


        ressource2.getProductionUsage().setProductionNow(40000);
        ressource2.getProductionUsage().setUsageNow(4000);
        return rmd;
    }

    /**
     * Testklasse
     */
    // FIXME unused!
    private void makeFile() {
        htmlFile = new File("menutest.html");
        try {
            f1 = new FileWriter(htmlFile);
            f1.write(menuHtml.toString());
            f1.close();

        } catch (Exception e) {
        }

    }

    public table getLeftMenuTable() {
        return leftMenuTable;
    }

    public void setLeftMenuTable(table leftMenuTable) {
        this.leftMenuTable = leftMenuTable;
    }

    public table getStatusBarTable() {
        return statusBarTable;
    }

    public void setStatusBarTable(table statusBarTable) {
        this.statusBarTable = statusBarTable;
    }

    public ProductionResult getPr() {
        if (pr == null) {
            PlanetCalculation pc = null;
            try {
                pc = new PlanetCalculation(planetId);
            } catch (Exception ex) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Error in Menu while building Statusbar");
            }

            pr = pc.getPlanetCalcData().getProductionData();
            prFuture = pc.getProductionDataFuture();
            epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        }
        return pr;
    }

    public int getUserId() {
        return userId;
    }

    public int getPlanetId() {
        return planetId;
    }

    public ProductionResult getPrFuture() {
        if (prFuture == null) {
            PlanetCalculation pc = null;
            try {
                pc = new PlanetCalculation(planetId);
            } catch (Exception ex) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Error in Menu while building Statusbar");
            }

            pr = pc.getPlanetCalcData().getProductionData();
            prFuture = pc.getProductionDataFuture();
            epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        }
        return prFuture;
    }

    /**
     * @return the epcr
     */
    public ExtPlanetCalcResult getEpcr() {
        if (epcr == null) {
            PlanetCalculation pc = null;
            try {
                pc = new PlanetCalculation(planetId);
            } catch (Exception ex) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Error in Menu while building Statusbar");
            }

            pr = pc.getPlanetCalcData().getProductionData();
            prFuture = pc.getProductionDataFuture();
            epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        }
        return epcr;
    }
}
