/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.GameUtilities;
import at.viswars.Logger.Logger;
import at.viswars.Threading.CopyJob;
import at.viswars.Threading.ThreadController;
import at.viswars.dao.AllianceDAO;
import at.viswars.dao.AllianceMemberDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.EmbassyDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.UserDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.dao.ViewTableDAO;
import at.viswars.diplomacy.UserShareEntry;
import at.viswars.model.Alliance;
import at.viswars.model.AllianceMember;
import at.viswars.model.DiplomacyRelation;
import at.viswars.model.Planet;
import at.viswars.model.ViewTable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Dreloc
 */
public class ViewTableUtilities {

    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static EmbassyDAO eDAO = (EmbassyDAO) DAOFactory.get(EmbassyDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);

    public void addSystem(int userId, int systemId, int time) {
        ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
        ArrayList<ViewTable> vtList = new ArrayList<ViewTable>();

        for (Planet p : pList) {
            ViewTable vt = new ViewTable();
            vt.setUserId(userId);
            vt.setSystemId(systemId);
            vt.setPlanetId(p.getId());
            vt.setLastTimeVisited(time);
            vt.setMostRecent(true);
            vtList.add(vt);
        }
        //Create fake entry
        if (pList.isEmpty()) {
            ViewTable vt = new ViewTable();
            vt.setUserId(userId);
            vt.setSystemId(systemId);
            vt.setPlanetId(0);
            vt.setLastTimeVisited(time);
            vt.setMostRecent(true);
            vtList.add(vt);
        }

        vtDAO.insertAll(vtList);
    }

    //Refresh for a single user
    public static synchronized void addOrRefreshSystemForUser(int userId, int systemId, int time) {
        try {
            ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
            ArrayList<ViewTable> vtList = new ArrayList<ViewTable>();

            ArrayList<ViewTable> addList = new ArrayList<ViewTable>();
            ArrayList<ViewTable> updList = new ArrayList<ViewTable>();
            
            vtList = vtDAO.findByUserAndSystem(userId, systemId);
            if (vtList.isEmpty()) {
                for (Planet p : pList) {
                    boolean found = false;
                    ViewTable vtAct = null;
                    
                    for (ViewTable vt : vtList) {
                        if (vt.getPlanetId() == p.getId()) {
                            vtAct = vt;                        
                            found = true;
                            break;
                        }
                    }
                    
                    if (found) {
                        if (vtAct.getLastTimeVisited() > time) {
                            continue;
                        } else {
                            vtAct.setLastTimeVisited(Math.max(vtAct.getLastTimeVisited(), time));
                            updList.add(vtAct);
                        }                        
                    } else {
                        ViewTable vt = new ViewTable();
                        vt.setUserId(userId);
                        vt.setSystemId(systemId);
                        vt.setPlanetId(p.getId());
                        vt.setLastTimeVisited(time);
                        vt.setMostRecent(true);
                        addList.add(vt);
                    }
                }
                
                //Create fake entry
                if (pList.isEmpty()) {
                    ViewTable vt = new ViewTable();
                    vt.setUserId(userId);
                    vt.setSystemId(systemId);
                    vt.setPlanetId(0);
                    vt.setLastTimeVisited(time);
                    vt.setMostRecent(true);
                    addList.add(vt);
                }                               
            } else {
                for (Iterator<ViewTable> vtIt = vtList.iterator(); vtIt.hasNext();) {
                    ViewTable vt = vtIt.next();

                    if (vt.getLastTimeVisited() > time) {
                        vtIt.remove();
                        continue;
                    } else {
                        vt.setLastTimeVisited(Math.max(vt.getLastTimeVisited(), time));
                    }
                }

                updList.addAll(vtList);
            }
            
            if (!addList.isEmpty()) vtDAO.insertAll(addList);
            if (!updList.isEmpty()) vtDAO.updateAll(updList);
        } catch (Exception e) {
            DebugBuffer.error("System " + systemId + " could not be updated for user " + userId);
        }
    }

    /**
     *  MAIN - REFRESH FUNCTION
     * Refresh System for user and his friends
     *
     */
    public static void addOrRefreshSystem(int userId, int systemId, int time) {
        ArrayList<UserShareEntry> useList = DiplomacyUtilities.getAffectedShareUsers(userId);

        ArrayList<Integer> users = new ArrayList<Integer>();
        users.add(userId);

        for (UserShareEntry use : useList) {
            for (Integer user : use.getUsers()) {
                if (user == userId) {
                    continue;
                }

                Logger.getLogger().write("Updating Viewtable for User " + user);
                users.add(user);
            }
        }

        // addOrRefreshSystemForUser(userId, systemId, GameUtilities.getCurrentTick2());

        Logger.getLogger().write("Requesting Update for system " + systemId);
        CopyJob cj = new CopyJob(users, systemId, GameUtilities.getCurrentTick2());
        ThreadController.getViewTableThread().addJob(cj);
    }

    /*
     * MAINFUNCTION FOR COPYING VIEWTABLE ENTRIES
     * USE OTHERS AND DIE!
     *
     */
    public static boolean copyEntries(int fromId, int toId, Direction direction, boolean mutual) {
        boolean result = false;

        if (true) {
            Logger.getLogger().write("######## COPY VIEWTABLE ENTRIES #######");
            Logger.getLogger().write("### FROM => TO : " + fromId + " => " + toId + "");
            Logger.getLogger().write("### DIRECTION : " + direction);
            Logger.getLogger().write("### MUTUAL COPY? : " + mutual);
        }

        if (direction.equals(Direction.USER_TO_USER)) {


            // Copy fromId => toId
            result = copyEntriesUserToUser(fromId, toId, false);
            if (mutual) {
                //Copy toId => fromId
                result = copyEntriesUserToUser(toId, fromId, false);
                //Copy from Ally to User
            }

        } else if (direction.equals(Direction.USER_TO_ALLIANCE)) {
            result = copyEntriesUserToAlliance(fromId, toId);
            //Copy from User to Ally (e.g.: Trial joins Alliance)
            if (mutual) {
                result = copyEntriesAllianceToUser(toId, fromId);
                //Copy from Ally to User
            }
        } else if (direction.equals(Direction.ALLIANCE_TO_USER)) {
            result = copyEntriesAllianceToUser(fromId, toId);
            //Copy from Ally to User (e.g.: Promoting Trial user
            if (mutual) {
                result = copyEntriesUserToAlliance(toId, fromId);
            }

        } else if (direction.equals(Direction.ALLIANCE_TO_ALLIANCE)) {

            result = copyEntriesAllianceToAlliance(fromId, toId);
            if (mutual) {
                result = copyEntriesAllianceToAlliance(toId, fromId);
            }
        }
        return result;
    }

    private static boolean copyEntriesAllianceToAlliance(int fromId, int toId) {
        Alliance a1 = aDAO.findById(fromId);
        Alliance a2 = aDAO.findById(toId);
        AllianceMember am1 = amDAO.findByAllianceIdNotTrial(a1.getId()).get(0);
        AllianceMember am2 = amDAO.findByAllianceIdNotTrial(a2.getId()).get(0);
        if (am1 == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "In alliance : " + a1.getName() + " are only Trial members");
        }
        if (am2 == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "In alliance : " + a2.getName() + " are only Trial members");
        }

        DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRelationAllianceToAlliance(fromId, toId);

        Logger.getLogger().write("Retrieved DR: DTID=" + dr.getDiplomacyTypeId() + " ACK=" + dr.getAcknowledged() + " TYPE=" + dr.getType() + " SM=" + dr.getSharingMap());
        if (!dr.getSharingMap()) {
            Logger.getLogger().write("Ignore share try forbidden!");
            return false;
        }

        return copyEntriesUserToUser(am1.getUserId(), am2.getUserId(),true);

    }

    private static boolean copyEntriesUserToAlliance(int fromId, int toId) {
        Alliance a = aDAO.findById(toId);
        AllianceMember am = amDAO.findByAllianceIdNotTrial(a.getId()).get(0);
        if (am == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "In alliance : " + a.getName() + " are only Trial members");
        }
        return copyEntriesUserToUser(fromId, am.getUserId(),false);

    }

    private static boolean copyEntriesAllianceToUser(int fromId, int toId) {
        Alliance a = aDAO.findById(fromId);
        AllianceMember am = amDAO.findByAllianceIdNotTrial(a.getId(), toId).get(0);
        if (am == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "In alliance : " + a.getName() + " are only Trial members");
        }
        return copyEntriesUserToUser(am.getUserId(), toId, false);

    }

    private static boolean copyEntriesUserToUser(int fromId, int toId, boolean forced) {
        //Copy fromId => toId
        Logger.getLogger().write("Retrieved DR: " + fromId + "=>" + toId);
        if (!forced) {
            DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRel(fromId, toId);

            Logger.getLogger().write("Retrieved DR: DTID=" + dr.getDiplomacyTypeId() + " ACK=" + dr.getAcknowledged() + " TYPE=" + dr.getType() + " SM=" + dr.getSharingMap());
            if (!dr.getSharingMap()) {
                Logger.getLogger().write("Ignore share try forbidden!");
                return false;
            }
        }
        /*
        if (!eDAO.hasEmbassy(fromId, toId)) {
            DebugBuffer.addLine(DebugLevel.WARNING, "Ignore share try forbidden - NO EMBASSY!");
            return false;
        }*/

        ArrayList<UserShareEntry> shares = DiplomacyUtilities.getAffectedShareUsers(toId);
        ArrayList<ViewTable> vtList = vtDAO.findByUser(fromId);

        HashMap<Integer, Integer> procSysTime = new HashMap<Integer, Integer>();

        for (ViewTable vt : vtList) {
            if (!procSysTime.containsKey(vt.getSystemId())) {
                procSysTime.put(vt.getSystemId(), vt.getLastTimeVisited());
            } else {
                continue;
            }
        }

        ArrayList<Integer> users = new ArrayList<Integer>();
        for (UserShareEntry use : shares) {
            for (Integer user : use.getUsers()) {
                if (user != fromId);
                Logger.getLogger().write("Add user " + user);
                users.add(user);
            }
        }

        ArrayList<CopyJob> cjBatch = new ArrayList<CopyJob>();
        for (Map.Entry<Integer, Integer> vtEntry : procSysTime.entrySet()) {
            CopyJob cj = new CopyJob(users, vtEntry.getKey(), vtEntry.getValue());
            cjBatch.add(cj);
        }
        ThreadController.getViewTableThread().addJobBatch(cjBatch);

        return true;
    }

    public enum Direction {

        ALLIANCE_TO_ALLIANCE, USER_TO_ALLIANCE, ALLIANCE_TO_USER, USER_TO_USER
    }

    class ViewTableKey {

        private final int systemId;
        private final int planetId;

        public ViewTableKey(int systemId, int planetId) {
            this.systemId = systemId;
            this.planetId = planetId;
        }

        /**
         * @return the systemId
         */
        public int getSystemId() {
            return systemId;
        }

        /**
         * @return the planetId
         */
        public int getPlanetId() {
            return planetId;
        }
    }
}
