/*
 * PopulationUtilities.java
 *
 * Created on 04. Dezember 2005, 13:45
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package at.viswars.utilities;

import at.viswars.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.enumeration.EPlanetRessourceType;

import at.viswars.service.Service;
import at.viswars.model.Construction;
import at.viswars.model.Galaxy;
import at.viswars.model.Planet;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.PlanetRessource;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Production;
import at.viswars.model.Ressource;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.result.FoodCoverageResult;
import at.viswars.result.MoraleResult;
import at.viswars.result.GrowthResult;
import at.viswars.viewbuffer.HomeSystemBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class PopulationUtilities extends Service {

    @Deprecated
    public static long getMaxPopulation(int planetId) {
        Planet p = planetDAO.findById(planetId);
        long maxPopulation = 0;
        if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_A) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_B)) {
            maxPopulation = 0;
            return maxPopulation;
        }

        int modifier = 100;
        int tmpDiff = 0;

        if (p.getAvgTemp() < 15) {
            tmpDiff = 15 - p.getAvgTemp();
        } else if (p.getAvgTemp() > 15) {
            tmpDiff = p.getAvgTemp() - 15;
        }

        double malus = 0; // max = 100;

        if ((tmpDiff > 12) && (tmpDiff <= 25)) {
            malus = Math.pow(12.5d, 2.7) / 20d;
            malus += 50d - Math.pow(25 - tmpDiff, 2.7d) / 20d;
        } else if (tmpDiff <= 12) {
            malus = Math.pow(tmpDiff, 2.7) / 20d;
        } else if (tmpDiff > 25) {
            malus = 100;
        }

        modifier = 100 - (int) malus;

        if (modifier < 0) {
            modifier = 0;
        }
        if (p.isLebensfeindlich()) {
            modifier = 0;
        }

        maxPopulation = (long) (((float) p.getSurface().getSurface() * (float) 45) / (float) 100 * modifier);
        if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {
            maxPopulation = (long) (maxPopulation / 2);
        } else if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G)) {
            maxPopulation = (long) ((float) maxPopulation / 3.5f);
        }




        long addPopulation = 0;

        for (PlanetConstruction pc : planetConstructionDAO.findByPlanetId(planetId)) {

            Construction c = constructionDAO.findById(pc.getConstructionId());

            addPopulation += c.getPopulation() * pc.getNumber();
        }


        maxPopulation += addPopulation;

        return maxPopulation;
    }

    public static GrowthResult getGrowth(int planetId) {
        float changevalueGrowth = 0;
        float maxGrowth = 5;
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        FoodCoverageResult fcr = getFoodCoverage(planetId);
        int morale = pp.getMoral();
        // Increase Maximum Population Growth by 2 % per 30 extra Morale
        if (morale > 100) {
            maxGrowth += (float) 0.02 * ((float) (morale - 100) * ((float) 100 / (float) 30));
        }

        if (morale < 50) {
            maxGrowth -= (50 - morale) * 0.15;
        }

        if (fcr.getFoodCoverage() < 100) {
            changevalueGrowth -= (100 - fcr.getFoodCoverage()) * (float) 0.01;
        } else {
            changevalueGrowth += 1;
        }

        float minGrowth = (-100 + fcr.getFoodCoverage());
        if (minGrowth > 0) {
            minGrowth = 0;
        }
        if (minGrowth > maxGrowth) {
            minGrowth = maxGrowth;
        }

        return new GrowthResult(minGrowth, maxGrowth, changevalueGrowth);
    }

    public static MoraleResult getMorale(int planetId) {
        Planet p = planetDAO.findById(planetId);
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        double lowFoodFactor = 1d;
//        changevalueMorale = 0;
        int maxMorale = 100;
        int moraleBonus = 0;
        int estimatedMorale = 100;
        // changevalueMorale += 1;
        float foodCoverage = getFoodCoverage(planetId).getFoodCoverage();
        // DebugBuffer.addLine("ESTIMATED START = " + estimatedMorale);
        if (foodCoverage >= 100) {
            float foodBonus = (int) ((getFoodCoverage(planetId).getFoodCoverage() - 100) / 2);
            if (foodBonus > 5) {
                foodBonus = 5;
            }
            estimatedMorale += foodBonus;
            maxMorale += foodBonus;
            // DebugBuffer.addLine("ADD BONUS BY FOOD = " + foodBonus);
        } else if (foodCoverage <= 100) {
            float foodMalus = (int) (100f - foodCoverage);
            estimatedMorale -= foodMalus;

            if (foodMalus > 25) {
                lowFoodFactor = 0d;
            } else {
                lowFoodFactor = 1d - (1d / 25d * foodMalus);
            }
        }

        if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M)
                || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G)
                || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {
            moraleBonus = 30;

            if (!(p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_A) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_B))) {
                int freeSurface = (int) (((float) 100 / (float) p.getSurfaceSquares()) * (float) p.getSurface().getFreeSurface());

                if (freeSurface < 30) {
                    moraleBonus = (int) (moraleBonus * (1d / 30d * freeSurface));
                }

                if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {

                    PlanetConstruction pc = planetConstructionDAO.findBy(planetId, Construction.ID_ACCOMODATIONUNIT);
                    if (pc != null) {
                        int parkBuildings = pc.getNumber();
                        if (parkBuildings > 0) {
                            double effPark = 100d / (double) pp.getPopulation() * ((double) parkBuildings * 250000000d);
                            if (effPark > 200) {
                                effPark = 200;
                            }

                            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M)) {
                                moraleBonus += 50d / 100d * effPark;
                            } else {
                                moraleBonus += 25d / 100d * effPark;
                            }
                        }
                    }
                }

                if (moraleBonus > 50) {
                    moraleBonus = 50;
                }
            }

            maxMorale += moraleBonus;

            estimatedMorale += moraleBonus * lowFoodFactor;
            // DebugBuffer.addLine("ADD BONUS BY SURFACE = " + moraleBonus);
        }

        if (pp.getTax() < 25) {
            estimatedMorale += 25 - pp.getTax();
        } else if (pp.getTax() > 25) {
            estimatedMorale -= (pp.getTax() - 25) * 2;
        }
        return new MoraleResult(pp.getMoral(), moraleBonus, maxMorale, estimatedMorale);
    }

    public static int getTaxIncome(int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);

        int taxIncome = (int) (((float) pp.getPopulation() * (float) pp.getTax()) / (float) GameConstants.GLOBAL_MONEY_MODIFICATOR);

        float taxFactor = 1f;

        if (planetConstructionDAO.findBy(planetId, Construction.ID_LOCALADMINISTRATION) != null) {
            taxFactor += 0.05f;
        }
        if (planetConstructionDAO.findBy(planetId, Construction.ID_PLANETADMINISTRATION) != null) {
            taxFactor += 0.1f;
        }
        if (planetConstructionDAO.findBy(planetId, Construction.ID_PLANETGOVERNMENT) != null) {
            taxFactor += 0.15f;
        }
        if (pp.getHomeSystem()) {
            taxFactor += 0.2f;
        }

        taxIncome = (int) ((float) taxIncome * taxFactor);

        if (pp.getMoral() < 60) {
            taxIncome = (int) ((float) taxIncome / (float) 60 * (float) pp.getMoral());
        }

        return taxIncome;
    }

    public static FoodCoverageResult getFoodCoverage(int planetId) {

        float foodCoverage = 0;

        long foodIncrease = 0;
        long foodDecrease = 0;

        for (Production p : productionDAO.findByRessourceId(Ressource.FOOD)) {
            if (p.getDecinc() == Production.PROD_DECREASE || p.getDecinc() == Production.PROD_INCREASE) {
                PlanetConstruction pc = planetConstructionDAO.findBy(planetId, p.getConstructionId());
                if (pc == null) {
                    continue;
                }
                int count = pc.getNumber();
                if (p.getDecinc() == Production.PROD_DECREASE) {
                    foodDecrease += count * p.getQuantity();
                } else if (p.getDecinc() == Production.PROD_INCREASE) {
                    foodIncrease += count * p.getQuantity();
                }
            }

        }
        //Miteinberechnen der Bevölkerung
        foodDecrease += (long) playerPlanetDAO.findByPlanetId(planetId).getPopulation() / 1000;
        //Miteinberechnen der Effizienz
        try {
            PlanetCalculation pc = new PlanetCalculation(planetId);
            ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

            foodIncrease = (int) (foodIncrease * epcr.getEffAgriculture());
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Couldnt initialize PlanetCalculation");
        }
        long storage = 0;
        PlanetRessource food = planetRessourceDAO.findBy(planetId, Ressource.FOOD, EPlanetRessourceType.INSTORAGE);
        if (food != null) {
            storage = food.getQty();
        }

        float coverageByStock = (float) storage / (float) (foodDecrease - foodIncrease);
        if (Float.isInfinite(coverageByStock) || Float.isNaN(coverageByStock)) {
            coverageByStock = (float) storage / 1f;
        }
        if (coverageByStock <= 0) {

            if (foodDecrease > 0) {
                foodCoverage = ((float) 100 / foodDecrease * foodIncrease) + Math.abs(storage / (float) foodDecrease);
            }
            // if (pd.getUserId() == 30) Logger.getLogger().write("FC[PF](1)->"+foodcoverage);
        } else {
            if (coverageByStock < 1) {
                if (foodDecrease > 0) {
                    foodCoverage = (float) 100 / foodDecrease * (float) (foodIncrease + coverageByStock);
                }
                // if (pd.getUserId() == 30) Logger.getLogger().write("FC[PF](2)->"+foodcoverage);
            } else {
                foodCoverage = 100f + coverageByStock;
                // if (pd.getUserId() == 30) Logger.getLogger().write("FC[PF](3)->"+foodcoverage);
            }
        }

        float foodCoverageNoStock = (float) 100 / (float) foodDecrease * (float) foodIncrease;
        return new FoodCoverageResult(foodCoverage, foodCoverageNoStock);
        // DebugBuffer.addLine("Food coverage = " + foodcoverage);
    }

    public static void updateRiots(ArrayList<PlayerPlanet> ppList) {
        for (PlayerPlanet pp : ppList) {
            double distance = getDistanceToHomeSystem(pp.getPlanetId());
            
            double growth = getGrowthRiots(distance, pp.getPlanetId());
            double riots = pp.getUnrest();
            pp.setUnrest(Math.max((riots - Math.abs(growth)), 0d));
        }
    }
    public static double getDistanceToHomeSystem(int planetId) {

        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);

        if (pp == null) {
            DebugBuffer.warning("Tried to calculate DistanceToHomeSystem for non existing playerplanet " + planetId);
            return 0d;
        }

        int userId = pp.getUserId();
        Planet p = planetDAO.findById(pp.getPlanetId());
        at.viswars.model.System s = HomeSystemBuffer.getForUser(userId);
        if (s == null) {
            PlayerPlanet ppTmp = playerPlanetDAO.findHomePlanetByUserId(userId);
            if(ppTmp == null){
                return 100d;
            }
            Planet pTmp = planetDAO.findById(ppTmp.getPlanetId());
            s = systemDAO.findById(pTmp.getSystemId());
            HomeSystemBuffer.setForUser(userId, s);
        }
        if (s.getId() == p.getSystemId()) {
            return 0;
        } else {
            at.viswars.model.System s2 = systemDAO.findById(p.getSystemId());

            double distance = Math.sqrt(Math.pow(Math.abs(s.getX() - s2.getX()), 2) + Math.pow(Math.abs(s.getY() - s2.getY()), 2));
            distance = Math.round(distance);
            return distance;
        }

    }

    public static double getGrowthRiots(double distanceToHomeSys, int planetId) {
        
        
        if(distanceToHomeSys == 0){
            return 200d;
        }
        return (double) (200d / distanceToHomeSys + 0.25d)/10d;
    }
    

    public static String getChange(int planetId) {
        String change = " ";

        // DebugBuffer.addLine("morale="+morale+"  estimated="+estimatedMorale);
        MoraleResult mr = getMorale(planetId);
        int diff = (int) (mr.getMorale() - mr.getEstimatedMorale());
        // DebugBuffer.addLine("DIFF = " + diff);

        if (diff > 0) {
            change += "<FONT style=\"font-size:14px\" color=\"red\"><B>-";
            if (diff > 15) {
                change += "-";
            }
            change += "</B></FONT> ";
        } else if (diff < 0) {
            change += "<FONT color=\"green\"><B>+";
            if (diff < -15) {
                change += "+";
            }
            change += "</B></FONT> ";
        }

        return change;
    }
}
