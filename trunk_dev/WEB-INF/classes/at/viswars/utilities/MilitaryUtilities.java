/*
 * MilitaryUtilities.java
 *
 * Created on 03. Dezember 2007, 23:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.ships.ShipStorage;
import at.viswars.ships.ShipData;
import at.viswars.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.admin.module.AttributeTree;
import at.viswars.admin.module.ModuleAttributeResult;
import at.viswars.dao.ConstructionModuleDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ShipTypeAdjustmentDAO;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.diplomacy.combat.CombatGroupEntry;
import at.viswars.diplomacy.combat.CombatGroupResolver;
import at.viswars.diplomacy.combat.CombatGroupResult;
import at.viswars.enumeration.EAttribute;
import at.viswars.enumeration.EDefenseType;
import at.viswars.enumeration.EShipType;
import at.viswars.fleet.FleetData;
import at.viswars.fleet.LoadingInformation;
import at.viswars.military.DefensePlanetEntry;
import at.viswars.model.Chassis;
import at.viswars.model.Construction;
import at.viswars.model.ConstructionModule;
import at.viswars.model.DiplomacyType;
import at.viswars.model.FleetLoading;
import at.viswars.model.Module;
import at.viswars.model.Planet;
import at.viswars.model.PlayerFleet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.PlayerTroop;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipTypeAdjustment;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.service.Service;
import at.viswars.ships.Reactor;
import at.viswars.ships.Shield;
import at.viswars.ships.ShipDesignDetailed;
import at.viswars.ships.SimpleModule;
import at.viswars.ships.Weapon;
import at.viswars.spacecombat.*;
import at.viswars.spacecombat.CombatGroupFleet.UnitTypeEnum;
import at.viswars.spacecombat.helper.ShieldDefinition;
import java.util.*;

public class MilitaryUtilities extends Service {
 private static ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);    
 private static ConstructionModuleDAO cmDAO = (ConstructionModuleDAO) DAOFactory.get(ConstructionModuleDAO.class);


    public ArrayList<DefensePlanetEntry> getDefensePlanetEntries(int userId) {
        ArrayList<DefensePlanetEntry> defList = new ArrayList<DefensePlanetEntry>();

        // Get all own planets
        try {
            // Statement stmt = DbConnect.createStatement();
            // PreparedStatement pstmt1 = DbConnect.prepareStatement("SELECT userId FROM playerfleet WHERE planetId=?");
            //  PreparedStatement pstmt2 = DbConnect.prepareStatement("SELECT COUNT(planetId) FROM playertroops WHERE planetId=?");
            //   PreparedStatement pstmt3 = DbConnect.prepareStatement("SELECT DISTINCT(type) FROM planetdefense WHERE planetId=? GROUP BY type");
            //  PreparedStatement pstmt4 = DbConnect.prepareStatement("SELECT userId FROM playerfleet WHERE planetId=0 AND systemId=?");
            // PreparedStatement pstmt5 = DbConnect.prepareStatement("SELECT DISTINCT(type) FROM planetdefense WHERE planetId=0 AND systemId=? GROUP BY type");

            ArrayList<PlayerPlanet> pps = playerPlanetDAO.findByUserIdSorted(userId);
            //ResultSet rs = stmt.executeQuery("SELECT pp.planetID, pp.name FROM playerplanet pp WHERE userID="+userId);

            HashMap<Integer,Boolean> hasScannerArray = new HashMap<Integer,Boolean>();

            for (PlayerPlanet pp : pps) {
                int systemId = planetDAO.findById(pp.getPlanetId()).getSystemId();

                DefensePlanetEntry dpe = new DefensePlanetEntry();

                dpe.setId(pp.getPlanetId());
                dpe.setSystemId(systemId);
                dpe.setName(pp.getName());

                //   pstmt1.setInt(1, rs.getInt(1));
                //   pstmt2.setInt(1,rs.getInt(1));
                //   pstmt3.setInt(1,rs.getInt(1));
                //pstmt4.setInt(1, systemId);
                //  pstmt5.setInt(1, systemId);

                // Check for Space Ships  

                for (PlayerFleet pf : playerFleetDAO.findByPlanetId(pp.getPlanetId())) {
                    if (pf.getUserId() == userId) {
                        dpe.setHasFleetDefense(true);
                        continue;
                    }

                    if (!dpe.isHasFleetDefense() && 
                            (DiplomacyUtilities.hasAllianceRelation(pf.getUserId(), userId) ||
                            DiplomacyUtilities.hasRelation(pf.getUserId(), userId, DiplomacyType.TREATY))) {
                        dpe.setHasFleetDefenseAllied(true);
                        continue;
                    } else if (!dpe.isHasEnemyFleet() && !(
                            DiplomacyUtilities.hasAllianceRelation(pf.getUserId(), userId) ||
                            DiplomacyUtilities.hasRelation(pf.getUserId(), userId, DiplomacyType.TREATY)
                            )) {
                        dpe.setHasEnemyFleet(true);
                        continue;
                    }
                }


                // Check for Ground Troops        
                if (playerTroopDAO.findByPlanetId(pp.getPlanetId()).size() > 0) {
                    dpe.setHasMilitary(true);
                }

                // Check for space stations or planetary defense
                if (planetDefenseDAO.findByPlanet(pp.getPlanetId(), EDefenseType.TURRET).size() > 0) {
                    dpe.setHasPlanetDefense(true);
                }
                if (planetDefenseDAO.findByPlanet(pp.getPlanetId(), EDefenseType.SPACESTATION).size() > 0) {
                    dpe.setHasOrbitalDefense(true);
                }


                // Check for system fleets
                for (PlayerFleet pf : playerFleetDAO.findBy(0, systemId)) {
                    if (pf.getUserId() == userId) {
                        dpe.setHasSystemFleet(true);
                        continue;
                    }

                    if (!dpe.isHasFleetDefenseAllied() && (AllianceUtilities.areAllied(pf.getUserId(), userId))) {
                        dpe.setHasSystemFleetAllied(true);
                        continue;
                    } else if (!dpe.isHasEnemySystemFleet() && (!AllianceUtilities.areAllied(pf.getUserId(), userId))) {
                        dpe.setHasEnemySystemFleet(true);
                        continue;
                    }
                }


                // Check for system space installations
                if (planetDefenseDAO.findBySystem(systemId, EDefenseType.SPACESTATION).size() > 0) {
                    dpe.setHasSystemDefense(true);
                }

                // Check for scanner Array
                if (!hasScannerArray.containsKey(systemId)) {
                    hasScannerArray.put(systemId, systemHasScannerArray(systemId));
                }

                dpe.setHasScannerArray(hasScannerArray.get(systemId));
                defList.add(dpe);
            }

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("ERROR: ", e);
        }

        return defList;
    }

    private boolean systemHasScannerArray(int systemId) {
        ArrayList<Planet> planetList = planetDAO.findBySystemId(systemId);
        for (Planet p : planetList) {
            if (planetConstructionDAO.isConstructed(p.getId(), Construction.ID_SCANNERPHALANX)) return true;
        }

        return false;
    }

    public static ArrayList<PlayerFleet> getInvadeableFleets(int userId, int planetId) {
        ArrayList<PlayerFleet> result = new ArrayList<PlayerFleet>();

        ArrayList<PlayerFleet> fleets = playerFleetDAO.findByUserIdPlanetId(userId, planetId);
        for (PlayerFleet pf : fleets) {
            Logger.getLogger().write("Check fleet " + pf.getId() + " - " + pf.getName() + "[LOC: "+pf.getSystemId()+":"+pf.getPlanetId()+"]");

            PlayerFleetExt currFleet = new PlayerFleetExt(pf.getId());
            LoadingInformation li = currFleet.getLoadingInformation();
            if (li.getCurrLoadedTroops() > 0) {
                result.add(pf);
            }
        }


        return result;
    }

    public static void invade(int fleetId) {
        PlayerFleet pf = playerFleetDAO.findById(fleetId);
        PlayerFleetExt pfExt = new PlayerFleetExt(pf.getId());
        LoadingInformation li = pfExt.getLoadingInformation();
        int planetId = pf.getPlanetId();
        int userId = pf.getUserId();

        for (ShipStorage ss : li.getLoadedStorage()) {
            if (ss.getLoadtype() == FleetLoading.LOADTYPE_TROOPS) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "Loop");
                int troopId = ss.getId();
                int count = ss.getCount();

                if (count == 0) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Continue");
                    continue;
                }
                if (playerPlanetDAO.findByPlanetId(planetId) != null) {

                    GroundCombatUtilities.checkForCombat(userId, planetId);
                }
                // Erstma
                PlayerTroop pt = playerTroopDAO.findBy(userId, planetId, troopId);
                FleetLoading fl = fleetLoadingDAO.getByFleetAndFreight(fleetId, 2, troopId);

                if (fl.getCount() == count) {
                    fleetLoadingDAO.remove(fl);
                } else {
                    fl.setCount(fl.getCount() - count);
                    fleetLoadingDAO.update(fl);
                }

                DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading1");
                if (pt != null) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading2");

                    pt.setNumber(pt.getNumber() + count);
                    playerTroopDAO.update(pt);
                } else {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading3");
                    pt = new PlayerTroop();
                    pt.setPlanetId(planetId);
                    pt.setTroopId(troopId);
                    pt.setNumber((long) count);
                    DebugBuffer.addLine(DebugLevel.DEBUG, "userId : " + pf.getUserId());
                    pt.setUserId(pf.getUserId());
                    playerTroopDAO.add(pt);
                }
            }

        }
    }

    public static boolean loadingTroopsPossible(int userId, int planetId) {
        ArrayList<PlayerTroop> ptList = playerTroopDAO.findBy(userId, planetId);
        if (ptList.isEmpty()) return false;

        for (PlayerTroop pt : ptList) {
            if (pt.getTroopId() != 0) return true;
        }

        return false;
    }

    public static boolean unloadTroopPossible(int userId, int planetId) {
        if (!DiplomacyUtilities.getDiplomacyResult(userId, playerPlanetDAO.findByPlanetId(planetId).getUserId()).isHelps()) {
            return false;
        }

        return true;
    }

    @Deprecated
    /**     
     * Please use invasionPossible in FleetService if possible
     * as it is about 3 times faster
     */
    public static boolean invasionPossible (int userId, int planetId, List<FleetData> fleetList) {
        DebugBuffer.addLine(DebugLevel.DEBUG,"Called by user " + userId + " for planet " + planetId);
        if (fleetList != null) { DebugBuffer.addLine(DebugLevel.DEBUG,"FleetCount = " + fleetList.size()); }
        
        Logger.getLogger().write("Find fleets for ("+userId+" / "+planetId+")");
        ArrayList<PlayerFleet> fleets = getInvadeableFleets(userId, planetId);
        if (fleets.size() <= 0) {
            Logger.getLogger().write("No fleets available for invasion ("+userId+" / "+planetId+")");
            return false;
        }

        ArrayList<PlayerFleet> allFleets = playerFleetDAO.findByPlanetId(planetId);
        
        /*
        boolean enemyInOrbit = false;
        for (PlayerFleet pf : allFleets) {
            if (!AllianceUtilities.areAllied(pf.getUserId(), userId) && 
                 !DiplomacyUtilities.getDiplomacyResult(userId, pf.getUserId()).isAttacks()) {
                enemyInOrbit = true;
            }
        }
        */

        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);

        if (pp == null) {
            Logger.getLogger().write("REJECT INVASION DUE TO null");
            return false;
        } else if (userId == pp.getUserId()) {
            Logger.getLogger().write("REJECT INVASION DUE TO SAME USERID");
            return false;
        /*
        } else if (enemyInOrbit) {
            Logger.getLogger().write("REJECT INVASION DUE TO ORBITAL FLEETS");
            return false;
        */
        } else if (getAttackerStrength(planetId, userId) < (getPlanetaryShieldStrength(planetId) / 2d)) {
            Logger.getLogger().write("REJECT INVASION DUE TO SHIELD");
            return false;
        } else {
            ArrayList<Integer> users = new ArrayList<Integer>();
            users.add(pp.getUserId());
            users.add(userId);

            // Logger.getLogger().write("Adding users >> " + pp.getUserId() + " and " + userId);

            CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(users, new RelativeCoordinate(0,planetId));
            /*
            for (CombatGroupEntry cg : cgr.getCombatGroups()) {
                Logger.getLogger().write("CG ID: " + cg.getGroupId() + ":");
                for (Integer p : cg.getPlayers()) {
                    Logger.getLogger().write("Player >> " + p);
                }
            }
            */
            if (!cgr.hasConflictingParties()) {
                Logger.getLogger().write("No conflict");
                return false;
            }
        }
        return true;
    }

    public static int getPlanetaryShieldStrength(int planetId) {
        ShieldDefinition planetaryShield = new ShieldDefinition();
        int PLANETARY_HU = 0;
        int PLANETARY_PA = 1;
        // Check if planet has Colony - if no skip following stuff
        if (playerPlanetDAO.findByPlanetId(planetId) == null) {
            return 0;
        }

        // Load free Energy from Planet
        try {
            PlanetCalculation pc = new PlanetCalculation(planetId);

            ProductionResult prodRes = pc.getPlanetCalcData().getProductionData();
            long freeEnergy = prodRes.getRessProduction(at.viswars.model.Ressource.ENERGY) - prodRes.getRessConsumption(at.viswars.model.Ressource.ENERGY);
            long shieldPower = 0;

            if (freeEnergy < 0) {
                freeEnergy = 0;
            }


            int shieldType = -1;

            if (planetConstructionDAO.isConstructed(planetId, Construction.ID_PLANETARY_HUESHIELD)) {
                shieldType = PLANETARY_HU;
                shieldPower = freeEnergy * 10;
            }
            if (planetConstructionDAO.isConstructed(planetId, Construction.ID_PLANETARY_PARATRONSHIELD)) {
                shieldType = PLANETARY_PA;
                shieldPower = freeEnergy * 20;
            }

            if (freeEnergy > 0) {
                if (shieldType == PLANETARY_HU) {
                    planetaryShield.setShield_HU((int) shieldPower);
                    planetaryShield.setSharedShield_HU((int) shieldPower);
                    planetaryShield.setEnergyNeed_HU((int) freeEnergy);
                    planetaryShield.setShieldEnergy((int) freeEnergy);
                } else if (shieldType == PLANETARY_PA) {
                    planetaryShield.setShield_PA((int) shieldPower);
                    planetaryShield.setSharedShield_PA((int) shieldPower);
                    planetaryShield.setEnergyNeed_PA((int) freeEnergy);
                    planetaryShield.setShieldEnergy((int) freeEnergy);
                }

                // Logger.getLogger().write("Planetary shield strength: " + planetaryShield.getShield_HU());
                // planetaryShield.setShieldEnergyPerShip(shieldEnergyConsumption);
                // planetaryShield.setShieldEnergy(shieldEnergyConsumption * count);
            }
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error in Calculation of Planetaryshield : " + e);
        }
        DebugBuffer.debug("Getting Shield for Invasioncheck vs Shield" + Math.max(planetaryShield.getShield_HU(), planetaryShield.getShield_PA()));

        return Math.max(planetaryShield.getShield_HU(), planetaryShield.getShield_PA());
    }

    public static long getAttackerStrength(int planetId, int userId) {
        long attackStr = 0l;
        for (PlayerFleet pf : playerFleetDAO.findByUserIdPlanetId(userId, planetId)) {                        
            PlayerFleetExt pfe = new PlayerFleetExt(pf.getId());
            long fleetStrength = 0l;
            
            for (ShipData sd : pfe.getShipList()) {
                long shipStrength = 0l;
                ShipDesignDetailed sdd = new ShipDesignDetailed(sd.getDesignId());
                for (Map.Entry<Integer, Integer> entries : sdd.getModuleCount().entrySet()) {
                    if (moduleDAO.findById(entries.getKey()).getType().equals(ModuleType.WEAPON)) {
                        Weapon w = sdd.getWeapon(entries.getKey());
                        //Add each weapon to shipStrength
                        shipStrength += w.getAttackStrength() * entries.getValue();
                    }
                }
                //Add Shipdesign damage for the number of ships in this fleet
                fleetStrength += sd.getCount() * shipStrength;
            }
            
            //Add each Fleet            
            DebugBuffer.debug("Add strength of "+fleetStrength+" by fleet " + pf.getName());            
            attackStr += fleetStrength;
        }
        
        // Find other fleets of users which are allied to this user
        // First loop all other fleets on this planet and gather users 
        ArrayList<Integer> users = new ArrayList<Integer>();
        
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        users.add(pp.getUserId());
        
        for (PlayerFleet pf : playerFleetDAO.findByPlanetId(planetId)) {
            DebugBuffer.debug("Found fleet "+pf.getName()+" by user " + pf.getUserId()); 
            if (!users.contains(pf.getUserId())) users.add(pf.getUserId());
        }                
        
        CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(users, new RelativeCoordinate(0,planetId));
        DebugBuffer.debug("CONFLICT??? " + cgr.hasConflictingParties());
        
        for (CombatGroupEntry cge : cgr.getCombatGroups()) {
            for (Integer tmpId : cge.getPlayers()) {
                DebugBuffer.debug("CG "+cge.getGroupId()+" contains user " + tmpId); 
            }
            
            if (cge.getPlayers().contains(userId)) {
                for (PlayerFleet pf : playerFleetDAO.findByPlanetId(planetId)) {
                    if (pf.getUserId().equals(userId)) continue;
                    
                    if (cge.getPlayers().contains(pf.getUserId())) {
                        PlayerFleetExt pfe = new PlayerFleetExt(pf.getId());
                        long fleetStrength = 0l;
                        
                        for (ShipData sd : pfe.getShipList()) {
                            long shipStrength = 0l;
                            ShipDesignDetailed sdd = new ShipDesignDetailed(sd.getDesignId());
                            for (Map.Entry<Integer, Integer> entries : sdd.getModuleCount().entrySet()) {
                                if (moduleDAO.findById(entries.getKey()).getType().equals(ModuleType.WEAPON)) {
                                    Weapon w = sdd.getWeapon(entries.getKey());
                                    //Add each weapon to shipStrength
                                    shipStrength += w.getAttackStrength() * entries.getValue();
                                }
                            }
                            //Add Shipdesign damage for the number of ships in this fleet
                            fleetStrength += sd.getCount() * shipStrength;
                        }
                        
                        //Add each Fleet
                        DebugBuffer.debug("Add strength of "+fleetStrength+" by fleet " + pf.getName());  
                        attackStr += fleetStrength;                        
                    }
                }                
            } else {
                continue;
            }
        }
        
        DebugBuffer.debug("Getting attackstrength for Invasioncheck vs Shield" + attackStr);
        return attackStr;
    }

    public BattleShipNew buildBattleShipNew(int designId, int count, int shipFleetId) {
        BattleShipNew bsn = null;
        
        try {
            bsn = new BattleShipNew(designId, shipFleetId, count);                  
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Unexpected error in new function: ", e);
        }
        
        return bsn;
    }
    
    public BattleShip buildBattleShip(int designId, int count, int shipFleetId) {
        // bs.overrideFactors(probabilityPA,minDamagePA,maxDamagePA);

        try {
            ShipDesign sd = shipDesignDAO.findById(designId);
            Chassis c = chassisDAO.findById(sd.getChassis());
            int groupingMultiplier = c.getMinBuildQty();


            // Statement stmt = DbConnect.createStatement();
            // ResultSet rs = stmt.executeQuery("SELECT m.chassisSize, sd.name, sd.primaryTarget, sd.type, sd.standardspeed, m.baseStructure, sd.chassis FROM shipdesigns sd, module m WHERE sd.id=" + designId + " AND m.id=sd.chassis");

            int chassisId = 0;

            ShipTypeAdjustment sta = staDAO.findByType(sd.getType());

            BattleShip bs = new BattleShip(designId);

            bs.setDesignId(designId);
            bs.setCount(count);
            bs.setShipFleetId(shipFleetId);

            bs.setChassisSize(sd.getChassis());
            bs.setName(sd.getName());
            // bs.setPrimaryTarget(sd.getPrimaryTarget());
            bs.setPurpose(sd.getType());
            bs.setSpeed(sd.getStandardSpeed());
            // chassisId = rs.getInt(7);


            // bs.setMaxAttackPerUnit(maxAttackMap(bs));

            // Check trough modules
            ArrayList<WeaponModule> weaponArray = new ArrayList<WeaponModule>();
            int energyProduction = 0;
            int weaponEnergyConsumption = 0;
            int shieldEnergyConsumption = 0;
            int internalEnergyConsumption = 0;
            int armorValue = 0;
            int shieldHpPS = 0;
            int shieldHpHU = 0;
            int shieldHpPA = 0;
            int energyPS = 0;
            int energyHU = 0;
            int energyPA = 0;
            int armorType = 0;

            ShipDesignDetailed sdd = new ShipDesignDetailed(designId);
            HashMap<Integer, Integer> modules = sdd.getModuleCount();

            //  rs = stmt.executeQuery("SELECT m.name, m.type, m.value, m.energy, m.chassisSize, m.isConstruct, dm.count, m.id FROM designmodule dm, module m WHERE dm.designId=" + designId + " AND m.id=dm.moduleId");
            bs.setHp((int) ((float) sdd.getBase().getStructure() * sta.getStructureFactor()) * groupingMultiplier);
            bs.setGroupingMultiplier(groupingMultiplier);
            bs.setRestCount(groupingMultiplier * count);
            DebugBuffer.addLine(DebugLevel.DEBUG, "Hp of ship = " + bs.getHp());

            int totalRange = 0;
            int weaponCount = 0;
            int maxTargetAllocation = 0;
            int fireSpeed = 0;

            for (Map.Entry<Integer, Integer> moduleEntry : modules.entrySet()) {
                Module m = moduleDAO.findById(moduleEntry.getKey());
                int modCount = moduleEntry.getValue();

                ModuleType type = m.getType();

                DebugBuffer.addLine(DebugLevel.DEBUG, "Check moduel " + m.getName() + " with id " + moduleEntry.getKey() + " and type " + m.getType());
                DebugBuffer.addLine(DebugLevel.DEBUG, "Type of corresponding moduleObjekt is " + sdd.getModule(m.getId()));
                if (type == ModuleType.ARMOR) {
                    switch (moduleEntry.getKey()) {
                        case (2):
                            armorType = 1;
                            break;
                        case (21):
                            armorType = 2;
                            break;
                        case (22):
                            armorType = 3;
                            break;
                        case (23):
                            armorType = 4;
                            break;
                    }
                } else if (type == ModuleType.WEAPON) {
                    WeaponModule wm = new WeaponModule();
                    Weapon w = sdd.getWeapon(moduleEntry.getKey());

                    wm.setWeaponId(m.getId());

                    wm.setMultifire(w.getFireRate());
                    wm.setMultifired(0);

                    fireSpeed += w.getFireRate() * modCount;
                    
                    maxTargetAllocation += wm.getMultifire() * modCount;

                    wm.setRange(w.getRange());

                    totalRange += w.getRange() * modCount;
                    weaponCount += modCount;

                    DebugBuffer.addLine(DebugLevel.DEBUG, "Multifire = " + wm.getMultifire() + " for weapon " + w.getName());

                    wm.setAttackPower((int) ((sta.getAttackFactor() * (float) w.getAttackStrength()) / wm.getMultifire()) * groupingMultiplier);
                    wm.setAccuracy(w.getAccuracy());
                    wm.setCount(modCount * wm.getMultifire());
                    wm.setTotalCount(modCount * count * wm.getMultifire());
                    wm.setEnergyConsumption((int) (w.getEnergyConsumption() / wm.getMultifire()) * groupingMultiplier);
                    weaponEnergyConsumption += wm.getEnergyConsumption() * wm.getCount();
                    // DebugBuffer.addLine("Add weapons to " + bs.getName());

                    weaponArray.add(wm);
                } else if (type == ModuleType.ENGINE) { //
                    // TODO internalEnergyConsumption += tmpEnergy;
                } else if (type == ModuleType.CHASSIS) {
                    // chassis - NOT USED
                } else if (false) {
                    // Rockets NOT USED currently
                } else if (false) { // 7??
                    // FREE
                } else if (type == ModuleType.SHIELD) {
                    Shield s = sdd.getShield(moduleEntry.getKey());

                    switch (moduleEntry.getKey()) {
                        case (50):
                            shieldEnergyConsumption += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                            shieldHpPS += (int) ((float) s.getDefenseStrength() * (float) modCount * sta.getShieldFactor()) * groupingMultiplier;
                            energyPS += (s.getEnergyConsumption() * modCount);
                            break;
                        case (51):
                            shieldEnergyConsumption += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                            shieldHpHU += (int) ((float) s.getDefenseStrength() * (float) modCount * sta.getShieldFactor()) * groupingMultiplier;
                            energyHU += (s.getEnergyConsumption() * modCount);
                            break;
                        case (52):
                            shieldEnergyConsumption += (s.getEnergyConsumption() * modCount) * groupingMultiplier;
                            shieldHpPA += (int) ((float) s.getDefenseStrength() * (float) modCount * sta.getShieldFactor()) * groupingMultiplier;
                            energyPA += (s.getEnergyConsumption() * modCount);
                            break;
                    }
                } else if (false) { // 9??
                    // FREE
                } else if (type == ModuleType.REACTOR) {
                    Reactor r = sdd.getReactor(moduleEntry.getKey());
                    energyProduction += (r.getEnergyProduction() * modCount * groupingMultiplier);
                } else if ((type == ModuleType.TRANSPORT_POPULATION) || (type == ModuleType.TRANSPORT_RESSOURCE) || (type == ModuleType.COLONISATION)) {
                    SimpleModule sm = (SimpleModule) sdd.getModule(moduleEntry.getKey());
                    internalEnergyConsumption += sm.getEnergyConsumption() * modCount * groupingMultiplier;
                } else if (type == ModuleType.HANGAR) {
                    // TODO internalEnergyConsumption += tmpEnergy;
                }
            }

            // Analyse primary target
            int baseValue = bs.getChassisSize();
            int primary = baseValue;

            if (bs.getAverageRange() < 5) {
                primary = Math.max(1, (int)Math.floor(baseValue / 2.5d));
            } else if (bs.getAverageRange() < 10) {
                primary = Math.max(1, (int)Math.floor(baseValue / 2d));
            } else if (bs.getAverageRange() < 20) {
                primary = Math.max(1, (int)Math.floor(baseValue / 1.5d));
            }

            bs.setPrimaryTarget(primary);
            bs.setMaxTargetAllocation(maxTargetAllocation);
            bs.setWeapons(weaponArray);
            bs.setAverageRange((float) totalRange / weaponCount);
            bs.setFireSpeed(fireSpeed);
            bs.setSizeLevel((float) Math.log10(sdd.getBase().getStructure()));
            // bs.setHp(bs.getHp() + (bs.getHp() / 100 * armorValue));

            ShieldDefinition sDef = new ShieldDefinition();

            sDef.setShield_PS(shieldHpPS);
            sDef.setShield_HU(shieldHpHU);
            sDef.setShield_PA(shieldHpPA);
            sDef.setSharedShield_PS(sDef.getShield_PS() * count);
            sDef.setSharedShield_HU(sDef.getShield_HU() * count);
            sDef.setSharedShield_PA(sDef.getShield_PA() * count);
            sDef.setEnergyNeed_PS(energyPS);
            sDef.setEnergyNeed_HU(energyHU);
            sDef.setEnergyNeed_PA(energyPA);

            bs.setEnergy(energyProduction);
            bs.setArmorType(armorType);
            bs.setSharedHp(bs.getHp() * count);
            bs.setWeaponEnergyPerShip(weaponEnergyConsumption);
            bs.setWeaponEnergy(weaponEnergyConsumption * count);

            sDef.setShieldEnergyPerShip(shieldEnergyConsumption);
            sDef.setShieldEnergy(shieldEnergyConsumption * count);

            bs.setShields(sDef);
                        
            // if (!c.isStarbase()) { This function is not called if it is a starbase
                bs.initDamageStack();
            // }

            return bs;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Military Utilities - buildBattleShip: ", e);
        }

        return null;
    }

    public ICombatUnit buildDefenseNew(int designId, int count, UnitTypeEnum ute, int userId) {
        ICombatUnit icu = null;

        try {
            if (ute == UnitTypeEnum.PLATTFORM_DEFENSE) {
                icu = new BattleShipNew(designId, count);
            } else if (ute == UnitTypeEnum.SURFACE_DEFENSE) {
                icu = new DefenseBuilding(designId, count, userId);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Unexpected error in new function: ", e);
        }

        return icu;
    }

    public ICombatUnit buildDefense(CombatGroupFleet.DesignDescriptor unit, int count, int userId) {
        BattleShip bs = new BattleShip(userId);

        bs.setDesignId(unit.getId());
        bs.setCount(count);
        // bs.overrideFactors(probabilityPA,minDamagePA,maxDamagePA);

        try {
            if (unit.getType() == UnitTypeEnum.SURFACE_DEFENSE) {
                bs.setChassisSize(7);
                bs.setGroupingMultiplier(1);

                ShipTypeAdjustment sta = staDAO.findByType(EShipType.SPACEBASE);
                Construction c = constructionDAO.findById(unit.getId());                
                
                if (c != null) {
                    bs.setName(c.getName());
                    bs.setHp((int) ((float) c.getHp() * sta.getStructureFactor()));

                    if (c.getId() == Construction.ID_PLANETARYLASERCANNON) {
                        //Logger.getLogger().write("Building a planetary Laser");
                        bs.setPrimaryTarget(5);
                    } else if (c.getId() == Construction.ID_PLANETARYMISSLEBASE) {
                        //Logger.getLogger().write("Building a missile Battery");
                        bs.setPrimaryTarget(2);
                    } else if (c.getId() == Construction.ID_PLANETARY_INTERVALCANNON) {
                        //Logger.getLogger().write("Building a planetary Intervall");
                        bs.setPrimaryTarget(5);
                    }
                }

                // bs.setMaxAttackPerUnit(maxAttackMap(bs));

                ArrayList<WeaponModule> weaponArray = new ArrayList<WeaponModule>();
                int energyProduction = 0;
                int weaponEnergyConsumption = 0;
                int shieldEnergyConsumption = 0;
                int internalEnergyConsumption = 0;
                int shieldHpPS = 0;
                int shieldHpHU = 0;
                int shieldHpPA = 0;
                int armorType = 0;

                ArrayList<ConstructionModule> cmList = cmDAO.findByConstruction(unit.getId());
                AttributeTree at = new AttributeTree(userId,0l);
                
                for (ConstructionModule cm : cmList) {
                    Module m = moduleDAO.findById(cm.getModuleId());
                    
                    if (m.getType() == ModuleType.WEAPON) {
                        WeaponModule wm = new WeaponModule();

                        wm.setWeaponId(m.getId());

                        ModuleAttributeResult mar = at.getAllAttributes(0, wm.getWeaponId());
                        
                        wm.setMultifire((int)mar.getAttributeValue(EAttribute.MULTIFIRE));
                        wm.setMultifired(0);
                        wm.setRange((int)mar.getAttributeValue(EAttribute.RANGE));

                        wm.setAttackPower((int)(mar.getAttributeValue(EAttribute.ATTACK_STRENGTH) / wm.getMultifire()));
                        wm.setAccuracy((float)mar.getAttributeValue(EAttribute.ACCURACY));
                        wm.setCount(cm.getCount() * wm.getMultifire());
                        wm.setTotalCount(cm.getCount() * count * wm.getMultifire());  

                        weaponArray.add(wm);                        
                    }
                }

                bs.setWeapons(weaponArray);
                
                ShieldDefinition sd = new ShieldDefinition();

                sd.setShield_PS(shieldHpPS);
                sd.setShield_HU(shieldHpHU);
                sd.setShield_PA(shieldHpPA);
                sd.setSharedShield_PS(sd.getShield_PS() * count);
                sd.setSharedShield_HU(sd.getShield_HU() * count);
                sd.setSharedShield_PA(sd.getShield_PA() * count);

                bs.setShields(sd);

                bs.setSharedHp(bs.getHp() * count);
                bs.setEnergy(energyProduction);
                bs.setArmorType(armorType);
                bs.setIsPlanetaryDefense(true);
            } else if (unit.getType() == UnitTypeEnum.PLATTFORM_DEFENSE) {
                bs = buildBattleShip(unit.getId(), count, userId);
                bs.setIsDefenseStation(true);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Military Utilities - buildBattleShip (Defense): ", e);
        }

        bs.initDamageStack();
        return bs;
    }


}
