/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.GameConstants;
import at.viswars.utilities.MilitaryUtilities;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.ships.ShipStorage;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.FleetLoadingDAO;
import at.viswars.dao.GroundTroopDAO;
import at.viswars.dao.PlanetRessourceDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.PlayerTroopDAO;
import java.util.*;
import java.security.InvalidParameterException;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.fleet.LoadingInformation;
import at.viswars.model.FleetLoading;
import at.viswars.model.GroundTroop;
import at.viswars.model.PlanetRessource;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.PlayerTroop;
import at.viswars.model.Ressource;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.service.PlanetService;
import at.viswars.service.RessourceService;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class RessTransfer {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    public final static int LOADING_RESS = 1;
    public final static int LOADING_TROOPS = 2;
    public final static int LOADING_POPULATION = 3;

    public static enum Direction {

        LOAD,
        UNLOAD;
    }
    private int fleetId;
    private int planetId;
    private int userId;
    private int type;
    private Direction direction;
    private MutableRessourcesEntry mre = new MutableRessourcesEntry();
    private int population;
    private Map<Integer, Integer> troops;
    private Map<String, ?> allPars;
    private PlayerFleetExt currFleet;
    private LoadingInformation loadInfo;
    private PlayerPlanet currPlanet;
    private boolean invasion = false;
    private boolean transactionReady = false;
    private String errMsg;

    /** Creates a new instance of RessTransfer for Fleet <--> Planet transactions */
    public RessTransfer() {
    }

    public void preparePlanetFleetTransfer(
            int fleetId, int userId, int type,
            Direction direction, Map<String, ?> allPars_)
            throws InvalidParameterException {
        allPars = allPars_;
        //this.allPars = allPars;
        this.fleetId = fleetId;
        this.userId = userId;
        this.type = type;
        this.direction = direction;

        currFleet = new PlayerFleetExt(fleetId);
        loadInfo = currFleet.getLoadingInformation();

        // Retrieve PlanetId from fleetEntry as loading actions are always locally
        planetId = currFleet.getRelativeCoordinate().getPlanetId();

        currPlanet = ppDAO.findByPlanetId(planetId);

        // Check if current Fleet is owned by curr User Id (same for Planet)
        // Transfer is allowed downwards if planet is allied and
        // both directions if it's an own planet
        // DebugBuffer.addLine("CF UserId="+currFleet.getUserId()+" CP UserId="+currPlanet.getPlanetId()+" CF PlanetId="+currFleet.getPlanetId()+" CP PlanetID="+currPlanet.getPlanetId());
        if ((currFleet.getUserId() != currPlanet.getUserId())) {
            boolean ok = true;
            if ((!AllianceUtilities.areAllied(currFleet.getUserId(), currPlanet.getUserId()) || (direction == Direction.LOAD)) && (type != GameConstants.LOADING_TROOPS)) {
                ok = false;
            }

            if (!ok) {
                throw new InvalidParameterException(
                        "Keine Berechtigung (" + currFleet.getUserId() + "=" + userId + "),(" + currPlanet.getUserId() + "=" + userId + "),(" + planetId + "=" + planetId + ")!");
            }

            if(type == GameConstants.LOADING_TROOPS && direction.equals(Direction.UNLOAD)){
                if ((!currFleet.canInvade()) &&
                    (!MilitaryUtilities.unloadTroopPossible(userId, planetId))) {
                    throw new InvalidParameterException(
                        "User hat keine Berechtigung zu Invadieren (" + currFleet.getUserId() + "=" + userId + "),(" + currPlanet.getUserId() + "=" + userId + "),(" + planetId + "=" + planetId + ")!");
                }
            }
            // Set invasion flag to perform updating of groundcombat table
            if ((!AllianceUtilities.areAllied(currFleet.getUserId(), currPlanet.getUserId())) && (type == GameConstants.LOADING_TROOPS)) {
                invasion = true;
            }
        }

        try {
            switch (type) {
                case (GameConstants.LOADING_RESS):
                    if (direction == Direction.LOAD) {
                        checkPlanetToFleetRess();
                    } else {
                        checkFleetToPlanetRess();
                    }
                    break;
                case (GameConstants.LOADING_TROOPS):
                    if (direction == Direction.LOAD) {
                        checkPlanetToFleetTroops();
                    } else {
                        checkFleetToPlanetTroops();
                    }
                    break;
                case (GameConstants.LOADING_POPULATION):
                    if (direction == Direction.LOAD) {
                        checkPlanetToFleetPopulation();
                    } else {
                        checkFleetToPlanetPopulation();
                    }
                    break;
            }
        } catch (InvalidParameterException ipe) {
            // DebugBuffer.error("Parameter Error encountered in RessTransfer: ", ipe);
            throw ipe;
        } catch (Exception e) {
            DebugBuffer.error("Unexpected Error encountered in RessTransfer: ", e);
        }
    }

    private void checkPlanetToFleetPopulation() throws InvalidParameterException {
        // Init Extended Planet Calculations
        PlanetCalculation pc = null;

        try {
            pc = new PlanetCalculation(currPlanet);
        } catch (Exception e) {
            throw new InvalidParameterException(
                    "Planet ungültig");
        }

        if (allPars.containsKey("loadpop")) {
            population = convertToInt(((String[]) allPars.get("loadpop"))[0]);
        }

        //Erstens vorhandene Bevölkerung berechnen:
        // long bev = currPlanet.getPopulation();
        //Dann wieviel AP gebraucht werden
        // long apRequired = pc.getPlanetCalcData().getExtPlanetCalcData().getUsedPopulation();
        //Und das ergibt dann, wieviel Bevölkerung gebraucht wird
        // long minPop = PlanetService.findMinPopPossible(apRequired);
        long freePop = PlanetService.getFreePopulation(currPlanet.getPlanetId());

        if (population > freePop) {
            throw new InvalidParameterException(
                    "Zu wenig Arbeiter auf Planet");
        }

        if (population > (loadInfo.getTroopSpace() - (loadInfo.getCurrLoadedTroops() + loadInfo.getCurrLoadedPopulation()))) {
            throw new InvalidParameterException(
                    "Zuwenig Platz in Flotte");
        }

        if (population < 0) {
            throw new InvalidParameterException(
                    "Scherzkeks");
        }

        transactionReady = true;
    }

    private void checkFleetToPlanetPopulation() throws InvalidParameterException {
        if (allPars.containsKey("unloadPop")) {
            population = convertToInt(((String[]) allPars.get("unloadPop"))[0]);
        }

        if (fleetStoragePop() < population) {
            throw new InvalidParameterException(
                    "Zuwenig Ressourcen in Flotte");
        }

        if (population < 0) {
            throw new InvalidParameterException(
                    "Scherzkeks");
        }

        transactionReady = true;
    }

    private void checkPlanetToFleetRess() throws InvalidParameterException {
        // Read Parameters
        for (Ressource re : RessourceService.getAllStorableRessources()) {
            if (allPars.containsKey("load" + re.getName())) {
                mre.setRess(re.getId(), convertToInt(((String[]) allPars.get("load" + re.getName()))[0]));
                // Logger.getLogger().write("Add Ressource " + re.getId() + " with value " + mre.getRess(re.getId()));
            }
        }

        long totalQty = 0;

        for (Ressource re : RessourceService.getAllStorableRessources()) {
            if (mre.getRess(re.getId()) > PlanetService.getStockRessourceOnPlanet(planetId, re.getId())) {
                throw new InvalidParameterException(
                        "Zu wenig Ressourcen auf Planet");
            }

            if (mre.getRess(re.getId()) < 0) {
                throw new InvalidParameterException(
                        "Scherzkeks");
            }

            totalQty += mre.getRess(re.getId());
        }

        if (totalQty > (loadInfo.getRessSpace() - loadInfo.getCurrLoadedRess())) {
            throw new InvalidParameterException(
                    "Zuwenig Platz in Flotte");
        }

        transactionReady = true;
    }

    private void checkFleetToPlanetRess() throws InvalidParameterException {
        // Read Parameters
        for (Ressource re : RessourceService.getAllStorableRessources()) {
            if (allPars.containsKey("unload" + re.getId())) {
                mre.setRess(re.getId(), convertToInt(((String[]) allPars.get("unload" + re.getId()))[0]));
            }

            if (fleetStorageRess(re.getId()) < mre.getRess(re.getId())) {
                throw new InvalidParameterException(
                        "Zuwenig Ressourcen in Flotte");
            }
        }

        for (Ressource re : RessourceService.getAllStorableRessources()) {
            if (mre.getRess(re.getId()) < 0) {
                throw new InvalidParameterException(
                        "Scherzkeks");
            }
        }

        transactionReady = true;
    }

    private int fleetStoragePop() {
        if (loadInfo != null) {
            for (ShipStorage ss : loadInfo.getLoadedStorage()) {
                if (ss.getLoadtype() == 4) {
                    if (ss.getId() == 20) {
                        return ss.getCount();
                    }
                }
            }
        }

        return 0;
    }

    private int fleetStorageRess(int ressId) {
        if (loadInfo != null) {
            for (ShipStorage ss : loadInfo.getLoadedStorage()) {
                if (ss.getLoadtype() == 1) {
                    if (ss.getId() == ressId) {
                        return ss.getCount();
                    }
                }
            }
        }

        return 0;
    }

    private void checkPlanetToFleetTroops() throws InvalidParameterException {
        // Read Parameters and recheck with Troops on ground
        troops = new HashMap<Integer, Integer>();
        int addSpaceNeeded = 0;

        for (String parName : allPars.keySet()) {
            if (parName.startsWith("load")) {
                String[] valueStr = (String[]) allPars.get(parName);

                int value = convertToInt(valueStr[0]);
                int troopId = Integer.parseInt(parName.substring(4, parName.length()));
                DebugBuffer.addLine(DebugLevel.DEBUG, "TroopId : " + troopId);
                PlayerTroop pt = ptDAO.findBy(userId, planetId, troopId);
                if (pt == null) {
                    DebugBuffer.warning("PlayerTroop could not be found for userId: " + userId + " planetId: " + planetId + " troopId: " + troopId);
                    continue;
                }
                
                DebugBuffer.addLine(DebugLevel.DEBUG, "Number : " + pt.getNumber() + " value : " + value);
                GroundTroop gt = gtDAO.findById(troopId);

                if (gt == null) {
                    throw new InvalidParameterException("Error: Diese Truppentyp (" + troopId + ") existiert nicht");
                }

                if (value < 0) {
                    throw new InvalidParameterException(
                            "Error: Scherzkeks");
                }

                if (pt == null) {
                    throw new InvalidParameterException("Error: Diese Truppentyp (" + troopId + ") ist nicht auf dem Planeten vorhanden");
                } else if (pt.getNumber() < value) {
                    throw new InvalidParameterException("Error: Diese Anzahl an Truppen ist nicht auf dem Planet vorhanden");
                }

                addSpaceNeeded += gt.getSize() * value;

                if (value != 0) {
                    troops.put(troopId, value);
                }
            }
        }

        if (addSpaceNeeded > (loadInfo.getTroopSpace() - (loadInfo.getCurrLoadedTroops() + loadInfo.getCurrLoadedPopulation()))) {
            throw new InvalidParameterException(
                    "Error: Nicht gen&uuml;gend Platz in Flotte vorhanden!");
        }

        transactionReady = true;
    }



    private void checkFleetToPlanetTroops() throws InvalidParameterException {
        // Read Parameters and recheck with Troops in fleet
        troops = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> availTroops = new HashMap<Integer, Integer>();

        if ((loadInfo.getLoadedStorage() != null) && !loadInfo.getLoadedStorage().isEmpty()) {
            for (ShipStorage ss : loadInfo.getLoadedStorage()) {
                if (ss.getLoadtype() == GameConstants.LOADING_TROOPS) {
                    availTroops.put(ss.getId(), ss.getCount());
                }
            }
        }


        for (String parName : allPars.keySet()) {
            if (parName.startsWith("unload")) {
                String[] valueStr = (String[]) allPars.get(parName);

                int value = convertToInt(valueStr[0]);
                int troopId = 0;

                try {
                    troopId = Integer.parseInt(parName.substring(6, parName.length()));
                } catch (NumberFormatException nfe) {
                    throw new InvalidParameterException(
                            "TroopId is not a number");
                }

                if (value < 0) {
                    throw new InvalidParameterException(
                            "Error: Scherzkeks");
                }

                if (availTroops.get(troopId) == null) {
                    throw new InvalidParameterException("TroopId " + troopId + " is not contained in fleet");
                }

                if (((Integer) availTroops.get(troopId)).intValue() < value) {
                    throw new InvalidParameterException(
                            "Soviele Truppen sind in der Flotte nicht verf&uuml;gbar!");
                } else {
                    troops.put(troopId, value);
                }
            }
        }
        transactionReady = true;
    }

    public boolean transact() {
        String invasionOverride = null;

        boolean success = true;
        if (!transactionReady) {
            throw new InvalidParameterException(
                    "Keine Transaktion verf&uuml;gbar");
        } else {
            transactionReady = false;
            switch (type) {
                case (GameConstants.LOADING_RESS):
                    if (direction == Direction.LOAD) {
                        // Planet --> Fleet
                        // Reduce planet ressources
                        for (Ressource re : RessourceService.getAllStorableRessources()) {
                            PlanetRessource pr = prDAO.findBy(planetId, re.getId(), EPlanetRessourceType.INSTORAGE);
                            if (pr == null) {
                                continue;
                            }
                            pr.setQty(pr.getQty() - (long) mre.getRess(re.getId()));

                            if (pr.getQty() == 0) {
                                prDAO.remove(pr);
                            } else {
                                prDAO.update(pr);
                            }
                        }

                        // Check Loading List and set flags
                        ArrayList<Integer> ressLoaded = new ArrayList<Integer>();

                        if (loadInfo.getLoadedStorage() != null) {
                            ArrayList<ShipStorage> loaded = loadInfo.getLoadedStorage();
                            for (ShipStorage ss : loaded) {
                                if (ss.getLoadtype() == GameConstants.LOADING_RESS) {
                                    ressLoaded.add(ss.getId());
                                }
                            }
                        }

                        // Increase fleet ressources
                        for (Ressource re : RessourceService.getAllStorableRessources()) {
                            if (mre.getRess(re.getId()) > 0) {
                                if (ressLoaded.contains(re.getId())) {
                                    FleetLoading fl = flDAO.getByFleetAndFreight(fleetId, 1, re.getId());
                                    fl.setCount(fl.getCount() + (int) mre.getRess(re.getId()));
                                    flDAO.update(fl);
                                } else {
                                    FleetLoading fl = new FleetLoading();
                                    fl.setFleetId(fleetId);
                                    fl.setLoadType(1);
                                    fl.setId(re.getId());
                                    fl.setCount((int) mre.getRess(re.getId()));
                                    flDAO.add(fl);
                                }
                            }
                        }
                    } else {
                        // Fleet -> Planet
                        for (Ressource re : RessourceService.getAllStorableRessources()) {
                            if (mre.getRess(re.getId()) > 0) {
                                FleetLoading fl = flDAO.getByFleetAndFreight(fleetId, 1, re.getId());
                                if ((fl.getCount() - mre.getRess(re.getId())) == 0) {
                                    flDAO.remove(fl);
                                } else {
                                    fl.setCount(fl.getCount() - (int) mre.getRess(re.getId()));
                                    flDAO.update(fl);
                                }
                            }
                        }

                        PlanetCalculation pc = null;

                        try {
                            pc = new PlanetCalculation(currPlanet);
                        } catch (Exception e) {
                            throw new InvalidParameterException(
                                    "Planet ungültig");
                        }
                        ProductionResult pr = pc.getPlanetCalcData().getProductionData();

                        for (Ressource re : RessourceService.getAllStorableRessources()) {
                            PlanetRessource pRes = prDAO.findBy(planetId, re.getId(), EPlanetRessourceType.INSTORAGE);
                            long newQty = 0;
                            if (pRes != null) {
                                newQty += pRes.getQty();
                            }
                            newQty += mre.getRess(re.getId());

                            if (newQty > pr.getRessMaxStock(re.getId())) {
                                newQty = Math.max(newQty, pr.getRessMaxStock(re.getId()));
                            }

                            if (pRes == null) {
                                pRes = new PlanetRessource();
                                pRes.setPlanetId(planetId);
                                pRes.setRessId(re.getId());
                                pRes.setType(EPlanetRessourceType.INSTORAGE);
                                pRes.setQty(newQty);
                                prDAO.add(pRes);
                            } else {
                                pRes.setQty(newQty);
                                prDAO.update(pRes);
                            }
                        }
                    }
                    break;
                case (GameConstants.LOADING_TROOPS):
                    if (direction == Direction.LOAD) {
                        // If all troops are removed it seems to be a retreat
                        boolean retreat = true;

                        // Planet --> Fleet
                        if (troops.isEmpty()) {
                            throw new InvalidParameterException(
                                    "Nothing to do");
                        }

                        for (Map.Entry<Integer, Integer> troopEntry : troops.entrySet()) {
                            int troopId = troopEntry.getKey();
                            int count = troopEntry.getValue();

                            PlayerTroop pt = ptDAO.findBy(userId, planetId, troopId);
                            FleetLoading fl = flDAO.getByFleetAndFreight(fleetId, 2, troopId);

                            if (fl == null) {
                                retreat = false;

                                fl = new FleetLoading();
                                fl.setFleetId(fleetId);
                                fl.setLoadType(2);
                                fl.setId(troopId);
                                fl.setCount(count);
                                flDAO.add(fl);
                            } else {
                                fl.setCount(fl.getCount() + count);
                                flDAO.update(fl);
                            }

                            pt.setNumber(pt.getNumber() - count);
                            if (pt.getNumber() == 0) {
                                ptDAO.remove(pt);
                            } else {
                                ptDAO.update(pt);
                            }
                        }

                        // if (invasion && retreat) {
                        //     invasionOverride = Invasion.retreatvasion(currPlanet,currFleet.getUserId());
                        // }
                    } else {
                        if (troops.isEmpty()) {
                            throw new InvalidParameterException(
                                    "Nothing to do");
                        }
                        DebugBuffer.addLine(DebugLevel.DEBUG, "UNLOADING UNITS");
                            GroundCombatUtilities.checkForCombat(userId, planetId);
                        // Erstmaliges abladen oder Verstärkung
                        boolean reinforcement = false;

                        for (Map.Entry<Integer, Integer> troopEntry : troops.entrySet()) {
                            DebugBuffer.addLine(DebugLevel.DEBUG, "Loop");
                            int troopId = troopEntry.getKey();
                            int count = troopEntry.getValue();

                            if (count == 0) {
                                DebugBuffer.addLine(DebugLevel.DEBUG, "Continue");
                                continue;
                            }
                            PlayerTroop pt = ptDAO.findBy(userId, planetId, troopId);
                            FleetLoading fl = flDAO.getByFleetAndFreight(fleetId, 2, troopId);

                            if (fl.getCount() == count) {
                                flDAO.remove(fl);
                            } else {
                                fl.setCount(fl.getCount() - count);
                                flDAO.update(fl);
                            }

                            DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading1");
                            if (pt != null) {
                                DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading2");
                                reinforcement = true;

                                pt.setNumber(pt.getNumber() + count);
                                ptDAO.update(pt);
                            } else {
                                DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading3");
                                pt = new PlayerTroop();
                                pt.setPlanetId(planetId);
                                pt.setTroopId(troopId);
                                pt.setNumber((long) count);
                                DebugBuffer.addLine(DebugLevel.DEBUG, "userId : " + currFleet.getUserId());
                                pt.setUserId(currFleet.getUserId());
                                ptDAO.add(pt);
                            }
                        }

                        /*
                        if (invasion) {
                        if (reinforcement) {
                        invasionOverride = Invasion.reinforceInvasion(currPlanet,currFleet.getUserId());
                        } else {
                        invasionOverride = Invasion.startevasion(currPlanet,currFleet.getUserId());
                        }
                        }
                         */
                    }
                    break;
                case (GameConstants.LOADING_POPULATION):
                    if (direction == Direction.LOAD) {
                        // Planet --> Fleet
                        FleetLoading fl = flDAO.getByFleetAndFreight(fleetId, 4, Ressource.POPULATION);

                        // Increase fleet ressources
                        if (fl != null) {
                            fl.setCount(fl.getCount() + population);
                            flDAO.update(fl);
                        } else {
                            fl = new FleetLoading();
                            fl.setFleetId(fleetId);
                            fl.setLoadType(4);
                            fl.setId(Ressource.POPULATION);
                            fl.setCount(population);
                            flDAO.add(fl);
                        }

                        currPlanet.setPopulation(currPlanet.getPopulation() - population);
                        ppDAO.update(currPlanet);
                    } else {
                        // Fleet -> Planet
                        if (population != 0) {
                            FleetLoading fl = flDAO.getByFleetAndFreight(fleetId, 4, Ressource.POPULATION);
                            fl.setCount(fl.getCount() - population);

                            if (fl.getCount() == 0) {
                                flDAO.remove(fl);
                            } else {
                                flDAO.update(fl);
                            }
                        }

                        currPlanet.setPopulation(currPlanet.getPopulation() + population);
                        ppDAO.update(currPlanet);
                    }
                    break;
            }
        }

        switch (type) {
            case (GameConstants.LOADING_RESS):
                errMsg = "0Waren umgeladen!";
                break;
            case (GameConstants.LOADING_TROOPS):
                if (invasionOverride != null) {
                    errMsg = invasionOverride;
                } else {
                    errMsg = "0Truppen erfolgreich versetzt!";
                }
                break;
            case (GameConstants.LOADING_POPULATION):
                errMsg = "0Arbeiter &uuml;berstellt!";
                break;
        }
        return success;
    }

    public String getErrMsg() {
        if (errMsg != null) {
            return errMsg;
        } else {
            return "";
        }
    }

    private int convertToInt(String input) throws NumberFormatException {
        if (input.equalsIgnoreCase("")) {
            input = "0";
        }
        return Integer.parseInt(input);
    }
}
