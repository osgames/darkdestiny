/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.service.Service;
import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.UniverseCreation;
import at.viswars.construction.ConstructionInterface;
import at.viswars.database.framework.QueryKey;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.enumeration.*;
import at.viswars.model.*;
import at.viswars.result.BaseResult;
import at.viswars.service.AllianceService;
import at.viswars.service.TransportService;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class DestructionUtilities extends Service {

    public static void deleteAllUsersExcept(int userId) {
        ArrayList<User> uList = userDAO.findAll();

        for (User u : uList) {
            if (userId == u.getUserId() || u.isSystemUser()) {
                continue;
            }
            destroyPlayer(u.getUserId());
        }
    }

    public static void deleteAllUsersExcept(ArrayList<Integer> users) {
        ArrayList<User> uList = userDAO.findAll();

        for (User u : uList) {
            if (u.isSystemUser() || users.contains(u.getUserId())) {
                continue;
            }
            destroyPlayer(u.getUserId());
        }
    }

    public static void destroyPlayer(int userId) {
        DebugBuffer.addLine(DebugLevel.TRACE, "Delete user " + userId);



        TransactionHandler th = TransactionHandler.getTransactionHandler();


        try {
            UserData ud = userDataDAO.findByUserId(userId);
            UserSettings us = userSettingsDAO.findByUserId(userId);
            User u = userDAO.findById(userId);

            //Newslettercheck
            try {
                DeletedUser du = deletedUserDAO.findByEmail(u.getEmail().trim());
                if (du == null) {
                    du = new DeletedUser();
                    if (us.getNewsletter()) {
                        du.setEmail(u.getEmail().trim());
                    }else{
                        du.setEmail("");
                    }
                    if (us.getProtectaccount()) {
                        du.setPassword(u.getPassword());
                        du.setGameName(u.getGameName());
                        du.setUserName(u.getUserName());
                    }else{
                        
                        du.setPassword("");
                        du.setGameName("");
                        du.setUserName("");
                    }
                    deletedUserDAO.add(du);
                }
            } catch (Exception e) {
                DebugBuffer.warning("Deleted user add did not succeed for " + u.getEmail().trim());
            }

            th.startTransaction();

            if (u.getTrial()) {
                DebugBuffer.addLine(DebugLevel.TRACE, "User is still in trial");
                // Look for System id and restore System to free login space
                // int systemId = 0;
                ArrayList<PlayerPlanet> ppList = playerPlanetDAO.findByUserId(userId);

                if (ppList.size() > 0) {
                    for (PlayerPlanet pp : ppList) {
                        int planetId = pp.getPlanetId();
                        at.viswars.model.System s = systemDAO.findById(planetDAO.findById(planetId).getSystemId());
                        s.setVisibility(0);
                        systemDAO.update(s);

                        Planet p = planetDAO.findById(planetId);

                        if (p.getLandType().equalsIgnoreCase("C")
                                || p.getLandType().equalsIgnoreCase("M")
                                || p.getLandType().equalsIgnoreCase("G")) {
                            p = UniverseCreation.rebuildPlanet(p);
                        }
                    }
                } else {
                    DebugBuffer.addLine(DebugLevel.WARNING, "User had no playerplanet entries??");
                }
                /*
                DebugBuffer.addLine(DebugLevel.TRACE, "Rebuilding system and planet info for " + systemId);
                try {
                // SystemTable.removeSystem(systemId);
                UniverseCreation.addSystemToUniverse(systemId, 0, 0, userId, true);
                } catch (Exception ex) {
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error while UniverseCreation.addSystemToUnivers : " + ex);
                }
                 */
            }

            destroyMessages(userId);
            destroyTradeRoutes(userId);
            destroyShipDesigns(userId);
            destroyAllFleets(userId);
            destroyResearch(userId);
            destroyUserDiplomacy(userId);
            destroyActions(userId);
            destroyCategories(userId);
            cleanUpViewTableData(userId);
            destroyPersonalInformation(userId);
            destroyPriceLists(userId);
            destroyAllianceBoardPermissions(userId);

            for (PlayerPlanet pp : playerPlanetDAO.findByUserId(userId)) {
                destroyPlayerPlanet(pp.getPlanetId());
            }

            if (allianceMemberDAO.findByUserId(userId) != null) {
                destroyAllianceMemberShip(userId);
            }

            if (ud != null) {
                userDataDAO.remove(ud);
            }
            //Update TerritoryEntries
            try {
                TerritoryUtilities.updateAllTerritoryPermissions();
            } catch (Exception e) {

                DebugBuffer.writeStackTrace("Error in user deletion: Could not update TerritoryEntries -> " + e.getMessage() + ":<BR>", e);
            }


            userDAO.remove(u);

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on deleting user rolling back: ", e);
            th.rollback();
        } finally {
            th.endTransaction();
        }
    }

    public static void cleanVoting(int userId) {
    }

    private static void cleanUpViewTableData(int userId) {
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("userId", userId));

        planetLogDAO.removeAll(qks);
        viewTableDAO.removeAll(qks);
    }

    private static void destroyPersonalInformation(int userId) {
        // Delete style information
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("userId", userId));

        styleToUserDAO.removeAll(qks);

        // Delete title/campaign information
        ArrayList<ConditionToUser> ctuList = conditionToUserDAO.findBy(userId);

        titleToUserDAO.removeAll(qks);
        conditionToUserDAO.removeAll(qks);
        campaignToUserDAO.removeAll(qks);

        // Notification settings
        notificationToUserDAO.removeAll(qks);



        // Filters
        for (Filter f : filterDAO.findByUserId(userId)) {
            QueryKeySet qksInner = new QueryKeySet();
            qksInner.addKey(new QueryKey("filterId", f.getId()));

            filterToValueDAO.removeAll(qksInner);
        }

        filterDAO.removeAll(qks);

        // Clean logintracker data
        loginTrackerDAO.removeAll(qks);

        // Clean statistics
        imperialStatisticDAO.removeAll(qks);
        playerStatisticDAO.removeAll(qks);
        statisticDAO.removeAll(qks);
    }

    private static void destroyUserDiplomacy(int userId) {
        for (DiplomacyRelation ur : diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_USER)) {
            diplomacyRelationDAO.remove(ur);
        }
        for (DiplomacyRelation ur : diplomacyRelationDAO.findByToId(userId, EDiplomacyRelationType.USER_TO_USER)) {
            diplomacyRelationDAO.remove(ur);
        }
        for (DiplomacyRelation ur : diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_ALLIANCE)) {
            diplomacyRelationDAO.remove(ur);
        }
        for (DiplomacyRelation ur : diplomacyRelationDAO.findByToId(userId, EDiplomacyRelationType.ALLIANCE_TO_USER)) {
            diplomacyRelationDAO.remove(ur);
        }
    }

    private static void destroyTradeRoutes(int userId) {
        for (TradeRoute tr : tradeRouteDAO.findByUserId(userId)) {
            BaseResult br = TransportService.deleteRoute(userId, tr.getId());
            /*
            for (TradeRouteShip trs : tradeRouteShipDAO.findByTradeRouteId(userId)) {
            tradeRouteShipDAO.remove(trs);
            }
            tradeRouteDAO.remove(tr);
             */
        }
    }

    private static void destroyResearch(int userId) {
        for (PlayerResearch pr : playerResearchDAO.findByUserId(userId)) {
            playerResearchDAO.remove(pr);
        }
        for (ResearchProgress rp : researchProgressDAO.findByUserId(userId)) {
            researchProgressDAO.remove(rp);
        }
    }

    private static void destroyShipDesigns(int userId) {
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("userId", userId));

        for (ShipDesign sd : shipDesignDAO.findByUserId(userId)) {
            ArrayList<RessourceCost> rcList = ressourceCostDAO.find(ERessourceCostType.SHIP, sd.getId());
            for (RessourceCost rc : rcList) {
                Logger.getLogger().write("Deleteing Ressourcecost for ship " + sd.getId());
                ressourceCostDAO.remove(rc);
            }

            designModuleDAO.deleteDesign(sd.getId());
        }

        shipDesignDAO.removeAll(qks);
    }

    private static void destroyMessages(int userId) {
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("sourceUserId", userId));

        messageDAO.removeAll(qks);


        qks = new QueryKeySet();
        qks.addKey(new QueryKey("targetUserId", userId));

        messageDAO.removeAll(qks);
    }

    public static void destroyPlayerPlanet(int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        int userId = pp.getUserId();

        // Get PlanetLog -- if planetfounder was someone else than deleter give planet back
        /*
        int targetUser = 0;
        
        ArrayList<PlanetLog> plList = planetLogDAO.findByPlanetIdSorted(planetId);
        if (plList.size() > 0) {
        PlanetLog firstEntry = plList.get(plList.size() - 1);
        
        if (firstEntry.getUserId() == userId) {
        targetUser = firstEntry.getUserId();
        System.out.println("User deletion transfering planet " + planetId + " from user " + userId + " to " + targetUser);
        }
        }
         */

        destroyPlanetStoredRessources(planetId);
        destroyPlanetTroops(planetId);
        destroyPlanetConstructionOrders(planetId);
        destroyPlanetDefense(planetId);
        destroyTradePost(planetId);
        destroyBonus(planetId);
        destroyCategory(planetId);
        destroyActionsByPlanetId(planetId);
        destroyTradeRoutesByPlanetId(planetId);
        destroyNotes(planetId, userId);
        destroyPlanetLoyality(planetId);

        if (pp != null) {
            destroyPlanetConstructions(planetId);
            playerPlanetDAO.remove(pp);
        }
    }

    private static void destroyNotes(int planetId, int userId) {
        Note n = noteDAO.findBy(userId, planetId);
        if (n != null) {
            noteDAO.remove(n);
        }

    }

    private static void destroyPlanetLoyality(int planetId) {
        ArrayList<PlanetLoyality> pLoyList = planetLoyalityDAO.findByPlanetId(planetId);
        planetLoyalityDAO.removeAll(pLoyList);
    }

    private static void destroyAllFleets(int userId) {
        for (PlayerFleet pf : playerFleetDAO.findByUserId(userId)) {
            Logger.getLogger().write("Delete Fleet " + pf.getId());

            for (ShipFleet sf : shipFleetDAO.findByFleetId(pf.getId())) {
                Logger.getLogger().write("Delete ShipFleet");
                shipFleetDAO.remove(sf);
            }
            FleetDetail fd = fleetDetailDAO.findById(pf.getId());
            if (fd != null) {
                Logger.getLogger().write("Delete FleetDetail");
                fleetDetailDAO.remove(fd);
            }

            for (FleetLoading fl : fleetLoadingDAO.findByFleetId(pf.getId())) {
                Logger.getLogger().write("Delete FleetLoading");
                fleetLoadingDAO.remove(fl);
            }

            playerFleetDAO.remove(pf);
        }

    }

    private static void destroyAllianceMemberShip(int userId) {
        AllianceService.removeUserFromAlliance(allianceMemberDAO.findByUserId(userId));
    }

    private static void destroyTradeRoutesByPlanetId(int planetId) {
        //Deleting Traderoutes
        ArrayList<TradeRoute> tradeRoutes = tradeRouteDAO.findByStartPlanet(planetId, ETradeRouteType.TRANSPORT);
        tradeRoutes.addAll(tradeRouteDAO.findByEndPlanet(planetId, ETradeRouteType.TRANSPORT));
        for (TradeRoute tr : tradeRoutes) {
            TransportService.deleteRoute(tr.getUserId(), tr.getId());
        }
    }

    private static void destroyTradePost(int planetId) {

        DebugBuffer.addLine(DebugLevel.TRACE, "Deleting trade related data ...");
        TradePost tp = tradePostDAO.findByPlanetId(planetId);
        if (tp != null) {
            for (TradeFleet tf : tradeFleetDAO.findByTradePostId(tp.getId())) {
                tradeFleetDAO.remove(tf);
            }
            for (TradePostShip tps : tradePostShipDAO.findByTradePostId(tp.getId())) {
                tradePostShipDAO.remove(tps);
            }
            for (TradeOffer to : tradeOfferDAO.findByTradePostId(tp.getId())) {
                tradeOfferDAO.remove(to);
            }

            tradePostDAO.remove(tp);
        }

    }

    private static void destroyPriceLists(int userId) {

        for (PriceList pl : priceListDAO.findByUserId(userId)) {
            for (PriceListEntry ple : priceListEntryDAO.findByPriceList(pl.getId())) {
                priceListEntryDAO.remove(ple);
            }
            priceListDAO.remove(pl);
        }
        // Clean tradedata
        for (PriceList pl : (ArrayList<PriceList>) Service.priceListDAO.findAll()) {
            if (pl.getTradeOfferType().equals(TradeOfferType.USER) && pl.getRefId() == userId) {
                for (PriceListEntry ple : priceListEntryDAO.findByPriceList(pl.getId())) {
                    priceListEntryDAO.remove(ple);
                }
                priceListDAO.remove(pl);
            }
        }
    }

    private static void destroyPlanetStoredRessources(int planetId) {
        for (PlanetRessource pr : planetRessourceDAO.findByPlanetId(planetId)) {
            if (pr.getType() != EPlanetRessourceType.PLANET) {
                planetRessourceDAO.remove(pr);
            }
        }
    }

    private static void destroyPlanetDefense(int planetId) {
        for (PlanetDefense pf : planetDefenseDAO.findByPlanetId(planetId)) {
            planetDefenseDAO.remove(pf);
        }
    }

    private static void destroyPlanetConstructions(int planetId) {
        try {
            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);

            for (PlanetConstruction pc : planetConstructionDAO.findByPlanetId(pp.getPlanetId())) {
                ConstructionRestriction cr = null;
                cr = constructionRestrictionDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, pc.getConstructionId());

                if (cr != null) {
                    Class rc = Class.forName("at.viswars.construction.restriction." + cr.getRestrictionClass());
                    ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                    cir.onDestruction(pp);
                }
            }

            for (PlanetConstruction pc : planetConstructionDAO.findByPlanetId(planetId)) {
                planetConstructionDAO.remove(pc);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on destroyPlanetConstruction: ", e);
        }
    }

    private static void destroyPlanetTroops(int planetId) {
        for (PlayerTroop pt : playerTroopDAO.findByPlanetId(planetId)) {
            playerTroopDAO.remove(pt);
        }
    }

    private static void destroyPlanetConstructionOrders(int planetId) {
        for (Action a : actionDAO.findRunningConstructions(planetId)) {
            ProductionOrder po = productionOrderDAO.findByConstructionOrderById(a.getProductionOrderId());
            productionOrderDAO.remove(po);
            actionDAO.remove(a);
        }
    }

    private static void destroyBonus(int planetId) {
        for (ActiveBonus ab : activeBonusDAO.findByPlanetId(planetId)) {
            activeBonusDAO.remove(ab);
        }
    }

    private static void destroyActions(int userId) {
        for (Action a : actionDAO.findByUserId(userId)) {
            //ProductionOrder
            if (a.getType() == EActionType.BUILDING || a.getType() == EActionType.SHIP || a.getType() == EActionType.GROUNDTROOPS || a.getType() == EActionType.DECONSTRUCT) {
                ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                if (po != null) {
                    productionOrderDAO.remove(po);
                }
                //ResearchProgress
            } else if (a.getType() == EActionType.RESEARCH) {
                ResearchProgress rp = researchProgressDAO.findById(a.getRefTableId());
                if (rp != null) {
                    researchProgressDAO.remove(rp);
                }
                //FleetDetail
            } else if (a.getType() == EActionType.FLIGHT) {
                FleetDetail fd = fleetDetailDAO.findById(a.getRefTableId());
                if (fd != null) {
                    fleetDetailDAO.remove(fd);
                }
            }
            //Remove Action
            actionDAO.remove(a);
        }
    }

    private static void destroyActionsByPlanetId(int planetId) {
        for (Action a : actionDAO.findByPlanet(planetId)) {
            //ProductionOrder
            if (a.getType() == EActionType.BUILDING || a.getType() == EActionType.SHIP || a.getType() == EActionType.GROUNDTROOPS || a.getType() == EActionType.DECONSTRUCT) {
                ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                if (po != null) {
                    productionOrderDAO.remove(po);
                }
                //ResearchProgress
            }
            //Remove Action
            actionDAO.remove(a);
        }
    }

    private static void destroyCategories(int userId) {
        for (PlayerCategory pc : playerCategoryDAO.findByUserId(userId)) {
            for (PlanetCategory plc : planetCategoryDAO.findByCategoryId(pc.getId())) {
                planetCategoryDAO.remove(plc);
            }
            playerCategoryDAO.remove(pc);
        }
    }

    private static void destroyCategory(int planetId) {
        PlanetCategory pc = planetCategoryDAO.findByPlanetId(planetId);
        if (pc != null) {
            planetCategoryDAO.remove(pc);
        }

    }

    private static void destroyAllianceBoardPermissions(int userId) {
        for (AllianceBoardPermission abp : Service.allianceBoardPermissionDAO.findByUserId(userId)) {
            Service.allianceBoardPermissionDAO.remove(abp);
        }
    }
}
