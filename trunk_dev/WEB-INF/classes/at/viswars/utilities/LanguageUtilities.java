/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.utilities;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.LanguageDAO;
import at.viswars.dao.UserDAO;
import at.viswars.model.Language;
import at.viswars.model.User;
import java.util.Locale;

/**
 *
 * @author Stefan
 */
public class LanguageUtilities {
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static LanguageDAO lDAO = (LanguageDAO) DAOFactory.get(LanguageDAO.class);

    public static Locale getMasterLocaleForUser(int userId) {
        User u = uDAO.findById(userId);

        Language actLang = lDAO.findBy(u.getLocale());
        Language baseLang = lDAO.getMasterLanguageOf(actLang.getId());

        Locale l = new Locale(baseLang.getLanguage(),baseLang.getCountry().toUpperCase());
        return l;
    }
}
