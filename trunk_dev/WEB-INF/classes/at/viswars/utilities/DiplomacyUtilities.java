/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.enumeration.EOwner;
import at.viswars.Logger.Logger;
import at.viswars.diplomacy.UserShareEntry;
import at.viswars.diplomacy.relations.Aggressive;
import at.viswars.diplomacy.relations.Ally;
import at.viswars.diplomacy.relations.Friendly;
import at.viswars.diplomacy.relations.IRelation;
import at.viswars.diplomacy.relations.Nap;
import at.viswars.diplomacy.relations.Neutral;
import at.viswars.diplomacy.relations.Trade;
import at.viswars.diplomacy.relations.Treaty;
import at.viswars.diplomacy.relations.War;
import at.viswars.enumeration.EDiplomacyRelationType;
import at.viswars.enumeration.ESharingType;
import at.viswars.model.Alliance;
import at.viswars.model.AllianceMember;
import at.viswars.model.DiplomacyRelation;
import at.viswars.model.DiplomacyType;
import at.viswars.model.UserData;
import at.viswars.result.DiplomacyResult;
import at.viswars.service.Service;
import at.viswars.test.AlliedConnector;
import at.viswars.test.EnemyConnector;
import java.lang.reflect.Constructor;
import at.viswars.test.Connector;
import at.viswars.view.DiplomacyManagementView;
import at.viswars.view.DiplomacyManagementViewEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Bullet
 */
public class DiplomacyUtilities extends Service {

    private static HashMap<String, IRelation> classCache;

    public static Connector getDiplomacyRelation(int fromUser, int toUser) {
        try {
            if (AllianceUtilities.areAllied(fromUser, toUser)) {
                return new AlliedConnector();
            }
            DiplomacyRelation dr = getDiplomacyRel(fromUser, toUser);
            if (dr != null) {
            } else {
                return new EnemyConnector();
            }
            //Holen des Typs der diplomatischen Beziehung
            DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());

            IRelation sbe = null;
            if (classCache == null) {
                classCache = new HashMap<String, IRelation>();
            }
            if (classCache.containsKey(dt.getClazz())) {
                sbe = classCache.get(dt.getClazz());
            } else {

                //Holen der Klasse die die diplomatische Beziehung beschreibt
                Class clzz = Class.forName(dt.getClazz());
                //Get Constructor
                Constructor con = clzz.getConstructor();

                //Cast Relation
                sbe = (IRelation) con.newInstance();
                classCache.put(dt.getClazz(), sbe);
            }


            //Get specific settings for this Relation
            DiplomacyResult diplomacyResult = sbe.getDiplomacyResult();
            if (diplomacyResult.isAttacks()) {
                return new EnemyConnector();
            } else if (diplomacyResult.isHelps()) {
                return new AlliedConnector();
            }
        } catch (Exception e) {
            Logger.getLogger().write("Error in DiplomacyUtilities : " + e);
        }
        return new EnemyConnector();


    }

    public static ArrayList<Integer> findViewTableUsers(int userId) {
        ArrayList<Integer> users = new ArrayList<Integer>();

        for (UserData ud : (ArrayList<UserData>) userDataDAO.findAll()) {
            if (ud.getUserId() == userId) {
                continue;
            }

            DiplomacyResult dr = getDiplomacyResult(userId, ud.getUserId());
            if (dr.isSharingMap()) {
                users.add(ud.getUserId());
            }
        }

        return users;
    }

    public static ArrayList<Integer> findViewTableUsersNotInList(ArrayList<Integer> users, int userId) {

        for (UserData ud : (ArrayList<UserData>) userDataDAO.findAll()) {
            if (ud.getUserId() == userId) {
                continue;
            }

            DiplomacyResult dr = getDiplomacyResult(userId, ud.getUserId());
            if (dr.isSharingMap()) {
                if (!users.contains(ud.getUserId()));
                users.add(ud.getUserId());
            }
        }

        return users;
    }

    public static boolean hasRelation(int fromUser, int toUser, int diplomacyTypeId) {

        if (getDiplomacyRel(fromUser, toUser).getDiplomacyTypeId() == diplomacyTypeId) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean hasAllianceRelation(int fromUser, int toUser) {

        return hasRelation(fromUser, toUser, 0);

    }

    public static DiplomacyResult getDiplomacyResult(int fromUser, int toUser) {
        try {
            DiplomacyRelation dr = getDiplomacyRel(fromUser, toUser);


            if (dr == null) {
                return new Neutral().getDiplomacyResult();

            }
            //Holen des Typs der diplomatischen Beziehung
            DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            IRelation sbe = null;
            if (classCache == null) {
                classCache = new HashMap<String, IRelation>();
            }
            if (classCache.containsKey(dt.getClazz())) {
                sbe = classCache.get(dt.getClazz());
            } else {
                //Holen der Klasse die die diplomatische Beziehung beschreibt
                Class clzz = Class.forName(dt.getClazz());
                //Get Constructor
                Constructor con = clzz.getConstructor();

                //Cast Relation
                sbe = (IRelation) con.newInstance();
                classCache.put(dt.getClazz(), sbe);
            }

            //Get specific settings for this Relation
            DiplomacyResult diplomacyResult = sbe.getDiplomacyResult();


            //Optional... look up if he checked it
            if (diplomacyResult.getSharingStarmapInfo().equals(ESharingType.OPTIONAL)) {
                if (dr.getSharingMap()) {
                    diplomacyResult.setSharingStarmapInfo(ESharingType.ALL);
                } else {
                    diplomacyResult.setSharingStarmapInfo(ESharingType.NONE);
                }
            }

            return diplomacyResult;
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger().write("Error in DiplomacyUtilities : " + e);
        }
        return null;


    }

    public static synchronized ArrayList<DiplomacyRelation> getDiplomacyRelsByToId(int toId, EDiplomacyRelationType type) {
        ArrayList<DiplomacyRelation> relations = new ArrayList<DiplomacyRelation>();
        ArrayList<DiplomacyRelation> relBase = diplomacyRelationDAO.findByToId(toId, type);
        // Logger.getLogger().write("Search " + type + " relation with target " + toId + " (Found: " + relBase.size());

        if (type == EDiplomacyRelationType.USER_TO_ALLIANCE) {
            relations.addAll(relBase);
        } else if (type == EDiplomacyRelationType.USER_TO_USER) {
            for (DiplomacyRelation dr : relBase) {
                DiplomacyRelation drNew = getDiplomacyRel(dr.getFromId(), dr.getToId());
                dr.setSharingMap(drNew.getSharingMap());
                dr.setDiplomacyTypeId(drNew.getDiplomacyTypeId());

                // Logger.getLogger().write("Found relation user to user (from " + dr.getFromId() + " to " + dr.getToId() + ") [SHARING: " + dr.getSharingMap() + "]");
            }

            relations.addAll(relBase);
        } else if (type == EDiplomacyRelationType.ALLIANCE_TO_USER) {
            relations.addAll(relBase);

            AllianceMember am = allianceMemberDAO.findByUserId(toId);
            if ((am != null) && !am.getIsTrial()) {
                DiplomacyRelation dr = new DiplomacyRelation();
                dr.setFromId(am.getAllianceId());
                dr.setToId(toId);
                dr.setAcknowledged(true);
                dr.setDiplomacyTypeId(DiplomacyType.ALLY);
                dr.setSharingMap(true);
                dr.setType(EDiplomacyRelationType.ALLIANCE_TO_USER);
                relations.add(dr);
            }
        } else if (type == EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE) {
            relations.addAll(relBase);
        }

        return relations;
    }

    public static synchronized ArrayList<DiplomacyRelation> getDiplomacyRelsByFromId(int fromId, EDiplomacyRelationType type) {
        ArrayList<DiplomacyRelation> relations = new ArrayList<DiplomacyRelation>();
        ArrayList<DiplomacyRelation> relBase = diplomacyRelationDAO.findByFromId(fromId, type);

        for (Iterator<DiplomacyRelation> drIt = relBase.iterator();drIt.hasNext();) {
            DiplomacyRelation drTmp = (DiplomacyRelation)drIt.next();
            if (drTmp == null) {
                DebugBuffer.warning("DiplomacyRelation BASE content ("+fromId+"/"+type+") contains "+drTmp+" - please check why ;)");
                drIt.remove();
            }
        }

        if (type == EDiplomacyRelationType.USER_TO_ALLIANCE) {
            relations.addAll(relBase);

            AllianceMember am = allianceMemberDAO.findByUserId(fromId);
            if (am != null) {
                DiplomacyRelation dr = new DiplomacyRelation();
                dr.setFromId(fromId);
                dr.setToId(am.getAllianceId());
                dr.setAcknowledged(true);
                dr.setDiplomacyTypeId(DiplomacyType.ALLY);
                dr.setSharingMap(true);
                dr.setType(EDiplomacyRelationType.USER_TO_ALLIANCE);
                relations.add(dr);
            }
        } else if (type == EDiplomacyRelationType.USER_TO_USER) {
            for (DiplomacyRelation dr : relBase) {
                DiplomacyRelation drNew = getDiplomacyRel(dr.getFromId(), dr.getToId());
                dr.setSharingMap(drNew.getSharingMap());
                dr.setDiplomacyTypeId(drNew.getDiplomacyTypeId());

                // Logger.getLogger().write("Found relation user to user (from " + dr.getFromId() + " to " + dr.getToId() + ") [SHARING: " + dr.getSharingMap() + "]");
            }

            relations.addAll(relBase);
        } else if (type == EDiplomacyRelationType.ALLIANCE_TO_USER) {
            relations.addAll(relBase);
        } else if (type == EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE) {
            relations.addAll(relBase);
        }

        return relations;
    }

    public static DiplomacyRelation getDiplomacyRelationAllianceToAlliance(int allianceId, int allianceId2) {
        DiplomacyRelation dr = null;

        ArrayList<DiplomacyRelation> drResult = diplomacyRelationDAO.findBy(allianceId, allianceId2, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);

        for (Iterator<DiplomacyRelation> drIt = drResult.iterator();drIt.hasNext();) {
            DiplomacyRelation drTmp = drIt.next();
            if (drTmp.getPending()) drIt.remove();
        }

        if (drResult.isEmpty()) {
            dr = new DiplomacyRelation();
            dr.setFromId(allianceId);
            dr.setToId(allianceId2);
            dr.setAcknowledged(true);
            dr.setDiplomacyTypeId(DiplomacyType.NEUTRAL);
            dr.setSharingMap(false);
            dr.setType(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            return dr;
        }

        return drResult.get(0);
    }

    public static DiplomacyRelation getDiplomacyRelationAllianceToUser(int allianceId, int userId) {
        DiplomacyRelation dr = null;

        ArrayList<DiplomacyRelation> drResult = diplomacyRelationDAO.findBy(allianceId, userId, EDiplomacyRelationType.ALLIANCE_TO_USER);

        for (Iterator<DiplomacyRelation> drIt = drResult.iterator();drIt.hasNext();) {
            DiplomacyRelation drTmp = drIt.next();
            if (drTmp.getPending()) drIt.remove();
        }

        if (drResult.isEmpty()) {
            dr = new DiplomacyRelation();
            dr.setFromId(allianceId);
            dr.setToId(userId);
            dr.setAcknowledged(true);
            dr.setDiplomacyTypeId(DiplomacyType.NEUTRAL);
            dr.setSharingMap(false);
            dr.setType(EDiplomacyRelationType.ALLIANCE_TO_USER);
            return dr;
        }

        return drResult.get(0);
    }

    public static DiplomacyRelation getDiplomacyRelationUserToAlliance(int userId, int allianceId) {
        DiplomacyRelation dr = null;

        ArrayList<DiplomacyRelation> drResult = diplomacyRelationDAO.findBy(userId, allianceId, EDiplomacyRelationType.USER_TO_ALLIANCE);

        for (Iterator<DiplomacyRelation> drIt = drResult.iterator();drIt.hasNext();) {
            DiplomacyRelation drTmp = drIt.next();
            if (drTmp.getPending()) drIt.remove();
        }

        if (drResult.isEmpty()) {
            dr = new DiplomacyRelation();
            dr.setFromId(userId);
            dr.setToId(allianceId);
            dr.setAcknowledged(true);
            dr.setDiplomacyTypeId(DiplomacyType.NEUTRAL);
            dr.setSharingMap(false);
            dr.setType(EDiplomacyRelationType.USER_TO_ALLIANCE);
            return dr;
        }

        return drResult.get(0);
    }

    public static DiplomacyRelation getDiplomacyRel(int fromUser, int toUser) {
        DiplomacyRelation dr = null;
        //Check vor AllianceRelation
        if (AllianceUtilities.areAllied(fromUser, toUser)) {
            dr = new DiplomacyRelation();
            dr.setFromId(fromUser);
            dr.setToId(toUser);
            dr.setAcknowledged(true);
            dr.setDiplomacyTypeId(DiplomacyType.ALLY);

            AllianceMember am = allianceMemberDAO.findByUserId(toUser);
            dr.setSharingMap(!am.getIsTrial());
            dr.setType(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            return dr;
        }
        int fromAllianceId = 0;
        int toAllianceId = 0;
        if (AllianceUtilities.hasAlliance(fromUser)) {
            fromAllianceId = allianceMemberDAO.findByUserId(fromUser).getAllianceId();
        }
        if (AllianceUtilities.hasAlliance(toUser)) {
            toAllianceId = allianceMemberDAO.findByUserId(toUser).getAllianceId();
        }
        if (fromAllianceId > 0 && toAllianceId > 0) {
            dr = diplomacyRelationDAO.findBy(fromAllianceId, toAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE, false);
        }
        //Find Alliance User Relation
        if (dr == null) {
            if (fromAllianceId > 0) {
                dr = diplomacyRelationDAO.findBy(fromAllianceId, toUser, EDiplomacyRelationType.ALLIANCE_TO_USER, false);
            }
        }
        if (dr == null) {
            if (toAllianceId > 0) {
                dr = diplomacyRelationDAO.findBy(fromUser, toAllianceId, EDiplomacyRelationType.USER_TO_ALLIANCE, false);
            }
        }
        if (dr == null) {
            dr = diplomacyRelationDAO.findBy(fromUser, toUser, EDiplomacyRelationType.USER_TO_USER, false);

        }
        if (dr == null) {
            dr = new DiplomacyRelation();
            dr.setFromId(fromUser);
            dr.setToId(toUser);
            dr.setAcknowledged(true);
            dr.setDiplomacyTypeId(DiplomacyType.NEUTRAL);
            dr.setSharingMap(false);
            dr.setType(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            return dr;
        }
        return dr;
    }

    public static String getColor(EOwner eOwner) {

        if (eOwner.equals(EOwner.AGGRESSIVE)) {
            return new Aggressive().getDiplomacyResult().getColor();
        } else if (eOwner.equals(EOwner.ALLY)) {
            return new Ally().getDiplomacyResult().getColor();
        } else if (eOwner.equals(EOwner.TRADE)) {
            return new Trade().getDiplomacyResult().getColor();
        } else if (eOwner.equals(EOwner.TREATY)) {
            return new Treaty().getDiplomacyResult().getColor();
        } else if (eOwner.equals(EOwner.FRIENDLY)) {
            return new Friendly().getDiplomacyResult().getColor();
        } else if (eOwner.equals(EOwner.WAR)) {
            return new War().getDiplomacyResult().getColor();
        } else if (eOwner.equals(EOwner.NEUTRAL)) {
            return new Neutral().getDiplomacyResult().getColor();
        } else if (eOwner.equals(EOwner.NAP)) {
            return new Nap().getDiplomacyResult().getColor();
        } else if (eOwner.equals(EOwner.OWN)) {
            return "#33CC00";
        } else if (eOwner.equals(EOwner.INHABITATED)) {
            return "#FFFFFF";
        } else {
            return "#FFFFFF";
        }
    }

    // ********************************************************************
    // Get user may get shared data from input user
    // ********************************************************************
    public static synchronized ArrayList<UserShareEntry> getAffectedShareUsers(int userId) {
        HashSet<Integer> processedUsers = new HashSet<Integer>();
        HashSet<Integer> processedAlliances = new HashSet<Integer>();
        ArrayList<UserShareEntry> shares = new ArrayList<UserShareEntry>();

        ArrayList<DiplomacyRelation> drListAlly = getDiplomacyRelsByFromId(userId, EDiplomacyRelationType.USER_TO_ALLIANCE);

        for (DiplomacyRelation dr : drListAlly) {
            if (!dr.getSharingMap()) {
                continue;
            }
            if (processedAlliances.contains(dr.getToId())) {
                continue;
            }

            // Logger.getLogger().write("Process Alliance Dependency from user "+userId+" to alliance " + dr.getToId());
            processAllianceDependency(dr.getToId(), shares, processedUsers, processedAlliances);
        }

        ArrayList<DiplomacyRelation> drListUser = getDiplomacyRelsByFromId(userId, EDiplomacyRelationType.USER_TO_USER);
        for (DiplomacyRelation dr : drListUser) {
            if (!dr.getSharingMap()) {
                continue;
            }
            if (processedUsers.contains(dr.getToId())) {
                continue;
            }

            // Logger.getLogger().write("[1] Process User Dependency from user " + userId + " to " + dr.getToId());
            processUserDependency(dr.getToId(), shares, processedUsers, processedAlliances);
        }

        if (!processedUsers.contains(userId)) {
            processedUsers.add(userId);
            shares.add(new UserShareEntry(userId));
        }

        return shares;
    }

    private static void processAllianceDependency(int allianceId,
            ArrayList<UserShareEntry> shares, HashSet<Integer> processedUsers,
            HashSet<Integer> processedAlliances) {
        if (processedAlliances.contains(allianceId)) {
            return;
        }

        Alliance a = allianceDAO.findById(allianceId);
        ArrayList<Alliance> aList = allianceDAO.findByMasterId(a.getMasterAlliance());

        ArrayList<Integer> user = new ArrayList<Integer>();
        for (Iterator<Alliance> aIt = aList.iterator(); aIt.hasNext();) {
            Alliance aInner = aIt.next();
            if (processedAlliances.contains(aInner.getId())) {
                aIt.remove();
                continue;
            }

            processedAlliances.add(aInner.getId());
            ArrayList<AllianceMember> amList = allianceMemberDAO.findByAllianceIdNotTrial(aInner.getId());
            for (AllianceMember amInner : amList) {
                processedUsers.add(amInner.getUserId());
                user.add(amInner.getUserId());
            }
        }

        shares.add(new UserShareEntry(a.getMasterAlliance(), user));

        for (Alliance aInner : aList) {
            ArrayList<DiplomacyRelation> drListAlly = getDiplomacyRelsByFromId(aInner.getId(), EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            for (DiplomacyRelation dr : drListAlly) {
                if (!dr.getSharingMap()) {
                    continue;
                }
                if (processedAlliances.contains(dr.getToId())) {
                    continue;
                }

                // Logger.getLogger().write("Process Alliance Dependency from Alliance " + aInner.getId() + " to Alliance " + dr.getToId());
                processAllianceDependency(dr.getToId(), shares, processedUsers, processedAlliances);
            }

            ArrayList<DiplomacyRelation> drListUser = getDiplomacyRelsByFromId(aInner.getId(), EDiplomacyRelationType.ALLIANCE_TO_USER);
            for (DiplomacyRelation dr : drListUser) {
                if (!dr.getSharingMap()) {
                    continue;
                }
                if (processedUsers.contains(dr.getToId())) {
                    continue;
                }

                // Logger.getLogger().write("Process User Dependency from Alliance " + aInner.getId() + " to User " + dr.getToId());
                processUserDependency(dr.getToId(), shares, processedUsers, processedAlliances);
            }
        }

        for (Integer userId : user) {
            processUserDependency(userId, shares, processedUsers, processedAlliances);
        }
        if (!processedAlliances.contains(allianceId)) {
            processedAlliances.add(allianceId);
        }
    }

    private static void processUserDependency(int userId,
            ArrayList<UserShareEntry> shares, HashSet<Integer> processedUsers,
            HashSet<Integer> processedAlliances) {
        // Logger.getLogger().write("Process User " + userId);       

        ArrayList<DiplomacyRelation> drListAlly = getDiplomacyRelsByFromId(userId, EDiplomacyRelationType.USER_TO_ALLIANCE);
        for (DiplomacyRelation dr : drListAlly) {
            if (!dr.getSharingMap()) {
                continue;
            }
            if (processedAlliances.contains(dr.getToId())) {
                continue;
            }

            // Logger.getLogger().write("Process Alliance Dependency from user "+userId+" to alliance " + dr.getToId());
            processAllianceDependency(dr.getToId(), shares, processedUsers, processedAlliances);
        }

        ArrayList<DiplomacyRelation> drListUser = getDiplomacyRelsByFromId(userId, EDiplomacyRelationType.USER_TO_USER);

        if (drListUser == null) DebugBuffer.warning("DiplomacyRelation List is null for ("+userId+"/"+EDiplomacyRelationType.USER_TO_USER+")");
        for (DiplomacyRelation dr : drListUser) {
            if (dr == null) DebugBuffer.warning("DiplomacyRelation is null for ("+userId+"/"+EDiplomacyRelationType.USER_TO_USER+")");
        }

        for (DiplomacyRelation dr : drListUser) {
            if (!dr.getSharingMap()) {
                continue;
            }
            if (processedUsers.contains(dr.getToId())) {
                continue;
            }

            // Logger.getLogger().write("[2] Process User Dependency from user "+userId+" to user " + dr.getToId());
            if (!processedUsers.contains(userId)) {
                processedUsers.add(userId);
                shares.add(new UserShareEntry(userId));
            }
            processUserDependency(dr.getToId(), shares, processedUsers, processedAlliances);
        }

        if (!processedUsers.contains(userId)) {
            shares.add(new UserShareEntry(userId));
        }
    }

    public static synchronized ArrayList<Integer> findSharingUsers(int userId) {

        ArrayList<Integer> result = new ArrayList<Integer>();
        ArrayList<UserShareEntry> uses = getSharingUsers(userId);

        for (UserShareEntry use : uses) {
            result.addAll(use.getUsers());
        }

        return result;

    }

    // ********************************************************************
    // Get all users which share to me
    // ********************************************************************    
    public static synchronized ArrayList<UserShareEntry> getSharingUsers(int userId) {
        HashSet<Integer> processedUsers = new HashSet<Integer>();
        HashSet<Integer> processedAlliances = new HashSet<Integer>();
        ArrayList<UserShareEntry> shares = new ArrayList<UserShareEntry>();
        // Logger.getLogger().write("Determine users sharing to " + userId);

        ArrayList<DiplomacyRelation> drListAlly = getDiplomacyRelsByToId(userId, EDiplomacyRelationType.ALLIANCE_TO_USER);
        for (DiplomacyRelation dr : drListAlly) {
            if (!dr.getAcknowledged()) {
                continue;
            }
            if (!dr.getSharingMap()) {
                continue;
            }
            if (processedAlliances.contains(dr.getFromId())) {
                continue;
            }

            // Logger.getLogger().write("Process Alliance Dependency from Alliance " + dr.getFromId() + " to user " + userId);
            // If this user is only trial skip this check
            processAllianceDependency2(dr.getFromId(), shares, processedUsers, processedAlliances);
        }

        ArrayList<DiplomacyRelation> drListUser = getDiplomacyRelsByToId(userId, EDiplomacyRelationType.USER_TO_USER);
        for (DiplomacyRelation dr : drListUser) {
            if (!dr.getAcknowledged()) {
                continue;
            }
            // Logger.getLogger().write("Sharing : " + dr.getSharingMap() + " processedUsers " + processedUsers.contains(dr.getToId()));
            if (!dr.getSharingMap() && !processedUsers.contains(dr.getToId())) {
                continue;
            }
            if (processedUsers.contains(dr.getToId())) {
                continue;
            }

            // Logger.getLogger().write("[1] Process User Dependency from user " + userId + " to " + dr.getToId());
            processUserDependency2(dr.getToId(), shares, processedUsers, processedAlliances);
        }

        if (!processedUsers.contains(userId)) {
            processedUsers.add(userId);
            shares.add(new UserShareEntry(userId));
        }

        return shares;
    }

    private static void processAllianceDependency2(int allianceId,
            ArrayList<UserShareEntry> shares, HashSet<Integer> processedUsers,
            HashSet<Integer> processedAlliances) {
        if (processedAlliances.contains(allianceId)) {
            return;
        }

        Logger.getLogger().write("Processing AllianceDependency2 for [" + allianceId + "]");

        Alliance a = allianceDAO.findById(allianceId);
        ArrayList<Alliance> aList = allianceDAO.findByMasterId(a.getMasterAlliance());

        ArrayList<Integer> user = new ArrayList<Integer>();
        for (Iterator<Alliance> aIt = aList.iterator(); aIt.hasNext();) {
            Alliance aInner = aIt.next();
            if (processedAlliances.contains(aInner.getId())) {
                aIt.remove();
                continue;
            }

            processedAlliances.add(aInner.getId());
            ArrayList<AllianceMember> amList = allianceMemberDAO.findByAllianceId(aInner.getId());
            for (AllianceMember amInner : amList) {
                processedUsers.add(amInner.getUserId());
                user.add(amInner.getUserId());
            }
        }

        shares.add(new UserShareEntry(a.getMasterAlliance(), user));

        for (Alliance aInner : aList) {
            Logger.getLogger().write("Check Alliance " + aInner.getId());
            ArrayList<DiplomacyRelation> drListAlly = getDiplomacyRelsByToId(aInner.getId(), EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            for (DiplomacyRelation dr : drListAlly) {
                if (!dr.getAcknowledged()) {
                    continue;
                }
                if (!dr.getSharingMap()) {
                    continue;
                }
                if (processedAlliances.contains(dr.getFromId())) {
                    continue;
                }

                Logger.getLogger().write("[A] Process Alliance Dependency from Alliance " + dr.getFromId() + " to Alliance " + aInner.getId());
                processAllianceDependency2(dr.getFromId(), shares, processedUsers, processedAlliances);
            }

            ArrayList<DiplomacyRelation> drListUser = getDiplomacyRelsByToId(aInner.getId(), EDiplomacyRelationType.USER_TO_ALLIANCE);
            for (DiplomacyRelation dr : drListUser) {
                if (!dr.getAcknowledged()) {
                    continue;
                }
                if (!dr.getSharingMap()) {
                    continue;
                }
                if (processedUsers.contains(dr.getFromId())) {
                    continue;
                }

                Logger.getLogger().write("[A] Process User Dependency from User " + dr.getFromId() + " to Alliance " + aInner.getId());
                processUserDependency2(dr.getFromId(), shares, processedUsers, processedAlliances);
            }
        }

        for (Integer userId : user) {
            processUserDependency2(userId, shares, processedUsers, processedAlliances);
        }
    }

    public static DiplomacyRelation getAllyRelation(int userId1, int userId2) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setFromId(userId1);
        dr.setToId(userId2);
        dr.setAcknowledged(true);
        dr.setDiplomacyTypeId(DiplomacyType.ALLY);
        dr.setSharingMap(false);
        dr.setType(EDiplomacyRelationType.USER_TO_USER);
        return dr;
    }

    public static DiplomacyRelation getHostileRelation(int userId1, int userId2) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setFromId(userId1);
        dr.setToId(userId2);
        dr.setAcknowledged(true);
        dr.setDiplomacyTypeId(DiplomacyType.WAR);
        dr.setSharingMap(false);
        dr.setType(EDiplomacyRelationType.USER_TO_USER);
        return dr;
    }

    private static void processUserDependency2(int userId,
            ArrayList<UserShareEntry> shares, HashSet<Integer> processedUsers,
            HashSet<Integer> processedAlliances) {


        Logger.getLogger().write("Processing UserDependency2 for [" + userId + "]");

        ArrayList<DiplomacyRelation> drListAlly = getDiplomacyRelsByToId(userId, EDiplomacyRelationType.ALLIANCE_TO_USER);
        for (DiplomacyRelation dr : drListAlly) {
            if (!dr.getAcknowledged()) {
                continue;
            }
            if (!dr.getSharingMap()) {
                continue;
            }
            if (processedAlliances.contains(dr.getFromId())) {
                continue;
            }

            Logger.getLogger().write("[U] Process Alliance Dependency from alliance " + dr.getFromId() + " to user " + dr.getToId());
            processAllianceDependency2(dr.getFromId(), shares, processedUsers, processedAlliances);
        }

        ArrayList<DiplomacyRelation> drListUser = getDiplomacyRelsByToId(userId, EDiplomacyRelationType.USER_TO_USER);
        for (DiplomacyRelation dr : drListUser) {
            if (!dr.getAcknowledged()) {
                continue;
            }
            if (!dr.getSharingMap()) {
                continue;
            }
            if (processedUsers.contains(dr.getFromId())) {
                continue;
            }

            Logger.getLogger().write("[U] Process User Dependency from user " + dr.getFromId() + " to user " + userId);
            if (!processedUsers.contains(userId)) {
                processedUsers.add(userId);
                shares.add(new UserShareEntry(userId));
            }

            processUserDependency2(dr.getFromId(), shares, processedUsers, processedAlliances);
        }

        if (!processedUsers.contains(userId)) {
            processedUsers.add(userId);
            shares.add(new UserShareEntry(userId));
        }
    }

    public static void calcDiplomacyOverrides(DiplomacyManagementView dmv) {
        // Player to Player Relations my be override by
        // Player to Alliance
        // Alliance to Player
        // Alliance to Alliance

        // Check Player to Player Relations and if they are overridden
        for (DiplomacyManagementViewEntry dmve : dmv.getPlayerToPlayer()) {
            int targetUserId = dmve.getBase().getToId();

            // Check if targetUser is in an Alliance
            AllianceMember amTarget = allianceMemberDAO.findByUserId(targetUserId);

            if (amTarget != null) {
                int targetAllianceId = amTarget.getAllianceId();

                for (DiplomacyManagementViewEntry dmveP2A : dmv.getPlayerToAlliance()) {
                    DiplomacyRelation dr = dmveP2A.getBase();
                    if (dr.getPending()) continue;

                    if (dr.getToId() == targetAllianceId) {
                        dmve.setOverridden(true);
                        dmve.setOverriddenBy(dmveP2A);
                    }
                }
            }

            // Check if own alliance has overriding entries
            for (DiplomacyManagementViewEntry dmveA2P : dmv.getAllianceToPlayer()) {
                DiplomacyRelation dr = dmveA2P.getBase();
                if (dr.getPending()) continue;

                if (dr.getToId() == targetUserId) {
                    dmve.setOverridden(true);
                    dmve.setOverriddenBy(dmveA2P);
                }
            }

            // If 2 players are in same alliance .. the relation is overridden as well
            AllianceMember amSource = allianceMemberDAO.findByUserId(dmve.getBase().getFromId());
            if ((amSource != null) && (amTarget != null)) {
                if (amSource.getAllianceId().equals(amTarget.getAllianceId())) {
                    dmve.setOverridden(true);
                }
            }
        }

        // P2A and A2P relations may be overriden by A2A relations
        for (DiplomacyManagementViewEntry dmve : dmv.getPlayerToAlliance()) {
            for (DiplomacyManagementViewEntry dmveA2A : dmv.getAllianceToAlliance()) {
                DiplomacyRelation dr = dmveA2A.getBase();
                if (dr.getPending()) continue;

                if (dr.getToId() == dmve.getBase().getToId()) {
                    dmve.setOverridden(true);
                    dmve.setOverriddenBy(dmveA2A);
                }
            }
        }

        for (DiplomacyManagementViewEntry dmve : dmv.getAllianceToPlayer()) {
            // Check if targetUser is in an Alliance
            int targetUserId = dmve.getBase().getToId();
            AllianceMember amTarget = allianceMemberDAO.findByUserId(targetUserId);

            if (amTarget != null) {
                int targetAllianceId = amTarget.getAllianceId();

                for (DiplomacyManagementViewEntry dmveA2A : dmv.getAllianceToAlliance()) {
                    DiplomacyRelation dr = dmveA2A.getBase();
                    if (dr.getPending()) continue;

                    if (dr.getToId() == targetAllianceId) {
                        dmve.setOverridden(true);
                        dmve.setOverriddenBy(dmveA2A);
                    }
                }
            }
        }
    }
}
