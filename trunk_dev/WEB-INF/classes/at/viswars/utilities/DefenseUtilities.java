/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.utilities;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.UserDataDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.model.Model;
import at.viswars.model.Ressource;
import at.viswars.model.ShipDesign;
import at.viswars.model.UserData;
import at.viswars.ships.ShipScrapCost;
import at.viswars.ships.ShipUpgradeCost;
import at.viswars.view.ShipModifyEntry;

/**
 *
 * @author Stefan
 */
public class DefenseUtilities {
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    
    public static boolean canBeUpgraded(Model from, Model to, int userId, int planetId, int count) {
        ShipDesign SDFrom = (ShipDesign)from;
        ShipDesign SDTo = (ShipDesign)to;
               
        if (SDFrom.getUserId() != userId) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 1 ("+userId+"<>"+SDFrom.getUserId()+")");
            return false;
        }
        
        if (SDFrom.getUserId() != SDTo.getUserId()) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 2");
            return false;
        }
        
        ShipModifyEntry sme = ShipUtilities.getShipModifyEntry(SDFrom, planetId);
        if (sme.getCount() < count) return false;
        
        ShipDesignExt SDEFrom = new ShipDesignExt(SDFrom.getId());
        ShipDesignExt SDETo = new ShipDesignExt(SDTo.getId());
        
        ShipUpgradeCost suc = SDEFrom.getShipUpgradeCost(SDETo, count);
        
        MutableRessourcesEntry mre = new MutableRessourcesEntry(userId,planetId);
        RessourcesEntry re = new RessourcesEntry(suc.getRess());
        mre.subtractRess(re, count);
        
        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getQty() < 0) {
                DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 4 With ress " + rae.getRessId() + " >> "+ rae.getQty());
                return false;
            }
        }
        
        return true;        
    }
    
    public static boolean canBeScrapped(Model ship, int userId, int planetId, int count) {
        ShipDesign sd = (ShipDesign)ship;
        
        if (sd.getUserId() != userId) {
            return false;
        }
        
        ShipModifyEntry sme = ShipUtilities.getShipModifyEntry(sd, planetId);
        if (sme.getCount() < count) return false;
        
        UserData ud = udDAO.findByUserId(userId);        
        ShipDesignExt sde = new ShipDesignExt(sd.getId());
        ShipScrapCost ssc = sde.getShipScrapCost(count);
        
        if (ud.getCredits() < ssc.getRess(Ressource.CREDITS)) {
            return false;
        }
        
        return true;
    }    
}
