/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars;

import at.viswars.model.Construction;
import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import at.viswars.scanner.StarMapQuadTree;
import at.viswars.scanner.SystemDesc;
import at.viswars.service.Service;
import at.viswars.utilities.DiplomacyUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Aion
 */
public class StarMapInfoData extends Service {
    private ArrayList<Integer> sharingUsers;
    private HashMap<Integer, ArrayList<Planet>> planetList;
    private HashMap<Integer, ArrayList<at.viswars.model.System>> hyperscanners;
     private HashMap<Integer, ArrayList<at.viswars.model.System>> observatories;
    private StarMapQuadTree hyperArea;
    int userId;

    public StarMapInfoData(int userId) {
        this.userId = userId;
        init();
    }

    private void init() {

        sharingUsers = DiplomacyUtilities.findSharingUsers(userId);
        planetList = planetDAO.findAllSortedBySystem();
        hyperscanners = findHyperscanners(userId, getSharingUsers());
        hyperArea = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
       observatories = findObservatories(userId);

        //Adding All scanners to the quadtree
        for (Map.Entry<Integer, ArrayList<at.viswars.model.System>> entry0 : hyperscanners.entrySet()) {
            for (at.viswars.model.System entry : entry0.getValue()) {
                at.viswars.model.System s = entry;
                SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
                hyperArea.addItemToTree(sd);
            }
        }
    }

    private static HashMap<Integer, ArrayList<at.viswars.model.System>> findHyperscanners(int userId, ArrayList<Integer> sharingUsers) {
        HashMap<Integer, ArrayList<at.viswars.model.System>> hyperscanners = new HashMap<Integer, ArrayList<at.viswars.model.System>>();
        ArrayList<PlayerPlanet> pps = playerPlanetDAO.findAll();

        for (PlayerPlanet pp : pps) {
            if (pp.getUserId() != userId
                    && !sharingUsers.contains(pp.getUserId())) {
                continue;
            }
            if (planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_HYPERSPACESCANNER) != null) {
                ArrayList<at.viswars.model.System> entries = hyperscanners.get(pp.getUserId());

                if (entries == null) {
                    entries = new ArrayList<at.viswars.model.System>();
                }
                entries.add(systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId()));
                hyperscanners.put(pp.getUserId(), entries);
            }

        }
        return hyperscanners;


    }

    private static HashMap<Integer, ArrayList<at.viswars.model.System>> findObservatories(int userId) {
        HashMap<Integer, ArrayList<at.viswars.model.System>> observatories = new HashMap<Integer, ArrayList<at.viswars.model.System>>();
        ArrayList<PlayerPlanet> pps = playerPlanetDAO.findAll();

        for (PlayerPlanet pp : pps) {
            if (pp.getUserId() != userId) {
                continue;
            }

            if (planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_OBSERVATORY) != null) {
                ArrayList<at.viswars.model.System> entries = observatories.get(pp.getUserId());
                if (entries == null) {
                    entries = new ArrayList<at.viswars.model.System>();
                }
                if (!entries.contains(systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId()))) {
                    entries.add(systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId()));
                }
                observatories.put(pp.getUserId(), entries);
            }
        }
        return observatories;
    }

    /**
     * @return the sharingUsers
     */
    public ArrayList<Integer> getSharingUsers() {
        return sharingUsers;
    }

    /**
     * @return the planetList
     */
    public HashMap<Integer, ArrayList<Planet>> getPlanetList() {
        return planetList;
    }

    /**
     * @return the hyperscanners
     */
    public HashMap<Integer, ArrayList<at.viswars.model.System>> getHyperscanners() {
        return hyperscanners;
    }

    /**
     * @return the hyperArea
     */
    public StarMapQuadTree getHyperArea() {
        return hyperArea;
    }

    /**
     * @return the observatories
     */
    public HashMap<Integer, ArrayList<at.viswars.model.System>> getObservatories() {
        return observatories;
    }
}
