/*
 * Colonizing.java
 *
 * Created on 13. Oktober 2005, 21:36
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package at.viswars;

import at.viswars.ships.ShipData;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.dao.*;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.enumeration.EShipType;
import at.viswars.model.*;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.result.BaseResult;
import at.viswars.scanner.StarMapQuadTree;
import at.viswars.scanner.SystemDesc;
import at.viswars.service.LoginService;
import at.viswars.service.ResearchService;
import at.viswars.utilities.ViewTableUtilities;
import at.viswars.viewbuffer.HeaderBuffer;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class Colonizing {

    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlayerResearchDAO pResDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetLoyalityDAO pLoyalDAO = (PlanetLoyalityDAO) DAOFactory.get(PlanetLoyalityDAO.class);
    
    public final static int MAX_PLANET_COUNT_TRIAL = 10;
    public final static int MAX_SYSTEM_COUNT_TRIAL = 3;

    public static boolean calcColonizeableByFleet(PlayerFleetExt pfe) {
        if (pfe.getRelativeCoordinate().getPlanetId() <= 0) {
            return false;
        }

        if (pfe.canColonize()) {
            if (ppDAO.findByPlanetId(pfe.getRelativeCoordinate().getPlanetId()) == null) {
                // Check type of planet and if all necessary buildings are researched
                Planet p = pDAO.findById(pfe.getRelativeCoordinate().getPlanetId());
                if (p.getLandType().equalsIgnoreCase("A") || p.getLandType().equalsIgnoreCase("B")) {
                    return ResearchService.isResearched(pfe.getUserId(), Research.ID_ORBITALKOLONIE);
                } else {
                    return true;
                }
            }
        }

        return false;
    }

    public static BaseResult colonize(PlayerFleetExt pfe) {
        boolean isColonizeable = calcColonizeableByFleet(pfe);
        if (!isColonizeable) {
            // errorTxt = "<FONT size=-1>ERROR!<BR>Planet (" + planetId + ") not available for colonizing!<BR>Administrator has been informed.</FONT>";
            // DebugBuffer.addLine("COLONIZE: " + errorTxt);
            return new BaseResult("Planet (" + pfe.getRelativeCoordinate().getPlanetId() + ") not available for colonizing!", true);
        }

        boolean needsOrbital = false;
        boolean inhabitable = false;

        int planetId = pfe.getRelativeCoordinate().getPlanetId();

        BaseResult endResult = null;
        TransactionHandler th = TransactionHandler.getTransactionHandler();

        try {
            // Check ob Orbital oder normal
            Planet p = pDAO.findById(planetId);
            int systemId = p.getSystemId();

            if (p == null) {
                return new BaseResult("No Planet Found for Colonization (" + planetId + ")", true);
            }

            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_A) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_B)) {
                needsOrbital = true;
            } else if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {
            } else {
                inhabitable = true;
            }

            // Get chassisSize and adjust Factors
            int chassisSize = 0;
            int selectedDesignId = 0;

            ArrayList<ShipData> sdList = pfe.getShipList();
            for (ShipData sd : sdList) {
                ShipDesign sDesign = sdDAO.findById(sd.getDesignId());
                if (sDesign.getType() == EShipType.COLONYSHIP) {
                    chassisSize = sDesign.getChassis();
                    selectedDesignId = sDesign.getId();
                    break;
                }
            }

            if (chassisSize == 0) {
                return new BaseResult("No appropriate ship found", true);
            }

            boolean fusionResearched = ResearchService.isResearched(pfe.getUserId(), Research.ID_FUSIONPLANT);

            th.startTransaction();

            ArrayList<Integer[]> buildingsG = new ArrayList<Integer[]>();
            ArrayList<Integer[]> buildingsO = new ArrayList<Integer[]>();
            ArrayList<Integer[]> buildingsI = new ArrayList<Integer[]>();
            int sPop = 10000;
            int ressMod = 1;

            switch (chassisSize) {
                case (Chassis.ID_FRIGATE):
                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1});

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    break;
                case (Chassis.ID_DESTROYER):
                    ressMod = 2;
                    sPop = 30000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    buildingsG.add(new Integer[]{Construction.ID_SMALLAGRICULTUREFARM, 1}); // Small Agrar
                    buildingsG.add(new Integer[]{Construction.ID_SMALLRESSOURCESTORAGE, 1}); // Small Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 1}); // Orbital Storage

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    buildingsI.add(new Integer[]{Construction.ID_SMALLRESSOURCESTORAGE, 1}); // Small Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 1}); // Hydroponic farm
                    break;
                case (Chassis.ID_CRUISER):
                    ressMod = 5;
                    sPop = 80000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsG.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 2});
                    } else {
                        buildingsG.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 1});
                    }
                    buildingsG.add(new Integer[]{Construction.ID_BIGAGRICULTUREFARM, 1});
                    buildingsG.add(new Integer[]{Construction.ID_SMALLRESSOURCESTORAGE, 1}); // Small Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 1}); // Orbital Storage
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_ACCOMODATIONUNIT, 1}); // Orbital Hydro
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_HYDROPONDICFARM, 1}); // Orbital Habitat

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    buildingsI.add(new Integer[]{Construction.ID_SMALLRESSOURCESTORAGE, 1}); // Small Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 1}); // Hydroponic farm
                    buildingsI.add(new Integer[]{Construction.ID_ACCOMODATIONUNIT, 1}); // Habitat
                    break;
                case (Chassis.ID_BATTLESHIP):
                    ressMod = 20;
                    sPop = 240000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsG.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 4});
                    } else {
                        buildingsG.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 2});
                    }
                    buildingsG.add(new Integer[]{Construction.ID_AGROCOMPLEX, 1}); // Agrarkomplex
                    buildingsG.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 1}); // Large Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 3}); // Orbital Storage
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_ACCOMODATIONUNIT, 5}); // Orbital Hydro
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_HYDROPONDICFARM, 5}); // Orbital Habitat

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsI.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 2});
                    } else {
                        buildingsI.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 1});
                    }
                    buildingsI.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 1}); // Large Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 5}); // Hydroponic farm
                    buildingsI.add(new Integer[]{Construction.ID_ACCOMODATIONUNIT, 5}); // Habitat
                    break;
                case (Chassis.ID_SUPERBATTLESHIP):
                    ressMod = 60;
                    sPop = 1400000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1}); // Col Center
                    if (!fusionResearched) {
                        buildingsG.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 16});
                    } else {
                        buildingsG.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 8});
                    }
                    buildingsG.add(new Integer[]{Construction.ID_AGROCOMPLEX, 2}); // Agrarkomplex
                    buildingsG.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 3}); // Large Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 9}); // Orbital Storage
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_ACCOMODATIONUNIT, 28}); // Orbital Hydro
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_HYDROPONDICFARM, 28}); // Orbital Habitat

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsI.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 8});
                    } else {
                        buildingsI.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 4});
                    }
                    buildingsI.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 2}); // Large Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 28}); // Hydroponic farm
                    buildingsI.add(new Integer[]{Construction.ID_ACCOMODATIONUNIT, 28}); // Habitat
                    break;
                case (Chassis.ID_TENDER):
                    ressMod = 120;
                    sPop = 4200000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1}); // Col Center
                    if (!fusionResearched) {
                        buildingsG.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 16});
                    } else {
                        buildingsG.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 8});
                    }
                    buildingsG.add(new Integer[]{Construction.ID_AGROCOMPLEX, 6}); // Agrarkomplex
                    buildingsG.add(new Integer[]{Construction.ID_COMPLEXIRONMINE, 2}); // Agrarkomplex
                    buildingsG.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 3}); // Large Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 10}); // Orbital Storage
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_ACCOMODATIONUNIT, 50}); // Orbital Hydro
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_HYDROPONDICFARM, 50}); // Orbital Habitat

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsI.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 8});
                    } else {
                        buildingsI.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 4});
                    }
                    buildingsI.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 2}); // Large Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 50}); // Hydroponic farm
                    buildingsI.add(new Integer[]{Construction.ID_ACCOMODATIONUNIT, 50}); // Habitat
                    buildingsI.add(new Integer[]{Construction.ID_COMPLEXIRONMINE, 1}); // Agrarkomplex
                    break;
            }

            // Erstelle alle benötigten Daten um den Planet dem User hinzuzufügen
            PlayerPlanet pp = new PlayerPlanet();
            pp.setName("Planet #" + planetId);
            pp.setUserId(pfe.getUserId());
            pp.setPlanetId(planetId);
            pp.setPopulation((long) sPop);
            pp.setMoral(100);
            pp.setTax(20);
            pp.setPriorityIndustry(1);
            pp.setPriorityAgriculture(0);
            pp.setPriorityResearch(2);
            pp.setGrowth(5f);
            pp.setMigration(0);
            pp.setUnrest(0d);
            pp.setInvisibleFlag(false);
            pp.setHomeSystem(false);

            ppDAO.add(pp);

            // Planet Loyality
            PlanetLoyality pLoyal = new PlanetLoyality();
            pLoyal.setPlanetId(planetId);
            pLoyal.setUserId(pfe.getUserId());
            pLoyal.setValue(100d);
            pLoyalDAO.add(pLoyal);
            
            // Planet Ressources
            ArrayList<PlanetRessource> prs = prDAO.findByPlanetId(planetId);
            for (PlanetRessource pr : prs) {
                if (pr.getType() != EPlanetRessourceType.PLANET) {
                    prDAO.remove(pr);
                }
            }
            PlanetRessource iron = new PlanetRessource();
            iron.setPlanetId(planetId);
            iron.setType(EPlanetRessourceType.INSTORAGE);
            iron.setRessId(Ressource.IRON);
            iron.setQty((long) 8000 * ressMod);

            prDAO.add(iron);

            PlanetRessource steel = new PlanetRessource();
            steel.setPlanetId(planetId);
            steel.setType(EPlanetRessourceType.INSTORAGE);
            steel.setRessId(Ressource.STEEL);
            steel.setQty((long) 8000 * ressMod);

            prDAO.add(steel);

            PlanetRessource food = new PlanetRessource();

            food.setPlanetId(planetId);
            food.setType(EPlanetRessourceType.INSTORAGE);
            food.setRessId(Ressource.FOOD);
            food.setQty(500l);

            prDAO.add(food);

            // Erstelle Koloniebasis

            if (needsOrbital) {
                for (Integer[] entry : buildingsO) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "ORBITAL ADD BUILDING " + entry[0] + " WITH COUNT " + entry[1]);
                    PlanetConstruction pc = new PlanetConstruction();
                    pc.setPlanetId(planetId);
                    pc.setConstructionId(entry[0]);
                    pc.setNumber(entry[1]);
                    pcDAO.add(pc);
                }
            } else if (inhabitable) {
                for (Integer[] entry : buildingsI) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "INHABITABLE ADD BUILDING " + entry[0] + " WITH COUNT " + entry[1]);
                    PlanetConstruction pc = new PlanetConstruction();
                    pc.setPlanetId(planetId);
                    pc.setConstructionId(entry[0]);
                    pc.setNumber(entry[1]);
                    pcDAO.add(pc);
                }
            } else {
                for (Integer[] entry : buildingsG) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "GAIA ADD BUILDING " + entry[0] + " WITH COUNT " + entry[1]);
                    PlanetConstruction pc = new PlanetConstruction();
                    pc.setPlanetId(planetId);
                    pc.setConstructionId(entry[0]);
                    pc.setNumber(entry[1]);
                    pcDAO.add(pc);
                }
            }

            // Nach erfolgreicher Erstellung aller Daten, Kolonieschiff löschen
            ShipFleet sf = sfDAO.getByFleetDesign(pfe.getId(), selectedDesignId);
            if (sf != null) {
                if (sf.getCount() > 1) {
                    sf.setCount((sf.getCount() - 1));
                    sfDAO.update(sf);
                } else {
                    sfDAO.remove(sf);

                    if (sfDAO.findByFleetId(pfe.getId()).size() == 0) {
                        PlayerFleet pf = pfDAO.findById(pfe.getId());
                        pfDAO.remove(pf);
                    }
                }
            }

            //Remove Trial Status wenn m�glich
            User u = uDAO.findById(pfe.getUserId());
            if (u.getTrial()) {
                ArrayList<PlayerPlanet> ppList = ppDAO.findByUserId(pfe.getUserId());

                if (ppList.size() > MAX_PLANET_COUNT_TRIAL) {
                    Logger.getLogger().write(LogLevel.INFO,"Remove user " + u.getGameName() + " [" + pfe.getUserId() + "] from Trial due to maxPlanetCount");
                    LoginService.unlockTrialUser(u);
                }

                HashSet<Integer> uniqueSystems = new HashSet<Integer>();
                for (PlayerPlanet ppTmp : ppList) {
                    Planet pTmp = pDAO.findById(ppTmp.getPlanetId());
                    uniqueSystems.add(pTmp.getSystemId());
                }

                boolean secureSystem = true;
                if (uniqueSystems.size() > MAX_SYSTEM_COUNT_TRIAL) {
                    Logger.getLogger().write(LogLevel.INFO,"Remove user " + u.getGameName() + " [" + pfe.getUserId() + "] from Trial due to maxSystemCount");
                    LoginService.unlockTrialUser(u);
                    secureSystem = false;
                }

                for (Planet pTmp : pDAO.findBySystemId(pDAO.findById(pfe.getRelativeCoordinate().getPlanetId()).getSystemId())) {
                    PlayerPlanet ppTmp = ppDAO.findByPlanetId(pTmp.getId());
                    if (ppTmp != null) {
                        if (ppTmp.getUserId() != pfe.getUserId()) {
                            Logger.getLogger().write(LogLevel.INFO,"Remove user " + u.getGameName() + " [" + pfe.getUserId() + "] from Trial due to colonizing in enemy system");
                            LoginService.unlockTrialUser(u);
                            secureSystem = false;
                            break;
                        }
                    }
                }

                if (secureSystem) {
                    at.viswars.model.System sys = sDAO.findById(systemId);
                    sys.setVisibility(u.getUserId());
                    sDAO.update(sys);
                }
            }

            PlanetLog pl = new PlanetLog();
            pl.setPlanetId(planetId);
            pl.setUserId(u.getUserId());
            pl.setTime(GameUtilities.getCurrentTick2());
            plDAO.add(pl);

            //Viewtable entry hinzuf�gen
            ViewTableUtilities.addOrRefreshSystem(pfe.getUserId(), p.getSystemId(), GameUtilities.getCurrentTick2());

            //Updaten der Viewtable f�r SPieler die ein Observatorium haben und diesen Planeten sehen
            StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
            at.viswars.model.System s = sDAO.findById(pDAO.findById(p.getId()).getSystemId());
            SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
            quadtree.addItemToTree(sd);
            ArrayList<Integer> usersRefreshed = new ArrayList<Integer>();
            for (PlayerPlanet ppTmp : (ArrayList<PlayerPlanet>) ppDAO.findAll()) {
                if(usersRefreshed.contains(ppTmp.getUserId())){
                    continue;
                }
                at.viswars.model.System s1 = sDAO.findById(pDAO.findById(ppTmp.getPlanetId()).getSystemId());
                usersRefreshed.add(ppTmp.getUserId());
                if (pcDAO.isConstructed(ppTmp.getPlanetId(), Construction.ID_OBSERVATORY)) {
                    ArrayList<SystemDesc> tmpSys = quadtree.getItemsAround(s1.getX(), s1.getY(), (int) 100);
                    //Found the colonized system by this observatory
                    if(!tmpSys.isEmpty()){
                        DebugBuffer.addLine(DebugLevel.DEBUG, "Refreshing Viewtabhle after Colonizing for User : " + ppTmp.getUserId() + " systemId : " + systemId);
                        ViewTableUtilities.addOrRefreshSystem(ppTmp.getUserId(), systemId, GameUtilities.getCurrentTick2());
                    }

                }
            }


            HeaderBuffer.reloadUser(pfe.getUserId());

            endResult = new BaseResult("Planet " + planetId + " wurde erfolgreich besiedelt!", false);
        } catch (Exception e) {
            th.rollback();

            DebugBuffer.writeStackTrace("Exception in Colonizing - colonize: ", e);
            endResult = new BaseResult(e.getMessage(), true);
        } finally {
            th.endTransaction();
        }

        return endResult;
    }
}
