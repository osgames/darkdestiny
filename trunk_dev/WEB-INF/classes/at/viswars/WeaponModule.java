/*
 * WeaponModule.java
 *
 * Created on 01. Februar 2006, 19:30
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package at.viswars;

/**
 *
 * @author Stefan
 */
public class WeaponModule {
    private int weaponId;
    private int attackPower;
    private int energyConsumption;
    private int count;
    private int totalCount;
    private float accuracy;
    private int speed;
    private int fired;
    private int multifire;
    private int multifired;
    private int range;
    
    public WeaponModule() {
    }

    public int getWeaponId() {
        return weaponId;
    }

    public void setWeaponId(int weaponId) {
        this.weaponId = weaponId;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }

    public int getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }    

    public int getFired() {
        return fired;
    }

    public void setFired(int fired) {
        this.fired = fired;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getMultifire() {
        return multifire;
    }

    public void setMultifire(int multifire) {
        this.multifire = multifire;
    }

    public int getMultifired() {
        return multifired;
    }

    public void setMultifired(int multifired) {
        this.multifired = multifired;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }
}
