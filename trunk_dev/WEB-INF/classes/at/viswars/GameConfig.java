package at.viswars;

import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.GameDataDAO;
import at.viswars.enumeration.EGameDataStatus;
import java.io.IOException;
import java.util.*;

import at.viswars.model.GameData;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;

public class GameConfig {

    private static GameConfig instance = null;
    private static GameDataDAO gdDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
    /**
     * Parameter der Datenbank-Anbindung:
     */
    private String driver;
    private String database;
    private String username;
    private String password;
    private String gamehostURL;
    private String webserviceURL;
    private String startURL;
    private String srcPath = "";
    private LogLevel logMode = LogLevel.WARNING;
    /**
     * Basis URL der Bilder:
     */
    private static String picPath = "";
    private int tickTime = -1;
    private long startTime = -1;

    public static GameConfig getInstance() {
        if (instance == null) {
            instance = new GameConfig();
        }
        return instance;
    }

    private GameConfig() {
        loadValues();
    }

    private void loadValues() {

        String additionalParameters = "?autoReconnect=true&dontTrackOpenResources=true";

        Properties gameProperties = new Properties();
        Properties dbProperties = new Properties();
        try {
            gameProperties.load(GameConfig.class.getResourceAsStream("/game.properties"));
            dbProperties.load(GameConfig.class.getResourceAsStream("/db.properties"));
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(GameConfig.class.getName()).log(Level.SEVERE, null, ex);
        }

        driver = loadValue(dbProperties, SetupProperties.DATABASE_DRIVERCLASS);
        database = loadValue(dbProperties, SetupProperties.DATABASE_URL) + additionalParameters;
        username = loadValue(dbProperties, SetupProperties.DATABASE_USER);
        password = loadValue(dbProperties, SetupProperties.DATABASE_PASS);

        gamehostURL = loadValue(gameProperties, SetupProperties.GAME_GAMEHOSTURL);
        webserviceURL = loadValue(gameProperties, SetupProperties.GAME_WEBHOSTURL);
        startURL = loadValue(gameProperties, SetupProperties.GAME_STARTURL);
        srcPath = loadValue(gameProperties, SetupProperties.GAME_SRCPATH);

        String logLevel = loadValue(gameProperties, SetupProperties.GAME_LOGGING);

        if (logLevel == null) {
            Logger.getLogger().write(LogLevel.WARNING, "Loglevel in game.properties not found");
            logLevel = LogLevel.WARNING.toString();
        }

        if (logLevel.equalsIgnoreCase(LogLevel.DEBUG.toString())) {
            Logger.getLogger().write(LogLevel.INFO, "Set LogLevel to " + LogLevel.DEBUG);
            Logger.setLogLevel(LogLevel.DEBUG);
        } else if (logLevel.equalsIgnoreCase(LogLevel.TRACE.toString())) {
            Logger.getLogger().write(LogLevel.INFO, "Set LogLevel to " + LogLevel.TRACE);
            Logger.setLogLevel(LogLevel.TRACE);
        } else if (logLevel.equalsIgnoreCase(LogLevel.INFO.toString())) {
            Logger.getLogger().write(LogLevel.INFO, "Set LogLevel to " + LogLevel.INFO);
            Logger.setLogLevel(LogLevel.INFO);
        } else if (logLevel.equalsIgnoreCase(LogLevel.ERROR.toString())) {
            Logger.getLogger().write(LogLevel.INFO, "Set LogLevel to " + LogLevel.ERROR);
            Logger.setLogLevel(LogLevel.ERROR);
        } else if (logLevel.equalsIgnoreCase(LogLevel.WARNING.toString())) {
            Logger.getLogger().write(LogLevel.INFO, "Set LogLevel to " + LogLevel.WARNING);
            Logger.setLogLevel(LogLevel.WARNING);
        } else {
            Logger.getLogger().write(LogLevel.INFO, "LogLevel undefined: Set LogLevel to " + LogLevel.WARNING);
            Logger.setLogLevel(LogLevel.WARNING);
        }
    }

    public void reloadValues() {
        loadValues();
    }

    public String loadValue(Properties propertiesFile, String key) {
        try {
            return propertiesFile.getProperty(key.toString());
        } catch (Exception e) {
            DebugBuffer.error("Error loading setup properties key : " + key + " from file " + propertiesFile);
        }
        return null;
    }

    public String getDriverClassName() {
        return driver;
    }

    public java.lang.String getDatabase() {
        return database;
    }

    public java.lang.String getUsername() {
        return username;
    }

    public java.lang.String getPassword() {
        return password;
    }

    public static java.lang.String getHostURL() {
        return GameConfig.getInstance().gamehostURL;
    }

    private void initDBValues() {
        GameData gd = (GameData) gdDAO.findAll().get(0);

        if (gd != null) {
            tickTime = gd.getTickTime();
            startTime = gd.getStartTime();
        } else {
            DebugBuffer.error("Base configuration values not found in database!");
            throw new RuntimeException("Configuration not found in database!");
        }
    }

    /**
     * @return the duration of a tick in milliseconds!
     */
    public static int getTicktime() {
        if (getInstance().tickTime < 0) {
            getInstance().initDBValues();
        }
        return getInstance().tickTime;
    }

    public static long getStarttime() {
        if (getInstance().tickTime < 0) {
            getInstance().initDBValues();
        }
        return getInstance().startTime;
    }

    public static String picPath() {
        return picPath;
    }

    public static void rereadValues() {
        getInstance().tickTime = -1;
        getInstance().initDBValues();
    }

    /**
     * @return the startURL
     */
    public String getStartURL() {
        return startURL;
    }

    /**
     * @return the srcPath
     */
    public String getSrcPath() {
        return srcPath;
    }

    /**
     * @return the webserviceURL
     */
    public String getWebserviceURL() {
        return webserviceURL;
    }
}
