/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars;

import at.viswars.Logger.Logger;
import at.viswars.dao.ConstructionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetRessourceDAO;
import at.viswars.database.framework.exception.TransactionException;
import at.viswars.database.framework.transaction.Transaction;
import at.viswars.database.framework.transaction.TransactionHandler;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.model.Construction;
import at.viswars.model.PlanetRessource;

/**
 *
 * @author Stefan
 */
public class TransactionTest {
    private static ConstructionDAO cDAO = (ConstructionDAO)DAOFactory.get(ConstructionDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO)DAOFactory.get(PlanetRessourceDAO.class);
    
    public static String runTest() {
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        
        Construction c = new Construction();
        c.setName("Testgebäude");
        
        th.addTransaction(new Transaction(cDAO,c,Transaction.TRANSACTION_TYPE_INSERT));        
        
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(16169);
        pr.setQty(1000l);
        pr.setRessId(11);
        pr.setType(EPlanetRessourceType.PLANET);
        
        th.addTransaction(new Transaction(prDAO,pr,Transaction.TRANSACTION_TYPE_UPDATE));
                
        try {
            th.execute();
        } catch (TransactionException te) {
            Logger.getLogger().write("ERROR: " + te.getMessage());
            return "Transaction failed!<BR><BR>" + th.getTransactionLog();
        }
        
        return "Transaction successful!<BR><BR>" + th.getTransactionLog();
    }
}
