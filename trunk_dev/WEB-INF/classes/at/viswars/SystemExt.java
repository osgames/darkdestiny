/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetDAO;
import at.viswars.model.Planet;
import at.viswars.model.ViewTable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class SystemExt {
    private static PlanetDAO pDAO = (PlanetDAO)DAOFactory.get(PlanetDAO.class);
    
    private final at.viswars.model.System sys;
    private final at.viswars.model.System refSys;
    private ArrayList<PlanetExt> pList;    
    
    private HashMap<Integer,ViewTable> sysVTEntries = null;
    private Double distance = null;
    
    public SystemExt(at.viswars.model.System sys) {
        this.sys = sys;
        this.refSys = null;
        pList = null;
    }

    public SystemExt(at.viswars.model.System sys, at.viswars.model.System refSys) {
        this.sys = sys;
        this.refSys = refSys;
        pList = null;
    }    
            
    public void setViewTable(HashMap<Integer,ViewTable> vtMap) {
        this.sysVTEntries = vtMap;
    }
    
    public ArrayList<PlanetExt> getPlanets() {
        if (sysVTEntries == null) return new ArrayList<PlanetExt>();
        
        if (pList == null) {
            ArrayList<Planet> planets = pDAO.findBySystemId(sys.getId());
            pList = new ArrayList<PlanetExt>();
            
            for (Planet p : planets) {
                PlanetExt pe = new PlanetExt(p);
                pList.add(pe);
                if (sysVTEntries != null) {
                    pe.setViewTable(sysVTEntries.get(p.getId()));
                }
            }
        }
        
        return pList;
    }
    
    public ArrayList<PlanetExt> getPlanetsOrderedByOrbit() {
        if (pList == null) {
            pList = getPlanets();
        }
        
        TreeMap<Integer,PlanetExt> sorted = new TreeMap<Integer,PlanetExt>();
        for (PlanetExt pe : pList) {
            sorted.put(pe.getBase().getOrbitLevel(), pe);
        }
        
        ArrayList<PlanetExt> sortedList = new ArrayList<PlanetExt>();
        sortedList.addAll(sorted.values());
        
        return sortedList;
    }    
    
    public double getDistanceToRef() {
        if (distance == null) {
            distance = Math.sqrt(Math.pow(sys.getX() - refSys.getX(), 2d) + 
                    Math.pow(sys.getY() - refSys.getY(), 2d));
        }
        
        return distance;
    }

    public boolean isSystemInViewtable() {
        return (sysVTEntries != null);
    }
    
    public at.viswars.model.System getBase() {
        return sys;
    }
}
