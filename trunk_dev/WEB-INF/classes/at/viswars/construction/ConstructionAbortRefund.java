/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.construction;

import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.buildable.ConstructionExt;
import at.viswars.dao.ActionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ProductionOrderDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.enumeration.EActionType;
import at.viswars.model.Action;
import at.viswars.model.ProductionOrder;
import at.viswars.model.Ressource;
import at.viswars.ships.BuildTime;

/**
 *
 * @author Stefan
 */
public class ConstructionAbortRefund extends MutableRessourcesEntry {
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);

    private final int count;
    private final int orderId;
    private final ConstructionExt ce;

    public ConstructionAbortRefund(ConstructionExt ce, int orderId) {
        this.count = 1;
        this.ce = ce;
        this.orderId = orderId;

        calcOrderAbortRefund();
    }

    private void calcOrderAbortRefund() {
        RessourcesEntry re = ce.getRessCost();

        Action a = aDAO.findByTimeFinished(orderId, EActionType.BUILDING);
        ProductionOrder po = poDAO.findById(orderId);

        // int countInOrder = a.getNumber();
        int countInOrder = 1;
        int totalNeed = po.getIndNeed();
        int done = po.getIndProc();

        //If no tick total ressources back
        if (done == 0) {
            for (RessAmountEntry rae : re.getRessArray()) {
                this.setRess(rae.getRessId(), (long) rae.getQty());
            }
            //Do nothing
        } else {
            double progress = 100d / (double)totalNeed * (double)done;

            if (progress <= 30) { // Full ressource refund - credit refund goes down to 90%
                for (RessAmountEntry rae : re.getRessArray()) {
                    if (rae.getRessId() == Ressource.CREDITS) {
                        int actRefund = (int)rae.getQty() - (int)Math.floor((1d / 30d * progress) * rae.getQty() * (double)countInOrder * 0.1d);
                        this.setRess(rae.getRessId(), actRefund);
                    } else {
                        this.setRess(rae.getRessId(), (long)(re.getRess(rae.getRessId()) * (double)countInOrder));
                    }
                }
            } else { // Ressource refund down to 70% - credit refund goes down to 0%
                for (RessAmountEntry rae : re.getRessArray()) {
                    if (rae.getRessId() == Ressource.CREDITS) {
                        int actRefund = (int)Math.floor((1d - (1d / 70d * (progress - 30d))) * rae.getQty() * 0.9d * (double)countInOrder);
                        this.setRess(rae.getRessId(), actRefund);
                    } else {
                        int actRefund = (int)Math.floor((1d - (1d / 70d * (progress - 30d))) * rae.getQty() * (double)countInOrder);
                        this.setRess(rae.getRessId(), actRefund);
                    }
                }
            }
        }
    }

    public int getCount() {
        return count;
    }
}
