package at.viswars.construction;

import at.viswars.DebugBuffer;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.buildable.ConstructionExt;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.enumeration.EConstructionType;
import at.viswars.enumeration.EDefenseType;
import at.viswars.model.Construction;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.PlanetDefense;
import at.viswars.model.Ressource;

/**
 * This class describes the cost for destroying a building
 *
 * @author martin
 */
public class BuildingDeconstructionRessourceCost extends ConstructionExt {
    public static PlanetConstructionDAO planetConstructionDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    public static PlanetDefenseDAO planetDefenseDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);

    private final int count;
    private final int buildTime;
    private MutableRessourcesEntry mre = new MutableRessourcesEntry();

    public BuildingDeconstructionRessourceCost(int constructionId, int planetId) {
        // do kicktsn dann weil er Wenn er COnstructionExt initialisieren will die Cons Id 0 is
        super(constructionId);
        PlanetConstruction pc = planetConstructionDAO.findBy(planetId, constructionId);
        PlanetDefense pd = planetDefenseDAO.findBy(planetId, constructionId, EDefenseType.TURRET);
        
        if (!((Construction)getBase()).getType().equals(EConstructionType.PLANETARY_DEFENSE) && pc == null) {
            DebugBuffer.error("[DECONSTRUCTION] Found no construction for planet " + planetId + " and building id "+ constructionId);
            throw new RuntimeException("Building (P:"+planetId+"/C:"+constructionId+") not found!");
        }
        if (((Construction)getBase()).getType().equals(EConstructionType.PLANETARY_DEFENSE) && pd == null) {
            DebugBuffer.error("[DECONSTRUCTION] Found no planetdefense for planet " + planetId + " and building id "+ constructionId);
            throw new RuntimeException("Building (P:"+planetId+"/C:"+constructionId+") not found!");
        }
        if (((Construction)getBase()).getType().equals(EConstructionType.PLANETARY_DEFENSE)){
            this.count = pd.getCount();
        }else{
            this.count = pc.getNumber();
        }
        this.buildTime = (int)Math.round(((Construction)this.getBase()).getBuildtime() * SetupValues.BUILDTIME_MULTI);

        for (RessAmountEntry re : super.getRessCost().getRessArray()) {
            if (re.getRessId() != Ressource.CREDITS) {
                mre.setRess(re.getRessId(), (long) ((double)re.getQty() * SetupValues.RESSOURCE_MULTI));
            } else {
                mre.setRess(re.getRessId(), (long) ((double)re.getQty() * SetupValues.CREDIT_MULTI));
            }
        }
    }
      public BuildingDeconstructionRessourceCost(int constructionId) {
        super(constructionId);
        count = 0;
        buildTime = 0;

        for (RessAmountEntry re : super.getRessCost().getRessArray()) {
            if (re.getRessId() != Ressource.CREDITS) {
                mre.setRess(re.getRessId(), (long) ((double)re.getQty() * SetupValues.RESSOURCE_MULTI));
            } else {
                mre.setRess(re.getRessId(), (long) ((double)re.getQty() * SetupValues.CREDIT_MULTI));
            }
        }
    }

    @Override
    public MutableRessourcesEntry getRessCost(){
        return mre;
    }

    public int getCount() {
        return count;
    }
}
