/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.construction.restriction;

import at.viswars.construction.ConstructionInterface;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.enumeration.ERestrictionReason;
import at.viswars.model.Construction;
import at.viswars.model.PlayerPlanet;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class RPlanetaryHUEShield implements ConstructionInterface  {

    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();
    public boolean onConstructionCheck(PlayerPlanet pp) throws Exception {
        
        PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);   
        if(pcDAO.isConstructed(pp.getPlanetId(), Construction.ID_PLANETARY_PARATRONSHIELD)){
            errMsg.put("Planetarer Paratronschirm bereits vorhanden", ERestrictionReason.OUTDATED);
            return false;
        }
        return true;
    }

    public void onConstruction(PlayerPlanet pp) throws Exception {
    }

    public void onFinishing(PlayerPlanet pp) throws Exception {
    }

    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    public void onDeconstruction(PlayerPlanet pp) throws Exception {
    }

    public void onDestruction(PlayerPlanet pp) throws Exception {
    }

    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }
    
    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);   
        if(pcDAO.isConstructed(pp.getPlanetId(), Construction.ID_PLANETARY_PARATRONSHIELD)){
            errMsg.put("Planetarer Paratronschirm bereits vorhanden", ERestrictionReason.DEACTIVATED);
            return true;
        }
        return false;
    }
}
