/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.construction.restriction;

import at.viswars.FormatUtilities;
import at.viswars.GameConstants;
import at.viswars.ML;
import at.viswars.construction.ConstructionInterface;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.enumeration.ERestrictionReason;
import at.viswars.model.PlayerPlanet;
import at.viswars.utilities.LanguageUtilities;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class PlanetarySupercomputer implements ConstructionInterface {
    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();

    public boolean onConstructionCheck(PlayerPlanet pp) {
        PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);

        if (pp.getPopulation() < 1500000000) {
            errMsg.put(ML.getMLStr("construction_err_notenoughpopulation", pp.getUserId()).replace("%POP", FormatUtilities.getFormatScaledNumber(1500000000, 1, GameConstants.VALUE_TYPE_NORMAL, LanguageUtilities.getMasterLocaleForUser(pp.getUserId()))), ERestrictionReason.NOTENOUGHPOPULATION);
            return false;
        }

        return true;
    }

    public void onConstruction(PlayerPlanet pp) {

    }

    public void onFinishing(PlayerPlanet pp) {

    }

    public void onDeconstruction(PlayerPlanet pp) {

    }

    public void onDestruction(PlayerPlanet pp) {

    }
    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }


    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }
}
