/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.construction.restriction;

import at.viswars.GameConstants;
import at.viswars.GameUtilities;
import at.viswars.construction.ConstructionInterface;
import at.viswars.enumeration.ERestrictionReason;
import at.viswars.model.PlayerPlanet;
import at.viswars.scanner.StarMapQuadTree;
import at.viswars.scanner.SystemDesc;
import at.viswars.service.Service;
import at.viswars.utilities.ViewTableUtilities;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class Observatory extends Service implements ConstructionInterface {

    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();
    public boolean onConstructionCheck(PlayerPlanet pp) {
        return true;
    }

    public void onConstruction(PlayerPlanet pp) throws Exception {
    }

    public void onFinishing(PlayerPlanet pp) {
        refreshViewTable(pp);
    }

    public void onDeconstruction(PlayerPlanet pp) throws Exception {
        onDestruction(pp);
    }

    public void onDestruction(PlayerPlanet pp) throws Exception {
        refreshViewTable(pp);
    }

    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }


    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }
    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }

    private void refreshViewTable(PlayerPlanet pp) {
        StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
        ArrayList<at.viswars.model.System> systems = systemDAO.findAll();

        for (at.viswars.model.System s : systems) {
            SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
            quadtree.addItemToTree(sd);
        }
        at.viswars.model.System s = systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId());
        ArrayList<SystemDesc> tmpSys = quadtree.getItemsAround(s.getX(), s.getY(), (int) 100);
        //Creating Viewtable Entries
        int currTick = GameUtilities.getCurrentTick2();
        for (SystemDesc sd : tmpSys) {
           ViewTableUtilities.addOrRefreshSystem(pp.getUserId(), sd.id, currTick);
        }        
    }
}
