/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.construction.restriction;

import at.viswars.ML;
import at.viswars.construction.ConstructionInterface;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.enumeration.ERestrictionReason;
import at.viswars.model.PlayerPlanet;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class ColonyBase implements ConstructionInterface {
    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();
    private final static int LOCAL_ADMINISTRATION = 3;
    private final static int LOCAL_GOVERNMENT = 4;
    private final static int PLANETARY_GOVERNMENT = 5;
    
    public boolean onConstructionCheck(PlayerPlanet pp) {
        PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);
        
        if ((pcDAO.findBy(pp.getPlanetId(), LOCAL_ADMINISTRATION) != null) ||
            (pcDAO.findBy(pp.getPlanetId(), LOCAL_GOVERNMENT) != null) ||
            (pcDAO.findBy(pp.getPlanetId(), PLANETARY_GOVERNMENT) != null)) {
            errMsg.put(ML.getMLStr("construction_err_buildingoutdated", pp.getUserId()), ERestrictionReason.OUTDATED);
            return false;
        }
        
        return true;
    }

    public void onConstruction(PlayerPlanet pp) {
        
    }       
    
    public void onFinishing(PlayerPlanet pp) {
        
    }       
    
    public void onDeconstruction(PlayerPlanet pp) {
        
    }       
    
    public void onDestruction(PlayerPlanet pp) {
        
    }    
    
    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }

    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }
}
