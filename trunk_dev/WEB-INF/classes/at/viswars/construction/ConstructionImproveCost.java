/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.construction;

import at.viswars.buildable.ConstructionExt;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.model.Construction;
import at.viswars.model.PlanetConstruction;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.service.ConstructionService;
import at.viswars.ships.BuildTime;

/**
 *
 * @author Stefan
 */
public class ConstructionImproveCost extends MutableRessourcesEntry {
    private PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);

    private final int count = 1;
    private final int planetId;
    private final BuildTime buildTime;
    private final ConstructionExt ce;
    private final PlanetConstruction pc;

    public ConstructionImproveCost(ConstructionExt ce, int planetId) {
        this.ce = ce;
        this.planetId = planetId;
        this.buildTime = new BuildTime();
        this.pc = pcDAO.findBy(planetId, ((Construction)ce.getBase()).getId());

        if (pc.getLevel() == -1 && !((Construction)ce.getBase()).getLevelable()) throw new RuntimeException("ConstructionImproveCost not available for non-improvable buildings.");

        calcImproveCost();
    }

    private void calcImproveCost() {       
        // buliding to improve
        RessourcesEntry re = ce.getRessCost();
        this.addRess(re, count);
        buildTime.setIndPoints((int)Math.ceil(ConstructionService.getIndustryPoints(planetId) * count));

        int nextLevel = pc.getLevel() + 1;

        this.multiplyRessources((Math.pow(1.75d,(double)nextLevel))/(double)nextLevel);
        buildTime.setIndPoints(buildTime.getIndPoints() + (int)Math.floor(buildTime.getIndPoints() * nextLevel * 0.25d));

        roundValues();
    }
}
