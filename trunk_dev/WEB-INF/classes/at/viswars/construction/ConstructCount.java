package at.viswars.construction;

import at.viswars.enumeration.EActionType;
import at.viswars.enumeration.EProductionOrderType;
import at.viswars.model.Construction;

public class ConstructCount {
    private int count;
    private int priority;
    private double completion;
    private final Construction construct;

    private final EProductionOrderType prodOrderType;
    private final EActionType actionType;

    public ConstructCount(Construction c, EActionType actionType, EProductionOrderType prodOrderType, int count) {
        construct = c;
        this.count = count;
        this.actionType = actionType;
        this.prodOrderType = prodOrderType;
    }

    public ConstructCount(Construction construct, EActionType actionType, EProductionOrderType prodOrderType, int count, int priority, double completion) {
        this.count = count;
        this.priority = priority;
        this.completion = completion;
        this.construct = construct;
        this.actionType = actionType;
        this.prodOrderType = prodOrderType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Construction getConstruct() {
        return construct;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @return the completion
     */
    public double getCompletion() {
        return completion;
    }

    /**
     * @return the prodType
     */
    public EProductionOrderType getProdOrderType() {
        return prodOrderType;
    }

    /**
     * @return the actionType
     */
    public EActionType getActionType() {
        return actionType;
    }
}
