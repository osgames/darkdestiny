/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.construction;

import at.viswars.buildable.ConstructionExt;
import at.viswars.dao.ConstructionDAO;
import at.viswars.dao.ConstructionUpgradeDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.model.Construction;
import at.viswars.model.ConstructionUpgrade;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.service.ConstructionService;
import at.viswars.ships.BuildTime;

/**
 *
 * @author Stefan
 * How to determine upgrade costs
 * scrap cost of previous buildings + construction cost of new building
 * credits / 3
 * buildingTime / 2
 */
public class ConstructionUpgradeCost extends MutableRessourcesEntry {
    private ConstructionUpgradeDAO cuDAO = (ConstructionUpgradeDAO)DAOFactory.get(ConstructionUpgradeDAO.class);
    private ConstructionDAO cDAO = (ConstructionDAO)DAOFactory.get(ConstructionDAO.class);

    private final int count;
    private final int planetId;
    private final BuildTime buildTime;
    private final ConstructionExt ceOld;
    private final ConstructionExt ceNew;

    public ConstructionUpgradeCost(ConstructionExt ceOld, ConstructionExt ceNew, int planetId, int count) {
        this.count = count;
        this.ceOld = ceOld;
        this.ceNew = ceNew;
        this.planetId = planetId;
        this.buildTime = new BuildTime();

        calcUpgradeCost();
    }

    private void calcUpgradeCost() {
        ConstructionUpgrade cu = cuDAO.findBySourceAndTarget(((Construction)ceOld.getBase()).getId(), ((Construction)ceNew.getBase()).getId());

        // New building
        RessourcesEntry re = ceNew.getRessCost();
        this.addRess(re, cu.getTargetConsCreation());
        getBuildTime().setIndPoints((int)Math.ceil(ConstructionService.getIndustryPoints(planetId) * count * cu.getTargetConsCreation()));

        // Old building
        ConstructionScrapCost css = new ConstructionScrapCost(ceOld,planetId);
        css.multiplyRessources(cu.getBaseConsConsumption(), cu.getBaseConsConsumption());
        this.addRess(css);
        getBuildTime().setIndPoints(getBuildTime().getIndPoints() + css.getBuildTime().getIndPoints() * cu.getBaseConsConsumption());

        // Adjust cost
        getBuildTime().setIndPoints((int)Math.floor(getBuildTime().getIndPoints() / 2d));
        this.multiplyRessources(1d, 0.3d);
    }

    /**
     * @return the buildTime
     */
    public BuildTime getBuildTime() {
        return buildTime;
    }
}
