/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.construction;

import at.viswars.enumeration.ERestrictionReason;
import at.viswars.model.PlayerPlanet;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public interface ConstructionInterface {        
    public boolean onConstructionCheck(PlayerPlanet pp) throws Exception;
    public void onConstruction(PlayerPlanet pp) throws Exception;
    public void onFinishing(PlayerPlanet pp) throws Exception;
    public void onImproving(PlayerPlanet pp) throws Exception;
    public void onImproved(PlayerPlanet pp) throws Exception;
    public void onDeconstruction(PlayerPlanet pp) throws Exception;
    public void onDestruction(PlayerPlanet pp) throws Exception;
    
    public boolean isDeactivated(PlayerPlanet pp) throws Exception;
    
    public HashMap<String,ERestrictionReason> getReasons();
}
