package at.viswars.construction;

import at.viswars.update.ProdOrderBuffer;
import at.viswars.update.ProdOrderBufferEntry;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import at.viswars.databuffer.*;

import at.viswars.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.buildable.ConstructionExt;
import at.viswars.service.ResearchService.res;

/**
 * @author Rayden
 * @author martin
 */
public class DeconstructionFunctions implements SetupValues, ConfirmParameters {

    private int buildingID;
    private int orderId;
    private int planetId;
    private int systemID;
    private int mode;

    /**
     *
     */
    public DeconstructionFunctions(int mode) throws Exception {
        if ((mode != GameConstants.DESTROY_BUILDING) &&
                (mode != GameConstants.DESTROY_BUILDING_ORDER) &&
                (mode != GameConstants.DESTROY_SHIP)) {
            throw new Exception("Invalid Constructor Parameter for DeconstructionFunctions");
        }

        if (mode == GameConstants.DESTROY_SHIP) {
            mode = GameConstants.DESTROY_BUILDING;
        }
        this.mode = mode;
    }

    /*
    public BuildingDeconstructionRessourceCost findForOrderDeconstruction(int orderId, int planetId, int userID) {
        this.orderId = orderId;
        this.planetId = planetId;

        ProdOrderBufferEntry poe = ProdOrderBuffer.getOrderEntry(orderId);

        BuildingDeconstructionRessourceCost result = findForOrderDeconstruction(orderId, planetId);

        return result;
    }
    */

    
    // Depending on progress credit costs and ressource cost have to be balanced
    /*
    public BuildingDeconstructionRessourceCost findForOrderDeconstruction(int orderId, int planetId) {
        this.orderId = orderId;
        this.planetId = planetId;

        ProdOrderBufferEntry poe = ProdOrderBuffer.getOrderEntry(orderId);

        ///Logger.getLogger().write("DESIGNID=" + poe.getDesignId());

       // BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(construct);

        // Calculate construction progress
        // Physical construction till 30% 
        // -> full ress restore, deconstruction refunding 90%
        // From 30% - 100% 
        // -> linear increase up to 30% ressource loss and 50% credit costs
        int totalConsPoints = poe.getDockNeed() + poe.getIndNeed() + poe.getModuleNeed() + poe.getOrbDockNeed();
        int progressPoints = poe.getDockProc() + poe.getIndProc() + poe.getModuleProc() + poe.getOrbDockProc();

        int currProgress = (int) Math.floor(100d / totalConsPoints * progressPoints);

        if (currProgress < 30) {
         //   bdrc.multiplyRessources(1d, -0.9d);
        } else {
            double creditAdj = 1d;

            if (currProgress < 70) {
                creditAdj = -0.9d + (0.9d / 40d * (40d - (70d - currProgress)));
            } else {
                creditAdj = (0.5d / 30d * (30d - (100d - currProgress)));
            }

          //  Logger.getLogger().write("Using credit multiplier: " + creditAdj);
            double ressAdj = 1.0d - (0.7d / 70d * (70d - (100d - currProgress)));

        //    bdrc.multiplyRessources(ressAdj, creditAdj);
        }

        return null;
    }
     */
    
    public BuildingDeconstructionRessourceCost findForDeconstruction(int buildingID, int planetId, int userID) {

        return new BuildingDeconstructionRessourceCost(buildingID, planetId);
    }

    public BuildingDeconstructionRessourceCost findForDeconstruction(int buildingID, int planetId) {
        this.buildingID = buildingID;
        this.planetId = planetId;

      //  BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(construct);
      //  bdrc.multiplyRessources(0.7d, 0.5d);

      //  return bdrc;
        return null;
    }

    public int getQueueIdent(int userId) {
        Map<String, Object> map = new TreeMap<String, Object>();
        map.put(PLANETID, Integer.valueOf(planetId));
        map.put(SYSTEMID, Integer.valueOf(systemID));
        map.put(USERID, Integer.valueOf(userId));

        if (mode == GameConstants.DESTROY_BUILDING) {
            map.put(BUILDINGID, Integer.valueOf(buildingID));
            return ConfirmBuffer.addNewConfirmEntry(map, userId, GameConstants.DESTROY_BUILDING);
        } else if (mode == GameConstants.DESTROY_BUILDING_ORDER) {
            map.put(ORDERID, Integer.valueOf(orderId));
            return ConfirmBuffer.addNewConfirmEntry(map, userId, GameConstants.DESTROY_BUILDING_ORDER);
        }

        return -1;
    }

    /*
    public BuildingDeconstructionRessourceCost getDeconstructionInformation(HttpServletRequest request, Map<String, Object> params) {
        try {
            //String name = (String)params.get(NAME);
            int buildingID = -1;
            int orderId = -1;

            if (mode == GameConstants.DESTROY_BUILDING) {
                buildingID = ((Integer) params.get(BUILDINGID)).intValue();
            } else if (mode == GameConstants.DESTROY_BUILDING_ORDER) {
                orderId = ((Integer) params.get(ORDERID)).intValue();
            }
            int planetID = ((Integer) params.get(PLANETID)).intValue();
            int userID = ((Integer) params.get(USERID)).intValue();
            if (request.getParameter("destroyCount") == null) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "Der Parameter destroyCount wurde nicht &uuml;bergeben. &Uuml;bergeben wurden nur: " + request.getParameterMap());
                return null;
            }
            int destroyCount = Integer.parseInt(request.getParameter("destroyCount"));
            if (destroyCount <= 0) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "destroyCount == 0 ist M&uuml;ll");
                return null;
            }

            MutableRessourcesEntry mre = null;
            ConstructionExt ce = new ConstructionExt(buildingID);

            if (mode == GameConstants.DESTROY_BUILDING) {
                mre = ce.getScrapCost(orderId)  findForDeconstruction(buildingID, planetID, userID);
                destroyCount = Math.min(res.getCount(), destroyCount);
                if (destroyCount == 0) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Du willst mehr Geb&auml;ude abreissen, als es gibt!!");
                    return null;
                }
            } else if (mode == GameConstants.DESTROY_BUILDING_ORDER) {
                mre = findForOrderDeconstruction(orderId, planetID, userID);
                destroyCount = Math.min(res.getCount(), destroyCount);
                if (destroyCount == 0) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Du willst mehr Geb&auml;ude abreissen, als es gibt!!");
                    return null;
                }
            }

            params.put(COUNT, Integer.valueOf(destroyCount));

           // res.setCount(destroyCount);

            return res;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getDeconstructionInformation: ", e);
        }

        return null;
    }
    */

    public void initiateDeconstruction(ConfirmEntry ce) {
        /*
        int buildingID = -1;
        int prodOrderId = -1;

        if (mode == GameConstants.DESTROY_BUILDING_ORDER) {
            prodOrderId = ((Integer) ce.getParMap().get(ORDERID)).intValue();
        } else {
            buildingID = ((Integer) ce.getParMap().get(BUILDINGID)).intValue();
        }

        int systemID = ((Integer) ce.getParMap().get(SYSTEMID)).intValue();
        int planetID = ((Integer) ce.getParMap().get(PLANETID)).intValue();
        int destroyCount = ((Integer) ce.getParMap().get(COUNT)).intValue();
        int userID = ((Integer) ce.getParMap().get(USERID)).intValue();

        if (mode == GameConstants.DESTROY_BUILDING) {
            BuildingDeconstructionRessourceCost c = findForDeconstruction(buildingID, planetID, userID);

            // ProductionUtilities pu = new ProductionUtilities();
            UserData ud = UserBuffer.getUserData(ce.getUserId()); // pu.getUserData(ce.getUserId());        
            if (ud.getCredits() < destroyCount * c.getCredits()) {
                return;
            }
            ud.setCredits(ud.getCredits() - destroyCount * c.getCredits());
            UserBuffer.storeUserData(ud);

            try {
                //Erst das Geb&auml;ude l&ouml;schen:
                //TODO: Dies resultiert in der sch&ouml;nen Eigenschaft, dass auf abgerissenen Geb&auml;uden gleich wieder gebaut werden kann
                // Andererseits ist es wichtig, da sonst das Geb&auml;ude weiter produzieren w&uuml;rde
                DebugBuffer.addLine(DebugLevel.DEBUG, "Deconstructing BId=" + buildingID + " on planet " + planetID + " for user " + ce.getUserId());
                if (!ProdConsBuffer.deleteBuildings(planetID, buildingID, destroyCount)) {
                    return;
                }
                Statement stmt = DbConnect.createStatement();
                /*            
                ResultSet rs = stmt.executeQuery("SELECT * FROM actions WHERE actionType="+GameConstants.ACT_TYPE_DECONSTRUCT+" AND " +
                "timeFinished="+(gu.getCurrentTick()+c.getDeconBuildTime())+" AND userId="+ce.getUserId()+" AND buildingId="+buildingID+" AND number="+destroyCount+" AND planetId="+planetID+" AND " + 
                "systemId="+systemID);
                
                if (rs.next()) {
                stmt.executeUpdate("UPDATE actions SET number=number+"+destroyCount+" " +
                "WHERE actionType="+GameConstants.ACT_TYPE_DECONSTRUCT+" AND timeFinished="+(gu.getCurrentTick()+c.getDeconBuildTime())+" AND " + 
                "userId="+ce.getUserId()+" AND buildingId="+buildingID+" AND number="+destroyCount+" AND planetId="+planetID+" AND " + 
                "systemId="+systemID);
                } else {
                // HERE SHOULD BE A * / 
                // Create Deconstruction Order
                stmt.execute("INSERT INTO productionorder SET " +
                        "planetId=" + planetId + ", " +
                        "indNeed=" + (c.getDeconBuildTime() * destroyCount) + ", " +
                        "priority=2, " +
                        "type=" + GameConstants.PROD_C_ORDER_SCRAP);

                int orderId = 0;
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    orderId = rs.getInt(1);
                }

                stmt.execute("INSERT INTO actions VALUES ('" + GameConstants.ACT_TYPE_DECONSTRUCT + "', '" +
                        orderId + "', '" +
                        ce.getUserId() + "', '" +
                        buildingID + "', '" +
                        destroyCount + "','" +
                        planetID + "','" +
                        systemID + "','0','00')");

                stmt.close();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in initiateDeconstruction(): ", e);
            }
        } else if (mode == GameConstants.DESTROY_BUILDING_ORDER) {
            BuildingDeconstructionRessourceCost c = findForOrderDeconstruction(prodOrderId, planetID, userID);
            ProdOrderBufferEntry poe = ProdOrderBuffer.getOrderEntry(prodOrderId);
            
            // ProductionUtilities pu = new ProductionUtilities();
            UserData ud = UserBuffer.getUserData(ce.getUserId()); // pu.getUserData(ce.getUserId());        
            if (ud.getCredits() < destroyCount * c.getCredits()) {
                return;
            }
            
            ud.setCredits(ud.getCredits() - destroyCount * c.getCredits());
            UserBuffer.storeUserData(ud);  

            PlanetData pd = new PlanetData(planetID);
            
            for (RessourceEntry re : RessourceBuffer.getAllStorableRessources()) {
                if (c.getRess(re.getRessourceId()) > 0) {
                    pd.setStockRessource(re.getRessourceId(), pd.getStockRessource(re.getRessourceId()) + c.getRess().get(re.getRessourceId()).getQty());
                }
            }            
            
            int orgCount = poe.getCount();
            
            poe.setCount(poe.getCount() - destroyCount);
            poe.setDockNeed(poe.getDockNeed() - (int)((poe.getDockNeed() / orgCount) * destroyCount));
            poe.setOrbDockNeed(poe.getOrbDockNeed() - (int)((poe.getOrbDockNeed() / orgCount) * destroyCount));
            poe.setIndNeed(poe.getIndNeed() - (int)((poe.getIndNeed() / orgCount) * destroyCount));
            poe.setModuleNeed(poe.getModuleNeed() - (int)((poe.getModuleNeed() / orgCount) * destroyCount));
            poe.setKasNeed(poe.getKasNeed() - (int)((poe.getKasNeed() / orgCount) * destroyCount));

            poe.setDockProc(poe.getDockProc() - (int)((poe.getDockProc() / orgCount) * destroyCount));
            poe.setOrbDockProc(poe.getOrbDockProc() - (int)((poe.getOrbDockProc() / orgCount) * destroyCount));
            poe.setIndProc(poe.getIndProc() - (int)((poe.getIndProc() / orgCount) * destroyCount));
            poe.setModuleProc(poe.getModuleProc() - (int)((poe.getModuleProc() / orgCount) * destroyCount));
            poe.setKasProc(poe.getKasProc() - (int)((poe.getKasProc() / orgCount) * destroyCount));            
            
            ProdOrderBuffer.storeOrderEntry(poe);            
        }
        */
    }

}
