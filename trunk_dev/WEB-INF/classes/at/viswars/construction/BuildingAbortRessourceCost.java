/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.construction;

import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.buildable.ConstructionExt;
import at.viswars.dao.ActionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.ProductionOrderDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.enumeration.EActionType;
import at.viswars.model.Action;
import at.viswars.model.ProductionOrder;
import at.viswars.model.Ressource;

/**
 *
 * @author HorstRabe
 */
public class BuildingAbortRessourceCost extends ConstructionExt {

    public static PlanetConstructionDAO planetConstructionDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    public static ProductionOrderDAO productionOrderDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);
    public static ActionDAO actionDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private MutableRessourcesEntry mre = new MutableRessourcesEntry();
    private int count;

    public BuildingAbortRessourceCost(int constructionId, ProductionOrder po) {
        super(constructionId);
        //Getting the deconstruction Cost
        BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(constructionId);

        Action a = actionDAO.findByTimeFinished(po.getId(), EActionType.BUILDING);
        count = a.getNumber();
        int totalNeed = po.getIndNeed();
        int done = po.getIndProc();
        //If no tick total ressources back
        if (done == 0) {
            for (RessAmountEntry re : super.getRessCost().getRessArray()) {
                mre.setRess(re.getRessId(), (long) ((double) re.getQty()));
            }
            //Do nothing
        }else{
            double factor = (double) ((double) done / (double) totalNeed);
            for (RessAmountEntry re : super.getRessCost().getRessArray()) {
                if (re.getRessId() == Ressource.CREDITS) {
                    long diff = (long) (re.getQty() - (-bdrc.getRessCost().getRess(re.getRessId())));
                    mre.setRess(re.getRessId(), (long) ((double) re.getQty() - diff * factor));
                      } else {
                    long diff = (long) (re.getQty() - bdrc.getRessCost().getRess(re.getRessId()));
                       mre.setRess(re.getRessId(), (long) ((double) re.getQty() - diff * factor));
                }
            }

        }

    }

    @Override
    public MutableRessourcesEntry getRessCost() {
        return mre;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }
}
