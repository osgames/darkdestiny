/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.SpyAction;

/**
 *
 * @author Stefan
 */
public class SpyActionDAO extends ReadOnlyTable<SpyAction> implements GenericDAO {
    public SpyAction findById(int id) {
        SpyAction spyAction = new SpyAction();
        spyAction.setId(id);
        return (SpyAction)get(spyAction);
    }
}
