/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.AIMessageParameter;

/**
 *
 * @author Aion
 */
public class AIMessageParameterDAO extends ReadWriteTable<AIMessageParameter> implements GenericDAO {
    public AIMessageParameter findById(int id){
        AIMessageParameter aimp = new AIMessageParameter();
        aimp.setId(id);
        return (AIMessageParameter)get(aimp);
    }
}
