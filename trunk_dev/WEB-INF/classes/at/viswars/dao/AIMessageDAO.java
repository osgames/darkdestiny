/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.AIMessage;

/**
 *
 * @author Aion
 */
public class AIMessageDAO extends ReadWriteTable<AIMessage> implements GenericDAO {
    public AIMessage findById(int id){
        AIMessage aim = new AIMessage();
        aim.setId(id);
        return (AIMessage)get(aim);
    }
}
