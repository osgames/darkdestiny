/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.ShipFleet;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ShipFleetDAO extends ReadWriteTable<ShipFleet> implements GenericDAO {

    public ShipFleet getById(int id) {
        ShipFleet sf = new ShipFleet();
        sf.setId(id);

        return (ShipFleet) get(sf);
    }

    public ArrayList<ShipFleet> findByFleetId(Integer fleetId) {
        ShipFleet sf = new ShipFleet();
        sf.setFleetId(fleetId);
        return find(sf);
    }

    public ShipFleet getByFleetDesign(int fleetId, int designId) {
        ShipFleet sf = new ShipFleet();
        sf.setFleetId(fleetId);
        sf.setDesignId(designId);

        ArrayList<ShipFleet> sfResult = find(sf);
        if (sfResult.isEmpty()) {
            return null;
        } else {
            return sfResult.get(0);
        }
    }

    public ShipFleet findBy(Integer fleetId, Integer designId) {
        ShipFleet sf = new ShipFleet();
        sf.setFleetId(fleetId);
        sf.setDesignId(designId);

        ArrayList<ShipFleet> sfList = find(sf);
        if (sfList.isEmpty()) {
            return null;
        } else {
            return sfList.get(0);
        }
    }

    public ShipFleet updateOrDelete(Integer fleetId, Integer designId, Integer value) {
        ShipFleet sfSearch = new ShipFleet();
        sfSearch.setFleetId(fleetId);
        sfSearch.setDesignId(designId);

        ArrayList<ShipFleet> result = find(sfSearch);
        if (result.isEmpty()) {
            return null;
        } else {
            ShipFleet sf = result.get(0);
            if (sf.getCount().equals(value)) {
                remove(sf);
            } else if (sf.getCount() < value) {
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Wanted to delete more ships than existing");
            } else {
                sf.setCount(sf.getCount() - value);
                update(sf);
            }
            if (result.size() > 1) {
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "There were existing 2 ShipFleet entries for Fleet : " + fleetId + " design : " + designId + " value : " + value);
            }
        }
        return null;
    }

    public ShipFleet updateOrCreate(Integer fleetId, Integer designId, Integer value) {
        ShipFleet sfSearch = new ShipFleet();
        sfSearch.setFleetId(fleetId);
        sfSearch.setDesignId(designId);
        ArrayList<ShipFleet> result = find(sfSearch);

        if (result.isEmpty()) {
            // Create
            if (value == 0) return null;
            ShipFleet sfNew = new ShipFleet();
            sfNew.setFleetId(fleetId);
            sfNew.setDesignId(designId);
            sfNew.setCount(value);
            return add(sfNew);
        } else {
            // Update
            ShipFleet sfUpd = result.get(0);
            sfUpd.setCount(sfUpd.getCount() + value);
            return update(sfUpd);
        }
    }
}
