/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EAddLocation;
import at.viswars.model.TradeRouteShip;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class TradeRouteShipDAO extends ReadWriteTable<TradeRouteShip> implements GenericDAO {
    public ArrayList<TradeRouteShip> findByTradeRouteId(Integer tradeRouteId) {
        TradeRouteShip trs = new TradeRouteShip();
        trs.setTradeRouteId(tradeRouteId);
        return find(trs);
    }

    public TradeRouteShip findByRouteDesignAndAddLocation(int routeId, int designId, EAddLocation addLoc) {
        TradeRouteShip trs = new TradeRouteShip();
        trs.setTradeRouteId(routeId);
        trs.setAddLocation(addLoc);
        trs.setShipType(TradeRouteShip.SHIPTYPE_NORMAL);
        trs.setDesignId(designId);
        
        ArrayList<TradeRouteShip> trsList = find(trs);
        if (trsList.size() == 0) {
            return null;
        } else {
            return trsList.get(0);
        }
    }    
    
    public ArrayList<TradeRouteShip> findByRouteAndDesign(int routeId, int designId) {
        TradeRouteShip trs = new TradeRouteShip();
        trs.setTradeRouteId(routeId);
        trs.setShipType(TradeRouteShip.SHIPTYPE_NORMAL);
        trs.setDesignId(designId);
        
        ArrayList<TradeRouteShip> trsList = find(trs);
        return trsList;
    }
    
    public TradeRouteShip getTransmitterEntry(int routeId) {
        TradeRouteShip trs = new TradeRouteShip();
        trs.setTradeRouteId(routeId);
        trs.setAddLocation(EAddLocation.IGNORE);
        trs.setShipType(TradeRouteShip.SHIPTYPE_CONSTRUCTION);
        trs.setDesignId(0);
        
        return (TradeRouteShip)get(trs);
    }
}
