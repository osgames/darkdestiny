/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.DebugBuffer;
import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.LoginTracker;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class LoginTrackerDAO extends ReadWriteTable<LoginTracker> implements GenericDAO {

    public LoginTracker findLatestByIp(String ip) {
        try {
            LoginTracker ltSearch = new LoginTracker();
            ltSearch.setLoginIP(ip);
            ArrayList<LoginTracker> result = find(ltSearch);

            TreeMap<Long, LoginTracker> tmp = new TreeMap<Long, LoginTracker>();
            for (LoginTracker lt : result) {
                tmp.put(lt.getTime(), lt);
            }
            result.clear();
            result.addAll(tmp.descendingMap().values());
            if (result.isEmpty()) {
                return null;
            } else {
                return result.get(0);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Login tracker error for IP " + ip, e);
        } finally {
            return null;
        }
    }
}
