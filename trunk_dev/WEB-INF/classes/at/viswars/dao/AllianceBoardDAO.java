/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.AllianceBoard;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class AllianceBoardDAO extends ReadWriteTable<AllianceBoard> implements GenericDAO {

    public AllianceBoard findById(Integer id) {
        AllianceBoard ab = new AllianceBoard();
        ab.setId(id);

        return (AllianceBoard)get(ab);
    }

    public ArrayList<AllianceBoard> findByAllianceId(int allianceId) {
        AllianceBoard ab = new AllianceBoard();
        ab.setAllianceId(allianceId);
        return find(ab);
    }
   
}
