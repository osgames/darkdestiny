/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Intelligence;

/**
 *
 * @author Stefan
 */
public class IntelligenceDAO extends ReadWriteTable<Intelligence> implements GenericDAO {
    public Intelligence findById(Integer id) {
        Intelligence i = new Intelligence();
        i.setId(id);
        return (Intelligence)get(i);
    }
}
