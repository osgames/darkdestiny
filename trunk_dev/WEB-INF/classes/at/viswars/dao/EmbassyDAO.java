/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Embassy;

/**
 *
 * @author Stefan
 */
public class EmbassyDAO extends ReadWriteTable<Embassy> implements GenericDAO {

    public boolean hasEmbassy(Integer userId, Integer remUserId) {
        Embassy e = new Embassy();
        e.setUserId(userId);
        e.setRemUserId(remUserId);
        e = (Embassy) get(e);
        if (e == null) {
            return false;
        } else {
            return true;
        }
    }

    public Embassy findBy(int userId, int remUserId) {
        Embassy e = new Embassy();
        e.setUserId(userId);
        e.setRemUserId(remUserId);
        return (Embassy) get(e);
    }

    public void updateSync(Integer userId, Integer remUserId, Boolean synced) {
        Embassy e1 = findBy(userId, remUserId);
        e1.setSynced(synced);
        update(e1);

        Embassy e2 = findBy(userId, remUserId);
        e2.setSynced(synced);
        update(e2);
    }

    public Embassy findMutualBy(Integer userId, Integer userId1) {
        Embassy e = new Embassy();
        e.setUserId(userId);
        e.setRemUserId(userId1);
        e = (Embassy) get(e);
        if (e == null) {
            e = new Embassy();
            e.setUserId(userId1);
            e.setRemUserId(userId);
            return (Embassy) get(e);
        } else {
            return e;
        }
    }
}
