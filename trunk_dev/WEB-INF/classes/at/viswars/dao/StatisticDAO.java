/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.AllianceMember;
import at.viswars.model.Statistic;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class StatisticDAO extends ReadWriteTable<Statistic>  implements GenericDAO{

    public Statistic findByUserId(int userId) {
        Statistic s = new Statistic();
        s.setUserId(userId);
        return (Statistic) get(s);
    }

    public TreeMap<Integer, ArrayList<Statistic>> findAllOrdered() {
        ArrayList<Statistic> result = findAll();
        TreeMap<Integer, ArrayList<Statistic>> orderedResult = new TreeMap<Integer, ArrayList<Statistic>>();
        for(Statistic s : result){
            ArrayList<Statistic> ranks = orderedResult.get(s.getRank());
            if(ranks == null){
                ranks = new ArrayList<Statistic>();
            }
            ranks.add(s);
            orderedResult.put(s.getRank(), ranks);
        }
        result.clear();
        return orderedResult;
    }



}
