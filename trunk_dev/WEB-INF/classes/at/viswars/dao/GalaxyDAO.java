/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.Logger.Logger;
import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Galaxy;
import at.viswars.model.Planet;
import at.viswars.model.PlanetRessource;
import at.viswars.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class GalaxyDAO extends ReadWriteTable<Galaxy> implements GenericDAO {
    
    public void deleteGalaxy(Integer galaxyId) {
        Logger.getLogger().write("Deleting Galaxy  : " + galaxyId);
        Galaxy g = new Galaxy();
        g.setId(galaxyId);
        g = (Galaxy) get(g);
        
        ArrayList<PlanetRessource> resToDelete = new ArrayList<PlanetRessource>();
        ArrayList<Planet> planToDelete = new ArrayList<Planet>();
        ArrayList<at.viswars.model.System> sysToDelete = new ArrayList<at.viswars.model.System>();
        for (at.viswars.model.System s : (ArrayList<at.viswars.model.System>) Service.systemDAO.findAll()) {
            if (s.getId() < g.getStartSystem() || s.getId() > g.getEndSystem()) {
                continue;
            }
            for (Planet p : Service.planetDAO.findBySystemId(s.getId())) {
                for (PlanetRessource pr : Service.planetRessourceDAO.findByPlanetId(p.getId())) {
                    
                    resToDelete.add(pr);
                }
                planToDelete.add(p);
            }
            sysToDelete.add(s);
        }
        Service.planetRessourceDAO.removeAll(resToDelete);
        Service.planetDAO.removeAll(planToDelete);
        Service.systemDAO.removeAll(sysToDelete);
        remove(g);
        Logger.getLogger().write("Done - Deleting Galaxy  : ");
        
    }

    public Galaxy findById(int galaxyId) {
        Galaxy g = new Galaxy();
        g.setId(galaxyId);
        return (Galaxy) get(g);
    }
}
