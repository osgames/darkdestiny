/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.fleet.FleetType;
import at.viswars.model.FleetOrder;

/**
 *
 * @author Stefan
 */
public class FleetOrderDAO extends ReadWriteTable<FleetOrder> implements GenericDAO {
    public FleetOrder get(Integer fleetId, FleetType fleetType) {
        FleetOrder fd = new FleetOrder();
        fd.setFleetId(fleetId);
        fd.setFleetType(fleetType);

        return (FleetOrder)get(fd);
    }
}
