/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.Logger.Logger;
import at.viswars.database.framework.QueryKey;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.DesignModule;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class DesignModuleDAO extends ReadWriteTable<DesignModule> implements GenericDAO {
    public ArrayList<DesignModule> findByDesignId(Integer designId) {
        DesignModule dm = new DesignModule();
        dm.setDesignId(designId);
        return find(dm);
    }
    
    public void deleteDesign(int designId) {
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("designId",designId));
  
        removeAll(qks);
    }
    
    public void addModulesForDesign(int designId, HashMap<Integer,Integer> modules) {
        ArrayList<DesignModule> toInsert = new ArrayList<DesignModule>();
        
        for (Map.Entry<Integer,Integer> mEntry : modules.entrySet()) {
            if (mEntry.getValue() <= 0) continue;
            
            DesignModule dm = new DesignModule();
            dm.setDesignId(designId);
            dm.setModuleId(mEntry.getKey());
            dm.setCount(mEntry.getValue());
            dm.setIsConstruction(false);        
            
            Logger.getLogger().write("----------------- BEFORE ----------------");
            Logger.getLogger().write("-- DID: "+designId+" MID: "+mEntry.getKey()+" C: "+mEntry.getValue()+" ISC: false --");
            toInsert.add(dm);
            Logger.getLogger().write("----------------- AFTER ----------------");
        }
        
        this.insertAll(toInsert);
    }
}
