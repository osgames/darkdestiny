/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Planet;
import at.viswars.model.PlanetLog;
import at.viswars.model.ViewTable;
import at.viswars.result.DiplomacyResult;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Eobane
 */
//public class ViewTableDAO extends ReadOnlyTable<ViewTable> implements GenericDAO {
public class ViewTableDAO extends ReadWriteTable<ViewTable> implements GenericDAO {

    private PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private UserDataDAO userDataDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);

    public void addSystem(int userId, int systemId, int time) {
        ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
        ArrayList<ViewTable> vtList = new ArrayList<ViewTable>();

        for (Planet p : pList) {
            ViewTable vt = new ViewTable();
            vt.setUserId(userId);
            vt.setSystemId(systemId);
            vt.setPlanetId(p.getId());
            vt.setLastTimeVisited(time);
            vt.setMostRecent(true);
            vtList.add(vt);
        }
        //Create fake entry
        if (pList.size() == 0) {
            ViewTable vt = new ViewTable();
            vt.setUserId(userId);
            vt.setSystemId(systemId);
            vt.setPlanetId(0);
            vt.setLastTimeVisited(time);
            vt.setMostRecent(true);
            vtList.add(vt);
        }

        insertAll(vtList);
    }


    /*
    public void addSystem(int userId, int systemId, int time, TransactionHandler th) throws TransactionException {
    Logger.getLogger().write("CALLED ADD SYSTEM WITH TRANSACTION! ("+getTransactionMode()+")");
    Logger.getLogger().write("CURR THREAD: " + Thread.currentThread());
    
    ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
    ArrayList<ViewTable> vtList = new ArrayList<ViewTable>();
    
    for (Planet p : pList) {
    ViewTable vt = new ViewTable();
    vt.setUserId(userId);
    vt.setSystemId(systemId);
    vt.setPlanetId(p.getId());
    vt.setLastTimeVisited(time);
    vt.setMostRecent(true);
    vtList.add(vt);
    }
    
    insertAll(vtList,th);
    }
     */
    public void deleteSystem(int userId, int systemId) {
        ViewTable vtSearch = new ViewTable();
        vtSearch.setUserId(userId);
        vtSearch.setSystemId(systemId);
        
        ArrayList<ViewTable> vtList = find(vtSearch);
        for (ViewTable vt : vtList) {
            remove(vt);
        }        
    }

    public boolean containsSystem(int userId, int systemId) {
        ViewTable vtSearch = new ViewTable();
        vtSearch.setUserId(userId);
        vtSearch.setSystemId(systemId);

        ArrayList<ViewTable> result = find(vtSearch);
        return (!(result.size() == 0));
    }

    public ArrayList<ViewTable> findByUser(int userId) {
        ViewTable vtSearch = new ViewTable();
        vtSearch.setUserId(userId);
        return find(vtSearch);
    }

    @Deprecated //Does not involve PlanetLog and Diplomacy
    public HashMap<Integer, HashMap<Integer, ViewTable>> findByUserCategorized(int userId) {
        ArrayList<ViewTable> vtList = new ArrayList<ViewTable>();
        vtList = findByUser(userId);

        HashMap<Integer, HashMap<Integer, ViewTable>> categorizedEntries =
                new HashMap<Integer, HashMap<Integer, ViewTable>>();

        for (ViewTable vtEntry : vtList) {
            if (categorizedEntries.containsKey(vtEntry.getSystemId())) {
                HashMap<Integer, ViewTable> planetEntries = categorizedEntries.get(vtEntry.getSystemId());
                planetEntries.put(vtEntry.getPlanetId(), vtEntry);
            } else {
                HashMap<Integer, ViewTable> planetEntries = new HashMap<Integer, ViewTable>();
                planetEntries.put(vtEntry.getPlanetId(), vtEntry);
                categorizedEntries.put(vtEntry.getSystemId(), planetEntries);
            }
        }

        return categorizedEntries;
    }

    @Deprecated //Does not involve PlanetLog and Diplomacy
    public HashMap<Integer, HashMap<Integer, ViewTable>> findByUserAndSystemCategorized(int userId, int systemId) {
        ArrayList<ViewTable> vtList = new ArrayList<ViewTable>();
        vtList = findByUserAndSystem(userId, systemId);
        HashMap<Integer, HashMap<Integer, ViewTable>> categorizedEntries =
                new HashMap<Integer, HashMap<Integer, ViewTable>>();

        for (ViewTable vtEntry : vtList) {
            if (categorizedEntries.containsKey(vtEntry.getSystemId())) {
                HashMap<Integer, ViewTable> planetEntries = categorizedEntries.get(vtEntry.getSystemId());
                planetEntries.put(vtEntry.getPlanetId(), vtEntry);
            } else {
                HashMap<Integer, ViewTable> planetEntries = new HashMap<Integer, ViewTable>();
                planetEntries.put(vtEntry.getPlanetId(), vtEntry);
                categorizedEntries.put(vtEntry.getSystemId(), planetEntries);
            }
        }

        return categorizedEntries;
    }

    public ArrayList<ViewTable> findByUserAndSystem(int userId, int systemId) {
        ViewTable vtSearch = new ViewTable();
        vtSearch.setUserId(userId);
        vtSearch.setSystemId(systemId);
        return find(vtSearch);
    }

    public ViewTable findBy(int userId, int systemId, int planetId) {
        ViewTable vtSearch = new ViewTable();
        vtSearch.setUserId(userId);
        vtSearch.setSystemId(systemId);
        vtSearch.setPlanetId(planetId);
        return (ViewTable) get(vtSearch);
    }

    public boolean isSystemInViewtable(int userId, int systemId) {

        ViewTable vtSearch = new ViewTable();
        vtSearch.setUserId(userId);
        vtSearch.setSystemId(systemId);
        if (find(vtSearch).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public int getLastUser(int lastUser, int planetId, int lastTimeVisited) {
        
        for (PlanetLog pl : plDAO.findByPlanetIdSorted(planetId)) {
            if(pl.getTime() > lastTimeVisited){
                continue;
            }else{
                lastUser = pl.getUserId();
            }
        }
        return lastUser;
    }
}
