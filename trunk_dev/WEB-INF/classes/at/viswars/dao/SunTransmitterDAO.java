/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.SunTransmitter;

/**
 *
 * @author Stefan
 */
public class SunTransmitterDAO extends ReadWriteTable<SunTransmitter> implements GenericDAO  {
    public SunTransmitter findById(int id) {
        SunTransmitter st = new SunTransmitter();
        st.setId(id);
        return (SunTransmitter)get(st);
    }       
}
