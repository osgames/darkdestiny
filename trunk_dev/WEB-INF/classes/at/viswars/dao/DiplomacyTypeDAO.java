/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.DiplomacyType;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class DiplomacyTypeDAO extends ReadOnlyTable<DiplomacyType> implements GenericDAO {

    public DiplomacyType findById(int diplomacyTypeId) {
        DiplomacyType dtSearch = new DiplomacyType();
        dtSearch.setId(diplomacyTypeId);
        return (DiplomacyType)get(dtSearch);
    }
    public ArrayList<DiplomacyType> findAllSettable() {
        ArrayList<DiplomacyType> result = new ArrayList<DiplomacyType>();
        for(DiplomacyType dt : (ArrayList<DiplomacyType>)findAll()){
            if(dt.getUserDefineable()){
                result.add(dt);
            }
        }
        return result;
    }
}
