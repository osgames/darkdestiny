/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.StyleElement;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class StyleElementDAO extends ReadOnlyTable<StyleElement> implements GenericDAO {

    public StyleElement findBy(int id, int styleId) {
        StyleElement se = new StyleElement();
        se.setId(id);
        se.setStyleId(styleId);
        return (StyleElement) get(se);
    }

    public ArrayList<StyleElement> findByStyleId(int styleId) {
        StyleElement se = new StyleElement();
        se.setStyleId(styleId);
        return find(se);

    }
}
