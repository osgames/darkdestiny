/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EActionType;
import at.viswars.model.Action;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class ActionDAO extends ReadWriteTable<Action> implements GenericDAO {

    public Action findByTimeFinished(int id, EActionType type) {
        Action a = new Action();
        a.setType(type);
        a.setRefTableId(id);
        ArrayList<Action> result = find(a);
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public ArrayList<Action> findByPlanet(int planetId) {
        Action a = new Action();
        a.setPlanetId(planetId);
        return find(a);
    }

    public ArrayList<Action> findByUserAndType(int userId, EActionType type) {
        Action a = new Action();
        a.setUserId(userId);
        a.setType(type);
        return find(a);
    }

    public Action findResearchByTimeFinished(int userId, int refTableId) {
        Action a = new Action();
        a.setUserId(userId);
        a.setType(EActionType.RESEARCH);
        a.setRefTableId(refTableId);
        ArrayList<Action> result = find(a);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public Action findResearchBy(int userId, int researchId) {
        Action a = new Action();
        a.setUserId(userId);
        a.setType(EActionType.RESEARCH);
        a.setResearchId(researchId);
        ArrayList<Action> result = find(a);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public Action findByConstructionId(int userId, int constructionId, int planetId) {
        Action a = new Action();
        a.setUserId(userId);
        a.setType(EActionType.BUILDING);
        a.setRefEntityId(constructionId);
        a.setPlanetId(planetId);
        ArrayList<Action> result = find(a);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<Action> findResearchesByUserId(int userId) {
        Action a = new Action();
        a.setUserId(userId);
        a.setType(EActionType.RESEARCH);
        return find(a);

    }

    public ArrayList<Action> findRunningConstructions(int planetId, int userId) {
        Action action = new Action();
        action.setUserId(userId);
        action.setPlanetId(planetId);
        action.setType(EActionType.BUILDING);
        ArrayList<Action> result = find(action);
        TreeMap<Integer, Action> sortedResult = new TreeMap<Integer, Action>();
        for (Action a : result) {
            sortedResult.put(a.getRefTableId(), a);
        }
        result.clear();
        result.addAll(sortedResult.values());
        return result;
    }

    public Action findFlightEntryByFleetDetail(int fleetDetailId) {
        Action a = new Action();
        a.setType(EActionType.FLIGHT);
        a.setRefTableId(fleetDetailId);
        ArrayList<Action> aList = find(a);

        if (aList.size() == 0) {
            return null;
        } else {
            return aList.get(0);
        }
    }

    public ArrayList<Action> findRunningConstructions(int planetId) {
        Action action = new Action();
        action.setPlanetId(planetId);
        action.setType(EActionType.BUILDING);
        ArrayList<Action> result = find(action);
        TreeMap<Integer, Action> sortedResult = new TreeMap<Integer, Action>();
        for (Action a : result) {
            sortedResult.put(a.getRefTableId(), a);
        }
        result.clear();
        result.addAll(sortedResult.values());
        return result;
    }

    public ArrayList<Action> findRunningConstructionsGlobal(int userId) {
        Action action = new Action();
        action.setUserId(userId);
        action.setType(EActionType.BUILDING);
        ArrayList<Action> result = find(action);
        TreeMap<Integer, Action> sortedResult = new TreeMap<Integer, Action>();
        for (Action a : result) {
            sortedResult.put(a.getRefTableId(), a);
        }
        result.clear();
        result.addAll(sortedResult.values());
        return result;
    }

    public ArrayList<Action> findByUserId(int userId) {
        Action a = new Action();
        a.setUserId(userId);
        return find(a);
    }
}
