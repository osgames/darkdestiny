/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.TradePost;
import at.viswars.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TradePostDAO extends ReadWriteTable<TradePost> implements GenericDAO {

    public TradePost findByPlanetId(Integer planetId) {
        TradePost tp = new TradePost();
        tp.setPlanetId(planetId);
        ArrayList<TradePost> result = find(tp);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }
public ArrayList<TradePost> findByUserId(Integer userId) {
        
    ArrayList<TradePost> result = new ArrayList<TradePost>();
    for(TradePost tp : (ArrayList<TradePost>)findAll()){
        PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(tp.getPlanetId());
        if(pp != null && pp.getUserId() == userId){
            result.add(tp);
        }
    }
    return result;
    }
    public TradePost findById(Integer id) {
        TradePost tp = new TradePost();
        tp.setId(id);
        return (TradePost) get(tp);
    }
}
