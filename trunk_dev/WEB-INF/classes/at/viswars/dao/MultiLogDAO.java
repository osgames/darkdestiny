/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.MultiLog;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class MultiLogDAO extends ReadWriteTable<MultiLog> implements GenericDAO {


    public MultiLog findBy(String address, int tick, int userId) {
        MultiLog mlSearch = new MultiLog();
        mlSearch.setIp(address);
        mlSearch.setTick(tick);
        mlSearch.setUserId(userId);
        return(MultiLog)get(mlSearch);
    }

    public void deleteByIp(String ip) {

        MultiLog mlSearch = new MultiLog();
        mlSearch.setIp(ip);
        for(MultiLog ml : (ArrayList<MultiLog>)find(mlSearch)){
            remove(ml);
        }

    }


    public void deleteByIpAndTick(String ip, int tick) {

        MultiLog mlSearch = new MultiLog();
        mlSearch.setIp(ip);
        mlSearch.setTick(tick);
        for(MultiLog ml : (ArrayList<MultiLog>)find(mlSearch)){
            remove(ml);
        }

    }
    public void deleteBy(String ip, int tick, int userId) {

        MultiLog mlSearch = new MultiLog();
        mlSearch.setIp(ip);
        mlSearch.setTick(tick);
        mlSearch.setUserId(userId);
        remove((MultiLog)get(mlSearch));

    }
    
}
