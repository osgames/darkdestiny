/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.UserData;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class UserDataDAO extends ReadWriteTable<UserData> implements GenericDAO {

    public UserData findByUserId(Integer userId){
        UserData ud = new UserData();
        ud.setUserId(userId);
        ArrayList<UserData> result = find(ud);
        
        if(result.size() == 0){
            return null;
        }else{
            return result.get(0);
        }
    }
}
