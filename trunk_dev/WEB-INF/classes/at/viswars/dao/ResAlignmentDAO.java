/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.ResAlignment;

/**
 *
 * @author Bullet
 */
public class ResAlignmentDAO extends ReadWriteTable<ResAlignment> implements GenericDAO {

    public ResAlignment findById(int id) {
        ResAlignment ra = new ResAlignment();
        ra.setId(id);
        return (ResAlignment)get(ra);
    }
}
