/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.TerritoryMapPoint;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TerritoryMapPointDAO extends ReadWriteTable<TerritoryMapPoint> implements GenericDAO {

    public ArrayList<TerritoryMapPoint> findByTerritoryId(Integer id) {
        TerritoryMapPoint tmp = new TerritoryMapPoint();
        tmp.setTerritoryId(id);
        return find(tmp);
    }
}
