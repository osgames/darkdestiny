/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.ShipDesign;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ShipDesignDAO extends ReadWriteTable<ShipDesign> implements GenericDAO {

    public ShipDesign findById(Integer id) {
        ShipDesign sd = new ShipDesign();
        sd.setId(id);
        return (ShipDesign)get(sd);
    }

    public ArrayList<ShipDesign> findByUserId(int userId) {
        ShipDesign sd = new ShipDesign();
        sd.setUserId(userId);
        return find(sd);
    }
    
    public ArrayList<ShipDesign> findByUserIdAndChassis(int userId, int chassisId) {
        ShipDesign sd = new ShipDesign();
        sd.setUserId(userId);
        sd.setChassis(chassisId);
        return find(sd);        
    }


    public ArrayList<ShipDesign> findByChassisId(int chassisId) {
        ShipDesign sd = new ShipDesign();
        sd.setChassis(chassisId);
        return find(sd);
    }
}
