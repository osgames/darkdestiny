/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EPassword;
import at.viswars.model.Password;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PasswordDAO extends ReadWriteTable<Password> implements GenericDAO {
    public Password findByType(EPassword password) {
        Password p = new Password();
        p.setPassword(password);

        return (Password)get(p);
    }
}
