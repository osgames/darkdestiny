/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.UserSettings;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class PlayerPlanetDAO extends ReadWriteTable<PlayerPlanet> implements GenericDAO {
    private PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private UserSettingsDAO usDAO = (UserSettingsDAO) DAOFactory.get(UserSettingsDAO.class);
    
    public PlayerPlanet findById(int id) {
        PlayerPlanet pp = new PlayerPlanet();
        pp.setId(id);
        return (PlayerPlanet) get(pp);
    }

    public ArrayList<PlayerPlanet> findByNameAndUserId(String name, int userId) {
        PlayerPlanet pp = new PlayerPlanet();
        pp.setUserId(userId);
        pp.setName(name);

        return find(pp);
    }    
    public ArrayList<PlayerPlanet> findAllToUpdate() {
        ArrayList<PlayerPlanet> ppList = findAll();
        ArrayList<PlayerPlanet> result = new ArrayList<PlayerPlanet>();
        HashSet<Integer> onLeave = new HashSet<Integer>();
        for(UserSettings us : (ArrayList<UserSettings>)usDAO.findAll()){
            if(us.isFrozen()){
                onLeave.add(us.getUserId());
            }
        }
        for(PlayerPlanet pp : ppList){
            if(!onLeave.contains(pp.getUserId())){
                result.add(pp);
            }
        }


        return result;
    }
    public ArrayList<PlayerPlanet> findByName(String name) {
        PlayerPlanet pp = new PlayerPlanet();
        pp.setName(name);

        return find(pp);
    }

    public PlayerPlanet findByPlanetId(int planetId) {
        PlayerPlanet pp = new PlayerPlanet();
        pp.setPlanetId(planetId);
        ArrayList<PlayerPlanet> result = find(pp);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<PlayerPlanet> findByUserIdSortedByName(int userId) {

        PlayerPlanet pp = new PlayerPlanet();
        pp.setUserId(userId);
        ArrayList<PlayerPlanet> result = find(pp);
        TreeMap<String, PlayerPlanet> sortedResult = new TreeMap<String, PlayerPlanet>();
        for (PlayerPlanet tmpPP : result) {
            sortedResult.put(tmpPP.getName(), tmpPP);
        }
        result.clear();
        result.addAll(sortedResult.values());
        return result;

    }

    public ArrayList<PlayerPlanet> findByUserId(int userId) {
        PlayerPlanet pp = new PlayerPlanet();
        pp.setUserId(userId);
        return find(pp);
    }

    public ArrayList<PlayerPlanet> findByUserIdSorted(int userId) {
        PlayerPlanet ppSearch = new PlayerPlanet();
        ppSearch.setUserId(userId);
        ArrayList<PlayerPlanet> ppList = find(ppSearch);        
        
        TreeMap<Integer,TreeMap<Integer,PlayerPlanet>> sorted = 
                new TreeMap<Integer,TreeMap<Integer,PlayerPlanet>>();
        
        for (PlayerPlanet ppTmp : ppList) {
            Planet p = pDAO.findById(ppTmp.getPlanetId());
            
            if (sorted.containsKey(p.getSystemId())) {
                TreeMap<Integer,PlayerPlanet> innerList = sorted.get(p.getSystemId());
                innerList.put(p.getId(), ppTmp);
            } else {
                TreeMap<Integer,PlayerPlanet> innerList = 
                        new TreeMap<Integer,PlayerPlanet>();
                
                innerList.put(p.getId(), ppTmp);
                sorted.put(p.getSystemId(), innerList);
            }
        }
        
        ArrayList<PlayerPlanet> outputList = new ArrayList<PlayerPlanet>();
        
        for (Map.Entry<Integer,TreeMap<Integer,PlayerPlanet>> entry : sorted.entrySet()) {
            TreeMap<Integer,PlayerPlanet> innerList = entry.getValue();
            
            for (Map.Entry<Integer,PlayerPlanet> innerEntry : innerList.entrySet()) {
                outputList.add(innerEntry.getValue());
            }
        }
        
        return outputList;
    }
    
    public ArrayList<PlayerPlanet> findVisibleByUserId(int userId, boolean invisible) {
        PlayerPlanet pp = new PlayerPlanet();
        pp.setUserId(userId);
        pp.setInvisibleFlag(invisible);
        return find(pp);

    }

    public PlayerPlanet findHomePlanetByUserId(int userId) {

        PlayerPlanet pp = new PlayerPlanet();
        pp.setUserId(userId);
        pp.setHomeSystem(true);
        ArrayList<PlayerPlanet> result = find(pp);
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
