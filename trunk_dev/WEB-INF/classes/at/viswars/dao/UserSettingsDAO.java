/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.UserSettings;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class UserSettingsDAO extends ReadWriteTable<UserSettings> implements GenericDAO {

    public UserSettings findByUserId(Integer userId){
        UserSettings us = new UserSettings();
        us.setUserId(userId);
        ArrayList<UserSettings> result = find(us);
        
        if(result.isEmpty()){
            return null;
        }else{
            return result.get(0);
        }
    }
}
