/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.ETerritoryMapType;
import at.viswars.model.TerritoryMap;
import at.viswars.model.TerritoryMapPoint;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TerritoryMapDAO extends ReadWriteTable<TerritoryMap> implements GenericDAO {

    public ArrayList<TerritoryMap> findBy(int refId, ETerritoryMapType type) {
        TerritoryMap tmSearch = new TerritoryMap();
        tmSearch.setRefId(refId);
        tmSearch.setType(type);
        return (ArrayList<TerritoryMap>) find(tmSearch);
    }

    public TerritoryMap findById(int territoryId) {
        TerritoryMap tmSearch = new TerritoryMap();
        tmSearch.setId(territoryId);
        return (TerritoryMap)get(tmSearch);
    }
    
    public ArrayList<TerritoryMap> findByUserId(int userId) {
        TerritoryMap tmSearch = new TerritoryMap();
        tmSearch.setRefId(userId);
        return find(tmSearch);
    }
}
