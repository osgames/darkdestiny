/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.ConditionToTitle;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ConditionToTitleDAO extends ReadWriteTable<ConditionToTitle> implements GenericDAO {

    public ArrayList<ConditionToTitle> findByTitleId(int titleId){
        ConditionToTitle ctt = new ConditionToTitle();
        ctt.setTitleId(titleId);
        return (ArrayList<ConditionToTitle>)find(ctt);
    }
    public ConditionToTitle findById(int id){
        ConditionToTitle ctt = new ConditionToTitle();
        ctt.setId(id);
        return (ConditionToTitle)get(ctt);
    }
    public ArrayList<ConditionToTitle> findByConditionId(int conditionId){
        ConditionToTitle ctt = new ConditionToTitle();
        ctt.setConditionId(conditionId);
        return (ArrayList<ConditionToTitle>)find(ctt);
    }

}
