/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.Language;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class LanguageDAO extends ReadOnlyTable<Language> implements GenericDAO {
    public Language findById(Integer id) {
        Language l = new Language();
        l.setId(id);
        return (Language)get(l);
    }

    public Language findBy(String locale) {               
        Language l = new Language();
        l.setLanguage(locale.substring(0, locale.indexOf("_")));
        l.setCountry(locale.substring(locale.indexOf("_") + 1, locale.length()));
        ArrayList<Language> ls = find(l);
        if(ls.size() > 0){
            return ls.get(0);
        }else{
            return null;
        }
        
        
    }
    public Language getMasterLanguageOf(int languageId){
        Language l = new Language();
        l.setId(languageId);
        l = get(l);
        if(l.getId() == l.getMasterLanguageId()){
            return l;
        }else{
           int masterId = l.getMasterLanguageId();
           l = new Language();
           l.setId(masterId);
           return get(l);
        }
    }
}
