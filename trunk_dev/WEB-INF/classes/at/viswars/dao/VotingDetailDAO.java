/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.VotingDetail;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class VotingDetailDAO extends ReadWriteTable<VotingDetail> implements GenericDAO {
    public ArrayList<VotingDetail> findByVoteId(int voteId) {
        VotingDetail vd = new VotingDetail();
        vd.setVoteId(voteId);
        return find(vd);
    }        
    
    public ArrayList<VotingDetail> findByVoteIdAndOption(int voteId, int optionId) {
        VotingDetail vd = new VotingDetail();
        vd.setVoteId(voteId);
        vd.setOptionVoted(optionId);
        return find(vd);
    }

    public VotingDetail findByVoteIdAndUser(int voteId, int userId) {
        VotingDetail vd = new VotingDetail();
        vd.setVoteId(voteId);
        vd.setUserId(userId);

        ArrayList<VotingDetail> vdList = find(vd);
        if (vdList.isEmpty()) {
            return null;
        } else {
            return vdList.get(0);
        }
    }
}
