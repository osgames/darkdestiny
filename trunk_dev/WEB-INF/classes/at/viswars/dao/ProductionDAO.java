/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.Production;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class ProductionDAO extends ReadOnlyTable<Production> implements GenericDAO {

    public ArrayList<Production> findProductionForConstructionId(int constructionId) {
        Production p = new Production();
        p.setConstructionId(constructionId);
        return find(p);
    }

    public Iterable<Production> findByRessourceId(int ressourceId) {
        Production p = new Production();
        p.setRessourceId(ressourceId);
        return find(p);
    }
}
