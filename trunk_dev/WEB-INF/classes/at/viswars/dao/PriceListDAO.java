/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.TradeOfferType;
import at.viswars.model.PriceList;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class PriceListDAO extends ReadWriteTable<PriceList> implements GenericDAO {

    public PriceList getById(int id) {
        PriceList pl = new PriceList();
        pl.setId(id);
        return (PriceList) get(pl);
    }

    public ArrayList<PriceList> findByUserId(int userId) {
        PriceList pl = new PriceList();
        pl.setUserId(userId);
        return find(pl);
    }

    public PriceList findByUserIdAndType(int userId, TradeOfferType type) {
        PriceList pl = new PriceList();
        pl.setUserId(userId);
        pl.setTradeOfferType(type);
        ArrayList<PriceList> result = find(pl);
        if (result.isEmpty()) {
            return null;
        } else {
            return (PriceList) result.get(0);
        }
    }

    public PriceList findBy(int userId, int refId, TradeOfferType type) {
        PriceList pl = new PriceList();
        pl.setUserId(userId);
        pl.setRefId(refId);
        pl.setTradeOfferType(type);
        ArrayList<PriceList> result = find(pl);
        if (result.isEmpty()) {
            return null;
        } else {
            return (PriceList) result.get(0);
        }
    }
}
