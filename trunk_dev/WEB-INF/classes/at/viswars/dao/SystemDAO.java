/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class SystemDAO extends ReadWriteTable<at.viswars.model.System> implements GenericDAO{

    public void findAllOrdered() {
        this.findAll();
    }

    public at.viswars.model.System findById(int id) {
        at.viswars.model.System s = new at.viswars.model.System();
        s.setId(id);
        return (at.viswars.model.System)get(s);
    }

    public ArrayList<at.viswars.model.System> findByName(String name) {
        at.viswars.model.System s = new at.viswars.model.System();
        s.setName(name);
        return find(s);
    }
        
    
    public AbsoluteCoordinate getAbsoluteCoordinate(int systemId) {
        at.viswars.model.System s = new at.viswars.model.System();
        s.setId(systemId);
        ArrayList<at.viswars.model.System> sList = find(s);
        
        if (sList.size() == 0) {
            return null;
        } else {            
            AbsoluteCoordinate ac = new AbsoluteCoordinate(sList.get(0).getX(),sList.get(0).getY());
            return ac;
        }
    }

    public int findLastId() {
        int sysId = 0;
        for(at.viswars.model.System s : (ArrayList<at.viswars.model.System>)findAll()){
            if(s.getId() > sysId){
                sysId = s.getId();
            }
        }
        sysId = sysId + 1;;
        return sysId;
    }
}
