/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Research;

/**
 *
 * @author Eobane
 */
public class ResearchDAO extends ReadWriteTable<Research> implements GenericDAO{
    public Research findById(Integer id) {
        Research r = new Research();
        r.setId(id);
        return (Research) get(r);
    }
}
