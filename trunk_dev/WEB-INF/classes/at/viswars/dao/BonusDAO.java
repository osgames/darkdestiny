/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EBonusRange;
import at.viswars.model.Bonus;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class BonusDAO extends ReadWriteTable<Bonus> implements GenericDAO  {

    public Bonus findById(int bonusId) {
        Bonus b = new Bonus();
        b.setId(bonusId);
        return (Bonus)get(b);
    }
    public ArrayList<Bonus> findPlanetBonus() {
        Bonus b = new Bonus();
        b.setBonusRange(EBonusRange.PLANET);
        return find(b);
    }
 
}
