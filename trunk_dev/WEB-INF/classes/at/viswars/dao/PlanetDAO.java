/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Planet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Eobane
 */
public class PlanetDAO extends ReadWriteTable<Planet> implements GenericDAO {

    public ArrayList<Planet> findBySystemId(Integer systemId) {
        Planet p = new Planet();
        p.setSystemId(systemId);
        return find(p);
    }

    public Planet findById(Integer planetId) {
        Planet p = new Planet();
        p.setId(planetId);
        return (Planet)get(p);
    }

    public HashMap<Integer, ArrayList<Planet>> findAllSortedBySystem(){
        HashMap<Integer, ArrayList<Planet>> result = new HashMap<Integer, ArrayList<Planet>>();
        for(Planet p : (ArrayList<Planet>)findAll()){
            ArrayList<Planet> entries = result.get(p.getSystemId());
            if(entries == null){
                entries = new ArrayList<Planet>();
            }
            entries.add(p);
            result.put(p.getSystemId(), entries);
        }

        return result;
    }

    public int findLastPlanet() {
        
        int planetId = 1;
        for(at.viswars.model.Planet p : (ArrayList<at.viswars.model.Planet>)findAll()){
            if(p.getId() >= planetId){
                planetId = p.getId();
            }
        }
        planetId = planetId + 1;
        return planetId;
    }
}
