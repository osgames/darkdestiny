/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.Style;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class StyleDAO extends ReadOnlyTable<Style> implements GenericDAO{
    public Style findById(int id) {
        Style s = new Style();
        s.setId(id);
        return (Style) get(s);
    }
    
}
