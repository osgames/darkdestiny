/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Quiz;

/**
 *
 * @author Stefan
 */
public class QuizDAO extends ReadWriteTable<Quiz> implements GenericDAO  {

    public Quiz findById(Integer id) {
        Quiz qSearch = new Quiz();
        qSearch.setId(id);
        return (Quiz)get(qSearch);
    }

 
}
