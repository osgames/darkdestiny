/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Attribute;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class AttributeDAO extends ReadWriteTable<Attribute> implements GenericDAO {

    public Attribute findById(Integer id) {
        Attribute a = new Attribute();
        a.setId(id);
        ArrayList<Attribute> result = find(a);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }        
    }
}
