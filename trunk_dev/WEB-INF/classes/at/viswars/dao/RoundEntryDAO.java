/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.RoundEntry;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class RoundEntryDAO  extends ReadWriteTable<RoundEntry> implements GenericDAO {

    public ArrayList<RoundEntry> findByCombatRoundId(Integer id) {
        RoundEntry re = new RoundEntry();
        re.setCombatRoundId(id);
        return find(re);
    }
        
}
