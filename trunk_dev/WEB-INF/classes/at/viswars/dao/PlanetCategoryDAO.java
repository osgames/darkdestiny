/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.PlanetCategory;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlanetCategoryDAO extends ReadWriteTable<PlanetCategory> implements GenericDAO {
    public ArrayList<PlanetCategory> findByCategoryId(int categoryId) {
        PlanetCategory pc = new PlanetCategory();
        pc.setCategoryId(categoryId);
        return find(pc);
    }

    public ArrayList<PlanetCategory> findAllByPlanetId(int planetId) {
        PlanetCategory pc = new PlanetCategory();
        pc.setPlanetId(planetId);
        return find(pc);

    }
    public PlanetCategory findBy(int planetId, int categoryId) {
        PlanetCategory pc = new PlanetCategory();
        pc.setPlanetId(planetId);
        pc.setCategoryId(categoryId);

        ArrayList<PlanetCategory> result = find(pc);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
    public PlanetCategory findByPlanetId(int planetId) {
        PlanetCategory pc = new PlanetCategory();
        pc.setPlanetId(planetId);
        ArrayList<PlanetCategory> result = find(pc);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
