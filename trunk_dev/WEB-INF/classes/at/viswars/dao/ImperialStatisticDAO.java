/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.ImperialStatistic;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class ImperialStatisticDAO extends ReadWriteTable<ImperialStatistic> implements GenericDAO {

    public ArrayList<ImperialStatistic> findByUserId(int userId){
        ImperialStatistic is = new ImperialStatistic();
        is.setUserId(userId);
        return find(is);
    }
        public ArrayList<ImperialStatistic> findByAllianceId(int allianceId){
        ImperialStatistic is = new ImperialStatistic();
        is.setAllyId(allianceId);
        return find(is);
    }
}
