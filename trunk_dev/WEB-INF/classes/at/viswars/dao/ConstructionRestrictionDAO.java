/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.ConstructionRestriction;

/**
 *
 * @author Eobane
 */
public class ConstructionRestrictionDAO extends ReadOnlyTable<ConstructionRestriction> implements GenericDAO{

    public ConstructionRestriction getConstructionRestrictionBy(int type, Integer constructionId){
        ConstructionRestriction cr = new ConstructionRestriction();
        cr.setType(type);
        cr.setConstructionId(constructionId);
        return (ConstructionRestriction)get(cr);
    }

}
