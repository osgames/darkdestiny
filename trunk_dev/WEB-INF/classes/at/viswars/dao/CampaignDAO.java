/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Campaign;
import at.viswars.model.CampaignToUser;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class CampaignDAO extends ReadWriteTable<Campaign> implements GenericDAO  {
    public ArrayList<Campaign> findAllOrdered() {
        ArrayList<Campaign> result = new ArrayList<Campaign>();
        result = findAll();
        TreeMap<Integer, Campaign> sorted = new TreeMap<Integer, Campaign>();
        for(Campaign c : result){
            sorted.put(c.getOrder(), c);
        }
        result.clear();
        result.addAll(sorted.values());

        return result;
    }
}
