/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.CombatPlayer;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class CombatPlayerDAO  extends ReadWriteTable<CombatPlayer> implements GenericDAO  {

    public ArrayList<CombatPlayer> findByCombatId(Integer combatId) {
        CombatPlayer cp = new CombatPlayer();
        cp.setCombatId(combatId);
        return find(cp);
    }

    public CombatPlayer findBy(Integer combatId, Integer userId) {
        CombatPlayer cp = new CombatPlayer();
        cp.setCombatId(combatId);
        cp.setUserId(userId);
        return (CombatPlayer)get(cp);
    }

}
