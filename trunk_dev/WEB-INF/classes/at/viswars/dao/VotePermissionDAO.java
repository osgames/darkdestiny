/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.VotePermission;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class VotePermissionDAO extends ReadWriteTable<VotePermission> implements GenericDAO {
    public VotePermission getById(int id) {
        VotePermission vp = new VotePermission();
        vp.setId(id);
        return (VotePermission)get(vp);
    }
    
    public ArrayList<VotePermission> findByVoteId(int voteId) {
        VotePermission vp = new VotePermission();
        vp.setVoteId(voteId);
        return find(vp);
    }
}
