/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.CreditDonation;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Bullet
 */
public class CreditDonationDAO extends ReadWriteTable<CreditDonation> implements GenericDAO{
    public ArrayList<CreditDonation> findLastWeekDonations(int userId) {
        CreditDonation cd = new CreditDonation();
        cd.setFromUserId(userId);

        long currDate = System.currentTimeMillis();
        ArrayList<CreditDonation> cdList = find(cd);
        for (Iterator<CreditDonation> cdIt = cdList.iterator();cdIt.hasNext();) {
            CreditDonation cdTmp = cdIt.next();

            long timeDiff = currDate - cdTmp.getDate();
            if (timeDiff > (1000 * 60 * 60 * 24 * 7)) {
                cdIt.remove();
            }
        }

        return cdList;
    }

    public ArrayList<CreditDonation> findByToUserId(int userId) { 
        CreditDonation cd = new CreditDonation();
        cd.setToUserId(userId);
        return find(cd);
    }
}
