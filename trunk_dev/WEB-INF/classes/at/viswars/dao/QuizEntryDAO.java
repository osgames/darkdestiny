/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.QuizEntry;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class QuizEntryDAO extends ReadWriteTable<QuizEntry> implements GenericDAO  {

    public QuizEntry findById(Integer id) {
        QuizEntry qSearch = new QuizEntry();
        qSearch.setId(id);
        return (QuizEntry)get(qSearch);
    }

 
    public ArrayList<QuizEntry> findByQuizId(Integer quizId) {
        QuizEntry qSearch = new QuizEntry();
        qSearch.setQuizId(quizId);
        return (ArrayList<QuizEntry>)find(qSearch);
    }

}
