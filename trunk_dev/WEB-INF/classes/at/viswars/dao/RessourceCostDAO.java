/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Ressource;
import at.viswars.model.RessourceCost;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.enumeration.ERessourceCostType;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class RessourceCostDAO extends ReadWriteTable<RessourceCost> implements GenericDAO<Ressource> {

    public ArrayList<RessAmountEntry> findRessAmountEntries(ERessourceCostType type, int id) {
        RessourceCost rc = new RessourceCost();
        rc.setType(type);
        rc.setId(id);

        ArrayList<RessourceCost> tmp = find(rc);

        ArrayList<RessAmountEntry> result = new ArrayList<RessAmountEntry>();
        for (RessourceCost tmpRc : tmp) {
            result.add(new RessAmountEntry(tmpRc.getRessId(), tmpRc.getQty()));
        }
        return result;
    }

    public RessourceCost findBy(ERessourceCostType type, int id, int ressourceId) {
        RessourceCost rc = new RessourceCost();
        rc.setType(type);
        rc.setId(id);
        rc.setRessId(ressourceId);

        return (RessourceCost) get(rc);
    }

    public ArrayList<RessourceCost> find(ERessourceCostType type, int id) {
        RessourceCost rc = new RessourceCost();
        rc.setType(type);
        rc.setId(id);

        return find(rc);
    }
}
