/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.ConstructionModule;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ConstructionModuleDAO extends ReadOnlyTable<ConstructionModule> implements GenericDAO {
    private ConstructionModuleDAO() {
    }
    
    public ArrayList<ConstructionModule> findByConstruction(int constructionId) {
        ConstructionModule cm = new ConstructionModule();
        cm.setConstructionId(constructionId);
        
        return find(cm);
    }
}
