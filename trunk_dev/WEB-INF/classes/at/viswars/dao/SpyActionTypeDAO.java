/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.SpyActionType;

/**
 *
 * @author Stefan
 */
public class SpyActionTypeDAO extends ReadWriteTable<SpyActionType> implements GenericDAO {
    public SpyActionType findById(int id) {
        SpyActionType spyActionType = new SpyActionType();
        spyActionType.setId(id);
        return (SpyActionType)get(spyActionType);
    }
}
