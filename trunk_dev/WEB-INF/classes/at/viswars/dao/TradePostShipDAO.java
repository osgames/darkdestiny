/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.TradePostShip;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class TradePostShipDAO extends ReadWriteTable<TradePostShip> implements GenericDAO {

    public ArrayList<TradePostShip> findByTradePostId(int tradepostId) {
        TradePostShip tps = new TradePostShip();
        tps.setTradePostId(tradepostId);
        return find(tps);
    }
}
