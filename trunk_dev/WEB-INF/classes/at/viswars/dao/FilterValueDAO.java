/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.FilterValue;

/**
 *
 * @author Aion
 */
public class FilterValueDAO extends ReadWriteTable<FilterValue> implements GenericDAO {

    public FilterValue findById(int valueId){
        FilterValue searchfv = new FilterValue();
        searchfv.setId(valueId);
        return (FilterValue)get(searchfv);
    }

}
