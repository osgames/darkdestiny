/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Note;

/**
 *
 * @author Aion
 */
public class NoteDAO extends ReadWriteTable<Note> implements GenericDAO {


    public Note findBy(int userId, int planetId){
        Note n = new Note();
        n.setPlanetId(planetId);
        n.setUserId(userId);

        return (Note)get(n);
    }

}
