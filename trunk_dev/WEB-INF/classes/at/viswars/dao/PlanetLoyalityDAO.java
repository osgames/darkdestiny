/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.PlanetLoyality;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class PlanetLoyalityDAO extends ReadWriteTable<PlanetLoyality> implements GenericDAO {
    public ArrayList<PlanetLoyality> findByPlanetId(int planetId) {
        ArrayList<PlanetLoyality> result = null;
        
        PlanetLoyality pl = new PlanetLoyality();
        pl.setPlanetId(planetId);
        
        result = find(pl);
        
        return result;
    }
    
    public HashMap<Integer,PlanetLoyality> findByPlanetIdMap(int planetId) {
        HashMap<Integer,PlanetLoyality> result = new HashMap<Integer,PlanetLoyality>();
        
        PlanetLoyality pl = new PlanetLoyality();
        pl.setPlanetId(planetId);
        
        for (PlanetLoyality pl2 : (ArrayList<PlanetLoyality>)find(pl)) {
            result.put(pl2.getUserId(),pl2);
        }
        
        return result;
    }    
    
    public PlanetLoyality getByPlanetAndUserId(int planetId, int userId) {
        PlanetLoyality pl = new PlanetLoyality();
        pl.setPlanetId(planetId);
        pl.setUserId(userId);
        
        return (PlanetLoyality)get(pl);
    }
}
