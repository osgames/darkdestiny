/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EBonusRange;
import at.viswars.model.ActiveBonus;
import java.util.ArrayList;
import java.util.Set;

/**
 *
 * @author Stefan
 */
public class ActiveBonusDAO extends ReadWriteTable<ActiveBonus> implements GenericDAO  {

    public ArrayList<ActiveBonus> findByPlanetId(int planetId) {
        ActiveBonus ab = new ActiveBonus();
        ab.setRefId(planetId);
        ab.setBonusRange(EBonusRange.PLANET);
        ArrayList<ActiveBonus> result = find(ab);
        return result;
    }

    public ArrayList<ActiveBonus> findByBonusId(int bonusId) {
        ActiveBonus ab = new ActiveBonus();
        ab.setBonusId(bonusId);
        ArrayList<ActiveBonus> result = find(ab);
        return result;
    }
}
