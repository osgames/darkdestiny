/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.DebugBuffer;
import at.viswars.GameConstants;
import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EUserType;
import at.viswars.model.User;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class UserDAO extends ReadWriteTable<User> implements GenericDAO {

    public User findBy(String userName) {
        User u = new User();
        u.setUserName(userName);
        ArrayList<User> result = find(u);
        if ((result.isEmpty()) || (result.size() > 1)) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<User> findAllPlayers() {
        User u = new User();
        u.setUserType(EUserType.HUMAN);
        return find(u);
    }

    public ArrayList<User> findAllNonSystem() {
        ArrayList<User> users = findAll();

        for (Iterator<User> uIt = users.iterator(); uIt.hasNext();) {
            User u = uIt.next();
            if (u.isSystemUser()) {
                uIt.remove();
                break;
            }
        }

        return users;
    }

    public ArrayList<User> findAllSorted(boolean includeAI) {
        ArrayList<User> result = new ArrayList<User>();
        TreeMap<Integer, User> sorted = new TreeMap<Integer, User>();

        ArrayList<User> users = findAllPlayers();
        if (includeAI) {
            users = findAll();
        }
        for (User u : (ArrayList<User>) users) {
            sorted.put(u.getUserId(), u);
        }
        result.addAll(sorted.values());
        return result;
    }

    public ArrayList<User> findAllSortedByName() {
        ArrayList<User> result = new ArrayList<User>();
        TreeMap<String, User> sorted = new TreeMap<String, User>();

        for (User u : (ArrayList<User>) findAllPlayers()) {
            sorted.put(u.getGameName(), u);
        }
        result.addAll(sorted.values());
        return result;
    }

    public User findBy(String userName, String password) {
        User u = new User();
        u.setUserName(userName);
        u.setPassword(password);
        ArrayList<User> result = find(u);
        if (result.isEmpty()) {
            return null;
        } else {
            if (GameConstants.LOGIN_AVAILABLE) {
                if (GameConstants.LOGIN_ONLYADMIN) {
                    if (result.get(0).getAdmin()) {
                        return result.get(0);
                    } else {
                        return null;
                    }
                } else {
                    return result.get(0);
                }
            } else {
                return null;
            }
        }
    }

    public User findPortalUserBy(String userName, String portalpassword) {
        User u = new User();
        u.setUserName(userName);
        u.setPortalPassword(portalpassword);
        ArrayList<User> result = find(u);
        if (result.isEmpty()) {
            return null;
        } else {
            if (GameConstants.LOGIN_AVAILABLE) {
                if (GameConstants.LOGIN_ONLYADMIN) {
                    if (result.get(0).getAdmin()) {
                        return result.get(0);
                    } else {
                        return null;
                    }
                } else {
                    return result.get(0);
                }
            } else {
                return null;
            }
        }
    }

    public User findById(Integer userId) {
        User u = new User();
        u.setUserId(userId);
        return (User) get(u);
    }

    public User findByGameName(String gameName) {
        User u = new User();
        u.setGameName(gameName);
        ArrayList<User> result = find(u);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public User findByUserName(String userName) {
        User u = new User();
        u.setUserName(userName);
        ArrayList<User> result = find(u);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public User getSystemUser() {
        User u = new User();
        u.setUserType(EUserType.SYSTEM);

        ArrayList<User> uList = find(u);
        if (uList.isEmpty()) {
            DebugBuffer.error("No system user found, please create a user with type SYSTEM in user table");
            return null;
        }
        return uList.get(0);
    }
}
