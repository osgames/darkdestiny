/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.PlanetLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class PlanetLogDAO extends ReadWriteTable<PlanetLog> implements GenericDAO {

    public ArrayList<PlanetLog> findByPlanetIdSorted(int planetId) {
        PlanetLog searchLog = new PlanetLog();
        searchLog.setPlanetId(planetId);

        TreeMap<Integer, ArrayList<PlanetLog>> sorted = new TreeMap<Integer, ArrayList<PlanetLog>>();
        for (PlanetLog p : (ArrayList<PlanetLog>) find(searchLog)) {
            if (sorted.containsKey(p.getTime())) {
                sorted.get(p.getTime()).add(p);
            } else {
                ArrayList<PlanetLog> plList = new ArrayList<PlanetLog>();
                plList.add(p);
                sorted.put(p.getTime(),plList);
            }
        }
        ArrayList<PlanetLog> result = new ArrayList<PlanetLog>();
        for (Map.Entry<Integer,ArrayList<PlanetLog>> plTimeEntry : sorted.descendingMap().entrySet()) {
            result.addAll(plTimeEntry.getValue());
        }

        return result;
    }

    public HashMap<Integer, ArrayList<PlanetLog>> findAllSorted() {
        HashMap<Integer, ArrayList<PlanetLog>> sorted0 = new HashMap<Integer, ArrayList<PlanetLog>>();
        HashMap<Integer, ArrayList<PlanetLog>> result = new HashMap<Integer, ArrayList<PlanetLog>>();

        for (PlanetLog p : (ArrayList<PlanetLog>) findAll()) {
            ArrayList<PlanetLog> entries = sorted0.get(p.getPlanetId());
            if (entries == null) {
                entries = new ArrayList<PlanetLog>();
            }
            entries.add(p);
            sorted0.put(p.getPlanetId(), entries);
        }
        for (Map.Entry<Integer, ArrayList<PlanetLog>> entry1 : sorted0.entrySet()) {


            TreeMap<Integer, PlanetLog> sorted = new TreeMap<Integer, PlanetLog>();
            for (PlanetLog pl : entry1.getValue()) {
                sorted.put(pl.getTime(), pl);
            }
            ArrayList<PlanetLog> logs = new ArrayList<PlanetLog>();
            logs.addAll(sorted.descendingMap().values());
            result.put(entry1.getKey(), logs);
        }

        return result;
    }
}
