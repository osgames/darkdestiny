/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Module;

/**
 *
 * @author Stefan
 */
public class ModuleDAO extends ReadWriteTable<Module> implements GenericDAO { 
    public Module findById(int id) {
        Module m = new Module();
        m.setId(id);
        return (Module)get(m);
    }
}
