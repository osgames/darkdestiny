/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.model.FleetDetail;
import at.viswars.database.framework.ReadWriteTable;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class FleetDetailDAO extends ReadWriteTable<FleetDetail> implements GenericDAO {
    public FleetDetail findById(Integer id) {
        FleetDetail fd = new FleetDetail();
        fd.setId(id);
        return (FleetDetail)get(fd);
    }
    
    public FleetDetail findByFleetId(Integer fleetId) {
        FleetDetail fdSearch = new FleetDetail();
        fdSearch.setFleetId(fleetId);       
        ArrayList<FleetDetail> result = find(fdSearch);
        
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
