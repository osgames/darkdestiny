/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.CombatRound;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class CombatRoundDAO  extends ReadWriteTable<CombatRound> implements GenericDAO  {

    public ArrayList<CombatRound> findByCombatId(Integer combatRoundId) {
        ArrayList<CombatRound> result = new ArrayList<CombatRound>();
        TreeMap<Integer, CombatRound> sorted = new TreeMap<Integer, CombatRound>();

        CombatRound cr = new CombatRound();
        cr.setCombatId(combatRoundId);

        result = find(cr);


        for(CombatRound crTmp : result){
            sorted.put(crTmp.getRound(), crTmp);
        }
        result.clear();
        result.addAll(sorted.values());
        return result;
    }
    public CombatRound findLastRound(Integer combatRoundId) {
        ArrayList<CombatRound> result = new ArrayList<CombatRound>();
        TreeMap<Integer, CombatRound> sorted = new TreeMap<Integer, CombatRound>();

        CombatRound cr = new CombatRound();
        cr.setCombatId(combatRoundId);

        result = find(cr);
        if(result.size() == 0){
            return null;
        }
        for(CombatRound crTmp : result){
            sorted.put(crTmp.getRound(), crTmp);
        }
        return sorted.get(sorted.size());
    }


}
