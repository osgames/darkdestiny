/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.FleetFormation;
import at.viswars.service.SystemService;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class FleetFormationDAO extends ReadWriteTable<FleetFormation> implements GenericDAO {
    public FleetFormation findById(int formationId) {
        FleetFormation ffSearch = new FleetFormation();
        ffSearch.setId(formationId);
        return (FleetFormation)get(ffSearch);
    }
    
    public ArrayList<FleetFormation> findFleetFormations(int userId) {
        ArrayList<FleetFormation> allFF = findAll();
        ArrayList<FleetFormation> result = new ArrayList<FleetFormation>();
        
        for (FleetFormation ff : allFF) {
            if (SystemService.areAllied(userId, ff.getUserId())) {
                result.add(ff);
            }
        }
        
        return result;
    }
}
