/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.TitleToUser;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class TitleToUserDAO extends ReadWriteTable<TitleToUser> implements GenericDAO {

    public ArrayList<TitleToUser> findByUserId(int userId) {
        TitleToUser titleToUser = new TitleToUser();
        titleToUser.setUserId(userId);
        return find(titleToUser);
    }
    public TitleToUser findBy(int userId, int titleId) {
        TitleToUser titleToUser = new TitleToUser();
        titleToUser.setUserId(userId);
        titleToUser.setTitleId(titleId);
        return (TitleToUser)get(titleToUser);
    }

   
}
