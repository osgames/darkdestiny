/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.AllianceRank;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class AllianceRankDAO extends ReadWriteTable<AllianceRank> implements GenericDAO {

    public AllianceRank findBy(Integer rankId, Integer allianceId) {
        AllianceRank ar = new AllianceRank();
        ar.setAllianceId(allianceId);
        ar.setRankId(rankId);
        return (AllianceRank)get(ar);
    }

    public ArrayList<AllianceRank> findByAllianceId(Integer allianceId) {
          AllianceRank ar = new AllianceRank();
        ar.setAllianceId(allianceId);
        return find(ar);

    }
    //Todo !!!
    public AllianceRank findByAllianceIdLastRank(Integer allianceId){
        ArrayList<AllianceRank> result = findByAllianceId(allianceId);
        if(result.size() == 0){
            return null;
        }else if(result.size() == 1){
            return result.get(0);
        }else{
            AllianceRank highestRank = null;
            for(AllianceRank ar : result){
                if(highestRank == null){
                    highestRank = ar;
                }else{
                 if(ar.getRankId() > highestRank.getRankId())
                     highestRank=ar;

                }
            }
            return highestRank;
        }
    }
}
