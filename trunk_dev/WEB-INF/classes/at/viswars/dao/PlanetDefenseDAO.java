/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EDefenseType;
import at.viswars.model.PlanetDefense;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class PlanetDefenseDAO extends ReadWriteTable<PlanetDefense> implements GenericDAO {
    public PlanetDefense get(int systemId, int planetId, int userId, EDefenseType edt, int unitId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setSystemId(systemId);
        pd.setPlanetId(planetId);
        pd.setUserId(userId);
        pd.setUnitId(unitId);
        pd.setType(edt);
        pd.setFleetId(0);
        return (PlanetDefense)get(pd);
    }

    public ArrayList<PlanetDefense> findByUserAndType(int userId, EDefenseType edt) {
        PlanetDefense pd = new PlanetDefense();
        pd.setUserId(userId);
        pd.setType(edt);
        return find(pd);
    }

    public ArrayList<PlanetDefense> findByUserAndShipDesign(int userId, int designId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setUserId(userId);
        pd.setType(EDefenseType.SPACESTATION);
        pd.setUnitId(designId);
        return find(pd);
    }

    public ArrayList<PlanetDefense> findByPlanetId(int planetId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(planetId);
        return find(pd);
    }
    
    public ArrayList<PlanetDefense> findBySystemId(int systemId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(0);
        pd.setSystemId(systemId);
        return find(pd);
    }    

    public ArrayList<PlanetDefense> findByPlanetAndUserId(int planetId, int userId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setUserId(userId);
        pd.setPlanetId(planetId);
        return find(pd);
    }
    
    public ArrayList<PlanetDefense> findBySystemAndUserId(int systemId, int userId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setUserId(userId);
        pd.setPlanetId(0);
        pd.setSystemId(systemId);
        return find(pd);
    }        

    public PlanetDefense findBy(int planetId, int unitId, EDefenseType type) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(planetId);
        pd.setUnitId(unitId);
        pd.setType(type);
        ArrayList<PlanetDefense> result = find(pd);
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }
    public PlanetDefense findBy(int planetId, int unitId, int userId, EDefenseType type) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(planetId);
        pd.setUnitId(unitId);
        pd.setType(type);
        pd.setUserId(userId);
        ArrayList<PlanetDefense> result = find(pd);
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }
    
    public ArrayList<PlanetDefense> findByPlanet(int planetId, EDefenseType type) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(planetId);
        pd.setType(type);
        return find(pd);
    }
    public ArrayList<PlanetDefense> findBySystem(int systemId, EDefenseType type) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(0);
        pd.setType(type);
        pd.setSystemId(systemId);
        return find(pd);
    }
}
