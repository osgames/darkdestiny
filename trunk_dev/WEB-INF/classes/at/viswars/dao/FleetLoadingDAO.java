/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.FleetLoading;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class FleetLoadingDAO extends ReadWriteTable<FleetLoading> implements GenericDAO {

    public ArrayList<FleetLoading> findByFleetId(Integer fleetId) {
        FleetLoading flSearch = new FleetLoading();
        flSearch.setFleetId(fleetId);
        return find(flSearch);
    }

    public FleetLoading getByFleetAndFreight(int fleetId, int loadtype, int id) {
        FleetLoading flSearch = new FleetLoading();
        flSearch.setFleetId(fleetId);
        flSearch.setLoadType(loadtype);
        flSearch.setId(id);
        return  (FleetLoading)get(flSearch);
    }
    
    public void deleteByFleetAndFreight(int fleetId, int loadtype, int id) {
        FleetLoading flDel = new FleetLoading();
        flDel.setFleetId(fleetId);
        flDel.setLoadType(loadtype);
        flDel.setId(id);
        remove(flDel);
    }    

    public FleetLoading setNewLoadingAmount(int fleetId, int loadtype, int ressId, int amount) {
        FleetLoading flSearch = new FleetLoading();
        flSearch.setFleetId(fleetId);
        flSearch.setLoadType(loadtype);
        flSearch.setId(ressId);
        ArrayList<FleetLoading> result = this.find(flSearch);

        if (result.size() == 0) {
            if (amount == 0) return null;

            // Create Entry
            FleetLoading flNew = new FleetLoading();
            flNew.setFleetId(fleetId);
            flNew.setLoadType(loadtype);
            flNew.setId(ressId);
            flNew.setCount(amount);
            return add(flNew);
        } else {
            if (amount == 0) {
                FleetLoading actEntry = result.get(0);
                remove(actEntry);
                return null;
            }

            // Adjust Entry
            FleetLoading actEntry = result.get(0);
            actEntry.setCount(amount);
            return update(actEntry);
        }
    }

    public FleetLoading changeLoadingAmount(int fleetId, int loadtype, int ressId, int amount) {
        FleetLoading flSearch = new FleetLoading();
        flSearch.setFleetId(fleetId);
        flSearch.setLoadType(loadtype);
        flSearch.setId(ressId);
        ArrayList<FleetLoading> result = this.find(flSearch);

        if (result.size() == 0) {
            // Create Entry
            FleetLoading flNew = new FleetLoading();
            flNew.setFleetId(fleetId);
            flNew.setLoadType(loadtype);
            flNew.setId(ressId);
            flNew.setCount(amount);
            return add(flNew);
        } else {
            // Adjust Entry
            FleetLoading actEntry = result.get(0);
            actEntry.setCount(actEntry.getCount() + amount);
            return update(actEntry);
        }  
    }
}
