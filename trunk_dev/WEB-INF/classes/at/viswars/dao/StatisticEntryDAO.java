/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.StatisticEntry;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class StatisticEntryDAO  extends ReadWriteTable<StatisticEntry>  implements GenericDAO{

    public StatisticEntry findById(int id){
        StatisticEntry se = new StatisticEntry();
        se.setId(id);
        return (StatisticEntry)get(se);
    }

    public ArrayList<StatisticEntry>findByCategoryId(int categoryId){
        StatisticEntry se = new StatisticEntry();
        se.setCategoryId(categoryId);
        return find(se);
    }
}
