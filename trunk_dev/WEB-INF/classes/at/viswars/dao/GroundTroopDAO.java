/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.GroundTroop;
import at.viswars.model.Research;

/**
 *
 * @author Eobane
 */
public class GroundTroopDAO extends ReadOnlyTable<GroundTroop> implements GenericDAO {

    public static PlayerResearchDAO playerResearchDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);

    public GroundTroop findById(Integer id, Integer userId) {
        GroundTroop gtTmp = new GroundTroop();
        gtTmp.setId(id);
        GroundTroop gt = (GroundTroop) get(gtTmp);
        if (userId > 0) {
            if (gt.getId() == GroundTroop.ID_LIGHTINFANTRY || gt.getId() == GroundTroop.ID_HEAVYINFANTRY) {
                if (playerResearchDAO.findBy(userId, Research.KAMPFANZUG) != null) {
                    gt.setHitpoints(gt.getHitpoints() + 1);
                }
                if (playerResearchDAO.findBy(userId, Research.SERUN) != null) {
                    gt.setHitpoints(gt.getHitpoints() + 1);
                }
            }
        }
        return gt;
    }

    public GroundTroop findById(Integer id) {
        GroundTroop gt = new GroundTroop();
        gt.setId(id);
        return (GroundTroop) get(gt);
    }
}
