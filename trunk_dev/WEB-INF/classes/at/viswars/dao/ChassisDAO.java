/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Chassis;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class ChassisDAO extends ReadWriteTable<Chassis> implements GenericDAO  {
    public Chassis findById(int id) {
        Chassis c = new Chassis();
        c.setId(id);
        return (Chassis)get(c);
    }       
    
    public Chassis findByRelModuleId(int relModuleId) {
        Chassis c = new Chassis();      
        c.setRelModuleId(relModuleId);
        ArrayList<Chassis> result = find(c);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }
    
    public ArrayList<Chassis> findAllOrdered() {
        ArrayList<Chassis> result = new ArrayList<Chassis>();

        TreeMap<Integer, Chassis> sorted = new TreeMap<Integer, Chassis>();
        for(Chassis c : (ArrayList<Chassis>)findAll()){
            sorted.put(c.getId(), c);
        }
        result.addAll(sorted.values());
        return result;
    }
}
