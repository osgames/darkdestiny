/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;

import at.viswars.model.DiplomacyRelation;
import at.viswars.enumeration.EDiplomacyRelationType;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class DiplomacyRelationDAO extends ReadWriteTable<DiplomacyRelation> implements GenericDAO {

    public ArrayList<DiplomacyRelation> findBy(int fromId, int toId, EDiplomacyRelationType type) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setFromId(fromId);
        dr.setToId(toId);
        dr.setType(type);
        return (find(dr));
    }

    public ArrayList<DiplomacyRelation> findByToId(int toId, EDiplomacyRelationType type) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setToId(toId);
        dr.setType(type);
        return (find(dr));
    }
    public ArrayList<DiplomacyRelation> findByType(EDiplomacyRelationType type) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setType(type);
        return (find(dr));
    }
 public ArrayList<DiplomacyRelation> findByToId(int toId, EDiplomacyRelationType type, boolean pending) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setToId(toId);
        dr.setType(type);
        dr.setPending(pending);
        return (find(dr));
    }
    public ArrayList<DiplomacyRelation> findByFromId(int fromId, EDiplomacyRelationType type) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setFromId(fromId);
        dr.setType(type);
        return (find(dr));
    }

    public ArrayList<DiplomacyRelation> findByFromId(int fromId, EDiplomacyRelationType type, boolean pending) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setFromId(fromId);
        dr.setType(type);
        dr.setPending(pending);

        return find(dr);
    }
    public DiplomacyRelation findBy(int fromId, int toId, EDiplomacyRelationType type, boolean pending) {
        DiplomacyRelation dr = new DiplomacyRelation();
        dr.setFromId(fromId);
        dr.setToId(toId);
        dr.setType(type);
        dr.setPending(pending);
        ArrayList<DiplomacyRelation> result = new ArrayList<DiplomacyRelation>();
        result = find(dr);
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
