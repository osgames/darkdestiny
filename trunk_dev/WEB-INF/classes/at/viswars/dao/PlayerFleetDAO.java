/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.ships.ShipUtilities;
import at.viswars.database.framework.ReadWriteTable;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.ELocationType;
import at.viswars.model.FleetFormation;
import at.viswars.model.PlayerFleet;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlayerFleetDAO extends ReadWriteTable<PlayerFleet> implements GenericDAO {

    public PlayerFleet findById(int id) {
        PlayerFleet pf = new PlayerFleet();
        pf.setId(id);
        return (PlayerFleet)get(pf);
    }

    public Iterable<PlayerFleet> findByUserId(int userId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setUserId(userId);
        return find(pf);
    }
    public ArrayList<PlayerFleet> findByPlanetId(int planetId) {
        PlayerFleet pf = new PlayerFleet();        
        pf.setPlanetId(planetId);
        return find(pf);
    }
    public ArrayList<PlayerFleet> findByUserIdPlanetId(int userId, int planetId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setUserId(userId);
        pf.setPlanetId(planetId);
        return find(pf);
    }
    public ArrayList<PlayerFleet> findBySystemId(int systemId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setSystemId(systemId);
        return find(pf);
    }
    
    @Deprecated
    public ArrayList<PlayerFleet> findBy(int planetId, int systemId) {
        PlayerFleet pf = new PlayerFleet();
        
        pf.setPlanetId(planetId);
        pf.setSystemId(systemId);

        return find(pf);
    }

    // TODO dont use deprecated function
    public ArrayList<PlayerFleet> findBy(RelativeCoordinate rc) {
        return findBy(rc.getPlanetId(),rc.getSystemId());
    }

    @Deprecated
    public ArrayList<PlayerFleet> findBy(int planetId, int systemId, int userId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setPlanetId(planetId);
        pf.setSystemId(systemId);
        pf.setUserId(userId);
        return find(pf);
    }
    public ArrayList<PlayerFleet> findByFleetFormation(int formationId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setFleetFormationId(formationId);
        return find(pf);
    }

    
    public PlayerFleet createNewFleet(String name, Integer userId, Integer systemId, Integer planetId) {
        int fleetId = ShipUtilities.getFreeFleetId();        
        String fleetName = name.replace("%ID%", String.valueOf(fleetId));
        
        PlayerFleet pf = new PlayerFleet();
        pf.setId(fleetId);
        pf.setName(fleetName);
        pf.setUserId(userId);        
        pf.setPlanetId(planetId);
        pf.setSystemId(systemId);
        pf.setStatus(0);
        
        return add(pf);
    }
    
    public ArrayList<PlayerFleet> getParticipatingFleets(FleetFormation ff) {
        PlayerFleet pfSearch = new PlayerFleet();
        pfSearch.setFleetFormationId(ff.getId());
        return find(pfSearch);
    }
}
