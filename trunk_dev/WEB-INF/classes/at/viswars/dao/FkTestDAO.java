/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.FkTest;

/**
 *
 * @author Aion
 */
public class FkTestDAO extends ReadWriteTable<FkTest> implements GenericDAO {
    public FkTest findById(int id){
        FkTest f = new FkTest();
        f.setId(id);
        return (FkTest)get(f);
    }
}
