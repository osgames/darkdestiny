/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.SizeRelation;

/**
 *
 * @author Stefan
 */
public class SizeRelationDAO extends ReadOnlyTable<SizeRelation> implements GenericDAO<SizeRelation> {
    public SizeRelation getByRelModuleId(int id) {
        SizeRelation sr = new SizeRelation();
        sr.setChassisId(id);
        return get(sr);
    }
}
