/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Condition;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ConditionDAO extends ReadWriteTable<Condition> implements GenericDAO {

    public Condition findById(int id) {
        Condition titleCondition = new Condition();
        titleCondition.setId(id);
        return(Condition)get(titleCondition);
    }

}
