/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.QueryKey;
import at.viswars.database.framework.QueryKeySet;
import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.model.Ressource;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "ressource")
public class RessourceDAO extends ReadOnlyTable<Ressource> implements GenericDAO {

    private RessourceDAO() {
    }

    public ArrayList<Ressource> findAllDisplayableRessources() {
        ArrayList<Ressource> tmp = findAll();
        ArrayList<Ressource> result = new ArrayList<Ressource>();
        for (Ressource r : tmp) {
            if (r.getDisplayLocation() >= 0){
                result.add(r);
            }
        }
        return result;
    }

    public Ressource findRessourceById(Integer id) {
        Ressource r = new Ressource();
        r.setId(id);
        return get(r);
    }    
    
    public ArrayList<Ressource> findAllStoreableRessources() {

        ArrayList<Ressource> tmp = findAll();
        ArrayList<Ressource> result = new ArrayList<Ressource>();
        for (Ressource r : tmp) {
            //Skip cv embinium
            // if(r.getId() == 6)continue;
            if (!r.getMineable() && r.getTransportable()) {
                result.add(r);
            }
        }
        return result;

    }

    public ArrayList<Ressource> findAllMineableRessources() {

        ArrayList<Ressource> tmp = findAll();
        ArrayList<Ressource> result = new ArrayList<Ressource>();
        for (Ressource r : tmp) {
            if (r.getMineable()) {
                result.add(r);
            }
        }
        return result;
    }
    
    public ArrayList<Ressource> findAllPayableRessources() {
        Ressource r = new Ressource();
        r.setRessourcecost(true);
        return find(r);
    }    
}
