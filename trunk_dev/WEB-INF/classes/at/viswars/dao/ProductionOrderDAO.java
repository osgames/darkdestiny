/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EProductionOrderType;
import at.viswars.model.Action;
import at.viswars.model.ProductionOrder;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class ProductionOrderDAO extends ReadWriteTable<ProductionOrder> implements GenericDAO {
    public ProductionOrder findById(Integer id) {
        ProductionOrder po = new ProductionOrder();
        po.setId(id);
        return (ProductionOrder)get(po);

    }

    public ArrayList<ProductionOrder> findByActionList(ArrayList<Action> actions) {
        TreeMap<Integer, ProductionOrder> tmpResult = new TreeMap<Integer, ProductionOrder>();
        for (Action a : actions) {
            ProductionOrder po = findByConstructionOrderById(a.getConstructionId());
            tmpResult.put(po.getPriority(), po);
        }
        return (ArrayList<ProductionOrder>) tmpResult.values();
    }

    public ProductionOrder findByAction(Action a) {
        ProductionOrder po = new ProductionOrder();
        po.setId(a.getRefTableId());
        
        ArrayList<ProductionOrder> result = find(po);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }
    
    public ProductionOrder findByConstructionOrderById(Integer id) {
        ProductionOrder po = new ProductionOrder();
        po.setId(id);
        po.setType(EProductionOrderType.C_BUILD);
        ArrayList<ProductionOrder> result = find(po);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
