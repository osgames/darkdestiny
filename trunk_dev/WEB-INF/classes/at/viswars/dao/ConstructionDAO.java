/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EConstructionType;
import at.viswars.model.Construction;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class ConstructionDAO extends ReadWriteTable<Construction> implements GenericDAO {

    private ConstructionDAO() {
    }

    public ArrayList<Construction> findByType(EConstructionType type) {
        Construction c = new Construction();
        c.setType(type);
        ArrayList<Construction> result = find(c);

        return result;
    }

    public Construction findById(Integer constructionId) {
        Construction c = new Construction();
        c.setId(constructionId);
        return (Construction)get(c);
    }

}
