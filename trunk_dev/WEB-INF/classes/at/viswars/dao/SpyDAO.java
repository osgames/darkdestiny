/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Spy;

/**
 *
 * @author Stefan
 */
public class SpyDAO extends ReadWriteTable<Spy> implements GenericDAO {
    public Spy findById(int id) {
        Spy spy = new Spy();
        spy.setId(id);
        return (Spy)get(spy);
    }
}
