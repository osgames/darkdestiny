/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.ETerritoryMapShareType;
import at.viswars.enumeration.ETerritoryMapType;
import at.viswars.model.AllianceMember;
import at.viswars.model.TerritoryMapShare;
import at.viswars.model.User;
import at.viswars.service.Service;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Eobane
 */
public class TerritoryMapShareDAO extends ReadWriteTable<TerritoryMapShare> implements GenericDAO {

    public TerritoryMapShare findBy(Integer id, Integer refId, ETerritoryMapShareType type) {
        TerritoryMapShare tms = new TerritoryMapShare();
        tms.setRefId(refId);
        tms.setTerritoryId(id);
        tms.setType(type);
        return (TerritoryMapShare) get(tms);
    }

    public ArrayList<TerritoryMapShare> findBy(Integer refId, ETerritoryMapShareType type) {
        TerritoryMapShare tms = new TerritoryMapShare();
        tms.setRefId(refId);
        tms.setType(type);
        return (ArrayList<TerritoryMapShare>) find(tms);
    }

    public ArrayList<TerritoryMapShare> findByTerritoryIdSorted(Integer territoryId) {
        ArrayList<TerritoryMapShare> result = new ArrayList<TerritoryMapShare>();
        TerritoryMapShare tms = new TerritoryMapShare();
        tms.setTerritoryId(territoryId);
        for(ETerritoryMapShareType tmst : ETerritoryMapShareType.values()){
            tms.setType(tmst);
            result.addAll((ArrayList<TerritoryMapShare>) find(tms));
        }
        return result;
    }
    
    public ArrayList<TerritoryMapShare> findByTerritoryId(Integer territoryId, ETerritoryMapShareType type) {
        TerritoryMapShare tms = new TerritoryMapShare();
        tms.setTerritoryId(territoryId);
        tms.setType(type);
        return (ArrayList<TerritoryMapShare>) find(tms);
    }
    public ArrayList<TerritoryMapShare> findByUserId(Integer userId) {

        ArrayList<TerritoryMapShare> result = new ArrayList<TerritoryMapShare>();

        HashSet<Integer> addedTerritories = new HashSet<Integer>();
        //Search by USER
        {
            TerritoryMapShare tms = new TerritoryMapShare();
            tms.setRefId(userId);
            tms.setType(ETerritoryMapShareType.USER);
            for (TerritoryMapShare tms_tmp : (ArrayList<TerritoryMapShare>) find(tms)) {
                if (!addedTerritories.contains(tms_tmp.getTerritoryId())) {
                    addedTerritories.add(tms_tmp.getTerritoryId());
                    result.add(tms_tmp);
                }
            }
        }
        //Search by USER_BY_ALLIANCE
        {
            TerritoryMapShare tms = new TerritoryMapShare();
            tms.setRefId(userId);
            tms.setType(ETerritoryMapShareType.USER_BY_ALLIANCE);
            for (TerritoryMapShare tms_tmp : (ArrayList<TerritoryMapShare>) find(tms)) {
                if (!addedTerritories.contains(tms_tmp.getTerritoryId())) {
                    addedTerritories.add(tms_tmp.getTerritoryId());
                    result.add(tms_tmp);
                }
            }
        }
        //Search by USER_BY_ALL
        {
            TerritoryMapShare tms = new TerritoryMapShare();
            tms.setRefId(userId);
            tms.setType(ETerritoryMapShareType.USER_BY_ALL);
            for (TerritoryMapShare tms_tmp : (ArrayList<TerritoryMapShare>) find(tms)) {
                if (!addedTerritories.contains(tms_tmp.getTerritoryId())) {
                    addedTerritories.add(tms_tmp.getTerritoryId());
                    result.add(tms_tmp);
                }
            }
        }
        return result;
    }

    public ArrayList<User> findUsersByTerritoryIdWithAlliance(Integer territoryId) {

        ArrayList<User> result = new ArrayList<User>();

        TerritoryMapShare tmsSearch = new TerritoryMapShare();
        tmsSearch.setTerritoryId(territoryId);
        HashSet<Integer> usersAdded = new HashSet<Integer>();
        for (TerritoryMapShare tms : (ArrayList<TerritoryMapShare>) find(tmsSearch)) {
            if (tms.getType().equals(ETerritoryMapShareType.USER) || tms.getType().equals(ETerritoryMapShareType.USER_BY_ALL) || tms.getType().equals(ETerritoryMapShareType.USER_BY_ALLIANCE)) {
                if (!usersAdded.contains(tms.getRefId())) {
                    User u = Service.userDAO.findById(tms.getRefId());
                    result.add(u);
                    usersAdded.add(tms.getRefId());
                }
            }
        }

        return result;
    }

    public ArrayList<TerritoryMapShare> findByTerritoryId(Integer territoryId) {
        TerritoryMapShare tms = new TerritoryMapShare();
        tms.setTerritoryId(territoryId);
        return find(tms);
    }
}
