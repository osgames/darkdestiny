/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.WeaponFactor;

/**
 *
 * @author Stefan
 */
public class WeaponFactorDAO extends ReadOnlyTable<WeaponFactor> implements GenericDAO {
    public WeaponFactor findByWeaponId(int weaponId) {
        WeaponFactor wf = new WeaponFactor();
        wf.setWeaponId(weaponId);
        return get(wf);
    }
}
