/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.VoteMetadata;
import java.util.ArrayList;
/**
 *
 * @author Bullet
 */
public class VoteMetadataDAO extends ReadWriteTable<VoteMetadata> implements GenericDAO {
       public ArrayList<VoteMetadata> findByVoteId(int voteId) {
           VoteMetadata vm = new VoteMetadata();
           vm.setVoteId(voteId);
           return find(vm);
    }
}
