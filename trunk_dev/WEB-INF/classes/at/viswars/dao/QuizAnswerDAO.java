/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.QuizAnswer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class QuizAnswerDAO extends ReadWriteTable<QuizAnswer> implements GenericDAO  {

    public ArrayList<QuizAnswer> findByQuizEntryId(Integer id) {
        
        QuizAnswer qa = new QuizAnswer();
        qa.setQuizEntryId(id);
        
        return (ArrayList<QuizAnswer>)find(qa);
    }

 
}
