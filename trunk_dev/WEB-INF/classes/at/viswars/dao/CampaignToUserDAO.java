/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.CampaignToUser;
import at.viswars.model.Chassis;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class CampaignToUserDAO extends ReadWriteTable<CampaignToUser> implements GenericDAO  {
    public CampaignToUser findBy(int userId, int campaignId) {
        CampaignToUser c = new CampaignToUser();
        c.setUserId(userId);
        c.setCampaignId(campaignId);
        return (CampaignToUser)get(c);
    }       
    
}
