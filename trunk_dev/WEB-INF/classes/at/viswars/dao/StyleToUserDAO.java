/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.StyleToUser;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class StyleToUserDAO extends ReadWriteTable<StyleToUser> implements GenericDAO {

    public ArrayList<StyleToUser> findByUserId(int userId) {
        StyleToUser stu = new StyleToUser();
        stu.setUserId(userId);
        return find(stu);
    }

    public StyleToUser findBy(int userId, int styleId) {
        StyleToUser stu = new StyleToUser();
        stu.setUserId(userId);
        stu.setStyleId(styleId);
        return (StyleToUser) get(stu);
    }
}
