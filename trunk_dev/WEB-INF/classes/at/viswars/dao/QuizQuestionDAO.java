/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.QuizQuestion;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class QuizQuestionDAO extends ReadWriteTable<QuizQuestion> implements GenericDAO  {

 
    public ArrayList<QuizQuestion> findByQuizEntryId(Integer id) {
        
        QuizQuestion qq = new QuizQuestion();
        qq.setQuizEntryId(id);
        
        return (ArrayList<QuizQuestion>)find(qq);
    }
}
