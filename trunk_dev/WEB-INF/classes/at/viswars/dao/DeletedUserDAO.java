/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.DeletedUser;

/**
 *
 * @author Aion
 */
public class DeletedUserDAO extends ReadWriteTable<DeletedUser> implements GenericDAO {
    public DeletedUser findByEmail(String email) {
        DeletedUser du = new DeletedUser();
        du.setEmail(email);
        return (DeletedUser)get(du);
    }
}
