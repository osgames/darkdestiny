/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.model.PlanetRessource;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlanetRessourceDAO extends ReadWriteTable<PlanetRessource> implements GenericDAO {

    public ArrayList<PlanetRessource> findByPlanetId(Integer planetId) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        return find(pr);
    }

    public PlanetRessource findBy(Integer planetId, Integer ressourceId, EPlanetRessourceType type) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setRessId(ressourceId);
        pr.setType(type);

        return (PlanetRessource) get(pr);
    }

    public PlanetRessource create(PlanetRessource u) {
        PlanetRessource uNew = this.add(u);
        return uNew;
    }

    public PlanetRessource updateRessource(PlanetRessource u) {
        PlanetRessource uNew = this.update(u);
        return uNew;
    }

    public ArrayList<PlanetRessource> findMineableRessources(int planetId) {

        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setType(EPlanetRessourceType.PLANET);

        return find(pr);
    }

    public PlanetRessource findMinStockProduction(int ressourceId, int planetId) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setRessId(ressourceId);
        pr.setType(EPlanetRessourceType.MINIMUM_PRODUCTION);

        ArrayList<PlanetRessource> result = find(pr);

        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<PlanetRessource> findBy(int planetId, EPlanetRessourceType ePlanetRessourceType) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setType(ePlanetRessourceType);

        return find(pr);

    }
}
