/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.TerritoryEntry;
import java.util.ArrayList;
/**
 *
 * @author Aion
 */
public class TerritoryEntryDAO extends ReadWriteTable<TerritoryEntry> implements GenericDAO {

    public TerritoryEntry findBy(Integer userId, Integer combatRoundId) {
        TerritoryEntry te = new TerritoryEntry();
        te.setUserId(userId);
        te.setCombatRoundId(combatRoundId);
        return (TerritoryEntry)get(te);
    }

    public ArrayList<TerritoryEntry> findByCombatRoundId(Integer combatRoundId) {
        TerritoryEntry te = new TerritoryEntry();
        te.setCombatRoundId(combatRoundId);
        return find(te);

    }

}
