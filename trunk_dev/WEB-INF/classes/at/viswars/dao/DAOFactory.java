/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.DebugBuffer;
import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.database.framework.ReadOnlyTable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Eobane
 */
public class DAOFactory {

    private static HashMap<Class, GenericDAO> instanceList = new HashMap<Class, GenericDAO>();

    public static <T> T get(Class clazz) {
        if (!instanceList.containsKey(clazz)) {
            try {

                GenericDAO genericDAO = null;
                Constructor constructor;
                constructor = clazz.getDeclaredConstructor();
                constructor.setAccessible(true);
                genericDAO = (GenericDAO) constructor.newInstance();
                instanceList.put(clazz, genericDAO);

                return (T) genericDAO;
            } catch (NoSuchMethodException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            } catch (SecurityException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            } catch (InstantiationException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            } catch (IllegalAccessException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            } catch (InvocationTargetException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            }
        } else {
            return (T) instanceList.get(clazz);
        }

        return null;
    }

    public static void dropAll() {
        for (GenericDAO gDAO : instanceList.values()) {
            Class clazz = gDAO.getClass();
            try {
                Class clazz2 = clazz;
                while (!(clazz2.getName().equals(ReadOnlyTable.class.getName()))) {
                    clazz2 = clazz2.getSuperclass();
                }

                Method reloadMethod = clazz2.getDeclaredMethod("reloadDataSource");
                reloadMethod.setAccessible(true);
                reloadMethod.invoke(gDAO);
            } catch (NoSuchMethodException nsme) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", nsme);
                Logger.getLogger().write("Method reloadDataSource not found for " + clazz);
                nsme.printStackTrace();
            } catch (IllegalAccessException iae) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", iae);
                iae.printStackTrace();
            } catch (InvocationTargetException ite) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", ite);
                ite.printStackTrace();
            }
        }

        System.gc();
    }

    public static ArrayList<Exception> gatherAllLocks() {
        ArrayList<Exception> eList = new ArrayList<Exception>();

        for (Map.Entry<Class, GenericDAO> daoEntry : instanceList.entrySet()) {
            Exception e = daoEntry.getValue().getWriteLock();
            if (e != null) {
                eList.add(e);
            } else {
                Logger.getLogger().write(LogLevel.INFO, "No lock in " + daoEntry.getValue());
            }
        }

        return eList;
    }
}
