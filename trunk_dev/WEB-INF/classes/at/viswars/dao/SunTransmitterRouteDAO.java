/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.SunTransmitterRoute;

/**
 *
 * @author Stefan
 */
public class SunTransmitterRouteDAO extends ReadWriteTable<SunTransmitterRoute> implements GenericDAO  {
    public SunTransmitterRoute findById(int id) {
        SunTransmitterRoute str = new SunTransmitterRoute();
        str.setId(id);
        return (SunTransmitterRoute)get(str);
    }       
}
