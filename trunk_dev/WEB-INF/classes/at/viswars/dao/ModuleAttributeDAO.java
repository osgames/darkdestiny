/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.Logger.Logger;
import at.viswars.Logger.Logger.LogLevel;
import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.ModuleAttribute;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class ModuleAttributeDAO extends ReadWriteTable<ModuleAttribute> implements GenericDAO {
    public ModuleAttribute findById(Integer id) {
        ModuleAttribute ma = new ModuleAttribute();
        ma.setId(id);
        return (ModuleAttribute)get(ma);
    }
    public ArrayList<ModuleAttribute> findBy(Integer moduleId, Integer chassisId) {
        Logger.getLogger().write(LogLevel.DEBUG,"LOOKING FOR " + moduleId + " and chassis " + chassisId);

        ModuleAttribute ma = new ModuleAttribute();
        ma.setModuleId(moduleId);
        ma.setChassisId(chassisId);
        
        return find(ma);
    }
    
    public ArrayList<ModuleAttribute> findValidForChassis(Integer chassisId) {
        ModuleAttribute ma = new ModuleAttribute();
        ma.setChassisId(chassisId);

        ModuleAttribute ma2 = new ModuleAttribute();
        ma2.setChassisId(0);
        
        ArrayList<ModuleAttribute> result = new ArrayList<ModuleAttribute>();
                
        ArrayList<ModuleAttribute> res1 = find(ma);               
        ArrayList<ModuleAttribute> res2 = find(ma2); 

        if (res1 != null) result.addAll(res1);
        if (res2 != null) result.addAll(res2);
        
        return result;        
    }        
    
    public ArrayList<ModuleAttribute> findByChassisAndModule(Integer chassisId, Integer moduleId) {
        ModuleAttribute ma = new ModuleAttribute();
        ma.setChassisId(chassisId);
        ma.setModuleId(moduleId);

        return find(ma);        
    }    
    
    public ArrayList<ModuleAttribute> findByModuleId(Integer moduleId) {
        ModuleAttribute ma = new ModuleAttribute();
        ma.setModuleId(moduleId);
        return find(ma);
    }
    
    public ArrayList<ModuleAttribute> findByAppliedToId(Integer moduleAttributeId) {
        ModuleAttribute ma = new ModuleAttribute();
        ma.setAppliesTo(moduleAttributeId);
        return find(ma);
    }    
}
