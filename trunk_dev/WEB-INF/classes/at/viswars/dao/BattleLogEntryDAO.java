/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.BattleLogEntry;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class BattleLogEntryDAO extends ReadWriteTable<BattleLogEntry> implements GenericDAO  {


    public BattleLogEntry findBy(Integer id, Integer designId){
       BattleLogEntry bl = new BattleLogEntry();
       bl.setId(id);
       bl.setDesignId(designId);
       return (BattleLogEntry)get(bl);
    }
    public ArrayList<BattleLogEntry> findById(Integer id){
       BattleLogEntry bl = new BattleLogEntry();
       bl.setId(id);
       return find(bl);
    }
}
