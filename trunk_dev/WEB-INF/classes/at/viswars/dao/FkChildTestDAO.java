/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Filter;
import at.viswars.model.FilterToValue;
import at.viswars.model.FkChildTest;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class FkChildTestDAO extends ReadWriteTable<FkChildTest> implements GenericDAO {
    public FkChildTest findById(int filterId){
        FkChildTest f = new FkChildTest();
        f.setId(filterId);
        return (FkChildTest)get(f);
    }
}
