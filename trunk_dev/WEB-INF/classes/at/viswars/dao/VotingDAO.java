/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.Voting;
import java.util.ArrayList;
/**
 *
 * @author Bullet
 */
public class VotingDAO extends ReadWriteTable<Voting> implements GenericDAO {
    public ArrayList<Voting> findByType(int type) {
           Voting v = new Voting();
           v.setType(type);
           return find(v);
    }
       
    public Voting findById(int id) {
        Voting v = new Voting();
        v.setVoteId(id);
        return (Voting)get(v);
    }

    public ArrayList<Voting> findAllOpenVotes() {
        Voting v = new Voting();
        v.setClosed(false);
        return find(v);
    }

    public ArrayList<Voting> findAllOpenVotesByType(int type) {
        Voting v = new Voting();
        v.setClosed(false);
        v.setType(type);
        return find(v);
    }
}
