/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.TechType;

/**
 *
 * @author Bullet
 */
public class TechTypeDAO extends ReadOnlyTable<TechType> implements GenericDAO {

    public TechType findById(Integer id) {
        TechType tt = new TechType();
        tt.setId(id);
        return (TechType) get(tt);
    }
}
