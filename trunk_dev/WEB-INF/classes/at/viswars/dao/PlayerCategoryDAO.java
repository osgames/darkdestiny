/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.PlayerCategory;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlayerCategoryDAO extends ReadWriteTable<PlayerCategory> implements GenericDAO {

    public ArrayList<PlayerCategory> findByUserId(int userId) {
        PlayerCategory pc = new PlayerCategory();
        pc.setUserId(userId);
        return find(pc);
    }

    public PlayerCategory findById(int id) {
        PlayerCategory pc = new PlayerCategory();
        pc.setId(id);
        return (PlayerCategory) get(pc);
    }

    public PlayerCategory findBy(int userId, int id) {
        PlayerCategory pc = new PlayerCategory();
        pc.setId(id);
        pc.setUserId(userId);
        ArrayList<PlayerCategory> result = find(pc);
        if (result.size() == 0) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
