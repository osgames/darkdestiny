/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.TechRelation;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TechRelationDAO extends ReadOnlyTable<TechRelation> implements GenericDAO {

    public ArrayList<TechRelation> findByConstructionId(Integer constructionId) {
        TechRelation tr = new TechRelation();
        tr.setSourceId(constructionId);
        tr.setTreeType(TechRelation.TREETYPE_BUILD);
        return find(tr);
    }

    public ArrayList<TechRelation> findByGroundTroopId(Integer groundtroopId) {
        TechRelation tr = new TechRelation();
        tr.setSourceId(groundtroopId);
        tr.setTreeType(TechRelation.TREETYPE_GROUNDTROOPS);
        return find(tr);
    }

    public ArrayList<TechRelation> findByModuleId(Integer moduleId) {
        TechRelation tr = new TechRelation();
        tr.setSourceId(moduleId);
        tr.setTreeType(TechRelation.TREETYPE_MODULE);
        return find(tr);
    }

    public ArrayList<TechRelation> findByResearchId(Integer researchId) {
        TechRelation tr = new TechRelation();
        tr.setSourceId(researchId);
        tr.setTreeType(TechRelation.TREETYPE_RESEARCH);
        return find(tr);
    }
    public ArrayList<TechRelation> findBy(Integer sourceId, Integer treeType) {
        TechRelation tr = new TechRelation();
        tr.setSourceId(sourceId);
        tr.setTreeType(treeType);
        return find(tr);
    }

    public ArrayList<TechRelation> findByRequiredConstructionId(Integer reqConstructionId) {
        TechRelation tr = new TechRelation();
        tr.setReqConstructionId(reqConstructionId);
        return find(tr);
    }

    public ArrayList<TechRelation> findByRequiredResearchId(Integer reqResearchId) {
        TechRelation tr = new TechRelation();
        tr.setReqResearchId(reqResearchId);
        return find(tr);
    }
}
