/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.TradeRouteDetail;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TradeRouteDetailDAO extends ReadWriteTable<TradeRouteDetail> implements GenericDAO {
    public TradeRouteDetail getById(Integer detailId) {
        TradeRouteDetail trd = new TradeRouteDetail();
        trd.setId(detailId);
        return (TradeRouteDetail)get(trd);
    }
    
    public ArrayList<TradeRouteDetail> getByRouteId(int routeId) {
        TradeRouteDetail trd = new TradeRouteDetail();
        trd.setRouteId(routeId);
        return find(trd);
    }
    
    public ArrayList<TradeRouteDetail> getActiveByRouteId(int routeId) {
        TradeRouteDetail trd = new TradeRouteDetail();
        trd.setRouteId(routeId);
        trd.setDisabled(false);
        return find(trd);
    }    
    
    public ArrayList<TradeRouteDetail> getInactiveRoutes() {
        TradeRouteDetail trd = new TradeRouteDetail();
        trd.setDisabled(true);
        return find(trd);
    }
    public TradeRouteDetail findBy(int routeId, int ressourceId, boolean reversed) {
        TradeRouteDetail trd = new TradeRouteDetail();
        trd.setRouteId(routeId);
        trd.setRessId(ressourceId);
        trd.setReversed(reversed);
        ArrayList<TradeRouteDetail> result = find(trd);
        if(!result.isEmpty()){
            return result.get(0);
        }else{
            return null;
        }
    }
}
