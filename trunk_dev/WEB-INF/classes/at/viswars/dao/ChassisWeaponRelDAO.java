/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadOnlyTable;
import at.viswars.model.ChassisWeaponRel;

/**
 *
 * @author Stefan
 */
public class ChassisWeaponRelDAO extends ReadOnlyTable<ChassisWeaponRel> implements GenericDAO {
    public ChassisWeaponRel getByChassisAndModule(int chassisId, int moduleId) {
        ChassisWeaponRel cwr = new ChassisWeaponRel();
        cwr.setChassisId(chassisId);
        cwr.setModuleId(moduleId);
        return get(cwr);
    }
}
