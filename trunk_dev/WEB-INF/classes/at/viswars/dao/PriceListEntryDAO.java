/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.dao;

import at.viswars.database.framework.ReadWriteTable;
import at.viswars.model.PriceListEntry;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class PriceListEntryDAO extends ReadWriteTable<PriceListEntry> implements GenericDAO {
    public PriceListEntry getById(int id) {
        PriceListEntry ple = new PriceListEntry();
        ple.setId(id);
        return (PriceListEntry)get(ple);
    }

    public ArrayList<PriceListEntry> findByPriceList(int id) {
        PriceListEntry ple = new PriceListEntry();
        ple.setPriceListId(id);
        return find(ple);
    }

    public PriceListEntry findByPriceListAndRess(int id, int ressId) {
        PriceListEntry ple = new PriceListEntry();
        ple.setPriceListId(id);
        ple.setRessId(ressId);

        ArrayList<PriceListEntry> result = find(ple);
        if (!result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
