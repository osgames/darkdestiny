/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.diplomacy.combat;

import at.viswars.model.DiplomacyRelation;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class AllianceMemberEntry extends PlayerEntry {
    private final int allianceId;
    private AllianceEntry masterEntry = null;

    private HashMap<AllianceEntry,DiplomacyRelation> relationsToAlliance =
            new HashMap<AllianceEntry,DiplomacyRelation>();
    private HashMap<PlayerEntry,DiplomacyRelation> relationsToPlayer =
            new HashMap<PlayerEntry,DiplomacyRelation>();

    protected AllianceMemberEntry(int userId, int allianceId) {
        super(userId);
        this.allianceId = allianceId;
    }

    public int getAllianceId() {
        return allianceId;
    }

    public AllianceEntry getMasterEntry() {
        return masterEntry;
    }
}
