/* To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.diplomacy.combat;

import at.viswars.Logger.Logger;
import at.viswars.dao.*;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.ETerritoryType;
import at.viswars.model.*;
import at.viswars.utilities.DiplomacyUtilities;
import java.awt.Polygon;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.util.*;

/**
 *
 * @author Stefan
 */
public class CombatGroupResolver {

    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static DiplomacyTypeDAO dtDAO = (DiplomacyTypeDAO) DAOFactory.get(DiplomacyTypeDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static TerritoryMapDAO tmDAO = (TerritoryMapDAO) DAOFactory.get(TerritoryMapDAO.class);
    private static TerritoryMapPointDAO tmpDAO = (TerritoryMapPointDAO) DAOFactory.get(TerritoryMapPointDAO.class);

    public static DiplomacyGroupResult resolveUsersSimulation(ArrayList<Integer> userList, HashMap<Integer, ArrayList<Integer>> groups) {
        ArrayList<AllianceEntry> aeList = new ArrayList<AllianceEntry>();
        ArrayList<PlayerEntry> peList = new ArrayList<PlayerEntry>();

        for (Integer user : userList) {
            PlayerEntry pe = new PlayerEntry(user);
            peList.add(pe);
        }

        for (PlayerEntry pe : peList) {
            for (PlayerEntry pe2 : peList) {
                if (pe != pe2) {
                    pe.addPlayerRelation(pe, DiplomacyUtilities.getHostileRelation(pe.getUserId(), pe2.getUserId()));
                }
            }
        }

        return new DiplomacyGroupResult(aeList, peList);
    }

    public static DiplomacyGroupResult resolveUsers(ArrayList<Integer> userList) {
        ArrayList<AllianceEntry> aeList = new ArrayList<AllianceEntry>();
        ArrayList<PlayerEntry> peList = new ArrayList<PlayerEntry>();

        // **********************************************
        // STEP 1: Init all users
        // **********************************************
        for (Integer userId : userList) {
            AllianceMember am = amDAO.findByUserId(userId);

            if (am != null) {
                AllianceMemberEntry ame = new AllianceMemberEntry(am.getUserId(), am.getAllianceId());

                AllianceEntry ae = null;
                for (AllianceEntry aeTmp : aeList) {
                    if (aeTmp.getAllianceId() == ame.getAllianceId()) {
                        ae = aeTmp;
                        break;
                    }
                }

                if (ae == null) {
                    ae = new AllianceEntry(am.getAllianceId());
                    ae.addAllianceMember(ame);
                    aeList.add(ae);
                } else {
                    ae.addAllianceMember(ame);
                }
            } else {
                PlayerEntry pe = new PlayerEntry(userId);
                peList.add(pe);
            }
        }

        // **********************************************
        // STEP 2: Build alliance structures
        // **********************************************         
        ArrayList<AllianceEntry> newAllEntries = new ArrayList<AllianceEntry>();
        HashMap<AllianceEntry, ArrayList<AllianceEntry>> subAlliances = new HashMap<AllianceEntry, ArrayList<AllianceEntry>>();

        // for (AllianceEntry ae : aeList) {
        //     Logger.getLogger().write("BEFORE allianceEntry: " + ae.getAllianceId());
        // }

        for (Iterator<AllianceEntry> aeIt = aeList.iterator(); aeIt.hasNext();) {
            AllianceEntry aeAct = aeIt.next();

            Alliance a = aDAO.findById(aeAct.getAllianceId());
            // Logger.getLogger().write("Process alliance " + aeAct.getAllianceId() + " (MasterId: " + a.getMasterAlliance() + ")");

            if (a.getMasterAlliance() != a.getId()) {
                boolean found = false;

                ArrayList<AllianceEntry> aeListRecent = new ArrayList<AllianceEntry>();
                aeListRecent.addAll(aeList);
                aeListRecent.addAll(newAllEntries);

                AllianceEntry masterAe = null;

                for (AllianceEntry aeInner : aeListRecent) {
                    if (aeInner.getAllianceId() == a.getMasterAlliance()) {
                        aeAct.setMasterAlliance(aeInner);
                        masterAe = aeInner;
                        found = true;
                    }
                }

                if (!found) {
                    AllianceEntry ae = new AllianceEntry(a.getMasterAlliance());
                    newAllEntries.add(ae);
                    masterAe = ae;
                    aeAct.setMasterAlliance(ae);
                }

                if (!subAlliances.containsKey(masterAe)) {
                    ArrayList<AllianceEntry> aeListTmp = new ArrayList<AllianceEntry>();
                    aeListTmp.add(aeAct);
                    subAlliances.put(masterAe, aeListTmp);
                } else {
                    subAlliances.get(masterAe).add(aeAct);
                }

                // Move alliance members to master Alliance
                for (AllianceMemberEntry ameOld : aeAct.getMembers()) {
                    masterAe.addAllianceMember(ameOld);
                }

                aeIt.remove();
                continue;
            }
        }

        aeList.addAll(newAllEntries);

        // **********************************************
        // STEP 3: Set relations
        // **********************************************              
        // Process Alliances
        for (AllianceEntry ae : aeList) {
            for (AllianceEntry aeInner : aeList) {
                if (ae.getAllianceId() == aeInner.getAllianceId()) {
                    continue;
                }
                DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRelationAllianceToAlliance(ae.getAllianceId(), aeInner.getAllianceId());
                // Logger.getLogger().write("Diplomacy Relation between " + ae.getAllianceId() + " and " + aeInner.getAllianceId() + " is " + dr.getDiplomacyTypeId());

                ae.addAllianceRelation(aeInner, dr);

                // Alliance has no specific relation to other alliance
                // Accordingly set relations between users
                if (dr.getDiplomacyTypeId() == DiplomacyType.NEUTRAL) {
                    // Check for relations of Alliance AE to players of Alliance AEInner
                    for (AllianceMemberEntry ameOuter : aeInner.getMembers()) {
                        // Logger.getLogger().write("Check for relation of alliance " + ae.getAllianceId() + " to player " + pe.getUserId());
                        DiplomacyRelation drTmp = DiplomacyUtilities.getDiplomacyRelationAllianceToUser(ae.getAllianceId(), ameOuter.getUserId());
                        // Logger.getLogger().write("Found " + drTmp.getDiplomacyTypeId());
                        if (drTmp.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                            for (AllianceMemberEntry ame : ae.getMembers()) {
                                // Logger.getLogger().write("Add relation " + drTmp.getDiplomacyTypeId() + " of " + ame.getUserId() + " to " + pe.getUserId());
                                ame.addPlayerRelation(ameOuter, drTmp);
                            }

                            for (AllianceMemberEntry ame2 : ae.getMembers()) {
                                ameOuter.addPlayerRelation(ame2, drTmp);
                            }
                        }
                    }

                    for (AllianceMemberEntry ame : ae.getMembers()) {
                        for (AllianceMemberEntry ameInner : aeInner.getMembers()) {
                            if (!ame.getRelationsToPlayer().containsKey(ameInner)) {
                                ame.addPlayerRelation(ameInner, DiplomacyUtilities.getDiplomacyRel(ame.getUserId(), ameInner.getUserId()));
                            }

                            if (!ameInner.getRelationsToPlayer().containsKey(ame)) {
                                ameInner.addPlayerRelation(ame, DiplomacyUtilities.getDiplomacyRel(ame.getUserId(), ameInner.getUserId()));
                            }
                        }
                    }
                }
            }

            for (AllianceMemberEntry ame : ae.getMembers()) {
                for (PlayerEntry pe : peList) {
                    ame.addPlayerRelation(pe, DiplomacyUtilities.getDiplomacyRel(ame.getUserId(), pe.getUserId()));
                }
            }

            // Logger.getLogger().write("Look for AE: " + ae);
            // Also check for suballiances of current Alliance
            ArrayList<AllianceEntry> subAeList = subAlliances.get(ae);
            if (subAeList != null) {
                // Logger.getLogger().write("Found subAlliance for " + ae.getAllianceId());
                for (AllianceEntry subAe : subAeList) {
                    // Logger.getLogger().write("Found subAlliance " + subAe.getAllianceId());
                    for (AllianceMemberEntry subAme : subAe.getMembers()) {
                        // Logger.getLogger().write("Found Member " + subAme.getUserId());
                        for (PlayerEntry pe : peList) {
                            // Logger.getLogger().write("Adding relation to player " + pe.getUserId() + ": " + DiplomacyUtilities.getDiplomacyRel(subAme.getUserId(), pe.getUserId()).getDiplomacyTypeId());
                            subAme.addPlayerRelation(pe, DiplomacyUtilities.getDiplomacyRel(subAme.getUserId(), pe.getUserId()));
                        }
                    }
                }
            }

            for (PlayerEntry pe : peList) {
                DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRelationAllianceToUser(ae.getAllianceId(), pe.getUserId());
                ae.addPlayerRelation(pe, dr);
            }
        }

        // Process Player
        for (PlayerEntry pe : peList) {
            // Logger.getLogger().write("Processing PlayerEntry " + pe.getUserId());

            for (AllianceEntry aeInner : aeList) {
                DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRelationUserToAlliance(pe.getUserId(), aeInner.getAllianceId());
                // Logger.getLogger().write("Relation to Alliance " + aeInner.getAllianceId() + " is " + dr.getDiplomacyTypeId());
                pe.addAllianceRelation(aeInner, dr);

                if (dr.getDiplomacyTypeId() == DiplomacyType.NEUTRAL) {
                    // Logger.getLogger().write("Look for AE: " + aeInner);
                    // Also check for suballiances of current Alliance
                    ArrayList<AllianceEntry> subAeList = subAlliances.get(aeInner);
                    if (subAeList != null) {
                        // Logger.getLogger().write("Found subAlliance for " + aeInner.getAllianceId());
                        for (AllianceEntry subAe : subAeList) {
                            // Logger.getLogger().write("Found subAlliance " + subAe.getAllianceId());
                            for (AllianceMemberEntry subAme : subAe.getMembers()) {
                                // Logger.getLogger().write("Found Member " + subAme.getUserId());
                                // Logger.getLogger().write("Adding relation to player " + pe.getUserId() + ": " + DiplomacyUtilities.getDiplomacyRel(pe.getUserId(), subAme.getUserId()));
                                pe.addPlayerRelation(subAme, DiplomacyUtilities.getDiplomacyRel(pe.getUserId(), subAme.getUserId()));
                            }
                        }
                    }
                }
            }

            for (PlayerEntry peInner : peList) {
                if (pe.getUserId() == peInner.getUserId()) {
                    continue;
                }
                DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRel(pe.getUserId(), peInner.getUserId());
                // Logger.getLogger().write("Relation to Player " + peInner.getUserId() + " is " + dr.getDiplomacyTypeId());
                pe.addPlayerRelation(peInner, dr);
            }
        }

        return new DiplomacyGroupResult(aeList, peList);
    }

    public static CombatGroupResult getCombatGroupsSimulation(ArrayList<Integer> users, RelativeCoordinate rc, HashMap<Integer, ArrayList<Integer>> groups) {
        DiplomacyGroupResult dgr = resolveUsersSimulation(users, groups);

        CombatGroupResult cgr = new CombatGroupResult();
        HashMap<Integer, ArrayList<Integer>> assignedGroups = new HashMap<Integer, ArrayList<Integer>>();

        // Put each user in its own CombatGroup and set all other groups to hostile
        int groupId = -1;
        for (PlayerEntry pe : dgr.getPlayers()) {
            if (groupId == -1) {
                // Create first group
                groupId++;
                ArrayList<Integer> playerList = new ArrayList<Integer>();
                playerList.add(pe.getUserId());
                assignedGroups.put(groupId, playerList);
            } else {
                boolean newGroup = true;
                int existingGroupId = -1;

                for (Map.Entry<Integer, ArrayList<Integer>> aGroupEntry : assignedGroups.entrySet()) {
                    for (Map.Entry<Integer, ArrayList<Integer>> groupEntry : groups.entrySet()) {
                        // If a user in current assigned group is in same group as in group
                        // put him into same list
                        if (groupEntry.getValue().contains(pe.getUserId())) {
                            for (Integer player : groupEntry.getValue()) {
                                for (Integer aPlayer : aGroupEntry.getValue()) {
                                    if (player.equals(aPlayer)) {
                                        // same group
                                        newGroup = false;
                                        existingGroupId = aGroupEntry.getKey();
                                        break;
                                    }
                                }

                                if (!newGroup) {
                                    break;
                                }
                            }
                        }

                        if (!newGroup) {
                            break;
                        }
                    }

                    if (!newGroup) {
                        break;
                    }
                }

                if (newGroup) {
                    groupId++;
                    ArrayList<Integer> playerList = new ArrayList<Integer>();
                    playerList.add(pe.getUserId());
                    assignedGroups.put(groupId, playerList);
                } else {
                    assignedGroups.get(existingGroupId).add(pe.getUserId());
                }
            }
        }

        for (Map.Entry<Integer, ArrayList<Integer>> aGroupEntry : assignedGroups.entrySet()) {
            CombatGroupEntry cge = new CombatGroupEntry(aGroupEntry.getKey());

            for (Integer player : aGroupEntry.getValue()) {
                cge.addPlayer(player);
                cgr.addCombatGroup(cge);
            }
        }

        for (CombatGroupEntry cge : cgr.getCombatGroups()) {
            for (CombatGroupEntry cge2 : cgr.getCombatGroups()) {
                if (cge != cge2) {
                    cge.addAttackingGroup(cge2.getGroupId());
                }
            }
        }

        return cgr;
    }

    public static CombatGroupResult getCombatGroups(ArrayList<Integer> users, RelativeCoordinate rc) {
        DiplomacyGroupResult dgr = resolveUsers(users);
        
        /*
        for (AllianceEntry ae : dgr.getAlliances()) {
        Logger.getLogger().write("Found alliance " + ae.getAllianceId());
        for (Map.Entry<AllianceEntry,DiplomacyRelation> entry : ae.getRelationsToAlliance().entrySet()) {
        Logger.getLogger().write("> Alliance has relation " + getNameForRelation(entry.getValue().getDiplomacyTypeId()) + " to Alliance " + entry.getKey().getAllianceId());
        }
        for (Map.Entry<PlayerEntry,DiplomacyRelation> entry : ae.getRelationsToPlayer().entrySet()) {
        Logger.getLogger().write("> Alliance has relation " + getNameForRelation(entry.getValue().getDiplomacyTypeId()) + " to Player " + entry.getKey().getUserId());
        }

        for (AllianceMemberEntry ame : ae.getMembers()) {
        Logger.getLogger().write(">> Member: " + ame.getUserId());
        for (Map.Entry<AllianceEntry,DiplomacyRelation> entry : ame.getRelationsToAlliance().entrySet()) {
        Logger.getLogger().write(">>> AllianceMember has relation " + getNameForRelation(entry.getValue().getDiplomacyTypeId()) + " to Alliance " + entry.getKey().getAllianceId());
        }
        for (Map.Entry<PlayerEntry,DiplomacyRelation> entry : ame.getRelationsToPlayer().entrySet()) {
        Logger.getLogger().write(">>> AllianceMember has relation " + getNameForRelation(entry.getValue().getDiplomacyTypeId()) + " to Player " + entry.getKey().getUserId());
        }
        }
        }        
         */

        // Obviously nobody fights against himself
        if (dgr.getAlliances().size() + dgr.getPlayers().size() == 1) {
            return new CombatGroupResult();
        }

        // Loading users in system
        if (rc == null) {
            return null;
        }
        HashSet<Integer> colonyUser = new HashSet<Integer>();

        int systemId = rc.getSystemId();
        ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
        for (Planet p : pList) {
            PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
            if (pp != null) {
                colonyUser.add(pp.getUserId());
            }
        }

        // **********************************************
        // STEP 4: Combine treaties into CombatGroups
        //         If treaties have conflicting goals to 
        //         other groups take most hostile 
        //         relation
        // **********************************************          

        int combatGroupCounter = 1;
        HashMap<Integer, ArrayList<CombatParticipent>> groupedParticipents = new HashMap<Integer, ArrayList<CombatParticipent>>();
        ArrayList<CombatGroupEntry> combatGroups = new ArrayList<CombatGroupEntry>();

        for (AllianceEntry ae : dgr.getAlliances()) {
            if (groupedParticipents.isEmpty()) {
                ArrayList<CombatParticipent> cpList = new ArrayList<CombatParticipent>();
                cpList.add(ae);
                groupedParticipents.put(1, cpList);
            } else {
                int matchingGroup = 0;

                for (Map.Entry<Integer, ArrayList<CombatParticipent>> gpEntry : groupedParticipents.entrySet()) {
                    for (CombatParticipent cp : gpEntry.getValue()) {
                        if (cp.getRelationToAlliance(ae).getDiplomacyTypeId() == DiplomacyType.TREATY) {
                            matchingGroup = gpEntry.getKey();
                            break;
                        }
                    }

                    if (matchingGroup != 0) {
                        break;
                    }
                }

                if (matchingGroup != 0) {
                    groupedParticipents.get(matchingGroup).add(ae);
                } else {
                    combatGroupCounter++;
                    ArrayList<CombatParticipent> cpList = new ArrayList<CombatParticipent>();
                    cpList.add(ae);
                    groupedParticipents.put(combatGroupCounter, cpList);
                }
            }
        }

        for (PlayerEntry pe : dgr.getPlayers()) {
            if (groupedParticipents.isEmpty()) {
                ArrayList<CombatParticipent> cpList = new ArrayList<CombatParticipent>();
                cpList.add(pe);
                groupedParticipents.put(1, cpList);
            } else {
                int matchingGroup = 0;

                for (Map.Entry<Integer, ArrayList<CombatParticipent>> gpEntry : groupedParticipents.entrySet()) {
                    for (CombatParticipent cp : gpEntry.getValue()) {
                        if (cp instanceof PlayerEntry) {
                            if (((PlayerEntry) cp).getUserId() == pe.getUserId()) {
                                continue;
                            }
                        }

                        if (cp.getRelationTo(pe).getDiplomacyTypeId() == DiplomacyType.TREATY) {
                            matchingGroup = gpEntry.getKey();
                            break;
                        }
                    }

                    if (matchingGroup != 0) {
                        break;
                    }
                }

                if (matchingGroup != 0) {
                    groupedParticipents.get(matchingGroup).add(pe);
                } else {
                    combatGroupCounter++;
                    ArrayList<CombatParticipent> cpList = new ArrayList<CombatParticipent>();
                    cpList.add(pe);
                    groupedParticipents.put(combatGroupCounter, cpList);
                }
            }
        }

        // **********************************************
        // STEP 5: Groups have been combined according to 
        //         treaty. Now check groops for conflicting 
        //         goals and split them up
        // **********************************************           

        // **********************************************
        // STEP 6: Groups have been combined according to 
        //         treaty. Now check groops for conflicting 
        //         goals and split them up
        // **********************************************                     
        HashMap<Integer, CombatGroupEntry> cgeMap = new HashMap<Integer, CombatGroupEntry>();

        for (Map.Entry<Integer, ArrayList<CombatParticipent>> gpEntry : groupedParticipents.entrySet()) {
            for (Map.Entry<Integer, ArrayList<CombatParticipent>> gpEntryInner : groupedParticipents.entrySet()) {
                if (gpEntry.getKey() == gpEntryInner.getKey()) {
                    continue;
                }
                boolean usersHaveColony = false;
                boolean usersClaimArea = false;

                // highest is most hostile
                int highestRelation = -1;
                for (CombatParticipent cp : gpEntry.getValue()) {
                    if (usersHaveColony == false) usersHaveColony = usersHaveColony(colonyUser, cp.getAllMemberIds());
                    if (usersClaimArea == false) usersClaimArea = usersClaimArea(cp.getAllMemberIds(),rc);
                    
                    for (CombatParticipent cp2 : gpEntryInner.getValue()) {
                        if (usersHaveColony == false) usersHaveColony = usersHaveColony(colonyUser, cp2.getAllMemberIds());
                        if (usersClaimArea == false) usersClaimArea = usersClaimArea(cp.getAllMemberIds(),rc);
                        

                        // If 2 alliances have neutral relations, check for relations between the players
                        // Logger.getLogger().write("RELATIONS BETWEEN " + cp + " ("+cp.getId()+") AND " + cp2 + " ("+cp2.getId()+") ARE " + cp.getRelationTo(cp2).getDiplomacyTypeId());
                        // Logger.getLogger().write(cp + " " + cp2);

                        if ((cp instanceof AllianceEntry) && (cp2 instanceof AllianceEntry) && ((cp.getRelationTo(cp2)).getDiplomacyTypeId() == DiplomacyType.NEUTRAL)) {
                            // Logger.getLogger().write("I'm here");

                            for (AllianceMemberEntry ame : ((AllianceEntry) cp).getMembers()) {
                                // Logger.getLogger().write("I'm here 2 " + IdToNameService.getUserName(ame.getId()));

                                for (AllianceMemberEntry ame2 : ((AllianceEntry) cp2).getMembers()) {
                                    // Logger.getLogger().write("I'm here 2 " + IdToNameService.getUserName(ame2.getId()));
                                    // Logger.getLogger().write("Comparing members " + ame.getId() + " and " + ame2.getId() + " RELATION: " + ame.getRelationTo(ame2).getDiplomacyTypeId());

                                    int currRelation = ame.getRelationToPlayer(ame2).getDiplomacyTypeId();
                                    highestRelation = Math.max(highestRelation, currRelation);
                                }
                            }
                        } else if ((cp instanceof PlayerEntry) && (cp2 instanceof AllianceEntry) && ((cp.getRelationTo(cp2)).getDiplomacyTypeId() == DiplomacyType.NEUTRAL)) {
                            for (AllianceMemberEntry ame : ((AllianceEntry) cp2).getMembers()) {
                                int currRelation = ame.getRelationTo(cp).getDiplomacyTypeId();
                                highestRelation = Math.max(highestRelation, currRelation);
                            }
                        } else if ((cp instanceof AllianceEntry) && (cp2 instanceof PlayerEntry) && ((cp.getRelationTo(cp2)).getDiplomacyTypeId() == DiplomacyType.NEUTRAL)) {
                            for (AllianceMemberEntry ame : ((AllianceEntry) cp).getMembers()) {
                                int currRelation = ame.getRelationTo(cp2).getDiplomacyTypeId();
                                highestRelation = Math.max(highestRelation, currRelation);
                            }
                        } else {
                            if (cp.getClass().equals(cp2.getClass()) && (cp.getId() == cp2.getId())) {
                                continue;
                            }

                            highestRelation = Math.max(highestRelation, cp.getRelationTo(cp2).getDiplomacyTypeId());
                        }
                    }
                }

                Logger.getLogger().write("Determined highest relation is " + highestRelation);
                if (highestRelation == -1) {
                    highestRelation = DiplomacyType.NEUTRAL;
                }

                // Relation determined, check against combat Location
                boolean peace = true;

                if (highestRelation >= 6) { // Always fight
                    peace = false;
                    CombatGroupEntry cge = null;

                    if (cgeMap.containsKey(gpEntry.getKey())) {
                        cge = cgeMap.get(gpEntry.getKey());
                    } else {
                        cge = new CombatGroupEntry(gpEntry.getKey());
                        for (CombatParticipent cp : gpEntry.getValue()) {
                            cge.addPlayers(cp.getAllMemberIds());
                        }
                    }

                    cge.addAttackingGroup(gpEntryInner.getKey());

                    if (!cgeMap.containsKey(gpEntry.getKey())) {
                        cgeMap.put(gpEntry.getKey(), cge);
                    }
                } else if (highestRelation >= 3) { // Always fight in own system
                    if (usersHaveColony || usersClaimArea) {
                        peace = false;
                        CombatGroupEntry cge = null;

                        if (cgeMap.containsKey(gpEntry.getKey())) {
                            cge = cgeMap.get(gpEntry.getKey());
                        } else {
                            cge = new CombatGroupEntry(gpEntry.getKey());
                            for (CombatParticipent cp : gpEntry.getValue()) {
                                cge.addPlayers(cp.getAllMemberIds());
                            }
                        }

                        cge.addAttackingGroup(gpEntryInner.getKey());

                        if (!cgeMap.containsKey(gpEntry.getKey())) {
                            cgeMap.put(gpEntry.getKey(), cge);
                        }
                    }
                }

                if (peace) {
                    if (cgeMap.containsKey(gpEntry.getKey())) {
                        continue;
                    }

                    CombatGroupEntry cge = null;

                    if (cgeMap.containsKey(gpEntry.getKey())) {
                        cge = cgeMap.get(gpEntry.getKey());
                    } else {
                        cge = new CombatGroupEntry(gpEntry.getKey());
                        for (CombatParticipent cp : gpEntry.getValue()) {
                            cge.addPlayers(cp.getAllMemberIds());
                        }
                    }

                    if (!cgeMap.containsKey(gpEntry.getKey())) {
                        cgeMap.put(gpEntry.getKey(), cge);
                    }
                }
            }
        }

        CombatGroupResult cgr = new CombatGroupResult();
        for (Map.Entry<Integer, CombatGroupEntry> cgeEntry : cgeMap.entrySet()) {
            cgr.addCombatGroup(cgeEntry.getValue());
        }

        return cgr;
    }

    private static boolean usersClaimArea(ArrayList<Integer> users, RelativeCoordinate rc) {
        // Build Areas
        ArrayList<Area> areas = new ArrayList<Area>();
        
        for (Integer user : users) {
            ArrayList<TerritoryMap> tmList = tmDAO.findByUserId(user);
            for (TerritoryMap tm : tmList) {
                if (tm.getTerritoryType() != ETerritoryType.EMPIRE) continue;
                
                ArrayList<TerritoryMapPoint> tmpList = tmpDAO.findByTerritoryId(tm.getId());
                TreeMap<Integer,TerritoryMapPoint> sortedPoints = new TreeMap<Integer,TerritoryMapPoint>();
                
                for (TerritoryMapPoint tmp : tmpList) {
                    sortedPoints.put(tmp.getId(),tmp);
                }
                
                Polygon p = new Polygon();
                for (TerritoryMapPoint tmp : sortedPoints.values()) {
                    p.addPoint(tmp.getX(), tmp.getY());
                }
                
                Area a = new Area(p);
                areas.add(a);
            }
        }
        
        Point2D point = new Point2D.Float(rc.toAbsoluteCoordinate().getX(),rc.toAbsoluteCoordinate().getY());
        
        for (Area a : areas) {
            if (a.contains(point)) return true;
        }        
        
        return false;
    }
    
    private static boolean usersHaveColony(HashSet<Integer> colUser, ArrayList<Integer> users) {
        for (Integer user : users) {
            if (colUser.contains(user)) {
                return true;
            }
        }

        return false;
    }

    public static String getNameForRelation(int id) {
        DiplomacyType dt = dtDAO.findById(id);
        if (dt == null) {
            return "Invalid";
        }
        return dt.getName();
    }
}
