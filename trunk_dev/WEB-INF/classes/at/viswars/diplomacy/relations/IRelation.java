/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.diplomacy.relations;

import at.viswars.result.DiplomacyResult;

/**
 *
 * @author Bullet
 */
public interface IRelation {

    public DiplomacyResult getDiplomacyResult();

}
