/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.diplomacy.relations;

import at.viswars.enumeration.EOwner;
import at.viswars.enumeration.ESharingType;
import at.viswars.result.DiplomacyResult;

/**
 *
 * @author Bullet
 */
public class Treaty implements IRelation{

    public DiplomacyResult getDiplomacyResult() {
        boolean attacks = false;
        boolean helps = true;
        boolean notification = false;
        boolean attackTradeFleets = false;
        ESharingType sharingStarMapInfo = ESharingType.OPTIONAL;
        boolean ableToUseShipyard = true;
        EAttackType battleInNeutral = EAttackType.NO;
        EAttackType battleInOwn = EAttackType.NO;

        EOwner owner = EOwner.TREATY;
        String color = "#78BCFF";

        DiplomacyResult dr = new DiplomacyResult(attacks, helps, notification, attackTradeFleets, color, sharingStarMapInfo, ableToUseShipyard , battleInNeutral, battleInOwn, owner);
        return dr;
    }
}
