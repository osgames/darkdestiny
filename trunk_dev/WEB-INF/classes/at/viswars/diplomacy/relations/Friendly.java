/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.diplomacy.relations;

import at.viswars.enumeration.EOwner;
import at.viswars.enumeration.ESharingType;
import at.viswars.result.DiplomacyResult;

/**
 *
 * @author Bullet
 */
public class Friendly implements IRelation{

    public DiplomacyResult getDiplomacyResult() {
        boolean attacks = false;
        boolean helps = false;
        boolean notification = false;
        boolean attackTradeFleets = true;
        ESharingType sharingStarMapInfo = ESharingType.NONE;
        EAttackType battleInNeutral = EAttackType.NO;
        EAttackType battleInOwn = EAttackType.YES;
        boolean ableToUseShipyard = false;

        EOwner owner = EOwner.FRIENDLY;
        String color = "#FBFF00";

        DiplomacyResult dr = new DiplomacyResult(attacks, helps, notification, attackTradeFleets, color, sharingStarMapInfo, ableToUseShipyard , battleInNeutral, battleInOwn, owner);
        return dr;
    }
}
