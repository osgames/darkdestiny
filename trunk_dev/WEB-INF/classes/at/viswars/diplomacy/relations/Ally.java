/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.diplomacy.relations;

import at.viswars.enumeration.EOwner;
import at.viswars.enumeration.ESharingType;
import at.viswars.result.DiplomacyResult;

/**
 *
 * @author Dreloc
 */
public class Ally implements IRelation{


    public DiplomacyResult getDiplomacyResult() {
        boolean attacks = true;
        boolean helps = true;
        boolean notification = false;
        boolean attackTradeFleets = false;
        ESharingType sharingStarMapInfo = ESharingType.ALL;
        EAttackType battleInNeutral = EAttackType.NO;
        EAttackType battleInOwn = EAttackType.NO;
        boolean ableToUseShipyard = true;
        EOwner owner = EOwner.ALLY;
        String color = "#1700C1";

        DiplomacyResult dr = new DiplomacyResult(attacks, helps, notification, attackTradeFleets, color, sharingStarMapInfo, ableToUseShipyard , battleInNeutral, battleInOwn, owner);
        return dr;
    }
}
