/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.diplomacy.relations;

import at.viswars.enumeration.EOwner;
import at.viswars.enumeration.ESharingType;
import at.viswars.result.DiplomacyResult;

/**
 *
 * @author Bullet
 */
public class Nap implements IRelation{

    public DiplomacyResult getDiplomacyResult() {
        boolean attacks = false;
        boolean helps = false;
        boolean notification = true;
        boolean attackTradeFleets = false;
        ESharingType sharingStarMapInfo = ESharingType.NONE;
        boolean ableToUseShipyard = false;
        EAttackType battleInNeutral = EAttackType.NO;
        EAttackType battleInOwn = EAttackType.NO;

        EOwner owner = EOwner.NAP;
        String color = "#78BCFF";

        DiplomacyResult dr = new DiplomacyResult(attacks, helps, notification, attackTradeFleets, color, sharingStarMapInfo, ableToUseShipyard , battleInNeutral, battleInOwn, owner);
        return dr;
    }

}

/*
B�ndnis - wie gehabt
Freundlich - Kein Angriff egal wo  aber auch keine Hilfestellung
NAP - Kein automatischer Angriff Benachrichtigung bei Verletzung des NAP durch eindringen in Territorium
NEUTRAL - Kein Kampf in neutralen Systemen Automatischer Kampf in eigenen
Feindlich - Kampf �berall
Krieg - Kampf �berall
 */