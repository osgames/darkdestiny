/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.scanner;

/**
 *
 * @author Stefan
 */
public abstract class AbstractCoordinate {
    public final int x;
    public final int y;
    
    public AbstractCoordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }
}