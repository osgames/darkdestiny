/*
 * ImageMapBuffer.java
 *
 * Created on 30. April 2007, 01:43
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.scanner;

import java.util.*;
/**
 *
 * @author Stefan
 */
public class ImageMapBuffer {
    private static HashMap<Integer,String> imageMapBuffer;
    private static boolean init = false;
    
    private ImageMapBuffer() {
        
    }
        
    private static void init() {
        imageMapBuffer = new HashMap<Integer,String>();
        init = true;        
    }
    
    public static synchronized void addMap(int userId, String map) {
        if (!init) init();
        imageMapBuffer.put(userId,map);
    }
    
    public static String getMap(int userId) {       
        if (!init) return "";
        if (imageMapBuffer.containsKey(userId)) {
            return imageMapBuffer.get(userId);
        } else {
            return "";
        }                
    }
    
    public static synchronized void removeMap(int userId) {
        if (imageMapBuffer.containsKey(userId)) {
            imageMapBuffer.remove(userId);
        }
    }
}
