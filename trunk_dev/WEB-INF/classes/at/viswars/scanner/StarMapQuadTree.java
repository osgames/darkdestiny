/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.scanner;

import at.viswars.Logger.Logger;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class StarMapQuadTree<T extends AbstractCoordinate> {
    private final Quad baseRoot;
    
    public StarMapQuadTree(int width, int height, int resSteps) {
        baseRoot = new Quad(0,0,width,height);
                        
        if (resSteps > 1) {
            baseRoot.split(resSteps - 1);
        }
    }
     public StarMapQuadTree(int originX, int originY, int width, int height, int resSteps) {
        baseRoot = new Quad(originX,originY,width,height);
                        
        if (resSteps > 1) {
            baseRoot.split(resSteps - 1);
        }
    }
    public ArrayList<T> getItemsAround(int x, int y, int distance) {
        ArrayList<T> sysList = new ArrayList<T>();
        
        int tlX = x - (int)(distance); int tlY = y - (int)(distance);
        int trX = x + (int)(distance); int trY = y - (int)(distance);
        int brX = x - (int)(distance); int brY = y + (int)(distance);
        int blX = x + (int)(distance); int blY = y + (int)(distance);

        QuadTreeCoordinateSet qtcs = new QuadTreeCoordinateSet();
        qtcs.tlX = tlX;
        qtcs.tlY = tlY;
        qtcs.trX = trX;
        qtcs.trY = trY;
        qtcs.brX = brX;
        qtcs.brY = brY;
        qtcs.blX = blX;
        qtcs.blY = blY;
        
        /*
        HashMap<Quad,Object> quads = new HashMap<Quad,Object>();
        quads.put(baseRoot.getQuadAt(x, y),null);
        quads.put(baseRoot.getQuadAt(tlX, tlY),null);
        quads.put(baseRoot.getQuadAt(trX, trY),null);
        quads.put(baseRoot.getQuadAt(blX, blY),null);
        quads.put(baseRoot.getQuadAt(brX, brY),null);
        */
                
        int i = 0;
        ArrayList<Quad> selQuads = new ArrayList<Quad>();
        baseRoot.getQuads(selQuads, qtcs);

        for (Quad q : selQuads) {
            if (q == null) continue;                      
            
            for (T sd : (ArrayList<T>)q.systems) {
                i++;
                if (Math.sqrt(Math.pow(sd.x - x,2) + Math.pow(sd.y - y,2)) <= distance) {
                    sysList.add(sd);                    
                }
            }
        }
        
        return sysList;
    }     
    
    public boolean addItemToTree(T sd) {
        return baseRoot.addItem(sd);
    } 
    
    public void countQuadTreeElements() {
        Logger.getLogger().write("Elements in Tree: " + baseRoot.getQuadElements());
    }
}
