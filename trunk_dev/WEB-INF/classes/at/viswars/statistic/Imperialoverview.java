/*
 * Imperialoverview.java
 *
 * Created on 21. Februar 2008, 10:41
 *
 */
package at.viswars.statistic;

/**
 *
 * @author craft4
 */
import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.GameConstants;
import at.viswars.enumeration.EImperialStatisticType;
import at.viswars.model.ImperialStatistic;
import at.viswars.service.Service;
import at.viswars.utilities.AllianceUtilities;
import java.util.*;

public class Imperialoverview extends Service {
    // Number of Ships of the user 

    HashMap<Integer, Long> userShipCount;
    // Number of Ships of the alliance
    HashMap<Integer, Long> allyShipCount;
    // Amount of ressources 
    HashMap<Integer, Long> userRess;
    HashMap<Integer, Long> allyRess;
    // Amount of Troops on Planet - Miliz = 0 - more info - set/get Method
    HashMap<Integer, Long> userTroops;
    HashMap<Integer, Long> allyTroops;
    // Amount of defensive Stations - more info - set/get Method
    HashMap<Integer, Long> userDefense;
    HashMap<Integer, Long> allyDefense;
    private long user_population;
    private long ally_population;
    private int user_income;
    private int ally_income;
    private int user_planets;
    private int ally_planets;
    private int user_systems;
    private int ally_systems;
    //private int current_uid ;
    private int current_aid;

    /**
     * Creates a new empty instance of Imperialoverview
     */
    public Imperialoverview() {
        // Startup Initialization
        userShipCount = new HashMap<Integer, Long>();
        allyShipCount = new HashMap<Integer, Long>();

        userRess = new HashMap<Integer, Long>();
        allyRess = new HashMap<Integer, Long>();

        userTroops = new HashMap<Integer, Long>();
        allyTroops = new HashMap<Integer, Long>();

        userDefense = new HashMap<Integer, Long>();
        allyDefense = new HashMap<Integer, Long>();

        user_income = ally_income = 0;
        user_planets = ally_planets = 0;
        user_systems = ally_systems = 0;
        user_population = ally_population = 0;

        //current_uid = 0;
        current_aid = 0;
    }

    /**
     * creates a new Instance and loades the values for the specific user
     */
    public Imperialoverview(int userid) {
        // Startup Initialization
        userShipCount = new HashMap<Integer, Long>();
        allyShipCount = new HashMap<Integer, Long>();

        userRess = new HashMap<Integer, Long>();
        allyRess = new HashMap<Integer, Long>();

        userTroops = new HashMap<Integer, Long>();
        allyTroops = new HashMap<Integer, Long>();

        userDefense = new HashMap<Integer, Long>();
        allyDefense = new HashMap<Integer, Long>();

        user_income = ally_income = 0;
        user_planets = ally_planets = 0;
        user_systems = ally_systems = 0;
        user_population = ally_population = 0;

        try {
            OverviewloadValues(userid);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Failed to Load values - at.viswars.statistic.Imperialoverview.java: ", e);
        }
    }

    /*
     * Section of User Access Functions
     */
    /**
     * @param id - get the Number of Ships for this User
     * id specifies the shiptype
     *
     * @return Amount of Ships or 0 in case of error
     */
    public long getUserShipcount(int id) {
        if (!userShipCount.containsKey(id)) {
            return 0;
        }

        return userShipCount.get(id);
    }

    /**
     * @param id - set the Number of Ships for this User
     * id specifies the shiptype
     *
     * @return 0 in case of error, 0 if everything done
     * @param number - the current amount of ships
     */
    public int setUserShipcount(long number, int id) {
        userShipCount.put(id, number);

        return 0;
    }

    /**
     * @param id - get the Amount of Ressources for this User
     * id specifies the ressource
     *
     * @return Amount of Ressource or 0 in case of error
     */
    public long getUserRessource(int id) {
        if (!userRess.containsKey(id)) {
            return 0;
        }

        return userRess.get(id);
    }

    /**
     * @param id - set the Amount of Ressources for this User
     * id specifies the ressource
     *
     * @param number - the current amount of ressources
     *
     *  @return Amount of Ressource or 0 in case of error
     */
    public int setUserRessource(long number, int id) {
        userRess.put(id, number);

        return 0;
    }

    /**
     * @param id - get the Amount of Groundtroops for this User
     * id specifies the troopstype
     *
     * @return Amount of Ressource or 0 in case of error
     */
    public long getUserTroops(int id) {
        if (!userTroops.containsKey(id)) {
            return 0;
        }

        return userTroops.get(id);
    }

    /**
     * @param id - set the Amount of Groundtroops for this User
     * id specifies the troopstype
     *
     * @return  0 in case of error or 0 if OK
     */
    public int setUserTroops(long number, int id) {
        userTroops.put(id, number);

        return 0;
    }

    /**
     * @param id - get the Amount of defence stations for this User
     * id specifies the type
     *
     * @return Amount of Ressource or 0 in case of error
     *
     * 29 - Raketenbasis
     * 28 - planetares Lasergeschuetz
     * 74 - planetares Intervallgeschuetz
     * ? - kleine Kampfstation
     * ? - grosse Kampfstation
     */
    public long getUserDefence(int id) {
        if (!userDefense.containsKey(id)) {
            return 0;
        }

        return userDefense.get(id);
    }

    /**
     * @param id - set the Amount of defence for this User
     * id specifies the type
     *
     * @return  0 in case of error or 0 if OK
     *
     * 29 - Raketenbasis
     * 28 - planetares Lasergeschuetz
     * 74 - planetares Intervallgeschuetz
     * ? - kleine Kampfstation
     * ? - grosse Kampfstation
     */
    public int setUserDefence(long number, int id) {
        userDefense.put(id, number);
        return 0;
    }

    public long getUserPopulation() {
        return user_population;
    }

    public int getUserPlanets() {
        return user_planets;
    }

    public int getUserSystems() {
        return user_systems;
    }

    public int getUserIncome() {
        return user_income;
    }

    public void setUserPopulation(long number) {
        user_population = number;
    }

    public void setUserPlanets(int number) {
        user_planets = number;
    }

    public void setUserSystems(int number) {
        user_systems = number;
    }

    public void setUserIncome(int number) {
        user_income = number;
    }

    /*
     * Section of ally access functions
     */
    /**
     * @param id - get the Number of Ships for the ally
     * id specifies the shiptype
     *
     * @return Amount of Ships or 0 in case of error
     */
    public long getAllyShipcount(int id) {
        if (!allyShipCount.containsKey(id)) {
            return 0;
        }

        return allyShipCount.get(id);
    }

    /**
     * @param id - set the Number of Ships for the ally
     * id specifies the shiptype
     *
     * @return 0 in case of error, 0 if everything done
     *
     * @param number - the current amount of ships
     */
    public int setAllyShipcount(long number, int id) {
        allyShipCount.put(id, number);

        return 0;
    }

    /**
     * @param id - get the Amount of Ressources for the ally
     * id specifies the ressource
     *
     * @return Amount of Ressource or 0 in case of error
     */
    public long getAllyRessource(int id) {
        if (!allyRess.containsKey(id)) {
            return 0;
        }

        return allyRess.get(id);
    }

    /**
     * @param id - set the Amount of Ressources for the ally
     * id specifies the ressource
     *
     * @param number - the current amount of ressources
     *
     *  @return Amount of Ressource or 0 in case of error
     */
    public int setAllyRessource(long number, int id) {
        allyRess.put(id, number);

        return 0;
    }

    /**
     * @param id - get the Amount of Groundtroops for the ally
     * id specifies the troopstype
     *
     * @return Amount of Ressource or 0 in case of error
     */
    public long getAllyTroops(int id) {
        if (!allyTroops.containsKey(id)) {
            return 0;
        }

        return allyTroops.get(id);
    }

    /**
     * @param id - set the Amount of Groundtroops for the ally
     * id specifies the troopstype
     *
     * @return  0 in case of error or 0 if OK
     */
    public int setAllyTroops(long number, int id) {
        allyTroops.put(id, number);

        return 0;
    }

    /**
     * @param id - get the Amount of defence stations for the ally
     * id specifies the type
     *
     * @return Amount of Ressource or 0 in case of error
     *
     * 29 - Raketenbasis
     * 28 - planetares Lasergeschuetz
     * 74 - planetares Intervallgeschuetz
     * ? - kleine Kampfstation
     * ? - grosse Kampfstation
     */
    public long getAllyDefence(int id) {
        if (!allyDefense.containsKey(id)) {
            return 0;
        }

        return allyDefense.get(id);
    }

    /**
     * @param id - set the Amount of defence for the ally
     * id specifies the type
     *
     * @return  0 in case of error or 0 if OK
     *
     * 29 - Raketenbasis
     * 28 - planetares Lasergeschuetz
     * 74 - planetares Intervallgeschuetz
     * ? - kleine Kampfstation
     * ? - grosse Kampfstation
     */
    public int setAllyDefence(long number, int id) {
        allyDefense.put(id, number);

        return 0;
    }

    public long getAllyPopulation() {
        return ally_population;
    }

    public int getAllyPlanets() {
        return ally_planets;
    }

    public int getAllySystems() {
        return ally_systems;
    }

    public int getAllyIncome() {
        return ally_income;
    }

    public void setAllyPopulation(long number) {
        ally_population = number;
    }

    public void setAllyPlanets(int number) {
        ally_planets = number;
    }

    public void setAllySystems(int number) {
        ally_systems = number;
    }

    public void setAllyIncome(int number) {
        ally_income = number;
    }

    /**
     * Loads the calculated values for the specific user from the Database
     */
    private void OverviewloadValues(int userId) throws Exception {
        ArrayList<ImperialStatistic> stats = imperialStatisticDAO.findByUserId(userId);

        DebugBuffer.addLine(DebugLevel.TRACE, "Loading Statistics");
        ArrayList<Integer> allys = AllianceUtilities.getAllAlliedUsers(userId);
        // while(set.next()) {
        for (ImperialStatistic is : stats) {
            // Logger.getLogger().write("Type: "+ is.getTypeId());
            //current_uid = set.getInt(1);
{
                if(is.getTypeId() == EImperialStatisticType.STATISTIC){
                    switch (is.getSourceId()) {
                        case GameConstants.STATISTIC_INCOME:
                            user_income = (int) (long) (is.getValue());
                            break;
                        case GameConstants.STATISTIC_PLANETS:
                            user_planets = (int) (long) (is.getValue());
                            break;
                        case GameConstants.STATISTIC_SYSTEMS:
                            user_systems = (int) (long) (is.getValue());
                            break;
                        case GameConstants.STATISTIC_POPULATION:
                            user_population = is.getValue();
                            break;
                        default:
                            break;
                    }
                }

                if(is.getTypeId() == EImperialStatisticType.DEFENCE){
                    userDefense.put(is.getSourceId(), (long) is.getValue());

                }
                 if(is.getTypeId() == EImperialStatisticType.RESSOURCE){
                    userRess.put(is.getSourceId(),  (long) is.getValue());
                 }

                 if(is.getTypeId() == EImperialStatisticType.TROOPS){
                    userTroops.put(is.getSourceId(),  (long) is.getValue());
                 }

                 if(is.getTypeId() ==  EImperialStatisticType.SHIPS){
                    userShipCount.put(is.getSourceId(), (long) is.getValue());
                 }
            }
        }

        // alliance calculations


        if (allys.size() > 0) {
            for (Integer allyMember : allys) {
            /*    if (allyMember == userId) {
                    continue;
                }*/

                stats.clear();
                stats = imperialStatisticDAO.findByUserId(allyMember);


                //while(set.next()) {
                for (ImperialStatistic is : stats) {
                        if(is.getTypeId() == EImperialStatisticType.STATISTIC){
                            switch (is.getSourceId()) {
                                case GameConstants.STATISTIC_INCOME:
                                     ally_income += (int) (long) (is.getValue());
                                    break;
                                case GameConstants.STATISTIC_PLANETS:
                                    ally_planets += (int) (long) (is.getValue());
                                    break;
                                case GameConstants.STATISTIC_SYSTEMS:
                                   ally_systems += (int) (long) (is.getValue());
                                    break;
                                case GameConstants.STATISTIC_POPULATION:
                                    ally_population += (long) (is.getValue());
                                    break;
                                default:
                                    break;
                            }
                    }

                if(is.getTypeId() == EImperialStatisticType.DEFENCE){
                            addToMap(allyDefense, is.getSourceId(), (long) is.getValue());
                }
                 if(is.getTypeId() == EImperialStatisticType.RESSOURCE){
                            addToMap(allyRess, is.getSourceId(),  (long) is.getValue());
                 }

                 if(is.getTypeId() == EImperialStatisticType.TROOPS){
                            addToMap(allyTroops, is.getSourceId(),  (long) is.getValue());
                 }
                 if(is.getTypeId() ==  EImperialStatisticType.SHIPS){
                            addToMap(allyShipCount, is.getSourceId(), (long) is.getValue());
                 }
                    
                }
            }
        }

    }

    private void addToMap(HashMap<Integer, Long> hm, Integer key, Long value) {
        if (hm.containsKey(key)) {
            hm.put(key, hm.get(key) + value);
        } else {
            hm.put(key, value);
        }
    }
}
