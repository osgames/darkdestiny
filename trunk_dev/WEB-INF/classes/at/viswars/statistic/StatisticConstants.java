/*
 * statisticConstants.java
 *
 * Created on 08. Juni 2007, 16:49
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.statistic;

/**
 *
 * @author Morgenstern
 */
public class StatisticConstants {
    
    static final int PIE_CHART = 1; 
    static final int BAR_CHART = 2;
    
     public static final String CHART_DIRECTORY = "c:/csn";
    // public static final String CHART_DIRECTORY = "/webs/web495/html/homepage/subSites/statistics";
    //   static final String CHART_DIRECTORY = "/webs/web495/html/homepage/subSites/statistics";
    static final boolean localOutput = false;
    /** Creates a new instance of statisticConstants */
    public StatisticConstants() {
       
    }
    
}
