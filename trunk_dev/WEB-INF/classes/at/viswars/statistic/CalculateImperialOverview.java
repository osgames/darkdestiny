/*
 * CalculateImperialOverview.java
 *
 * Created on 21. Februar 2008, 13:21
 *
 */
package at.viswars.statistic;

import at.viswars.ships.ShipData;
import at.viswars.enumeration.EFleetViewType;
import at.viswars.*;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.dao.AllianceMemberDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ImperialStatisticDAO;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.dao.PlanetRessourceDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.dao.PlayerTroopDAO;
import at.viswars.dao.RessourceDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.TradeRouteDAO;
import at.viswars.dao.TradeRouteShipDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.enumeration.EDefenseType;
import at.viswars.enumeration.EImperialStatisticType;
import at.viswars.enumeration.EPlanetRessourceType;
import at.viswars.fleet.LoadingInformation;
import at.viswars.model.AllianceMember;
import at.viswars.model.Construction;
import at.viswars.model.HangarRelation;
import at.viswars.model.Planet;
import at.viswars.model.PlanetDefense;
import at.viswars.model.Ressource;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.UserData;
import at.viswars.model.PlanetRessource;
import at.viswars.model.ImperialStatistic;
import at.viswars.model.PlayerTroop;
import at.viswars.model.ShipDesign;
import at.viswars.model.TradeRoute;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.result.FleetListResult;
import at.viswars.service.FleetService;
import at.viswars.service.ManagementService;
import at.viswars.service.Service;
import at.viswars.ships.ShipStorage;
import at.viswars.trade.TradeRouteExt;
import java.util.*;

/**
 *
 * @author craft4
 */
public class CalculateImperialOverview extends Thread implements Runnable {

    private int currentuser;
    private int currentally;
    ArrayList<ImperialStatistic> toPersist;
    private HashMap<Integer, Integer> userShipCount;
    //private HashMap<Integer,Integer> allyShipCount;
    private HashMap<Integer, Integer> userRess;
    private HashMap<Integer, Integer> userTroops;
    private HashMap<Integer, Integer> userDefense;
    private HashMap<Integer, HashMap<Integer, Integer>> planetDefense;
    private HashMap<Integer, HashMap<Integer, Integer>> spaceStations;
    private long user_population;
    private int user_income;
    private int user_planets;
    private int user_systems;
    private static UserDataDAO userDataDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static ImperialStatisticDAO imperialStatisticDAO = (ImperialStatisticDAO) DAOFactory.get(ImperialStatisticDAO.class);
    private static PlanetDefenseDAO planetDefenseDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static AllianceMemberDAO allianceMemberDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static PlanetDAO planetDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetRessourceDAO planetRessourceDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static PlayerPlanetDAO playerPlanetDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static RessourceDAO ressourceDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static ShipDesignDAO shipDesignDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlayerTroopDAO playerTroopDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private static TradeRouteShipDAO tradeRouteShipDAO = (TradeRouteShipDAO) DAOFactory.get(TradeRouteShipDAO.class);
    private static TradeRouteDAO tradeRouteDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);

    /** Creates a new instance of CalculateImperialOverview */
    public CalculateImperialOverview() {
        setPriority(MIN_PRIORITY);

        //Delete all old statistics
        imperialStatisticDAO.removeAll();

        toPersist = new ArrayList<ImperialStatistic>();
        userShipCount = new HashMap<Integer, Integer>();
        userRess = new HashMap<Integer, Integer>();
        userTroops = new HashMap<Integer, Integer>();
        userDefense = new HashMap<Integer, Integer>();
        user_income = 0;
        user_planets = 0;
        user_systems = 0;
        user_population = 0;


        planetDefense = new HashMap<Integer, HashMap<Integer, Integer>>();
        spaceStations = new HashMap<Integer, HashMap<Integer, Integer>>();

        for (PlanetDefense pd : (ArrayList<PlanetDefense>) planetDefenseDAO.findAll()) {
            if (pd.getType() == EDefenseType.TURRET) {
                if (!planetDefense.containsKey(pd.getPlanetId())) {
                    planetDefense.put(pd.getPlanetId(), new HashMap<Integer, Integer>());
                }

                if (!planetDefense.get(pd.getPlanetId()).containsKey(pd.getUnitId())) {
                    // Create new construction entry
                    planetDefense.get(pd.getPlanetId()).put(pd.getUnitId(), pd.getCount());
                }
            } else if (pd.getType() == EDefenseType.SPACESTATION) {
                ShipDesign sd = shipDesignDAO.findById(pd.getUnitId());
                if (!spaceStations.containsKey(sd.getUserId())) {
                    spaceStations.put(sd.getUserId(), new HashMap<Integer, Integer>());
                }

                if (!spaceStations.get(sd.getUserId()).containsKey(sd.getChassis())) {
                    // Create new space station entry
                    spaceStations.get(sd.getUserId()).put(sd.getChassis(), pd.getCount());
                } else {
                    HashMap<Integer, Integer> stationCount = spaceStations.get(sd.getUserId());
                    stationCount.put(sd.getChassis(), stationCount.get(sd.getChassis()) + pd.getCount());
                }
            }
        }
    }

    private void reInit() {
        userRess.clear();
        userTroops.clear();
        userDefense.clear();
        userShipCount.clear();

        user_income = 0;
        user_planets = 0;
        user_systems = 0;
        user_population = 0;
    }

    @Override
    public void run() {
        // clear the statistic table from last time
        imperialStatisticDAO.removeAll();
        //   stmt_users.execute("DELETE FROM imperialstatistic");

        ArrayList<UserData> uds = userDataDAO.findAll();
        //   result_user = stmt_users.executeQuery("SELECT u.id, a.allianceId from user u LEFT OUTER JOIN alliancemembers a ON u.id = a.userId");

        DebugBuffer.addLine(DebugLevel.TRACE, "yep, i am doing the statistics, sucker");

        // cycle through all players
        for (UserData ud : uds) {
            //while (result_user.next()) {
            currentuser = ud.getUserId();
            DebugBuffer.addLine(DebugLevel.TRACE, "working on user: " + Integer.toString(currentuser));
            AllianceMember am = allianceMemberDAO.findByUserId(currentuser);
            if (am == null) {
                currentally = 0;
            } else {
                currentally = am.getAllianceId();
            }
            ArrayList<PlayerPlanet> pps = playerPlanetDAO.findByUserId(currentuser);
            //  result_planets = stmt.executeQuery("SELECT planetID from playerplanet WHERE userID = \""+currentuser+"\" ");
            ArrayList<Integer> systems = new ArrayList<Integer>();

            for (PlayerPlanet pp : pps) {
                Planet p = planetDAO.findById(pp.getPlanetId());
                if (!systems.contains(p.getSystemId())) {
                    systems.add(p.getSystemId());
                }
                //while (result_planets.next()) {
                // DebugBuffer.addLine("currentplanet: " + Integer.toString(result_planets.getInt(1)));
                for (Ressource r : ressourceDAO.findAllStoreableRessources()) {
                    PlanetRessource pr = planetRessourceDAO.findBy(pp.getPlanetId(), r.getId(), EPlanetRessourceType.INSTORAGE);
                    if (pr != null) {
                        addToMap(userRess, r.getId(), (int) (long) pr.getQty());
                    }
                }

                user_population += pp.getPopulation();

                // get the taxes of the current planet

                PlanetCalculation planetCalc = ManagementService.getCalculatedPlanetValues(pp.getPlanetId());
                PlanetCalcResult pcr = planetCalc.getPlanetCalcData();
                ExtPlanetCalcResult epcr = pcr.getExtPlanetCalcData();
                user_income += epcr.getTaxIncome();

                // check for planetare abwehr
                // DebugBuffer.addLine("Planetare Abwehr");
                // Raketenbasis
                if (getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARYMISSLEBASE) > 0) {
                    addToMap(userDefense, Construction.ID_PLANETARYMISSLEBASE, getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARYMISSLEBASE));
                }

                // Lasergesch&uuml;tz
                if (getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARYLASERCANNON) > 0) {
                    addToMap(userDefense, Construction.ID_PLANETARYLASERCANNON, getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARYLASERCANNON));
                }

                // Intervallgesch&uuml;tz
                if (getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARY_INTERVALCANNON) > 0) {
                    addToMap(userDefense, Construction.ID_PLANETARY_INTERVALCANNON, getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARY_INTERVALCANNON));
                }

            }


            user_planets = pps.size();

            // Get all Systems of the Player
            user_systems = systems.size();

            // get the ships of the current player in fleets

            FleetListResult flr = FleetService.getFleetList(currentuser, 0, 0, EFleetViewType.OWN_ALL);
            ArrayList<PlayerFleetExt> fleets = flr.getSingleFleets();
            fleets.addAll(FleetService.getFleetList(currentuser, 0, 0, EFleetViewType.OWN_ALL_WITH_ACTION).getSingleFleets());
            for (PlayerFleetExt pfe : flr.getSingleFleets()) {

                LoadingInformation li = pfe.getLoadingInformation();
                for (ShipStorage ss : li.getLoadedStorage()) {
                    if (ss.getLoadtype() == ShipStorage.LOADTYPE_GROUNDTROOP) {

                        addToMap(userTroops, ss.getId(), (int) ss.getCount());
                    }
                }

                //Check for Ships in Hangar
                for (HangarRelation hr : Service.hangarRelationDAO.findByFleetId(pfe.getId())) {
                    int designId = hr.getDesignId();
                    ShipDesign sd = shipDesignDAO.findById(designId);
                    if (sd == null) {
                        DebugBuffer.error("Fleet " + pfe.getName() + " ["+pfe.getId()+"] contains ship id " + designId + " in hangar which does not exist!");
                        continue;
                    }
                    addToMap(userShipCount, sd.getChassis(), hr.getCount());
                }

                for (ShipData shipData : pfe.getShipList()) {
                    //    while (result_temp.next()) {
                    // Skip space stations
                    ShipDesign sd = shipDesignDAO.findById(shipData.getDesignId());
                    if (sd == null) {
                        DebugBuffer.error("Fleet " + pfe.getName() + " ["+pfe.getId()+"] contains shipdesing which does not exist!");
                        continue;
                    }
                    addToMap(userShipCount, sd.getChassis(), shipData.getCount());
                }
            }
            for (TradeRoute tr : tradeRouteDAO.findByUserId(currentuser)) {
                TradeRouteExt tre = new TradeRouteExt(tr);
                for (Map.Entry<ShipDesign, Integer> entry : tre.getAssignedShips().entrySet()) {

                    addToMap(userShipCount, entry.getKey().getChassis(), entry.getValue());
                }
            }

            if (spaceStations.containsKey(currentuser)) {
                HashMap<Integer, Integer> stationCount = spaceStations.get(currentuser);

                for (Map.Entry<Integer, Integer> stationEntry : stationCount.entrySet()) {
                    addToMap(userShipCount, stationEntry.getKey(), stationEntry.getValue());
                }
            }
            /*
            // ships in handelflotten finden
            // DebugBuffer.addLine("Handelsflotten");
            result_temp = stmt.executeQuery("SELECT s.count,d.chassis FROM tradefleets s LEFT JOIN shipdesigns d ON s.designId = d.id WHERE d.userId = \"" + currentuser + "\"");
            
            while (result_temp.next()) {
            addToMap(userShipCount, result_temp.getInt(2), result_temp.getInt(1));
            }
            
            
            // ships in handelsrouten finden
            //DebugBuffer.addLine("Handelsrouten");
            result_temp = stmt.executeQuery("SELECT s.number,d.chassis FROM traderouteships s LEFT JOIN shipdesigns d ON s.designId = d.id WHERE d.userId = \"" + currentuser + "\"");
            
            while (result_temp.next()) {
            addToMap(userShipCount, result_temp.getInt(2), result_temp.getInt(1));
            }
             */
            // bodentruppen checken
            //DebugBuffer.addLine("Bodentruppen");
            ArrayList<PlayerTroop> pts = playerTroopDAO.findByUserId(currentuser);
            //   result_temp = stmt.executeQuery("SELECT number, troopId FROM playertroops WHERE userId = \"" + currentuser + "\"");
            for (PlayerTroop pt : pts) {
                //     while (result_temp.next()) {
                addToMap(userTroops, pt.getTroopId(), (int) (long) pt.getNumber());
            }

            // kampfstationen einbauen
            // to be done

            // Daten des Users in Datebank schreiben

            // Add Ships
            for (Map.Entry<Integer, Integer> me : userShipCount.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.SHIPS);
                is.setSourceId(me.getKey());
                is.setValue((long) me.getValue());
                toPersist.add(is);
            }

            // Add Defence Systems
            for (Map.Entry<Integer, Integer> me : userDefense.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.DEFENCE);
                is.setSourceId(me.getKey());
                is.setValue((long) me.getValue());
                toPersist.add(is);
            }

            // Add Ressources
            for (Map.Entry<Integer, Integer> me : userRess.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.RESSOURCE);
                is.setSourceId(me.getKey());
                is.setValue((long) me.getValue());
                toPersist.add(is);
            }

            // Add Troops
            for (Map.Entry<Integer, Integer> me : userTroops.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.TROOPS);
                is.setSourceId(me.getKey());
                is.setValue((long) me.getValue());
                toPersist.add(is);
            }

            // add remaining stuff
            for (int i = 0; i < 4; i++) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.STATISTIC);
                is.setSourceId(i);
                toPersist.add(is);
                switch (i) {
                    case GameConstants.STATISTIC_POPULATION:
                        is.setValue(user_population);
                        // pstmt1.setDouble(5, user_population);
                        break;
                    case GameConstants.STATISTIC_INCOME:
                        is.setValue((long) user_income);
                        //pstmt1.setDouble(5, user_income);
                        break;
                    case GameConstants.STATISTIC_PLANETS:

                        is.setValue((long) user_planets);
                        // pstmt1.setDouble(5, user_planets);
                        break;
                    case GameConstants.STATISTIC_SYSTEMS:
                        is.setValue((long) user_systems);
                        //  pstmt1.setDouble(5, user_systems);
                        break;
                    default:
                        break;
                }

            }

            reInit();
        }
        for (ImperialStatistic is : toPersist) {
            // imperialStatisticDAO.add(is);
            //     Logger.getLogger().write("User Id = " + is.getUserId() + " sourceId : " + is.getSourceId() + " value " + is.getValue());
        }
        imperialStatisticDAO.insertAll(toPersist);
    }

    private int getCountForBuilding(int planetId, int constructionId) {
        int count = 0;

        if (planetDefense.containsKey(planetId)) {

            HashMap<Integer, Integer> constructs = planetDefense.get(planetId);
            if (constructs.containsKey(constructionId)) {
                count = constructs.get(constructionId);
            }
        }

        return count;
    }

    private void addToMap(HashMap<Integer, Integer> hm, Integer key, Integer value) {
        if (hm.containsKey(key)) {
            hm.put(key, hm.get(key) + value);
        } else {
            hm.put(key, value);
        }
    }
}
