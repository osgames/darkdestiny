/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai;

import at.viswars.service.Service;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class AIThreadHandler {
    
    
    private static ArrayList<AIThread> threads = new ArrayList<AIThread>();
    
    
    
    protected static void addThread(AIThread thread){
        getThreads().add(thread);
    }

    /**
     * @return the threads
     */
    public static ArrayList<AIThread> getThreads() {
        return threads;
    }

    public static void addThread(Integer userId) {
         long initValue = (long) (Math.random() * 60000);
         AIDebugBuffer.add(AIDebugLevel.INFO, "Starting AI thread for userId : " + userId + " initValue " + initValue);
         AIThread thread = new AIThread(Service.userDAO.findById(userId));
         thread.setName("AI"+userId);
         thread.start();
         addThread(thread);
    }  
}
