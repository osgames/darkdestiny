/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.ai.goals.AIGoal;
import at.viswars.ai.goals.IGoal;
import at.viswars.ai.goals.PlanetDevelopmentGoal;
import at.viswars.ai.goals.ResearchGoal;
import at.viswars.model.Construction;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Ressource;
import at.viswars.model.User;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.service.ConstructionService;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class AIThread extends Thread {
    private final User ai;
    private boolean run = true;
    private ArrayList<AIGoal> goals = new ArrayList<AIGoal>();
    private boolean tickCalculated = false;
    private boolean updateRunning = false;
    private boolean idle = false;
    
    public AIThread(User ai) {
        AIDebugBuffer.add(AIDebugLevel.INFO, "Initialize AI (userId: " + ai.getUserId() + ")");
        goals.add(new PlanetDevelopmentGoal());
        goals.add(new ResearchGoal());
        this.ai = ai;
    }    

    @Override
    public void run() {
        while (run) {
            try {                
                // AIDebugBuffer.add(AIDebugLevel.INFO, "Ticking AI (userId: " + ai.getUserId() + ")");
                while (tickCalculated || updateRunning) {
                    idle = true;
                    Thread.sleep(50);
                    if (!run) break;
                }
                idle = false;
                if (!run) break;
                
                DebugBuffer.addLine(DebugLevel.TRACE, ai.getGameName() + " processing ...");
                
                for (IGoal ig : goals) {
                    System.out.println("["+Thread.currentThread().getName()+"] Processing Goal " + ig.getClass());
                    ig.processGoal(ai.getUserId());
                }
                /*
                AIDebugBuffer.add(AIDebugLevel.INFO, "Got " + Service.playerPlanetDAO.findByUserId(ai.getUserId()).size() + " planets");
                for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(ai.getUserId())) {
                    planetActions(pp);
                } 
                */
                
                tickCalculated = true;

            } catch (InterruptedException ex) {
                Logger.getLogger(AIThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Do some Funky Stuff;
    }


    public void planetActions(PlayerPlanet pp) {

        AIDebugBuffer.add(AIDebugLevel.INFO, "Checking mah planet : [" + pp.getPlanetId() + "] " + pp.getName());
        try {
            PlanetCalculation pc = new PlanetCalculation(pp.getPlanetId());
            ProductionResult pcrf = pc.getProductionDataFuture();
            ProductionResult pcr = pc.getPlanetCalcData().getProductionData();

            AIDebugBuffer.add(AIDebugLevel.INFO, "Checking mah food production ");
            AIDebugBuffer.add(AIDebugLevel.INFO, "producing " + pcr.getRessProduction(Ressource.FOOD) + " consuming " + pcr.getRessConsumption(Ressource.FOOD) + " producing in future " + pcrf.getRessProduction(Ressource.FOOD));

            if (pcrf.getRessProduction(Ressource.FOOD) <= pcr.getRessConsumption(Ressource.FOOD)) {
                AIDebugBuffer.add(AIDebugLevel.INFO, "Consumption > Production in future => Act immediatly. Trying to build small agriculture");
                ArrayList<String> result = ConstructionService.buildBuilding(pp.getUserId(), pp.getPlanetId(), Construction.ID_SMALLAGRICULTUREFARM, 1);
                for (String s : result) {
                    AIDebugBuffer.add(AIDebugLevel.DEBUG, s);
                }
            } else if ((pcrf.getRessProduction(Ressource.FOOD) - pcr.getRessConsumption(Ressource.FOOD)) / (pcrf.getRessProduction(Ressource.FOOD)) < 0.05d) {
                AIDebugBuffer.add(AIDebugLevel.INFO, ((pcrf.getRessProduction(Ressource.FOOD) - pcr.getRessConsumption(Ressource.FOOD)) / (pcrf.getRessProduction(Ressource.FOOD))) + "% more production than consumption. maybe i should act. Trying to build small agriculture");
                ArrayList<String> result = ConstructionService.buildBuilding(pp.getUserId(), pp.getPlanetId(), Construction.ID_SMALLAGRICULTUREFARM, 1);
                for (String s : result) {
                    AIDebugBuffer.add(AIDebugLevel.DEBUG, s);
                }
            } else if ((pcrf.getRessProduction(Ressource.FOOD) - pcr.getRessConsumption(Ressource.FOOD)) / (pcrf.getRessProduction(Ressource.FOOD)) > 0.05d) {
                AIDebugBuffer.add(AIDebugLevel.INFO, ((pcrf.getRessProduction(Ressource.FOOD) - pcr.getRessConsumption(Ressource.FOOD)) / (pcrf.getRessProduction(Ressource.FOOD))) + "% more production than consumption totally okay");
            }

        } catch (Exception ex) {
            Logger.getLogger(AIThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param run the run to set
     */
    public void setRun(boolean run) {
        this.run = run;

    }

    /**
     * @return the ai
     */
    public User getAi() {
        return ai;
    }
    
    public void resetTick() {
        tickCalculated = false;
        updateRunning = false;
    }
    
    public void stopForUpdate() {
        updateRunning = true;
        while (!idle) {
            try {
                Thread.sleep(20);
            } catch (Exception e) {
                
            }
        }
    }    
}
