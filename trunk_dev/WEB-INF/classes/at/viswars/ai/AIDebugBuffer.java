/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai;

import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class AIDebugBuffer {
    
    
    private static TreeMap<Long, AIDebugEntry> entries = new TreeMap<Long, AIDebugEntry>();
    
    public static synchronized void add(AIDebugLevel level, String message){
        long time = System.currentTimeMillis();
        while(getEntries().containsKey(time)){
            time++;
        }
        getEntries().put(time, new AIDebugEntry(level, message));
    }

    /**
     * @return the entries
     */
    public static TreeMap<Long, AIDebugEntry> getEntries() {
        return entries;
    }
    
    
}
