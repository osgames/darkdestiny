/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai;

import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class AIMessagePayload {
    private HashMap<String,String> paramMap = new HashMap<String,String>();
    
    public void addParameter(String parName, String parValue) {
        paramMap.put(parName, parValue);
    }
    
    public String getParameter(String parName) {
        return paramMap.get(parName);
    }
    
    public HashMap<String,String> getAll() {
        return (HashMap<String,String>)paramMap.clone();
    }
}
