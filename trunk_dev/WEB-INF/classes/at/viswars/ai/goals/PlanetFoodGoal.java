/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai.goals;

import at.viswars.ai.AIDebugBuffer;
import at.viswars.ai.AIDebugLevel;
import at.viswars.ai.AIMessageHandler;
import at.viswars.ai.AIMessagePayload;
import at.viswars.ai.ThreadSharedObjects;
import at.viswars.model.Construction;
import at.viswars.model.Production;
import at.viswars.model.Ressource;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.service.ConstructionService;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class PlanetFoodGoal extends AIGoal {
    private int goalId = 2;

    @Override
    public boolean processGoal(int userId) {
        PlanetCalculation pc = (PlanetCalculation)ThreadSharedObjects.getThreadSharedObject("PlanetCalculation");        
        ProductionResult prFuture = pc.getProductionDataFuture();
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();                        
                        
        long population = pc.getPlayerPlanet().getPopulation();
        
        long foodProductionFuture = prFuture.getRessBaseProduction(Ressource.FOOD);
        long foodConsumption = pc.getPlanetCalcData().getProductionData().getRessBaseConsumption(Ressource.FOOD);
        
        long futureBalance = foodProductionFuture - foodConsumption;
        
        float foodCoverageStock = pc.getPlanetCalcData().getProductionData().getRessStock(Ressource.FOOD) 
                / pc.getPlanetCalcData().getProductionData().getRessConsumption(Ressource.FOOD);
        float planetFill = 100f / epcr.getMaxPopulation() * population;
        
        int popIncreasePerTick = (int)(population / 360d * epcr.getGrowth());
        int estimatedFoodConsInc = popIncreasePerTick / 10000;
                        
        long futureBalanceEstimated = futureBalance / estimatedFoodConsInc;
        float rateValue = futureBalanceEstimated + foodCoverageStock;
        
        /*
        boolean small = (population < 1000000) && ;
        boolean medium = (population > 1000000) && ;
        boolean large = (population > 10000000l) && ;
        */
        
        // Get building / foodcons ratio get appropriate Building
        int level = -1;
        int increaseLimit = 100;
        double ratio = 0;
        
        int[] selectedAgriculture = {Construction.ID_SMALLAGRICULTUREFARM,Construction.ID_BIGAGRICULTUREFARM,Construction.ID_AGROCOMPLEX};
        
        while ((ratio < increaseLimit) && (level < 2)) {
            level++;
            ArrayList<Production> prodList = ConstructionService.findProductionForConstructionId(pc.getPlanetId(), selectedAgriculture[level]);
            
            long foodProduction = 0;
            for (Production pe : prodList) {
                if (pe.getRessourceId() == Ressource.FOOD) {
                    foodProduction = pe.getQuantity();
                }
            }
            
            ratio = (double)(foodProduction / (long)estimatedFoodConsInc);
        }
        
        if (planetFill < 100d) {
            if (rateValue < 100) {
                if (level == 2) {
                    AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] foodCoverage < 100 => Build Complex");  
                    sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_AGROCOMPLEX,AIGoal.MESSAGE_PRIORITY_HIGH);
                } else if (level == 1) {
                    AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] foodCoverage < 100 => Build Large"); 
                    sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_BIGAGRICULTUREFARM,AIGoal.MESSAGE_PRIORITY_HIGH);
                } else {
                    AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] foodCoverage < 100 => Build Small"); 
                    sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_SMALLAGRICULTUREFARM,AIGoal.MESSAGE_PRIORITY_HIGH);
                }                
            }
        }
        
        return false;
    }    
    
    @Override
    public void evaluateGoal(int userId) {
        
    }
    
    @Override
    public int getGoalId() {
        return goalId;
    }    
    
    private void sendDirectConstructionOrder(int planetId, Integer construction, Integer priority) {
        AIMessagePayload amp = new AIMessagePayload();
        amp.addParameter("CONSTRUCTIONID", construction.toString());
        amp.addParameter("PRIORITY", priority.toString());
        
        AIMessageHandler aimh = new AIMessageHandler();
        aimh.sendMessage(goalId, 5, planetId, amp);
    }    
}
