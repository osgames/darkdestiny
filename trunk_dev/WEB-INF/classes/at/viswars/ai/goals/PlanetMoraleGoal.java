/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai.goals;

import at.viswars.ai.AIDebugBuffer;
import at.viswars.ai.AIDebugLevel;
import at.viswars.ai.AIMessageHandler;
import at.viswars.ai.AIMessagePayload;
import at.viswars.ai.ThreadSharedObjects;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetDAO;
import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.service.Service;

/**
 *
 * @author Admin
 */
public class PlanetMoraleGoal extends AIGoal {
    PlanetDAO pDAO = DAOFactory.get(PlanetDAO.class);
    
    private int goalId = 3;

    @Override
    public boolean processGoal(int userId) {
        // AIDebugBuffer.add(AIDebugLevel.INFO, "Processing PlanetMoraleGoal");        
        
        PlanetCalculation pc = (PlanetCalculation)ThreadSharedObjects.getThreadSharedObject("PlanetCalculation");
        
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        double morale = epcr.getMorale();
        double crime = epcr.getCriminalityValue();
        double crimeDev = epcr.getDevCriminality();
        double riot = epcr.getRiotValue();
        int loyality = epcr.getLoyalityValue().intValue();
        
        PlayerPlanet pp = pc.getPlayerPlanet();
        Planet p = pDAO.findById(pp.getPlanetId());
        
        int planetFill = (int)Math.floor(100l / p.getMaxPopulation() * pp.getPopulation());
        
        
        
        if ((morale > 100) && (epcr.getMaxMorale() >= 100)) {
            if (loyality < 100) {
                if (morale == epcr.getMaxMorale()) {
                    AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pp.getPlanetId()+"] Loyality < 100 && Morale >= MaxMorale => Increase Tax ("+pp.getTax()+">"+(pp.getTax()+1)+")");                    
                    pp.setTax(pp.getTax() + 1);
                }
            } else {
                AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pp.getPlanetId()+"] Morale > 100 => Increase Tax ("+pp.getTax()+">"+(pp.getTax()+1)+")");    
                pp.setTax(pp.getTax() + 1);
            }
        } else if (morale < 70) {
            AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pp.getPlanetId()+"] Morale < 70 => Decrease Tax ("+pp.getTax()+">"+(pp.getTax()-1)+")");   
            pp.setTax(pp.getTax() - 2);
        } else {
            if ((epcr.getFoodcoverageNoStock() > 1.5f) && (planetFill < 80)) { 
                AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pp.getPlanetId()+"] Enough food, planet not full => Decrease Tax ("+pp.getTax()+">"+(pp.getTax()-1)+")");   
                pp.setTax(pp.getTax() - 1);                
            }
        }
        
        /*
        if (crime > 0) {           
            AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pp.getPlanetId()+"] Crime Level Increasing => Build barracks"); 
            sendDirectConstructionOrder(pc.getPlanetId(), Construction.ID_BARRACKS, AIGoal.MESSAGE_PRIORITY_HIGH);
        }                               
        */
        
        Service.playerPlanetDAO.update(pp);
        
        return false;
    }    
    
    @Override
    public void evaluateGoal(int userId) {
        
    }
    
    @Override
    public int getGoalId() {
        return goalId;
    }    
    
    private void sendDirectConstructionOrder(int planetId, Integer construction, Integer priority) {
        AIMessagePayload amp = new AIMessagePayload();
        amp.addParameter("CONSTRUCTIONID", construction.toString());
        amp.addParameter("PRIORITY", priority.toString());
        
        AIMessageHandler aimh = new AIMessageHandler();
        aimh.sendMessage(goalId, 5, planetId, amp);
    }
}
