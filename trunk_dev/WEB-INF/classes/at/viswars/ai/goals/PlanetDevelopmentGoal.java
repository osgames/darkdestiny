/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai.goals;

import at.viswars.GameUtilities;
import at.viswars.ai.AIDebugBuffer;
import at.viswars.ai.AIDebugLevel;
import at.viswars.ai.ThreadSharedObjects;
import at.viswars.model.PlayerPlanet;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.service.Service;

/**
 *
 * @author Admin
 */
public class PlanetDevelopmentGoal extends AIGoal {
    private int goalId = 1;

    public PlanetDevelopmentGoal() {
        addSubGoal(new PlanetMoraleGoal());
        addSubGoal(new PlanetResourceGoal());
        addSubGoal(new PlanetFoodGoal());
        addSubGoal(new PlanetConstructionGoal());
    }
    
    @Override
    public boolean processGoal(int userId) {
        System.out.println("----------------------- PROCESS START -------------------------");
        System.out.println(GameUtilities.getCurrentTick2());
        AIDebugBuffer.add(AIDebugLevel.INFO, "Processing PlanetDevelopmentGoal ["+userId+"]");
        
        for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)) {
            // AIDebugBuffer.add(AIDebugLevel.INFO, "Processing planet " + pp.getPlanetId());
            
            try {
                PlanetCalculation pc = new PlanetCalculation(pp.getPlanetId());
                ThreadSharedObjects.registerThreadSharedObject("PlanetCalculation", pc);
                
                for (IGoal ig : getSubGoals()) {                    
                    boolean result = ig.processGoal(userId);
                }            
                
                ThreadSharedObjects.removeThreadSharedObject("PlanetCalculation");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }         
                
        System.out.println("----------------------- PROCESS  END  -------------------------");
        return false;
    }    
    
    @Override
    public void evaluateGoal(int userId) {
        AIDebugBuffer.add(AIDebugLevel.INFO, "Evaluating PlanetDevelopmentGoal");
    }
    
    @Override
    public int getGoalId() {
        return goalId;
    }    
}
