/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai.goals;

import at.viswars.ai.AIThread;
import at.viswars.model.Research;
import at.viswars.model.ResearchProgress;
import at.viswars.service.ResearchService;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ResearchGoal extends AIGoal {
    private final int goalId = 6;
    
    @Override
    public boolean processGoal(int userId) {
        AIThread context = (AIThread)Thread.currentThread();
        
        // Take the 2 cheapest researches available and start them
        // If 2 researches are already beeing processed - do nothing
        // If only computer research points are left => enforce building of 
        // planetary supercomputers
        ArrayList<ResearchProgress> rpList = ResearchService.getResearchInProgress(context.getAi().getUserId());
        System.out.println("RP List Size: " + rpList.size());
        
        if (rpList.size() >= 2) return false;
        
        int addResearches = 2 - rpList.size();
        int targetResearchId = -1;
        
        if (!ResearchService.isResearched(context.getAi().getUserId(), Research.ID_FUSIONPLANT)) {
            targetResearchId = Research.ID_FUSIONPLANT;    
        } else if (!ResearchService.isResearched(context.getAi().getUserId(), Research.ID_ORBITALKOLONIE)) {
            targetResearchId = Research.ID_ORBITALKOLONIE;  
        } else if (!ResearchService.isResearched(context.getAi().getUserId(), Research.ID_FIGHTER)) {
            targetResearchId = Research.ID_FIGHTER;  
        } else if (!ResearchService.isResearched(context.getAi().getUserId(), Research.ID_COLONIZATION)) {
            targetResearchId = Research.ID_COLONIZATION;  
        } else if (!ResearchService.isResearched(context.getAi().getUserId(), Research.ID_DESTROYER)) {
            targetResearchId = Research.ID_DESTROYER;  
        } else if (!ResearchService.isResearched(context.getAi().getUserId(), Research.ID_BATTLESHIP)) {
            targetResearchId = Research.ID_BATTLESHIP;  
        }
        
        System.out.println("Research " + targetResearchId);
        
        ArrayList<Research> rList = ResearchService.getAllRequiredResearchesForResearch(targetResearchId);
        for (Research r : rList) {
            System.out.println("Loop " + r.getName());
            if (!ResearchService.isResearched(context.getAi().getUserId(), r.getId())) {
                System.out.println(r.getName() + " not researched");
                if (ResearchService.isPossibleToResearch(0, r.getId(), context.getAi().getUserId())) {
                    System.out.println(r.getName() + " in starting");
                    if (addResearches > 0) {
                        ResearchService.processAction(context.getAi().getUserId(), r.getId(), 0, ResearchService.PROCESS_START);
                        addResearches--;
                    }
                }
            }
        }
        
        return false;
    }    
    
    @Override
    public void evaluateGoal(int userId) {
        
    }    
    
    public int getGoalId() {
        return goalId;
    }
}
