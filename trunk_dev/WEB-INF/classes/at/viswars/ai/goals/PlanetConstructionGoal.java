/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai.goals;

import at.viswars.Buildable;
import at.viswars.DebugBuffer;
import at.viswars.LongObj;
import at.viswars.ai.AIDebugBuffer;
import at.viswars.ai.AIDebugLevel;
import at.viswars.ai.AIMessageHandler;
import at.viswars.ai.AIThread;
import at.viswars.ai.ThreadSharedObjects;
import at.viswars.ai.message.AIMessage;
import at.viswars.ai.message.ConstructMessage;
import at.viswars.enumeration.EConstructionType;
import at.viswars.model.Construction;
import at.viswars.model.PlanetConstruction;
import at.viswars.model.Production;
import at.viswars.model.Ressource;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.result.BuildableResult;
import at.viswars.result.InConstructionEntry;
import at.viswars.result.InConstructionResult;
import at.viswars.result.ProductionBuildResult;
import at.viswars.service.ConstructionService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class PlanetConstructionGoal extends AIGoal {

    private int goalId = 5;
    private AIThread context;
    private HashMap<Integer, ArrayList<ConstructMessage>> orders = new HashMap<Integer, ArrayList<ConstructMessage>>();
    private AIMessageHandler aimh = null;
    PlanetCalculation pc = null;
    private int currQueueLength = 0;
    boolean researchInQueue = false;
    boolean foodInQueue = false;
    boolean industryInQueue = false;
    boolean energyInQueue = false;

    @Override
    public boolean processGoal(int userId) {        
        context = (AIThread) Thread.currentThread();
        orders.clear();
        currQueueLength = 0;
        pc = (PlanetCalculation) ThreadSharedObjects.getThreadSharedObject("PlanetCalculation");
        
        if (pc.getPlanetId() == 2285) System.out.println("Processing ConstructionGoal of Planet " + pc.getPlanetId());
        
        aimh = null;

        ProductionResult prFuture = pc.getProductionDataFuture();
        ProductionResult prNow = pc.getPlanetCalcData().getProductionData();

        LongObj energyBalance = new LongObj(prNow.getRessProduction(Ressource.ENERGY) - prNow.getRessConsumption(Ressource.ENERGY));

        // 0 is highest - 1 lowest
        double priorityAgriculture = 0.1d;
        double priorityResearch = 0.3d;
        double priorityIndustry = 0.5d;

        InConstructionResult icr = ConstructionService.findRunnigConstructions(pc.getPlanetId());
        currQueueLength = icr.getEntries().size();

        processMessages();
        boolean newBuildings = false;
        boolean newBldgTmp;
        
        if (pc.getPlanetId() == 2285) System.out.println("START ENERGY BALANCE: " + energyBalance.getLong());
        
        do {
            researchInQueue = false;
            foodInQueue = false;
            industryInQueue = false;           
            energyInQueue = false;
            
            for (InConstructionEntry ice : icr.getEntries()) {
                Construction c = (Construction) ice.getUnit();

                switch (c.getId()) {
                    case (Construction.ID_AGROCOMPLEX):
                    case (Construction.ID_BIGAGRICULTUREFARM):
                    case (Construction.ID_SMALLAGRICULTUREFARM):
                        if (pc.getPlanetId() == 2285) System.out.println("Food in Queue");
                        foodInQueue = true;
                        break;
                    case (Construction.ID_COMPLEXIRONMINE):
                    case (Construction.ID_BIGIRONMINE):
                    case (Construction.ID_SMALLIRONMINE):
                    case (Construction.ID_STEELCOMPLEX):
                    case (Construction.ID_STEELFACTORY):
                    case (Construction.ID_STEELCOMPACTOR):
                        if (pc.getPlanetId() == 2285) System.out.println("Industry in Queue");
                        industryInQueue = true;
                        break;
                    case (Construction.ID_RESEARCHLABORATORY):
                        if (pc.getPlanetId() == 2285) System.out.println("Research in Queue");
                        researchInQueue = true;
                        break;
                    case (Construction.ID_NUCLEARPOWERPLANT):
                    case (Construction.ID_SOLARPLANT):
                    case (Construction.ID_FUSIONPOWERPLANT):
                        if (pc.getPlanetId() == 2285) System.out.println("Energy in Queue");
                        energyInQueue = true;
                        break;
                }
            }
            
            newBldgTmp = construct();            
            if (newBldgTmp) icr = ConstructionService.findRunnigConstructions(pc.getPlanetId());
            
            newBuildings = newBuildings || newBldgTmp;
            if (pc.getPlanetId() == 2285) System.out.println("New buildings = " + newBuildings);
        } while (newBldgTmp);

        // --------------------------------------------------------------
        // Order construction Queue by Priority and Energy demands        
        // --------------------------------------------------------------        
        // Step 1 - Split all orders into their respective areas
        // Step 2a - Count up priority, set buildings 
        // Step 2b - Add Energy if needed
        // Step 3 - Compress Queue
        ConstructionService.compressOrders(context.getAi().getUserId(), pc.getPlanetId());
        
        // --------------------------------------------------------------
        // Step 1
        // --------------------------------------------------------------
        if (true || newBuildings) {
            ArrayList<InConstructionEntry> agrarInCons = new ArrayList<InConstructionEntry>();
            ArrayList<InConstructionEntry> researchInCons = new ArrayList<InConstructionEntry>();
            ArrayList<InConstructionEntry> industryInCons = new ArrayList<InConstructionEntry>();
            ArrayList<InConstructionEntry> energyInCons = new ArrayList<InConstructionEntry>();

            for (InConstructionEntry ice : icr.getEntries()) {
                Construction c = (Construction) ice.getUnit();

                switch (c.getId()) {
                    case (Construction.ID_AGROCOMPLEX):
                    case (Construction.ID_BIGAGRICULTUREFARM):
                    case (Construction.ID_SMALLAGRICULTUREFARM):
                        agrarInCons.add(ice);
                        break;
                    case (Construction.ID_COMPLEXIRONMINE):
                    case (Construction.ID_BIGIRONMINE):
                    case (Construction.ID_SMALLIRONMINE):
                    case (Construction.ID_STEELCOMPLEX):
                    case (Construction.ID_STEELFACTORY):
                    case (Construction.ID_STEELCOMPACTOR):
                        industryInCons.add(ice);
                        break;
                    case (Construction.ID_RESEARCHLABORATORY):
                        researchInCons.add(ice);
                        break;
                    case (Construction.ID_NUCLEARPOWERPLANT):
                    case (Construction.ID_SOLARPLANT):
                    case (Construction.ID_FUSIONPOWERPLANT):
                        energyInCons.add(ice);
                        break;                        
                }            
            }
            
            // --------------------------------------------------------------
            // Step 2
            // --------------------------------------------------------------         
            int actPrio = 0;
            double agrarCnt = priorityAgriculture;
            double industryCnt = priorityIndustry;
            double researchCnt = priorityResearch;
            
            if (agrarInCons.isEmpty()) agrarCnt = 999;
            if (researchInCons.isEmpty()) researchCnt = 999;
            if (industryInCons.isEmpty()) industryCnt = 999;
            
            // Check queue by order
            while ((agrarInCons.size() + researchInCons.size() + industryInCons.size()) > 0) {
                if (pc.getPlanetId() == 2285) System.out.println("CURRENT ENERGY BALANCE: " + energyBalance.getLong());
                
                if ((agrarCnt <= industryCnt) && (agrarCnt <= researchCnt) && !agrarInCons.isEmpty()) {
                    // Build agriculture                         
                    if (foodInQueue) {
                        InConstructionEntry ice = getMostAdvancedConstructionEntry(agrarInCons,energyBalance);       
                        if (ice != null) {
                            if (pc.getPlanetId() == 2285) System.out.println("Set Agriculture on priority " + actPrio);
                            ConstructionService.setNewPriority(ice.getOrder().getId(), actPrio);
                            agrarCnt += priorityAgriculture;
                            actPrio++;
                        } else {
                            // Build energy
                            // If energy in queue prioritize else build new
                            if ((energyInQueue) && energyInCons.size() > 0) {
                                InConstructionEntry ice2 = getMostAdvancedConstructionEntry(energyInCons,energyBalance); 
                                ConstructionService.setNewPriority(ice2.getOrder().getId(), actPrio);
                            } else {
                                ConstructionService.buildBuilding(context.getAi().getUserId(), pc.getPlanetId(), Construction.ID_NUCLEARPOWERPLANT, 1, actPrio);
                                energyBalance.addLong(getEnergyValue(Construction.ID_NUCLEARPOWERPLANT));                                
                            }
                            
                            if (pc.getPlanetId() == 2285) System.out.println("Set Energy on priority " + actPrio);
                            actPrio++;
                        }
                    }
                    
                    if (agrarInCons.isEmpty()) agrarCnt = 999;
                } else if ((industryCnt <= agrarCnt) && (industryCnt <= researchCnt) && !industryInCons.isEmpty()) {
                    // Build industry
                    if (industryInQueue) {
                        InConstructionEntry ice = getMostAdvancedConstructionEntry(industryInCons,energyBalance);
                        if (ice != null) {
                            if (pc.getPlanetId() == 2285) System.out.println("Set Industry on priority " + actPrio);
                            ConstructionService.setNewPriority(ice.getOrder().getId(), actPrio);
                            industryCnt += priorityIndustry;
                            actPrio++;
                        } else {
                            // If energy in queue prioritize else build new
                            if ((energyInQueue) && energyInCons.size() > 0) {
                                InConstructionEntry ice2 = getMostAdvancedConstructionEntry(energyInCons,energyBalance); 
                                ConstructionService.setNewPriority(ice2.getOrder().getId(), actPrio);
                            } else {
                                ConstructionService.buildBuilding(context.getAi().getUserId(), pc.getPlanetId(), Construction.ID_NUCLEARPOWERPLANT, 1, actPrio);
                                energyBalance.addLong(getEnergyValue(Construction.ID_NUCLEARPOWERPLANT));                                
                            }
                            
                            if (pc.getPlanetId() == 2285) System.out.println("Set Energy on priority " + actPrio);
                            actPrio++;
                        }
                    }    
                    
                    if (industryInCons.isEmpty()) industryCnt = 999;
                } else if ((researchCnt <= industryCnt) && (researchCnt <= agrarCnt) && !researchInCons.isEmpty()) {
                    // Build research
                    if (researchInQueue) {
                        InConstructionEntry ice = getMostAdvancedConstructionEntry(researchInCons,energyBalance);
                        if (ice != null) {
                            if (pc.getPlanetId() == 2285) System.out.println("Set Research on priority " + actPrio);
                            ConstructionService.setNewPriority(ice.getOrder().getId(), actPrio);
                            researchCnt += priorityResearch;
                            actPrio++;
                        } else {
                            // If energy in queue prioritize else build new
                            if ((energyInQueue) && energyInCons.size() > 0) {
                                InConstructionEntry ice2 = getMostAdvancedConstructionEntry(energyInCons,energyBalance); 
                                ConstructionService.setNewPriority(ice2.getOrder().getId(), actPrio);
                            } else {
                                ConstructionService.buildBuilding(context.getAi().getUserId(), pc.getPlanetId(), Construction.ID_NUCLEARPOWERPLANT, 1, actPrio);
                                energyBalance.addLong(getEnergyValue(Construction.ID_NUCLEARPOWERPLANT));                                 
                            }
                            
                            if (pc.getPlanetId() == 2285) System.out.println("Set Energy on priority " + actPrio);
                            actPrio++;
                        }
                    }                    
                    
                    if (researchInCons.isEmpty()) researchCnt = 999;
                }
            }
            
            if (pc.getPlanetId() == 2285) System.out.println("Not processed orders: " + (agrarInCons.size() + researchInCons.size() + industryInCons.size() + energyInCons.size()));         
        }
        
        // All existing orders have been queued
        // New orders can be added now

        
        ConstructionService.compressOrders(context.getAi().getUserId(), pc.getPlanetId());
        
        return false;
    }
    
    private InConstructionEntry getMostAdvancedConstructionEntry(ArrayList<InConstructionEntry> selectionList, LongObj energyBalance) {
        InConstructionEntry selected = null;
        int actProgress = 0;
        
        if (pc.getPlanetId() == 2285) System.out.println("SELECTION LIST SIZE: " + selectionList.size());
        
        for (InConstructionEntry iceTmp : selectionList) {
            if (iceTmp.getOrder().getIndProc() >= actProgress) {
                selected = iceTmp;
                actProgress = iceTmp.getOrder().getIndProc();
            }
        }
        
        // Get Energy consumption of building
        long currEnergyValue = getEnergyValue(((Construction)selected.getUnit()).getId());
        
        if (pc.getPlanetId() == 2285) System.out.println("CB: " + energyBalance.getLong() + " Cons/Prod of current: " + currEnergyValue);
                
        if (energyBalance.getLong() < 0 && currEnergyValue < 0) {
            return null;
        } else {
            energyBalance.addLong(currEnergyValue);
        }
        
        selectionList.remove(selected);
        
        return selected;
    }
    
    private long getEnergyValue(int constructionId) {
        ArrayList<Production> prodList = ConstructionService.findProductionForConstructionId(pc.getPlanetId(), constructionId);
        for (Production p : prodList) {
            if (p.getRessourceId() == Ressource.ENERGY) {
                if (p.getDecinc() == Production.PROD_DECREASE) {                    
                    return -p.getQuantity();
                } else if (p.getDecinc() == Production.PROD_INCREASE) {
                    return p.getQuantity();
                }
            }
        }        
        
        return 0;
    }
    
    private boolean construct() {
        // --------------------------------------------------------------
        // Research Construction
        // --------------------------------------------------------------
        // 10% of available workers will be spent on Research
        // Skip if there is already a research lab beeing built
        int usedWorkers = pc.getPlanetCalcData().getExtPlanetCalcData().getUsedPopulation();
        int availWorkers = pc.getPlayerPlanet().getPopActionPoints();
        int resPercentageGoal = (int) (availWorkers / 10d);
        int currResPercentage = 0;

        TreeMap<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>> constructions = ConstructionService.findPlanetConstructions(pc.getPlanetId());
        // If research takes less than 10% of Workers build research 
        for (Map.Entry<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>> entry : constructions.entrySet()) {
            if (entry.getKey() == EConstructionType.ZIVIL) {
                for (ArrayList<PlanetConstruction> actList : entry.getValue().values()) {
                    for (PlanetConstruction pcons : actList) {
                        if (pcons.getConstructionId() == Construction.ID_RESEARCHLABORATORY) {
                            currResPercentage += 2000 * pcons.getNumber();
                        }
                    }
                }
            }
        }

        System.out.println("[" + pc.getPlanetId() + "] CRP: " + currResPercentage + " RPG: " + resPercentageGoal);

        if (currResPercentage < resPercentageGoal) {
            // and nothing is already built => issue order
            if (!researchInQueue) {
                if (ConstructionService.buildBuilding(context.getAi().getUserId(), pc.getPlanetId(), Construction.ID_RESEARCHLABORATORY, 1).isEmpty()) {
                    currQueueLength++;
                }
            }
        }

        // --------------------------------------------------------------
        // Generate Construction from Messages
        // --------------------------------------------------------------
        for (int prio = AIGoal.MESSAGE_PRIORITY_HIGHEST; prio > AIGoal.MESSAGE_PRIORITY_LOWEST; prio--) {
            ArrayList<ConstructMessage> cMsgs = orders.get(prio);

            if (cMsgs == null) {
                continue;
            }

            for (ConstructMessage cm : cMsgs) {
                if (currQueueLength >= 5) break;
                // System.out.println("Process Message " + cm.getMessageId() + " -- Constructing " + cm.getConstructionId() + " on Planet " + cm.getPlanetId());

                ArrayList<String> result = ConstructionService.buildBuilding(context.getAi().getUserId(), cm.getPlanetId(), cm.getConstructionId(), 1);
                if (result.isEmpty()) {
                    // System.out.println("Try to delete Message ID: " + cm.getMessageId());
                    aimh.deleteMessage(cm.getMessageId());
                    currQueueLength++;
                    AIDebugBuffer.add(AIDebugLevel.INFO, "Successfully built " + cm.getConstructionId() + " on planet " + cm.getPlanetId());
                } else {
                    for (String error : result) {
                        AIDebugBuffer.add(AIDebugLevel.INFO, "Could not build " + cm.getConstructionId() + " on planet " + cm.getPlanetId() + ": " + error);
                    }
                }
            }
        }

        // Check for Planetary Management 
        ProductionBuildResult pbr = ConstructionService.getConstructionList(context.getAi().getUserId(), pc.getPlanetId(), EConstructionType.ADMINISTRATION);
        for (Buildable b : pbr.getUnits()) {
            BuildableResult br = pbr.getBuildInfo(b);
            if ((br.getPossibleCount() == 1) && (((Construction) b.getBase()).getId() == Construction.ID_LOCALADMINISTRATION)) {
                ConstructionService.buildBuilding(context.getAi().getUserId(), pc.getPlanetId(), Construction.ID_LOCALADMINISTRATION, 1, 1);
            }
            if ((br.getPossibleCount() == 1) && (((Construction) b.getBase()).getId() == Construction.ID_PLANETADMINISTRATION)) {
                ConstructionService.buildBuilding(context.getAi().getUserId(), pc.getPlanetId(), Construction.ID_PLANETADMINISTRATION, 1, 1);
            }
            if ((br.getPossibleCount() == 1) && (((Construction) b.getBase()).getId() == Construction.ID_PLANETGOVERNMENT)) {
                ConstructionService.buildBuilding(context.getAi().getUserId(), pc.getPlanetId(), Construction.ID_PLANETGOVERNMENT, 1, 1);
            }
        }

        return false;
    }

    private void processMessages() {
        aimh = new AIMessageHandler();
        ArrayList<AIMessage> msgListIn = aimh.getReceivedMessages(goalId, pc.getPlanetId());

        try {
            for (AIMessage aim : msgListIn) {
                if (aim instanceof ConstructMessage) {
                    ConstructMessage cmsg = (ConstructMessage) aim;

                    ArrayList<ConstructMessage> cMsgs = orders.get(cmsg.getPriority());
                    if (cMsgs == null) {
                        cMsgs = new ArrayList<ConstructMessage>();
                    }

                    cMsgs.add(cmsg);
                    orders.put(cmsg.getPriority(), cMsgs);

                    AIDebugBuffer.add(AIDebugLevel.INFO, "Adding " + aim + " in goal " + goalId + " from sender " + aim.getSenderId() + " to construction queue");
                }
            }
        } catch (Exception e) {
            DebugBuffer.error("Error at parsing Messages: ", e);
        }
    }

    @Override
    public void evaluateGoal(int userId) {
    }

    @Override
    public int getGoalId() {
        return goalId;
    }
}
