/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai.goals;

import at.viswars.ai.AIDebugBuffer;
import at.viswars.ai.AIDebugLevel;
import at.viswars.ai.AIMessageHandler;
import at.viswars.ai.AIMessagePayload;
import at.viswars.ai.ThreadSharedObjects;
import at.viswars.model.Construction;
import at.viswars.model.Ressource;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;

/**
 *
 * @author Admin
 */
public class PlanetResourceGoal extends AIGoal {
    private int goalId = 4;

    @Override
    public boolean processGoal(int userId) {
        PlanetCalculation pc = (PlanetCalculation)ThreadSharedObjects.getThreadSharedObject("PlanetCalculation");        
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();   
        
        long population = pc.getPlayerPlanet().getPopulation();
        double baseValue = Math.max(1d,Math.log10(population) - 3);        
        
        int targetBaseStorage = (int)(Math.pow(baseValue, 2d) * 50000);
        int targetSpecialStorage = 0;
        int targetIronProduction = (int)(Math.pow(baseValue, 2d) * 400);
        int targetSteelProduction = (int)(Math.pow(baseValue, 2d) * 150);;
        int targetTerkonitProduction = 0;        
        
        ProductionResult pr = pc.getProductionDataFuture();
        
        // Build Storage
        if (pr.getRessMaxStock(Ressource.IRON) < targetBaseStorage) {
            if (pr.getRessMaxStock(Ressource.IRON) < 200000) {
                AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] Build small storage"); 
                sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_SMALLRESSOURCESTORAGE,AIGoal.MESSAGE_PRIORITY_LOW);
            } else {
                AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] Build large storage"); 
                sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_BIGRESSOURCESTORAGE,AIGoal.MESSAGE_PRIORITY_LOW);
            }
        }

        System.out.println("Future Iron prod ["+pc.getPlanetId()+"]: " + pr.getRessBaseProduction(Ressource.IRON) + " Target="+targetIronProduction);
        
        // Build Iron Mines
        if (pr.getRessBaseProduction(Ressource.IRON) < targetIronProduction) {
            if (pr.getRessBaseProduction(Ressource.IRON) < 500) {
                // Build small
                 AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] Build small iron mine"); 
                sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_SMALLIRONMINE,AIGoal.MESSAGE_PRIORITY_MEDIUM);
            } else if (pr.getRessBaseProduction(Ressource.IRON) < 3000) {
                // Build medium
                AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] Build large iron mine"); 
                sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_BIGIRONMINE,AIGoal.MESSAGE_PRIORITY_MEDIUM);
            } else {
                // Build Large
                AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] Build iron mine complex"); 
                sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_COMPLEXIRONMINE,AIGoal.MESSAGE_PRIORITY_MEDIUM);
            }
        }
        
        // Build Steel mills
        int steelstorageLevel = (int)(100d / pr.getRessMaxStock(Ressource.STEEL) * pr.getRessStock(Ressource.STEEL));
        
        if ((pr.getRessBaseProduction(Ressource.STEEL) < targetSteelProduction) && (steelstorageLevel < 50)) {
            if (pr.getRessProduction(Ressource.IRON) < 2000) {
                // Build small
                AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] Build small steel mill"); 
                sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_STEELFACTORY,AIGoal.MESSAGE_PRIORITY_MEDIUM);
            } else {
                // Build Large
                AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pc.getPlanetId()+"] Build steel complex"); 
                sendDirectConstructionOrder(pc.getPlanetId(),Construction.ID_STEELCOMPLEX,AIGoal.MESSAGE_PRIORITY_MEDIUM);
            }            
        }
        
        return false;
    }    
    
    @Override
    public void evaluateGoal(int userId) {
        
    }
    
    @Override
    public int getGoalId() {
        return goalId;
    }    
    
    private void sendDirectConstructionOrder(int planetId, Integer construction, Integer priority) {
        AIMessagePayload amp = new AIMessagePayload();
        amp.addParameter("CONSTRUCTIONID", construction.toString());
        amp.addParameter("PRIORITY", priority.toString());
        
        AIMessageHandler aimh = new AIMessageHandler();
        aimh.sendMessage(goalId, 5, planetId, amp);
    }    
}
