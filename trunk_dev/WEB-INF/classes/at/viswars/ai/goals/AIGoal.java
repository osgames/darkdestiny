/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai.goals;

import at.viswars.ai.AIAction;
import at.viswars.ai.goals.IGoal;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public abstract class AIGoal implements IGoal {
    private ArrayList<AIAction> actions = new ArrayList<AIAction>();
    private ArrayList<AIGoal> subGoals = new ArrayList<AIGoal>();
    
    private double priority = 0d;
    
    public final static int MESSAGE_PRIORITY_LOWEST = 1;
    public final static int MESSAGE_PRIORITY_LOW = 2;
    public final static int MESSAGE_PRIORITY_MEDIUM = 3;
    public final static int MESSAGE_PRIORITY_HIGH = 4;
    public final static int MESSAGE_PRIORITY_HIGHEST = 5;
    
    // Get priority of goal (0-1)
    @Override
    public double getPriority() {
        return this.priority;
    }
    
    // Adjust priority of this goal (0-1)
    @Override
    public void setPriority(double priority) {
        if ((priority >= 0d) && (priority <= 1d)) {
            this.priority = priority;
        }  
    }
    
    // Returns true if goal could be successfully applied
    @Override
    public abstract boolean processGoal(int userId); 
    
    @Override
    public abstract void evaluateGoal(int userId);
    
    public abstract int getGoalId();

    /**
     * @return the actions
     */
    public ArrayList<AIAction> getActions() {
        return actions;
    }

    /**
     * @return the subGoals
     */
    public ArrayList<AIGoal> getSubGoals() {
        return subGoals;
    }
    
    public void addSubGoal(AIGoal goal) {
        subGoals.add(goal);
    }
    
    public void addAction(AIAction action) {
        actions.add(action);
    }
}
