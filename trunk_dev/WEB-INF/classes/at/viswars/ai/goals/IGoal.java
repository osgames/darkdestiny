/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ai.goals;

/**
 *
 * @author Admin
 */
public interface IGoal {
    // Get priority of goal (0-1)
    public double getPriority();
    
    // Adjust priority of this goal (0-1)
    public void setPriority(double priority);
    
    // Returns true if goal could be successfully applied
    public boolean processGoal(int userId);
    
    // Perform some checks to set the importance of this goal
    public void evaluateGoal(int userId);
}
