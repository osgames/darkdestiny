/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.model.Model;
import at.viswars.ships.BuildTime;

/**
 *
 * @author Stefan
 */
public interface Buildable {
    public RessourcesEntry getRessCost();
    public Model getBase();
    public BuildTime getBuildTime();
}
