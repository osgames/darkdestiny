/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.interfaces;

import at.viswars.databuffer.fleet.RelativeCoordinate;

/**
 *
 * @author Stefan
 */
public interface IStellarBody extends ILocation {
    public RelativeCoordinate getRelativeCoordinate();
}
