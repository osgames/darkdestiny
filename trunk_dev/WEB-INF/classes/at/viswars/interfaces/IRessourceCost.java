package at.viswars.interfaces;

import at.viswars.databuffer.RessAmountEntry;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Beschreibt Ressourcen-Kosten / Guthaben
 * für alle möglichen Operationen
 * 
 * @author martin
 */
public interface IRessourceCost {
	public double getRess(int ressId);
        public HashMap<Integer,RessAmountEntry> getRess();
        public ArrayList<RessAmountEntry> getRessArray();
}