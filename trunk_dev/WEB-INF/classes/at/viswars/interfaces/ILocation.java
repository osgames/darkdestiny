/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.interfaces;

import at.viswars.databuffer.fleet.AbsoluteCoordinate;

/**
 *
 * @author Stefan
 */
public interface ILocation {
    public AbsoluteCoordinate getAbsoluteCoordinate();
}
