/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.Threading;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class CopyJob {
    private final int systemId;
    private final int time;
    private final ArrayList<Integer> toUsers;
    
    public CopyJob(ArrayList<Integer> toUsers, int systemId, int time) {
        this.toUsers = toUsers;
        this.systemId = systemId;
        this.time = time;
    }

    public ArrayList<Integer> getToUsers() {
        return toUsers;
    }

    public int getSystemId() {
        return systemId;
    }

    public int getTime() {
        return time;
    }
}
