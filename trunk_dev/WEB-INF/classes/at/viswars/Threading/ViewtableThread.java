/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.Threading;

import at.viswars.DebugBuffer;
import at.viswars.interfaces.IDDThread;
import at.viswars.utilities.ViewTableUtilities;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ViewtableThread extends Thread implements Runnable, IDDThread {

    private ArrayList<CopyJob> jobs = new ArrayList<CopyJob>();
    private boolean running = true;
    private final Object mutex = new Object();

    protected ViewtableThread() {
        // mutex = new Object();
        setPriority(Thread.MIN_PRIORITY);
    }

    @Override
    public void run() {
        // synchronized(mutex) {
        while (running) {
            if (jobs.size() > 0) {
                CopyJob cj = jobs.get(0);

                // PROCESS JOB
                for (Integer userId : cj.getToUsers()) {
                    ViewTableUtilities.addOrRefreshSystemForUser(userId, cj.getSystemId(), cj.getTime());
                    try {
                        Thread.sleep(10);
                    } catch (Exception e) {
                    }
                }

                // DebugBuffer.addLine(DebugLevel.DEBUG, "Finished copy job for system " + cj.getSystemId() + " to "+cj.getToUsers().size()+" users");
                jobs.remove(0);
            } else {
                while (jobs.isEmpty()) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                    }
                    // DebugBuffer.addLine(DebugLevel.DEBUG, "ViewTableThread -> switch to IDLE mode");
                    // mutex.wait();
                }
            }
        }
        // }
    }

    public void addJob(CopyJob cj) {
        synchronized (mutex) {
            jobs.add(cj);
            // mutex.notifyAll();
        }
    }

    public void addJobBatch(ArrayList<CopyJob> cjBatch) {
        synchronized (mutex) {
            jobs.addAll(cjBatch);
            // mutex.notifyAll();
        }
    }

    public void stopMe() {
        synchronized (mutex) {
            running = false;
            // mutex.notifyAll();
        }
    }
}
