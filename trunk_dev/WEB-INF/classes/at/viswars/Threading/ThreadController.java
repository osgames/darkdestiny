/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.Threading;

import at.viswars.interfaces.IDDThread;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ThreadController {
    private static ArrayList<IDDThread> activeWorkerThreads = new ArrayList<IDDThread>();
    
    public static ViewtableThread getViewTableThread() {
        ViewtableThread vtt = null;
        
        for (IDDThread thread : activeWorkerThreads) {
            if (thread instanceof ViewtableThread) {
                vtt = (ViewtableThread)thread;
            }
        }
        
        if (vtt == null) {
            vtt = new ViewtableThread();
            vtt.setName("Viewtable Updater");
            vtt.start();
            activeWorkerThreads.add(vtt);            
        }
        
        return vtt;
    }

    public static void stopAllThreads() {
        for (IDDThread thread : activeWorkerThreads) {
            thread.stopMe();
        }
    }
}
