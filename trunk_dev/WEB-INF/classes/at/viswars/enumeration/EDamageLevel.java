/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.enumeration;

/**
 *
 * @author Stefan
 */
public enum EDamageLevel {
    NODAMAGE, MINORDAMAGE, LIGHTDAMAGE, MEDIUMDAMAGE, HEAVYDAMAGE, DESTROYED;
    
}
