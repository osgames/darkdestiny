/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.enumeration;

/**
 *
 * @author Stefan
 */
public enum EOwner {
    AGGRESSIVE,
    FRIENDLY,
    WAR,
    NEUTRAL, 
    TRADE,
    ALLY,
    TREATY,
    NAP,
    OWN,
    INHABITATED
}
