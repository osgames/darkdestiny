/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.enumeration;

/**
 *
 * @author Bullet
 */
public enum EActionType {
    BUILDING, RESEARCH, MODULE, SHIP, GROUNDTROOPS, FLIGHT, DEFENSE, SPYRELATED, TRADE,
    DECONSTRUCT, PROBE, SPACESTATION, SCAN_SYSTEM, SCAN_PLANET
}
