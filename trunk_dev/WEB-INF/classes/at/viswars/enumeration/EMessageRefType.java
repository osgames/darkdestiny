/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.enumeration;

/**
 *
 * @author Stefan
 */
public enum EMessageRefType {
    NOTHING, VOTE, ALLIANCEBOARD_ID_ADD, ALLIANCEBOARD_ID_DELETE, TERRITORY_ID_ADD, TERRITORY_ID_DELETE;
}
