package at.viswars.groundcombat;

import at.viswars.GameConstants;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetLoyalityDAO;
import at.viswars.model.*;
import at.viswars.service.Service;
import java.util.ArrayList;

public class MilizTraining extends Service {

    private static PlanetLoyalityDAO pLoyDAO = (PlanetLoyalityDAO)DAOFactory.get(PlanetLoyalityDAO.class);
    
    private static ArrayList<PlayerTroop> toAdd;
    private static ArrayList<PlayerTroop> toUpdate;
    private static ArrayList<PlayerTroop> toRemove;



    public static void updateMiliz(ArrayList<PlayerPlanet> ppList) {

        toAdd = new ArrayList<PlayerTroop>();
        toRemove = new ArrayList<PlayerTroop>();
        toUpdate = new ArrayList<PlayerTroop>();
        for (PlayerPlanet pp : (ArrayList<PlayerPlanet>) ppList) {
            if(groundCombatDAO.findRunningCombat(pp.getPlanetId()) != null){
                continue;
            }
            processPlanet(pp.getPlanetId(), pp);
        }
        //Add when implemented
        //playerTroopDAO.removeAll(toRemove);
        playerTroopDAO.updateAll(toUpdate);
        playerTroopDAO.insertAll(toAdd);

    }
    private static final long TOTAL_MAX_MILIZ = 5l;

    public static void processPlanet(int planetId, PlayerPlanet pp) {
        PlanetConstruction barracks = Service.planetConstructionDAO.findBy(planetId, Construction.ID_BARRACKS);
        if (barracks == null) {
            return;
        }
        // find the miliz entry:
        PlayerTroop pt = Service.playerTroopDAO.findBy(pp.getUserId(), planetId, GroundTroop.ID_MILIZ);
        boolean newEntry = false;
        if (pt == null) {
            pt = new PlayerTroop();
            pt.setPlanetId(planetId);
            pt.setUserId(pp.getUserId());
            pt.setNumber(0l);
            pt.setTroopId(GroundTroop.ID_MILIZ);
            newEntry = true;
        }

        PlanetLoyality pLoy = pLoyDAO.getByPlanetAndUserId(pp.getPlanetId(), pp.getUserId());        
        double loyality = 0d;
        
        if (pLoy != null) {
            loyality = pLoy.getValue();
        }
        
        long milizCount = pt.getNumber();
        long maxMiliz = (long) (pp.getPopulation() * GameConstants.GLOBAL_MAX_MILIZ_PERC * (loyality / 100d));
        int barracksCount = barracks.getNumber();
        int addMiliz = barracksCount * GameConstants.GLOBAL_MILIZ_TRAIN_UNIT;
        if (milizCount < maxMiliz) {
            milizCount = Math.min(maxMiliz, milizCount + addMiliz);
        } else if (milizCount > maxMiliz) {
            milizCount = (long) ((double) milizCount * 0.99d);
        }
        //BEvölkerungsbeschränkung
        milizCount = Math.min(milizCount, pp.getPopulation());

        if (milizCount > 0) {
            pt.setNumber(milizCount);
            if (newEntry) {
                //Service.playerTroopDAO.add(pt);
                toAdd.add(pt);
            } else {
                //Service.playerTroopDAO.update(pt);
                toUpdate.add(pt);
            }
        } else {
            if(!newEntry){
            Service.playerTroopDAO.remove(pt);
            //toRemove.add(pt);

            }
        }

    }
}
