/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view;

import at.viswars.model.Condition;
import at.viswars.model.ConditionToTitle;
import at.viswars.model.ParameterToTitle;

/**
 *
 * @author Aion
 */
public class ViewCondition {

    private Condition condition;
    private ConditionToTitle conditionToTitle;

    public ViewCondition(Condition condition, ConditionToTitle conditionToTitle) {
        this.condition = condition;
        this.conditionToTitle = conditionToTitle;

    }

    /**
     * @return the condition
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * @return the conditionToTitle
     */
    public ConditionToTitle getConditionToTitle() {
        return conditionToTitle;
    }

}
