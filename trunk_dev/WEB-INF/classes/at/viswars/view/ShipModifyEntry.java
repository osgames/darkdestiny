/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view;

import at.viswars.ships.ShipData;

/**
 *
 * @author Stefan
 */
public class ShipModifyEntry extends ShipData {
    private boolean canBeModified;
    private boolean canBeScrapped;
    private boolean canBeRepaired;
    private boolean blockedByLoading;
    private int damagedCount;

    public boolean isCanBeModified() {
        return canBeModified;
    }

    public void setCanBeModified(boolean canBeModified) {
        this.canBeModified = canBeModified;
    }

    public boolean isCanBeScrapped() {
        return canBeScrapped;
    }

    public void setCanBeScrapped(boolean canBeScrapped) {
        this.canBeScrapped = canBeScrapped;
    }
    
    public void setShipData(ShipData sd) {
        super.setId(sd.getId());
        super.setChassisSize(sd.getChassisSize());
        super.setCount(sd.getCount());
        super.setDesignId(sd.getDesignId());
        super.setDesignName(sd.getDesignName());
        super.setFleetId(sd.getFleetId());
        super.setFleetName(sd.getFleetName());
    }

    public boolean isCanBeRepaired() {
        return canBeRepaired;
    }

    public void setCanBeRepaired(boolean canBeRepaired) {
        this.canBeRepaired = canBeRepaired;
    }

    /**
     * @return the blockedByLoading
     */
    public boolean isBlockedByLoading() {
        return blockedByLoading;
    }

    /**
     * @param blockedByLoading the blockedByLoading to set
     */
    public void setBlockedByLoading(boolean blockedByLoading) {
        this.blockedByLoading = blockedByLoading;
    }

    /**
     * @return the damagedCount
     */
    public int getDamagedCount() {
        return damagedCount;
    }

    /**
     * @param damagedCount the damagedCount to set
     */
    public void setDamagedCount(int damagedCount) {
        this.damagedCount = damagedCount;
    }
}
