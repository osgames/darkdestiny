/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view;

/**
 *
 * @author Stefan
 */
public class ShippingEntity {
    private int incoming = 0;
    private int outgoing = 0;

    /**
     * @return the incomming
     */
    public int getIncoming() {
        return incoming;
    }

    /**
     * @param incomming the incomming to set
     */
    public void setIncoming(int incoming) {
        this.incoming = incoming;
    }

    /**
     * @return the outgoing
     */
    public int getOutgoing() {
        return outgoing;
    }

    /**
     * @param outgoing the outgoing to set
     */
    public void setOutgoing(int outgoing) {
        this.outgoing = outgoing;
    }

    
    
}
