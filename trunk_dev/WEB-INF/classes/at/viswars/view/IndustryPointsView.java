/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view;

/**
 *
 * @author Stefan
 */
public class IndustryPointsView {
    private final int industryPoints;
    private final int industryPointsBonus;

    public IndustryPointsView(int industryPoints, int industryPointsBonus) {
        this.industryPoints = industryPoints;
        this.industryPointsBonus = industryPointsBonus;
    }

    public int getIndustryPoints() {
        return industryPoints;
    }

    public int getIndustryPointsBonus() {
        return industryPointsBonus;
    }
}
