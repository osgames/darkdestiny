/*
 * FleetInfo.java
 *
 * Created on 24. April 2007, 13:50
 *
 * This class contains Information about the scanned fleet
 */

package at.viswars.view;

import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import java.util.*;

/**
 *
 * @author Stefan
 */
public class FleetScanInfo {
    private int fleetId;
    private String fleetName;
    private float speed;
    private int userId;
    private int timeToDest;
    private AbsoluteCoordinate currLoc;
    private RelativeCoordinate targetLoc;
    private HashMap<Integer,Integer> sizeCount;

    /** Creates a new instance of FleetInfo */
    public FleetScanInfo() {
        sizeCount = new HashMap<Integer,Integer>();
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public AbsoluteCoordinate getCurrLoc() {
        return currLoc;
    }

    public void setCurrLoc(AbsoluteCoordinate currLoc) {
        this.currLoc = currLoc;
    }

    public HashMap<Integer, Integer> getSizeCount() {
        return sizeCount;
    }

    public void addSizeCount(int size, int count) {
        if (sizeCount.containsKey(size)) {
            sizeCount.put(size,sizeCount.get(size)+count);
        } else {
            sizeCount.put(size,count);
        }
    }

    public RelativeCoordinate getTargetLoc() {
        return targetLoc;
    }

    public void setTargetLoc(RelativeCoordinate targetLoc) {
        this.targetLoc = targetLoc;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTimeToDest() {
        return timeToDest;
    }

    public void setTimeToDest(int timeToDest) {
        this.timeToDest = timeToDest;
    }

    public int getFleetId() {
        return fleetId;
    }

    public void setFleetId(int fleetId) {
        this.fleetId = fleetId;
    }
}
