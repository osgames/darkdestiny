 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.html;

/**
 *
 * @author Horst
 */
public class styleDivCont {

    String name;
    String position;
    int top,left,width,height,border,zIndex;
    String imageUrl;
    String repeat;
    private String misc = "";
    
    /*
     * 
     * e.g.:
     *#leftBlink { position:absolute; top:7px; left:48px; 
     * width:23px; height:23px; border:0px;z-index:1; 
     * background-image:url(menu/messageBlink.png);}
     * 
     */
     
     
    public styleDivCont(String name, String position, int top, int left, int width, int height, int border, int zIndex, String imageUrl,String repeat){
        this.name = name;
        this.position = position;
        this.top = top;
        this.left = left;
        this.width = width;
        this.height = height;
        this.border = border;
        this.zIndex = zIndex;
        this.imageUrl = imageUrl;
        this.repeat = repeat;
    }
    
    public String toString(){
        String returnString = "";
        returnString += "#"+name+"{";
        returnString += "position:"+position+"; ";
        returnString += "top:"+top+"px; ";
        returnString += "left:"+left+"px; ";
        returnString += "width:"+width+"px; ";
        returnString += "height:"+height+"px; ";
        returnString += "border:"+border+"px; ";
        returnString += "z-index:"+zIndex+"; ";
        returnString += misc;
        if(!imageUrl.equals("")){
            
           returnString += "background-image:url("+imageUrl+"); ";
        }
        if(!repeat.equals("")){
            returnString += "background-repeat:"+repeat+"; ";
        }
        
        returnString +="}";
        
        
        return returnString;
    }

    /**
     * @return the misc
     */
    public String getMisc() {
        return misc;
    }

    /**
     * @param misc the misc to set
     */
    public void setMisc(String misc) {
        this.misc = misc;
    }
    
}
