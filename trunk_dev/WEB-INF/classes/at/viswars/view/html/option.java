 /*
 * td.java
 *
 * Created on 10. Mai 2007, 13:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.viswars.view.html;

import java.util.ArrayList;

/**
 *
 * @author Horst
 */
public class option extends htmlObject {

    private ArrayList<div> divs;

    /**
     * Creates a new instance of td
     */
    public option() {
        properties = "";
        data = "";
        divs = new ArrayList<div>();
    }

    public option(String data) {
        properties = "";
        this.data = data;
        divs = new ArrayList<div>();
    }

    public String toString() {
        String returnString = "";
        returnString += "\t\t\t\t<option ";
        if (!properties.equals("")) {
            returnString += properties + " ";
        }
        returnString += ">";

        for (int i = 0; i < divs.size(); i++) {

            data += "\n" + divs.get(i).toString();

        }
        if (!data.equals("")) {
            returnString += data;
        }


        returnString += "</option>\n";

        return returnString;
    }

    public void addDiv(div div1) {
        divs.add(div1);
    }
}
