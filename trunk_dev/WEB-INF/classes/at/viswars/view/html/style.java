 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.html;

import java.util.ArrayList;


/**
 *
 * @author Horst
 */
public class style extends htmlObject{

private ArrayList<styleDivCont> divContainers;

public style(){
  divContainers = new ArrayList<styleDivCont>();
  properties = "";
  data = "";
}
   
    
    public String toString(){
         
       String dataTmp = "";
          if(properties.equals("") && data.equals("") && getDivContainers().size() == 0){
              return "";
          }
          String returnString = "";
          returnString += "\t\t<style";
          
           if(!properties.equals("")){
               returnString += " "+properties+ " ";
           }
           returnString += ">";
                 
           if(!data.equals("")){
                returnString += data;
           }
           for(int i = 0; i < divContainers.size(); i++){
               
               dataTmp += "\t\t\t\n"+divContainers.get(i).toString();
             }
           returnString += dataTmp;
           
           returnString += "\t\t\n</style>\n";
           
        return returnString;
    }
    
    public void addDivCont(styleDivCont divContainer){
        this.getDivContainers().add(divContainer);
    }

    public ArrayList<styleDivCont> getDivContainers() {
        return divContainers;
    }

    public void setDivContainers(ArrayList<styleDivCont> divContainers) {
        this.divContainers = divContainers;
    }
}
