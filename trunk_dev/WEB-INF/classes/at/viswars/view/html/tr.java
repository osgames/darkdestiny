 /*
 * tr.java
 *
 * Created on 10. Mai 2007, 13:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.viswars.view.html;

import java.util.*;

/**
 *
 * @author Horst
 */
public class tr extends htmlObject {

    public ArrayList<td> tds;

    /**
     * Creates a new instance of tr
     */
    public tr() {
        properties = "";
        data = "";
        tds = new ArrayList<td>();

    }

    public void addTd(String tdData) {
        addTd(new td(tdData));
    }

    public void addTd(String tdData, String properties) {
        addTd(new td(tdData, properties));
    }

    public tr(td td1) {

        properties = "";
        data = "";
        tds = new ArrayList<td>();
        tds.add(td1);
    }

    public void addTd(td td) {

        tds.add(td);

    }

    public ArrayList<td> getTds() {
        return tds;
    }

    public void setTds(ArrayList<td> tds) {
        this.tds = tds;
    }

    public String toString() {
        String returnString = "";
        data += "";
        for (int i = 0; i < tds.size(); i++) {
            data += " " + tds.get(i).toString();
        }
        returnString += "\t\t\t<tr";
        if (!properties.equals("")) {
            returnString += properties + " ";
        }
        returnString += ">\n";

        if (!data.equals("")) {
            returnString += data;
        }

        returnString += "\n\t\t\t</tr>\n";
        return returnString;
    }
}
