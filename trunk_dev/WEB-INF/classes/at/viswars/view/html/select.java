 /*
 * tr.java
 *
 * Created on 10. Mai 2007, 13:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.viswars.view.html;

import java.util.*;

/**
 *
 * @author Horst
 */
public class select extends htmlObject {

    public ArrayList<option> options;

    /**
     * Creates a new instance of tr
     */
    public select() {
        properties = "";
        data = "";
        options = new ArrayList<option>();

    }

    public void addOption(String optionData) {
        addOption(new option(optionData));
    }

    public select(option option) {

        properties = "";
        data = "";
        options = new ArrayList<option>();
        options.add(option);
    }

    public void addOption(option option) {

        options.add(option);

    }

    public ArrayList<option> getOptions() {
        return options;
    }

    public void setTds(ArrayList<option> optionss) {
        this.options = options;
    }

    public String toString() {
        String returnString = "";
        data += "";
        for (int i = 0; i < options.size(); i++) {
            data += " " + options.get(i).toString();
        }
        returnString += "\t\t\t<select";
        if (!properties.equals("")) {
            returnString += properties + " ";
        }
        returnString += ">\n";

        if (!data.equals("")) {
            returnString += data;
        }

        returnString += "\n\t\t\t</select>\n";
        return returnString;
    }
}
