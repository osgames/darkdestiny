 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.menu;

import at.viswars.model.Ressource;
import at.viswars.view.ShippingEntity;

/**
 *
 * Datenklasse f?r die Klasse StatusBarData.java
 * Daten zum Erstellen der StatusBar
 * 
 * @author Dreloc
 */
public class RessourceEntity {

    private final Ressource ressource;
    private StorageEntity storage;
    private ProductionUsageEntity productionUsage;
    private ShippingEntity trade;
    private ShippingEntity transport;
    
    public RessourceEntity(Ressource ressource){
    	this.ressource = ressource;
        storage = new StorageEntity();
        productionUsage = new ProductionUsageEntity();
        trade = new ShippingEntity();
        transport = new ShippingEntity();
    }
    
    public String getName() {
        return getRessource().getName();
    }

    public StorageEntity getStorage() {
        return storage;
    }

    public void setStorage(StorageEntity storage) {
        this.storage = storage;
    }

    public ProductionUsageEntity getProductionUsage() {
        return productionUsage;
    }

    public void setProductionUsage(ProductionUsageEntity productionUsage) {
        this.productionUsage = productionUsage;
    }


    public ShippingEntity getTrade() {
        return trade;
    }

    public void setTrade(ShippingEntity trade) {
        this.trade = trade;
    }

    public Ressource getRessource() {
        return ressource;
    }

    /**
     * @return the transport
     */
    public ShippingEntity getTransport() {
        return transport;
    }

    /**
     * @param transport the transport to set
     */
    public void setTransport(ShippingEntity transport) {
        this.transport = transport;
    }

}
