 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.menu;

/**
 *
 * Datenklasse f&uuml;r die Klasse StatusBarData.java
 * Daten zum Erstellen der StatusBar
 * 
 * @author Dreloc
 */
public class DevelopementEntity {
    
    private double globalPoints = 0;
    private double planetPoints = 0;

    /**
     * @return the globalPoints
     */
    public double getGlobalPoints() {
        return globalPoints;
    }

    /**
     * @param globalPoints the globalPoints to set
     */
    public void setGlobalPoints(double globalPoints) {
        this.globalPoints = globalPoints;
    }

    /**
     * @return the planetPoints
     */
    public double getPlanetPoints() {
        return planetPoints;
    }

    /**
     * @param planetPoints the planetPoints to set
     */
    public void setPlanetPoints(double planetPoints) {
        this.planetPoints = planetPoints;
    }
    
    
}
