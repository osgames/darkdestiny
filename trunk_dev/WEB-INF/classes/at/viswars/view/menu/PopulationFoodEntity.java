 
package at.viswars.view.menu;

import at.viswars.model.Ressource;
import at.viswars.view.menu.RessourceEntity;

/**
 *
 * Datenklasse f&uuml;r die Klasse StatusBarData.java
 * Daten zum Erstellen der StatusBar
 * 
 * @author Dreloc
 */
public class PopulationFoodEntity {
    
    private long planetPopulation = 0;
    private int moral = 0;
    private double growth = 0;
    private int taxes = 0;
    private long emigration;
    private long growthPopulation;
    //Nahrungs Produktion und Verbrauch momentan
    private RessourceEntity food;
    private String popName = "Planetare Bev?lkerung";
    
    public PopulationFoodEntity(Ressource ressource){
        
       food = new RessourceEntity(ressource);
        
    }
    
    public long getPlanetPopulation() {
        return planetPopulation;
    }

    public void setPlanetPopulation(long planetPopulation) {
        this.planetPopulation = planetPopulation;
    }

    public String getPopName() {
        return popName;
    }

    public void setPopName(String popName) {
        this.popName = popName;
    }

    public RessourceEntity getFood() {
        return food;
    }

    public void setFood(RessourceEntity food) {
        this.food = food;
    }

    /**
     * @return the moral
     */
    public int getMoral() {
        return moral;
    }

    /**
     * @param moral the moral to set
     */
    public void setMoral(int moral) {
        this.moral = moral;
    }

    /**
     * @return the growth
     */
    public double getGrowth() {
        return growth;
    }

    /**
     * @param growth the growth to set
     */
    public void setGrowth(double growth) {
        this.growth = growth;
    }

    /**
     * @return the taxes
     */
    public int getTaxes() {
        return taxes;
    }

    /**
     * @param taxes the taxes to set
     */
    public void setTaxes(int taxes) {
        this.taxes = taxes;
    }

    /**
     * @return the emigration
     */
    public long getEmigration() {
        return emigration;
    }

    /**
     * @param emigration the emigration to set
     */
    public void setEmigration(long emigration) {
        this.emigration = emigration;
    }

    /**
     * @return the growthPopulation
     */
    public long getGrowthPopulation() {
        return growthPopulation;
    }

    /**
     * @param growthPopulation the growthPopulation to set
     */
    public void setGrowthPopulation(long growthPopulation) {
        this.growthPopulation = growthPopulation;
    }

}
