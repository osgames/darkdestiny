 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu;

/**
 *
 * Datenklasse f&uuml;r die Klasse StatusBarData.java
 * Daten zum Erstellen der StatusBar
 * 
 * @author Dreloc
 */
public class MessageEntity {

    private boolean messageReceived;
    private boolean aMessageReceived;
    private boolean pMessageReceived;
    private boolean sMessageReceived;

    public MessageEntity() {

        messageReceived = false;
        aMessageReceived = false;
        pMessageReceived = false;
        sMessageReceived = false;
    }

    public boolean isMessageReceived() {
        return messageReceived;
    }

    public void setMessageReceived(boolean messageReceived) {
        this.messageReceived = messageReceived;
    }

    public boolean isAMessageReceived() {
        return aMessageReceived;
    }

    public void setAMessageReceived(boolean aMessageReceived) {
        this.aMessageReceived = aMessageReceived;
    }

    public boolean isPMessageReceived() {
        return pMessageReceived;
    }

    public void setPMessageReceived(boolean pMessageReceived) {
        this.pMessageReceived = pMessageReceived;
    }

    public boolean isSMessageReceived() {
        return sMessageReceived;
    }

    public void setSMessageReceived(boolean sMessageReceived) {
        this.sMessageReceived = sMessageReceived;
    }
}
