 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.menu;

/**
 *
 * Datenklasse f?r die Klasse StatusBarData.java
 * Daten zum Erstellen der StatusBar
 * 
 * @author Dreloc
 */
public class StorageEntity {
private long storageCapicity;
private long storageStock;
private long storageMin_Production;
private long storageMin_Trade;
private long storageMin_Transport;
private String ressourceName;
private double percent;


public StorageEntity(){
    storageCapicity = 0;
    storageStock = 0;
    storageMin_Production = 0;
    storageMin_Trade = 0;
    storageMin_Transport = 0;
    ressourceName = "";
    percent = 0;
}

    public long getStorageCapicity() {
        return storageCapicity;
    }

    public void setStorageCapicity(long storageCapicity) {
        this.storageCapicity = storageCapicity;
    }

    public long getStorageStock() {
        return storageStock;
    }

    public void setStorageStock(long storageStock) {
        this.storageStock = storageStock;
    }

    public String getRessourceName() {
        return ressourceName;
    }

    public void setRessourceName(String ressourceName) {
        this.ressourceName = ressourceName;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    /**
     * @return the storageMin_Production
     */
    public long getStorageMin_Production() {
        return storageMin_Production;
    }

    /**
     * @param storageMin_Production the storageMin_Production to set
     */
    public void setStorageMin_Production(long storageMin_Production) {
        this.storageMin_Production = storageMin_Production;
    }

    /**
     * @return the storageMin_Trade
     */
    public long getStorageMin_Trade() {
        return storageMin_Trade;
    }

    /**
     * @param storageMin_Trade the storageMin_Trade to set
     */
    public void setStorageMin_Trade(long storageMin_Trade) {
        this.storageMin_Trade = storageMin_Trade;
    }

    /**
     * @return the storageMin_Transport
     */
    public long getStorageMin_Transport() {
        return storageMin_Transport;
    }

    /**
     * @param storageMin_Transport the storageMin_Transport to set
     */
    public void setStorageMin_Transport(long storageMin_Transport) {
        this.storageMin_Transport = storageMin_Transport;
    }

}
