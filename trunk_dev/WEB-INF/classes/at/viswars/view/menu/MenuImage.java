 

package at.viswars.view.menu;

/**
 *
 * Abbildung der Datenbanktabelle
 * MenuImages
 * 
 * @author Dreloc
 */
public class MenuImage {
    private int id;
    private int type;
    private String description;
    private String imageUrl;
    private int width;
    private int height;
    /**
     * Creates a new instance of MenuImage
     */
    public MenuImage() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    
}
