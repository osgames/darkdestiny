/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.menu.statusbar.style.basic;

import at.viswars.view.menu.statusbar.*;
import at.viswars.GameConfig;
import at.viswars.ML;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.StatusBarData;
import java.util.Locale;

/**
 *
 * @author Bullet
 */
public class EnergySpacer extends StatusBarEntity implements IStatusBarEntity{


    public EnergySpacer(StatusBarData stb, int userId, int width) {
        super(stb, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
         styleDivCont ressSpacerPositioner = new styleDivCont(
                    "energySpacerPositioner", "relative", 0, 0, 1, 1, 0, 1, "", "");
            rootNode.addDivCont(ressSpacerPositioner);

            styleDivCont ressSpacerText = new styleDivCont(
                    "energySpacerText", "absolute", -7, 10, 160, 1, 0, 1,"", "");
            rootNode.addDivCont(ressSpacerText);

            return rootNode;
    }

    @Override
    public table addTr(table table) {

        int height = 25;



        String imgName = "pic/menu/statusbar/stb_energy_spacer.png";

        String properties = "";

        tr tr1 = new tr();

        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + GameConfig.picPath() + imgName + ")';  position:relative; background-repeat: repeat-x;";

        div ressSpacer = new div();
        ressSpacer.setProperties("id='energySpacerPositioner'");

        div spacerText = new div();
        spacerText.setProperties("id='energySpacerText'  align=center");
        spacerText.setData(
                "<FONT color=#FFFFFF size='-2'>" + ML.getMLStr("statusbar_spacer_energy", userId) + "</FONT>");

        ressSpacer.addDiv(spacerText);

        td1.setProperties(properties);
        td1.addDiv(ressSpacer);
        tr1.addTd(td1);

        table.addTr(tr1);

        return table;
    }


}
