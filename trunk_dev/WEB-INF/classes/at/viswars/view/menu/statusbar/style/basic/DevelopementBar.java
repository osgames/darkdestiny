/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu.statusbar.style.basic;

import at.viswars.view.menu.statusbar.*;
import at.viswars.FormatUtilities;
import at.viswars.GameConfig;
import at.viswars.GameConstants;
import at.viswars.ML;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.StatusBarData;
import java.util.Locale;

/**
 *
 * @author Aion
 */
public class DevelopementBar extends StatusBarEntity implements IStatusBarEntity {

    public DevelopementBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        //Population DivContainers
        styleDivCont positionerPopulation = new styleDivCont("positionerDevelopement", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(positionerPopulation);
        String imageIcon = GameConfig.picPath() + "pic/menu/icons/happiness.png";

        String emigrationIconImage = "";
        if (statusBarData.getDevelopement().getPlanetPoints() >= 0) {
            emigrationIconImage = GameConfig.picPath() + "pic/menu/icons/emigration_plus.png";
        } else {
            emigrationIconImage = GameConfig.picPath() + "pic/menu/icons/emigration_minus.png";
        }

        styleDivCont globalCredits = new styleDivCont("globalDev", "absolute", 9, 63, 95, 1, 0, 1, "", "");
        styleDivCont changeCredits = new styleDivCont("changeDev", "absolute", 22, 63, 95, 1, 0, 1, "", "");
        styleDivCont creditsIcon = new styleDivCont("devIcon", "absolute", 8, 17, 30, 30, 0, 1, imageIcon, "no-repeat");
        styleDivCont creditsChangeIcon = new styleDivCont("devChangeIcon", "absolute", 25, 58, 10, 10, 0, 1, emigrationIconImage, "no-repeat");

        rootNode.addDivCont(globalCredits);
        rootNode.addDivCont(changeCredits);
        rootNode.addDivCont(creditsIcon);
        rootNode.addDivCont(creditsChangeIcon);


        return rootNode;
    }

    @Override
    public table addTr(table table) {

        int height = 50;
        String imageUrl = GameConfig.picPath() + "pic/menu/statusbar/stb_credit_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ")';  position:relative; background-repeat: repeat-x;";

        td1.setProperties(properties);

         div positionerPopulation = new div();
        positionerPopulation.setProperties("id='positionerDevelopement'");

        div gloabalCredits = new div();
        gloabalCredits.setProperties("id='globalDev' align=right  onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_developementglobal", userId) + "')\" onmouseout=\"hideTip()\"");
        gloabalCredits.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormatScaledNumber(statusBarData.getDevelopement().getGlobalPoints(), 2, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "</FONT>");

         div planetaryincome = new div();
        planetaryincome.setProperties("id='changeDev' align=right  onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_developementplanetary", userId) + "')\" onmouseout=\"hideTip()\"");
        planetaryincome.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormatScaledNumber(statusBarData.getDevelopement().getPlanetPoints(), 2, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "</FONT>");


        //Hier wird der ToolTip aufgebaut der produktion verbrauch handel usw anzeigt
        div creditsIcon = new div();
        creditsIcon.setProperties("id='devIcon' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_developementpoints", userId) + "')\" onmouseout=\"hideTip()\" ");
        positionerPopulation.addDiv(creditsIcon);
        div creditsChangeIcon = new div();
        creditsChangeIcon.setProperties("id='devChangeIcon' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_developementplanetary", userId) + "')\" onmouseout=\"hideTip()\" ");
        positionerPopulation.addDiv(creditsChangeIcon);

        positionerPopulation.addDiv(gloabalCredits);
        positionerPopulation.addDiv(planetaryincome);



        td1.addDiv(positionerPopulation);


        tr1.addTd(td1);

        table.addTr(tr1);

        return table;
    }
}
