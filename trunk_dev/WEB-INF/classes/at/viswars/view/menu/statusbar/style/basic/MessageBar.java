/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu.statusbar.style.basic;

import at.viswars.view.menu.statusbar.*;
import at.viswars.GameConfig;
import at.viswars.ML;
import at.viswars.utilities.AllianceUtilities;
import at.viswars.view.html.a;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.StatusBarData;

/**
 *
 * @author Aion
 */
public class MessageBar extends StatusBarEntity implements IStatusBarEntity {

    public MessageBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        //Population DivContainers
        styleDivCont positionerMessage = new styleDivCont("positionerMessage", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(positionerMessage);


        styleDivCont privateString = new styleDivCont("privateString", "absolute", 5, 56, 0, 1, 0, 1, "", "");
        styleDivCont allianceString = new styleDivCont("allianceString", "absolute", 18, 56, 0, 1, 0, 1, "", "");
        styleDivCont systemString = new styleDivCont("systemString", "absolute", 31, 56, 0, 1, 0, 1, "", "");

        rootNode.addDivCont(privateString);
        rootNode.addDivCont(allianceString);
        rootNode.addDivCont(systemString);

        // >>>>>>>>>>>>>


        styleDivCont tmp2 = new styleDivCont("pBlink", "absolute", 1, 148, 17, 17, 0, 2, GameConfig.picPath() + "pic/menu/statusbar/messageBlink.png", "no-repeat");
        rootNode.addDivCont(tmp2);

        styleDivCont msgP = new styleDivCont("pNormal", "absolute", 5, 56, 110, 10, 0, 3, GameConfig.picPath() + "pic/menu/smallMessage.png", "no-repeat");
        rootNode.addDivCont(msgP);


        styleDivCont tmp3 = new styleDivCont("aBlink", "absolute", 14, 148, 17, 17, 0, 2, GameConfig.picPath() + "pic/menu/statusbar/messageBlink.png", "repeat");
        rootNode.addDivCont(tmp3);

        styleDivCont msgA = new styleDivCont("aNormal", "absolute", 18, 56, 110, 10, 0, 3, GameConfig.picPath() + "pic/menu/smallMessage.png", "no-repeat");
        rootNode.addDivCont(msgA);


        styleDivCont tmp4 = new styleDivCont("sBlink", "absolute", 27, 148, 17, 17, 0, 2, GameConfig.picPath() + "pic/menu/statusbar/messageBlink.png", "no-repeat");
        rootNode.addDivCont(tmp4);

        styleDivCont msgS = new styleDivCont("sNormal", "absolute", 33, 56, 110, 10, 0, 3, GameConfig.picPath() + "pic/menu/smallMessage.png", "no-repeat");
        rootNode.addDivCont(msgS);

        //>>>>>>>>><
        return rootNode;
    }

    @Override
    public table addTr(table table) {

        int height = 56;
        String imageUrl = GameConfig.picPath() + "pic/menu/statusbar/stb_message_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ")';  position:relative; background-repeat: repeat-x;";

        td1.setProperties(properties);

        div positionerMessage = new div();
        positionerMessage.setProperties("id='positionerMessage'");

        div privateString = new div();
        privateString.setProperties("id='privateString' align=left");
        privateString.setData("<FONT color=#FFFFFF size='-2'><B>" + ML.getMLStr("statusbar_message_privatemessage", userId) + "</B></FONT>");

        div allianceString = new div();
        allianceString.setProperties("id='allianceString' align=left");
        allianceString.setData("<FONT color=#FFFFFF size='-2'><B>" + ML.getMLStr("statusbar_message_alliance", userId) + "</B></FONT>");

        div systemString = new div();
        systemString.setProperties("id='systemString' align=left");
        systemString.setData("<FONT color=#FFFFFF size='-2'><B>" + ML.getMLStr("statusbar_message_system", userId) + "</B></FONT>");

        //Hier wird der ToolTip aufgebaut der produktion verbrauch handel usw anzeigt
        //     div creditsIcon = new div();
        //     creditsIcon.setProperties("id='messageIcon' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_credits", userId) + "')\" onmouseout=\"hideTip()\" ");
        //     positionerMessage.addDiv(creditsIcon);

        positionerMessage.addDiv(privateString);
        positionerMessage.addDiv(allianceString);
        positionerMessage.addDiv(systemString);

 //Hinzuf?gen ob ?berhaupt eine PRIVATE - Nachricht empfangen wurde
        //TODO Setzen des Hyperlinks
            div div3b = new div();
            div3b.setProperties("id='pNormal'");
            a privateHref = new a("main.jsp?page=new/messages");
            div3b.setHyperlink(privateHref);
            positionerMessage.addDiv(div3b);
            
            // If user is not in an Alliance or not in any groups we can skip this
            if (AllianceUtilities.userIsInAllyOrHasBoards(userId)) {
                div div4a = new div();
                div4a.setProperties("id='aNormal'");
                a allianceHref = new a("main.jsp?page=new/alliance&type=11&from=0&to=15");
                div4a.setHyperlink(allianceHref);
                positionerMessage.addDiv(div4a);
            }
            div div5a = new div();
            div5a.setProperties("id='sNormal'");
            a systemHref = new a("main.jsp?page=new/messages");
            div5a.setHyperlink(systemHref);
            positionerMessage.addDiv(div5a);

        if (statusBarData.getMessages().isPMessageReceived()) {

            div div3 = new div();
            div3.setProperties("id='pBlink'");
            positionerMessage.addDiv(div3);
        }
        //Hinzuf?gen ob ?berhaupt eine ALLIANZ - Nachricht empfangen wurde
        //TODO Setzen des Hyperlinks
        if (statusBarData.getMessages().isAMessageReceived()) {
            div div4 = new div();
            div4.setProperties("id='aBlink'");
            positionerMessage.addDiv(div4);
        }

        //Hinzuf?gen ob ?berhaupt eine SYSTEM-Nachricht empfangen wurde
        //TODO Setzen des Hyperlinks
        if (statusBarData.getMessages().isSMessageReceived()) {
            div div5 = new div();
            div5.setProperties("id='sBlink'");
            positionerMessage.addDiv(div5);
        }
        td1.addDiv(positionerMessage);

        td1.setProperties(properties);
        td1.setData("");
        tr1.addTd(td1);

        // =>>>>>>>>>><
        table.addTr(tr1);

        return table;
    }
}
