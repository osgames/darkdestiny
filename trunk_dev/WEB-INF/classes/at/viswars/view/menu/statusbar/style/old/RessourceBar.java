/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu.statusbar.style.old;

import at.viswars.view.menu.statusbar.*;
import at.viswars.FormatUtilities;
import at.viswars.GameConfig;
import at.viswars.GameConstants;
import at.viswars.ML;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.ProductionUsageEntity;
import at.viswars.view.menu.RessourceEntity;
import at.viswars.view.menu.StatusBarData;
import at.viswars.view.menu.StorageEntity;

/**
 *
 * @author Bullet
 */
public class RessourceBar extends StatusBarEntity implements IStatusBarEntity {

    private static int minLength = 0;
    private static int maxLength = 88;
    private static int barLength;
    String storageImage = "pic/menu/storage";
    String tmpStorageImage = "";

    public RessourceBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    public table addTr(table table) {
        for (int i = 0; i < statusBarData.getRessources().size(); i++) {

            // Logger.getLogger().write("Searching tha traderoutes for this Planet");

            //Adding <TR> for every Ressource
            if (statusBarData.getRessources().get(i).getProductionUsage().getProductionNow() != 0 || statusBarData.getRessources().get(i).getStorage().getStorageStock() != 0 || statusBarData.getRessources().get(i).getTrade().getIncoming() != 0 || statusBarData.getRessources().get(i).getTrade().getOutgoing() != 0) {
                table.addTr(buildRessourcePanel(statusBarData.getRessources().get(i)));
            }

            // Logger.getLogger().write("TradeSize4:  "+tradeRoutes.size());

        }
        return table;
    }

    public style addStyle(style rootNode) {
        for (int i = 0; i < statusBarData.getRessources().size(); i++) {

            //Length of StorageBarImage

            RessourceEntity ressource = statusBarData.getRessources().get(i);

            styleDivCont ressPositioner = new styleDivCont(
                    ressource.getName() + "positioner", "relative", -23, 0, 1, 1, 0, 1, "", "");
            rootNode.addDivCont(ressPositioner);



            styleDivCont icon = new styleDivCont(
                    ressource.getName() + "icon", "absolute", 9, 5, 30, 30, 0, 1,
                    GameConfig.picPath() + ressource.getRessource().getImageLocation(), "no-repeat");
            rootNode.addDivCont(icon);

            // If Production > Usage GreenImage
            tmpStorageImage = changeStorageImage(storageImage, ressource);

            // Calculating StorageBarImageLength X_x

            barLength = calculateStorageBarLength(minLength, maxLength, ressource.getStorage());

            styleDivCont stock = new styleDivCont(
                    ressource.getName() + "stock", "absolute", 9, 56, 95, 1, 0, 1, "", "");
            rootNode.addDivCont(stock);

            styleDivCont storage = new styleDivCont(
                    ressource.getName() + "storage", "absolute", 27, 56, barLength, 9, 0, 1,
                    GameConfig.picPath() + tmpStorageImage, "no-repeat");
            rootNode.addDivCont(storage);

            /**
             *
             * Berechnung der Position f�r den MindestBestand einer Ressource
             */
            int minStock_ProductionPositionShift = 0;
            if (ressource.getStorage().getStorageMin_Production() > ressource.getStorage().getStorageCapicity()) {
                minStock_ProductionPositionShift = maxLength;
            } else if (ressource.getStorage().getStorageCapicity() > 0) {
                minStock_ProductionPositionShift = (int) (maxLength * ressource.getStorage().getStorageMin_Production() / ressource.getStorage().getStorageCapicity());

            }

            int minStock_ProductionPosition = 56 + minStock_ProductionPositionShift;


            int minStock_TransportPositionShift = 0;
            if (ressource.getStorage().getStorageMin_Transport() > ressource.getStorage().getStorageCapicity()) {
                minStock_TransportPositionShift = maxLength;
            } else if (ressource.getStorage().getStorageCapicity() > 0) {
                minStock_TransportPositionShift = (int) (maxLength * ressource.getStorage().getStorageMin_Transport() / ressource.getStorage().getStorageCapicity());

            }

            int minStock_TransportPosition = 56 + minStock_TransportPositionShift;


            int minStock_TradePositionShift = 0;
            if (ressource.getStorage().getStorageMin_Trade() > ressource.getStorage().getStorageCapicity()) {
                minStock_TradePositionShift = maxLength;
            } else if (ressource.getStorage().getStorageCapicity() > 0) {
                minStock_TradePositionShift = (int) (maxLength * ressource.getStorage().getStorageMin_Trade() / ressource.getStorage().getStorageCapicity());

            }

            int minStock_TradePosition = 56 + minStock_TradePositionShift;


            styleDivCont minStock_Production = new styleDivCont(
                    ressource.getName() + "minStock", "absolute", 27, minStock_ProductionPosition, 2, 6, 0, 2,
                    GameConfig.picPath() + "pic/menu/minStock.png", "no-repeat");
            rootNode.addDivCont(minStock_Production);
            styleDivCont minStock_Transport = new styleDivCont(
                    ressource.getName() + "minStock", "absolute", 27, minStock_TransportPosition, 2, 6, 0, 2,
                    GameConfig.picPath() + "pic/menu/minStock_Transport.png", "no-repeat");
            rootNode.addDivCont(minStock_Transport);
            styleDivCont minStock_Trade = new styleDivCont(
                    ressource.getName() + "minStock", "absolute", 27, minStock_TradePosition, 2, 6, 0, 2,
                    GameConfig.picPath() + "pic/menu/minStock_Trade.png", "no-repeat");
            rootNode.addDivCont(minStock_Trade);

        }
        return rootNode;
    }

    private String changeStorageImage(String storageImage, RessourceEntity ressource) {

        ProductionUsageEntity productionUsage = ressource.getProductionUsage();

        // Logger.getLogger().write("TradeSize4:  "+tradeRoutes.size());
        if (productionUsage.getProductionNow() + ressource.getTrade().getIncoming() > productionUsage.getUsageNow() + ressource.getTrade().getOutgoing()) {
            storageImage += "Plus";
        }
        // If Production < Usage RedImage
        if (productionUsage.getProductionNow() + ressource.getTrade().getIncoming() < productionUsage.getUsageNow() + ressource.getTrade().getOutgoing()) {
            storageImage += "Minus";
        }
        // Else normale picture
        storageImage += ".png";

        return storageImage;
    }

    public int calculateStorageBarLength(int minLength, int maxLength, StorageEntity storage) {

        if (storage.getStorageCapicity() > 0) {
            double stock = (double) storage.getStorageStock();
            double capicity = (double) storage.getStorageCapicity();

            double multiplicator = (stock / capicity);
            minLength = minLength + (int) Math.round(multiplicator * maxLength);
            storage.setPercent(Math.round(100 * multiplicator));
        }
        return minLength;
    }

    private tr buildRessourcePanel(RessourceEntity ressource) {
        int height = 42;
        String imageUrl = GameConfig.picPath() + "pic/menu/ressourcesImage.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ")';  position:relative; background-repeat: repeat-x;";



        td1.setProperties(properties);
        div ressPositioner = new div();
        ressPositioner.setProperties("id='" + ressource.getName() + "positioner'");



        //Hier wird der ToolTip aufgebaut der produktion verbrauch handel usw anzeigt
        div ressIcon = new div();

        String ressMouseOver = "id='" + ressource.getName() + "icon' onmouseover=\"doTooltip(event,'";
        ressMouseOver += ML.getMLStr(ressource.getName(), userId) + " <BR> ";
        ressMouseOver += "<TABLE border=0><TR><TD> </TD><TD><FONT size=1pt>" + ML.getMLStr("statusbar_pop_production", userId) + "</FONT></TD><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_usage", userId) + "</FONT></TD><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_total", userId) + "</FONT></TD></TR>";

        ressMouseOver += "<TR><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_planet", userId) + "</FONT></TD>";

        String color;

        if (ressource.getProductionUsage().getProductionNow() == 0) {
            color = "#000000";
        } else {
            color = "#33FF00";
        }

        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getProductionUsage().getProductionNow() + "</B></FONT></TD>";
        if (ressource.getProductionUsage().getUsageNow() == 0) {
            color = "#000000";
        } else {
            color = "#FFCC00";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + ressource.getProductionUsage().getUsageNow() + "</B></FONT></TD>";

        if (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow() > 0) {
            color = "#33FF00";
        } else if (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow() < 0) {
            color = "#FFCC00";
        } else {
            color = "#000000";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD></TR>";

        ressMouseOver += "<TR><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_trade", userId) + "</FONT></TD>";
        if (ressource.getTrade().getIncoming() == 0) {
            color = "#000000";
        } else {
            color = "#33FF00";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + ressource.getTrade().getIncoming() + "</B></FONT></TD>";


        if (ressource.getTrade().getOutgoing() == 0) {
            color = "#000000";
        } else {
            color = "#FFCC00";
        }
        ressMouseOver += " <TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getTrade().getOutgoing() + "</B></FONT></TD>";
        if (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing() > 0) {
            color = "#33FF00";
        } else if (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing() < 0) {
            color = "#FFCC00";
        } else {
            color = "#000000";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing()) + "</B></FONT></TD></TR>";

        ressMouseOver += "<TR><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_total", userId) + "</FONT></TD> ";

        if (ressource.getProductionUsage().getProductionNow() + ressource.getTrade().getIncoming() == 0) {
            color = "#000000";
        } else {
            color = "#33FF00";
        }

        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + (ressource.getProductionUsage().getProductionNow() + ressource.getTrade().getIncoming()) + "</B></FONT></TD>";

        if (ressource.getTrade().getOutgoing() + ressource.getProductionUsage().getUsageNow() == 0) {
            color = "#000000";
        } else {
            color = "#FFCC00";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getOutgoing() + ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD>";
        if (ressource.getTrade().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getProductionUsage().getUsageNow() > 0) {
            color = "#33FF00";
        } else if (ressource.getTrade().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getProductionUsage().getUsageNow() < 0) {
            color = "#FFCC00";
        } else {
            color = "#000000";
        }


        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD></TR></TABLE>";


        ressMouseOver += "')\" onmouseout=\"hideTip()\" ";
        ressIcon.setProperties(ressMouseOver);
        /*  Properties(
        "id='" + ressource.getName() + "icon' onmouseover=\"doTooltip(event,'" + ressource.getName() + "')\" onmouseout=\"hideTip()\" ");
         */


        /**
         * Lagerbestand + ToolTip
         *
         */
        ressPositioner.addDiv(ressIcon);

        div ressStock = new div();
        ressStock.setProperties("id='" + ressource.getName() + "stock'  align=right");
        ressStock.setData(
                "<FONT color=#FFFFFF size='-2'>"
                + (FormatUtilities.getFormatScaledNumber(
                ressource.getStorage().getStorageStock(), 0, GameConstants.VALUE_TYPE_NORMAL)) + "</FONT>");

        ressPositioner.addDiv(ressStock);

        div minStock = new div();
        minStock.setProperties("id='" + ressource.getName() + "minStock'  align=left");
        minStock.setData("");

        ressPositioner.addDiv(minStock);


        div ressStorage = new div();
        ressStorage.setProperties(
                "id='" + ressource.getName() + "storage' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_storeusage", userId) + ": <BR>" + (ressource.getStorage().getPercent() + "% von " + FormatUtilities.getFormatScaledNumber(ressource.getStorage().getStorageCapicity(), 0, GameConstants.VALUE_TYPE_NORMAL)) + "')\" onmouseout=\"hideTip()\" ");
        ressPositioner.addDiv(ressStorage);





        td1.addDiv(ressPositioner);


        tr1.addTd(td1);

        return tr1;

    }
}
