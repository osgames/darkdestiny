/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu.statusbar.style.old;

import at.viswars.view.menu.statusbar.*;
import at.viswars.FormatUtilities;
import at.viswars.GameConfig;
import at.viswars.GameConstants;
import at.viswars.ML;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.ProductionUsageEntity;
import at.viswars.view.menu.RessourceEntity;
import at.viswars.view.menu.StatusBarData;
import at.viswars.view.menu.StorageEntity;

/**
 *
 * @author Aion
 */
public class PopulationBar extends StatusBarEntity implements IStatusBarEntity {

    public PopulationBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        //Population DivContainersstyle addStyle(style rootNode) {
        int minLength = 0;
        int maxLength = 104;
        int barLength;
        /**
         * L?nge der Foodanzeige Storage
         *
         */
        styleDivCont positionerFood = new styleDivCont("positionerFood", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(positionerFood);
        String storageImage = "pic/menu/storage";
        String tmpStorageImage = "";
        //Population DivContainers
        styleDivCont positionerPopulation = new styleDivCont("positionerPopulation", "relative", -23, 0, 1, 1, 0, 1, GameConfig.picPath() + "pic/menu/messageBlink.png", "");
        rootNode.addDivCont(positionerPopulation);

        styleDivCont planetPopulation = new styleDivCont("planetPopulation", "absolute", 2, 55, 115, 2, 0, 1, "", "");
        rootNode.addDivCont(planetPopulation);
        styleDivCont foodicon = new styleDivCont(statusBarData.getPopulationFood().getFood().getRessource().getName() + "icon", "absolute", 17, 9, 30, 30, 0, 1, GameConfig.picPath() + "pic/menu/foodicon.png", "no-repeat");

        rootNode.addDivCont(foodicon);

        tmpStorageImage = changeStorageImage(storageImage, statusBarData.getPopulationFood().getFood());
        /**
         * Calculating StorageBarImageLength X_x
         */
        barLength = calculateStorageBarLength(minLength, maxLength, statusBarData.getPopulationFood().getFood().getStorage());
        styleDivCont foodStorage = new styleDivCont("foodStorage", "absolute", 39, 56, barLength, 9, 0, 1, GameConfig.picPath() + tmpStorageImage, "no-repeat");
        rootNode.addDivCont(foodStorage);

        String foodUsageImage = "pic/menu/foodRed.png";
        String foodProductionImage = "pic/menu/foodProduction.png";
        String foodImage = "pic/menu/foodEqual.png";
        int usageBarLength = 0;
        int productionBarLength = 0;

        int productionBarZIndex = 0;
        ProductionUsageEntity productionUsage = statusBarData.getPopulationFood().getFood().getProductionUsage();
        if (productionUsage.getProductionNow() > productionUsage.getUsageNow()) {

            productionBarLength = maxLength;

            foodUsageImage = foodImage;

            double production = (double) productionUsage.getProductionNow();
            double usage = (double) productionUsage.getUsageNow();
            double multiplicator = (usage / production);
            usageBarLength = minLength + (int) Math.round(multiplicator * maxLength);


        } else if (productionUsage.getProductionNow() < productionUsage.getUsageNow()) {

            foodProductionImage = foodImage;

            double production = (double) productionUsage.getProductionNow();
            double usage = (double) productionUsage.getUsageNow();
            double multiplicator = (production / usage);

            productionBarLength = minLength + (int) Math.round(multiplicator * maxLength);
            productionBarZIndex = 2;

            usageBarLength = maxLength;
        } else {
            productionBarZIndex = 2;
            foodProductionImage = foodImage;
            productionBarLength = maxLength;
        }
        styleDivCont foodProduction = new styleDivCont("foodProduction", "absolute", 22, 49, productionBarLength, 10, 0, productionBarZIndex, GameConfig.picPath() + foodProductionImage, "no-repeat");
        rootNode.addDivCont(foodProduction);

        styleDivCont foodUsage = new styleDivCont("foodUsage", "absolute", 22, 49, usageBarLength, 10, 0, 1, GameConfig.picPath() + foodUsageImage, "no-repeat");
        rootNode.addDivCont(foodUsage);


        return rootNode;
    }

    @Override
    public table addTr(table table) {
        String foodProductionToolTip = "'" + ML.getMLStr("statusbar_pop_foodproduction", userId) + ":" + FormatUtilities.getFormatScaledNumber(statusBarData.getPopulationFood().getFood().getProductionUsage().getProductionNow(), 0, GameConstants.VALUE_TYPE_NORMAL) + " <br> "
                + "" + ML.getMLStr("statusbar_pop_foodusage", userId) + ":" + FormatUtilities.getFormatScaledNumber(statusBarData.getPopulationFood().getFood().getProductionUsage().getUsageNow(), 0, GameConstants.VALUE_TYPE_NORMAL) + "'";
        String foodUsageToolTip = "'" + ML.getMLStr("statusbar_pop_foodproduction", userId) + ":" + FormatUtilities.getFormatScaledNumber(statusBarData.getPopulationFood().getFood().getProductionUsage().getProductionNow(), 0, GameConstants.VALUE_TYPE_NORMAL) + " <br> "
                + "" + ML.getMLStr("statusbar_pop_foodusage", userId) + ":" + FormatUtilities.getFormatScaledNumber(statusBarData.getPopulationFood().getFood().getProductionUsage().getUsageNow(), 0, GameConstants.VALUE_TYPE_NORMAL) + "'";
        if (statusBarData.getPopulationFood().getFood().getProductionUsage().getProductionNow()
                == statusBarData.getPopulationFood().getFood().getProductionUsage().getUsageNow()) {

            foodProductionToolTip = "'" + ML.getMLStr("statusbar_pop_usage", userId) + " = " + ML.getMLStr("statusbar_pop_production", userId) + ":'" + statusBarData.getPopulationFood().getFood().getProductionUsage().getProductionNow();
            foodUsageToolTip = "'" + ML.getMLStr("statusbar_pop_usage", userId) + " = " + ML.getMLStr("statusbar_pop_production", userId) + ":'" + statusBarData.getPopulationFood().getFood().getProductionUsage().getUsageNow();

        }

        int height = 59;
        String imageUrl = GameConfig.picPath() + "pic/menu/populationImage.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ")';  position:relative; background-repeat: repeat-x;";

        td1.setProperties(properties);

        div positionerPopulation = new div();
        positionerPopulation.setProperties("id='positionerPopulation'");

        div planetPopulation = new div();

        planetPopulation.setProperties("id='planetPopulation' align=right  onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_population", userId) + "')\" onmouseout=\"hideTip()\"");
        planetPopulation.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormatScaledNumber(statusBarData.getPopulationFood().getPlanetPopulation(), 2, GameConstants.VALUE_TYPE_NORMAL) + "</FONT>");
        // Nahrungsmouse Over

        RessourceEntity ressource = statusBarData.getPopulationFood().getFood();

        //Hier wird der ToolTip aufgebaut der produktion verbrauch handel usw anzeigt
        div ressIcon = new div();

        String ressMouseOver = "id='" + ressource.getName() + "icon' onmouseover=\"doTooltip(event,'";
        ressMouseOver += ML.getMLStr(ressource.getName(), userId) + " <BR> ";
        ressMouseOver += "<TABLE border=0><TR><TD> </TD><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_production", userId) + "</FONT></TD><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_usage", userId) + "</FONT></TD><TD> <FONT size=1pt>" + ML.getMLStr("statusbar_pop_total", userId) + "</FONT></TD></TR>";

        ressMouseOver += "<TR><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_planet", userId) + "</FONT></TD>";

        String color;

        if (ressource.getProductionUsage().getProductionNow() == 0) {
            color = "#000000";
        } else {
            color = "#33FF00";
        }

        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getProductionUsage().getProductionNow() + "</B></FONT></TD>";
        if (ressource.getProductionUsage().getUsageNow() == 0) {
            color = "#000000";
        } else {
            color = "#FFCC00";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right ><FONT  align=right size=1pt color=#000000 ><B>" + ressource.getProductionUsage().getUsageNow() + "</B></FONT></TD>";

        if (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow() > 0) {
            color = "#33FF00";
        } else if (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow() < 0) {
            color = "#FFCC00";
        } else {
            color = "#000000";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD></TR>";
        
        //Trade
        if (ressource.getTrade().getIncoming() != 0 && ressource.getTrade().getOutgoing() != 0) {
            ressMouseOver += "<TR><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_trade", userId) + "</FONT></TD>";
            if (ressource.getTrade().getIncoming() == 0) {
                color = "#000000";
            } else {
                color = "#33FF00";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + ressource.getTrade().getIncoming() + "</B></FONT></TD>";


            if (ressource.getTrade().getOutgoing() == 0) {
                color = "#000000";
            } else {
                color = "#FFCC00";
            }
            ressMouseOver += " <TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getTrade().getOutgoing() + "</B></FONT></TD>";
            if (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing() > 0) {
                color = "#33FF00";
            } else if (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing() < 0) {
                color = "#FFCC00";
            } else {
                color = "#000000";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing()) + "</B></FONT></TD></TR>";
        }
          //####TRANSPORT
        if (ressource.getTransport().getIncoming() != 0 && ressource.getTransport().getOutgoing() != 0) {
            ressMouseOver += "<TR><TD> <FONT size=1pt> " + "Transport" + "</FONT></TD>";
            if (ressource.getTransport().getIncoming() == 0) {
                color = "#000000";
            } else {
                color = "#33FF00";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + ressource.getTransport().getIncoming() + "</B></FONT></TD>";


            if (ressource.getTransport().getOutgoing() == 0) {
                color = "#000000";
            } else {
                color = "#FFCC00";
            }
            ressMouseOver += " <TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getTransport().getOutgoing() + "</B></FONT></TD>";
            if (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing() > 0) {
                color = "#33FF00";
            } else if (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing() < 0) {
                color = "#FFCC00";
            } else {
                color = "#000000";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing()) + "</B></FONT></TD></TR>";
        }
        ressMouseOver += "<TR><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_total", userId) + "</FONT></TD> ";

        if (ressource.getProductionUsage().getProductionNow() + ressource.getTrade().getIncoming() == 0) {
            color = "#000000";
        } else {
            color = "#33FF00";
        }

        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + (ressource.getProductionUsage().getProductionNow() + ressource.getTrade().getIncoming()) + "</B></FONT></TD>";

        if (ressource.getTrade().getOutgoing() + ressource.getProductionUsage().getUsageNow() == 0) {
            color = "#000000";
        } else {
            color = "#FFCC00";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getOutgoing() + ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD>";
        if (ressource.getTrade().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getProductionUsage().getUsageNow() > 0) {
            color = "#33FF00";
        } else if (ressource.getTrade().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getProductionUsage().getUsageNow() < 0) {
            color = "#FFCC00";
        } else {
            color = "#000000";
        }


        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD></TR></TABLE>";


        ressMouseOver += "')\" onmouseout=\"hideTip()\" ";
        ressIcon.setProperties(ressMouseOver);

        //NahrungsmouseOver Ende

        positionerPopulation.addDiv(ressIcon);

        positionerPopulation.addDiv(planetPopulation);

        div ressStorage = new div();
        ressStorage.setProperties("id='foodStorage' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_storeusage", userId) + ": " + statusBarData.getPopulationFood().getFood().getStorage().getPercent() + "%')\" onmouseout=\"hideTip()\"");
        positionerPopulation.addDiv(ressStorage);

        div foodProduction = new div();
        foodProduction.setProperties("id='foodProduction' onmouseover=\"doTooltip(event," + foodProductionToolTip + ")\" onmouseout=\"hideTip()\" ");
        positionerPopulation.addDiv(foodProduction);

        div foodUsage = new div();
        foodUsage.setProperties("id='foodUsage' onmouseover=\"doTooltip(event," + foodUsageToolTip + ")\" onmouseout=\"hideTip()\"");
        positionerPopulation.addDiv(foodUsage);

        td1.addDiv(positionerPopulation);


        tr1.addTd(td1);

        table.addTr(tr1);
        return table;
    }

    private String changeStorageImage(String storageImage, RessourceEntity ressource) {

        ProductionUsageEntity productionUsage = ressource.getProductionUsage();
        // Logger.getLogger().write("TradeSize4:  "+tradeRoutes.size());
        if (productionUsage.getProductionNow() + ressource.getTrade().getIncoming() > productionUsage.getUsageNow() + ressource.getTrade().getOutgoing()) {
            storageImage += "Plus";
        }
        // If Production < Usage RedImage
        if (productionUsage.getProductionNow() + ressource.getTrade().getIncoming() < productionUsage.getUsageNow() + ressource.getTrade().getOutgoing()) {
            storageImage += "Minus";
        }
        // Else normale picture
        storageImage += ".png";

        return storageImage;
    }

    public int calculateStorageBarLength(int minLength, int maxLength, StorageEntity storage) {

        if (storage.getStorageCapicity() > 0) {
            double stock = (double) storage.getStorageStock();
            double capicity = (double) storage.getStorageCapicity();

            double multiplicator = (stock / capicity);
            minLength = minLength + (int) Math.round(multiplicator * maxLength);
            storage.setPercent(Math.round(100 * multiplicator));
        }
        return minLength;
    }
}
