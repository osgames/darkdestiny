/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.menu.statusbar;

import at.viswars.view.menu.StatusBarData;
import java.util.Locale;

/**
 *
 * @author Bullet
 */
public abstract class StatusBarEntity implements IStatusBarEntity{

    public StatusBarData statusBarData;
    public int width;
    public int userId;

    public StatusBarEntity(StatusBarData statusBarData, int userId, int width){
        this.statusBarData = statusBarData;
        this.userId = userId;
        this.width = width;

    }


}
