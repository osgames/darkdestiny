/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.menu.statusbar.style.old;

import at.viswars.view.menu.statusbar.*;
import at.viswars.GameConfig;
import at.viswars.ML;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.StatusBarData;

/**
 *
 * @author Bullet
 */
public class CreditSpacer extends StatusBarEntity implements IStatusBarEntity{


    public CreditSpacer(StatusBarData stb, int userId, int width) {
        super(stb, userId, width);
    }
    @Override
    public style addStyle(style rootNode) {
         styleDivCont creditSpacerPositioner = new styleDivCont(
                    "creditSpacerPositioner", "relative", 0, 0, 1, 1, 0, 1, "", "");
            rootNode.addDivCont(creditSpacerPositioner);

            styleDivCont creditSpacerText = new styleDivCont(
                    "creditSpacerText", "absolute", -7, 10, 160, 1, 0, 1,"", "");
            rootNode.addDivCont(creditSpacerText);

            return rootNode;
    }

    @Override
    public table addTr(table table) {

              String properties = "";

        tr tr1 = new tr();

        td td1 = new td();

        properties += "height='" + 18 + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + GameConfig.picPath() + "pic/menu/creditsSpacer.png" + ")';  position:relative; background-repeat: repeat-x;";

        td1.setProperties(properties);

        tr1.addTd(td1);

        table.addTr(tr1);
        return table;
    }


}
