/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu.statusbar.style.old;

import at.viswars.view.menu.statusbar.*;
import at.viswars.FormatUtilities;
import at.viswars.GameConfig;
import at.viswars.GameConstants;
import at.viswars.ML;
import at.viswars.view.html.a;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.StatusBarData;
import java.util.Locale;

/**
 *
 * @author Aion
 */
public class MessageBar extends StatusBarEntity implements IStatusBarEntity {

    public MessageBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        styleDivCont positioner = new styleDivCont("positioner", "relative", -23, 0, 1, 1, 0, 1, GameConfig.picPath() + "pic/menu/messageBlink.png", "");
        rootNode.addDivCont(positioner);
       //Message DivContainers
        styleDivCont tmp0 = new styleDivCont("leftBlink", "absolute", 0, 40, 23, 23, 0, 1, GameConfig.picPath() + "pic/menu/messageBlink.png", "no-repeat");
        rootNode.addDivCont(tmp0);

        styleDivCont tmp1 = new styleDivCont("rightBlink", "absolute", 0, 137, 23, 23, 0, 1, GameConfig.picPath() + "pic/menu/messageBlink.png", "no-repeat");
        rootNode.addDivCont(tmp1);

        styleDivCont tmp2 = new styleDivCont("pBlink", "absolute", 24, 12, 50, 12, 0, 1, GameConfig.picPath() + "pic/menu/smallMessageBlink.png", "no-repeat");
        rootNode.addDivCont(tmp2);

        styleDivCont msgP = new styleDivCont("pNormal", "absolute", 24, 12, 50, 12, 0, 1, GameConfig.picPath() + "pic/menu/smallMessage.png", "no-repeat");
        rootNode.addDivCont(msgP);


        styleDivCont tmp3 = new styleDivCont("aBlink", "absolute", 24, 67, 50, 12, 0, 1, GameConfig.picPath() + "pic/menu/smallMessageBlink.png", "no-repeat");
        rootNode.addDivCont(tmp3);

                styleDivCont msgA = new styleDivCont("aNormal", "absolute", 24, 67, 50, 12, 0, 1, GameConfig.picPath() + "pic/menu/smallMessage.png", "no-repeat");
        rootNode.addDivCont(msgA);


        styleDivCont tmp4 = new styleDivCont("sBlink", "absolute", 24, 123, 50, 12, 0, 1, GameConfig.picPath() + "pic/menu/smallMessageBlink.png", "no-repeat");
        rootNode.addDivCont(tmp4);

        styleDivCont msgS = new styleDivCont("sNormal", "absolute", 24, 123, 50, 12, 0, 1, GameConfig.picPath() + "pic/menu/smallMessage.png", "no-repeat");
        rootNode.addDivCont(msgS);
        //>>>>>>>>><
        return rootNode;
    }

    @Override
    public table addTr(table table) {

      int height = 49;
        String imageUrl = GameConfig.picPath() + "pic/menu/messageImage.png";

        String properties = "";
        //String data = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ")';  position:relative; background-repeat: repeat-x;";

        div div0 = new div();
        div0.setProperties("id='positioner'");

        //Hinzuf?gen ob ?berhaupt eine Nachricht empfangen wurde
        if (statusBarData.getMessages().isMessageReceived()) {

            div div1 = new div();
            div1.setProperties("id='leftBlink'");
            div0.addDiv(div1);

            div div2 = new div();
            div2.setProperties("id='rightBlink'");
            div0.addDiv(div2);
        }
        //Hinzuf?gen ob ?berhaupt eine PRIVATE - Nachricht empfangen wurde
        //TODO Setzen des Hyperlinks
        if (statusBarData.getMessages().isPMessageReceived()) {

            div div3 = new div();
            div3.setProperties("id='pBlink'");
            a privateHref = new a("main.jsp?page=new/messages");
            div3.setHyperlink(privateHref);
            div0.addDiv(div3);
        }else if(!statusBarData.getMessages().isPMessageReceived()){

            div div3 = new div();
            div3.setProperties("id='pNormal'");
            a privateHref = new a("main.jsp?page=new/messages");
            div3.setHyperlink(privateHref);
            div0.addDiv(div3);
        }
        //Hinzuf?gen ob ?berhaupt eine ALLIANZ - Nachricht empfangen wurde
        //TODO Setzen des Hyperlinks
        if (statusBarData.getMessages().isAMessageReceived()) {
            div div4 = new div();
            div4.setProperties("id='aBlink'");
            a allianceHref = new a("main.jsp?page=new/alliance&type=11&from=0&to=15");
            div4.setHyperlink(allianceHref);
            div0.addDiv(div4);
        }else if(!statusBarData.getMessages().isAMessageReceived()){
             div div4 = new div();
            div4.setProperties("id='aNormal'");
            a allianceHref = new a("main.jsp?page=new/alliance&type=11&from=0&to=15");
            div4.setHyperlink(allianceHref);
            div0.addDiv(div4);
        }

        //Hinzuf?gen ob ?berhaupt eine SYSTEM-Nachricht empfangen wurde
        //TODO Setzen des Hyperlinks
        if (statusBarData.getMessages().isSMessageReceived()) {
            div div5 = new div();
            div5.setProperties("id='sBlink'");
            a systemHref = new a("main.jsp?page=new/messages");
            div5.setHyperlink(systemHref);
            div0.addDiv(div5);
        }else if(!statusBarData.getMessages().isSMessageReceived()){
              div div5 = new div();
            div5.setProperties("id='sNormal'");
            a systemHref = new a("main.jsp?page=new/messages");
            div5.setHyperlink(systemHref);
            div0.addDiv(div5);
        }
        td1.addDiv(div0);

        td1.setProperties(properties);
        td1.setData("");
        tr1.addTd(td1);
        table.addTr(tr1);
        return table;
    }
}
