/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu.statusbar.style.old;

import at.viswars.view.menu.statusbar.*;
import at.viswars.FormatUtilities;
import at.viswars.GameConfig;
import at.viswars.GameConstants;
import at.viswars.ML;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.StatusBarData;

/**
 *
 * @author Aion
 */
public class CreditBar extends StatusBarEntity implements IStatusBarEntity {

    public CreditBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        //Population DivContainers
        styleDivCont positionerCredits = new styleDivCont("positionerC", "relative", -23, 0, 1, 1, 0, 1, GameConfig.picPath() + "pic/menu/messageBlink.png", "");
        rootNode.addDivCont(positionerCredits);

        styleDivCont globalCredits = new styleDivCont("globalCredits", "absolute", 4, 90, 90, 1, 0, 1, "", "");
        rootNode.addDivCont(globalCredits);

        styleDivCont planetCredits = new styleDivCont("planetCredits", "absolute", 25, 90, 90, 1, 0, 1, "", "");
        rootNode.addDivCont(planetCredits);



        return rootNode;
    }

    @Override
    public table addTr(table table) {
        int height = 52;
        String imageUrl = GameConfig.picPath() + "pic/menu/creditsImage.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ")';  position:relative; background-repeat: repeat-x;";

        td1.setProperties(properties);

        div positionerC = new div();
        positionerC.setProperties("id='positionerC'");


        div globalCredits = new div();
        globalCredits.setProperties("id='globalCredits' align=right");
        globalCredits.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormatScaledNumber(statusBarData.getCredits().getGlobalCredits(), 2, GameConstants.VALUE_TYPE_NORMAL) + "</FONT>");
        positionerC.addDiv(globalCredits);

        div planetCredits = new div();
        planetCredits.setProperties("id='planetCredits' align=right");
        planetCredits.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormatScaledNumber(statusBarData.getCredits().getPlanetCredits(), 2, GameConstants.VALUE_TYPE_NORMAL) + "</FONT>");
        positionerC.addDiv(planetCredits);

        td1.addDiv(positionerC);

        tr1.addTd(td1);
        table.addTr(tr1);
        return table;
    }
}
