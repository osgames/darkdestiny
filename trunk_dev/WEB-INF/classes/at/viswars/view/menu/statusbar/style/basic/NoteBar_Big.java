/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu.statusbar.style.basic;

import at.viswars.view.menu.statusbar.*;
import at.viswars.FormatUtilities;
import at.viswars.GameConfig;
import at.viswars.GameConstants;
import at.viswars.ML;
import at.viswars.view.html.a;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.StatusBarData;

/**
 *
 * @author Aion
 */
public class NoteBar_Big extends StatusBarEntity implements IStatusBarEntity {

    public NoteBar_Big(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        //Population DivContainers
        styleDivCont positionerNote = new styleDivCont("positionerNoteBig", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(positionerNote);


        styleDivCont privateString = new styleDivCont("noteStringBig", "absolute", 1, 31, 120, 60, 0, 1, "", "");

        rootNode.addDivCont(privateString);

        String imageIcon = GameConfig.picPath() + "pic/menu/statusbar/stb_notessmall_button.png";
        styleDivCont noteIcon = new styleDivCont("noteBigIcon", "absolute", 13, 7, 30, 30, 0, 1, imageIcon, "no-repeat");
        // >>>>>>>>>>>>>

        rootNode.addDivCont(noteIcon);


        //>>>>>>>>><
        return rootNode;
    }

    @Override
    public table addTr(table table) {

        int height = 56;
        String imageUrl = GameConfig.picPath() + "pic/menu/statusbar/stb_notesbig_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ")';  position:relative; background-repeat: repeat-x;";

        td1.setProperties(properties);

        div positionerNote = new div();
        positionerNote.setProperties("id='positionerNoteBig'");

        div privateString = new div();
        if (statusBarData.getNote() != null && !statusBarData.getNote().getMessage().equals("")) {
            privateString.setProperties("id='noteStringBig' style='overflow:hidden;width:130px; height:40px;' onmouseover=\"doTooltip(event,'" + statusBarData.getNote().getMessage() + "')\" onmouseout=\"hideTip()\" align=left");
        } else {
            privateString.setProperties("id='noteStringBig' align=left style='overflow:auto;width:130px; height:40px;'");

        }
        if (statusBarData.getNote() != null && !statusBarData.getNote().getMessage().equals("")) {
            String message = statusBarData.getNote().getMessage();
            if(message.length() > 40){
                message = message.substring(0,40) + "...";
            }
            privateString.setData("<TABLE style='width:130px; height:40px;table-layout:fixed; word-wrap:break-word;'><TR><TD ><FONT color=#FFFFFF size='-2'><B>" + message + "</B></TD></TR></TABLE></FONT>");
        } else {
            privateString.setData("<FONT color=#FFFFFF size='-2'><B> </B></FONT>");
        }


        positionerNote.addDiv(privateString);

        div noteIcon = new div();
        noteIcon.setProperties("id='noteBigIcon' onmouseover=\"doTooltip(event,'" + ML.getMLStr("administration_pop_editnote", userId) + "')\" onmouseout=\"hideTip()\" ");



        a privateHref = new a("noteEditor.jsp?planetId=" + statusBarData.getPlanetId());
        privateHref.setPropertes(" onClick='return popupNote(this, \"editor\")' ");
        noteIcon.setHyperlink(privateHref);
        positionerNote.addDiv(noteIcon);
        td1.addDiv(positionerNote);
        td1.setProperties(properties);
        td1.setData("");
        tr1.addTd(td1);

        // =>>>>>>>>>><
        table.addTr(tr1);

        return table;
    }
}
