/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.menu.statusbar;

import at.viswars.view.html.style;
import at.viswars.view.html.table;

/**
 *
 * @author Bullet
 */
public interface IStatusBarEntity{

    abstract style addStyle(style rootNode);

    abstract table addTr(table table);
    
}
