/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view.menu.statusbar.style.basic;

import at.viswars.view.menu.statusbar.*;
import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.FormatUtilities;
import at.viswars.GameConfig;
import at.viswars.ML;
import at.viswars.view.html.div;
import at.viswars.view.html.style;
import at.viswars.view.html.styleDivCont;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.view.menu.RessourceEntity;
import at.viswars.view.menu.StatusBarData;
import java.util.Locale;

/**
 *
 * @author Bullet
 */
public class EnergyBar extends StatusBarEntity implements IStatusBarEntity{


    public EnergyBar(StatusBarData statusBarData, int userId, int width) {
            super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {

        RessourceEntity energy = statusBarData.getEnergy().getEnergy();
        int maxLength = 104;
        int maxLengthUsageTotal = maxLength;
        int maxLengthProductionTotal = maxLength;


        String productionNowImage = "pic/menu/energyProductionNow.png";
        String productionLaterImage = "pic/menu/energyProductionLater.png";
//        String productionEqual = "pic/menu/energyEqual.png";

        String usageNowImage = "pic/menu/energyUsageNow.png";
        String usageLaterImage = "pic/menu/energyUsageLater.png";
//        String usageEqual = "pic/menu/energyEqual.png";

        int barLengthProductionNow = 0;
        int barLengthProductionLater = 0;
        int barLengthUsageNow = 0;
        int barLengthUsageLater = 0;

        int productionNowZIndex = 2;
        int usageNowZIndex = 2;

        /**
         *
         * Zum richtigen Skalieren muss als erstes der Wert herausgefunden
         * werden der am h?chsten ist und an ihm wird dann skaliert
         *
         *
         */
        long productionNow = energy.getProductionUsage().getProductionNow();
        long productionLater = energy.getProductionUsage().getProductionLater();
        long usageNow = energy.getProductionUsage().getUsageNow();
        long usageLater = energy.getProductionUsage().getUsageLater();

        if (usageLater > productionLater) {
            maxLengthProductionTotal = (int) Math.round((double) maxLength * getMultiplicator(usageLater, productionLater));

            if (usageLater >= usageNow) {
                barLengthUsageLater = maxLengthUsageTotal;
                barLengthUsageNow = 0 + (int) Math.round((double) maxLengthUsageTotal * getMultiplicator(usageLater, usageNow));
            } else {
                barLengthUsageNow = maxLengthUsageTotal;
                usageNowZIndex = 2;
                barLengthUsageLater = 0 + (int) Math.round((double) maxLengthUsageTotal * getMultiplicator(usageNow, usageLater));
            }

            if (productionLater >= productionNow) {
                barLengthProductionLater = maxLengthProductionTotal;
                barLengthProductionNow = 0 + (int) Math.round((double) maxLengthProductionTotal * getMultiplicator(productionLater, productionNow));
            } else {
                barLengthProductionNow = maxLengthProductionTotal;
                productionNowZIndex = 2;
                barLengthProductionLater = 0 + (int) Math.round((double) maxLengthProductionTotal * getMultiplicator(productionNow, productionLater));
            }
        } else if (usageLater == productionLater) {
            if (usageLater >= usageNow) {
                barLengthUsageLater = maxLengthUsageTotal;
                barLengthUsageNow = 0 + (int) Math.round((double) maxLengthUsageTotal * getMultiplicator(usageLater, usageNow));
            } else {
                barLengthUsageNow = maxLengthUsageTotal;
                usageNowZIndex = 2;
                barLengthUsageLater = 0 + (int) Math.round((double) maxLengthUsageTotal * getMultiplicator(usageNow, usageLater));
            }

            if (productionLater >= productionNow) {
                barLengthProductionLater = maxLengthProductionTotal;
                barLengthProductionNow = 0 + (int) Math.round((double) maxLengthProductionTotal * getMultiplicator(productionLater, productionNow));
            } else {
                barLengthProductionNow = maxLengthProductionTotal;
                productionNowZIndex = 2;
                barLengthProductionLater = 0 + (int) Math.round((double) maxLengthProductionTotal * getMultiplicator(productionNow, productionLater));
            }
        } else if (usageLater < productionLater) {
            maxLengthUsageTotal = (int) Math.round((double) maxLength * getMultiplicator(productionLater, usageLater));

            if (usageLater >= usageNow) {
                barLengthUsageLater = maxLengthUsageTotal;
                barLengthUsageNow = 0 + (int) Math.round((double) maxLengthUsageTotal * getMultiplicator(usageLater, usageNow));
            } else {
                barLengthUsageNow = maxLengthUsageTotal;
                usageNowZIndex = 2;
                barLengthUsageLater = 0 + (int) Math.round((double) maxLengthUsageTotal * getMultiplicator(usageNow, usageLater));
            }

            if (productionLater >= productionNow) {
                barLengthProductionLater = maxLengthProductionTotal;
                barLengthProductionNow = 0 + (int) Math.round((double) maxLengthProductionTotal * getMultiplicator(productionLater, productionNow));
            } else {
                barLengthProductionNow = maxLengthProductionTotal;
                productionNowZIndex = 2;
                barLengthProductionLater = 0 + (int) Math.round((double) maxLengthProductionTotal * getMultiplicator(productionNow, productionLater));
            }
        } else {
            DebugBuffer.addLine(DebugLevel.ERROR, "Energie wird nicht angezeigt: ");
            DebugBuffer.addLine(DebugLevel.ERROR, ":Verbrauch jetzt: " + usageNow);
            DebugBuffer.addLine(DebugLevel.ERROR, ":Verbrauch sp�ter: " + usageLater);
            DebugBuffer.addLine(DebugLevel.ERROR, ":Produktion jetzt: " + productionNow);
            DebugBuffer.addLine(DebugLevel.ERROR, ":Produktion sp�ter: " + productionLater);
        }

        styleDivCont energyPositioner = new styleDivCont("energyPositioner", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(energyPositioner);

        styleDivCont energyIcon = new styleDivCont("energyIcon", "absolute", 14, 15, 20, 20, 0, productionNowZIndex, GameConfig.picPath() + "pic/menu/icons/energy.png", "no-repeat");
        rootNode.addDivCont(energyIcon);


        styleDivCont energyProductionNow = new styleDivCont("energyProductionNow", "absolute", 11, 57, barLengthProductionNow, 9, 0, productionNowZIndex, GameConfig.picPath() + productionNowImage, "no-repeat");
        rootNode.addDivCont(energyProductionNow);

        styleDivCont energyProductionLater = new styleDivCont("energyProductionLater", "absolute", 11, 57, barLengthProductionLater, 9, 0, 1, GameConfig.picPath() + productionLaterImage, "no-repeat");
        rootNode.addDivCont(energyProductionLater);

        styleDivCont energyUsageNow = new styleDivCont("energyUsageNow", "absolute", 27, 57, barLengthUsageNow, 9, 0, usageNowZIndex, GameConfig.picPath() + usageNowImage, "no-repeat");
        rootNode.addDivCont(energyUsageNow);

        styleDivCont energyUsageLater = new styleDivCont("energyUsageLater", "absolute", 27, 57, barLengthUsageLater, 9, 0, 1, GameConfig.picPath() + usageLaterImage, "no-repeat");
        rootNode.addDivCont(energyUsageLater);

 

        return rootNode;
    }
    @Override
    public table addTr(table table) {
        String productionNow = String.valueOf(FormatUtilities.getFormattedNumber(statusBarData.getEnergy().getEnergy().getProductionUsage().getProductionNow(), ML.getLocale(userId)));
        String productionLater = String.valueOf(FormatUtilities.getFormattedNumber(statusBarData.getEnergy().getEnergy().getProductionUsage().getProductionLater(), ML.getLocale(userId)));
        String usageNow = String.valueOf(FormatUtilities.getFormattedNumber(statusBarData.getEnergy().getEnergy().getProductionUsage().getUsageNow(), ML.getLocale(userId)));
        String usageLater = String.valueOf(FormatUtilities.getFormattedNumber(statusBarData.getEnergy().getEnergy().getProductionUsage().getUsageLater(), ML.getLocale(userId)));


        String productionNowToolTip = "'" + ML.getMLStr("statusbar_pop_productionatthemoment", userId) + ":" + productionNow + "<br>" + "" + ML.getMLStr("statusbar_pop_productioninfuture", userId) + ":" + productionLater + "'";
        String productionLaterToolTip = "'" + ML.getMLStr("statusbar_pop_productionatthemoment", userId) + ":" + productionNow + "<br>" + "" + ML.getMLStr("statusbar_pop_productioninfuture", userId) + ":" + productionLater + "'";

        String usageNowToolTip = "'" + ML.getMLStr("statusbar_pop_usageatthemoment", userId) + ":" + usageNow + "<br>" + "" + ML.getMLStr("statusbar_pop_usageinfuture", userId) + ":" + usageLater + "'";
        String usageLaterToolTip = "'" + ML.getMLStr("statusbar_pop_usageatthemoment", userId) + ":" + usageNow + "<br>" + "" + ML.getMLStr("statusbar_pop_usageinfuture", userId) + ":" + usageLater + "'";

        if (statusBarData.getEnergy().getEnergy().getProductionUsage().getProductionNow() == statusBarData.getEnergy().getEnergy().getProductionUsage().getProductionLater()) {
            productionNowToolTip = "'" + ML.getMLStr("statusbar_pop_production", userId) + ":" + productionNow + "'";
            productionLaterToolTip = "'" + ML.getMLStr("statusbar_pop_production", userId) + ":" + productionNow + "'";
        }
        if (statusBarData.getEnergy().getEnergy().getProductionUsage().getUsageNow() == statusBarData.getEnergy().getEnergy().getProductionUsage().getUsageLater()) {
            usageNowToolTip = "'" + ML.getMLStr("statusbar_pop_usage", userId) + ":" + usageNow + "'";
            usageLaterToolTip = "'" + ML.getMLStr("statusbar_pop_usage", userId) + ":" + usageNow + "'";


        }

        int height = 50;
        String imageUrl = GameConfig.picPath() + "pic/menu/statusbar/stb_energy_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ")';  position:relative; background-repeat: repeat-x;";


        td1.setProperties(properties);
        div energyPositioner = new div();
        energyPositioner.setProperties("id='energyPositioner'");


        div energyIcon = new div();
        energyIcon.setProperties("id='energyIcon' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_energy", userId) + "')\" onmouseout=\"hideTip()\" ");
        energyPositioner.addDiv(energyIcon);

        div energyProductionNow = new div();
        energyProductionNow.setProperties("id='energyProductionNow' onmouseover=\"doTooltip(event," + productionNowToolTip + ")\" onmouseout=\"hideTip()\" ");
        energyPositioner.addDiv(energyProductionNow);

        div energyProductionLater = new div();
        energyProductionLater.setProperties("id='energyProductionLater'onmouseover=\"doTooltip(event," + productionLaterToolTip + ")\"  onmouseout=\"hideTip()\" ");
        energyPositioner.addDiv(energyProductionLater);

        div energyUsageNow = new div();
        energyUsageNow.setProperties("id='energyUsageNow' onmouseover=\"doTooltip(event," + usageNowToolTip + ")\" onmouseout=\"hideTip()\" ");
        energyPositioner.addDiv(energyUsageNow);

        div energyUsageLater = new div();
        energyUsageLater.setProperties("id='energyUsageLater' onmouseover=\"doTooltip(event," + usageLaterToolTip + ")\" onmouseout=\"hideTip()\" ");
        energyPositioner.addDiv(energyUsageLater);


        td1.addDiv(energyPositioner);

        td1.setProperties(properties);

        tr1.addTd(td1);

        table.addTr(tr1);
        return table;
    }

    private double getMultiplicator(double higherValue, double lowerValue) {
        double multiplicator = (lowerValue / higherValue);
        return multiplicator;

    }

}
