/*
 * LeftMenu.java
 *
 * Created on 03. Juli 2008, 14:03
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.viswars.view.menu;

import at.viswars.GameConfig;
import at.viswars.Logger.Logger;
import at.viswars.ML;
import at.viswars.chat.ServerListener;
import at.viswars.service.Service;
import at.viswars.viewbuffer.MenuBuffer;
import at.viswars.model.User;
import at.viswars.model.UserData;
import at.viswars.model.MenuLink;
import at.viswars.model.MenuImage;
import at.viswars.model.UserSettings;
import at.viswars.view.html.table;
import at.viswars.view.html.td;
import at.viswars.view.html.tr;
import at.viswars.webservice.WebData;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * Klasse zum HTML-Aufbau des linken Men?s
 *
 * @author Dreloc
 */
public class LeftMenu {

    private int subCategory = 0;
    private int userId = 0;
    //Hyperlinktext Eigenschaften
    private final String fontColor = "WHITE";
    //private final String linkSize = "2";
    private final String titleSize = "3";
    //Zusammengest?pselte Table
    private table leftMenuTable;
    //Datenbankwerte
    //Hintergrundbilder
    public static HashMap<Integer, MenuImage> menuImages;
    //Die zu darstellenden Links
    public static ArrayList<MenuLink> menuLinks;
    private User u;
    private UserData ud;
    private UserSettings us;

    /**
     * Creates a new instance of LeftMenu
     */
    public LeftMenu(int subCategory, int userId, boolean isAdmin) {

        this.subCategory = subCategory;
        this.userId = userId;
        //Laden aus der Statischen Klasse
        menuImages = MenuBuffer.getMenuImages();
        menuLinks = MenuBuffer.getMenuLinks();
        try {

            u = Service.userDAO.findById(userId);
            ud = Service.userDataDAO.findByUserId(userId);
            us = Service.userSettingsDAO.findByUserId(userId);
        } catch (Exception e) {
            Logger.getLogger().write("LeftMenu error : " + e);
        }
        //Aufbauen des Men?s
        leftMenuTable = buildLeftMenu(isAdmin);
    }

    public table buildLeftMenu(boolean isAdmin) {

        //Table
        leftMenuTable = new table();
        //Setzen des Randes und cellspacing sowie cellpading auf null
        leftMenuTable.setProperties(" border = '0' cellspacing='0' cellpadding='0'");


        //Statisch
        //Hinzuf?gen des Tops Elements (Tickanzeige)
   /*     MenuLink topImage = new MenuLink();
         topImage.setMenuImageId(4);


         td topImageTd = menuLinkToHtml(topImage);

         topImageTd.setData("<div id='tickTime' align=right><form  name='countdownform'><input size ='3' align='right' style='border: solid 2px #000000; text-align:right; color: #ffffff; background-color: #000000;' name='countdowninput'>    </form></div>");
         leftMenuTable.addTr(new tr(topImageTd));
         */
        //Durch die Linkliste der Datenbank durchloopen
        for (int i = 0; i < menuLinks.size(); i++) {
            if (menuLinks.get(i).getLinkName().equals("menu_welcome") && (ud == null || !us.getShowWelcomeSite())) {
                continue;
            }
            if (menuLinks.get(i).getSubCategory() != subCategory) {

                //Enth&auml;lt nicht die gew&uuml;nschte Subcategory
                continue;
            }

            /**
             *
             * Eine Abfrage ob der User ein Admin ist
             *
             */
            if (menuLinks.get(i).getAdminOnly() && (!isAdmin)) {
                continue;
            }


            tr tr1 = new tr();
            MenuLink menuLink = menuLinks.get(i);

            td tdTmp = new td();
            //Erstellen der <TD> Zelle f?r das Item aus der Datenbank
            tdTmp = menuLinkToHtml(menuLink);
            //Hinzuf?gen zur Spalte
            tr1.addTd(tdTmp);
            //Hinzuf?gen zur Tabelle
            leftMenuTable.addTr(tr1);
        }
        return leftMenuTable;
    }

    public static String replaceConstant(String constant) {
        String replacement = "";

        if (constant.equalsIgnoreCase("%CHAT_USR%")) {
            replacement = String.valueOf(WebData.getChatUserCount());
        }

        return replacement;
    }

    public td menuLinkToHtml(MenuLink menuLink) {

        td td1 = new td();

        MenuImage menuImage = menuImages.get(menuLink.getMenuImageId());
        int type = menuImage.getType();
        String properties = "";
        String data = "";

        /**
         * @type 0 : Link-Item (Link mit Hintergrund) 1 : Title-Item
         * (?berschrift) 2 : Spacer
         *
         */
        switch (type) {
            case 0:


                properties += "height='" + menuImage.getHeight() + "' ";
                properties += "width='" + menuImage.getWidth() + "' ";
                properties += "align='center' ";
                properties += "style='background-image: url(" + GameConfig.picPath() + menuImage.getImageUrl() + ")'; background-repeat: repeat-x;";

                data += "<a ";
                String link = menuLink.getLinkUrl();
                String target = menuLink.getLinkDest();

                if (us != null) {
                    if (menuLink.getLinkName().equals("menu_starmap") && us.getStarmapExternalBrowser()) {
                        target = "_new";
                        link = "visstarmapextern.jsp";
                    }
                    if (menuLink.getLinkName().equals("menu_techtree") && us.getTtvExternalBrowser()) {
                        target = "_new";
                        link = "techtreeextern.jsp";
                    }
                }

                data += "href='" + link + "' ";
                if (target != null || target.equals("")) {
                    data += "target=" + target + " ";
                }

                data += ">";
                //  data += "<font color='" + fontColor + "' size ='" + linkSize + "'>";
                String tmpLinkName = ML.getMLStr(menuLink.getLinkName(), userId);
                tmpLinkName = tmpLinkName.replace("%CHAT_USR%", replaceConstant("%CHAT_USR%"));
                data += tmpLinkName;
                //  data += "</font> ";
                data += "</a>";

                td1.setProperties(properties);
                td1.setData(data);
                break;

            case 1:

                properties += "height='" + menuImage.getHeight() + "' ";
                properties += "width='" + menuImage.getWidth() + "' ";
                properties += "align='center' ";
                properties += "style='background-image: url(" + GameConfig.picPath() + menuImage.getImageUrl() + ")'; background-repeat: repeat-x;";


                data += "<font color='" + fontColor + "' size ='" + titleSize + "'><b>";

                tmpLinkName = ML.getMLStr(menuLink.getLinkName(), userId);
                tmpLinkName = tmpLinkName.replace("%CHAT_USR%", replaceConstant("%CHAT_USR%"));
                data += tmpLinkName;
                data += "</b></font> ";


                td1.setProperties(properties);
                td1.setData(data);

                break;
            case 2:
                properties += "height='" + menuImage.getHeight() + "' ";
                properties += "width='" + menuImage.getWidth() + "' ";
                properties += "align='center' ";
                properties += "style='background-image: url(" + GameConfig.picPath() + menuImage.getImageUrl() + ")'; background-repeat: repeat-x;";

                td1.setProperties(properties);
                td1.setData(data);

                break;
            case 3:
                properties += "height='" + menuImage.getHeight() + "' ";
                properties += "width='" + menuImage.getWidth() + "' ";
                properties += "align='center' ";
                properties += "style='background-image: url(" + GameConfig.picPath() + menuImage.getImageUrl() + ")'; background-repeat: repeat-x;";

                td1.setProperties(properties);
                td1.setData(data);

                break;
        }


        return td1;
    }

    public table getLeftMenuTable() {
        return leftMenuTable;
    }

    public void setLeftMenuTable(table leftMenuTable) {
        this.leftMenuTable = leftMenuTable;
    }

    public int getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(int subCategory) {
        this.subCategory = subCategory;
    }
}
