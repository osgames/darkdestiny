 

package at.viswars.view.menu;

/**
 *
 * Abbildung der Datenbanktabelle
 * MenuImages
 * 
 * @author Dreloc
 */
public class MenuLink {
    private int id;
    private int linkOrder;
    private String linkName;
    private String linkUrl;
    private String linkDest;
    //Refers to SQL Table: menuImage-ID
    private int menuLinkId;
    private int accessLevel;
    private boolean adminOnly;
    private int subCategory;
    /** Creates a new instance of MenuLink */
    public MenuLink() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkDest() {
        return linkDest;
    }

    public void setLinkDest(String linkDest) {
        this.linkDest = linkDest;
    }


    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public boolean isAdminOnly() {
        return adminOnly;
    }

    public void setAdminOnly(boolean adminOnly) {
        this.adminOnly = adminOnly;
    }

    public int getLinkOrder() {
        return linkOrder;
    }

    public void setLinkOrder(int linkOrder) {
        this.linkOrder = linkOrder;
    }

    public int getMenuLinkId() {
        return menuLinkId;
    }

    public void setMenuLinkId(int menuLinkId) {
        this.menuLinkId = menuLinkId;
    }

    public int getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(int subCategory) {
        this.subCategory = subCategory;
    }


   
    
}
