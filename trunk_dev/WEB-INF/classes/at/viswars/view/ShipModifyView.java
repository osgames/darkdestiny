/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ShipModifyView {
    private final int fleetId;
    private final ArrayList<ShipModifyEntry> shipList = new ArrayList<ShipModifyEntry>();
    
    public ShipModifyView(int fleetId) {
        this.fleetId = fleetId;
    }
    
    public void addEntry (ShipModifyEntry sme) {
        getShipList().add(sme);
    }

    public int getFleetId() {
        return fleetId;
    }

    public ArrayList<ShipModifyEntry> getShipList() {
        return shipList;
    }
}
