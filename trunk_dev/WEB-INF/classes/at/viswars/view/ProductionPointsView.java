/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.view;

/**
 *
 * @author Orlov
 */
public class ProductionPointsView {

    private final int modulePoints;
    private final int dockPoints;
    private final int orbDockPoints;
    private final int kasPoints;
    private final double modBonus;
    private final double dockBonus;
    private final double orbDockBonus;
    private final double kasBonus;

    public ProductionPointsView(int modulePoints, int dockPoints, int orbDockPoints, int kasPoints, double modBonus, double dockBonus, double orbDockBonus, double kasBonus) {
        this.modulePoints = modulePoints;
        this.dockPoints = dockPoints;
        this.orbDockPoints = orbDockPoints;
        this.kasPoints = kasPoints;
        this.modBonus = modBonus;
        this.dockBonus = dockBonus;
        this.orbDockBonus = orbDockBonus;
        this.kasBonus = kasBonus;
    }

    /**
     * @return the modulePoints
     */
    public int getModulePoints() {
        return modulePoints;
    }

    /**
     * @return the dockPoints
     */
    public int getDockPoints() {
        return dockPoints;
    }

    /**
     * @return the orbDockPoints
     */
    public int getOrbDockPoints() {
        return orbDockPoints;
    }

    /**
     * @return the kasPoints
     */
    public int getKasPoints() {
        return kasPoints;
    }

    /**
     * @return the modBonus
     */
    public double getModBonus() {
        return modBonus;
    }

    /**
     * @return the dockBonus
     */
    public double getDockBonus() {
        return dockBonus;
    }

    /**
     * @return the orbDockBonus
     */
    public double getOrbDockBonus() {
        return orbDockBonus;
    }

    /**
     * @return the kasBonus
     */
    public double getKasBonus() {
        return kasBonus;
    }
}
