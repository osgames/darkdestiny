/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view;

/**
 *
 * @author Stefan
 */
public class AdministrationMessage {
    private final EAdminMessageType type;
    private final String text;
    
    public AdministrationMessage(EAdminMessageType type, String text) {
        this.type = type;
        this.text = text;
    }

    public EAdminMessageType getType() {
        return type;
    }

    public String getText() {
        return text;
    }
    
    public String printText() {
        String tmpText = "";
        
        if (type == EAdminMessageType.WARNING) {
            tmpText += "<SPAN style=\"color:#FFA500;\">";
        } else if (type == EAdminMessageType.CRITICAL) {
            tmpText += "<SPAN style=\"color:#FF6347;\">";
        }

        tmpText += text;
        
        if (type != EAdminMessageType.INFO) {
            tmpText += "</SPAN>";
        }
        
        return tmpText;
    }
}
