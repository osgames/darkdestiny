/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.view.header;

import at.viswars.view.header.CategoryEntity;
import java.util.HashMap;

/**
 *
 * @author Horst
 */
public class HeaderData {            
    private HashMap<Integer,CategoryEntity> categorys;
    
    public HeaderData(){
        categorys = new HashMap<Integer,CategoryEntity>();
    }

    public HashMap<Integer, CategoryEntity> getCategorys() {
        return categorys;
    }

    public void setCategorys(HashMap<Integer, CategoryEntity> categorys) {
        this.categorys = categorys;
    }

  

}
