/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation("designmodule")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class DesignModule extends Model<DesignModule> {

    @IdFieldAnnotation
    @FieldMappingAnnotation("designId")
    private Integer designId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("moduleId")
    private Integer moduleId;
    @FieldMappingAnnotation("isConstruction")
    private Boolean isConstruction;
    @FieldMappingAnnotation("count")
    private Integer count;

    public Integer getDesignId() {
        return designId;
    }

    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Boolean getIsConstruction() {
        return isConstruction;
    }

    public void setIsConstruction(Boolean isConstruction) {
        this.isConstruction = isConstruction;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
