/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "votingdetails")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class VotingDetail extends Model<VotingDetail> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @IndexAnnotation(indexName = "voteId")
    @FieldMappingAnnotation("voteId")
    private Integer voteId;
    @FieldMappingAnnotation("userId")
    private Integer userId;
    @FieldMappingAnnotation("optionVoted")
    private Integer optionVoted;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the voteId
     */
    public Integer getVoteId() {
        return voteId;
    }

    /**
     * @param voteId the voteId to set
     */
    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the optionVoted
     */
    public Integer getOptionVoted() {
        return optionVoted;
    }

    /**
     * @param optionVoted the optionVoted to set
     */
    public void setOptionVoted(Integer optionVoted) {
        this.optionVoted = optionVoted;
    }
}
