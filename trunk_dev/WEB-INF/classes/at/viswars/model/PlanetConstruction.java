/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "planetconstruction")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlanetConstruction extends Model<PlanetConstruction> {

    @FieldMappingAnnotation("planetId")
    @IdFieldAnnotation
    private Integer planetId;
    @FieldMappingAnnotation("constructionId")
    @IdFieldAnnotation
    private Integer constructionId;
    @FieldMappingAnnotation("number")
    private Integer number;
    @FieldMappingAnnotation("idle")
    @DefaultValue("0")
    private Integer idle;
    @FieldMappingAnnotation("level")
    @DefaultValue("-1")
    private Integer level;

    public PlanetConstruction() {
    }

    public PlanetConstruction(int planetId, int constructionId, int number) {
        this.planetId = planetId;
        this.constructionId = constructionId;
        this.number = number;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the constructionId
     */
    public Integer getConstructionId() {
        return constructionId;
    }

    /**
     * @param constructionId the constructionId to set
     */
    public void setConstructionId(Integer constructionId) {
        this.constructionId = constructionId;
    }

    /**
     * @return the number
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    public int getActiveCount() {
        return number - idle;
    }

    /**
     * @return the idle
     */
    public Integer getIdle() {
        return idle;
    }

    /**
     * @param idle the idle to set
     */
    public void setIdle(Integer idle) {
        this.idle = idle;
    }

    /**
     * @return the level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(Integer level) {
        this.level = level;
    }
}
