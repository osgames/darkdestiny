/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.ECombatStatus;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "groundcombat")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class GroundCombat extends Model<GroundCombat> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @IndexAnnotation(indexName = "planetId")
    @FieldMappingAnnotation("planetId")
    private Integer planetId;
    @FieldMappingAnnotation("startTime")
    private Long startTime;
    @FieldMappingAnnotation("winner")
    private Integer winner;
    @FieldMappingAnnotation("planetOwner")
    private Integer planetOwner;
    @FieldMappingAnnotation("combatStatus")
    private ECombatStatus combatStatus;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the combatStatus
     */
    public ECombatStatus getCombatStatus() {
        return combatStatus;
    }

    /**
     * @param combatStatus the combatStatus to set
     */
    public void setCombatStatus(ECombatStatus combatStatus) {
        this.combatStatus = combatStatus;
    }

    /**
     * @return the winner
     */
    public Integer getWinner() {
        return winner;
    }

    /**
     * @param winner the winner to set
     */
    public void setWinner(Integer winner) {
        this.winner = winner;
    }

    /**
     * @return the planetOwner
     */
    public Integer getPlanetOwner() {
        return planetOwner;
    }

    /**
     * @param planetOwner the planetOwner to set
     */
    public void setPlanetOwner(Integer planetOwner) {
        this.planetOwner = planetOwner;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
}
