/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "fleetdetails")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class FleetDetail extends Model<FleetDetail> {

    @FieldMappingAnnotation("Id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation(value = "fleetId")
    @IndexAnnotation(indexName = "fleetId")
    private Integer fleetId;
    @FieldMappingAnnotation(value = "startSystem")
    private Integer startSystem;
    @FieldMappingAnnotation(value = "startPlanet")
    private Integer startPlanet;
    @FieldMappingAnnotation(value = "destSystem")
    private Integer destSystem;
    @FieldMappingAnnotation(value = "destPlanet")
    private Integer destPlanet;
    @FieldMappingAnnotation(value = "commandTypeId")
    private Integer commandTypeId;
    @FieldMappingAnnotation(value = "commandType")
    private Integer commandType;
    @FieldMappingAnnotation(value = "startTime")
    private Integer startTime;
    @FieldMappingAnnotation(value = "flightTime")
    private Integer flightTime;
    @FieldMappingAnnotation(value = "speed")
    private Float speed;
    @FieldMappingAnnotation(value = "startX")
    private Integer startX;
    @FieldMappingAnnotation(value = "startY")
    private Integer startY;
    @FieldMappingAnnotation(value = "destX")
    private Integer destX;
    @FieldMappingAnnotation(value = "destY")
    private Integer destY;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the fleetId
     */
    public Integer getFleetId() {
        return fleetId;
    }

    /**
     * @param fleetId the fleetId to set
     */
    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    /**
     * @return the startSystem
     */
    public Integer getStartSystem() {
        return startSystem;
    }

    /**
     * @param startSystem the startSystem to set
     */
    public void setStartSystem(Integer startSystem) {
        this.startSystem = startSystem;
    }

    /**
     * @return the startPlanet
     */
    public Integer getStartPlanet() {
        return startPlanet;
    }

    /**
     * @param startPlanet the startPlanet to set
     */
    public void setStartPlanet(Integer startPlanet) {
        this.startPlanet = startPlanet;
    }

    /**
     * @return the destSystem
     */
    public Integer getDestSystem() {
        return destSystem;
    }

    /**
     * @param destSystem the destSystem to set
     */
    public void setDestSystem(Integer destSystem) {
        this.destSystem = destSystem;
    }

    /**
     * @return the destPlanet
     */
    public Integer getDestPlanet() {
        return destPlanet;
    }

    /**
     * @param destPlanet the destPlanet to set
     */
    public void setDestPlanet(Integer destPlanet) {
        this.destPlanet = destPlanet;
    }

    /**
     * @return the commandTypeId
     */
    public Integer getCommandTypeId() {
        return commandTypeId;
    }

    /**
     * @param commandTypeId the commandTypeId to set
     */
    public void setCommandTypeId(Integer commandTypeId) {
        this.commandTypeId = commandTypeId;
    }

    /**
     * @return the commandType
     */
    public Integer getCommandType() {
        return commandType;
    }

    /**
     * @param commandType the commandType to set
     */
    public void setCommandType(Integer commandType) {
        this.commandType = commandType;
    }

    /**
     * @return the startTime
     */
    public Integer getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the flightTime
     */
    public Integer getFlightTime() {
        return flightTime;
    }

    /**
     * @param flightTime the flightTime to set
     */
    public void setFlightTime(Integer flightTime) {
        this.flightTime = flightTime;
    }

    /**
     * @return the speed
     */
    public Float getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    /**
     * @return the startX
     */
    public Integer getStartX() {
        return startX;
    }

    /**
     * @param startX the startX to set
     */
    public void setStartX(Integer startX) {
        this.startX = startX;
    }

    /**
     * @return the startY
     */
    public Integer getStartY() {
        return startY;
    }

    /**
     * @param startY the startY to set
     */
    public void setStartY(Integer startY) {
        this.startY = startY;
    }

    /**
     * @return the destX
     */
    public Integer getDestX() {
        return destX;
    }

    /**
     * @param destX the destX to set
     */
    public void setDestX(Integer destX) {
        this.destX = destX;
    }

    /**
     * @return the destY
     */
    public Integer getDestY() {
        return destY;
    }

    /**
     * @param destY the destY to set
     */
    public void setDestY(Integer destY) {
        this.destY = destY;
    }
}
