/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.StringType;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EAllianceBoardType;
import at.viswars.enumeration.ETerritoryMapType;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "territorymappoint")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TerritoryMapPoint extends Model<TerritoryMapPoint> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    private Integer id;
    @IndexAnnotation(indexName = "territoryId")
    @FieldMappingAnnotation("territoryId")
    @IdFieldAnnotation
    private Integer territoryId;
    @FieldMappingAnnotation("x")
    private Integer x;
    @FieldMappingAnnotation("y")
    private Integer y;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the territoryId
     */
    public Integer getTerritoryId() {
        return territoryId;
    }

    /**
     * @param territoryId the territoryId to set
     */
    public void setTerritoryId(Integer territoryId) {
        this.territoryId = territoryId;
    }

    /**
     * @return the x
     */
    public Integer getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(Integer x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public Integer getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(Integer y) {
        this.y = y;
    }
}
