/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "planetloyality")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlanetLoyality extends Model<PlanetLoyality> implements DataChangeAware {

    @FieldMappingAnnotation("planetId")
    @IdFieldAnnotation
    private Integer planetId;
    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("value")
    private Double value;
    private boolean modified = false;

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the value
     */
    public Double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Double value) {
        if ((this.value != null) && !this.value.equals(value)) {
            modified = true;
        }
        this.value = value;
    }

    /**
     * @return the modified
     */
    public boolean isModified() {
        return modified;
    }
}
