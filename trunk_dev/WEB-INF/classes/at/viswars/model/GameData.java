/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EGameDataStatus;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "gamedata")
public class GameData extends Model<GameData>{
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true, autoIncrement=true)
    private Integer id;
    @FieldMappingAnnotation(value = "prago")
    private Integer prago;
    @FieldMappingAnnotation(value = "daArk")
    private Integer daArk;
    @FieldMappingAnnotation(value = "startTime")
    private Long startTime;
    @FieldMappingAnnotation(value = "tickTime")
    private Integer tickTime;
    @FieldMappingAnnotation(value = "lastUpdatedTick")
    private Integer lastUpdatedTick;
    @FieldMappingAnnotation(value = "status")
    private EGameDataStatus status;
    @FieldMappingAnnotation(value = "version")
    private String version;
    @FieldMappingAnnotation(value = "width")
    private Integer width;
    @FieldMappingAnnotation(value = "height")
    private Integer height;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the prago
     */
    public Integer getPrago() {
        return prago;
    }

    /**
     * @param prago the prago to set
     */
    public void setPrago(Integer prago) {
        this.prago = prago;
    }

    /**
     * @return the daArk
     */
    public Integer getDaArk() {
        return daArk;
    }

    /**
     * @param daArk the daArk to set
     */
    public void setDaArk(Integer daArk) {
        this.daArk = daArk;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the tickTime
     */
    public Integer getTickTime() {
        return tickTime;
    }

    /**
     * @param tickTime the tickTime to set
     */
    public void setTickTime(Integer tickTime) {
        this.tickTime = tickTime;
    }

    /**
     * @return the lastUpdatedTick
     */
    public Integer getLastUpdatedTick() {
        return lastUpdatedTick;
    }

    /**
     * @param lastUpdatedTick the lastUpdatedTick to set
     */
    public void setLastUpdatedTick(Integer lastUpdatedTick) {
        this.lastUpdatedTick = lastUpdatedTick;
    }

    /**
     * @return the status
     */
    public EGameDataStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EGameDataStatus status) {
        this.status = status;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }


}
