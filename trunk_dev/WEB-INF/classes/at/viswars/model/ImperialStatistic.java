/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EImperialStatisticType;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "imperialstatistic")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ImperialStatistic extends Model<ImperialStatistic> {
    /*
     public static final int TYPE_STATISTIC = 0;
     public static final int TYPE_TROOPS = 1;
     public static final int TYPE_SHIPS = 2;
     public static final int TYPE_DEFENCE = 3;
     public static final int TYPE_RESSOURCE = 4;
     public static final int TYPE_RANK = 5;
     */

    @IdFieldAnnotation
    @FieldMappingAnnotation("userid")
    Integer userId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("allyid")
    Integer allyId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("typeid")
    EImperialStatisticType typeId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("sourceid")
    Integer sourceId;
    @FieldMappingAnnotation("value")
    Long value;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the allyId
     */
    public Integer getAllyId() {
        return allyId;
    }

    /**
     * @param allyId the allyId to set
     */
    public void setAllyId(Integer allyId) {
        this.allyId = allyId;
    }

    /**
     * @return the typeId
     */
    public EImperialStatisticType getTypeId() {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(EImperialStatisticType typeId) {
        this.typeId = typeId;
    }

    /**
     * @return the sourceId
     */
    public Integer getSourceId() {
        return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * @return the value
     */
    public Long getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Long value) {
        this.value = value;
    }
}
