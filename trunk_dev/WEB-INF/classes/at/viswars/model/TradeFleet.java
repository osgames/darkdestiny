/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "tradefleets")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TradeFleet extends Model<TradeFleet> {

    @FieldMappingAnnotation("tradeId")
    @IdFieldAnnotation
    private Integer tradeId;
    @FieldMappingAnnotation("designId")
    @IdFieldAnnotation
    private Integer designId;
    @IndexAnnotation(indexName = "tradePostId")
    @FieldMappingAnnotation("tradePostId")
    private Integer tradePostId;
    @FieldMappingAnnotation("count")
    private Integer count;

    /**
     * @return the tradeId
     */
    public Integer getTradeId() {
        return tradeId;
    }

    /**
     * @param tradeId the tradeId to set
     */
    public void setTradeId(Integer tradeId) {
        this.tradeId = tradeId;
    }

    /**
     * @return the designId
     */
    public Integer getDesignId() {
        return designId;
    }

    /**
     * @param designId the designId to set
     */
    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    /**
     * @return the tradePostId
     */
    public Integer getTradePostId() {
        return tradePostId;
    }

    /**
     * @param tradePostId the tradePostId to set
     */
    public void setTradePostId(Integer tradePostId) {
        this.tradePostId = tradePostId;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }
}
