/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "planetcategory")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlanetCategory extends Model<PlanetCategory> {

    @FieldMappingAnnotation("planetId")
    @IdFieldAnnotation
    private Integer planetId;
    @FieldMappingAnnotation("categoryId")
    @IndexAnnotation(indexName = "category")
    private Integer categoryId;

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the categoryId
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
