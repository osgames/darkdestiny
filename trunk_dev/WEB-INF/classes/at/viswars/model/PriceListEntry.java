/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "pricelistentry")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PriceListEntry extends Model<PriceListEntry> {

    @FieldMappingAnnotation("id")
    @ColumnProperties(autoIncrement = true)
    @IdFieldAnnotation
    private Integer id;
    @FieldMappingAnnotation("priceListId")
    @IndexAnnotation(indexName = "priceRessourceIdx")
    private Integer priceListId;
    @FieldMappingAnnotation("ressId")
    @IndexAnnotation(indexName = "priceRessourceIdx")
    private Integer ressId;
    @FieldMappingAnnotation("price")
    @DefaultValue("0")
    private Integer price;
    @FieldMappingAnnotation("sold")
    @DefaultValue("0")
    private Long sold;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the priceListId
     */
    public Integer getPriceListId() {
        return priceListId;
    }

    /**
     * @param priceListId the priceListId to set
     */
    public void setPriceListId(Integer priceListId) {
        this.priceListId = priceListId;
    }

    /**
     * @return the ressId
     */
    public Integer getRessId() {
        return ressId;
    }

    /**
     * @param ressId the ressId to set
     */
    public void setRessId(Integer ressId) {
        this.ressId = ressId;
    }

    /**
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * @return the sold
     */
    public Long getSold() {
        return sold;
    }

    /**
     * @param sold the sold to set
     */
    public void setSold(Long sold) {
        this.sold = sold;
    }
}
