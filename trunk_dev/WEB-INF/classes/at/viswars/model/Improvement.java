/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "improvement")
public class Improvement extends Model<Improvement> implements ITechFunctions{

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true, autoIncrement=true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("description")
    private String description;

    public int getTechId() {
        return id;
    }

    public String getTechName() {
        return name;
    }

    public String getTechDescription() {
        return description;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
