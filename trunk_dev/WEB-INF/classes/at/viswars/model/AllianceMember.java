/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "alliancemembers")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class AllianceMember extends Model<AllianceMember> {

    @FieldMappingAnnotation("allianceId")
    @IdFieldAnnotation
    private Integer allianceId;
    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("joinedOn")
    private Long joinedOn;
    @FieldMappingAnnotation("isTrial")
    private Boolean isTrial;
    @FieldMappingAnnotation("isAdmin")
    private Boolean isAdmin;
    @FieldMappingAnnotation("isCouncil")
    private Boolean isCouncil;
    @FieldMappingAnnotation("specialRankId")
    @DefaultValue("0")
    private Integer specialRankId;
    @FieldMappingAnnotation("lastRead")
    @DefaultValue("0")
    private Long lastRead;

    /**
     * @return the allianceId
     */
    public Integer getAllianceId() {
        return allianceId;
    }

    /**
     * @param allianceId the allianceId to set
     */
    public void setAllianceId(Integer allianceId) {
        this.allianceId = allianceId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the isTrial
     */
    public Boolean getIsTrial() {
        return isTrial;
    }

    /**
     * @param isTrial the isTrial to set
     */
    public void setIsTrial(Boolean isTrial) {
        this.isTrial = isTrial;
    }

    /**
     * @return the isAdmin
     */
    public Boolean getIsAdmin() {
        return isAdmin;
    }

    /**
     * @param isAdmin the isAdmin to set
     */
    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * @return the isCouncil
     */
    public Boolean getIsCouncil() {
        return isCouncil;
    }

    /**
     * @param isCouncil the isCouncil to set
     */
    public void setIsCouncil(Boolean isCouncil) {
        this.isCouncil = isCouncil;
    }

    /**
     * @return the specialRankId
     */
    public Integer getSpecialRankId() {
        return specialRankId;
    }

    /**
     * @param specialRankId the specialRankId to set
     */
    public void setSpecialRankId(Integer specialRankId) {
        this.specialRankId = specialRankId;
    }

    /**
     * @return the lastRead
     */
    public Long getLastRead() {
        return lastRead;
    }

    /**
     * @param lastRead the lastRead to set
     */
    public void setLastRead(Long lastRead) {
        this.lastRead = lastRead;
    }

    /**
     * @return the joinedOn
     */
    public long getJoinedOn() {
        return joinedOn;
    }

    /**
     * @param joinedOn the joinedOn to set
     */
    public void setJoinedOn(long joinedOn) {
        this.joinedOn = joinedOn;
    }
}
