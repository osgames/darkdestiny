/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "voteoptions")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class VoteOption extends Model<VoteOption> {

    public final static int OPTION_TYPE_ACCEPT = 1;
    public final static int OPTION_TYPE_DENY = 2;
    public final static int OPTION_TYPE_VALUE = 3;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @IndexAnnotation(indexName = "voteId")
    @FieldMappingAnnotation("voteId")
    private Integer voteId;
    @FieldMappingAnnotation("voteOption")
    private String voteOption;
    @FieldMappingAnnotation("ML")
    private Boolean constant;
    @FieldMappingAnnotation("preSelect")
    private Boolean preSelect;
    @FieldMappingAnnotation("type")
    private Integer type;

    public VoteOption() {
    }

    public VoteOption(String text, boolean preSelect, int type) {
        this.voteOption = text;
        this.preSelect = preSelect;
        this.type = type;
        this.constant = false;
    }

    public VoteOption(String text, boolean preSelect, int type, boolean constant) {
        this.voteOption = text;
        this.preSelect = preSelect;
        this.type = type;
        this.constant = constant;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the voteId
     */
    public Integer getVoteId() {
        return voteId;
    }

    /**
     * @param voteId the voteId to set
     */
    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    /**
     * @return the voteOption
     */
    public String getVoteOption() {
        return voteOption;
    }

    /**
     * @param voteOption the voteOption to set
     */
    public void setVoteOption(String voteOption) {
        this.voteOption = voteOption;
    }

    /**
     * @return the preSelect
     */
    public Boolean getPreSelect() {
        return preSelect;
    }

    /**
     * @param preSelect the preSelect to set
     */
    public void setPreSelect(Boolean preSelect) {
        this.preSelect = preSelect;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the constant
     */
    public Boolean isConstant() {
        return constant;
    }

    /**
     * @param constant the constant to set
     */
    public void setConstant(Boolean constant) {
        this.constant = constant;
    }
}
