/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "weaponfactor")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class WeaponFactor extends Model<WeaponFactor> {

    @FieldMappingAnnotation(value = "weaponId")
    @IdFieldAnnotation
    private Integer weaponId;
    @FieldMappingAnnotation(value = "steelarmor")
    private Double steelArmor;
    @FieldMappingAnnotation(value = "terkonitarmor")
    private Double terkonitArmor;
    @FieldMappingAnnotation(value = "ynkenitarmor")
    private Double YnkenitArmor;
    @FieldMappingAnnotation(value = "soloniumarmor")
    private Double SoloniumArmor;
    @FieldMappingAnnotation(value = "prallschirm_penetration")
    private Double prallschirmPenetration;
    @FieldMappingAnnotation(value = "huschirm_penetration")
    private Double huschirmPenetration;
    @FieldMappingAnnotation(value = "paratron_penetration")
    private Double paratronPenetration;
    @FieldMappingAnnotation(value = "small_penalty")
    private Double smallPenalty;
    @FieldMappingAnnotation(value = "atmosphere_penalty")
    private Double atmospherePenalty;
    @FieldMappingAnnotation(value = "multifire")
    private Integer multifire;
    @FieldMappingAnnotation(value = "weaponrange")
    private Integer range;

    public Integer getWeaponId() {
        return weaponId;
    }

    public void setWeaponId(Integer weaponId) {
        this.weaponId = weaponId;
    }

    public Double getSteelArmor() {
        return steelArmor;
    }

    public void setSteelArmor(Double steelArmor) {
        this.steelArmor = steelArmor;
    }

    public Double getTerkonitArmor() {
        return terkonitArmor;
    }

    public void setTerkonitArmor(Double terkonitArmor) {
        this.terkonitArmor = terkonitArmor;
    }

    public Double getYnkenitArmor() {
        return YnkenitArmor;
    }

    public void setYnkenitArmor(Double YnkenitArmor) {
        this.YnkenitArmor = YnkenitArmor;
    }

    public Double getSoloniumArmor() {
        return SoloniumArmor;
    }

    public void setSoloniumArmor(Double SoloniumArmor) {
        this.SoloniumArmor = SoloniumArmor;
    }

    public Double getPrallschirmPenetration() {
        return prallschirmPenetration;
    }

    public void setPrallschirmPenetration(Double prallschirmPenetration) {
        this.prallschirmPenetration = prallschirmPenetration;
    }

    public Double getHuschirmPenetration() {
        return huschirmPenetration;
    }

    public void setHuschirmPenetration(Double huschirmPenetration) {
        this.huschirmPenetration = huschirmPenetration;
    }

    public Double getParatronPenetration() {
        return paratronPenetration;
    }

    public void setParatronPenetration(Double paratronPenetration) {
        this.paratronPenetration = paratronPenetration;
    }

    public Double getSmallPenalty() {
        return smallPenalty;
    }

    public void setSmallPenalty(Double smallPenalty) {
        this.smallPenalty = smallPenalty;
    }

    public Double getAtmospherePenalty() {
        return atmospherePenalty;
    }

    public void setAtmospherePenalty(Double atmospherePenalty) {
        this.atmospherePenalty = atmospherePenalty;
    }

    public Integer getMultifire() {
        return multifire;
    }

    public void setMultifire(Integer multifire) {
        this.multifire = multifire;
    }

    public Integer getRange() {
        return range;
    }

    public void setRange(Integer range) {
        this.range = range;
    }
}
