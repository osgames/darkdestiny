/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.ERessourceCostType;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "ressourcecost")
@DataScope(type = DataScope.EScopeType.MIXED)
public class RessourceCost extends Model {
    /*
     public final static int COST_CONSTRUCTION = 0;
     public final static int COST_MODULE = 1;
     public final static int COST_GROUPTROOPS = 2;
     public final static int COST_SHIP = 3;
     */

    @FieldMappingAnnotation("type")
    @IdFieldAnnotation
    private ERessourceCostType type;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    private Integer id;
    @FieldMappingAnnotation("ressId")
    @IdFieldAnnotation
    private Integer ressId;
    @FieldMappingAnnotation("qty")
    private Long qty;

    /**
     * @return the type
     */
    public ERessourceCostType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ERessourceCostType type) {
        this.type = type;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the ressId
     */
    public Integer getRessId() {
        return ressId;
    }

    /**
     * @param ressId the ressId to set
     */
    public void setRessId(Integer ressId) {
        this.ressId = ressId;
    }

    /**
     * @return the qty
     */
    public Long getQty() {
        return qty;
    }

    /**
     * @param qty the qty to set
     */
    public void setQty(Long qty) {
        this.qty = qty;
    }
}
