/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EShipType;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "shipdesigns")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ShipDesign extends Model<ShipDesign> {

    @FieldMappingAnnotation(value = "id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation(value = "chassis")
    private Integer chassis;
    @FieldMappingAnnotation(value = "userId")
    @IndexAnnotation(indexName = "userId")
    private Integer userId;
    @FieldMappingAnnotation(value = "name")
    private String name;
    @FieldMappingAnnotation(value = "designTime")
    private Long designTime;
    @FieldMappingAnnotation(value = "primaryTarget")
    @DefaultValue("0")
    private Integer primaryTarget;
    @FieldMappingAnnotation(value = "type")
    private EShipType type;
    @FieldMappingAnnotation(value = "crew")
    @DefaultValue("0")
    private Integer crew;
    @FieldMappingAnnotation(value = "buildable")
    @DefaultValue("false")
    private Boolean buildable;
    @FieldMappingAnnotation(value = "standardspeed")
    @DefaultValue("0")
    private Float standardSpeed;
    @FieldMappingAnnotation(value = "hyperspeed")
    @DefaultValue("0")
    private Float hyperSpeed;
    @FieldMappingAnnotation(value = "troopSpace")
    @DefaultValue("0")
    private Integer troopSpace;
    @FieldMappingAnnotation(value = "ressSpace")
    @DefaultValue("0")
    private Integer ressSpace;
    @FieldMappingAnnotation(value = "isConstruct")
    @DefaultValue("0")
    private Boolean isConstruct;
    @FieldMappingAnnotation(value = "smallHangar")
    @DefaultValue("0")
    private Integer smallHangar;
    @FieldMappingAnnotation(value = "mediumHangar")
    @DefaultValue("0")
    private Integer mediumHangar;
    @FieldMappingAnnotation(value = "largeHangar")
    @DefaultValue("0")
    private Integer largeHangar;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the chassis
     */
    public Integer getChassis() {
        return chassis;
    }

    /**
     * @param chassis the chassis to set
     */
    public void setChassis(Integer chassis) {
        this.chassis = chassis;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the buildTime
     */
    public Long getDesignTime() {
        return designTime;
    }

    /**
     * @param buildTime the buildTime to set
     */
    public void setDesignTime(Long designTime) {
        this.designTime = designTime;
    }

    /**
     * @return the primaryTarget
     */
    public Integer getPrimaryTarget() {
        return primaryTarget;
    }

    /**
     * @param primaryTarget the primaryTarget to set
     */
    public void setPrimaryTarget(Integer primaryTarget) {
        this.primaryTarget = primaryTarget;
    }

    /**
     * @return the type
     */
    public EShipType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EShipType type) {
        this.type = type;
    }

    /**
     * @return the crew
     */
    public Integer getCrew() {
        return crew;
    }

    /**
     * @param crew the crew to set
     */
    public void setCrew(Integer crew) {
        this.crew = crew;
    }

    /**
     * @return the buildable
     */
    public Boolean getBuildable() {
        return buildable;
    }

    /**
     * @param buildable the buildable to set
     */
    public void setBuildable(Boolean buildable) {
        this.buildable = buildable;
    }

    /**
     * @return the standardSpeed
     */
    public Float getStandardSpeed() {
        return standardSpeed;
    }

    /**
     * @param standardSpeed the standardSpeed to set
     */
    public void setStandardSpeed(Float standardSpeed) {
        this.standardSpeed = standardSpeed;
    }

    /**
     * @return the hyperSpeed
     */
    public Float getHyperSpeed() {
        return hyperSpeed;
    }

    /**
     * @param hyperSpeed the hyperSpeed to set
     */
    public void setHyperSpeed(Float hyperSpeed) {
        this.hyperSpeed = hyperSpeed;
    }

    /**
     * @return the troopSpace
     */
    public Integer getTroopSpace() {
        return troopSpace;
    }

    /**
     * @param troopSpace the troopSpace to set
     */
    public void setTroopSpace(Integer troopSpace) {
        this.troopSpace = troopSpace;
    }

    /**
     * @return the ressSpace
     */
    public Integer getRessSpace() {
        return ressSpace;
    }

    /**
     * @param ressSpace the ressSpace to set
     */
    public void setRessSpace(Integer ressSpace) {
        this.ressSpace = ressSpace;
    }

    /**
     * @return the isConstruct
     */
    public Boolean getIsConstruct() {
        return isConstruct;
    }

    /**
     * @param isConstruct the isConstruct to set
     */
    public void setIsConstruct(Boolean isConstruct) {
        this.isConstruct = isConstruct;
    }

    /**
     * @return the smallHangar
     */
    public Integer getSmallHangar() {
        return smallHangar;
    }

    /**
     * @param smallHangar the smallHangar to set
     */
    public void setSmallHangar(Integer smallHangar) {
        this.smallHangar = smallHangar;
    }

    /**
     * @return the mediumHangar
     */
    public Integer getMediumHangar() {
        return mediumHangar;
    }

    /**
     * @param mediumHangar the mediumHangar to set
     */
    public void setMediumHangar(Integer mediumHangar) {
        this.mediumHangar = mediumHangar;
    }

    /**
     * @return the largeHangar
     */
    public Integer getLargeHangar() {
        return largeHangar;
    }

    /**
     * @param largeHangar the largeHangar to set
     */
    public void setLargeHangar(Integer largeHangar) {
        this.largeHangar = largeHangar;
    }
}
