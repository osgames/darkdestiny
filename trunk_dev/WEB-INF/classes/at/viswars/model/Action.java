/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EActionType;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "action")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Action extends Model<Action> {

    @FieldMappingAnnotation("type")
    @IndexAnnotation(indexName = "buildIndex")
    private EActionType type;
    @IdFieldAnnotation
    @FieldMappingAnnotation("refTableId")
    @ColumnProperties(unsigned = true)
    @DefaultValue("0")
    private Integer refTableId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("userId")
    @ColumnProperties(unsigned = true)
    private Integer userId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("refEntityId")
    @IndexAnnotation(indexName = "buildIndex")
    @ColumnProperties(unsigned = true)
    @DefaultValue("0")
    private Integer refEntityId;
    @FieldMappingAnnotation("number")
    @ColumnProperties(unsigned = true)
    @DefaultValue("0")
    private Integer number;
    @IdFieldAnnotation
    @IndexAnnotation(indexName = "buildIndex")
    @FieldMappingAnnotation("planetId")
    @DefaultValue("0")
    @ColumnProperties(unsigned = true)
    private Integer planetId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("systemId")
    @DefaultValue("0")
    @ColumnProperties(unsigned = true)
    private Integer systemId;
    @FieldMappingAnnotation("onHold")
    @DefaultValue("0")
    private Boolean onHold;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the number
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the systemId
     */
    public Integer getSystemId() {
        return systemId;
    }

    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the onHold
     */
    public Boolean getOnHold() {
        return onHold;
    }

    /**
     * @param onHold the onHold to set
     */
    public void setOnHold(Boolean onHold) {
        this.onHold = onHold;
    }

    /**
     * @return the type
     */
    public EActionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EActionType type) {
        this.type = type;
    }

    /**
     * @return the refTableId
     */
    public Integer getRefTableId() {
        return refTableId;
    }

    /**
     * @return the refTableId
     */
    public Integer getProductionOrderId() {
        return refTableId;
    }

    public Integer getFleetDetailId() {
        return refTableId;
    }

    /**
     * @param refTableId the refTableId to set
     */
    public void setRefTableId(Integer refTableId) {
        this.refTableId = refTableId;
    }

    public void setProductionOrderId(Integer productionOrderId) {
        this.refTableId = productionOrderId;
    }

    /**
     * @return the refEntityId
     */
    public Integer getRefEntityId() {
        return refEntityId;
    }

    public Integer getResearchId() {
        return refEntityId;
    }

    public Integer getConstructionId() {
        return refEntityId;
    }

    public Integer getGroundTroopId() {
        return refEntityId;
    }

    public Integer getShipDesignId() {
        return refEntityId;
    }

    public Integer getFleetId() {
        return refEntityId;
    }

    /**
     * @param refEntityId the refEntityId to set
     */
    public void setRefEntityId(Integer refEntityId) {
        this.refEntityId = refEntityId;
    }

    /**
     * @param refEntityId the refEntityId to set
     */
    public void setConstructionId(Integer constructionId) {
        this.refEntityId = constructionId;
    }

    public void setFleetId(Integer fleetId) {
        this.refEntityId = fleetId;
    }

    public void setShipDesignId(Integer shipDesignId) {
        this.refEntityId = shipDesignId;
    }

    public void setGroundTroopId(Integer groundTroopId) {
        this.refEntityId = groundTroopId;
    }

    /**
     * @param refEntityId the refEntityId to set
     */
    public void setResearchId(Integer researchId) {
        this.refEntityId = researchId;
    }
}
