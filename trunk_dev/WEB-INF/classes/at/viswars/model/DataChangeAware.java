/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.model;

/**
 *
 * @author Stefan
 */
public interface DataChangeAware {
    public boolean isModified();
}
