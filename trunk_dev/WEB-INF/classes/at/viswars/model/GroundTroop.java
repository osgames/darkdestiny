/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "groundtroops")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class GroundTroop extends Model<GroundTroop> implements ITechFunctions {

    public static final int ID_MILIZ = 0;
    public static final int ID_LIGHTINFANTRY = 10;
    public static final int ID_HEAVYINFANTRY = 20;
    public static final int ID_ROBOTER = 40;
    public static final int ID_ROCKETTANK = 45;
    public static final int ID_LIGHTTANK = 50;
    public static final int ID_HEAVYTANK = 60;
    public static final int ID_ANTIGRAVTANK = 70;
    public static final int ID_INTERCEPTORPLANE = 80;
    public static final int ID_BOMBER = 90;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = false)
    private Integer id;
    @FieldMappingAnnotation("hitpoints")
    private Integer hitpoints;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("description")
    private String description;
    @FieldMappingAnnotation("credits")
    private Long credits;
    @FieldMappingAnnotation("crew")
    private Integer crew;
    @FieldMappingAnnotation("size")
    private Integer size;
    @FieldMappingAnnotation("modulPoints")
    private Integer modulPoints;
    @FieldMappingAnnotation("image")
    private String image;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the credits
     */
    public Long getCredits() {
        return credits;
    }

    /**
     * @param credits the credits to set
     */
    public void setCredits(Long credits) {
        this.credits = credits;
    }

    /**
     * @return the hitpoints
     */
    public Integer getHitpoints() {
        return hitpoints;
    }

    /**
     * @param hitpoints the hitpoints to set
     */
    public void setHitpoints(Integer hitpoints) {
        this.hitpoints = hitpoints;
    }

    public int getTechId() {
        return id;
    }

    public String getTechName() {
        return name;
    }

    public String getTechDescription() {
        return "";
    }

    public Integer getCrew() {
        return crew;
    }

    public void setCrew(Integer crew) {
        this.crew = crew;
    }

    public Integer getModulPoints() {
        return modulPoints;
    }

    public void setModulPoints(Integer modulPoints) {
        this.modulPoints = modulPoints;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
