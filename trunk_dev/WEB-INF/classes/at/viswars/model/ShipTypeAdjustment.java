/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EShipType;

/**
 *
 * @author Dreloc
 */
@TableNameAnnotation("shiptypeadjustment")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class ShipTypeAdjustment extends Model<ShipTypeAdjustment> {

    @FieldMappingAnnotation("type")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("type_E")
    private EShipType enumId;
    @FieldMappingAnnotation("attackFactor")
    private Float attackFactor;
    @FieldMappingAnnotation("structureFactor")
    private Float structureFactor;
    @FieldMappingAnnotation("shieldFactor")
    private Float shieldFactor;
    @FieldMappingAnnotation("ressFactor")
    private Float ressFactor;
    @FieldMappingAnnotation("troopFactor")
    private Float troopFactor;
    @FieldMappingAnnotation("hangarFactor")
    private Float hangarFactor;
    @FieldMappingAnnotation("costFactor")
    private Float costFactor;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the enumId
     */
    public EShipType getEnumId() {
        return enumId;
    }

    /**
     * @param enumId the enumId to set
     */
    public void setEnumId(EShipType enumId) {
        this.enumId = enumId;
    }

    /**
     * @return the attackFactor
     */
    public Float getAttackFactor() {
        return attackFactor;
    }

    /**
     * @param attackFactor the attackFactor to set
     */
    public void setAttackFactor(Float attackFactor) {
        this.attackFactor = attackFactor;
    }

    /**
     * @return the structureFactor
     */
    public Float getStructureFactor() {
        return structureFactor;
    }

    /**
     * @param structureFactor the structureFactor to set
     */
    public void setStructureFactor(Float structureFactor) {
        this.structureFactor = structureFactor;
    }

    /**
     * @return the shieldFactor
     */
    public Float getShieldFactor() {
        return shieldFactor;
    }

    /**
     * @param shieldFactor the shieldFactor to set
     */
    public void setShieldFactor(Float shieldFactor) {
        this.shieldFactor = shieldFactor;
    }

    /**
     * @return the ressFactor
     */
    public Float getRessFactor() {
        return ressFactor;
    }

    /**
     * @param ressFactor the ressFactor to set
     */
    public void setRessFactor(Float ressFactor) {
        this.ressFactor = ressFactor;
    }

    /**
     * @return the troopFactor
     */
    public Float getTroopFactor() {
        return troopFactor;
    }

    /**
     * @param troopFactor the troopFactor to set
     */
    public void setTroopFactor(Float troopFactor) {
        this.troopFactor = troopFactor;
    }

    /**
     * @return the hangarFactor
     */
    public Float getHangarFactor() {
        return hangarFactor;
    }

    /**
     * @param hangarFactor the hangarFactor to set
     */
    public void setHangarFactor(Float hangarFactor) {
        this.hangarFactor = hangarFactor;
    }

    /**
     * @return the costFactor
     */
    public Float getCostFactor() {
        return costFactor;
    }

    /**
     * @param costFactor the costFactor to set
     */
    public void setCostFactor(Float costFactor) {
        this.costFactor = costFactor;
    }
}
