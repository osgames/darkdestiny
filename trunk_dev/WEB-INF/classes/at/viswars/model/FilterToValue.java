/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EFilterComparator;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "filtertovalue")
@DataScope(type = DataScope.EScopeType.MIXED)
public class FilterToValue extends Model<FilterToValue> {

    @FieldMappingAnnotation("filterId")
    @IdFieldAnnotation
    private Integer filterId;
    @FieldMappingAnnotation("valueId")
    @IdFieldAnnotation
    private Integer valueId;
    @FieldMappingAnnotation("comparator")
    private EFilterComparator comparator;
    @FieldMappingAnnotation("value")
    private String value;

    /**
     * @return the filterId
     */
    public Integer getFilterId() {
        return filterId;
    }

    /**
     * @param filterId the filterId to set
     */
    public void setFilterId(Integer filterId) {
        this.filterId = filterId;
    }

    /**
     * @return the valueId
     */
    public Integer getValueId() {
        return valueId;
    }

    /**
     * @param valueId the valueId to set
     */
    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

    /**
     * @return the comparator
     */
    public EFilterComparator getComparator() {
        return comparator;
    }

    public String getComparatorString() {
        if (comparator == EFilterComparator.BIGGER) {
            return ">";
        } else if (comparator == EFilterComparator.BIGGEREQUALS) {
            return "&ge;";
        } else if (comparator == EFilterComparator.EQUALS) {
            return "=";
        } else if (comparator == EFilterComparator.SMALLEREQUALS) {
            return "&le;";
        } else if (comparator == EFilterComparator.SMALLER) {

            return "<";
        }
        return "Comparator not found";
    }

    /**
     * @param comparator the comparator to set
     */
    public void setComparator(EFilterComparator comparator) {
        this.comparator = comparator;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
