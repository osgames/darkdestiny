/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "techtype")
public class TechType  extends Model<TechType>{
    @FieldMappingAnnotation(value = "id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true)
    private Integer id;
    @FieldMappingAnnotation(value = "name")
    private String name;
    @FieldMappingAnnotation(value = "refDAO")
    private String refDAO;
    @FieldMappingAnnotation(value = "refModel")
    private String refModel;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

   

    /**
     * @return the refDAO
     */
    public String getRefDAO() {
        return refDAO;
    }

    /**
     * @param refDAO the refDAO to set
     */
    public void setRefDAO(String refDAO) {
        this.refDAO = refDAO;
    }

    /**
     * @return the refModel
     */
    public String getRefModel() {
        return refModel;
    }

    /**
     * @param refModel the refModel to set
     */
    public void setRefModel(String refModel) {
        this.refModel = refModel;
    }
}
