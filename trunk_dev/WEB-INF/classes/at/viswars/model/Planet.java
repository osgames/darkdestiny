/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.dao.ConstructionDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetConstructionDAO;
import at.viswars.dao.PlanetDefenseDAO;
import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.EDefenseType;
import at.viswars.interfaces.IStellarBody;
import at.viswars.result.SurfaceResult;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "planet")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Planet extends Model<Planet> implements IStellarBody {

    // PLANET TYPES
    public static final String LANDTYPE_A = "A";
    public static final String LANDTYPE_B = "B";
    public static final String LANDTYPE_C = "C";
    public static final String LANDTYPE_E = "E";
    public static final String LANDTYPE_G = "G";
    public static final String LANDTYPE_M = "M";
    public static final String LANDTYPE_L = "L";
    public static final String LANDTYPE_I = "I";
    public static final String LANDTYPE_J = "J";
    public static int MAX_ORBIT_LEVELS = 12;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer id;
    @IndexAnnotation(indexName = "systemId")
    @FieldMappingAnnotation("systemId")
    private Integer systemId;
    @FieldMappingAnnotation("landType")
    private String landType;
    @FieldMappingAnnotation("atmosphereType")
    private String atmosphereType;
    @FieldMappingAnnotation("avgTemp")
    private Integer avgTemp;
    @FieldMappingAnnotation("diameter")
    private Integer diameter;
    @FieldMappingAnnotation("orbitLevel")
    private Integer orbitLevel;
    @FieldMappingAnnotation("growthBonus")
    private Float growthBonus;
    @FieldMappingAnnotation("researchBonus")
    private Float researchBonus;
    @FieldMappingAnnotation("productionBonus")
    private Float productionBonus;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the systemId
     */
    public Integer getSystemId() {
        return systemId;
    }

    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the landType
     */
    public String getLandType() {
        return landType;
    }

    /**
     * @param landType the landType to set
     */
    public void setLandType(String landType) {
        this.landType = landType;
    }

    /**
     * @return the atmosphereType
     */
    public String getAtmosphereType() {
        return atmosphereType;
    }

    /**
     * @param atmosphereType the atmosphereType to set
     */
    public void setAtmosphereType(String atmosphereType) {
        this.atmosphereType = atmosphereType;
    }

    /**
     * @return the avgTemp
     */
    public Integer getAvgTemp() {
        return avgTemp;
    }

    /**
     * @param avgTemp the avgTemp to set
     */
    public void setAvgTemp(Integer avgTemp) {
        this.avgTemp = avgTemp;
    }

    /**
     * @return the diameter
     */
    public Integer getDiameter() {
        return diameter;
    }

    /**
     * @param diameter the diameter to set
     */
    public void setDiameter(Integer diameter) {
        this.diameter = diameter;
    }

    /**
     * @return the orbitLevel
     */
    public Integer getOrbitLevel() {
        return orbitLevel;
    }

    /**
     * @param orbitLevel the orbitLevel to set
     */
    public void setOrbitLevel(Integer orbitLevel) {
        this.orbitLevel = orbitLevel;
    }

    /**
     * @return the growthBonus
     */
    public Float getGrowthBonus() {
        return growthBonus;
    }

    /**
     * @param growthBonus the growthBonus to set
     */
    public void setGrowthBonus(Float growthBonus) {
        this.growthBonus = growthBonus;
    }

    /**
     * @return the researchBonus
     */
    public Float getResearchBonus() {
        return researchBonus;
    }

    /**
     * @param researchBonus the researchBonus to set
     */
    public void setResearchBonus(Float researchBonus) {
        this.researchBonus = researchBonus;
    }

    /**
     * @return the productionBonus
     */
    public Float getProductionBonus() {
        return productionBonus;
    }

    /**
     * @param productionBonus the productionBonus to set
     */
    public void setProductionBonus(Float productionBonus) {
        this.productionBonus = productionBonus;
    }

    public int getSurfaceSquares() {
        return (int) ((getSurface().getSurface() / 1000000) * 20);
    }

    public boolean isLebensfeindlich() {
        return LANDTYPE_A.equalsIgnoreCase(landType)
                || LANDTYPE_B.equalsIgnoreCase(landType)
                || LANDTYPE_J.equalsIgnoreCase(landType)
                || LANDTYPE_E.equalsIgnoreCase(landType);
    }

    public int getPlanetarySurface() {
        return (int) ((4 * Math.PI * Math.pow((diameter / 2), 2)) / 3);
    }

    public SurfaceResult getSurface() {
        int surface;
        int freeSurface;

        if (landType.equalsIgnoreCase(LANDTYPE_A)
                || landType.equalsIgnoreCase(LANDTYPE_B)) {
            return new SurfaceResult(0, 0);
        }

        surface = (int) ((4 * Math.PI * Math.pow((diameter / 2), 2)) / 3);
        freeSurface = (int) ((surface / 1000000) * 20);

        PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
        PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
        ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
        ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(id);
        ArrayList<PlanetDefense> pdList = pdDAO.findByPlanet(id, EDefenseType.TURRET);

        for (PlanetConstruction pc : pcList) {
            Construction c = cDAO.findById(pc.getConstructionId());
            freeSurface = freeSurface - (c.getSurface() * pc.getNumber());
        }

        for (PlanetDefense pd : pdList) {
            Construction c = cDAO.findById(pd.getUnitId());
            freeSurface = freeSurface - (c.getSurface() * pd.getCount());
        }

        return new SurfaceResult(surface, freeSurface);
    }

    public String getPlanetPic() {
        String picURL = "empty.gif";
        // ResultSet rs = stmt1.executeQuery("select * from planet where id="+planetID);

        if (landType.equalsIgnoreCase(LANDTYPE_A)) {
            picURL = "A/nb1k.jpg";
        } else if (landType.equalsIgnoreCase(LANDTYPE_B)) {
            picURL = "B/nb1k.jpg";
        } else if (landType.equalsIgnoreCase(LANDTYPE_C)) {
            if (diameter < 10500) {
                picURL = "C/ns1k.jpg";
            } else if (diameter > 16000) {
                picURL = "C/nb1k.jpg";
            } else {
                picURL = "C/nm1k.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_E)) {
            if (diameter < 10500) {
                picURL = "E/ns1k.jpg";
            } else if (diameter > 16000) {
                picURL = "E/nb1k.jpg";
            } else {
                picURL = "E/nm1k.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_G)) {
            if (diameter < 10500) {
                picURL = "G/ns1k.jpg";
            } else if (diameter > 16000) {
                picURL = "G/nb1k.jpg";
            } else {
                picURL = "G/nm1k.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_I)) {
            picURL = "planetI.jpg";
        } else if (landType.equalsIgnoreCase(LANDTYPE_J)) {
            if (diameter < 10500) {
                picURL = "J/ns1k.jpg";
            } else if (diameter > 16000) {
                picURL = "J/nb1k.jpg";
            } else {
                picURL = "J/nm1k.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_M)) {
            if (diameter < 10500) {
                picURL = "M/ns1k.jpg";
            } else if (diameter > 16000) {
                picURL = "M/nb1k.jpg";
            } else {
                picURL = "M/nm1k.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_L)) {
            if (diameter < 10500) {
                picURL = "L/ns1k.jpg";
            } else if (diameter > 16000) {
                picURL = "L/nb1k.jpg";
            } else {
                picURL = "L/nm1k.jpg";
            }
        }
        return picURL;
    }

    public String getLargePic() {
        String picURL = "empty.gif";

        if (landType.equalsIgnoreCase(LANDTYPE_A)) {
            picURL = "A/nb1.jpg";
        } else if (landType.equalsIgnoreCase(LANDTYPE_B)) {
            picURL = "B/nb1.jpg";
        } else if (landType.equalsIgnoreCase(LANDTYPE_C)) {
            if (diameter < 10500) {
                picURL = "C/ns1.jpg";
            } else if (diameter > 16000) {
                picURL = "C/nb1.jpg";
            } else {
                picURL = "C/nm1.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_E)) {
            if (diameter < 10500) {
                picURL = "E/ns1.jpg";
            } else if (diameter > 16000) {
                picURL = "E/nb1.jpg";
            } else {
                picURL = "E/nm1.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_G)) {
            if (diameter < 10500) {
                picURL = "G/ns1.jpg";
            } else if (diameter > 16000) {
                picURL = "G/nb1.jpg";
            } else {
                picURL = "G/nm1.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_I)) {
            picURL = "planetI.jpg";
        } else if (landType.equalsIgnoreCase(LANDTYPE_J)) {
            if (diameter < 10500) {
                picURL = "J/ns1.jpg";
            } else if (diameter > 16000) {
                picURL = "J/nb1.jpg";
            } else {
                picURL = "J/nm1.jpg";
            }
        } else if (landType.equalsIgnoreCase(LANDTYPE_M)) {
            int nPics = 30;
            int cPics = 1;
            int hPics = 1;
            picURL = "";
            picURL += "M/";
            if (avgTemp >= 12 && avgTemp <= 18) {
                picURL += "n_";
            } else if (avgTemp > 18) {
                picURL += "h_";
            } else if (avgTemp < 12) {
                picURL += "c_";
            }
            if (diameter < 10500) {
                picURL += "s_";
            } else if (diameter > 16000) {
                picURL += "b_";
            } else {
                picURL += "m_";
            }

            if (avgTemp >= 12 && avgTemp <= 18) {
                picURL += ((diameter + id) % nPics) + 1;
            } else if (avgTemp > 18) {
                picURL += ((diameter + id) % hPics) + 1;
            } else if (avgTemp < 12) {
                picURL += ((diameter + id) % cPics) + 1;
            }


            picURL += ".png";
        } else if (landType.equalsIgnoreCase(LANDTYPE_L)) {
            if (diameter < 10500) {
                picURL = "L/ns1.jpg";
            } else if (diameter > 16000) {
                picURL = "L/nb1.jpg";
            } else {
                picURL = "L/nm1.jpg";
            }
        }

        return picURL;
    }

    public long getMaxPopulation() {
        long maxPopulation = 0l;

        if (landType.equalsIgnoreCase("A") || landType.equalsIgnoreCase("B")) {
            maxPopulation = 0;
            return 0l;
        }

        int modifier = 100;
        int tmpDiff = 0;

        if (avgTemp < 15) {
            tmpDiff = 15 - avgTemp;
        } else if (avgTemp > 15) {
            tmpDiff = avgTemp - 15;
        }

        double malus = 0; // max = 100;

        if ((tmpDiff > 12) && (tmpDiff <= 25)) {
            malus = Math.pow(12.5d, 2.7) / 20d;
            malus += 50d - Math.pow(25 - tmpDiff, 2.7d) / 20d;
        } else if (tmpDiff <= 12) {
            malus = Math.pow(tmpDiff, 2.7) / 20d;
        } else if (tmpDiff > 25) {
            malus = 100;
        }

        modifier = 100 - (int) malus;

        if (modifier < 0) {
            modifier = 0;
        }
        if (isLebensfeindlich()) {
            modifier = 0;
        }

        // SurfaceResult sf = getSurface();
        maxPopulation = (long) (((float) getPlanetarySurface() * (float) 45) / (float) 100 * modifier);
        if (landType.equalsIgnoreCase("C")) {
            maxPopulation = (long) (maxPopulation / 2);
        } else if (landType.equalsIgnoreCase("G")) {
            maxPopulation = (long) ((float) maxPopulation / 3.5f);
        }

        return maxPopulation;
    }

    @Override
    public RelativeCoordinate getRelativeCoordinate() {
        return new RelativeCoordinate(0, id);
    }

    @Override
    public AbsoluteCoordinate getAbsoluteCoordinate() {
        return getRelativeCoordinate().toAbsoluteCoordinate();
    }
}
