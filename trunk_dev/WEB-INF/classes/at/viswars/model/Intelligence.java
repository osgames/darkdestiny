/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EIntelRefEntity;
import at.viswars.enumeration.EIntelType;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "intelligence")
public class Intelligence extends Model<Intelligence> {
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true, autoIncrement=true)
    private Integer id;
    @FieldMappingAnnotation("userId")
    @ColumnProperties(unsigned=true, nullable=false)
    private Integer userId;
    @FieldMappingAnnotation("refDocumentId")
    @ColumnProperties(unsigned=true)
    private Integer refDocumentId;
    @FieldMappingAnnotation("refEntity")
    @ColumnProperties(nullable=false)
    private EIntelRefEntity refEntity;
    @FieldMappingAnnotation("refEntityId")
    @ColumnProperties(unsigned=true, nullable=false)
    private Integer refEntityId;
    @FieldMappingAnnotation("type")
    @ColumnProperties(nullable=false)
    private EIntelType type;
    @FieldMappingAnnotation("refId")
    @ColumnProperties(unsigned=true, nullable=false)
    private Integer refId;
    @FieldMappingAnnotation("value")
    @ColumnProperties(nullable=false)
    private Integer value;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the refDocumentId
     */
    public Integer getRefDocumentId() {
        return refDocumentId;
    }

    /**
     * @param refDocumentId the refDocumentId to set
     */
    public void setRefDocumentId(Integer refDocumentId) {
        this.refDocumentId = refDocumentId;
    }

    /**
     * @return the refEntity
     */
    public EIntelRefEntity getRefEntity() {
        return refEntity;
    }

    /**
     * @param refEntity the refEntity to set
     */
    public void setRefEntity(EIntelRefEntity refEntity) {
        this.refEntity = refEntity;
    }

    /**
     * @return the refEntityId
     */
    public Integer getRefEntityId() {
        return refEntityId;
    }

    /**
     * @param refEntityId the refEntityId to set
     */
    public void setRefEntityId(Integer refEntityId) {
        this.refEntityId = refEntityId;
    }

    /**
     * @return the type
     */
    public EIntelType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EIntelType type) {
        this.type = type;
    }

    /**
     * @return the refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    /**
     * @return the value
     */
    public Integer getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Integer value) {
        this.value = value;
    }
}
