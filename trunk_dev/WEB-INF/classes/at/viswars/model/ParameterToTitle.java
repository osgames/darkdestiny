/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.StringType;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "parametertotitle")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class ParameterToTitle extends Model<ParameterToTitle> {

    @FieldMappingAnnotation("conditionToTitleId")
    @IdFieldAnnotation
    private Integer conditionToTitleId;
    @FieldMappingAnnotation("parameterId")
    @IdFieldAnnotation
    private Integer parameterId;
    @FieldMappingAnnotation("value")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String value;

    /**
     * @return the conditionToTitleId
     */
    public Integer getConditionToTitleId() {
        return conditionToTitleId;
    }

    /**
     * @param conditionToTitleId the conditionToTitleId to set
     */
    public void setConditionToTitleId(Integer conditionToTitleId) {
        this.conditionToTitleId = conditionToTitleId;
    }

    /**
     * @return the parameterId
     */
    public Integer getParameterId() {
        return parameterId;
    }

    /**
     * @param parameterId the parameterId to set
     */
    public void setParameterId(Integer parameterId) {
        this.parameterId = parameterId;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
