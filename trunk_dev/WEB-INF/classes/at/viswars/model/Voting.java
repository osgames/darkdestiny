/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "voting")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Voting extends Model<Voting> {

    public static final int TYPE_MEMBERJOIN = 0;
    public static final int TYPE_ALLIANCEJOIN = 1;
    public static final int TYPE_DEGRADE_ADMIN = 2;
    public static final int TYPE_ADMIN = 3;
    public static final int TYPE_STANDARD = 4;
    public static final int TYPE_INVASION = 5;
    public static final int TYPE_USERRELATION = 6;
    public static final int TYPE_USERALLIANCERELATION = 7;
    public static final int TYPE_ALLIANCEUSERRELATION = 8;
    public static final int TYPE_ALLIANCEUSERRELATION_REQUEST = 18;
    public static final int TYPE_ALLIANCERELATION = 9;
    public static final int TYPE_ALLIANCERELATION_REQUEST = 19;
    // new voting types due to alliance restructuring
    public static final int TYPE_MEMBERPROMOTE = 20;
    public static final int TYPE_MEMBERDEGRADE = 21;
    public static final int TYPE_CHANGELEADERSHIP = 22;
    public static final int TYPE_TRANSFERPLANET = 30;
    public static final int TYPE_VOTEROUNDEND = 100;
    @FieldMappingAnnotation("voteId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer voteId;
    @FieldMappingAnnotation("minVotes")
    private Integer minVotes;
    @FieldMappingAnnotation("totalVotes")
    private Integer totalVotes;
    @FieldMappingAnnotation("expire")
    private Integer expire;
    @FieldMappingAnnotation("type")
    private Integer type;
    @FieldMappingAnnotation("votetext")
    private String votetext;
    @IndexAnnotation(indexName = "votestate")
    @FieldMappingAnnotation("closed")
    @DefaultValue("0")
    private Boolean closed;
    @FieldMappingAnnotation("result")
    @DefaultValue("0")
    private Integer result;

    /**
     * @return the voteId
     */
    public Integer getVoteId() {
        return voteId;
    }

    /**
     * @param voteId the voteId to set
     */
    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    /**
     * @return the minVotes
     */
    public Integer getMinVotes() {
        return minVotes;
    }

    /**
     * @param minVotes the minVotes to set
     */
    public void setMinVotes(Integer minVotes) {
        this.minVotes = minVotes;
    }

    /**
     * @return the totalVotes
     */
    public Integer getTotalVotes() {
        return totalVotes;
    }

    /**
     * @param totalVotes the totalVotes to set
     */
    public void setTotalVotes(Integer totalVotes) {
        this.totalVotes = totalVotes;
    }

    /**
     * @return the expire
     */
    public Integer getExpire() {
        return expire;
    }

    /**
     * @param expire the expire to set
     */
    public void setExpire(Integer expire) {
        this.expire = expire;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the votetext
     */
    public String getVotetext() {
        return votetext;
    }

    /**
     * @param votetext the votetext to set
     */
    public void setVotetext(String votetext) {
        this.votetext = votetext;
    }

    /**
     * @return the closed
     */
    public Boolean getClosed() {
        return closed;
    }

    /**
     * @param closed the closed to set
     */
    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    /**
     * @return the result
     */
    public Integer getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(Integer result) {
        this.result = result;
    }
}
