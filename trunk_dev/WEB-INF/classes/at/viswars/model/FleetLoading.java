/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "fleetloading")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class FleetLoading extends Model<FleetLoading> {

    public static final int LOADTYPE_RESS = 1;
    public static final int LOADTYPE_TROOPS = 2;
    public static final int LOADTYPE_SHIPS = 3;
    public static final int LOADTYPE_POPULATION = 4;
    @FieldMappingAnnotation("fleetId")
    @IdFieldAnnotation
    private Integer fleetId;
    @FieldMappingAnnotation(value = "loadtype")
    @IdFieldAnnotation
    private Integer loadType;
    @FieldMappingAnnotation(value = "id")
    @IdFieldAnnotation
    private Integer id;
    @FieldMappingAnnotation(value = "count")
    private Integer count;

    /**
     * @return the fleetId
     */
    public Integer getFleetId() {
        return fleetId;
    }

    /**
     * @param fleetId the fleetId to set
     */
    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    /**
     * @return the loadType
     */
    public Integer getLoadType() {
        return loadType;
    }

    /**
     * @param loadType the loadType to set
     */
    public void setLoadType(Integer loadType) {
        this.loadType = loadType;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }
}
