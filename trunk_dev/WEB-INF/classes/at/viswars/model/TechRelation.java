/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "techrelation")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class TechRelation extends Model<TechRelation> {

    public static final int TREETYPE_BUILD = 0;
    public static final int TREETYPE_RESEARCH = 1;
    public static final int TREETYPE_MODULE = 2;
    public static final int TREETYPE_GROUNDTROOPS = 3;
    @FieldMappingAnnotation("sourceID")
    @IdFieldAnnotation
    private Integer sourceId;
    @FieldMappingAnnotation("reqConstructionID")
    @IdFieldAnnotation
    private Integer reqConstructionId;
    @FieldMappingAnnotation("reqResearchID")
    @IdFieldAnnotation
    private Integer reqResearchId;
    @FieldMappingAnnotation("treeType")
    @IdFieldAnnotation
    private Integer treeType;

    /**
     * @return the sourceId
     */
    public Integer getSourceId() {
        return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * @return the reqConstructionId
     */
    public Integer getReqConstructionId() {
        return reqConstructionId;
    }

    /**
     * @param reqConstructionId the reqConstructionId to set
     */
    public void setReqConstructionId(Integer reqConstructionId) {
        this.reqConstructionId = reqConstructionId;
    }

    /**
     * @return the reqResearchId
     */
    public Integer getReqResearchId() {
        return reqResearchId;
    }

    /**
     * @param reqResearchId the reqResearchId to set
     */
    public void setReqResearchId(Integer reqResearchId) {
        this.reqResearchId = reqResearchId;
    }

    /**
     * @return the treeType
     */
    public Integer getTreeType() {
        return treeType;
    }

    /**
     * @param treeType the treeType to set
     */
    public void setTreeType(Integer treeType) {
        this.treeType = treeType;
    }
}
