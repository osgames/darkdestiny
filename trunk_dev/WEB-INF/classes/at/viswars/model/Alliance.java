/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.StringType;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.ELeadership;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "alliance")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Alliance extends Model<Alliance> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("userId")
    private Integer userId;
    @IndexAnnotation(indexName = "allianceName")
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("picture")
    @ColumnProperties(length = 400)
    @StringType("text")
    private String picture;
    @FieldMappingAnnotation("description")
    @ColumnProperties(length = 1000)
    @StringType("text")
    private String description;
    @DefaultValue("DEMOCRATIC")
    @FieldMappingAnnotation("leadership")
    private ELeadership leadership;
    @IndexAnnotation(indexName = "allianceName")
    @FieldMappingAnnotation("tag")
    private String tag;
    @FieldMappingAnnotation("isSubAllianceOf")
    @IndexAnnotation(indexName = "isSubAllianceOf")
    private Integer isSubAllianceOf;
    @FieldMappingAnnotation("masterAllianceId")
    @IndexAnnotation(indexName = "masterAllianceId")
    private Integer masterAllianceId;
    @FieldMappingAnnotation("isRecruiting")
    private Boolean isRecruiting;
// <editor-fold defaultstate="expanded" desc="Setter and getter">

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the picture
     */
    public String getPicture() {
        return picture;
    }

    /**
     * @param picture the picture to set
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }

    /**
     * @return the descrioption
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param descrioption the descrioption to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * @return the isSubAllianceOf
     */
    public Integer getIsSubAllianceOf() {
        return isSubAllianceOf;
    }

    /**
     * @param isSubAllianceOf the isSubAllianceOf to set
     */
    public void setIsSubAllianceOf(Integer isSubAllianceOf) {
        this.isSubAllianceOf = isSubAllianceOf;
    }

    /**
     * @return the masterAlliance
     */
    public Integer getMasterAlliance() {
        return masterAllianceId;
    }

    /**
     * @param masterAlliance the masterAlliance to set
     */
    public void setMasterAlliance(Integer masterAllianceId) {
        this.masterAllianceId = masterAllianceId;
    }

    /**
     * @return the isRecruiting
     */
    public Boolean getIsRecruiting() {
        return isRecruiting;
    }

    /**
     * @param isRecruiting the isRecruiting to set
     */
    public void setIsRecruiting(Boolean isRecruiting) {
        this.isRecruiting = isRecruiting;
    }

    /**
     * @return the leadership
     */
    public ELeadership getLeadership() {
        return leadership;
    }

    /**
     * @param leadership the leadership to set
     */
    public void setLeadership(ELeadership leadership) {
        this.leadership = leadership;
    }
    // </editor-fold>
}
