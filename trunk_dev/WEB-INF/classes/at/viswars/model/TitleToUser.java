/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "titletouser")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TitleToUser extends Model<TitleToUser> {

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("titleId")
    @IdFieldAnnotation
    private Integer titleId;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the titleId
     */
    public Integer getTitleId() {
        return titleId;
    }

    /**
     * @param titleId the titleId to set
     */
    public void setTitleId(Integer titleId) {
        this.titleId = titleId;
    }
}
