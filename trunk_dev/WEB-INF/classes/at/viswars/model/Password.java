/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EPassword;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "password")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Password extends Model<Password> {

    @FieldMappingAnnotation("password")
    @IdFieldAnnotation
    private EPassword password;
    @FieldMappingAnnotation("value")
    private String value;

    /**
     * @return the password
     */
    public EPassword getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(EPassword password) {
        this.password = password;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
