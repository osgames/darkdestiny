/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.ETradeOfferType;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "tradelog")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TradeLog extends Model<TradeLog> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("fromUserId")
    private Integer fromUserId;
    @FieldMappingAnnotation("date")
    private Long date;
    @FieldMappingAnnotation("toUserId")
    private Integer toUserId;
    @FieldMappingAnnotation("ressourceId")
    private Integer ressourceId;
    @FieldMappingAnnotation("quantity")
    private Long quantity;
    @FieldMappingAnnotation("price")
    private Long price;
    @FieldMappingAnnotation("total")
    private Long total;
    @FieldMappingAnnotation("fromPlanetId")
    private Integer fromPlanetId;
    @FieldMappingAnnotation("toPlanetId")
    private Integer toPlanetId;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the fromUserId
     */
    public Integer getFromUserId() {
        return fromUserId;
    }

    /**
     * @param fromUserId the fromUserId to set
     */
    public void setFromUserId(Integer fromUserId) {
        this.fromUserId = fromUserId;
    }

    /**
     * @return the toUserId
     */
    public Integer getToUserId() {
        return toUserId;
    }

    /**
     * @param toUserId the toUserId to set
     */
    public void setToUserId(Integer toUserId) {
        this.toUserId = toUserId;
    }

    /**
     * @return the ressourceId
     */
    public Integer getRessourceId() {
        return ressourceId;
    }

    /**
     * @param ressourceId the ressourceId to set
     */
    public void setRessourceId(Integer ressourceId) {
        this.ressourceId = ressourceId;
    }

    /**
     * @return the quantity
     */
    public Long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the price
     */
    public Long getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Long price) {
        this.price = price;
    }

    /**
     * @return the total
     */
    public Long getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Long total) {
        this.total = total;
    }

    /**
     * @return the fromPlanetId
     */
    public Integer getFromPlanetId() {
        return fromPlanetId;
    }

    /**
     * @param fromPlanetId the fromPlanetId to set
     */
    public void setFromPlanetId(Integer fromPlanetId) {
        this.fromPlanetId = fromPlanetId;
    }

    /**
     * @return the toPlanetId
     */
    public Integer getToPlanetId() {
        return toPlanetId;
    }

    /**
     * @param toPlanetId the toPlanetId to set
     */
    public void setToPlanetId(Integer toPlanetId) {
        this.toPlanetId = toPlanetId;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }
}
