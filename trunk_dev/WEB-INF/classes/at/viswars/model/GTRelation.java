/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "gtrelation")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class GTRelation extends Model<GTRelation> {

    @FieldMappingAnnotation("srcId")
    @IdFieldAnnotation
    private Integer srcId;
    @FieldMappingAnnotation("targetId")
    @IdFieldAnnotation
    private Integer targetId;
    @FieldMappingAnnotation("factor")
    private Float factor;
    @FieldMappingAnnotation("researchRequired")
    @IdFieldAnnotation
    private Boolean researchRequired;
    @FieldMappingAnnotation("attackPoints")
    private Float attackPoints;
    @FieldMappingAnnotation("attackProbab")
    private Float attackProbab;
    @FieldMappingAnnotation("hitProbab")
    private Float hitProbab;

    /**
     * @return the srcId
     */
    public Integer getSrcId() {
        return srcId;
    }

    /**
     * @param srcId the srcId to set
     */
    public void setSrcId(Integer srcId) {
        this.srcId = srcId;
    }

    /**
     * @return the targetId
     */
    public Integer getTargetId() {
        return targetId;
    }

    /**
     * @param targetId the targetId to set
     */
    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    /**
     * @return the factor
     */
    public Float getFactor() {
        return factor;
    }

    /**
     * @param factor the factor to set
     */
    public void setFactor(Float factor) {
        this.factor = factor;
    }

    /**
     * @return the researchRequired
     */
    public Boolean getResearchRequired() {
        return researchRequired;
    }

    /**
     * @param researchRequired the researchRequired to set
     */
    public void setResearchRequired(Boolean researchRequired) {
        this.researchRequired = researchRequired;
    }

    /**
     * @return the attackPoints
     */
    public Float getAttackPoints() {
        return attackPoints;
    }

    /**
     * @param attackPoints the attackPoints to set
     */
    public void setAttackPoints(Float attackPoints) {
        this.attackPoints = attackPoints;
    }

    /**
     * @return the attackProbab
     */
    public Float getAttackProbab() {
        return attackProbab;
    }

    /**
     * @param attackProbab the attackProbab to set
     */
    public void setAttackProbab(Float attackProbab) {
        this.attackProbab = attackProbab;
    }

    /**
     * @return the hitProbab
     */
    public Float getHitProbab() {
        return hitProbab;
    }

    /**
     * @param hitProbab the hitProbab to set
     */
    public void setHitProbab(Float hitProbab) {
        this.hitProbab = hitProbab;
    }
}
