/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "conditiontotitle")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class ConditionToTitle extends Model<ConditionToTitle> {

    public static final int COMMUNICATIVE = 21;
    public static final int CHATTERBOX = 22;
    public static final int EXPLORER_LEAVEYOURSYSTEM = 26;
    public static final int TINKERER_FIGHTERDESIGN = 30;
    public static final int EXPLORER_FRIGATEDESIGN = 37;
    public static final int COLONISATOR_COLONYSHIP = 51;
    public static final int MERCHANT_RESOURCES = 53;
    public static final int TREASUREHUNTER_PRODUCEHOWALGONIUM = 71;
    public static final int MANIAC = 68;
    public static final int STINKER = 72;
    public static final int SLAUGHTERER = 74;
    public static final int THEEXECUTOR = 75;
    public static final int CONQUERER = 76;
    public static final int INVINCIBLE = 77;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("titleId")
    private Integer titleId;
    @FieldMappingAnnotation("conditionId")
    private Integer conditionId;
    @FieldMappingAnnotation("description")
    private String description;
    @FieldMappingAnnotation("conditionOrder")
    private Integer order;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the conditionId
     */
    public Integer getConditionId() {
        return conditionId;
    }

    /**
     * @param conditionId the conditionId to set
     */
    public void setConditionId(Integer conditionId) {
        this.conditionId = conditionId;
    }

    /**
     * @return the titleId
     */
    public Integer getTitleId() {
        return titleId;
    }

    /**
     * @param titleId the titleId to set
     */
    public void setTitleId(Integer titleId) {
        this.titleId = titleId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Integer order) {
        this.order = order;
    }
}
