/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

@TableNameAnnotation(value = "production")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Production extends Model<Production> {

    public static final int PROD_INCREASE = 0;
    public static final int PROD_DECREASE = 1;
    public static final int PROD_STORAGE = 2;
    @FieldMappingAnnotation("constructionId")
    @IndexAnnotation(indexName = "constructionId")
    @IdFieldAnnotation
    private Integer constructionId;
    @IndexAnnotation(indexName = "ressourceId")
    @FieldMappingAnnotation("ressourceId")
    @IdFieldAnnotation
    private Integer ressourceId;
    @FieldMappingAnnotation("quantity")
    private Long quantity;
    @FieldMappingAnnotation("incdec")
    @IdFieldAnnotation
    private Integer incdec;

    /**
     * @return the constructionId
     */
    public Integer getConstructionId() {
        return constructionId;
    }

    /**
     * @param constructionId the constructionId to set
     */
    public void setConstructionId(Integer constructionId) {
        this.constructionId = constructionId;
    }

    /**
     * @return the ressourceId
     */
    public Integer getRessourceId() {
        return ressourceId;
    }

    /**
     * @param ressourceId the ressourceId to set
     */
    public void setRessourceId(Integer ressourceId) {
        this.ressourceId = ressourceId;
    }

    /**
     * @return the quantity
     */
    public Long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the decinc
     */
    public Integer getDecinc() {
        return incdec;
    }

    /**
     * @param decinc the decinc to set
     */
    public void setDecinc(Integer incdec) {
        this.incdec = incdec;
    }
}
