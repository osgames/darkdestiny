/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.model;

import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.OneToMany;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation("fk_testtable")
public class FkTest extends Model<FkTest> {
    @IdFieldAnnotation
    @FieldMappingAnnotation("id")        
    private Integer id;    
    @OneToMany(baseField="id",refTable="FkChildTest",refField="refId")
    private ArrayList<FkChildTest> fkChildTestList;
    @FieldMappingAnnotation("name")
    private String name;

    /**
     * @return the id
     */
    public Integer getId() {
        return id; 
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<FkChildTest> getFkChildTest() {        
        return fkChildTestList;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
