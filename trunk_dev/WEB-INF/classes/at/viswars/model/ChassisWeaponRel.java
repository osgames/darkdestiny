/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "chassisweaponrel")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class ChassisWeaponRel extends Model<ChassisWeaponRel> {

    public final static int COST_FACTOR = 1;
    public final static int SIZE_FACTOR = 2;
    public final static int STRENGTH_FACTOR = 3;
    public final static int ENERGY_FACTOR = 4;
    @FieldMappingAnnotation("chassisId")
    @IdFieldAnnotation()
    private Integer chassisId;
    @FieldMappingAnnotation("moduleId")
    @IdFieldAnnotation()
    private Integer moduleId;
    @FieldMappingAnnotation("cost_factor")
    private Double cost_factor;
    @FieldMappingAnnotation("size_factor")
    private Double size_factor;
    @FieldMappingAnnotation("strength_factor")
    private Double strength_factor;
    @FieldMappingAnnotation("energy_factor")
    private Double energy_factor;

    public Integer getChassisId() {
        return chassisId;
    }

    public void setChassisId(Integer chassisId) {
        this.chassisId = chassisId;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Double getCost_factor() {
        return cost_factor;
    }

    public void setCost_factor(Double cost_factor) {
        this.cost_factor = cost_factor;
    }

    public Double getSize_factor() {
        return size_factor;
    }

    public void setSize_factor(Double size_factor) {
        this.size_factor = size_factor;
    }

    public Double getStrength_factor() {
        return strength_factor;
    }

    public void setStrength_factor(Double strength_factor) {
        this.strength_factor = strength_factor;
    }

    public Double getEnergy_factor() {
        return energy_factor;
    }

    public void setEnergy_factor(Double energy_factor) {
        this.energy_factor = energy_factor;
    }
}
