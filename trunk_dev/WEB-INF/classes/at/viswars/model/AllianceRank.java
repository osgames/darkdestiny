/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "alliancerank")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class AllianceRank extends Model<AllianceRank> {

    public static final String RANK_TRIAL = "alliance_lbl_rank_trial";
    public static final String RANK_MEMBER = "alliance_lbl_rank_member";
    public static final String RANK_COUNCIL = "alliance_lbl_rank_council";
    public static final String RANK_ADMIN = "alliance_lbl_rank_admin";
    public final static int RANK_ADMIN_INT = 4;
    public final static int RANK_COUNCIL_INT = 3;
    public final static int RANK_MEMBER_INT = 2;
    public final static int RANK_TRIAL_INT = 1;
    @FieldMappingAnnotation("allianceId")
    @IdFieldAnnotation
    private Integer allianceId;
    @FieldMappingAnnotation("rankId")
    @IdFieldAnnotation
    private Integer rankId;
    @FieldMappingAnnotation("rankName")
    private String rankName;
    @FieldMappingAnnotation("comparisonRank")
    private Integer comparisonRank;

    /**
     * @return the allianceId
     */
    public Integer getAllianceId() {
        return allianceId;
    }

    /**
     * @param allianceId the allianceId to set
     */
    public void setAllianceId(Integer allianceId) {
        this.allianceId = allianceId;
    }

    /**
     * @return the rankId
     */
    public Integer getRankId() {
        return rankId;
    }

    /**
     * @param rankId the rankId to set
     */
    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    /**
     * @return the rankName
     */
    public String getRankName() {
        return rankName;
    }

    /**
     * @param rankName the rankName to set
     */
    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    /**
     * @return the comparisonRank
     */
    public Integer getComparisonRank() {
        return comparisonRank;
    }

    /**
     * @param comparisonRank the comparisonRank to set
     */
    public void setComparisonRank(Integer comparisonRank) {
        this.comparisonRank = comparisonRank;
    }

    public static String getComparisonRankName(int compareRankId) {
        switch (compareRankId) {
            case (RANK_ADMIN_INT):
                return RANK_ADMIN;
            case (RANK_COUNCIL_INT):
                return RANK_COUNCIL;
            case (RANK_MEMBER_INT):
                return RANK_MEMBER;
            case (RANK_TRIAL_INT):
                return RANK_TRIAL;
        }

        return "DEFAULT";
    }
}
