/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EAddLocation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "traderouteship")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TradeRouteShip extends Model<TradeRouteShip> {

    @FieldMappingAnnotation(value = "traderouteId")
    @IdFieldAnnotation
    private Integer tradeRouteId;
    @FieldMappingAnnotation(value = "shiptype")
    @IdFieldAnnotation
    private Integer shipType;
    @FieldMappingAnnotation(value = "addLocation")
    @IdFieldAnnotation
    private EAddLocation addLocation;
    @FieldMappingAnnotation(value = "designId")
    @IdFieldAnnotation
    private Integer designId;
    @FieldMappingAnnotation(value = "number")
    private Integer number;
    public static final int SHIPTYPE_NORMAL = 1;
    public static final int SHIPTYPE_CONSTRUCTION = 2;

    /**
     * @return the tradeRouteId
     */
    public Integer getTradeRouteId() {
        return tradeRouteId;
    }

    /**
     * @param tradeRouteId the tradeRouteId to set
     */
    public void setTradeRouteId(Integer tradeRouteId) {
        this.tradeRouteId = tradeRouteId;
    }

    /**
     * @return the shipType
     */
    public Integer getShipType() {
        return shipType;
    }

    /**
     * @param shipType the shipType to set
     */
    public void setShipType(Integer shipType) {
        this.shipType = shipType;
    }

    /**
     * @return the designId
     */
    public Integer getDesignId() {
        return designId;
    }

    /**
     * @param designId the designId to set
     */
    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    /**
     * @return the number
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    public EAddLocation getAddLocation() {
        return addLocation;
    }

    public void setAddLocation(EAddLocation addLocation) {
        this.addLocation = addLocation;
    }
}
