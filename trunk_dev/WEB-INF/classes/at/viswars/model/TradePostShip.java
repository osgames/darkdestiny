/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "tradepostship")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TradePostShip extends Model<TradePostShip> {

    @FieldMappingAnnotation("tradePostId")
    @IdFieldAnnotation
    private Integer tradePostId;
    @IdFieldAnnotation
    @FieldMappingAnnotation(value = "designId")
    private Integer designId;
    @FieldMappingAnnotation(value = "count")
    private Integer count;

    /**
     * @return the tradePostId
     */
    public Integer getTradePostId() {
        return tradePostId;
    }

    /**
     * @param tradePostId the tradePostId to set
     */
    public void setTradePostId(Integer tradePostId) {
        this.tradePostId = tradePostId;
    }

    /**
     * @return the designId
     */
    public Integer getDesignId() {
        return designId;
    }

    /**
     * @param designId the designId to set
     */
    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }
}
