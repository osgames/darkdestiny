/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DataScope.EScopeType;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.StringType;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EMessageRefType;
import at.viswars.enumeration.EMessageType;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "message")
@DataScope(type = EScopeType.USER_GENERATED)
public class Message extends Model<Message> {

    @FieldMappingAnnotation("messageId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer messageId;
    @IndexAnnotation(indexName = "message")
    @FieldMappingAnnotation("sourceUserId")
    @DefaultValue("0")
    @ColumnProperties(unsigned = true)
    private Integer sourceUserId;
    @IndexAnnotation(indexName = "message")
    @FieldMappingAnnotation("targetUserId")
    @DefaultValue("0")
    @ColumnProperties(unsigned = true)
    private Integer targetUserId;
    @FieldMappingAnnotation("topic")
    @StringType("text")
    private String topic;
    @FieldMappingAnnotation("text")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String text;
    @FieldMappingAnnotation("timeSent")
    @DefaultValue("0")
    @ColumnProperties(unsigned = true)
    private Long timeSent;
    @IndexAnnotation(indexName = "message")
    @FieldMappingAnnotation("type")
    @DefaultValue("SYSTEM")
    private EMessageType type;
    @FieldMappingAnnotation("viewed")
    @DefaultValue("0")
    private Boolean viewed;
    @FieldMappingAnnotation("delBySource")
    @DefaultValue("0")
    private Boolean delBySource;
    @FieldMappingAnnotation("delByTarget")
    @DefaultValue("0")
    private Boolean delByTarget;
    @FieldMappingAnnotation("archiveBySource")
    @DefaultValue("0")
    private Boolean archiveBySource;
    @FieldMappingAnnotation("archiveByTarget")
    @DefaultValue("0")
    private Boolean archiveByTarget;
    @FieldMappingAnnotation("outMaster")
    @DefaultValue("0")
    private Boolean outMaster;
    @FieldMappingAnnotation("refType")
    @DefaultValue("NOTHING")
    private EMessageRefType refType;
    @FieldMappingAnnotation("refId")
    @DefaultValue("0")
    private Integer refId;

    /**
     * @return the messageId
     */
    public Integer getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the sourceUserId
     */
    public Integer getSourceUserId() {
        return sourceUserId;
    }

    /**
     * @param sourceUserId the sourceUserId to set
     */
    public void setSourceUserId(Integer sourceUserId) {
        this.sourceUserId = sourceUserId;
    }

    /**
     * @return the targetUserId
     */
    public Integer getTargetUserId() {
        return targetUserId;
    }

    /**
     * @param targetUserId the targetUserId to set
     */
    public void setTargetUserId(Integer targetUserId) {
        this.targetUserId = targetUserId;
    }

    /**
     * @return the topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the timeSent
     */
    public Long getTimeSent() {
        return timeSent;
    }

    /**
     * @param timeSent the timeSent to set
     */
    public void setTimeSent(Long timeSent) {
        this.timeSent = timeSent;
    }

    /**
     * @return the messageType
     */
    public EMessageType getType() {
        return type;
    }

    /**
     * @param messageType the messageType to set
     */
    public void setType(EMessageType type) {
        this.type = type;
    }

    /**
     * @return the viewed
     */
    public Boolean getViewed() {
        return viewed;
    }

    /**
     * @param viewed the viewed to set
     */
    public void setViewed(Boolean viewed) {
        this.viewed = viewed;
    }

    /**
     * @return the delBySource
     */
    public Boolean getDelBySource() {
        return delBySource;
    }

    /**
     * @param delBySource the delBySource to set
     */
    public void setDelBySource(Boolean delBySource) {
        this.delBySource = delBySource;
    }

    /**
     * @return the delByTarget
     */
    public Boolean getDelByTarget() {
        return delByTarget;
    }

    /**
     * @param delByTarget the delByTarget to set
     */
    public void setDelByTarget(Boolean delByTarget) {
        this.delByTarget = delByTarget;
    }

    /**
     * @return the outMaster
     */
    public Boolean getOutMaster() {
        return outMaster;
    }

    /**
     * @param outMaster the outMaster to set
     */
    public void setOutMaster(Boolean outMaster) {
        this.outMaster = outMaster;
    }

    /**
     * @return the archiveBySource
     */
    public Boolean getArchiveBySource() {
        return archiveBySource;
    }

    /**
     * @param archiveBySource the archiveBySource to set
     */
    public void setArchiveBySource(Boolean archiveBySource) {
        this.archiveBySource = archiveBySource;
    }

    /**
     * @return the archiveByTarget
     */
    public Boolean getArchiveByTarget() {
        return archiveByTarget;
    }

    /**
     * @param archiveByTarget the archiveByTarget to set
     */
    public void setArchiveByTarget(Boolean archiveByTarget) {
        this.archiveByTarget = archiveByTarget;
    }

    /**
     * @return the refType
     */
    public EMessageRefType getRefType() {
        return refType;
    }

    /**
     * @param refType the refType to set
     */
    public void setRefType(EMessageRefType refType) {
        this.refType = refType;
    }

    /**
     * @return the refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }
}
