/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.ESpyTargetEntityType;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "spyaction")
public class SpyAction extends Model<SpyAction> {
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true, autoIncrement=true)
    private Integer id;
    @FieldMappingAnnotation("spyId")
    private Integer spyId;
    @FieldMappingAnnotation("spyActionId")
    private Integer spyActionId;
    @FieldMappingAnnotation("durationLeft")
    private Integer durationLeft;
    @FieldMappingAnnotation("targetEntityType")
    private ESpyTargetEntityType targetEntityType;
    @FieldMappingAnnotation("targetEntityId")
    private Integer targetEntityId;
    @FieldMappingAnnotation("successProbability")
    private Integer successProbability;
    @FieldMappingAnnotation("startSystem")
    private Integer startSystem;
    @FieldMappingAnnotation("startPlanet")
    private Integer startPlanet;
    @FieldMappingAnnotation("targetSystem")
    private Integer targetSystem;
    @FieldMappingAnnotation("targetPlanet")
    private Integer targetPlanet;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the spyId
     */
    public Integer getSpyId() {
        return spyId;
    }

    /**
     * @param spyId the spyId to set
     */
    public void setSpyId(Integer spyId) {
        this.spyId = spyId;
    }

    /**
     * @return the spyActionId
     */
    public Integer getSpyActionId() {
        return spyActionId;
    }

    /**
     * @param spyActionId the spyActionId to set
     */
    public void setSpyActionId(Integer spyActionId) {
        this.spyActionId = spyActionId;
    }

    /**
     * @return the durationLeft
     */
    public Integer getDurationLeft() {
        return durationLeft;
    }

    /**
     * @param durationLeft the durationLeft to set
     */
    public void setDurationLeft(Integer durationLeft) {
        this.durationLeft = durationLeft;
    }

    /**
     * @return the targetEntityType
     */
    public ESpyTargetEntityType getTargetEntityType() {
        return targetEntityType;
    }

    /**
     * @param targetEntityType the targetEntityType to set
     */
    public void setTargetEntityType(ESpyTargetEntityType targetEntityType) {
        this.targetEntityType = targetEntityType;
    }

    /**
     * @return the targetEntityId
     */
    public Integer getTargetEntityId() {
        return targetEntityId;
    }

    /**
     * @param targetEntityId the targetEntityId to set
     */
    public void setTargetEntityId(Integer targetEntityId) {
        this.targetEntityId = targetEntityId;
    }

    /**
     * @return the successProbability
     */
    public Integer getSuccessProbability() {
        return successProbability;
    }

    /**
     * @param successProbability the successProbability to set
     */
    public void setSuccessProbability(Integer successProbability) {
        this.successProbability = successProbability;
    }

    /**
     * @return the startSystem
     */
    public Integer getStartSystem() {
        return startSystem;
    }

    /**
     * @param startSystem the startSystem to set
     */
    public void setStartSystem(Integer startSystem) {
        this.startSystem = startSystem;
    }

    /**
     * @return the startPlanet
     */
    public Integer getStartPlanet() {
        return startPlanet;
    }

    /**
     * @param startPlanet the startPlanet to set
     */
    public void setStartPlanet(Integer startPlanet) {
        this.startPlanet = startPlanet;
    }

    /**
     * @return the targetSystem
     */
    public Integer getTargetSystem() {
        return targetSystem;
    }

    /**
     * @param targetSystem the targetSystem to set
     */
    public void setTargetSystem(Integer targetSystem) {
        this.targetSystem = targetSystem;
    }

    /**
     * @return the targetPlanet
     */
    public Integer getTargetPlanet() {
        return targetPlanet;
    }

    /**
     * @param targetPlanet the targetPlanet to set
     */
    public void setTargetPlanet(Integer targetPlanet) {
        this.targetPlanet = targetPlanet;
    }
}
