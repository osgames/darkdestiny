/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.MultiLogDAO;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Dreloc
 */
@TableNameAnnotation(value = "multilog")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class MultiLog extends Model<MultiLog> {

    @FieldMappingAnnotation("tick")
    @IdFieldAnnotation
    private Integer tick;
    @FieldMappingAnnotation("ip")
    @IdFieldAnnotation
    private String ip;
    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("date")
    private Long date;
    @FieldMappingAnnotation("count")
    private Integer count;
    @FieldMappingAnnotation("comment")
    private String comment;

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the tick
     */
    public Integer getTick() {
        return tick;
    }

    /**
     * @param tick the tick to set
     */
    public void setTick(Integer tick) {
        this.tick = tick;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    public void incrementCount() {
        MultiLogDAO mlDAO = (MultiLogDAO) DAOFactory.get(MultiLogDAO.class);
        count++;
        mlDAO.update(this);
    }
}
