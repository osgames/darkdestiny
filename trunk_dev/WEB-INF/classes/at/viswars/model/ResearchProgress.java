/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "researchprogress")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ResearchProgress extends Model<ResearchProgress> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @IndexAnnotation(indexName = "userresearch")
    @FieldMappingAnnotation("userId")
    private Integer userId;
    @IndexAnnotation(indexName = "userresearch")
    @FieldMappingAnnotation("researchId")
    private Integer researchId;
    @FieldMappingAnnotation("rpLeft")
    private Integer rpLeft;
    @FieldMappingAnnotation("rpLeftComputer")
    private Integer rpLeftComputer;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the resId
     */
    public Integer getResearchId() {
        return researchId;
    }

    /**
     * @param resId the resId to set
     */
    public void setResearchId(Integer researchId) {
        this.researchId = researchId;
    }

    /**
     * @return the rpLeft
     */
    public Integer getRpLeft() {
        return rpLeft;
    }

    /**
     * @param rpLeft the rpLeft to set
     */
    public void setRpLeft(Integer rpLeft) {
        this.rpLeft = rpLeft;
    }

    /**
     * @return the rpLeftComputer
     */
    public Integer getRpLeftComputer() {
        return rpLeftComputer;
    }

    /**
     * @param rpLeftComputer the rpLeftComputer to set
     */
    public void setRpLeftComputer(Integer rpLeftComputer) {
        this.rpLeftComputer = rpLeftComputer;
    }
}
