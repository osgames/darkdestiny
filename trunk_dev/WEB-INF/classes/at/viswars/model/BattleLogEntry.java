/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "battlelogentry")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class BattleLogEntry extends Model<BattleLogEntry> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    private Integer id;
    @IdFieldAnnotation
    @FieldMappingAnnotation("designId")
    private Integer designId;
    @FieldMappingAnnotation("count")
    private Integer count;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the designId
     */
    public Integer getDesignId() {
        return designId;
    }

    /**
     * @param designId the designId to set
     */
    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }
}
