/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Admin
 */
@TableNameAnnotation(value = "galaxy")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Galaxy extends Model<Galaxy> {

    @FieldMappingAnnotation("id")
    @ColumnProperties(unsigned = true, autoIncrement = true)
    @IdFieldAnnotation
    private Integer id;
    @FieldMappingAnnotation("width")
    private Integer width;
    @FieldMappingAnnotation("height")
    private Integer height;
    @FieldMappingAnnotation("originX")
    private Integer originX;
    @FieldMappingAnnotation("originY")
    private Integer originY;
    @FieldMappingAnnotation("startSystem")
    private Integer startSystem;
    @FieldMappingAnnotation("endSystem")
    private Integer endSystem;
    @FieldMappingAnnotation("playerStart")
    @DefaultValue("0")
    private Boolean playerStart;
    @FieldMappingAnnotation("allowIntergalacticFlight")
    @DefaultValue("0")
    private Boolean intergalacticFlight;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return the originX
     */
    public Integer getOriginX() {
        return originX;
    }

    /**
     * @param originX the originX to set
     */
    public void setOriginX(Integer originX) {
        this.originX = originX;
    }

    /**
     * @return the originY
     */
    public Integer getOriginY() {
        return originY;
    }

    /**
     * @param originY the originY to set
     */
    public void setOriginY(Integer originY) {
        this.originY = originY;
    }

    /**
     * @return the startSystem
     */
    public Integer getStartSystem() {
        return startSystem;
    }

    /**
     * @param startSystem the startSystem to set
     */
    public void setStartSystem(Integer startSystem) {
        this.startSystem = startSystem;
    }

    /**
     * @return the endSystem
     */
    public Integer getEndSystem() {
        return endSystem;
    }

    /**
     * @param endSystem the endSystem to set
     */
    public void setEndSystem(Integer endSystem) {
        this.endSystem = endSystem;
    }

    /**
     * @return the playerStart
     */
    public Boolean getPlayerStart() {
        return playerStart;
    }

    /**
     * @param playerStart the playerStart to set
     */
    public void setPlayerStart(Boolean playerStart) {
        this.playerStart = playerStart;
    }

    /**
     * @return the intergalacticFlight
     */
    public Boolean isIntergalacticFlight() {
        return intergalacticFlight;
    }

    /**
     * @param intergalacticFlight the intergalacticFlight to set
     */
    public void setIntergalacticFlight(Boolean intergalacticFlight) {
        this.intergalacticFlight = intergalacticFlight;
    }
}
