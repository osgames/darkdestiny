/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.StringType;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EFilterValue;
import at.viswars.enumeration.EFilterValueType;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "filtervalue")
public class FilterValue extends Model<FilterValue> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("value")
    private EFilterValue value;
    @FieldMappingAnnotation("type")
    private EFilterValueType type;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public EFilterValue getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(EFilterValue value) {
        this.value = value;
    }

    /**
     * @return the type
     */
    public EFilterValueType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EFilterValueType type) {
        this.type = type;
    }
}
