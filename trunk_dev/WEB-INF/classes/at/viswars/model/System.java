/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.interfaces.IStellarBody;
import at.viswars.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "system")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class System extends Model<at.viswars.model.System> implements IStellarBody {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("posx")
    private Integer posx;
    @FieldMappingAnnotation("posy")
    private Integer posy;
    @FieldMappingAnnotation("visibility")
    private Integer visibility;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the x
     */
    public Integer getX() {
        return posx;
    }

    /**
     * @param x the x to set
     */
    public void setX(Integer x) {
        this.posx = x;
    }

    /**
     * @return the y
     */
    public Integer getY() {
        return posy;
    }

    /**
     * @param y the y to set
     */
    public void setY(Integer y) {
        this.posy = y;
    }

    /**
     * @return the visibility
     */
    public Integer getVisibility() {
        return visibility;
    }

    /**
     * @param visibility the visibility to set
     */
    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public Integer getGalaxyId() {
        for (Galaxy g : (ArrayList<Galaxy>) Service.galaxyDAO.findAll()) {
            if (g.getStartSystem() <= id && g.getEndSystem() >= id) {
                return g.getId();
            }
        }
        return null;
    }

    public Galaxy getGalaxy() {
        for (Galaxy g : (ArrayList<Galaxy>) Service.galaxyDAO.findAll()) {
            if (g.getStartSystem() <= id && g.getEndSystem() >= id) {
                return g;
            }
        }

        java.lang.System.err.println("System : " + id + " is not within GalaxyBounds");
        return null;
    }

    @Override
    public RelativeCoordinate getRelativeCoordinate() {
        return new RelativeCoordinate(0, id);
    }

    @Override
    public AbsoluteCoordinate getAbsoluteCoordinate() {
        return getRelativeCoordinate().toAbsoluteCoordinate();
    }
}
