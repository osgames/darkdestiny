/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

/**
 *
 * @author Eobane
 */
public class Model<T> implements Cloneable {        
    @Override
    @SuppressWarnings("unchecked")
    public T clone() {
        try {
            T tmp = (T) super.clone();
            return tmp;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public void debugPrint() {
        
    }      
}
