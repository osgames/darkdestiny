/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "ressource")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Ressource extends Model<Ressource> implements ITechFunctions {

    public static final int IRON = 1;
    public static final int STEEL = 2;
    public static final int TERKONIT = 3;
    public static final int YNKELONIUM = 4;
    public static final int HOWALGONIUM = 5;
    public static final int CVEMBINIUM = 6;
    public static final int WASSERSTOFF = 7;
    public static final int ENERGY = 8;
    public static final int FOOD = 9;
    public static final int RP = 10;
    public static final int RP_COMP = 14;
    public static final int PLANET_IRON = 11;
    public static final int PLANET_YNKELONIUM = 12;
    public static final int PLANET_HOWALGONIUM = 13;
    public static final int CREDITS = 20;
    public static final int POPULATION = 20;
    public static final int MODUL_AP = 30;
    public static final int PL_DOCK = 31;
    public static final int ORB_DOCK = 32;
    public static final int KAS = 33;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    Integer id;
    @FieldMappingAnnotation("name")
    String name;
    @FieldMappingAnnotation("transportable")
    Boolean transportable;
    @FieldMappingAnnotation("mineable")
    Boolean mineable;
    @FieldMappingAnnotation("storeable")
    Boolean storable;
    @FieldMappingAnnotation("ressourcecost")
    Boolean ressourcecost;
    @FieldMappingAnnotation("baseRess")
    Integer baseRess;
    @FieldMappingAnnotation("researchRequired")
    Integer researchRequired;
    @FieldMappingAnnotation("displayLocation")
    Integer displayLocation;
    @FieldMappingAnnotation("imageLocation")
    String imageLocation;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the transportable
     */
    public Boolean getTransportable() {
        return transportable;
    }

    /**
     * @param transportable the transportable to set
     */
    public void setTransportable(Boolean transportable) {
        this.transportable = transportable;
    }

    /**
     * @return the mineable
     */
    public Boolean getMineable() {
        return mineable;
    }

    /**
     * @param mineable the mineable to set
     */
    public void setMineable(Boolean mineable) {
        this.mineable = mineable;
    }

    /**
     * @return the baseRess
     */
    public Integer getBaseRess() {
        return baseRess;
    }

    /**
     * @param baseRess the baseRess to set
     */
    public void setBaseRess(Integer baseRess) {
        this.baseRess = baseRess;
    }

    /**
     * @return the researchRequired
     */
    public Integer getResearchRequired() {
        return researchRequired;
    }

    /**
     * @param researchRequired the researchRequired to set
     */
    public void setResearchRequired(Integer researchRequired) {
        this.researchRequired = researchRequired;
    }

    /**
     * @return the displayLocation
     */
    public Integer getDisplayLocation() {
        return displayLocation;
    }

    /**
     * @param displayLocation the displayLocation to set
     */
    public void setDisplayLocation(Integer displayLocation) {
        this.displayLocation = displayLocation;
    }

    /**
     * @return the imageLocation
     */
    public String getImageLocation() {
        return imageLocation;
    }

    /**
     * @param imageLocation the imageLocation to set
     */
    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    public int getTechId() {
        return id;
    }

    public String getTechName() {
        return name;
    }

    public String getTechDescription() {
        return "";
    }

    public Boolean getRessourcecost() {
        return ressourcecost;
    }

    public void setRessourcecost(Boolean ressourcecost) {
        this.ressourcecost = ressourcecost;
    }

    public Boolean getStorable() {
        return storable;
    }

    public void setStorable(Boolean storable) {
        this.storable = storable;
    }
}
