/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EPlanetRessourceType;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "planetressource")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlanetRessource extends Model<PlanetRessource> implements DataChangeAware {

    /*
     public static final Integer TYPE_RESSOURCE_PLANET = 0;
     public static final Integer TYPE_RESSOURCE_INSTORAGE = 1;
     public static final Integer TYPE_RESSOURCE_MINIMUM = 2;
     */
    @FieldMappingAnnotation("planetId")
    @IdFieldAnnotation
    private Integer planetId;
    @FieldMappingAnnotation("ressId")
    @IdFieldAnnotation
    private Integer ressId;
    @FieldMappingAnnotation("type")
    @IdFieldAnnotation
    private EPlanetRessourceType type;
    @FieldMappingAnnotation("qty")
    private Long qty;
    private boolean modified = false;

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the type
     */
    public EPlanetRessourceType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EPlanetRessourceType type) {
        this.type = type;
    }

    /**
     * @return the ressId
     */
    public Integer getRessId() {
        return ressId;
    }

    /**
     * @param ressId the ressId to set
     */
    public void setRessId(Integer ressId) {
        this.ressId = ressId;
    }

    /**
     * @return the qty
     */
    public Long getQty() {
        return qty;
    }

    /**
     * @param qty the qty to set
     */
    public void setQty(Long qty) {
        if ((this.qty != null) && !this.qty.equals(qty)) {
            modified = true;
        }
        this.qty = qty;
    }

    /**
     * @return the modified
     */
    public boolean isModified() {
        return modified;
    }
}
