/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Admin
 */
@TableNameAnnotation(value = "quizquestion")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class QuizQuestion extends Model<QuizQuestion> {

    @IdFieldAnnotation
    @FieldMappingAnnotation(value = "quizEntryId")
    private Integer quizEntryId;
    @IdFieldAnnotation
    @FieldMappingAnnotation(value = "question")
    private String question;

    /**
     * @return the quizEntryId
     */
    public Integer getQuizEntryId() {
        return quizEntryId;
    }

    /**
     * @param quizEntryId the quizEntryId to set
     */
    public void setQuizEntryId(Integer quizEntryId) {
        this.quizEntryId = quizEntryId;
    }

    /**
     * @return the question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * @param question the question to set
     */
    public void setQuestion(String question) {
        this.question = question;
    }
}
