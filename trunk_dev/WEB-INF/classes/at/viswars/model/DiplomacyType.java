/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "diplomacytype")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class DiplomacyType extends Model<DiplomacyType> {

    public static final Integer ALLY = 0;
    public static final Integer TREATY = 1;
    public static final Integer NAP = 2;
    public static final Integer NEUTRAL = 5;
    public static final Integer WAR = 7;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = false)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("description")
    private String description;
    @FieldMappingAnnotation("clazz")
    private String clazz;
    @FieldMappingAnnotation("voteRequired")
    private boolean voteRequired;
    @FieldMappingAnnotation("requestString")
    private String requestString;
    @FieldMappingAnnotation("weight")
    private Integer weight;
    @FieldMappingAnnotation("userDefineable")
    private Boolean userDefineable;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the clazz
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * @param clazz the clazz to set
     */
    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    /**
     * @return the voteRequired
     */
    public boolean isVoteRequired() {
        return voteRequired;
    }

    /**
     * @param voteRequired the voteRequired to set
     */
    public void setVoteRequired(boolean voteRequired) {
        this.voteRequired = voteRequired;
    }

    /**
     * @return the requestString
     */
    public String getRequestString() {
        return requestString;
    }

    /**
     * @param requestString the requestString to set
     */
    public void setRequestString(String requestString) {
        this.requestString = requestString;
    }

    /**
     * @return the weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * @return the userDefineable
     */
    public Boolean getUserDefineable() {
        return userDefineable;
    }

    /**
     * @param userDefineable the userDefineable to set
     */
    public void setUserDefineable(Boolean userDefineable) {
        this.userDefineable = userDefineable;
    }
}
