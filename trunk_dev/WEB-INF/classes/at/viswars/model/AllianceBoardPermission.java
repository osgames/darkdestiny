/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EAllianceBoardPermissionType;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "allianceboardpermission")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class AllianceBoardPermission extends Model<AllianceBoardPermission> {

    @FieldMappingAnnotation("refId")
    @IdFieldAnnotation
    private Integer refId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("allianceBoardId")
    private Integer allianceBoardId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("type")
    private EAllianceBoardPermissionType type;
    @FieldMappingAnnotation("isWrite")
    private Boolean write;
    @FieldMappingAnnotation("isDelete")
    private Boolean delete;
    @FieldMappingAnnotation("isConfirmed")
    private Boolean confirmed;

    /**
     * @return the refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    /**
     * @return the allianceboardId
     */
    public Integer getAllianceBoardId() {
        return allianceBoardId;
    }

    /**
     * @param allianceboardId the allianceboardId to set
     */
    public void setAllianceBoardId(Integer allianceBoardId) {
        this.allianceBoardId = allianceBoardId;
    }

    /**
     * @return the type
     */
    public EAllianceBoardPermissionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EAllianceBoardPermissionType type) {
        this.type = type;
    }

    /**
     * @return the write
     */
    public Boolean getWrite() {
        return write;
    }

    /**
     * @param write the write to set
     */
    public void setWrite(Boolean write) {
        this.write = write;
    }

    /**
     * @return the delete
     */
    public Boolean getDelete() {
        return delete;
    }

    /**
     * @param delete the delete to set
     */
    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    /**
     * @return the confirmed
     */
    public Boolean getConfirmed() {
        return confirmed;
    }

    /**
     * @param confirmed the confirmed to set
     */
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }
}
