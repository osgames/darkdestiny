/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "conditiontouser")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ConditionToUser extends Model<ConditionToUser> {

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("conditionToTitleId")
    @IdFieldAnnotation
    private Integer conditionToTitleId;
    @FieldMappingAnnotation("value")
    private String value;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the conditionToTitleId
     */
    public Integer getConditionToTitleId() {
        return conditionToTitleId;
    }

    /**
     * @param conditionToTitleId the conditionToTitleId to set
     */
    public void setConditionToTitleId(Integer conditionToTitleId) {
        this.conditionToTitleId = conditionToTitleId;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
