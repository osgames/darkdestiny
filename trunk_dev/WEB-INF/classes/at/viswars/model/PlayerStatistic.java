/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "playerstatistics")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlayerStatistic extends Model<PlayerStatistic> {

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("date")
    @IdFieldAnnotation
    private Long date;
    @FieldMappingAnnotation("researchPoints")
    private Long researchPoints;
    @FieldMappingAnnotation("ressourcePoints")
    private Long ressourcePoints;
    @FieldMappingAnnotation("populationPoints")
    private Long populationPoints;
    @FieldMappingAnnotation("militaryPoints")
    private Long militaryPoints;
    @FieldMappingAnnotation("rank")
    private Integer rank;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the rank
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Long getSum() {
        return getMilitaryPoints() + getPopulationPoints() + getRessourcePoints() + getResearchPoints();
    }

    /**
     * @return the researchPoints
     */
    public Long getResearchPoints() {
        return researchPoints;
    }

    /**
     * @param researchPoints the researchPoints to set
     */
    public void setResearchPoints(Long researchPoints) {
        this.researchPoints = researchPoints;
    }

    /**
     * @return the ressourcePoints
     */
    public Long getRessourcePoints() {
        return ressourcePoints;
    }

    /**
     * @param ressourcePoints the ressourcePoints to set
     */
    public void setRessourcePoints(Long ressourcePoints) {
        this.ressourcePoints = ressourcePoints;
    }

    /**
     * @return the populationPoints
     */
    public Long getPopulationPoints() {
        return populationPoints;
    }

    /**
     * @param populationPoints the populationPoints to set
     */
    public void setPopulationPoints(Long populationPoints) {
        this.populationPoints = populationPoints;
    }

    /**
     * @return the militaryPoints
     */
    public Long getMilitaryPoints() {
        return militaryPoints;
    }

    /**
     * @param militaryPoints the militaryPoints to set
     */
    public void setMilitaryPoints(Long militaryPoints) {
        this.militaryPoints = militaryPoints;
    }
}
