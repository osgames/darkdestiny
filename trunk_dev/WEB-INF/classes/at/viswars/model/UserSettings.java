/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "usersettings")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class UserSettings extends Model<UserSettings> {

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("starmapHeight")
    @DefaultValue("600")
    private Integer starmapHeight;
    @FieldMappingAnnotation("starmapWidth")
    @DefaultValue("600")
    private Integer starmapWidth;
    @FieldMappingAnnotation("starmapExternalBrowser")
    @DefaultValue("0")
    private Boolean starmapExternalBrowser;
    @FieldMappingAnnotation("ttvHeight")
    @DefaultValue("600")
    private Integer ttvHeight;
    @FieldMappingAnnotation("ttvWidth")
    @DefaultValue("600")
    private Integer ttvWidth;
    @FieldMappingAnnotation("ttvExternalBrowser")
    @DefaultValue("0")
    private Boolean ttvExternalBrowser;
    @FieldMappingAnnotation("leaveDays")
    @DefaultValue("0")
    private Integer leaveDays;
    @FieldMappingAnnotation("maxLeaveTicks")
    @DefaultValue("0")
    private Integer maxLeaveTicks;
    @FieldMappingAnnotation("leaveDate")
    @DefaultValue("0")
    private Long leaveDate;
    @FieldMappingAnnotation("sittedById")
    @DefaultValue("0")
    private Integer sittedById;
    @FieldMappingAnnotation("showWelcomeSite")
    @DefaultValue("1")
    private Boolean showWelcomeSite;
    @FieldMappingAnnotation("showConstructionDetails")
    @DefaultValue("1")
    private Boolean showConstructionDetails;
    @FieldMappingAnnotation("showProductionDetails")
    @DefaultValue("1")
    private Boolean showProductionDetails;
    @FieldMappingAnnotation("newsletter")
    @DefaultValue("1")
    private Boolean newsletter;
    @FieldMappingAnnotation("protectaccount")
    @DefaultValue("1")
    private Boolean protectaccount;

    public boolean isFrozen() {
        if (leaveDays > 0 && sittedById == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return the starmapHeight
     */
    public Integer getStarmapHeight() {
        return starmapHeight;
    }

    /**
     * @param starmapHeight the starmapHeight to set
     */
    public void setStarmapHeight(Integer starmapHeight) {
        this.starmapHeight = starmapHeight;
    }

    /**
     * @return the starmapWidth
     */
    public Integer getStarmapWidth() {
        return starmapWidth;
    }

    /**
     * @param starmapWidth the starmapWidth to set
     */
    public void setStarmapWidth(Integer starmapWidth) {
        this.starmapWidth = starmapWidth;
    }

    /**
     * @return the starmapExternalBrowser
     */
    public Boolean getStarmapExternalBrowser() {
        return starmapExternalBrowser;
    }

    /**
     * @param starmapExternalBrowser the starmapExternalBrowser to set
     */
    public void setStarmapExternalBrowser(Boolean starmapExternalBrowser) {
        this.starmapExternalBrowser = starmapExternalBrowser;
    }

    /**
     * @return the leaveDays
     */
    public Integer getLeaveDays() {
        return leaveDays;
    }

    /**
     * @param leaveDays the leaveDays to set
     */
    public void setLeaveDays(Integer leaveDays) {
        this.leaveDays = leaveDays;
    }

    /**
     * @return the leaveDate
     */
    public Long getLeaveDate() {
        return leaveDate;
    }

    /**
     * @param leaveDate the leaveDate to set
     */
    public void setLeaveDate(Long leaveDate) {
        this.leaveDate = leaveDate;
    }

    /**
     * @return the sittedById
     */
    public Integer getSittedById() {
        return sittedById;
    }

    /**
     * @param sittedById the sittedById to set
     */
    public void setSittedById(Integer sittedById) {
        this.sittedById = sittedById;
    }

    /**
     * @return the showWelcomeSite
     */
    public Boolean getShowWelcomeSite() {
        return showWelcomeSite;
    }

    /**
     * @param showWelcomeSite the showWelcomeSite to set
     */
    public void setShowWelcomeSite(Boolean showWelcomeSite) {
        this.showWelcomeSite = showWelcomeSite;
    }

    /**
     * @return the showConstructionDetails
     */
    public Boolean getShowConstructionDetails() {
        return showConstructionDetails;
    }

    /**
     * @param showConstructionDetails the showConstructionDetails to set
     */
    public void setShowConstructionDetails(Boolean showConstructionDetails) {
        this.showConstructionDetails = showConstructionDetails;
    }

    /**
     * @return the ttvHeight
     */
    public Integer getTtvHeight() {
        return ttvHeight;
    }

    /**
     * @param ttvHeight the ttvHeight to set
     */
    public void setTtvHeight(Integer ttvHeight) {
        this.ttvHeight = ttvHeight;
    }

    /**
     * @return the ttvWidth
     */
    public Integer getTtvWidth() {
        return ttvWidth;
    }

    /**
     * @param ttvWidth the ttvWidth to set
     */
    public void setTtvWidth(Integer ttvWidth) {
        this.ttvWidth = ttvWidth;
    }

    /**
     * @return the ttvExternalBrowser
     */
    public Boolean getTtvExternalBrowser() {
        return ttvExternalBrowser;
    }

    /**
     * @param ttvExternalBrowser the ttvExternalBrowser to set
     */
    public void setTtvExternalBrowser(Boolean ttvExternalBrowser) {
        this.ttvExternalBrowser = ttvExternalBrowser;
    }

    /**
     * @return the maxLeaveTicks
     */
    public Integer getMaxLeaveTicks() {
        return maxLeaveTicks;
    }

    /**
     * @param maxLeaveTicks the maxLeaveTicks to set
     */
    public void setMaxLeaveTicks(Integer maxLeaveTicks) {
        this.maxLeaveTicks = maxLeaveTicks;
    }

    /**
     * @return the newsletter
     */
    public Boolean getNewsletter() {
        return newsletter;
    }

    /**
     * @param newsletter the newsletter to set
     */
    public void setNewsletter(Boolean newsletter) {
        this.newsletter = newsletter;
    }

    /**
     * @return the protectaccount
     */
    public Boolean getProtectaccount() {
        return protectaccount;
    }

    /**
     * @param protectaccount the protectaccount to set
     */
    public void setProtectaccount(Boolean protectaccount) {
        this.protectaccount = protectaccount;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the showProductionDetails
     */
    public Boolean getShowProductionDetails() {
        return showProductionDetails;
    }

    /**
     * @param showProductionDetails the showProductionDetails to set
     */
    public void setShowProductionDetails(Boolean showProductionDetails) {
        this.showProductionDetails = showProductionDetails;
    }
}
