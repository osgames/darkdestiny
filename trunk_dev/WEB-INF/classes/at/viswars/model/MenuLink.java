/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.StringType;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "menulink")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class MenuLink extends Model<MenuLink> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("linkOrder")
    private Integer linkOrder;
    @FieldMappingAnnotation("linkName")
    @ColumnProperties(length = 400)
    @StringType("text")
    private String linkName;
    @ColumnProperties(length = 400)
    @StringType("text")
    @FieldMappingAnnotation("linkUrl")
    private String linkUrl;
    @FieldMappingAnnotation("linkDest")
    private String linkDest;
    //Refers to SQL Table: menuImage-ID
    @FieldMappingAnnotation("menuLinkId")
    private Integer menuImageId;
    @FieldMappingAnnotation("accessLevel")
    private Integer accessLevel;
    @FieldMappingAnnotation("adminOnly")
    private Boolean adminOnly;
    @FieldMappingAnnotation("subCategory")
    private Integer subCategory;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the linkOrder
     */
    public Integer getLinkOrder() {
        return linkOrder;
    }

    /**
     * @param linkOrder the linkOrder to set
     */
    public void setLinkOrder(Integer linkOrder) {
        this.linkOrder = linkOrder;
    }

    /**
     * @return the linkName
     */
    public String getLinkName() {
        return linkName;
    }

    /**
     * @param linkName the linkName to set
     */
    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    /**
     * @return the linkUrl
     */
    public String getLinkUrl() {
        return linkUrl;
    }

    /**
     * @param linkUrl the linkUrl to set
     */
    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    /**
     * @return the linkDest
     */
    public String getLinkDest() {
        return linkDest;
    }

    /**
     * @param linkDest the linkDest to set
     */
    public void setLinkDest(String linkDest) {
        this.linkDest = linkDest;
    }

    /**
     * @return the menuImageId
     */
    public Integer getMenuImageId() {
        return menuImageId;
    }

    /**
     * @param menuImageId the menuImageId to set
     */
    public void setMenuImageId(Integer menuImageId) {
        this.menuImageId = menuImageId;
    }

    /**
     * @return the accessLevel
     */
    public Integer getAccessLevel() {
        return accessLevel;
    }

    /**
     * @param accessLevel the accessLevel to set
     */
    public void setAccessLevel(Integer accessLevel) {
        this.accessLevel = accessLevel;
    }

    /**
     * @return the adminOnly
     */
    public Boolean getAdminOnly() {
        return adminOnly;
    }

    /**
     * @param adminOnly the adminOnly to set
     */
    public void setAdminOnly(Boolean adminOnly) {
        this.adminOnly = adminOnly;
    }

    /**
     * @return the subCategory
     */
    public Integer getSubCategory() {
        return subCategory;
    }

    /**
     * @param subCategory the subCategory to set
     */
    public void setSubCategory(Integer subCategory) {
        this.subCategory = subCategory;
    }
}
