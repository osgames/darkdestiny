    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "hangarrelation")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class HangarRelation extends Model<HangarRelation> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("fleetId")
    @IndexAnnotation(indexName = "fleetId")
    private Integer fleetId;
    @FieldMappingAnnotation("designId")
    private Integer designId;
    @FieldMappingAnnotation("count")
    private Integer count;
    @FieldMappingAnnotation("relationToDesign")
    private Integer relationToDesign;
    @FieldMappingAnnotation("relationToSF")
    private Integer relationToSF;

    public Integer getFleetId() {
        return fleetId;
    }

    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    public Integer getDesignId() {
        return designId;
    }

    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getRelationToDesign() {
        return relationToDesign;
    }

    public void setRelationToDesign(Integer relationToDesign) {
        this.relationToDesign = relationToDesign;
    }

    public Integer getRelationToSF() {
        return relationToSF;
    }

    public void setRelationToSF(Integer relationToSF) {
        this.relationToSF = relationToSF;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
