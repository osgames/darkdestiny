/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.admin.module.AttributeType;
import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EBonusRange;
import at.viswars.enumeration.EBonusType;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "bonus")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Bonus extends Model<Bonus> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("description")
    private String description;
    @FieldMappingAnnotation("value")
    private Double value;
    @FieldMappingAnnotation("valueType")
    private AttributeType valueType;
    @FieldMappingAnnotation("bonusType")
    private EBonusType bonusType;
    @FieldMappingAnnotation("duration")
    private Integer duration;
    @FieldMappingAnnotation("bonusRange")
    private EBonusRange bonusRange;
    @FieldMappingAnnotation("cost")
    private Long cost;
    @FieldMappingAnnotation("reqResearch")
    private Integer reqResearch;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the value
     */
    public Double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * @return the valueType
     */
    public AttributeType getValueType() {
        return valueType;
    }

    /**
     * @param valueType the valueType to set
     */
    public void setValueType(AttributeType valueType) {
        this.valueType = valueType;
    }

    /**
     * @return the bonusType
     */
    public EBonusType getBonusType() {
        return bonusType;
    }

    /**
     * @param bonusType the bonusType to set
     */
    public void setBonusType(EBonusType bonusType) {
        this.bonusType = bonusType;
    }

    /**
     * @return the duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * @return the cost
     */
    public Long getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(Long cost) {
        this.cost = cost;
    }

    /**
     * @return the reqResearch
     */
    public Integer getReqResearch() {
        return reqResearch;
    }

    /**
     * @param reqResearch the reqResearch to set
     */
    public void setReqResearch(Integer reqResearch) {
        this.reqResearch = reqResearch;
    }

    /**
     * @return the bonusRange
     */
    public EBonusRange getBonusRange() {
        return bonusRange;
    }

    /**
     * @param bonusRange the bonusRange to set
     */
    public void setBonusRange(EBonusRange bonusRange) {
        this.bonusRange = bonusRange;
    }
}
