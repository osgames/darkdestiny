/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "statistic")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Statistic extends Model<Statistic> {

    @FieldMappingAnnotation("userid")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("rank")
    private Integer rank;
    @FieldMappingAnnotation("lastrank")
    private Integer lastRank;
    @FieldMappingAnnotation("userName")
    private String userName;
    @FieldMappingAnnotation("points")
    private Integer points;

    /**
     * @return the rank
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    /**
     * @return the lastRank
     */
    public Integer getLastRank() {
        return lastRank;
    }

    /**
     * @param lastRank the lastRank to set
     */
    public void setLastRank(Integer lastRank) {
        this.lastRank = lastRank;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(Integer points) {
        this.points = points;
    }
}
