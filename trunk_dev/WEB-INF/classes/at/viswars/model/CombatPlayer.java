/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.ECombatReportStatus;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "combatplayer")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class CombatPlayer extends Model<CombatPlayer> {

    @FieldMappingAnnotation("combatId")
    @IdFieldAnnotation
    private Integer combatId;
    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("startTime")
    private Long startTime;
    @FieldMappingAnnotation("reportStatus")
    private ECombatReportStatus reportStatus;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the combatId
     */
    public Integer getCombatId() {
        return combatId;
    }

    /**
     * @param combatId the combatId to set
     */
    public void setCombatId(Integer combatId) {
        this.combatId = combatId;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the reportStatus
     */
    public ECombatReportStatus getReportStatus() {
        return reportStatus;
    }

    /**
     * @param reportStatus the reportStatus to set
     */
    public void setReportStatus(ECombatReportStatus reportStatus) {
        this.reportStatus = reportStatus;
    }
}
