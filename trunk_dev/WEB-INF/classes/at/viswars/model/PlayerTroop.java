/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "playertroops")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlayerTroop extends Model<PlayerTroop> implements DataChangeAware {

    @FieldMappingAnnotation("planetId")
    @IdFieldAnnotation
    private Integer planetId;
    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("troopId")
    @IdFieldAnnotation
    private Integer troopId;
    @FieldMappingAnnotation("number")
    private Long number;
    private boolean modified = false;

    public PlayerTroop() {
    }

    public PlayerTroop(int planetId, int userId, int troopId, long number) {
        this.planetId = planetId;
        this.userId = userId;
        this.troopId = troopId;
        this.number = number;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the troopId
     */
    public Integer getTroopId() {
        return troopId;
    }

    /**
     * @param troopId the troopId to set
     */
    public void setTroopId(Integer troopId) {
        this.troopId = troopId;
    }

    /**
     * @return the number
     */
    public Long getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Long number) {
        if ((this.number != null) && !this.number.equals(number)) {
            modified = true;
        }
        this.number = number;
    }

    /**
     * @return the modified
     */
    public boolean isModified() {
        return modified;
    }
}
