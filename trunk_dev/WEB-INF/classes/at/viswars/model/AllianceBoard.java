/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.StringType;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.EAllianceBoardType;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "allianceboard")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class AllianceBoard extends Model<AllianceBoard> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @IndexAnnotation(indexName = "allianceId")
    @FieldMappingAnnotation("allianceId")
    private Integer allianceId;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("description")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String description;
    @FieldMappingAnnotation("type")
    private EAllianceBoardType type;
    public static final int ALLIANCE_BOARD_ID = -1;
    public static final int TREATY_BOARD_ID = -2;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the allianceId
     */
    public Integer getAllianceId() {
        return allianceId;
    }

    /**
     * @param allianceId the allianceId to set
     */
    public void setAllianceId(Integer allianceId) {
        this.allianceId = allianceId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the type
     */
    public EAllianceBoardType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EAllianceBoardType type) {
        this.type = type;
    }
}
