/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.model;

/**
 *
 * @author Eobane
 */
public interface ITechFunctions{

     abstract int getTechId();
     abstract String getTechName();
     abstract String getTechDescription();

}
