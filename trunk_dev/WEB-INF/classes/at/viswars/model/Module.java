/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.ModuleType;
import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "module")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Module extends Model<Module> implements ITechFunctions {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("type")
    private ModuleType type;
    @FieldMappingAnnotation("isChassis")
    private Boolean isChassis;
    @FieldMappingAnnotation("uniquePerShip")
    private Boolean uniquePerShip;
    @FieldMappingAnnotation("freeForDesign")
    private Boolean freeForDesign;
    @FieldMappingAnnotation("uniquePerType")
    private Boolean uniquePerType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModuleType getType() {
        return type;
    }

    public void setType(ModuleType type) {
        this.type = type;
    }

    public Boolean getIsChassis() {
        return isChassis;
    }

    public void setIsChassis(Boolean isChassis) {
        this.isChassis = isChassis;
    }

    public Boolean getUniquePerShip() {
        return uniquePerShip;
    }

    public void setUniquePerShip(Boolean uniquePerShip) {
        this.uniquePerShip = uniquePerShip;
    }

    public Boolean getFreeForDesign() {
        return freeForDesign;
    }

    public void setFreeForDesign(Boolean freeForDesign) {
        this.freeForDesign = freeForDesign;
    }

    public Boolean getUniquePerType() {
        return uniquePerType;
    }

    public void setUniquePerType(Boolean uniquePerType) {
        this.uniquePerType = uniquePerType;
    }

    public int getTechId() {
        return id;
    }

    public String getTechName() {
        return name;
    }

    public String getTechDescription() {
        return type.toString();
    }
}
