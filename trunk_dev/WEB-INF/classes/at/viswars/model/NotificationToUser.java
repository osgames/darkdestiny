/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "notificationtouser")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class NotificationToUser extends Model<NotificationToUser> {

    @FieldMappingAnnotation("notificationId")
    @IdFieldAnnotation
    private Integer notificationId;
    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("enabled")
    private Boolean enabled;
    @FieldMappingAnnotation("awayMode")
    private Boolean awayMode;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the awayMode
     */
    public Boolean getAwayMode() {
        return awayMode;
    }

    /**
     * @param awayMode the awayMode to set
     */
    public void setAwayMode(Boolean awayMode) {
        this.awayMode = awayMode;
    }

    /**
     * @return the notificationId
     */
    public Integer getNotificationId() {
        return notificationId;
    }

    /**
     * @param notificationId the notificationId to set
     */
    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }
}
