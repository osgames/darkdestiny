/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.service.Service;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "tradepost")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TradePost extends Model<TradePost> {

    public static final int TRADEPOST_CAPACITY = 200000;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @IndexAnnotation(indexName = "planetId")
    @FieldMappingAnnotation("planetId")
    private Integer planetId;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("transportPerLJ")
    private Integer transportPerLJ;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the transportPerLJ
     */
    public Integer getTransportPerLJ() {
        return transportPerLJ;
    }
    // Get best engine based capacity boost

    public double getCapBoost() {
        double capBoost = 1d;
        PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(planetId);
        if (pp != null) {
            PlayerResearch prPlasma = Service.playerResearchDAO.findBy(pp.getUserId(), 43); // Plasmaengine
            PlayerResearch prImpulse = Service.playerResearchDAO.findBy(pp.getUserId(), 80); // Impulseengine
            PlayerResearch prLinear = Service.playerResearchDAO.findBy(pp.getUserId(), 81); // Linear engine
            PlayerResearch prMetagrav = Service.playerResearchDAO.findBy(pp.getUserId(), 109); // Metagrav engine

            if (prPlasma != null) {
                capBoost = 1.5d;
            }
            if (prImpulse != null) {
                capBoost = 2d;
            }
            if (prLinear != null) {
                capBoost = 10d;
            }
            if (prMetagrav != null) {
                capBoost = 20d;
            }
        }
        return capBoost;
    }

    /**
     * @param transportPerLJ the transportPerLJ to set
     */
    public void setTransportPerLJ(Integer transportPerLJ) {
        this.transportPerLJ = transportPerLJ;
    }
}
