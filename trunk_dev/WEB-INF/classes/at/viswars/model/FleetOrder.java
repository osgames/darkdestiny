/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.enumeration.ELocationType;
import at.viswars.fleet.FleetType;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "fleetorders")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class FleetOrder extends Model<FleetOrder> {

    @FieldMappingAnnotation("fleetId")
    @IdFieldAnnotation
    private Integer fleetId;
    @FieldMappingAnnotation("fleetType")
    @IdFieldAnnotation
    private FleetType fleetType;
    @FieldMappingAnnotation("fleetorder")
    private Integer fleetOrder;
    @FieldMappingAnnotation("retreatFactor")
    private Integer retreatFactor;
    @FieldMappingAnnotation("retreatToType")
    private ELocationType retreatToType;
    @FieldMappingAnnotation("retreatTo")
    private Integer retreatTo;

    public Integer getFleetId() {
        return fleetId;
    }

    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    public Integer getFleetOrder() {
        return fleetOrder;
    }

    public void setFleetOrder(Integer fleetOrder) {
        this.fleetOrder = fleetOrder;
    }

    public Integer getRetreatFactor() {
        return retreatFactor;
    }

    public void setRetreatFactor(Integer retreatFactor) {
        this.retreatFactor = retreatFactor;
    }

    public ELocationType getRetreatToType() {
        return retreatToType;
    }

    public void setRetreatToType(ELocationType retreatToType) {
        this.retreatToType = retreatToType;
    }

    public Integer getRetreatTo() {
        return retreatTo;
    }

    public void setRetreatTo(Integer retreatTo) {
        this.retreatTo = retreatTo;
    }

    public FleetType getFleetType() {
        return fleetType;
    }

    public void setFleetType(FleetType fleetType) {
        this.fleetType = fleetType;
    }
}
