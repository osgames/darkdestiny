/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.DefaultValue;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.IndexAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.enumeration.ELocationType;
import at.viswars.interfaces.ILocation;
import at.viswars.spacecombat.ICombatParticipent;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "playerfleet")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlayerFleet extends Model<PlayerFleet> implements ICombatParticipent, ILocation {
    /*
     public static enum Status {
     STATIONARY, FLIGHT, DEFENSE, INUPDATE;
     }
     */

    public final static int STATIONARY = 0;
    public final static int INFLIGHT = 1;
    public final static int DEFENSE = 2;
    public final static int INUPDATE = 3;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @IndexAnnotation(indexName = "userId")
    @FieldMappingAnnotation("userId")
    private Integer userId;
    @IndexAnnotation(indexName = "coordinates")
    @FieldMappingAnnotation("planetId")
    private Integer planetId;
    @IndexAnnotation(indexName = "coordinates")
    @FieldMappingAnnotation("systemId")
    private Integer systemId;
    @IndexAnnotation(indexName = "location")
    @FieldMappingAnnotation("locationId")
    private Integer locationId;
    @IndexAnnotation(indexName = "location")
    @FieldMappingAnnotation("locationType")
    private ELocationType locationType;
    @FieldMappingAnnotation("status")
    @DefaultValue("0")
    private Integer status;
    @FieldMappingAnnotation("fleetFormationId")
    @DefaultValue("0")
    private Integer fleetFormationId;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the systemId
     */
    public Integer getSystemId() {
        return systemId;
    }

    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFleetFormationId() {
        return fleetFormationId;
    }

    public void setFleetFormationId(Integer fleetFormationId) {
        this.fleetFormationId = fleetFormationId;
    }

    /**
     * @return the locationId
     */
    public Integer getLocationId() {
        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(Integer locationId) {
        throw new RuntimeException("Call not allowed use setLocation instead");
    }

    /**
     * @return the locationType
     */
    public ELocationType getLocationType() {
        return locationType;
    }

    /**
     * @param locationType the locationType to set
     */
    public void setLocationType(ELocationType locationType) {
        throw new RuntimeException("Call not allowed use setLocation instead");
    }

    public void setLocation(ELocationType locationType, int locationId) {
        this.locationType = locationType;
        this.locationId = locationId;

        if (locationType == ELocationType.PLANET) {
            RelativeCoordinate rc = new RelativeCoordinate(0, locationId);
            systemId = rc.getSystemId();
            planetId = locationId;
            setStatus(STATIONARY);
        } else if (locationType == ELocationType.SYSTEM) {
            systemId = locationId;
            setStatus(STATIONARY);
        } else if (locationType == ELocationType.TRANSIT) {
            setStatus(INFLIGHT);
            planetId = 0;
            systemId = 0;
        } else {
            systemId = 0;
            planetId = 0;
            setStatus(STATIONARY);
        }
    }

    public void setLocation(ELocationType locationType) {
        if (locationType != ELocationType.TRANSIT) {
            throw new RuntimeException("Invalid Parameter " + locationType.name());
        }

        setLocation(locationType, 0);
    }

    public AbsoluteCoordinate getAbsoluteCoordinate() {
        if (locationType == ELocationType.SYSTEM) {
            return (new RelativeCoordinate(locationId, 0)).toAbsoluteCoordinate();
        } else if (locationType == ELocationType.PLANET) {
            return (new RelativeCoordinate(0, locationId)).toAbsoluteCoordinate();
        } else {
            throw new RuntimeException("Invalid LocationType");
        }
    }

    public boolean sameLocationAs(PlayerFleet pfComp) {
        if ((this.getLocationType() == pfComp.getLocationType()) && (this.getLocationId().equals(pfComp.getLocationId()))) {
            return true;
        }

        return false;
    }
}
