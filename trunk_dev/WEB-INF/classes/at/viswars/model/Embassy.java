/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.model;

import at.viswars.database.framework.annotations.ColumnProperties;
import at.viswars.database.framework.annotations.DataScope;
import at.viswars.database.framework.annotations.FieldMappingAnnotation;
import at.viswars.database.framework.annotations.IdFieldAnnotation;
import at.viswars.database.framework.annotations.TableNameAnnotation;

/**
 *
 * @author Dreloc
 */
@TableNameAnnotation(value = "embassy")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Embassy extends Model<Embassy> {

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("remUserId")
    @IdFieldAnnotation
    private Integer remUserId;
    @FieldMappingAnnotation("synced")
    private Boolean synced;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the remUserId
     */
    public Integer getRemUserId() {
        return remUserId;
    }

    /**
     * @param remUserId the remUserId to set
     */
    public void setRemUserId(Integer remUserId) {
        this.remUserId = remUserId;
    }

    /**
     * @return the synced
     */
    public Boolean isSynced() {
        return synced;
    }

    /**
     * @param synced the synced to set
     */
    public void setSynced(Boolean synced) {
        this.synced = synced;
    }
}
