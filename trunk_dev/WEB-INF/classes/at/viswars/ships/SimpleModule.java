/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

/**
 *
 * @author Dreloc
 */
public abstract class SimpleModule extends BuiltInModule {
    private int energyConsumption;

    public SimpleModule(ModInitParameter mip, int id) {
        super(mip,id);
    }

    /**
     * @return the energyConsumption
     */
    public int getEnergyConsumption() {
        return energyConsumption;
    }

    /**
     * @param energyConsumption the energyConsumption to set
     */
    public void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }
}
