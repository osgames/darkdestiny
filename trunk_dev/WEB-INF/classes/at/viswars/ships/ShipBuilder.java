/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ships;

import at.viswars.enumeration.EAttribute;
import at.viswars.DebugBuffer;
import at.viswars.Logger.Logger;
import at.viswars.ModuleType;
import at.viswars.admin.module.AttributeTree;
import at.viswars.admin.module.ModuleAttributeResult;
import at.viswars.dao.AttributeDAO;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DesignModuleDAO;
import at.viswars.dao.ModuleAttributeDAO;
import at.viswars.dao.ModuleDAO;
import at.viswars.dao.PlayerResearchDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.TechRelationDAO;
import at.viswars.model.Chassis;
import at.viswars.model.DesignModule;
import at.viswars.model.Module;
import at.viswars.model.ModuleAttribute;
import at.viswars.model.TechRelation;
import at.viswars.service.ResearchService;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class ShipBuilder {
    private static ModuleAttributeDAO maDAO = (ModuleAttributeDAO) DAOFactory.get(ModuleAttributeDAO.class);
    private static AttributeDAO aDAO = (AttributeDAO) DAOFactory.get(AttributeDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static DesignModuleDAO dmDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);
    private static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);  
    private static TechRelationDAO techDAO = (TechRelationDAO) DAOFactory.get(TechRelationDAO.class);
    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static ChassisDAO cDAO = (ChassisDAO)DAOFactory.get(ChassisDAO.class);

    private static boolean debug = false;

    protected static void createShipDesign(ShipDesignDetailed sd) {
        try {
        // Get chassis and user
        at.viswars.model.ShipDesign ship = sdDAO.findById(sd.getId());

        // Get 
        if (debug) Logger.getLogger().write("Find chassis for " + ship.getChassis());
        Chassis c = cDAO.findById(ship.getChassis());      
        if (debug) Logger.getLogger().write("Chassis is " + c);
        ShipChassis sc = sd.getBase();
        
        // Allow to override chassis of current design to be able to retrieve data for 
        // other chassis
        if (sd.getBase() == null) {
            sc = new ShipChassis(null, c.getRelModuleId()); 
        } else {
            if (debug) Logger.getLogger().write("2 Find chassis for " + sc.getId());
            c = cDAO.findByRelModuleId(sc.getId());
            if (debug) Logger.getLogger().write("2 Chassis is " + c);
        }
        
        // if (debug) Logger.getLogger().write("Loading ship with chassis " + sc.getId());

        sd.setName(ship.getName());
        sd.setUserId(sd.getUserId());
        sd.setBase(sc);
        
        if (sd.getAttributeTree() == null) {
            sd.buildAttributeTree();
        }

        // Complete Chassis
        if (debug) Logger.getLogger().write("C="+c);
        ModuleAttributeResult mar = sd.getAttributeTree().getAllAttributes(c.getId(), c.getRelModuleId());
        
        double space = mar.getAttributeValue(EAttribute.SPACE_PROVIDING);
        double structure = mar.getAttributeValue(EAttribute.HITPOINTS);
        
        sc.setSpace((int)space);
        sc.setStructure((int)structure);
        
        // if (debug) Logger.getLogger().write("space "+space+" structure "+structure+" for " + c.getId());
        
        // Get modules
        ArrayList<DesignModule> modules = dmDAO.findByDesignId(sd.getId());
        for (DesignModule dm : modules) {
            Module m = mDAO.findById(dm.getModuleId());                        

            if (m == null) {
                DebugBuffer.error("Invalid module "+dm.getModuleId()+" for design " + sd.getId());
                continue;
            }

            ModInitParameter mip = new ModInitParameter(ship.getChassis(),sd.getUserId(),sd.getDesignTime(),sd.getAttributeTree());
            
            if (m.getType() == ModuleType.WEAPON) { // WEAPON
                sd.addModule(new Weapon(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.ENGINE) { // ENGINE 
                sd.addModule(new Engine(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.ARMOR) { // ARMOR
                sd.addModule(new Armor(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.REACTOR) { // REACTOR
                sd.addModule(new Reactor(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.COLONISATION) { // COLONIZATION
                sd.addModule(new ColonisationModule(mip, dm.getModuleId()) , dm.getCount());
            } else if (m.getType() == ModuleType.TRANSPORT_POPULATION) {
                sd.addModule(new TroopTransport(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.TRANSPORT_RESSOURCE) {
                sd.addModule(new RessTransport(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.SHIELD) {
                sd.addModule(new Shield(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.HANGAR) {
                sd.addModule(new Hangar(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.COMPUTER) {
                sd.addModule(new Computer(mip, dm.getModuleId()), dm.getCount());
            } else if (m.getType() == ModuleType.SPECIAL) {
                sd.addModule(new SpecialModule(mip, dm.getModuleId()), dm.getCount());
            }
        }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in SDD: ", e);
        }
    }

    protected static void createModule(ShipModule sm) {
        // DebugBuffer.addLine(DebugLevel.DEBUG,"Looking for module Id " + sm.getId());
        Module m = mDAO.findById(sm.getId());

        sm.setName(m.getName());
        sm.setType(m.getType());
        
        if (sm instanceof Weapon) {
            loadWeapon((Weapon)sm);
        } else if (sm instanceof Reactor) {
            loadReactor((Reactor) sm);
        } else if (sm instanceof Armor) {
            loadArmor((Armor) sm);
        } else if (sm instanceof Engine) {
            loadEngine((Engine) sm);
        } else if (sm instanceof Computer) {
            loadComputer((Computer) sm);
        } else if (sm instanceof Shield) {
            loadShield((Shield) sm);
        } else if (sm instanceof SpecialModule) {
            loadSpecial((SpecialModule) sm);
        } else if (sm instanceof SimpleModule) {
            loadSimpleModule((SimpleModule) sm);        
        }
    }
    
    private static void loadSpecial(SpecialModule specm) {
        ModuleAttributeResult mar = specm.getBase().at.getAllAttributes(specm.getBase().chassisId, specm.getId());
        
        for (ModuleAttribute ma : mar.getAllSingleEntries()) {
            // if (debug) Logger.getLogger().write("Found entry for chassis: " + ma.getChassisId() + " and moduleId: " + ma.getModuleId() + " and type: " + ma.getAttributeType());

            try {
                at.viswars.model.Attribute a = aDAO.findById(ma.getAttributeId());
                EAttribute at = EAttribute.valueOf(a.getName());                
                
                if (at == EAttribute.DEFENSE_BONUS) {
                    specm.setDefenseBonus((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.TARGET_FIRE_EFF) {
                    specm.setDecreasedTargetFireEff((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                }
            } catch (Exception e) {
                if (debug) Logger.getLogger().write("ERROR IN LOAD SPECIAL: " + e);
                e.printStackTrace();
            }
        }              
    }
    
    private static void loadComputer(Computer c) {
        ModuleAttributeResult mar = c.getBase().at.getAllAttributes(c.getBase().chassisId, c.getId());
        
        for (ModuleAttribute ma : mar.getAllSingleEntries()) {
            // if (debug) Logger.getLogger().write("Found entry for chassis: " + ma.getChassisId() + " and moduleId: " + ma.getModuleId() + " and type: " + ma.getAttributeType());

            try {
                at.viswars.model.Attribute a = aDAO.findById(ma.getAttributeId());
                EAttribute at = EAttribute.valueOf(a.getName());                
                
                if (at == EAttribute.ATTACK_BONUS) {
                    c.setDamageBonus((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.TARGET_FIRE_EFF) {
                    c.setTargetFireAccBonus((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                }
            } catch (Exception e) {
                if (debug) Logger.getLogger().write("ERROR IN LOAD COMPUTER: " + e);
                e.printStackTrace();
            }
        }        
    }
    
    private static void loadWeapon(Weapon w) {
        // if (debug) Logger.getLogger().write("Load Weapon with id " + w.getId() + " for chassis " + w.getBase().chassisId);

        // ArrayList<ModuleAttribute> maList = maDAO.findByChassisAndModule(w.getBase().chassisId, w.getId());
        ModuleAttributeResult mar = w.getBase().at.getAllAttributes(w.getBase().chassisId, w.getId());
        
        for (ModuleAttribute ma : mar.getAllSingleEntries()) {
            // if (debug) Logger.getLogger().write("Found entry for chassis: " + ma.getChassisId() + " and moduleId: " + ma.getModuleId() + " and type: " + ma.getAttributeType());

            try {
                at.viswars.model.Attribute a = aDAO.findById(ma.getAttributeId());
                EAttribute at = EAttribute.valueOf(a.getName());                
                
                if (at == EAttribute.ENERGY_CONSUMPTION) {
                    w.setEnergyConsumption((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.SPACE_CONSUMPTION) {
                    w.setSpaceConsumption((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.RANGE) {
                    w.setRange((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.MULTIFIRE) {
                    w.setFireRate((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.ACCURACY) {
                    w.setAccuracy((float) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.ATTACK_STRENGTH) {
                    w.setAttackStrength((int) mar.getAttributeValue(ma.getAttributeId()));
                }
            } catch (Exception e) {
                if (debug) Logger.getLogger().write("ERROR IN LOAD WEAPON: " + e);
                e.printStackTrace();
            }
        }
    }

    private static void loadEngine(Engine e) {
        if (debug) Logger.getLogger().write("Load Engine with id " + e.getId() + " for chassis " + e.getBase().chassisId);
    }

    private static void loadArmor(Armor a) {
        if (debug) Logger.getLogger().write("Load Armor with id " + a.getId() + " for chassis " + a.getBase().chassisId);
    }

    private static void loadReactor(Reactor r) {
        if (debug) Logger.getLogger().write("Load Reactor with id " + r.getId() + " for chassis " + r.getBase().chassisId);
        ModuleAttributeResult mar = r.getBase().at.getAllAttributes(r.getBase().chassisId, r.getId());

        for (ModuleAttribute ma : mar.getAllSingleEntries()) {
            // if (debug) Logger.getLogger().write("Found entry for chassis: " + ma.getChassisId() + " and moduleId: " + ma.getModuleId() + " and type: " + ma.getAttributeType());

            try {
                at.viswars.model.Attribute a = aDAO.findById(ma.getAttributeId());
                EAttribute at = EAttribute.valueOf(a.getName());

                if (at == EAttribute.ENERGY_PRODUCTION) {
                    r.setEnergyProduction((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                }
            } catch (Exception e) {
                if (debug) Logger.getLogger().write("ERROR IN LOAD SIMPLE: " + e);
                e.printStackTrace();
            }
        }
    }

    private static void loadShield(Shield s) {
        if (debug) Logger.getLogger().write("Load Shield with id " + s.getId() + " for chassis " + s.getBase().chassisId);
        ModuleAttributeResult mar = s.getBase().at.getAllAttributes(s.getBase().chassisId, s.getId());

        for (ModuleAttribute ma : mar.getAllSingleEntries()) {
            // if (debug) Logger.getLogger().write("Found entry for chassis: " + ma.getChassisId() + " and moduleId: " + ma.getModuleId() + " and type: " + ma.getAttributeType());

            try {
                at.viswars.model.Attribute a = aDAO.findById(ma.getAttributeId());
                EAttribute at = EAttribute.valueOf(a.getName());

                if (at == EAttribute.ENERGY_CONSUMPTION) {
                    s.setEnergyConsumption((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.SPACE_CONSUMPTION) {
                    s.setSpaceConsumption((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                } else if (at == EAttribute.DEFENSE_STRENGTH) {
                    s.setDefenseStrength((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                }
            } catch (Exception e) {
                if (debug) Logger.getLogger().write("ERROR IN LOAD SHIELD: " + e);
                e.printStackTrace();
            }
        }
    }

    private static void loadSimpleModule(SimpleModule sm) {
        if (debug) Logger.getLogger().write("Load Simple Module with id " + sm.getId() + " for chassis " + sm.getBase().chassisId);
        ModuleAttributeResult mar = sm.getBase().at.getAllAttributes(sm.getBase().chassisId, sm.getId());

        for (ModuleAttribute ma : mar.getAllSingleEntries()) {
            // if (debug) Logger.getLogger().write("Found entry for chassis: " + ma.getChassisId() + " and moduleId: " + ma.getModuleId() + " and type: " + ma.getAttributeType());

            try {
                at.viswars.model.Attribute a = aDAO.findById(ma.getAttributeId());
                EAttribute at = EAttribute.valueOf(a.getName());

                if (at == EAttribute.ENERGY_CONSUMPTION) {
                    sm.setEnergyConsumption((int) mar.getAttributeValue(ma.getAttributeId()));
                    // if (debug) Logger.getLogger().write("Set Attribute " + at + " to " + w.getBase().at.getAttributeValue(ma.getChassisId(), ma.getModuleId(), ma.getAttributeId()));
                }
            } catch (Exception e) {
                if (debug) Logger.getLogger().write("ERROR IN LOAD SIMPLE: " + e);
                e.printStackTrace();
            }
        }
    }

    public static ArrayList<ShipModule> getModulesForChassis(Chassis ch, int userId, long time) {
        ArrayList<ShipModule> sm = new ArrayList<ShipModule>();
        
        // if (debug) Logger.getLogger().write("CHASSIS ID: " + ch.getId());
        
        ArrayList<ModuleAttribute> maList = maDAO.findValidForChassis(ch.getId());

        HashSet<Integer> validModules = new HashSet<Integer>();
        HashSet<Integer> checkedModules = new HashSet<Integer>();
        
        AttributeTree at = new AttributeTree(userId,time);
        
        for (ModuleAttribute ma : maList) {                    
            if (checkedModules.contains(ma.getModuleId())) continue;
            if (ma.getModuleId() == ch.getRelModuleId()) continue;
                        
            // if (debug) Logger.getLogger().write("Checking module: " + ma.getModuleId());
            
            checkedModules.add(ma.getModuleId());
            
            boolean allowedByTech = false;
            
            if (ResearchService.isPossibleByTech(TechRelation.TREETYPE_MODULE, ma.getModuleId(), userId, 0)) {
                // if (debug) Logger.getLogger().write("ALLOWED BY TECH");
                allowedByTech = true;                            
            } else {
                // if (debug) Logger.getLogger().write("SKIPPED BY TECH");
            }
            
            if (allowedByTech) {
                if (at.contains(ch.getId(), ma.getModuleId())) {
                    validModules.add(ma.getModuleId());    
                }
            }            
        }                        
        
        for (Integer mId : validModules) {
            Module m = mDAO.findById(mId);
            
            ModInitParameter mip = new ModInitParameter(ch.getId(),userId,time,at);
            ShipModule actModule = null;
            
            if (m.getType() == ModuleType.WEAPON) {
                actModule = new Weapon(mip,m.getId());                
            } else if (m.getType() == ModuleType.REACTOR) {
                actModule = new Reactor(mip,m.getId());
            } else if (m.getType() == ModuleType.SHIELD) {
                actModule = new Shield(mip,m.getId());
            } else if (m.getType() == ModuleType.ARMOR) {
                actModule = new Armor(mip,m.getId());                
            } else if (m.getType() == ModuleType.ENGINE) {
                actModule = new Engine(mip,m.getId());
            } else if (m.getType() == ModuleType.COLONISATION) {
                actModule = new ColonisationModule(mip,m.getId());
            } else if (m.getType() == ModuleType.TRANSPORT_POPULATION) {
                actModule = new TroopTransport(mip, m.getId());
            } else if (m.getType() == ModuleType.TRANSPORT_RESSOURCE) {
                actModule = new RessTransport(mip, m.getId());
            } else if (m.getType() == ModuleType.HANGAR) {
                actModule = new Hangar(mip, m.getId());
            } else if (m.getType() == ModuleType.COMPUTER) {
                actModule = new Computer(mip, m.getId());
            } else if (m.getType() == ModuleType.SPECIAL) {
                actModule = new ShipModule(mip, m.getId());
            }
            
            if (actModule != null) sm.add(actModule);
        }
        
        return sm;
    }
}
