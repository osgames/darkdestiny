/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

import at.viswars.Logger.Logger;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.service.ConstructionService;

/**
 *
 * @author Stefan
 */
public class BuildTime {
    private int indPoints;
    private int kasPoints;
    private int modulePoints;
    private int dockPoints;
    private int orbDockPoints;

    public int getKasPoints() {
        return kasPoints;
    }

    public void setKasPoints(int kasPoints) {
        this.kasPoints = kasPoints;
    }

    public int getModulePoints() {
        return modulePoints;
    }

    public void setModulePoints(int modulePoints) {
        this.modulePoints = modulePoints;
    }

    public int getDockPoints() {
        return dockPoints;
    }

    public void setDockPoints(int dockPoints) {
        this.dockPoints = dockPoints;
    }

    public int getOrbDockPoints() {
        return orbDockPoints;
    }

    public void setOrbDockPoints(int orbDockPoints) {
        this.orbDockPoints = orbDockPoints;
    }

    public BuildTime getAdjustedBuildTime(double multiplier) {
        BuildTime bt = new BuildTime();

        bt.setIndPoints((int)Math.ceil(indPoints * multiplier));
        bt.setKasPoints((int)Math.ceil(kasPoints * multiplier));
        bt.setModulePoints((int)Math.ceil(modulePoints * multiplier));
        bt.setDockPoints((int)Math.ceil(dockPoints * multiplier));
        bt.setOrbDockPoints((int)Math.ceil(orbDockPoints * multiplier));

        return bt;
    }

    public void adjustBuildTime(double multiplier) {
        indPoints = (int)Math.ceil(indPoints * multiplier);
        kasPoints = (int)Math.ceil(kasPoints * multiplier);
        modulePoints = (int)Math.ceil(modulePoints * multiplier);
        dockPoints = (int)Math.ceil(dockPoints * multiplier);
        orbDockPoints = (int)Math.ceil(orbDockPoints * multiplier);
    }

    public BuildTime getAdjustedBuildTime(double indMultiplier, double kasMultiplier, double moduleMultiplier, double dockMultiplier, double orbDockMultiplier) {
        BuildTime bt = new BuildTime();

        bt.setIndPoints((int)Math.ceil(indPoints * indMultiplier));
        bt.setKasPoints((int)Math.ceil(kasPoints * kasMultiplier));
        bt.setModulePoints((int)Math.ceil(modulePoints * moduleMultiplier));
        bt.setDockPoints((int)Math.ceil(dockPoints * dockMultiplier));
        bt.setOrbDockPoints((int)Math.ceil(orbDockPoints * orbDockMultiplier));

        return bt;
    }

    public double getTickTimeOnPlanet(int planetId) {
        try {
            PlanetCalculation pc = new PlanetCalculation(planetId);
            ProductionResult pr = pc.getPlanetCalcData().getProductionData();

            int indPoints = ConstructionService.getIndustryPoints(planetId);
            int modulePoints = (int)pr.getRessProduction(at.viswars.model.Ressource.MODUL_AP);
            int dockPoints = (int)pr.getRessProduction(at.viswars.model.Ressource.PL_DOCK);
            int orbDockPoints = (int)pr.getRessProduction(at.viswars.model.Ressource.ORB_DOCK);
            int kasPoints = (int)pr.getRessProduction(at.viswars.model.Ressource.KAS);

            double buildTime = Double.MIN_VALUE;        

            if (indPoints > 0) buildTime = Math.max(0,((double)getIndPoints() / (double)indPoints));
            if (dockPoints > 0) buildTime = Math.max(0,((double)getDockPoints() / (double)dockPoints));
            if (orbDockPoints > 0) buildTime = Math.max(buildTime,((double)getOrbDockPoints() / (double)orbDockPoints));
            if (kasPoints > 0) buildTime = Math.max(buildTime,((double)getKasPoints() / (double)kasPoints));
            if (modulePoints > 0) buildTime = Math.max(buildTime,((double)getModulePoints() / (double)modulePoints));
            if ((indPoints == 0) && (getIndPoints() > 0)) buildTime = Double.MAX_VALUE;
            if ((orbDockPoints == 0) && (getOrbDockPoints() > 0)) buildTime = Double.MAX_VALUE;
            if ((dockPoints == 0) && (getDockPoints() > 0)) buildTime = Double.MAX_VALUE;
            if ((modulePoints == 0) && (getModulePoints() > 0)) buildTime = Double.MAX_VALUE;        
            if ((kasPoints == 0) && (getKasPoints() > 0)) buildTime = Double.MAX_VALUE;                  

            Logger.getLogger().write("returning : " + buildTime);
            return buildTime;
        } catch (Exception e) {
            Logger.getLogger().write("E : " + e);
            return Double.MIN_VALUE;
        }
    }

    /**
     * @return the indPoints
     */
    public int getIndPoints() {
        return indPoints;
    }

    /**
     * @param indPoints the indPoints to set
     */
    public void setIndPoints(int indPoints) {
        this.indPoints = indPoints;
    }
}
