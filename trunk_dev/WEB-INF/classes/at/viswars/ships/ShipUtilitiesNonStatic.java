/*
 * ShipUtilitiesNonStatic.java
 *
 * Created on 24. Februar 2008, 12:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.viswars.ships;

import at.viswars.DebugBuffer;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.construction.ModuleEntry;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.SystemDAO;
import at.viswars.database.access.DbConnect;
import at.viswars.databuffer.RessourceCostBuffer;
import at.viswars.enumeration.ELocationType;
import at.viswars.fleet.Movable;
import at.viswars.model.ShipDesign;
import at.viswars.movable.FleetFormationExt;
import at.viswars.movable.PlayerFleetExt;
import at.viswars.utilities.ResearchUtilities;

/**
 *
 * @author Stefan
 */
public class ShipUtilitiesNonStatic extends ShipUtilities {
    public static SystemDAO systemDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);

    private final int userId;
    private final int planetId;
    private final boolean initialized;

    /** Creates a new instance of ShipUtilitiesNonStatic */
    public ShipUtilitiesNonStatic(int userId, int planetId) {
        this.userId = userId;
        this.planetId = planetId;

        initialized = isInitialized();
    }

    private boolean isInitialized() {
        return ((userId != 0) && (planetId != 0));
    }

    public void changeChassis(int designId, int chassisId) {
        if (!initialized) return;

        try {
            if (!ResearchUtilities.isModuleResearched(chassisId,userId)) {
                outMsg = "Dieses Chassis steht nicht zur Verf?gung";
                return;
            }

            String errMsg = checkForBuiltShips(designId);
            if (!errMsg.equalsIgnoreCase("")) {
                outMsg = errMsg;
                return;
            }

            deleteModules(designId);
            /*
            DesignData dd = FleetBuffer2.getDesignById(designId);
            dd.setChassis(chassisId);
            dd.saveData();
             *
             */

        ShipDesign dd = shipDesignDAO.findById(designId);
        dd.setChassis(chassisId);
        shipDesignDAO.update(dd);

        } catch (Exception e) {
            outMsg = e.getMessage();
            DebugBuffer.writeStackTrace("Error in ShipUtilities: ", e);
        }
    }

    // Following extension have to be done for this function
    // Space check - Evaluate space for chassis and reduce by all modules
    // Speed calculation - conventional drives and hyper-drives have to be considered and saved in shipdesign table

    public void saveDesign(Map<String, String[]> allPars, int[] relListID) {
        if (!initialized) return;

        String errMsg = "";

        boolean engineFound = false;
        boolean energySource = false;
        boolean energyLackage = false;
        boolean spaceError = false;
        boolean relModuleError = false;
        boolean starBaseError = false;
        boolean starBaseError2 = false;
        boolean colonisationError = false;

        boolean colModulFound = false;
        boolean isStarbase = false;

        String moduleErrorMessage = "";

        float shipSpace = 0;
        float shipSpaceOrg = 0;
        int chassisId = 0;
        float engineFactor = 0;
        //float hyperFactor = 0;
        float propulsionFactor = 0;
        //float creditFactor = 0;
        int isConstruct = 0;
        int shipType = 1;
        ShipCapacity shipCap = new ShipCapacity();

        float speed = 0;
        float hyperspeed = 0;

        int designId = Integer.parseInt(((String[])allPars.get("currentDesign"))[0]);
        ResearchUtilities ru = new ResearchUtilities();

        try {
            String warnMessage = checkForBuiltShips(designId);

            if (!warnMessage.equalsIgnoreCase("")) {
                outMsg = warnMessage;
                return;
            }

            deleteModules(designId);
            Statement chStmt = DbConnect.createStatement();
            ResultSet chRs = chStmt.executeQuery("SELECT sd.chassis, m.space, sr.engineFactor, sr.propulsionFactor, sr.hyperFactor, sr.creditFactor, m.isConstruct FROM module m, shipdesigns sd, sizerelation sr WHERE sd.id="+designId+" AND m.id=sd.chassis AND sr.chassisId=sd.chassis");

            if (chRs.next()) {
                //Earlier public boolean isResearched(int unitId, int type, int userId, int planetId)
                if (!ResearchUtilities.isModuleResearched(chRs.getInt(1),userId)) {
                    outMsg = "Dieses Chassis steht nicht zur Verf?gung";
                    return;
                }

                chassisId = chRs.getInt(1);
                shipSpace = chRs.getFloat(2);
                shipSpaceOrg = shipSpace;
                engineFactor = chRs.getFloat(3);
                propulsionFactor = chRs.getFloat(4);
                //hyperFactor = chRs.getFloat(5);
                //creditFactor = chRs.getFloat(6);
                isConstruct = chRs.getInt(7);

                DebugBuffer.addLine(DebugLevel.TRACE,"Try to save shipdesign " + designId + " with maximum space " + shipSpaceOrg);

                if ((chassisId == 325) || (chassisId == 330)) isStarbase = true;
            }
            chStmt.close();
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.DEBUG,"Error while determing Ship chassis" + e);
        }

        // DebugBuffer.addLine(DebugLevel.DEBUG,"ShipSpace " + shipSpace);
        int minBaseEnergy = 0;
        int availEnergy = 0;

        for(String parName : allPars.keySet()) {
            // DebugBuffer.addLine(DebugLevel.DEBUG,"Parameter Name: " + parName);

            if (parName.startsWith("module")) {
                //String[] moduleStr = allPars.get(parName);
                // int moduleId = relListID[Integer.parseInt(moduleStr[0])];
                String tmpId = parName.substring(6);

                // DebugBuffer.addLine(DebugLevel.DEBUG,"Temp Id " + tmpId + " gefunden!");
                int moduleId = 0;

                try {
                    moduleId = Integer.parseInt(allPars.get("modulid"+tmpId)[0]);
                } catch (NumberFormatException e) {
                    continue;
                }

                // DebugBuffer.addLine(DebugLevel.DEBUG,"Zugeh?rige modulID="+moduleId);
                int count = Integer.parseInt(allPars.get("number"+tmpId)[0]);
                // DebugBuffer.addLine(DebugLevel.DEBUG,"Count="+count);

                if (count > 0) {
                    try {

                //Earlier public boolean isResearched(int unitId, int type, int userId, int planetId)
                        if (!ResearchUtilities.isModuleResearched(moduleId,userId)) {
                            outMsg = "Ung?ltige Module im Schiff!";
                            return;
                        }

                        // Check Module
                        ModuleEntry me = getModuleData(moduleId,chassisId);

                        // Logger.getLogger().write("SPACE CONSUMPTION FOR " + me.getName() + " = " + me.getSpace());

                        int tmpEnergy = me.getEnergy();
                        int tmpSpace = me.getSpace();
                        int tmpValue = me.getValue();

                        DebugBuffer.addLine(DebugLevel.TRACE,"Space consumption of  " + me.getSpace() + " for module " + me.getName());

                        if (moduleId == 1000) {
                            colModulFound = true;
                        }

                        switch (me.getType()) {
                            case(1):
                                minBaseEnergy += (int)((float)tmpEnergy * (float)engineFactor);
                            hyperspeed = tmpValue;
                                break;
                            case(10):
                                energySource = true;

                                availEnergy += tmpEnergy * count;
                                break;
                            case(4):
                                engineFound = true;
                                // DebugBuffer.addLine(DebugLevel.DEBUG,"Speed="+(rs.getInt(3))+"*"+engineFactor+"/"+shipSpaceOrg);
                                speed = ((float)tmpValue * ((float)engineFactor) / (float)100 * (float)propulsionFactor) / (float)shipSpaceOrg;
                                minBaseEnergy += (int)((float)tmpEnergy * (float)engineFactor);
                                if (moduleId == META_GRAV) {
                                    hyperspeed = 4;
                                } else if (moduleId == CHEMICAL) {
                                    if (hyperspeed < 0.1f) {
                                        hyperspeed = 0.1f;
                                    }
                                } else if (moduleId == PLASMA) {
                                    if (hyperspeed < 0.15f) {
                                        hyperspeed = 0.15f;
                                    }
                                } else if (moduleId == IMPULS) {
                                    if (hyperspeed < 0.2f) {
                                        hyperspeed = 0.2f;
                                    }
                                }

                                break;
                            case(11):
                                minBaseEnergy += tmpEnergy * count;
                                if (moduleId == 1010) {
                                    shipCap.troopSpace += tmpSpace*count;
                                } else if (moduleId == 1020) {
                                    shipCap.ressSpace += tmpSpace*count;
                                }
                                break;
                            case(12):
                                DebugBuffer.addLine(DebugLevel.DEBUG,"HANGAR FOUND");
                                minBaseEnergy += tmpEnergy * count;
                                if (moduleId == 1030) {
                                    shipCap.smallHangarCap += tmpValue * count;
                                    DebugBuffer.addLine(DebugLevel.DEBUG,"SMALLHANGER="+shipCap.smallHangarCap);
                                } else if (moduleId == 1031) {
                                    shipCap.mediumHangarCap += tmpValue * count;
                                    DebugBuffer.addLine(DebugLevel.DEBUG,"MEDIUMHANGER="+shipCap.mediumHangarCap);
                                } else if (moduleId == 1032) {
                                    shipCap.largeHangarCap += tmpValue * count;
                                    DebugBuffer.addLine(DebugLevel.DEBUG,"LARGEHANGER="+shipCap.largeHangarCap);
                                }
                                break;
                        }

                        switch(me.getType()) {
                            case(1):
                                shipSpace -= tmpSpace;
                                DebugBuffer.addLine(DebugLevel.TRACE,"Reducing shipspace with hyperengine value of " + me.getSpace() + " by " + me.getSpace() + " to " + shipSpace);
                                // Logger.getLogger().write("Decrease Ship Space for HyperEngine by " + (tmpSpace * hyperFactor)+ " to " + shipSpace);
                                break;
                            case(4):
                                shipSpace -= tmpSpace;
                                DebugBuffer.addLine(DebugLevel.TRACE,"Reducing shipspace with engine value of " + me.getSpace() + " by " + me.getSpace() + " to " + shipSpace);
                                // Logger.getLogger().write("Decrease Ship Space for Engine by " + (tmpSpace * engineFactor)+ " to " + shipSpace);
                                break;
                            default:
                                shipSpace -= tmpSpace * count;
                                DebugBuffer.addLine(DebugLevel.TRACE,"Reducing shipspace with module value of " + me.getSpace() + " by " + (me.getSpace() * count) + " to " + shipSpace);
                                // Logger.getLogger().write("Decrease Ship Space for General by " + (tmpSpace * count)+ " to " + shipSpace);
                                break;
                        }

                        Statement stmt = DbConnect.createStatement();
                        stmt.executeUpdate("insert into designmodule set designId="+designId+", moduleId="+moduleId+", count="+count);
                        stmt.close();
                    } catch (Exception e) {
                        DebugBuffer.addLine(DebugLevel.DEBUG,"Error in ShipUtilities -- save Design: " + e);
                    }
                }
            } else if (parName.equals("primary")) {
                try {
                    Statement stmt = DbConnect.createStatement();
                    stmt.executeUpdate("Update shipdesigns set primaryTarget="+Integer.parseInt(((String[])allPars.get("primary"))[0])+" where id="+designId);
                    stmt.close();
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.DEBUG,"error in saveDesign(primary): " + e);
                }
            } else if (parName.equals("purpose")) {
                try {
                    shipType = Integer.parseInt(((String[])allPars.get("purpose"))[0]);

                    Statement stmt = DbConnect.createStatement();
                    stmt.executeUpdate("Update shipdesigns set type="+Integer.parseInt(((String[])allPars.get("purpose"))[0])+" where id="+designId);
                    stmt.close();
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.DEBUG,"error in saveDesign(purpose): " + e);
                }
            }
        }

        // DebugBuffer.addLine(DebugLevel.DEBUG,"Design ID: " + designId);
        // ShipRestructuringRessourceCost shipCosts = getShipCost(designId);

        try {
            int buildable = 0;

            // DebugBuffer.addLine(DebugLevel.DEBUG,"ShipSpace2 " + shipSpace);

            // Check ShipSpace
            if (shipSpace < 0) {
                spaceError = true;
            }

            // Check energy support
            // DebugBuffer.addLine(DebugLevel.DEBUG,"MinBase="+minBaseEnergy+" availEnergy="+availEnergy);
            if (minBaseEnergy > availEnergy) {
                energyLackage = true;
            }

            // Check for invalid types related to modules for selected type
            // Colonisation Module forces type Colonisation Ship
            if ((shipType != 4) && colModulFound) {
                colonisationError = true;
            }

            // Starbase chassis forces Starbase Type
            if ((shipType != 5) && isStarbase) {
                starBaseError = true;
            }

            // Ship may not have Type Starbase
            if ((shipType == 5) && !isStarbase) {
                starBaseError2 = true;
            }

            // Check for missing modules
            moduleErrorMessage = checkForMissingModule(designId);
            if (!moduleErrorMessage.equalsIgnoreCase("")) {
                relModuleError = true;
            }

            // Check for engine and energy
            if (isConstruct == 1) { engineFound = true; }
            if (engineFound && energySource && !spaceError && !relModuleError && !energyLackage) {
                buildable = 1;
            }

            // Read ajustment values for this ship design and adjust values
            Statement stmt = DbConnect.createStatement();

            // adjustToShipType(shipCosts,shipCap,shipType);
            // RessourceCostBuffer.setRessourceCost(RessourceCostBuffer.COST_SHIP, designId, shipCosts.getRessArray());
            /*
            stmt.executeUpdate("Update shipdesigns set credits="+shipCosts.getCredits()+", buildable="+buildable+", standardspeed="+speed+
                    ", troopSpace="+shipCap.troopSpace+", ressSpace="+shipCap.ressSpace+", isConstruct="+isConstruct+", buildtime=3, hyperspeed="+hyperspeed+
                    ", smallHangar="+shipCap.smallHangarCap+", mediumHangar="+shipCap.mediumHangarCap+", largeHangar="+shipCap.largeHangarCap+" WHERE id="+designId);
            */
            stmt.close();

            RessourceCostBuffer.storeEntry(RessourceCostBuffer.COST_SHIP, designId);
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.DEBUG,"Error in ShipUtilities -- save Design(2): " + e);
        }

        if (!engineFound) {
            errMsg = "Kein Antrieb vorhanden";
        }

        if (!energySource) {
            if (!errMsg.equalsIgnoreCase("")) {
                errMsg += "<BR>Keine Energiequelle vorhanden";
            } else {
                errMsg = "Keine Energiequelle vorhanden";
            }
        } else if (energyLackage) {
            if (!errMsg.equalsIgnoreCase("")) {
                errMsg += "<BR>Unzureichende Energieversorgung";
            } else {
                errMsg = "Unzureichende Energieversorgung";
            }
        }

        if (spaceError) {
            if (!errMsg.equalsIgnoreCase("")) {
                errMsg += "<BR>Zuwenig Platz f?r die eingestellten Module";
            } else {
                errMsg = "Zuwenig Platz f?r die eingestellten Module";
            }
        }

        if (relModuleError) {
            if (!errMsg.equalsIgnoreCase("")) {
                errMsg += "<BR>" + moduleErrorMessage;
            } else {
                errMsg = moduleErrorMessage;
            }
        }

        if (starBaseError) {
            if (!errMsg.equalsIgnoreCase("")) {
                errMsg += "<BR>Raumstationen m?ssen Typ Raumbasis haben";
            } else {
                errMsg = "Raumstationen m?ssen Typ Raumbasis haben";
            }
        }

        if (starBaseError2) {
            if (!errMsg.equalsIgnoreCase("")) {
                errMsg += "<BR>Typ Raumbasis ist nicht erlaubt f?r Raumschiffe";
            } else {
                errMsg = "Typ Raumbasis ist nicht erlaubt f?r Raumschiffe";
            }
        }

        if (colonisationError) {
            if (!errMsg.equalsIgnoreCase("")) {
                errMsg += "<BR>Kolonisationsmodul kann nur auf Schiffe mit Typ Kolonieschiff gebaut werden!";
            } else {
                errMsg = "Kolonisationsmodul kann nur auf Schiffe mit Typ Kolonieschiff gebaut werden!";
            }
        }

        if (!errMsg.equalsIgnoreCase("")) {
            errMsg += "<BR>Schiff wird nicht in Bauliste erscheinen.";
        }

        outMsg = errMsg;
    }

    public void checkFleetOrders(int fleetId, boolean isFF, at.viswars.model.FleetOrder fo) {
        try {
            int compareUser = 0;

            if (isFF) {
                FleetFormationExt ffe = new FleetFormationExt(fleetId);
                compareUser = ffe.getUserId();
            } else {
                PlayerFleetExt pfe = new PlayerFleetExt(fleetId);
                compareUser = pfe.getUserId();
            }

            // FleetData fd = new FleetData(fleetId);
            if (compareUser != userId) {
                outMsg = "1Keine Berechtigung";
                return;
            }

            if ((fo.getRetreatFactor() < 40) || (fo.getRetreatFactor() > 100) || ((fo.getRetreatFactor() % 10) != 0)) {
                outMsg = "1Ung�ltiger R�ckzugsfaktor";
                return;
            }

            if ((fo.getRetreatToType() == null) && (fo.getRetreatFactor() < 100)) {
                outMsg = "1Bitte R�ckzugsort angeben";
                return;
            }

            if (fo.getRetreatFactor() == 100) return;
            if (fo.getRetreatToType() != null) {
                if (fo.getRetreatToType() == ELocationType.SYSTEM) { // SYSTEM
                    at.viswars.model.System s = systemDAO.findById(fo.getRetreatTo());
                    if (s == null) {
                        outMsg = "1Ung�ltiges R�ckzugssystem";
                        return;
                    }
                } else { // PLANET
                    Statement stmt = DbConnect.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT id FROM planet WHERE id="+fo.getRetreatTo());

                    boolean found = false;

                    if (rs.next()) {
                        found = true;
                    }

                    stmt.close();

                    if (!found) {
                        outMsg = "1Ung�ltiger R�ckzugsplanet";
                        return;
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.error("Error in checkFleetOrders: ",e);
            outMsg = "FEHLER";
        }
    }
}
