/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

import at.viswars.ModuleType;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ModuleDAO;

/**
 *
 * @author Stefan
 */
public class ShipModule {
    private final int id;
    private String name;
    private ModuleType type;
    private final ModInitParameter mip;

    ModuleDAO mDAO = (ModuleDAO)DAOFactory.get(ModuleDAO.class);
    
    public ShipModule(ModInitParameter mip, int id) {
        this.mip = mip;
        this.id = id;
        if (mDAO.findById(id) == null) {
            System.err.println("[ERROR] Module " + id + " does not exist!");
        } else {
            this.type = mDAO.findById(id).getType();
        }
        ShipBuilder.createModule(this);        
    }
    
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public ModInitParameter getBase() {
        return mip;
    }

    public ModuleType getType() {
        return type;
    }

    protected void setType(ModuleType type) {
        this.type = type;
    }
}
