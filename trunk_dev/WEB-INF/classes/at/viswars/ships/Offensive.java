/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

/**
 *
 * @author Stefan
 */
public interface Offensive {
    public double getPenetration(Defensive module, ShipDesignDetailed enemy);
}
