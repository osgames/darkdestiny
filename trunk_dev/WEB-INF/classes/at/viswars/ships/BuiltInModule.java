/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

/**
 *
 * @author Stefan
 */
public abstract class BuiltInModule extends ShipModule {
    private int spaceConsumption;

    public BuiltInModule(ModInitParameter mip, int id) {
        super(mip,id);
    }
    
    public int getSpaceConsumption() {
        return spaceConsumption;
    }

    public void setSpaceConsumption(int spaceConsumption) {
        this.spaceConsumption = spaceConsumption;
    }
}
