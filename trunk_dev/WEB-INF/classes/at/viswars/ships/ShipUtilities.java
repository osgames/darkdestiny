package at.viswars.ships;

import at.viswars.DebugBuffer;
import at.viswars.fleet.FleetData;
import java.sql.*;
import java.util.*;

import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.GameConstants;
import at.viswars.GameUtilities;
import at.viswars.database.access.DbConnect;
import at.viswars.databuffer.ConfirmBuffer;
import at.viswars.construction.*;
import at.viswars.model.Ressource;
import at.viswars.service.Service;
import at.viswars.model.ShipDesign;


import at.viswars.utilities.DiplomacyUtilities;
import javax.servlet.jsp.JspWriter;

public class ShipUtilities extends Service{

    public final static int CHEMICAL = 1;
    public final static int PLASMA = 80;
    public final static int IMPULS = 84;
    public final static int META_GRAV = 85;
    public String outMsg = null;

    public static void renameDesign(String newName, int designId) {
        if (newName.equalsIgnoreCase("")) {
            return;
        }
        ShipDesign dd = shipDesignDAO.findById(designId);
        dd.setName(newName);
        shipDesignDAO.update(dd);
    }

    public static void newDesign(String userId) {
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("select count(id) as count FROM shipdesigns WHERE userId=" + userId);
            if (rs.next()) {
                if (rs.getInt(1) < 50) {
                    stmt.execute("INSERT INTO shipdesigns (id, chassis, userId, name) VALUES (NULL, 300, " + userId + ", 'Neues Design')");
                }
            }
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities: ", e);
        }
    }


    public static String getChassisName(int designId) {
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("select module.name from module, shipdesigns where shipdesigns.id=" + designId + " and module.id=shipdesigns.chassis");
            if (rs.next()) {
                String returnValue = rs.getString(1);
                stmt.close();
                return returnValue;
            }
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities - getChassisName: ", e);
        }

        return "";
    }

    public static void deleteModules(int designId) {
        try {
            Statement stmt = DbConnect.createStatement();
            stmt.executeUpdate("delete from designmodule where designId=" + designId);
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities: ", e);
        }
    }

    public static String checkForMissingModule(int designId) {
        String errorMessage = "";

        try {
            Statement stmt = DbConnect.createStatement();
            Statement stmt2 = DbConnect.createStatement();
            Statement stmt3 = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("select dm.moduleId, m.name from designmodule dm, module m where dm.designId=" + designId + " AND m.id=dm.moduleId");
            while (rs.next()) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "Checking module " + rs.getString(2));
                ResultSet rs2 = stmt2.executeQuery("SELECT mr.requiredId, m.name FROM modulerelation mr, module m WHERE mr.sourceId=" + rs.getInt(1) + " AND m.id=mr.requiredId");
                if (rs2.next()) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Module " + rs.getString(2) + " seems to rely on " + rs2.getString(2));
                    ResultSet rs3 = stmt3.executeQuery("SELECT moduleId FROM designmodule WHERE moduleId=" + rs2.getInt(1) + " AND designId=" + designId);
                    if (!rs3.next()) {
                        errorMessage = "Modul " + rs.getString(2) + " ben�tigt eingebautes Modul " + rs2.getString(2);
                        stmt.close();
                        stmt2.close();
                        stmt3.close();
                        break;
                    } else {
                        DebugBuffer.addLine(DebugLevel.DEBUG, "Needed module " + rs2.getString(2) + " found");
                    }
                }
            }

            stmt.close();
            stmt2.close();
            stmt3.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error for missingModule=", e);
        }

        return errorMessage;
    }

    public static int[][] getAllModules(int designId) {
        int moduleArray[][] = new int[17][2];
        try {
            int i = 0;

            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("select moduleId, count from designmodule where designId=" + designId + " order by moduleId");
            while (rs.next()) {
                moduleArray[i][0] = rs.getInt(1);
                moduleArray[i][1] = rs.getInt(2);

                // DebugBuffer.addLine(DebugLevel.DEBUG,"Found module entered: " + moduleArray[i][0] + " -- " + moduleArray[i][1]);
                i++;
            }
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities -- getAllModules: ", e);
        }

        return moduleArray;
    }

    public static int[] readElementFromDesignArray(int currentDesign[][], int relListID[]) {
        int[] moduleData = new int[3];

        for (int i = 0; i < 17; i++) {
            for (int j = 0; j < 100; j++) {
                if ((currentDesign[i][0] == relListID[j]) && (currentDesign[i][0] != 0)) {

                    moduleData[0] = j;
                    moduleData[1] = currentDesign[i][1];
                    moduleData[2] = currentDesign[i][0];

                    currentDesign[i][0] = 0;

                    // DebugBuffer.addLine(DebugLevel.DEBUG,"ModuleData enter values [0]="+moduleData[0]+" [1]="+moduleData[1]+" [2]="+moduleData[2]);

                    return (moduleData);
                }
            }
        }
        return moduleData;
    }

    /*
    public static ShipRestructuringRessourceCost getShipScrapCost(int designId) {
        ShipRestructuringRessourceCost shipCost = getShipCost(designId);

        shipCost.multiplyRessources(0.7d,0.15d);

        return shipCost;
    }
    */

    /*
    public static ShipRestructuringRessourceCost getShipCost(int designId) {
        ShipRestructuringRessourceCost shipCost = new ShipRestructuringRessourceCost(designId);

        try {
            int chassisId = 0;

            Statement stmt = DbConnect.createStatement();
            Statement stmt2 = DbConnect.createStatement();

            // ResultSet rs = stmt.executeQuery("SELECT module.chassisSize, sizerelation.engineFactor, sizerelation.hyperFactor, sizerelation.creditFactor, sizerelation.hyperCrystalFactor, shipdesigns.chassis FROM shipdesigns, module, sizerelation WHERE module.id=shipdesigns.chassis AND shipdesigns.id="+designId+" AND sizerelation.chassisId=shipdesigns.chassis");
            ResultSet rs = stmt.executeQuery("SELECT shipdesigns.chassis FROM shipdesigns WHERE shipdesigns.id=" + designId);
            if (rs.next()) {
                /*
                chassisMP = rs.getInt(1);
                engineFactor = rs.getFloat(2);
                hyperFactor = rs.getFloat(3);
                creditFactor = rs.getFloat(4);
                hyperCrystalFactor = rs.getFloat(5);
                 
                chassisId = rs.getInt(1);
            // DebugBuffer.addLine(DebugLevel.DEBUG,"Chassis modifier " + chassisMP);
            }

            // DebugBuffer.addLine(DebugLevel.DEBUG,"Check: " + designId);
            rs.close();
            rs = stmt.executeQuery("SELECT dm.moduleId, dm.count, m.type, m.name FROM designmodule dm, module m WHERE dm.designId=" + designId + " AND m.id=dm.moduleId");
            while (rs.next()) {
                // DebugBuffer.addLine(DebugLevel.DEBUG,"Module found: " + rs.getInt(1));
                ModuleEntry md = getModuleData(rs.getInt(1), chassisId);
                shipCost.addRess(md, rs.getInt(2));
            }

            // Add costs for chassis
            rs = stmt.executeQuery("SELECT m.id FROM module m, shipdesigns sd WHERE sd.id=" + designId + " AND m.id=sd.chassis");
            if (rs.next()) {
                RessourcesEntry chassisCost = ConstructionUtilities.getConstructionCost(ERessourceCostType.MODULE, rs.getInt(1));
                shipCost.addRess(chassisCost, 1d);
            }

            stmt.close();
            stmt2.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities -- getShipCost: ", e);
        }

        return shipCost;
    }
    */
            
    public static int getQueueIdentStarBase(int userId, int planetId, int systemId, int designId) {
        Map<String, Object> map = new TreeMap<String, Object>();
        map.put("planetId", planetId);
        map.put("designId", designId);
        map.put("systemId", systemId);
        map.put("userId", userId);

        return ConfirmBuffer.addNewConfirmEntry(map, userId, GameConstants.CONFIRM_DESTROY_STARBASE);
    }

    public static int getQueueIdent(int userId, int planetId, int fleetId, int designId, String designName) {
        Map<String, Object> map = new TreeMap<String, Object>();
        map.put("planetId", planetId);
        map.put("designId", designId);
        map.put("fleetId", fleetId);
        map.put("userId", userId);
        map.put("name", designName);

        return ConfirmBuffer.addNewConfirmEntry(map, userId, GameConstants.DESTROY_SHIP);
    }

    public static int getChassisId(int designId) {
        int chassisId = 0;

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT chassis FROM shipdesigns WHERE id=" + designId);
            if (rs.next()) {
                chassisId = rs.getInt(1);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while getting ChassisSize=", e);
        }

        return chassisId;
    }

    public static List<ShipData> getShipList(int userId, int locId, boolean planSys, int mode) {
        // mode description
        // 1 = all ships with ress transfer capacity
        LinkedList<ShipData> shipList = new LinkedList<ShipData>();

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT shipfleet.fleetId, shipfleet.designId, shipfleet.count, shipdesigns.name, playerfleet.name FROM shipfleet, shipdesigns, playerfleet WHERE shipfleet.fleetId=playerfleet.id AND playerfleet.planetId=" + locId + " AND playerfleet.userId=" + userId + " AND shipdesigns.id=shipfleet.designId AND shipdesigns.ressSpace<>0 ORDER BY shipfleet.fleetId, shipfleet.designId");
            while (rs.next()) {
                ShipData sd = new ShipData();

                sd.setFleetId(rs.getInt(1));
                sd.setCount(rs.getInt(3));
                sd.setDesignId(rs.getInt(2));
                sd.setDesignName(rs.getString(4));
                // sd.setLoaded(rs.getInt(5) == 1);
                sd.setFleetName(rs.getString(5));

                shipList.add(sd);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while building Ship List=", e);
        }

        return shipList;
    }

    public static List<FleetData> getFleetList(int systemId, int userId, int mode) {
        // mode description
        // 1 = own fleets in system
        // 2 = own fleets not in system
        // 3 = own fleets anywhere with special status (!= 0)
        // 4 = all fleets in system
        // 5 = all fleets on currPlanet
        // 6 = all fleet of other users in system
        // 7 = all own fleets
        // 8 = add defense fleets in system
        // 9 = add defense fleets

        LinkedList<FleetData> fleetList = new LinkedList<FleetData>();

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = null;

            switch (mode) {
                case (1):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, playerfleet.planetId, playerfleet.systemId, playerfleet.status FROM playerfleet WHERE playerfleet.systemId=" + systemId + " AND playerfleet.userId=" + userId + " ORDER BY name ASC");
                    break;
                case (2):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, playerfleet.planetId, playerfleet.systemId, playerfleet.status FROM playerfleet WHERE playerfleet.systemId<>" + systemId + " AND playerfleet.userId=" + userId + " AND systemId<>0 AND status<>2 ORDER BY name ASC");
                    break;
                case (3):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, fleetdetails.destplanet, fleetdetails.destsystem, playerfleet.status, actions.timeFinished, fleetdetails.commandtype, fleetdetails.startsystem, fleetdetails.startplanet FROM playerfleet, fleetdetails, actions WHERE fleetdetails.fleetId=actions.number AND playerfleet.id=actions.number AND playerfleet.userId=" + userId + " AND actions.userId=" + userId + " and actions.actionType=6 ORDER BY name ASC");
                    break;
                case (4):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, playerfleet.planetId, playerfleet.systemId, playerfleet.status FROM playerfleet WHERE playerfleet.systemId=" + systemId + " ORDER BY name ASC");
                    break;
                case (5):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, playerfleet.planetId, playerfleet.systemId, playerfleet.status FROM playerfleet WHERE playerfleet.planetId=" + systemId + " AND playerfleet.userId=" + userId + " ORDER BY name ASC");
                    break;
                case (6):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, playerfleet.planetId, playerfleet.systemId, playerfleet.status FROM playerfleet WHERE playerfleet.systemId=" + systemId + " AND playerfleet.userId<>" + userId + " ORDER BY name ASC");
                    break;
                case (7):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, playerfleet.planetId, playerfleet.systemId, playerfleet.status FROM playerfleet WHERE playerfleet.userId=" + userId + " AND playerfleet.status<>2 AND playerfleet.id NOT IN ( SELECT number FROM actions WHERE actionType=6 AND number=playerfleet.id) ORDER BY name ASC");
                    break;
                case (8):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, playerfleet.planetId, playerfleet.systemId, playerfleet.status FROM playerfleet WHERE playerfleet.systemId=" + systemId + " AND playerfleet.userId=" + userId + " AND playerfleet.status=2 ORDER BY name ASC");
                    break;
                case (9):
                    rs = stmt.executeQuery("SELECT playerfleet.id, playerfleet.name, playerfleet.userId, playerfleet.planetId, playerfleet.systemId, playerfleet.status FROM playerfleet WHERE playerfleet.userId=" + userId + " AND playerfleet.status=2 ORDER BY name ASC");
                    break;
            }

            while ((rs != null) && (rs.next())) {
                FleetData fd;
                if (mode != 3) {
                    fd = new FleetData(rs.getInt(1));
                } else {
                    fd = new FleetData();
                }

                // Setze Flottenwerte f�r Anzeige falls modus 3 in verwendung (Flotten im Flug)
                if (mode == 3) {
                    GameUtilities gu = new GameUtilities();

                    fd.setFleetId(rs.getInt(1));
                    fd.setName(rs.getString(2));
                    fd.setUserId(rs.getInt(3));
                    fd.setPlanetId(rs.getInt(4));
                    fd.setSystemId(rs.getInt(5));
                    fd.setETA(rs.getInt(7) - gu.getCurrentTick());
                    fd.setOrder(rs.getInt(8));

                    if ((rs.getInt(9) == 0) && (rs.getInt(10) == 0)) {
                        fd.setAbleToCallBack(false);
                    } else {
                        fd.setAbleToCallBack(true);
                    }
                }

                // Setze Flottenhaltung f�r Flotten die dem aktuellen Spieler geh�ren
                if (rs.getInt(3) == userId) {
                    fd.setFleetAttitude(1);
                }

                // Handling f�r Flotten die nicht dem aktuellen Spieler geh�ren
                if (rs.getInt(3) != userId) {
                }

                if ((mode == 1) || (mode == 2) || (mode == 3)) {
                    FleetData extInf = ShipUtilities.prepareLoadingInformation(fd.getFleetId());
                    fd.setTroopSpace(extInf.getTroopSpace());
                    fd.setRessSpace(extInf.getRessSpace());
                    fd.setLoadingList(extInf.getLoadingList());
                    fd.setCurrLoadedRess(extInf.getCurrLoadedRess());
                    fd.setCurrLoadedTroops(extInf.getCurrLoadedTroops());
                    fd.setCurrLoadedPopulation(extInf.getCurrLoadedPopulation());
                    fd.setSmallHangarSpace(extInf.getSmallHangarSpace());
                    fd.setMediumHangarSpace(extInf.getMediumHangarSpace());
                    fd.setLargeHangarSpace(extInf.getLargeHangarSpace());
                }

                fleetList.add(fd);
            }

            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities - getCompleteFleetList: ", e);
        }

        return fleetList;
    }
/*
    public static void RenameFleet(int fleetId, String fleetName) {
        FleetData f = FleetBuffer2.getFleetById(fleetId);
        if (f == null) {
            return;
        }
        f.setName(fleetName);
        f.save();
    }*/

    public static List<ShipData> getShipList(int fleetId) {
        LinkedList<ShipData> shipList = new LinkedList<ShipData>();

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT shipfleet.fleetId, shipfleet.designId, shipfleet.count, shipdesigns.name, shipdesigns.chassis FROM shipfleet, shipdesigns WHERE shipfleet.fleetId="+fleetId+" AND shipdesigns.id=shipfleet.designId");

            while (rs.next()) {
                ShipData sd = new ShipData();

                sd.setFleetId(rs.getInt(1));
                sd.setDesignId(rs.getInt(2));
                sd.setDesignName(rs.getString(4));
                sd.setCount(rs.getInt(3));
                sd.setChassisSize(rs.getInt(5));

                shipList.add(sd);
            }
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilties - getShipList: ", e);
        }

        return shipList;
    }

    public static int getFreeFleetId() {
        try {
            Statement stmt = DbConnect.createStatement();

            for (int i = 1; i < 999999; i++) {
                ResultSet rs = stmt.executeQuery("SELECT id FROM playerfleet WHERE id=" + i);
                if (!rs.next()) {
                    stmt.close();
                    return i;
                }
            }
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities - getFreeFleetId: ", e);
        }

        return 0;
    }

    public HashMap<Integer, Integer> getModuleCostForDesign(int designId) {
        HashMap<Integer, Integer> mc = new HashMap<Integer, Integer>();

        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT designmodule.moduleId, designmodule.count FROM designmodule WHERE designmodule.designId=" + designId);
            while (rs.next()) {
                mc.put(rs.getInt(1), rs.getInt(2));
            }
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities - getModuleCostForDesign: ", e);
        }

        return mc;
    }

    public static FleetData loadLoadingInformation(FleetData fd) {
        int troopSpace = 0;
        int ressSpace = 0;
        int smallHangarSpace = 0;
        int mediumHangarSpace = 0;
        int largeHangarSpace = 0;

        // Loop through all Ship Designs and sum up
        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT userId, planetId FROM playerfleet WHERE id=" + fd.getFleetId());
            if (rs.next()) {
                fd.setUserId(rs.getInt(1));
                fd.setPlanetId(rs.getInt(2));
            }

            rs = stmt.executeQuery("SELECT sd.troopSpace, sd.ressSpace, sd.smallHangar, sd.mediumHangar, sd.largeHangar, sf.count FROM shipdesigns sd, shipfleet sf WHERE sf.fleetId=" + fd.getFleetId() + " AND sf.designId=sd.id");
            while (rs.next()) {
                troopSpace += rs.getInt(1) * rs.getInt(6);
                ressSpace += rs.getInt(2) * rs.getInt(6);
                smallHangarSpace += rs.getInt(3) * rs.getInt(6);
                mediumHangarSpace += rs.getInt(4) * rs.getInt(6);
                largeHangarSpace += rs.getInt(5) * rs.getInt(6);
            }

            fd.setTroopSpace(troopSpace);
            fd.setRessSpace(ressSpace);
            fd.setSmallHangarSpace(smallHangarSpace);
            fd.setMediumHangarSpace(mediumHangarSpace);
            fd.setLargeHangarSpace(largeHangarSpace);

            // Calculate Loading
            int currLoadRess = 0;
            int currLoadTroops = 0;
            int currLoadPopulation = 0;
            ArrayList<ShipStorage> loadingDetails = new ArrayList<ShipStorage>();
            rs = stmt.executeQuery("SELECT fl.loadtype, fl.id, fl.count FROM fleetloading fl WHERE fl.fleetId=" + fd.getFleetId() + " ORDER BY fl.loadtype, fl.id ASC");
            while (rs.next()) {
                ShipStorage ss = new ShipStorage();
                ss.setLoadtype(rs.getInt(1));
                ss.setId(rs.getInt(2));
                ss.setCount(rs.getInt(3));

                String name = "#" + ss.getLoadtype() + "-" + ss.getId();

                Statement stmt2 = DbConnect.createStatement();

                switch (ss.getLoadtype()) {
                    case (GameConstants.LOADING_RESS):
                        currLoadRess += ss.getCount();
                        ResultSet rs2 = stmt2.executeQuery("SELECT name FROM ressource WHERE id=" + ss.getId());
                        if (rs2.next()) {
                            name = rs2.getString(1);
                        }
                        break;
                    case (GameConstants.LOADING_TROOPS):
                        rs2 = stmt2.executeQuery("SELECT name, size FROM groundtroops WHERE id=" + ss.getId());
                        if (rs2.next()) {
                            currLoadTroops += ss.getCount() * rs2.getInt(2);
                            name = rs2.getString(1);
                        }
                        break;
                    case (GameConstants.LOADING_SHIPS):
                        rs2 = stmt2.executeQuery("");
                        break;
                    case (GameConstants.LOADING_POPULATION):
                        currLoadPopulation += ss.getCount();
                        name = "Arbeiter";
                        break;
                }

                stmt2.close();
                ss.setName(name);

                loadingDetails.add(ss);
            }

            fd.setCurrLoadedRess(currLoadRess);
            fd.setCurrLoadedTroops(currLoadTroops);
            fd.setCurrLoadedPopulation(currLoadPopulation);
            fd.setLoadingList(loadingDetails);

            // Calculate Hangar Loading
/*            int currLoadSmallHangar = 0;
            int currLoadMediumHangar = 0;
            int currLoadLargeHangar = 0; */
            /*
            
             */
            stmt.close();
            return fd;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while preparing Loading Information ", e);
            return null;
        }
    }    
    
    @Deprecated
    public static FleetData prepareLoadingInformation(int fleetId) {
        FleetData fd = new FleetData();
        int troopSpace = 0;
        int ressSpace = 0;
        int smallHangarSpace = 0;
        int mediumHangarSpace = 0;
        int largeHangarSpace = 0;

        // Loop through all Ship Designs and sum up
        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT userId, planetId FROM playerfleet WHERE id=" + fleetId);
            if (rs.next()) {
                fd.setUserId(rs.getInt(1));
                fd.setPlanetId(rs.getInt(2));
            }

            rs = stmt.executeQuery("SELECT sd.troopSpace, sd.ressSpace, sd.smallHangar, sd.mediumHangar, sd.largeHangar, sf.count FROM shipdesigns sd, shipfleet sf WHERE sf.fleetId=" + fleetId + " AND sf.designId=sd.id");
            while (rs.next()) {
                troopSpace += rs.getInt(1) * rs.getInt(6);
                ressSpace += rs.getInt(2) * rs.getInt(6);
                smallHangarSpace += rs.getInt(3) * rs.getInt(6);
                mediumHangarSpace += rs.getInt(4) * rs.getInt(6);
                largeHangarSpace += rs.getInt(5) * rs.getInt(6);
            }

            fd.setTroopSpace(troopSpace);
            fd.setRessSpace(ressSpace);
            fd.setSmallHangarSpace(smallHangarSpace);
            fd.setMediumHangarSpace(mediumHangarSpace);
            fd.setLargeHangarSpace(largeHangarSpace);

            // Calculate Loading
            int currLoadRess = 0;
            int currLoadTroops = 0;
            int currLoadPopulation = 0;
            ArrayList<ShipStorage> loadingDetails = new ArrayList<ShipStorage>();
            rs = stmt.executeQuery("SELECT fl.loadtype, fl.id, fl.count FROM fleetloading fl WHERE fl.fleetId=" + fleetId + " ORDER BY fl.loadtype, fl.id ASC");
            while (rs.next()) {
                ShipStorage ss = new ShipStorage();
                ss.setLoadtype(rs.getInt(1));
                ss.setId(rs.getInt(2));
                ss.setCount(rs.getInt(3));

                String name = "#" + ss.getLoadtype() + "-" + ss.getId();

                Statement stmt2 = DbConnect.createStatement();

                switch (ss.getLoadtype()) {
                    case (GameConstants.LOADING_RESS):
                        currLoadRess += ss.getCount();
                        ResultSet rs2 = stmt2.executeQuery("SELECT name FROM ressource WHERE id=" + ss.getId());
                        if (rs2.next()) {
                            name = rs2.getString(1);
                        }
                        break;
                    case (GameConstants.LOADING_TROOPS):
                        rs2 = stmt2.executeQuery("SELECT name, size FROM groundtroops WHERE id=" + ss.getId());
                        if (rs2.next()) {
                            currLoadTroops += ss.getCount() * rs2.getInt(2);
                            name = rs2.getString(1);
                        }
                        break;
                    case (GameConstants.LOADING_SHIPS):
                        rs2 = stmt2.executeQuery("");
                        break;
                    case (GameConstants.LOADING_POPULATION):
                        currLoadPopulation += ss.getCount();
                        name = "Arbeiter";
                        break;
                }

                stmt2.close();
                ss.setName(name);

                loadingDetails.add(ss);
            }

            fd.setCurrLoadedRess(currLoadRess);
            fd.setCurrLoadedTroops(currLoadTroops);
            fd.setCurrLoadedPopulation(currLoadPopulation);
            fd.setLoadingList(loadingDetails);

            // Calculate Hangar Loading
/*            int currLoadSmallHangar = 0;
            int currLoadMediumHangar = 0;
            int currLoadLargeHangar = 0; */
            /*
            
             */
            stmt.close();
            return fd;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while preparing Loading Information ", e);
            return null;
        }
    }

    public static boolean isRestricted(int chassisId, int moduleId) {
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT chassisId FROM restrictions WHERE chassisId=" + chassisId + " AND restrictedModule=" + moduleId + " AND count=0");
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public static String checkForBuiltShips(int designId) {
        String message = "";

        try {
            // Check standard fleet data
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT designId FROM shipfleet WHERE designId=" + designId);
            if (rs.next()) {
                stmt.close();
                return "Schiffe f�r dieses Design vorhanden. �nderung nicht erlaubt!";
            }

            // Check trade ships
            rs.close();
            rs = stmt.executeQuery("SELECT designId FROM tradepostship WHERE designId=" + designId);
            if (rs.next()) {
                stmt.close();
                return "Schiffe f�r dieses Design im Handel. �nderung nicht erlaubt!";
            }

            // Check upgrade, destruction and production orders
            rs.close();
            rs = stmt.executeQuery("SELECT buildingId FROM actions WHERE actionType=4 AND buildingId=" + designId);
            if (rs.next()) {
                stmt.close();
                return "Schiffe f�r dieses Design in Werft. �nderung nicht erlaubt!";
            }

            rs.close();
            rs = stmt.executeQuery("SELECT id FROM productionorder WHERE toId=" + designId);
            if (rs.next()) {
                stmt.close();
                return "Schiffe f�r dieses Design in Werft. �nderung nicht erlaubt!";
            }

            // Check planetary defense
            rs.close();
            rs = stmt.executeQuery("SELECT unitId FROM planetdefense WHERE unitId=" + designId);
            if (rs.next()) {
                stmt.close();
                return "Raumstation vorhandern. �nderung nicht erlaubt!";
            }

            // Check traderoutes
            rs.close();
            rs = stmt.executeQuery("SELECT designId FROM traderouteships WHERE designId=" + designId);
            if (rs.next()) {
                stmt.close();
                return "Schiffe f�r dieses Design in Handelsroute. �nderung nicht erlaubt!";
            }

            // Check hangars
            rs.close();
            rs = stmt.executeQuery("SELECT designId FROM hangarrelation WHERE designId=" + designId);
            if (rs.next()) {
                stmt.close();
                return "Schiffe f�r dieses Design in Hangars. �nderung nicht erlaubt!";
            }

            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while built ships check: ", e);
            return "Internal Error: " + e;
        }

        return message;
    }

/*
    public static boolean checkIfFleetValidReconstruct(int user, int fleetId) {

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT planetId FROM playerfleet WHERE id=" + fleetId + " AND userId=" + user);

            if (!rs.next()) {
                stmt.close();
                return false;
            } else {
                ProdConsBuffer pcb = new ProdConsBuffer(ProdConsBuffer.BYPLANET, rs.getInt(1));
                if ((pcb.getCountForBuilding(rs.getInt(1), ProdConsBuffer.DOCK) == 0) || (pcb.getCountForBuilding(rs.getInt(1), ProdConsBuffer.MODULE_FACTORY) == 0)) {
                    stmt.close();
                    return false;
                }
            }

            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while check for correct Fleet = ", e);
        }

        return true;
    }*/

    /*
    public ShipRestructuringRessourceCost getDeconstructionInformation(HttpServletRequest request, Map<String, Object> params, int type) {
        int designId = ((Integer) params.get("designId")).intValue();
        /*        int planetId = ((Integer)params.get("planetId")).intValue();
        int userId = ((Integer)params.get("userId")).intValue();
        if (type == 1) {
        int fleetId = ((Integer)params.get("fleetId")).intValue();
        } else if (type == 2) {
        int systemId = ((Integer)params.get("systemId")).intValue();
        } 

        if (request.getParameter("destroyCount") == null) {
            DebugBuffer.addLine(DebugLevel.DEBUG, "Der Parameter destroyCount wurde nicht �bergeben. �bergeben wurden nur: " + request.getParameterMap());
            return null;
        }

        int destroyCount = Integer.parseInt(request.getParameter("destroyCount"));
        if (destroyCount <= 0) {
            DebugBuffer.addLine(DebugLevel.DEBUG, "destroyCount == 0 ist M�ll");
            return null;
        }
        
        ShipRestructuringRessourceCost res = getShipScrapCost(designId);
        if (destroyCount == 0) {
            DebugBuffer.addLine(DebugLevel.DEBUG, "Du willst mehr Schiffe vernichten, als es gibt!!");
            return null;
        }

        params.put("count", Integer.valueOf(destroyCount));
        res.setCount(destroyCount);
        res.setName((String) params.get("name"));
        return res;
    }
    */

    public static ShipData getShipData(int fleetId, int designId) {
        ShipData sd = new ShipData();

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT shipfleet.fleetId, shipfleet.designId, shipfleet.count, shipdesigns.chassis, shipdesigns.name, playerfleet.name FROM shipfleet, shipdesigns, playerfleet WHERE shipfleet.fleetId=playerfleet.id AND playerfleet.id=" + fleetId + " AND shipdesigns.id=" + designId + " AND shipfleet.designId=shipdesigns.id");
            while (rs.next()) {
                sd.setFleetId(rs.getInt(1));
                sd.setCount(rs.getInt(3));
                sd.setDesignId(rs.getInt(2));
                sd.setChassisSize(rs.getInt(4));
                sd.setDesignName(rs.getString(5));
                // sd.setLoaded(rs.getInt(5) == 1);
                sd.setFleetName(rs.getString(6));
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while building Ship Data: ", e);
        }

        return sd;
    }

    public static ModuleEntry getModuleDataSum(int designId) {
        ModuleEntry me = new ModuleEntry();

        try {
            int chassisId = 0;

            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT chassis FROM shipdesigns WHERE id=" + designId);
            if (rs.next()) {
                chassisId = rs.getInt(1);
            }

            rs = stmt.executeQuery("SELECT module.id FROM designmodule dm, module m WHERE dm.designId=" + designId + " AND m.id=dm.moduleId");
            while (rs.next()) {
                ModuleEntry tmp = getModuleData(rs.getInt(1), chassisId);
                me.addRess(tmp, 1d);
                // me.setCredits(me.getCredits() + tmp.getCredits());
                me.setSpace(me.getSpace() + tmp.getSpace());
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Exception while getModuleDataSum - ConstructionUtilities: ", e);
        }

        return me;
    }

    public static ModuleEntry getModuleData(int moduleId, int chassisSize) {
        ModuleEntry me = new ModuleEntry();

        try {
//            int chassisMP = 0;
            float engineFactor = 0;
            float hyperFactor = 0;
            float creditFactor = 0;
            float hyperCrystalFactor = 0;

            Statement stmt = DbConnect.createStatement();
            Statement stmt2 = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT module.chassisSize, sizerelation.engineFactor, sizerelation.hyperFactor, sizerelation.creditFactor, sizerelation.hyperCrystalFactor FROM module, sizerelation WHERE module.id=" + moduleId + " AND sizerelation.chassisId=" + chassisSize);
            if (rs.next()) {
//                chassisMP = rs.getInt(1);
                engineFactor = rs.getFloat(2);
                hyperFactor = rs.getFloat(3);
                creditFactor = rs.getFloat(4);
                hyperCrystalFactor = rs.getFloat(5);
            // Logger.getLogger().write("MODIFIERS FOUND - ENGINE FACTOR: " + engineFactor);
            // DebugBuffer.addLine(DebugLevel.DEBUG,"Chassis modifier " + chassisMP);
            }

            rs = stmt2.executeQuery("SELECT credits, iron, steel, terkonit, ynkelonium, howalgonium, cvEmbinium, space, modulPoints, name, type, value, energy FROM module WHERE id=" + moduleId);
            if (rs.next()) {
                float tmpSpace = 1f;
                float tmpCredit = 1f;
                float tmpEnergy = 1f;
                float tmpStrength = 1f;

                // Falls modul Panzerung oder Antrieb - Multiplikationsfaktor ber�cksichtigen
                float factor = 1;
                float creditFactorLocal = 1;
                float hyperCrystalFactorLocal = 1;

                me.setModuleId(moduleId);
                me.setType(rs.getInt(11));
                me.setName(rs.getString(10));
                me.setSpace((int) Math.floor(rs.getInt(8) * tmpSpace));
                me.setModulePoints((int) Math.floor(rs.getInt(9) * tmpSpace));
                me.setValue((int) Math.floor(rs.getInt(12) * tmpStrength));
                me.setEnergy((int) Math.floor(rs.getInt(13) * tmpEnergy));
                DebugBuffer.addLine(DebugLevel.DEBUG, "Preparing module " + me.getName());

                // Engine
                if (rs.getInt(11) == 4) {
                    factor = engineFactor;
                    hyperCrystalFactorLocal = hyperCrystalFactor;
                    creditFactorLocal = creditFactor;

                    DebugBuffer.addLine(DebugLevel.DEBUG, "SPACE -> " + rs.getInt(8) + " * " + factor + " = " + (int) (rs.getInt(8) * factor));

                    me.setSpace((int) (me.getSpace() * factor));
                    // Logger.getLogger().write("MP (E) = " +rs.getInt(9) + " * " + factor);
                    me.setModulePoints((int) Math.floor(me.getModulePoints() * factor));
                }

                // Panzerung
                if (rs.getInt(11) == 2) {
                    factor = engineFactor;
                    hyperCrystalFactorLocal = hyperCrystalFactor;
                    creditFactorLocal = engineFactor;

                    DebugBuffer.addLine(DebugLevel.DEBUG, "SPACE -> " + factor + " = " + (int) (factor));

                    me.setSpace(0);
                    // Logger.getLogger().write("MP (P) = " +(int)Math.floor(me.getModulePoints() * factor)+ " * " + factor);
                    me.setModulePoints((int) Math.floor(me.getModulePoints() * factor));
                }

                // Hyperspace anpassung
                if (rs.getInt(11) == 1) {
                    factor = hyperFactor;
                    hyperCrystalFactorLocal = hyperCrystalFactor;
                    creditFactorLocal = hyperFactor;

                    DebugBuffer.addLine(DebugLevel.DEBUG, "SPACE -> " + rs.getInt(8) + " * " + factor + " = " + (int) (rs.getInt(8) * factor));

                    me.setSpace((int) Math.floor(me.getSpace() * factor));
                    // Logger.getLogger().write("MP (HS) = " +(int)Math.floor(me.getModulePoints() * factor)+ " * " + factor);
                    me.setModulePoints((int) Math.floor(me.getModulePoints() * factor));
                }

                if (me.getSpace() == 0) {
                    me.setSpace((int) Math.floor(rs.getInt(8) * tmpSpace));
                    DebugBuffer.addLine(DebugLevel.DEBUG, "SET DEFAULT = " + (int) Math.floor(rs.getInt(8) * tmpSpace));
                }

                // Not useful here as there is a distinction between types of ressources
                // Maybe in future additonal dynamic properties for ressources should be added
                // RessourcesEntry moduleCost = ConstructionUtilities.getConstructionCost(RessourceCostBuffer.COST_MODULE, moduleId);

                // me.setCredits((int) (rs.getInt(1) * creditFactorLocal * tmpCredit));
                me.setRess(Ressource.IRON, (int) (rs.getInt(2) * factor * tmpSpace));
                me.setRess(Ressource.STEEL, (int) (rs.getInt(3) * factor * tmpSpace));
                me.setRess(Ressource.TERKONIT, (int) (rs.getInt(4) * factor * tmpSpace));
                me.setRess(Ressource.YNKELONIUM, (int) (rs.getInt(5) * factor * tmpSpace));
                me.setRess(Ressource.HOWALGONIUM, (int) (rs.getInt(6) * hyperCrystalFactorLocal * tmpSpace));
                me.setRess(Ressource.CVEMBINIUM, (int) (rs.getInt(7) * hyperCrystalFactorLocal * tmpSpace));

            }

            stmt.close();
            stmt2.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipUtilities -- getModuleEntry: ", e);
        }

        // Logger.getLogger().write("Module " + me.getName() + " Cost="+me.getCredits());

        return me;
    }

    /*
    public static void adjustToShipType(ShipRestructuringRessourceCost shipCosts, ShipCapacity shipCap, int shipType) {
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ressFactor, troopFactor, hangarFactor, costFactor FROM shiptypeadjustment WHERE type=" + shipType);
            if (rs.next()) {
                // shipCosts.setCredits((long) (shipCosts.getCredits() * rs.getFloat(4)));
                if (shipCap != null) {
                    shipCap.ressSpace = (int) ((float) shipCap.ressSpace * rs.getFloat(1));
                    shipCap.troopSpace = (int) ((float) shipCap.troopSpace * rs.getFloat(2));
                    shipCap.smallHangarCap = (int) ((float) shipCap.smallHangarCap * rs.getFloat(3));
                    shipCap.mediumHangarCap = (int) ((float) shipCap.mediumHangarCap * rs.getFloat(3));
                    shipCap.largeHangarCap = (int) ((float) shipCap.largeHangarCap * rs.getFloat(3));
                }
            }

            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while adjusting costs by shiptype: ", e);
        }
    }
    */


    /*
    @Deprecated    
    public static ShipRestructuringRessourceCost getRebuildCost(int fromDesign, int toDesign, int count) {
        ShipRestructuringRessourceCost ressBalance = new ShipRestructuringRessourceCost(fromDesign);
        HashMap<Integer, Integer> moduleAdd = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> moduleRemove = new HashMap<Integer, Integer>();

        // Get type of old and new design (if it is from or to transporter increase cost by 60%)        
        long baseCost = 0;
        float creditFactor = 1f;
//        boolean ship1Bonus = false;
        boolean ship2Bonus = false;

        try {
            Statement stmt = DbConnect.createStatement();
            Statement stmt2 = DbConnect.createStatement();

            ResultSet rs1 = stmt.executeQuery("SELECT costFactor FROM shiptypeadjustment WHERE type=2");
            if (rs1.next()) {
                creditFactor = rs1.getFloat(1);
            }

            rs1.close();

            rs1 = stmt.executeQuery("SELECT type, credits FROM shipdesigns WHERE id=" + fromDesign);
            ResultSet rs2 = stmt2.executeQuery("SELECT type, credits FROM shipdesigns WHERE id=" + toDesign);
            if (rs1.next()) {
//                if (rs1.getInt(1) == 2) ship1Bonus = true;

                if (rs2.next()) {
                    if (rs2.getInt(1) == 2) {
                        ship2Bonus = true;
                    }
                    if (((rs1.getInt(1) == 2) && (rs2.getInt(1) != 2)) ||
                            ((rs1.getInt(1) != 2) && (rs2.getInt(1) == 2))) {
                        if (rs1.getInt(1) == 2) {
                            baseCost = rs2.getLong(2) - rs1.getLong(2);
                        } else {
                            baseCost = rs1.getLong(2) - rs2.getLong(2);
                        }
                    }
                }
            }

            stmt.close();
            stmt2.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getRebuildCost: ", e);
        }

        // Ermitteln der Unterschiede zwischen beiden Designs und Kosten
        // berechnen
        LinkedList<ModuleData> modulesOld = getModuleData(fromDesign);
        LinkedList<ModuleData> modulesNew = getModuleData(toDesign);

        LinkedList<ModuleData> copyMO = new LinkedList<ModuleData>();
        LinkedList<ModuleData> copyMN = new LinkedList<ModuleData>();
        copyMO.addAll(modulesOld);
        copyMN.addAll(modulesNew);

        int totalWorkSpace = 0;

        // ressBalance h�lt die insgesamten Ressourcenver�nderung die durch recycling und
        // Neuproduktion entstehen
        int chassisId = getChassisId(fromDesign);

        // Berechnen von Modulen bei denen sich nur die Anzahl ver�ndert hat.
        for (ModuleData mdOld : copyMO) {

            for (ModuleData mdNew : copyMN) {

                if (mdOld.getModuleId() == mdNew.getModuleId()) {
                    int diff = mdNew.getCount() - mdOld.getCount();
                    int diffAbs = Math.abs(diff);

                    DebugBuffer.addLine(DebugLevel.TRACE, "Module " + mdNew.getName() + " has diff=" + diff);

                    ModuleEntry me = getModuleData(mdNew.getModuleId(), chassisId);

                    totalWorkSpace += me.getSpace() * (Math.abs(diff));
                    DebugBuffer.addLine(DebugLevel.TRACE, "Adding by change = " + (me.getSpace() * (Math.abs(diff))));

                    if (diff < 0) {
                        moduleRemove.put(me.getModuleId(), diff);
                        DebugBuffer.addLine(DebugLevel.TRACE, "Increasing Cost by removing (" + me.getName() + "): " + me.getSpace() * diffAbs + "*" + SetupValues.CREDIT_COST_PERSPACE + "*" + count + "=" + (me.getSpace() * diffAbs * SetupValues.CREDIT_COST_PERSPACE * count));
                        // ressBalance.setCredits(ressBalance.getCredits() + (long) ((long) me.getSpace() * (long) diffAbs * (long) SetupValues.CREDIT_COST_PERSPACE * (long) count));

                        for (Ressource re : ressourceDAO.findAllStoreableRessources()) {
                            ressBalance.setRess(re.getId(),
                                    ressBalance.getRess(re.getId()) + (int) (me.getRess(re.getId()) * diffAbs * SetupValues.RESSOURCE_MULTI * count));
                        }
                    } else if (diff > 0) {
                        moduleAdd.put(me.getModuleId(), diff);
                        if (ship2Bonus) {
                            //me.setCredits((long) Math.floor(me.getCredits() * creditFactor));
                        }
                        //DebugBuffer.addLine(DebugLevel.TRACE, "Increasing Cost by adding (" + me.getName() + "): " + me.getCredits() + "*" + diffAbs + "*" + count + "=" + (me.getCredits() * (long) diffAbs * (long) count));
                        //ressBalance.setCredits(ressBalance.getCredits() + (long) ((long) me.getCredits() * (long) diffAbs * (long) count));
                        
                    /*    for (RessourceEntry re : RessourceBuffer.getAllStorableRessources()) {
                            ressBalance.setRess(re.getRessourceId(),
                                    ressBalance.getRess(re.getRessourceId()) - (int) (me.getRess(re.getRessourceId()) * diffAbs * count));
                        }
                    }

                    // DebugBuffer.addLine(ressBalance.getCredits());

                    modulesOld.remove(mdOld);
                    modulesNew.remove(mdNew);
                }
            }
        }

        // Module die ausgebaut werden m�ssen        
        for (ModuleData mdRemove : modulesOld) {
            ModuleEntry me = getModuleData(mdRemove.getModuleId(), chassisId);
            totalWorkSpace += me.getSpace() * mdRemove.getCount();
            DebugBuffer.addLine(DebugLevel.TRACE, "Adding by remove = " + (me.getSpace() * mdRemove.getCount()));
            DebugBuffer.addLine(DebugLevel.TRACE, "Increasing Cost by removing 2 (" + me.getName() + "): " + me.getSpace() * mdRemove.getCount() + "*" + SetupValues.CREDIT_COST_PERSPACE + "*" + count + "=" + (me.getSpace() * mdRemove.getCount() * SetupValues.CREDIT_COST_PERSPACE * count));
            //ressBalance.setCredits(ressBalance.getCredits() + (long) (((long) me.getSpace() * (long) mdRemove.getCount() * (long) SetupValues.CREDIT_COST_PERSPACE * (long) count)));
            
            for (Ressource re : ressourceDAO.findAllStoreableRessources()) {
                ressBalance.setRess(re.getId(),
                        ressBalance.getRess(re.getId()) + (long)(me.getRess(re.getId()) * mdRemove.getCount() * SetupValues.RESSOURCE_MULTI * count));
            }            

            moduleRemove.put(me.getModuleId(), mdRemove.getCount());
            DebugBuffer.addLine(DebugLevel.DEBUG, "Module " + mdRemove.getName() + " has to be removed");
        }

        // Module die neu eingebaut werden        
        for (ModuleData mdAdd : modulesNew) {
            ModuleEntry me = ShipUtilities.getModuleData(mdAdd.getModuleId(), chassisId);
            totalWorkSpace += me.getSpace() * mdAdd.getCount();
            DebugBuffer.addLine(DebugLevel.TRACE, "Adding by add = " + (me.getSpace() * mdAdd.getCount()));

            if (ship2Bonus) {
                //me.setCredits((long) Math.floor(me.getCredits() * creditFactor));
            }
            //DebugBuffer.addLine(DebugLevel.TRACE, "Increasing Cost by adding 2 (" + me.getName() + "): " + me.getCredits() + "*" + mdAdd.getCount() + "*" + count + "=" + (me.getCredits() * mdAdd.getCount() * count));
            //ressBalance.setCredits(ressBalance.getCredits() + (long) ((long) me.getCredits() * (long) mdAdd.getCount() * (long) count));
            
          /*  for (RessourceEntry re : RessourceBuffer.getAllStorableRessources()) {
                ressBalance.setRess(re.getRessourceId(),
                        ressBalance.getRess(re.getRessourceId()) - (long)(me.getRess(re.getRessourceId()) * mdAdd.getCount() * count));
            }     

            DebugBuffer.addLine(DebugLevel.TRACE, "Module " + mdAdd.getName() + " has to be added");
            moduleAdd.put(me.getModuleId(), mdAdd.getCount());
        }

        ressBalance.setRemove(moduleRemove);
        ressBalance.setAdding(moduleAdd);
        ressBalance.setBuildTime(totalWorkSpace);

        //ressBalance.setCredits(ressBalance.getCredits() + baseCost);

        return ressBalance;
    }
    */
            
    /*
    / @deprecated Replaced through FleetService.checkValidUserView
    */
    @Deprecated
    public static void checkValidUserView(String fleetId, int userId, int systemId, boolean scanner) throws Exception {
        if ((fleetId == null) || (fleetId.equalsIgnoreCase("0"))) {
            return;
        }
        int fleet = Integer.parseInt(fleetId);

        Statement stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT userId, systemId, status FROM playerfleet WHERE id=" + fleet);
        if (rs.next()) {
            if ((DiplomacyUtilities.hasAllianceRelation(rs.getInt(1), userId)) || (rs.getInt(1) == userId)) {
                return;
            }
            if ((rs.getInt(2) == systemId) && scanner) {
                return;
            }
        }

        stmt.close();

        System.err.println("[3] Spieler " + userId + " hat keine Berechtigung auf Flotte " + fleetId + " zuzugreifen!");
        try {
            throw new Exception("[3] Spieler " + userId + " hat keine Berechtigung auf Flotte " + fleetId + " zuzugreifen!");
        } catch (Exception e) {
            throw e;
        }
    }

    /*
    / @deprecated Replaced through FleetService.checkValidUser
    */
    @Deprecated
    public static void checkValidUser(String fleetId, int userId, JspWriter out) throws Exception {
        // Logger.getLogger().write("FleetId " + fleetId + " userId " + userId);

        if ((fleetId == null) || (fleetId.equalsIgnoreCase("0"))) {
            return;
        }
        int fleet = Integer.parseInt(fleetId);

        Statement stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT playerfleet.userId FROM playerfleet WHERE playerfleet.id=" + fleet);
        if (rs.next()) {
            if (rs.getInt(1) != userId) {
                //B�se manipulation gefunden !!!
                stmt.close();
                out.write("Diese Flotte geh�rt dir nicht, also kannst du sie auch nicht manipulieren !!!");
                //        	out.close();
                System.err.println("[4] Spieler " + userId + " hat keine Berechtigung auf Flotte " + fleetId + " zuzugreifen!");
                try {
                    throw new Exception("[4] Spieler " + userId + " hat keine Berechtigung auf Flotte " + fleetId + " zuzugreifen!");
                } catch (Exception e) {
                    throw e;
                }
            }
        }
        stmt.close();
    }
    // Check for loaded ressources or troops which have to be reduced
    // Check current available loading space and compare with needed space
    // Reduces ressources/troops by their shares in loading    
    public static void cleanUpLoading(int fleetId) {
        // Check if fleet has loading space
        FleetData fd = ShipUtilities.prepareLoadingInformation(fleetId);

        // Compare available and loaded space         
        // If necessary reduce loading
        if (fd.getCurrLoadedRess() > fd.getRessSpace()) {
            int totalLoading = fd.getCurrLoadedRess();

            HashMap<Integer, Double> sharesByRess = new HashMap<Integer, Double>();
            HashMap<Integer, Integer> newRess = new HashMap<Integer, Integer>();
            double adjustFactor = 1;

            try {
                Statement stmt = DbConnect.createStatement();

                if (fd.getRessSpace() == 0) {
                    // if no space delete all and return
                    stmt.execute("DELETE FROM fleetloading WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_RESS);
                } else {
                    // get adjustFactor
                    adjustFactor = (double) fd.getRessSpace() / (double) fd.getCurrLoadedRess();
                }

                if (fd.getRessSpace() > 0) {
                    ResultSet rs = stmt.executeQuery("SELECT id, count FROM fleetloading WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_RESS);

                    while (rs.next()) {
                        double share = 100 / totalLoading * rs.getInt(2);
                        sharesByRess.put(rs.getInt(1), share);
                        newRess.put(rs.getInt(1), rs.getInt(2));
                    }

                    for (Map.Entry<Integer, Double> me : sharesByRess.entrySet()) {
                        int ressId = me.getKey();
                        newRess.put(ressId, (int) (Math.floor(newRess.get(ressId) * adjustFactor)));

                        if (newRess.get(ressId) > 0) {
                            stmt.executeUpdate("UPDATE fleetloading SET count=" + newRess.get(ressId) + " WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_RESS + " AND id=" + ressId);
                        } else {
                            stmt.execute("DELETE FROM fleetloading WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_RESS + " AND id=" + ressId);
                        }
                    }
                }

                stmt.close();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in cleanUpLoading: ", e);
            }
        }

        if ((fd.getCurrLoadedTroops() + fd.getCurrLoadedPopulation()) > fd.getTroopSpace()) {
            int totalLoading = fd.getCurrLoadedTroops() + fd.getCurrLoadedPopulation();

            HashMap<Integer, Double> sharesByUnit = new HashMap<Integer, Double>();
            HashMap<Integer, Integer> newUnit = new HashMap<Integer, Integer>();
            double adjustFactor = 1;

            try {
                Statement stmt = DbConnect.createStatement();

                if (fd.getTroopSpace() == 0) {
                    // if no space delete all and return
                    stmt.execute("DELETE FROM fleetloading WHERE fleetId=" + fleetId + " AND (loadtype=" + GameConstants.LOADING_POPULATION + " OR loadtype=" + GameConstants.LOADING_TROOPS + ")");
                } else {
                    // get adjustFactor
                    adjustFactor = (double) fd.getTroopSpace() / (double) (fd.getCurrLoadedPopulation() + fd.getCurrLoadedTroops());
                }

                if ((fd.getCurrLoadedTroops() > 0) && (fd.getTroopSpace() > 0)) {
                    ResultSet rs = stmt.executeQuery("SELECT id, count FROM fleetloading WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_TROOPS);

                    HashMap<Integer, Integer> troopSpaceMap = new HashMap<Integer, Integer>();

                    Statement stmt2 = DbConnect.createStatement();
                    ResultSet rs2 = stmt2.executeQuery("SELECT id, size FROM groundtroops");

                    while (rs2.next()) {
                        troopSpaceMap.put(rs2.getInt(1), rs2.getInt(2));
                    }

                    stmt2.close();

                    while (rs.next()) {
                        double share = 100d / totalLoading * (rs.getInt(2) * troopSpaceMap.get(rs.getInt(1)));
                        sharesByUnit.put(rs.getInt(1), share);
                        newUnit.put(rs.getInt(1), rs.getInt(2));
                    }

                    for (Map.Entry<Integer, Double> me : sharesByUnit.entrySet()) {
                        int unitId = me.getKey();
                        newUnit.put(unitId, (int) ((double) Math.floor(newUnit.get(unitId) * adjustFactor)));

                        if (newUnit.get(unitId) > 0) {
                            stmt.executeUpdate("UPDATE fleetloading SET count=" + newUnit.get(unitId) + " WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_TROOPS + " AND id=" + unitId);
                        } else {
                            stmt.execute("DELETE FROM fleetloading WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_TROOPS + " AND id=" + unitId);
                        }
                    }
                }

                if ((fd.getCurrLoadedPopulation() > 0) && (fd.getTroopSpace() > 0)) {
//                    double share = 100d / totalLoading * fd.getCurrLoadedPopulation();
                    int newUnitCount = (int) (Math.floor((double) fd.getCurrLoadedPopulation() * adjustFactor));

                    if (newUnitCount > 0) {
                        stmt.executeUpdate("UPDATE fleetloading SET count=" + newUnitCount + " WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_POPULATION);
                    } else {
                        stmt.execute("DELETE FROM fleetloading WHERE fleetId=" + fleetId + " AND loadtype=" + GameConstants.LOADING_POPULATION);
                    }
                }

                stmt.close();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in cleanUpLoading: ", e);
            }
        }
    }

    public static void setFleetToDef(int fleetId) throws Exception {
        Statement stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT status FROM playerfleet WHERE id=" + fleetId);
        if (rs.next()) {
            if (rs.getInt(1) != 0) {
                stmt.close();
                throw new Exception("Invalid fleet status");
            }
        }

        stmt.executeUpdate("UPDATE playerfleet SET status=2 WHERE id=" + fleetId);
    }

    public static void restoreFleetFromDef(int fleetId) throws Exception {
        Statement stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT status FROM playerfleet WHERE id=" + fleetId);
        if (rs.next()) {
            if (rs.getInt(1) != 2) {
                stmt.close();
                throw new Exception("Invalid fleet status");
            }
        }

        stmt.executeUpdate("UPDATE playerfleet SET status=0 WHERE id=" + fleetId);
    }   
}
