/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

/**
 *
 * @author Stefan
 */
public class Shield extends BuiltInModule implements Defensive {
    private int energyConsumption;
    private int defenseStrength;
    
    public Shield(ModInitParameter mip, int id) {
        super(mip,id);
    }    
    
    public double getAbsorbtion(Offensive module, ShipDesignDetailed enemy) {
        return 1d;
    }

    public int getEnergyConsumption() {
        return energyConsumption;
    }

    protected void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public int getDefenseStrength() {
        return defenseStrength;
    }

    protected void setDefenseStrength(int defenseStrength) {
        this.defenseStrength = defenseStrength;
    }
}
