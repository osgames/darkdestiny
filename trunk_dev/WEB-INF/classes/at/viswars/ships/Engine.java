/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

/**
 *
 * @author Stefan
 */
public class Engine extends BuiltInModule {
    private int energyConsumption;
    private float interstellarSpeed;
    private float acceleration;

    public Engine(ModInitParameter mip, int id) {
        super(mip,id);
    }        
    
    public int getEnergyConsumption() {
        return energyConsumption;
    }

    protected void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public float getInterstellarSpeed() {
        return interstellarSpeed;
    }

    protected void setInterstellarSpeed(float interstellarSpeed) {
        this.interstellarSpeed = interstellarSpeed;
    }

    public float getAcceleration() {
        return acceleration;
    }

    protected void setAcceleration(float acceleration) {
        this.acceleration = acceleration;
    }
}
