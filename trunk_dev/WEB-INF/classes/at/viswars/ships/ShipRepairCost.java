/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.enumeration.EDamageLevel;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ShipRepairCost extends MutableRessourcesEntry {
    // private ChassisDAO chDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);

    private final ShipDesignExt sde;
    // private final HashMap<EDamageLevel,Integer> count = new HashMap<EDamageLevel,Integer>();
    // private BuildTime bt;

    public ShipRepairCost(ShipDesignExt sde) {       
        this.sde = sde;
    }    
    
    public ShipRepairCost(ShipDesignExt sde, int count) {       
        this.sde = sde;
    }

    private void setRepairCost(EDamageLevel ess) {
        this.multiplyRessources(0);

        RessourcesEntry re = sde.getRessCost();    
        this.addRess(re, 1);

        if (ess == EDamageLevel.NODAMAGE) {
            this.multiplyRessources(0d, 0d);
        } else if (ess == EDamageLevel.MINORDAMAGE) { // 20% damage
            this.multiplyRessources(0.2d, 0.1d);
        } else if (ess == EDamageLevel.LIGHTDAMAGE) { // 40% damage
            this.multiplyRessources(0.4d, 0.2d);
        } else if (ess == EDamageLevel.MEDIUMDAMAGE) { // 60% damage
            this.multiplyRessources(0.6d, 0.4d);
        } else if (ess == EDamageLevel.HEAVYDAMAGE) { // 80% damage
            this.multiplyRessources(0.8d, 0.6d);
        }

        // Load chassis multiplicator
        // Chassis ch = chDAO.findById(((ShipDesign)sde.getBase()).getChassis());
        // this.multiplyRessources(ch.getMinBuildQty());
    }

    public BuildTime getBuildTime(EDamageLevel edl) {
        BuildTime orgBt = sde.getBuildTime();
        BuildTime newBt = null;

        if (edl == EDamageLevel.NODAMAGE) {
            newBt = orgBt.getAdjustedBuildTime(0d,0d,0d,0d,0d);
        } else if (edl == EDamageLevel.MINORDAMAGE) { // 20% damage
            newBt = orgBt.getAdjustedBuildTime(0d,0d,0.1d,0.05d,0.2d);
        } else if (edl == EDamageLevel.LIGHTDAMAGE) { // 40% damage
            newBt = orgBt.getAdjustedBuildTime(0d,0d,0.2d,0.1d,0.4d);
        } else if (edl == EDamageLevel.MEDIUMDAMAGE) { // 60% damage
            newBt = orgBt.getAdjustedBuildTime(0d,0d,0.3d,0.2d,0.8d);
        } else if (edl == EDamageLevel.HEAVYDAMAGE) { // 80% damage
            newBt = orgBt.getAdjustedBuildTime(0d,0d,0.5d,0.4d,1.2d);
        }

        // Load chassis multiplicator
        // Chassis ch = chDAO.findById(((ShipDesign)sde.getBase()).getChassis());
        // newBt = newBt.getAdjustedBuildTime(ch.getMinBuildQty());

        return newBt;
    }

    public MutableRessourcesEntry getRepairCost(EDamageLevel ess, int count) {
        setRepairCost(ess);
        
        MutableRessourcesEntry mre = new MutableRessourcesEntry();
        mre.addRess(this, 1);
        mre.multiplyRessources(count);
        return mre;
    }
    
    public RessourcesEntry getTotalCosts(HashMap<EDamageLevel,Integer> toRepair) {
        MutableRessourcesEntry total = new MutableRessourcesEntry();
        
        for (Map.Entry<EDamageLevel,Integer> entries : toRepair.entrySet()) {
            total.addRess(getRepairCost(entries.getKey(), entries.getValue()),1d);
        }
        
        return total;
    }
}
