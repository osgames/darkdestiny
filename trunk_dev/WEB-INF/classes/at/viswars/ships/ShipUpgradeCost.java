/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

import at.viswars.Logger.Logger;
import at.viswars.enumeration.EAttribute;
import at.viswars.ressources.MutableRessourcesEntry;
import at.viswars.admin.module.AttributeTree;
import at.viswars.admin.module.ModuleAttributeResult;
import at.viswars.buildable.ShipDesignExt;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DesignModuleDAO;
import at.viswars.dao.ShipTypeAdjustmentDAO;
import at.viswars.databuffer.RessAmountEntry;
import at.viswars.databuffer.RessourcesEntry;
import at.viswars.enumeration.EShipType;
import at.viswars.model.Chassis;
import at.viswars.model.DesignModule;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipTypeAdjustment;
import at.viswars.service.IdToNameService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ShipUpgradeCost extends MutableRessourcesEntry {
    private DesignModuleDAO dmDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);
    private ChassisDAO chDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private static ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);
    
    private final int count;
    private final BuildTime buildTime;
    private final ShipDesignExt sde;
    private final ShipDesignExt sdeTo;

    public ShipUpgradeCost(ShipDesignExt sde, ShipDesignExt sdeTo) {       
        this.count = 1;
        this.sde = sde;
        this.sdeTo = sdeTo;
        this.buildTime = new BuildTime();
        
        calcUpgradeCost();
    }    
    
    public ShipUpgradeCost(ShipDesignExt sde, ShipDesignExt sdeTo, int count) {       
        this.count = count;
        this.sde = sde;
        this.sdeTo = sdeTo;
        this.buildTime = new BuildTime();

        calcUpgradeCost();
    }

    private void calcUpgradeCost() {
        // @TODO
        // Compare Modules between sde and sdeTo
        // and calculate BuildTime and RessourceCost
        ShipDesign orgDesign = (ShipDesign)sde.getBase();
        ShipDesign newDesign = (ShipDesign)sdeTo.getBase();

        boolean addChassisReconCost = false;

        if (((orgDesign.getType() == EShipType.TRANSPORT) && (newDesign.getType() != EShipType.TRANSPORT)) ||
            ((orgDesign.getType() != EShipType.TRANSPORT) && (newDesign.getType() == EShipType.TRANSPORT))) {
            addChassisReconCost = true;
        }

        ArrayList<DesignModule> orgModuleList = dmDAO.findByDesignId(orgDesign.getId());                
        ArrayList<DesignModule> newModuleList = dmDAO.findByDesignId(newDesign.getId());  
        
        HashMap<Integer,Integer> oldModules = new HashMap<Integer,Integer>();
        HashMap<Integer,Integer> newModules = new HashMap<Integer,Integer>();
        
        for (DesignModule dm : orgModuleList) {
            oldModules.put(dm.getModuleId(), dm.getCount());
        }
        for (DesignModule dm : newModuleList) {
            newModules.put(dm.getModuleId(), dm.getCount());
        }        
        
        HashMap<Integer,Integer> toAdd = new HashMap<Integer,Integer>();
        HashMap<Integer,Integer> toRemove = new HashMap<Integer,Integer>();        
        
        for (Map.Entry<Integer,Integer> oldEntry : oldModules.entrySet()) {
            int diffCount = 0;
            if (newModules.containsKey(oldEntry.getKey())) {
                diffCount = newModules.get(oldEntry.getKey()) - oldEntry.getValue();
            } else {
                diffCount -= oldEntry.getValue();
            }

            if (diffCount < 0) {
                toRemove.put(oldEntry.getKey(), Math.abs(diffCount));
            } else {
                toAdd.put(oldEntry.getKey(), diffCount);
            }
        }
        
        for (Map.Entry<Integer,Integer> newEntry : newModules.entrySet()) {
            if (!oldModules.containsKey(newEntry.getKey())) {
                toAdd.put(newEntry.getKey(), newEntry.getValue());
            }
        }

        for (Iterator<Map.Entry<Integer,Integer>> removeIt = toRemove.entrySet().iterator();removeIt.hasNext();) {
            Map.Entry<Integer,Integer> removeEntry = removeIt.next();
            if (removeEntry.getValue() == 0) {
                removeIt.remove();
                continue;
            }
        }

        for (Iterator<Map.Entry<Integer,Integer>> addIt = toAdd.entrySet().iterator();addIt.hasNext();) {
            Map.Entry<Integer,Integer> addEntry = addIt.next();
            if (addEntry.getValue() == 0) {
                addIt.remove();
                continue;
            }
        }

        AttributeTree atOld = new AttributeTree(orgDesign.getUserId(),orgDesign.getDesignTime());
        AttributeTree atNew = new AttributeTree(newDesign.getUserId(),newDesign.getDesignTime());
        
        // For removed Modules calculate recycling cost
        for (Map.Entry<Integer,Integer> oldEntry : toRemove.entrySet()) {
            ArrayList<RessAmountEntry> raeList = atOld.getRessourceCost(newDesign.getChassis(), oldEntry.getKey());
            RessourcesEntry re = new RessourcesEntry(raeList);
            
            MutableRessourcesEntry mre = new MutableRessourcesEntry();
            mre.addRess(re, oldEntry.getValue());
            mre.multiplyRessources(count);
            
            RessourcesEntry re2 = new RessourcesEntry(mre.getRessArray());
            this.addRess(re2, 1d);

            buildTime.setDockPoints(buildTime.getDockPoints() + (10 * oldEntry.getValue()) * count);
        }        

        this.multiplyRessources(-0.8d, 0.1d);

        // COST after removing some crap
        /*
        for (RessAmountEntry rae : this.getRessArray()) {
            Logger.getLogger().write("RessId: " + rae.getRessId() + " Qty: " + rae.getQty());
        }
        */
        IdToNameService itns = new IdToNameService(((ShipDesign)sde.getBase()).getUserId());

        // For added modules calculate production cost
        ShipTypeAdjustment sta = staDAO.findByType(newDesign.getType());

        for (Map.Entry<Integer,Integer> newEntry : toAdd.entrySet()) {
            ArrayList<RessAmountEntry> raeList = atNew.getRessourceCost(newDesign.getChassis(), newEntry.getKey());
            RessourcesEntry re = new RessourcesEntry(raeList);
            MutableRessourcesEntry mre = new MutableRessourcesEntry();
            mre.addRess(re);
            mre.multiplyRessources(1d, sta.getCostFactor());

            /*
            Logger.getLogger().write("You just built in " + newEntry.getValue() + " fucking " + moduleDAO.findById(newEntry.getKey()).getName() + " for the price of ");
            for (RessAmountEntry rae : mre.getRessArray()) {
                Logger.getLogger().write("RessId: " + rae.getRessId() + " Qty: " + rae.getQty());
            }
            */
            
            this.addRess(mre, newEntry.getValue() * count);
            
            ModuleAttributeResult mar = atNew.getAllAttributes(newDesign.getChassis(), newEntry.getKey());
            int modConsPoints = (int)mar.getAttributeValue(EAttribute.MOD_CONS_POINTS);
            
            buildTime.setModulePoints(buildTime.getModulePoints() + (modConsPoints * newEntry.getValue()) * count);
        }

        if (addChassisReconCost) {
            // DebugBuffer.warning("Increasing cost due to chassis change");
            Chassis ch = chDAO.findById(newDesign.getChassis());
            ArrayList<RessAmountEntry> raeList = atOld.getRessourceCost(newDesign.getChassis(), ch.getRelModuleId());
            MutableRessourcesEntry mre = new MutableRessourcesEntry();
            mre.addRess(new RessourcesEntry(raeList));
            mre.multiplyRessources(0d, 1d);
            this.addRess(mre);
        }

        // Load chassis multiplicator
        Chassis ch = chDAO.findById(newDesign.getChassis());
        this.multiplyRessources(ch.getMinBuildQty());
    }
    
    public int getCount() {
        return count;
    }

    public BuildTime getBuildTime() {
        return buildTime;
    }
}
