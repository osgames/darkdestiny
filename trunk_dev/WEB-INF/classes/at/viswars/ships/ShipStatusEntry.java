/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

import at.viswars.enumeration.EDamageLevel;

/**
 *
 * @author Stefan
 */
public class ShipStatusEntry {
    private final EDamageLevel status;
    private int count;
    
    public ShipStatusEntry(EDamageLevel status, int count) {
        this.status = status;
        this.count = count;
    }

    public EDamageLevel getStatus() {
        return status;
    }

    public int getCount() {
        return count;
    }
    
    public void increaseCount(int value) {
        count += value;
    }
}
