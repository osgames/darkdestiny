/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

import at.viswars.admin.module.AttributeTree;

/**
 *
 * @author Stefan
 */
public class ModInitParameter {
    public final int chassisId;
    public final int userId;
    public final long time;
    public final AttributeTree at;   
    
    public ModInitParameter(int chassisId, int userId, long time, AttributeTree at) {
        this.chassisId = chassisId;
        this.userId = userId;
        this.time = time;
        this.at = at;
    }
}
