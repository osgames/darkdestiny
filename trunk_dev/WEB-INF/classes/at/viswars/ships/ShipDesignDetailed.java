/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ships;

import at.viswars.Logger.Logger;
import at.viswars.ModuleType;
import at.viswars.admin.module.AttributeTree;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ShipDesignDetailed {

    private final int id;
    private String name;
    private int userId;
    private ShipChassis base;
    private AttributeTree mAttribTree;
    private long designTime;
    private HashMap<ModuleType, HashMap<ShipModule, Integer>> modules = new HashMap<ModuleType, HashMap<ShipModule, Integer>>();

    public ShipDesignDetailed(int id, int chassisModuleId, AttributeTree tree) {
        try {
            Logger.getLogger().write("Create shipdesign " + id + " for chassis " + chassisModuleId);

            this.id = id;
            this.base = new ShipChassis(null, chassisModuleId);
            this.mAttribTree = tree;
            ShipBuilder.createShipDesign(this);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("ERROR: " + e.getMessage());
        }
    }

    public ShipDesignDetailed(int id, int chassisModuleId) {
        try {
            Logger.getLogger().write("Create shipdesign " + id + " for chassis " + chassisModuleId);

            this.id = id;
            this.base = new ShipChassis(null, chassisModuleId);
            ShipBuilder.createShipDesign(this);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("ERROR: " + e.getMessage());
        }
    }

    public ShipDesignDetailed(int id, AttributeTree tree) {
        try {
            this.id = id;
            this.mAttribTree = tree;
            ShipBuilder.createShipDesign(this);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("ERROR: " + e.getMessage());
        }
    }

    public ShipDesignDetailed(int id) {
        try {
            this.id = id;
            ShipBuilder.createShipDesign(this);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("ERROR: " + e.getMessage());
        }
    }

    public int getId() {
        return id;
    }

    protected void addModule(ShipModule sm, int count) {
        if (!modules.containsKey(sm.getType())) {
            HashMap<ShipModule, Integer> moduleList = new HashMap<ShipModule, Integer>();
            moduleList.put(sm, count);
            modules.put(sm.getType(), moduleList);
        } else {
            HashMap<ShipModule, Integer> moduleList = modules.get(sm.getType());
            moduleList.put(sm, count);
        }
    }

    public HashMap<Integer, Integer> getModuleCount() {
        HashMap<Integer, Integer> modCount = new HashMap<Integer, Integer>();

        for (HashMap<ShipModule, Integer> moduleEntry : modules.values()) {
            for (Map.Entry<ShipModule, Integer> entries : moduleEntry.entrySet()) {
                modCount.put(entries.getKey().getId(), entries.getValue());
            }
        }

        return modCount;
    }

    public Weapon getWeapon(int id) {
        HashMap<ShipModule, Integer> weaponModules = modules.get(ModuleType.WEAPON);

        for (Map.Entry<ShipModule, Integer> weaponEntry : weaponModules.entrySet()) {
            if (weaponEntry.getKey().getId() == id) {
                return (Weapon)weaponEntry.getKey();
            }
        }

        return null;
    }

    public Shield getShield(int id) {
        HashMap<ShipModule, Integer> weaponModules = modules.get(ModuleType.SHIELD);

        for (Map.Entry<ShipModule, Integer> weaponEntry : weaponModules.entrySet()) {
            if (weaponEntry.getKey().getId() == id) {
                return (Shield)weaponEntry.getKey();
            }
        }

        return null;
    }

    public SpecialModule getSpecial(int id) {
        HashMap<ShipModule, Integer> specialModules = modules.get(ModuleType.SPECIAL);

        for (Map.Entry<ShipModule, Integer> weaponEntry : specialModules.entrySet()) {
            if (weaponEntry.getKey().getId() == id) {
                return (SpecialModule)weaponEntry.getKey();
            }
        }

        return null;
    }        
    
    public Computer getComputer(int id) {
        HashMap<ShipModule, Integer> computerModules = modules.get(ModuleType.COMPUTER);

        for (Map.Entry<ShipModule, Integer> weaponEntry : computerModules.entrySet()) {
            if (weaponEntry.getKey().getId() == id) {
                return (Computer)weaponEntry.getKey();
            }
        }

        return null;
    }    
    
    public Reactor getReactor(int id) {
        HashMap<ShipModule, Integer> reactorModules = modules.get(ModuleType.REACTOR);

        for (Map.Entry<ShipModule, Integer> weaponEntry : reactorModules.entrySet()) {
            if (weaponEntry.getKey().getId() == id) {
                return (Reactor)weaponEntry.getKey();
            }
        }

        return null;
    }

    public ShipModule getModule(int id) {
        for (Map.Entry<ModuleType, HashMap<ShipModule, Integer>> typeEntry : modules.entrySet()) {
            for (Map.Entry<ShipModule, Integer> moduleEntry : typeEntry.getValue().entrySet()) {
                if (moduleEntry.getKey().getId() == id) {
                    return (ShipModule)moduleEntry.getKey();
                }
            }
        }

        return null;
    }

    public ShipChassis getBase() {
        return base;
    }

    protected void setBase(ShipChassis base) {
        this.base = base;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public AttributeTree getAttributeTree() {
        return mAttribTree;
    }

    protected void buildAttributeTree() {
        this.mAttribTree = new AttributeTree(getUserId(), getDesignTime());
    }

    public int getUserId() {
        return userId;
    }

    protected void setUserId(int userId) {
        this.userId = userId;
    }

    public long getDesignTime() {
        return designTime;
    }

    public boolean needsOrbitalDock() {
        return true;
    }
}
