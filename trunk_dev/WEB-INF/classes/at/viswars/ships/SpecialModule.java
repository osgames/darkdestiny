/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.ships;

/**
 *
 * @author Stefan
 */
public class SpecialModule extends BuiltInModule {
    private double defenseBonus;
    private double decreasedTargetFireEff;
    
    public SpecialModule(ModInitParameter mip, int id) {
        super(mip,id);
    }

    /**
     * @return the defenseBonus
     */
    public double getDefenseBonus() {
        return defenseBonus;
    }

    /**
     * @param defenseBonus the defenseBonus to set
     */
    public void setDefenseBonus(double defenseBonus) {
        this.defenseBonus = defenseBonus;
    }

    /**
     * @return the decreasedTargetFireEff
     */
    public double getDecreasedTargetFireEff() {
        return decreasedTargetFireEff;
    }

    /**
     * @param decreasedTargetFireEff the decreasedTargetFireEff to set
     */
    public void setDecreasedTargetFireEff(double decreasedTargetFireEff) {
        this.decreasedTargetFireEff = decreasedTargetFireEff;
    }
}
