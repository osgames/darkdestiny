/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

/**
 *
 * @author Stefan
 */
public class Armor extends AttachableModule implements Defensive {
    public Armor(ModInitParameter mip, int id) {
        super(mip,id);
    }     
    
    public double getAbsorbtion(Offensive module, ShipDesignDetailed enemy) {
        return 1d;
    }
}
