/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.ships;

/**
 *
 * @author Stefan
 */
public class Reactor extends BuiltInModule {
    private int energyProduction;

    public Reactor(ModInitParameter mip, int id) {
        super(mip,id);
    }    
    
    public int getEnergyProduction() {
        return energyProduction;
    }

    protected void setEnergyProduction(int energyProduction) {
        this.energyProduction = energyProduction;
    }
}
