/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class AbortableResult {

    private final boolean abortable;
    private final ArrayList<String> errors;

    public AbortableResult(boolean abortable, ArrayList<String> errors){
        this.abortable = abortable;
        this.errors = errors;

    }
    public AbortableResult(boolean abortable, String error){
        this.abortable = abortable;
        this.errors = new ArrayList<String>();
        errors.add(error);

    }

    /**
     * @return the destructable
     */
    public boolean isAbortable() {
        return abortable;
    }

    /**
     * @return the errors
     */
    public ArrayList<String> getErrors() {
        return errors;
    }
}
