/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.result;

import at.viswars.Buildable;
import at.viswars.enumeration.EConstructionType;
import at.viswars.model.Construction;
import at.viswars.model.GroundTroop;
import at.viswars.model.ShipDesign;
import at.viswars.service.ConstructionService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class ProductionBuildResult {

    private final HashMap<Buildable, BuildableResult> availUnits = new HashMap<Buildable, BuildableResult>();
    private final TreeMap<Boolean, TreeMap<Object, HashMap<Buildable, BuildableResult>>> availConstructionsSorted = new TreeMap<Boolean, TreeMap<Object, HashMap<Buildable, BuildableResult>>>();

    public ProductionBuildResult() {
    }

    public void addProduction(Buildable b, BuildableResult br) {
        availUnits.put(b, br);
    }

    public void addConstruction(Buildable b, BuildableResult br) {
        TreeMap<Object, HashMap<Buildable, BuildableResult>> result = getAvailConstructionsSorted().get(br.isDeniedByTech() || br.isDeniedByUnique() || br.isDeniedByLandType() || br.isDeniedBySurface());
        if (result == null) {
            result = new TreeMap<Object, HashMap<Buildable, BuildableResult>>();
        }
            if (b.getBase() instanceof GroundTroop) {
                HashMap<Buildable, BuildableResult> entries = result.get(((GroundTroop) b.getBase()).getName());
                if(entries == null){
                    entries = new HashMap<Buildable, BuildableResult>();
                }
                entries.put(b, br);
                result.put(((GroundTroop) b.getBase()).getName(), entries);
            } else if (b.getBase() instanceof ShipDesign) {
                HashMap<Buildable, BuildableResult> entries = result.get(((ShipDesign) b.getBase()).getName());
                if(entries == null){
                    entries = new HashMap<Buildable, BuildableResult>();
                }
                entries.put(b, br);
                result.put(((ShipDesign) b.getBase()).getName(), entries);
            } else if (b.getBase() instanceof Construction) {
                HashMap<Buildable, BuildableResult> entries = result.get(((Construction) b.getBase()).getOrderNo());
                if(entries == null){
                    entries = new HashMap<Buildable, BuildableResult>();
                }
                entries.put(b, br);
                result.put(((Construction) b.getBase()).getOrderNo(), entries);
            }
        // Hierbei handelt es sich um Constructionen die Ausgegraut dargestellt werden sollen
        getAvailConstructionsSorted().put(br.isDeniedByTech() || br.isDeniedByUnique() || br.isDeniedByLandType() || br.isDeniedBySurface(), result);

        }

    public ArrayList<Buildable> getUnits() {
        TreeMap<Object, ArrayList<Buildable>> sorted = sortEntries();

        ArrayList<Buildable> units = new ArrayList<Buildable>();

        for (ArrayList<Buildable> bList : sorted.values()) {
            units.addAll(bList);
        }

        return units;
    }
    // Boolean states if Construction is Buildable by Tech or not

    public TreeMap<Boolean, HashMap<Buildable, BuildableResult>> getConstructionsSorted(int userId, int planetId, EConstructionType type) {
        TreeMap<Boolean, HashMap<Buildable, BuildableResult>> result = new TreeMap<Boolean, HashMap<Buildable, BuildableResult>>();
        TreeMap<Object, ArrayList<Buildable>> sorted = sortEntries();

        ProductionBuildResult pbr = ConstructionService.getConstructionList(userId, planetId, type);

        ArrayList<Buildable> units = new ArrayList<Buildable>();

        for (ArrayList<Buildable> bList : sorted.values()) {
            units.addAll(bList);
        }
        for (Buildable b : units) {

            BuildableResult br = pbr.getBuildInfo(b);
            HashMap<Buildable, BuildableResult> cons = result.get(br.isDeniedByTech());
            if (cons == null) {
                cons = new HashMap<Buildable, BuildableResult>();
            }
            cons.put(b, br);
            result.put(br.isDeniedByTech(), cons);
        }
        return result;
    }

    public ArrayList<Buildable> getUnitsWithPossibleTech() {
        ArrayList<Buildable> units = new ArrayList<Buildable>();

        TreeMap<Object, ArrayList<Buildable>> sorted = sortEntries();

        for (ArrayList<Buildable> bList : sorted.values()) {
            for (Buildable b : bList) {
                if (availUnits.get(b).isDeniedByTech()) {
                    continue;
                }
                if (availUnits.get(b).isNotFinished()) {
                    continue;
                }
                
                units.add(b);
            }
        }

        return units;
    }

    public BuildableResult getBuildInfo(Buildable b) {
        return availUnits.get(b);
    }

    private TreeMap<Object, ArrayList<Buildable>> sortEntries() {
        TreeMap<Object, ArrayList<Buildable>> sorted = new TreeMap<Object, ArrayList<Buildable>>();
        for (Buildable b : availUnits.keySet()) {
            if (b.getBase() instanceof GroundTroop) {               
                int orderKey = ((GroundTroop) b.getBase()).getId();
                if (!sorted.containsKey(orderKey)) {
                    ArrayList<Buildable> elements = new ArrayList<Buildable>();
                    elements.add(b);
                    sorted.put(orderKey, elements);
                } else {
                    ArrayList<Buildable> elements = sorted.get(orderKey);
                    elements.add(b);
                }
            } else if (b.getBase() instanceof ShipDesign) {
                String orderKey = ((ShipDesign) b.getBase()).getName();
                if (!sorted.containsKey(orderKey)) {
                    ArrayList<Buildable> elements = new ArrayList<Buildable>();
                    elements.add(b);
                    sorted.put(orderKey, elements);
                } else {
                    ArrayList<Buildable> elements = sorted.get(orderKey);
                    elements.add(b);
                }
            } else if (b.getBase() instanceof Construction) {
                int orderKey = ((Construction) b.getBase()).getOrderNo();
                if (!sorted.containsKey(orderKey)) {
                    ArrayList<Buildable> elements = new ArrayList<Buildable>();
                    elements.add(b);
                    sorted.put(orderKey, elements);
                } else {
                    ArrayList<Buildable> elements = sorted.get(orderKey);
                    elements.add(b);
                }
            }
        }

        return sorted;
    }

    private TreeMap<Object, Buildable> sortEntries(HashMap<Buildable, BuildableResult> entries) {
        TreeMap<Object, Buildable> sorted = new TreeMap<Object, Buildable>();
        for (Buildable b : entries.keySet()) {
            if (b.getBase() instanceof GroundTroop) {
                sorted.put(((GroundTroop) b.getBase()).getName(), b);
            } else if (b.getBase() instanceof ShipDesign) {
                sorted.put(((ShipDesign) b.getBase()).getName(), b);
            } else if (b.getBase() instanceof Construction) {
                sorted.put(((Construction) b.getBase()).getOrderNo(), b);
            }
        }

        return sorted;
    }

    /**
     * @return the availConstructionsSorted
     */
    public TreeMap<Boolean, TreeMap<Object, HashMap<Buildable, BuildableResult>>> getAvailConstructionsSorted() {
        return availConstructionsSorted;
    }

    /**
     * @return the availConstructionsSorted
     */


}
