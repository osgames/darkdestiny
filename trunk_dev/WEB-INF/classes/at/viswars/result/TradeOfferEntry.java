/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.result;

import at.viswars.GameConstants;
import at.viswars.Logger.Logger;

/**
 *
 * @author Aion
 */
public class TradeOfferEntry {
    
    private int tradePostId;
    private long amount;
    private int ressourceId;
    private double distance;
    private double price;
    private long availOnPlanet;
    private int tradePostLevel;

    private int owningUser;
    private DiplomacyResult diplomacyResult;

    /**
     * @return the tradePostId
     */
    public int getTradePostId() {
        return tradePostId;
    }

    public long getAvailAmount(){
        return Math.min(availOnPlanet, amount);
    }
    
    /**
     * @param tradePostId the tradePostId to set
     */
    public void setAvailOnPlanet(long availOnPlanet) {
        this.availOnPlanet = availOnPlanet;
    }

    /**
     * @return the amount
     */
    public long getAvailOnPlanet() {
        return availOnPlanet;
    }


    /**
     * @param tradePostId the tradePostId to set
     */
    public void setTradePostId(int tradePostId) {
        this.tradePostId = tradePostId;
    }

    /**
     * @return the amount
     */
    public long getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(long amount) {
        this.amount = amount;
    }

    /**
     * @return the ressourceId
     */
    public int getRessourceId() {
        return ressourceId;
    }

    /**
     * @param ressourceId the ressourceId to set
     */
    public void setRessourceId(int ressourceId) {
        this.ressourceId = ressourceId;
    }

    /**
     * @return the distance
     */
    public double getDistance() {
        return distance;
    }
    /**
     * @return the distance
     */
    public String getDistanceIcon() {
        double maxDistance = Math.sqrt(Math.pow(GameConstants.UNIVERSE_HEIGHT,2d) + Math.pow(GameConstants.UNIVERSE_HEIGHT,2d));
        double divisor = 5d;
        double value = maxDistance/divisor;
        String img = "";

        if(distance > (maxDistance - value)){
            //Farthest
            img = "5";
        }else if(distance > (maxDistance - 2d * value)){
            img = "4";
        }else if(distance > (maxDistance - 3d * value)){
            img = "3";
        }else if(distance > (maxDistance - 4d * value)){
            img = "2";
        }else if(distance > (maxDistance - 5d * value)){
            img = "1";
        }
        return img;
    }

    
    public double getLowerBorder(){
          double maxDistance = Math.sqrt(Math.pow(GameConstants.UNIVERSE_HEIGHT,2d) + Math.pow(GameConstants.UNIVERSE_WIDTH,2d));
        double divisor = 20d;
        double value = maxDistance/divisor;

        for(int multiplier = 0; multiplier < (int)divisor; multiplier++){
            if(distance > (value * multiplier) && distance < (value * (multiplier + 1))) {
               return (Math.floor(((value * multiplier)/100d)) * 100d);
            }
            
        }
       return 0d;
    }
    
    public double getHigherBorder(){
          double maxDistance = Math.sqrt(Math.pow(GameConstants.UNIVERSE_HEIGHT,2d) + Math.pow(GameConstants.UNIVERSE_WIDTH,2d));
        double divisor = 20d;
        double value = maxDistance/divisor;

        for(int multiplier = 0; multiplier < (int)divisor; multiplier++){
            if(distance > (value * multiplier) && distance < (value * (multiplier + 1))) {
                return (Math.floor(((value * (multiplier + 1))/100d)) * 100d);
            }
            
        }
       
        return 0d;
    }
    public String getDistanceString() {
        double maxDistance = Math.sqrt(Math.pow(GameConstants.UNIVERSE_HEIGHT,2d) + Math.pow(GameConstants.UNIVERSE_WIDTH,2d));
        double divisor = 20d;
        double value = maxDistance/divisor;

        String distanceString = "";
        for(int multiplier = 0; multiplier < (int)divisor; multiplier++){
            if(distance > (value * multiplier) && distance < (value * (multiplier + 1))) {
                distanceString = (Math.floor(((value * multiplier)/100d)) * 100d) + " - " + (Math.floor(((value * (multiplier + 1))/100d)) * 100d);
                break;
            }
            
        }
       
        return distanceString;
    }
    /**
     * @param distance the distance to set
     */
    public void setDistance(double distance) {
        this.distance = (Math.round(distance * 100d))/100d;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the diplomacyResult
     */
    public DiplomacyResult getDiplomacyResult() {
        return diplomacyResult;
    }

    /**
     * @param diplomacyResult the diplomacyResult to set
     */
    public void setDiplomacyResult(DiplomacyResult diplomacyResult) {
        this.diplomacyResult = diplomacyResult;
    }

    /**
     * @return the tradePostLevel
     */
    public int getTradePostLevel() {
        return tradePostLevel;
    }

    /**
     * @param tradePostLevel the tradePostLevel to set
     */
    public void setTradePostLevel(int tradePostLevel) {
        this.tradePostLevel = tradePostLevel;
    }

    /**
     * @return the owningUser
     */
    public int getOwningUser() {
        return owningUser;
    }

    /**
     * @param owningUser the owningUser to set
     */
    public void setOwningUser(int owningUser) {
        this.owningUser = owningUser;
    }

    
}
