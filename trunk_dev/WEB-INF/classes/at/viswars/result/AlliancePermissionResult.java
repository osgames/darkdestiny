/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.result;

import at.viswars.model.Alliance;
import at.viswars.model.AllianceBoardPermission;
import at.viswars.model.User;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class AlliancePermissionResult {

    private ArrayList<BaseResult> result;
    private ArrayList<AllianceBoardPermission> permissions;
    private HashMap<Integer, User> users;
    private ArrayList<User> usersSorted;
    private ArrayList<Alliance> alliancesSorted;
    private HashMap<Integer, Alliance> alliances;
    private HashMap<Integer, ArrayList<Integer>> allianceMembers;

    /**
     * @return the permissions
     */
    public ArrayList<AllianceBoardPermission> getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(ArrayList<AllianceBoardPermission> permissions) {
        this.permissions = permissions;
    }

    /**
     * @return the users
     */
    public HashMap<Integer, User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(HashMap<Integer, User> users) {
        this.users = users;
    }

    /**
     * @return the alliances
     */
    public HashMap<Integer, Alliance> getAlliances() {
        return alliances;
    }

    /**
     * @param alliances the alliances to set
     */
    public void setAlliances(HashMap<Integer, Alliance> alliances) {
        this.alliances = alliances;
    }

    /**
     * @return the result
     */
    public ArrayList<BaseResult> getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(ArrayList<BaseResult> result) {
        this.result = result;
    }

    /**
     * @return the allianceMembers
     */
    public HashMap<Integer, ArrayList<Integer>> getAllianceMembers() {
        return allianceMembers;
    }

    /**
     * @param allianceMembers the allianceMembers to set
     */
    public void setAllianceMembers(HashMap<Integer, ArrayList<Integer>> allianceMembers) {
        this.allianceMembers = allianceMembers;
    }

    /**
     * @return the usersSorted
     */
    public ArrayList<User> getUsersSorted() {
        return usersSorted;
    }

    /**
     * @param usersSorted the usersSorted to set
     */
    public void setUsersSorted(ArrayList<User> usersSorted) {
        this.usersSorted = usersSorted;
    }

    /**
     * @return the alliancesSorted
     */
    public ArrayList<Alliance> getAlliancesSorted() {
        return alliancesSorted;
    }

    /**
     * @param alliancesSorted the alliancesSorted to set
     */
    public void setAlliancesSorted(ArrayList<Alliance> alliancesSorted) {
        this.alliancesSorted = alliancesSorted;
    }
}
