/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class DestructableResult {


    private final boolean destructable;
    private final ArrayList<String> errors;

    public DestructableResult(boolean destructable, ArrayList<String> errors){
        this.destructable = destructable;
        this.errors = errors;

    }
    public DestructableResult(boolean destructable, String error){
        this.destructable = destructable;
        this.errors = new ArrayList<String>();
        errors.add(error);

    }

    /**
     * @return the buildable
     */
    public boolean isDestructable() {
        return destructable;
    }

    /**
     * @return the errors
     */
    public ArrayList<String> getErrors() {
        return errors;
    }
}
