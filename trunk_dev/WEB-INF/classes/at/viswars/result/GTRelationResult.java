/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.model.GTRelation;
import java.util.HashMap;

/**
 *
 * @author HorstRabe
 */
public class GTRelationResult {

    private HashMap<Integer, GTRelation> gtRelations;
    private HashMap<Integer, Double> damage;
    private double meanDamage;

    /**
     * @return the gtRelations
     */
    public HashMap<Integer, GTRelation> getGtRelations() {
        return gtRelations;
    }

    /**
     * @param gtRelations the gtRelations to set
     */
    public void setGtRelations(HashMap<Integer, GTRelation> gtRelations) {
        this.gtRelations = gtRelations;
    }

    /**
     * @return the damage
     */
    public HashMap<Integer, Double> getDamage() {
        return damage;
    }

    /**
     * @param damage the damage to set
     */
    public void setDamage(HashMap<Integer, Double> damage) {
        this.damage = damage;
    }

    /**
     * @return the meanDamage
     */
    public double getMeanDamage() {
        return meanDamage;
    }

    /**
     * @param meanDamage the meanDamage to set
     */
    public void setMeanDamage(double meanDamage) {
        this.meanDamage = meanDamage;
    }

}
