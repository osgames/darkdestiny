/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

/**
 *
 * @author Bullet
 */
public class GrowthResult {
    private float changevalueGrowth;
    private float maxGrowth;
    private float minGrowth;

    public GrowthResult(float minGrowth, float maxGrowth, float changevalueGrowth){
        this.minGrowth = minGrowth;
        this.maxGrowth = maxGrowth;
        this.changevalueGrowth = changevalueGrowth;
    }

    /**
     * @return the changevalueGrowth
     */
    public float getChangevalueGrowth() {
        return changevalueGrowth;
    }

    /**
     * @param changevalueGrowth the changevalueGrowth to set
     */
    public void setChangevalueGrowth(float changevalueGrowth) {
        this.changevalueGrowth = changevalueGrowth;
    }

    /**
     * @return the maxGrowth
     */
    public float getMaxGrowth() {
        return maxGrowth;
    }

    /**
     * @param maxGrowth the maxGrowth to set
     */
    public void setMaxGrowth(float maxGrowth) {
        this.maxGrowth = maxGrowth;
    }

    /**
     * @return the minGrowth
     */
    public float getMinGrowth() {
        return minGrowth;
    }

    /**
     * @param minGrowth the minGrowth to set
     */
    public void setMinGrowth(float minGrowth) {
        this.minGrowth = minGrowth;
    }
}
