/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.result;

import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class TradeOfferResult {
    private ArrayList<BaseResult> results;
    private ArrayList<TradeOfferEntry> offers;

    public TradeOfferResult(ArrayList<BaseResult> results, ArrayList<TradeOfferEntry> offers) {
        this.results = results;
        this.offers = offers;
    }

    /**
     * @return the results
     */
    public ArrayList<BaseResult> getResults() {
        return results;
    }

    /**
     * @return the offers
     */
    public ArrayList<TradeOfferEntry> getOffers() {
        return offers;
    }
    
    
    
    
}
