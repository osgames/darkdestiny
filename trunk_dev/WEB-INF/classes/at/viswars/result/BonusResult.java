/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

/**
 *
 * @author Dreloc
 */
public class BonusResult {

    private double numericBonus = 0d;
    private double percBonus = 0d;

    public BonusResult(double numericBonus, double percBonus){
        this.numericBonus = numericBonus;
        this.percBonus = percBonus;
    }

    /**
     * @return the numericBonus
     */
    public double getNumericBonus() {
        return numericBonus;
    }

    /**
     * @return the percBonus
     */
    public double getPercBonus() {
        return percBonus;
    }



}
