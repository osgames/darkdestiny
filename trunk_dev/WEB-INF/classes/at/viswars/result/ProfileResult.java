/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class ProfileResult {
private ArrayList<BaseResult> result;
    private boolean invalidateSession;
    private boolean autoLogOut;

    /**
     * @return the result
     */
    public ArrayList<BaseResult> getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(ArrayList<BaseResult> result) {
        this.result = result;
    }

    /**
     * @return the invalidateSession
     */
    public boolean isInvalidateSession() {
        return invalidateSession;
    }

    /**
     * @param invalidateSession the invalidateSession to set
     */
    public void setInvalidateSession(boolean invalidateSession) {
        this.invalidateSession = invalidateSession;
    }

    /**
     * @return the autoLogOut
     */
    public boolean isAutoLogOut() {
        return autoLogOut;
    }

    /**
     * @param autoLogOut the autoLogOut to set
     */
    public void setAutoLogOut(boolean autoLogOut) {
        this.autoLogOut = autoLogOut;
    }
}
