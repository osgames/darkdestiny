/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

/**
 *
 * @author Stefan
 */
public class RegroupResult extends BaseResult {
    private final boolean deleted;

    public RegroupResult(String message, boolean error) {
        super(message,error);
        deleted = false;
    }    
    
    public RegroupResult(String message, boolean error, boolean fleetDeleted) {
        super(message,error);
        deleted = fleetDeleted;
    }

    public boolean isDeleted() {
        return deleted;
    }
}
