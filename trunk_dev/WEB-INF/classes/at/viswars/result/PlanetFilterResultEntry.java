/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.result;

import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;
import at.viswars.construction.InConstructionData;
import at.viswars.enumeration.EFilterComparator;
import at.viswars.enumeration.EFilterValue;
import at.viswars.enumeration.EFilterValueType;
import at.viswars.model.Filter;
import at.viswars.model.FilterToValue;
import at.viswars.model.FilterValue;
import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import at.viswars.model.Ressource;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import at.viswars.planetcalc.ProductionResult;
import at.viswars.service.AdministrationService;
import at.viswars.service.ResearchService;
import at.viswars.service.Service;
import at.viswars.utilities.FilterUtilities;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author Aion
 */
public class PlanetFilterResultEntry extends Service {

    HashMap<EFilterValue, Object> values;
    private Filter filter;
    private PlayerPlanet pp;
    private PlanetCalcResult pcr;
    private ProductionResult pr;
    private PlanetCalculation pc;
    private ExtPlanetCalcResult epcr;
    private ResearchResult rr;
    private Planet p;

    public PlanetFilterResultEntry(int filterId, int pId) {
        try {
            pp = AdministrationService.findPlayerPlanet(pId);
            p = Service.planetDAO.findById(pId);

            rr = ResearchService.getResearchPointsForPlanet(pId);
            pc = new PlanetCalculation(pId);
            pcr = pc.getPlanetCalcData();
            pr = pcr.getProductionData();
            epcr = pcr.getExtPlanetCalcData();


            values = new HashMap<EFilterValue, Object>();

            values.put(EFilterValue.POPULATION_VALUE, pp.getPopulation());
            values.put(EFilterValue.MIGRATION_VALUE, pp.getMigration());
            values.put(EFilterValue.VISIBLE_FLAG, pp.getInvisibleFlag());
            values.put(EFilterValue.MORAL_VALUE, pp.getMoral());

            double energyUsage = 0d;
            double energyFree = 0d;
            energyUsage = ((double) pr.getRessConsumption(Ressource.ENERGY) / (double) pr.getRessProduction(Ressource.ENERGY)) * 100d;
            energyFree = 100 - energyUsage;
            values.put(EFilterValue.ENERGY_PERCENTAGE_USAGE, Math.round(energyUsage));
            values.put(EFilterValue.ENERGY_PERCENTAGE_FREE, Math.round(energyFree));
            LinkedList<InConstructionData> icds = at.viswars.utilities.ConstructionUtilities.getInConstructionData(pId);

            if (icds.size() > 0) {
                values.put(EFilterValue.HAS_IN_CONSTRUCTION, true);
            } else {
                values.put(EFilterValue.HAS_IN_CONSTRUCTION, false);
            }
            if (p.getLandType().equals(Planet.LANDTYPE_A) || p.getLandType().equals(Planet.LANDTYPE_B)) {
                values.put(EFilterValue.HOSTILE_GASEOUS, true);
            } else if (p.getLandType().equals(Planet.LANDTYPE_C) || p.getLandType().equals(Planet.LANDTYPE_M)
                    || p.getLandType().equals(Planet.LANDTYPE_G)) {
                values.put(EFilterValue.HABITABLE, true);
            } else if (p.getLandType().equals(Planet.LANDTYPE_I) || p.getLandType().equals(Planet.LANDTYPE_J)
                    || p.getLandType().equals(Planet.LANDTYPE_L) || p.getLandType().equals(Planet.LANDTYPE_E)) {
                values.put(EFilterValue.HOSTILE_COMPACT, true);
            }


            filter = filterDAO.findById(filterId);
        } catch (Exception e) {
            DebugBuffer.error("Error in PlanetFilterResultEntry : " + e);
        }

    }

    /**
     * @return the show
     */
    public boolean isShow() {

        if (filter != null) {
            for (FilterToValue ftv : filterToValueDAO.findByFilterId(filter.getId())) {
                FilterValue fv = filterValueDAO.findById(ftv.getValueId());

                if (values.containsKey(fv.getValue())) {
                    if (!compare((Object) ftv.getValue(), values.get(fv.getValue()), fv.getType(), ftv.getComparator())) {
                        return false;
                    }
                } else if (fv.getType() == EFilterValueType.BOOLEAN) {
                    //Closed world assumption
                    return false;
                } else {
                    DebugBuffer.addLine(DebugLevel.ERROR, "A Filtervalue : '" + fv.getName() + "' has been set in the Filter but cannot be found in the planetValues");

                }
            }
        }

        boolean show = true;

        return show;
    }

    private boolean compare(Object obj1, Object obj2, EFilterValueType type, EFilterComparator comparator) {
        int compareResult = 0;
        if (type.equals(EFilterValueType.LONG)) {
            Long value1 = Long.parseLong((String) obj1);
            Long value2 = Long.parseLong(String.valueOf(obj2));
            compareResult = value1.compareTo(value2);
        }
        if (type.equals(EFilterValueType.INTEGER)) {
            Integer value1 = Integer.parseInt((String) obj1);
            Integer value2 = Integer.parseInt(String.valueOf(obj2));
            compareResult = value1.compareTo(value2);
        }
        if (type.equals(EFilterValueType.DOUBLE)) {
            Double value1 = Double.parseDouble((String) obj1);
            Double value2 = Double.parseDouble(String.valueOf(obj2));
            compareResult = value1.compareTo(value2);
        }
        if (type.equals(EFilterValueType.FLOAT)) {
            Float value1 = Float.parseFloat((String) obj1);
            Float value2 = Float.parseFloat(String.valueOf(obj2));
            compareResult = value1.compareTo(value2);
        }
        if (type.equals(EFilterValueType.BOOLEAN)) {
            Boolean value1 = false;
            if (Integer.parseInt(((String) obj1)) == 1) {
                value1 = true;
            }
            Boolean value2 = (Boolean) (obj2);
            compareResult = value1.compareTo(value2);
            //    Logger.getLogger().write("Boolean value : " + value1 + " " + FilterUtilities.getComparatorString(comparator) + " " + value2);
            //    Logger.getLogger().write("Result value : " + compareResult);

        }
        if ((comparator == EFilterComparator.EQUALS || comparator == EFilterComparator.BIGGEREQUALS
                || comparator == EFilterComparator.SMALLEREQUALS) && compareResult == 0) {
            return true;
        }
        if ((comparator == EFilterComparator.BIGGER || comparator == EFilterComparator.BIGGEREQUALS)
                && compareResult == -1) {
            return true;
        }
        if ((comparator == EFilterComparator.SMALLER || comparator == EFilterComparator.SMALLEREQUALS)
                && compareResult == 1) {
            return true;
        }


        return false;
    }

    /**
     * @return the pp
     */
    public PlayerPlanet getPp() {
        return pp;
    }

    /**
     * @return the pcr
     */
    public PlanetCalcResult getPcr() {
        return pcr;
    }

    /**
     * @return the pr
     */
    public ProductionResult getPr() {
        return pr;
    }

    /**
     * @return the epcr
     */
    public ExtPlanetCalcResult getEpcr() {
        return epcr;
    }

    /**
     * @return the rr
     */
    public ResearchResult getRr() {
        return rr;
    }

    /**
     * @return the pc
     */
    public PlanetCalculation getPc() {
        return pc;
    }

    /**
     * @return the p
     */
    public Planet getP() {
        return p;
    }
}
