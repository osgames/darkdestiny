/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.enumeration.ECombatStatus;
import at.viswars.model.CombatRound;
import at.viswars.model.RoundEntry;
import at.viswars.model.TerritoryEntry;
import at.viswars.model.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author Aion
 */
public class GroundCombatResult {
    private ECombatStatus status;
    private int rounds;
    private long startTime;
    private int planetId;
    private int planetOwner;
    private int winner = 0;
    private int userId;
    private ArrayList<CombatRound> combatRounds;
    private TreeMap<Integer, User> users;
    private HashMap<Integer, ArrayList<RoundEntry>> startUnits = new HashMap<Integer, ArrayList<RoundEntry>>();
    private HashMap<Integer, ArrayList<RoundEntry>> endUnits = new HashMap<Integer, ArrayList<RoundEntry>>();
    private TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> history;
    private TreeMap<Integer, TreeMap<Integer, RoundEntry>> result;
    private HashMap<Integer, HashMap<Integer, TerritoryEntry>> territoryUser;
    private StringBuffer report = new StringBuffer();
    private TreeMap<Integer, ArrayList<Integer>> participatingTypes;
    private ArrayList<Integer> participatingTypesTotal;
    private JFreeChart chart;

    /**
     * @return the status
     */
    public ECombatStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(ECombatStatus status) {
        this.status = status;
    }

    /**
     * @return the rounds
     */
    public int getRounds() {
        return rounds;
    }

    /**
     * @param rounds the rounds to set
     */
    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    /**
     * @return the planetOwner
     */
    public int getPlanetOwner() {
        return planetOwner;
    }

    /**
     * @param planetOwner the planetOwner to set
     */
    public void setPlanetOwner(int planetOwner) {
        this.planetOwner = planetOwner;
    }

    /**
     * @return the winner
     */
    public int getWinner() {
        return winner;
    }

    /**
     * @param winner the winner to set
     */
    public void setWinner(int winner) {
        this.winner = winner;
    }

    /**
     * @return the startUnits
     */
    public HashMap<Integer, ArrayList<RoundEntry>> getStartUnits() {
        return startUnits;
    }

    /**
     * @param startUnits the startUnits to set
     */
    public void setStartUnits(HashMap<Integer, ArrayList<RoundEntry>> startUnits) {
        this.startUnits = startUnits;
    }

    /**
     * @return the endUnits
     */
    public HashMap<Integer, ArrayList<RoundEntry>> getEndUnits() {
        return endUnits;
    }

    /**
     * @param endUnits the endUnits to set
     */
    public void setEndUnits(HashMap<Integer, ArrayList<RoundEntry>> endUnits) {
        this.endUnits = endUnits;
    }


    /**
     * @return the planetId
     */
    public int getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(int planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the territoryUser
     */
    public HashMap<Integer, HashMap<Integer, TerritoryEntry>> getTerritoryUser() {
        return territoryUser;
    }

    /**
     * @param territoryUser the territoryUser to set
     */
    public void setTerritoryUser(HashMap<Integer, HashMap<Integer, TerritoryEntry>> territoryUser) {
        this.territoryUser = territoryUser;
    }


    /**
     * @return the report
     */
    public StringBuffer getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(StringBuffer report) {
        this.report = report;
    }

    /**
     * @return the users
     */
    public TreeMap<Integer, User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(TreeMap<Integer, User> users) {
        this.users = users;
    }

    /**
     * @return the startTime
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the history
     */
    public TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> getHistory() {
        return history;
    }

    /**
     * @param history the history to set
     */
    public void setHistory(TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> history) {
        this.history = history;
    }

    /**
     * @return the result
     */
    public TreeMap<Integer, TreeMap<Integer, RoundEntry>> getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(TreeMap<Integer, TreeMap<Integer, RoundEntry>> result) {
        this.result = result;
    }

    /**
     * @return the participatingTypes
     */
    public TreeMap<Integer, ArrayList<Integer>> getParticipatingTypes() {
        return participatingTypes;
    }

    /**
     * @param participatingTypes the participatingTypes to set
     */
    public void setParticipatingTypes(TreeMap<Integer, ArrayList<Integer>> participatingTypes) {
        this.participatingTypes = participatingTypes;
    }

    /**
     * @return the combatRounds
     */
    public ArrayList<CombatRound> getCombatRounds() {
        return combatRounds;
    }

    /**
     * @param combatRounds the combatRounds to set
     */
    public void setCombatRounds(ArrayList<CombatRound> combatRounds) {
        this.combatRounds = combatRounds;
    }

    /**
     * @return the participatingTypesTotal
     */
    public ArrayList<Integer> getParticipatingTypesTotal() {
        return participatingTypesTotal;
    }

    /**
     * @param participatingTypesTotal the participatingTypesTotal to set
     */
    public void setParticipatingTypesTotal(ArrayList<Integer> participatingTypesTotal) {
        this.participatingTypesTotal = participatingTypesTotal;
    }

    /**
     * @return the chart
     */
    public JFreeChart getChart() {
        return chart;
    }

    /**
     * @param chart the chart to set
     */
    public void setChart(JFreeChart chart) {
        this.chart = chart;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }



}
