/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.DebugBuffer;
import at.viswars.model.Action;
import at.viswars.model.Model;
import at.viswars.model.ProductionOrder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class InConstructionResult {
    //Priority InConstructionEntry
    private TreeMap<Integer, ArrayList<InConstructionEntry>> inCons = new TreeMap<Integer, ArrayList<InConstructionEntry>>();
    
    public InConstructionResult() {
        
    }
    
    public void addEntry(ProductionOrder po, Action a, Model unit) {
        InConstructionEntry ice = new InConstructionEntry(po,a,unit);

        if (po == null) {
            DebugBuffer.warning("PO is null in addEntry (InConstructionResult)");
            return;
        }

        ArrayList<InConstructionEntry> entries = inCons.get(po.getPriority());
        if(entries == null){
            entries = new ArrayList<InConstructionEntry>();
        }
        entries.add(ice);
        inCons.put(po.getPriority(), entries);
    }
        public void addEntry(InConstructionEntry ice) {

        ArrayList<InConstructionEntry> entries = inCons.get(ice.getOrder().getPriority());
        if(entries == null){
            entries = new ArrayList<InConstructionEntry>();
        }
        entries.add(ice);
        inCons.put(ice.getOrder().getPriority(), entries);
    }
    public ArrayList<InConstructionEntry> getEntries() {
        ArrayList<InConstructionEntry> result = new ArrayList<InConstructionEntry>();
        for(Map.Entry<Integer, ArrayList<InConstructionEntry>> entry : inCons.entrySet()){
            result.addAll(entry.getValue());
        }
        return result;
    }    
    public void addEntries(Collection<InConstructionEntry> entries) {
       for(InConstructionEntry ice : entries){
           addEntry(ice);
       }
    }
}
