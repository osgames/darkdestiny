/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.model.Alliance;
import at.viswars.model.DiplomacyType;
import at.viswars.model.User;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author Aion
 */
public class RelationResult {
        private HashMap<User, LinkedList<DiplomacyType>> userRelations = new HashMap<User, LinkedList<DiplomacyType>>();
    private HashMap<Alliance, LinkedList<DiplomacyType>> userAllianceRelations = new HashMap<Alliance, LinkedList<DiplomacyType>>();
    private HashMap<User, LinkedList<DiplomacyType>> allianceUserRelations = new HashMap<User, LinkedList<DiplomacyType>>();
    private HashMap<Alliance, LinkedList<DiplomacyType>> AllianceRelations = new HashMap<Alliance, LinkedList<DiplomacyType>>();

        public RelationResult(){

        }

    /**
     * @return the userRelations
     */
    public HashMap<User, LinkedList<DiplomacyType>> getUserRelations() {
        return userRelations;
    }

    /**
     * @param userRelations the userRelations to set
     */
    public void setUserRelations(HashMap<User, LinkedList<DiplomacyType>> userRelations) {
        this.userRelations = userRelations;
    }

    /**
     * @return the userAllianceRelations
     */
    public HashMap<Alliance, LinkedList<DiplomacyType>> getUserAllianceRelations() {
        return userAllianceRelations;
    }

    /**
     * @param userAllianceRelations the userAllianceRelations to set
     */
    public void setUserAllianceRelations(HashMap<Alliance, LinkedList<DiplomacyType>> userAllianceRelations) {
        this.userAllianceRelations = userAllianceRelations;
    }

    /**
     * @return the allianceUserRelations
     */
    public HashMap<User, LinkedList<DiplomacyType>> getAllianceUserRelations() {
        return allianceUserRelations;
    }

    /**
     * @param allianceUserRelations the allianceUserRelations to set
     */
    public void setAllianceUserRelations(HashMap<User, LinkedList<DiplomacyType>> allianceUserRelations) {
        this.allianceUserRelations = allianceUserRelations;
    }

    /**
     * @return the AllianceRelations
     */
    public HashMap<Alliance, LinkedList<DiplomacyType>> getAllianceRelations() {
        return AllianceRelations;
    }

    /**
     * @param AllianceRelations the AllianceRelations to set
     */
    public void setAllianceRelations(HashMap<Alliance, LinkedList<DiplomacyType>> AllianceRelations) {
        this.AllianceRelations = AllianceRelations;
    }


}
