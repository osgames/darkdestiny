/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.buildable.ConstructionExt;
import at.viswars.construction.ConstructionUpgradeCost;
import at.viswars.model.PlanetConstruction;
import at.viswars.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Orlov
 */
public class ConstructionUpgradeResult extends BaseResult{

    private ConstructionUpgradeCost constructionUpgradeCost;
    private ConstructionExt consExt;
    private ConstructionExt consExtTarget;
    private final boolean buildable;
    private final int possibleCount;    
    private ArrayList<String> missingRequirements;
    
    private boolean deniedByTech = false;
    private boolean deniedByUnique = false;
    private boolean deniedByLandType = false;
    private boolean deniedBySurface = false;
    private boolean deniedByOutdated = false;
    private boolean deniedByLockedTech = false;
    private boolean possibleByPopActionPoints = true;
    private boolean deniedByMineableRessource = false;
    private boolean notFinished = false;
   

    public ConstructionUpgradeResult(boolean buildable, int constructionId, int targetConstruction, int planetId, int count, ArrayList<String> missingRequirements){
        super(false);
        this.buildable = buildable;
        this.missingRequirements = missingRequirements;
        this.possibleCount = count;
        consExt = new ConstructionExt(constructionId);
        consExtTarget = new ConstructionExt(targetConstruction);
        
        if(buildable){
        constructionUpgradeCost = new ConstructionUpgradeCost(consExt, consExtTarget,planetId,count);
        }
    }
    
    /**
     * @return the constructionImproveCost
     */
    public ConstructionUpgradeCost getConstructionUpgradeCost() {
        return constructionUpgradeCost;
    }

    /**
     * @return the consExt
     */
    public ConstructionExt getConsExt() {
        return consExt;
    }

    /**
     * @return the consExtTarget
     */
    public ConstructionExt getConsExtTarget() {
        return consExtTarget;
    }

    /**
     * @return the possibleCount
     */
    public int getPossibleCount() {
        return possibleCount;
    }
    
    public boolean isBuildable(){
        return buildable;
    }

    /**
     * @param missingRequirements the missingRequirements to set
     */
    public void setMissingRequirements(ArrayList<String> missingRequirements) {
        this.missingRequirements = missingRequirements;
    }

    /**
     * @param deniedByTech the deniedByTech to set
     */
    public void setDeniedByTech(boolean deniedByTech) {
        this.deniedByTech = deniedByTech;
    }

    /**
     * @param deniedByUnique the deniedByUnique to set
     */
    public void setDeniedByUnique(boolean deniedByUnique) {
        this.deniedByUnique = deniedByUnique;
    }

    /**
     * @param deniedByLandType the deniedByLandType to set
     */
    public void setDeniedByLandType(boolean deniedByLandType) {
        this.deniedByLandType = deniedByLandType;
    }

    /**
     * @param deniedBySurface the deniedBySurface to set
     */
    public void setDeniedBySurface(boolean deniedBySurface) {
        this.deniedBySurface = deniedBySurface;
    }

    /**
     * @param deniedByOutdated the deniedByOutdated to set
     */
    public void setDeniedByOutdated(boolean deniedByOutdated) {
        this.deniedByOutdated = deniedByOutdated;
    }

    /**
     * @param possibleByPopActionPoints the possibleByPopActionPoints to set
     */
    public void setPossibleByPopActionPoints(boolean possibleByPopActionPoints) {
        this.possibleByPopActionPoints = possibleByPopActionPoints;
    }

    /**
     * @param deniedByMineableRessource the deniedByMineableRessource to set
     */
    public void setDeniedByMineableRessource(boolean deniedByMineableRessource) {
        this.deniedByMineableRessource = deniedByMineableRessource;
    }

    /**
     * @param notFinished the notFinished to set
     */
    public void setNotFinished(boolean notFinished) {
        this.notFinished = notFinished;
    }

    /**
     * @return the deniedByTech
     */
    public boolean isDeniedByTech() {
        return deniedByTech;
    }

    /**
     * @return the deniedByUnique
     */
    public boolean isDeniedByUnique() {
        return deniedByUnique;
    }

    /**
     * @return the deniedByLandType
     */
    public boolean isDeniedByLandType() {
        return deniedByLandType;
    }

    /**
     * @return the deniedBySurface
     */
    public boolean isDeniedBySurface() {
        return deniedBySurface;
    }

    /**
     * @return the deniedByOutdated
     */
    public boolean isDeniedByOutdated() {
        return deniedByOutdated;
    }

    /**
     * @return the possibleByPopActionPoints
     */
    public boolean isPossibleByPopActionPoints() {
        return possibleByPopActionPoints;
    }

    /**
     * @return the deniedByMineableRessource
     */
    public boolean isDeniedByMineableRessource() {
        return deniedByMineableRessource;
    }

    /**
     * @return the notFinished
     */
    public boolean isNotFinished() {
        return notFinished;
    }

    /**
     * @return the missingRequirements
     */
    public ArrayList<String> getMissingRequirements() {
        return missingRequirements;
    }

    /**
     * @param deniedByLockedTech the deniedByLockedTech to set
     */
    public void setDeniedByLockedTech(boolean deniedByLockedTech) {
        this.deniedByLockedTech = deniedByLockedTech;
    }

    /**
     * @return the deniedByLockedTech
     */
    public boolean isDeniedByLockedTech() {
        return deniedByLockedTech;
    }
}
