/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ShipDesignResult {
    private final boolean success;
    private final ArrayList<String> errors;
    
    public ShipDesignResult(boolean success, ArrayList<String> errors) {
        this.success = success;
        this.errors = errors;
    }

    public boolean isSuccess() {
        return success;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }
}
