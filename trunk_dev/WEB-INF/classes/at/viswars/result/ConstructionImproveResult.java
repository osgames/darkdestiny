/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.buildable.ConstructionExt;
import at.viswars.construction.ConstructionImproveCost;
import at.viswars.model.PlanetConstruction;
import at.viswars.service.Service;

/**
 *
 * @author Orlov
 */
public class ConstructionImproveResult extends BaseResult{

    private ConstructionImproveCost constructionImproveCost;
    private ConstructionExt consExt;
    private int level;

    public ConstructionImproveResult(int constructionId, int planetId){
        super(false);
        consExt = new ConstructionExt(constructionId);
        PlanetConstruction pc = Service.planetConstructionDAO.findBy(planetId, constructionId);
        if(pc == null){
            throw new IllegalStateException("Wanna Improve non existing Building");
        }else{
            level = pc.getLevel();
        }
        constructionImproveCost = new ConstructionImproveCost(consExt,planetId);
    }
    public int getLevel(){
        return level;
    }

    /**
     * @return the constructionImproveCost
     */
    public ConstructionImproveCost getConstructionImproveCost() {
        return constructionImproveCost;
    }

    /**
     * @return the consExt
     */
    public ConstructionExt getConsExt() {
        return consExt;
    }
}
