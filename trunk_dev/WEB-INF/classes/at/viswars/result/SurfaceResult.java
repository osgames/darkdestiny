/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

/**
 *
 * @author Eobane
 */
public class SurfaceResult {

    private int surface;
    private int freeSurface;

    public SurfaceResult(int surface, int freeSurface){

        this.surface = surface;
        this.freeSurface = freeSurface;
    }

    /**
     * @return the surface
     */
    public int getSurface() {
        return surface;
    }

    /**
     * @return the freeSurface
     */
    public int getFreeSurface() {
        return freeSurface;
    }

}
