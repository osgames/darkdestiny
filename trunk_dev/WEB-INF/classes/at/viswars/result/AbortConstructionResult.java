/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.construction.ConstructionAbortRefund;
import at.viswars.model.Action;
import at.viswars.model.Construction;

/**
 *
 * @author Stefan
 */
public class AbortConstructionResult extends BaseResult {
    private final ConstructionAbortRefund consAbortRefund;
    private final Action actionEntry;
    private final Construction cons;

    public AbortConstructionResult(Construction cons, Action actionEntry, ConstructionAbortRefund consAbortRefund) {
        super(false);

        this.consAbortRefund = consAbortRefund;
        this.actionEntry = actionEntry;
        this.cons = cons;
    }

    public AbortConstructionResult(String error) {
        super(error,true);

        this.consAbortRefund = null;
        this.actionEntry = null;
        this.cons = null;
    }

    /**
     * @return the consAbortRefund
     */
    public ConstructionAbortRefund getConsAbortRefund() {
        return consAbortRefund;
    }

    /**
     * @return the actionEntry
     */
    public Action getActionEntry() {
        return actionEntry;
    }

    /**
     * @return the cons
     */
    public Construction getCons() {
        return cons;
    }
}
