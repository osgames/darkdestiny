/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.dao.AllianceDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.model.Alliance;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class AllianceStructureResult {
    private final int masterAllianceId;
    private final ArrayList<Alliance> subAlliances;
    private final ArrayList<Alliance> equallyAllied;

    public static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);

    public AllianceStructureResult(int masterAllianceId, ArrayList<Alliance> subAlliances, ArrayList<Alliance> equallyAllied) {
        this.masterAllianceId = masterAllianceId;
        this.subAlliances = subAlliances;
        this.equallyAllied = equallyAllied;
    }

    public ArrayList<Alliance> getSubAlliancesFor(int alliance) {
        ArrayList<Alliance> alliances = new ArrayList<Alliance>();

        for (Alliance subAlliance : subAlliances) {
            if (subAlliance.getIsSubAllianceOf() == alliance) {
                alliances.add(subAlliance);
            }
        }

        return alliances;
    }

    public ArrayList<Alliance> getTopLevel() {
        ArrayList<Alliance> alliances = new ArrayList<Alliance>();

        alliances.add(aDAO.findById(masterAllianceId));
        alliances.addAll(equallyAllied);
        
        return alliances;
    }

    public boolean display() {
        return (!subAlliances.isEmpty() || !equallyAllied.isEmpty());
    }
}
