/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.result;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class RegisterResult {
 
    
   private final boolean ableToRegister;
   private final ArrayList<BaseResult> results;

    public RegisterResult(boolean ableToRegister, ArrayList<BaseResult> results) {
        this.ableToRegister = ableToRegister;
        this.results = results;
    }

    /**
     * @return the ableToRegister
     */
    public boolean isAbleToRegister() {
        return ableToRegister;
    }

    /**
     * @return the results
     */
    public ArrayList<BaseResult> getResults() {
        return results;
    }
    
   
}
