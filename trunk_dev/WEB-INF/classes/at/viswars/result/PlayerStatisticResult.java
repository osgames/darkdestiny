/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.model.Statistic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class PlayerStatisticResult {
    private TreeMap<Integer, ArrayList<Statistic>> playerStats;
    private HashMap<Integer, String> userNames;

  

    /**
     * @return the userNames
     */
    public HashMap<Integer, String> getUserNames() {
        return userNames;
    }

    /**
     * @param userNames the userNames to set
     */
    public void setUserNames(HashMap<Integer, String> userNames) {
        this.userNames = userNames;
    }

    /**
     * @return the playerStats
     */
    public TreeMap<Integer, ArrayList<Statistic>> getPlayerStats() {
        return playerStats;
    }

    /**
     * @param playerStats the playerStats to set
     */
    public void setPlayerStats(TreeMap<Integer, ArrayList<Statistic>> playerStats) {
        this.playerStats = playerStats;
    }
}
