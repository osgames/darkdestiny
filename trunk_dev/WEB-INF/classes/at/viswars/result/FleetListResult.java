/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.result;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.SystemDAO;
import at.viswars.databuffer.fleet.AbsoluteCoordinate;
import at.viswars.movable.FleetFormationExt;
import at.viswars.movable.IFleet;
import at.viswars.movable.PlayerFleetExt;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class FleetListResult {

    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private final ArrayList<FleetFormationExt> formations =
            new ArrayList<FleetFormationExt>();
    private final ArrayList<PlayerFleetExt> singleFleets =
            new ArrayList<PlayerFleetExt>();

    public void addFleetFormation(FleetFormationExt ffe) {
        formations.add(ffe);
    }

    public void addSingleFleet(PlayerFleetExt pfe) {
        singleFleets.add(pfe);
    }

    public ArrayList<FleetFormationExt> getFleetFormations() {
        return formations;
    }

    public ArrayList<PlayerFleetExt> getSingleFleets() {
        return singleFleets;
    }

    public ArrayList<PlayerFleetExt> getSingleFleetsSorted(ESortType type, int systemId) {
        if (type.equals(ESortType.ID)) {
            TreeMap<Integer, TreeMap<Integer, ArrayList<PlayerFleetExt>>> sorted = new TreeMap<Integer, TreeMap<Integer, ArrayList<PlayerFleetExt>>>();
            for (PlayerFleetExt pfe : singleFleets) {
                TreeMap<Integer, ArrayList<PlayerFleetExt>> entry1 = null;
                if (!pfe.isMoving()) {
                    entry1 = sorted.get(pfe.getRelativeCoordinate().getSystemId());
                } else {
                    entry1 = sorted.get(pfe.getTargetRelativeCoordinate().getSystemId());
                }
                if (entry1 == null) {
                    entry1 = new TreeMap<Integer, ArrayList<PlayerFleetExt>>();
                }
                ArrayList<PlayerFleetExt> fleets = null;

                if (!pfe.isMoving()) {
                    fleets = entry1.get(pfe.getRelativeCoordinate().getPlanetId());
                } else {
                    fleets = entry1.get(pfe.getTargetRelativeCoordinate().getPlanetId());
                }
                if (fleets == null) {
                    fleets = new ArrayList<PlayerFleetExt>();
                }
                fleets.add(pfe);
                if (!pfe.isMoving()) {
                    entry1.put(pfe.getRelativeCoordinate().getPlanetId(), fleets);
                    sorted.put(pfe.getRelativeCoordinate().getSystemId(), entry1);
                } else {
                    entry1.put(pfe.getTargetRelativeCoordinate().getPlanetId(), fleets);
                    sorted.put(pfe.getTargetRelativeCoordinate().getSystemId(), entry1);
                }
            }
            singleFleets.clear();
            for (Map.Entry<Integer, TreeMap<Integer, ArrayList<PlayerFleetExt>>> sort1 : sorted.entrySet()) {
                for (Map.Entry<Integer, ArrayList<PlayerFleetExt>> sort2 : sort1.getValue().entrySet()) {
                    singleFleets.addAll(sort2.getValue());
                }
            }
        } else if (type.equals(ESortType.NAME)) {
            TreeMap<String, ArrayList<PlayerFleetExt>> sorted = new TreeMap<String, ArrayList<PlayerFleetExt>>();
            for (PlayerFleetExt pfe : singleFleets) {
                ArrayList<PlayerFleetExt> entry1 = sorted.get(pfe.getName());
                if (entry1 == null) {
                    entry1 = new ArrayList<PlayerFleetExt>();
                }
                entry1.add(pfe);
                sorted.put(pfe.getName(), entry1);
            }
            singleFleets.clear();
            for (Map.Entry<String, ArrayList<PlayerFleetExt>> sort1 : sorted.entrySet()) {

                singleFleets.addAll(sort1.getValue());

            }
        } else if (type.equals(ESortType.DISTANCE)) {
            at.viswars.model.System s = sDAO.findById(systemId);
            AbsoluteCoordinate ac = new AbsoluteCoordinate(s.getX(), s.getY());
            TreeMap<Double, ArrayList<PlayerFleetExt>> sorted = new TreeMap<Double, ArrayList<PlayerFleetExt>>();
            for (PlayerFleetExt pfe : singleFleets) {
                Double d = ac.distanceTo(pfe.getAbsoluteCoordinate());
                ArrayList<PlayerFleetExt> entry1 = sorted.get(d);
                if (entry1 == null) {
                    entry1 = new ArrayList<PlayerFleetExt>();
                }
                entry1.add(pfe);
                sorted.put(d, entry1);
            }
            singleFleets.clear();
            for (Map.Entry<Double, ArrayList<PlayerFleetExt>> sort1 : sorted.entrySet()) {
                singleFleets.addAll(sort1.getValue());
            }
        }
        return singleFleets;
    }

    public ArrayList<PlayerFleetExt> getAllContainedFleets() {
        ArrayList<PlayerFleetExt> allFleets = new ArrayList<PlayerFleetExt>();
        allFleets.addAll(singleFleets);

        for (FleetFormationExt ffe : formations) {
            allFleets.addAll(ffe.getParticipatingFleets());
        }

        return allFleets;
    }

    public ArrayList<IFleet> getAllItems() {
        ArrayList<IFleet> items = new ArrayList<IFleet>();
        items.addAll(formations);
        items.addAll(singleFleets);
        return items;
    }

    public int getFormationCount() {
        return formations.size();
    }

    public int getSingleFleetCount() {
        return singleFleets.size();
    }

    public int getFormationAndSingleFleetCount() {
        return getFormationCount() + getSingleFleetCount();
    }

    public enum ESortType {

        NAME, ID, DISTANCE
    }
}
