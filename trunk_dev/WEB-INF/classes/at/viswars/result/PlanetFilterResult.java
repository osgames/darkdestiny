/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class PlanetFilterResult {
    private int items;
    private int totalItems;
    // private TreeMap<Integer, PlanetFilterResultEntry> result;
    private ArrayList<PlanetFilterResultEntry> result;

    /**
     * @return the items
     */
    public int getItems() {
        return items;
    }

    public PlanetFilterResult(int items, int maxItems, ArrayList<PlanetFilterResultEntry> result) {
    // public PlanetFilterResult(int items, int maxItems, TreeMap<Integer, PlanetFilterResultEntry> result) {
        this.items = items;
        this.totalItems = maxItems;
        this.result = result;
    }

    /**
     * @return the result
     */
    // public TreeMap<Integer, PlanetFilterResultEntry> getResult() {
    public ArrayList<PlanetFilterResultEntry> getResult() {
        return result;
    }

    /**
     * @return the totalItems
     */
    public int getTotalItems() {
        return totalItems;
    }

    /**
     * @param totalItems the totalItems to set
     */
    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

}
