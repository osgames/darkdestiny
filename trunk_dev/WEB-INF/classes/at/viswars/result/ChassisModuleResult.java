/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.result;

import at.viswars.ships.Armor;
import at.viswars.ships.Engine;
import at.viswars.ships.Reactor;
import at.viswars.ships.Shield;
import at.viswars.ships.ShipChassis;
import at.viswars.ships.Weapon;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ChassisModuleResult {
    private ArrayList<ShipChassis> chassis;
    private ArrayList<Engine> engines;
    private ArrayList<Armor> armors;
    // private ArrayList<ComputerSystem> computerSystems;    
    private ArrayList<Weapon> weapons;
    private ArrayList<Shield> shields;
    private ArrayList<Reactor> reactors;
}
