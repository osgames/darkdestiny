/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.result;

/**
 *
 * @author Stefan
 */
public class TestCombatResult extends BaseResult {

    private final Object combatReport;
    private final long attackerCost;
    private final long defenderCost;

    public TestCombatResult(String message, boolean error) {
        super(message, error);
        this.combatReport = null;
        this.attackerCost = 0;
        this.defenderCost = 0;        
    }

    public TestCombatResult(Object message, long attackerCost, long defenderCost) {
        super("Calculation success", false);
        this.combatReport = message;
        this.attackerCost = attackerCost;
        this.defenderCost = defenderCost;
    }

    public Object getCombatReport() {
        return combatReport;
    }

    public long getAttackerCost() {
        return attackerCost;
    }

    public long getDefenderCost() {
        return defenderCost;
    }
}
