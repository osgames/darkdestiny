/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars;

import at.viswars.ships.ShipData;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DamagedShipsDAO;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.ShipFleetDAO;
import at.viswars.databuffer.fleet.RelativeCoordinate;
import at.viswars.model.PlayerFleet;
import at.viswars.model.ShipDesign;
import at.viswars.model.ShipFleet;

/**
 *
 * @author Stefan
 */
public class TradeShipData extends ShipData {
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);    
    
    private final RelativeCoordinate home;
    
    public TradeShipData(RelativeCoordinate home) {
        this.home = home;
    }
    
    public TradeShipData(int shipFleetId, RelativeCoordinate home) {
        super(shipFleetId);
        this.home = home;
    }

    public RelativeCoordinate getHome() {
        return home;
    }
}
