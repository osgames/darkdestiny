package at.viswars.admin;

import java.sql.SQLException;

/**
 * Um für die Anzeige der Admin-Seiten alles vorzubereiten
 * 
 * @author martin
 */
public interface IMyPageProducer {

	
	public void setup(Object o);
	public String producePage() throws SQLException; 
}
