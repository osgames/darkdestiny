/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.admin;

import at.viswars.Logger.Logger;
import at.viswars.admin.module.AttributeType;
import at.viswars.dao.AttributeDAO;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DesignModuleDAO;
import at.viswars.dao.ModuleAttributeDAO;
import at.viswars.dao.ModuleDAO;
import at.viswars.dao.RessourceDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.model.Attribute;
import at.viswars.model.Chassis;
import at.viswars.model.DesignModule;
import at.viswars.model.Module;
import at.viswars.model.ModuleAttribute;
import at.viswars.model.Ressource;
import at.viswars.model.ShipDesign;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class FixJobs {

    private static ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private static ModuleAttributeDAO maDAO = (ModuleAttributeDAO) DAOFactory.get(ModuleAttributeDAO.class);
    private static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    private static AttributeDAO aDAO = (AttributeDAO) DAOFactory.get(AttributeDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static DesignModuleDAO dmDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);

    public static void adjustSizesOfModules() {
        try {
            int[] lasG = new int[]{75, 120, 170, 260, 500, 500};
            int[] lasB = new int[]{105, 160, 240, 360, 720, 720};
            int[] lasGS = new int[]{120, 180, 270, 410, 500, 500};
            int[] plG = new int[]{150, 225, 340, 510, 900, 900};
            int[] plGS = new int[]{180, 270, 405, 610, 720, 720};
            int[] impG = new int[]{170, 260, 390, 580, 1000, 1000};
            int[] intG = new int[]{175, 265, 400, 600, 1100, 1600};
            int[] mhvG = new int[]{200, 300, 450, 675, 1300, 1200};

            HashMap<Integer, HashMap<Integer, Integer[]>> chassisWeaponSizeChange =
                    new HashMap<Integer, HashMap<Integer, Integer[]>>();

            for (Chassis c : cDAO.findAllOrdered()) {
                int actLasG = 0;
                int actLasB = 0;
                int actLasGS = 0;
                int actPlG = 0;
                int actPlGS = 0;
                int actImpG = 0;
                int actIntG = 0;
                int actMhvG = 0;

                switch (c.getId()) {
                    case (3):
                        actLasG = lasG[0];
                        actLasB = lasB[0];
                        actLasGS = lasGS[0];
                        actPlG = plG[0];
                        actPlGS = plGS[0];
                        actImpG = impG[0];
                        actIntG = intG[0];
                        actMhvG = mhvG[0];
                        break;
                    case (4):
                        actLasG = lasG[1];
                        actLasB = lasB[1];
                        actLasGS = lasGS[1];
                        actPlG = plG[1];
                        actPlGS = plGS[1];
                        actImpG = impG[1];
                        actIntG = intG[1];
                        actMhvG = mhvG[1];
                        break;
                    case (5):
                        actLasG = lasG[2];
                        actLasB = lasB[2];
                        actLasGS = lasGS[2];
                        actPlG = plG[2];
                        actPlGS = plGS[2];
                        actImpG = impG[2];
                        actIntG = intG[2];
                        actMhvG = mhvG[2];
                        break;
                    case (6):
                    case (7):
                        actLasG = lasG[3];
                        actLasB = lasB[3];
                        actLasGS = lasGS[3];
                        actPlG = plG[3];
                        actPlGS = plGS[3];
                        actImpG = impG[3];
                        actIntG = intG[3];
                        actMhvG = mhvG[3];
                        break;
                    case (8):
                    case (9):
                        actLasG = lasG[4];
                        actLasB = lasB[4];
                        actLasGS = lasGS[4];
                        actPlG = plG[4];
                        actPlGS = plGS[4];
                        actImpG = impG[4];
                        actIntG = intG[4];
                        actMhvG = mhvG[4];
                        break;
                    case (10):
                        actLasG = lasG[5];
                        actLasB = lasB[5];
                        actLasGS = lasGS[5];
                        actPlG = plG[5];
                        actPlGS = plGS[5];
                        actImpG = impG[5];
                        actIntG = intG[5];
                        actMhvG = mhvG[5];
                        break;
                    default:
                        continue;
                }

                // Adjust values
                ArrayList<ModuleAttribute> entries = maDAO.findBy(201, c.getId()); // LasG
                int oldSize = adjustEntries(entries, actLasG);
                addToHashMap(c.getId(), 201, oldSize, actLasG, chassisWeaponSizeChange);

                entries = maDAO.findBy(501, c.getId()); // PlG
                oldSize = adjustEntries(entries, actPlG);
                addToHashMap(c.getId(), 201, oldSize, actPlG, chassisWeaponSizeChange);

                entries = maDAO.findBy(401, c.getId()); // ImpG
                oldSize = adjustEntries(entries, actImpG);
                addToHashMap(c.getId(), 401, oldSize, actImpG, chassisWeaponSizeChange);

                entries = maDAO.findBy(404, c.getId()); // IntG
                oldSize = adjustEntries(entries, actIntG);
                addToHashMap(c.getId(), 404, oldSize, actIntG, chassisWeaponSizeChange);

                entries = maDAO.findBy(203, c.getId()); // LasGS
                oldSize = adjustEntries(entries, actLasGS);
                addToHashMap(c.getId(), 203, oldSize, actLasGS, chassisWeaponSizeChange);

                entries = maDAO.findBy(503, c.getId()); // PlGS
                oldSize = adjustEntries(entries, actPlGS);
                addToHashMap(c.getId(), 503, oldSize, actPlGS, chassisWeaponSizeChange);

                entries = maDAO.findBy(405, c.getId()); // MHV
                oldSize = adjustEntries(entries, actMhvG);
                addToHashMap(c.getId(), 405, oldSize, actMhvG, chassisWeaponSizeChange);

                entries = maDAO.findBy(600, c.getId()); // LasB
                oldSize = adjustEntries(entries, actLasB);
                addToHashMap(c.getId(), 600, oldSize, actLasB, chassisWeaponSizeChange);
            }

            // Loop all designs and check for modules which has to be adjusted
            for (ShipDesign sd : (ArrayList<ShipDesign>) sdDAO.findAll()) {
                if (sd.getChassis() < 3) {
                    continue;
                }

                Chassis c = cDAO.findById(sd.getChassis());
                ArrayList<DesignModule> dmList = dmDAO.findByDesignId(sd.getId());

                if (chassisWeaponSizeChange.containsKey(sd.getChassis())) {
                    HashMap<Integer, Integer[]> changeForModule = chassisWeaponSizeChange.get(sd.getChassis());

                    int availExcess = 0;

                    for (DesignModule dm : dmList) {
                        if (changeForModule.containsKey(dm.getModuleId())) {
                            Module m = mDAO.findById(dm.getModuleId());

                            Integer[] values = changeForModule.get(dm.getModuleId());
                            int oldValue = values[0] * dm.getCount();
                            int newValue = values[1] * dm.getCount();

                            Logger.getLogger().write("Design " + sd.getName() + " [" + c.getName() + "] {" + sd.getUserId() + "} needs adjustment for weapon " + m.getName());

                            if (newValue > (oldValue + availExcess)) {
                                int newCount = (int) Math.floor((oldValue + availExcess) / values[1]);
                                Logger.getLogger().write("Decrease count from " + dm.getCount() + " to " + newCount);
                                if (newCount == 0) {
                                    newCount = 1;
                                }

                                dm.setCount(newCount);

                                if (newCount == 0) {
                                    dmDAO.remove(dm);
                                } else {
                                    dmDAO.update(dm);
                                }
                                availExcess = oldValue - (newCount * values[1]);
                            } else {
                                int newCount = (int) Math.floor((oldValue + availExcess) / values[1]);
                                Logger.getLogger().write("Increase count from " + dm.getCount() + " to " + newCount);

                                availExcess = oldValue - (newCount * values[1]);
                                dm.setCount(newCount);
                                dmDAO.update(dm);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addToHashMap(int chassisSize, int moduleId, int oldSize, int newSize, HashMap<Integer, HashMap<Integer, Integer[]>> buffer) {
        if (oldSize == -1) {
            return;
        }

        if (buffer.containsKey(chassisSize)) {
            HashMap<Integer, Integer[]> weaponOldSize = buffer.get(chassisSize);
            Integer[] sizeChange = new Integer[2];

            sizeChange[0] = oldSize;
            sizeChange[1] = newSize;

            weaponOldSize.put(moduleId, sizeChange);
        } else {
            HashMap<Integer, Integer[]> weaponOldSize = new HashMap<Integer, Integer[]>();
            Integer[] sizeChange = new Integer[2];

            sizeChange[0] = oldSize;
            sizeChange[1] = newSize;

            weaponOldSize.put(moduleId, sizeChange);
            buffer.put(chassisSize, weaponOldSize);
        }
    }

    private static int adjustEntries(ArrayList<ModuleAttribute> entries, int newSize) {
        Module m = mDAO.findById(entries.get(0).getModuleId());
        Chassis c = cDAO.findById(entries.get(0).getChassisId());

        Logger.getLogger().write("Updating module " + m.getName() + " for chassis " + c.getName());

        ModuleAttribute maOldSize = null;
        double adjustFactor = 0d;
        for (ModuleAttribute mTmp : entries) {
            Attribute a = aDAO.findById(mTmp.getAttributeId());
            if (a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.SPACE_CONSUMPTION.name())) {
                if ((int) Math.round(mTmp.getValue()) == newSize) {
                    return -1;
                }

                maOldSize = mTmp;
                adjustFactor = newSize / mTmp.getValue();
                Logger.getLogger().write("Change size from " + mTmp.getValue() + " to " + newSize + "(Faktor: " + adjustFactor + ")");
                maOldSize.setValue((double) newSize);
                maDAO.update(maOldSize);
                break;
            }
        }

        // Adjust ressource entries
        for (ModuleAttribute mTmp : entries) {
            Attribute a = aDAO.findById(mTmp.getAttributeId());
            if (a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.RESSOURCE.name())) {
                Ressource r = rDAO.findRessourceById(mTmp.getRefId());

                int newValue = 0;

                if (mTmp.getValue() < 100) {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor));
                } else if (mTmp.getValue() > 100000) {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor) / 1000d) * 1000;
                } else {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor) / 10d) * 10;
                }

                Logger.getLogger().write("Change ressource " + r.getName() + " from " + mTmp.getValue() + " to " + newValue);

                mTmp.setValue((double) newValue);
                maDAO.update(mTmp);
            }
        }

        // Adjust construction points
        for (ModuleAttribute mTmp : entries) {
            Attribute a = aDAO.findById(mTmp.getAttributeId());
            if (a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.ORB_DOCK_CONS_POINTS.name())
                    || a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.DOCK_CONS_POINTS.name())
                    || a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.MOD_CONS_POINTS.name())) {
                Ressource r = rDAO.findRessourceById(mTmp.getRefId());

                int newValue = 0;

                if (mTmp.getValue() < 100) {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor));
                } else if (mTmp.getValue() > 100000) {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor) / 1000d) * 1000;
                } else {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor) / 10d) * 10;
                }

                Logger.getLogger().write("Change construction cost " + a.getName() + " from " + mTmp.getValue() + " to " + newValue);

                mTmp.setValue((double) newValue);
                maDAO.update(mTmp);
            }
        }

        // Adjust energy consumption
        for (ModuleAttribute mTmp : entries) {
            Attribute a = aDAO.findById(mTmp.getAttributeId());
            if (a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.ENERGY_CONSUMPTION.name())) {
                int newValue = 0;

                if (mTmp.getValue() < 100) {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor));
                } else {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor) / 10d) * 10;
                }

                Logger.getLogger().write("Change ENERGY_CONSUMPTION from " + mTmp.getValue() + " to " + newValue);

                mTmp.setValue((double) newValue);
                maDAO.update(mTmp);
                break;
            }
        }

        // Adjust attack power
        for (ModuleAttribute mTmp : entries) {
            Attribute a = aDAO.findById(mTmp.getAttributeId());
            if (a.getName().equalsIgnoreCase(at.viswars.enumeration.EAttribute.ATTACK_STRENGTH.name())) {
                int newValue = 0;

                if (mTmp.getValue() < 100) {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor));
                } else {
                    newValue = (int) Math.ceil((mTmp.getValue() * adjustFactor) / 10d) * 10;
                }

                Logger.getLogger().write("Change ATTACK_STRENGTH from " + mTmp.getValue() + " to " + newValue);

                mTmp.setValue((double) newValue);
                maDAO.update(mTmp);
                break;
            }
        }

        return (int) Math.round(maOldSize.getValue());
    }

    public static void doIt() {
        ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
        ModuleAttributeDAO maDAO = (ModuleAttributeDAO) DAOFactory.get(ModuleAttributeDAO.class);

        ArrayList<Chassis> allChassis = cDAO.findAll();

        for (Chassis c : allChassis) {
            Logger.getLogger().write("blubblo " + c.getId());

            ArrayList<ModuleAttribute> chassisList = maDAO.findByChassisAndModule(c.getId(), c.getRelModuleId());

            double enginePlaceCons = 0d;

            if (c.getId() == 1) {
                continue;
            }
            if ((c.getId() == 7) || (c.getId() == 9)) {
                continue;
            }

            for (ModuleAttribute ma : chassisList) {
                Logger.getLogger().write("1.1");

                if ((ma.getAttributeType() == AttributeType.DEFINE)
                        && (ma.getAttributeId() == 50)) {
                        enginePlaceCons = (int)Math.floor(ma.getValue() / 5d);
                }
            }

            // Plasma Engine
            /*
            ModuleAttribute maMetaDock = new ModuleAttribute();
            maMetaDock.setChassisId(c.getId());
            maMetaDock.setModuleId(80);
            maMetaDock.setAttributeId(49);
            maMetaDock.setAttributeType(AttributeType.DEFINE);
            ArrayList<ModuleAttribute> maMetaDockList = maDAO.find(maMetaDock);
            if (maMetaDockList.size() == 1) {
                maMetaDock = maMetaDockList.get(0);
                maMetaDock.setValue(enginePlaceCons);
                maDAO.update(maMetaDock);
            } else {
                maMetaDock.setValue(enginePlaceCons);
                maDAO.add(maMetaDock);
            }
            */

            // Impulse engine
            ModuleAttribute maMetaOrbDock = new ModuleAttribute();
            maMetaOrbDock.setChassisId(c.getId());
            maMetaOrbDock.setModuleId(84);
            maMetaOrbDock.setAttributeId(49);
            maMetaOrbDock.setAttributeType(AttributeType.DEFINE);
            ArrayList<ModuleAttribute> maMetaOrbDockList = maDAO.find(maMetaOrbDock);
            if (maMetaOrbDockList.size() == 1) {
                maMetaOrbDock = maMetaOrbDockList.get(0);
                maMetaOrbDock.setValue(enginePlaceCons);
                maDAO.update(maMetaOrbDock);
            } else {
                maMetaOrbDock.setValue(enginePlaceCons);
                maDAO.add(maMetaOrbDock);
            }

            /*
            ModuleAttribute maImpulseDock = new ModuleAttribute();
            maImpulseDock.setChassisId(c.getId());
            maImpulseDock.setModuleId(84);
            maImpulseDock.setAttributeId(70);
            maImpulseDock.setAttributeType(AttributeType.DEFINE);
            ArrayList<ModuleAttribute> maImpulseDockList = maDAO.find(maImpulseDock);
            if (maImpulseDockList.size() == 1) {
                maImpulseDock = maImpulseDockList.get(0);
                maImpulseDock.setValue(dockPointsImpulse);
                maDAO.update(maImpulseDock);
            } else {
                maImpulseDock.setValue(dockPointsImpulse);
                maDAO.add(maImpulseDock);
            }
            */
        }
    }
}
