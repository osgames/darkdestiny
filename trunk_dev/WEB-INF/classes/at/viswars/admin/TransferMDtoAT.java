/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.admin;

import at.viswars.ModuleType;
import at.viswars.admin.module.AttributeType;
import at.viswars.admin.module.ReferenceType;
import at.viswars.dao.AttributeDAO;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.ModuleAttributeDAO;
import at.viswars.dao.ModuleDAO;
import at.viswars.enumeration.EAttribute;
import at.viswars.Logger.Logger;
import at.viswars.dao.ChassisDAO;
import at.viswars.dao.ChassisWeaponRelDAO;
import at.viswars.dao.SizeRelationDAO;
import at.viswars.dao.WeaponFactorDAO;
import at.viswars.model.Chassis;
import at.viswars.model.ChassisWeaponRel;
import at.viswars.model.Module;
import at.viswars.model.ModuleAttribute;
import at.viswars.model.Ressource;
import at.viswars.model.SizeRelation;
import at.viswars.model.WeaponFactor;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TransferMDtoAT {
    private static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    private static ModuleAttributeDAO maDAO = (ModuleAttributeDAO) DAOFactory.get(ModuleAttributeDAO.class);
    private static AttributeDAO aDAO = (AttributeDAO) DAOFactory.get(AttributeDAO.class);    
    private static ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private static SizeRelationDAO srDAO = (SizeRelationDAO) DAOFactory.get(SizeRelationDAO.class);
    private static ChassisWeaponRelDAO cwrDAO = (ChassisWeaponRelDAO) DAOFactory.get(ChassisWeaponRelDAO.class);
    private static WeaponFactorDAO wfDAO = (WeaponFactorDAO) DAOFactory.get(WeaponFactorDAO.class);
    /*
    public static void transferData() {
        maDAO.removeAll();
        
        ArrayList<Module> modules = mDAO.findAll();
        
        for (Module m : modules) {
            ModuleType mt = m.getType();
            
            if (mt == ModuleType.CHASSIS) {
                Chassis c = cDAO.findByRelModuleId(m.getId());
                
                createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_PROVIDING,m.getSpace(),0,null);
                createAttributeEntry(m.getId(),c.getId(),EAttribute.HITPOINTS,m.getBaseStructure(),0,null);
                
                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getOrbitalDockPoints(),0,null);                
                
                if (m.getCredits() > 0) 
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits(),Ressource.CREDITS,ReferenceType.RESSOURCE);
                if (m.getIron() > 0) 
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron(),Ressource.IRON,ReferenceType.RESSOURCE);
                if (m.getSteel() > 0) 
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel(),Ressource.STEEL,ReferenceType.RESSOURCE);
                if (m.getTerkonit() > 0) 
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                if (m.getYnkelonium() > 0) 
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                if (m.getHowalgonium() > 0) 
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getHowalgonium(),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);                                    
            } else if (mt == ModuleType.ENGINE) {
                ArrayList<Chassis> allChassis = cDAO.findAll();
                for (Chassis c : allChassis) {
                    SizeRelation sr = srDAO.getByRelModuleId(c.getRelModuleId());
                    
                    if ((m.getId() == 82) || (m.getId() == 85)) {
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_CONSUMPTION,m.getSpace() * sr.getHyperFactor(),0,null);
                        if (m.getEnergy() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.ENERGY_CONSUMPTION,m.getEnergy() * sr.getHyperFactor(),0,null);                         
                    } else {
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_CONSUMPTION,m.getSpace() * sr.getEngineFactor(),0,null);
                        if (m.getEnergy() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.ENERGY_CONSUMPTION,m.getEnergy() * sr.getEngineFactor(),0,null);                          
                    }
                    
                    if ((m.getId() == 82) || (m.getId() == 85)) {
                        if (m.getCredits() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits() * sr.getHyperFactor(),Ressource.CREDITS,ReferenceType.RESSOURCE);
                        if (m.getIron() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron() * sr.getHyperFactor(),Ressource.IRON,ReferenceType.RESSOURCE);
                        if (m.getSteel() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel() * sr.getHyperFactor(),Ressource.STEEL,ReferenceType.RESSOURCE);
                        if (m.getTerkonit() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit() * sr.getHyperFactor(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                        if (m.getYnkelonium() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium() * sr.getHyperFactor(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);                        
                    } else {
                        if (m.getCredits() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits() * sr.getCreditFactor(),Ressource.CREDITS,ReferenceType.RESSOURCE);
                        if (m.getIron() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron() * sr.getEngineFactor(),Ressource.IRON,ReferenceType.RESSOURCE);
                        if (m.getSteel() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel() * sr.getEngineFactor(),Ressource.STEEL,ReferenceType.RESSOURCE);
                        if (m.getTerkonit() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit() * sr.getEngineFactor(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                        if (m.getYnkelonium() > 0) 
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium() * sr.getEngineFactor(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                    }
                    
                    if (m.getHowalgonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getHowalgonium() * sr.getHyperCrystalFactor(),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);                    
                    
                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.DOCK_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ORB_DOCK_CONS_POINTS,m.getOrbitalDockPoints(),0,null);   
                    
                    switch (m.getId()) {
                        case(1):
                            switch (c.getId()) {
                                case (Chassis.ID_FIGHTER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.2,0,null);
                                    break;
                                case (Chassis.ID_CORVETTE):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.2,0,null);
                                    break;
                                case (Chassis.ID_FRIGATE):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.15,0,null);
                                    break;
                                case (Chassis.ID_DESTROYER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.15,0,null);
                                    break;
                                case (Chassis.ID_CRUISER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.12,0,null);
                                    break;
                                case (Chassis.ID_BATTLESHIP):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.12,0,null);
                                    break;
                                case (Chassis.ID_SUPERBATTLESHIP):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.1,0,null);
                                    break;
                                case (Chassis.ID_TENDER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.05,0,null);
                                    break;                                    
                            }                            
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_HYPER,0.1d,0,null);
                            break;
                        case(80):
                            switch (c.getId()) {
                                case (Chassis.ID_FIGHTER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.6,0,null);
                                    break;
                                case (Chassis.ID_CORVETTE):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.6,0,null);
                                    break;
                                case (Chassis.ID_FRIGATE):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.3,0,null);
                                    break;
                                case (Chassis.ID_DESTROYER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.3,0,null);
                                    break;
                                case (Chassis.ID_CRUISER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.25,0,null);
                                    break;
                                case (Chassis.ID_BATTLESHIP):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.25,0,null);
                                    break;
                                case (Chassis.ID_SUPERBATTLESHIP):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.2,0,null);
                                    break;
                                case (Chassis.ID_TENDER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.15,0,null);
                                    break;                                    
                            }              
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_HYPER,0.15d,0,null);
                            break;
                        case(84):
                            switch (c.getId()) {
                                case (Chassis.ID_FIGHTER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.6,0,null);
                                    break;
                                case (Chassis.ID_CORVETTE):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.6,0,null);
                                    break;
                                case (Chassis.ID_FRIGATE):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.3,0,null);
                                    break;
                                case (Chassis.ID_DESTROYER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.3,0,null);
                                    break;
                                case (Chassis.ID_CRUISER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.25,0,null);
                                    break;
                                case (Chassis.ID_BATTLESHIP):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.25,0,null);
                                    break;
                                case (Chassis.ID_SUPERBATTLESHIP):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.2,0,null);
                                    break;
                                case (Chassis.ID_TENDER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.15,0,null);
                                    break;                                    
                            }              
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_HYPER,0.2d,0,null);
                            break;
                        case(82):
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_HYPER,2,0,null);
                            break;
                        case(85):
                            switch (c.getId()) {
                                case (Chassis.ID_FRIGATE):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.3,0,null);
                                    break;
                                case (Chassis.ID_DESTROYER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.3,0,null);
                                    break;
                                case (Chassis.ID_CRUISER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.25,0,null);
                                    break;
                                case (Chassis.ID_BATTLESHIP):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.25,0,null);
                                    break;
                                case (Chassis.ID_SUPERBATTLESHIP):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.2,0,null);
                                    break;
                                case (Chassis.ID_TENDER):
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_ACCELERATION,0.15,0,null);
                                    break;                                    
                            }              
                            createAttributeEntry(m.getId(),c.getId(),EAttribute.SPEED_HYPER,4,0,null);
                            break;
                    }    
                }
            } else if (mt == ModuleType.ARMOR) {
                ArrayList<Chassis> allChassis = cDAO.findAll();
                for (Chassis c : allChassis) {                
                    SizeRelation sr = srDAO.getByRelModuleId(c.getRelModuleId());
                    
                    if (m.getCredits() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits() * sr.getEngineFactor(),Ressource.CREDITS,ReferenceType.RESSOURCE);
                    if (m.getIron() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron() * sr.getEngineFactor(),Ressource.IRON,ReferenceType.RESSOURCE);
                    if (m.getSteel() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel() * sr.getEngineFactor() ,Ressource.STEEL,ReferenceType.RESSOURCE);
                    if (m.getTerkonit() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit() * sr.getEngineFactor(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                    if (m.getYnkelonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium() * sr.getEngineFactor(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                    if (m.getHowalgonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getHowalgonium() * sr.getEngineFactor(),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);
                              
                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.DOCK_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ORB_DOCK_CONS_POINTS,m.getOrbitalDockPoints(),0,null);   
                    
                    Module sMod = new Module();
                    sMod.setType(ModuleType.WEAPON);
                    ArrayList<Module> wModList = mDAO.find(sMod);
                    
                    for (Module mTmp : wModList) {                                                
                        WeaponFactor wf = wfDAO.findByWeaponId(mTmp.getId());                        
                        Logger.getLogger().write("TEST: " + wf);
                        
                        if (wf == null) continue;                        
                        
                        switch (m.getId()) {
                            case(2): // steel
                                if (wf.getSteelArmor() < 1d) 
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.DAMAGE_REDUCTION,1d-wf.getSteelArmor(),mTmp.getId(),ReferenceType.MODULE);
                                break;
                            case(21): // terk
                                if (wf.getTerkonitArmor() < 1d) 
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.DAMAGE_REDUCTION,1d-wf.getTerkonitArmor(),mTmp.getId(),ReferenceType.MODULE);                                
                                break;
                            case(22): // ynke
                                if (wf.getYnkenitArmor() < 1d) 
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.DAMAGE_REDUCTION,1d-wf.getYnkenitArmor(),mTmp.getId(),ReferenceType.MODULE);                                
                                break;
                            case(23): // solonium
                                if (wf.getSoloniumArmor() < 1d) 
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.DAMAGE_REDUCTION,1d-wf.getSoloniumArmor(),mTmp.getId(),ReferenceType.MODULE);                                
                                break;
                        }
                    }
                }                          
            } else if (mt == ModuleType.COLONISATION) {
                ArrayList<Chassis> allChassis = cDAO.findAll();
                for (Chassis c : allChassis) {                
                    if (m.getCredits() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits(),Ressource.CREDITS,ReferenceType.RESSOURCE);
                    if (m.getIron() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron(),Ressource.IRON,ReferenceType.RESSOURCE);
                    if (m.getSteel() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel(),Ressource.STEEL,ReferenceType.RESSOURCE);
                    if (m.getTerkonit() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                    if (m.getYnkelonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                    if (m.getHowalgonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getHowalgonium(),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);                                                   

                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.DOCK_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ORB_DOCK_CONS_POINTS,m.getOrbitalDockPoints(),0,null);                    
                    
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.ENERGY_CONSUMPTION,m.getEnergy(),0,null);
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_CONSUMPTION,m.getSpace(),0,null);                       
                }                                                       
            } else if (mt == ModuleType.COMPUTER) {
                
            } else if (mt == ModuleType.ENERGY_STORAGE) {
                
            } else if (mt == ModuleType.HANGAR) {
                
            } else if (mt == ModuleType.REACTOR) {
                ArrayList<Chassis> allChassis = cDAO.findAll();
                for (Chassis c : allChassis) {                
                    if (m.getCredits() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits(),Ressource.CREDITS,ReferenceType.RESSOURCE);
                    if (m.getIron() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron(),Ressource.IRON,ReferenceType.RESSOURCE);
                    if (m.getSteel() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel(),Ressource.STEEL,ReferenceType.RESSOURCE);
                    if (m.getTerkonit() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                    if (m.getYnkelonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                    if (m.getHowalgonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getHowalgonium(),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);                                                   
                    
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.ENERGY_PRODUCTION,m.getEnergy(),0,null);
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_CONSUMPTION,m.getSpace(),0,null);
                    
                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.DOCK_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ORB_DOCK_CONS_POINTS,m.getOrbitalDockPoints(),0,null);   
                }                                                
            } else if (mt == ModuleType.SHIELD) {
                ArrayList<Chassis> allChassis = cDAO.findAll();
                for (Chassis c : allChassis) {                                                  
                    ChassisWeaponRel cwr = cwrDAO.getByChassisAndModule(c.getRelModuleId(), m.getId());   
                    
                    double costFactor = 1d;
                    double sizeFactor = 1d;
                    double strengthFactor = 1d;
                    double energyFactor = 1d;
                    
                    if (cwr != null) {
                        costFactor = cwr.getCost_factor();
                        sizeFactor = cwr.getSize_factor();
                        strengthFactor = cwr.getStrength_factor();
                        energyFactor = cwr.getEnergy_factor();
                    }
                    
                    if (m.getCredits() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits() * costFactor,Ressource.CREDITS,ReferenceType.RESSOURCE);
                    if (m.getIron() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron(),Ressource.IRON,ReferenceType.RESSOURCE);
                    if (m.getSteel() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel(),Ressource.STEEL,ReferenceType.RESSOURCE);
                    if (m.getTerkonit() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                    if (m.getYnkelonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                    if (m.getHowalgonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getHowalgonium(),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);                                                                    
                    
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.ENERGY_CONSUMPTION,m.getEnergy() * energyFactor,0,null);
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_CONSUMPTION,m.getSpace() * sizeFactor,0,null);   
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.DEFENSE_STRENGTH,m.getValue() * strengthFactor,0,null);
                    
                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.DOCK_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ORB_DOCK_CONS_POINTS,m.getOrbitalDockPoints(),0,null);   
                    
                    Module sMod = new Module();
                    sMod.setType(ModuleType.WEAPON);
                    ArrayList<Module> wModList = mDAO.find(sMod);
                    
                    for (Module mTmp : wModList) {                                                
                        WeaponFactor wf = wfDAO.findByWeaponId(mTmp.getId());                        
                        
                        if (wf == null) continue;                        
                        
                        switch (m.getId()) {
                            case(50): // steel
                                if (wf.getPrallschirmPenetration() < 1d) 
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.DAMAGE_ABSORBTION,1d-wf.getPrallschirmPenetration(),mTmp.getId(),ReferenceType.MODULE);
                                break;
                            case(51): // terk
                                if (wf.getHuschirmPenetration() < 1d) 
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.DAMAGE_ABSORBTION,1d-wf.getHuschirmPenetration(),mTmp.getId(),ReferenceType.MODULE);                                
                                break;
                            case(52): // ynke
                                if (wf.getParatronPenetration() < 1d) 
                                    createAttributeEntry(m.getId(),c.getId(),EAttribute.DAMAGE_ABSORBTION,1d-wf.getParatronPenetration(),mTmp.getId(),ReferenceType.MODULE);                                
                                break;
                        }
                    }                    
                }                        
            } else if (mt == ModuleType.SPECIAL) {
                
            } else if (mt == ModuleType.TRANSPORT_POPULATION) {
                ArrayList<Chassis> allChassis = cDAO.findAll();
                for (Chassis c : allChassis) {                
                    if (m.getCredits() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits(),Ressource.CREDITS,ReferenceType.RESSOURCE);
                    if (m.getIron() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron(),Ressource.IRON,ReferenceType.RESSOURCE);
                    if (m.getSteel() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel(),Ressource.STEEL,ReferenceType.RESSOURCE);
                    if (m.getTerkonit() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                    if (m.getYnkelonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                    if (m.getHowalgonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getHowalgonium(),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);
                                
                    
                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.DOCK_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ORB_DOCK_CONS_POINTS,m.getOrbitalDockPoints(),0,null);   
                    
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_CONSUMPTION,m.getSpace(),0,null);
                    if (m.getEnergy() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ENERGY_CONSUMPTION,m.getEnergy(),0,null);
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.TROOP_STORAGE,3000,0,null);                            
                }                                       
            } else if (mt == ModuleType.TRANSPORT_RESSOURCE) {
                ArrayList<Chassis> allChassis = cDAO.findAll();
                for (Chassis c : allChassis) {                
                    if (m.getCredits() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getCredits(),Ressource.CREDITS,ReferenceType.RESSOURCE);
                    if (m.getIron() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getIron(),Ressource.IRON,ReferenceType.RESSOURCE);
                    if (m.getSteel() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getSteel(),Ressource.STEEL,ReferenceType.RESSOURCE);
                    if (m.getTerkonit() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getTerkonit(),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                    if (m.getYnkelonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getYnkelonium(),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                    if (m.getHowalgonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,m.getHowalgonium(),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);
                                              
                    
                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.DOCK_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ORB_DOCK_CONS_POINTS,m.getOrbitalDockPoints(),0,null);   
                    
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_CONSUMPTION,m.getSpace(),0,null);
                    if (m.getEnergy() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ENERGY_CONSUMPTION,m.getEnergy(),0,null);
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.RESS_STORAGE,3000,0,null);
                }                
            } else if (mt == ModuleType.WEAPON) {
                ArrayList<Chassis> allChassis = cDAO.findAll();
                for (Chassis c : allChassis) {                                
                    ChassisWeaponRel cwr = cwrDAO.getByChassisAndModule(c.getRelModuleId(), m.getId());
                    WeaponFactor wf = wfDAO.findByWeaponId(m.getId());
                    
                    double costFactor = 1d;
                    
                    if (cwr != null) {
                        costFactor = cwr.getCost_factor();
                    }
                    
                    if (m.getCredits() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,(int)((double)m.getCredits() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.COST_FACTOR)),Ressource.CREDITS,ReferenceType.RESSOURCE);
                    if (m.getIron() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,(int)((double)m.getIron() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.SIZE_FACTOR)),Ressource.IRON,ReferenceType.RESSOURCE);
                    if (m.getSteel() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,(int)((double)m.getSteel() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.SIZE_FACTOR)),Ressource.STEEL,ReferenceType.RESSOURCE);
                    if (m.getTerkonit() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,(int)((double)m.getTerkonit() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.SIZE_FACTOR)),Ressource.TERKONIT,ReferenceType.RESSOURCE);
                    if (m.getYnkelonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,(int)((double)m.getYnkelonium() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.SIZE_FACTOR)),Ressource.YNKELONIUM,ReferenceType.RESSOURCE);
                    if (m.getHowalgonium() > 0) 
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RESSOURCE,(int)((double)m.getHowalgonium() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.SIZE_FACTOR)),Ressource.HOWALGONIUM,ReferenceType.RESSOURCE);
                                
                    
                    if (m.getModulPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.MOD_CONS_POINTS,m.getModulPoints(),0,null);
                    if (m.getGroundDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.DOCK_CONS_POINTS,m.getGroundDockPoints(),0,null);
                    if (m.getOrbitalDockPoints() > 0) createAttributeEntry(m.getId(),c.getId(),EAttribute.ORB_DOCK_CONS_POINTS,m.getOrbitalDockPoints(),0,null);   
                    
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.SPACE_CONSUMPTION,m.getSpace() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.SIZE_FACTOR),0,null);
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.ATTACK_STRENGTH,m.getValue() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.STRENGTH_FACTOR),0,null);
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.ENERGY_CONSUMPTION,m.getEnergy() * getModifiers(c.getRelModuleId(),m.getId(),ChassisWeaponRel.ENERGY_FACTOR),0,null);
                    createAttributeEntry(m.getId(),c.getId(),EAttribute.ACCURACY,1,0,null);
                    
                    if (wf != null) {                                                               
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.MULTIFIRE,wf.getMultifire(),0,null);
                        createAttributeEntry(m.getId(),c.getId(),EAttribute.RANGE,wf.getMultifire(),0,null);  
                    }
                }
            }
        }
    }
    
    private static double getModifiers(int chassisId, int moduleId, int type) {
        ChassisWeaponRel cwr = cwrDAO.getByChassisAndModule(chassisId, moduleId);
        if (cwr == null) return 1d;
        
        double value = 1d;
        
        if (type == ChassisWeaponRel.ENERGY_FACTOR) {
            value = cwr.getEnergy_factor();
        } else if (type == ChassisWeaponRel.COST_FACTOR) {
            value = cwr.getCost_factor();
        } else if (type == ChassisWeaponRel.SIZE_FACTOR) {
            value = cwr.getSize_factor();
        } else if (type == ChassisWeaponRel.STRENGTH_FACTOR) {
            value = cwr.getStrength_factor();
        }
        
        return value;
    }
    
    private static int getAttributeId(EAttribute att) {
        ArrayList<at.viswars.model.Attribute> aList = aDAO.findAll();
        
        for (at.viswars.model.Attribute aTmp : aList) {
            if (aTmp.getName().equalsIgnoreCase(att.toString())) {
                return aTmp.getId();
            }
        }
        
        return 0;
    }
    
    private static void createAttributeEntry(int moduleId, int chassisId, EAttribute a, double value, int refId, ReferenceType rt) {
        ModuleAttribute ma = new ModuleAttribute(); 
        
        ma.setModuleId(moduleId);
        ma.setChassisId(chassisId);
        ma.setValue(value);
        ma.setAttributeType(AttributeType.DEFINE);
        ma.setAttributeId(getAttributeId(a));
        ma.setRefId(refId);
        ma.setRefType(rt);
        maDAO.add(ma);
    }
     * */
     
}
