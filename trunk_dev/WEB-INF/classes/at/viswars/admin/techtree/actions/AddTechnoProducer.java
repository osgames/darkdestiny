package at.viswars.admin.techtree.actions;

import java.sql.SQLException;
import java.sql.Statement;

import at.viswars.admin.IMyPageProducer;
import at.viswars.admin.TechTreeAdmin;
import at.viswars.admin.techtree.ConstructionTechTreeEntry;
import at.viswars.admin.techtree.GroundTroopTechTreeEntry;
import at.viswars.admin.techtree.ModulTechTreeEntry;
import at.viswars.admin.techtree.TechTree;
import at.viswars.admin.techtree.TechnologieTechTreeEntry;
import at.viswars.database.access.DbConnect;

/**
 * Hiermit wird eine Abhängigkeit hinzugefügt
 * 
 * @author martin
 */
public class AddTechnoProducer implements IMyPageProducer {

	private int id;
	private int tid;

	/**
	 * 
	 */
	public AddTechnoProducer(int id, int tid) {
		this.id = id;
		this.tid = tid;
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.IMyPageProducer#producePage()
	 */
	public String producePage() throws SQLException {
		if (TechTreeAdmin.techTree == null)
			TechTreeAdmin.loadTechTree();
		
		StringBuffer res = new StringBuffer();
		
		if ((id != -1) && (tid != -1))
		{
			TechTree tt_source = TechTreeAdmin.getTreeItemByID(id);
			TechTree tt_target = TechTreeAdmin.getTreeItemByID(tid);
			
			Statement state = DbConnect.getConnection().createStatement();
			
			int type = 1;
			if (tt_source instanceof ConstructionTechTreeEntry) {
				type = 0;
			} else if (tt_source instanceof TechnologieTechTreeEntry) {
				type = 1;
			} else if (tt_source instanceof ModulTechTreeEntry) {
				type = 2;
			} else if (tt_source instanceof GroundTroopTechTreeEntry) {
				type = 3;
			}
			
			int target_ConsID = 0;
			int target_researchID = 0;
			if (tt_target instanceof ConstructionTechTreeEntry)
				target_ConsID = tt_target.getBaseID();
			if (tt_target instanceof TechnologieTechTreeEntry)
				target_researchID = tt_target.getBaseID();
			
			state.execute("INSERT INTO `techrelation` VALUES ("+tt_source.getBaseID()+","+target_ConsID+","+target_researchID+","+type+")");
			res.append("Verknüpfung hinzugefügt<br>");
			res.append(TechTreeAdmin.makeLink(tt_source)+"<br>");
			res.append(TechTreeAdmin.makeLink(tt_target));
			TechTreeAdmin.forceReload();
			return res.toString();
		}
		if ((id == -1) && (tid == -1))
		{
			res.append("Du kannst diese Seite nur benutzen, wenn du schon eine Technologie gew&auml;hlt hast<br>");
			res.append(TechTreeAdmin.makeLink("Zur Liste",TechTreeAdmin.ACTION_FULL_TREE,""));
			return res.toString();
		}
		
		TechTree knownTech;
		String addParams = "";
		if (tid == -1)
		{
			 knownTech = TechTreeAdmin.getTreeItemByID(id);
			 res.append("<h2>Technologie, die von "+knownTech.getTyp()+" \""+knownTech.getName()+"\" abh�ngt hinzufügen</h2>");
			 addParams = "id="+id+"&tid=";
		}
		else {
			 knownTech = TechTreeAdmin.getTreeItemByID(tid);
			 res.append("<h2>Abh&auml;ngigkeit von "+knownTech.getTyp()+" \""+knownTech.getName()+"\" hinzufügen</h2>");
			 addParams = "tid="+tid+"&id=";
			 id = tid;
		}
		
		res.append("<ul>");
		
		for (TechTree tt  :TechTreeAdmin.allTechnologies.values())
		{
			if (tt.getID() != id)
			{
				res.append("<li>"+TechTreeAdmin.makeLink(tt.getTyp()+" \""+tt.getName()+"\"", TechTreeAdmin.ACTION_ADD_DEPENDENCY, addParams+tt.getID()));
			}
		}
		res.append("</ul>");
		
		return res.toString();
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.IMyPageProducer#setup(java.lang.Object)
	 */
	public void setup(Object o) {
	    // Empty on purpose
	}

}
