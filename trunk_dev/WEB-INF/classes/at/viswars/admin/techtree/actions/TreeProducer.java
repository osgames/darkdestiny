package at.viswars.admin.techtree.actions;

import java.sql.SQLException;

import at.viswars.admin.IMyPageProducer;
import at.viswars.admin.TechTreeAdmin;
import at.viswars.admin.techtree.TechTree;

/**
 * Hier werden alle Technologien dargestellt
 * 
 * @author martin
 */
public class TreeProducer 
        implements IMyPageProducer {

	/**
	 * 
	 */
	public TreeProducer() {
        // Empty on purpose
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.IMyPageProducer#producePage()
	 */
	public String producePage() throws SQLException {
		if (TechTreeAdmin.techTree == null)
			TechTreeAdmin.loadTechTree();
		
		StringBuffer res = new StringBuffer();
		for (TechTree tt : TechTreeAdmin.allTechnologies.values())
		{
			res.append(
			        "<tr><td><b>" + TechTreeAdmin.makeLink(tt) 
			        + "</b></td><td>"
			        ); 
			for (TechTree t : tt.getDependsOn()) {
				res.append("requires: "+TechTreeAdmin.makeLink(t) + "<br>");
			}
			for (TechTree t : tt.getDependencies())
				res.append(
				        "is required for: " + TechTreeAdmin.makeLink(t) 
				        + "<br>"
				        );
			res.append("</td></tr>");
		}
		res.append(
		        "<tr><td><b>" + TechTreeAdmin.makeLink(
		                "neue Forschung", TechTreeAdmin.ACTION_ONE_ITEM, 
		                "id=-1"
		             ) + "</b></td></tr>"
		         );
		res.append(
		        "<tr><td><b>" + TechTreeAdmin.makeLink(
		                "neues Modul", TechTreeAdmin.ACTION_ONE_ITEM, "id=-2"
		            ) + "</b></td></tr>"
		        );
		res.append(
		        "<tr><td><b>" + TechTreeAdmin.makeLink(
		                "neues Geb&auml;ude", TechTreeAdmin.ACTION_ONE_ITEM, 
		                "id=-3"
		            ) + "</b></td></tr>"
		        );
		res.append(
		        "<tr><td><b>" + TechTreeAdmin.makeLink(
		                "neue Bodentruppe", TechTreeAdmin.ACTION_ONE_ITEM, 
		                "id=-4"
		            ) + "</b></td></tr>"
		        );
		return res.toString();
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.IMyPageProducer#setup(java.lang.Object)
	 */
	public void setup(Object o) {
        // Empty on purpose
	}

}
