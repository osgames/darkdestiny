package at.viswars.admin.techtree.actions;

import java.sql.SQLException;
import java.sql.Statement;

import at.viswars.admin.IMyPageProducer;
import at.viswars.admin.TechTreeAdmin;
import at.viswars.admin.techtree.ConstructionTechTreeEntry;
import at.viswars.admin.techtree.GroundTroopTechTreeEntry;
import at.viswars.admin.techtree.ModulTechTreeEntry;
import at.viswars.admin.techtree.TechTree;
import at.viswars.admin.techtree.TechnologieTechTreeEntry;
import at.viswars.database.access.DbConnect;

/**
 * Hiermit wird eine Technologie (forschung, Modul, Konstruktion) entfernt
 * Das ist nur mäglich, wenn es keine Abhängigkeiten gibt, die diese Technologie involvieren
 * 
 * @author martin
 */
public class DeleteTechnoAction implements IMyPageProducer {

	private int id;
	private boolean sure;

	/**
	 * 
	 */
	public DeleteTechnoAction(int id, boolean sure) {
		this.id = id;
		this.sure = sure;
	}
	
	/**
	 * Wenn wir schon nachgefragt haben, dann kann die Technologie entfernt werden,
	 * ohne großes Aufsehen zu erregen. 
	 * @return Ob es erfolgreich war, und damit keine Ausgabe mehr durchgeführt werden muss
	 * @throws SQLException Wenn die Datenbank rumzickt ;-)
	 */
	public boolean doAction() throws SQLException
	{
		if (!sure)
			return false;
		if (TechTreeAdmin.techTree == null)
			TechTreeAdmin.loadTechTree();
		TechTree tt = TechTreeAdmin.getTreeItemByID(id);
		
		if ((tt.getDependencies().size() > 0) ||
				(tt.getDependsOn().size() > 0))
			return false;
		
		Statement state = DbConnect.getConnection().createStatement();
		if (tt instanceof TechnologieTechTreeEntry)
			state.execute("DELETE FROM research WHERE id="+tt.getBaseID());
		if (tt instanceof ConstructionTechTreeEntry)
			state.execute("DELETE FROM construction WHERE id="+tt.getBaseID());
		if (tt instanceof ModulTechTreeEntry)
			state.execute("DELETE FROM module WHERE id="+tt.getBaseID());
		if (tt instanceof GroundTroopTechTreeEntry)
			state.execute("DELETE FROM groundtroops WHERE id="+tt.getBaseID());
		
		TechTreeAdmin.forceReload();
		return true;
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.IMyPageProducer#producePage()
	 */
	public String producePage() throws SQLException {
		if (TechTreeAdmin.techTree == null)
			TechTreeAdmin.loadTechTree();
		
		StringBuffer res = new StringBuffer();
		
		TechTree tt = TechTreeAdmin.getTreeItemByID(id);
		if ((sure) || (tt.getDependencies().size() > 0) ||
				(tt.getDependsOn().size() > 0))
		{
			res.append("<h2>"+tt.getTyp()+" \""+tt.getName()+"\" entfernen</h2>");
			res.append("Leider kann diese Technologie nicht entfernt werden, da es noch Abh&auml;ngigkeiten darauf gibt. Solltest du die Technologie l&ouml;schen wollen, entferne bitte erst die Abh&auml;ngigkeiten<br>");
			res.append(TechTreeAdmin.makeLink(tt));
			return res.toString();
		}
		res.append("<h2>"+tt.getTyp()+" \""+tt.getName()+"\" entfernen</h2>");
		res.append("Bist du dir sicher, dass du die "+tt.getTyp()+" ");
		res.append(TechTreeAdmin.makeLink(tt));
		res.append(" entfernen m&ouml;chtest?<br>");
		res.append(TechTreeAdmin.makeLink("Ja, sicher", TechTreeAdmin.ACTION_DELETE_TECH, "id="+id+"&sure=1"));
		res.append("<br>");
		res.append(TechTreeAdmin.makeLink(tt));
		
		return res.toString();
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.IMyPageProducer#setup(java.lang.Object)
	 */
	public void setup(Object o) {
        // Empty on purpose
	}

}
