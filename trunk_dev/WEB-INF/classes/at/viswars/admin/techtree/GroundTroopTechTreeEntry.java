package at.viswars.admin.techtree;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import at.viswars.DebugBuffer;
import at.viswars.admin.db.DBDescription;
import at.viswars.admin.db.IDatabaseColumn;
import at.viswars.database.access.DbConnect;

/**
 * Hiermit werden Bodentruppen beschrieben
 * 
 * @author martin
 */
public class GroundTroopTechTreeEntry extends TechTree {

	/**
	 * Der OFFSET für die IDs, um Verwechslungen auszuschließen
	 */
	public static final int OFFSET = 0x30000000;

	/**
	 * @param ID
	 */
	public GroundTroopTechTreeEntry(int ID) {
		super(ID);
		try {
			Statement statement = DbConnect.getConnection().createStatement();
			ResultSet rs = statement
					.executeQuery("SELECT * FROM groundtroops WHERE id=" + ID);
			if (rs.next())
				this.init(rs);
		} catch (SQLException e) {
			DebugBuffer.writeStackTrace("Error in GroundTroopTechTreeEntry: ",e);
		}
	}

	public GroundTroopTechTreeEntry(ResultSet rs) throws SQLException {
		super(rs.getInt(1));
		init(rs);
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.techtree.TechTree#getAllDBColumns()
	 */
	@Override
	public List<IDatabaseColumn> getAllDBColumns() {
		return DBDescription.alleFelder_groundtroops;
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.techtree.TechTree#getBaseID()
	 */
	@Override
	public int getBaseID() {
		return ID;
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.techtree.TechTree#getID()
	 */
	@Override
	public int getID() {
		return ID + OFFSET;
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.techtree.TechTree#getName()
	 */
	@Override
	public String getName() {
		if (get("name") != null)
			return ""+get("name");
		return "BodenTruppe " + ID;
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.techtree.TechTree#getTable()
	 */
	@Override
	public String getTable() {
		return "groundtroops";
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.techtree.TechTree#getTitle()
	 */
	@Override
	public String getTitle() {
		return "die Bodentruppe";
	}

	/* (non-Javadoc)
	 * @see at.viswars.admin.techtree.TechTree#getTyp()
	 */
	@Override
	public String getTyp() {
		return "Bodentruppe";
	}

	public static int makeID(int int1) {
		return int1 + OFFSET;
	}

}
