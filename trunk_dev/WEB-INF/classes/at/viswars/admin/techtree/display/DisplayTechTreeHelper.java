package at.viswars.admin.techtree.display;

import java.util.ArrayList;
import java.util.List;

import at.viswars.admin.TechTreeDisplayAdmin;
import at.viswars.admin.techtree.ConstructionTechTreeEntry;
import at.viswars.admin.techtree.GroundTroopTechTreeEntry;
import at.viswars.admin.techtree.ModulTechTreeEntry;
import at.viswars.admin.techtree.TechTree;
import at.viswars.admin.techtree.TechnologieTechTreeEntry;

/**
 * @author martin
 *
 */
public class DisplayTechTreeHelper implements Comparable<Object>{

	private final TechTree tt;
	private int level = -1;
	private int xposition;
	private float pull = 0;
	private List<DisplayTechTreeHelper> dependentNoShow = new ArrayList<DisplayTechTreeHelper>();
	private int y;
	private int x;

	/**
	 * @param tt 
	 * 
	 */
	public DisplayTechTreeHelper(TechTree tt) {
		this.tt = tt;
	}

	public int compareTo(Object o) {
		if (o instanceof DisplayTechTreeHelper)
			return new Integer(tt.getID()).compareTo(((DisplayTechTreeHelper)o).tt.getID());
		return new Integer(tt.getID()).compareTo((Integer)o);
	}

	public ArrayList<TechTree> getDependsOn() {
		return tt.getDependsOn();
	}

	public void setLevel(int level) {
		this.level  = level;		
	}
	
	public String toString()
	{
		return tt.getName()+" (level = "+level+", x = "+xposition+")";
	}

	public int getLevel() {
		return level;
	}

	public void setXPos(int i) {
		this.xposition = i;
	}

	public int getXPos() {
		return xposition;
	}

	public String getTitle() {
		return tt.getPlainTitle()+" \""+tt.getName()+"\"";
	}

	public String getColor() {
		if (tt instanceof TechnologieTechTreeEntry)
			return "white";
		if (tt instanceof ModulTechTreeEntry)
			return "green";
		if (tt instanceof ConstructionTechTreeEntry)
			return "blue";
		if (tt instanceof GroundTroopTechTreeEntry)
			return "#FFA000";
		return "red";
	}

	public TechTree getTechno() {
		return tt;
	}

	public ArrayList<TechTree> getDependencies() {
		return tt.getDependencies();
	}

	public void setPull(float f) {
		pull  = f;
	}

	public float getPull() {
		return pull;
	}

	public void addDependentNoShow(DisplayTechTreeHelper dtth) {
		dependentNoShow.add(dtth);
	}
	
	public List<DisplayTechTreeHelper> getDependentNoShow()
	{
		return dependentNoShow;
	}

	public void moveTo(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setRow(int row) {
		level = row;
	}

	public int getHeight() {
		return TechTreeDisplayAdmin.HoeheProElement;
	}

	public int getWidth() {
		return TechTreeDisplayAdmin.BreiteProElement;
	}

	public int getSpace() {
		return 100;
	}

	public int getY() {
		return y;
	}

	public int getX() {
		return x;
	}

	public int getRow() {
		return level;
	}

}
