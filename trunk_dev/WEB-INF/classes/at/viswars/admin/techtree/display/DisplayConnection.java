package at.viswars.admin.techtree.display;


import java.util.ArrayList;
import java.util.List;

import at.viswars.admin.TechTreeDisplayAdmin;
import at.viswars.admin.techtree.TechTree;


/**
 * Hiermit wird die Linie zwischen zwei Technologien angezeigt
 * @author Martin Tsch&ouml;pe
 * @version 0.2.2
 */
public class DisplayConnection
{
	
	/**
	 * Die Linie hat einen Pfeil am Ende
	 */
	public static final int	STYLE_ARROW	= 1;
	/**
	 * Von wo soll die Linie starten
	 */
	private DisplayTechTreeHelper startTechnologie = null;
	/**
	 * Wohin soll die Linie gehen
	 */
	private DisplayTechTreeHelper targetTechnologie = null;
	/**
	 * Wie soll sie aussehen (vgl Konstanten mit STYLE_)
	 */
	private int style = 0;
	
	/**
	 * Die Punkte, die zu verbinden sind
	 */
	private List<DisplayPoint> points = new ArrayList<DisplayPoint>();

	/**
	 * @param from Von welcher Technologie
	 * @param to zu welcher anderen
	 * @param style der Stil (mit Konstanten STYLE_*)
	 */
	public DisplayConnection(DisplayTechTreeHelper from, DisplayTechTreeHelper to, int style)
	{
		super();
		startTechnologie = from;
		targetTechnologie = to;
		this.style = style;
	}

	/**
	 * @return Wo die Linie los geht
	 */
	public TechTree getStartTechnologie()
	{
		return startTechnologie.getTechno();
	}

	/**
	 * @return Wohin die Linie geht
	 */
	public TechTree getTargetTechnologie()
	{
		return targetTechnologie.getTechno();
	}

	/**
	 * @return Wie die Linie aussieht
	 */
	public int getType()
	{
		return style;
	}

	/**
	 * @return Wo die Linie lang geht
	 */
	public List<DisplayPoint> getPoints()
	{
		return points;
	}

	/**
	 * Die Linie ist die direkte Verbindung
	 */
	public void clearPoints()
	{
		points.clear();
	}

	/**
	 * Neuen Zwischenpunkt setzen
	 * @param x Koordinate (relativ)
	 * @param y Koordinate (relativ)
	 * @param allTechnos alle bekannten Technologien
	 */
	public void addPoint(float x, float y, List<DisplayTechTreeHelper> allTechnos)
	{
		points.add(new DisplayPoint(x,y,true, allTechnos));
	}

	/**
	 * Neuen Zwischenpunkt setzen
	 * @param x Koordinate (absolut)
	 * @param y Koordinate (absolut)
	 * @param allTechnos alle bekannten Technologien
	 */
	public void addPointAbs(int x, int y, List<DisplayTechTreeHelper> allTechnos)
	{
		points.add(new DisplayPoint(x,y,false, allTechnos));
	}

	/**
	 * @return Startpunkt für die Linie 
	 */
	public DisplayTechTreeHelper getStart()
	{
		return startTechnologie;
	}

	/**
	 * @return Endpunkt für die Linie 
	 */
	public DisplayTechTreeHelper getTarget()
	{
		return targetTechnologie;
	}

	public String render() {
        int sx = startTechnologie.getX() + startTechnologie.getWidth()/2;
        int sy = startTechnologie.getY() + startTechnologie.getHeight();
        int ex = targetTechnologie.getX() + targetTechnologie.getWidth()/2;
        int ey = targetTechnologie.getY();
        if (points.size() == 0)
        	return "<polyline points=\""+sx+".5 "+sy+".5 "+
        	ex+".5 "+ey+".5 "+"\" fill=\"none\" stroke=\"black\" stroke-width=\""+
        	TechTreeDisplayAdmin.LINE_WIDTH+"px\"/>\n";
        String zwischenpunkte = "";
        for (DisplayPoint dp : points)
        	zwischenpunkte = zwischenpunkte + dp.getX()+".5 "+dp.getY()+".5 ";
    	return "<polyline points=\""+sx+".5 "+sy+".5 "+zwischenpunkte+
    	ex+".5 "+ey+".5 "+"\" fill=\"none\" stroke=\"black\" stroke-width=\""+
    	TechTreeDisplayAdmin.LINE_WIDTH+"px\"/>\n";
	}
}
