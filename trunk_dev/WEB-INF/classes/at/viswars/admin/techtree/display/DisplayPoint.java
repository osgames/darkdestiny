package at.viswars.admin.techtree.display;

import java.util.List;

/**
 * Ein Punkt
 * @author Martin Tsch&ouml;pe
 * @version 0.2.2
 */
public class DisplayPoint
{
	/**
	 * Absolute Koordinaten:X
	 */
	int x;
	/**
	 * Absolute Koordinaten:Y
	 */
	int y;
	/**
	 * Relative Koordinaten:X
	 */
	float relX;
	/**
	 * Relative Koordinaten:Y
	 */
	float relY;

	/**
	 * @param x Anfängliche Koordinaten
	 * @param y Anfängliche Koordinaten
	 * @param relative ob die Koordinaten relativ sind
	 * @param allTechnos alle Technologien
	 */
	public DisplayPoint(float x, float y, boolean relative, List<DisplayTechTreeHelper> allTechnos)
	{
		super();
		if (relative)
		{
			this.relX = x;
			this.relY = y;
			makeAbsolute(allTechnos);
		}
		else
		{
			this.x = Math.round(x);
			this.y = Math.round(y);
			makeRelative(allTechnos);
		}
	}

	/**
	 * Koordinaten in relative umrechnen
	 * @param allTechnos alle Technologien
	 */
	protected void makeRelative(List<DisplayTechTreeHelper> allTechnos)
	{
		DisplayTechTreeHelper dt = ((DisplayTechTreeHelper)allTechnos.get(0));
		relX = x *1.0f / (dt.getWidth() + dt.getSpace());
		for (int i = 0; i < allTechnos.size(); i++)
		{
			DisplayTechTreeHelper dp = ((DisplayTechTreeHelper)allTechnos.get(i));
			if ((y >= dp.getY()) && (y < dp.getHeight() + dp.getSpace() + dp.getY()))
			{
				relY = dp.getRow() + (y - dp.getY())*1.0f/(dp.getHeight() + dp.getSpace());
				return;
			}
		}
	}

	/**
	 * Koordinaten in absolute umrechnen
	 * @param allTechnos alle Technologien
	 */
	protected void makeAbsolute(List<DisplayTechTreeHelper> allTechnos)
	{
		DisplayTechTreeHelper dt = ((DisplayTechTreeHelper)allTechnos.get(0));
		x = Math.round(relX * (dt.getWidth() + dt.getSpace()));
		int level = Math.round((float)Math.floor(relY));
		for (int i = 0; i < allTechnos.size(); i++)
		{
			DisplayTechTreeHelper dp = ((DisplayTechTreeHelper)allTechnos.get(i));
			if (level == dp.getRow())
			{
				y = dp.getY() + Math.round((relY - dp.getRow())*(dp.getHeight() + dp.getSpace()));
				return;
			}
		}
	}

	/**
	 * @return Absolute X-Koordinate
	 */
	public int getX()
		{
		return x;
		}
	
	/**
	 * @return Absolute Y-Koordinate
	 */
	public int getY()
		{
		return y;
		}

	/**
	 * @return Relative X-Koordinate
	 */
	public float getRelX()
	{
		return relX;
	}

	/**
	 * @return Relative Y-Koordinate
	 */
	public float getRelY()
	{
		return relY;
	}
	
	public String toString()
	{
		return x+","+y;
	}

}
