package at.viswars.admin.techtree;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import at.viswars.DebugBuffer;
import at.viswars.admin.db.DBDescription;
import at.viswars.admin.db.IDatabaseColumn;
import at.viswars.database.access.DbConnect;

/**
 * 
 * @author martin
 */
public class ConstructionTechTreeEntry extends TechTree {

	/**
	 * Der OFFSET für die IDs, um Verwechslungen auszuschließen
	 */
	public static final int OFFSET = 0x10000000;
	
	/**
	 * @param ID
	 */
	public ConstructionTechTreeEntry(int ID) {
		super(ID);

		try {
			Statement statement = DbConnect.getConnection().createStatement();
			ResultSet rs = statement
					.executeQuery("SELECT * FROM construction WHERE id=" + ID);
			if (rs.next())
				this.init(rs);
		} catch (SQLException e) {
			DebugBuffer.writeStackTrace("Error in ConstructionTechTreeEntry: ",e);
		}
		
	}

	public ConstructionTechTreeEntry(ResultSet rs) throws SQLException{
		super(rs.getInt(1));

		this.init(rs);
	}

	public String toString()
	{
		return "Eintrag, Typ Construction, ID = "+ID;
	}

	@Override
	public int getID() {
		return ID+OFFSET;
	}

	@Override
	public String getName() {
		if (get("name") != null)
			return ""+get("name");
		return "Construction"+ID;
	}

	@Override
	public String getTyp() {
		return "Construction";
	}

	public static int makeID(int int1) {
		return int1+OFFSET;
	}

	@Override
	public int getBaseID() {
		return ID;
	}

	@Override
	public List<IDatabaseColumn> getAllDBColumns() {
		return DBDescription.alleFelder_construction;
	}

	@Override
	public String getTitle() {
		return "das Geb&auml;ude";
	}

	@Override
	public String getPlainTitle() {
		return "das Geb&auml;ude";
	}

	@Override
	public String getTable() {
		return "construction";
	}
}
