package at.viswars.admin.techtree;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import at.viswars.admin.db.IDatabaseColumn;

/**
 * Ein Eintrag im TechTree
 * 
 * @author martin
 */
public abstract class TechTree {

	protected int ID;
	private ArrayList<TechTree> dependsOn = new ArrayList<TechTree>();
	private ArrayList<TechTree> dependencies = new ArrayList<TechTree>();
	private TreeMap<String, Object> allProperties = new TreeMap<String, Object>();


	/**
	 * 
	 */
	public TechTree(int ID) 
	{
		this.ID = ID;
	}
	
	protected void init(ResultSet rs) throws SQLException {
		for (IDatabaseColumn idc : getAllDBColumns())
			allProperties.put(idc.getName(), idc.getFromResultSet(rs));
	}

	public String toString()
	{
		return "Eintrag, Typ unbekannt, ID = "+ID;
	}

	public void addRequirement(TechTree requirement) {
		dependsOn.add(requirement);
		requirement.dependencies.add(this);
	}

	public ArrayList<TechTree> getDependsOn() {
		return dependsOn;
	}

	public ArrayList<TechTree> getDependencies() {
		return dependencies;
	}
	
	public abstract String getName();
	public abstract String getTyp();
	public abstract int getID();
	public abstract int getBaseID();
	public abstract String getTitle();
	public abstract List<IDatabaseColumn> getAllDBColumns();

	public Object get (String key)
	{
		return allProperties.get(key);
	}

	public abstract String getTable();

	public String getPlainTitle()
	{
		return getTitle();
	}
}
