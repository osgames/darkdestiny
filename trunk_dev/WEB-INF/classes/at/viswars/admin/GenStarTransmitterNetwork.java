/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.admin;

import at.viswars.dao.*;
import at.viswars.databuffer.fleet.*;
import at.viswars.model.Galaxy;
import at.viswars.model.SunTransmitter;
import at.viswars.model.SunTransmitterRoute;
import at.viswars.model.System;
import at.viswars.tools.graph.Edge;
import at.viswars.tools.graph.Graph;
import at.viswars.tools.graph.Node;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Stefan
 */
public class GenStarTransmitterNetwork {
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static GalaxyDAO gDAO = (GalaxyDAO) DAOFactory.get(GalaxyDAO.class);
    private static SunTransmitterDAO stDAO = (SunTransmitterDAO) DAOFactory.get(SunTransmitterDAO.class);
    private static SunTransmitterRouteDAO strDAO = (SunTransmitterRouteDAO) DAOFactory.get(SunTransmitterRouteDAO.class);
    
    public void generate() {
        // Select a random star from a galaxy and place a transmitter there
        // depending on size of the galaxy there can only be a certain amount of transmitters
        stDAO.removeAll();
        strDAO.removeAll();
        
        ArrayList<Integer> unprocessedSystems = new ArrayList<Integer>();
        HashMap<Integer,ArrayList<System>> transmitters = 
                new HashMap<Integer,ArrayList<System>>();
        HashMap<Integer,Integer> transmitterCountNeed = 
                new HashMap<Integer,Integer>();        
        
        for (System s : (ArrayList<System>)sDAO.findAll()) {
            unprocessedSystems.add(s.getId());
        }
        
        for (Galaxy g : (ArrayList<Galaxy>)gDAO.findAll()) {
            int transmitterCount = (int)Math.ceil((g.getEndSystem() - g.getStartSystem()) / 1000d);    
            transmitterCountNeed.put(g.getId(),transmitterCount);
        }
        
        // Get a random system
        // Determine Galaxy Id .. check if max Count is reached
        // If yes continue -- otherwise check if there already is a transmitter and if yes define a minimum distance
        boolean satisfied = false;
        
        while (!satisfied && (unprocessedSystems.size() > 0)) {
            int currSys = unprocessedSystems.get((int)Math.floor(unprocessedSystems.size() * Math.random()));
                        
            System actSystem = sDAO.findById(currSys);            
            Integer galaxyId = actSystem.getGalaxyId();

            Galaxy g = gDAO.findById(galaxyId);
            
            if (transmitters.get(g.getId()) == null) {
                // We are planting the first transmitter to this galaxy - ITS OK
                ArrayList<System> sList = new ArrayList<System>();
                sList.add(actSystem);
                transmitters.put(g.getId(), sList);
                
                java.lang.System.out.println("[BASIC] Created Suntransmitter at " + actSystem.getX() + ":" + actSystem.getY());
            } else {
                // Check if this galaxy already has maximum transmitter Count
                int allowed = transmitterCountNeed.get(g.getId());
                int created = transmitters.get(g.getId()).size();
                
                if (created < allowed) {
                    // We can do it .. ITS OKAY
                    double minDistance = ((double)g.getWidth() + (double)g.getHeight()) / 4d;
                    
                    AbsoluteCoordinate acCurr = new AbsoluteCoordinate(actSystem.getX(),actSystem.getY());
                    System newToAdd = null;
                
                    boolean distanceOK = true;
                    
                    for (System compSys : transmitters.get(g.getId())) {
                        AbsoluteCoordinate acComp = new AbsoluteCoordinate(compSys.getX(),compSys.getY());
                        
                        if (acCurr.distanceTo(acComp) >= minDistance) {
                            // ArrayList<System> sList = transmitters.get(g.getId());
                            // sList.add(actSystem);
                            java.lang.System.out.println("DISTANCE IS " + acCurr.distanceTo(acComp)+ " MINDISTANCE="+minDistance);
                            newToAdd = actSystem;               
                        } else {
                            distanceOK = false;
                        }
                    }
                    
                    if (distanceOK) {                    
                        if (newToAdd != null) {
                            ArrayList<System> sList = transmitters.get(g.getId());
                            sList.add(actSystem);    
                            java.lang.System.out.println("[ADDITIONAL] Created Suntransmitter at " + actSystem.getX() + ":" + actSystem.getY());
                        }
                    }
                } else {
                    java.lang.System.out.println("Max count reached ["+created+">="+allowed+"]");
                }
            }
            
            unprocessedSystems.remove((Integer)currSys);
        }
        
        // Create Database Entries
        /* HashMap<Integer,ArrayList<System>> transmitters = 
                new HashMap<Integer,ArrayList<System>>();  */
        Graph g = new Graph();
        
        for (Map.Entry<Integer,ArrayList<System>> entry : transmitters.entrySet()) {
            int i = 0;
            
            for (System s : entry.getValue()) {
                i++;
                
                SunTransmitter st = new SunTransmitter();
                st.setSystemId(s.getId());
                st.setPlanetId(0);
                st.setUserId(0);
                st = stDAO.add(st);
                
                SunTransmitterEntry ste = new SunTransmitterEntry(st,entry.getKey());              
                Node n = new Node("Trans " + entry.getKey() + "-" + i,ste);               
                
                g.addNode(n);
            }
        }        
        
        // All nodes have been created
        // Connect intra Galaxy
        ArrayList<Integer> unConnectedGalaxies = new ArrayList<Integer>();
        ArrayList<Integer> connectedGalaxies = new ArrayList<Integer>();
        
        for (Galaxy gal : gDAO.findAll()) {
            unConnectedGalaxies.add(gal.getId());
            
            // Get all Nodes for a galaxy
            int galId = gal.getId();
            
            ArrayList<Node> unConnected = new ArrayList<Node>();
            ArrayList<Node> connected = new ArrayList<Node>();
            
            for (Node n : g.getNodes()) {
                SunTransmitterEntry ste = (SunTransmitterEntry)n.getLoad();
                if (ste.galaxyId == galId) unConnected.add(n);
            }
            
            switch (unConnected.size()) {
                case(1): // Nothing to do
                    continue;
                case(2): // Trivial
                    g.addEdge(unConnected.get(0), unConnected.get(1));
                    continue;
                case(3): // Build randomized network
                    while (!unConnected.isEmpty()) {
                        if (connected.isEmpty()) {
                            // Connect 2 random unconnected nodes
                            Node n1 = unConnected.get((int)Math.floor(Math.random() * unConnected.size()));
                            Node n2 = unConnected.get((int)Math.floor(Math.random() * unConnected.size()));
                            while (n1.equals(n2)) {
                                n2 = unConnected.get((int)Math.floor(Math.random() * unConnected.size()));
                            }
                            
                            unConnected.remove(n1);
                            unConnected.remove(n2);
                            connected.add(n1);
                            connected.add(n2);
                            
                            g.addEdge(n1, n2);
                        } else if (!unConnected.isEmpty()) {
                            // Connect a free node to a random assigned node
                            Node n1 = unConnected.get((int)Math.floor(Math.random() * unConnected.size()));
                            Node n2 = connected.get((int)Math.floor(Math.random() * connected.size()));
                            
                            g.addEdge(n1,n2);
                            unConnected.remove(n1);
                            connected.add(n1);
                        } 
                    }                   
            }
        }
        
        // We have connected all galaxies inside
        // Now we have to connect the galaxies to each other
        while (!unConnectedGalaxies.isEmpty()) {
            if (connectedGalaxies.isEmpty()) {
                // Connect 2 random nodes from 2 random galaxies
                int gal1 = unConnectedGalaxies.get((int)Math.floor(Math.random() * unConnectedGalaxies.size()));
                int gal2 = unConnectedGalaxies.get((int)Math.floor(Math.random() * unConnectedGalaxies.size()));

                while (gal1 == gal2) {
                    gal2 = unConnectedGalaxies.get((int)Math.floor(Math.random() * unConnectedGalaxies.size()));
                }

                ArrayList<Node> nodesGal1 = new ArrayList<Node>();
                ArrayList<Node> nodesGal2 = new ArrayList<Node>();
                
                for (Node n : g.getNodes()) {
                    SunTransmitterEntry ste = (SunTransmitterEntry)n.getLoad();
                    if (ste.galaxyId == gal1) {
                        nodesGal1.add(n);
                    } else if (ste.galaxyId == gal2) {
                        nodesGal2.add(n);
                    }
                }
                
                Node n1 = nodesGal1.get((int)Math.floor(nodesGal1.size() * Math.random()));
                Node n2 = nodesGal2.get((int)Math.floor(nodesGal2.size() * Math.random()));
                
                g.addEdge(n1,n2);
                
                unConnectedGalaxies.remove((Integer)gal1);
                unConnectedGalaxies.remove((Integer)gal2);
                connectedGalaxies.add(gal1);
                connectedGalaxies.add(gal2);
            } else if (!unConnectedGalaxies.isEmpty()) {
                // Connect a free node to a random assigned node
                int gal1 = unConnectedGalaxies.get((int)Math.floor(Math.random() * unConnectedGalaxies.size()));
                int gal2 = connectedGalaxies.get((int)Math.floor(Math.random() * connectedGalaxies.size()));

                ArrayList<Node> nodesGal1 = new ArrayList<Node>();
                ArrayList<Node> nodesGal2 = new ArrayList<Node>();
                
                for (Node n : g.getNodes()) {
                    SunTransmitterEntry ste = (SunTransmitterEntry)n.getLoad();
                    if (ste.galaxyId == gal1) {
                        nodesGal1.add(n);
                    } else if (ste.galaxyId == gal2) {
                        nodesGal2.add(n);
                    }
                }
                
                Node n1 = nodesGal1.get((int)Math.floor(nodesGal1.size() * Math.random()));
                Node n2 = nodesGal2.get((int)Math.floor(nodesGal2.size() * Math.random()));
                
                g.addEdge(n1,n2);
                
                unConnectedGalaxies.remove((Integer)gal1);
                connectedGalaxies.add(gal1);
            }             
        }       
        
        // Add all determined routes
        for (Edge e : g.getEdges()) {
            SunTransmitterRoute str = new SunTransmitterRoute();
            str.setStartId(((SunTransmitterEntry)e.getNode1().getLoad()).stObj.getId());
            str.setEndId(((SunTransmitterEntry)e.getNode2().getLoad()).stObj.getId());
            strDAO.add(str);
        }
    }
    
    private class SunTransmitterEntry {
        private final SunTransmitter stObj;
        private final int galaxyId;
        
        public SunTransmitterEntry(SunTransmitter stObj, int galaxyId) {
            this.stObj = stObj;
            this.galaxyId = galaxyId;
        }
    }
}
