/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.admin;

import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.RessourceCostDAO;
import at.viswars.enumeration.ERessourceCostType;
import at.viswars.model.RessourceCost;
import java.util.ArrayList;

/**
 *
 * @author Rudolf2
 */
public class DeleteAllTest {
    public static RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    
    public static void testDeleteAll() {
        // Create 5 test entries
        RessourceCost rc = new RessourceCost();
        rc.setId(1);
        rc.setRessId(1);
        rc.setType(ERessourceCostType.MODULE);
        rc.setQty(1000l);
        rcDAO.add(rc);
        
        rc.setId(2);
        rc.setRessId(1);
        rc.setType(ERessourceCostType.MODULE);
        rc.setQty(2000l);
        rcDAO.add(rc);
        
        rc.setId(3);
        rc.setRessId(1);
        rc.setType(ERessourceCostType.MODULE);
        rc.setQty(3000l);
        rcDAO.add(rc);
        
        rc.setId(4);
        rc.setRessId(1);
        rc.setType(ERessourceCostType.MODULE);
        rc.setQty(4000l);
        rcDAO.add(rc);
        
        rc.setId(5);
        rc.setRessId(1);
        rc.setType(ERessourceCostType.MODULE);
        rc.setQty(5000l);
        rcDAO.add(rc);
        
        RessourceCost rcFind = new RessourceCost();
        rcFind.setType(ERessourceCostType.MODULE);
        ArrayList<RessourceCost> rcList = rcDAO.find(rcFind);
        
        Logger.getLogger().write("[After Creation] Found " + rcList.size() + " entries");
        
        rcDAO.removeAll(rcList);
        rcList = rcDAO.find(rcFind);
        
        Logger.getLogger().write("[After DeleteAll] Found " + rcList.size() + " entries");
    }
}
