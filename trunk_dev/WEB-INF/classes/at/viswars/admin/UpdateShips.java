/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.admin;

import at.viswars.admin.UpdateShipPreResult.State;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.DesignModuleDAO;
import at.viswars.dao.ShipDesignDAO;
import at.viswars.dao.UserDataDAO;
import at.viswars.model.DesignModule;
import at.viswars.model.ShipDesign;
import at.viswars.model.UserData;
import at.viswars.service.OverviewService;
import at.viswars.service.ShipDesignService;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class UpdateShips {
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static DesignModuleDAO dmDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);

    private HashMap<String,ShipDesign> oldDesigns = new HashMap<String,ShipDesign>();
    private HashMap<String,ShipDesign> newDesigns = new HashMap<String,ShipDesign>();

    public ArrayList<UpdateShipPreResult> checkForUpdates(int userId) {
        ArrayList<UpdateShipPreResult> result = new ArrayList<UpdateShipPreResult>();

        ArrayList<ShipDesign> sdList = sdDAO.findByUserId(userId);
        for (ShipDesign sd : sdList) {
            if (sd.getName().startsWith("NEU_")) {
                newDesigns.put(sd.getName(),sd);
            } else {
                oldDesigns.put(sd.getName(),sd);
            }
        }

        for (ShipDesign sd : newDesigns.values()) {
            for (ShipDesign sd2 : oldDesigns.values()) {
                if (sd.getName().replace("NEU_", "").trim().equalsIgnoreCase(sd2.getName().trim())) {
                    ArrayList<DesignModule> dmOld = dmDAO.findByDesignId(sd2.getId());
                    ArrayList<DesignModule> dmNew = dmDAO.findByDesignId(sd.getId());

                    State state = State.VALID;

                    if (!(sd.getChassis().equals(sd2.getChassis()))) {
                        state = State.INVALID_CHASSIS;
                    }

                    if (state == State.VALID) {
                        if (sd.getType() != sd2.getType()) {
                            state = State.INVALID_TYPE;
                        }
                    }

                    if (state == State.VALID) {
                        for (DesignModule dm : dmNew) {
                            boolean contains = false;

                            for (DesignModule dm2 : dmOld) {
                                if (dm2.getModuleId().equals(dm.getModuleId())) contains = true;
                            }

                            if (!contains) state = State.INVALID_MODULES;
                        }
                    }

                    UpdateShipPreResult uspr = new UpdateShipPreResult(sd2,sd,state);
                    result.add(uspr);
                    break;
                }
            }
        }

        return result;
    }

    public synchronized boolean processChanges(int userId) {
        UserData ud = OverviewService.findUserData(userId);
        if (ud.getTemporary() == 1) return false;

        ArrayList<UpdateShipPreResult> result = checkForUpdates(userId);

        for (UpdateShipPreResult uspr : result) {
            if (uspr.getState() != State.VALID) {
                return false;
            }
        }

        for (UpdateShipPreResult uspr : result) {
            ArrayList<DesignModule> dmOld = dmDAO.findByDesignId(uspr.getOldDesign().getId());
            ArrayList<DesignModule> dmNew = dmDAO.findByDesignId(uspr.getNewDesign().getId());

            for (DesignModule dm : dmOld) {
                dmDAO.remove(dm);
            }

            for (DesignModule dm : dmNew) {
                DesignModule newModule = new DesignModule();
                newModule.setDesignId(uspr.getOldDesign().getId());
                newModule.setIsConstruction(false);
                newModule.setModuleId(dm.getModuleId());
                newModule.setCount(dm.getCount());
                dmDAO.add(newModule);
            }

            ShipDesignService.updateDesignCosts(uspr.getOldDesign().getId());
        }

        ud.setTemporary(1);
        udDAO.update(ud);

        return true;
    }
}
