/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.admin;

import at.viswars.CheckForUpdate;
import at.viswars.GameConfig;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.GameDataDAO;
import at.viswars.model.GameData;

/**
 *
 * @author Stefan
 */
public class AdminService {
    private static GameDataDAO gdDAO = (GameDataDAO)DAOFactory.get(GameDataDAO.class);
    
    public static long skipTicks(int userId, int ticks) {
        GameData gd = (GameData)gdDAO.findAll().get(0);
        
	int tickTime = gd.getTickTime();
	int lastUpdatedTick = gd.getLastUpdatedTick();
        
        long time = System.currentTimeMillis() - tickTime * ((long)lastUpdatedTick+ticks);
        gd.setStartTime(time);
        gdDAO.update(gd);
        
        GameConfig.rereadValues();
        CheckForUpdate.requestUpdate();
        
        return lastUpdatedTick;
    }
}
