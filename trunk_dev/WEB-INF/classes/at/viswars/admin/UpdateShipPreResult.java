/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.admin;

import at.viswars.model.ShipDesign;

/**
 *
 * @author Stefan
 */
public class UpdateShipPreResult {
    private final ShipDesign oldDesign;
    private final ShipDesign newDesign;
    private final State state;

    /**
     * @return the oldDesign
     */
    public ShipDesign getOldDesign() {
        return oldDesign;
    }

    /**
     * @return the newDesign
     */
    public ShipDesign getNewDesign() {
        return newDesign;
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    public enum State {
        VALID, INVALID_CHASSIS, INVALID_TYPE, INVALID_MODULES, INVALID_CHANGES;
    }

    public UpdateShipPreResult(ShipDesign oldDesign, ShipDesign newDesign, State state) {
        this.oldDesign = oldDesign;
        this.newDesign = newDesign;
        this.state = state;
    }
}
