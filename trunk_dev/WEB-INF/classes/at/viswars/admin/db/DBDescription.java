package at.viswars.admin.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import at.viswars.DebugBuffer;
import at.viswars.GameConfig;
import at.viswars.database.access.DbConnect;
import at.viswars.enumeration.EConstructionProdType;

import static at.viswars.GameConstants.*;

/**
 * Hiermit werden die Tabellen beschrieben
 * Für jedes Feld wird ein Kommentar, die Spalte in der Tabelle, und eine Darstellung 
 * für ein Eingabefeld hinterlegt
 * 
 * @author martin
 */
public class DBDescription {

	/**
	 * Hiermit werden die Verschiedenen Datentypen dargestellt
	 */
	private static final int FIELD_INTEGER = 0x01;
	private static final int FIELD_STRING = 0x02;
	private static final int FIELD_SELECT = 0x04;	//Ein Integer, der aus einer Liste gewählt wird.
	private static final int FIELD_LONGINTEGER = 0x08;
	private static final int FIELD_BOOLEAN_INT = 0x10;  //Ein Integer, der als Boolean interpretiert wird
	private static final int FIELD_STRING_IMG_FILENAME = 0x20; //Ein String, der als Pfad zu einem Bild interpretiert wird

	private static final int FIELD_READONLY = 0x1000;
	private static final int FIELD_MULTILINE = 0x2000;
	private static final int FIELD_STRING_NULL = 0x4000;
	
    public final static int BUILDING_VERWALTUNG = 1;
    public final static int BUILDING_ZIVIL = 2;
    public final static int BUILDING_ENERGIE = 3;
    public final static int BUILDING_INDUSTRY = 4;
    public final static int BUILDING_MILITAER =5;
    
    public final static int START_ID_DESIGNS = 9000;

	
	public static ArrayList<IDatabaseColumn> alleFelder_module;
	public static ArrayList<IDatabaseColumn> alleFelder_construction;
	public static ArrayList<IDatabaseColumn> alleFelder_research;
	public static ArrayList<IDatabaseColumn> alleFelder_groundtroops;
	
	/**
	 * 
	 */
	protected DBDescription() {}
	
	static {
		try {
			initDBDescription();
		} catch (SQLException e) {
			DebugBuffer.writeStackTrace("Error in DBDescription: ",e);
		}
	}

	private static void initDBDescription() throws SQLException {
		
	//Alle Daten für die Module-Tabelle laden
		alleFelder_module = new ArrayList<IDatabaseColumn>();
		add(alleFelder_module, "id", FIELD_INTEGER | FIELD_READONLY, "ID dieses Moduls");
		add(alleFelder_module, "name", FIELD_STRING, "Name dieses Moduls");
		add(alleFelder_module, "type", FIELD_SELECT, "Typ dieses Moduls", 
			    MODULE_HYPER_ENGINE, "Überlicht - Antrieb",
			    MODULE_ARMOR,"Panzerung",
			    MODULE_WEAPON,"Bewaffnung",
			    MODULE_ENGINE, "Antrieb",
			    MODULE_CHASSIS, "Rumpf",
			    MODULE_ROCKET, "Rakete",
			    MODULE_FREE_2, "Frei 2",
			    MODULE_SHIELD, "Schild",
			    MODULE_WEAPON_SUPPORT, "Waffenhalterung",
			    MODULE_REACTOR, "Reaktor",
			    MODULE_SPECIAL, "Spezial",
			    MODULE_HANGAR, "Hangar");
		add(alleFelder_module, "buildTime", FIELD_INTEGER, "Wie lange dauert es dieses Modul zu bauen");
		add(alleFelder_module, "value ", FIELD_INTEGER, "Wieviel Platz braucht das Modul");
		add(alleFelder_module, "energy ", FIELD_INTEGER, "Wieviel Energie braucht das Modul");
		add(alleFelder_module, "credits ", FIELD_LONGINTEGER, "Was kostet das Modul");
		add(alleFelder_module, "iron ", FIELD_LONGINTEGER, "Wieviel Eisen braucht es um das Modul zu bauen");
		add(alleFelder_module, "steel ", FIELD_LONGINTEGER, "Wieviel Stahl braucht es um das Modul zu bauen");
		add(alleFelder_module, "terkonit ", FIELD_LONGINTEGER, "Wieviel terkonit braucht es um das Modul zu bauen");
		add(alleFelder_module, "ynkelonium ", FIELD_LONGINTEGER, "Wieviel ynkelonium braucht es um das Modul zu bauen");
		add(alleFelder_module, "howalgonium ", FIELD_LONGINTEGER, "Wieviel Howalgonium braucht es um das Modul zu bauen");
		add(alleFelder_module, "cvEmbinium ", FIELD_LONGINTEGER, "Wieviel cvEmbinium braucht es um das Modul zu bauen");
		add(alleFelder_module, "baseStructure ", FIELD_INTEGER, "Anzahl der HitPoints / Strukturpunkte");		//Bez. die Strukturpunkte (= HitPoints)
		add(alleFelder_module, "space ", FIELD_INTEGER, "Platzbedarf bzw. wieviel Platz stellt das Chassis zur Verf�gung");				//Platzbedarf / bzw. zur Verf�gung stellen
		add(alleFelder_module, "isChassis ", FIELD_BOOLEAN_INT, "Ist die ein Rumpf");			//1 oder 0
		add(alleFelder_module, "uniquePerShip ", FIELD_BOOLEAN_INT, "Ist diese Einbaut einmalig pro Schiff");		//1 oder 0
		add(alleFelder_module, "freeForDesign ", FIELD_BOOLEAN_INT, "Taucht dieses Modul in der Design-Liste auf");		//1 oder 0, taucht in der Designliste auf
		add(alleFelder_module, "uniquePerType ", FIELD_BOOLEAN_INT, "Darf von diesem Typ nur ein Element eingebaut werden");		//1 oder 0, Antriebe
		add(alleFelder_module, "chassisSize ", FIELD_INTEGER, "Chassis-Gr��e, nur zur Sortierung");		//sorierung
		add(alleFelder_module, "isConstruct ", FIELD_BOOLEAN_INT, "Wird dieses Modul im Konstruktionsmenu angezeigt? (andernfalls findet es sich unter Produktion)");		//1 oder 0, Konstruktionsmenu bzw. Produktionsmenu
		add(alleFelder_module, "orbitalDockPoints ", FIELD_INTEGER, "Anzahl der Konstruktionspunkte in der Orbit-Werft, die f�r den Bau notwendig sind");	//Wieviele Konstruktionspunkte f�r bau
		add(alleFelder_module, "groundDockPoints ", FIELD_INTEGER, "Anzahl der Konstruktionspunkte in der Werft am Boden, die f�r den Bau notwendig sind");  //Wieviele Konstruktionspunkte f�r bau
		add(alleFelder_module, "modulPoints ", FIELD_INTEGER, "Anzahl der Konstruktionspunkte in der Modulfabrik, die f�r den Bau notwendig sind");		//Wieviele Konstruktionspunkte f�r bau
		
	//Alle Felder f�r die construction Tabelle laden	
		alleFelder_construction = new ArrayList<IDatabaseColumn>();
		add(alleFelder_construction, "id", FIELD_INTEGER | FIELD_READONLY, "ID dieses Geb&auml;udes");
		add(alleFelder_construction, "name", FIELD_STRING, "Name dieses Geb&auml;udes");
		add(alleFelder_construction, "infotext", FIELD_STRING, "Kurze Beschreibung dieses Geb&auml;udes");
		add(alleFelder_construction, "typplanet", FIELD_STRING, "Wo kann dieses Geb&auml;ude gebaut werden? (Kombination aus ABCGMLJ)");
		add(alleFelder_construction, "credits ", FIELD_LONGINTEGER, "Was kostet das Geb&auml;udes");
		add(alleFelder_construction, "buildTime", FIELD_INTEGER, "Wie lange dauert es dieses Geb&auml;udes zu bauen");
		add(alleFelder_construction, "iron ", FIELD_LONGINTEGER, "Wieviel Eisen braucht es um das Geb&auml;udes zu bauen");
		add(alleFelder_construction, "steel ", FIELD_LONGINTEGER, "Wieviel Stahl braucht es um das Geb&auml;udes zu bauen");
		add(alleFelder_construction, "terkonit ", FIELD_LONGINTEGER, "Wieviel terkonit braucht es um das Geb&auml;udes zu bauen");
		add(alleFelder_construction, "ynkelonium ", FIELD_LONGINTEGER, "Wieviel ynkelonium braucht es um das Geb&auml;udes zu bauen");
		add(alleFelder_construction, "howalgonium ", FIELD_LONGINTEGER, "Wieviel Howalgonium braucht es um das Geb&auml;udes zu bauen");
		add(alleFelder_construction, "cvEmbinium ", FIELD_LONGINTEGER, "Wieviel cvEmbinium braucht es um das Geb&auml;udes zu bauen");
		add(alleFelder_construction, "type", FIELD_SELECT, "Typ dieses Geb&auml;udes", 
			    BUILDING_VERWALTUNG, "Verwaltung",
			    BUILDING_ZIVIL,"Zivil",
			    BUILDING_ENERGIE,"Energie",
			    BUILDING_INDUSTRY, "Industrie",
			    BUILDING_MILITAER, "Milit�r");
		add(alleFelder_construction, "orderNo ", FIELD_INTEGER, "Reihenfolge innerhalb der Kathegorie");//Reihenfolge innerhalb der Kathegorie
		add(alleFelder_construction, "energy ", FIELD_INTEGER, "VERALTET");
		add(alleFelder_construction, "population ", FIELD_INTEGER, "MaximalBev�lkerung steigt um");
		add(alleFelder_construction, "food ", FIELD_INTEGER, "VERALTET");
		add(alleFelder_construction, "prodType", FIELD_SELECT, "Produktionstyp dieses Geb&auml;udes", 
				0, "Keine",
				EConstructionProdType.AGRAR, "Agrar",
				EConstructionProdType.INDUSTRY,"Industrie",
				EConstructionProdType.ENERGY,"Energie",
				EConstructionProdType.RESEARCH, "Forschung",
				EConstructionProdType.COMMON, "Anderes");
		add(alleFelder_construction, "hp", FIELD_INTEGER, "HitPoints f�r Bombadierung");
		add(alleFelder_construction, "mobileDefense", FIELD_BOOLEAN_INT, "Unbekannt");		//1 oder 0
		add(alleFelder_construction, "stationaryDefense", FIELD_BOOLEAN_INT, "Unbekannt");		//1 oder 0
		add(alleFelder_construction, "designId", FIELD_SELECT, "Zu welchem Schiffsdesign ist dieses Zugeordnet" , DbConnect.getConnection().createStatement().executeQuery("SELECT id, name FROM shipdesigns WHERE id > "+START_ID_DESIGNS));// Planetare Verteidigungsanlagen, => Schiff f�r Verteidigungsanlagen (IDs > 9000)
		add(alleFelder_construction, "workers", FIELD_INTEGER, "Arbeiter notwendig");
		add(alleFelder_construction, "surface", FIELD_INTEGER, "Platzbedarf");
		add(alleFelder_construction, "uniquePerPlanet ", FIELD_BOOLEAN_INT, "Ist dieses Geb&auml;ude einmalig pro Planet");		//1 oder 0
		add(alleFelder_construction, "uniquePerSystem ", FIELD_BOOLEAN_INT, "Ist dieses Geb&auml;ude einmalig pro System");		//1 oder 0
		add(alleFelder_construction, "isMine ", FIELD_BOOLEAN_INT, "Ist dieses Geb&auml;ude eine Mine");		//1 oder 0
		add(alleFelder_construction, "specialBuildingId", FIELD_INTEGER, "F�r Aktionen, wenn dieses Geb�ude fertiggestellt wird");
		add(alleFelder_construction, "minPop", FIELD_INTEGER, "Minimale Bev�lkerung um das Bauen zu d�rfen");

	// Alle Felder f�r die research Tabelle laden
		alleFelder_research = new ArrayList<IDatabaseColumn>();
		add(alleFelder_research, "id", FIELD_INTEGER | FIELD_READONLY, "ID dieser Forschung");
		add(alleFelder_research, "name", FIELD_STRING, "Name dieser Forschung");
		add(alleFelder_research, "rp ", FIELD_INTEGER, "Anzahl der Forschungspunkte notwendig, um dieses zu entdecken");
		add(alleFelder_research, "shortText", FIELD_STRING | FIELD_STRING_NULL, "Eine kurze Beschreibung");
		add(alleFelder_research, "smallPic", FIELD_STRING_IMG_FILENAME | FIELD_STRING_NULL, "Ein kleines Bild", GameConfig.picPath()+"pic/research/");
		add(alleFelder_research, "detailText", FIELD_STRING | FIELD_STRING_NULL | FIELD_MULTILINE, "Eine l�ngere Beschreibung");
		add(alleFelder_research, "largePic", FIELD_STRING_IMG_FILENAME | FIELD_STRING_NULL, "Ein gr��eres Bild", GameConfig.picPath()+"pic/research/");

		
		alleFelder_groundtroops = new ArrayList<IDatabaseColumn>();
		add(alleFelder_groundtroops,"id", FIELD_INTEGER | FIELD_READONLY, "ID dieser Truppe");
		add(alleFelder_groundtroops,"name", FIELD_STRING | FIELD_STRING_NULL, "Name dieser Truppe");
		add(alleFelder_groundtroops,"armorclass", FIELD_INTEGER, "armorclass dieser Truppe");
		add(alleFelder_groundtroops,"strengthmin", FIELD_INTEGER, "Minimale Angriffsst�rke");
		add(alleFelder_groundtroops,"strengthmax", FIELD_INTEGER, "Maximale Angriffsst�rke");
		add(alleFelder_groundtroops,"hitpoints", FIELD_INTEGER, "hitpoints dieser Truppe");
		add(alleFelder_groundtroops,"build", FIELD_INTEGER, "unused");
		add(alleFelder_groundtroops,"credits", FIELD_INTEGER, "Kosten (Credits)");
		add(alleFelder_groundtroops,"iron", FIELD_INTEGER, "Eisen-Menge f�r die Produktion");
		add(alleFelder_groundtroops,"steel", FIELD_INTEGER, "Stahl-Menge f�r die Produktion");
		add(alleFelder_groundtroops,"terkonit", FIELD_INTEGER, "Terkonit-Menge f�r die Produktion");
		add(alleFelder_groundtroops,"ynkelonium", FIELD_INTEGER, "Ynkelonium-Menge f�r die Produktion");
		add(alleFelder_groundtroops,"howalgonium", FIELD_INTEGER, "Howalgonium-Menge f�r die Produktion");
		add(alleFelder_groundtroops,"cvEmbinium", FIELD_INTEGER, "CV-Embinium-Menge f�r die Produktion");
		add(alleFelder_groundtroops,"size", FIELD_INTEGER , "Gr��e dieser Truppe");
		add(alleFelder_groundtroops,"crew", FIELD_INTEGER , "Produktionszeit in der Kaserne");
		add(alleFelder_groundtroops,"modulPoints", FIELD_INTEGER , "Anzahl der Punkte um diese Truppe zu Produzieren");
        add(alleFelder_groundtroops,"shields", FIELD_INTEGER , "Anzahl der Schildpunkte (werden jede runde aufgefrischt)");
	}

	private static void add(ArrayList<IDatabaseColumn> felder, String name, int flags, String comment, Object ... args) throws SQLException 
	{
		if ((flags & FIELD_BOOLEAN_INT) != 0)
			felder.add(new BooleanField(felder.size()+1, name, (flags & FIELD_READONLY) != 0, comment));
		else if ((flags & FIELD_STRING) != 0)
			felder.add(new StringField(felder.size()+1, name, (flags & FIELD_READONLY) != 0, (flags & FIELD_MULTILINE) != 0, (flags & FIELD_STRING_NULL) != 0, comment));
		else if ((flags & FIELD_INTEGER) != 0)
			felder.add(new IntegerField(felder.size()+1, name, (flags & FIELD_READONLY) != 0, comment));
		else if ((flags & FIELD_LONGINTEGER) != 0)
			felder.add(new LongField(felder.size()+1, name, (flags & FIELD_READONLY) != 0, comment));
		else if ((flags & FIELD_STRING_IMG_FILENAME) != 0)
			felder.add(new ImgField(felder.size()+1, name, (flags & FIELD_READONLY) != 0, comment, ""+args[0]));
		else if ((flags & FIELD_SELECT) != 0)
		{
			SelectField sf = new SelectField (felder.size()+1, name, (flags & FIELD_READONLY) != 0, comment);
			felder.add(sf);
			if ((args.length == 1) && (args[0] instanceof ResultSet))
			{
				sf.addValue(0, "Keins");
				ResultSet rs = (ResultSet)args[0];
				while (rs.next())
					sf.addValue(rs.getInt(1), rs.getString(2));
			}
			else {
				for (int i = 0; i+1< args.length; i+= 2)
				{
					sf.addValue((Integer)args[i], ""+args[i+1]);
				}
			}
		}
	}
}
