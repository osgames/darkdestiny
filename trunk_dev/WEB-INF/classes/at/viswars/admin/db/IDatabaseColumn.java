package at.viswars.admin.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import at.viswars.DebugBuffer;
import at.viswars.DebugBuffer.DebugLevel;

/**
 * @author martin
 *
 */
public abstract class IDatabaseColumn {

	protected String name;
	protected String comment;
	protected boolean readonly;
	
	
	public IDatabaseColumn(String name2, String comment2, boolean readOnly) {
		name = name2;
		comment = comment2;
		readonly = readOnly;
	}

	/**
	 * @return den Namen der Spalte
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param rs
	 * @return den Eintrag in der Richtigen Spalte, als passendes Object
	 */
	public abstract Object getFromResultSet(ResultSet rs) throws SQLException;

	/**
	 * Ein passendes Edit-Feld erzeugen, für den Datenbankeintrag
	 * @param object der Startwert
	 * @return das Edit-Feld als HTML-Code
	 */
	public abstract String makeEdit(Object object);

	/**
	 * @return einen Kommentar zu dieser Spalte
	 */
	public String getComment()
	{
		return comment;
	}

	public void update(Statement state, String table, String parameter, int id) throws SQLException 
	{
		if (readonly)
			return;
		if ("".equals(parameter))
			parameter =getValueOnEmpty();
		else parameter = "'"+parameter+"'";
		DebugBuffer.addLine(DebugLevel.DEBUG,"UPDATE "+table+" SET "+getName()+"="+parameter+" WHERE id="+id);
		state.execute("UPDATE "+table+" SET "+getName()+"="+parameter+" WHERE id="+id);
	}

	protected String getValueOnEmpty()
	{
		return "''";
	}

}
