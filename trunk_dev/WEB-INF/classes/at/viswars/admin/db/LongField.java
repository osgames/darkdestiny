/**
 * 
 */
package at.viswars.admin.db;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author martin
 *
 */
public class LongField extends IDatabaseColumn {

	private final int column;

	/**
	 * @param comment 
	 * @param readonly 
	 * @param name 
	 * @param column 
	 * 
	 */
	public LongField(int column, String name, boolean readonly, String comment) {
		super(name,comment, readonly);
		this.column = column;
	}

	public Object getFromResultSet(ResultSet rs) throws SQLException {
		return rs.getLong(column);
	}

	public String makeEdit(Object object) {
		if (object == null)
			object = 0L;

		return "<input type=\"text\" name=\"f_"+name+"\" size=\"80\" value=\""+
			((Long)object).longValue()+"\" "+
			(readonly?"readonly ":"")+"/>";
	}

	protected String getValueOnEmpty()
	{
		return "'0'";
	}
}
