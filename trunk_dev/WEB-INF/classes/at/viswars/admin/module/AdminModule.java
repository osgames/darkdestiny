/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.admin.module;

import at.viswars.DebugBuffer;
import at.viswars.database.access.DbConnect;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class AdminModule {
    private int id;
    private String name;
    private ArrayList<ModuleAttributeRel> attributes;
    private boolean attributesInit = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ModuleAttributeRel> getAttributes() {               
        
        if (!attributesInit) {
            try {
                Statement stmt = DbConnect.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT ma.id, ma.chassisId, a.id, ma.researchId, a.name, ma.attributeType, ma.value, ma.refId, ma.refType, ma.appliesTo " + 
                        "FROM moduleattributes ma, attribute a WHERE ma.moduleId="+id+" AND a.id=ma.attributeId ORDER BY ma.chassisId ASC, a.name ASC");
                while (rs.next()) {
                    if (attributes == null) {
                        attributes = new ArrayList<ModuleAttributeRel>();
                    }
                    
                    ModuleAttributeRel mar = new ModuleAttributeRel();
                    
                    mar.setId(rs.getInt(1));
                    mar.setChassisId(rs.getInt(2));
                    mar.setAttributeId(rs.getInt(3));
                    mar.setResearchId(rs.getInt(4));
                    mar.setAttributeName(rs.getString(5));
                    mar.setAttributeType(AttributeType.valueOf(rs.getString(6)));
                    mar.setValue(rs.getDouble(7));
                    mar.setRefId(rs.getInt(8));
                    
                    if (rs.getString(9) != null) {
                        mar.setRefType(ReferenceType.valueOf(rs.getString(9)));  
                    }                    
                    
                    mar.setAppliesTo(rs.getInt(10));
                    
                    attributes.add(mar);                    
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in retrieve relations:<BR>", e);
            }
            
            attributesInit = true;
        }
        
        return attributes;
    }

    public void setAttributes(ArrayList<ModuleAttributeRel> attributes) {
        this.attributes = attributes;
    }
}
