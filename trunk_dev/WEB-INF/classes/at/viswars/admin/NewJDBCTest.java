/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.admin;

import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlayerFleetDAO;
import at.viswars.model.PlayerFleet;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class NewJDBCTest {
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);

    public static void jdbcTest() {
        try {
            PlayerFleet pf = new PlayerFleet();
            pf.setName("Test1");
            pf.setPlanetId(1);
            pf.setStatus(0);
            pf.setSystemId(1);

            pf = pfDAO.add(pf);
            Logger.getLogger().write("PF ID is " + pf.getId());

            PlayerFleet pf2 = new PlayerFleet();
            pf2.setName("Test2a");
            pf2.setPlanetId(1);
            pf2.setStatus(0);
            pf2.setSystemId(1);

            PlayerFleet pf3 = new PlayerFleet();
            pf3.setName("Test2b");
            pf3.setPlanetId(1);
            pf3.setStatus(0);
            pf3.setSystemId(1);

            ArrayList<PlayerFleet> pfList = new ArrayList<PlayerFleet>();
            pfList.add(pf2);
            pfList.add(pf3);
            pfList = pfDAO.insertAll(pfList);

            for (PlayerFleet pfTmp : pfList) {
                Logger.getLogger().write("[2] Generated Id: " + pfTmp.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
