/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.viswars.admin;

import at.viswars.Logger.Logger;
import at.viswars.dao.DAOFactory;
import at.viswars.dao.PlanetDAO;
import at.viswars.dao.PlayerPlanetDAO;
import at.viswars.model.Planet;
import at.viswars.model.PlayerPlanet;
import at.viswars.planetcalc.ExtPlanetCalcResult;
import at.viswars.planetcalc.PlanetCalculation;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class RestorePopulation {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    
    public static synchronized void restorePop() {
       ArrayList<PlayerPlanet> ppList = ppDAO.findAll();
       for (PlayerPlanet pp : ppList) {
           Planet p = pDAO.findById(pp.getPlanetId());
           if (!(p.getLandType().equalsIgnoreCase("M") ||
                p.getLandType().equalsIgnoreCase("C") ||
                p.getLandType().equalsIgnoreCase("G"))) {
                try {
                    PlanetCalculation pc = new PlanetCalculation(p.getId());
                    ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
                    long maxPop = epcr.getMaxPopulation();
                                        
                    Logger.getLogger().write("#" + p.getId() + " ["+pp.getName()+"]> Fixing planet type " + p.getLandType() + " -> MaxPop Calculated: " + maxPop);                   
                    
                    pp.setPopulation(maxPop);
                    pp.setMoral(100);
                    ppDAO.update(pp);
                } catch (Exception e) {
                   
                }
           }
       }
    }
}
