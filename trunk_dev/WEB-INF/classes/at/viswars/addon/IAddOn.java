/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.viswars.addon;

import at.viswars.update.UpdaterDataSet;

/**
 *
 * @author Admin
 */
public interface IAddOn {
    
  void install();
  AddOnProperties getProperties();
  void print();
  void unInstall();
  void onUpdate(UpdaterDataSet uds);
}
