/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import at.viswars.GameUtilities;
import at.viswars.service.Service;
import at.viswars.model.Planet;
import at.viswars.model.System;
import at.viswars.model.PlanetRessource;
import at.viswars.model.User;
import at.viswars.utilities.MD5;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class TestUniverse extends Service {

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
    /**
     *
     * @author Bullet
     */
    User user;
    private ArrayList<System> systems;
    private ArrayList<Planet> planets;
    private ArrayList<PlanetRessource> planetRessources;

    public TestUniverse() {
    }

    public void initializeData() {
        user = new User();
        systems = new ArrayList<System>();
        planets = new ArrayList<Planet>();
        int multiplier = 0;
        for (int i = 100000; i < 100002; i++) {
            System s = new System();

            s.setId(i);
            s.setX(i + multiplier * 100);
            s.setY(i + multiplier * 100);
            s.setName("sys : " + i);
            s.setVisibility(0);
            systems.add(s);
            boolean Msetted = false;
            for (int j = 100000; j < 100010; j++) {
                Planet p = new Planet();
                p.setId(j + multiplier * 10);
                if (!Msetted) {
                    p.setLandType(Planet.LANDTYPE_M);
                    Msetted = true;
                } else {
                    p.setLandType(Planet.LANDTYPE_G);
                }
                p.setAvgTemp(17);
                p.setAtmosphereType("Z");
                p.setDiameter(40000);
                p.setGrowthBonus(0f);
                p.setOrbitLevel(j - 99999);
                p.setProductionBonus(0f);
                p.setResearchBonus(0f);
                p.setSystemId(i);
                planets.add(p);
            }
            multiplier++;

        }
    }

    public void create() {
        for (System s : systems) {
            systemDAO.add(s);
        }
        for (Planet p : planets) {
            planetDAO.add(p);
        }
    }

    public void destroy() {
        for (System s : systems) {
            systemDAO.remove(s);
        }
        for (Planet p : planets) {
            planetDAO.remove(p);
        }
    }

    public void createUser() {
        String passwort1 = "test";
        String gameName = "TestUser";
        String email = "emehl@mehl.at";

        GameUtilities gu = new GameUtilities();
        user.setUserId(100000);
        user.setPassword(MD5.encryptPassword(passwort1));
        user.setUserName("Test");
        user.setGameName(gameName);
        user.setLastUpdate(gu.getCurrentTick());
        user.setIp("127.0.0.1");
        user.setEmail(email);
        user.setSystemAssigned(false);
        user.setActive(true);
        user.setAdmin(false);
        user.setTrial(true);
        user.setBetaAdmin(false);
        user.setLocked(0);
        user.setActivationCode("");
        user.setDeleteDate(0);
        user.setMulti(false);
        user.setJoinDate(java.lang.System.currentTimeMillis());
        userDAO.add(user);
    }

    public void destroyUser() {

        userDAO.remove(user);
    }
}

