package test;

/**
 * Allow multiple tests in this class
 * 
 * @author martin
 */
public interface IMultiTest {

	/**
	 * Set the params for the current test
	 */
	void setParams(Object parameter);
	/*
	 * List all Params available for single tests 
	 */
	//public static Object [] getParams();
	
}
