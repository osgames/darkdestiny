/**
 * 
 */
package test;

/**
 * @author martin
 *
 */
public interface ITest {

	/**
	 * Alles vorbereiten
	 */
	void prepare() throws Throwable;

	/**
	 * Test durchf&uuml;hren, Exception schmei&szlig;en, 
	 * wenn etwas nicht klappt
	 * @throws Throwable
	 */
	void run() throws Throwable;

	/**
	 * Aufr&auml;umen, muss Fehlerresistent sein, 
	 * da es im finally block zu prepare und run ausgef&uuml;hrt wird
	 */
	void tearDown();

}
