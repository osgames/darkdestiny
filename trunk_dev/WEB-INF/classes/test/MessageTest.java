/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import at.viswars.dao.DAOFactory;
import at.viswars.dao.UserDAO;
import at.viswars.enumeration.EMessageType;
import at.viswars.model.Message;

/**
 *
 * @author Bullet
 */
public class MessageTest extends Message{
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    
    public MessageTest(){
        this.setArchiveBySource(false);
        this.setArchiveByTarget(false);
        this.setDelBySource(false);
        this.setDelByTarget(false);
        this.setTopic("testtopic");
        this.setType(EMessageType.SYSTEM);
        this.setOutMaster(true);
        this.setSourceUserId(uDAO.getSystemUser().getUserId());
        this.setTargetUserId(1);
        this.setText("testtext");
        this.setTimeSent(System.currentTimeMillis());
        this.setViewed(false);

    }

}
