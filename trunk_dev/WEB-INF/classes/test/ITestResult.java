/**
 * 
 */
package test;

/**
 * @author martin
 *
 */
public interface ITestResult {
	
	
	public final static ITestResult TEST_OK = new ITestResult() { 
		// This is a default implementation for successful tests
	};
	
	public static final class TEST_IGNORED 
			implements ITestResult {
		
		Throwable error;
		public TEST_IGNORED (Throwable error_) {
			error = error_;
		}
		
		public Throwable getError() {
			return error;
		}
	}

	public static final class TEST_FAILED
			implements ITestResult {

		Throwable error;
		public TEST_FAILED (Throwable error_) {
			error = error_;
		}
		
		public Throwable getError() {
			return error;
		}
	}
}
