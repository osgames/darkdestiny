/**
 * 
 */
package test;

/**
 * Dieser Test wird ignoriert 
 * Da er noch nicht fertig ist
 * 
 * @author martin
 */
public class IgnoreThisTest extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3801727185833375368L;


	/**
	 * @param message
	 */
	public IgnoreThisTest(String message) {
		super(message);
	}
}
