/**
 * 
 */
package test;


/**
 * Run all tests, and return a array of errors ;-)
 * 
 * @author martin
 */
public class TestRunner {

	public static ITestResult[] run(String className) {
		try {
			Class<ITest> test = (Class<ITest>) TestRunner.class.getClassLoader().loadClass(className);
			try {
				test.asSubclass(IMultiTest.class);
			} catch (ClassCastException e) {
				return new ITestResult[] { run(test, null) };
			}
			
			Object[] params = (Object[]) test.getMethod("getParams").invoke(null);
			ITestResult[] results = new ITestResult[params.length];
			
			int i = 0;
			for (Object p : params) {
				try {
					results[i++] = run(test, p) ;
				} catch (Throwable e) {
					results[i++] = new ITestResult.TEST_FAILED(e);
				}
			}
			return results;
				
		} catch (Throwable e) {
			return new ITestResult[] { new ITestResult.TEST_FAILED(e) };
		}
	}
	
	
	/**
	 * 
	 */
	public static ITestResult run(Class<ITest> testClass, Object parameter) {
		
		try {
			ITest test = testClass.newInstance();
			
			if (test instanceof IMultiTest) {
				((IMultiTest)test).setParams(parameter);
			}

			try {
				test.prepare();

				//TODO Hier fehlt ein Annotations-Support, so dass die Klassen wirklich gut getestet werden ...
				test.run();

			} finally {
				test.tearDown();
			}

			return ITestResult.TEST_OK;
		} catch (Throwable t)
		{
			if (t instanceof IgnoreThisTest)
				return new ITestResult.TEST_IGNORED(t);
			return new ITestResult.TEST_FAILED(t);
		}
	}

	public static StringBuilder createStackTrace(Throwable err, String indent, String nextIndent)
	{
		StringBuilder res = new StringBuilder();
		res.append(indent+err.getMessage()+" ("+err.getClass().getName()+")");
		for (StackTraceElement ste : err.getStackTrace())
		{
			res.append(indent+nextIndent + ste.toString());
		}
		
		if (err.getCause() != null)
		{
			res.append(indent+"Caused by: ");
			res.append(createStackTrace(err.getCause(), indent+nextIndent, nextIndent).toString());
		}
		
		return res;
	}

}
