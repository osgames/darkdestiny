package test;

/**
 * An assertion failed
 * (this is an Exception, not an error!)
 * 
 * @author martin
 */
public class AssertionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6137371749645276618L;

	/**
	 * 
	 */
	public AssertionException() {
		super();
	}

	/**
	 * @param message
	 */
	public AssertionException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public AssertionException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public AssertionException(String message, Throwable cause) {
		super(message, cause);
	}

}
