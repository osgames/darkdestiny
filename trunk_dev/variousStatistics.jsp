<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.statistics.*" %>
<%@page import="at.viswars.service.Service" %>
<%@page import="at.viswars.service.StatisticService" %>
<%@page import="at.viswars.ML" %>
<%@page import="at.viswars.model.StatisticCategory" %>
<%@page import="at.viswars.model.StatisticEntry" %>
<%@page import="org.jfree.chart.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%

            int type = 0;
            String homepage = "http://thedarkdestiny.at/homepage/subSites/statistics/";
            int categoryId = 0;
            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));

            }

            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            if (request.getParameter("categoryId") != null) {
                categoryId = Integer.parseInt(request.getParameter("categoryId"));

            }
            int statisticEntryId = 0;

            if (request.getParameter("statisticEntryId") != null) {
                statisticEntryId = Integer.parseInt(request.getParameter("statisticEntryId"));
            }
            String myImageMap = "";
            try {
                JFreeChart chart = StatisticChartCreater.createChart(statisticEntryId, userId);
                if (session.getAttribute("chart") != null) {
                    session.removeAttribute("chart");
                }
                session.setAttribute("chart", chart);
                // get ImageMap
                ChartRenderingInfo info = new ChartRenderingInfo();
                // populate the info
                chart.createBufferedImage(500, 500, info);
                myImageMap = ChartUtilities.getImageMap("map", info);
            } catch (Exception e) {
            }

%>
<%= myImageMap%>
<%

%>

<TABLE width="80%">
    <TR align="center">
        <TD width="20%"><U><A href="main.jsp?page=new/statistic&type=1"><%= ML.getMLStr("statistic_link_empires", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=new/statistic&type=2"><%= ML.getMLStr("statistic_link_alliances", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=imperialoverview"><%= ML.getMLStr("statistik_link_internalstatistic", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=variousStatistics"><%= ML.getMLStr("statistic_link_variousstatistics", userId)%></A></U></TD>
    </TR>
</TABLE>
<TABLE  align="center">
    <TR>
        <TD valign="center" align="Center">
            <FORM method="post" name="statCategoryForm" action="">

                <SELECT style='background-color: #000000; color: #ffffff; width:170px; height:17px; border:0px; text-align: center;' name="statCategory" onChange="document.statCategoryForm.action=document.statCategoryForm.statCategory.options[document.statCategoryForm.statCategory.selectedIndex].value; document.statCategoryForm.submit();">

                    <% if (categoryId == 0) { %>
                    <OPTION selected value='main.jsp?page=variousStatistics'>- <%= ML.getMLStr("chart_lbl_noselection", userId) %> -</OPTION>
                    <%}
                                for (StatisticCategory sc : (ArrayList<StatisticCategory>) Service.statisticCategoryDAO.findAll()) {
                    %>

                    <%
                                                        if (categoryId == sc.getId()) {
                    %>

                    <OPTION selected value='main.jsp?page=variousStatistics&categoryId=<%= sc.getId()%>'><%= ML.getMLStr(sc.getName(), userId) %></OPTION>

                    <%
                                                                                } else {
                    %>

                    <OPTION value='main.jsp?page=variousStatistics&categoryId=<%= sc.getId()%>'><%= ML.getMLStr(sc.getName(), userId) %></OPTION>

                    <%
                                    }

                                }
                    %>
                </SELECT>

            </FORM>
        </TD>
    </TR>

    <%
                if (categoryId > 0) {
    %>

    <TR>
        <TD>
            <FORM method="post" name="subStatCategoryForm" action="">

                <SELECT style='background-color: #000000; color: #ffffff; width:170px; height:17px; border:0px; text-align: center;' name="subStatCategory" onChange="document.subStatCategoryForm.action = document.subStatCategoryForm.subStatCategory.options[document.subStatCategoryForm.subStatCategory.selectedIndex].value; document.subStatCategoryForm.submit();">

                    <%

                    %>
                    <OPTION value='main.jsp?page=variousStatistics'>- <%= ML.getMLStr("chart_lbl_noselection", userId) %> -</OPTION>
                    <%

                                        for (StatisticEntry se : StatisticService.getStaticEntriesByCategoryId(categoryId)) {


                                            if (statisticEntryId == se.getId()) {
                    %>
                    <OPTION selected value='main.jsp?page=variousStatistics&categoryId=<%= categoryId%>&statisticEntryId=<%= se.getId()%>'><%= ML.getMLStr(se.getName(), userId) %></OPTION>

                    <%
                                                                    } else {
                    %>
                    <OPTION value='main.jsp?page=variousStatistics&categoryId=<%= categoryId%>&statisticEntryId=<%= se.getId()%>'><%= ML.getMLStr(se.getName(), userId) %></OPTION>

                    <%
                                            }
                                        }

                    %>
                </SELECT>
            </FORM>
        </TD>
    </TR>
    <%  } %>
</TABLE>
<%             
    if (statisticEntryId > 0) {
%>
<TABLE>
    <TR>
        <TD>
            <IMG src="StatChartViewer" usemap="#map">
        </TD>
    </TR>
</TABLE>
<%            }


%>