<%@page import="at.viswars.*" %>
<%@page import="at.viswars.construction.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.model.Ressource" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.ships.*" %>
<%@page import="at.viswars.databuffer.RessAmountEntry" %>
<%
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));

int fleetId = 0;

if (request.getParameter("fleetId") != null) {
    fleetId = Integer.parseInt((String)request.getParameter("fleetId"));  
}
else throw new Exception("Die FleetId muss gesetzt sein");
FleetService.checkValidUser(""+fleetId, false, userId, out);

FleetData fd = new FleetData(fleetId);
planetId = fd.getPlanetId();

boolean redesign = false;
boolean scrap = false;
int designId = 0;
//int queueIdent = 0;

ShipData sd = null;

if (request.getParameter("redesign") != null) {
    designId = Integer.parseInt((String)request.getParameter("redesign"));
    sd = ShipUtilities.getShipData(fleetId,designId);  
    ReconstructionFunctions rf = new ReconstructionFunctions();
    //queueIdent = rf.getQueueIdent(userId,fleetId,designId,planetId);
    // DebugBuffer.addLine(DebugBuffer.DebugLevel.DEBUG,"QueueIdent is " + queueIdent);
    redesign = true;
} else if (request.getParameter("scrap") != null) {
    designId = Integer.parseInt((String)request.getParameter("scrap"));
    sd = ShipUtilities.getShipData(fleetId,designId);  
    //queueIdent = ShipUtilities.getQueueIdent(userId,planetId,fleetId,designId,sd.getDesignName());
    scrap = true;
}

List<ShipData> shipList = ShipUtilities.getShipList(fleetId);

if (fd.getFleetId() != 0) {
%>
<BR><FONT size=+1><B>Flotte "<%= fd.getName() %>" bearbeiten</B></FONT><BR><BR>
<%
// Anzahl und das Desing eingeben zu dem das aktuell gew&auml;hlte Schiff ge&auml;ndert werden soll
if (redesign) {
    // ShipData sd = ShipUtilities.getShipData(fleetId,designId);   
    
    Statement stmt = DbConnect.createStatement();
%>
    <FORM method='post' action='main.jsp?page=confirmAction&qi=' name='confirm' >
    <TABLE width="80%" style="font-size:13px">
    	<TR>
    		<TD colspan=2 align="center">Bitte das Design w&auml;hlen in das das aktuelle Schiff ge&auml;ndert werden soll und die Anzahl der zu &auml;ndernden Schiff eingeben.
    			<BR>
    			<BR>
    		</TD>
    	</TR>
    	<TR valign="middle">
	    	<TD width="200">Schiffsname:</TD>
    		<TD><%= sd.getDesignName() %></TD>
    	</TR>
    	<TR valign="middle">
	    	<TD width="200">Typ:</TD>
    		<TD><%= ShipUtilities.getChassisName(designId) %></TD>
    	</TR>
    	<TR valign="middle">
    		<TD>Planet:</TD>
    		<TD><%= planetId %></TD>
    	</TR>
    	<TR valign="middle">
    		<TD>Anzahl:</TD>
    		<TD><INPUT name="upgradeCount" 
    					id="ugradeCount" 
    					value="1" 
    					SIZE="3" 
    					MAXLENGTH="6" 
    					ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();" /> / <%= sd.getCount() %>
    					<BR>
    		</TD>
    	</TR>
    	<TR valign="top">
    		<TD>Upgraden zu Design:</TD>
    		<TD>
    			<SELECT id="toDesign" name="toDesign">
        			<OPTION value=0>-- Design w&auml;hlen --</OPTION><%
        			ResultSet rs = stmt.executeQuery("SELECT id, name FROM shipdesigns WHERE chassis='"+sd.getChassisSize()+"' AND id<>'"+sd.getDesignId()+"' AND userId='"+userId+"'");
        			while (rs.next()) { 
						%><OPTION value="<%= rs.getInt(1) %>"><%= rs.getString(2) %></OPTION><%
        			}
				%></SELECT>
    		</TD>
    	</TR>
    	<TR>
    		<TD colspan=2 align="CENTER">
    			<BR><INPUT type="submit" name="Ok" value="Ok" ONCLICK ='if (document.getElementById("toDesign").value == 0) { alert("Neues Design muss angegeben werden!"); return false; }'>
    		</TD>
    	</TR>
    </TABLE><BR><BR>                   
    </FORM>         
<%
}

// Anzahl eingeben und dann confirmQueue bef&uuml;llen und confirmDialog anzeigen
if (scrap) {
	ShipRestructuringRessourceCost shipCost = ShipUtilities.getShipScrapCost(designId);
%>
    <script language="Javascript">
<%
        for (RessAmountEntry rae : shipCost.getRessArray()) {
            out.write("var "+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()
                    +"Per = " + rae.getQty() + ";");
        }
%>        
        var maxCount = <%= sd.getCount() %>;

        var actCount = 1;

<%
        out.write("var totalCredit = creditPer;");
        for (RessAmountEntry rae : shipCost.getRessArray()) {
            out.write("var total" + Service.ressourceDAO.findRessourceById(rae.getRessId()).getName() + " = " + Service.ressourceDAO.findRessourceById(rae.getRessId()).getName() + "Per;");
        }
%>        
        function calcTotal() {
            totalCredit = actCount * creditPer;
<%
        for (RessAmountEntry rae : shipCost.getRessArray()) {
                out.write("total" + Service.ressourceDAO.findRessourceById(rae.getRessId()).getName() + " = actCount * " + Service.ressourceDAO.findRessourceById(rae.getRessId()).getName() + "Per;");
            }            
%>            

            document.getElementById("credit").innerHTML = number_format(totalCredit,0) + ' Credits';
<%
        for (RessAmountEntry rae : shipCost.getRessArray()) {
                if (rae.getQty() != 0) {
                    out.write("document.getElementById(\""+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()+"\").innerHTML = number_format(total"+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()+",0);");
                }              
            }        
%>
        }
    </script>
    <FORM method='post' action='main.jsp?page=confirmAction&qi=' name='confirm' >
    <TABLE width="80%" style="font-size:13px">
    	<TR>
    		<TD colspan=2 align="center">
    			Bitte Anzahl der zu verschrottenden Schiffe eingeben<BR><BR>
    		</TD>
    	</TR>
    	<TR valign="middle">
    		<TD width="200">Schiffsname:</TD>
    		<TD><%= sd.getDesignName() %></TD>
    	</TR>
    	<TR valign="middle">
	    	<TD width="200">Typ:</TD>
	    	<TD><%= ShipUtilities.getChassisName(designId) %></TD>
	    </TR>
    	<TR valign="middle">
    		<TD>Planet:</TD>
    		<TD><%= planetId %></TD>
    	</TR>
    	<TR valign="middle">
    		<TD>Anzahl:</TD>
    		<TD><INPUT  name="destroyCount"
    					Id="destroyCount" 
    					value="1" 
    					SIZE="3" 
    					MAXLENGTH="6" 
    					ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();"> / <%= sd.getCount() %>
    			<BR>
    		</TD>
    	</TR>
    	<TR valign="top">
    		<TD>Kosten:</TD>
                <TD><DIV id="credit"> <%= FormatUtilities.getFormattedNumber(shipCost.getRess(Ressource.CREDITS)) %></DIV>
    			<BR>
    		</TD>
    	</TR>
        <TR>
            <TD align="center" colspan=2 class="blue2"><B>Ressourcenr&uuml;ckverg&uuml;tung</B></TD>
        </TR>
        <TR>
            <TD class="blue2"><B>Ressource</B></TD>
            <TD class="blue2"><B>Menge</B></TD>                
        </TR>
<%
        for (RessAmountEntry rae : shipCost.getRessArray()) {
            if (rae.getQty() != 0) {
                if(rae.getRessId() == Ressource.CREDITS){
                        continue;
                    }
                out.write("<TR><TD>"+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()+"</TD><TD><DIV id=\""+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()+"\">" + FormatUtilities.getFormattedNumber(rae.getQty()) + "</DIV></TD></TR>");
            }
        }      
%>
    <TR><TD colspan=2 align="CENTER"><BR><INPUT type="submit" name="Ok" value="Ok"></TD></TR>
    </TABLE><BR><BR>                   
    </FORM>      
<%
}
%>
<TABLE width="80%" class="blue">
<TR> 
    <TD class="blue2"><B>Name</B></TD><TD class="blue2"><B>Anzahl</B></TD><TD class="blue2"><B>Aktion</B></TD>
</TR>
<%
    for (int i=0;i<shipList.size();i++) {
        sd = (ShipData)shipList.get(i);        
%>
        <TR> 
            <TD><%= sd.getDesignName() %></TD><TD><%= sd.getCount() %></TD>
            <TD>
                <TABLE cellspacing=0 cellpadding=0>
<%
                    if (true) {
                   // if (ShipUtilities.checkIfDesignCanBeModified(sd.getDesignId(),planetId)) {
%>
                    <TR>                    
                        <TD><IMG src="<%=GameConfig.picPath()%>pic/modify.jpg" onmouseover="doTooltip(event,'Schiffe umr&uuml;sten')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=military&type=7&fleetId=<%= fleetId %>&redesign=<%= sd.getDesignId() %>';"></IMG></TD>
                        <TD><IMG src="<%=GameConfig.picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'Schiffe abr&uuml;sten')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=military&type=7&fleetId=<%= fleetId %>&&scrap=<%= sd.getDesignId() %>';"></IMG></TD>
                    </TR>
<%
                    } else {
%>
                    <TR>                    
                        <TD>-</TD>
                    </TR>
<%
                    } 
%>
                </TABLE>
            </TD>
            <TD></TD>
        </TR>
<%
    }
%>       
</TABLE>
<%
} else {
%>
<CENTER><BR>Flotte wurde aufgel&ouml;st, keine weiteren Aktionen m&ouml;glich!</CENTER>
<%
}   
%>    