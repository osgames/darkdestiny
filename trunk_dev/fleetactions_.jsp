<%@page import="at.viswars.*" %>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.databuffer.*" %>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%
    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
    int systemId = Integer.parseInt((String) session.getAttribute("actSystem"));

    int sourcePage = 0;

    if (session.getAttribute("source") != null) {
        sourcePage = Integer.parseInt((String) session.getAttribute("source"));
    } else {
        %><jsp:forward page="main.jsp" >
        <jsp:param name="msg" value='Ung&uuml;ltiger Aufruf' />
        <jsp:param name="page" value='showerror' />
        </jsp:forward><%
        return;
    }

    int targetType = 0;
    int targetId = 0;
    String destSystem = "x";
    String destPlanet = "x";
    List<FleetData> fleetList = null;

    switch (sourcePage) {
    case (FleetMovement.MOVE_FROM_SYSVIEW):
        fleetList = ShipUtilities.getFleetList(0, userId, 7);

        // Build a java script array with retreat info of all fleets
        out.println("<script language=\"Javascript\">");
        out.print("var fleetIds = new Array("+fleetList.size()+");");
        out.print("var retreatFactor = new Array("+fleetList.size()+");");
        out.print("var retreatToType = new Array("+fleetList.size()+");");
        out.print("var retreatTo = new Array("+fleetList.size()+");");        
        
        int index = 0;
        
        for (FleetData fd : fleetList) {
            int retreatFactor = 100;
            int retreatToType = 0;
            int retreatTo = 0;
            
            if (fd.getFo() != null) {
                retreatFactor = fd.getFo().getRetreatFactor();
                retreatToType = fd.getFo().getRetreatToType();
                retreatTo = fd.getFo().getRetreatTo();
            } 
            
            out.print("fleetIds["+index+"]="+fd.getFleetId()+";");
            out.print("retreatFactor["+index+"]="+retreatFactor+";");
            out.print("retreatToType["+index+"]="+retreatToType+";");
            out.print("retreatTo["+index+"]="+retreatTo+";");
                    
            index++;
        }
%>
        function updateFields() {
            var actFleetId = document.getElementById("fleetid").value;
            var index = -1;
            
            for (i=0;i<fleetIds.length;i++) {
                if (fleetIds[i] == actFleetId) {
                    index = i;
                }
            }
            
            if (index > -1) {
                document.getElementById("retreat").value = retreatFactor[index];
                document.getElementById("retreatToType").value = retreatToType[index];
                document.getElementById("retreatTo").value = retreatTo[index];
            }
        }
<%
        out.println("</script>");
        
        if (((String) request.getParameter("toSystem")).equalsIgnoreCase("true")) {
            targetType = 1;
        } else {
            targetType = 2;
        }
        targetId = Integer.parseInt((String) request.getParameter("targetId"));

        Statement stmt = DbConnect.createStatement();
        if (targetType == 2) {
            ResultSet rs = stmt.executeQuery("SELECT systemId FROM planet WHERE id="+ targetId);
            if (rs.next()) {
                destSystem = Integer.toString(rs.getInt(1));
                destPlanet = Integer.toString(targetId);
            }
        } else if (targetType == 1) {
            destSystem = Integer.toString(targetId);
        }
        stmt.close();
        break;
    }

    int fleetId = 0;
    // 0 = military
    // 1 = planetControl
    // 2 = spionage
    int source = 0;

    if (request.getParameter("fleetId") != null) {
        source = 1;
        fleetId = Integer.parseInt(request.getParameter("fleetId"));
    }

    if (request.getParameter("sourcepage") != null) {
        Map<String, Object> pmap = request.getParameterMap();
        FleetMovement fm = new FleetMovement(pmap, userId, 1);
        fm.checkParameter();
        if (fm.isError()) {
            %><jsp:forward page="main.jsp" >
            <jsp:param name="msg" value='<%=fm.getErrorMess()%>' />
            <jsp:param name="page" value='showerror' />
            </jsp:forward><%
        } else {
            %><jsp:forward page="main.jsp" >
                <jsp:param name="qi" value='<%= ConfirmBuffer.addNewConfirmEntry(pmap,userId,GameConstants.CONFIRM_FLEET_MOVEMENT) %>' />
                <jsp:param name="page" value='confirmAction' />
            </jsp:forward><%
         }
    }

    switch (sourcePage) {
         case (FleetMovement.MOVE_FROM_FLEETVIEW):
             FleetData fd = new FleetData(fleetId);
            %><FONT style="font-size:13px"><B>Ziel und Auftrag f&uuml;r Flotte <%=fd.getName()%></B></FONT><BR><%
             break;
         case (FleetMovement.MOVE_FROM_SYSVIEW):
             %><FONT style="font-size:13px"><B>Flotte f&uuml;r Ziel (<%=destSystem%>:<%=destPlanet%>) w&auml;hlen</B></FONT><BR><%
             break;
         }

    // Abfrage Parameter f&uuml;r Seitenanzeige
    int showfleet = 0;
    int showaction = 0;

    if (request.getParameter("fleetId") != null
            && request.getParameter("action") != null) {
        
        
        try {
            showfleet = Integer.parseInt(request.getParameter("fleetId"));
            showaction = Integer.parseInt(request.getParameter("action"));
        } catch (NumberFormatException e) {
            DebugBuffer.writeStackTrace("fleetactions (Parameter error): ", e);
            return;
        }
    }

    try {
        %><BR>
        <FORM method='post' action='fleetactions.jsp<%
                boolean first = true;
                for (Object o : request.getParameterMap().entrySet()) {
                    Map.Entry<String, String[]> e = (Map.Entry<String, String[]>) o;
                    for (String value : e.getValue()) {
                        if (first) {
                            out.print("?");
                        } else {
                            out.print("&");
                        }

                        out.print(e.getKey() + "=" + value);
                        first = false;
                    }
                }
                %>' name='action'>
            <TABLE style="font-size:13px" width="60%">
                <TR><%
                    if (sourcePage == FleetMovement.MOVE_FROM_FLEETVIEW) {
                        %><TD><BR>Ziel (System/Planet)</TD><%
                    } else {
                        %><TD><BR>Flotte</TD><%
                    }
                    %><TD><%
                    if (sourcePage == FleetMovement.MOVE_FROM_FLEETVIEW) {
                        %><BR>
                            <select id="target" name="target">
                            <option value="0">-- Bitte w&auml;hlen --</option>
                            <option value="1">System</option>
                            <option value="2">Planet</option>
                        </select>
                        &nbsp;
                        <input size="10" name="ziel" value=""/><%
                    } else if (sourcePage == FleetMovement.MOVE_FROM_SYSVIEW) {
                        // Show fleetlist
                        %><BR>
                        <select id="fleetid" name="fleetid" onChange="updateFields();">
                            <option value="0" selected>-- Flotte w&auml;hlen --</option><%
                            for (int i = 0; i < fleetList.size(); i++) {
                                FleetData fd = (FleetData) fleetList.get(i);
                                out.write("<OPTION value=\"" + fd.getFleetId() + "\">"
                                    + fd.getName() + " (" + fd.getSystemId() + ":"
                                    + fd.getPlanetId() + ")</option>");
                            }
                        %></select><%
                    }
                    %></TD>
                </TR>
                <!--<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                <TR>
                    <TD>Aktionsart</TD>
                    <TD><%
                        switch(source) {
                        case 0:
                        case 1:
                            %><select disabled id="actiontype" name="actiontype">
                                <option value="1">Bewegen</option>
                                <option value="2">Angriff</option>
                                <option value="3">Bombardierung</option>
                                <option value="4">Invasion</option>
                                <option value="5">Belagerung</option>
                              </select><%
                            break;
                        case 2:
                            %><select id="actiontype" name="actiontype">
                                <option value="1">Kolonisieren</option>
                              </select><%
                            break;
                        case 3:
                            %><select id="actiontype" name="actiontype">
                                <option value="1">Spionieren</option>
                              </select><%
                            break;
                        }
                    %></TD>
                </TR> -->
                <TR>
                    <TD valign="bottom"><%
                        switch (source) {
                            case (0):
                            case (1):
                                %>R&uuml;ckzugsfaktor<%
                                break;
                            }
                    %></TD>
                    <TD><%
                        switch (sourcePage) {
                        case (FleetMovement.MOVE_FROM_FLEETVIEW): 
                            // Read retreat info from database
                            int retreatPercentage = 100;
                            int retreatToType = 0;
                            int retreatTo = 0;

                            FleetData fd = new FleetData(Integer.parseInt(request.getParameter("fleetId")));
                            FleetOrder fo = fd.getFo();

                            if (fo != null) {
                                retreatPercentage = fo.getRetreatFactor();
                                retreatToType = fo.getRetreatToType();
                                retreatTo = fo.getRetreatTo();
                            }                            
%>
                            <BR>Sobald dieser %-Satz der Flotte vernichtet wurde, zieht sie sich zum angegebenen Ort zur&uuml;ck<BR><BR>
                            <select id="retreat" name="retreat">
<%
                            for (int i=40;i<=100;i+=10) { 
                                String text = Integer.toString(i) + "%";
                                if (i == 100) text = "Kein R&uuml;ckzug";
                                if (i == retreatPercentage) { %>
                                    <option value="<%= i %>" selected><%= text %></option>
                             <% } else { %>
                                    <option value="<%= i %>"><%= text %></option>
                             <% }
                            }
%>                                
                            </select>&nbsp nach <select id="retreatToType" name="retreatToType">
                            <option value="0">-- Bitte w&auml;hlen --</option>
                            <% 
                            if (retreatToType == 1) { %>
                                <option value="1" selected>System</option>
                                <option value="2">Planet</option>                                
                         <% } else if (retreatToType == 2) { %>
                                <option value="1">System</option>
                                <option value="2" selected>Planet</option>                                 
                         <% } else { %>                            
                                <option value="1">System</option>
                                <option value="2">Planet</option>     
                         <% } %>
                        </select>
                        &nbsp;
                        <input size="10" id="retreatTo" name="retreatTo" value="<%= retreatTo %>"/><BR><BR>                                                          
                   <%       break;
                        case (FleetMovement.MOVE_FROM_SYSVIEW):
                            %><BR>
                            Sobald dieser %-Satz der Flotte vernichtet wurde, zieht sie sich zur&uuml;ck<BR>
                            <select id="retreat" name="retreat">
                                <option value="40">40%</option>
                                <option value="50">50%</option>
                                <option value="60">60%</option>
                                <option value="70">70%</option>
                                <option value="80">80%</option>
                                <option value="90">90%</option>
                                <option value="100">Kein R&uuml;ckzug</option>
                            </select><%
                        %>&nbsp nach <select id="retreatToType" name="retreatToType">
                            <option value="0">-- Bitte w&auml;hlen --</option>
                            <option value="1">System</option>
                            <option value="2">Planet</option>
                        </select>
                        &nbsp;
                        <input size="10" id="retreatTo" name="retreatTo" value=""/></TD>
<%
                        }
%>
                </TR>
                <TR>
                    <TD valign="bottom"><%
                        switch (source) {
                        case (0):
                        case (1):
                            %>Angriffsart<%
                            break;
                        }
                    %></TD>
                    <TD><%
                        switch (source) {
                        case (0):
                        case (1):
                            %><BR>
                            <select disabled id="attacktype" name="attacktype">
                                   <option value="10">Standard</option>
                                   <option value="10">Auch Neutrale</option>
                                   <option value="10">ALLE</option>
                               </select><%
                            break;
                        }
                    %></TD>
                </TR>
            </TABLE>
            <BR><%
            switch (sourcePage) {
            case (2):
                %><INPUT type='hidden' id="sourcepage" name="sourcepage" value="1" />
                <INPUT type='hidden' id="target" name="target" value="<%= targetType %>" />
                <INPUT type='hidden' id="ziel" name="ziel" value="<%= targetId %>" /><%
                break;
             case (1):
                 %><INPUT type='hidden' id="sourcepage" name="sourcepage" value="2" />
                <INPUT type='hidden' id="sysid" name="sysid" value="<%= systemId %>">
                <INPUT type='hidden' id="fleetid" name="fleetid" value="<%= fleetId %>" /><%
                 break;
             }
             %> <INPUT type="submit" name="Weiter" value="Weiter" onclick="<%
             if (sourcePage == 1) {
                 %>if (document.getElementById('target').value == 0) { alert('Ziel w&auml;hlen!'); return false; }<%
             } else {
                %>if (document.getElementById('fleetid').value == 0) { alert('Flotte w&auml;hlen!'); return false; }<%
             }
             %>">
        </FORM> <%
     } catch (Exception e) {
         %>Der folgende Fehler ist aufgetreten:<br/><%
         PrintWriter outputWriter = new PrintWriter(out);
        e.printStackTrace(outputWriter);
        outputWriter.flush();
     }
 %>
