<%@page import="at.viswars.*"%>
<%@page import="java.sql.*"%>
<%@page import="at.viswars.database.access.DbConnect" %>
<%@page import="at.viswars.Logger.*"%>
<link rel="stylesheet" href="main.css.jsp" type="text/css">
<%
session = request.getSession();
int userID = Integer.parseInt((String)session.getAttribute("userId"));
%>
<FORM>
<TABLE align="center" width="800px" style="font-size:13px">
    <TR>
        <TD>
            Du kannst dir jetzt dein Heimatsystem konfigurieren, du hast 10 Punkte zur Verf�gung, wenn du dir bei den Ressourcen schlechtere Werte einstellst, 
            bekommst du zus�tzliche Punkte mit denen du die maximale Bev�lkerung in deinem Heimatsystem erh�hen kannst.<BR>
            Falls du ein abgelegenes System gew�hlt hast, ist es ratsam eher mit vielen Rohstoffen zu starten, da es sonst m�glichweise aufwendig werden kann, 
            Rohstoffe zu deinem Hauptplanet zu transportieren!<BR><BR>
        </td>
    </tr>
    <TR>
        <TD>
            <FONT style="font-size:17px"><B>Basispunkte: 10</B></FONT><BR><BR>
        </td>
    </tr>
    <TR>
        <TD>
    <TABLE style="font-size:13px"><TR>
        <TD>
            <FONT style="font-size:15px"><B>Gr��e des Heimatplanets einstellen (7,5 - 9,5 Mrd. Einwohner)</B></FONT>
        </td>
        </TR><TR>
        <TD>
            <TABLE style="font-size:13px"><TR valign="middle"><TD>
                        <IMG src="<%= GameConfig.picPath() %>pic/M/ns1k.jpg"/>
                    </td>
                    <TD>
                        <input type="radio" name="homeplanet" value="2" checked>Wenig [ca. 7,5 Mrd.] (0 Punkte)<br>
                    </td>
                </tr>
            </TABLE>
        </td>
        </TR><TR>
        <TD>
            <TABLE style="font-size:13px"><TR valign="middle"><TD>
                        <IMG src="<%= GameConfig.picPath() %>pic/M/nm1k.jpg"/>
                    </td>
                    <TD>
                        <input type="radio" name="homeplanet" value="2">Wenig [ca. 8,5 Mrd.] (-6 Punkte)<br>
                    </td>
                </tr>
            </TABLE>
        </td>
        </TR><TR>
        <TD>
            <TABLE style="font-size:13px"><TR valign="middle"><TD>
                        <IMG src="<%= GameConfig.picPath() %>pic/M/nb1k.jpg"/>
                    </td>
                    <TD>
                        <input type="radio" name="homeplanet" value="2">Wenig [ca. 9,5 Mrd.] (-14 Punkte)<br>
                    </td>
                </tr>
            </TABLE>
        </td>   
        </TR>
        </TABLE></TD>
    </tr>
    <TR>
        <TD>
            <BR><FONT style="font-size:15px"><B>Zus�tzliche Bev�lkerung auf anderen Planeten im System</B></FONT>
        </td></TR>
    <TR><TD>
            <SELECT size='1' id="addPop" name="addPop">
                <OPTION value="0" SELECTED>Keine (0 Punkte)</OPTION>
                <OPTION value="-2">+1 Milliarde (-2 Punkte)</OPTION>
                <OPTION value="-4">+2 Milliarde (-4 Punkte)</OPTION>
                <OPTION value="-6">+3 Milliarde (-6 Punkte)</OPTION>
                <OPTION value="-8">+4 Milliarde (-8 Punkte)</OPTION>
                <OPTION value="-10">+5 Milliarde (-10 Punkte)</OPTION>
            </SELECT>        
        </TD>
    </tr>    
    <TR>
        <TD>
            <BR><TABLE style="font-size:15px"><TR valign="middle"><TD><B>Menge an Eisenerz im System</B></TD><TD><IMG src="<%= GameConfig.picPath() %>pic/menu/icons/ore.png"/></TD></TR></TABLE>
        </td>
    <TR>
        <TD>
            <input type="radio" name="ironress" value="2">Wenig [50%] (+2 Punkte)<br>
            <input type="radio" name="ironress" value="0" checked>Normal (0 Punkte)<br>
            <input type="radio" name="ironress" value="-4">Viel [200%] (-4 Punkte)       
        </td>
    </tr>
    </tr>       
    <TR>
        <TD>
            <BR><TABLE style="font-size:15px"><TR valign="middle"><TD><B>Menge an seltenen Rohstoffen im System</B></TD><TD><IMG src="<%= GameConfig.picPath() %>pic/menu/icons/ynkeore.png"/></TD><TD><IMG src="<%= GameConfig.picPath() %>pic/menu/icons/howalore.png"/></TD></TR></TABLE>
        </td>
    </tr>    
    <TR>
        <TD>
            <input type="radio" name="rareress" value="4">Keine seltenen Rohstoffe (+4 Punkte)<br>
            <input type="radio" name="rareress" value="0" checked>Normal (0 Punkte)<br>
            <input type="radio" name="rareress" value="-6">Viel [300%] (-6 Punkte)       
        </td>
    </tr>    
    <TR>
        <TD align="center">
            <BR><input type="submit" value="Los geht's!">
        </td>
    </tr>
</TABLE>
</FORM>