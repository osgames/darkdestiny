/**************************************************
 * Zahlenformatierung                             *
 **************************************************
 *
 */
function number_format (zahl,decimal_num,cutEndZero) {
    var dec_point = ',';
    var mill_sep = '.';
    var sign = '';
    
    if (zahl < 0) {
        sign = '-';
    }
    
    zahl = Math.abs(zahl);
    var divisor = 1;
    
    for (var i = 0; i < decimal_num; i++) {
        zahl *= 10; 
        divisor *= 10;
    }
    
    zahl = Math.floor(zahl);    
    var decimals = zahl % divisor;
    decimals = '' + decimals;
    
    for (var i = 0; i < decimal_num; i++) {
        zahl /= 10;
    }
    
    zahl = Math.floor(zahl);
    zahl = '' + zahl;
    zahl_grp = '';
    while (zahl.length > 3) {
        zahl_grp = mill_sep + zahl.substr(zahl.length-3,3) + zahl_grp;
        zahl = zahl.substr(0,zahl.length-3);
    }
    
    var str = zahl + zahl_grp;
    if (decimal_num > 0) {
        str = str + dec_point;
        while (decimals.length != decimal_num) {
            decimals = '0' + decimals;
        }
        str = str + decimals
    }
    
    if (cutEndZero == true) {
      while (str.charAt(str.length - 1) == '0') {
          str = str.substr(0,str.length-1);          
      }
      
      if (str.charAt(str.length - 1) == ',') {
          str = str.substr(0,str.length-1);
      }
    }
    
    return sign + str;
}          