<%@page import="at.viswars.*"%>
<%@page import="at.viswars.service.MainService"%>
<%@page import="at.viswars.model.*"%>
<%
session = request.getSession();

if (session.getAttribute("userId") == null) {
    // Let the invalid or timed out login be handled by main.jsp
    %>
        <jsp:forward page="main.jsp" >
                <jsp:param name="page" value='empty' />
        </jsp:forward>
    <%
    return;
}
int userId = Integer.parseInt((String)session.getAttribute("userId"));

GameUtilities gu = new GameUtilities();
gu.setUserId(userId);

if (request.getParameter("showPlanet") == null) {
%>
    <jsp:forward page="main.jsp" >
            <jsp:param name="page" value='empty' />
    </jsp:forward>
<%
} else {
    int planetToShow = Integer.parseInt(request.getParameter("showPlanet"));
    gu.setPlanetId(planetToShow);
    if (gu.getSwitchInfo() == GameConstants.OWN_PLANET) {
        session.setAttribute("actPlanet",Integer.toString(planetToShow));
        Planet p = MainService.findPlanetById(planetToShow);
        session.setAttribute("actSystem",""+p.getSystemId());
%>
        <jsp:forward page="main.jsp" >
                <jsp:param name="page" value='new/overview' />
        </jsp:forward>
<%
    } else if (gu.getSwitchInfo() == GameConstants.ENEMY_PLANET) {
%>
    <jsp:forward page="main.jsp" >
            <jsp:param name="page" value='empty' />
    </jsp:forward>
<%
    } else if (gu.getSwitchInfo() == GameConstants.NEUTRAL_PLANET) {
%>
    <jsp:forward page="main.jsp" >
            <jsp:param name="page" value='empty' />
    </jsp:forward>
<%
    }
}

gu = null;
%>
