<%
session = request.getSession();
// Diese Seite ruft die Flottenversendungsseite ausgehend von der Systemansicht auf
int systemId = 0;
int planetId = 0;
boolean toSystem = false;

if (request.getParameter("systemId") != null) {
    systemId = Integer.parseInt((String)request.getParameter("systemId"));
    toSystem = true;
}

if (request.getParameter("planetId") != null) {
    planetId = Integer.parseInt((String)request.getParameter("planetId"));
    toSystem = false;
}

int target = 0;
if ((systemId != 0) || (planetId != 0)) {
    if (toSystem) {
        target = systemId;
    } else {
        target = planetId;
    }
    
    session.setAttribute("source","2");
%>
    <jsp:include page="fleetactions.jsp" >
            <jsp:param name="toSystem" value='<%= toSystem %>' />
            <jsp:param name="targetId" value='<%= target %>' />
    </jsp:include>
<%
}
%>