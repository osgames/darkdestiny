<%@page import="at.viswars.voting.VoteUtilities"%>
<%@page import="at.viswars.voting.Vote"%>
<%@page import="at.viswars.service.VotingService"%>
<%@page import="at.viswars.model.UserData" %>
<%@page import="at.viswars.model.Voting"%>
<%@page import="at.viswars.ML"%>
<%@page import="at.viswars.DebugBuffer" %>
<%@page import="at.viswars.DebugBuffer.DebugLevel" %>

<%

Integer voteId = Integer.parseInt(request.getParameter("voteId"));
Vote v = VoteUtilities.getVote(voteId);

 int userId = Integer.parseInt((String) session.getAttribute("userId"));

%>
<%= VoteUtilities.getHtmlCode(v, voteId, userId) %>
