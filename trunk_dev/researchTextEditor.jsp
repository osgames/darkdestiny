
<%@page import="java.util.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.model.Research" %>
<%@page import="at.viswars.ML" %>
<%@page import="at.viswars.GameConfig" %>
<SCRIPT TYPE="text/javascript">
   
    function popup(mylink, windowname)
    {
        if (! window.focus)return true;
        var href;
        if (typeof(mylink) == 'string')
            href=mylink;
        else
            href=mylink.href;
        window.open(href, windowname, "resizable=no,scrollbars=yes,status=no");
        return false;
    }
</SCRIPT>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    </head>
    <body bgcolor="BLACK">
        <%
                    int researchId = 0;

                    int type = 0;

                    if (request.getParameter("researchId") != null) {
                        researchId = Integer.parseInt(request.getParameter("researchId"));
                    }
                    if (request.getParameter("type") != null) {
                        type = Integer.parseInt(request.getParameter("type"));
                    }
                    if (type == 1) {
                        ResearchService.updateResearchText(request.getParameterMap());
                    }

                    if (researchId == 0) {
                        HashMap<String, Research> sorted = new HashMap<String, Research>();
                        for (Research r : (ArrayList<Research>) Service.researchDAO.findAll()) {
                            sorted.put(ML.getMLStr(r.getName(), request.getLocale()), r);
                    }

        %>
        <TABLE style="border-color: white; border-width: 2px; border-style: solid;" width="100%">
            <TR>
                <TD style="width:15%; border-color: white; border-width: 1px; border-style: solid; color: white">
                    Name
                </TD>
                <TD style="width:35%; border-color: white; border-width: 1px; border-style: solid; color: white">
                    Kurzer Text
                </TD>
                <TD style="width:35%; border-color: white; border-width: 1px; border-style: solid; color: white">
                    Langer Text
                </TD>
                <TD style="width:35%">
                    
                </TD>
            </TR>
            <%

        int i = 0;
        for (Map.Entry<String, Research> entry : sorted.entrySet()) {
            Research r = entry.getValue();
            String color = "#555555";
            if (i % 2 == 0) {
                color = "#666666";
            }
            %>
            <TR bgcolor="<%= color%>">
                <TD style="color: white">
                    <%= entry.getKey()%>
                </TD>
                <TD style="color: white">
                    <%= ML.getAtlanMLStr(r.getShortText(), request.getLocale())%>
                </TD>
                <TD style="color: white">
                    <%= ML.getAtlanMLStr(r.getDetailText(), request.getLocale())%>
                </TD>
                <TD>
                    <A href="researchTextEditor.jsp?researchId=<%= r.getId()%>"><IMG alt="Editieren" src="<%= GameConfig.picPath()%>pic/edit.jpg" /></A><BR>
                    <A onClick="return popup(this, 'notes')" target="blank" href="researchText.jsp?researchId=<%= r.getId()%>&preview=true"><FONT color="white">Vorschau-Text</FONT></A><BR>
                    <A onClick="return popup(this, 'notes')" target="blank" href="researchNotification.jsp?researchId=<%= r.getId()%>"><FONT color="white">Vorschau-Nachricht</FONT></A>
                </TD>
            </TR>

            <%
                                        i++;
                                    }
            %>
        </TABLE>
        <%
                            } else {

                                Research r = Service.researchDAO.findById(researchId);
        %>
        <FORM action="researchTextEditor.jsp?researchId=<%= r.getId()%>&type=1" method="post">
            <CENTER>
                <H1>
                    <FONT color="WHITE"><%= ML.getMLStr(r.getName(), request.getLocale())%></FONT>
                </H1>
            </CENTER>
            <TABLE align="center" style="border-color: white; border-width: 2px; border-style: solid;" width="80%">
                <TR>
                    <TD style="border-color: white; border-width: 1px; border-style: solid; color: white">
                        Kurzer Text
                    </TD>
                    <TD style="border-color: white; border-width: 1px; border-style: solid; color: white">
                        Langer Text
                    </TD>
                </TR>
                <TR>
                    <TD align="left" width="50%">
                        <TEXTAREA name="shortText" style="width:90%" rows="20"  style="" ><%= ML.getAtlanMLStr(r.getShortText(), request.getLocale())%></TEXTAREA>
                    </TD>
                    <TD align="left" width="50%">
                        <TEXTAREA name="detailText" style="width:90%" rows="20" style="" ><%= ML.getAtlanMLStr(r.getDetailText(), request.getLocale())%></TEXTAREA>
                    </TD>
                </TR>
                <TR>
                    <TD colspan="2" align="center">
                        <input type="submit" value="Speichern" />
                    </TD>
                </TR>
                <TR>
                    <TD colspan="2" align="center">
                        <A onClick="return popup(this, 'notes')" target="blank" href="researchText.jsp?researchId=<%= r.getId()%>&preview=true"><FONT color="white">Vorschau</FONT></A>
                    </TD>
                </TR>
            </TABLE>
        </FORM>

        <%

                    }
        %>

    </body>
</html>
