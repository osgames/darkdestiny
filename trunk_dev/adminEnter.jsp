
<%@page import="at.viswars.service.*"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.utilities.MD5"%>
<%
            String userName = "";
            String password = "";
            if (request.getParameter("user") != null) {
                userName = request.getParameter("user");
            }
            if (request.getParameter("password") != null) {
                password = MD5.encryptPassword(request.getParameter("password"));
            }            
            
            User u = LoginService.findUserBy(userName, password);
            if(u == null){

            %>
            <CENTER>User nicht gefunden</CENTER>
            <%
            } else if (u.getAdmin()) {
            session = request.getSession();
            session.setAttribute("userId", String.valueOf(u.getUserId()));

            if (u.getAdmin()) {
                session.setAttribute("admin", true);
                session.setAttribute("adminLogin", true);
                session.setAttribute("actSystem", "0");
                session.setAttribute("userId", String.valueOf(u.getUserId()));
                
                response.sendRedirect("main.jsp?subCategory=99&page=admin/index");
            }else{
                try{
                    session.removeAttribute("admin");
                   }catch(Exception e){
                   }                                    
            }

} else {
            %>
            <CENTER>Kein Admin</CENTER>
            <%            
}            
%>