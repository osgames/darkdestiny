<%@page import="at.viswars.utilities.LoginUtilities"
%><%@page import="at.viswars.img.charts.GroundCombatChart"
%><%@page import="at.viswars.img.charts.ChartDrawer"
%><%@page import="at.viswars.img.BaseRenderer"
%><%@page import="at.viswars.img.BackgroundPainter"
%><%@page import="at.viswars.img.IModifyImageFunction"
%><%@page import="java.util.List"
%><%@page import="java.util.LinkedList"
%><%@ page contentType="image/png" %><%

LoginUtilities.checkLogin(request, response);

List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();

// Check for DATA_FOR_SCAN_PIC
Object attr = session.getAttribute("DATA_FOR_SCAN_PIC");
if (attr != null) {
    allItems = (LinkedList<IModifyImageFunction>)attr;    
    session.removeAttribute("DATA_FOR_SCAN_PIC");
} else if (request.getParameterMap().containsKey("dynamicImageType")) {
    String type = request.getParameterValues("dynamicImageType")[0];
    if ("groundFightLog".equals(type.trim())) {
        int tick = Integer.parseInt(request.getParameterValues("tick")[0]);
        int planetId = Integer.parseInt(request.getParameterValues("planetId")[0]);
        
        allItems.add(new ChartDrawer(GroundCombatChart.createFor(
                LoginUtilities.getUser(request, response),
                tick, planetId
                )));
    }
}

BaseRenderer renderer = new BaseRenderer(400,400);
// Schreibt den Mime-Type in den Log, Nur Benutzen, wenn 
// genau das gew&uuml;nscht wird
// renderer.getImageMimeType(); 

renderer.modifyImage(new BackgroundPainter(0x000000));

for (IModifyImageFunction mif : allItems) {
	renderer.modifyImage(mif);
}
        
renderer.sendToUser(response.getOutputStream());
%>