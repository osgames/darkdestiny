<%@page import="at.viswars.*"%>
<%@page import="at.viswars.DebugBuffer.DebugLevel"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="at.viswars.StarMapInfo"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="at.viswars.xml.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.OutputStreamWriter"%>
<%@page import="at.viswars.database.access.*" %>
<%
        Locale locale = request.getLocale();

if(session.getAttribute("Language") != null){
    locale = (Locale)session.getAttribute("Language");
}
        session = request.getSession();
        int userID = Integer.parseInt((String) session.getAttribute("userId"));

        XMLMemo techtree = new XMLMemo("Techtree");

        techtree = TechTreeInfo.addConstants(techtree, userID, locale);
        techtree = TechTreeInfo.addUnlockedTechnologies(techtree, userID);
        techtree = TechTreeInfo.addResearches(techtree, userID);
        techtree = TechTreeInfo.addTechRelation(techtree, userID);
        
        // PrintWriter toJSP = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "ISO-8859-1"));
        response.setCharacterEncoding("ISO-8859-1");
        // PrintWriter toJSP = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "ISO-8859-1"));
        PrintWriter toJSP = new PrintWriter(out);
        techtree.write(toJSP);
%>