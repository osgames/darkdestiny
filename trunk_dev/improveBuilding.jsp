<%@page import="at.viswars.utilities.ConstructionUtilities"%>
<%@page import="at.viswars.buildable.ConstructionExt"%>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.databuffer.*" %>
<%@page import="at.viswars.construction.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.result.*" %>
<%@page import="at.viswars.requestbuffer.*" %>
<%@page import="at.viswars.model.Ressource" %>
<%@page import="at.viswars.model.PlayerPlanet" %>
<%@page import="at.viswars.model.Construction" %>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int constructionId = Integer.parseInt(request.getParameter("constructionId"));

DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"ConstructionId = "+constructionId);

//AbortConstructionResult acr = ConstructionService.getAbortRefund(orderId);
//ConstructionAbortRefund car = acr.getConsAbortRefund();
BuildableResult br = ConstructionUtilities.canBeImproved(Service.constructionDAO.findById(constructionId), userId, planetId);
ConstructionImproveResult cir = new ConstructionImproveResult(constructionId, planetId);
ConstructionImproveCost cic = cir.getConstructionImproveCost();

if (request.getParameter("qi") != null) {
    int qIdent = Integer.parseInt(request.getParameter("qi"));
%>
    <jsp:forward page="main.jsp?page=confirm/confirmBuildingUpgrade" />
<%
}

ConstructionImproveBuffer coib = new ConstructionImproveBuffer(userId);
coib.setParameter(coib.CONSTRUCTION_ID, constructionId);
coib.setParameter(coib.PLANET_ID, planetId);
%>
        <script language="Javascript">
<%
    for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (cic.getRess(r.getId()) > 0) {
            out.write("var " + r.getName() + "Per = " + cic.getRess(r.getId()) + ";");
            out.write("\n");
        }else if( cic.getRess(r.getId()) < 0){
            out.write("var " + r.getName() + "Per = " + (-cic.getRess(r.getId())) + ";");
            out.write("\n");
            }
    }
%>
            var maxCount = 1;
            
            var actCount = 1;
            
          //  var totalCredit = db_ressource_creditsPer;
<%
    for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (cic.getRess(r.getId()) > 0||
                cic.getRess(r.getId()) < 0) {
            out.write("var total" + r.getName() + " = " + r.getName() + "Per;");
            out.write("\n");
        }
    }
%>            
            
            function calcTotal() {
                if(actCount > maxCount){
                    actCount = maxCount;
                              <%
                              out.write("document.getElementById(\"destroyCount\").value = number_format(maxCount,0);");
                              %>

                }
                totalCredit = actCount * db_ressource_creditsPer;
<%
        for (Ressource r : OverviewService.findAllStoreableRessources()) {
            if (cic.getRess(r.getId()) > 0||
                cic.getRess(r.getId()) < 0) {
                out.write("total" + r.getName() + " = actCount * " + r.getName() + "Per;");
                out.write("\n");
            }
        }
%>                                   
           //     document.getElementById("db_ressource_creditsPer").innerHTML = number_format(totalCredit,0) + ' Credits';
<%
        for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (cic.getRess(r.getId()) > 0||
                cic.getRess(r.getId()) < 0) {
            out.write("document.getElementById(\""+r.getName()+"\").innerHTML = number_format(total"+r.getName()+",0);");
            out.write("\n");
        }
    }
%>
            }

        </script>
        <FORM method='post' action='main.jsp?page=improveBuilding&qi=<%= coib.getId() %>' name='confirm' >
            <INPUT type="hidden" name="constructionId" value="<%= constructionId %>">
        <TABLE width="80%" style="font-size:13px"><TR>
        <TD colspan=2 align="center"><%= ML.getMLStr("construction_msg_improveconstruction",userId)%>.<BR><BR></TD></TR>
        <TR valign="middle">
            <TD width="200"><%= ML.getMLStr("construction_lbl_constructionType", userId) %>:</TD><TD><%= ML.getMLStr(((Construction)cir.getConsExt().getBase()).getName(), userId) %></TD></TR>
        <TR valign="middle"><TD><%= ML.getMLStr("construction_lbl_planet", userId)%>:</TD><TD><%= OverviewService.findPlayerPlanetByPlanetId(planetId).getName() %></TD></TR>
        <TR valign="middle"><TD><%= ML.getMLStr("construction_lbl_level",userId)%></TD><TD><%= (cir.getLevel() + 1) %> (<%= cir.getLevel()  %>)<BR></TD></TR>
        <TR valign="top">
                      <% if (cic.getRess(Ressource.CREDITS) < 0){
            %>
            <TD><%= ML.getMLStr("construction_lbl_costs", userId)%>:</TD>
            <TD><SPAN id="db_ressource_credits"> <%= FormatUtilities.getFormattedNumber(-cic.getRess(Ressource.CREDITS)) %></SPAN><BR></TD>

            <%
            }else{
            %>
             <TD><%= ML.getMLStr("construction_lbl_creditrefund", userId)%></TD>
             <TD><SPAN id="db_ressource_credits"> <%= FormatUtilities.getFormattedNumber(cic.getRess(Ressource.CREDITS)) %></SPAN><BR></TD>

            <%
            }
            %>
        </TR>
        
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcecost",userId) %></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
            </TR>
<%
        for (Ressource r : RessourceService.getAllStorableRessources()) {
            if(r.getId() == Ressource.CREDITS)continue;
        if (cic.getRess(r.getId()) > 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(), userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(cic.getRess(r.getId())) + "</SPAN></TD></TR>");
        }
    }
        if(br.isBuildable()){
%>
        <TR><TD colspan=2 align="CENTER"><BR><INPUT type="submit" name="Ok" value="Ok"></TD></TR>
        <% }else{ 
        for(String msg : br.getMissingRequirements()){    
        %>
        
        <TR><TD style="font-weight: bolder; color: yellow;" colspan=2 align="CENTER"><%= msg %></TD></TR>
        <% } 
                   }
        %>
        </TABLE>                    
        </FORM>        
