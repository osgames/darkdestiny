<%-- 
    Document   : galaxyTester
    Created on : Jul 11, 2012, 6:13:09 PM
    Author     : Admin
--%>

<%@page import="at.viswars.result.GalaxyCreationResult"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="at.viswars.model.Galaxy"%>
<%@page import="java.util.HashSet"%>
<%@page import="at.viswars.enumeration.EGalaxyType"%>
<%@page import="at.viswars.enumeration.EPlanetDensity"%>
<%@page import="at.viswars.UniverseCreation"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello I will scale the Galaxys for you</h1>
    </body>
    <%
    
    int[] sizes = new int[6];
    sizes[0] = 250;
    sizes[1] = 500;
    sizes[2] = 1000;
    sizes[3] = 2000;
    sizes[4] = 5000;
    sizes[5] = 10000;
    
    TreeMap<Integer, Integer> generatedGalaxys = new TreeMap<Integer, Integer>();
    
        java.lang.System.out.println("starting generation : ");
    for(int i = 0; i < sizes.length ; i++){
        int size = sizes[i];
        GalaxyCreationResult g = UniverseCreation.createNewGalaxy(size, size, false, EPlanetDensity.middle, EGalaxyType.spiral, 30, size, size);
        g.persist();
        java.lang.System.out.println("done galaxy : " + size);
        generatedGalaxys.put(size, g.getGalaxy().getId());
    }
        
    
    %>
    <TABLE>
        <TR>
            <% for(Map.Entry<Integer, Integer> entry : generatedGalaxys.entrySet()){ %>
            <TD>
                <TABLE>
                    <TR>
                        <TD>
                            <%= entry.getKey() %>-<%= entry.getValue() %>
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            <% if(entry.getKey() < 1000){ %>
                            <IMG src="GetGalaxyPic?galaxyId=<%= entry.getValue() %>" width="<%= entry.getKey() %>" height="<%= entry.getKey() %>" />
                            <% }else{ %>
                            <IMG src="GetGalaxyPic?galaxyId=<%= entry.getValue() %>" width="1000" height="1000" />
                            <% } %>
                        </TD>
                    </TR>
                </TABLE>
            </TD>
            <% } %>
        </TR>
    </TABLE>
    
        <%
        
   /* for(Map.Entry<Integer, Integer> entry : generatedGalaxys.entrySet()){
        Service.galaxyDAO.deleteGalaxy(entry.getValue());
    }*/
        %>
    
</html>
