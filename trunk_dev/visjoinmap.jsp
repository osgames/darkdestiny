<%@page import="at.viswars.DebugBuffer"%>
<%@page import="java.util.Locale" %>
<%@page import="at.viswars.ML" %><%
// Determine correct connection path
    session = request.getSession();

    Locale locale = request.getLocale();
    if (session.getAttribute("Language") != null) {
        locale = (Locale) session.getAttribute("Language");
    }
    int userId = 0;
    try {
        userId = Integer.parseInt((String) session.getAttribute("userId"));
    } catch (Exception e) {
    }


    if ((userId == 0) || ((session.getAttribute("actSystem") != null && (session.getAttribute("adminLogin") == null)))) {

        java.lang.System.out.println("userId" + userId);
        java.lang.System.out.println("session.getAttribute()" + session.getAttribute("actSystem"));
        java.lang.System.out.println(session.getAttribute("adminLogin"));
        try {
            session.invalidate();
        } catch (IllegalStateException ise) {
        }
%>
<jsp:forward page="login.jsp">
    <jsp:param name="errmsg" value='Alte Session gel�scht bitte erneut einloggen!' />
</jsp:forward>         
<%    }

    StringBuffer xmlUrlBuffer = request.getRequestURL();
    String xmlUrl = "";
    int slashCounter = 0;

    for (int i = 0; i < xmlUrlBuffer.length(); i++) {
        char currChar = xmlUrlBuffer.charAt(i);
        if (currChar == ("/".toCharArray())[0]) {
            slashCounter++;
        }

        xmlUrl += currChar;
        if (slashCounter == 4) {
            break;
        }
    }

    String systemIdStr = "";
    if (request.getParameter("marker") != null) {
        try {
            int systemId = Integer.parseInt(request.getParameter("marker"));
            if (systemId > 9999) {
                systemId = 0;
            } else {
                systemIdStr = "" + systemId;
            }
        } catch (NumberFormatException nfe) {
            DebugBuffer.addLine(nfe);
        }
    }
%>
<html>
    <head>
        <title>
            Dark Destiny - The ultimate online strategy game
        </title>
    </head>
    <body bgcolor="black">

    <center>
        <FORM method="post" action="visjoinmap.jsp">
            <TABLE width="60%" align="center">
                <TR>
                    <TD>
                        <P><font color="yellow"><B><%= ML.getMLStr("visjoinmap_msg_1", locale)%><BR /><BR />
                                <%= ML.getMLStr("visjoinmap_msg_2", locale)%><BR/><BR />
                                <BR>
                                <%= ML.getMLStr("visjoinmap_msg_jre", locale)%>

                            </B></font>
                        </P>
                    </TD>
                </TR>
            </TABLE>
        </FORM>
        <applet code="at.viswars.vismap.gui.AppletMain" width="600" height="600" alt="" archive="applet/vismap.jar">
            <param name="source" value="<%= xmlUrl%>createJoinMapInfo.jsp"/>
            <param name="sessionId" value="<%= request.getSession().getId()%>"/>
            <PARAM NAME="cache_option" VALUE="NO">
            <param name="MODE" value="JOINMAP">
        </applet>
    </center>
</body>
</html>