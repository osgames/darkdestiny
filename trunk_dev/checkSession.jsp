<%@page import="at.viswars.*"%>
<%@page import="at.viswars.chat.ChatHandler"%>
<%
    session = request.getSession();   
    
    String userId = (String)session.getAttribute("userId");
    
    if (userId != null) {
        ChatHandler.allowAutoLogin(userId, session.getId());
    }
%>