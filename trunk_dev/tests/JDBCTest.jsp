<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.database.access.*" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JDBC Test</title>
    </head>
    <body>
<%
    Statement stmt = DbConnect.createStatement();
    PreparedStatement pstmt = DbConnect.prepareStatement("INSERT INTO testtable (feld2,feld3,feld4) VALUES (?,?,?)");
  
    try {
        stmt.execute("CREATE TABLE testtable (" +
        "feld1 INT AUTO_INCREMENT, " +
        "feld2 MEDIUMINT NOT NULL, " +
        "feld3 TINYINT NOT NULL, " +
        "feld4 VARCHAR(30), PRIMARY KEY(feld1)) ENGINE=MyISAM");
    } catch (Exception e) {
        out.write("CREATING TABLE FAILED: " + e + "<BR><BR>");
    }
    
    out.write("TABLE testtable SUCESSFULLY CREATED<BR><BR>");
    
    try {
        DbConnect.getConnection().setAutoCommit(false);
        
        int i=1;
        String testText = "BLABLA";
        while (i<4) {
            pstmt.setInt(1,i*12);
            pstmt.setInt(2,i);
            pstmt.setString(3,testText);
            pstmt.addBatch();
            i++;
        }
            
        pstmt.executeBatch();        
        stmt.execute("COMMIT");
        
        DbConnect.getConnection().setAutoCommit(true);
    } catch (Exception e) {
        out.write("COMPLEX TEST FAILED: " + e + "<BR><BR>");
    }
    
    out.write("WRITTEN DATA:<BR><BR>");
    
    try {
    ResultSet rs = stmt.executeQuery("SELECT feld1, feld2, feld3, feld4 FROM testtable");
    while (rs.next()) {
        out.write("FELD 1="+rs.getInt(1)+" FELD 2="+rs.getInt(2)+" FELD 3="+rs.getInt(3)+" FELD 4="+rs.getString(4)+"<BR>");
    }
    } catch (Exception e) {
        out.write("SELECT FAILED: " + e + "<BR><BR>");
    }
    out.write("<BR>ALL TESTS SUCCESSFULL<BR><BR>");
    
    try {
        stmt.execute("DROP TABLE testtable");
    } catch (Exception e) {
        out.write("DROP TABLE FAILED: " + e + "<BR><BR>");
    }    
    
    out.write("DELETED TESTTABLE");
    
    stmt.close();
    pstmt.close();
%>
    </body>
</html>
