<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="at.viswars.PlanetData"%>
<%@page import="at.viswars.ResearchData"%>
<%@page import="at.viswars.UserData"%>

<%@page import="at.viswars.database.access.*"%>

<%@page import="at.viswars.databuffer.GroundTroopRelationsTable"%>
<%@page import="at.viswars.databuffer.GroundTroopTable"%>
<%@page import="at.viswars.databuffer.ResearchTable"%>
<%@page import="at.viswars.databuffer.UserBuffer"%>

<%@page import="at.viswars.groundcombat.GroundFight"%>
<%@page import="at.viswars.groundcombat.GroundTroop"%>
<%@page import="at.viswars.groundcombat.GroundTroopSize"%>
<%@page import="at.viswars.groundcombat.GroundTroopsRelationEntry"%>

<%@page import="java.sql.Statement"%>

<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="test.ITestResult"%>
<%@page import="test.TestRunner"%>

<%@page import="test.at.viswars.groundcombat.TestHelper"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Test für die neuen Ground-Troops - Teil 1</title>
</head>
<body>
<h1>TODO-Liste</h1>
<ul>
	<li><del>Truppen-Ids, Daten aus der DB laden</del></li>
	<li><del>Angriffswerte aus der DB laden</del> 
        <a href="Bodentruppen%20test.jsp?part=1#liste">Siehe Hier</a></li>
	<li><del>Forschungsverbesserungen für LP, Schilde, Angriffe aus DB laden
        </del> (die gtRelations sind noch nicht angepasst)
		<a href="Bodentruppen%20test.jsp?part=2&userId=1#verb">siehe Hier
        </a></li>
	<li><font color="#800000">Irgendwie sollten alle derzeitigen Eintr&auml;ge 
        ge&auml;ndert werden (Damit bei den Truppen die richtige OwnerId drin steht), 
        Hier fehlt ein Migrationsskript, Unittest ist vorhanden</font></li>
	<li><del>Truppenstaerken laden (implementiert, auch sch&auml;den können geladen 
        werden) </del></li>
	<li><font color="#808000">Eine Runde k&auml;mpfen</font> 
        (Nur einheiten implementiert, keine Verluste bei Geb&auml;uden 
		und Zivilisten, Außerdem fehlen direkte angriffe auf Milit&auml;rgeb&auml;ude, 
		siehe <a href="Bodentruppen%20test.jsp?part=3#kampftab">siehe Hier</a>)
        </li>
	<li><font color="#808000">Infos speichern 
        (Zivilisten und Geb&auml;ude werden noch nicht gespeichert)</font></li>
	<li><font color="#808000">
        <a href="Bodentruppen%20test.jsp?part=4#totalFight">
		Kampf ueber mehrere Ticks</a></font> 
        (Nicht im Tick-Update eingebaut)</li>
	<li><font color="#800000">Log erzeugen</font></li>
	<li>Informationen dem Spieler anzeigen</li>
	<li>Abschluss-Information dem Spieler anzeigen</li>
	<li>Verschiedene Kampf-Typen</li>
	<li><font color="#808000">Alles Testen, auch Unit-Tests erstellen
        </font></li>
	<li>Testen, ob Truppen ordentlich auf Planeten laden können</li>
	<li>Testen, ob Truppen produziert werden können</li>
	<li>Den Cache-Algo untersuchen: Speicherbedarf, Speicherfreigabe 
        (findet eine solche statt?), ist das alles Thread-Save</li>
</ul>
<% if ("null".equalsIgnoreCase(""+request.getParameter("part"))) {%>
<h1>Tests</h1>
<table border="1">
<tr><th>TestName</th><th>Resultat</th><th>StackTrace</th></tr>
<%
	List<String> allTests = new ArrayList<String>();
	// Mal schauen, ob wir alles sicher aus der DB laden können ...
	allTests.add("test.at.viswars.groundcombat.TroopsTest1"); 
	allTests.add("test.at.viswars.groundcombat.TroopsTest2");
	// Check ob die K&auml;mpfe gefunden werden
	allTests.add("test.at.viswars.groundcombat.TroopsTest3"); 
	// Test, ob die Datenbank konsistent ist ...
	allTests.add("test.at.viswars.groundcombat.DBCheck1");    
	// Werden alle Daten für den Kampf ordentlich vorbereitet (Truppengrößen)
	allTests.add("test.at.viswars.groundcombat.TroopsTest4");
	// Ein Testkampf - ein Tick, alles im Memory
	allTests.add("test.at.viswars.groundcombat.MemFight"); 
	// Ein Testkampf - ein Tick, alles im Memory
	allTests.add("test.at.viswars.groundcombat.MemFight2"); 
	// Laden und speichern eines Kampfzustandes testen
	allTests.add("test.at.viswars.groundcombat.TroopsTest5"); 
	allTests.add("test.at.viswars.groundcombat.TroopsTest6"); 
	
	//Tests für den Log
	allTests.add("test.at.viswars.databuffer.DBCheck1"); 
	allTests.add("test.at.viswars.databuffer.DBCheck2"); 
	
	
	int passed = 0;
	int total = 0;
	int ignored = 0;
	int errors = 0;
	for (String s : allTests)
	{
		out.print("<tr><td>"+s+"</td>");
		ITestResult[]  results = TestRunner.run(s);
		if (results.length > 1) {
			out.print("<td colspan=\"2\"><table border=\"1\">\n");
		}
		for (ITestResult res : results) {
			if (results.length > 1) {
				out.print("<tr>\n");
			}
			total ++;
			if (res == ITestResult.TEST_OK)
			{
				out.print("<td bgcolor=\"#00FF00\">OK</td><td></td>");
				passed ++;
			}
			else if (res instanceof ITestResult.TEST_IGNORED)
			{
				out.print("<td bgcolor=\"#FFFF00\">IGNORED</td><td>");
				out.print(TestRunner.createStackTrace(
				        ((ITestResult.TEST_IGNORED)res).getError(),
				        "<br>","&nbsp;&nbsp;&nbsp;&nbsp;"
				        ).toString());
				out.print("</td>");
				ignored ++;
			}
			else 		{
				out.print("<td bgcolor=\"#FF0000\">ERROR</td><td>");
				out.print(TestRunner.createStackTrace(
				        ((ITestResult.TEST_FAILED)res).getError(),
				        "<br>","&nbsp;&nbsp;&nbsp;&nbsp;"
				        ).toString());
				out.print("</td>");
				errors ++;
			}
			if (results.length > 1) {
				out.print("</tr>\n");
			}
		}
		if (results.length > 1) {
			out.print("</td></table>\n");
		}
		out.println("</tr>");
	}
%>
</table><br>
Ergibt folgende Statistik:<br>
<table border="1">
<tr><th>Tests durchgeführt:</th><td></td><td><%= total %></td></tr>
<tr>
    <td></td>
    <th>davon erfolgreich:</th>
    <td><%= passed %> ( <%= (passed*100)/total %> %)</td>
</tr>
<tr>
    <td></td>
    <th>davon ignoriert:</th>
    <td><%= ignored %> ( <%= (ignored*100)/total %> %)</td>
</tr>
<tr>
    <td></td>
    <th>davon fehlgeschlagen:</th>
    <td><%= errors %> ( <%= (errors*100)/total %> %)</td>
</tr>
</table> 
<% } %>
<h1>Beschreibung</h1>
<code>
Bevor ich mit dem Umbau des Bodenkampfs anfange möchte ich kurz 
das neue Kampfkonzept diskutieren:<br>
<br>
Mein Vorschlag:<br>
<br>
* Ein Kampf dauert mehrere Runden<br>
* Es können mehrere Spieler auf einem Planeten Einheiten haben 
        (&auml;NDERUNG IN DER TABELLE PlayerTroops)<br>
* Wenn diese Spieler nicht alliierte sind, wird dort gek&auml;mpft 
    (Zus&auml;tzliches Einführen einer Tabelle mit allen K&auml;mpfen,
	muss beim Ausladen aktualisiert werden ...)<br>
* Einheiten, die auf einem Planeten ausgeladen werden, haben einen Nachteil.
	Deshalb darf ERST der Verteidiger schießen, dann erst der Angrifer<br>
* Es sind verschiedene Kampf-Strategien möglich 
    (Einführen eines Feldes in die Planetenverwaltung,
	da der Verteidiger die Art des Kampfes bestimmt)
	(Es wird erstmal nur die offensive Verteidigung eingebaut und getestet. 
    Guerillia-Krieg kommt dann sp&auml;ter)<br>
* Einführung von Verteidigungsgeb&auml;uden (Bunker, etc.) (erst sp&auml;ter ...)<br>
<br>
* Technologien bringen Boni für den entsprechenden Spieler 
    (Einbau einer neuen Tabelle, die die entsprechenden 
	Boni listet), Angriffskr&auml;fte, Schilde und LPs<br>
* Die Angriffskr&auml;fte sind zwischen verschiedenen Einheiten verschieden, 
    aber nie 0 (Einbau einer Tabelle mit den
	Kr&auml;ften, d.h. A greift B an -&gt; Kraft = ..., 
    Ein unteres Limit sichert, dass ein Planet nicht mit EINER passenden 
	Einheit erobert werden kann. Die Kr&auml;fte können abh&auml;ngig sein von der 
    Art der Kriegsführung ..., vgl. gtRelation)<br>
<br>
* Einheiten haben Lebenspunkte und Schilde. Schilde werden bei jedem 
    TICK wieder aufgeladen, Lebenspunkte erst beim Ende des Kampfes<br>
<br>
Tabellen:<br>
<br>
gtrelation: Beschreibt die St&auml;rke des Angriffs, sowie die verbesserungen. 
        Ein negativer wert in der Spalte researchRequired beschreibt die 
        Basisst&auml;rke, wobei die relative Größe dieser Werte die 
        Wahrscheinlichkeit eines Angriffs auf diesen Truppentyp darstellt.<br>
<br>
PlayerTroops: folgende Felder werden benötigt: OwnerId, Erlittener Schaden, 
    Getötete Einheiten (um eine verbesserung
	bei der Todesraten-Berechnung zu erzielen)<br>
GroundTroops: ArmorClass, Strength-min, Strengthmax entfernen, ersetzen 
    durch SchildWert, Lebenspunkte<br>
GroundTroopsAttackValues: AttackingID, DefendingID, ResearchId, WarType, 
    AttackValue &lt;- gtRelation, WarType fehlt, 
    ResearchId &lt;= 0 folgt keine!<br>
GroundTroopsDefResearchValues: unitID, researchId, ShieldInc, LPIncrement<br>
GroundFights: planetID, FightType, starting-Tick, Sowie einstellungen des 
    Angreifers (Bombadierung, Zivilbevölkerung schonen)<br>
PlayerPlanet: FightType muss hinzugefügt werden 
    (darf derzeit aber nur 0 enthalten ...)<br>
GroundFightTypes: Muss folgende Felder enthalten: ID, Name, Java-Klasse<br>
</code>
<% 
if ("0".equalsIgnoreCase(""+request.getParameter("part"))){
	UserBuffer.loadUserBuffer();
	for (UserData ud : UserBuffer.getAllUsers())
	{
		out.println(
		        "Benutzer gefunden: " + ud.getUserName()
		        + "( id = " + ud.getUserId() + ")<br>\n"
		        );
	}
} else if ("1".equalsIgnoreCase(""+request.getParameter("part"))){ 
	//Check for updates of the Attack-values
	if ("update".equalsIgnoreCase(""+request.getParameter("action"))) {
		int srcId = Integer.parseInt(request.getParameter("srcId"));
		int destId = Integer.parseInt(request.getParameter("destId"));
		int research = Integer.parseInt(request.getParameter("research"));
		float power = Float.parseFloat(request.getParameter("power"));
		float hitProb = Float.parseFloat(request.getParameter("hitProb"));
		float attackProb = Float.parseFloat(request.getParameter("attackProb"));
		GroundTroopsRelationEntry entry = new GroundTroopsRelationEntry(
		        srcId, destId, research, power, attackProb, hitProb
		        );
		GroundTroopRelationsTable.updateAttackingDescription(entry);
	}
%>
<h1>Liste aller Typen</h1>
<a name="liste"></a>
<p>
Hier noch eine kurze Einführung in die Bedeutung der Werte:
</p>
<ul>
	<li> Attack-Power entspricht der Anzahl an Lebenspunkten den der 
         Gegner bei einem Treffer verliert
	<li> Hit Probab(ility) entspricht der Wahrscheinlichkeit 
         (ACHTUNG: Sollte zwischen 0 und 1 liegen) dass eine gegnerische 
         Einheit getroffen wird. Wird nicht getroffen, werden Zivil-Bevölkerung 
		 und Geb&auml;ude getroffen!!!
	<li> attack Probab(ility) ist ein Faktor, welche Einheit bevorzugt 
         angegriffen wird. Braucht sollte nur vergleichbare Werte haben 
         (z.B. im Bereich 0.1 bis 10000)
</ul>
<p><table border="1">
<tr><th>ID</th><th>Name</th><th>Verteidigung</th><th>Offensive</th></tr>
<%
	for (int id : GroundTroopTable.getInstance().getAllAvailableIds())
	{ 
	GroundTroop gt = GroundTroopTable.getInstance().getById(id); 
	%><tr><td><%=gt.getTroopId() %></td><td><%=gt.getName() %></td>
		<td>LP: <%=gt.getLivePoints(-1)%>
            <br>Schilde: <%=gt.getShieldPoints(-1)%>
        </td>
        <td><%
	for (int id2 : GroundTroopTable.getAllTypeIds())
		if (id2 != id)
		{
			GroundTroop gt2 = GroundTroopTable.getTroopById(id2);
			out.print("gegen "+gt2.getName()+" : "); %>
			<table border="1" cellspacing="0" cellpadding="0">
			<tr>
                <th>Research Required</th>
                <th>AttackPower</th>
                <th>hitProbab</th>
                <th>attackProbab</th>
            </tr>
			<%
			for (
			        GroundTroopsRelationEntry entry : 
			            GroundTroopRelationsTable
			                    .getAllAttackingDescription(id, id2)
			            ) {
				%><tr>
					<form action="Bodentruppen%20test.jsp" method="post">
						<input type="hidden" name="action" value="update" />
						<input type="hidden" name="part" value="1" />
						<input type="hidden" name="srcId" value="<%=
						    gt.getTroopId() %>" />
						<input type="hidden" name="destId" value="<%=id2 %>" />
						<td><select name="research">
							<option value="0" <%=
							((entry.getResearchRequired() == 0) ? "SELECTED":"")
							%>" >Keine</option>
							<% 
							for (
							        ResearchData rd 
							             : ResearchTable.getResearchList()
							        ) {
								out.print(
								        "<option value=\"0\" "
								        + ((entry.getResearchRequired() 
								                == rd.getResearchId()) 
								                ? "SELECTED" : "")
								        + " >"
								        + rd.getResearchName() + "</option>"
								        );
								} %>
							</select></td>
						<td><input type="text" name="power" value="<%= 
						    entry.getAttackHitPoints() %>"></td>
						<td><input type="text" name="hitProb" value="<%=
						    entry.getHitProbab() %>"></td>
						<td><input type="text" name="attackProb" value="<%=
						    entry.getAttackProbab() %>"></td>
						<td><input type="submit" value="update"></td>
					</form>
				</tr>
<%			} %>
			</table>
<%		} %>
	</td></tr>
<%	}%>
</table>
<% }
else if ("2".equalsIgnoreCase(""+request.getParameter("part"))){
%>
<h1>Und Ihrer Verbesserungen</h1>
<a name="verb"></a>
<%
	int userId = 7;
	if (request.getParameter("userId") != null)
	{
		userId = Integer.parseInt(request.getParameter("userId"));
	}
%>
Für den Spieler <%= userId %> "<% 
	UserBuffer.loadUserBuffer();
	UserData ud = UserBuffer.getUserData(userId);
	out.print(ud.getUserName());
%>" <br>
Select another user <form action="Bodentruppen%20test.jsp" method="post">
<input type="hidden" name="part" value="2" />
<select name="userId">
<%
/*
	for (UserData d : UserBuffer.getAllUsers())
	{
		out.println(
		        "<option value=\"" + d.getUserId() + "\" "
		        + ((d.getUserId() == userId) ? "selected" : "") + " > " 
		        + d.getUserName() + " </option>"
		        );
	}
 */
%>
</select><input type="submit" />
</form><p>
<table border="1">
<tr><th>Name</th><th>Verteidigungen</th><th>Offensive</th></tr>
<%
	for (int id : GroundTroopTable.getAllTypeIds())
	{ 
	GroundTroop gt = GroundTroopTable.getTroopById(id);
	out.print("<tr><td>"+gt.getTroopId());
	out.print("</td><td>"+gt.getName());
	out.print(
	        "</td><td>LP: " + gt.getLivePoints(userId) + 
	        "<br>Schilde: " + gt.getShieldPoints(userId)
	        );
	out.print("</td><td>");
	for (int id2 : GroundTroopTable.getAllTypeIds())
		if (id2 != id)
		{
			GroundTroop gt2 = GroundTroopTable.getTroopById(id2);
			out.print(
			        "gegen " + gt2.getName() + " : " 
			        + gt.getAttackPowerAgainst(id2,userId) + "<br>"
			        );
		}
	out.println("</td></tr>");
	}
%>
</table><br>
<% }  else if ("3".equalsIgnoreCase(""+request.getParameter("part"))){
%>
<h1>Verlusttabelle</h1>
<a name="kampftab"></a>
Es k&auml;mpfen jeweils 100 Einheiten der einen Sorte gegen 100 der anderen<br>
<%
	int userId1 = 8;
	if (request.getParameter("userId1") != null) {
		userId1 = Integer.parseInt(request.getParameter("userId1"));
	}
	int userId2 = 7;
	if (request.getParameter("userId2") != null) {
		userId2 = Integer.parseInt(request.getParameter("userId2"));
	}
%>
<form action="Bodentruppen%20test.jsp" method="post">
<input type="hidden" name="part" value="3" />
Für den Spieler <%= userId1 %> "<% 
	UserBuffer.loadUserBuffer();
	UserData user1 = UserBuffer.getUserData(userId1);
	out.print(user1.getUserName());
%>" <br>
Select another user 
<select name="userId1">
<%
/*
	for (UserData d : UserBuffer.getAllUsers())
	{
		out.println(
				"<option value=\"" + d.getUserId() + "\" "
				+ ((d.getUserId() == userId1) ? "selected" : "") + " > " 
				+ d.getUserName()
				+ " </option>"
				);
	} */
%>
</select><br>
Gegen <%= userId2 %> "<% 
	UserData user2 = UserBuffer.getUserData(userId2);
	out.print(user2.getUserName());
%>" <br>
Select another user 
<input type="hidden" name="part" value="2" />
<select name="userId2">
<%
/*
	for (UserData d : UserBuffer.getAllUsers())
	{
		out.println(
				"<option value=\"" + d.getUserId() + "\" " 
				+ ((d.getUserId() == userId2) ? "selected" : "") + " > " 
				+ d.getUserName()
				+ " </option>"
				);
	} */
%></select><br>
<input type="submit" />
</form>
<%
	out.flush();
	PlanetData pd = TestHelper.findPlanetWithoutUnits();
	
%>
<table border="1">
<tr><th>Der eine: \ Der Andere</th> <% 

for (int id : GroundTroopTable.getAllTypeIds()) {
	GroundTroop gt = GroundTroopTable.getTroopById(id);
	out.println("<th>" + gt.getName() + "</th> ");
}
%></tr>
<%

for (int id_1 : GroundTroopTable.getAllTypeIds())
{ 
	GroundTroop gt_1 = GroundTroopTable.getTroopById(id_1);
	out.print("<tr><th>"+gt_1.getName()+"</th>");
	for (int id_2 : GroundTroopTable.getAllTypeIds())
	{ 
		try {
			Statement stmt = DbConnect.createStatement();
			stmt.execute(
					"INSERT INTO playertroops SET planetId=" + pd.getPlanetId()
					+ ", userId=" + user1.getUserId() + ", troopId=" + id_1 
					+ ", number=100"
					);
			stmt.execute(
					"INSERT INTO playertroops SET planetId=" + pd.getPlanetId()
					+ ", userId=" + user2.getUserId() + ", troopId=" + id_2
					+ ", number=100"
					);
			stmt.close();
			
			GroundFight gf = new GroundFight(
					pd, UserBuffer.getUserData(pd.getUserId()),
					Arrays.asList(
					        new UserData[] {user1}), 
					        Arrays.asList(new UserData[] {user2})
					);
		
			gf.setDryRun(true);
		
			gf.fightOneRound(0);
		
			long remaining = 0;
		
			if (!gf.getAllDefendingTroops().isEmpty()) {
				GroundTroopSize troop = gf.getAllDefendingTroops().get(0);
				remaining = troop.getCount();
			}
		
			out.print("<td>"+(100 - remaining)+"</td>");
		} finally {
			try {
				Statement stmt = DbConnect.createStatement();
				stmt.execute(
				        "DELETE FROM playertroops WHERE planetId="
				        + pd.getPlanetId() + " AND userId=" + user2.getUserId()
				        );
				stmt.execute(
				        "DELETE FROM playertroops WHERE planetId="
				        + pd.getPlanetId() + " AND userId=" + user1.getUserId()
				        );
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	out.println("</tr>");
}
%>
</table>
<% }  else if ("4".equalsIgnoreCase(""+request.getParameter("part"))){
%>
<h1>Kampfsimulator für zwei Spieler</h1>
<a name="totalFight"></a>
Eine frei definierte Armee des einen k&auml;mpft gegen eine frei definierte des 
Anderen:<br>
<%
	int userId1 = 8;
	if (request.getParameter("userId1") != null) {
		userId1 = Integer.parseInt(request.getParameter("userId1"));
	}
	int userId2 = 7;
	if (request.getParameter("userId2") != null) {
		userId2 = Integer.parseInt(request.getParameter("userId2"));
	}
%>
<form action="Bodentruppen%20test.jsp" method="post">
<input type="hidden" name="part" value="4" />
Für den Spieler <%= userId1 %> "<% 
	UserBuffer.loadUserBuffer();
	UserData user1 = UserBuffer.getUserData(userId1);
	out.print(user1.getUserName());
%>" <br>
Select another user 
<select name="userId1">
<%
/*
	for (UserData d : UserBuffer.getAllUsers())
	{
		out.println(
		        "<option value=\"" + d.getUserId() + "\" "
		        + ((d.getUserId() == userId1) ? "selected" : "") + " > "
		        + d.getUserName() + " </option>"
		        );
	} */
%></select><br>
Gegen <%= userId2 %> "<% 
	UserData user2 = UserBuffer.getUserData(userId2);
	out.print(user2.getUserName());
%>" <br>
Select another user 
<input type="hidden" name="part" value="2" />
<select name="userId2">
<%
/*
	for (UserData d : UserBuffer.getAllUsers())
	{
		out.println(
		        "<option value=\"" + d.getUserId() + "\" " 
		        + ((d.getUserId() == userId2) ? "selected" : "") + " > " 
		        + d.getUserName() + " </option>"
		        );
	}*/
%></select><br>
<%
	PlanetData pd = TestHelper.findPlanetWithoutUnits();
	Statement stmt = DbConnect.createStatement();
%>
<table border="1">
    <tr>
        <th></th>
        <th><%=user1.getUserName() %></th>
        <th><%=user2.getUserName() %></th>
    </tr>
<% 

	for (int id : GroundTroopTable.getAllTypeIds()) {
		GroundTroop gt = GroundTroopTable.getTroopById(id);
		out.print(
		        "<tr><th>" + gt.getName() + "</th>"+ 
		        "<td> <input type=\"text\" name=\"units1_" + id + "\" value=\""
		        );
		if (request.getParameter("units1_"+id) != null) {
            if (Integer.parseInt(request.getParameter("units1_"+id)) > 0) {
                stmt.execute(
                    "INSERT INTO playertroops SET planetId="
				    + pd.getPlanetId() + ", userId=" + user1.getUserId() 
				    + ", troopId=" + id + ", number=" 
				    + Integer.parseInt(request.getParameter("units1_"+id))
				    );
            }
			out.print(Integer.parseInt(request.getParameter("units1_"+id)));
		} else {
			out.print("0");
		}
		out.print(
		        "\" /></td><td><input type=\"text\" name=\"units2_" 
		        + id + "\" value=\""
		        );
		if (request.getParameter("units2_"+id) != null) {
            if (Integer.parseInt(request.getParameter("units2_"+id)) > 0) {
                stmt.execute(
			        "INSERT INTO playertroops SET planetId="+
			        pd.getPlanetId() + ", userId=" + user2.getUserId() 
				    + ", troopId=" + id + ", number=" 
				    + Integer.parseInt(request.getParameter("units2_"+id))
				    );
            }
			out.print(Integer.parseInt(request.getParameter("units2_"+id)));
		} else {
			out.print("0");
		}
		out.println("\" /></td></tr>");
	}
%>
</table>
<input type="submit" />
</form>
<h2>&Uuml;berlebende je Runde:</h2>
<table border="1">
<tr><th><%=user1.getUserName() %></th><th><%=user2.getUserName() %></th></tr>
<tr><th>Verteidiger</th><th>Angreifer</th></tr>
<%
	GroundFight gf = new GroundFight(
		pd, UserBuffer.getUserData(pd.getUserId()),
		Arrays.asList(user1), Arrays.asList(user2)
		);

	gf.setDryRun(false);

	int countDown = 1000;
	while (
	        !gf.getAllDefendingTroops().isEmpty() 
			&& !gf.getAllAttackingTroops().isEmpty()
			&& (countDown > 0)
			) {
        %><tr><td>
        <%
        for (GroundTroopSize troop : gf.getAllDefendingTroops()) {
            out.print(
                    troop.getCount() + " " + troop.getTroop().getName()+ "<br>"
                    );
        }
        %></td><td>
        <%
        for (GroundTroopSize troop : gf.getAllAttackingTroops()) {
            out.print(
                    troop.getCount() + " " + troop.getTroop().getName() + "<br>"
                    );
        }
        %></td></tr>
        <%
		gf.fightOneRound(0);
		countDown --;
	}
%>
</table>
<h2>&Uuml;berlebende:</h2>
<%	boolean player1Lost = gf.getAllDefendingTroops().isEmpty();
	if (player1Lost) {
		%> <b> Spieler <%=user1.getUserName() %> hat verloren! </b><br><%
		for (GroundTroopSize troop : gf.getAllAttackingTroops()) {
			out.print(
			        troop.getCount() + " " + troop.getTroop().getName() 
			        + " haben überlebt<br>"
			        );
		}
	} else {
		%> <b> Spieler <%=user2.getUserName() %> hat verloren! </b><br><%
		for (GroundTroopSize troop : gf.getAllDefendingTroops()) {
			out.print(
			        troop.getCount() + " " + troop.getTroop().getName() 
			        + " haben überlebt<br>"
			        );
		}
	}
	stmt.execute("DELETE FROM playertroops WHERE planetId=" + pd.getPlanetId());
	stmt.close();
}
%>
</body>
</html>