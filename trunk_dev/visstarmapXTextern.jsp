<%@page import="at.viswars.model.UserSettings"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.service.ProfileService"%>
<%@page import="at.viswars.model.User"%>
<%@page import="at.viswars.model.UserData"%>
<%
// Determine correct connection path
StringBuffer xmlUrlBuffer = request.getRequestURL();
String xmlUrl = "";
int slashCounter = 0;
int userId = Integer.parseInt((String) session.getAttribute("userId"));

    UserSettings us = Service.userSettingsDAO.findByUserId(userId);
for (int i=0;i<xmlUrlBuffer.length();i++) {
    char currChar = xmlUrlBuffer.charAt(i);
    if (currChar == ("/".toCharArray())[0]) slashCounter++;
    
    xmlUrl += currChar;
    if (slashCounter == 4) break;
}
%>
<html>
<title>
</title>
<body bgColor="BLACK">
<CENTER>
            <applet id="starmap" code="uebersichtskarte.AppletMain" width="<%= us.getStarmapWidth()%>" height="<%= us.getStarmapHeight()%>" alt="" archive="applet/Sternenkarte.jar">
                <param name="source" value="<%= xmlUrl %>createStarMapInfoXT.jsp"/>
                <PARAM NAME="cache_option" VALUE="NO">

            </applet>
</CENTER>

</body>
<script language="javascript" type="text/javascript">
var disableHTMLScroll = false;

    document.getElementById("starmap").addEventListener('click',
    function(e){
        disableHTMLScroll = true;
    }
    , false);

    document.getElementById("starmap").addEventListener('mouseover',
    function(e){
        disableHTMLScroll = true;
    }
    , false);

    document.getElementById("starmap").addEventListener('mouseout',
    function(e){
        disableHTMLScroll = false;
    }
    , false);

document.addEventListener('DOMMouseScroll', function(e){
    if (disableHTMLScroll) {
        e.stopPropagation();
        e.preventDefault();
        e.cancelBubble = false;
        return false;
    } else {
        return true;
    }
}, false);
</script>
</html>
