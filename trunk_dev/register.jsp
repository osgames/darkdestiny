<%@page import="at.viswars.result.BaseResult"%>
<%@page import="at.viswars.result.RegisterResult"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.utilities.MD5"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.service.LoginService"%>
<%@page import="java.sql.*" %>
<%@page import="java.util.Locale" %>
<%@page import="at.viswars.database.access.DbConnect" %>
<%@page import="at.viswars.enumeration.*" %>
<%@page import="at.viswars.service.*" %>
<HTML>
    <HEAD>
        <TITLE>Dark Destiny - Register Account</TITLE>
        <style type="text/css">

            /*  ... Style-Sheet-Angaben ... */
            a:link { color:#C0C0C0; font-weight:bold }
            a:visited { color:#C0C0C0; font-weight:bold }
            a:active { color:#C0C0C0; font-weight:bold }
            a:hover { color:#ffffff; font-weight:bold; }
            a { text-decoration:none; }
        </style>
    </HEAD>
    <body text="#FFFF00" BGCOLOR="#000000" background="<%=GameConfig.picPath()%>pic/sh.jpg">
        <TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
            <TR>
                <TD width="25%" align="left"><IMG border=0 src="<%=GameConfig.picPath()%>pic/galaxy.gif"></TD>
                <TD width="50%" valign="top" align="center"><IMG border=0 src="<%=GameConfig.picPath()%>pic/darkdestiny.gif"></TD>
                <TD width="25%"></TD>
            </TR>
            <TR>
                <TD></TD>
                <TD width="*" align="center">
                    <%

                        session = request.getSession();
                        Class.forName("com.mysql.jdbc.Driver");
                        Locale locale = request.getLocale();
                        if (session.getAttribute("Language") != null) {
                            locale = (Locale) session.getAttribute("Language");
                        }
                        try {
                            String userName = request.getParameter("user");
                            String passwort1 = request.getParameter("pass1");
                            String passwort2 = request.getParameter("pass2");
                            String gameName = request.getParameter("gamename");
                            EUserGender gender = EUserGender.valueOf(request.getParameter("gender"));
                            String email = request.getParameter("email");
                            Integer language = Integer.parseInt(request.getParameter("language"));

                            request.setAttribute("languageId", language);

                            if (language == 0) {
                                session.setAttribute("Language", Locale.US);
                            } else if (language == 1) {
                                session.setAttribute("Language", Locale.GERMANY);
                            }
                            int ErrorValue = 0;

                            if (!passwort1.equals(passwort2)) {
                                ErrorValue = 2;
                            }

                            if ((userName.length() == 0) | (passwort1.length() == 0) | (passwort2.length() == 0) | (gameName.length() == 0) | (email.length() == 0)) {
                                ErrorValue = 1;
                            }

                            if (ErrorValue != 0) {
                    %>
                    <jsp:forward page="createacc.jsp">
                        <jsp:param name="neu" value='<%= ErrorValue%>' />
                        <jsp:param name="user" value='<%= userName%>' />
                        <jsp:param name="pass1" value='<%= passwort1%>' />
                        <jsp:param name="pass2" value='<%= passwort2%>' />
                        <jsp:param name="gamename" value='<%= gameName%>' />
                        <jsp:param name="gender" value='<%= gender%>' />
                        <jsp:param name="email" value='<%= email%>' />
                        <jsp:param name="language" value='<%= language%>' />
                    </jsp:forward>
                    <%                        }

                        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Starting User creation routine!");
                        GameUtilities gu = new GameUtilities();

                        User user = new User();
                        RegisterResult rr = LoginService.checkUserForCreation(request.getRemoteAddr(), userName, gameName, email, passwort1, gu.getCurrentTick(), request.getLocale(), false);
                        if (rr.isAbleToRegister()) {

                            user.setUserName(userName);
                            user.setGender(gender);
                            user.setPassword(MD5.encryptPassword(passwort1));
                            user.setGameName(gameName);
                            user.setLastUpdate(gu.getCurrentTick());
                            user.setIp(request.getRemoteAddr());
                            user.setEmail(email.trim());
                            user.setSystemAssigned(false);
                            user.setActive(true);
                            user.setAdmin(false);
                            user.setTrial(true);
                            user.setBetaAdmin(false);
                            user.setLocked(0);
                            user.setActivationCode("");
                            user.setDeleteDate(0);
                            user.setMulti(false);
                            user.setAcceptedUserAgreement(true);
                            Language l = Service.languageDAO.findById(language);
                            user.setLocale(l.getLanguage() + "_" + l.getCountry());
                            user.setJoinDate(java.lang.System.currentTimeMillis());
                            // user.setNewsletter(true);
                            // user.setUserType(EUserType.HUMAN);

                            user = LoginService.saveUser(user);

                            session.setAttribute("userId", userName);
                            session.setAttribute("password", passwort1);

                            LoginTracker loginTracker = new LoginTracker();
                            loginTracker.setUserId(user.getUserId());
                            loginTracker.setTime(java.lang.System.currentTimeMillis());
                            loginTracker.setLoginIP(request.getRemoteAddr());

                            LoginService.saveLoginTracker(loginTracker);
                            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Usertabellen Eintrag erzeugt f�r " + user.getUserName() + "!");

                            if (user.getUserId() > 0) {
                    %>

                    <jsp:forward page="enter.jsp">
                        <jsp:param name="user" value='<%= userName%>' />
                        <jsp:param name="password" value='<%= passwort1%>' />
                    </jsp:forward>
                    <%
                            out.println(ML.getMLStr("login_msg_usercreated", locale));

                            out.println("<br><br><a href='./login.jsp'>" + ML.getMLStr("login_link_continuetologin", locale) + " </a>");
                        } else {
                            out.println(ML.getMLStr("login_err_couldntcreateuser", locale));
                            out.println("<br><br><a href='./login.jsp'>" + ML.getMLStr("login_link_continuetologin", locale) + " </a>");
                        }

                    } else {
                        for (BaseResult br : rr.getResults()) {
                    %>
            <CENTER><%= br.getFormattedString()%><BR></CENTER><BR>
                <%
                    }
                %>

            <a href='./createacc.jsp'>Zur&uuml;ck zur Registrierung</a>
            <%

                }

            } catch (Exception e) {
            %>
            <%= e%>
            <%
                }

            %>
        </TD>
        <TD align="right"><img border=0 src="<%=GameConfig.picPath()%>pic/plan2.gif" width="141" height="280"></TD>
    </TR>
</TABLE>
</BODY>
</HTML>