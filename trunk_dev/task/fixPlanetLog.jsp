

<%@page import="javax.swing.text.View"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.dao.*"%>
<%@page import="at.viswars.*"%>
<%@page import="java.util.*"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.utilities.*"%>
<%@page import="at.viswars.scanner.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%

            HashMap<Integer, HashMap<Integer, ArrayList<PlanetLog>>> logs = new HashMap<Integer, HashMap<Integer, ArrayList<PlanetLog>>>();

            for (PlanetLog pl : (ArrayList<PlanetLog>) Service.planetLogDAO.findAll()) {

                HashMap<Integer, ArrayList<PlanetLog>> pls = logs.get(pl.getPlanetId());
                if (pls == null) {
                    pls = new HashMap<Integer, ArrayList<PlanetLog>>();
                }
                ArrayList<PlanetLog> ps = pls.get(pl.getTime());
                if (ps == null) {
                    ps = new ArrayList<PlanetLog>();
                }
                ps.add(pl);
                pls.put(pl.getTime(), ps);
                logs.put(pl.getPlanetId(), pls);
            }

            for (Map.Entry<Integer, HashMap<Integer, ArrayList<PlanetLog>>> entry1 : logs.entrySet()) {
                for (Map.Entry<Integer, ArrayList<PlanetLog>> entry2 : entry1.getValue().entrySet()) {

                    if(entry2.getValue().size() > 1){
                        Logger.getLogger().write("Planet : " + entry1.getKey() + " has more than 2 PlanetLogs at time : " + entry2.getKey());
                        PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(entry1.getKey());
                        for(PlanetLog pl : entry2.getValue()){
                            if(pp == null){
                                continue;
                                }
                            if(pl.getUserId() != pp.getUserId()){
                                Logger.getLogger().write("Removing for user " + pl.getUserId());
                                Service.planetLogDAO.remove(pl);
                                }
                            }
                        }

                }
            }
%>