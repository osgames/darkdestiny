<%--
    Document   : converter
    Created on : 17.04.2013, 06:46:36
    Author     : LAZYPAD
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
        <%


            String date = "";
            long l = 0;
            if (request.getParameter("longdate") != null) {
                l = Long.parseLong(request.getParameter("longdate"));
            }
            Date converted = new Date(l);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            date = sdf.format(converted);
        %>
    </head>
    <body>
        <form action="converter.jsp" method="post">
            <table>
                <tr>
                    <td><input name="longdate"></td>
                </tr>
                <tr>
                    <td><%= date%></td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="konvertieren"/>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
