<%@page import="at.viswars.CheckForUpdate"%>
<%@page import="at.viswars.utilities.MD5"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.enumeration.EPassword"%>
<%@page import="at.viswars.statistic.*"%>
<%@page import="at.viswars.DebugBuffer"%>
<%@page import="at.viswars.Logger.*"%>
<%@page import="at.viswars.DebugBuffer.DebugLevel"%>
<%
            DebugBuffer.addLine(DebugLevel.WARNING, "Statistic jsp got called from: " + request.getRemoteAddr());

            try {                                
                
        String pass = request.getParameter("password");
        
        if (pass.equals(MD5.encryptPassword(Service.passwordDAO.findByType(EPassword.TASK_CALCSTATISTICS).getValue()))) {
                    CheckForUpdate.requestUpdate();
               // String pass = request.getParameter("pass");
               // if (pass.equals("cs")) {
                    Logger.getLogger().write(Logger.LogLevel.INFO, "Calculation of statistic started");
                    
                    UserStatisticsUpdater updater = new UserStatisticsUpdater();                    
                    updater.setName("Statistic Update");
                    updater.start();

                    /* Interne Statistikberechnung */
                    CalculateImperialOverview test = new CalculateImperialOverview();
                    test.start();
                    test.setName("Imperial Statistic Update");
                    out.println("Imperial Statistic Calculation done");
                    
                    Logger.getLogger().write(Logger.LogLevel.INFO, "Calculation of statistic ended");     
        }else{
            
                    Logger.getLogger().write(Logger.LogLevel.INFO, "Password for statisic caluclation was wrong : " + pass);
        }                                           
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in statistic: ", e);
                e.printStackTrace();
            }
%>
