<%--
    Document   : createDB
    Created on : 02.05.2011, 19:02:40
    Author     : Stefan
--%>

<%@page import="java.lang.reflect.ParameterizedType"%>
<%@page import="at.viswars.dao.DAOFactory"%>
<%@page import="com.google.common.base.Strings"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.viswars.database.framework.utilities.*"%>
<%@page import="at.viswars.model.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Autocreate database</title>
    </head>
    <body>
        <%
            Class[] classes = CreateFromModel.getClasses("at.viswars.dao");
            for (Class c : classes) {
                // Generate
                String sql = CreateFromModel.createInsertStatementsFromDBForDAOClass(c);
                if (!Strings.nullToEmpty(sql).equals("")) {
                    out.write("/* Found class: " + c.getName() + "<BR>");
                    out.write("Generated SQL: */ <BR><BR>");
                    out.write(CreateFromModel.createInsertStatementsFromDBForDAOClass(c));

                    out.write("<BR>/* --------------------------------------------- */<BR>");
                }
            }
        %>
        <h1>Created database</h1>
    </body>
</html>
