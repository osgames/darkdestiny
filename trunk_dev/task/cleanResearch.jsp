<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.database.access.DbConnect" %>
<%
Statement stmt = DbConnect.createStatement();
ResultSet rs = stmt.executeQuery("SELECT * FROM playerresearch ORDER BY userId, researchId");

HashMap<Integer,LinkedList<Integer>> buffer = new HashMap<Integer,LinkedList<Integer>>();

int totalEntriesBefore = 0;
int totalEntriesAfter = 0;

while (rs.next()) {
    totalEntriesBefore++;
    if (buffer.containsKey(rs.getInt(2))) {
        if (!buffer.get(rs.getInt(2)).contains(rs.getInt(3))) {
            buffer.get(rs.getInt(2)).add(rs.getInt(3));
        }
    } else {
        LinkedList<Integer> res = new LinkedList<Integer>();
        res.add(rs.getInt(3));
        buffer.put(rs.getInt(2),res);
    }
}

out.write("Data added for " + buffer.size() + " players ("+totalEntriesBefore+" entries)<BR>");
stmt.execute("DELETE FROM playerresearch");

for (int user : buffer.keySet()) {
    out.write("Adding user " + user + "<BR>");
    LinkedList<Integer> tmpRes = buffer.get(user);
    
    for (int resId : tmpRes) {
        totalEntriesAfter++;
        stmt.execute("INSERT INTO playerresearch (userId,researchId) VALUES ("+user+","+resId+")");
    }
}

out.write("Total entries after "+ totalEntriesAfter);
%>