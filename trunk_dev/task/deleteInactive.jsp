<%@page import="at.viswars.Logger.Logger"%>
<%@page import="at.viswars.enumeration.EPassword"%>
<%@page import="at.viswars.utilities.MD5"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.DebugBuffer.DebugLevel"%>
<%@page import="java.sql.*"%>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.utilities.DestructionUtilities" %>
<%@page import="at.viswars.service.MainService" %>
<%@page import="at.viswars.model.User" %>
<%@page import="at.viswars.model.UserData" %>
<%@page import="java.util.*" %>
<%

    try {

        String pass = request.getParameter("password");
        
        if (pass.equals(MD5.encryptPassword(Service.passwordDAO.findByType(EPassword.TASK_DELETEUSER).getValue()))) {
            CheckForUpdate.requestUpdate();
            boolean foundUserToDelete = false;
            ArrayList<User> toDelete = new ArrayList<User>();
            for (User u : (ArrayList<User>) Service.userDAO.findAll()) {
                if(u.getUserId() == 0){
                    continue;
                }
                if (u.getDeleteDate() != null && u.getDeleteDate() > 0) {
                    foundUserToDelete = true;
                    toDelete.add(u);
                }
            }
            if (foundUserToDelete) {
                for (User u : toDelete) {
                    DebugBuffer.addLine(DebugLevel.WARNING, "User " + u.getGameName() + " id: " + u.getUserId() + " deletionDate : " + u.getDeleteDate() + " acutalDate : " + GameUtilities.getCurrentTick2() + " (actualDate - DeletaionDate) > deletionDelay?: " + (GameUtilities.getCurrentTick2() - u.getDeleteDate()) + " > " + GameConstants.DELETION_DELAY);
                    if (GameUtilities.getCurrentTick2() - u.getDeleteDate() > GameConstants.DELETION_DELAY) {
                        DebugBuffer.addLine(DebugLevel.WARNING, "Deleting User");
                        DestructionUtilities.destroyPlayer(u.getUserId());
                    } else {
                        DebugBuffer.addLine(DebugLevel.WARNING, (GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - u.getDeleteDate()))+ " Ticks till deletion");
                    }
                }
            }
        }else{
            
                    Logger.getLogger().write(Logger.LogLevel.INFO, "Delete Inactive password was wrong : " + pass);
        }
    } catch (Exception e) {
        DebugBuffer.addLine(DebugLevel.ERROR, "Error in deleteInactive.jsp : " + e);
        DebugBuffer.writeStackTrace("DeleteInactive error", e);
    }
%>