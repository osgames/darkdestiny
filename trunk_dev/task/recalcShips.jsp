<%@page import="at.viswars.service.ShipDesignService"%>
<%@ page import="at.viswars.*" %>
<%@ page import="at.viswars.database.access.*" %>
<%@ page import="java.sql.*" %>

<%
Statement stmt = DbConnect.createStatement();
ResultSet rs = stmt.executeQuery("SELECT id FROM shipdesigns");

while (rs.next()) {
    ShipDesignService.updateDesignCosts(rs.getInt(1));

    out.println("Recalculated " + rs.getInt(1));
}

rs.close();
stmt.close();
%>