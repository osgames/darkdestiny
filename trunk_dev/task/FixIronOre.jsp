<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.database.access.*" %>

<%
    Logger.getLogger().write("START PROCESSING");

            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT p.id, p.diameter, p.orbitlevel, pr.qty FROM planet p LEFT OUTER JOIN planetressources pr ON pr.planetId=p.id AND type=0 AND ressId=11 WHERE (landType<>'A' AND landType<>'B')");
            while (rs.next()) {
                    long iron = rs.getLong(4);

                    long volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (rs.getInt(2) / 2), 3)));
                    long ironNew = 0l;         

                    int orbitLevelDiff = 6-rs.getInt(3);
                    if (orbitLevelDiff > 0) {
                        ironNew = (long)(((float)volume / (float)1000) * (1f + ((float)orbitLevelDiff / (float)2.5)));
                    } else if (orbitLevelDiff < 0) {
                        ironNew = (long)(((float)volume / (float)1000) / (1f + ((float)Math.abs(orbitLevelDiff) / (float)2.5)));
                    } else {
                        ironNew = (long)((float)volume / (float)1000);
                    }                                           

                    double diff = (double)iron / (double)ironNew;
                    out.write("DIFF: " + diff + "<BR>");
                    
                    if ((diff < 0.7d) || (diff > 1.3d)) {
                        if (iron == 0) {
                            Statement stmt2 = DbConnect.createStatement();
                            stmt2.execute("INSERT INTO planetressources (planetId, type, ressId, qty) VALUES ("+rs.getInt(1)+","+0+","+11+","+ironNew+")");                    
                            stmt2.close();                            
                            
                            out.write("CREATE IRON ORE FOR PLANET " + rs.getInt(1) + " FROM " + iron + " TO " + ironNew + "<BR>");
                        } else {
                            Statement stmt2 = DbConnect.createStatement();
                            stmt2.executeUpdate("UPDATE planetressources SET qty=" + ironNew + " WHERE planetId=" + rs.getInt(1) + " AND type=" + 0 + " AND ressId=" + 11);                    
                            stmt2.close();
                            
                            out.write("UPDATED IRON FOR PLANET " + rs.getInt(1) + " FROM " + iron + " TO " + ironNew + "<BR>");
                        }                        
                    } 
           }
%>
