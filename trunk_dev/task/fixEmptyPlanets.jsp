<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.database.access.*" %>

<%
int planetCount = PlanetTable.getTotalCount();
ProdConsBuffer pcb = new ProdConsBuffer();

for (int pId=0;pId<planetCount;pId++) {    
    PlanetData pd = new PlanetData(PlanetTable.getPlanetId(pId));
    
    if (pd == null) {
        out.write("ERROR FOR INDEX " + pId);
        continue;
    }
    
    HashMap<Integer,Integer> consOnPlanet = pcb.getConsForPlanet(pd.getPlanetId());
    if (consOnPlanet.isEmpty()) {
        out.write("Fixing planet " + pd.getPlanetId() + "<BR>");
        
        Statement stmt = DbConnect.createStatement();
        
        if (pd.getPlanetClass().equalsIgnoreCase("M") ||
                pd.getPlanetClass().equalsIgnoreCase("C") ||
                pd.getPlanetClass().equalsIgnoreCase("G")) {
            // Insert default buildings for habitable
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",2,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",25,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",14,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",22,1)");
        } else if (pd.getPlanetClass().equalsIgnoreCase("A") ||
                pd.getPlanetClass().equalsIgnoreCase("B")) {
            // Insert default buildings for orbital
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",34,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",37,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",36,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",38,1)");         
        } else {
            // Insert default buildings for not habitable
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",2,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",22,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",12,1)");
            stmt.execute("INSERT INTO planetconstruction (planetID,constructionID,number) VALUES ("+pd.getPlanetId()+",40,1)");           
        }
        
        pd.setMoral(100);
        pd.setPopulation(20000);
    }
}
%>
