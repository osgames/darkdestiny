<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.database.access.*" %>
<%
Statement stmt = DbConnect.createStatement();
ResultSet rs = stmt.executeQuery("SELECT id FROM system WHERE posx=999 AND posy=999 AND id<>2001");

while (rs.next()) {
    int userId = 0;
    
    Statement stmt3 = DbConnect.createStatement();
    ResultSet rs3 = stmt3.executeQuery("SELECT pp.userID FROM planet p LEFT OUTER JOIN playerplanet pp ON pp.planetID=p.id WHERE p.systemId="+rs.getInt(1)+" AND pp.homesystem=1");
    if (rs3.next()) {
        userId = rs3.getInt(1);
    } else {
        Logger.getLogger().write("<BR><BR>Skipping " + rs.getInt(1) + "!!!!<BR><BR>");
        continue;
    }
    
    int minX;
    int maxX;
    int minY;
    int maxY;
    int middleX = 0;
    int middleY = 0;
    
    PlanetTable.initialize();
    SystemTable.initialize();
    
// Determine dimensions of Universe
    Statement stmt2 = DbConnect.createStatement();
    
    try {
        ResultSet rs2 = stmt2.executeQuery("SELECT MAX(id), MIN(posx), MIN(posy), MAX(posx), MAX(posy) FROM system");
        if (rs2.next()) {
            minX = rs2.getInt(2);
            minY = rs2.getInt(3);
            maxX = rs2.getInt(4);
            maxY = rs2.getInt(5);
            middleX = (int)(((float)maxX - (float)minX) / 2f);
            middleY = (int)(((float)maxY - (float)minY) / 2f);
        }
    } catch (Exception e) {
        DebugBuffer.writeStackTrace("Error while initialising of getStartPosition",e);
    }
    
// Build blocking Area
// - at least 130 Lightyears away from next planet with more than 10 Mio. inhabitants
// - at least 10 Lightyears away from next system
    StartingArea.refreshStaticData();
    StartingArea.refresh();
    
// Check blocking area
    int xSpan = 1; int currXMove = 1; boolean xRight = true;
    int ySpan = 1; int currYMove = 1; boolean yUp = true;
    int currXPos = middleX;
    int currYPos = middleY;
    boolean processingX = true;
    
    boolean blocked = StartingArea.isBlocked(currXPos,currYPos);
    
// expand rectangle till partly outside of blocked area
// to speed up searching of starting position
    if (blocked) {
        int leftX = middleX;
        int rightX = middleX+1;
        int topY = middleY;
        int bottomY = middleY+1;
        
// Fast Scan
        int xScanStep = 20;
        int yScanStep = 20;
        
        while (StartingArea.isBlockedRect(leftX,topY,rightX,bottomY)) {
            leftX-=xScanStep;
            rightX+=xScanStep;
            topY-=yScanStep;
            bottomY+=yScanStep;
        }
        
        leftX+= xScanStep;
        rightX-=xScanStep;
        topY+=yScanStep;
        bottomY-=yScanStep;
        
// Medium Scan
        xScanStep = 5;
        yScanStep = 5;
        
        while (StartingArea.isBlockedRect(leftX,topY,rightX,bottomY)) {
            leftX-=xScanStep;
            rightX+=xScanStep;
            topY-=yScanStep;
            bottomY+=yScanStep;
        }
        
        leftX+= xScanStep;
        rightX-=xScanStep;
        topY+=yScanStep;
        bottomY-=yScanStep;
        
// Detail Scan
        xScanStep = 1;
        yScanStep = 1;
        
        while (StartingArea.isBlockedRect(leftX,topY,rightX,bottomY)) {
            leftX-=xScanStep;
            rightX+=xScanStep;
            topY-=yScanStep;
            bottomY+=yScanStep;
        }
        
        leftX+= xScanStep;
        rightX-=xScanStep;
        topY+=yScanStep;
        bottomY-=yScanStep;
        
        currXPos = leftX+xScanStep;
        currYPos = bottomY-yScanStep;
        rightX-=xScanStep;
        topY-=yScanStep;
        
        xSpan = rightX-leftX;
        currXMove = xSpan;
        ySpan = bottomY-topY;
        currYMove = ySpan;
    }
    
    while (blocked) {
        if (processingX) {
            if (xRight) {
                currXPos++;
                currXMove--;
                if (currXMove == 0) {
                    xRight = false;
                    xSpan++;
                    currXMove = xSpan;
                    processingX = false;
                }
            } else {
                currXPos--;
                currXMove--;
                if (currXMove == 0) {
                    xRight = true;
                    xSpan++;
                    currXMove = xSpan;
                    processingX = false;
                }
            }
        } else {
            if (yUp) {
                currYPos--;
                currYMove--;
                if (currYMove == 0) {
                    yUp = false;
                    ySpan++;
                    currYMove = ySpan;
                    processingX = true;
                }
            } else {
                currYPos++;
                currYMove--;
                if (currYMove == 0) {
                    yUp = true;
                    ySpan++;
                    currYMove = ySpan;
                    processingX = true;
                }
            }
        }
        
        blocked = StartingArea.isBlocked(currXPos,currYPos);
    }
    
// if found -- create new system at this location with at least one M planet
    out.write("Optimal Position found at " + currXPos + "/" + currYPos + " for user " + userId + " with systemId " + rs.getInt(1) +"<BR>");
    
    out.write("<BR>Starting to clean viewtable"+userId+"<BR>");
    
    stmt2.execute("DELETE FROM viewtable"+userId);
    
    out.write("Repositioning player " + userId + " with system " + rs.getInt(1) +" at "+currXPos+"/"+currYPos);
    
    stmt3.execute("UPDATE system SET posx="+currXPos+", posy="+currYPos+" WHERE id="+rs.getInt(1));
    
    out.write("<BR>Refilling viewtable"+userId);
    
    ResultSet rs2 = stmt2.executeQuery("SELECT id FROM planet WHERE systemId="+rs.getInt(1));
    while (rs2.next()) {
        
        stmt3.execute("INSERT INTO viewtable"+userId+" (systemid, planetid) " +
                      "VALUES ("+rs.getInt(1)+","+rs2.getInt(1)+")");
    }    
    
    stmt3.close();
    
    out.write("<BR><BR>");
    stmt2.close();
    
    out.flush();
}

stmt.close();
%>
