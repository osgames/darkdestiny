<%@page import="at.viswars.Logger.Logger"%>
<%@page import="at.viswars.enumeration.EPassword"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.DebugBuffer.DebugLevel"%>
<%@page import="java.sql.*"%>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.utilities.DestructionUtilities" %>
<%@page import="at.viswars.service.MainService" %>
<%@page import="at.viswars.model.User" %>
<%@page import="at.viswars.model.UserData" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.CheckForUpdate"%>
<%@page import="at.viswars.utilities.MD5"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.enumeration.EPassword"%>
<%
// Scan for users which are in trial status and has been inactive
// for at least 2 days (=288 Ticks)
// IF FOUND DESTROY!!!!!!!!

        String pass = request.getParameter("password");
        
        if (pass.equals(MD5.encryptPassword(Service.passwordDAO.findByType(EPassword.TASK_DELETETRIAL).getValue()))) {
                    CheckForUpdate.requestUpdate();
                    
int currentTick = GameUtilities.getCurrentTick2();
ArrayList<User> users = MainService.getAllUsers();

for (User u : users) {
    if (u.getUserId() == 0) continue;
    
    int inactiveTime = currentTick - u.getLastUpdate();

    // DELETE HARDCORE
    if (!u.getSystemAssigned()) {
        if (inactiveTime > 144) {
            DebugBuffer.addLine(DebugLevel.TRACE, "User " + u.getGameName() + " ["+u.getUserId()+"] was deleted (REASON: no system selected)");
            DestructionUtilities.destroyPlayer(u.getUserId());
        }    
    } else if (u.getJoinDate().equals((long)u.getLastUpdate()) ) {
        UserData ud = MainService.findUserDataByUserId(u.getUserId());
        int holidayLimit = (ud.getLeaveDays() + 7) * 144;

        if (inactiveTime > 288 + holidayLimit) {
            DebugBuffer.addLine(DebugLevel.TRACE, "User " + u.getGameName() + " ["+u.getUserId()+"] was deleted (REASON: system selected but no action taken)");            
            DestructionUtilities.destroyPlayer(u.getUserId());           
        }
    } else {
        UserData ud = MainService.findUserDataByUserId(u.getUserId());
        int holidayLimit = (ud.getLeaveDays() + 7) * 144;
        
        if (inactiveTime > ((144*14) + holidayLimit)) {
            DebugBuffer.addLine(DebugLevel.TRACE, "User " + u.getGameName() + " ["+u.getUserId()+"] was deleted (REASON: 2 weeks inactive and no holiday modus or expired)");
            DestructionUtilities.destroyPlayer(u.getUserId());
        }
    }
}
}else{
            
                    Logger.getLogger().write(Logger.LogLevel.INFO, "Delete Trial password was wrong : " + pass);
        }
%>