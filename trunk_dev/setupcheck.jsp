<%-- 
    Document   : setupcheck
    Created on : 04.01.2012, 14:41:31
    Author     : Stefan
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="at.viswars.GameConfig"%>
<%@page import="at.viswars.result.BaseResult"%>
<%@page import="at.viswars.result.PropertiesCheckResult"%>
<%@page import="at.viswars.setup.ConfigurationResult"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.viswars.service.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body bgcolor="black" style="color: yellow;">
        <script type="text/javascript">
        
            /* Hiermit k�nnen Elemente immer wieder angezeigt werden bzw. verschwinden*/
            function toogleVisibility(name)
            {
                if (document.getElementById(name).style.display == 'none')
                {
                    document.getElementById(name).style.display = '';
                }
                else 
                {
                    document.getElementById(name).style.display = 'none';
                }
            }

        </script>
        <%

            final int PROCESS_GENERATE_DATABASE = 1;
            int process = 0;
            ArrayList<BaseResult> results = new ArrayList<BaseResult>();
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            if (process == PROCESS_GENERATE_DATABASE) {

                results.addAll(SetupService.generateDB());

            }
        %>
        <center><h1>Hello Install-Guy!<BR>
                This is se System Check</h1>
        
          <% for (BaseResult br : results) {%>
        <%= br.getFormattedString()%><BR>
        <% }%>
        </center>

      
        <TABLE align="center" style="border-style: dashed; border-width:1px;" width="400px">
            <TR>
                <TD width="300px">Check</TD>
                <TD wdith="100px">Result</TD>
            </TR>


            <%
                PropertiesCheckResult pcr = SetupService.checkProperties();

                boolean collapsed = true;
                String style = "";
                if (collapsed) {
                    style = "none";
                }
            %>
            <!-- Property files-->
            <TR>
                <TD colspan="2">Property Files Check</TD>
            </TR
            <TR>
                <TD>
                    <a href="setup.jsp?type=1">Generate New Property Files</a>
                </TD>
            </TR>

            <!-- db.properties -->
            <TR>
                <TD>db.properties Check</TD>
                <TD>
                    <% if (pcr.isIsDbPropertiesFileOkay()) {%>
                    <IMG alt="Check" src="pic/checked.png" />
                    <% } else {%>
                    <IMG alt="NotCheck" src="pic/unchecked.png" />
                    <% }%>
                    <span onclick="toogleVisibility('db');">
                        <img src="<%=GameConfig.picPath()%>pic/view.jpg" alt="-" border="0">&nbsp;
                    </span>
                </TD>
            </TR>
            <TR style="display:<%= style%>" id="db">
                <TD style="font-size: 10px;" colspan="2">
                    <% for (BaseResult br : pcr.getDbResult()) {%>
                    <%= br.getFormattedString()%><BR>
                    <% }%>
                </TD>
            </TR>
            <!-- game.properties -->
            <TR>
                <TD>game.properties Check</TD>
                <TD>
                    <% if (pcr.isIsGamePropertiesFileOkay()) {%>
                    <IMG alt="Check" src="pic/checked.png" />
                    <% } else {%>
                    <IMG alt="NotCheck" src="pic/unchecked.png" />
                    <% }%>
                    <span onclick="toogleVisibility('game');">
                        <img src="<%=GameConfig.picPath()%>pic/view.jpg" alt="-" border="0">&nbsp;
                    </span>
                </TD>
            </TR>
            <TR style="display:<%= style%>" id="game">
                <TD style="font-size: 10px;" colspan="2">
                    <% for (BaseResult br : pcr.getGameResult()) {%>
                    <%= br.getFormattedString()%><BR>
                    <% }%>
                </TD>
            </TR>
            <% if (!pcr.isAllPropertiesFilesOkay()) {%>
            <TR>
                <TD colspan="2">
                    <FONT color="red">
                        <B>
                            One or more properties files are missing and
                            the server might not work. Probably the properties files
                            are not existing or not configured properly.

                            <BR><BR>
                            Important: Please note that the Tomcat Server buffers
                            property files. To reload them or delete them from your
                            cache you need to restart your server.
                        </B>
                    </FONT>
                </TD>
            </TR>
            <% }
                ConfigurationResult cr = SetupService.checkServer();
            %>
            <!-- DB Connection Check -->
            <TR>
                <TD>Database Connection Check</TD>
                <TD>
                    <% if (cr.isDbConnectionOk()) {%>
                    <IMG alt="Check" src="pic/checked.png" />
                    <% } else {%>
                    <IMG alt="NotCheck" src="pic/unchecked.png" />
                    <% }%>
                </TD>
            </TR>
            <!-- DB Connection Check | Message -->
            <% if (cr.isDbConnectionOk()) {%>
            <!-- <TR>
                <TD colspan="2" style="color: lightgreen;">INFO: Your database connection is working (no guarantees about the data inside ;) )</TD>
            </TR> -->
            <% } else {%>
            <TR>
                <TD colspan="2" style="color: red;">ERROR: The game could not connect to the database, please check your db.properties file</TD>
            </TR>
            <% }%>
            <!-- Constants Existing Check -->
            <TR>
                <TD>Constants Existing Check</TD>
                <TD>
                    <% if (cr.isConstantsExisting()) {%>
                    <IMG alt="Check" src="pic/checked.png" />
                    <% } else {%>
                    <IMG alt="NotCheck" src="pic/unchecked.png" />
                    <% }%>
                    <% if(!cr.isConstantsExisting()){%>
                     <span onclick="toogleVisibility('constants');">
                        <img src="<%=GameConfig.picPath()%>pic/view.jpg" alt="-" border="0">&nbsp;
                    </span>
                    <% } %>
                </TD>
            </TR>
            <!-- Constants Existing Check | Message -->
            <% if (!cr.isConstantsExisting()) {%>
            <TR>
                <TD colspan="2" style="color: red;">ERROR: The game could not find some important constants. You need a SQL file holding all game constants such as 
                    researches, constructions, groundtroops and so on. We normally deliver a Constants.sql you can either inject yourself or using the link below. Note that
                    mostlikely if you use the actual SVN version the Constants.sql might not fit. Just let us know and we will get you an actual one.
                   
                </TD>
            </TR>
            <TR style="display:<%= style%>" id="constants">
                <TD style="font-size: 10px;" colspan="2">
                    <% for (BaseResult br : cr.getConstantsResult()) {%>
                    <%= br.getFormattedString()%><BR>
                    <% }%>
                </TD>
            </TR>
            
            <TR>                
                <TD>
                    <a href="setup.jsp?type=4">Insert Constants</a>
                </TD>
            </TR>
            <% }%><!-- Table existing Check -->
            <TR>
                <TD>User-Table Existing Check</TD>
                <TD>
                    <% if (cr.isTableExisting()) {%>
                    <IMG alt="Check" src="pic/checked.png" />
                    <% } else {%>
                    <IMG alt="NotCheck" src="pic/unchecked.png" />
                    <% }%>
                </TD>
            </TR>
            <!-- Table existing  Check | Message -->
            <% if (!cr.isTableExisting()) {%>
            <TR>
                <TD colspan="2" style="color: red;">ERROR: The game could not find the "User"-Table in your Database. Please Check if the tables are existing</TD>
            </TR>
            <TR>                
                <TD>
                    <a href="setupcheck.jsp?process=<%= PROCESS_GENERATE_DATABASE %>">Generate new Database</a>
                </TD>
            </TR>
            <% }%>
            <!-- Galaxy existing Check -->
            <TR>
                <TD>Galaxy Existing Check</TD>
                <TD>
                    <% if (cr.isGalaxyExisting()) {%>
                    <IMG alt="Check" src="pic/checked.png" />
                    <% } else {%>
                    <IMG alt="NotCheck" src="pic/unchecked.png" />
                    <% }%>
                </TD>
            </TR>
            <!-- Galaxy existing Check | Message -->
            <% if (!cr.isGalaxyExisting()) {%>
            <TR>
                <TD colspan="2" style="color: red;">There is no galaxy with systems existing yet. Please log into the Admin-Panel via <a href="adminLogin.jsp">adminLogin.jsp</a> and generate a galaxy
                    Note that you need to be flagged as admin in the db-table "user"</TD>
            </TR>
            <% }%>
            <!-- Server status Check -->
            <TR>
                <TD>Server Status Check</TD>
                <TD>
                    <% if (cr.isServerRunning()) {%>
                    <IMG alt="Check" src="pic/checked.png" />
                    <% } else {%>
                    <IMG alt="NotCheck" src="pic/unchecked.png" />
                    <% }%></TD>
            </TR>
            <!-- Server status Check Message -->
            <% if (cr.isServerRunning()) {%>
            <TR>
                <TD>Current State is</TD>
                <TD><%= cr.getServerState().toString()%></TD>
            </TR>
            <TR>
                <TD colspan="2" style="color:lightgreen;">INFO: People should be able to register and login into the game</TD>
            </TR>
            <% } else {%>
            <TR>
                <TD>Current State is</TD>
                <TD><%= cr.getServerState().toString()%></TD>
            </TR>
            <TR>
                <TD colspan="2" style="color:red;">WARNING: As long as the status is not STOPPED and the database connection is working this may be intended.<BR>
                    Reasons for a stopped server may be:<BR>
                    - No entry in table gamedata in database<BR>
                    - Database connection is not working<BR>
                    - Server is set to STOPPED in gamedata database entry<BR><BR>
                    Mode REGISTRATION indicates that users can only register, but not actually join the game. Anyway Administrators are allowed to
                    login into the game anyway.<BR><BR>
                    Please change the status to RUNNING in gamedata table if you want users to be able to join the game and restart the server and call this
                    page again.</TD>
            </TR>
            <% }%>
            <!-- System User Check -->
            <TR>
                <TD>System User Check</TD>
                <TD><% if (cr.isSystemUserOk()) {%><IMG alt="Check" src="pic/checked.png" /><% } else {%><IMG alt="NotCheck" src="pic/unchecked.png" /><% }%></TD>
            </TR>
            <% if (!cr.isSystemUserOk()) {%>
            <TR>
                <TD colspan="2" style="color: red;">ERROR: The game requires a System user for automated ingame messages, this user needs to have the UserType: System, you
                    can setup a System user manually in database table 'user', after creating the user restart the server and call this page again.</TD>
            </TR>
            <% }%>
            <!-- Admin User Check -->
            <TR>
                <TD>Admin User Check</TD>
                <TD><% if (cr.isAdminUserOk()) {%><IMG alt="Check" src="pic/checked.png" /><% } else {%><IMG alt="NotCheck" src="pic/unchecked.png" /><% }%></TD>
            </TR>
            <% if (!cr.isAdminUserOk()) {%>
            <TR>
                <TD colspan="2" style="color: red;">WARNING: I could not find any user with administration rights, Administrators can access files in /admin folder and
                    have also ingame access to admin tools for administrating users and other setup stuff.<BR>
                    You can create an admin user by assigning 1 into the field admin in user table, after changing the value please restart the server and call this page again.</TD>
            </TR>
            <% }%>
        </TABLE>
    </body>
</html>
