function handleMouseWheel(delta) {
	if (delta < 0)
		window.alert("MOUSEWHEEL DOWN");
	else
		window.alert("MOUSEWHEEL UP");
}

function wheelCalculation(event){
	var delta = 0;
	if (!event) event = window.event;
	if (event.wheelDelta) {
		delta = event.wheelDelta/120; 
	} else if (event.detail) {
		delta = -event.detail/3;
	}
	if (delta)
		handleMouseWheel(delta);
        if (event.preventDefault)
                event.preventDefault();
        event.returnValue = false;
}

/* Initialization code. */
if (window.addEventListener)
	window.addEventListener('DOMMouseScroll', wheelCalculation, false);
window.onmousewheel = document.onmousewheel = wheelCalculation;
