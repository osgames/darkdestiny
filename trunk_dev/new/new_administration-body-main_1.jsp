<%@page import="at.viswars.utilities.ShippingUtilities"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="at.viswars.construction.InConstructionData"%>
<%@page import="java.util.*"%>
<%@page import="java.io.IOException"%>
<%@page import="at.viswars.view.header.*"%>
<%@page import="at.viswars.planetcalc.*"%>
<%@page import="at.viswars.result.*"%>
<%@page import="at.viswars.service.AdministrationService"%>
<%@page import="at.viswars.utilities.PopulationUtilities"%>
<%@page import="at.viswars.utilities.ConstructionUtilities"%>
<%@page import="at.viswars.viewbuffer.HeaderBuffer"%>
<%@page import="at.viswars.enumeration.*"%>
<%@page import="at.viswars.result.*"%>
<%@page import="at.viswars.service.*"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.filter.*"%>
<%@page import="at.viswars.model.Note"%>
<%@page import="at.viswars.model.Filter"%>
<%@page import="at.viswars.model.Ressource"%>
<%

    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int actCategory = Integer.parseInt((String) session.getAttribute("actCategory"));

%>
<script  type="text/javascript">

    var language = '<%= ML.getLocale(userId) %>';
    function name(){
        return "administration";
    }
</script>

<%
    int process = 0;
    final int PROCESS_BUILDBUILDING = 1;
    final int PROCESS_CHANGETAXES = 2;
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }

    if (process > 0) {
        if (process == PROCESS_BUILDBUILDING) {
            ConstructionService.buildBuilding(userId, request.getParameterMap());
        }
        if (process == PROCESS_CHANGETAXES) {
            ManagementService.changeTaxes(request.getParameterMap(), userId, Integer.parseInt(request.getParameter("planetId")));
        }
    }


    String basePicPath = GameConfig.picPath();

    HeaderData hData = HeaderBuffer.getUserHeaderData(userId);
    HashMap<Integer, SystemEntity> allActSystems = hData.getCategorys().get(actCategory).getSystems();
    HashMap<Integer, HashMap<Integer, HashMap<Boolean, Long>>> tradeBalances = ShippingUtilities.findTradeBalanceForUserSorted(userId);


// Filter START
    int filterId = 0;
    if (request.getParameter("filterId") != null) {
        filterId = Integer.parseInt(request.getParameter("filterId"));
    }
// Filter ENDE
// Variablen zum verwalten der Seitenanzeige START
    int firstItem = 0;
    int fromItem = 0;
    int toItem = 15;
    int itemsPerPage = 15;

    if (request.getParameter("fromItem") != null) {
        fromItem = Integer.parseInt(request.getParameter("fromItem"));
    }
    if (request.getParameter("toItem") != null) {
        toItem = Integer.parseInt(request.getParameter("toItem"));
    }

    PlanetFilter pf = new PlanetFilter(allActSystems, fromItem, toItem, filterId, userId);
    fromItem = fromItem + 1;
    PlanetFilterResult pfr = pf.getResult();
    int lastItem = pfr.getItems();
    if (toItem > lastItem) {
        toItem = lastItem;
    }
    if (lastItem == 0) {
        fromItem = 0;
    }
// Variablen zum verwalten der Seitenanzeige ENDE

%>
<BR>
<BR>
<TABLE id="planetData" border="0" style="font-size:13px" width="90%"  align="center" cellpadding="0" cellspacing="0">    
    <SCRIPT type="text/javascript">    
        var doJustOnce = true;
    </SCRIPT>
    <%

String basePath = "";
int slashCounter = 0;

StringBuffer requestUrl = request.getRequestURL();

for (int i=0;i<requestUrl.length();i++) {
    char currChar = requestUrl.charAt(i);
    if (currChar == ("/".toCharArray())[0]) slashCounter++;
    
    basePath += currChar;
    if (slashCounter == 4) break;
}


        for (PlanetFilterResultEntry pfre : pfr.getResult()) {

            Planet p = pfre.getP();
            PlayerPlanet pp = pfre.getPp();

%>
   <TR><TD>
            <!-- Planeteneintrag-->
            <TABLE>
                <TR>
                    
                    <!-- Rohstoffe -->
                    <TD>Ressources
                        <TABLE id="<%= p.getId()%>_ressources"></TABLE>
                    </TD>
                    <TD>RawRessources
                        <TABLE style="font-size:10px; height:14px; font-weight:bold;  border-style: solid; border-width: thin;">
                <TBODY id="<%= p.getId()%>_rawressources"></TBODY>
                </TABLE>                   </TD>
    </TR>
    
</TABLE>
                        <script type="text/javascript">
                            buildTables(<%= userId %>, <%= p.getId()%>, planetData);
                            if(doJustOnce){
                            ajaxCall('<%= basePath %>GetJSONManagementData?userId=<%= userId %>&planetId=<%= p.getId() %>',processManagementData);
                            doJustOnce = true;
                            }
                        </script>
</TD></TR>
<%

    }
%>
</TABLE><%

%>
