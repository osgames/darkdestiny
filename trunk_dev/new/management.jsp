<%@page import="at.viswars.enumeration.EPlanetRessourceType"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="java.util.*"%>
<%@page import="at.viswars.construction.InConstructionData"%>
<%@page import="at.viswars.utilities.*"%>
<%@page import="at.viswars.html.*"%>
<%@page import="at.viswars.service.ManagementService"%>
<%@page import="at.viswars.service.OverviewService"%>
<%@page import="at.viswars.service.CategoryService"%>
<%@page import="at.viswars.model.Ressource"%>
<%@page import="at.viswars.model.PlayerPlanet"%>
<%@page import="at.viswars.model.Planet"%>
<%@page import="at.viswars.model.PlanetCategory"%>
<%@page import="at.viswars.model.PlayerCategory"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="at.viswars.planetcalc.*"%>
<%@page import="at.viswars.result.*"%>
<%
            ArrayList<String> results = new ArrayList<String>();

            session.setAttribute("actPage", "new/management");
            session = request.getSession();
            String planetID = (String) session.getAttribute("actPlanet");

            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int systemId = Integer.parseInt((String) session.getAttribute("actSystem"));
            int planetId = Integer.parseInt(planetID);
            int categoryId = 0;
            int mode = 0;
            int process = 0;
            int type = 0;

            final int TYPE_ABADONCOLONY = 1;

            final int PROCESS_CHANGEMINIMUM = 1;
            final int PROCESS_UPDATEPRIORITIES = 2;
            final int PROCESS_CHANGECATEGORY = 3;
            final int PROCESS_CHANGENAMES = 4;
            final int PROCESS_DISMANTLECOLONY = 6;
            final int PROCESS_SETINVISIBLE = 7;
            final int PROCESS_ABADONCOLONY = 8;
            final int PROCESS_CHANGETAXES = 5;
            PlayerPlanet pp = ManagementService.getPlayerPlanetById(planetId);


////////Corruption ---- START



////////Corruption ---- END

            if (pp.getUserId() != userId) {
                throw new Exception("Wo wolltest du hin? Zu fernen Welten? Tut mir leid .. zumindest diese hier geh�rt dir nicht!");
            }
            if (request.getParameter("result") != null) {
                results.add(request.getParameter("result"));
            }

            if (request.getParameter("process") != null) {
                process = Integer.parseInt((String) request.getParameter("process"));
            }

            if (request.getParameter("type") != null) {
                type = Integer.parseInt((String) request.getParameter("type"));
            }

            switch (process) {

                case (PROCESS_CHANGEMINIMUM):
                    ManagementService.changeLimits(request.getParameterMap(), userId, planetId);
                    break;
                case (PROCESS_UPDATEPRIORITIES):
                    ManagementService.updatePriorities(request.getParameterMap(), planetId);

                    break;
                case (PROCESS_CHANGENAMES):
                    BaseResult br = ManagementService.renamePlanSys(request.getParameterMap(), userId, planetId, systemId);
%>
<jsp:forward page="/main.jsp?page=new/management">
    <jsp:param name="process" value="0"/>
    <jsp:param name="result" value="<%= br.getFormattedString() %>"/>
</jsp:forward>
<%
                    break;
                case (PROCESS_CHANGECATEGORY):
                    Map<String, String[]> parMap = request.getParameterMap();

                    for (final Map.Entry<String, String[]> entry : parMap.entrySet()) {
                        String parName = entry.getKey();
                        if (parName.startsWith("category")) {
                            categoryId = Integer.parseInt(parMap.get("category")[0]);
                        }

                    }
                    ManagementService.changeCategoryId(userId, planetId, categoryId);
%>
<jsp:forward page="/main.jsp?page=new/management">
    <jsp:param name="process" value="0"/>
</jsp:forward>
<%
                    break;
                case (PROCESS_DISMANTLECOLONY):
                    ManagementService.dismantleColony(userId, planetId);

                    break;

                case (PROCESS_SETINVISIBLE):
                    ManagementService.setVisible(userId, planetId);
                    break;

                case (PROCESS_ABADONCOLONY):
                    ManagementService.abadonColony(userId, planetId);
                    break;

                case (PROCESS_CHANGETAXES):
                    ManagementService.changeTaxes(request.getParameterMap(), userId, planetId);
                    break;


            }

            PlanetCalculation planetCalc = ManagementService.getCalculatedPlanetValues(planetId);
            PlanetCalcResult pcr = planetCalc.getPlanetCalcData();
            ProductionResult prodRes = pcr.getProductionData();
            ExtPlanetCalcResult epcr = pcr.getExtPlanetCalcData();
            double criminalityValue = epcr.getCriminalityValue();
            double riotValue = epcr.getRiotValue();


//Change in data occured, reload data
            if (process > 0) {
                pp = ManagementService.getPlayerPlanetById(planetId);
            }
            switch (type) {
                //Normal management site
                case (0):
                    String happinessImage = GameConfig.picPath() + "pic/menu/icons/happiness.png";
                    String criminalityImage = GameConfig.picPath() + "pic/menu/icons/crim_equal.png";
                    if (criminalityValue == 0) {
                        criminalityImage = GameConfig.picPath() + "pic/menu/icons/crim_verygood.png";
                    } else if (criminalityValue > 0 && criminalityValue < 25d) {
                        criminalityImage = GameConfig.picPath() + "pic/menu/icons/crim_good.png";
                    } else if (criminalityValue >= 25d && criminalityValue < 50) {
                        criminalityImage = GameConfig.picPath() + "pic/menu/icons/crim_equal.png";
                    } else {
                        criminalityImage = GameConfig.picPath() + "pic/menu/icons/crim_bad.png";
                    }
                    String loyalityImage = GameConfig.picPath() + "pic/menu/icons/loyal_equal.png";
                    if (riotValue <= 0) {
                        loyalityImage = GameConfig.picPath() + "pic/menu/icons/loyal_verygood.png";
                    } else if (riotValue <= 10 && riotValue > 0) {
                        loyalityImage = GameConfig.picPath() + "pic/menu/icons/loyal_good.png";
                    } else if (riotValue <= 20 && riotValue > 5) {
                        loyalityImage = GameConfig.picPath() + "pic/menu/icons/loyal_equal.png";
                    } else {
                        loyalityImage = GameConfig.picPath() + "pic/menu/icons/loyal_bad.png";
                    }
                    String moraleImage = GameConfig.picPath() + "pic/menu/icons/mood_equal.png";
                    if (epcr.getMorale() >= 100) {
                        moraleImage = GameConfig.picPath() + "pic/menu/icons/mood_verygood.png";
                    } else if (epcr.getMorale() >= 80 && epcr.getMorale() < 100) {
                        moraleImage = GameConfig.picPath() + "pic/menu/icons/mood_good.png";
                    } else if (epcr.getMorale() >= 50 && epcr.getMorale() < 80) {
                        moraleImage = GameConfig.picPath() + "pic/menu/icons/mood_equal.png";
                    } else {
                        moraleImage = GameConfig.picPath() + "pic/menu/icons/mood_bad.png";
                    }
%>
<%
for(String result : results){ %>
    <CENTER> <%= result %><BR></CENTER>         
<%
    }
%>
<FORM method="post" action="main.jsp?page=new/management&process=<%= PROCESS_CHANGETAXES%>" Name="Production">
    <TABLE class="blue" width="80%" style="font-size: 12px;">
        <TR><TD colspan="6" class="blue" align=center><B>Zufriedenheit</FONT> </B></TD></TR>
        <TR>
            <TD align="center" colspan="2" width="33%" class="blue">
                <B><%= ML.getMLStr("management_lbl_criminality", userId)%></B>
            </TD>
            <TD align="center" colspan="2" width="33%" class="blue">
                <B><%= ML.getMLStr("management_lbl_riots", userId)%></B>
            </TD>
            <TD align="center" colspan="2" width="33%" class="blue">
                <B><%= ML.getMLStr("management_lbl_moraletaxes", userId)%></B>
            </TD>
        </TR>
        <TR>
            <TD align="center" colspan="2">
                <IMG alt="criminality" src="<%= GameConfig.picPath()%><%= criminalityImage%>" />
            </TD>
            <TD align="center" colspan="2">
                <IMG alt="loyality" src="<%= GameConfig.picPath()%><%= loyalityImage%>" />
            </TD>
            <TD align="center" colspan="2">
                <IMG alt="morale" src="<%= GameConfig.picPath()%><%= moraleImage%>" />
            </TD>
        </TR>
        <TR>
            <TD  valign="top">
                <%= ML.getMLStr("management_lbl_criminality", userId)%>
            </TD>
            <TD><FONT color="<%
                                if (criminalityValue > 50d) {
                      %>red<%   } else if ((criminalityValue > 25d) && (criminalityValue <= 50d)) {
                      %>yellow<%  } else {
                      %>#33FF00<%  }%>">
                    <%= (criminalityValue / 10)%> %
                </FONT>
            </TD>
            <TD>
                <%= ML.getMLStr("management_lbl_riots", userId)%>
            </TD>
            <TD>
                <FONT color="<%
                                    if (riotValue > 20) {
                      %>red<%   } else if ((riotValue <= 20) && (riotValue > 10)) {
                      %>yellow<%  } else {
                      %>#33FF00<%  }%>">
                    <%= riotValue%> % <% if(riotValue != 0) { %>-<%= Math.round(epcr.getRiotValueChange() * 144 * 100d)/100d %> <% } %>
                </FONT>
            </TD>
            <TD>
                <%= ML.getMLStr("management_lbl_morale", userId)%>
            </TD>
            <TD>
                <FONT color="<%
                                    if (epcr.getMorale() < 70) {
                      %>red<%   } else if ((epcr.getMorale() < 80) && (epcr.getMorale() >= 50)) {
                      %>yellow<%  } else {
                      %>#33FF00<%  }%>">
                    <%= FormatUtilities.getFormattedDecimal(epcr.getMorale(), 0)%>
                </FONT><%= epcr.getMoraleChangeStr()%>(max. <%= FormatUtilities.getFormattedDecimal(epcr.getMaxMorale() - epcr.getMoraleBonus(), 0)%> + <%= epcr.getMoraleBonus()%>)
            </TD>
        </TR>
        <TR>
            <TD  valign="top">
                <%= ML.getMLStr("management_lbl_creditdiscount", userId)%>
            </TD>
            <TD>
              
                - <%= FormatUtilities.getFormattedNumber(epcr.getTaxCrimDiscount())%>
            </TD>
            <TD  valign="top">
                <%= ML.getMLStr("management_lbl_creditdiscount", userId)%>
            </TD>
            <TD>
                - <%= FormatUtilities.getFormattedNumber(epcr.getTaxRiotDiscount())%>
            </TD>
            <TD>
                <%= ML.getMLStr("management_lbl_taxrate", userId)%>:
            </TD>
            <TD>
                <INPUT name="tax" value="<%= ManagementService.getTaxes(planetId)%>" size="2" maxlength='3'>&nbsp;%
                <INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("management_but_change", userId)%>">
            </TD>

        </TR>
        <TR>
            <TD class="blue" colspan="3">
                <%= ML.getMLStr("management_lbl_income", userId)%>
            </TD>
            <TD colspan="3">
                <B><%= FormatUtilities.getFormattedNumber(epcr.getTaxIncome())%> <%= ML.getMLStr("management_lbl_credits", userId)%></B>
            </TD>
        </TR>
    </TABLE>
</FORM>
<%

                    double happinessLoyality = epcr.getDevRiots();
                    double happinessCriminality = epcr.getDevCriminality();
                    double happinessMorale = epcr.getDevMorale();

%>
<TABLE border="0" class="blue" width="250px" style="font-size: 12px;">

    <TR class="blue2">
        <TD align="center" colspan="3">
            <%= ML.getMLStr("management_lbl_developementpoints", userId)%><A href="http://www.thedarkdestiny.at/wiki/index.php/Entwicklungspunkte" target="new"><IMG border="0" alt="techtree" onmouseover="doTooltip(event,'<%= ML.getMLStr("management_pop_developementinfo", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.picPath()%>pic/infoT.png" /></A>
        </TD>
    </TR>
    <TR>
        <TD align="center" width="33%">
            <B><%= ML.getMLStr("management_lbl_criminality", userId)%></B>
        </TD>
        <TD align="center">
            <IMG style="height: 18px; width: 18px;" alt="criminality" src="<%= GameConfig.picPath()%><%= criminalityImage%>" />
        </TD>
        <TD align="center">
            <%= happinessCriminality%>
        </TD>
    </TR>
    <TR>
        <TD align="center" width="33%">
            <B><%= ML.getMLStr("management_lbl_riots", userId)%></B>
        </TD>
        <TD align="center">
            <IMG style="height: 18px; width: 18px;" alt="loyality" src="<%= GameConfig.picPath()%><%= loyalityImage%>" />
        </TD>
        <TD align="center">
            <%= happinessLoyality%>
        </TD>
    </TR>
    <TR>
        <TD align="center" width="33%">
            <B><%= ML.getMLStr("management_lbl_moraletaxes", userId)%></B>
        </TD>
        <TD align="center">
            <IMG style="height: 18px; width: 18px;" alt="morale" src="<%= GameConfig.picPath()%><%= moraleImage%>" />
        </TD>
        <TD align="center">
            <%= happinessMorale%>
        </TD>
    </TR>
    <TR>
        <TD align="center">
            <%= ML.getMLStr("management_lbl_sum", userId)%>
        </TD>
        <TD align="center">
        </TD>
        <TD align="center">
            <%= Math.round((epcr.getDevelopementPoints()) * 100d) / 100d%>
        </TD>
    </TR>
    <TR>
        <TD align="center">
            Global
        </TD>
        <TD align="center">
            <IMG style="height: 18px; width: 18px;" alt="morale" src="<%= GameConfig.picPath()%><%= happinessImage%>" />
        </TD>
        <TD align="center">
            <%= Math.round((epcr.getDevelopementPointsGlobal()) * 10000d) / 10000d%>
        </TD>
    </TR>
</TABLE>
<BR>
<TABLE width="80%" style="font-size: 12px;">
    <TR>
        <TD align="right">
            <TABLE width="100%" style="font-size: 12px;">
                <TR>
                    <TD width="33%" height="100%">
                        <FORM method="post" action="main.jsp?page=new/management&process=<%= PROCESS_UPDATEPRIORITIES%>" Name="Production">
                            <TABLE class="blue" width="100%" style="font-size: 12px;">
                                <TR class="heading"><TD colspan='7'><%= ML.getMLStr("management_lbl_productionpriorities", userId)%></TD></TR>
                                <TR><TD><%= ML.getMLStr("management_lbl_industry", userId)%></TD><TD>
                                        <%
                                                            OptionField optionField;
                                                            optionField = new OptionField(out, "priorityI");
                                                            optionField.addEntry(0, ML.getMLStr("management_opt_high", userId));
                                                            optionField.addEntry(1, ML.getMLStr("management_opt_average", userId));
                                                            optionField.addEntry(2, ML.getMLStr("management_opt_low", userId));

                                                            optionField.setSelected(pp.getPriorityIndustry());
                                                            optionField.setClass("dark");
                                                            optionField.output();
                                        %>
                                    </TD></TR><TR><TD><%= ML.getMLStr("management_lbl_agrar", userId)%></TD><TD>
                                    <%
                                                        optionField = new OptionField(out, "priorityA");
                                                        optionField.addEntry(0, ML.getMLStr("management_opt_high", userId));
                                                        optionField.addEntry(1, ML.getMLStr("management_opt_average", userId));
                                                        optionField.addEntry(2, ML.getMLStr("management_opt_low", userId));

                                                        optionField.setSelected(pp.getPriorityAgriculture());
                                                            optionField.setClass("dark");
                                                        optionField.output();
                                    %>
                                    </TD></TR><TR><TD><%= ML.getMLStr("management_lbl_research", userId)%></TD><TD>
                                    <%
                                                        optionField = new OptionField(out, "priorityF");
                                                        optionField.addEntry(0, ML.getMLStr("management_opt_high", userId));
                                                        optionField.addEntry(1, ML.getMLStr("management_opt_average", userId));
                                                        optionField.addEntry(2, ML.getMLStr("management_opt_low", userId));

                                                        optionField.setSelected(pp.getPriorityResearch());
                                                            optionField.setClass("dark");
                                                        optionField.output();
                                    %>
                                    </TD></TR><TR><TD colspan="2"><BR><CENTER><INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("management_but_change", userId)%>"></CENTER></TD></TR>
                            </TABLE>
                        </FORM>
                    </TD>
                    <TD width="33%" height="100%" valign="top">
                        <FORM method="post" action="main.jsp?page=new/management&process=<%= PROCESS_CHANGENAMES%>" Name="Rename">
                            <TABLE class="blue" width="100%" style="font-size: 12px;">
                                <TR><TD colspan=5 class="blue" align=center><B><%= ML.getMLStr("management_lbl_renameplanetsystem", userId)%></B></TD></TR>
                                <TR><TD width="90"><%= ML.getMLStr("management_lbl_planet", userId)%></TD><TD><INPUT name="planetname" value="<%= ManagementService.getPlanetName(planetId)%>" size="25" maxlength='25'></TD></TR>
                                <TR><TD><%= ML.getMLStr("management_lbl_system", userId)%></TD><TD><INPUT name="systemname" value="<%= ManagementService.getSystemNamefromPlanetId(planetId)%> "  size="25" maxlength='25'></TD></TR>

                                <TR><TD colspan="2" align=center height="100%"><BR><CENTER><INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("management_but_change", userId)%>"></CENTER></TD></TR>
                            </TABLE>
                        </FORM>
                    </TD>
                    <TD width="33%" height="100%" valign="top">
                        <FORM method="post" action="main.jsp?page=new/management&process=<%= PROCESS_CHANGECATEGORY%>" name="categoryForm">

                            <TABLE class="blue" width="100%" style="font-size: 12px;">

                                <TR><TD colspan="2" class="blue" align=center><B><%= ML.getMLStr("management_lbl_category", userId)%></B></TD></TR>
                                <TR><TD colspan="2"><CENTER>
                                            <SELECT name="category" class="dark">
                                                <OPTION value='0'/> <%= ML.getMLStr("category_opt_nocategory", userId)%>

                                                <%
                                                                    int tmpCategoryId = CategoryService.getCategoryIdFromPlanetId(planetId);
                                                                    for (PlayerCategory pc : CategoryService.getAllCategorysByUserId(userId)) {
                                                %>
                                                <OPTION <%= (pc.getId() == tmpCategoryId) ? "SELECTED" : ""%> value='<%= pc.getId()%>'/> <%=pc.getName()%>
                                                <%
                                                                    }
                                                %>
                                            </SELECT>
                                        </CENTER></TD></TR>

                                <TR>
                                    <TD align=center><INPUT type="submit" name="change" value="<%= ML.getMLStr("management_but_change", userId)%>"></TD>

                                </TR>
                                <TR>
                                    <TD align=center>
                                        <A href="main.jsp?page=new/categorys"><FONT style="font-size:13"><%= ML.getMLStr("management_link_manage", userId)%></FONT></A>
                                    </TD>
                                </TR>
                            </TABLE>
                        </FORM>
                    </TD></TR></TABLE>
            <BR>
            <%

                                ArrayList<Ressource> ress = ManagementService.getMinStockRessources(planetId, prodRes);
                                int colspanFactor = 1;
                                if (ress.size() > 1) {
                                    colspanFactor = 2;
                                }
            %>
            <FORM method="post" action="main.jsp?page=new/management&process=<%= PROCESS_CHANGEMINIMUM%>" Name="Production">
                <TABLE cellpadding="0" cellspacing="0" class="blue" width="100%" style="font-size: 12px;">
          
                    <TR>
                        <TD class="blue2"></TD>
                        <TD valign="left"  class="blue2"><B><%= ML.getMLStr("management_lbl_ressource", userId)%></B></TD>
                        <TD colspan="3" valign="middle" align="center" class="blue2"><%= ML.getMLStr("management_lbl_minimumstock", userId)%><IMG alt="minimumStorage" onmouseover="doTooltip(event,'<%= ML.getMLStr("management_pop_minimumstorage", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.picPath()%>pic/infoT.png" /></TD>
                    </TR>
                    <TR>
                        <TD colspan="2" class="blue2"></TD>
                        <TD valign="middle" align="center" class="blue2">Produktion</TD>
                        <TD valign="middle" align="center" class="blue2">Transport</TD>
                        <TD valign="middle" align="center" class="blue2">Handel</TD>    
                    </TR>
<%
                    for (Ressource r : ress) {
%>
                    <TR> 
                        <TD width="20px" ><IMG alt="<%= ML.getMLStr(r.getName(), userId)%>" height="20" width="20" src="<%= GameConfig.picPath()%><%= r.getImageLocation()%>"/></TD>
                        <TD align="left"><%= ML.getMLStr(r.getName(), userId)%></TD>
                        <TD align="center"><INPUT class="dark" name="r_prod_<%= r.getId()%>" value="<%= ManagementService.findMinStock(planetId, r.getId(), EPlanetRessourceType.MINIMUM_PRODUCTION)%>" size="10" maxlength='10'></TD>
                        <TD align="center"><INPUT class="dark" name="r_trans_<%= r.getId()%>" value="<%= ManagementService.findMinStock(planetId, r.getId(),  EPlanetRessourceType.MINIMUM_TRANSPORT)%>" size="10" maxlength='10'></TD>
                        <TD align="center"><INPUT class="dark" name="r_trade_<%= r.getId()%>" value="<%= ManagementService.findMinStock(planetId, r.getId(), EPlanetRessourceType.MINIMUM_TRADE)%>" size="10" maxlength='10'></TD>
                          
                    </TR>
                    <% }%>
                    <TR align="center"><TD colspan="8" align="center"><BR><INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("management_but_change", userId)%>"></TD></TR>
                </TABLE>
            </FORM>
        </TD>
</TABLE>
<FORM method="post" action="main.jsp?page=new/management&process=<%= PROCESS_SETINVISIBLE%>" Name="Production">
    <TABLE style="font-size: 12px;">

        <TR>
            <TD><%= ML.getMLStr("overview_lbl_setinvisible", userId)%></TD>
            <TD>
                <%
                                    if (!pp.getInvisibleFlag()) {%>
                <INPUT TYPE="checkbox" />
                <%      } else {%>
                <INPUT CHECKED TYPE="checkbox" />
                <%      }%>
            </TD><TD><INPUT type="submit" name="Ok" value="Ok"></TD>

        </TR>
    </TABLE>
</FORM>
<%
                    if (pp.getPopulation() > 0) {
%>
<TABLE style="font-size: 12px;">
    <FORM method="post" action="main.jsp?page=new/management&process=<%= PROCESS_DISMANTLECOLONY%>" Name="Production">
        <TR>
            <TD><%= ML.getMLStr("overview_lbl_dismantlecolony", userId)%></TD>
            <TD>
                <%

                                    if ((ManagementService.userDAO.findById(userId).isGuest()) || pp.getHomeSystem()) {%>
                <INPUT <% if (pp.getDismantle()) {%>CHECKED<% }%> disabled name="dismantle" TYPE="checkbox" />
                <%      } else {%>
                <INPUT <% if (pp.getDismantle()) {%>CHECKED<% }%> name="dismantle" TYPE="checkbox" />
                <%      }
                %>
            </TD>
            <TD>
                <% if ((ManagementService.userDAO.findById(userId).isGuest()) || pp.getHomeSystem()) {%>
                <INPUT style="color:gray;" disabled type="submit" name="Ok" value="Ok">
                <% } else {%>
                <INPUT type="submit" name="Ok" value="Ok">
                <% }%>
            </TD>
            <TD>
                <IMG alt="Dismantle" onmouseover="doTooltip(event,'<%= ML.getMLStr("management_pop_dismantle", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.picPath()%>pic/info.jpg" />
            </TD>
    </FORM>
    <% if (ManagementService.abadonable(userId, planetId)) {%>
    <FORM method="post" action="main.jsp?page=new/management&process=<%= TYPE_ABADONCOLONY%>" Name="Production">
        <TR>
            <TD colspan="4">
                <INPUT type="submit" name="Ok" value="<%= ML.getMLStr("management_abadoncolony", userId)%>">
            </TD>
        </TR>
    </FORM>
    <% }
                    }%>
</TABLE><%

                    break;

                // Security check for abadoning colony
                case (TYPE_ABADONCOLONY):
%>
<CENTER><%= ML.getMLStr("management_msg_abadoncolony", userId)%><BR>
    <A href="main.jsp?page=new/management"><%= ML.getMLStr("management_link_cancel", userId)%></A>
    <A href="main.jsp?page=new/management&process=<%= PROCESS_DISMANTLECOLONY%>"><%= ML.getMLStr("management_link_accept", userId)%></A>
</CENTER>
<%
                    break;
            }
%>

