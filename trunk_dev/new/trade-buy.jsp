<%@page import="at.viswars.model.TradeRouteDetail"%>
<%@page import="at.viswars.model.TradeRoute"%>
<%@page import="at.viswars.enumeration.ETradeRouteType"%>
<%@page import="at.viswars.model.Construction"%>
<%@page import="at.viswars.model.TradePost"%>
<%@page import="at.viswars.enumeration.EOwner"%>
<%@page import="at.viswars.result.BaseResult"%>
<%@page import="java.util.ArrayList"%>
<%@page import="at.viswars.model.TradeOffer"%>
<%@page import="at.viswars.GameConstants"%>
<%@page import="at.viswars.FormatUtilities"%>
<%@page import="at.viswars.result.TradeOfferEntry"%>
<%@page import="at.viswars.result.TradeOfferResult"%>
<%@page import="at.viswars.service.TradeService2"%>
<%@page import="at.viswars.model.PlayerPlanet"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.model.Planet"%>
<%@page import="at.viswars.service.PlanetService"%>
<%@page import="at.viswars.service.RessourceService"%>
<%@page import="at.viswars.service.IdToNameService"%>
<%@page import="at.viswars.model.Ressource"%>
<%@page import="at.viswars.GameConfig"%>
<%@page import="at.viswars.ML"%>
<%
// Logger.getLogger().write("DEBUGMSG 1");

    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
    
    final int SUBTYPE_OVERVIEW = 0;
    final int SUBTYPE_BUY = 1;
    final int OPT_SHOW_ALL = -1;
    int subType = SUBTYPE_OVERVIEW;
    
    final int PROCESS_BUY = 1;
    int process = 0;
    
    ArrayList<BaseResult> results = new ArrayList<BaseResult>();

    int ressourceId = OPT_SHOW_ALL;
    if(request.getParameter("ressourceId") != null){
        ressourceId = Integer.parseInt(request.getParameter("ressourceId"));
    }
    int tradePostId = 0;
    if(request.getParameter("tradePostId") != null){
        tradePostId = Integer.parseInt(request.getParameter("tradePostId"));
    }
    int targetPlanetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
    if(request.getParameter("targetPlanetId") != null){
        targetPlanetId = Integer.parseInt(request.getParameter("targetPlanetId"));
    }
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }    
    if (request.getParameter("subType") != null) {
        subType = Integer.parseInt(request.getParameter("subType"));
    }
    
    if(process == PROCESS_BUY){
        results.addAll(TradeService2.buyGoods(userId, request.getParameterMap()));
        subType = SUBTYPE_OVERVIEW;
        process = 0;
    }

// Logger.getLogger().write("DEBUGMSG 2");
%>

<CENTER>
<% for(BaseResult br : results){ %>
<%= br.getFormattedString() %>
<% }
// Logger.getLogger().write("DEBUGMSG 2a");
%>
</CENTER>
<BR>
<%

    switch(subType){
        case(SUBTYPE_OVERVIEW):
            // Logger.getLogger().write("DEBUGMSG 2aa");
            %>
<TABLE width="80%" class="bluetrans">
    <TR>
        <TD>
            <%= ML.getMLStr("trade_lbl_chooseresource", userId) %>
        </TD>
        <TD>
            <SELECT id="ressource" name="ressource" class="dark" onChange="window.location.href='main.jsp?page=new/trade-main&type=2&ressourceId='+document.getElementById('ressource').value;">
                 <OPTION value="<%= OPT_SHOW_ALL %>"><%= ML.getMLStr("trade_opt_allressources", userId) %></OPTION>
                <% for (Ressource r : RessourceService.getAllStorableRessources()) {%>
                <OPTION value="<%= r.getId()%>" <% if(ressourceId == r.getId()){ %>selected<% } %>><%= ML.getMLStr(r.getName(), userId)%></OPTION>
                <% }%>
            </SELECT>
        </TD>
    </TR>
    <TR>
        <TD>
            <%= ML.getMLStr("trade_lbl_choosetarget", userId) %>
        </TD>
        <TD>
            <SELECT id="targetPlanetId" class="dark" name="targetPlanetId">
                <OPTION value="<%= planetId%>"><%= PlanetService.getPlanetName(planetId).getName()%></OPTION>
                <% for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserIdSortedByName(userId)) {%>
                <% if (pp.getPlanetId() == planetId) {
                        continue;
                    }%>
                <OPTION value="<%= pp.getPlanetId()%>"><%= pp.getName()%></OPTION>
                <% }%>
            </SELECT>
        </TD>
    </TR>
<!--
    <TR>
        <TD>
            Sortierung w&auml;hlen (TODO)
        </TD>
     <TD>
         <SELECT name="sortOrder" disabled="">
                <OPTION value="1">Absteigend</OPTION>
                <OPTION value="2">Aufsteigen</OPTION>
            </SELECT>
        </TD>
    </TR>
-->
</TABLE>
<BR>
<BR>
<TABLE width="80%" class="sortable" style="font-size: 11px; font-weight: bolder">
    <TR class="blue2">
        <TD>&nbsp;</TD>
        <TD align="center"><%= ML.getMLStr("trade_lbl_level", userId) %></TD>
        <TD align="center"><%= ML.getMLStr("trade_lbl_resource", userId) %></TD>
        <TD align="center"><%= ML.getMLStr("trade_lbl_priceperRU", userId) %></TD>
        <TD align="center"><%= ML.getMLStr("trade_lbl_distance_LY", userId) %></TD>
        <TD align="center" onmouseover="doTooltip(event,'<%= ML.getMLStr("trade_pop_actualtransportrate", userId) %>')" onmouseout="hideTip()" width="80px" style="font-size:9px;"><%= ML.getMLStr("trade_lbl_actualtransportrate", userId) %></TD>
        <TD align="center"><%= ML.getMLStr("trade_lbl_quantity", userId) %></TD>
        <TD align="center"><%= ML.getMLStr("trade_lbl_actions", userId) %></TD>
    </TR>
    <%
    // Logger.getLogger().write("DEBUGMSG 2z");
    TradeOfferResult tro = null;
    //show All
    if(ressourceId == OPT_SHOW_ALL){
        // Logger.getLogger().write("DEBUGMSG 2za");
        tro = TradeService2.getTradeOffers(userId, targetPlanetId);
      }else{
        // Logger.getLogger().write("DEBUGMSG 2zb");
        tro = TradeService2.getTradeOffers(userId, ressourceId, targetPlanetId);
      }

    // Logger.getLogger().write("DEBUGMSG 3a: Starting looping offers");
    for(TradeOfferEntry toe : tro.getOffers()){
        // Logger.getLogger().write("DEBUGMSG 3b: TOE: " + toe);
    %>
    <TR>
<%
        String sellerName = toe.getDiplomacyResult().getOwner().toString();
        String color = "black";

        String backgroundColor = toe.getDiplomacyResult().getColor();
        if (((toe.getDiplomacyResult().getOwner() == EOwner.ALLY) ||
            (toe.getDiplomacyResult().getOwner() == EOwner.TREATY) ||
            (toe.getDiplomacyResult().getOwner() == EOwner.TRADE)) && toe.getOwningUser() != userId) {
            sellerName = IdToNameService.getUserName(toe.getOwningUser()).toUpperCase();
            if (toe.getDiplomacyResult().getOwner() == EOwner.ALLY) {
                color = "gray";
            }
        }else if(toe.getOwningUser() == userId){
            sellerName = ML.getMLStr("trade_lbl_seller_own", userId);
            backgroundColor = "#00FF00";
        }        
        //Calculate transportrate
        TradePost tp = Service.tradePostDAO.findById(toe.getTradePostId());
        double capBoost = tp.getCapBoost();
        
        long capacity = 0;
        
        int level = Service.planetConstructionDAO.findBy(tp.getPlanetId(), Construction.ID_INTERSTELLARTRADINGPOST).getLevel();
        if(level <= 0){
            level = 1;
        }
        
        capacity += ((double)tp.getTransportPerLJ() * (double)level * capBoost)  / 2d / toe.getDistance();                
                          
       
          //Search outgoing Routes
        int count = 0;
         for(TradeRoute tr : Service.tradeRouteDAO.findByStartPlanet(tp.getPlanetId(), ETradeRouteType.TRADE)){
             for(TradeRouteDetail trd : Service.tradeRouteDetailDAO.getActiveByRouteId(tr.getId())){
                 if(trd.getLastTransport() > 0){
                    count++;
                 }
             }
         }
         //Pollute Value
         double meanCapacity = capacity / (count + 1);
        
         //Subtract 5% to be lower the estimation
         meanCapacity *= 0.95d;
         
         
         int randomValue = (int)(toe.getDistance() % 10) + tp.getPlanetId() % 10;
         meanCapacity *= 1d - (randomValue / 100d);
         
         
%>
        <TD align="Center" style="background-color: <%= backgroundColor %>;color:<%= color %>;"><%= sellerName %></TD>
        <% if(toe.getRessourceId() > 0){ 
            Ressource r = Service.ressourceDAO.findRessourceById(toe.getRessourceId());
            %>
            <TD align="center"><%= toe.getTradePostLevel() %></TD>
            <TD align="center"><IMG style="width:20px; height:20px" src="<%= GameConfig.picPath() + r.getImageLocation()%>"/></TD><%
          }else{%>
            <TD align="center">&nbsp;</TD>
          <% } %>
          <TD align="center"><%= FormatUtilities.getFormatScaledNumber(toe.getPrice(), 2, GameConstants.VALUE_TYPE_NORMAL) %></TD>
        <TD align="center">
            <% if(toe.getOwningUser() != userId){ %>
            <%= toe.getDistanceString() %>
             <% } %>
        </TD>
        <TD align="center">
            <% if(toe.getOwningUser() != userId){ %>
            ~ <%= FormatUtilities.getFormatScaledNumber(meanCapacity, 2, GameConstants.VALUE_TYPE_NORMAL) %>
              <% } %>
        </TD>
        <TD align="center"><%= FormatUtilities.getFormatScaledNumber(toe.getAvailAmount(), 2, GameConstants.VALUE_TYPE_NORMAL) %></TD>
        <TD align="center">
            <% if(toe.getOwningUser() != userId){ %>
            <IMG src="<%= GameConfig.picPath()%>pic/buy.jpg" style="cursor:pointer;" alt="Rohstoffe kaufen" onmouseover="doTooltip(event,'Rohstoffe kaufen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/trade-main&type=2&subType=<%= SUBTYPE_BUY %>&ressourceId=<%= toe.getRessourceId() %>&tradePostId=<%= toe.getTradePostId() %>&targetPlanetId=' +document.getElementById('targetPlanetId').value" />
            <% } %>
        </TD>
    </TR>
    <% }
    // Logger.getLogger().write("DEBUGMSG 3c: Finished looping offers");
%>
</TABLE>
<%
    break;
    case(SUBTYPE_BUY): 
    TradeOfferEntry toe = TradeService2.getTradeOfferEntry(userId, tradePostId, ressourceId, targetPlanetId);    
    Ressource r = Service.ressourceDAO.findRessourceById(toe.getRessourceId());
    %>
    <SCRIPT type="text/javascript">
    var price = <%= toe.getPrice() %>;
    var amount = 0;
    function updatePrice(){        
        var totalCredit = amount * price;
        document.getElementById("price").innerHTML = number_format(totalCredit,0) + ' Credits';
    }
</SCRIPT>

    <FORM method="post" action="main.jsp?page=new/trade-main&type=2&process=<%= PROCESS_BUY %>" >
    <TABLE class="bluetrans">
        <INPUT type="hidden" value="<%= tradePostId %>" name="tradePostId"/>
        <INPUT type="hidden" value="<%= r.getId() %>" name="ressourceId"/>
        <INPUT type="hidden" value="<%= targetPlanetId %>" name="targetPlanetId"/>
        <TR class="blue2">
            <TD colspan="3">
                TradePost : <B><%= TradeService2.getTradePost(tradePostId).getName() %></B>
            </TD>
        </TR>
        <TR>
            <TD class="blue">
                <%= ML.getMLStr("trade_lbl_resource", userId) %>
            </TD>
            <TD align="right">
                <IMG style="width:20px; height:20px" src="<%= GameConfig.picPath() + r.getImageLocation()%>"/>
            </TD>
            <TD align="left">
                <%= ML.getMLStr(r.getName(), userId) %>
            </TD>
        </TR>
        <TR>
            <TD class="blue">
                <%= ML.getMLStr("trade_lbl_distance", userId) %>
            </TD>
            <TD colspan="2" align="center">
                <%= toe.getDistanceString() %>
            </TD>
        </TR>
        <TR>
            <TD class="blue">
                <%= ML.getMLStr("trade_lbl_quantity", userId) %>
            </TD>
            <TD colspan="2">
                <INPUT  id="amount" name="amount" style="width: 100px;" class="dark" type="input" value="0" ONKEYUP="amount=document.getElementById('amount').value;updatePrice();" />/<%= FormatUtilities.getFormatScaledNumber(toe.getAvailAmount(), GameConstants.VALUE_TYPE_NORMAL) %>
            </TD>
        </TR>
        <TR>
            <TD class="blue">
                <%= ML.getMLStr("trade_lbl_price", userId) %>
            </TD>
            <TD align="center" id="price" colspan="2">
                
            </TD>
        </TR>
        <TR>
            <TD colspan="3" align="center">
                <INPUT class="dark" type="submit" value="Kaufen" />
            </TD>
        </TR>
    </TABLE>
    </FORM>
    
    <%
    break;
    }
%>