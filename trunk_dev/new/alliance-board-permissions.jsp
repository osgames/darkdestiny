
<%@page import="at.viswars.service.AllianceBoardService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="at.viswars.service.IdToNameService"%>
<%@page import="at.viswars.GameConfig"%>
<%@page import="at.viswars.ML"%>
<%@page import="at.viswars.enumeration.EAllianceBoardPermissionType"%>
<%@page import="at.viswars.model.User"%>
<%@page import="at.viswars.model.Alliance"%>
<%@page import="at.viswars.model.AllianceBoardPermission"%>
<%@page import="at.viswars.result.AlliancePermissionResult"%>
<%@page import="at.viswars.model.AllianceBoard"%>
<%@page import="at.viswars.service.AllianceService"%>
<%

    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));

    

    int boardId = 0;
    if (request.getParameter("boardId") != null) {
        boardId = Integer.parseInt(request.getParameter("boardId"));
    }
    
    int process = 0;
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }
    if(process == 999){
        AllianceBoardService.updatePermissions(userId, boardId, request.getParameterMap());
    }
    AllianceBoard ab = AllianceBoardService.findAllianceBoardById(boardId);
    AlliancePermissionResult apr = AllianceBoardService.getAlliancePermissions(userId, ab.getAllianceId(), boardId);

%>
<script language="javascript" type="text/javascript">
var allianceMember = new Array(<%= apr.getAllianceMembers().size() %>);
    <% for(Map.Entry<Integer, ArrayList<Integer>> entry : apr.getAllianceMembers().entrySet()){ %>
        allianceMember[<%= entry.getKey() %>] = new Array(<%= entry.getValue().size() %>)
        <% for(Integer i : entry.getValue()){ %>
            allianceMember[<%= entry.getKey() %>][<%= i %>] = <%= i %>;
            <% } %>
    <% } %>
      
    var users = new Array(<%= apr.getUsers().size() %>);  
        <% for(Map.Entry<Integer, User> entry : apr.getUsers().entrySet()){ %>
        users[<%= entry.getKey() %>] = "<%= entry.getValue().getGameName() %>";
        
    <% } %>
    var allys = new Array(<%= apr.getAlliances().size() %>);  
        <% for(Map.Entry<Integer, Alliance> entry : apr.getAlliances().entrySet()){ %>
        allys[<%= entry.getKey() %>] = "<%= entry.getValue().getName() %>";
        
    <% } %>
    function updatePermissions(){
        
            
        
        var table = document.getElementById("permissionTable");
        var select = document.getElementById('targetEntities');
        
        for (var i=0; i < select.options.length;i++) {
            if (select.options[i].selected == true) {
                // do something
                var value = select.options[i].value;
                var refIdValue = value.substr(0, value.indexOf("-"));
                var typeValue = value.substr(value.indexOf("-")+1, value.length);
                
               appendPermission(table,value,refIdValue, typeValue, false, false);
                
            }
        }
    
    
    }
    function appendPermission(table,value, refIdValue, typeValue, isWrite, isDelete){
     
                
                var name = "UNKNOWN";
                        if(typeValue == '<%= EAllianceBoardPermissionType.ALLIANCE.toString() %>'){
                            name = allys[refIdValue];
                        }else if(typeValue == '<%= EAllianceBoardPermissionType.USER.toString() %>'){
                            name = users[refIdValue];
                        }
                
                
                var tr = document.createElement("tr");
                tr.id = "'" + value + "'";
                tr.setAttribute("id", value);
                var refId = document.createElement("td");
                refId.name = "refId";
                refId.appendChild(document.createTextNode(name));
                
                var idParam = document.createElement("input");
                idParam.setAttribute("type", "hidden");
                idParam.name = "perm_" + value + "-id";
                idParam.value = refIdValue;
                
                var type = document.createElement("td");
                type.name = value;
                type.appendChild(document.createTextNode(typeValue));
               
                var typeParam = document.createElement("input");
                typeParam.setAttribute("type", "hidden");
                typeParam.name = "perm_" + value + "-permType";
                typeParam.value = typeValue;
                
                type.appendChild(idParam);
                type.appendChild(typeParam);
                
                var write = document.createElement("td");
                var writeSelect =  document.createElement("input");
                writeSelect.name = "perm_" + value + "-write";
                writeSelect.setAttribute("type", "checkbox");
                if(isWrite == "true"){
                    writeSelect.setAttribute("checked", "true");
                }
                write.appendChild(writeSelect);
                var del = document.createElement("td");
                var delSelect =  document.createElement("input");
                delSelect.setAttribute("type", "checkbox");
                delSelect.name = "perm_" + value + "-delete";
                if(isDelete == "true"){
                    delSelect.setAttribute("checked", "true");
                }
                del.appendChild(delSelect);
                var action = document.createElement("td");
                var delImage = document.createElement("img");
                var image = "pic/cancel_message.gif";
                delImage.setAttribute("style", "width:17px; height:17px;");
                delImage.setAttribute("onclick", "deletePermission('" + value+ "')");
                delImage.setAttribute("src", image);
                action.appendChild(delImage);
                
                
                tr.appendChild(refId);
                tr.appendChild(type);
                tr.appendChild(write);
                tr.appendChild(del);
                tr.appendChild(action);
                
                
                var changeButton = document.getElementById('changeButton');
              
                table.appendChild(tr);
                if(typeValue == '<%= EAllianceBoardPermissionType.ALLIANCE.toString() %>'){
                            
                }
                
                table.appendChild(changeButton);
    }

     function deletePermission(value){
            var table = document.getElementById("permissionTable");
            var tr = document.getElementById(value);
        for(var i = 0; i < table.childNodes.length; i++) {
            
        }
    
            table.removeChild(tr);
            
    }
</script>
<TABLE width="80%">
    <TR>
        <TD align="right">
            <TABLE>
                <TR>
                    <TD>
                        <SELECT multiple size='30' name="targetEntities" id="targetEntities" onclick=''>
                            <OPTION value="0">Allianzen selektieren</OPTION>
                            <%

                                for (Alliance a : apr.getAlliancesSorted()) {
                                  %><OPTION style="background-color: black; color: white;" value="<%= a.getId()%>-<%= EAllianceBoardPermissionType.ALLIANCE%>"><%= a.getName()%></OPTION><%
                                }
                                %>
                                
                            <OPTION value="0">User selektieren</OPTION>
                            <%
                                for (User u : apr.getUsersSorted()) {
                                  %><OPTION style="background-color: black; color: white;" value="<%= u.getUserId()%>-<%= EAllianceBoardPermissionType.USER%>"><%= u.getGameName()%></OPTION><%
                                }

                            %>
                        </SELECT>
                    </TD>
                </TR>
                <TR>
                    <TD>
                        <INPUT type="button" value="Hinzufügen" onclick="updatePermissions();"/>
                    </TD>
                </TR>
            </TABLE>
        </TD>
        <TD align="left" valign="top">
            <FORM action="main.jsp?page=new/alliance&process=999&type=23&boardId=<%= ab.getId()%>" method="post">
            <TABLE name="permissions" class="bluetrans" width="600px" id="permissionTable">
                <TR class="blue2">
                    <TD>
                        Name
                    </TD>
                    <TD>
                        Type
                    </TD>
                    <TD>
                        canWrite
                    </TD>
                    <TD>
                        canDelete
                    </TD>
                    <TD>
                        Aktion
                    </TD>
                </TR>
             
                <TR id="changeButton">
                    <TD valign="bottom" colspan="5" align="center">
                        <INPUT type="submit" value="change"/>
                    </TD>
                </TR>
               
            </TABLE>
                </FORM>
        </TD>
    </TR>
</TABLE>
               <%
                    for (AllianceBoardPermission abp : apr.getPermissions()) {
                        String id = abp.getRefId() + "-" + abp.getType();
                        String name = "UNKNOWN";
                        if(abp.getType().equals(EAllianceBoardPermissionType.ALLIANCE)){
                            if (apr.getAlliances().get(abp.getRefId()) != null)
                                name = apr.getAlliances().get(abp.getRefId()).getName();
                        }else if(abp.getType().equals(EAllianceBoardPermissionType.USER)){
                            if (apr.getUsers().get(abp.getRefId()) != null)
                                name = apr.getUsers().get(abp.getRefId()).getGameName();
                        }
                        %>
<script language="javascript" type="text/javascript">
    appendPermission(document.getElementById("permissionTable"), '<%= id %>', '<%= abp.getRefId() %>', '<%= abp.getType().toString() %>', '<%= abp.getWrite() %>', '<%= abp.getDelete()  %>');
    </script>
                        <%
                                               }
                %>

