<%@page import="at.viswars.ML"%>
<%
int type = 2;

            int userId = Integer.parseInt((String) session.getAttribute("userId"));
if (request.getParameter("type") != null) {
    type = Integer.parseInt(request.getParameter("type"));
}
%>
<BR>
<TABLE width="80%">
    <TR align="center">
        <TD width="33%"><U><A href="main.jsp?page=new/trade-main&type=1"><%= ML.getMLStr("trade_link_overview", userId) %></A></U></TD>
        <TD width="34%"><U><A href="main.jsp?page=new/trade-main&type=2"><%= ML.getMLStr("trade_link_buy", userId) %></A></U></TD>
        <TD width="33%"><U><A href="main.jsp?page=new/trade-main&type=3"><%= ML.getMLStr("trade_link_sell", userId) %></A></U></TD>
    </TR>
</TABLE><BR>
<%
switch (type) {
    case(1):
%>
        <jsp:include page="../new/trade-ovw.jsp" />
<%
        break;
    case(2):
%>
        <jsp:include page="../new/trade-buy.jsp" />
<%
        break;
    case(3):
%>
        <jsp:include page="../new/trade-sell.jsp" />
<%
        break;
}
%>