<%@page import="at.viswars.GameConfig"%>
<%@ page import="at.viswars.service.*" %>
<%@ page import="at.viswars.model.*" %>
<%@ page import="at.viswars.result.*" %>
<%@ page import="at.viswars.ML" %>
<%
int userId = Integer.parseInt((String)session.getAttribute("userId"));
UserData ud = LoginService.findUserDataByUserId(userId);

//F�r Voreinstellung
int sourcePlanetId = 0;
int targetPlanetId = 0;

if (request.getParameter("sourcePlanetId") != null) {
    sourcePlanetId = Integer.parseInt(request.getParameter("sourcePlanetId"));
}
if (request.getParameter("targetPlanetId") != null) {
    targetPlanetId = Integer.parseInt(request.getParameter("targetPlanetId"));
}


StringBuffer requestUrl = request.getRequestURL();
String basePath = "";
int slashCounter = 0;

for (int i=0;i<requestUrl.length();i++) {
    char currChar = requestUrl.charAt(i);
    if (currChar == ("/".toCharArray())[0]) slashCounter++;
    
    basePath += currChar;
    if (slashCounter == 4) break;
}

BaseResult result = null;
if (request.getParameter("submit") != null) {
    result = TransportService.createTransportRoute(userId, request.getParameterMap());
}
%>
<script src="java/ajax.js" type="application/javascript"></script>
<script type="text/javascript">
var addedFromTo = 1;
var addedToFrom = 1;

var activeFromTo = 1;
var activeToFrom = 1;
    
function insertStartFromList(startId){
        document.getElementById("sourceId").value = startId;
    
}   
function insertTargetFromList(targetId){
        document.getElementById("targetId").value = targetId;
    
}
    
function checkStartAndTarget() {
    var source = document.getElementById("sourceId").value;
    var target = document.getElementById("targetId").value;
    
    ajaxCall('<%= basePath %>CheckCoordinates?userId=<%= userId %>&sourceId='+source+'&targetId='+target,processCheckResult);
}    
    
function processCheckResult(result) {
    if (result.error == "") {
        document.getElementById("response").innerHTML = '<FONT color="lightgreen"><B>OK<B></FONT';
        document.getElementById("sourceId").value = result.source.name;
        document.getElementById("targetId").value = result.target.name;
        setStart();
        setTarget();
    } else {
        alert (result.error);
    }
}    
    
function resetResult() {
    document.getElementById("response").innerHTML = '';
}    
    
function setStart() {
    var startHeading = document.getElementById("start");
    var startHeading2 = document.getElementById("start2");
    var textSource = document.getElementById("sourceId");
    startHeading.innerHTML = textSource.value;
    startHeading2.innerHTML = textSource.value;
}

function setTarget() {
    var startHeading = document.getElementById("target");
    var startHeading2 = document.getElementById("target2");
    var textSource = document.getElementById("targetId");
    startHeading.innerHTML = textSource.value;    
    startHeading2.innerHTML = textSource.value; 
}

    function addSlot(type) {                
        var genSlot;
        
        if (type == 1) { // from Start to Target
            if (activeFromTo == 6) return;
            genSlot = addRowFor('fromTo');
            addedFromTo++;
            activeFromTo++;
        } else if (type == 2) { // from Target to Start
            if (activeToFrom == 6) return;
            genSlot = addRowFor('toFrom');
            addedToFrom++;
            activeToFrom++;
        }
        
        return genSlot;
    }
    
    function addRowFor(typeName,optionData) {
        slotTable = document.getElementById("transportTable");
        var elements = slotTable.getElementsByTagName("TR");

        lastElement = null;
        var count = 1;
        var rowIndex = 0;
        var insertIndex = 0;

        for (i=0;i<elements.length;i++) {                
            if (elements[i].getAttribute("ID") != null) {
                if (elements[i].getAttribute("ID").substring(0, 6) == typeName) {
                    count++;
                    insertIndex = rowIndex;
                    lastElement = elements[i];
                } 
            }

            rowIndex++;
        }

        var newSlot = slotTable.insertRow(insertIndex);        
        
        // var newSlot = document.createElement("tr");        
        var ressCol = document.createElement("td");
        var maxQtyCol = document.createElement("td");
        var maxDurCol = document.createElement("td");
        var maxQtyTickCol = document.createElement("td");
        var actionCol = document.createElement("td");
        actionCol.align = 'middle';
        
        newSlot.appendChild(ressCol);
        newSlot.appendChild(maxQtyCol);
        newSlot.appendChild(maxDurCol);
        newSlot.appendChild(maxQtyTickCol);
        newSlot.appendChild(actionCol);        

        // leftCol.innerHTML = "&nbsp;";

        var sel = document.createElement("select");
        if (typeName == 'fromTo') {
            newSlot.id = typeName + 'Line' + (addedFromTo + 1);
            sel.name = typeName + 'RessSelect' + (addedFromTo + 1);
        } else {
            newSlot.id = typeName + 'Line' + (addedToFrom + 1);
            sel.name = typeName + 'RessSelect' + (addedToFrom + 1);
        }
        
        var currCount = 0;
        
        var baseSelect = document.getElementById("fromToRessSelect1");       
        
        for (i=0;i<baseSelect.options.length;i++) {        
            var newOption = document.createElement('option');
            newOption.text = baseSelect.options[i].text;         
            newOption.value = baseSelect.options[i].value;
            newOption.innerHTML = baseSelect.options[i].innerHTML;

            try {
                sel.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                sel.add(newOption); // IE only
            }            
        }                
        
        ressCol.appendChild(sel);
        
        var maxQtyInput = document.createElement("input");
        var maxDurInput = document.createElement("input");
        var maxQtyTickInput = document.createElement("input");
        maxQtyInput.type = 'text';
        maxQtyInput.maxLength = 7;
        maxQtyInput.size = 6;
        maxQtyInput.value = 0;        
        maxDurInput.type = 'text';
        maxDurInput.maxLength = 4;
        maxDurInput.size = 4;
        maxDurInput.value = 0;
        maxQtyTickInput.type = 'text';
        maxQtyTickInput.maxLength = 7;
        maxQtyTickInput.size = 6;
        maxQtyTickInput.value = 0;
        
        if (typeName == 'fromTo') {
            maxQtyInput.name = typeName + 'MaxQty' + (addedFromTo + 1);
            maxDurInput.name = typeName + 'MaxDur' + (addedFromTo + 1); 
            maxQtyTickInput.name = typeName + 'MaxQtyTick' + (addedFromTo + 1); 
        } else {
            maxQtyInput.name = typeName + 'MaxQty' + (addedToFrom + 1);
            maxDurInput.name = typeName + 'MaxDur' + (addedToFrom + 1); 
            maxQtyTickInput.name = typeName + 'MaxQtyTick' + (addedToFrom + 1); 
        }        
        
        maxQtyCol.appendChild(maxQtyInput);
        maxDurCol.appendChild(maxDurInput);
        maxQtyTickCol.appendChild(maxQtyTickInput);
                
        var imgField = document.createElement("img");
        imgField.setAttribute("id", 'delete_' + newSlot.id);
        imgField.setAttribute("src", './pic/cancel.jpg');
        createSafeListener(imgField,'click',function() { removeSlot(this); }); 
        actionCol.appendChild(imgField);                
                
        return sel;
    }       
    
    function removeSlot(elem) {
        var removeName = elem.id.replace('delete_','');        
        var elementToRemove = document.getElementById(removeName);              
        
        if (removeName.indexOf('fromTo') == 0) {
            if (activeFromTo < 2) {
                alert('You can\'t do that!');
                return;
            }
            
            activeFromTo--;
        } else if (removeName.indexOf('toFrom') == 0) {
            if (activeToFrom < 2) {
                alert('You can\'t do that!');
                return;
            }
            
            activeToFrom--;
        }

        var slotTable = document.getElementById("transportTable");
        var elements = slotTable.getElementsByTagName("TR");        

        for (i=0;i<elements.length;i++) {                
            if (elements[i].getAttribute("ID") == elementToRemove.id) {
                slotTable.deleteRow(i);
            }
        }                
    }
    
    function createSafeListener(toField, listenerType, functionToDo) {
        var appName = navigator.appName;
        
        if (appName == 'Microsoft Internet Explorer') {
            toField.attachEvent(listenerType, functionToDo);
        } else {
            toField.addEventListener(listenerType, functionToDo, false);
        }
    }    
</script>
<%
if (result != null) {
    if (result.isError()) {
%>
        <FONT color="red"><B><%= result.getMessage()%></B></FONT><BR><BR>
<%
    } else {
%>
        <FONT color="lightgreen"><B><%= result.getMessage()%></B></FONT><BR><BR>
<%                      
    }
}
%>
<FORM method="post" action="" Name="CreateRoute">
    <TABLE border="0" align="middle">
        <TR>
            <TD></TD>
            <TD align="right"><IMG onClick="toogleVisibility('startListTd');toogleVisibility('targetListTd');" style="width:20px; height:20px;" src="<%= GameConfig.picPath() %>pic/icons/list.png" ONMOUSEOVER="doTooltip(event,'Planetenlisten anzeigen')" ONMOUSEOUT="hideTip()"></TD>
            <TD></TD>
        </TR>
    <TR>
        <TD class="blue2" style="font-size: 12px; font-weight: bolder" width="100px"><%= ML.getMLStr("transport_lbl_startplanet", userId) %>:
        </TD>
        <TD>
            <INPUT ID="sourceId" name="sourceId" type="TEXT" MAXLENGTH="20" SIZE="10" value="<%= sourcePlanetId %>" onChange="setStart();" onKeyUp="resetResult(); setStart();">
        </TD>
        <TD id="startListTd" style="display: none;">
            <SELECT class="dark" id="startlist" onChange="insertStartFromList(document.getElementById('startlist').options[document.getElementById('startlist').selectedIndex].value);">
                <OPTION value="0"><--- None ---></OPTION>
                <% for(PlayerPlanet pp : Service.playerPlanetDAO.findByUserIdSortedByName(userId)) {
                at.viswars.model.System s = Service.systemDAO.findById(Service.planetDAO.findById(pp.getPlanetId()).getSystemId());
                %>
                <OPTION value="<%= pp.getPlanetId() %>"><%= pp.getName() %> (<%= s.getId() %>:<%= pp.getPlanetId() %>)</OPTION>                
                <% } %>
            </SELECT>
        </TD>
    </TR>
    <TR>
        <TD class="blue2" style="font-size: 12px; font-weight: bolder"><%= ML.getMLStr("transport_lbl_targetplanet", userId) %>:
        </TD>
        <TD>
            <INPUT ID="targetId" name="targetId" type="TEXT" MAXLENGTH="20" SIZE="10" value="<%= targetPlanetId %>" onChange="setTarget();" onKeyUp="resetResult(); setTarget();">
        </TD>        
        <TD id="targetListTd" style="display: none;">
          <SELECT class="dark" id="targetlist" onChange="insertTargetFromList(document.getElementById('targetlist').options[document.getElementById('targetlist').selectedIndex].value);">
                <OPTION value="0"><--- None ---></OPTION>
                <% for(PlayerPlanet pp : Service.playerPlanetDAO.findByUserIdSortedByName(userId)) {
                at.viswars.model.System s = Service.systemDAO.findById(Service.planetDAO.findById(pp.getPlanetId()).getSystemId());
                %>
                <OPTION value="<%= pp.getPlanetId() %>"><%= pp.getName() %> (<%= s.getId() %>:<%= pp.getPlanetId() %>)</OPTION>                
                <% } %>
            </SELECT>
        </TD>
    </TR>
    <TR>
        <TD colspan="3" align="center">
            <INPUT ID="verify" name="verify" type="BUTTON" value="<%= ML.getMLStr("transport_but_validate", userId) %>" ONCLICK="checkStartAndTarget();" />&nbsp;&nbsp;<SPAN ID="response"></SPAN>
        </TD>
    </TR>
</TABLE>
<BR>
<TABLE ID="transportTable" width="80%" class="blue">
    <TR class="blue2">
        <TD class="blue" align="middle" colspan="5"><B><%= ML.getMLStr("transport_lbl_planet", userId) %> <SPAN ID="start">?</SPAN> => <%= ML.getMLStr("transport_lbl_planet", userId) %> <SPAN ID="target">?</SPAN></B></TD>
    </TR>
    <TR class="blue2">
        <TD class="blue"><%= ML.getMLStr("transport_lbl_resource", userId) %></TD>
        <TD class="blue"><%= ML.getMLStr("transport_lbl_maxquantity", userId) %></TD>
        <TD class="blue"><%= ML.getMLStr("transport_lbl_maxduration", userId) %></TD>
        <TD class="blue"><%= ML.getMLStr("transport_lbl_maxquantitypertick", userId) %></TD>
        <TD class="blue" width="30">&nbsp</TD>
    </TR>        
    <TR ID="fromToLine1">
        <TD>
            <SELECT ID="fromToRessSelect1" NAME="fromToRessSelect1">
                <OPTION value="0">- <%= ML.getMLStr("transport_opt_noresource", userId) %> -</OPTION>
<%       
            Ressource[] ressources = RessourceService.getAllTransportableRessources(ud);            
            for (Ressource r : ressources) {
%>
                <OPTION VALUE="<%= r.getId() %>"><%= ML.getMLStr(r.getName(),userId) %></OPTION>
<%
            }
%>                
            </SELECT>            
        </TD>
        <TD><INPUT ID="fromToMaxQty1" NAME="fromToMaxQty1" type="TEXT" MAXLENGTH="7" SIZE="6" value="0"></TD>
        <TD><INPUT ID="fromToMaxDur1" NAME="fromToMaxDur1" type="TEXT" MAXLENGTH="4" SIZE="4" value="0"></TD>
        <TD><INPUT ID="fromToMaxQtyTick1" NAME="fromToMaxQtyTick1" type="TEXT" MAXLENGTH="7" SIZE="6" value="0"></TD>
        <TD></TD>
    </TR>    
    <TR ID="fromToAdd">
        <TD colspan="5" ONCLICK="addSlot(1);"><FONT size=-2><BR><U>(+) <%= ML.getMLStr("transport_link_addslot", userId) %></U></FONT></TD>
    </TR>       
    <TR class="blue2">
        <TD class="blue" align="middle" colspan="5"><B><%= ML.getMLStr("transport_lbl_planet", userId) %> <SPAN ID="target2">?</SPAN> => <%= ML.getMLStr("transport_lbl_planet", userId) %> <SPAN ID="start2">?</SPAN></B></TD>
    </TR>
    <TR class="blue2">
        <TD class="blue"><%= ML.getMLStr("transport_lbl_resource", userId) %></TD>
        <TD class="blue"><%= ML.getMLStr("transport_lbl_maxquantity", userId) %></TD>
        <TD class="blue"><%= ML.getMLStr("transport_lbl_maxduration", userId) %></TD>
        <TD class="blue"><%= ML.getMLStr("transport_lbl_maxquantitypertick", userId) %></TD>
        <TD class="blue" width="30">&nbsp</TD>
    </TR>          
    <TR ID="toFromLine1">
        <TD>
            <SELECT ID="toFromRessSelect1" NAME="toFromRessSelect1">
                <OPTION value="0">- <%= ML.getMLStr("transport_opt_noresource", userId) %> -</OPTION>
<%       
            for (Ressource r : ressources) {
%>
                <OPTION VALUE="<%= r.getId() %>"><%= ML.getMLStr(r.getName(),userId) %></OPTION>
<%
            }
%>                
            </SELECT>
        </TD>
        <TD><INPUT ID="toFromMaxQty1" NAME="toFromMaxQty1" type="TEXT" MAXLENGTH="7" SIZE="6" value="0"></TD>
        <TD><INPUT ID="toFromMaxDur1" NAME="toFromMaxDur1" type="TEXT" MAXLENGTH="4" SIZE="4" value="0"></TD>
        <TD><INPUT ID="toFromMaxQtyTick1" NAME="toFromMaxQtyTick1" type="TEXT" MAXLENGTH="7" SIZE="6" value="0"></TD>
        <TD></TD>
    </TR>    
    <TR ID="toFromAdd">
        <TD colspan="5" ONCLICK="addSlot(2);"><FONT size=-2><BR><U>(+) <%= ML.getMLStr("transport_link_addslot", userId) %></U></FONT></TD>
    </TR>         
</TABLE>
<BR>
<CENTER><INPUT type="SUBMIT" name="submit" value="<%= ML.getMLStr("transport_but_create", userId) %>"/></CENTER>
</FORM>