<%-- 
    Document   : tradeRouteView
    Created on : 28.02.2009, 14:59:34
    Author     : Stefan
--%>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.trade.TradeRouteInfo" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.databuffer.RessourceBuffer"%>
<%@page import="at.viswars.databuffer.RessourceEntry"%>
<%@page import="at.viswars.service.TradeService"%>
<%
	session = request.getSession();
	int userId = Integer.parseInt((String) session.getAttribute("userId"));
	int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
	int systemId = Integer.parseInt((String) session.getAttribute("actSystem"));

        TradeUtilities tu = new TradeUtilities(userId, planetId, systemId);
        
	LinkedList<TradeRouteInfo> allroutes = tu.getAllInternalRoutes();
	if (allroutes.size() != 0) {
%>    
    
    <TABLE width="80%" class="blue">
        <TR><TD class="blue" colspan="4" align="center"><B>Interne Handelsrouten</B></TD></TR>
        <TR>
            <TD class="blue">Handelsroute (Rohstoff)</TD><TD width="100" class="blue">Eff.</TD><TD width="100" class="blue">Kapazit&auml;t/Tick</TD><TD class="blue">Aktion</TD>
        </TR>
    <%
    	for (TradeRouteInfo tri : allroutes) {
    					String ressName = RessourceBuffer.getRessourceById(
                                                        tri.getRessId()).getName(); 
    					String sPlanet = TradeService.getPlanetName(tri.getStartPlanet());
    					String tPlanet = TradeService.getPlanetName(tri.getTargetPlanet());
                                        double efficiency = tri.getEfficiency();                                        
                                        if (tri.getCapacity() == 0) efficiency = 0;
    %>
    <TR><TD><%=sPlanet%> --&gt; <%=tPlanet%> (<%=ressName%>)</TD><TD><%= FormatUtilities.getFormattedDecimal(efficiency, 1) + " %" %></TD><TD><%=tri.getCapacity()%></TD><TD width="80"><TABLE cellspacing=0 cellpadding=0><TR>
                <TD><IMG alt="Handelsroute L&ouml;schen" src="<%=GameConfig.picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'Handelsroute l&ouml;schen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=trade&type=4&routeId=<%=tri.getId()%>&action=1'"></IMG></TD>
                <TD><IMG alt="Schiffe hinzuf&uuml;gen" src="<%=GameConfig.picPath()%>pic/addShips.jpg" onmouseover="doTooltip(event,'Schiffe hinzuf&uuml;gen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=trade&type=4&routeId=<%=tri.getId()%>&action=2'"></IMG></TD>
<%
	if (tri.getCapacity() > 0) {
%>                    
                <TD><IMG alt="Schiffe entfernen" src="<%=GameConfig.picPath()%>pic/removeShips.jpg" onmouseover="doTooltip(event,'Schiffe entfernen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=trade&type=4&routeId=<%=tri.getId()%>&action=3'"></IMG></TD>
<%
	}
	if (tri.isTransmitterPossible() && tri.getTransmitterUsage() == 0) {
%>                    
                <TD><IMG alt="Transmitter zuschalten" src="<%=GameConfig.picPath()%>pic/icons/transmitter_on.gif" onmouseover="doTooltip(event,'Transmitter zuschalten')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=trade&type=4&routeId=<%=tri.getId()%>&action=5'"></IMG></TD>
<%
        }
	if (tri.getTransmitterUsage() > 0) {
%>                    
                <TD><IMG alt="Transmitterkapazit&auml;t &auml;ndern" src="<%=GameConfig.picPath()%>pic/icons/transmitter.gif" onmouseover="doTooltip(event,'Transmitterkapazit&auml;t &auml;ndern')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=trade&type=4&routeId=<%=tri.getId()%>&action=6'"></IMG></TD>
<%
	}                                                       
%>            
            </TR></TABLE>
            </TD>
        </TR>
    <%
    	}
    %>
    </TABLE>    
    <BR>
<%	
}
			allroutes = tu.getAllExternalRoutes();
			if (allroutes.size() != 0) {
%>    
    <TABLE width="80%" class="blue">
        <TR><TD class="blue" colspan="3" align="center"><B>Externe Handelsrouten</B></TD></TR>
        <TR>
            <TD class="blue">Handelsroute (Rohstoff)</TD><TD width="100" class="blue">Kapazit&auml;t/Tick</TD><TD class="blue">Aktion</TD>
        </TR>
    <%
    	for (TradeRouteInfo tri : allroutes) {
            String ressName = "";

            RessourceEntry re = RessourceBuffer.getRessourceById(tri.getRessId());
            ressName = re.getName();
            
            String sPlanet = TradeService.getPlanetName(tri.getStartPlanet());
            String tPlanet = TradeService.getPlanetName(tri.getTargetPlanet());
    %>
        <TR><TD><%=sPlanet%> --&gt; <%=tPlanet%> (<%=ressName%>) [<%=Alliance.getUserName(tri
											.getTargetUserId())%>]</TD><TD><%=tri.getCapacity()%></TD><TD width="80"><TABLE cellspacing=0 cellpadding=0><TR>
                <TD><IMG alt="Handelsroute L&ouml;schen" src="<%=GameConfig.picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'Handelsroute l&ouml;schen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=trade&type=4&routeId=<%=tri.getId()%>&action=1'"></IMG></TD>
                <TD><IMG alt="Schiffe hinzuf&uuml;gen" src="<%=GameConfig.picPath()%>pic/addShips.jpg" onmouseover="doTooltip(event,'Schiffe hinzuf&uuml;gen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=trade&type=4&routeId=<%=tri.getId()%>&action=2'"></IMG></TD>
<%
	if (tri.getCapacity() > 0) {
%>                    
                <TD><IMG alt="Schiffe entfernen" src="<%=GameConfig.picPath()%>pic/removeShips.jpg" onmouseover="doTooltip(event,'Schiffe entfernen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=trade&type=4&routeId=<%=tri.getId()%>&action=3'"></IMG></TD>
<%
	}
%>            
            </TR></TABLE>
            </TD>
        </TR>
    <%
    	}
    %>
        </TABLE>  
    <%
        }
    %>          