<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="at.viswars.ships.*"%>
<%@page import="at.viswars.DebugBuffer.DebugLevel"%>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.model.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.result.*"%>
<%@page import="at.viswars.planetcalc.*"%>
<%@page import="at.viswars.buildable.*"%>
<%@page import="at.viswars.enumeration.*"%>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int type = 1;
int detail = 0;

if (request.getParameter("type") != null) {
    type = Integer.parseInt((String)request.getParameter("type"));
}

ConstructionUtilities cons = new ConstructionUtilities(userId,planetId);

Map<String, String[]> allPars = request.getParameterMap();
ArrayList<String> errors = new ArrayList<String>();

if (request.getParameter("submit") != null) {
    if (type == 1) {
        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Build ships");
        errors = ProductionService.buildShip(userId, planetId, allPars);
    } else if (type == 2) {
        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Build ground troops");
        errors = ProductionService.buildGroundTroop(userId, planetId, allPars);
    }
}

/*
MutableRessourceCost lowRess = null;
if (type == 0) {
    type = 1;
} else {
    lowRess = cons.Produce(allPars,type);
}


if (lowRess != null) {
    showLowRess = true;
}
*/

// Change Priority
if ((request.getParameter("oNo") != null) && (request.getParameter("priority") != null)) {
    int orderNo = Integer.parseInt((String)request.getParameter("oNo"));
    int priority = Integer.parseInt((String)request.getParameter("priority"));
    ProductionService.setNewPriority(orderNo,priority);
}

if (request.getParameter("detail") != null) {
    detail = Integer.parseInt((String)request.getParameter("detail"));
} else {
    detail = 1;
}

ProductionBuildResult pbr = null;
if (type == 1) {
    pbr = ProductionService.getProductionListShips(userId, planetId);
} else if (type == 2) {
    pbr = ProductionService.getProductionListGroundTroops(userId, planetId);
}

for (String error : errors) {
%>
    <%= error %><BR>
<%
}
%>
  <TABLE width="90%">
      <TR align="center">
          <TD width="50%"><A href="main.jsp?page=new/production&type=1"><%= ML.getMLStr("production_lbl_ships", userId) %></A></TD>
          <TD width="50%"><A href="main.jsp?page=new/production&type=2"><%= ML.getMLStr("production_lbl_groundtroops", userId) %></A></TD>
      </TR>
      <TR align="center">
          <TD width="50%"><U><A href="main.jsp?page=new/production&type=4"><%= ML.getMLStr("production_lbl_shipdesign", userId) %></A></U></TD>
          <TD></TD>
      </TR>
  </TABLE><BR>
<%
long modulePoints = 0;
long dockPoints = 0;
long orbDockPoints = 0;
long kasPoints = 0;

if(type == 4){
    %>
        <jsp:include page="shipdesignoverview.jsp" />
    <%}else{
if ((type == 1) || (type == 2)) {
    PlanetCalculation pc = new PlanetCalculation(planetId);
    ProductionResult pr = pc.getPlanetCalcData().getProductionData();
    
    modulePoints = pr.getRessProduction(at.viswars.model.Ressource.MODUL_AP);
    dockPoints = pr.getRessProduction(at.viswars.model.Ressource.PL_DOCK);
    orbDockPoints = pr.getRessProduction(at.viswars.model.Ressource.ORB_DOCK);
    kasPoints = pr.getRessProduction(at.viswars.model.Ressource.KAS);
    
    
    if ((modulePoints + dockPoints + orbDockPoints + kasPoints) > 0) {
%>
<TABLE style="font-size:13px" width="60%">
    <TR class="blue2" align="middle"><TD colspan=4><B><%= ML.getMLStr("production_lbl_productionpertick", userId) %></B></TD>
    </TR>
    <TR align="middle">
        <TD><B><%= ML.getMLStr("production_lbl_barracks", userId) %></B></TD>
        <TD><B><%= ML.getMLStr("production_lbl_modulefactory", userId) %></B></TD>
        <TD><B><%= ML.getMLStr("production_lbl_planetaryshipyards", userId)%></B></TD>
        <TD><B><%= ML.getMLStr("production_lbl_orbitalshipyards", userId) %></B></TD></TR>
        <TR align="middle"><TD><%= kasPoints%></TD>
            <TD><%= modulePoints%></TD>
            <TD><%= dockPoints%></TD>
            <TD><%= orbDockPoints%></TD>
    </TR>
    <TR><TD>&nbsp;</TD></TR>
</TABLE>
<%
    }
}    
/*
Statement stmt2 = conn.createStatement();
ResultSet rs = stmt.executeQuery("select buildingId, timeFinished, number, actionType from actions where (actionType=3 OR actionType=4 or actionType=5) and planetId="+planetId+" order by timeFinished asc");
*/
InConstructionResult inConsLocal = ProductionService.getInConstructionData(planetId);
InConstructionResult inConsRemote = ProductionService.getInConstructionDataOtherPlanets(userId,planetId);


if(!inConsLocal.getEntries().isEmpty()) {
%>
    <table cellpadding="0" cellspacing="0" width="90%"><tr><td>
    <TABLE align="center" class="blue" border="0" cellpadding="0" cellspacing="0" width="90%">
        <TR bgcolor="#808080" >
            <td class="blue" width="*"><B><%= ML.getMLStr("production_lbl_ongoingproduction", userId) %></B></td>
            <td class="blue" width="50" align="center"><B><%= ML.getMLStr("production_lbl_quantity", userId) %></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_industry", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_industry", userId)%></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_module", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_module", userId)%></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_shipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_shipyard", userId)%></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_orbitalshipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_orbitalshipyard", userId)%></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_barracks", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_barracks", userId)%></B></td>
            <td class="blue" width="73" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_priority", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_priority", userId)%></B></td>
            <td class="blue" width="25"><B>&nbsp;</B></td>
        </TR>
    <%  for (InConstructionEntry ice : inConsLocal.getEntries()) {
        
            String name = "N/A";
            if (ice.getOrder().getType() == EProductionOrderType.GROUNDTROOPS) {
                name = ML.getMLStr(((GroundTroop)ice.getUnit()).getName(), userId);
            } else if ((ice.getOrder().getType() == EProductionOrderType.PRODUCE) ||
                        (ice.getOrder().getType() == EProductionOrderType.SCRAP) ||
                        (ice.getOrder().getType() == EProductionOrderType.UPGRADE)) {
                name = ((ShipDesign)ice.getUnit()).getName();
            }

            String extensionTag = "";

            if (ice.getOrder().getType() == EProductionOrderType.SCRAP) extensionTag = " (A)";
            if (ice.getOrder().getType() == EProductionOrderType.UPGRADE) extensionTag = " (U)";
    %>
            <tr>
<%
                if (ice.getAction().getUserId() == userId) {
%>
                <td><%= name + extensionTag %></td>
<%
                } else {
                    User u = MainService.findUserByUserId(ice.getAction().getUserId());
%>
                <td><FONT color="lightblue"><%= name + extensionTag + " [" + u.getGameName() + "]" %></FONT></td>
<%
                }
%>
                <td align="center"><%= ice.getAction().getNumber() %></td>
<%
                // Rebuild to Industry
                if (ice.getOrder().getIndNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getIndPerc(),0) %> %</td>
<%
                }
                if (ice.getOrder().getModuleNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getModulePerc(),0) %> %</td>
<%
                }
                if (ice.getOrder().getDockNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getDockPerc(),0) %> %</td>
<%
                }
                if (ice.getOrder().getOrbDockNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getOrbDockPerc(),0) %> %</td>
<%
                }
                if (ice.getOrder().getKasNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getKasPerc(),0) %> %</td>
<%
                }
            
            
                switch(ice.getOrder().getPriority()) {
                    case(0):
						%><td>
                    		<SELECT id="priority<%= ice.getOrder().getId() %>" name="priority<%= ice.getOrder().getId() %>" onchange="window.location.href='main.jsp?page=new/production&type=<%= type %>&detail=<%= detail %>&oNo=<%= ice.getOrder().getId() %>&priority='+document.getElementById('priority<%= ice.getOrder().getId() %>').value;">
                        		<option value="0" selected><%= ML.getMLStr("production_opt_priority_high", userId) %></option>
                        		<option value="1"><%= ML.getMLStr("production_opt_priority_average", userId) %></option>
                        		<option value="2"><%= ML.getMLStr("production_opt_priority_low", userId) %></option>
                    		</SELECT>
                		</td><%
                        break;
                    case(1):
						%><td>
                    		<SELECT id="priority<%= ice.getOrder().getId() %>" name="priority<%= ice.getOrder().getId() %>" onchange="window.location.href='main.jsp?page=new/production&type=<%= type %>&detail=<%= detail %>&oNo=<%= ice.getOrder().getId() %>&priority='+document.getElementById('priority<%= ice.getOrder().getId() %>').value;">
                        		<option value="0"><%= ML.getMLStr("production_opt_priority_high", userId) %></option>
                        		<option value="1" selected><%= ML.getMLStr("production_opt_priority_average", userId) %></option>
                        		<option value="2"><%= ML.getMLStr("production_opt_priority_low", userId) %></option>
                    		</SELECT>
                		</td><%
                        break;
                    case(2):
						%><td>
                    		<SELECT id="priority<%= ice.getOrder().getId() %>" name="priority<%= ice.getOrder().getId() %>" onchange="window.location.href='main.jsp?page=new/production&type=<%= type %>&detail=<%= detail %>&oNo=<%= ice.getOrder().getId() %>&priority='+document.getElementById('priority<%= ice.getOrder().getId() %>').value;">
                        		<option value="0"><%= ML.getMLStr("production_opt_priority_high", userId) %></option>
                        		<option value="1"><%= ML.getMLStr("production_opt_priority_average", userId) %></option>
                        		<option value="2" selected><%= ML.getMLStr("production_opt_priority_low", userId) %></option>
                    		</SELECT>
                		</td><%
                        break;
                }
                if (ice.getOrder().getType() == EProductionOrderType.PRODUCE) {
                %><!--
                <td align="middle">
                    <IMG src="<%=GameConfig.picPath()%>pic/cancel.jpg" onclick="window.location.href='main.jsp?page=stopOrder&type=<%= type %>&oId=<%= ice.getOrder().getId() %>';" onmouseover="doTooltip(event,'Bauauftrag abbrechen')" onmouseout="hideTip()" />
                </td>-->
                <%
                }
             	%></tr><%
       /*
        }
        */
        } %>
</TABLE></td></tr></table>
<BR>
<%
}

if(!inConsRemote.getEntries().isEmpty()) {
%>
    <table cellpadding="0" cellspacing="0" width="90%"><tr><td>
    <TABLE class="blue" border="0" cellpadding="0" cellspacing="0" width="100%">
        <TR bgcolor="#808080" >
            <td class="blue" width="*"><B><%= ML.getMLStr("production_lbl_ongoingproductionotherplanets", userId) %></B></td>
            <td class="blue" width="50" align="center"><B><%= ML.getMLStr("production_lbl_quantity", userId) %></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_industry", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_industry", userId)%></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_module", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_module", userId)%></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_shipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_shipyard", userId)%></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_orbitalshipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_orbitalshipyard", userId)%></B></td>
            <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_barracks", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_barracks", userId)%></B></td>
        </TR>
    <%  for (InConstructionEntry ice : inConsRemote.getEntries()) {
        
            String name = "N/A";
            if (ice.getOrder().getType() == EProductionOrderType.GROUNDTROOPS) {
                name = ML.getMLStr(((GroundTroop)ice.getUnit()).getName(), userId);
            } else if ((ice.getOrder().getType() == EProductionOrderType.PRODUCE) ||
                        (ice.getOrder().getType() == EProductionOrderType.SCRAP) ||
                        (ice.getOrder().getType() == EProductionOrderType.UPGRADE)) {
                name = ((ShipDesign)ice.getUnit()).getName();
            }

            String extensionTag = "";
            if (ice.getOrder().getType() == EProductionOrderType.SCRAP) extensionTag = " (A)";
            if (ice.getOrder().getType() == EProductionOrderType.UPGRADE) extensionTag = " (U)";

            PlayerPlanet pp = PlanetService.getPlanetName(ice.getAction().getPlanetId());
%>

            <tr>
<%
                if (pp.getUserId() != userId) { %>
                <td><FONT color="lightblue"><%= name + extensionTag + " [Planet: " + pp.getName() + " (" + pp.getPlanetId() + ")]"%></FONT></td>
<%              } else  { %>
                <td><%= name + extensionTag + " [Planet: " + pp.getName() + " (" + pp.getPlanetId() + ")]" %></td>
<%
                }
%>
                <td align="center"><%= ice.getAction().getNumber() %></td>
<%
                // Rebuild to Industry
                if (ice.getOrder().getIndNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getIndPerc(),0) %> %</td>
<%
                }
                if (ice.getOrder().getModuleNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getModulePerc(),0) %> %</td>
<%
                }
                if (ice.getOrder().getDockNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getDockPerc(),0) %> %</td>
<%
                }
                if (ice.getOrder().getOrbDockNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getOrbDockPerc(),0) %> %</td>
<%
                }
                if (ice.getOrder().getKasNeed() == 0) {
%>
                <td align="center">-</td>
<%
                } else {
%>
                <td align="center"><%= FormatUtilities.getFormattedDecimal((float)ice.getOrder().getKasPerc(),0) %> %</td>
<%
                }
             	%></tr><%
        } %>
</TABLE></td></tr></table><BR>
<%
}

// Major check checks if needed buildings are built on this planet
boolean shipyard = true;
boolean orbitalShipyard = true;
boolean modulefactory = true;
boolean barracks = true;

if (type == 1) {
    shipyard = ProductionService.isConstructed(planetId, Construction.ID_PLANETARYSHIPYARD);
    orbitalShipyard = ProductionService.isConstructed(planetId, Construction.ID_ORBITAL_SHIPYARD);
}
if (type == 2) {
    barracks = ProductionService.isConstructed(planetId, Construction.ID_BARRACKS);
}
if (type > 2) {
    modulefactory = ProductionService.isConstructed(planetId, Construction.ID_MODULEFACTORY);
    modulefactory = false;
}


boolean showExtHashMap = false;
if (!( (type == 0) || ((type == 1) && !shipyard) || ((type == 2) && !barracks) || ((type > 2) && !modulefactory) )) {
    if ((detail != 0) || (type < 4)) {
        if (pbr.getUnitsWithPossibleTech().size() == 0) { %>
        <CENTER><FONT style="font-size:13px;">Kein Schiffsdesign verf&uuml;gbar</FONT></CENTER>
<%      } else {
        %>
        <FORM method="post" action="main.jsp?page=new/production&type=<%= type %>&detail=<%= detail %>" Name="Production">
        <INPUT type="submit" id="submit" name="submit" value="<%= ML.getMLStr("production_but_build", userId) %>">
        <% if(type == 2){%>
        <BR>
        <BR>
        <FONT size="2" color="FFFF00"><B><%= ML.getMLStr("production_lbl_effective", userId) %></B></FONT>&nbsp;
            <FONT color="FF0000" size="2"><B><%= ML.getMLStr("production_lbl_ineffective", userId) %></B></FONT>
        <% } %>
        <P>
<%
        }
    }
    for(Buildable c : pbr.getUnitsWithPossibleTech()) {         
        RessourcesEntry cost = c.getRessCost();
        ShipDesignDetailed sdd = null;
        String name = "N/A";
        String image = "";
        int crew = 0;
        int id = 0;
        
        BuildableResult br = pbr.getBuildInfo(c);
        double buildTime = Double.MIN_VALUE;

            ShipDesign sd = (ShipDesign)c.getBase();
            sdd = new ShipDesignDetailed(sd.getId());
            ShipDesignExt sde = (ShipDesignExt)c;
        if (type == 1) {            
            
            if (sde.getChassis().getMinBuildQty() > 1) {
                name = sdd.getName() + "-Geschwader" + " [&aacute; " + sde.getChassis().getMinBuildQty() + " "+ML.getMLStr(sde.getChassis().getName(),userId)+"]";
            } else {
                 name = sdd.getName() + " [" + ML.getMLStr(sde.getChassis().getName(), userId) + "]";
             }
            crew = sd.getCrew();
            id = sd.getId();

            showExtHashMap = true;
        } else if (type == 2) {
            GroundTroop gt = (GroundTroop)c.getBase();
            name = ML.getMLStr(gt.getName(), userId);
            crew = gt.getCrew();
            image = gt.getImage();
            id = gt.getId();
        }
        
        // calc Buildtime
        if (dockPoints > 0) buildTime = Math.max(0,((double)c.getBuildTime().getDockPoints() / (double)dockPoints));
        if (orbDockPoints > 0) buildTime = Math.max(buildTime,((double)c.getBuildTime().getOrbDockPoints() / (double)orbDockPoints));
        if (kasPoints > 0) buildTime = Math.max(buildTime,((double)c.getBuildTime().getKasPoints() / (double)kasPoints));
        if (modulePoints > 0) buildTime = Math.max(buildTime,((double)c.getBuildTime().getModulePoints() / (double)modulePoints));
        if ((orbDockPoints == 0) && (c.getBuildTime().getOrbDockPoints() > 0)) buildTime = Double.MAX_VALUE;
        if ((dockPoints == 0) && (c.getBuildTime().getDockPoints() > 0)) buildTime = Double.MAX_VALUE;
        if ((modulePoints == 0) && (c.getBuildTime().getModulePoints() > 0)) buildTime = Double.MAX_VALUE;        
        if ((kasPoints == 0) && (c.getBuildTime().getKasPoints() > 0)) buildTime = Double.MAX_VALUE;  
        
        boolean disabled = br.getPossibleCount() == 0;
%>
        <table style="font-size: 12px">
                 <tr class="blue2">
    <td>
       Dockpoints
    </td>
    <td>
        Orb-Dockpoints
    </td>
    <td>
        Mod Points
    </td>
</tr><tr>
    <td>
        <%= sde.getBuildTime().getDockPoints() %>
    </td>
    <td>
        <%= sde.getBuildTime().getOrbDockPoints() %>
    </td>
    <td>
        <%= sde.getBuildTime().getModulePoints() %>
    </td>
</tr>
                 <tr class="blue2">
    <td>
       Dock Ticks
    </td>
    <td>
        Orb Ticks
    </td>
    <td>
        Mod Ticks
    </td>
</tr><tr>
    <td>
        <% if (dockPoints > 0) { %><%= (Math.max(0,((double)c.getBuildTime().getDockPoints() / (double)dockPoints))) %><% } %>
    </td>
    <td>
        <% if (orbDockPoints > 0) { %><%= (Math.max(0,((double)c.getBuildTime().getOrbDockPoints() / (double)orbDockPoints)))  %><% } %>
    </td>
    <td>
        <% if (modulePoints > 0) { %><%= (Math.max(0,((double)c.getBuildTime().getModulePoints() / (double)modulePoints))) %><% } %>
    </td>
</tr>
        </table>
        <TABLE class="blue" border=0 width="80%">
            <TR bgcolor="#808080">
                <TD class="blue" width="60%"><B><%= name %><% if (type == 2) { %> (<%= FormatUtilities.getFormattedNumber(ProductionService.findGroundsTroopBy(id, planetId, userId)) %>)<% } %>
                    </B></TD>
            <TD class="blue" width="40%" align="right">(max: <%=FormatUtilities.getFormattedNumber(br.getPossibleCount())%>) <%= ML.getMLStr("production_lbl_quantity", userId) %>:
            <INPUT <% if (disabled) out.write("DISABLED"); %> NAME="build<%= id %>" TYPE="text" VALUE="0" SIZE="5" ALIGN=middle></TD>
            </TR>
        </TABLE>
            <TABLE class="blue" style="font-size:13px;" border=0 width="80%">
<%
            if ((type == 1) && (sdd.needsOrbitalDock() && !orbitalShipyard)) { %>
            <TR valign="top"><TD colspan=2 align=left width="100%"><B><FONT COLOR="yellow">Keine Orbitalwerft zur Fertigstellung vorhanden!</FONT></B></TD></TR>
<%          } %>
            <TR valign="top">
                <TD width="180px">
                    <% if(!image.equals("")){ %>
                    <IMG alt="<%= name %>" src="<%= GameConfig.picPath() %>pic/production/<%= image %>"/>
                    <% } %>
              </TD>
<%
            if (showExtHashMap) { %>
            <TD valign="middle" align=left width="50%">
<%
            } else { %>
                <TD valign="middle" align=left width="100%">
<%
            }%>

            <% if(type == 2) {
            GroundTroop gt = (GroundTroop)c.getBase();
                %>
                <%=  ML.getMLStr(gt.getDescription(),userId) %><BR><BR>
            <% }
            if (buildTime == Double.MAX_VALUE) { %>
            <%= ML.getMLStr("construction_lbl_constructiontime", userId) %>: &#8734;<BR>
   <%       } else if (buildTime < 1) { %>
            <%= ML.getMLStr("construction_lbl_constructiontime", userId) %>: <1 [<%= (int)Math.floor(1 / buildTime) %> / Tick]<BR>
   <%       } else { %>
            <%= ML.getMLStr("construction_lbl_constructiontime", userId) %>: <%= (int)Math.floor(buildTime) %><BR>
   <%       }
            
               if (crew > 0) { %>
                Crew: <%= FormatUtilities.getFormattedNumber(crew) %> <BR>
            <% }

                ArrayList<RessAmountEntry> ress = cost.getSortedRessArray();
                if(ress.size() > 0){
                    %>
                    <TABLE  style="font-size:13px">
                    <%
                    for (RessAmountEntry rce : cost.getSortedRessArray()) {
                        %>
                        <TR>
                            <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(ProductionService.findRessource(rce.getRessId()).getName(), userId) %>" height="20" width="20" src="<%= GameConfig.picPath() %><%= ProductionService.findRessource(rce.getRessId()).getImageLocation() %>"/></TD>
                        <%
                        if (rce.getQty() <= 0) continue;
                        //out.print("<IMG  alt='" + ML.getMLStr(ProductionService.findRessource(rce.getRessId()).getName(), userId)+ "' height='20' width='20' src='" + GameConfig.picPath() + ProductionService.findRessource(rce.getRessId()).getImageLocation() +"'/>");
                        out.println("<TD>" + ML.getMLStr(ProductionService.findRessource(rce.getRessId()).getName(),userId) + ": " + FormatUtilities.getFormattedDecimalCutEndZero(rce.getQty(),2) + "</TD>");

                        %>
                        </TR>
                        <%
                    }
                    %>
                    </TABLE>
                    <%
                }
            %>
            </TD>
<%

            if (showExtHashMap) {
%>
            <TD width="50%">
<%
            for (Map.Entry<Integer, Integer> me : sdd.getModuleCount().entrySet()) {
                Module mData = ProductionService.getModule(me.getKey());

                name = "";

                if (mData == null) {
                    name = "<FONT color=\"red\">Ung&uuml;ltiges Modul ("+me.getKey()+")</FONT>";
                } else {
                    name = ML.getMLStr(mData.getName(), userId);
                }

                int count = me.getValue(); %>
                <%= name %>: <%= count %><BR>
<%
            }
%>
            </TD>
<%          }

                %>
            </TR>

            <%
            if(type == 2){

                ArrayList<GroundTroop> gts = ProductionService.findAllGroundTroops();
                GTRelationResult gtrr = ProductionService.findGTRelationsBy(id);
                %>
            <TR>
                <TD colspan="2">
                    <TABLE class="blue2" border="0"  style="font-size:13px;" width="100%">
                     <%
                 for(int decisioner = 0; decisioner <= 1; decisioner++){
            %>
            <TR>
            <%
                     boolean first = false;
              for(GroundTroop gt : gts){
                  %>


                  <%
                  GTRelation gtr = gtrr.getGtRelations().get(gt.getId());
                  boolean effective = false;
                  boolean nonEffective = false;
                  Double damage = gtrr.getDamage().get(gt.getId());
                  if(damage == null){
                      damage = 0d;
                  }

                  if(damage > (gtrr.getMeanDamage() * 1.4d)){
                      effective = true;
                      }

                  if(damage < (gtrr.getMeanDamage() * 0.2d)){
                      nonEffective = true;
                  }
                 String attackPoints = "~0";
                 String attackProb = "~0";
                 String hitProb = "~0";
                 if(gtr != null){
                  attackPoints = String.valueOf(gtr.getAttackPoints());
                  attackProb = String.valueOf(gtr.getAttackProbab());
                  hitProb = String.valueOf(gtr.getHitProbab());
                 }

                     switch(decisioner){
                             case(1):
                             if(first){
                                 %>
                                 <TD>Name</TD>
                                 <%
                                 first = false;
                             }
                             if(effective){
                                 %>
                                 <TD align="center" width="10%" class="blue2"><FONT color="FFFF00"><B><%= ML.getMLStr(gt.getName(), userId) %></B></FONT></TD>
                                 <%
                                 }else if(nonEffective){
                                 %>
                                <TD align="center" width="10%" class="blue2"><FONT color="FF0000"><B><%= ML.getMLStr(gt.getName(), userId) %></B></FONT></TD>
                                 <%
                                 }else{
                                 %>
                                <TD align="center" width="10%" class="blue2"><%= ML.getMLStr(gt.getName(), userId) %></TD>
                                 <%
                                 }
                            %>
                            
                            <%

                             break;
                             case(2):
                             if(first){
                                 %>
                                 <TD>AngriffsPunkte</TD>
                                 <%
                                 first = false;
                             }
                             %>
                                <TD><%= attackPoints %></TD>                            
                            <%
                             break;
                             case(3):
                             if(first){
                                 %>
                                 <TD>AngriffsWahrscheinlichkeit</TD>
                                 <%
                                 first = false;
                             }
                            %>
                            <TD><%= attackProb %></TD>
                            <%
                             break;
                             case(4):
                             if(first){
                                 %>
                                 <TD>Trefferwahrscheinlcihkeit</TD>
                                 <%
                                 first = false;
                             }
                            %>
                            <TD><%= hitProb %></TD>
                            <%
                             break;
                        }
                     }
            %>
            </TR>
            <%
                 }
                %>            
        </TABLE></TD></TR>             
                <%
    } %>       
        </TABLE>
    <BR>
<%
    
    }
    if ((detail != 0) || (type < 4)) {
        if (pbr.getUnitsWithPossibleTech().size() > 0) {
        %>
            <BR><INPUT type="submit" id="submit" name="submit" value="<%= ML.getMLStr("production_but_build", userId) %>">
        </FORM>
<%
        }
    }
} else {
	%> <FONT style="font-size:13px;"> <%
	switch (type) {
	case 1: %>Keine Schiffswerft vorhanden <%
			break;
	case 2:  %>Keine Kaserne vorhanden<%
			break;
	default: %>Momentan deaktiviert<%
	}
	%> </FONT> <%

    // <FONT style="font-size:13px;">Keine Modulfabrik vorhanden</FONT>
}

allPars.clear();
}
// stmt2.close();
%>

