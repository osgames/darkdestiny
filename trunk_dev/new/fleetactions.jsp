 <%@page import="at.viswars.model.PlayerPlanet"%>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.result.*" %>
<%@page import="at.viswars.movable.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.ships.*" %>
<%@page import="at.viswars.fleet.*" %>
<%@page import="at.viswars.enumeration.*" %>
<%@page import="at.viswars.requestbuffer.*" %>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.databuffer.*" %>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<script type="text/javascript">
var addedFromTo = 1;
var addedToFrom = 1;

var activeFromTo = 1;
var activeToFrom = 1;
    
function insertTargetPlanet(planetId){
        document.getElementById("ziel").value = planetId;
        document.getElementById("systemList").selectedIndex = 0;
        switchPlanetSystem(2);
    
}   
function insertTargetSystem(systemId){
        document.getElementById("ziel").value = systemId;
        document.getElementById("planetList").selectedIndex = 0;
        switchPlanetSystem(1);
    
}

function switchPlanetSystem(index){
    document.getElementById("target").selectedIndex = index;
}
</script>
<%


    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
    int systemId = Integer.parseInt((String) session.getAttribute("actSystem"));

    int sourcePage = 0;

    if (session.getAttribute("source") != null) {
        sourcePage = Integer.parseInt((String) session.getAttribute("source"));
    } else {
        %><jsp:forward page="main.jsp" >
        <jsp:param name="msg" value='Ung&uuml;ltiger Aufruf' />
        <jsp:param name="page" value='showerror' />
        </jsp:forward><%
        return;
    }

    FleetParameterBuffer fpb = null;
    if (request.getParameter("bufId") == null) {
        fpb = FleetService.createRequestBuffer(userId);
                
        fpb.setParameter(fpb.SOURCEPAGE, sourcePage);
        if (request.getParameter("toSystem") != null) {
            fpb.setParameter("toSystem", request.getParameter("toSystem"));
            fpb.setParameter("targetId", request.getParameter("targetId"));            
        }

        if (request.getParameter("fleetId") != null) {
            if (request.getParameter("isFF") != null) {
                // out.write("SET FLEET_ID TO FF" + request.getParameter("fleetId"));
                fpb.setParameter(FleetParameterBuffer.FLEET_ID, "FF" + (String)request.getParameter("fleetId"));
            } else {
                // out.write("SET FLEET_ID TO " + request.getParameter("fleetId"));
                fpb.setParameter(FleetParameterBuffer.FLEET_ID, (String)request.getParameter("fleetId"));
            }
        }
    } else {
        fpb = FleetService.getRequestBuffer(userId, Integer.parseInt((String)request.getParameter("bufId")));

        
        if (request.getParameter("target") != null) {
            fpb.setParameter("toSystem", request.getParameter("target"));
            fpb.setParameter("targetId", request.getParameter("ziel"));            
        }
        
        if (fpb.getFleetId() == 0) {
            fpb.setParameter("fleetId", request.getParameter("fleetId"));
        }
        fpb.setParameter("retreat", request.getParameter("retreat"));

        if (request.getParameter("retreatToType").equalsIgnoreCase("1")) {
            fpb.setParameter("retreatToType", ELocationType.SYSTEM);
        } else if (request.getParameter("retreatToType").equalsIgnoreCase("2")) {
            fpb.setParameter("retreatToType", ELocationType.PLANET);
        }
        fpb.setParameter("retreatTo", request.getParameter("retreatTo"));
        
        BaseResult br = FleetService.checkFlightRequest(userId, fpb);

        if (br.isError()) {
            %><jsp:forward page="../main.jsp" >
            <jsp:param name="msg" value='<%=br.getMessage()%>' />
            <jsp:param name="page" value='showerror' />
            </jsp:forward><%
        } else {
            %><jsp:forward page="../main.jsp" >
                <jsp:param name="bufId" value='<%= fpb.getId() %>' />
                <jsp:param name="page" value='confirm/fleetMovement' />
            </jsp:forward><%
         }
    }        
    
    boolean targetIsSystem = false;
    int targetId = 0;
    String destSystem = "x";
    String destPlanet = "x";
    FleetListResult flr = null; 
    
    switch (sourcePage) {
    case (FleetMovement.MOVE_FROM_SYSVIEW):
        flr = FleetService.getFleetList(userId, systemId, planetId, EFleetViewType.OWN_ALL);

        // Build a java script array with retreat info of all fleets
        out.println("<script language=\"Javascript\">");
        out.print("var fleetIds = new Array("+flr.getFormationAndSingleFleetCount()+");");
        out.print("var isFormation = new Array("+flr.getFormationAndSingleFleetCount()+");");
        out.print("var retreatFactor = new Array("+flr.getFormationAndSingleFleetCount()+");");
        out.print("var retreatToType = new Array("+flr.getFormationAndSingleFleetCount()+");");
        out.print("var retreatTo = new Array("+flr.getFormationAndSingleFleetCount()+");");        
        
        int index = 0;
        
        for (Movable m : flr.getAllItems()) {
            int retreatFactor = 100;
            ELocationType retreatToType = null;
            int retreatTo = 0;
            
            if (m instanceof FleetFormationExt) {
                FleetFormationExt ffe = (FleetFormationExt)m;
                if (ffe.getFleetOrder() != null) {
                    retreatFactor = ffe.getFleetOrder().getRetreatFactor();
                    retreatToType = ffe.getFleetOrder().getRetreatToType();
                    retreatTo = ffe.getFleetOrder().getRetreatTo();
                } 

                out.print("fleetIds["+index+"]="+ffe.getBase().getId()+";");
                out.print("isFormation["+index+"]=true;");
                out.print("retreatFactor["+index+"]="+retreatFactor+";");
                out.print("retreatToType["+index+"]="+retreatToType+";");
                out.print("retreatTo["+index+"]="+retreatTo+";");

                index++;                
            } else if (m instanceof PlayerFleetExt) {
                PlayerFleetExt pfe = (PlayerFleetExt)m;
                if (pfe.getFleetOrder() != null) {
                    retreatFactor = pfe.getFleetOrder().getRetreatFactor();
                    retreatToType = pfe.getFleetOrder().getRetreatToType();
                    retreatTo = pfe.getFleetOrder().getRetreatTo();
                } 

                out.print("fleetIds["+index+"]="+pfe.getBase().getId()+";");
                out.print("isFormation["+index+"]=false;");
                out.print("retreatFactor["+index+"]="+retreatFactor+";");
                out.print("retreatToType["+index+"]="+retreatToType+";");
                out.print("retreatTo["+index+"]="+retreatTo+";");

                index++;
            }
        }
%>
        function updateFields() {
            var actFleetId = document.getElementById("fleetid").value;
            var index = -1;
            
            for (i=0;i<fleetIds.length;i++) {
                if (fleetIds[i] == actFleetId) {
                    index = i;
                }
            }
            
            if (index > -1) {
                document.getElementById("retreat").value = retreatFactor[index];
                if (retreatToType[index] = 'SYSTEM') {
                    document.getElementById("retreatToType").selectedIndex = 1;
                } else if (retreatToType[index] = 'PLANET') {
                    document.getElementById("retreatToType").selectedIndex = 2;
                } else {
                    document.getElementById("retreatToType").selectedIndex = 0;
                }

                document.getElementById("retreatTo").value = retreatTo[index];
            }
        }
<%
        out.println("</script>");
        
        if (((String) request.getParameter("toSystem")).equalsIgnoreCase("true")) {
            targetIsSystem = true;
        } else {
            targetIsSystem = false;
        }
        targetId = Integer.parseInt((String) request.getParameter("targetId"));

        Statement stmt = DbConnect.createStatement();
        if (!targetIsSystem) {
            ResultSet rs = stmt.executeQuery("SELECT systemId FROM planet WHERE id="+ targetId);
            if (rs.next()) {
                destSystem = Integer.toString(rs.getInt(1));
                destPlanet = Integer.toString(targetId);
            }
        } else if (targetIsSystem) {
            destSystem = Integer.toString(targetId);
        }
        stmt.close();
        break;
    }

    int fleetId = 0;
    // 0 = military
    // 1 = planetControl
    // 2 = spionage
    int source = 0;

    if (request.getParameter("fleetId") != null) {
        source = 1;
        fleetId = Integer.parseInt(request.getParameter("fleetId"));
    }

    boolean fleetFormation = (request.getParameter("isFF") != null);
        
    switch (sourcePage) {
         case (FleetMovement.MOVE_FROM_FLEETVIEW):
            PlayerFleetExt pfe = null;
            FleetFormationExt ffe = null;

            String name;
            if (fleetFormation) {
                ffe = new FleetFormationExt(fleetId);
                name = "Flottenverband " + ffe.getBase().getName();
            } else {
                pfe = new PlayerFleetExt(fleetId);
                name = "Flotte " + pfe.getBase().getName();
            }             
            %><FONT style="font-size:13px"><B>Ziel und Auftrag f&uuml;r <%= name %></B></FONT><BR><%
             break;
         case (FleetMovement.MOVE_FROM_SYSVIEW):
             %><FONT style="font-size:13px"><B>Flotte f&uuml;r Ziel (<%=destSystem%>:<%=destPlanet%>) w&auml;hlen</B></FONT><BR><%
             break;
         }

    // Abfrage Parameter f&uuml;r Seitenanzeige
    int showfleet = 0;
    int showaction = 0;

    if (request.getParameter("fleetId") != null
            && request.getParameter("action") != null) {
        
        
        try {
            showfleet = Integer.parseInt(request.getParameter("fleetId"));
            showaction = Integer.parseInt(request.getParameter("action"));
        } catch (NumberFormatException e) {
            DebugBuffer.writeStackTrace("fleetactions (Parameter error): ", e);
            return;
        }
    }

    try {
        %><BR>
        <FORM method='post' action='main.jsp?page=new/fleetactions&bufId=<%= fpb.getId() %>' name='action'>
            <TABLE class="bluetrans" style="font-size:13px">
          
                <TR><%
                    if (sourcePage == FleetMovement.MOVE_FROM_FLEETVIEW) {
                        %><TD align="center" class="blue"><BR>Ziel</TD><%
                    } else {
                        %><TD align="center" class="blue"><BR>Flotte</TD><%
                    }
                    %><TD><%
                    if (sourcePage == FleetMovement.MOVE_FROM_FLEETVIEW) {
                        %><BR>
                            <select id="target" name="target">
                            <option value="">-- Bitte w&auml;hlen --</option>
                            <option value="true">System</option>
                            <option value="false">Planet</option>
                        </select>
                        &nbsp;
                        <input size="10" id="ziel" name="ziel" value=""/>
                        <IMG onClick="toogleVisibility('listTd');" style="width:20px; height:20px;" src="<%= GameConfig.picPath() %>pic/icons/list.png" ONMOUSEOVER="doTooltip(event,'Planeten- Systemliste anzeigen')" ONMOUSEOUT="hideTip()">
                        </TD>
                        <TD id="listTd" align="left" style="display:none;">
                            <TABLE style="font-size:10px;">
                                <TR>
                                    <TD class="blue" align="center">System</TD>
                                    <TD class="blue" align="center">Planet</TD>
                                </TR>
                                <TR>
                                    <TD align="center">
                                         <SELECT class="dark" id="systemList" onChange="insertTargetSystem(document.getElementById('systemList').options[document.getElementById('systemList').selectedIndex].value);">
                                            <OPTION value="0"><--- None ---></OPTION>
                                            <% 
                                            int lastSystemId = 0;
                                            for(PlayerPlanet pp : Service.playerPlanetDAO.findByUserIdSortedByName(userId)) {
                                                int sysId = Service.planetDAO.findById(pp.getPlanetId()).getSystemId();
                                                if(sysId == lastSystemId){
                                                    continue;
                                                }
                                                lastSystemId = systemId;
                                            at.viswars.model.System s = Service.systemDAO.findById(sysId);
                                            
                                            %>
                                            <OPTION value="<%= s.getId() %>"><%= s.getName() %> (<%= s.getId() %>:0)</OPTION>                
                                            <% } %>
                                        </SELECT>                                      
                                        
                                    </TD>
                                    <TD align="center">
                                         <SELECT class="dark" id="planetList" onChange="insertTargetPlanet(document.getElementById('planetList').options[document.getElementById('planetList').selectedIndex].value);">
                                            <OPTION value="0"><--- None ---></OPTION>
                                            <% for(PlayerPlanet pp : Service.playerPlanetDAO.findByUserIdSortedByName(userId)) {
                                            at.viswars.model.System s = Service.systemDAO.findById(Service.planetDAO.findById(pp.getPlanetId()).getSystemId());
                                            %>
                                            <OPTION value="<%= pp.getPlanetId() %>"><%= pp.getName() %> (<%= s.getId() %>:<%= pp.getPlanetId() %>)</OPTION>                
                                            <% } %>
                                        </SELECT>                                      
                                        
                                    </TD>
                                </TR>
                            </Table>
                        <%
                    } else if (sourcePage == FleetMovement.MOVE_FROM_SYSVIEW) {
                        // Show fleetlist
                        %><BR>
                        <select id="fleetId" name="fleetId" onChange="updateFields();">
                            <option value="0" selected>-- Flotte w&auml;hlen --</option><%
                            for (Movable m : flr.getAllItems()) {
                                if (m instanceof FleetFormationExt) {
                                    FleetFormationExt ffe2 = (FleetFormationExt)m;
                                    out.write("<OPTION value=\"FF" + ffe2.getBase().getId() + "\">"
                                        + ffe2.getBase().getName() + " [Flottenverband]" + " (" + ffe2.getBase().getSystemId() + ":"
                                        + ffe2.getBase().getPlanetId() + ")</option>");                                       
                                } else if (m instanceof PlayerFleetExt) {
                                    PlayerFleetExt pfe2 = (PlayerFleetExt)m;
                                    out.write("<OPTION value=\"" + pfe2.getBase().getId() + "\">"
                                        + pfe2.getBase().getName() + " (" + pfe2.getBase().getSystemId() + ":"
                                        + pfe2.getBase().getPlanetId() + ")</option>");                                    
                                }
                            }
                        %></select><%
                    }
                    %></TD>
                </TR>
                
                <TR>
                    <TD valign="top" align="center" class="blue"><%
                        switch (source) {
                            case (0):
                            case (1):
                                %><BR>R&uuml;ckzugsfaktor<%
                                break;
                            }
                    %></TD>
                    <TD colspan="2"><%
                        switch (sourcePage) {
                        case (FleetMovement.MOVE_FROM_FLEETVIEW): 
                            // Read retreat info from database
                            int retreatPercentage = 100;
                            ELocationType retreatToType = null;
                            int retreatTo = 0;                            

                            at.viswars.model.FleetOrder fo = null;
                            if (fleetFormation) {
                                FleetFormationExt ffeAct = FleetService.getFleetFormation(fleetId);
                                fo = ffeAct.getFleetOrder();                               
                            } else {
                                PlayerFleetExt pfeAct = FleetService.getFleet(fleetId);
                                fo = pfeAct.getFleetOrder();
                            }

                            if (fo != null) {
                                retreatPercentage = fo.getRetreatFactor();
                                retreatToType = fo.getRetreatToType();
                                retreatTo = fo.getRetreatTo();
                            }                            
%>
                            <BR>Sobald dieser %-Satz der Flotte vernichtet wurde, zieht sie sich zum angegebenen Ort zur&uuml;ck<BR><BR>
                            <select id="retreat" name="retreat">
<%
                            for (int i=40;i<=100;i+=10) { 
                                String text = Integer.toString(i) + "%";
                                if (i == 100) text = "Kein R&uuml;ckzug";
                                if (i == retreatPercentage) { %>
                                    <option value="<%= i %>" selected><%= text %></option>
                             <% } else { %>
                                    <option value="<%= i %>"><%= text %></option>
                             <% }
                            }
%>                                
                            </select>&nbsp nach <select id="retreatToType" name="retreatToType">
                            <option value="0">-- Bitte w&auml;hlen --</option>
                            <% 
                            if (retreatToType == ELocationType.SYSTEM) { %>
                                <option value="1" selected>System</option>
                                <option value="2">Planet</option>                                
                         <% } else if (retreatToType == ELocationType.PLANET) { %>
                                <option value="1">System</option>
                                <option value="2" selected>Planet</option>                                 
                         <% } else { %>                            
                                <option value="1">System</option>
                                <option value="2">Planet</option>     
                         <% } %>
                        </select>
                        &nbsp;
                        <input size="10" id="retreatTo" name="retreatTo" value="<%= retreatTo %>"/><BR><BR>                                                          
                   <%       break;
                        case (FleetMovement.MOVE_FROM_SYSVIEW):
                            %><BR>
                            Sobald dieser %-Satz der Flotte vernichtet wurde, zieht sie sich zur&uuml;ck<BR>
                            <select id="retreat" name="retreat">
                                <option value="40">40%</option>
                                <option value="50">50%</option>
                                <option value="60">60%</option>
                                <option value="70">70%</option>
                                <option value="80">80%</option>
                                <option value="90">90%</option>
                                <option value="100" SELECTED>Kein R&uuml;ckzug</option>
                            </select><%
                        %>&nbsp nach <select id="retreatToType" name="retreatToType">
                            <option selected value="0">-- Bitte w&auml;hlen --</option>
                            <option value="1">System</option>
                            <option value="2">Planet</option>
                        </select>
                        &nbsp;
                        <input size="10" id="retreatTo" name="retreatTo" value=""/></TD>
<%
                        }
%>
                </TR>
            </TABLE>
            <BR><%
            switch (sourcePage) {
            case (2):
                %><INPUT type='hidden' id="sourcepage" name="sourcepage" value="1" />
                <INPUT type='hidden' id="target" name="target" value="<%= targetIsSystem %>" />
                <INPUT type='hidden' id="ziel" name="ziel" value="<%= targetId %>" /><%
                break;
             case (1):
                 %><INPUT type='hidden' id="sourcepage" name="sourcepage" value="2" />
                <INPUT type='hidden' id="sysid" name="sysid" value="<%= systemId %>" />
                <INPUT type='hidden' id="fleetid" name="fleetid" value="<%= fleetId %>" /><%
                 break;
             }
             %> <INPUT type="submit" name="Weiter" value="Weiter" onclick="<%
             if (sourcePage == 1) {
                 %>if (document.getElementById('target').value == 0) { alert('Ziel w&auml;hlen!'); return false; }<%
             } else {
                %>if (document.getElementById('fleetid').value == 0) { alert('Flotte w&auml;hlen!'); return false; }<%
             }
             %>" />
        </FORM> <%
     } catch (Exception e) {
         %>Der folgende Fehler ist aufgetreten:<br/><%
         PrintWriter outputWriter = new PrintWriter(out);
        e.printStackTrace(outputWriter);
        outputWriter.flush();
     }
 %>
