<%@page import="at.viswars.GameConfig"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.ML"%>
<%@page import="at.viswars.FormatUtilities"%>
<%

int userId = Integer.parseInt((String)session.getAttribute("userId"));


%>
<TABLE class="blue2" style="font-size: 13px; ">
    <TR class="blue2">
        <TD>
            <%= ML.getMLStr("bonuslist_lbl_name", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("bonuslist_lbl_description", userId) %>
        </TD>
        <TD align="center">
            <%= ML.getMLStr("bonuslist_lbl_cost", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("bonuslist_lbl_range", userId) %>
        </TD>
    </TR>
    <% for(Bonus b : (ArrayList<Bonus>)Service.bonusDAO.findAll()){ %>
    <TR>
        <TD>
            <%= ML.getMLStr(b.getName(), userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr(b.getDescription(), userId) %>
        </TD>
        <TD>
            <TABLE style="font-size: 12px; font-weight: bold;">
                <TR>
                    <TD valign="middle">
                        <%= FormatUtilities.getFormattedNumber(b.getCost()) %>
                    </TD>
                    <TD>
                        <IMG style="height:18px; width: 18px;" border=0 src="<%= GameConfig.picPath()%>pic/menu/icons/happiness.png"/>
                    </TD>
                </TR>
            </TABLE>
        </TD>
        <TD>
            <%= b.getBonusRange().toString() %>
        </TD>
    </TR>
    <% } %>
    <TR>
        <TD>
            &nbsp;
        </TD>
    </TR>
    <TR>
        <TD colspan="4" align="center">
            <A href="main.jsp?page=new/overview"><%= ML.getMLStr("bonuslist_link_back", userId) %></A>
        </TD>
    </TR>
</TABLE>