<%@page import="at.viswars.model.Planet"%>
<%@page import="at.viswars.requestbuffer.GenericBuffer"%>
<%@page import="at.viswars.requestbuffer.ParameterBuffer"%>
<%@page import="at.viswars.requestbuffer.BufferHandling"%>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.hangar.*" %>
<%@page import="at.viswars.databuffer.*" %>
<%@page import="at.viswars.movable.*" %>
<%@page import="at.viswars.ships.*" %>
<%@page import="at.viswars.ships.ShipUtilitiesNonStatic" %>
<%@page import="at.viswars.fleet.FleetOrder" %>
<%@page import="at.viswars.result.*" %>
<%@page import="at.viswars.viewbuffer.*" %>
<%@page import="at.viswars.model.ShipDesign" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.Logger.*"%>
<%@page import="at.viswars.ships.ShipUtilities"%>
<%@page import="at.viswars.utilities.*"%>
<%@page import="at.viswars.utilities.HangarUtilities"%>
<%@page import="at.viswars.enumeration.*"%>
<%@page import="at.viswars.ships.ShipData"%>
<%@page import="at.viswars.result.FleetListResult.ESortType"%>
<%@page import="at.viswars.database.access.*" %>
<%@page import="at.viswars.text.FormatTime"%>
<% 
final int FLEET_ACTION_SHOW_DETAIL = 1;
final int FLEET_ACTION_RENAME = 2;
final int FLEET_ACTION_REGROUP = 3;
final int FLEET_ACTION_SEND = 4;
final int FLEET_ACTION_COLONIZE = 5;
final int FLEET_ACTION_SCAN_PANET = 6;
final int FLEET_ACTION_SCAN_SYSTEM = 69;
final int FLEET_ACTION_ABORT_SCAN = 68;
final int FLEET_ACTION_SET_DEF = 7;
final int FLEET_ACTION_UNSET_DEF = 8;
final int FLEET_ACTION_REROUTE = 9;
final int FLEET_ACTION_CALLBACK = 10;
final int FLEET_ACTION_ENTER_RETREAT = 11;
final int FLEET_ACTION_SET_RETREAT = 12;
final int FLEET_ACTION_DO_FORMATION = 13;
final int FLEET_ACTION_UNDO_FORMATION = 14;
final int FLEET_ACTION_MODIFY = 15;
final int FLEET_ACTION_SCRAP = 16;
final int FLEET_ACTION_UPGRADE = 17;
final int FLEET_ACTION_REPAIR = 18;
final int FLEET_ACTION_ATTACK = 28;
final int FLEET_ACTION_ATTACK_NOW = 29;
final int FLEET_ACTION_SET_SORTTYPE = 30;
final int FLEET_ACTION_CREATEEMBASSY = 31;


session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int systemId = Integer.parseInt((String)session.getAttribute("actSystem"));
// int highlightFleetId = Integer.parseInt(request.getParameter("highlightFleetId"));
int highlightFleetId = 0;

// Parameter check sollte eingebaut werden falls user versucht Parameter abzu&auml;ndern

boolean scannerAvailable = FleetService.scannerAvailable(planetId, userId);
   

// Abfrage Parameter f&uuml;r Seitenanzeige
int showfleet = 0;
int showaction = 0;
boolean showFleetList = true;

if (request.getParameter("fmaction") != null) {
    showaction = Integer.parseInt(request.getParameter("fmaction"));
    if ((showaction == FLEET_ACTION_SEND || showaction == FLEET_ACTION_ATTACK)) {
        showFleetList = false;
    }
}

String moveError = "";
BaseResult result = null;
ArrayList<BaseResult> results = null;

boolean fleetFormation = (request.getParameter("isFF") != null);

if(showaction == FLEET_ACTION_SET_SORTTYPE){

    FleetSortBuffer.set(ESortType.valueOf(request.getParameter("sortType")), userId);
    }


if (request.getParameter("fleetId") != null && (showaction > 0)) {
    try {
        if (showaction == FLEET_ACTION_REGROUP) {  
            if (request.getParameter("regroup") != null) {
                int firsttransfer = Integer.parseInt(request.getParameter("firsttransfer"));
                result = FleetService.regroupFleet(request.getParameterMap(), userId, systemId, firsttransfer == 1);
                RegroupResult rr = (RegroupResult)result;
                if (rr.isDeleted()) {
                    Logger.getLogger().write("Source Fleet was deleted");
                    showaction = 0;
                } else {
                    Logger.getLogger().write("Source Fleet was not deleted");
                }
            }
            // moveError = mu.moveFleetLocal(request.getParameterMap(),userId,systemId);
        }
        // Kolonisation ausl&ouml;sen
        showfleet = Integer.parseInt(request.getParameter("fleetId"));
        if (showaction == FLEET_ACTION_COLONIZE) {
            result = FleetService.colonize(showfleet);         
        }        
        
        if (showaction == FLEET_ACTION_SCAN_PANET) {
            PlayerFleetExt pfe = new PlayerFleetExt(showfleet);
            FleetService.scanPlanet(pfe);
        }
        if (showaction == FLEET_ACTION_SCAN_SYSTEM) {
            PlayerFleetExt pfe = new PlayerFleetExt(showfleet);
            result = FleetService.scanSystem(pfe);
        }
        if (showaction == FLEET_ACTION_ABORT_SCAN) {
            FleetService.abortScan(userId, showfleet);
        }        
        if (showaction == FLEET_ACTION_CREATEEMBASSY) {
            PlayerFleetExt pfe = new PlayerFleetExt(showfleet);
            result = DiplomacyService.createEmbassy(userId, pfe);
              
        }
        /*
        if (showaction == FLEET_ACTION_SET_DEF) {
            ShipUtilities.setFleetToDef(Integer.parseInt(request.getParameter("fleetId")));
        }
        
        if (showaction == FLEET_ACTION_UNSET_DEF) {
            ShipUtilities.restoreFleetFromDef(Integer.parseInt(request.getParameter("fleetId")));
        } 
        */
        
        if (showaction == FLEET_ACTION_CALLBACK) {
            if(fleetFormation){
                Logger.getLogger().write("Invoking callbackfleetformation");
                result = FleetService.callBackFleetFormation(Integer.parseInt(request.getParameter("fleetId")), userId);
            }else{
                result = FleetService.callBackFleet(Integer.parseInt(request.getParameter("fleetId")), userId);
            }
        }

        /*
        if (showaction == FLEET_ACTION_SET_RETREAT) {
            ShipUtilitiesNonStatic su = new ShipUtilitiesNonStatic(userId,planetId);
            
            FleetOrder fo = new FleetOrder();
            
            int retreatFactor = Integer.parseInt(request.getParameter("retreat"));
            int retreatToType = Integer.parseInt(request.getParameter("retreatToType"));
            int retreatTo = Integer.parseInt(request.getParameter("retreatTo"));
            
            fo.setRetreatFactor(retreatFactor);
            fo.setRetreatToType(retreatToType);
            fo.setRetreatTo(retreatTo);
            
            // su.storeFleetOrders(Integer.parseInt(request.getParameter("fleetId")), fo);
            moveError = su.outMsg;
        }
        */
        
        if (showaction == FLEET_ACTION_DO_FORMATION) {
            if (request.getParameter("doFormation") != null) {
                result = FleetService.addToFormation(request.getParameterMap(), userId);
                showaction = 0;
            }                    
        }

        if (showaction == FLEET_ACTION_RENAME) {
            if (request.getParameter("fleetname") != null) {
                if (fleetFormation) {
                    FleetService.renameFleetFormation(showfleet, request.getParameter("fleetname"));
                } else {
                    FleetService.renameFleet(showfleet, request.getParameter("fleetname"));
                }
                showaction = 0;
            }
        }
        
        if (showaction == FLEET_ACTION_UNDO_FORMATION) {
            if (fleetFormation) {
                FleetService.disbandFormation(showfleet, userId);
            } else {
                FleetService.removeFromFormation(showfleet, userId);
            }
        }
        if (showaction == FLEET_ACTION_ATTACK_NOW) {
                    FleetService.attack(userId, showfleet, request.getParameterMap());
        }

        /*
        if (showaction == FLEET_ACTION_SCRAP) {
            if (request.getParameter("destroyCount") != null && request.getParameter("shipDesignId") != null && request.getParameter("fleetId") != null) {
                results = FleetService.scrapShips(request.getParameterMap(), userId);
                showaction = 0;
            }
        }
        */
    } catch (NumberFormatException e) { }
}


//Hier wird erst mal der Parameter fleetId nach dem richtigen Benuzer gepr&uuml;ft
if (showaction != FLEET_ACTION_SHOW_DETAIL) {
    FleetService.checkValidUser(request.getParameter("fleetId"),fleetFormation, userId, out);
} else {
    FleetService.checkValidUserView(request.getParameter("fleetId"),fleetFormation,userId,systemId,scannerAvailable);
}

// ------------------------------------------------------------------------------------------
// Any changes to fleet data behind this point will not be shown during current page creation
// ------------------------------------------------------------------------------------------
%>

<BR>
<%
PlayerFleetExt actFleet = null;
FleetFormationExt actFormation = null;

if (showfleet != 0) {    
    if (fleetFormation) {
        actFormation = FleetService.getFleetFormation(showfleet);
    } else {
        actFleet = FleetService.getFleet(showfleet);
    }
}

// Fehleranzeige
if (result != null) {
    if (!result.isError()) {
%>
        <FONT style="font-size:13px" color="green"><B><%= result.getMessage() %></B><BR><BR></FONT>
<%
    } else {
%>
        <FONT style="font-size:13px" color="red"><B><%= result.getMessage() %></B><BR><BR></FONT>
<%
    }
}


switch (showaction) {
    case(FLEET_ACTION_SHOW_DETAIL): {
        // FleetData fd = new FleetData(Integer.parseInt(request.getParameter("fleetId")));
        IFleet fleetObj = null;
        int fleetId = Integer.parseInt(request.getParameter("fleetId"));
        
        if (fleetFormation) {
            fleetObj = FleetService.getFleetFormation(fleetId);
        } else {
            fleetObj = FleetService.getFleet(fleetId);
        }
        
        // PlayerFleetExt pfExt = FleetService.getDetailedFleetData(Integer.parseInt(request.getParameter("fleetId")));       
        %>
        <TABLE cellpadding=0 class="blue" width="90%">
        <TR>
        <TD class="blue" style="font-size:15;" align="center" colspan=3><B><%= ML.getMLStr("fleetmanagement_lbl_fleetinformationfor", userId) %> <%= fleetObj.getName() %></B></TD>
        <TR>
        <TD width="50%">
            <TABLE cellpadding=0 class="blue" width="100%">
            <TR>
            <TD class="blue" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_name", userId) %></B></TD>
            <TD class="blue" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_condition", userId) %></B></TD>
            <TD class="blue" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_quantity", userId) %></B></TD>
            </TR>
            <%            
            for(ShipData sd : fleetObj.getShipList()) {
                boolean foreignShips = false;
                ShipDesign sdTmp = ShipDesignService.getShipDesign(sd.getDesignId());

                if (sdTmp.getUserId() != userId) {
                    foreignShips = true;
                }
            %>
                <TR>
<%
                if (foreignShips) {
                    if(DiplomacyUtilities.hasAllianceRelation(userId, fleetObj.getUserId())){
%>
<TD width="70%" align="center"><%= sd.getDesignName() %> [<%= ML.getMLStr(Service.chassisDAO.findById(sd.getChassisSize()).getName(), userId) %>]</TD>
<%                  }else{ %>
<TD width="70%" align="center">Design unbekannt[<%= ML.getMLStr(Service.chassisDAO.findById(sd.getChassisSize()).getName(), userId) %>]</TD>
                        <%}
                } else {
%>
                    <TD width="50%" align="center"><%= sd.getDesignName() %></TD>
<%
                }
%>
                <TD width="25%"><TABLE cellpadding=0 cellspacing=0 border=0><TR><%= sd.getBarContent() %></TR></TABLE></TD>
                <TD width="25%" align="center"><%= sd.getCount() %></TD>
                </TR>
            <%
                ArrayList<ShipTypeEntry> hangarLoading = HangarUtilities.getNestedHangarStructure(fleetObj.getId(),sd.getDesignId());
                if (hangarLoading.size() != 0) {
                    for (int i=0;i<hangarLoading.size();i++) {
                        ShipTypeEntry ste = (ShipTypeEntry)hangarLoading.get(i);
                        if (ste.getNestedLevel() == 0) continue;
                        
                        out.write("<TR><TD align=\"center\">");

                        ShipDesign sdInner = ShipDesignService.getShipDesign(ste.getId());
                        out.write(sdInner.getName()+" (Beiboot)</TD>");                                                 

                        out.write("<TD><TABLE cellpadding=0 cellspacing=0 border=0><TR>"+ShipData.getBarContent(HangarUtilities.getShipDataStatus(ste))+"</TABLE></TD>");
                        out.write("<TD align=\"center\">"+ste.getCount()+"</TD></TR>");                
                    }                    
                }
            }
            %>
            </TABLE>
        </TD>
        <TD>
            <!-- Hier noch variabel machen und genau ausarbeiten -->
            <TABLE style="font-size:13;">
            <TR>
            <TD width=25></TD>
            <TD width=110>
            <%= ML.getMLStr("fleetmanagement_lbl_status", userId) %><BR>
            <%= ML.getMLStr("fleetmanagement_lbl_experience", userId) %><BR>
            <%= ML.getMLStr("fleetmanagement_lbl_system", userId) %><BR>
            <%= ML.getMLStr("fleetmanagement_lbl_planet", userId) %><BR>
            </TD>
            <TD>
            <%
            if (!fleetObj.isMoving()) {
                out.write(ML.getMLStr("fleetmanagement_lbl_stationed", userId));
            } else {
                out.write(ML.getMLStr("fleetmanagement_lbl_inflight", userId));
            }
            %>
            <BR>
            <IMG src="<%= GameConfig.picPath() %>pic/icons/starorange.jpg" border=0 /><IMG src="<%= GameConfig.picPath() %>pic/icons/starorange.jpg" border=0 /><IMG src="<%= GameConfig.picPath() %>pic/icons/starorange.jpg" border=0 /><IMG src="<%= GameConfig.picPath() %>pic/icons/starorange.jpg" border=0 /><IMG src="<%= GameConfig.picPath() %>pic/icons/starorange.jpg" border=0 /><BR>
            <% if (fleetObj.isMoving()) { %>
                N/A<BR>
                N/A<BR>
            <% } else { %>
                System #<%= fleetObj.getRelativeCoordinate().getSystemId() %><BR>
                Planet #<%= fleetObj.getRelativeCoordinate().getPlanetId() %><BR>
            <% } %>
            </TD>
            </TR>
            </TABLE>
        </TD>
        </TR>
        </TABLE>
        <%
        break;
    	}
    /*
 * 
 * Colonize Confirmation
 * 
 * 
    */
    case(99): {
        results = FleetService.showColonizationConstraints(userId, Integer.parseInt(request.getParameter("fleetId")));
        PlayerFleetExt pfe = FleetService.getFleet(Integer.parseInt(request.getParameter("fleetId")));
        Planet planet = PlanetService.getPlanet(pfe.getRelativeCoordinate().getPlanetId());
        SurfaceResult sr = planet.getSurface();
        long maxPopulation = planet.getMaxPopulation();
        Locale loc = ML.getLocale(userId);
%>
        <TABLE style="font-size:13px">
            <TR>
                <TD>
                    <TABLE style="font-size:13px;" width="100%" cellpadding="0" cellspacing="0">
                    <TR>
                    <TD width="160"><IMG src="GetPlanetPic?planetId=<%= planet.getId() %>" width="150" height="150" /></TD>
                    <TD valign="top" style="font-size:11px;">
                        <U><%= ML.getMLStr("overview_lbl_common", userId)%></U><BR><BR>
                    <%
                    String pop = "systemsearch_pop_"+planet.getLandType();
                    %>
                    <%= ML.getMLStr("overview_lbl_planettype", userId)%>: <%= ML.getMLStr(pop, userId)%> <BR>
                    <%= ML.getMLStr("overview_lbl_diameter", userId)%> <%= FormatUtilities.getFormattedNumber(planet.getDiameter(),loc) %> km<BR>
                    <%= ML.getMLStr("overview_lbl_averagetemperature", userId)%>: <%= planet.getAvgTemp() %> �C<BR><BR>

                    <U><%= ML.getMLStr("overview_lbl_specificdata", userId)%></U><BR><BR>
                    <%= ML.getMLStr("overview_lbl_constructiblesurface",userId)%>: <%= FormatUtilities.getFormatScaledNumber(sr.getSurface(),GameConstants.VALUE_TYPE_NORMAL,loc) %> km� (<%= planet.getSurfaceSquares() %> <%= ML.getMLStr("overview_lbl_buildingsite",userId)%>)<BR>
                        <%= ML.getMLStr("overview_lbl_maxpopulation",userId)%>: <%= FormatUtilities.getFormatScaledNumber(maxPopulation,GameConstants.VALUE_TYPE_NORMAL,loc) %><BR>
                    </TD>
                    </TR>
                    </TABLE>
                    </TD>
                    </TR>                   
                </TD>
            </TR>            
            <TR align="center">
                <TD><B><%= ML.getMLStr("fleetmanagement_msg_colonizationquestion", userId) %></B></TD>
            </TR>
            <%for(BaseResult br : results){
            %>
            <TR align="center">
                <TD><%= br.getMessage()%></TD>
            </TR>
            <%}
            %>
        </TABLE>
        <FORM method="post" action="main.jsp?page=new/fleet&type=1&fleetId=<%= request.getParameter("fleetId") %>&fmaction=<%= FLEET_ACTION_COLONIZE %>">
            <BR><INPUT NAME="ok" TYPE="submit" VALUE="Ok">
        </FORM>
<%
        break;
		}
    case(FLEET_ACTION_RENAME): {
        String subject = ML.getMLStr("fleetmanagement_lbl_fleet", userId);
        String actName;
        String urlExt = "";
        
        if (fleetFormation) {
            subject = ML.getMLStr("fleetmanagement_lbl_fleetformation", userId);
            actName = actFormation.getBase().getName();
            urlExt = "&isFF=1";
        } else {
            actName = actFleet.getBase().getName();
        }
%>
        <FORM method="post" action="main.jsp?page=new/fleet&type=1<%= urlExt %>&fleetId=<%= request.getParameter("fleetId") %>&fmaction=2">
            <%= ML.getMLStr("fleetmanagement_msg_newnamefor", userId).replace("%SUBJ%", subject) %> <INPUT NAME="fleetname" TYPE="text" VALUE="<%= actName %>" SIZE="20">
            <INPUT NAME="ok" TYPE="submit" VALUE="Ok">
        </FORM>
<%
        break;
		}
    case(FLEET_ACTION_REGROUP): {
        List <ShipData> shipList2 = ShipUtilities.getShipList(Integer.parseInt(request.getParameter("fleetId")));
                
        Statement stmt = DbConnect.createStatement();        
        stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT fleetId FROM hangarrelation WHERE fleetId="+request.getParameter("fleetId"));
%>
        <FORM method="post" action="main.jsp?page=new/fleet&type=1&fleetId=<%= request.getParameter("fleetId") %>&fmaction=3" Name="fleetmovement">    
<%                      
        if (rs.next()) { %>            
        <FONT size=-1 color="yellow"><B><%= ML.getMLStr("fleetmanagement_txt_splitwarn", userId) %></B><BR><BR></FONT>
<%        
        }        
%>        
        <TABLE class="blue" width="90%" cellpadding=0 cellspacing=0>
        <TR>
        <TD class="blue2" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_name", userId) %></B></TD>
        <TD class="blue2" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_maxavailable", userId) %></B></TD>
        <TD class="blue2" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_tomove", userId) %></B></TD>
        </TR>
        <%
        for(ShipData sd : shipList2) {
                rs = stmt.executeQuery("SELECT fleetId FROM hangarrelation WHERE relationToDesign="+sd.getDesignId()+" AND fleetId="+request.getParameter("fleetId"));
                if (rs.next()) continue;
        %>
            <TR>
            <TD width="60%" align="center"><%= sd.getDesignName() %></TD>
            <TD width="20%" align="center"><%= sd.getCount() %></TD>
            <TD width="20%" align="center">
                <INPUT NAME="stype<%= sd.getDesignId() %>" TYPE="text" VALUE="0" SIZE="6" ALIGN=middle>
            </TD>
            </TR>
        <%
        }
        %>
        </TABLE>
        <BR>
        <TABLE style="font-size:13px" width="250"><TR>
            <TD><INPUT checked="checked" type="radio" name="firsttransfer" value="0" />Unbesch&auml;digte zuerst<br></TD><TR>
            <TR><TD><INPUT type="radio" name="firsttransfer" value="1" />Schwer besch&auml;digte zuerst</TD>
        <TR></TABLE>        
        <BR>
        <B><FONT style="font-size:13px"><%= ML.getMLStr("fleetmanagement_lbl_regroupto", userId) %>
        <SELECT id="targetfleet" name="targetfleet">
        <OPTION value="0"><%= ML.getMLStr("fleetmanagement_opt_newfleet", userId) %></OPTION>
<%
        PlayerFleetExt currFleet = FleetService.getFleet(Integer.parseInt((String)request.getParameter("fleetId")));
        FleetListResult fleetList = FleetService.getFleetList(userId,currFleet.getBase().getSystemId(),currFleet.getBase().getPlanetId(),EFleetViewType.OWN_ALL_PLANET);        

        for (PlayerFleetExt pfExt : fleetList.getAllContainedFleets()) {
            if (pfExt.getUserId() != userId) {
                continue;
            }
            if (pfExt.getBase().getId() == currFleet.getBase().getId()) {
                continue;
            }
            if (pfExt.getBase().getPlanetId().intValue() != currFleet.getBase().getPlanetId().intValue()) {
                continue;
            }
            
            String defString = "";
            if (pfExt.getBase().getStatus() == 2) {
                defString = " [Def]";
            }
            
            out.write("<OPTION value=\""+pfExt.getBase().getId()+"\">"+pfExt.getBase().getName()+defString+" ("+pfExt.getBase().getSystemId()+":"+pfExt.getBase().getPlanetId()+")</option>");
        }
%>
        </SELECT>
        <BR><BR>
        <INPUT type="submit" id="regroup" name="regroup" value="<%= ML.getMLStr("fleetmanagement_but_regroup", userId) %>">
        </FONT></B>
        </FORM><BR>
<%
        break;
    	}
    case(FLEET_ACTION_SEND): {
        // Hier Eingabeform f&uuml;r Angriff reinprogrammieren
        session.setAttribute("source","1");
%>
	<jsp:include page="fleetactions.jsp" >
		<jsp:param name="fleetId" value='<%= showfleet %>' />
	</jsp:include>
<%
		break;
    	}
    case(FLEET_ACTION_UPGRADE):
    case(FLEET_ACTION_SCRAP): 
    case(FLEET_ACTION_REPAIR): 
    case(FLEET_ACTION_MODIFY): {
        // Show ships of current fleet and provide options
%>
	<jsp:include page="../subViews/fleetModify.jsp" >
		<jsp:param name="fleetId" value='<%= showfleet %>' />
	</jsp:include>
<%
        break;
    }
    case(FLEET_ACTION_SET_DEF): {
        // Adding fleet to defense         
        PlayerFleetExt pfExt = FleetService.getFleet(Integer.parseInt(request.getParameter("fleetId")));        
        %>
        <FORM method="post" action="main.jsp?page=new/fleet&type=1&fleetId=<%= request.getParameter("fleetId") %>&fmaction=3" Name="fleetmovement">
        <TABLE width="90%" style="font-size:12px"><TR><TD align="center">
                    Flotte <%= pfExt.getBase().getName() %> wurde zur 
                    <%
                    if (pfExt.getBase().getPlanetId() != 0) { %>
                    planetaren Verteidigung auf Planet #<%= pfExt.getBase().getPlanetId() %> hinzugef&uuml;gt.
                    <%      } else { %>
                    Systemverteidigung in System #<%= pfExt.getBase().getSystemId() %> hinzugef&uuml;gt.
                    <%      } %>
    <BR></TD></TR></TABLE></FORM>
<%      break;
    	}
    case(FLEET_ACTION_UNSET_DEF):{
        // Removing fleet from defense         
        PlayerFleetExt pfExt = FleetService.getFleet(Integer.parseInt(request.getParameter("fleetId")));        
        %>
        <TABLE width="90%" style="font-size:12px"><TR><TD align="center">
                    Flotte <%= pfExt.getBase().getName() %> von der
                    <%
                    if (pfExt.getBase().getPlanetId() != 0) { %>
                    planetaren Verteidigung auf Planet #<%= pfExt.getBase().getPlanetId() %> abgezogen.
                    <%      } else { %>
                    Systemverteidigung in System #<%= pfExt.getBase().getSystemId() %> abgezogen.
                    <%      } %>
        <BR></TD></TR></TABLE>
<%      break;
    	}
    case(FLEET_ACTION_REROUTE): {
        // Umleiten
        session.setAttribute("source","1");
%>
	<jsp:include page="fleetactions.jsp" >
		<jsp:param name="fleetId" value='<%= showfleet %>' />
                <jsp:param name="reroute" value='1' />
	</jsp:include>
<%
        break;
    	}
    case(FLEET_ACTION_ENTER_RETREAT): { 
        // Read retreat info from database
        int retreatPercentage = 100;
        ELocationType retreatToType = null;
        int retreatTo = 0;
        
        PlayerFleetExt pfExt = FleetService.getFleet(Integer.parseInt(request.getParameter("fleetId")));
        at.viswars.model.FleetOrder fo = pfExt.getFleetOrder();
        
        if (fo != null) {
            retreatPercentage = fo.getRetreatFactor();
            retreatToType = fo.getRetreatToType();
            retreatTo = fo.getRetreatTo();
        }
        %>
<BR>
    <FORM method="post" action="main.jsp?page=new/fleet&type=1&fleetId=<%= request.getParameter("fleetId") %>&fmaction=12" Name="retreatInfo">
    <TABLE width="80%" style="font-size:12px"><TR><TD align="middle">
                            <%= ML.getMLStr("fleetmanagement_msg_retreatfactor", userId) %><BR><BR>
                            <select id="retreat" name="retreat">
<%
                            for (int i=40;i<=100;i+=10) { 
                                String text = Integer.toString(i) + "%";
                                if (i == 100) text = ML.getMLStr("fleetmanagement_opt_noretreat", userId);
                                if (i == retreatPercentage) { %>
                                    <option value="<%= i %>" selected><%= text %></option>
                             <% } else { %>
                                    <option value="<%= i %>"><%= text %></option>
                             <% }
                            }
%>                                
                            </select>&nbsp nach <select id="retreatToType" name="retreatToType">
                            <option value="0">-- <%= ML.getMLStr("fleetmanagement_opt_pleaseselect", userId) %> --</option>
                            <% 
                            if (retreatToType == ELocationType.SYSTEM) { %>
                                <option value="1" selected><%= ML.getMLStr("fleetmanagement_lbl_system", userId) %></option>
                                <option value="2"><%= ML.getMLStr("fleetmanagement_lbl_planet", userId) %></option>
                         <% } else if (retreatToType == ELocationType.PLANET) { %>
                                <option value="1"><%= ML.getMLStr("fleetmanagement_lbl_system", userId) %></option>
                                <option value="2" selected><%= ML.getMLStr("fleetmanagement_lbl_planet", userId) %></option>
                         <% } else { %>                            
                                <option value="1"><%= ML.getMLStr("fleetmanagement_lbl_system", userId) %></option>
                                <option value="2"><%= ML.getMLStr("fleetmanagement_lbl_planet", userId) %></option>
                         <% } %>
                        </select>
                        &nbsp;
                        <input size="10" id="retreatTo" name="retreatTo" value="<%= retreatTo %>"/><BR><BR>
                        <INPUT NAME="ok" TYPE="submit" VALUE="Ok">                            
                </TR></TABLE></FORM>
        <%
        break;
        }
    case(FLEET_ACTION_DO_FORMATION): {
        PlayerFleetExt pfe = FleetService.getFleet(showfleet);
        Logger.getLogger().write("Working on fleet " + pfe.getName() + " which is at system " + pfe.getRelativeCoordinate().getSystemId());

        FleetListResult flr = FleetService.getFleetList(userId, pfe.getRelativeCoordinate().getSystemId(), pfe.getRelativeCoordinate().getPlanetId(), EFleetViewType.OWN_SYSTEM);
        FleetListResult flrAllied = FleetService.getFleetList(userId, pfe.getRelativeCoordinate().getSystemId(), pfe.getRelativeCoordinate().getPlanetId(), EFleetViewType.ALL_SYSTEM_ALLIED);
        
        Logger.getLogger().write("FLRALLIED FLEETS: " + flrAllied.getAllContainedFleets().size());
        Logger.getLogger().write("FLRALLIED FORMATIONS: " + flrAllied.getFormationCount());

        ArrayList<FleetFormationExt> ffList = flr.getFleetFormations();
%>
        <FORM method="post" action="main.jsp?page=new/fleet&type=1&fleetId=<%= request.getParameter("fleetId") %>&fmaction=<%= FLEET_ACTION_DO_FORMATION %>" Name="addToFormation">
        <TABLE width="80%" style="font-size:12px">
            <TR align="middle">
                <TD><%= ML.getMLStr("fleetmanagement_msg_addtoformation", userId) %></TD>
                <TR><TD>&nbsp;</TD><TR>
                <TR align="middle"><TD>
                    <select id="formationId" name="formationId">
                        <option value="0" selected><%= ML.getMLStr("fleetformation_opt_newformation", userId) %></option>
<%
                    for (FleetFormationExt ffe : ffList) {                       
%>
                        <option value="<%= ffe.getBase().getId() %>"><%= ffe.getBase().getName() %></option>
<%
                    }

                    for (FleetFormationExt ffe : flrAllied.getFleetFormations()) {
%>
                        <option value="<%= ffe.getBase().getId() %>"><%= ffe.getBase().getName() %></option>
<%
                    }
%>
                    </select>&nbsp;&nbsp;<INPUT ID="doFormation" NAME="doFormation" TYPE="submit" VALUE="Ok">
                </TD></TR>
            <TR>
        </TABLE>
        </FORM>
<%
    }

    break;
    case(FLEET_ACTION_ATTACK):

        PlayerFleetExt pfExt = FleetService.getFleet(Integer.parseInt(request.getParameter("fleetId")));
%>
<FORM action="main.jsp?page=new/fleet&type=1&fleetId=<%= request.getParameter("fleetId") %>&fmaction=<%= FLEET_ACTION_ATTACK_NOW %>" method="post">
<TABLE class="blue2" width="500px">
    <TR style="font-size: 14px">
        <TD colspan="2">
            <%= ML.getMLStr("fleetmanagement_txt_attackwarn", userId) %>
        </TD>
    </TR>
    <%
    ArrayList<AttackPossibleResult> possibleToAttack = FleetService.getPossibleToAttack(userId, pfExt.getRelativeCoordinate().getSystemId(), pfExt.getRelativeCoordinate().getPlanetId());
    for(AttackPossibleResult entry : possibleToAttack){

    %>
    <TR style="font-size: 13px">
        <TD><B><FONT color="<%= entry.getColor() %>"><%= entry.getName() %></FONT></B></TD>
        <TD><INPUT name="users" value="<%= entry.getUserId() %>" type="checkbox" /></TD>
    </TR>
    <% } %>
    <TR>
        <TD colspan="2" align="center">
            <INPUT type="submit" value="<%= ML.getMLStr("fleetmanagement_but_attack", userId) %>">
        </TD>
    </TR>
</TABLE>
</FORM>
<%

    break;
}


    ESortType sort = FleetSortBuffer.getByUserId(userId);
if (showFleetList) {
%>
<TABLE class="bluetrans" width="90%" cellpadding="0" cellspacing="0">
    <TR valign="middle">
        <TD align="left">
            <TABLE style="font-size: 12px; font-weight: bolder;" cellpadding="0" cellspacing="0" align="left">
                <TR valign="middle">
                    <TD width="110px" align="left">
                        <%= ML.getMLStr("fleetmanagement_lbl_sortby", userId) %>:
                    </TD>
                    <TD width="80px" class="blue2" align="left">
                        <%= ML.getMLStr("fleetmanagement_enum_" + ESortType.ID.toString(), userId) %>
                    </TD>
                    <TD width="10px" align="left">
                        <INPUT <%if(sort.equals(ESortType.ID)){%> checked <%}%> name="sortType" value="<%= ESortType.ID %>" style='background-color: #000000; color: #ffffff; border:0px; text-align: center;' type="radio" onclick="window.location.href = 'main.jsp?page=new/fleet&type=1&sortType=<%= ESortType.ID %>&fmaction=<%= FLEET_ACTION_SET_SORTTYPE %>'">
                    </TD>
                    <TD width="80px" class="blue2" align="left">
                        <%= ML.getMLStr("fleetmanagement_enum_" + ESortType.DISTANCE.toString(), userId) %>
                    </TD>
                    <TD width="10px" align="left">
                        <INPUT <%if(sort.equals(ESortType.DISTANCE)){%> checked <%}%> name="sortType" value="<%= ESortType.DISTANCE %>" style='background-color: #000000; color: #ffffff; border:0px; text-align: center;' type="radio" onclick="window.location.href = 'main.jsp?page=new/fleet&type=1&sortType=<%= ESortType.DISTANCE %>&fmaction=<%= FLEET_ACTION_SET_SORTTYPE %>'"/>
                    </TD>
                    <TD width="80px" class="blue2" align="left">
                        <%= ML.getMLStr("fleetmanagement_enum_" + ESortType.NAME.toString(), userId) %>
                    </TD>
                    <TD width="10px" align="left">
                        <INPUT <%if(sort.equals(ESortType.NAME)){%> checked <%}%> name="sortType" value="<%= ESortType.NAME %>" style='background-color: #000000; color: #ffffff; border:0px; text-align: center;' type="radio" onclick="window.location.href = 'main.jsp?page=new/fleet&type=1&sortType=<%= ESortType.NAME %>&fmaction=<%= FLEET_ACTION_SET_SORTTYPE %>'"/>
                    </TD>
                </TR>
            </TABLE>            
        </TD>
    </TR>
</TABLE>
<%
}

GenericBuffer gb = new GenericBuffer(userId);   
int bufId = gb.getId();
    
if (showFleetList) {
%>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.OWN_SYSTEM %>" />
    <jsp:param name="pageType" value="<%= EFleetViewType.OWN_SYSTEM %>" />
    <jsp:param name="sortType" value="<%= sort %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.ALL_SYSTEM_ALLIED %>" />
    <jsp:param name="pageType" value="<%= EFleetViewType.ALL_SYSTEM_ALLIED %>" />
    <jsp:param name="sortType" value="<%= sort %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.ALL_SYSTEM_ENEMY %>" />
    <jsp:param name="pageType" value="<%= EFleetViewType.ALL_SYSTEM_ENEMY %>" />
    <jsp:param name="sortType" value="<%= sort %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<%
}

if (showFleetList) {
    try {
%>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.OWN_NOT_IN_SYSTEM %>" />
    <jsp:param name="pageType" value="<%= EFleetViewType.OWN_NOT_IN_SYSTEM %>" />
    <jsp:param name="sortType" value="<%= sort %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.ALLIED_NOT_IN_SYSTEM %>" />
    <jsp:param name="pageType" value="<%= EFleetViewType.ALLIED_NOT_IN_SYSTEM %>" />
    <jsp:param name="sortType" value="<%= sort %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.ENEMY_NOT_IN_SYSTEM %>" />
    <jsp:param name="pageType" value="<%= EFleetViewType.ENEMY_NOT_IN_SYSTEM %>" />
    <jsp:param name="sortType" value="<%= sort %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<%
        } catch (Exception e) {
            session.setAttribute("JSPError", e.getMessage());
            throw e;
        }
}

if (showFleetList) {
%>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.OWN_ALL_WITH_ACTION %>" />
    <jsp:param name="pageType" value="<%= EFleetViewType.OWN_ALL_WITH_ACTION %>" />
    <jsp:param name="sortType" value="<%= sort %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<%
}

if (showFleetList) {

}

if (showFleetList) {
    if (gb.getParameter("found") == null) {
        out.write("<BR>" + ML.getMLStr("fleetmanagement_txt_nodisplayablefleets", userId));
    }
}

gb.invalidate();
%>