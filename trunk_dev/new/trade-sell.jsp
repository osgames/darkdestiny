
<%@page import="at.viswars.GameConfig"%>
<%@page import="at.viswars.result.BaseResult"%>
<%@page import="at.viswars.result.NamedPriceListResult"%>
<%@page import="at.viswars.trade.TradeGoodPriceList"%>
<%@page import="at.viswars.enumeration.TradeOfferType"%>
<%@page import="java.util.ArrayList"%>
<%@page import="at.viswars.GameConstants"%>
<%@page import="at.viswars.FormatUtilities"%>
<%@page import="at.viswars.model.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.ML" %>
<%
            final int SUBTYPE_OVERVIEW = 0;
            final int SUBTYPE_ADD_CUSTOM = 1;
            final int SUBTYPE_ADDPRICELIST = 2;

            final int PROCESS_CHANGEPRICELIST = 1;
            final int PROCESS_DELETEPRICELIST = 2;

            ArrayList<BaseResult> results = new ArrayList<BaseResult>();
            int subType = SUBTYPE_OVERVIEW;
            int process = 0;

            int userId = Integer.parseInt((String) session.getAttribute("userId"));

            if (request.getParameter("subType") != null) {
                subType = Integer.parseInt(request.getParameter("subType"));
            }

            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }

            int priceListId = 0;
            if (request.getParameter("priceListId") != null) {
                priceListId = Integer.parseInt(request.getParameter("priceListId"));
            }

            if(process == PROCESS_CHANGEPRICELIST){
                TradeService2.setTradePrice(priceListId, userId, request.getParameterMap());
                subType = SUBTYPE_OVERVIEW;
            }
            if(process == PROCESS_DELETEPRICELIST){
                //Todo change to delete
                TradeService2.deletePriceList(priceListId, userId);
                priceListId = 0;
                subType = SUBTYPE_OVERVIEW;
            }

            int defaultPriceList = TradeService2.getDefaultPriceListId(userId);
            int activePriceList = defaultPriceList;

            TradeGoodPriceList tgpl = null;
            if (subType == SUBTYPE_OVERVIEW) {
                if (priceListId != 0) {
                    tgpl = TradeService2.getTradePrice(userId, priceListId);
                    activePriceList = priceListId;
                } else {
                    tgpl = TradeService2.getTradePrice(userId, defaultPriceList);
                }
            } else if (subType == SUBTYPE_ADDPRICELIST) {
                if (request.getParameter("targetUserId") != null) {
                    results.add(TradeService2.createTradePriceList(userId, TradeOfferType.USER, Integer.parseInt(request.getParameter("targetUserId"))));
                    tgpl = TradeService2.getTradePrice(userId, TradeOfferType.USER, Integer.parseInt(request.getParameter("targetUserId")));
                    activePriceList = tgpl.getId();
                }
                if (request.getParameter("targetAllianceId") != null) {
                    results.add(TradeService2.createTradePriceList(userId, TradeOfferType.ALLY, Integer.parseInt(request.getParameter("targetAllianceId"))));
                    tgpl = TradeService2.getTradePrice(userId, TradeOfferType.ALLY, Integer.parseInt(request.getParameter("targetAllianceId")));
                    activePriceList = tgpl.getId();
                }
                subType = SUBTYPE_OVERVIEW;
            }


            switch (subType) {
                case (SUBTYPE_OVERVIEW):
%>
<CENTER>
<% for(BaseResult br : results){ %>
<%= br.getFormattedString() %>
<% }

%>
</CENTER>
<% if(Service.tradePostDAO.findByUserId(userId).isEmpty()){ %>
<TABLE class="bluetrans">
    <TR class="blue2">
        <TD style="font-weight: bolder; color: yellow;" width="80%" align="center">
            <%= ML.getMLStr("trade_txt_sellwarning1", userId)%><BR>
            <%= ML.getMLStr("trade_txt_sellwarning2", userId)%>
        </TD>
    </TR>
</TABLE>
<BR>
<% } %>
<TABLE class="bluetrans">
    <TR class="blue2">
        <TD style="font-weight: bolder;" width="200px" align="center">
            <%= ML.getMLStr("trade_label_targetgroup", userId) %>
        </TD>
    </TR>
    <TR>
        <TD>
            <% NamedPriceListResult nplr = TradeService2.getNamedPriceLists(userId);

            %>
            <% if(nplr != null){ %>
            <%= nplr.getFormattedString() %>
            <% } %>
            <SELECT id="priceListId" name="priceListId" class="dark" style="width:200px;" onChange="window.location.href='main.jsp?page=new/trade-main&type=3&priceListId='+document.getElementById('priceListId').value;+''">
                <% for (Integer i : nplr.getSortedList()) {%>
                <OPTION value="<%= i %>" <% if(i == activePriceList){ %>selected<% } %>><%= nplr.getNameFor(i)%></OPTION>
                <%  }%>
            </SELECT>
        </TD>
    </TR>
    <TR>
        <TD align="center">
            <A href="main.jsp?page=new/trade-main&type=3&subType=1" style="font-size: 12px; font-weight: bolder;"><%= ML.getMLStr("trade_link_addplayeroralliance", userId) %></A>
        </TD>
    </TR>
</TABLE>
<BR>
<FORM action="main.jsp?page=new/trade-main&type=3&priceListId=<%= activePriceList %>&process=<%= PROCESS_CHANGEPRICELIST %>" method="post">
<TABLE class="bluetrans" align="center" cellpadding="2" border="0" cellspacing="0">
    <TR>
        <!-- Ressource -->
        <TD align="center">
            <TABLE width="250px" >
                <TR class="blue2" style="font-size: 12px; font-weight: bolder;">
                    <TD width="35px">
                        &nbsp;
                    </TD>
                    <TD align="center">
                        <%= ML.getMLStr("trade_label_priceperunit", userId) %>
                    </TD>
                    <TD align="center">
                        <%= ML.getMLStr("trade_lbl_soldamount", userId) %>
                    </TD>
                </TR>
                <%
                if (tgpl != null) {
                    for (PriceListEntry ple : tgpl.getAllPrices()) {
                        Ressource r = Service.ressourceDAO.findRessourceById(ple.getRessId());
                %>

                <TR>
                    <TD valign="middle" align="right" style="width:25px; height:25px" style="padding-bottom:10px;">
                        <TABLE bgcolor="#3366FF"  width="100%" style=" border-style: solid; border-width: thin;  font-size: 11px; font-weight:bold;">
                            <TR>
                                <TD align="center" onmouseover="doTooltip(event,'<%= ML.getMLStr(r.getName(), userId)%>')" onmouseout="hideTip()"  >
                                    <IMG style="width:25px; height:25px" src="<%= GameConfig.picPath() + r.getImageLocation()%>"/>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <TD valign="center">
                        <TABLE style="font-size:11px; font-weight:bold;" cellpadding="0"  align="center"  cellspacing="0">
                            <TR>
                                <TD colspan="2">
                                    <TABLE  cellpadding="0" cellspacing="0">
                                        <TR>
                                            <!-- PRICE -->
                                            <TD align="left" valign="top">
                                                <TABLE class="bluetrans" style=" border-style: solid; border-width: thin; font-size: 11px; font-weight:bold;">
                                                    <TR>
                                                        <TD align="right">
                                                            <INPUT id="ress_<%= ple.getRessId() %>" name="ress_<%= ple.getRessId() %>" style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;" type="textfield" value="<%= ple.getPrice()%>"/>
                                                        </TD>
                                                    </TR>
                                                </TABLE>
                                            </TD><TD>
                                                &nbsp;
                                            </TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <TD  style="width:100%;">
                        <TABLE style="font-size:11px; width: 100%; font-weight:bold;" cellpadding="0"  align="center"  cellspacing="0">
                            <TR>
                                <TD colspan="2" style="width:100%;">
                                    <TABLE style="width:100%;" cellpadding="0" cellspacing="0">
                                        <TR>
                                            <TD style="width:100%;">
                                                <TABLE class="bluetrans" style="width: 100%; border-style: solid; border-width: thin; font-size: 11px; font-weight:bold;">
                                                    <TR>
                                                        <TD align="right"  style="width:100%;">
                                                            <%= FormatUtilities.getFormatScaledNumber(((long)ple.getSold()), GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId))%>
                                                        </TD>
                                                    </TR>
                                                </TABLE>
                                            </TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
                <% }
                            }%>
                <TR>
                    <TD colspan="3" align="center">
                        <TABLE>
                            <TR>
                                <TD>
                                    <INPUT class="dark" type="submit" value="<%= ML.getMLStr("trade_but_change", userId) %>"/>
                                </TD>
                                <TD>
                                    <%
                                    if (activePriceList != defaultPriceList) {
                                    %>
                                    <INPUT onclick="window.location.href='main.jsp?page=new/trade-main&type=3&process=<%= PROCESS_DELETEPRICELIST %>&priceListId=<%= priceListId %>';" class="dark" type="button" value="L�schen"/>
                                    <%
                                    } else {
                                        out.write("&nbsp");
                                    }
                                    %>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
</TABLE>
<BR/>
<TABLE class="bluetrans">
    <TR class="blue2">
        <TD style="font-weight: bolder; color: yellow;" width="80%" align="center">
            <%= ML.getMLStr("trade_txt_sellwarning3", userId)%><BR>
            <%= ML.getMLStr("trade_txt_sellwarning4", userId)%>
        </TD>
    </TR>
</TABLE>
</FORM>
<%
            break;
        case (SUBTYPE_ADD_CUSTOM):

%>

<BR>
<TABLE class="bluetrans">
    <TR class="blue2">
        <TD align="center" colspan="2" style="font-weight: bolder;">
            <%= ML.getMLStr("trade_link_addplayeroralliance", userId) %>
        </TD>
    </TR>
    <FORM action="main.jsp?page=new/trade-main&type=3&subType=<%= SUBTYPE_ADDPRICELIST%>" method="post">
        <TR>
            <TD>
                <SELECT class="dark" style="width:100%; min-width: 200px;" name="targetUserId">
                    <%
                                for (User u : (ArrayList<User>) Service.userDAO.findAllSortedByName()) {
                                    if (!u.getSystemAssigned()) {
                                        continue;
                                    }
                                    if (u.getUserId() == userId) {
                                        continue;
                                    }
                    %>
                    <OPTION value="<%= u.getUserId()%>"> <%= u.getGameName()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
            <TD>
                <INPUT  style="width:100%;" class="dark" type="submit" name="userButt" value="<%= ML.getMLStr("trade_but_addplayer", userId) %>" />
            </TD>
        </TR>
    </FORM>
    <% if(!Service.allianceDAO.findAllSortedByName().isEmpty()){ %>
    <FORM action="main.jsp?page=new/trade-main&type=3&subType=<%= SUBTYPE_ADDPRICELIST%>" method="post">
        <TR>
            <TD>
                <SELECT class="dark" name="targetAllianceId">
                    <%
                                for (Alliance a : (ArrayList<Alliance>) Service.allianceDAO.findAllSortedByName()) {
                    %>
                    <OPTION value="<%= a.getId()%>"><%= a.getName()%>[<%= a.getTag()%>]</OPTION>
                    <% }%>
                </SELECT>
            </TD>
            <TD>
                <INPUT style="width:100%" class="dark" type="submit" name="allyButt" value="<%= ML.getMLStr("trade_but_addalliance", userId) %> "/>
            </TD>
        </TR>
    </FORM>
     <% } %>
</TABLE>
<%
            break;
        case (SUBTYPE_ADDPRICELIST):
%>
Listi Listi
<%
                            break;
                    }%>