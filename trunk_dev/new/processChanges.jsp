<%--
    Document   : upgradePatch
    Created on : 21.02.2011, 12:39:45
    Author     : Stefan
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.viswars.model.UserData"%>
<%@page import="at.viswars.service.*"%>
<%@page import="at.viswars.admin.*"%>
<%@page import="java.util.*"%>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));

UserData ud = OverviewService.findUserData(userId);

if (ud.getTemporary() != 0) {
%>
<BR><BR><B>Du darfst dies nicht mehr benutzen!</B>
<%
} else {
    UpdateShips us = new UpdateShips();
    boolean result = us.processChanges(userId);

    if (result) {
%>
        Änderungen wurden übernommen
<%
    } else {
%>
        Änderungen konnten nicht durchgeführt werden
<%
    }
}
%>
