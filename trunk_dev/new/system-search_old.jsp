<%@page import="at.viswars.*"%>
<%@page import="java.util.*"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.result.*"%>
<%@page import="at.viswars.service.SystemService"%>

<%
session.setAttribute("actPage", "new/system-search");
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
String value = "";
String targetUserName = "";
/**
/* @TODO MoveFleet richtig verlinken
*/
ArrayList<String> messages = new ArrayList<String>();

final int TYPE_SYSTEMOWN = 1;
final int TYPE_SYSTEMID = 2;
final int TYPE_SYSTEMNAME = 3;
final int TYPE_RANGE = 4;
final int TYPE_SYSTEMALLIED = 5;
final int TYPE_SYSTEMENEMY = 6;
//Default
int type = TYPE_SYSTEMOWN;

if (request.getParameter("type") != null) {
            type = Integer.parseInt(request.getParameter("type"));
   }
if (request.getParameter("value") != null) {
            value = request.getParameter("value");
   }

if (request.getParameter("targetUserName") != null) {
            targetUserName = request.getParameter("targetUserName");
   }

SystemSearchResult ssr = null;

switch (type) {
     case(TYPE_SYSTEMOWN):           
        ssr = SystemService.findOwnSystems(SystemService.findPlanetById(planetId).getSystemId(), userId);
        break;
     case(TYPE_SYSTEMALLIED):
        if (!targetUserName.equals("")) {
            ssr = SystemService.findAllAlliedSystems(SystemService.findPlanetById(planetId).getSystemId(), userId, targetUserName);
        } else {
            ssr = SystemService.findAllAlliedSystems(SystemService.findPlanetById(planetId).getSystemId(), userId);
        }
        break;
     case(TYPE_SYSTEMENEMY):
        if (!targetUserName.equals("")) {
            ssr = SystemService.findAllEnemySystems(SystemService.findPlanetById(planetId).getSystemId(), userId, targetUserName);
        } else {
            ssr = SystemService.findAllEnemySystems(SystemService.findPlanetById(planetId).getSystemId(), userId);
        }
        break;
    case(TYPE_SYSTEMID):
        if(!value.equals("")){
            int systemId = Integer.parseInt(value);
            ssr = SystemService.findSystemsBySystemId(SystemService.findPlanetById(planetId).getSystemId(), systemId);
        }
        break;
    case(TYPE_SYSTEMNAME):
        if (!value.equals("")) {
            ssr = SystemService.findSystemsByName(SystemService.findPlanetById(planetId).getSystemId(), value);
        }
        break;
    case(TYPE_RANGE):
        if (!value.equals("")) {
            int range = Integer.parseInt(value);
            ssr = SystemService.findSystemsByRange(SystemService.findPlanetById(planetId).getSystemId(), range);
        }
        break;
}
%>
<BR/>
<TABLE WIDTH="80%">
    <TR>
    <TD ALIGN="CENTER" WIDTH="50%"><A href="main.jsp?page=new/system"><%= ML.getMLStr("systemsearch_link_systemview", userId)%></A></TD>
        <TD ALIGN="CENTER" WIDTH="50%"><A href="main.jsp?page=new/system-search"><%= ML.getMLStr("systemsearch_link_search", userId)%></A></TD>
    </TR>
</TABLE>
<BR/>

<%
if (ssr.isError()) { %>
<FONT color="red"><B><%= ssr.getMessage() %></B></FONT>
<% } %>
<BR/><BR/>

<TABLE style="font-size:13px;">
<FORM name="typeForm" method="post">
    <TR>
        <TD>
            <SELECT  style='background-color: #000000; color: #ffffff; width:250px; height:17px; border:0px; text-align: center;' onChange="window.location=document.typeForm.typeSelect.options[document.typeForm.typeSelect.selectedIndex].value" id="type" name="typeSelect">
                <OPTION <% if(type == TYPE_SYSTEMOWN){%>SELECTED <%}%> VALUE="main.jsp?page=new/system-search&type=<%= TYPE_SYSTEMOWN%>"><%= ML.getMLStr("systemsearch_opt_systemown", userId)%>:</OPTION>
                <%
                if(SystemService.isInAlliance(userId)){
                %>
                <OPTION <% if(type == TYPE_SYSTEMALLIED){%>SELECTED <%}%>  VALUE="main.jsp?page=new/system-search&type=<%= TYPE_SYSTEMALLIED%>"><%= ML.getMLStr("systemsearch_opt_systemallied", userId)%>:</OPTION>
                <%
                }
                %>
                <OPTION <% if(type == TYPE_SYSTEMENEMY){%>SELECTED <%}%>  VALUE="main.jsp?page=new/system-search&type=<%= TYPE_SYSTEMENEMY%>"><%= ML.getMLStr("systemsearch_opt_systemenemy", userId)%>:</OPTION>
                <OPTION <% if(type == TYPE_SYSTEMID){%>SELECTED <%}%>  VALUE="main.jsp?page=new/system-search&type=<%= TYPE_SYSTEMID%>"><%= ML.getMLStr("systemsearch_opt_systemid", userId)%>:</OPTION>
                <OPTION <% if(type == TYPE_SYSTEMNAME){%>SELECTED <%}%>  VALUE="main.jsp?page=new/system-search&type=<%= TYPE_SYSTEMNAME%>"><%= ML.getMLStr("systemsearch_opt_systemname", userId)%>:</OPTION>
                <OPTION <% if(type == TYPE_RANGE){%>SELECTED <%}%>  VALUE="main.jsp?page=new/system-search&type=<%= TYPE_RANGE%>"><%= ML.getMLStr("systemsearch_opt_maxdistance", userId)%>:</OPTION>
               </SELECT>
        </TD>
        </TR>
        <TR>
        <TD>  <%
             if(type == TYPE_RANGE || type == TYPE_SYSTEMID || type == TYPE_SYSTEMNAME){
              %>
            <INPUT STYLE="width:250px; height:22px;" ID="value" NAME="value" TYPE="TEXTFIELD">
                <%
                }
                %>
                  <%
             if(type == TYPE_RANGE || type == TYPE_SYSTEMID || type == TYPE_SYSTEMNAME){
              %>
        </TD>
        </TR>
<TR>
    <TD COLSPAN="2" align="left">
    <CENTER><INPUT STYLE="width:250px;"  type="submit" value="<%= ML.getMLStr("systemsearch_but_search", userId)%>"></CENTER>
    </TD>
</TR>
        <%}%>
   <%
   if(type == TYPE_SYSTEMENEMY || type == TYPE_SYSTEMALLIED){
        TreeMap<String, String> names = new TreeMap<String,String>();
        if(type == TYPE_SYSTEMALLIED){
            if(!targetUserName.equals("")){
             names = SystemService.findAlliedUsers(SystemService.findAllAlliedSystems(SystemService.findPlanetById(planetId).getSystemId(), userId), userId);
      }else{
             names = SystemService.findAlliedUsers(systems, userId);
             }
        }else if(type == TYPE_SYSTEMENEMY){
            if(!targetUserName.equals("")){
             names = SystemService.findEnemyUsers(SystemService.findAllEnemySystems(SystemService.findPlanetById(planetId).getSystemId(), userId), userId);
              }else{
                names = SystemService.findEnemyUsers(systems, userId);
            }
        }

   %>
</FORM>
<TR>
       <TD>

           <FORM METHOD="POST" name="subSelection">
           <SELECT style='background-color: #000000; color: #ffffff; width:250px; height:17px; border:0px; text-align: center;' onChange="window.location=document.subSelection.targetUserName.options[document.subSelection.targetUserName.selectedIndex].value" id="targetUserName" name="targetUserName">
                <OPTION <% if(targetUserName.equals("")){%>SELECTED <%}%> VALUE=""><%= ML.getMLStr("systemsearch_opt_all", userId) %></OPTION>

                <%
                 for(Map.Entry<String, String> entry : names.entrySet()){
                %>
                <OPTION <% if(targetUserName.equals(entry.getKey())){%>SELECTED <%}%> VALUE="main.jsp?page=new/system-search&type=<%= type%>&targetUserName=<%= entry.getKey()%>"><%= entry.getValue()%></OPTION>
                <%
                }
                %>
               </SELECT>
           </FORM>
       <TD>&nbsp;</TD>
   </TR>
   <%

   }

   %>

</TABLE>
    <P>
    <TABLE CELLPADDING="0" CELLSPACING="0" style="font-size:13px" width="80%">
    <TR><TD  ALIGN="CENTER" class="blue2" colspan=18><B><%= ML.getMLStr("systemsearch_lbl_systemlist", userId)%></B></TD></TR>
    <TR>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_id", userId)%></TD>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_name", userId)%></TD>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_coordinates", userId)%></TD>
      <%
  for(int j = 0; j < (Planet.MAX_ORBIT_LEVELS+1); j++){
  %>
    <TD STYLE="width:25px;" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("systemsearch_pop_Orbitlevel", userId)%>')" ONMOUSEOUT="hideTip()" ALIGN="CENTER" class="blue2"><%= j %></TD>
    <%
    }
   %><TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_distance", userId)%></TD>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_action", userId)%></TD>
    </TR>

    <%
    int i = 0;
    for(Map.Entry<Double,at.viswars.model.System> entry : systems.entrySet()){
    
    if(i%30 == 0 && i > 1){
        %>
<TR>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_id", userId)%></TD>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_name", userId)%></TD>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_coordinates", userId)%></TD>
  <%
  for(int j = 0; j < (Planet.MAX_ORBIT_LEVELS+1); j++){
  %>
    <TD  STYLE="width:25px;" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("systemsearch_pop_Orbitlevel", userId)%>')" ONMOUSEOUT="hideTip()" ALIGN="CENTER" class="blue2"><%= j %></TD>
    <%
    }
   %>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_distance", userId)%></TD>
    <TD ALIGN="CENTER" class="blue2"><%= ML.getMLStr("systemsearch_lbl_action", userId)%></TD>
    </TR>
        <%
        }
     %>
     <TR>
         <TD ALIGN="CENTER"><%= entry.getValue().getId()%></TD>
         <TD ALIGN="CENTER"><%= entry.getValue().getName()%></TD>
         <TD ALIGN="CENTER"><%= entry.getValue().getX() %>:<%= entry.getValue().getY()%></TD>
         <%= SystemService.getPlanetString(entry.getValue(), userId)%>
        <TD ALIGN="CENTER"><%= Math.round(entry.getKey())%></TD>
         <TD ALIGN="CENTER">
             <IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("systemsearch_pop_viewsystem", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/system&systemId=<%= entry.getValue().getId() %>'" src="<%= GameConfig.picPath() %>pic/goSystem.jpg" />
             <IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("systemsearch_pop_sendfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=sendfleet&systemId=<%= entry.getValue().getId() %>'" src="<%= GameConfig.picPath() %>pic/movefleet.jpg" />
        </TD>
   </TR>
      <%
      i++;
           }
    %>
    </TABLE>