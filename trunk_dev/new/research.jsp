    <%@page import="at.viswars.view.TechListViewEntry"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.service.ResearchService"%>
<%@page import="at.viswars.result.ResearchResult"%>
<%@page import="at.viswars.view.ResearchProgressView"%>
<%@page import="at.viswars.model.Research"%>
<%@page import="at.viswars.result.BonusResult"%>
<%@page import="at.viswars.utilities.*"%>
<%@page import="at.viswars.enumeration.*"%>
<%@page import="at.viswars.text.*" %>
<%@page import="java.util.*"%>
<%
session = request.getSession();
final int TYPE_SHOWTECHTREE = 1;
int showRes = 3;
int process = 0;
int researchId = 0;
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int type = 0;
session.setAttribute("actPage", "new/research");

if (request.getParameter("type") != null) {
        type = Integer.parseInt(request.getParameter("type")); 
}
%>
<TABLE width="80%">
    <TR align="center">
        <TD width="31%"><U><A href="main.jsp?page=new/research"><%= ML.getMLStr("research_link_researches", userId)%></A></U></TD>
        <% if(ResearchService.techTreeExternal(userId)) { %>
        <TD width="31%"><U><A href="techtreeextern.jsp" target="_new"><%=ML.getMLStr("research_link_techtree", userId)%></A><A href="main.jsp?page=new/profile"><IMG border="0" alt="techtree" onmouseover="doTooltip(event,'<%= ML.getMLStr("research_pop_techtreeinfo", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.picPath()%>pic/infoT.png" /></A></U></TD>
        <% }else{ %>
        <TD width="31%"><U><A href="main.jsp?page=new/research&type=<%= TYPE_SHOWTECHTREE %>"><%= ML.getMLStr("research_link_techtree", userId)%></A></U><A href="main.jsp?page=new/profile"><IMG border="0" alt="techtree" onmouseover="doTooltip(event,'<%= ML.getMLStr("research_pop_techtreeinfo", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.picPath()%>pic/infoT.png" /></A></TD>
        <% } %>
        
        <TD width="31%"><U><A href="http://stud3.tuwien.ac.at/~e0418413/uploaded/adv_techtree2.png" target="_new"><%= ML.getMLStr("research_link_techtreejpg", userId)%></A></U></TD>
    </TR>
</TABLE>
<%
switch(type){

        case(0):
            int searchResId = 0;
            ETechListType searchResType = ETechListType.RESEARCH_TYPE_UNKNOWN;
    if (request.getParameter("showRes") != null) {
            showRes = Integer.parseInt(request.getParameter("showRes"));
    }
    if (request.getParameter("process") != null) {
            process = Integer.parseInt(request.getParameter("process"));
    }
    if (request.getParameter("searchResId") != null) {
            searchResId = Integer.parseInt(request.getParameter("searchResId"));
            if(searchResId > 0){
            showRes = ResearchService.RESEARCH_VIEW_TECHTREE;
                       }
    }
    if (request.getParameter("searchResType") != null) {
            searchResType = ETechListType.valueOf(request.getParameter("searchResType"));
    }
    if (request.getParameter("researchId") != null) {
            researchId = Integer.parseInt(request.getParameter("researchId"));
    }
    if(process == ResearchService.PROCESS_START){
        ResearchService.processAction(userId, researchId, planetId, process);
    }else if(process == ResearchService.PROCESS_PAUSE){
        ResearchService.processAction(userId, researchId, planetId, process);
    }else if(process == ResearchService.PROCESS_RESUME){
        ResearchService.processAction(userId, researchId, planetId, process);

    }
    HashMap<Integer, Integer> timeFinished = null;
    timeFinished = ResearchService.getFinishedTime(userId);
    // Ich bitte um Bugfix der java.util.ConcurrentModificationException ;)
    ArrayList<ResearchProgressView> rpvList = ResearchService.getResearchListView(userId, searchResId, searchResType);
    ResearchResult rr = ResearchService.getResearchPointsGlobal(userId);
    double rBonus = rr.getResearchPointsBonus();
    double crBonus = rr.getCompResearchPointsBonus();
    boolean showResInfo = ResearchService.showInfo(userId, planetId);
    %>

    <script>
    <!-- Generate a a researchinfo[id] for every research -->
    <%
    for(ResearchProgressView rpv : rpvList) {
        if(rpv.getResearchEntry().getId() == Research.LOCK_TECH)continue;
        // @TODO Module noch anzeigen
        String requiredForResearch = "";
        String researchEnables = "";

         requiredForResearch = ResearchService.buildRequiredForResearchString(ResearchService.getRequiredResearchesForResearch(rpv.getResearchEntry().getId()), ResearchService.getRequiredConstructionsForResearch(rpv.getResearchEntry().getId()), userId);
         researchEnables = ResearchService.buildEnablesForResearchString(ResearchService.getEnabledResearchesForResearch(rpv.getResearchEntry().getId()), ResearchService.getEnabledConstructionsForResearch(rpv.getResearchEntry().getId()), ResearchService.getEnabledGroundtroopsForResearch(rpv.getResearchEntry().getId()), userId);
         String picName = rpv.getResearchEntry().getSmallPic();
         if(picName == null || picName.equalsIgnoreCase("NULL")){
             picName = "empty.gif";
         }
    %>
        var researchinfo<%= rpv.getResearchEntry().getId() %> = '<TABLE><TR valign="top" align="left"><TD><img src="<%=GameConfig.picPath()%>pic/research/<%= picName %>"></TD><TD><FONT style="font-size:11px"><%= ML.getMLStr(rpv.getResearchEntry().getShortText(),userId) %><BR><%= requiredForResearch%><BR><%= researchEnables %></FONT></TD></TR></TABLE>';
    <%
    }
    %>
    </script>
    <TABLE style="font-size:12px;" width="90%">
        <TR>
            <TD colspan="2" align="center">
                <TABLE  border="0" style="font-size:12px;" width="500px">
                        <TR class="blue2" align="middle">
                            <TD colspan=4>
                                <B><%= ML.getMLStr("research_lbl_reserachpointspertick", userId)%></B><% if(showResInfo){ %><img width="20" height="20" onmouseout="hideTip()" onmouseover="doTooltip(event,'<%= ML.getMLStr("research_pop_info", userId) %>')"  alt="Forschung" src="<%= GameConfig.picPath()%>pic/info.jpg"><% } %>
                            </TD>
                        </TR>
                        <TR align="middle">
                            <TD width="250px" colspan="2"><%= ML.getMLStr("research_lbl_normalresearchpoints", userId) %></TD>
                            <TD width="250px" colspan="2"><%= ML.getMLStr("research_lbl_computerresearchpointspertick", userId) %></TD>
                        </TR>
                            <TR>
                                <TD width="120px" align="right" valign="middle"><%= rr.getResearchPoints() %>
                                    <% if(rBonus > 0) { %>
                                    <FONT style="color:#FF00FF; font-size: 10px; font-weight: bold;">(+ <%= rBonus %>)</FONT>
                                    <% } %>
                                </TD>
                                <TD width="130px" align="left">
                                    <img width="20" height="20" onmouseout="hideTip()" onmouseover="doTooltip(event,'<%= ML.getMLStr("research_pop_research", userId) %>')"  alt="Forschung" src="<%= GameConfig.picPath() %>pic/menu/icons/research.png">
                                </TD>
                                <TD width="120px" align="right" valign="middle"><%= rr.getCompResearchPoints() %>
                                    <% if(crBonus > 0) { %>
                                    <FONT style="color:#FF00FF; font-size: 10px; font-weight: bold;">(+ <%= crBonus %>)</FONT>
                                    <% } %>
                                </TD>
                                <TD width="130px" align="left">
                                    <img width="20" height="20" onmouseout="hideTip()" onmouseover="doTooltip(event,'<%= ML.getMLStr("research_pop_computerresearch", userId) %>')"  alt="Forschung" src="<%= GameConfig.picPath() %>pic/menu/icons/cresearch.png">
                                </TD>
                        </TR>
                </TABLE>
            </TD>
        </TR>
    <%
    // <TR><TD>Spezial-Forschungspunkte (Aktuell):</TD><TD>**= rp[2] ** (<A HREF="">Laufende Auftr&auml;ge</A>)</TD></TR>
    %>
        <TR>
            <TD>&nbsp;</TD>
            <TD>&nbsp;</TD>
        </TR>
        <TR>
            <TD width="250px"  class="blue2" align="left"><%= ML.getMLStr("research_lbl_selectresearchprojectstatus", userId)%>:</TD>
            <TD align="left">
    <select id="showRes" name="showRes" style='background-color: #000000; color: #ffffff; border:0px; width: 300px; font-size: 12px;' onChange="window.location.href = 'main.jsp?page=new/research&showRes='+document.getElementById('showRes').value">
    <%
        if (showRes == ResearchService.RESEARCH_VIEW_ALL) {
    %>
            <option value="1" selected><%= ML.getMLStr("research_opt_all", userId)%></option>
            <option value="2"><%= ML.getMLStr("research_opt_researched", userId)%></option>
            <option value="3"><%= ML.getMLStr("research_opt_researchableandinresearch", userId)%></option>
            <option value="4"><%= ML.getMLStr("research_opt_inresearch", userId)%></option>
    <%
        } else if (showRes == ResearchService.RESEARCH_VIEW_RESEARCHED) {
    %>
            <option value="1"><%= ML.getMLStr("research_opt_all", userId)%></option>
            <option value="2" selected><%= ML.getMLStr("research_opt_researched", userId)%></option>
            <option value="3"><%= ML.getMLStr("research_opt_researchableandinresearch", userId)%></option>
            <option value="4"><%= ML.getMLStr("research_opt_inresearch", userId)%></option>
    <%
        } else if (showRes == ResearchService.RESEARCH_VIEW_RESEARCHED_AND_INRESEARCH) {
    %>
            <option value="1"><%= ML.getMLStr("research_opt_all", userId)%></option>
            <option value="2"><%= ML.getMLStr("research_opt_researched", userId)%></option>
            <option value="3" selected><%= ML.getMLStr("research_opt_researchableandinresearch", userId)%></option>
            <option value="4"><%= ML.getMLStr("research_opt_inresearch", userId)%></option>
    <%
        } else if (showRes == ResearchService.RESEARCH_VIEW_INRESEARCH) {
    %>
            <option value="1"><%= ML.getMLStr("research_opt_all", userId)%></option>
            <option value="2"><%= ML.getMLStr("research_opt_researched", userId)%></option>
            <option value="3"><%= ML.getMLStr("research_opt_researchableandinresearch", userId)%></option>
            <option value="4" selected><%= ML.getMLStr("research_opt_inresearch", userId)%></option>
    <%
        } else if (showRes == ResearchService.RESEARCH_VIEW_TECHTREE) {
    %>
            
            <option value="0" selected></option>
            <option value="1"><%= ML.getMLStr("research_opt_all", userId)%></option>
            <option value="2"><%= ML.getMLStr("research_opt_researched", userId)%></option>
            <option value="3"><%= ML.getMLStr("research_opt_researchableandinresearch", userId)%></option>
            <option value="4"><%= ML.getMLStr("research_opt_inresearch", userId)%></option>
    <%
        }
    %>
    </select>
            </TD>
        </TR>
        <TR>
            <TD width="250px"  class="blue2" align="left"><%= ML.getMLStr("research_lbl_researchtreefortechnology", userId) %>:</TD>
            <TD align="left">
    <select id="searchResId" name="searchResId" style='background-color: #000000; color: #ffffff; border:0px; width: 300px; font-size: 12px;' onChange="window.location.href = 'main.jsp?page=new/research&'+document.getElementById('searchResId').value">
            
               <OPTION value="-1" style="text-align: center" selected><----- <%= ML.getMLStr("research_opt_none", userId)%> -----></OPTION>
    <%
       String techTypeName = "";
       for(Map.Entry<ETechListType, ArrayList<TechListViewEntry>> list: ResearchService.getTechListView(userId).getList().entrySet()){
           String ttNameTmp = ML.getMLStr(list.getKey().toString(), userId);
           if(!ttNameTmp.equalsIgnoreCase(techTypeName)){
               %>
               <OPTION value="-1" style="text-align: center" disabled="true"><----- <%= ttNameTmp %> -----></OPTION>
               <%
           }
           techTypeName = ttNameTmp;
           for(TechListViewEntry tlve : list.getValue()){
           %>
           <OPTION value="searchResId=<%= tlve.getTechRelation().getSourceId() %>&searchResType=<%= tlve.getTechType().toString() %>" <% if(searchResId == tlve.getTechRelation().getSourceId()){ %>selected<% } %> ><%= tlve.getName() %></OPTION>
           <%
           }
       }
    %>
    </select>
            </TD>
        </TR>
    </TABLE>
    <TABLE style="table-layout:auto" class="deftrans" width="90%" cellpadding="0" cellspacing="0">
        <TR style="height:20px; overflow:hidden;" bgcolor="#808080">
            <TD align="center" style="height:20px;" class="blue" width="*"><B><%= ML.getMLStr("research_lbl_name", userId)%></B></TD>
            <TD align="center" class="blue" width="60" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("research_pop_bonus", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("research_lbl_bonus", userId)%></B></TD>
            <TD class="blue" width="40">&nbsp;</TD>
            <TD style="width:115px;" align="center" class="blue" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("research_pop_rpcrp", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("research_lbl_abbr_researchpoints", userId)%>/<%= ML.getMLStr("research_lbl_abbr_computerresearchpoints", userId)%></B></TD>
            <TD align="center" class="blue" width="120"><B><%= ML.getMLStr("research_lbl_status", userId)%></B></TD>
            <TD align="center" colspan="2" class="blue" width="170" ><B><%= ML.getMLStr("research_lbl_finished", userId)%></B></TD>
        </TR>
    <%

    for (ResearchProgressView rpv : rpvList) {
        if(rpv.getResearchEntry().getId() == Research.LOCK_TECH)continue;
        if ((showRes == ResearchService.RESEARCH_VIEW_INRESEARCH) &&
                (rpv.getStatus() != ResearchService.RESEARCH_VIEW_INRESEARCH) &&
                (rpv.getStatus() != ResearchService.RESEARCH_VIEW_ON_HOLD)) continue;
        if ((showRes == ResearchService.RESEARCH_VIEW_RESEARCHED) &&
                (rpv.getStatus() != ResearchService.RESEARCH_VIEW_RESEARCHED)) continue;
        if ((showRes == ResearchService.RESEARCH_VIEW_RESEARCHED_AND_INRESEARCH) &&
                (rpv.getStatus() != ResearchService.RESEARCH_VIEW_INRESEARCH) &&
                (rpv.getStatus() != ResearchService.RESEARCH_VIEW_NOT_RESEARCHED)&&
                (rpv.getStatus() != ResearchService.RESEARCH_VIEW_ON_HOLD)) continue;
        %>
        <TR style="height:20px;">
            <TD style="height:10px; overflow:hidden;">
                <FONT size=-1><%= ML.getMLStr(rpv.getResearchEntry().getName(),userId) %></FONT>
            </TD>
            <TD align="Center" style="height:20px;" >
                <% if(rpv.getResearchEntry().getBonus() > 0){%><B>+<%= (int)(rpv.getResearchEntry().getBonus()*100)%></B> %<%}%>
            </TD>
        <TD style="height:20px; overflow:hidden;">
        <%
        if (rpv.getStatus() == ResearchService.RESEARCH_VIEW_NOT_RESEARCHED) {
            out.println("<TABLE cellpadding=0 cellspacing=0><TR><TD><IMG border=0 alt=\"Information\" src=\""+GameConfig.picPath()+"pic/info.jpg\" onmouseover=\"doTooltip(event,researchinfo"+rpv.getResearchEntry().getId()+")\" onmouseout=\"hideTip()\"> </IMG></TD>");
            out.println("<TD><IMG border=0 alt=\""+ML.getMLStr("research_pop_startresearch", userId)+"\" src=\""+GameConfig.picPath()+"pic/start.jpg\" onmouseover=\"doTooltip(event,'" + ML.getMLStr("research_pop_startresearch", userId) + "')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=new/research&researchId="+rpv.getResearchEntry().getId()+"&process=" + ResearchService.PROCESS_START + "&searchResId="+ searchResId + "&searchResType="+searchResType.toString() + "';\"></IMG></TD></TR></TABLE>");
        } else if (rpv.getStatus() == ResearchService.RESEARCH_VIEW_INRESEARCH) {
            out.println("<TABLE cellpadding=0 cellspacing=0><TR><TD><IMG border=0 alt=\"Information\" src=\""+GameConfig.picPath()+"pic/info.jpg\" onmouseover=\"doTooltip(event,researchinfo"+rpv.getResearchEntry().getId()+")\" onmouseout=\"hideTip()\"></IMG></TD>");
            out.println("<TD><IMG border=0 alt=\""+ML.getMLStr("research_pop_pauseresearch", userId)+"\" src=\""+GameConfig.picPath()+"pic/stop.jpg\" onmouseover=\"doTooltip(event,'" + ML.getMLStr("research_pop_pauseresearch", userId) + "')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=new/research&researchId="+rpv.getResearchEntry().getId()+"&process=" + ResearchService.PROCESS_PAUSE + "&showRes="+showRes+"&searchResId="+ searchResId + "&searchResType="+searchResType.toString() + "';\"></IMG></TD></TR></TABLE>");
        } else if (rpv.getStatus() == ResearchService.RESEARCH_VIEW_ON_HOLD) {
            out.println("<TABLE cellpadding=0 cellspacing=0><TR><TD><IMG border=0 alt=\"Information\" src=\""+GameConfig.picPath()+"pic/info.jpg\" onmouseover=\"doTooltip(event,researchinfo"+rpv.getResearchEntry().getId()+")\" onmouseout=\"hideTip()\"></IMG></TD>");
            out.println("<TD><IMG border=0 alt=\""+ML.getMLStr("research_pop_resumeresearch", userId)+"\" src=\""+GameConfig.picPath()+"pic/start.jpg\" onmouseover=\"doTooltip(event,'" + ML.getMLStr("research_pop_resumeresearch", userId) + "')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=new/research&researchId="+rpv.getResearchEntry().getId()+"&process=" + ResearchService.PROCESS_RESUME + "&showRes="+showRes+"&searchResId="+ searchResId + "&searchResType="+searchResType.toString() + "';\"></IMG></TD></TR></TABLE>");
        } else if (rpv.getStatus() == ResearchService.RESEARCH_VIEW_RESEARCHED) {
            out.println("<IMG border=0 alt=\"Information\" src=\""+GameConfig.picPath()+"pic/info.jpg\" onmouseover=\"doTooltip(event,researchinfo"+rpv.getResearchEntry().getId()+")\" onmouseout=\"hideTip()\" ></IMG>");
        }else if (rpv.getStatus() == ResearchService.RESEARCH_VIEW_NOT_RESEARCHED) {
            out.println("<IMG border=0 alt=\"Information\" src=\""+GameConfig.picPath()+"pic/info.jpg\" onmouseover=\"doTooltip(event,researchinfo"+rpv.getResearchEntry().getId()+")\" onmouseout=\"hideTip()\" ></IMG>");
        }
        %>
        </TD>
      <TD align="center">
            <FONT size=-1>
                <%= FormatUtilities.getFormatScaledNumber(rpv.getResearchEntry().getRp() - rpv.getResPointsResearched(),GameConstants.VALUE_TYPE_NORMAL) %>
            </FONT> /
            <FONT size=-1 color="#33FFFF">
                <%= FormatUtilities.getFormatScaledNumber(rpv.getResearchEntry().getRpComputer() - rpv.getCompResPointsResearched(),GameConstants.VALUE_TYPE_NORMAL) %>
            </FONT>
        </TD>
    <%
        out.print("<TD><DIV style=\"width:120px; height:18px; background-image: url('"+GameConfig.picPath()+"pic/notavailable.jpg"+"'); position: relative;\">");

        int progress = (int)Math.ceil(100d / (rpv.getResearchEntry().getRp() + rpv.getResearchEntry().getRpComputer()) * (rpv.getResPointsResearched() + rpv.getCompResPointsResearched()));

        if (rpv.getStatus() == ResearchService.RESEARCH_VIEW_NOT_RESEARCHED || rpv.getStatus() == ResearchService.RESEARCH_VIEW_NOT_RESEARCHABLE) {
                //out.print("<IMG border=0 src=\""+GameConfig.picPath()+"pic/notavailable.jpg\" height=\"18\" width=\"120\"/>");
        } else if ((rpv.getStatus() == ResearchService.RESEARCH_VIEW_INRESEARCH) ||
                (rpv.getStatus() == ResearchService.RESEARCH_VIEW_ON_HOLD)) {
                out.print("<IMG border=0 src=\""+GameConfig.picPath()+"pic/inprogress.jpg"+"\" width=\""+(progress*120/100)+"\" height=\"18\"/>");
                out.print("<SPAN style='position:absolute; top: 0; left: 0; width: 120px; height: 18px; text-align: center; color: #000;'>"+progress+" %</SPAN>");
        } else if (rpv.getStatus() == ResearchService.RESEARCH_VIEW_RESEARCHED) {
                out.print("<IMG border=0 src=\""+GameConfig.picPath()+"pic/finished.jpg\" height=\"18\" width=\"120\"/>");
        }
        out.print("</DIV></TD>");
                %>
                <%


    if (rpv.getStatus() == ResearchService.RESEARCH_VIEW_INRESEARCH) {
        if (timeFinished.get(rpv.getResearchEntry().getId()) != null) {
    %>
                <TD width="*" align="center">
                    <%= FormatTime.formatTimeSpanAsTimeStamp(timeFinished.get(rpv.getResearchEntry().getId())) %>
                </TD>
                <TD width="*" align="center">
                    <%= FormatTime.formatEndTimeofTimeSpanAsDate(timeFinished.get(rpv.getResearchEntry().getId())) %>
                </TD>

            <%
            } else { %>
                 <TD width="170" colspan=2 align="center">>1 Woche</TD>
        <%  }

        }else{
        %>
        <TD>&nbsp;</TD>
        <TD>&nbsp;</TD>
        <%
        }
    %>
            </TR>
    <% }   %>
    </TABLE>
    <%
    break;

    case(TYPE_SHOWTECHTREE):
        %>
        <jsp:include page="/techtree.jsp"/>
        <%

        break;
    }

    %>