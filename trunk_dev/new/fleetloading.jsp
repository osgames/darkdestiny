<%@page import="at.viswars.requestbuffer.GenericBuffer"%>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.movable.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.fleet.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.model.*" %>
<%@page import="at.viswars.planetcalc.*" %>
<%@page import="at.viswars.hangar.*" %>
<%@page import="at.viswars.ressources.*" %>
<%@page import="at.viswars.enumeration.*" %>
<%@page import="at.viswars.utilities.*" %>
<%@page import="at.viswars.ships.*" %>





<%
final int FLEET_ACTION_VIEW_FLEET = 1;
final int FLEET_ACTION_LOAD_RESS = 21;
final int FLEET_ACTION_UNLOAD_RESS = 22;
final int FLEET_ACTION_LOAD_TROOPS = 23;
final int FLEET_ACTION_UNLOAD_TROOPS = 24;
final int FLEET_ACTION_LOAD_POPULATION = 25;
final int FLEET_ACTION_UNLOAD_POPULATION = 26;
final int FLEET_ACTION_DO_HANGAR = 27;
final int FLEET_ACTION_INVADE = 28;

session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int systemId = Integer.parseInt((String)session.getAttribute("actSystem"));

int showaction = 0;
int fleetId = 0;

boolean showFleetList = true;

PlayerFleetExt currFleet = null; 
LoadingInformation loadInfo = null; 
ArrayList<ShipTypeEntry> newFleetStructure = null;
UserData ud = MainService.findUserDataByUserId(userId);

if (request.getParameter("fleetId") != null) {
    fleetId = Integer.parseInt(request.getParameter("fleetId"));
}
if (request.getParameter("fmaction") != null) {
    showaction = Integer.parseInt(request.getParameter("fmaction"));
}

if (fleetId != 0) {
    currFleet = new PlayerFleetExt(fleetId);
    loadInfo = currFleet.getLoadingInformation();
}

if (showaction == FLEET_ACTION_DO_HANGAR) {
    DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"GOT HERE");
    Map<String, String[]> parMap = request.getParameterMap();
    if (parMap.containsKey("hangar")) {
        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"GOT HERE TOO");
        HangarUtilities hu = new HangarUtilities();
        if (parMap.containsKey("load")) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"WOOHOO LOAD");
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"=== RECONSTRUCT UNLOADED FLEET IN DB ===");
            hu.reconstructUnloadedFleetInDB(fleetId);
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"=== INITALIZE FLEET ===");
            hu.initializeFleet(fleetId);
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"=== GET NESTED HANGAR STRUCTURE ===");
            newFleetStructure = hu.getNestedHangarStructure();
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"=== RECONSTRUCT FLEET IN DB ===");
            int firsttransfer = Integer.parseInt(request.getParameter("firsttransfer"));
            hu.reconstructFleetInDB(fleetId,firsttransfer);
        } else if (parMap.containsKey("unload")) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"WOOHOO UNLOAD");
            hu.reconstructUnloadedFleetInDB(fleetId);
        }
    }    
}

// Prebuild javascriptPlan
out.write("<script language=\"javascript\" type=\"text/javascript\">");
out.write(FleetService.getFleetLoadingJavaScript(showaction, currFleet, userId).toString());
out.write("</script>");

// Build HTML
switch (showaction) {
    case(FLEET_ACTION_VIEW_FLEET): {
%>
        <BR>
        <TABLE style="font-size:12px" cellpadding=0 class="blue" width="90%">
        <TR><TD class="blue" style="font-size:15;" align="center"><B><%= ML.getMLStr("fleetloading_txt_loadinginfo", userId) %> <%= currFleet.getName() %></B></TD></TR>
        <TR>
        <TD width="100%">
            <TABLE style="font-size:12px"><TR valign="top" align="left">
            <TD width="300">
            <%= ML.getMLStr("fleetloading_lbl_rescap", userId) %><BR>
            <%= ML.getMLStr("fleetloading_lbl_troopcap", userId) %><BR>
            <%= ML.getMLStr("fleetloading_lbl_hangarcapsmall", userId) %><BR>
            <%= ML.getMLStr("fleetloading_lbl_hangarcapmedium", userId) %><BR>
            <%= ML.getMLStr("fleetloading_lbl_hangarcaplarge", userId) %>
            </TD>
            <TD><%= FormatUtilities.getFormattedNumber(loadInfo.getCurrLoadedRess()) %> / <%= FormatUtilities.getFormattedNumber(loadInfo.getRessSpace()) %><BR>
            <%= FormatUtilities.getFormattedNumber(loadInfo.getCurrLoadedTroops()+loadInfo.getCurrLoadedPopulation()) %> / <%= FormatUtilities.getFormattedNumber(loadInfo.getTroopSpace()) %><BR>
            <%= FormatUtilities.getFormattedNumber(loadInfo.getCurrLoadedSmallHangar()+loadInfo.getSmallHangarSpace()) %> / <%= FormatUtilities.getFormattedNumber(loadInfo.getSmallHangarSpace()) %><BR>
            <%= FormatUtilities.getFormattedNumber(loadInfo.getCurrLoadedMediumHangar()+loadInfo.getMediumHangarSpace()) %> / <%= FormatUtilities.getFormattedNumber(loadInfo.getMediumHangarSpace()) %><BR>
            <%= FormatUtilities.getFormattedNumber(loadInfo.getCurrLoadedLargeHangar()+loadInfo.getLargeHangarSpace()) %> / <%= FormatUtilities.getFormattedNumber(loadInfo.getLargeHangarSpace()) %>
            </TD></TR></TABLE>
            <%
            // DebugBuffer.addLine("currFleet="+currFleet.getLoadingList());
            if ((loadInfo.getLoadedStorage() != null) && !loadInfo.getLoadedStorage().isEmpty()) {
            %>
                <BR><TABLE style="font-size:12px" cellpadding=0 cellspacing=0 width="100%">
                    <TR>
                    <TD width="50%" class="blue" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetloading_lbl_resstroop", userId) %></B></TD>
                    <TD width="50%" class="blue" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD>
                    </TR>
                <%
                    // DebugBuffer.addLine("gotHere");
                    for (ShipStorage ss : loadInfo.getLoadedStorage()) {
                %>
                        <TR>
                            <TD align="center"><%= ML.getMLStr(ss.getName(),userId) %></TD>
                        <TD align="center"><%= FormatUtilities.getFormattedNumber(ss.getCount()) %></TD>
                        </TR>
                <%
                    }
                %>
                </TABLE>
<%
            }
%>
        </TD>
        </TR>
        </TABLE>
<%
        break;}
    case(FLEET_ACTION_LOAD_RESS):{       
        MutableRessourcesEntry mre = new MutableRessourcesEntry();
               
        if ((loadInfo.getLoadedStorage() != null) && !loadInfo.getLoadedStorage().isEmpty()) {
            List<ShipStorage> storage = loadInfo.getLoadedStorage();

            for (ShipStorage ss : storage) {
                mre.setRess(ss.getId(), ss.getCount());
            }
        }
%>
        <FORM method="post" action="main.jsp?page=new/transferress&type=1&fleetId=<%= fleetId %>" Name="TransferRessUp" onSubmit="if (!endCheck()) { alert('Ung&uuml;ltige Werte'); return false; }">
        <BR><TABLE style="font-size:12px" cellpadding=0 class="blue" width="90%"><TR>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_resource", userId) %></B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_infleet", userId) %></B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_onplanet", userId) %></B></TD>
        <TD class="blue"><B>&nbsp;</B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD>
        </TR>
<%
for (at.viswars.model.Ressource re : RessourceService.getAllTransportableRessources(ud)) {
            if (PlanetService.getStockRessourceOnPlanet(currFleet.getRelativeCoordinate().getPlanetId(), re.getId()) != 0) { %>
            <TR>
                <TD><%= ML.getMLStr(re.getName(),userId) %></TD>
                <TD><%= FormatUtilities.getFormattedNumber(mre.getRess(re.getId())) %></TD>
                <TD><%= FormatUtilities.getFormattedNumber(PlanetService.getStockRessourceOnPlanet(currFleet.getRelativeCoordinate().getPlanetId(), re.getId())) %></TD>
                <TD><DIV style="border:none" onclick="maxRess(<%= re.getId() %>)">MAX</DIV></TD>
                <TD><INPUT id="load<%= re.getName() %>" name="load<%= re.getName() %>" value="0" size="6" maxlength='7'></TD>
            </TR>                
<%          }
        }             

        int totalRess = 0;
        
        for (at.viswars.model.Ressource re : RessourceService.getAllTransportableRessources(ud)) {
            totalRess += PlanetService.getStockRessourceOnPlanet(currFleet.getRelativeCoordinate().getPlanetId(), re.getId());
        }  
            if (totalRess > 0) { %>
            <TR align="center">
                <TD colspan=4><BR>
                    <INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("fleetloading_button_continue", userId) %>">
                </TD>
            </TR>
         <% }
%>
        </TABLE>
        </FORM>
<%
        break;}
    case(FLEET_ACTION_UNLOAD_RESS):{
%>
        <FORM method="post" action="main.jsp?page=new/transferress&type=2&fleetId=<%= fleetId %>" Name="TransferRessDown">
        <BR><TABLE style="font-size:12px" cellpadding=0 class="blue" width="90%"><TR>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_resource", userId) %></B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_infleet", userId) %></B></TD>
        <TD class="blue" colspan="3"><B>&nbsp;</B></TD>
        <TD class="blue" colspan="2"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD>
        </TR>
<%
        // Statement stmt = DbConnect.createStatement();
        // ResultSet rs = stmt.executeQuery("SELECT fl.id, r.name, fl.count FROM fleetloading fl, ressource r WHERE r.id=fl.id AND fl.fleetId="+fleetId+" AND fl.loadtype=1");
        boolean foundentry = false;
        for (ShipStorage ss : loadInfo.getLoadedStorage()) {
            if (ss.getLoadtype() != ShipStorage.LOADTYPE_RESSOURCE) continue;
            
            foundentry = true;
%>
            <TR>
                <TD><%= ML.getMLStr(RessourceService.getRessourceById(ss.getId()).getName(),userId) %></TD>
                <TD><%= FormatUtilities.getFormattedNumber(ss.getCount()) %></TD>
                <TD align="right"><DIV style="border:none" onclick="ressMax(<%= ss.getId() %>)">MAX&nbsp;</DIV></TD>
                <TD align="center">/</TD> 
                <TD align="left"><DIV style="border:none" onclick="ressOpt(<%= ss.getId() %>)">OPT</DIV></TD>
                <TD><INPUT id="unload<%= ss.getId() %>" name="unload<%= ss.getId() %>" value="0" size="6" maxlength='7' onkeyup="lightCheck(<%= ss.getId() %>)"></TD>
                <TD align="left"><IMG id="info<%= ss.getId() %>" name="info<%= ss.getId() %>" src="<%= GameConfig.picPath() %>pic/ress_ok.jpg" onmouseover="doTooltip(event,lightMessage)" onmouseout="hideTip()"/></TD>
            </TR>
<%
        }
        if (foundentry) {
%>
            <TR align="center"><TD colspan="7"><BR><INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("fleetloading_button_continue", userId) %>"></TD></TR>
<%      } %>
        </TABLE>
        </FORM>
<%      break;}
    case(FLEET_ACTION_LOAD_TROOPS): {
%>
        <FORM method="post" action="main.jsp?page=new/transferress&type=3&fleetId=<%= fleetId %>" Name="TransferTroopsUp">
        <BR><TABLE style="font-size:12px" cellpadding=0 class="blue" width="90%"><TR>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_trooptype", userId) %></B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_onplanet", userId) %></B></TD>
        <TD class="blue"><B>&nbsp;</B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD>
        </TR>
<%
        boolean foundentry = false;

        for (PlayerTroop pt : DefenseService.findAllGroundTroopsOnPlanetUser(currFleet.getRelativeCoordinate().getPlanetId(), userId)) {
            if (pt.getTroopId() == 0) continue;
            
            GroundTroop gt = DefenseService.getGroundTroop(pt.getTroopId());
            foundentry = true;
%>
            <TR>
                <TD><%= ML.getMLStr(gt.getName(), userId) %></TD>
                <TD><%= FormatUtilities.getFormattedNumber(pt.getNumber()) %></TD>
                <TD><DIV style="border:none" onclick="maxTroop(<%= gt.getId() %>)">MAX</DIV></TD>
                <TD><INPUT id="load<%= gt.getId() %>" name="load<%= gt.getId() %>" value="0" size="6" maxlength='7'></TD>
            </TR>
<%
        }
        if (foundentry) {
%>
            <TR align="center"><TD colspan=4><BR><INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("fleetloading_button_continue", userId) %>"></TD></TR>
<%      } %>
        </TABLE>
        </FORM>
<%      break;}
    case(FLEET_ACTION_UNLOAD_TROOPS):
%>
        <FORM method="post" action="main.jsp?page=new/transferress&type=4&fleetId=<%= fleetId %>" Name="TransferTroopsDown">
        <BR><TABLE style="font-size:12px" cellpadding=0 class="blue" width="90%"><TR>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_trooptype", userId) %></B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_infleet", userId) %></B></TD>
        <TD class="blue"><B>&nbsp;</B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD>
        </TR>
<%
        // Statement stmt = DbConnect.createStatement();
        // ResultSet rs = stmt.executeQuery("SELECT fl.id, gt.name, fl.count FROM fleetloading fl, groundtroops gt WHERE gt.id=fl.id AND fl.fleetId="+fleetId+" AND fl.loadtype=2");
        boolean foundentry = false;
        for (ShipStorage ss : loadInfo.getLoadedStorage()) {
            if (ss.getLoadtype() != ShipStorage.LOADTYPE_GROUNDTROOP) continue;
            
            foundentry = true;
            GroundTroop gt = DefenseService.getGroundTroop(ss.getId());
%>
            <TR>
                <TD><%= ML.getMLStr(gt.getName(), userId) %></TD>
                <TD><%= FormatUtilities.getFormattedNumber(ss.getCount()) %></TD>
                <TD><DIV style="border:none" onclick="maxTroop(<%= gt.getId() %>)">MAX</DIV></TD>
                <TD><INPUT id="unload<%= gt.getId() %>" name="unload<%= gt.getId() %>" value="0" size="6" maxlength='7'></TD>
            </TR>
<%
        }
        if (foundentry) {
%>
            <TR align="center"><TD colspan=4><BR><INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("fleetloading_button_continue", userId) %>"></TD></TR>
<%      } %>
        </TABLE>
        </FORM>
<%      
        break;
    case(FLEET_ACTION_LOAD_POPULATION):
%>
        <FORM method="post" action="main.jsp?page=new/transferress&type=5&fleetId=<%= fleetId %>" Name="TransferPopUp" onSubmit="if (!endCheck()) { alert('Ung&uuml;ltige Werte'); return false; }">
        <BR><TABLE style="font-size:12px" cellpadding=0 class="blue" width="90%"><TR>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_freeworker", userId) %></B></TD>
        <TD class="blue"><B>&nbsp;</B></TD>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD>
        </TR>
        <TR>
        <%
        PlayerPlanet pp = PlanetService.getPlayerPlanet(currFleet.getRelativeCoordinate().getPlanetId());        
        // PlanetCalculation pc = new PlanetCalculation(pp);
        
        //Erstens vorhandene Bev&ouml;lkerung berechnen:
        // long bev = pp.getPopulation();
        //Dann wieviel AP gebraucht werden
        // long apRequired = pc.getPlanetCalcData().getExtPlanetCalcData().getUsedPopulation();
        //Und das ergibt dann, wieviel Bev&ouml;lkerung gebraucht wird

        long freePop = PlanetService.getFreePopulation(pp.getPlanetId());
        freePop = Math.max(0, freePop);
        %>
        <TD><%= FormatUtilities.getFormatScaledNumber(freePop,1) %></TD>
        <TD><DIV style="border:none" onclick="maxPop()">MAX</DIV></TD>
        <TD><INPUT id="loadpop" name="loadpop" value="0" size="6" maxlength='7'></TD>
        <TR align="center"><TD colspan=3><BR><INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("fleetloading_button_continue", userId) %>"></TD></TR>
        </TABLE>
        </FORM>
<%
        break;
    case(FLEET_ACTION_UNLOAD_POPULATION):
%>
        <FORM method="post" action="main.jsp?page=new/transferress&type=6&fleetId=<%= fleetId %>" Name="TransferPopDown">
        <BR><TABLE style="font-size:12px" cellpadding=0 class="blue" width="90%"><TR>
        <TD class="blue"><B><%= ML.getMLStr("fleetloading_lbl_workerinfleet", userId) %></B></TD>
        <TD class="blue" colspan="3"><B>&nbsp;</B></TD>
        <TD class="blue" colspan="2"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD>
        </TR>
        <%
        // stmt = DbConnect.createStatement();
        // rs = stmt.executeQuery("SELECT fl.id, fl.count FROM fleetloading fl WHERE fl.fleetId="+fleetId+" AND fl.loadtype=4");
        foundentry = false;
        for (ShipStorage ss : loadInfo.getLoadedStorage()) {
            if (ss.getLoadtype() != ShipStorage.LOADTYPE_POPULATION) continue;
            foundentry = true;
        %>
            <TR>
                <TD><%= FormatUtilities.getFormattedNumber(ss.getCount()) %></TD>
                <TD align="right"><DIV style="border:none" onclick="popMax()">MAX</DIV></TD>
                <TD align="center"> / </TD>
                <TD><DIV style="border:none" onclick="popOpt()">OPT</DIV></TD>
                <TD><INPUT id="unloadPop" name="unloadPop" value="0" size="6" maxlength='7' onkeyup="lightCheck()"></TD>
                <TD><IMG id="info" name="info" src="<%= GameConfig.picPath() %>pic/ress_ok.jpg" onmouseover="doTooltip(event,lightMessage)" onmouseout="hideTip()" /></TD>
            </TR>
<%
        }
        if (foundentry) { %>
            <TR align="center">
                <TD colspan="6"><BR>
                    <INPUT type="submit" name="Speichern" value="<%= ML.getMLStr("fleetloading_button_continue", userId) %>"></TD>
            </TR>
<%      }
%>
        </TABLE>
        </FORM>
<%
        break;
    case(FLEET_ACTION_DO_HANGAR):    
%>        
        <TABLE width="80%" style="font-size:12px"><TR><TD align="middle"><BR><FONT style="font-size:13px"><%= ML.getMLStr("fleetloading_txt_hangarmessage", userId) %>
        </FONT><BR><BR></TD></TR></TABLE>
        <FORM method="post" action="main.jsp?page=new/fleet&type=2&fleetId=<%= fleetId %>&fmaction=<%= FLEET_ACTION_DO_HANGAR %>&hangar=1" Name="TransferPopDown">            
<%
        if (newFleetStructure != null) {
%>
            <FONT style="font-size:13px"><%= ML.getMLStr("fleetloading_txt_newfleetstruccreated", userId) %></FONT><BR><BR>
            <TABLE width="80%" style="font-size:12px">
            <TR><TD width="70%" class="blue"><B><%= ML.getMLStr("fleetloading_lbl_ship", userId) %></B></TD><TD width="30%" class="blue"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD></TR>
<%
            for (int i=0;i<newFleetStructure.size();i++) {
                ShipTypeEntry ste = (ShipTypeEntry)newFleetStructure.get(i);

                out.write("<TR><TD>");

                for (int j=0;j<ste.getNestedLevel();j++) {
                    out.write("---");
                }
                if (ste.getNestedLevel() != 0) out.write("> ");

                ShipDesign sd = ShipDesignService.getShipDesign(ste.getId());
                out.write(sd.getName()+"</TD>");                

                out.write("<TD>"+ste.getCount()+"</TD></TR>");
            }
%>
            </TABLE><BR>
            <TABLE style="font-size:13px" width="250"><TR>
            <TD><input checked="checked" type="radio" name="firsttransfer" value="0"><%= ML.getMLStr("fleetloading_radio_nodamagefirst", userId) %><br></TD><TR>
            <TR><TD><input type="radio" name="firsttransfer" value="1"><%= ML.getMLStr("fleetloading_radio_damagefirst", userId) %></TD>
            <TR></TABLE><BR>                
<%
        } else {
            // Check for an existing fleetStructure
            // stmt = DbConnect.createStatement();
            // rs = stmt.executeQuery("SELECT fleetId FROM hangarrelation WHERE fleetId="+fleetId+" LIMIT 1");

            // if (rs.next()) {
%>
                <FONT style="font-size:13px"><%= ML.getMLStr("fleetloading_lbl_currfleetstructure", userId) %></FONT><BR><BR>
                <TABLE width="80%" style="font-size:12px">
                <TR><TD width="70%" class="blue"><B><%= ML.getMLStr("fleetloading_lbl_ship", userId) %></B></TD><TD width="30%" class="blue"><B><%= ML.getMLStr("fleetloading_lbl_count", userId) %></B></TD></TR>
<%
                newFleetStructure = HangarUtilities.getNestedHangarStructure(fleetId);

                for (int i=0;i<newFleetStructure.size();i++) {
                    ShipTypeEntry ste = (ShipTypeEntry)newFleetStructure.get(i);

                    out.write("<TR><TD>");

                    for (int j=0;j<ste.getNestedLevel();j++) {
                        out.write("---");
                    }
                    if (ste.getNestedLevel() != 0) out.write("> ");

                    ShipDesign sd = ShipDesignService.getShipDesign(ste.getId());
                    // rs = stmt.executeQuery("SELECT name FROM shipdesigns WHERE id="+ste.getId());
                    // if (rs.next()) {
                        out.write(sd.getName()+"</TD>");
                    // }

                    out.write("<TD>"+ste.getCount()+"</TD></TR>");
                }
    %>
                </TABLE><BR>
                <TABLE style="font-size:13px" width="250"><TR>
                <TD><input checked="checked" type="radio" name="firsttransfer" value="0"><%= ML.getMLStr("fleetloading_radio_nodamagefirst", userId) %><br></TD><TR>
                <TR><TD><input type="radio" name="firsttransfer" value="1"><%= ML.getMLStr("fleetloading_radio_damagefirst", userId) %></TD>
                <TR></TABLE><BR>                    
    <%
            // }
        }
%>        
        <TABLE><TR align="center">
        <TD width="50%"><INPUT type="submit" name="load" value="<%= ML.getMLStr("fleetloading_button_assignships", userId) %>"></TD>
        <TD width="50%"><INPUT type="submit" name="unload" value="<%= ML.getMLStr("fleetloading_button_unloadhangar", userId) %>"></TD>
        </TR></TABLE>
        </FORM>
<%
        break;    
}

GenericBuffer gb = new GenericBuffer(userId);   
int bufId = gb.getId();

if (showFleetList) {

    if(showaction == FLEET_ACTION_INVADE){ %>
       <%= java.util.ResourceBundle.getBundle("Bundle_de_DE").getString("fleetmanagement_troops_unloaded") %>
        <% }
%>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.OWN_ALL_TRANSPORT_SYSTEM %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<%
}

if (showFleetList) {
%>
<jsp:include page="../subViews/fleetlistview.jsp">
    <jsp:param name="viewType" value="<%= EFleetViewType.OWN_ALL_TRANSPORT_NOT_IN_SYSTEM %>" />
    <jsp:param name="respObj" value="<%= bufId %>" />
</jsp:include>
<%
}

if (showFleetList) {
    if (gb.getParameter("found") == null) {
        out.write("<BR>" + ML.getMLStr("fleetloading_txt_nofleetsavail", userId));
    }
}

gb.invalidate();
%>