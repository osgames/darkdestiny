<%@page import="at.viswars.model.UserSettings"%>
<%@page import="at.viswars.model.Language"%>
<%@page import="at.viswars.*" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Locale" %>
<%@page import="at.viswars.model.User" %>
<%@page import="at.viswars.model.UserData" %>
<%@page import="at.viswars.model.Style" %>
<%@page import="at.viswars.model.StyleElement" %>
<%@page import="at.viswars.model.NotificationType" %>
<%@page import="at.viswars.model.NotificationToUser" %>
<%@page import="at.viswars.model.StyleToUser" %>
<%@page import="at.viswars.result.*" %>
<%@page import="at.viswars.utilities.MD5" %>
<%@page import="at.viswars.utilities.*" %>
<%@page import="at.viswars.view.menu.*" %>
<%@page import="at.viswars.view.html.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.service.ProfileService"%>
<%@page import="java.lang.reflect.*"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="at.viswars.view.menu.statusbar.*"%>

<%


            final int SUBPAGE_GENERAL_SETTINGS = 0;
            final int SUBPAGE_STATUSBAR_SETTINGS = 1;
            final int SUBPAGE_CHANGEPROFILEDATA = 2;
            final int SUBPAGE_NOTIFICATIONS = 4;
            final int PROCESS_CHANGEPROFILEDATA = 0;
            final int PROCESS_CHANGEGENERALSETTINGS = 1;
            final int PROCESS_ADDSTYLETOUSER = 2;
            final int PROCESS_DELETESTYLEENTRIES = 3;
            final int PROCESS_UPDATEORDER = 4;
            final int PROCESS_UPDATENOTIFICATION = 5;

            int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));

            int subPage = SUBPAGE_GENERAL_SETTINGS;
            session = request.getSession();
            GameUtilities gu = new GameUtilities();
            ArrayList<String> errors = new ArrayList<String>();
            ArrayList<BaseResult> result = new ArrayList<BaseResult>();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            //UserData userData = ProfileService.findUserDataByUserId(userId);
            User user = ProfileService.findUserByUserId(userId);
            UserData userData = ProfileService.findUserDataByUserId(userId);
            UserSettings userSettings = Service.userSettingsDAO.findByUserId(userId);
            session.setAttribute("actPage", "new/profile");
%>
<TABLE width="70%">
    <TR>
        <TD width="25%"><U><A href="main.jsp?page=new/profile&subpage=<%= SUBPAGE_CHANGEPROFILEDATA%>"><%= ML.getMLStr("profile_link_changeprofiledata", userId)%></A></U></TD>
        <TD width="25%"><U><A href="main.jsp?page=new/profile&subpage=<%= SUBPAGE_GENERAL_SETTINGS%>"><%= ML.getMLStr("profile_link_generalsettings", userId)%></A></U></TD>
        <TD width="25%"><U><A href="main.jsp?page=new/profile&subpage=<%= SUBPAGE_STATUSBAR_SETTINGS%>"><%= ML.getMLStr("profile_link_statusbarsettings", userId)%></A></U></TD>
        <TD width="25%"><U><A href="main.jsp?page=new/profile&subpage=<%= SUBPAGE_NOTIFICATIONS%>"><%= ML.getMLStr("profile_link_notifications", userId)%></A></U></TD>
    </TR>
</TABLE>    
<%
            if (request.getParameter("subpage") != null) {
                subPage = Integer.parseInt(request.getParameter("subpage"));
            }

            String tmpType = request.getParameter("type");
            if (tmpType != null) {
                if (tmpType.length() == 0) {
                    tmpType = null;
                }
            }
            int type = 0;
            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }

            int process = -1;
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }

            switch (process) {
                case PROCESS_CHANGEPROFILEDATA: //Change the Profile
                    ProfileResult pr = ProfileService.updateProfileData(userId, request.getParameterMap());
                    result.addAll(pr.getResult());
                    subPage = SUBPAGE_CHANGEPROFILEDATA;
                    userData = ProfileService.findUserDataByUserId(userId);
                    user = ProfileService.findUserByUserId(userId);

                    break;
                case PROCESS_CHANGEGENERALSETTINGS:

                    String starmap_external = request.getParameter("starmap_external");
                    boolean starmap_externalBoolean = false;
                    if (starmap_external != null) {
                        starmap_externalBoolean = true;
                    } else {
                        starmap_externalBoolean = false;
                    }

                    String ttv_external = request.getParameter("ttv_external");
                    boolean ttv_externalBoolean = false;
                    if (ttv_external != null) {
                        ttv_externalBoolean = true;
                    } else {
                        ttv_externalBoolean = false;
                    }
                    String constructionDetails = request.getParameter("showConDetails");
                    boolean conDetails = false;
                    if (constructionDetails == null) {
                        conDetails = true;
                    } else {
                        conDetails = false;
                    }

                    String productionDetails = request.getParameter("showProdDetails");
                    boolean prodDetails = false;
                    if (productionDetails == null) {
                        prodDetails = true;
                    } else {
                        prodDetails = false;
                    }
                    int starmap_height = 100;
                    if (request.getParameter("starmap_height") != null) {
                        starmap_height = Integer.parseInt(request.getParameter("starmap_height"));
                    }
                    int starmap_width = 100;
                    if (request.getParameter("starmap_width") != null) {
                        starmap_width = Integer.parseInt(request.getParameter("starmap_width"));
                    }
                    int ttv_height = 100;
                    if (request.getParameter("ttv_height") != null) {
                        ttv_height = Integer.parseInt(request.getParameter("ttv_height"));
                    }
                    int ttv_width = 100;
                    if (request.getParameter("ttv_width") != null) {
                        ttv_width = Integer.parseInt(request.getParameter("ttv_width"));
                    }
                    int leaveDays = -1;
                    if (request.getParameter("leaveDays") != null) {
                        leaveDays = Integer.parseInt(request.getParameter("leaveDays"));

                    }
                    int sittedById = 0;
                    if (leaveDays > 0) {
                        if (request.getParameter("sittedBy") != null) {
                            sittedById = Integer.parseInt(request.getParameter("sittedBy"));

                        }
                    }

                    ProfileService.updateUser(userId, starmap_height, starmap_width, starmap_externalBoolean, ttv_height, ttv_width, ttv_externalBoolean, sittedById, leaveDays, conDetails, prodDetails);
                    userData = ProfileService.findUserDataByUserId(userId);
                    user = ProfileService.findUserByUserId(userId);

                    break;

                case (PROCESS_ADDSTYLETOUSER):
                    int styleId = 0;
                    if (request.getParameter("styleid") != null) {
                        styleId = Integer.parseInt(request.getParameter("styleid"));
                    }
                    Map<String, ?> parMap = request.getParameterMap();
                    ArrayList<StyleElement> toAdd = new ArrayList<StyleElement>();
                    for (StyleElement se : ProfileService.findStyleElementByStyleId(styleId)) {

                        if (parMap.containsKey("seId" + se.getId())) {
                            toAdd.add(se);
                        }
                    }

                    ProfileService.addStyleToUser(userId, toAdd);
                    type = 3;
                    break;

                case (PROCESS_DELETESTYLEENTRIES):

                    ProfileService.deleteStyleEntries(userId);
                    break;
                case (PROCESS_UPDATEORDER):
                    Map<String, ?> addMap = request.getParameterMap();
                    ArrayList<StyleToUser> toOrder = new ArrayList<StyleToUser>();
                    boolean valid = true;
                    ArrayList<Integer> positions = new ArrayList<Integer>();
                    for (StyleToUser se : ProfileService.findStyleToUserByUserId(userId)) {

                        if (addMap.containsKey("position" + se.getElementId())) {
                            int rank = Integer.parseInt(request.getParameter("position" + se.getElementId()));
                            if (positions.contains(rank)) {
                                valid = false;
                                break;
                            } else {
                                positions.add(rank);
                            }
                            se.setRank(rank);
                            toOrder.add(se);
                        }
                    }
                    if (valid) {
                        ProfileService.updateOrder(toOrder);
                    } else {
                        type = 3;
                        errors.add("Sie k�nnen jeweils nur einem Element einer Ordnung zuweisen");
                    }
                    break;

                case (PROCESS_UPDATENOTIFICATION):
                    ProfileService.updateNotifications(userId, request.getParameterMap());
                    subPage = SUBPAGE_NOTIFICATIONS;
                    break;
            }
            userSettings = Service.userSettingsDAO.findByUserId(userId);
            if (errors.size() == 0) {
%><%        } else {
%>
<CENTER>
    <% for (String error : errors) {%>
    <%= error%>
    <%}%>
</CENTER>
<%
            }
 if (result.size() == 0) {
%><%        } else {
%>
<CENTER>
    <% for (BaseResult r : result) {%>
    <%= r.getFormattedString() %>
    <%}%>
</CENTER>
<%
            }

            switch (subPage) {
                case SUBPAGE_CHANGEPROFILEDATA:
%>
<%@page import="java.io.PrintWriter"%>
<script type="test/javascript" language="Javascript">
    function setPic() {
        var availSpace = (Weite-355) * 0.8;
        
        document.getElementById("bild").setAttribute("width", availSpace, 0);
    }
</script><%

boolean deleteRevokeExpired = false;
String delStr = "";

if(user.getDeleteDate() > 0){
    if (GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - user.getDeleteDate()) <= GameConstants.DELETION_DELAY / 2) {
        deleteRevokeExpired = true;
        delStr = "You did not revoke your deletion and it has been locked in";
    }
}
%>
<TABLE width="70%"  align="center" style="font-size:12px">
    <TR bgcolor="#708090" CLASS=blue2 ><TD align="center"><%= ML.getMLStr("profile_lbl_profileof", userId)%>: <B><%= user.getUserName()%></B></TD></TR>

    <% if ((userData.getPic() != null) && (!userData.getPic().equals(""))) {%>
    <TR><TD align="center"><IMG alt="" id="bild" src="<%= userData.getPic()%>"/></TD></TR>
            <% }%>
    <TR><TD align="center"><%= userData.getDescription()%></TD></TR>    
    <TR><TD colspan=2><BR>
            <FORM method="post" action="<%= GameConfig.getHostURL() %>/ProfileChange">
     <!--       <FORM method="post" action="main.jsp?page=new/profile&process=<%= PROCESS_CHANGEPROFILEDATA%> "> -->
                <TABLE class="blue2" width="80%" align="center" style="font-size:13px">
                    <TR>
                        <TD colspan="2"><TABLE width="100%" bgcolor="darkred"><TR>
                        <TD width="250"><B><%= ML.getMLStr("profile_lbl_requestdeletion", userId)%></B></TD><TD><input  align="middle" <% if (user.getDeleteDate() > 0) {%>checked<%}%> type="checkbox" name="delete" value ="1"/></TD>
                        </TR>
                        <TR><TD colspan="2"><FONT color="yellow"><%= ML.getMLStr("profile_lbl_deletionwarning", userId, GameConstants.DELETION_DELAY / 2) %></FONT></TD></TR>
                        </TABLE></TD>
                    </TR>
                    <TR valign="bottom"><TD width="250"><BR><%= ML.getMLStr("profile_lbl_ingamename", userId)%>:</TD><TD><%= user.getGameName()%></TD></TR>
                    <% if (false && user.isGuest()) {%>
                    <TR><TD><%= ML.getMLStr("profile_lbl_email", userId)%>:</TD><TD><input disabled type="text" name="email" size=20 maxlength="50" value="<%= user.getEmail()%> "></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_oldpassword", userId)%>:</TD><TD><input disabled TYPE="password" value="" name="oldpwd" size=30 maxlength="20"></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_newpassword", userId)%>:</TD><TD><input disabled TYPE="password" value="" name="newpwd" size=30 maxlength="20"></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_confirmpassword", userId)%>:</TD><TD><input disabled TYPE="password" value="" name="confirmpwd" size=30 maxlength="20"/></TD></TR>
                            <% } else {%>
                    <TR><TD><%= ML.getMLStr("profile_lbl_email", userId)%>:</TD><TD><input type="text" name="email" size=20 maxlength="50" value="<%= user.getEmail()%> "></TD></TR>  
                    <TR><TD><%= ML.getMLStr("profile_lbl_oldpassword", userId)%>:</TD><TD><input TYPE="password" value="" name="oldpwd" size=30 maxlength="20"></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_newpassword", userId)%>:</TD><TD><input TYPE="password" value="" name="newpwd" size=30 maxlength="20"></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_confirmpassword", userId)%>:</TD><TD><input TYPE="password" value="" name="confirmpwd" size=30 maxlength="20"/></TD></TR>
                            <% }%>

                    <TR><TD><input TYPE="checkbox" <% if(userSettings.getNewsletter()) { %>CHECKED<% } %> name="newsletter" size=30 maxlength="20"></TD><TD><%= ML.getMLStr("profile_msg_newsletter", userId)%></TD></TR>
                    <TR><TD><input TYPE="checkbox" <% if(userSettings.getProtectaccount()) { %>CHECKED<% } %> name="protectaccount" size=30 maxlength="20"></TD><TD><%= ML.getMLStr("profile_msg_protectaccout", userId)%></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_imglink", userId)%>:</TD><TD><input type="text" name="pic" size=30 maxlength="100" value="<% if (userData.getPic() != null) {
                                            out.write(userData.getPic());
                                        }%>"></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_description", userId)%>:</TD><TD><textarea name="desc" rows=6 cols=40><% if (userData.getDescription() != null) {
                                            out.write(userData.getDescription());
                                        }%></textarea></TD></TR>

                    <TR><TD align="center" colspan=2><P><BR><input value="<%= ML.getMLStr("profile_but_update", userId)%>" type=submit></TD></TR>
                    <TR class="blue2"><TD colspan="2"><%=ML.getMLStr("profile_lbl_country", userId) %></TD></TR>
                    <TR>
                        <% 
                        int x = 1;
                        for(Language l : Service.languageDAO.findAll()){ 
                            if(x%3 == 0){ 
                            x = 1;
                            %>
                    <TR></TR>
                            <% }
                        %>
                        <TD>
                              <A href='main.jsp?changeLanguage=<%= l.getLanguage() %>_<%= l.getCountry() %>&page=<%= request.getParameter("page")%>'><IMG border="0" alt="Deutsch" src="pic/icons/<%= l.getPic() %>"></A>
                        </TD>
                            
                        <%

                        x++;
                        
                        } %>
                    </TR>
                </TABLE>

            </FORM>
        </TD></TR>
</TABLE>
<%
                    break;

                case SUBPAGE_GENERAL_SETTINGS:%>
<TABLE width="70%">
    <TR><TD colspan=2><BR>
            <FORM method="post" action="main.jsp?page=new/profile&process=<%= PROCESS_CHANGEGENERALSETTINGS%>">
                <TABLE class="blue2" width="90%" align="center" style="font-size:13px">
                    <TR><TD COLSPAN="2">&nbsp;</TD></TR>
                    <TR><TD COLSPAN="2" class="blue2"><CENTER><B><%= ML.getMLStr("profile_lbl_starmap", userId)%></B></CENTER></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_label_width", userId)%></TD><TD><input type="text" name="starmap_width" size=20 maxlength="30" value="<%= userSettings.getStarmapWidth()%>" ></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_height", userId)%></TD><TD><input type="text" name="starmap_height" size=20 maxlength="30" value="<%= userSettings.getStarmapHeight()%>" ></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_showinowntab", userId)%></TD><TD><input  align="middle" <% if (userSettings.getStarmapExternalBrowser()) {%>checked<%}%> type="checkbox" name="starmap_external" value = 0/></TD></TR>
                    <TR><TD COLSPAN="2">&nbsp;</TD></TR>
                    <TR><TD COLSPAN="2" class="blue2"><CENTER><B><%= ML.getMLStr("profile_lbl_techtreeviewer", userId)%></B></CENTER></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_label_width", userId)%></TD><TD><input type="text" name="ttv_width" size=20 maxlength="30" value="<%= userSettings.getTtvWidth()%>" ></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_height", userId)%></TD><TD><input type="text" name="ttv_height" size=20 maxlength="30" value="<%= userSettings.getTtvHeight()%>" ></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_showinowntab", userId)%></TD><TD><input  align="middle" <% if (userSettings.getTtvExternalBrowser()) {%>checked<%}%> type="checkbox" name="ttv_external" value = 0/></TD></TR>
                    <TR><TD COLSPAN="2">&nbsp;</TD></TR>
                    <TR><TD COLSPAN="2" class="blue2"><CENTER><B><%= ML.getMLStr("profile_lbl_accountsitting", userId)%></B></CENTER></TD></TR>
                    <TR><TD COLSPAN="2"><%= ML.getMLStr("profile_msg_accountsitting", userId)%></TD></TR>
                    <TR><TD COLSPAN="2">&nbsp;</TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_numberofdays", userId)%></TD>

                        <TD>
                            <SELECT style='background-color: #000000; color: #ffffff; width:170px; height:17px; border:0px; text-align: center;' name="leaveDays"> 
                                <OPTION value='0'><%= ML.getMLStr("profile_opt_none", userId)%></OPTION>
                                <%
                                                    for (int i = 1; i <= 28; i++) {
                                                        if (i == 1) {
                                %>
                                <OPTION  <% if (i == userSettings.getLeaveDays()) {%>SELECTED<%}%>  value='<%= i%>'> <%=i%> <%= ML.getMLStr("profile_opt_day", userId)%></OPTION>
                                <%
                                                                                                } else {
                                %>
                                <OPTION  <% if (i == userSettings.getLeaveDays()) {%>SELECTED<%}%> value='<%= i%>'> <%=i%> <%= ML.getMLStr("profile_opt_days", userId)%></OPTION>

                                <%
                                                        }
                                                    }
                                %>
                            </SELECT>

                        </TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_sittedBy", userId)%></TD>
                        <TD>
                            <SELECT style='background-color: #000000; color: #ffffff; width:170px; height:17px; border:0px; text-align: center;' name="sittedBy"> 
                                <OPTION  value='0'><%= ML.getMLStr("profile_opt_none", userId)%></OPTION>

                                <%
                                                    ArrayList<User> users = ProfileService.findAllUsers();
                                                    for (User u : users) {
                                                        if ((u.getUserId() == user.getUserId()) || (u.getUserId() == 0)) {
                                                            continue;
                                                        }
                                %>
                                <OPTION  <% if (u.getUserId() == userSettings.getSittedById()) {%>SELECTED<%}%> value='<%= u.getUserId()%>'> <%=u.getGameName()%> </OPTION>

                                <%
                                                    }
                                %>
                            </SELECT>

                        </TD></TR>
                    <TR><TD COLSPAN="2" class="blue2"><CENTER><B><%= ML.getMLStr("profile_lbl_advancedsettings", userId)%></B></CENTER></TD></TR>
                    <TR><TD><%= ML.getMLStr("profile_lbl_deactivateconstructiondetails", userId)%></TD><TD> <INPUT  <% if (!userSettings.getShowConstructionDetails()) {%>checked<%}%> type="CHECKBOX" name="showConDetails" value='0'/></TD></TR>
                     <TR><TD><%= ML.getMLStr("profile_lbl_deactivateproductiondetails", userId)%></TD><TD> <INPUT  <% if (!userSettings.getShowProductionDetails()) {%>checked<%}%> type="CHECKBOX" name="showProdDetails" value='0'/></TD></TR>
                   <TR><TD COLSPAN="2">&nbsp;</TD></TR>

                    <TR><TD align="center" colspan=2><P><BR><input value="<%= ML.getMLStr("profile_but_update", userId)%>" type=submit></TD></TR>
                </TABLE>
            </FORM>
        </TD></TR>
</TABLE>
<%


                    break;

                case (SUBPAGE_NOTIFICATIONS):
%>
<FORM method="post" action="main.jsp?page=new/profile&process=<%= PROCESS_UPDATENOTIFICATION%>">
    <TABLE width="80%" class="blue">

        <TR class="blue2">
            <TD valign="middle"><%= ML.getMLStr("profile_lbl_notification", userId)%></TD>
            <TD valign="middle"><%= ML.getMLStr("profile_lbl_description2", userId)%></TD>
            <TD align="center" valign="middle"><%= ML.getMLStr("profile_lbl_loggedmode", userId)%></TD>
            <TD align="left" width="20px"><IMG alt="loggedmode" onmouseover="doTooltip(event,'<%= ML.getMLStr("profile_pop_loggedmode", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.picPath()%>pic/infoT.png" /></TD>
            <TD align="center" valign="middle"><%= ML.getMLStr("profile_lbl_awaymode", userId)%></TD>
            <TD align="left" width="20px"><IMG alt="awaymode" onmouseover="doTooltip(event,'<%= ML.getMLStr("profile_pop_awaymode", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.picPath()%>pic/infoT.png" /></TD>
        </TR>
        <%
                            for (NotificationType n : ProfileService.findNotification()) {
                                NotificationToUser ntu = ProfileService.findNotificationToUserBy(userId, n.getId());
        %>
        <TR style="font-size:13px">
            <TD><%= ML.getMLStr(n.getName(), userId)%></TD>
            <TD><%= ML.getMLStr(n.getDescription(), userId)%></TD>
            <%
                                   if (ntu == null) {
            %>
            <TD align="center"><INPUT type="checkbox" checked></TD>
            <TD></TD>
            <TD align="center"><INPUT type="checkbox" checked></TD>
            <TD></TD>
            <%} else {%>
            <TD align="center"><INPUT name="checkEnabled<%= n.getId()%>" type="checkbox" <%if (ntu.getEnabled()) {%>checked<%}%>></TD>
            <TD></TD>
            <TD align="center"><INPUT name="checkAway<%= n.getId()%>" type="checkbox" <%if (ntu.getAwayMode()) {%>checked<%}%>></TD>
            <TD></TD>
            <%
                                   }
            %>
        </TR>
        <%
                            }
        %>
        <TR>
            <TD colspan="4" align="center">
                <INPUT type="submit" value="update"/>
            </TD>
        </TR>
    </TABLE>
</FORM>
<%

                    break;

                case (SUBPAGE_STATUSBAR_SETTINGS):

                    //If the player already has a template scheme
                    ArrayList<StyleToUser> stu = ProfileService.findStyleToUserByUserId(userId);

%>
<CENTER>
    <% for (String error : errors) {%>
    <FONT color="RED"><%= error%></FONT>
    <%}%>
</CENTER>
<%
                    if (stu.size() > 0 && type != 3) {
%>
<TABLE>
    <TR>
        <TD>
            <B><%= ML.getMLStr("profile_lbl_chosenstyle", userId)%>:</B>
        </TD>
        <TD class="blue">
            <%= ProfileService.findStyleById(ProfileService.findStyleByUserId(userId)).getName()%>
        </TD>
    </TR>
    <TR>
        <TD>
            &nbsp;
        </TD>
    </TR>
    <TR><TD>
            <FORM method="post" action="main.jsp?page=new/profile&subpage=1&type=3">
                <INPUT type="submit" value="<%= ML.getMLStr("profile_but_neworder", userId)%>">
            </FORM>
        </TD>
        <TD>
            <FORM method="post" action="main.jsp?page=new/profile&subpage=1&process=<%= PROCESS_DELETESTYLEENTRIES%>">
                <INPUT type="submit" value="<%= ML.getMLStr("profile_but_newstyle", userId)%>">
            </FORM>
        </TD>
    </TR>
</TABLE>
<%            //If player has no template Scheme
        } else {


            switch (type) {
                case (0):
%>
<FORM method="post" action="main.jsp?page=new/profile&subpage=1&type=1">
    <TABLE class="blue" width="90%">
        <TR class="blue2">
            <TD class="blue" align="center" colspan="3">
                <B><%= ML.getMLStr("profile_lbl_availableStyles", userId)%></B>
            </TD>
        </TR>
        <%
                                for (Style s : ProfileService.findAllStyles()) {
        %>
        <TR>
            <TD>
                <%= s.getId()%>
            </TD>
            <TD>
                <%= s.getName()%>
            </TD>
            <TD>
                <%= ML.getMLStr(s.getDescription(), userId)%>
            </TD>
        </TR>
        <%
                                }
        %>

    </TABLE>
    <BR>
    <BR>
    <TABLE>
        <TR>
            <TD><%= ML.getMLStr("profile_lbl_availablestyles", userId)%></TD>
            <TD>
                <SELECT NAME="styleid">
                    <%
                                            for (Style s : ProfileService.findAllStyles()) {
                    %>
                    <OPTION ID="<%= s.getId()%>" VALUE="<%=  s.getId()%>"> <%= s.getName()%>
                        <%
                                                }
                        %>
                </SELECT>
            </TD>
        </TR>
        <TR>
            <TD>
                <INPUT TYPE="SUBMIT" VALUE="<%= ML.getMLStr("profil_but_next", userId)%>">
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                        break;

                    case (1):

                        int styleId = 0;
                        if (request.getParameter("styleid") != null) {
                            styleId = Integer.parseInt(request.getParameter("styleid"));
                        }
%>

<FORM  method="post" action="main.jsp?page=new/profile&subpage=1&process=<%= PROCESS_ADDSTYLETOUSER%>">
    <TABLE class="blue" width="60%">
        <TR class="blue2">
            <TD valign="top" align="center" class="blue" colspan="1">
                <B><%= ML.getMLStr("profile_lbl_show", userId)%></B>
            </TD>
            <TD valign="top" align="center" class="blue">
                <B><%= ML.getMLStr("profile_lbl_name", userId)%></B>
            </TD>
            <TD valign="top" align="center" class="blue">
                <B><%= ML.getMLStr("profile_lbl_styledescription", userId)%></B>
            </TD>
        </TR>
        <%
                                Menu m = new Menu(0, userId, planetId, false);
                                MenuUtilities mu = new MenuUtilities(m);
                                StatusBarData sbd = new StatusBarData();

                                // Logger.getLogger().write("In buildStatusBar: "+planetId);

                                MenuUtilities.addRessources(sbd);
                                sbd.setPlanetId(planetId);
                                mu.loadStatusBarData(sbd);
                                StatusBar statusBar = new StatusBar(sbd, userId);

                                for (StyleElement se : ProfileService.findStyleElementByStyleId(styleId)) {
                                    IStatusBarEntity data = null;
                                    try {
                                        Class clzz = Class.forName(se.getClazz());
                                        Constructor con = clzz.getConstructor(StatusBarData.class, int.class, int.class);

                                        data = (IStatusBarEntity) con.newInstance(sbd, userId, Service.styleDAO.findById(styleId).getWidth());


                                    } catch (Exception ex) {
                                    }
                                    table t = new table();
                                    t = data.addTr(t);

        %>
        <TR>
            <TD valign="top">
                <% if(se.isCore()){ %>
                <INPUT  NAME="seId<%=se.getId()%>" VALUE="<%= se.getId()%>" checked TYPE="CHECKBOX">
                <% }else{ %>
                <INPUT  NAME="seId<%=se.getId()%>" VALUE="<%= se.getId()%>" TYPE="CHECKBOX">
                <% } %>
            </TD>
            <TD valign="top" align="Center" width="50%" style="font-size:12">
                <%= ML.getMLStr(se.getName(), userId)%>
            </TD>
            <TD valign="top" width="50%" style="font-size:12">
                <%= ML.getMLStr(se.getDescription(), userId)%>
            </TD>
        </TR>
        <%
                                }
        %>
        <TR>
            <TD align="center" colspan="3">
                <INPUT TYPE="HIDDEN" NAME="styleid" VALUE="<%= styleId%>">
                <INPUT TYPE="SUBMIT" VALUE="<%= ML.getMLStr("profil_but_next", userId)%>">
            </TD>
        </TR>
    </TABLE>
</FORM>

<%

                        break;

                    case (3):


%>

<FORM  method="post" action="main.jsp?page=new/profile&subpage=1&process=<%= PROCESS_UPDATEORDER%>">
    <TABLE class="blue">
        <TR class="blue2">
            <TD class="blue" colspan="2">
                <%= ML.getMLStr("profile_err_orderincorrect", userId)%>
            </TD>
        </TR>
        <TR class="blue2">
            <TD align="center" style="font-size:12" class="blue">
                <B><%= ML.getMLStr("profile_lbl_element", userId)%></B>
            </TD>
            <TD align="center" style="font-size:12" class="blue">
                <B><%= ML.getMLStr("profile_lbl_position", userId)%></B>
            </TD>
        </TR>
        <%
                                ArrayList<StyleElement> ses = ProfileService.findStyleElementsByUserId(userId);
                                int j = 1;
                                for (StyleElement se : ses) {
                                    String ref = "position" + se.getId();
        %>

        <TR>
            <TD>
                <%= ML.getMLStr(se.getName(), userId)%>
            </TD>
            <TD align="center">
                <SELECT  id="<%= ref%>" name="<%= ref%>">
                    <%
                                        for (int i = 0; i < ses.size(); i++) {

                                            if (i + 1 == j) {
                    %>
                    <OPTION selected value="<%= (i + 1)%>"><%= (i + 1)%></OPTION>
                    <%
                                                            } else {
                    %>
                    <OPTION value="<%= (i + 1)%>"><%= (i + 1)%></OPTION>
                    <%
                                            }
                                        }
                    %>
                </SELECT>
            </TD>
        </TR>
        <%
                                    j++;
                                }
        %>
        <TR>
            <TD align="center" colspan="2">
                <INPUT TYPE="SUBMIT" VALUE="<%= ML.getMLStr("profil_but_next", userId)%>">
            </TD>
        </TR>
    </TABLE>
</FORM>

<%
                                break;

                        }
                    }
            }

%>