<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="at.viswars.construction.InConstructionData"%>
<%@page import="java.util.*"%>
<%@page import="java.io.IOException"%>
<%@page import="at.viswars.view.header.*"%>
<%@page import="at.viswars.planetcalc.*"%>
<%@page import="at.viswars.result.*"%>
<%@page import="at.viswars.Logger.*"%>
<%@page import="at.viswars.service.AdministrationService"%>
<%@page import="at.viswars.utilities.PopulationUtilities"%>
<%@page import="at.viswars.utilities.ConstructionUtilities"%>
<%@page import="at.viswars.viewbuffer.HeaderBuffer"%>
<%@page import="at.viswars.enumeration.*"%>
<%@page import="at.viswars.result.*"%>
<%@page import="at.viswars.service.*"%>
<%@page import="at.viswars.model.*"%>
<%! public void writeRessourceEntry(
            JspWriter out, String ressource, String imgName, long currentAmount, long storage,
            boolean mineable, int userId, String picPath) throws IOException {
        String basePicPath = GameConfig.picPath();


            imgName += picPath;
        
        if (currentAmount > 0) {
            out.println("<tr>");
            out.println("<td align=\"left\" valign=\"middle\">");
            out.println("<img width=\"20\" height=\"20\" onmouseout=\"hideTip()\" " + "onmouseover=\"doTooltip(event,'" + ressource + " " + ML.getMLStr("administration_pop_stock", userId) + " ')\"" + " alt=\"" + ressource + "\" src=\"" + imgName + "\">");
            out.println("</td>");
            out.println("<td valign=\"middle\">");
            if (!mineable) {
                out.println("&nbsp;" + FormatUtilities.getFormatScaledNumberWithShare(
                        currentAmount, storage, GameConstants.VALUE_TYPE_NORMAL));
            } else {
                out.println("&nbsp;" + FormatUtilities.getFormatScaledNumber(
                        currentAmount, GameConstants.VALUE_TYPE_NORMAL));
            }
            out.println("</td>");
            out.println("</tr>");
        }
    }
%>
<%
    int process = 0;
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int actCategory = Integer.parseInt((String) session.getAttribute("actCategory"));
    final int PROCESS_BUILDBUILDING = 1;
    if(request.getParameter("process") != null){
            process = Integer.parseInt(request.getParameter("process"));
        }

    if (process > 0) {
        ConstructionService.buildBuilding(userId, request.getParameterMap());
    }
    HeaderData hData = HeaderBuffer.getUserHeaderData(userId);
    HashMap<Integer, SystemEntity> allActSystems = hData.getCategorys().get(actCategory).getSystems();

%>


<TABLE border="0" class="blue" width="100%"  cellpadding="0" cellspacing="0">
    <%
                for (Map.Entry<Integer, SystemEntity> sys : allActSystems.entrySet()) {
                    // Logger.getLogger().write("Checking System (id: " + sys.getId()+")");
                    at.viswars.model.System system = AdministrationService.findSystem(sys.getKey());
                    int invisibleCount = 0;
                    for (Map.Entry<Integer, PlanetEntity> pEntry : sys.getValue().getPlanets().entrySet()) {
                        int pId = pEntry.getKey();
                        if (AdministrationService.findPlayerPlanet(pId).getInvisibleFlag()) {
                            invisibleCount++;
                            continue;
                        }
                    }

                    String basePicPath = GameConfig.picPath();

    %>
    <TR class="blue2">
        <TD>
            <span onclick="toogleVisibility('system<%= system.getId()%>');
                exchangeImgs('img_system<%= system.getId()%>', '<%=basePicPath%>pic/icons/menu_aus.jpg',
                '+','<%=GameConfig.picPath()%>pic/icons/menu_ein.jpg','-');">
                <img src="<%=GameConfig.picPath()%>pic/icons/menu_ein.jpg" alt="-" border="0" id="img_system<%= system.getId()%>">&nbsp;
                <%= ML.getMLStr("administration_lbl_system", userId)%> <B><%= system.getName()%></B>
                (<%= sys.getValue().getPlanets().size()%> <%= ML.getMLStr("administration_lbl_colonizedplanets", userId)%> [<%= invisibleCount%> <%= ML.getMLStr("administration_lbl_hidden", userId)%>])
            </span>
            &nbsp;
            <IMG alt="Send Fleet" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_lbl_sendfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=sendfleet&systemId=<%= system.getId()%>'" src="<%= basePicPath%>pic/movefleet.jpg" />
        </TD>
    </TR>
    <TR id="system<%= system.getId()%>">
        <TD>
            <TABLE width="100%" cellpadding="0" border="0" cellspacing="0">
                <TR>
                    <TD COLSPAN="5">
                        <TABLE class="blue" border="0" width="100%" cellpadding="0" cellspacing="0">
                            <TR class="blue2">
                                <TD width="15%">
                                    <Center><%= ML.getMLStr("administration_lbl_planet", userId)%></Center>
                                </TD>
                                <TD width="10%">
                                    <Center><%= ML.getMLStr("administration_lbl_actions", userId)%></Center>
                                </TD>
                                <TD width="25%">
                                    <Center><%= ML.getMLStr("administration_lbl_ressources", userId)%></Center>
                                </TD>
                                <TD width="25%">
                                    <Center><%= ML.getMLStr("administration_lbl_economy", userId)%></Center>
                                </TD>
                                <TD width="25%">
                                    <Center><%= ML.getMLStr("administration_lbl_population", userId)%></Center>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
                <%

                       for (Map.Entry<Integer, PlanetEntity> pEntry : sys.getValue().getPlanets().entrySet()) {
                           int pId = pEntry.getKey();
                               PlayerPlanet pp = AdministrationService.findPlayerPlanet(pId);
                               Planet p = AdministrationService.findPlanet(pId);

                %>
                <TR class="blue2">
                    <TD class="blue2" align="left" colspan="4">
                        Planet <B><%= pp.getName()%></B>
                    </TD>
                </TR>
                <TR>
                    <TD COLSPAN="6">
                        <TABLE class="blue" border="0" width="100%" class="blue">
                            <%

                               if (pp.getInvisibleFlag()) {
                                   continue;
                               }
                               ResearchResult rr = ResearchService.getResearchPointsForPlanet(pId);
                               PlanetCalculation pc = new PlanetCalculation(pId);
                               PlanetCalcResult pcr = pc.getPlanetCalcData();
                               ProductionResult pd = pcr.getProductionData();
                               ExtPlanetCalcResult epc = pcr.getExtPlanetCalcData();

                            %>
                            <TR>
                                <TD valign="top" width="15%">
                                    <SPAN onclick="window.location.href = 'selectview.jsp?showPlanet=<%= pp.getPlanetId()%>';">
                                        Planet <B><%= pp.getName()%></B><br>
				        (<%= system.getId()%>:<%= pp.getPlanetId()%>)<BR>
                                        <BR>
                                        <IMG alt="Planet" src="<%= basePicPath + "pic/" + p.getPlanetPic()%>" />
                                    </SPAN>
                                </TD>
                                <TD VALIGN="top" WIDTH="10%">
                                    <IMG alt="Send Fleet" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_lbl_sendfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=sendfleet&planetId=<%= pp.getPlanetId()%>'" src="<%= basePicPath%>pic/movefleet.jpg" />
                                </TD>
                                <TD valign="top" WIDTH="25%">
                                    <TABLE width="100%" cellspacing="0" cellpadding="0" style="font-size:13px;" >
                                        <%
                           for (at.viswars.model.Ressource r : AdministrationService.getMineableRessources()) {
                               writeRessourceEntry(out, ML.getMLStr(r.getName(), userId), r.getImageLocation(), (long) pd.getRessStock(r.getId()), (long) pd.getRessMaxStock(r.getId()), true, userId, GameConfig.picPath());
                           }
                                        %>
                                        <TR>
                                            <TD colspan="2" WIDTH="100">
                                                <HR>
                                            </TD>
                                        </TR>
                                        <%
                               for (at.viswars.model.Ressource r : AdministrationService.getStoreableRessources()) {
                                   writeRessourceEntry(out, ML.getMLStr(r.getName(), userId), r.getImageLocation(), (long) pd.getRessStock(r.getId()), (long) pd.getRessMaxStock(r.getId()), false, userId, GameConfig.picPath());
                               }
                                        %>
                                    </TABLE>
                                </TD>
                                <TD valign="top"  WIDTH="25%">
                                    <TABLE cellspacing="0" cellpadding="0" style="font-size:13px;" >

                                        <%
                                                   // Prepare food values
                                                   long foodProd = pd.getRessProduction(at.viswars.model.Ressource.FOOD);
                                                   long foodCons = pd.getRessConsumption(at.viswars.model.Ressource.FOOD);
                                                   long foodStorage = pd.getRessStock(at.viswars.model.Ressource.FOOD);
                                                   long foodStorageTotal = pd.getRessMaxStock(at.viswars.model.Ressource.FOOD);


                                        %>
                                        <TR>
                                            <TD valign="middle">
                                                <IMG onmouseout="hideTip()"
                                                     onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_lbl_creditsincome", userId)%>')"
                                                     alt="<%= ML.getMLStr("administration_lbl_credits", userId)%>"
                                                     src="<%= basePicPath%>pic/icons/money.jpg"/>
                                            </TD>
                                            <TD valign="middle">
                                                &nbsp;
                                                <%= FormatUtilities.getFormattedNumber(epc.getTaxIncome())%>
                                                (<%= FormatUtilities.getFormattedNumber(pp.getTax())%>%)
                                            </TD>
                                        </TR>

                                        <%
                                                   // Prepare Energy
                                                   long energyProd = pd.getRessProduction(at.viswars.model.Ressource.ENERGY);
                                                   long energyCons = pd.getRessConsumption(at.viswars.model.Ressource.ENERGY);
                                        %>
                                        <TR>
                                            <TD valign="middle">
                                                <IMG onmouseout="hideTip()"
                                                     onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_lbl_energy", userId)%>:<br><%= ML.getMLStr("administration_lbl_production", userId)%> <%= FormatUtilities.getFormatScaledNumber(energyProd, GameConstants.VALUE_TYPE_NORMAL)%><br> <%= ML.getMLStr("administration_lbl_consumption", userId)%>: <%= FormatUtilities.getFormatScaledNumber(
                        energyCons, GameConstants.VALUE_TYPE_NORMAL)%>')"
                                                     alt="<%= ML.getMLStr("administration_lbl_energyconsumption", userId)%>"
                                                     src="<%= basePicPath%>pic/icons/energy.jpg">
                                            </TD>
                                            <TD valign="middle">  &nbsp;
                                                <FONT color="<%= FormatUtilities.getColorFor(energyCons, energyProd)%>">
                                                    <%= FormatUtilities.getFormattedDecimal(energyCons * 100.0f / energyProd, 0)%>%
                                                </FONT>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD valign="middle">
                                                <IMG onmouseout="hideTip()"
                                                     onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_productivity", userId)%>:<br> <%= ML.getMLStr("administration_pop_industry", userId)%> :<%= epc.getEffIndustry()%>  <br> <%= ML.getMLStr("administration_pop_agrar", userId)%>: <%= epc.getEffAgriculture()%><br> <%= ML.getMLStr("administration_pop_research", userId)%>: <%= epc.getEffResearch()%>')"
                                                     alt="<%= ML.getMLStr("administration_lbl_productivity", userId)%>"
                                                     src="<%= basePicPath%>pic/icons/efficienty.jpg"/>
                                            </TD>
                                            <TD valign="middle">
                                                &nbsp;
                                                <FONT color="<%= FormatUtilities.getColorFor(Math.round(epc.getEffIndustry() * 100), 100, 60)%>">
                                                    <%= epc.getEffIndustry() * 100%> %
                                                </FONT>&nbsp;/&nbsp;
                                                <FONT color="<%= FormatUtilities.getColorFor(Math.round(epc.getEffAgriculture() * 100), 100, 60)%>">
                                                    <%= epc.getEffAgriculture() * 100%> %
                                                </FONT>&nbsp;/&nbsp;
                                                <FONT color="<%= FormatUtilities.getColorFor(Math.round(epc.getEffResearch() * 100), 100, 60)%>">
                                                    <%= epc.getEffResearch() * 100%> %
                                                </FONT>
                                            </TD>
                                        </TR>
                                        <% if (rr.getResearchPoints() > 0 || rr.getCompResearchPoints() > 0){ %>
                                        <TR>
                                            <TD colspan="2" width="100%">
                                                <HR>
                                            </TD>
                                        </TR>
                                        <% } %>
                                        <%
                                        if(rr.getResearchPoints() > 0){
                                        %>
                                        <TR>
                                            <TD valign="middle">
                                                <IMG width="20" height="20" onmouseout="hideTip()"
                                                     onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_research", userId)%>')"
                                                     alt="Forschung"
                                                     src="<%= basePicPath%>pic/menu/icons/research.png"/>
                                            </TD>
                                            <TD>
                                                <%= rr.getResearchPoints()%>
                                            </TD>
                                        </TR>
                                        <%
                                        }
                                        if(rr.getCompResearchPoints() > 0){
                                        %>
                                        <TR>
                                            <TD valign="middle">
                                                <IMG  width="20" height="20" onmouseout="hideTip()"
                                                      onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_lbl_computerresearch", userId)%>')"
                                                      alt="<%= ML.getMLStr("administration_lbl_computerresearch", userId)%>"
                                                      src="<%= basePicPath%>pic/menu/icons/cresearch.png"/>
                                            </TD>
                                            <TD>
                                                <%= rr.getCompResearchPoints()%>
                                            </TD>
                                        </TR>
                                        <%} %>
                                    </TABLE>
                                </TD>
                                <TD VALIGN="top" WIDTH="25%">
                                    <TABLE cellspacing="0" cellpadding="0" style="font-size:13px;" >
                                        <TR>
                                            <TD valign="middle">
                                                <IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_currentpopulation", userId)%>')"
                                                     onmouseout="hideTip()"
                                                     alt="<%= ML.getMLStr("administration_lbl_population", userId)%>"
                                                     src="<%= basePicPath%>pic/icons/population.jpg" />
                                            </TD>
                                            <TD valign="middle">
                                                &nbsp;
                                                <%= FormatUtilities.getFormatScaledNumber(pp.getPopulation(), GameConstants.VALUE_TYPE_NORMAL)%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <%
                           String ttfood = ML.getMLStr("administration_pop_food", userId) + ":<br> ";
                           ttfood += ML.getMLStr("administration_pop_production", userId) + ": ";
                           ttfood += FormatUtilities.getFormatScaledNumber(foodProd, GameConstants.VALUE_TYPE_NORMAL) + "<br> ";
                           ttfood += ML.getMLStr("administration_pop_consumption", userId) + ": ";
                           ttfood += FormatUtilities.getFormatScaledNumber(foodCons, GameConstants.VALUE_TYPE_NORMAL) + "<br> ";
                           ttfood += ML.getMLStr("administration_pop_stock", userId) + ":";
                           ttfood += FormatUtilities.getFormatScaledNumberWithShare(foodStorage, foodStorageTotal, GameConstants.VALUE_TYPE_NORMAL);
                                            %>
                                            <TD VALIGN="top">
                                                <IMG onmouseout="hideTip()" onmouseover="doTooltip(event,'<%= ttfood%>')"
                                                     alt="<%= ML.getMLStr("administration_lbl_food", userId)%>"
                                                     src="<%= basePicPath%>pic/icons/food.jpg" />
                                            </TD>
                                            <TD VALIGN="top">
                                                &nbsp;
                                                <FONT color="<%=(((foodProd > foodCons) || (foodProd - foodCons != 0) && (foodStorage / (foodCons - foodProd) > 200))
                        ? "#33FF00" : (((foodProd == foodCons) || (foodProd - foodCons != 0) && ((foodStorage / (foodCons - foodProd)) > 50))
                        ? "yellow" : "red"))%>">
                                                    <%= FormatUtilities.getFormatScaledNumber(
                        foodProd, GameConstants.VALUE_TYPE_NORMAL)%> /
                                                    <%= FormatUtilities.getFormatScaledNumber(
                        foodCons, GameConstants.VALUE_TYPE_NORMAL)%>
                                                    <BR>&nbsp;(
                                                    <%= FormatUtilities.getFormatScaledNumberWithShare(
                        foodStorage, foodStorageTotal,
                        GameConstants.VALUE_TYPE_NORMAL)%>
                                                </FONT>
                                            </TD>

                                        </TR>
                                        <TR>
                                            <TD VALIGN="top">
                                                <img onmouseout="hideTip()"
                                                     onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_growth", userId)%>')"
                                                     alt="<%= ML.getMLStr("administration_lbl_growth", userId)%>"
                                                     src="<%= basePicPath%>pic/icons/grow.jpg">
                                            </TD>
                                            <TD valign="middle">&nbsp;
                                                <FONT color="<%= FormatUtilities.getColorFor(0, epc.getGrowth())%>">
                                                    <%= FormatUtilities.getFormattedDecimal(epc.getGrowth(), 2)%>
                                                </FONT>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD valign="middle">
                                                <IMG onmouseout="hideTip()"
                                                     onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_moralchange", userId)%>')"
                                                     alt="<%= ML.getMLStr("administration_pop_morale", userId)%>"
                                                     src="<%= basePicPath%>pic/icons/moral.jpg"/>
                                            </TD>
                                            <TD valign="middle">&nbsp;
                                                <FONT color="<%= FormatUtilities.getColorFor(epc.getMorale(), 80, 50)%>">
                                                    <%= FormatUtilities.getFormattedDecimal(epc.getMorale(), 0)%>
                                                </FONT>
                                                <%= PopulationUtilities.getChange(pId)%>
                                            </TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>

                <FORM method="post" action="main.jsp?page=new/administration-body-main&process=<%= PROCESS_BUILDBUILDING %>">
                    <TR>
                        <!--<TD width="33%">
                            <SELECT  style='background-color: #000000; color: #ffffff; border:0px; '>
                                <OPTION>Alle</OPTION>
                                <OPTION>Zivil</OPTION>
                                <OPTION>Energie</OPTION>
                                <OPTION>Industrie</OPTION>
                                <OPTION>Milit�r</OPTION>
                            </SELECT>
                        </TD>-->
                        <TD width="*" align="right" >
                        <TD bgcolor="gray" width="270px" align="right" >
                            <SELECT  style='background-color: #000000; color: #ffffff; border:0px; width: 250px' NAME="cId" id="cId">
                                <%

                        TreeMap<EConstructionType, ProductionBuildResult> pbrs = AdministrationService.getConstructionList(userId, pp.getPlanetId());

                       for (Map.Entry<EConstructionType, ProductionBuildResult> entry : pbrs.entrySet()) {
                            boolean buildableFound = false;
                          ProductionBuildResult pbr = entry.getValue();
                           ArrayList<Buildable> tmp =  pbr.getUnits();
                           for(Buildable b : tmp){
                               Construction c = (Construction)b.getBase();
                               BuildableResult br = pbr.getBuildInfo(b);
                           //BuildableResult br = ConstructionUtilities.canBeBuilt(, userId, planetId, count)
                           if (br.isBuildable()) {
                               if(!buildableFound){
                                    buildableFound = true;
                                   }

                                %>
                                <OPTION VALUE="<%= c.getId()%>"><%= ML.getMLStr(c.getName(), userId)%> (<%= br.getPossibleCount()%>)</OPTION>
                                <%
                           }
                           }
                           if(buildableFound){
                           %>
                           <OPTION disabled><------</OPTION>

                           <%
                           }
                       }
                                %>
                            </SELECT>
                        </TD>

                        <TD width="210px" bgcolor="gray" ALIGN="right">
                            <FONT style="font-size: 12px"><%= ML.getMLStr("administration_lbl_quantity", userId)%>:</FONT> <INPUT NAME="number" type="text" size="10">
                        </TD>
                        <TD width="110px" bgcolor="gray" align="right" >
                            <INPUT type="HIDDEN" NAME="planetId" value="<%= pId%>">
                            <INPUT type="SUBMIT"  size="25" value="<%= ML.getMLStr("administration_but_buildnow", userId)%>">
                        </TD>
                    </TR>
                </FORM>
                <%
LinkedList<InConstructionData> icds = at.viswars.utilities.ConstructionUtilities.getInConstructionData(pId);
if (icds.size() > 0) {
                %>
                <TR>
                    <TD colspan="5">
                        <TABLE style="font-size:12px" width="100%">
                            <TR>
                                <TD align="left">
                                    <U><B><%= ML.getMLStr("administration_lbl_buildingsinconstruction", userId)%></B></U>
                                </TD>
                            </TR>
                            <%
                for (InConstructionData icd : icds) {%>
                            <TR>
                                <TD>
                                    <%= icd.getConstructionData().getCount()%>
                                    x <%= ML.getMLStr(icd.getConstructionData().getConstruct().getName(), userId)%>
                                </TD>
                            </TR>
                            <%
            }
                            %>
                        </TABLE>
                    </TD>
                </TR>
                <%
        }
    }
                %>
            </TABLE>
        </TD>
    </TR>
    <%
        }
    %>
</TABLE><%

%>
