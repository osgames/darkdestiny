<%@page import="at.viswars.utilities.LoginUtilities"%>
<%@page import="at.viswars.utilities.img.BaseRenderer"%>
<%@page import="at.viswars.utilities.img.BackgroundPainter"%>
<%@page import="at.viswars.utilities.img.PlanetRenderer"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.service.SystemService"%>
<%@page import="at.viswars.model.Planet"%>
<%@page import="at.viswars.utilities.img.IModifyImageFunction"%>
<%@page import="java.util.List"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="image/png" %>

<%
LoginUtilities.checkLogin(request, response);

List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();
// Check for DATA_FOR_SCAN_PIC

int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int mode = 1;
int picSize = 200;

if (request.getParameter("planetId") != null) {
    planetId = Integer.parseInt(request.getParameter("planetId"));
}

if (request.getParameter("mode") != null) {
    mode = Integer.parseInt(request.getParameter("mode"));
    if (mode == PlanetRenderer.LARGE_PIC) {
        picSize = 200;
    } else if (mode == PlanetRenderer.SMALL_PIC) {
        picSize = 40;

    }
}

allItems.add(new PlanetRenderer(planetId,this.getServletContext().getRealPath(GameConfig.picPath()+"pic/") + "/",mode));
Planet p = SystemService.findPlanetById(planetId);
// Wenn A oder B gr��eres Bild zeichnen
if(mode == PlanetRenderer.SMALL_PIC){
    if (p.getLandType().equals(Planet.LANDTYPE_A)) {
        picSize += 20;
    } else if (p.getLandType().equals(Planet.LANDTYPE_B)) {
         picSize += 10;
    }
}
BaseRenderer renderer = new BaseRenderer(picSize,picSize);
// Schreibt den Mime-Type in den Log, Nur Benutzen, wenn
// genau das gew&uuml;nscht wird
// renderer.getImageMimeType();

renderer.modifyImage(new BackgroundPainter(0x000000));

for (IModifyImageFunction mif : allItems) {
	renderer.modifyImage(mif);
}

renderer.sendToUser(response.getOutputStream());
%>