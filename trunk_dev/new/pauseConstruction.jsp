<%@page import="at.viswars.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="at.viswars.service.ConstructionService" %>
<%@page import="at.viswars.service.Service" %>
<%@page import="at.viswars.result.BuildableResult" %>
<%@page import="at.viswars.result.InConstructionResult" %>
<%@page import="at.viswars.result.InConstructionEntry" %>
<%@page import="at.viswars.model.*" %>
<%@page import="at.viswars.view.*" %>
<%@page import="at.viswars.result.*" %>
<%@page import="at.viswars.utilities.*" %>
<%@page import="at.viswars.enumeration.*" %>

<%

            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
            int constructionId = Integer.parseInt((String) request.getParameter("constructionId"));

            Construction c = ConstructionService.findConstructionById(constructionId);
            PlanetConstruction pc = Service.planetConstructionDAO.findBy(planetId, constructionId);
%>
<BR><BR>
<FORM method="post" action="main.jsp?page=new/construction&cId=<%= constructionId %>&idle=true">
<TABLE align="center" style="width:300px; font-size: 12px; font-weight: bolder">
    <TR>
        <TD align="center" colspan="2">
            Bitte die Anzahl der Geb&auml;ude w&auml;hlen, die stillgelegt werden sollen.<BR><BR>
        </TD>
    </TR>
    <TR class="blue2">
        <TD align="center" colspan="2">
            <%= ML.getMLStr(c.getName(), userId) %>
        </TD>
    </TR>
    <TR>
        <TD align="left" style="width:60%;" align="center">
            Anzahl
        </TD>
        <TD align="right">
            <INPUT type="text" name="count" size="3" value="<%= pc.getIdle() %>" /> / <%= pc.getNumber() %>
        </TD>
    </TR>
    <TR>
        <TD align="center" colspan="2">
            <INPUT value="Update" type="submit"/>
        </TD>
    </TR>
</TABLE>
</FORM>