<%@page import="at.viswars.*" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
int userId = Integer.parseInt((String)session.getAttribute("userId"));
                session.setAttribute("actPage", "new/transport");
%>
<TABLE width="80%">
    <TR align="center">
        <TD width="13%"><U><A href="main.jsp?page=new/transport_new&type=0"><%= ML.getMLStr("transport_link_overview", userId) %></A></U></TD>
        <TD align="center">
            <U><A href="main.jsp?page=new/transport_new&type=2"><%= ML.getMLStr("transport_link_planet", userId) %></A></U>
        </TD>
        <TD width="13%"><U><A href="main.jsp?page=new/transport_new&type=1"><%= ML.getMLStr("transport_link_addroute", userId) %></A></U></TD>
    </TR>
</TABLE><BR>
<%
int type = 2;

if (request.getParameter("type") != null) {
    type = Integer.parseInt((String)request.getParameter("type"));
}

if (type == 0) {
%>
    <jsp:include page="transport-overview_new.jsp" />
<%
} else if(type == 1){
%>
    <jsp:include page="transport-newRoute.jsp" />
<%
}else{
%>
    <jsp:include page="transport-planet_new.jsp" />
<%
}
%>
