<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.service.*"%>
<%@page import="at.viswars.result.*"%>
<%@page import="java.util.*"%>

<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int designId = 0;

BaseResult actionResponse = null;

// Check for parameters
if (request.getParameter("designId") != null) {
    designId = Integer.parseInt(request.getParameter("designId"));
}

boolean skipContent = false;

if (request.getParameter("newName") != null) {
    String newName = request.getParameter("newName");
    actionResponse = ShipDesignService.renameDesign(userId, designId, newName);
} else if (request.getParameter("action") != null) {
    String action = request.getParameter("action");
    
    if (action.equalsIgnoreCase("create")) {
        actionResponse = ShipDesignService.createDesign(userId);
    } else if (action.equalsIgnoreCase("delete")) {
        actionResponse = ShipDesignService.deleteDesign(userId, designId);
    } else if (action.equalsIgnoreCase("copy")) {
        actionResponse = ShipDesignService.copyDesign(userId, designId);
    } else if (action.equalsIgnoreCase("modify")) {
%>
        <jsp:include page="shipdesign.jsp" />           
<%                      
        return;
    }
}

if (!skipContent) {
    ArrayList<ShipDesign> designs = ShipDesignService.getAllDesigns(userId);
    boolean canDesign = ResearchService.isResearched(userId, 25);

            if (canDesign) {
%>              
            <BR>
            <FORM name="shipdesign">
            <CENTER><B><FONT size="+1">Schiffs Design �bersicht</FONT></B></CENTER>
            <BR>
<%
            if ((actionResponse != null) && (actionResponse.isError())) {
%>
                <BR>
                <FONT color="RED"><B>ERROR: <%= actionResponse.getMessage()%></B></FONT>
                <BR><BR>
<%
            }
%>
            <TABLE width="80%">
            <TR>
            <TD>
                <select size='20' id="selectedDesign" name="selectedDesign" onchange="
                    if (document.getElementById('selectedDesign').selectedIndex != 0) { 
                        document.getElementById('newName').value = document.getElementById('selectedDesign').options[document.getElementById('selectedDesign').selectedIndex].text; 
                    } else { 
                        document.getElementById('newName').value = ''; 
                    }">
                    <option value="0">-- Design selektieren --</option>
                    <%
                    for (ShipDesign sd : designs) {
                        %><option value='<%= sd.getId() %>'><%= sd.getName() %> </option><%
                    }
                    %>
                </select>
            </TD>
            <TD align="right">
                <INPUT type="button" value="Neu" onclick="window.location.href = 'main.jsp?page=new/production&type=4&action=create';"><P>
                <INPUT type="button" value="Bearbeiten" onclick="if (document.getElementById('selectedDesign').value != 0) { window.location.href = 'main.jsp?page=new/production&type=4&designId='+document.getElementById('selectedDesign').value+'&action=modify'; }"><P>
                <INPUT type="text" id="newName" name="newName" size="15">&nbsp;
                <INPUT type="button" value="Umbennen" onclick="if (document.getElementById('selectedDesign').value != 0) { window.location.href = 'main.jsp?page=new/production&type=4&newName='+document.getElementById('newName').value+'&designId='+document.getElementById('selectedDesign').value; } else { document.getElementById('newName').value = ''; }"><P> 
                <INPUT type="button" value="Kopieren" onclick="if (document.getElementById('selectedDesign').value != 0) { window.location.href = 'main.jsp?page=new/production&type=4&designId='+document.getElementById('selectedDesign').value+'&action=copy'; }"><P>
                <INPUT type="button" value="L&ouml;schen" onclick="if (document.getElementById('selectedDesign').value != 0) { window.location.href = 'main.jsp?page=new/production&type=4&designId='+document.getElementById('selectedDesign').value+'&action=delete'; }"><P>
            </TD>
            </TR>
            </TABLE>
            </FORM>
            <BR><BR>
<%
            } else { %>
                <BR><FONT style="font-size:13px;">Noch kein Schiffschassis erforscht!</FONT>
<%          
            }      
    }
%>
