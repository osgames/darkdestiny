<%@page import="java.util.*" %>
<%@page import="at.viswars.*" %>
   <%

session = request.getSession();

int userId = Integer.parseInt((String) session.getAttribute("userId"));
String subpage = "diplomacy-relations.jsp";
if (request.getParameter("subpage") != null) {
    subpage = request.getParameter("subpage")+".jsp";
}

   %>
 <TABLE>
    <TR>
        <TD width="50%"><U><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relations"><%= ML.getMLStr("diplomacy_link_relations", userId)%></A></U></TD>
        <TD width="50%"><U><A href="main.jsp?page=new/diplomacy&subpage=donatemoney"><%= ML.getMLStr("diplomacy_link_donatemoney", userId)%></A></U></TD>
    </TR>
</TABLE>
   <jsp:include page='<%= subpage%>'/>