<%@page import="at.viswars.*" %>
<%
session = request.getSession();

int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int systemId = Integer.parseInt((String)session.getAttribute("actSystem"));
int showPage = 1;
session.setAttribute("actPage", "new/fleet");

if (request.getParameter("type") != null) {
    try {
        showPage = Integer.parseInt(request.getParameter("type"));
    } catch (NumberFormatException e) { }
}

%>
<TABLE width="80%">
<TR align="center">
<TD width="25%"><U><A href="main.jsp?page=new/fleet&type=1"><%= ML.getMLStr("fleet_link_management", userId) %></A></U></TD>
<TD width="25%"><U><A href="main.jsp?page=new/fleet&type=2"><%= ML.getMLStr("fleet_link_transport", userId) %></A></U></TD>
<TD width="25%"><U><A href="main.jsp?page=new/fleet&type=3"><%= ML.getMLStr("fleet_link_admiral", userId) %></A></U></TD>
<!-- <TD width="25%"><U><A href="main.jsp?page=new/fleet&type=4">Schiffsdesign</A></U></TD> -->
</TR>
</TABLE>
<%
switch (showPage) {
    case(1):
%>
            <jsp:include page="fleetmanagement.jsp" />           
<%         
        break;
    case(2):
%>
            <jsp:include page="fleetloading.jsp" />           
<%                 
        break;
    case(3):
        %>
        <CENTER><IMG src="<%= GameConfig.picPath() %>pic/AdmiralAckbar.jpg"/><BR>
        </CENTER>
        <%
        break;
}
%>