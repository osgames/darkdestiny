<%@page import="at.viswars.utilities.ShippingUtilities"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="at.viswars.construction.InConstructionData"%>
<%@page import="java.util.*"%>
<%@page import="java.io.IOException"%>
<%@page import="at.viswars.view.header.*"%>
<%@page import="at.viswars.planetcalc.*"%>
<%@page import="at.viswars.result.*"%>
<%@page import="at.viswars.service.AdministrationService"%>
<%@page import="at.viswars.utilities.PopulationUtilities"%>
<%@page import="at.viswars.utilities.ConstructionUtilities"%>
<%@page import="at.viswars.viewbuffer.HeaderBuffer"%>
<%@page import="at.viswars.enumeration.*"%>
<%@page import="at.viswars.result.*"%>
<%@page import="at.viswars.service.*"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.filter.*"%>
<%@page import="at.viswars.model.Note"%>
<%@page import="at.viswars.model.Filter"%>
<%@page import="at.viswars.model.Ressource"%>

<%! public void writeRessourceEntry(
            JspWriter out, String ressource, String imgName, long currentAmount, long storage,
            boolean mineable, int userId, String picPath) throws IOException {
        String basePicPath = GameConfig.picPath();


        imgName += picPath;

        if (currentAmount > 0) {
            out.println("<tr>");
            out.println("<td align=\"left\" valign=\"middle\">");
            out.println("<img width=\"20\" height=\"20\" onmouseout=\"hideTip()\" " + "onmouseover=\"doTooltip(event,'" + ressource + " " + ML.getMLStr("administration_pop_stock", userId) + " ')\"" + " alt=\"" + ressource + "\" src=\"" + imgName + "\">");
            out.println("</td>");
            out.println("<td valign=\"middle\">");
            if (!mineable) {
                out.println("&nbsp;" + FormatUtilities.getFormatScaledNumberWithShare(
                        currentAmount, storage, GameConstants.VALUE_TYPE_NORMAL));
            } else {
                out.println("&nbsp;" + FormatUtilities.getFormatScaledNumber(
                        currentAmount, GameConstants.VALUE_TYPE_NORMAL));
            }
            out.println("</td>");
            out.println("</tr>");
        }
    }
%>
<%
            int process = 0;
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int actCategory = Integer.parseInt((String) session.getAttribute("actCategory"));
            final int PROCESS_BUILDBUILDING = 1;
            final int PROCESS_CHANGETAXES = 2;
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }

            if (process > 0) {
                if (process == PROCESS_BUILDBUILDING) {
                    ConstructionService.buildBuilding(userId, request.getParameterMap());
                }
                if (process == PROCESS_CHANGETAXES) {
                    ManagementService.changeTaxes(request.getParameterMap(), userId, Integer.parseInt(request.getParameter("planetId")));
                }
            }


            String basePicPath = GameConfig.picPath();

            HeaderData hData = HeaderBuffer.getUserHeaderData(userId);
            HashMap<Integer, SystemEntity> allActSystems = hData.getCategorys().get(actCategory).getSystems();
            HashMap<Integer, HashMap<Integer, HashMap<Boolean, Long>>> tradeBalances = ShippingUtilities.findTradeBalanceForUserSorted(userId);


// Filter START
            int filterId = 0;
            if (request.getParameter("filterId") != null) {
                filterId = Integer.parseInt(request.getParameter("filterId"));
            }
// Filter ENDE
// Variablen zum verwalten der Seitenanzeige START
            int firstItem = 0;
            int fromItem = 0;
            int toItem = 15;
            int itemsPerPage = 15;

            if (request.getParameter("fromItem") != null) {
                fromItem = Integer.parseInt(request.getParameter("fromItem"));
            }
            if (request.getParameter("toItem") != null) {
                toItem = Integer.parseInt(request.getParameter("toItem"));
            }

            PlanetFilter pf = new PlanetFilter(allActSystems, fromItem, toItem, filterId, userId);
            fromItem = fromItem + 1;
            PlanetFilterResult pfr = pf.getResult();
            int lastItem = pfr.getItems();
            if (toItem > lastItem) {
                toItem = lastItem;
            }
            if (lastItem == 0) {
                fromItem = 0;
            }
// Variablen zum verwalten der Seitenanzeige ENDE

%>
<BR>
<!-- FILTER START  -->
<FORM name="filterForm" method="post">
    <TABLE border="0" style="font-size:11px; color:white;"  align="center" cellpadding="0" cellspacing="0">
        <TR>
            <TD width="100px" class="blue2">
                <%= ML.getMLStr("administration_label_filter", userId) %>:
            </TD>
            <TD class="blue" width="300px" align="center">
                <SELECT name="filterSelect" style='background-color: #000000; color: #ffffff; border:0px; width: 100%; font-size: 11px;' onChange="window.location=document.filterForm.filterSelect.options[document.filterForm.filterSelect.selectedIndex].value">
                    <OPTION value="main.jsp?page=new/new_administration-body-main2&filterId=0&fromItem=<%= firstItem%>&toItem=<%= itemsPerPage%>"><%= ML.getMLStr("administration_opt_none", userId) %></OPTION>
                    <% for (Filter f : FilterService.findAllFilters()) {
                                    if (f.getUserId() != 0 && f.getUserId() != userId) {
                                        continue;
                                    }
                    %>
                    <OPTION value="main.jsp?page=new/new_administration-body-main2&filterId=<%= f.getId()%>&fromItem=<%= firstItem%>&toItem=<%= itemsPerPage%>"  <% if (f.getId() == filterId) {%>SELECTED<%}%>><%= f.getName()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
            <TD width="160px" align="center" style="width:150px;">
                <A href="main.jsp?page=new/filters"><%= ML.getMLStr("administration_lbl_configurefilter", userId) %></A>
            </TD>
        </TR>
    </TABLE>
</FORM>
<BR>
<!-- FILTER END -->
<!-- PageWeiterschaltor START -->
<TABLE border="0" style="font-size:11px; font-weight: bolder; color:white;" width="150px"  align="center" cellpadding="0" cellspacing="0">
    <TR class="blue2">
        <TD align="center" width="10px">
            <% if (fromItem > 1) {%>
            <A href="main.jsp?page=new/new_administration-body-main2&fromItem=0&toItem=15&filterId=<%= filterId%>">
                |<<
            </A>
            <% }%>
        </TD>
        <TD align="center"  width="10px">
            <% if (fromItem > 1) {%>
            <A href="main.jsp?page=new/new_administration-body-main2&fromItem=<%= ((fromItem - 1) - itemsPerPage)%>&toItem=<%= (fromItem - 1)%>&filterId=<%= filterId%>">
                <
            </A>
            <% }%>
        </TD>
        <TD  width="20px" align="center">
            <%= fromItem%> - <%= toItem%>
        </TD>
        <TD align="center"  width="10px">
            <% if (toItem < lastItem) {%>
            <A href="main.jsp?page=new/new_administration-body-main2&fromItem=<%= toItem%>&toItem=<%= (toItem + itemsPerPage)%>&filterId=<%= filterId%>">
                >
            </A>
            <% }%>
        </TD>
        <TD align="center"  width="10px">
            <% if (toItem < lastItem) {
                            int subtractor = pfr.getTotalItems() % itemsPerPage;
            %>
            <A href="main.jsp?page=new/new_administration-body-main2&fromItem=<%= (pfr.getTotalItems() - subtractor)%>&toItem=<%= pfr.getTotalItems()%>&filterId=<%= filterId%>">
                >>|
            </A>
            <%
                        }

            %>
        </TD>
    </TR>
</TABLE>
<!-- PageWeiterschaltor ENDE -->
<BR>
<TABLE border="0" style="font-size:13px" width="90%"  align="center" cellpadding="0" cellspacing="0">
    <%

                // for (Map.Entry<Integer, PlanetFilterResultEntry> pfres : pfr.getResult().entrySet()) {
                for (PlanetFilterResultEntry pfre : pfr.getResult()) {

                    // PlanetFilterResultEntry pfre = pfres.getValue();
                    Planet p = pfre.getP();
                    PlayerPlanet pp = pfre.getPp();
                    ProductionResult pr = pfre.getPr();
                    ExtPlanetCalcResult epcr = pfre.getEpcr();
                    // Content
%>
    <TR><TD align="center">
            <!-- Planeteneintrag-->
            <TABLE border="0" style="padding-bottom:10px;" cellspacing="0">
                <TR style="background-color:#4A4A4A;" valign="top">
                    <!-- Planetpic and general stuff -->
                    <TD width="200px">
                        <TABLE width="100%" cellspacing="0" border="0">
                            <TR>
                                <TD style="background-color:#000000; padding:5px;" align="middle" valign="middle"><IMG alt="Planet" onmouseover="doTooltip(event,'<%= pp.getName()%>')" onmouseout="hideTip()"  onclick="window.location.href = 'selectview.jsp?showPlanet=<%= pp.getPlanetId()%>';"  src="<%= basePicPath + "pic/" + p.getPlanetPic()%>" /></TD>
                            </TR>
                            <TR style="height:3px;"><TD></TD></TR>
                            <TR>
                                <TD align="center" style="background-color:#000000; font-weight: bold; font-size: 13px; padding:3px;"><%= pp.getName()%></TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <!-- Possible actions -->
                    <TD width="30">
                        <TABLE style="background-color:#2E2E2E;" cellpadding="3px" cellspacing="0" border="0">
                            <TR>
                                <TD>
                                    <IMG alt="Planet" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_showsystem", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/system&systemId=<%= p.getSystemId()%>'" src="<%= basePicPath%>pic/goSystem.jpg" />
                                </TD>
                            </TR>
                            <TR>
                                <TD>
                                    <IMG alt="Planet" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_showplanet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'selectview.jsp?showPlanet=<%= pp.getPlanetId()%>';" src="<%= basePicPath%>pic/goPlanet.jpg" />
                                </TD>
                            </TR>
                            <TR>
                                <TD>
                                    <IMG alt="Planet" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_lbl_sendfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/sendfleet&planetId=<%= pp.getPlanetId()%>'" src="<%= basePicPath%>pic/movefleet.jpg" />
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <!-- Rohstoffe -->
                    <TD>
                        <%
                                            ArrayList<at.viswars.model.Ressource> ressources = AdministrationService.getStoreableRessources();
                                            //  ressources.addAll(AdministrationService.getStoreableRessources());
%>
                        <TABLE style="font-size:11px;   font-weight:bold; border-style: solid; border-width: thin; width:100px">

                            <% for (at.viswars.model.Ressource r : ressources) {
                                                    if (r.getId() == at.viswars.model.Ressource.FOOD || pr.getRessStock(r.getId()) <= 0) {
                                                        continue;
                                                    }

                            %>
                            <TR>
                                <TD  onmouseover="doTooltip(event,'<%= ML.getMLStr(r.getName(), userId)%>')" onmouseout="hideTip()"  style=" border-style: solid; border-width: thin;" valign="top" align="center" width="17px"><%  if (pr.getRessStock(r.getId()) > 0) {%>
                                    <IMG style="width:17px; height:17px" src="<%= basePicPath + r.getImageLocation()%>"/>
                                    <% } else {%>

                                    <% }%>
                                </TD>
                                <%

                                     if (pr.getRessStock(r.getId()) > 0) {%>
                                <TD  style=" border-style: solid; border-width: thin;" align="right" valign="top">
                                    <% } else {%>
                                <TD  style=" border-style: solid; border-width: thin;" align="right" valign="top">
                                    <% }%>
                                    <%  if (pr.getRessStock(r.getId()) > 0) {%>
                                    <%= FormatUtilities.getFormattedNumber(pr.getRessStock(r.getId()))%>
                                    <% } else {%>

                                    <% }%>
                                </TD>
                            </TR>
                            <% }%>
                        </TABLE>
                    </TD>
                    <TD>
                        <TABLE  border="0" style="font-size:10px; font-weight:bold;  border-style: solid; border-width: thin;">
                            <% Ressource[] ressMine = RessourceService.getAllMineableRessources();
                                                boolean foundRes = false;
                                                for (int i = 0; i < ressMine.length; i++) {

                                                         if ((ressMine[i].getId() == Ressource.PLANET_YNKELONIUM) && !ResearchService.isResearched(userId, Research.YNKELONIUM)) continue;
                                                    if ((ressMine[i].getId() == Ressource.PLANET_HOWALGONIUM) && !ResearchService.isResearched(userId, Research.HOWALGONIUMSCANNER)) {
                                                        continue;
                                                    }
                                                    long ress = pr.getRessStock(ressMine[i].getId());
                                                    if (ress > 0) {
                                                        foundRes = true;%>
                            <TR valign="top" style="background-color:#2E2E2E;">
                                <TD >
                                    <IMG onmouseover="doTooltip(event,'<%= ML.getMLStr(ressMine[i].getName(), userId)%>: <%= FormatUtilities.getFormattedNumber(ress)%>')" onmouseout="hideTip()" style="width:17px; height:17px" src="<%= basePicPath + ressMine[i].getImageLocation()%>"/>
                                </TD>
                            </TR>
                            <% }%>
                            <% }
                                                if (!foundRes) {%>
                            <TR valign="top">
                                <TD style="width:17px; height:17px" >
                                    &nbsp;
                                </TD>
                            </TR>
                            <% }%>
                        </TABLE>
                    </TD>
                    <!-- Verwaltung -->
                    <TD valign="top">
                        <TABLE width="350px" cellpadding="0" border="0" cellspacing="0">
                            <TR>
                                <TD valign="top">
                                    <TABLE border="0" style="font-size:10px; font-weight:bold;  border-style: solid; border-width: thin;">
                                        <TR style="background-color:#2E2E2E;">
                                            <TD>
                                                <%= ML.getMLStr("administration_lbl_population", userId)%>
                                            </TD>
                                            <TD align="right" width="90px"><%= FormatUtilities.getFormatScaledNumber(pp.getPopulation(), GameConstants.VALUE_TYPE_NORMAL)%> (<%= FormatUtilities.getFormattedDecimal(100d / epcr.getMaxPopulation() * pp.getPopulation(), 1)%>%)</TD>
                                            <TD valign="center">
                                                <IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_currentpopulation", userId)%>')"
                                                     onmouseout="hideTip()" style="height:14px;" style="width:17px; height:17px" src="<%= basePicPath%>pic/icons/population.jpg"/>
                                            </TD>
                                        </TR>
                                        <TR style="background-color:#2E2E2E;">
                                            <TD>
                                                <%= ML.getMLStr("administration_lbl_food", userId)%>
                                            </TD>
                                            <%
                                             long foodIncoming = 0l;
                                                if(tradeBalances.get(pp.getPlanetId()) != null && tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD) != null
                                                        && tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD).get(Boolean.FALSE) != null){
                                                 foodIncoming = tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD).get(Boolean.FALSE);
                                                          }
                                                long foodOutgoing = 0l; 
                                                if(tradeBalances.get(pp.getPlanetId()) != null && tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD) != null
                                                        && tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD).get(Boolean.TRUE) != null){
                                                 foodOutgoing = tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD).get(Boolean.TRUE);
                                                          }
                                                
                                                                long foodBalance = pr.getRessBaseProduction(Ressource.FOOD) - pr.getRessConsumption(Ressource.FOOD);
                                                                long tradeBalance = foodIncoming - foodOutgoing;
                                                                String foodBalanceColor = "WHITE";
                                                                if (foodBalance < 0) {
                                                                    foodBalanceColor = "RED";
                                                                  }else if(foodBalance == 0){
                                                                           foodBalanceColor = "WHITE";                                                                
                                                                  }else{
                                                                           foodBalanceColor = "#33CC33";        
                                                                  }
                                                                String tradeBalanceColor = "WHITE";
                                                                if (tradeBalance < 0) {
                                                                    tradeBalanceColor = "RED";
                                                                  }else if(tradeBalance == 0){
                                                                           tradeBalanceColor = "WHITE";                                                                
                                                                  }else{
                                                                           tradeBalanceColor = "#33CC33";        
                                                                  }
                                                                                                                                      
                                            %>
                                            <TD align="right" width="90px"  onmouseout="hideTip()" onmouseover="doTooltip(event,'Produktion - Verbrauch(Handel)')">
                                                <FONT color="<%= foodBalanceColor %>"><%= FormatUtilities.getFormattedNumber(foodBalance)%></FONT>
                                                (<FONT color="<%= tradeBalanceColor %>"><%= FormatUtilities.getFormattedNumber(tradeBalance)%>)</FONT>
                                            </TD>
                                            
                                            <TD valign="center"><IMG style="height:14px;"  onmouseout="hideTip()" onmouseover="doTooltip(event,'Nahrung')"
                                                                     alt="<%= ML.getMLStr("administration_lbl_food", userId)%>"
                                                                     src="<%= basePicPath%>pic/icons/food.jpg" />
                                            </TD>
                                        </TR>
                                        <TR style="background-color:#2E2E2E;">
                                            <TD>
                                                <%= ML.getMLStr("administration_lbl_energy", userId)%>
                                            </TD>
                                            <%
                                                                long energyBalance = pr.getRessProduction(Ressource.ENERGY) - pr.getRessConsumption(Ressource.ENERGY);

                                                                if (energyBalance < 0) {
                                            %>
                                            <TD align="right" width="90px"><FONT color="RED"><%= FormatUtilities.getFormattedNumber(energyBalance)%></FONT></TD>
                                            <% } else if (energyBalance == 0) {%>
                                            <TD align="right" width="90px">+/-<%= FormatUtilities.getFormattedNumber(energyBalance)%></TD>
                                            <% } else {%>
                                            <TD align="right" width="90px"><FONT color="#33CC33">+<%= FormatUtilities.getFormattedNumber(energyBalance)%></FONT></TD>
                                            <% }%>
                                            <TD valign="center"><IMG style="height:14px;" onmouseout="hideTip()" onmouseover="doTooltip(event,'Energie')"
                                                                     alt="<%= ML.getMLStr("administration_lbl_food", userId)%>"
                                                                     src="<%= basePicPath%>pic/icons/energy.jpg" />
                                            </TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                                <TD>
                                    <% if (fromItem > 1) {%>
                                    <FORM action="main.jsp?page=new/new_administration-body-main2&filterId=<%= filterId%>&fromItem=<%= (fromItem - 1)%>&toItem=<%= (toItem)%>&process=<%= PROCESS_CHANGETAXES%>" method="post" style="margin-bottom: 0px;">

                                        <% } else {%>
                                        <FORM action="main.jsp?page=new/new_administration-body-main2&process=<%= PROCESS_CHANGETAXES%>" method="post" style="margin-bottom: 0px;">

                                            <% }%>
                                            <INPUT type="hidden" name="planetId" value="<%= pp.getPlanetId() %>"/>
                                            <TABLE width="150" style="font-size:10px; font-weight:bold;  border-style: solid; border-width: thin;">
                                                <TR  style="background-color:#2E2E2E;">
                                                    <TD width="*">
                                                        <%= ML.getMLStr("administration_lbl_income", userId)%>
                                                    </TD>
                                                    <TD align="right" colspan="2"><%= FormatUtilities.getFormattedNumber(epcr.getTaxIncome())%></TD>
                                                </TR>
                                                <TR style="background-color:#2E2E2E;">
                                                    <TD  width="*" >
                                                        <%= ML.getMLStr("administration_lbl_taxes", userId)%>
                                                    </TD>
                                                    <TD  align="right"><INPUT name="tax" style="height: 15px; font-size:10px; width:25px;" type="text" value="<%= FormatUtilities.getFormattedNumber(pp.getTax())%>"></TD>
                                                    <TD width="15px"><INPUT style="height: 15px; font-size:15px; width:15px;" type="image" src="<%= basePicPath%>pic/refresh.png"></TD>
                                                </TR>
                                                <TR style="background-color:#2E2E2E;"  >
                                                    <TD  width="*">
                                                        <%= ML.getMLStr("administration_lbl_morale", userId)%>
                                                    </TD>
                                                    <TD align="right" colspan="1">
                                                        <% if (pp.getMoral() > 70) {%>
                                                        <%= epcr.getMorale()%><%= epcr.getMoraleChangeStrWhite()%>
                                                        <% } else {%>
                                                        <font color="red"><%= epcr.getMorale()%><%= epcr.getMoraleChangeStrWhite()%></font>
                                                        <% }%>
                                                    </TD>
                                                    <TD>
                                                        <%

                                                                            String imageIcon = basePicPath + "pic/menu/icons/mood_equal.png";
                                                                            if (epcr.getMorale() >= 100) {
                                                                                imageIcon = GameConfig.picPath() + "pic/menu/icons/mood_verygood.png";
                                                                            } else if (epcr.getMorale() >= 75 && epcr.getMorale() < 100) {
                                                                                imageIcon = GameConfig.picPath() + "pic/menu/icons/mood_good.png";
                                                                            } else if (epcr.getMorale() >= 50 && epcr.getMorale() < 75) {
                                                                                imageIcon = GameConfig.picPath() + "pic/menu/icons/mood_equal.png";
                                                                            } else {
                                                                                imageIcon = GameConfig.picPath() + "pic/menu/icons/mood_bad.png";
                                                                            }
                                                        %>

                                                        <IMG onmouseout="hideTip()" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_moralchange", userId)%>')"
                                                             style="height:14px;" style="width:17px; height:17px" src="<%= imageIcon%>"/>
                                                    </TD>
                                                </TR>
                                            </TABLE>
                                        </FORM>
                                </TD>
                            </TR>
                            <!--- NOTES -->
                            <% Note n = AdministrationService.findNote(userId, p.getId());
                             int maxLength = 30;
                             int length = maxLength;
                            if(n != null){
                             length = Math.min(n.getMessage().length(), maxLength);
                            }
                             %>
                            <TR>
                                <TD colspan="2">
                                    <TABLE  width="342px" cellpadding="2" border="0" cellspacing="0">
                                        <TR>

                                            <TD onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_editnote", userId)%>')" onmouseout="hideTip()" style="font-size:10px; font-weight:bold;  border-style: solid; border-width: thin;" align="left" width="15px">
                                                <a onClick="return popupNote(this, 'editor')" target="blank" href="noteEditor.jsp?planetId=<%= p.getId() %>"> <IMG border="0" style="height: 18px; font-size:15px; width:18px;" type="image" src="<%= basePicPath%>pic/edit.jpg" /></a>
                                            </TD>
                                            <% if (n != null && !n.equals("")) {%>
                                            <TD onmouseover="doTooltip(event,'<%= n.getMessage() %>')" onmouseout="hideTip()" style="font-size:10px; font-weight:bold;  border-style: solid; border-width: thin;">
                                                <%= n.getMessage().substring(0, length).replace("<BR>", " ") %><% if (n.getMessage().length() > maxLength) {%>.... <% }%>
                                            </TD>
                                            <% } else {%>
                                            <TD>
                                                &nbsp;
                                           </TD>
                                            <% }%>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                            <%
                                                // Prepare food values
                                                long foodProd = pr.getRessBaseProduction(at.viswars.model.Ressource.FOOD);
                                                long foodCons = pr.getRessConsumption(at.viswars.model.Ressource.FOOD);
                                                long foodStorage = pr.getRessStock(at.viswars.model.Ressource.FOOD);
                                                long foodStorageTotal = pr.getRessMaxStock(at.viswars.model.Ressource.FOOD);
                                                 foodIncoming = 0l;
                                                if(tradeBalances.get(pp.getPlanetId()) != null && tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD) != null
                                                        && tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD).get(Boolean.FALSE) != null){
                                                 foodIncoming = tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD).get(Boolean.FALSE);
                                                          }
                                                 foodOutgoing = 0l; 
                                                if(tradeBalances.get(pp.getPlanetId()) != null && tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD) != null
                                                        && tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD).get(Boolean.TRUE) != null){
                                                 foodOutgoing = tradeBalances.get(pp.getPlanetId()).get(Ressource.FOOD).get(Boolean.TRUE);
                                                          }
                                                 foodProd += foodIncoming;
                                                 foodCons += foodOutgoing;
                                                boolean showFoodWarning = false;

                                                /*
                                                if (((foodProd < foodCons) ||
                                                ((foodProd == foodCons) &&
                                                (pp.getPopulation() < epcr.getMaxPopulation()))) &&
                                                ((foodStorage / (foodCons - foodProd)) < 200)) {
                                                showFoodWarning = true;
                                                }
                                                 */

                                                int coverage = 1;

                                                if ((foodCons - foodProd) > 0) {
                                                    coverage = (int) Math.floor(foodStorage / (foodCons - foodProd));
                                                }

                                                if (((foodProd < foodCons)
                                                        || ((foodProd == foodCons) && (pp.getPopulation() < epcr.getMaxPopulation())))
                                                        && (coverage < 200)) {
                                                    showFoodWarning = true;
                                                }

                                                boolean showWarning = false;
                                                boolean seriousWarning = false;
                                                boolean info = false;
                                                boolean showCivilHint = false;

                                                if (epcr.getEffAgriculture() < 1
                                                        || epcr.getEffIndustry() < 1
                                                        || epcr.getEffResearch() < 1
                                                        || pp.getMoral() < 70
                                                        || (energyBalance < 0)) {
                                                    showWarning = true;
                                                    seriousWarning = true;
                                                }

                                                if (showFoodWarning) {
                                                    showWarning = true;
                                                    if (!(((foodProd == foodCons) || (foodProd - foodCons != 0) && (coverage > 50)))) {
                                                        seriousWarning = true;
                                                    }
                                                }

                                                showCivilHint = PlanetAdministrationService.showMigrationWarning(userId, pp.getPlanetId());
                                                if (showCivilHint) info = true;
                            %>
                            <TR>
                                <TD style="height:14px;" width="100%" colspan="2">
                                    <% if (showWarning || info) {%>
                                    <TABLE bgcolor="<%= (seriousWarning ? "red" : (showWarning ? "yellow" : "green")) %>" width="100%" style="font-size:10px; height:14px; font-weight:bold;  border-style: solid; border-width: thin;" cellpadding="0" cellspacing="0">
                                        <TR>
                                            <TD style="width:2px"></TD>
                                            <TD style="height:14px">

                                                <% if (epcr.getEffAgriculture() < 1 || epcr.getEffIndustry() < 1 || epcr.getEffResearch() < 1) {%>
                                                <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("warn_low_productivity", userId)%>')" onmouseout="hideTip()" src="<%= basePicPath%>pic/icons/efficienty.jpg"/>
                                                <% }%>
                                                <% if (pp.getMoral() < 70) {%>
                                                <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("warn_low_morale", userId)%>')" onmouseout="hideTip()" src="<%= imageIcon%>"/>
                                                <% }%>
                                                <% if (energyBalance < 0) {%>
                                                <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("warn_low_energy", userId)%>')" onmouseout="hideTip()" src="<%= basePicPath%>pic/icons/energy.jpg"/>
                                                <% }%>
                                                <% if (showFoodWarning) {%>
                                                <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("warn_low_food", userId)%>')" onmouseout="hideTip()" src="<%= basePicPath%>pic/icons/food.jpg"/>
                                                <% }%>
                                                <% if (showCivilHint) {%>
                                                <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("info_max_migration", userId)%>')" onmouseout="hideTip()" src="<%= basePicPath%>pic/icons/zivmigration.png"/>
                                                <% }%>
                                            </TD>
                                        </TR>
                                    </TABLE>
                                    <% }%>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
                <TR>
                    <TD colspan="1" align="right" valign="top">
                        <FORM method="post" action="main.jsp?page=new/new_administration-body-main2&fromItem=<%= (fromItem - 1)%>&toItem=<%= toItem%>&filterId=<%= filterId%>&process=<%= PROCESS_BUILDBUILDING%>">
                            <TABLE width="100%" cellpadding="0" cellspacing="0">
                                <TR>
                                    <TD colspan="1">
                                        <TABLE cellpadding="0" cellspacing="0">
                                            <TR>
                                                <TD align="left">
                                                    <SELECT style='background-color: #000000; color: #ffffff; border:0px; width: 100%; font-size: 11px;' NAME="cId" id="cId">
                                                        <%
                                                                            TreeMap<EConstructionType, ProductionBuildResult> pbrs = AdministrationService.getConstructionList(userId, pp.getPlanetId());
                                                                            for (Map.Entry<EConstructionType, ProductionBuildResult> entry : pbrs.entrySet()) {
                                                                                boolean buildableFound = false;
                                                                                ProductionBuildResult pbr = entry.getValue();
                                                                                ArrayList<Buildable> tmp = pbr.getUnits();
                                                                                for (Buildable b : tmp) {
                                                                                    Construction c = (Construction) b.getBase();
                                                                                    BuildableResult br = pbr.getBuildInfo(b);
                                                                                    //BuildableResult br = ConstructionUtilities.canBeBuilt(, userId, planetId, count)
                                                                                    if (br.isBuildable()) {
                                                                                        if (!buildableFound) {
                                                                                            buildableFound = true;
                                                                                        }
                                                                                        if (c.getId() == Construction.ID_BIGIRONMINE) {
                                                        %>
                                                        <OPTION selected VALUE="<%= c.getId()%>"><%= ML.getMLStr(c.getName(), userId)%> (<%= br.getPossibleCount()%>)</OPTION>
                                                        <% } else {%>
                                                        <OPTION VALUE="<%= c.getId()%>"><%= ML.getMLStr(c.getName(), userId)%> (<%= br.getPossibleCount()%>)</OPTION>
                                                        <% }
                                                                                                                                            }
                                                                                                                                        }%>
                                                        if(buildableFound){
                                                        %>
                                                        <OPTION disabled><------</OPTION>

                                                        <%


                                                                            }
                                                        %>
                                                    </SELECT>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD>
                                                    <TABLE width="100%">
                                                        <TR>
                                                            <TD colspan="1" style="background-color:#4A4A4A;" ALIGN="right">
                                                                <INPUT style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;" value="1" NAME="number" type="text" size="5">
                                                            </TD>
                                                            <TD colspan="1" style="background-color:#4A4A4A;" bgcolor="gray" align="center" >
                                                                <INPUT type="HIDDEN" NAME="planetId" value="<%= pp.getPlanetId() %>">
                                                                <INPUT style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;" type="SUBMIT"  size="25" value="<%= ML.getMLStr("administration_but_buildnow", userId)%>">
                                                            </TD>
                                                        </TR>
                                                    </TABLE>
                                                </TD>
                                            </TR>
                                        </TABLE>
                                    </TD>

                                </TR>
                            </TABLE>
                        </FORM>
                    </TD>
                    <TD colspan="4" align="right" valign="top">
                        <TABLE>
                            <%
                                                // LinkedList<InConstructionData> icds = at.viswars.utilities.ConstructionUtilities.getInConstructionData(pfres.getKey());
                                                LinkedList<InConstructionData> icds = at.viswars.utilities.ConstructionUtilities.getInConstructionData(pp.getPlanetId());
                                                if (icds.size() > 0) {
                            %>
                            <TR>
                                <TD align="right" valign="top">
                                    <TABLE style="font-size:10px; font-weight: bold;" width="100%" cellpadding="0" cellspacing="0">
                                        <TR>
                                            <TD align="right">
                                                <U><B><%= ML.getMLStr("administration_lbl_buildingsinconstruction", userId)%></B></U>
                                            </TD>
                                        </TR>
                                        <%
                                                                                            for (InConstructionData icd : icds) {%>
                                        <TR>
                                            <TD align="right">
                                                <%= icd.getConstructionData().getCount()%>
                                                x <%= ML.getMLStr(icd.getConstructionData().getConstruct().getName(), userId)%>
                                            </TD>
                                        </TR>
                                        <%
                                                                                            }
                                        %>
                                    </TABLE>
                                </TD>
                            </TR>
                            <%
                                                }%>

                        </TABLE>
                    </TD>
                </TR>

            </TABLE>
        </TD></TR>
        <%

                    }
        %>
</TABLE><%

%>

<!-- PageWeiterschaltor START -->
<TABLE border="0" style="font-size:11px; font-weight: bolder; color:white;" width="150px"  align="center" cellpadding="0" cellspacing="0">
    <TR class="blue2">
        <TD align="center" width="10px">
            <% if (fromItem > 1) {%>
            <A href="main.jsp?page=new/new_administration-body-main2&fromItem=0&toItem=15&filterId=<%= filterId%>">
                |<<
            </A>
            <% }%>
        </TD>
        <TD align="center"  width="10px">
            <% if (fromItem > 1) {%>
            <A href="main.jsp?page=new/new_administration-body-main2&fromItem=<%= ((fromItem - 1) - itemsPerPage)%>&toItem=<%= (fromItem - 1)%>&filterId=<%= filterId%>">
                <
            </A>
            <% }%>
        </TD>
        <TD  width="20px" align="center">
            <%= fromItem%> - <%= toItem%>
        </TD>
        <TD align="center"  width="10px">
            <% if (toItem < lastItem) {%>
            <A href="main.jsp?page=new/new_administration-body-main2&fromItem=<%= toItem%>&toItem=<%= (toItem + itemsPerPage)%>&filterId=<%= filterId%>">
                >
            </A>
            <% }%>
        </TD>
        <TD align="center"  width="10px">
            <% if (toItem < lastItem) {
                            int subtractor = pfr.getTotalItems() % itemsPerPage;
            %>
            <A href="main.jsp?page=new/new_administration-body-main2&fromItem=<%= (pfr.getTotalItems() - subtractor)%>&toItem=<%= pfr.getTotalItems()%>&filterId=<%= filterId%>">
                >>|
            </A>
            <%
                        }

            %>
        </TD>
    </TR>
</TABLE>
<!-- PageWeiterschaltor ENDE -->