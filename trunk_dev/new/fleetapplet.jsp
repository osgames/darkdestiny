<%@page import="at.viswars.GameConstants"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.*" %>
<%@page import="at.viswars.requestbuffer.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.result.*" %>
<%
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            FleetParameterBuffer fpb = new FleetParameterBuffer(userId);

            if (request.getParameter("toSystem") != null) {
                fpb.setParameter("toSystem", request.getParameter("toSystem"));
                fpb.setParameter("targetId", request.getParameter("targetId"));
            }
            if (request.getParameter("fleetId") != null) {

                fpb.setParameter("fleetId", request.getParameter("fleetId"));
            }

            synchronized (GameConstants.appletSync) {
                BaseResult br = FleetService.checkFlightRequest(userId, fpb);

                if (!br.isError()) {
                    br = FleetService.processFlightRequest(userId, fpb);
                }

                if (!br.isError()) { %>
                    OKAY
<%              } else { %>
                    <%= br.getMessage() %>
<%              }
            }
%>

