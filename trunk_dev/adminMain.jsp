<%@page import="at.viswars.Logger.Logger"%>
<%@page import="at.viswars.view.menu.LeftMenu"%>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.utilities.Menu" %>
<%@page import="at.viswars.view.html.table" %>
<%@page import="at.viswars.service.LoginService"%>
<%@page import="at.viswars.model.UserData"%>
<%@page import="at.viswars.model.User"%>
<%@page import="at.viswars.service.MainService"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.util.*"%>
<%@page import="at.viswars.DebugBuffer.DebugLevel"%>
<%@page import="at.viswars.DebugBuffer"%>
<%@page import="at.viswars.database.access.DbConnect" %>
<%@page buffer="100kb" autoFlush="true" %>

<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<!-- <META http-equiv="Content-Type" content="text/html; charset=UTF-8" /> -->
<TITLE>Dark Destiny - The ultimate online space game</TITLE>
<style type="text/css"></style>
<link rel="stylesheet" href="main.css.jsp" type="text/css">
<script src="java/main.js" type="text/javascript"></script>
<script src="java/format.js" type="text/javascript"></script>
  <script>

     var timeServer=<%= GameUtilities.getMillisNextTick() %>
     var adjustMillis = 0;
     var timeAtStart = 0;
     var tickDuration=<%= GameConfig.getTicktime() %>;

   function init() {
    var d = new Date();
    timeAtStart = d.getTime();

    countdown();
   }

      function countdown() {
        var d = new Date();
        var timegone = d.getTime() - timeAtStart;

        if (timeServer < timegone) { timeServer += tickDuration; }
        var tillTick = ((timeServer - timegone) % tickDuration)

        minuten = Math.floor(tillTick/60/1000);
        sekunden = Math.floor(tillTick/1000 - minuten*60);
        hundertstel = Math.floor(tillTick - (minuten*60*1000 + sekunden*1000));

        document.countdownform.countdowninput.value = minuten+":"+PadDigits(sekunden,2);

        setTimeout("countdown();",250);
      }

    function PadDigits(n, totalDigits)
    {
        nStr = n.toString();
        var pd = '';
        if (totalDigits > nStr.length)
        {
            for (i=0; i < (totalDigits-nStr.length); i++)
            {
                pd += '0';
            }
        }
        return pd + nStr;
    }
    </script>

<%
session = request.getSession();

String language = "";
if(request.getParameter("changeLanguage") != null){
    language = request.getParameter("changeLanguage");
    if(language.equals("en_US")){
        session.setAttribute("Language", Locale.US);
    }else if(language.equals("de_DE")){
        session.setAttribute("Language", Locale.GERMANY);
    }
}
int userId = Integer.parseInt((String)session.getAttribute("userId"));

Logger.getLogger().write("4");
Locale locale = request.getLocale();
if(session.getAttribute("Language") != null){
    locale = (Locale)session.getAttribute("Language");
}
String errReqPage = "";
            try {
                // @TODO nachher verschrotten
            GameInit.initGame();


            Date dat = new Date();
            long time1 = dat.getTime();

Logger.getLogger().write("5");
            if (request.getParameter("shipdesign") != null) {
%>
                <BODY onload="init()" topmargin="0" leftmargin="0" rightmargin="0" BGCOLOR="#000000" onload="UpdateShipData();">
<%          } else {
                if (request.getParameter("page") != null) {
                    errReqPage = request.getParameter("page");

                    if (request.getParameter("page").equalsIgnoreCase("new/alliance") || request.getParameter("page").equalsIgnoreCase("showAlliance")) {
%>
                        <BODY onload="init(); setPic();" topmargin="0" leftmargin="0" rightmargin="0" BGCOLOR="#000000">
<%
                    } else {
%>
                        <BODY onload="init()" topmargin="0" leftmargin="0" rightmargin="0" BGCOLOR="#000000">
<%
                    }
                } else {
%>
                    <BODY onload="init()" topmargin="0" leftmargin="0" rightmargin="0" BGCOLOR="#000000">
<%
                }
            }
Logger.getLogger().write("6");
            int subCategory =99;
            String reqPage = "admin/index.jsp";
            if (request.getParameter("subCategory") != null) {
                subCategory = Integer.parseInt((String) request.getParameter("subCategory"));

            }
            if (request.getParameter("page") != null) {
                reqPage = "admin/"+(String)(request.getParameter("page"))+".jsp";

            }



Logger.getLogger().write("7");
            // reqPage += ".jsp";
            // DebugBuffer.addLine("Reqpage is " + reqPage);
            if (session.getAttribute("userId") != null) {
Logger.getLogger().write("8");
                    // DebugBuffer.addLine("Updating User " + session.getAttribute("userId"));
                    CheckForUpdate.requestUpdate();
            } else {
%>
                <jsp:forward page="login.jsp" >
                    <jsp:param name="errmsg" value='<%= ML.getMLStr("main_err_invalidsession", userId)%>' />
                </jsp:forward>
<%
            }

Logger.getLogger().write("9");


Logger.getLogger().write("10");
String leftMenuString = "";
try{

            LeftMenu leftMenu = new LeftMenu(99, userId, true);

             leftMenuString = leftMenu.getLeftMenuTable().toString();
            }catch(Exception e){
                Logger.getLogger().write("R : " + e);
                e.printStackTrace();
                                          }
        %>
        <table border = '0'  cellspacing='0' cellpadding='0' width=100% height=100%>
<%


Logger.getLogger().write("leftMenuString: " + leftMenuString);
%>
            <tr>
                <td align='left' valign='top'  width=137px ><%= leftMenuString %></td>
                <td align='center' valign='top' >
                    <%

                     if (!reqPage.equals("")){
                        %><jsp:include page='<%= reqPage %>'/>
                    <% } %>
                </td>
                <%
                %>

            </tr>
<%

            Date dat2 = new Date();
            long time2 = dat2.getTime();
            float diff = (float)(time2 - time1) / (float)1000;
%>
            <TR><TD align="center" style="font-size:9px;" colspan=3><BR>Seite wurde in <%= diff %> Sekunden erstellt</TD></TR>
<%
            } catch (IllegalStateException ise) {
                // No special handling required mostly related to invalid session
                // will cause logout anyway, even it should not happen
            } catch (Exception e) {
                String parameters = "";

                for (Object me : request.getParameterMap().entrySet()) {
                    Map.Entry mapEntry = (Map.Entry)me;
                    parameters += (String)mapEntry.getKey() + "=[";

                    String[] mapValue = (String[])mapEntry.getValue();
                    boolean firstLoop = true;
                    for (String tmpStr : mapValue) {
                        if (!firstLoop) parameters += ",";
                        parameters += tmpStr;
                        firstLoop = false;
                    }

                    parameters += "] ";
                }

                DebugBuffer.writeStackTrace("Error in Main.jsp:<BR>Requested page: " + errReqPage + "<BR>Parameters: " + parameters + "<BR>", e);
            }
%>
        </table>
    </body>
</html>
