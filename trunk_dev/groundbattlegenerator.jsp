

<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.model.*" %>
<%@page import="at.viswars.result.*" %>
<%@page import="at.viswars.utilities.*" %>
<%@page import="at.viswars.FormatUtilities" %>
<%@page import="at.viswars.ML" %>
<%@page import="at.viswars.enumeration.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.Logger.*"%>
<%
String[] names = new String[5];
names[1] = "<font color=red><b>Anton</b></font>";
names[2] = "<font color=orange><b>Bertha</b></font>";
names[3] = "<font color=black><b>Cesar</b></font>";
names[4] = "<font color=white><b>D-D-Dora</b></font>";
            int nrOfPlayers = 4;
            int nrOfGroups = 4;
            int userId = Integer.parseInt((String) session.getAttribute("userId"));

            if(request.getParameter("process") != null){
                  GroundCombatResult gcr = GroundCombatUtilities.prepareBattleTest(request.getParameterMap());

TreeMap<Integer, User> users = gcr.getUsers();
%>
Battle on: Planet #<%= gcr.getPlanetId() %><BR>
StartTime: <%= gcr.getStartTime()%><BR>
Status: <%= gcr.getStatus().toString()%><BR>
Rounds <%= gcr.getRounds()%><BR>
<%
        ArrayList<CombatRound> rounds = gcr.getCombatRounds();
%>
<BR>
<h3>Short Report</h3><BR>
<TABLE width="80%">
    <TR>
        <%
                //dbg Logger.getLogger().write("1");
                //dbg Logger.getLogger().write("2");
                int user = 0;
                for (Map.Entry<Integer, ArrayList<RoundEntry>> startUnits : gcr.getStartUnits().entrySet()) {
                    user++;
                    //dbg Logger.getLogger().write("3");
        %>
        
    <% if ((user % 2) != 0) {%>
    <TR>
        <% }%>
        <TD valign="top">
            <TABLE width="100%" style="font-size: 13px">
                <TR>
                    <TD bgcolor="gray" colspan="4">
                        <%= users.get(startUnits.getKey()).getGameName() %>
                        <% if(gcr.getWinner() == startUnits.getKey()){ %>
                        <B><font color="orange">Gewinner</font></B>
                        <% } %>
                    </TD>
                </TR>
                <TR class="blue2">
                    <TD>
                        Typ
                    </TD>
                    <TD>
                        Anzahl
                    </TD>
                    <TD>
                        Überlebend
                    </TD>
                    <TD>
                        Verloren
                    </TD>
                </TR>
                <%
                TreeMap<Integer, Long> ress = new TreeMap<Integer, Long>();
                TreeMap<Integer, Long> ressLoss = new TreeMap<Integer, Long>();
                                        ArrayList<RoundEntry> entries = startUnits.getValue();
                                        for (RoundEntry startUnit : entries) {
                                            //dbg Logger.getLogger().write("4");
                                            //dbg Logger.getLogger().write("entry.getNumber() " + startUnit.getNumber());
                                            //dbg Logger.getLogger().write("entry.getDead() " + startUnit.getDead());
                %>
                <TR>
                    <TD>
                        <%= ML.getMLStr(Service.groundTroopDAO.findById(startUnit.getTroopId()).getName(), userId)%>
                    </TD>
                    <TD>
                        <%= (startUnit.getNumber() + startUnit.getDead())%>
                    </TD>
                    <%
                                            //dbg Logger.getLogger().write("5");

                                            
                                            long survived = 0;
                                            long diedInTotal = startUnit.getNumber() + startUnit.getDead();
                                            if(gcr.getEndUnits().get(startUnits.getKey()) != null){
                                            ArrayList<RoundEntry> endUnits = gcr.getEndUnits().get(startUnits.getKey());
                                            for (RoundEntry endUnit : endUnits) {
                                                  if (endUnit.getTroopId() == startUnit.getTroopId()) {
                                                    survived = endUnit.getNumber();
                                                    diedInTotal = startUnit.getNumber() + startUnit.getDead() - survived;
                                                    if(survived < 0){
                                                        diedInTotal += survived;
                                                        survived = 0;
                                                        }
                                                    break;
                                                }
                                            }
                                            }
                                            for(RessourceCost rc : Service.ressourceCostDAO.find(ERessourceCostType.GROUNDTROOPS, startUnit.getTroopId())){
                                                //dbg Logger.getLogger().write("Qut : " + (rc.getQty() * (startUnit.getNumber() + startUnit.getDead())));
                                                    if(ress.get(rc.getRessId()) == null){
                                                         ress.put(rc.getRessId(), rc.getQty() * (startUnit.getNumber() + startUnit.getDead()));
                                                    }else{
                                                         ress.put(rc.getRessId(), (ress.get(rc.getRessId())) + (rc.getQty() * (startUnit.getNumber() + startUnit.getDead())));
                                                        }
                                                    if(ressLoss.get(rc.getRessId()) == null){
                                                         ressLoss.put(rc.getRessId(), rc.getQty() * (diedInTotal));
                                                    }else{
                                                         ressLoss.put(rc.getRessId(), (ressLoss.get(rc.getRessId())) + (rc.getQty() * (diedInTotal)));
                                                        }
                                                }
                                            //dbg Logger.getLogger().write("6");
                    %>
                    <TD>
                        <%= survived%>
                    </TD>
                    <TD>
                        <%= diedInTotal%>
                    </TD>
                </TR>
                <% }
                %>
            </TABLE>
            <TABLE width="100%" style="font-size: 13px">
                <TR bgcolor="gray">
                    <TD colspan="2">
                        Wert der Armee vor Kampf
                    </TD>
                </TR>
                                <TR class="blue2">
                    <TD>
                        Ressource
                    </TD>
                    <TD>
                        Menge
                    </TD>
                </TR>
                <% 
                for(Map.Entry<Integer, Long> entry : ress.entrySet()){

                %>
                <TR>
                    <TD>
                        <%= ML.getMLStr(Service.ressourceDAO.findRessourceById(entry.getKey()).getName(), userId) %>
                    </TD>
                    <TD>
                        <%= FormatUtilities.getFormattedNumber(entry.getValue()) %>
                    </TD>
                </TR>
                <% } %>
            </TABLE>
            <% if(ressLoss.size() > 0) { %>
            <TABLE width="100%" style="font-size: 13px">
                <TR bgcolor="gray">
                    <TD colspan="2">
                        Wertverlust
                    </TD>
                </TR>
                                <TR class="blue2">
                    <TD>
                        Ressource
                    </TD>
                    <TD>
                        Menge
                    </TD>
                </TR>
                <%
                for(Map.Entry<Integer, Long> entry : ressLoss.entrySet()){

                %>
                <TR>
                    <TD>
                        <%= ML.getMLStr(Service.ressourceDAO.findRessourceById(entry.getKey()).getName(), userId) %>
                    </TD>
                    <TD>
                        <%= FormatUtilities.getFormattedNumber(entry.getValue()) %>
                    </TD>
                </TR>
                <% } %>
            </TABLE>

            <% } %>
        </TD>
        <% if (user % 2 == 0) {%>
    </TR>
    <% }%>
        <%
                }
        %>
    </TR>
</TABLE>
<h3>Report</h3><BR>
<TABLE width="80%">
    <%
            //dbg Logger.getLogger().write("1y");
            //dbg Logger.getLogger().write("2y " + gcr.getHistory().size());
            int round = 0;
            for (Map.Entry<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> rEntry : gcr.getHistory().entrySet()) {
                round++;
                //dbg Logger.getLogger().write("3y");
                TreeMap<Integer, TreeMap<Integer, RoundEntry>> re2 = rEntry.getValue();
                float multiplicator = (float)Math.ceil(((float)round / 10f));
    %>

    <TR class="blue2">
        <TD align="center" colspan="<%= (4 * re2.size())%>">
            Round <%= rEntry.getKey()%> Schadensmultiplikator = <%= Math.round(multiplicator) %>x
        </TD>
    </TR>
    <TR>
        <%
        user = 0;
                            for (Map.Entry<Integer, TreeMap<Integer, RoundEntry>> re : re2.entrySet()) {
                                user++;
        %>

    <% if ((user % 2) != 0) {%>
    <TR>
        <% }%>
        <TD valign="top">
            <TABLE width="100%" style="font-size: 13px;">

                <TR>
                    <% //dbg Logger.getLogger().write("Trying to get : " + rEntry.getKey() + " => " + re.getKey() + " "); %>
                    <%
                    double bonus = 0d;
                    try{
                        bonus = Math.round((gcr.getTerritoryUser().get(rEntry.getKey()).get(re.getKey()).getTerritorySize() - gcr.getTerritoryUser().get(rEntry.getKey()).get(re.getKey()).getChange())  * 100d);
                        }catch(Exception e){

                            }
                    %>
                    <TD bgcolor="gray" colspan="4">
                        <%= users.get(re.getKey()).getGameName() %>
                         (<%= bonus %>% Bonus)
                    </TD>
                </TR>
                <TR class="blue2">
                    <TD>
                        Typ
                    </TD>
                    <TD>
                        Anzahl
                    </TD>
                    <TD>
                        Überlebend
                    </TD>
                    <TD>
                        Verloren
                    </TD>
                </TR>
                <%
            //dbg Logger.getLogger().write("ay");
                                for (Map.Entry<Integer, RoundEntry> entry : re.getValue().entrySet()) {
                                    //dbg Logger.getLogger().write("4y");
                               long survived = entry.getValue().getNumber();
                                            long dead = entry.getValue().getDead();
                                                 //dbg Logger.getLogger().write("4y");
                                                    if(survived < 0){
                                                        dead += survived;
                                                        survived = 0;
                                                        }
                %>
                <TR>
                    <TD>
                        <%= ML.getMLStr(Service.groundTroopDAO.findById(entry.getValue().getTroopId()).getName(), userId)%>
                    </TD>
                    <TD>

                        <%= (entry.getValue().getNumber() + entry.getValue().getDead())%>
                    </TD>
                    <TD>
                        <%= survived %>
                    </TD>
                    <TD>

                        <%= dead %>
                    </TD>
                </TR>
                <% }
                                    //dbg Logger.getLogger().write("5z");
                                    //dbg Logger.getLogger().write("Wanna acces : " + rEntry + " => " + re);
                                    //dbg Logger.getLogger().write("Wanna acces : " + rEntry.getKey() + " => " + re);
                                    //dbg Logger.getLogger().write("Wanna acces : " + rEntry + " => " + re.getKey());
                                    double terr = 0d;
                                    try{
                                 terr = Math.round(gcr.getTerritoryUser().get(rEntry.getKey()).get(re.getKey()).getChange() * 100d);
                                 }catch(Exception e){
                                     }
                                    //dbg Logger.getLogger().write("6");
                %>
                            <TR class="blue2">
                                <% if(terr >= 0){ %>
                    <TD align="center" colspan="2">
                       Gewinn Territorium
                    </TD>
                    <% }else{ %>
                    <TD align="center" colspan="2">
                       Verlust Territorium
                    </TD>
                    <% } %>
                    <TD align="center" colspan="2">
                        <%= terr %> %
                    </TD>
                </TR>
            </TABLE>
        </TD>
        <% if (user % 2 == 0) {%>
    </TR>
    <% }%>
        <%
                                    //dbg Logger.getLogger().write("a");
                            }
        %>
    </TR>
    <TR>
        <TD>
            &nbsp;
        </TD>
    </TR>

    <%
                                    //dbg Logger.getLogger().write("b");
            }
            
    %>

</TABLE>
<h3>Long Report</h3><BR>
<%= gcr.getReport().toString() %>
<%
                }
%>
<FORM action="main.jsp?page=groundbattlegenerator&process=1" method="post">
<TABLE width="80%" class="blue2">
    <% for (int i = 1; i <= nrOfPlayers; i++) {%>
    <% if (i % 2 != 0) {%>
    <TR>
        <% }%>
        <TD align="center">
            <TABLE width="100%">
                <TR class="blue2">
                    <TD align="center" colspan="2">
                        <B><%= names[i] %></B>
                    </TD>
                </TR>
                <TR class="blue2">
                    <TD>
                        Gruppe
                    </TD>
                    <% if(i == 1){ %>
                    <TD>
                        <SELECT disabled name="group<%= i %>">
                            <% for(int j = 1; j <= nrOfGroups; j++){ %>
                            <% if(j == i) { %>
                            <OPTION selected value="<%= j %>"><%= j %></OPTION>
                            <% }else{ %>
                            <OPTION value="<%= j %>"><%= j %></OPTION>
                            <% } %>
                            <% } %>
                        </SELECT>
                    </TD>
                    <% }else{ %>

                    <TD>
                        <SELECT name="group<%= i %>">
                            <% for(int j = 1; j <= nrOfGroups; j++){ %>
                            <% if(j == i) { %>
                            <OPTION selected value="<%= j %>"><%= j %></OPTION>
                            <% }else{ %>
                            <OPTION value="<%= j %>"><%= j %></OPTION>
                            <% } %>
                            <% } %>
                        </SELECT>
                    </TD>
                    <% } %>
                </TR>
                <%
                TreeMap<Integer, GroundTroop> sorted = new TreeMap<Integer, GroundTroop>();
               ArrayList<GroundTroop> gts = Service.groundTroopDAO.findAll();
               for(GroundTroop gt : gts){
                    sorted.put(gt.getId(), gt);
                   }
               gts.clear();
               gts.addAll(sorted.values());
                %>
                            <% for(GroundTroop gt : gts){ %>
                <TR>
                    <% if(i != 1 && gt.getId() == 0){ %>
                    <TD>
                    </TD>
                    <TD style="height:24px">
                        
                    </TD>
                    <% }else{ %>
                    <TD>
                        <%= ML.getMLStr(gt.getName(), userId) %>
                    </TD>
                    <TD>
                        <INPUT name="uId<%= i %>troop<%= gt.getId() %>" type="textfield" style="width: 100%" value="0" />
                    </TD>

                <% } %>
                </TR>
                <% } %>
            </TABLE>
        </TD>
        <% if (i % 2 == 0) {%>
    </TR>
    <% }%>

    <% }%>
    <TR>
        <TD colspan="2" align="center">
            <INPUT type="submit" name="engage" value="ENGAGE!">
        </TD>
    </TR>
</TABLE>
</FORM>
