
<%@page import="java.text.Format"%>
<%@page import="java.util.Map"%>
<%@page import="at.viswars.FormatUtilities"%>
<%@page import="at.viswars.model.Ressource"%>
<%@page import="at.viswars.result.BalancingResult"%>
<%@page import="at.viswars.service.LoginService"%>
<%@page import="at.viswars.model.PlayerPlanet"%>
<%@page import="at.viswars.model.PlanetRessource"%>
<%@page import="java.util.HashMap"%>
<%@page import="at.viswars.GameConfig"%>
<%@page import="at.viswars.model.Galaxy"%>
<%@page import="at.viswars.result.SystemGenerationResult"%>
<%@page import="java.util.TreeMap"%>
<%@page import="at.viswars.setup.AbstractGalaxy"%>
<%@page import="at.viswars.model.Planet"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<link rel="stylesheet" href="main.css.jsp" type="text/css">
<!DOCTYPE html>
<%

    final int TYPE_DEFAULT = 0;
    final int TYPE_GENERATE_SYSTEMS = 1;

    int type = TYPE_DEFAULT;
    if (request.getParameter("type") != null) {
        type = Integer.parseInt(request.getParameter("type"));
    }



%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>System Generator Test</title>
    </head>
    <%
        switch (type) {
            case (TYPE_DEFAULT):
    %>
    <body style="background-color: black; color: white;">
        <FORM action="systemGeneratorTest.jsp?type=<%= TYPE_GENERATE_SYSTEMS%>" method="post" />
        <TABLE align="center">
            <TR>
                <TD align ="center">
                    Number of comparing Systems: 

                    <SELECT name="number">
                        <% for (int i = 1; i < 20; i++) {%>
                        <OPTION  <% if (i == 5) {%>SELECTED<%}%>  value="<%= i%>"><%= i%></OPTION>
                        <% }%>
                    </SELECT>
                </TD>
            </TR>
            <TR>
                <TD align="center">
                    <INPUT type="submit" value="Systeme Erzeugen" />
                </TD>
            </TR>
        </TABLE>
    </FORM>
</body>
<% break;

    case (TYPE_GENERATE_SYSTEMS):

%>
<body style="background-color: black; color: white;">
    <%

        int number = 0;
        if (request.getParameter("number") != null) {
            number = Integer.parseInt(request.getParameter("number"));
        }
        ArrayList<Planet> planetList = new ArrayList<Planet>();
        ArrayList<at.viswars.model.System> systemList = new ArrayList<at.viswars.model.System>();
        ArrayList<at.viswars.model.PlanetRessource> ressourceList = new ArrayList<at.viswars.model.PlanetRessource>();
        int actSystemId = 1;
        int actPlanetId = 1;
        int posx = 10;
        int posy = 10;
        double distanceToCenter = 500;
        int i = 0;
        while (i < number) {
            SystemGenerationResult sgr = AbstractGalaxy.generateSystem(posx, posy, distanceToCenter, actSystemId, actPlanetId, systemList, planetList, ressourceList, true);
            if (sgr.isSystemGenerated()) {
                i++;
                actSystemId += sgr.getSystemsGenerated();
                actPlanetId += sgr.getPlanetsGenerated();
            }

            posx += 10;
            posy += 10;
        }

        HashMap<Integer, ArrayList<PlanetRessource>> sortedRessList = new HashMap<Integer, ArrayList<PlanetRessource>>();
        HashMap<Integer, ArrayList<Planet>> sortedPlanetList = new HashMap<Integer, ArrayList<Planet>>();
        for (PlanetRessource pr : ressourceList) {
            ArrayList<PlanetRessource> ress = sortedRessList.get(pr.getPlanetId());
            if (ress == null) {
                ress = new ArrayList<PlanetRessource>();
            }
            ress.add(pr);
            sortedRessList.put(pr.getPlanetId(), ress);
        }
        for (Planet p : planetList) {
            ArrayList<Planet> planets = sortedPlanetList.get(p.getSystemId());
            if (planets == null) {
                planets = new ArrayList<Planet>();
            }
            planets.add(p);
            sortedPlanetList.put(p.getSystemId(), planets);
        }
    %>
    <TABLE align="center">
        <TR>
            <TD>
                Generated <%= systemList.size()%> Systems
            </TD>
        </TR>
        <TR>
            <TD>
                Generated <%= planetList.size()%> Planets
            </TD>
        </TR>
    </TABLE>
    <%

        //Sort planets
        //SystemId <=> OrbitLevel <=> Planet
        TreeMap<Integer, TreeMap<Integer, Planet>> sortedPlanetsByOrbit = new TreeMap<Integer, TreeMap<Integer, Planet>>();
        for (at.viswars.model.Planet planet : planetList) {
            TreeMap<Integer, Planet> planets = sortedPlanetsByOrbit.get(planet.getSystemId());
            if (planets == null) {
                planets = new TreeMap<Integer, Planet>();
            }
            planets.put(planet.getOrbitLevel(), planet);
            sortedPlanetsByOrbit.put(planet.getSystemId(), planets);
        }

    %>
    <FORM action="systemGeneratorTest.jsp?type=<%= TYPE_GENERATE_SYSTEMS%>" method="post" />
    <TABLE align="center" class="bluetrans">
        <TR valign="top">
            <% for (at.viswars.model.System system : systemList) {

                    long populationHabitable = 0;
                    long populationUnhabitable = 0;
                    HashMap<Integer, Long> ressourcesInSystem = new HashMap<Integer, Long>();
            %>
            <TD align ="center">
                <TABLE valign="top" class="bluetrans">
                    <TR valign="top">
                        <TD>
                            System : <%= system.getId()%>
                        </TD>
                    </TR>
                    <% for (int j = 0; j < Planet.MAX_ORBIT_LEVELS; j++) {%>

                    <TR valign="top">
                        <TD style="height:50px;width:50px">
                            <%
                                Planet p = sortedPlanetsByOrbit.get(system.getId()).get(j);
                                if (p != null) {

                                    if (p.getLandType().equals(Planet.LANDTYPE_M) || p.getLandType().equals(Planet.LANDTYPE_G) || p.getLandType().equals(Planet.LANDTYPE_C)) {
                                        populationHabitable += p.getMaxPopulation();
                                    }

                            %>
                            <IMG class="pic1" src="<%= GameConfig.picPath() + "pic/" + p.getPlanetPic()%>" />

                            <% } else {%>
                            &nbsp;
                            <% }%>
                        </TD>
                        <TD>
                            <% if (p != null) {%>
                            <TABLE style="font-size: 10px;">
                                <TR>
                                    <TD>Id</TD>
                                    <TD><%= p.getId()%></TD>
                                </TR>
                                <%
                                    if (p.getLandType().equals(Planet.LANDTYPE_M) || p.getLandType().equals(Planet.LANDTYPE_G) || p.getLandType().equals(Planet.LANDTYPE_C)) {%>
                                <TR>
                                    <TD>Max - Population</TD>
                                    <TD><%= FormatUtilities.getFormattedNumber(p.getMaxPopulation())%></TD>
                                </TR>
                                <% }%>
                                <%
                                    for (PlanetRessource pr : sortedRessList.get(p.getId())) {
                                        Long ress = ressourcesInSystem.get(pr.getRessId());
                                        if (ress != null) {
                                            ressourcesInSystem.put(pr.getRessId(), ress + pr.getQty());
                                        } else {
                                            ressourcesInSystem.put(pr.getRessId(), pr.getQty());
                                        }
                                        Ressource r = Service.ressourceDAO.findRessourceById(pr.getRessId());
                                %>
                                <TR>
                                    <TD>
                                        <IMG style="width:10px; height:10px;" src="<%= GameConfig.picPath() + r.getImageLocation()%>"  />
                                    </TD>
                                    <TD>
                                        <%= FormatUtilities.getFormattedNumber(pr.getQty())%>
                                    </TD>
                                </TR>
                                <% }%>

                            </TABLE>
                            <% }%>
                        </TD>
                    </TR>

                    <% }%>
                    <TR>
                        <TD colspan="2">Zusammenfassung:</TD>
                    </TR>
                    <TR>
                        <TD>Max-Bevölkerung</TD>
                        <TD><%= FormatUtilities.getFormattedNumber(populationHabitable)%></TD>
                    </TR>
                    <%    for (Map.Entry<Integer, Long> pr : ressourcesInSystem.entrySet()) {

                            Ressource r = Service.ressourceDAO.findRessourceById(pr.getKey());
                    %>
                    <TR>
                        <TD>
                            <IMG style="width:10px; height:10px;" src="<%= GameConfig.picPath() + r.getImageLocation()%>"  />
                        </TD>
                        <TD>
                            <%= FormatUtilities.getFormattedNumber(pr.getValue())%>
                        </TD>
                        <% }%>
                </TABLE>

            </TD>

            <% }%>
        </TR>

        <!-- Balanced Systems -->
        <TR valign="top">
            <% for (at.viswars.model.System system : systemList) {%>


            <%

                // Perform balancing of system
                ArrayList<Planet> planetsToBalance = sortedPlanetList.get(system.getId());
                HashMap<Integer, Planet> sortedPlanetsToBalance = new HashMap<Integer, Planet>();
                for (Planet pTmp : planetsToBalance) {
                    sortedPlanetsToBalance.put(pTmp.getId(), pTmp);
                }

                HashMap<Integer, ArrayList<PlanetRessource>> sortedRessourcesToBalance = new HashMap<Integer, ArrayList<PlanetRessource>>();
                for (Planet pTmp : planetsToBalance) {
                    sortedRessourcesToBalance.put(pTmp.getId(), sortedRessList.get(pTmp.getId()));

                }

                HashMap<Integer, PlayerPlanet> playerPlanets = new HashMap<Integer, PlayerPlanet>();


                //serach startPlanet
                Planet startPlanet = null;
                for (Planet p : planetsToBalance) {
                    if (p.getLandType().equals(Planet.LANDTYPE_M)) {
                        startPlanet = p;

                        p.setAvgTemp(15);
                        p.setDiameter(12500);
                        break;
                    }
                }
                BalancingResult br = new BalancingResult();
                br.setActionTaken(true);
                while (br.isActionTaken()) {
                    br = LoginService.balanceStartingSystem(br, startPlanet.getId(), sortedPlanetsToBalance, sortedRessourcesToBalance, playerPlanets, system.getId());
                    System.out.println("Balancing System : " + system.getId());
                    Thread.sleep(100);
                }
                
                //
                planetList.clear();
                planetList.addAll(sortedPlanetsToBalance.values());



                TreeMap<Integer, Planet> planetsByOrbit = new TreeMap<Integer, Planet>();
                for (Map.Entry<Integer, Planet> entry : sortedPlanetsToBalance.entrySet()) {
                    planetsByOrbit.put(entry.getValue().getOrbitLevel(), entry.getValue());
                }
                long populationHabitable = 0;
                long populationUnhabitable = 0;
                HashMap<Integer, Long> ressourcesInSystem = new HashMap<Integer, Long>();
            %>
            <TD align ="center">
                <TABLE valign="top" class="bluetrans">
                    <TR valign="top">
                        <TD>
                            System : <%= system.getId()%>
                        </TD>
                    </TR>
                    <% for (int j = 0; j < Planet.MAX_ORBIT_LEVELS; j++) {%>

                    <TR valign="top">
                        <TD style="height:50px;width:50px">
                            <%
                                Planet p = planetsByOrbit.get(j);
                                if (p != null) {
                                    if (p.getLandType().equals(Planet.LANDTYPE_M) || p.getLandType().equals(Planet.LANDTYPE_G) || p.getLandType().equals(Planet.LANDTYPE_C)) {
                                        populationHabitable += p.getMaxPopulation();
                                    }
                                    String picURLPlanet = null;
                                    picURLPlanet = "GetPlanetPic?mode=2&planetId=" + p.getId();
                                    for (PlanetRessource pr : sortedRessList.get(p.getId())) {
                                        Long ress = ressourcesInSystem.get(pr.getRessId());
                                        if (ress != null) {
                                            ressourcesInSystem.put(pr.getRessId(), ress + pr.getQty());
                                        } else {
                                            ressourcesInSystem.put(pr.getRessId(), pr.getQty());
                                        }
                                    }

                            %>
                            <IMG class="pic1" src="<%= GameConfig.picPath() + "pic/" + p.getPlanetPic()%>" />

                            <%

                            } else {%>
                            &nbsp;
                            <% }%>
                        </TD>
                        <TD>
                            <% if (p != null) {%>
                            <TABLE style="font-size: 10px;">
                                <TR>
                                    <TD>Id</TD>
                                    <TD><%= p.getId()%></TD>
                                </TR>
                                <%
                                    if (p.getLandType().equals(Planet.LANDTYPE_M) || p.getLandType().equals(Planet.LANDTYPE_G) || p.getLandType().equals(Planet.LANDTYPE_C)) {%>
                                <TR>
                                    <TD>Max - Population</TD>
                                    <TD><%= FormatUtilities.getFormattedNumber(p.getMaxPopulation())%></TD>
                                </TR>
                                <% }%>
                            </TABLE>
                            <% }%>
                        </TD>
                    </TR>

                    <% }%>
                    <TR>
                        <TD colspan="2">Zusammenfassung:</TD>
                    </TR>
                    <TR>
                        <TD>Max-Bevölkerung</TD>
                        <TD><%= FormatUtilities.getFormattedNumber(populationHabitable)%></TD>
                    </TR>
                    <%
                        for (Map.Entry<Integer, Long> pr : ressourcesInSystem.entrySet()) {

                            Ressource r = Service.ressourceDAO.findRessourceById(pr.getKey());
                    %>
                    <TR>
                        <TD>
                            <IMG style="width:10px; height:10px;" src="<%= GameConfig.picPath() + r.getImageLocation()%>"  />
                        </TD>
                        <TD>
                            <%= FormatUtilities.getFormattedNumber(pr.getValue())%>
                        </TD>
                        <% }%>
                        <% for (String s : br.getActionsTaken()) {
                        %>
                    <TR><TD colspan="2" style="font-size:10px;"><%= s%></TD></TR>
                    <% }%>
                </TABLE>

            </TD>

            <% }%>
        </TR>
    </TABLE>
</FORM>
</body>
<% break;
    }%>
</html>
