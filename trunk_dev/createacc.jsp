<%@page import="at.viswars.service.LoginService" %>
<%@page import="java.util.Locale"%>
<%@page import="at.viswars.model.Language"%>
<%@page import="at.viswars.enumeration.*"%>
<%@page import="at.viswars.*"%>
<script src="java/main.js" type="text/javascript"></script>
<link rel="stylesheet" href="main.css.jsp" type="text/css">
<HTML>
    <HEAD>
        <TITLE>Dark Destiny - Register Account</TITLE>
        <style type="text/css">
            <!--
            /*  ... Style-Sheet-Angaben ... */
            a:link { color:#C0C0C0; font-weight:bold }
            a:visited { color:#C0C0C0; font-weight:bold }
            a:active { color:#C0C0C0; font-weight:bold }
            a:hover { color:#ffffff; font-weight:bold; }
            a { text-decoration:none; }
            --></style>
    </HEAD>
    <body text="#FFFF00" BGCOLOR="#000000" background="<%=GameConfig.picPath()%>pic/sh.jpg">
        <script language="javascript" type="text/javascript">
            
            
        var appName = navigator.appName;
            function changeSubmitButton(checked)
            {
                document.getElementById("submit").disabled = !checked;
                updateButton();
            }
            function updateButton(){
                
            if (appName != 'Microsoft Internet Explorer') {
                if(document.getElementById("submit").disabled){
                    document.getElementById("submit").setAttribute("style", "color:gray;");
                }else{
                    document.getElementById("submit").setAttribute("style", "color:black;");
                }
            }
            }
        </script>
        <TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
            <TR>
                <TD width="33%" align="left"><IMG border=0 src="<%=GameConfig.picPath()%>pic/galaxy.gif"></TD>
                <TD width="33%" valign="top" align="center"><IMG border=0 src="<%=GameConfig.picPath()%>pic/darkdestiny.gif"></TD>
                <TD width="33%"></TD>
            </TR>
            <%
                Locale locale = request.getLocale();
                if (session.getAttribute("Language") != null) {
                    locale = (Locale) session.getAttribute("Language");

                }

                String UserName = request.getParameter("user");
                String Passwort1 = request.getParameter("pass1");
                String Passwort2 = request.getParameter("pass2");
                String GameName = request.getParameter("gamename");
                String Email = request.getParameter("email");
                String Neu = request.getParameter("neu");

                if (UserName == null) {
                    UserName = "";
                }
                if (Passwort1 == null) {
                    Passwort1 = "";
                }
                if (Passwort2 == null) {
                    Passwort2 = "";
                }
                if (GameName == null) {
                    GameName = "";
                }
                if (Email == null) {
                    Email = "";
                }
                if (Neu == null) {
                    Neu = "0";
                }

                int Neu2 = Integer.parseInt(Neu);
            %>
            <TR>
                <TD></TD>
                <TD width="*" align="center">
                    <%
                        if (Neu2 == 1) {
                            out.println("<B>" + ML.getMLStr("login_err_filloutallfields", locale) + "</B>");
                            out.println("<P>");
                        } else if (Neu2 == 2) {
                            out.println("<B>" + ML.getMLStr("login_err_passwordsdonotconcur", locale) + "</B>");
                            out.println("<P>");
                        }

                    %>
                    <!-- <FORM name="anmelden" method="post" action="http://www.thedarkdestiny.de/DarkDestiny/login.jsp"> -->
                    <FORM name="anmelden" method="post" action="./register.jsp">
                        <TABLE width="100%" style="font-size: 13px; color: yellow;">
                            <TR>
                                <TD><B><%= ML.getMLStr("login_lbl_username", locale)%>:</B></TD>
                                <TD>
                                    <%
                                        out.println("<INPUT NAME=\"user\" TYPE=\"text\" VALUE=\"" + UserName + "\" SIZE=\"15\" ALIGN=middle>");
                                    %>
                                </TD>
                            </TR>
                            <TR>
                                <TD><b><%= ML.getMLStr("login_lbl_password", locale)%>:</b></TD>
                                <TD>
                                    <%
                                        out.println("<INPUT NAME=\"pass1\" TYPE=\"password\" VALUE=\"" + Passwort1 + "\" SIZE=\"15\" ALIGN=middle>");
                                    %>
                                </TD>
                            </TR>
                            <TR>
                                <TD><b><%= ML.getMLStr("login_lbl_passwordconfirm", locale)%>:</b></TD>
                                <TD>
                                    <%
                                        out.println("<INPUT NAME=\"pass2\" TYPE=\"password\" VALUE=\"" + Passwort2 + "\" SIZE=\"15\" ALIGN=middle>");
                                    %>
                                </TD>
                            </TR>
                            <TR>
                                <TD><b><%= ML.getMLStr("login_lbl_gamename", locale)%>:</b></TD>
                                <TD>
                                    <%
                                        out.println("<INPUT NAME=\"gamename\" TYPE=\"text\" VALUE=\"" + GameName + "\" SIZE=\"15\" ALIGN=middle>");
                                    %>
                                </TD>
                            </TR>
                            <TR>
                                <TD><b><%= ML.getMLStr("login_lbl_email", locale)%>:</b></TD>
                                <TD>
                                    <%
                                        out.println("<INPUT NAME=\"email\" TYPE=\"text\" VALUE=\"" + Email + "\" SIZE=\"25\" ALIGN=middle>");
                                    %>
                                </TD>
                            </TR>
                            <TR>
                                <TD><b><%= ML.getMLStr("login_lbl_language", locale)%>:</b></TD>
                                <TD>
                                    <%
                                        out.println("<SELECT NAME=\"language\" ALIGN=middle>");

                                        for (Language l : LoginService.findAllLanguages()) {
                                            if (l.getId() != l.getMasterLanguageId()) {
                                                continue;
                                            }
                                            out.println("<OPTION VALUE=\"" + l.getId() + "\">" + ML.getMLStr(l.getName(), locale));
                                        }
                                        out.println("</SELECT>");

                                    %>
                                </TD>
                            </TR>
                            <TR>
                                <TD><b><%= ML.getMLStr("login_lbl_gender", locale)%>: <img width="20" height="20" onmouseout="hideTip()" onmouseover="doTooltip(event,'<%= ML.getMLStr("login_pop_genderinfo", locale)%>')"  alt="Gender" src="<%= GameConfig.picPath()%>pic/info.jpg" /></b></TD>
                                <TD>
                                    <%
                                        out.println("<SELECT NAME=\"gender\" ALIGN=middle>");

                                        for (EUserGender e : EUserGender.values()) {
                                            out.println("<OPTION VALUE=\"" + e.toString() + "\">" + ML.getMLStr("login_opt_" + e.toString(), locale));
                                        }
                                        out.println("</SELECT>");

                                    %>
                                </TD>
                            </TR>
                            <TR>
                                <TD>Ich habe die <a href="UsageAgreement.htm" target="new">Nutzungs- und Datenschutzbestimmungen</a> gelesen und bin mit ihnen einverstanden</TD>
                                <TD>
                                    <INPUT type="checkbox" id="checkbox" onClick="changeSubmitButton(document.getElementById('checkbox').checked);" />
                                </TD>
                            </TR>
                        </TABLE>
                        <P>
                        <TABLE width="50%">
                            <TR align="center">
                                <TD><INPUT disabled="true" id="submit" type=submit name="submit" value="Anmelden" onClick="return ActionDeterminator2();"></TD>
                            </TR>
                        </TABLE>
                    </FORM>
                </TD>
                <TD align="right"><img border=0 src="<%=GameConfig.picPath()%>pic/plan2.gif" width="141" height="280"></TD>
            </TR>
        </TABLE>
        <script language="javascript" type="text/javascript">
            
            
       updateButton();
        </script>
    </BODY>
</HTML>