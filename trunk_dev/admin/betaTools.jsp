<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="at.viswars.admin.*" %>
<%
 /*
  * Hier wird das Menu für den Admin erzeugt
  */
	session = request.getSession();
	int userId = Integer.parseInt((String)session.getAttribute("userId"));
	new AdminUser(userId);
	
%>
<html>
<head><title>Beta Tools</title>
<style type="text/css">
<!--
/*  ... Style-Sheet-Angaben ... */
a:link { color:#C0C0C0; font-weight:bold }
a:visited { color:#C0C0C0; font-weight:bold }
a:active { color:#C0C0C0; font-weight:bold }
a:hover { color:#ffffff; font-weight:bold; }
a { text-decoration:none; }
//--></style></head>
<body text="#FFFF00" BGCOLOR="#000000">
<CENTER>
<FONT size="+2">Beta-Tools MENU</FONT>
<BR>
<BR>
Allgemeine Anmerkungen, falls ein Kampf generiert werden soll. Folgende Bedinungen müssen erfüllt sein:<BR>
Die Flotten 4,5,6 müssen exisierten, wobei Flotte 4 die Angreiferflotte, Flotte 6 die Systemverteidigung und Flotte 5 die Orbitale Verteidungsflotte ist. 
Für Flotte 4 muss zusätzliche die Tabelle "fleetdetails" ausgefüllt werden! Planetenverteidigungsanlagen/Truppen werden auf Planet 7 generiert.<BR><BR>
<A HREF="main.jsp?page=genBattle">Generate Battle</A><BR>
<A HREF="main.jsp?page=viewResult">View Result</A>
</CENTER>
</body>
</html>
