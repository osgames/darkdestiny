<%@page import="at.viswars.model.Language"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.database.access.DbConnect"%>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.admin.*" %>
<%@page import="at.viswars.service.NewsService" %>
<%@page import="at.viswars.model.News" %>
<%@page import="at.viswars.utilities.NewsGenerator" %>
<%@page import="java.util.*" %>
<%
    /*
     * Hier wird das User-Menu f&uuml;r den Admin erzeugt
     */
    String archiviert;
    News news = new News();

    int process = 0;
    int newsType = 0;
    int newsId = 0;

    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    new AdminUser(userId);

    if (request.getParameter("newsType") != null) {
        newsType = Integer.parseInt(request.getParameter("newsType"));
    }
    int languageId = 1;
    if (request.getParameter("languageId") != null) {
        languageId = Integer.parseInt(request.getParameter("languageId"));
    }
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }
    if (request.getParameter("newsid") != null) {
        newsId = Integer.parseInt(request.getParameter("newsid"));
        news = NewsService.getNews(newsId);
    }
    if (process != 0) {
        if (process == 3) {
            NewsService.setToArchive(news);
        } else if (process == 4) {
            NewsService.setToArchive(news);
        }
        NewsService.process(request.getParameterMap(), process, news, userId);
        process = 0;
        newsType = 0;
    }

    ArrayList<News> allNews = NewsService.getAllNews(languageId);

    switch (newsType) {
        case 0:
%>


<%@page import="at.viswars.database.access.DbConnect"%>
<a href="main.jsp?subCategory=99&page=admin/news&newsType=1&languageId=<%= languageId%>">Neue Nachricht<br></a>

<br>
<a href="main.jsp?subCategory=99&page=admin/news&newsType=3">Generiere Newshtml<br><br></a>
<br>
<SELECT id="languageId" onChange="window.location.href = 'main.jsp?subCategory=99&page=admin/news&languageId='+document.getElementById('languageId').value">
    <% for (Language l : Service.languageDAO.findAll()) {%>
    <OPTION <% if (l.getId() == languageId) {%>SELECTED<% }%> value="<%= l.getId()%>"><%= l.getName()%></OPTION>
    <% }%>
</SELECT>


<table border="0" cellspacing="0" cellpadding="0" class="blue" width="100%">
    <tr class="blue2">
        <th>Datum</th>
        <th>Betreff</th>
        <th>Message</th>
        <th>Submitter</th>
        <th>L&ouml;schen</th>
        <th>Archiviert</th>
        <th>&Auml;ndern</th>
    </tr>

    <%



        for (int i = 0; i < allNews.size(); i++) {

            News ne = allNews.get(i);

            java.util.Date msgDate = new java.util.Date(ne.getDate());
            String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate);
            //String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate) + " " + java.text.DateFormat.getDateInstance().getTimeInstance().format(msgDate);

    %>
    <tr align="center">
        <td><%=msgDateString%></td>
        <td><%=ne.getSubject()%></td>
        <td><%=ne.getMessage()%></td>
        <td><%=NewsService.getUserName(ne.getSubmittedById())%></td>
        <td><a href="main.jsp?subCategory=99&page=admin/news&process=2&newsid=<%=ne.getNewsId()%>">l&ouml;schen</a></td>
        <% if (ne.getIsArchived()) {
                archiviert = "ja";
                process = 3;

            } else {
                archiviert = "nein";
                process = 4;

            }%>
        <td><a href="main.jsp?subCategory=99&page=admin/news&process=<%=process%>&newsid=<%=ne.getNewsId()%>"><%=archiviert%></a></td>
        <td><a href="main.jsp?subCategory=99&page=admin/news&newsType=2&newsid=<%=ne.getNewsId()%>">&auml;ndern</a></td>
    </tr>


    <%
        }
    %>
</table>

<%



%>
<%
        break;
    case 1:

%>

<FORM method="post" action="main.jsp?subCategory=99&page=admin/news&process=1">
    <TABLE width="80%">
        <TR><TD align="center" colspan=2>News hinzuf&uuml;gen<P><BR></TD></TR>
        <TR><TD>Sprache</TD>
            <TD>
                <SELECT id="languageId" name="langId">
                    <% for (Language l : Service.languageDAO.findAll()) {%>
                    <OPTION <% if (l.getId() == languageId) {%>SELECTED<% }%> value="<%= l.getId()%>"><%= l.getName()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
        </TR>
        <TR><TD>Betreff</TD><TD><input type="text" name="subject" size=20 maxlength="30"></TD></TR>
        <TR><TD>Beschreibung<BR>HTML erlaubt</TD><TD><textarea name="message" rows=6 cols=40></textarea></TD></TR>
        <TR><TD align="center" colspan=2><P><BR><input value="Erstellen" type=submit></TD></TR>

    </TABLE>
</FORM>
<%
        break;


    case 2:

        News NE = news;

%>

<FORM method="post" action="main.jsp?subCategory=99&page=admin/news&process=5&newsid=<%=newsId%>">
    <TABLE width="80%">
        <TR><TD  align="center" colspan=2>News &auml;ndern<P><BR></TD></TR>
        <TR><TD>Betreff</TD><TD><input type="text" value="<%=NE.getSubject()%>" name="subject" size=20 maxlength="30"></TD></TR>
        <TR><TD>Beschreibung<BR>HTML erlaubt</TD><TD><textarea name="message" rows=6 cols=40><%=NE.getMessage()%></textarea></TD></TR>
        <TR><TD align="center" colspan=2><P><BR><input value="Updaten" type=submit></TD></TR>
    </TABLE>
</FORM>
<%
        break;



    case 3:
        //Map<String, String[]> allPars = request.getParameterMap();

        NewsGenerator NG = new NewsGenerator(1);
        //NG.setParameterMap(allPars);
        String result = NG.generateAllNews();
        NewsGenerator NG1 = new NewsGenerator(0);
        //NG.setParameterMap(allPars);
        result = NG1.generateAllNews();%>
        
<font color="WHITE"></font> <font color="Green"><b><%=result%></b></font><br>
<a href="main.jsp?subCategory=99&page=admin/news&newsType=0">Weiter<br></a><%
                    break;
            }

%>