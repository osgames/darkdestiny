<%@page import="at.viswars.*" %>
<%@page import="at.viswars.admin.*" %>
<%@page import="at.viswars.service.ProfileService"%>
<%@page import="at.viswars.model.User"%>
<%@page import="at.viswars.model.UserData"%>
<%
/*
 * Hier wird das User-Menu f&uuml;r den Admin erzeugt
 */
	session = request.getSession();
	new AdminUser(Integer.parseInt((String)session.getAttribute("userId")));

	int userId = Integer.parseInt(request.getParameter("user"));
        UserData ud = ProfileService.findUserDataByUserId(userId);
        User u = ProfileService.findUserByUserId(userId);

	if (request.getParameter("type") != null) {
		int type = Integer.parseInt(request.getParameter("type"));
		switch (type)
		{
		case 1:{ //Change the Profile
			String EMail = request.getParameter("email");
			if (!EMail.equalsIgnoreCase(u.getEmail()))
				u.setEmail(EMail);
			
			String pic = request.getParameter("pic");
			if (!pic.equalsIgnoreCase(u.getPic()))
				u.setPic(pic);
			
			String desc = request.getParameter("desc");
			if (!desc.equalsIgnoreCase(u.getDescription()))
				u.setDescription(desc);
			}
		}
                ProfileService.updateUser(u);
		}
	
%>
<script language="Javascript">        
    function setPic() {
        var availSpace = (Weite-355) * 0.8;
        
        document.getElementById("bild").setAttribute("width", availSpace, 0);
    }
</script>


<TABLE width="80%"  align="center">
<TR bgcolor="#708090" CLASS=blue2 ><TD align="center">Profil von: <B><%= u.getUserName() %></B></TD></TR>

 <% if ((u.getPic() != null) &&(!u.getPic().equals(""))){%>
<TR><TD align="center"><IMG id="bild" src="<%= u.getPic() %>"></IMG></TD></TR>
            <% } %>
            

<%
if (u.getDeleteDate()>0) {
%>
<TR><TD align="center" ><FONT color="red"><b>Accountl&ouml;schung beantragt. Dauer bis zur L&ouml;schung: 
	<%= ProfileService.getTimeTilDeletion(userId) %> beantragt: <a href="main.jsp?page=delUsr&user=<%= userId %>">
	Jetzt durchf&uuml;hren</a></b></FONT></TD></TR>
<%
}
%>

<TR><TD colspan=2>
    <FORM method="post" action="adminMain.jsp?page=changeUser&type=1">
    <input type="hidden" name="user" value="<%= userId %>" />
        <TABLE width="80%" align="center">
            <TR><TD>Ingame Name:</TD><TD><%= u.getGameName()%></TD></TR>
            <TR><TD>Email</TD><TD><input type="text" name="email" size=20 maxlength="30" value="<%= u.getEmail()%>" ></TD></TR>
            <TR><TD>Bild (Link)</TD><TD><input type="text" name="pic" size=30 maxlength="100" value="<%= u.getPic()%>"></TD></TR>
            <TR><TD>Beschreibung<BR>HTML erlaubt</TD><TD><textarea name="desc" rows=6 cols=40><%= u.getDescription()%></textarea></TD></TR>
            <TR><TD align="center" colspan=2><P><BR><input value="Updaten" type=submit></TD></TR>
        </TABLE>
    </FORM>
</TD></TR>
</TABLE>
