<%@page import="at.viswars.exceptions.UpdaterException"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="at.viswars.admin.*" %>
<%@page import="at.viswars.*" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.Map" %>
<%@page import="java.lang.Throwable" %>

<%
 /*
  * Hier wird das Menu für den Admin erzeugt
  */
        /*
	session = request.getSession();
	int userId = Integer.parseInt((String)session.getAttribute("userId"));
	new AdminUser(userId);
	*/
	if (request.getParameter("restart") != null)
	{
                CheckForUpdate.getException().clear();
                CheckForUpdate.getThrowables().clear();
		CheckForUpdate.restartUpdateThreadManual();
	}
        
%>
<%@page import="java.io.PrintWriter"%>
<CENTER>
<FONT size="+2">Informationen über den Update-Thread</FONT>
</CENTER>
<BR>
<BR>
Der Update-Thread <%= CheckForUpdate.isUpdateDead()?"ist tot":"l&auml;uft<BR>" %>. 
<% if (CheckForUpdate.isUpdateDead())
	{
	%> <br><br>
	Soll der Update-Thread neu gestartet werden? <a href="main.jsp?<%=GameConstants.LINK_ADMIN%>/update&restart=1">Neu starten</a><BR>
<%	}
        
        if (!CheckForUpdate.getException().isEmpty() || !CheckForUpdate.getException().isEmpty()) { %>
	<br>Exception Liste:<br> 
        <TABLE class="blue" width="100%"><TR><TD width="250" class="blue2">Timestamp</TD><TD class="blue2">Exception</TD></TR><%
            HashMap<Long,Exception> errorsE = CheckForUpdate.getException();
            for (Map.Entry<Long,Exception> me : errorsE.entrySet()) {
                Exception e = null;

                if (me.getValue() instanceof UpdaterException) {
                    e = ((UpdaterException)me.getValue()).getUnderlying();
                } else {
                    e = me.getValue();
                }

		StringBuffer sb = new StringBuffer("UPDATE ERROR: "+e.getMessage()+" ("+e.getClass().getName()+")");
		for (StackTraceElement ste : e.getStackTrace()) {
                    sb.append("<br>"+ste.toString());
		}
		if (e.getCause() != null) sb.append(DebugBuffer.produceStackTrace(e.getCause()));                                
            %>                
                <TR><TD><%= new java.util.Date(me.getKey()).toString() %></TD><TD><%= sb.toString() %></TD></TR>
         <% } %>
        </TABLE>
	<br>Throwable List:<br> 
        <TABLE class="blue" width="100%"><TR><TD width="250" class="blue2">Timestamp</TD><TD class="blue2">Throwable</TD></TR><%
            HashMap<Long,Throwable> errorsT = CheckForUpdate.getThrowables();
            for (Map.Entry<Long,Throwable> me : errorsT.entrySet()) { 
                Throwable t = me.getValue();
		StringBuffer sb = new StringBuffer("UPDATE ERROR: "+t.getMessage()+" ("+t.getClass().getName()+")");
		for (StackTraceElement ste : t.getStackTrace()) {
                    sb.append("<br>"+ste.toString());
		}
		if (t.getCause() != null) sb.append(DebugBuffer.produceStackTrace(t.getCause()));                             
            %>                
                <TR><TD><%= new java.util.Date(me.getKey()).toString() %></TD><TD><%= sb.toString() %></TD></TR>
         <% } %>
        </TABLE> <%            
        }
        %>