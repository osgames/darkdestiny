<%@page import="java.util.*" %>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.admin.*" %>
<%@page import="java.util.GregorianCalendar" %>
<%@page import="at.viswars.model.User" %>
<%@page import="at.viswars.model.PlayerPlanet" %>
<%@page import="at.viswars.model.Planet" %>
<%@page import="at.viswars.model.UserData" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.service.ProfileService" %>
<%
            /*
             * Hier wird das User-Menu f&uuml;r den Admin erzeugt
             */
            session = request.getSession();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));

            if(session.getAttribute("admin") == null){
                //do nothing
                }else{
            User u = ProfileService.findUserByUserId(userId);
            UserData ud = ProfileService.findUserDataByUserId(userId);

            if (request.getParameter("change") != null && session.getAttribute("admin") != null) {
                session.setAttribute("userId", (String)request.getParameter("change"));
                PlayerPlanet pp = LoginService.findHomePlanet(Integer.parseInt((String) request.getParameter("change")));
                int systemId = LoginService.findPlanetById(pp.getPlanetId()).getSystemId();

                    session.setAttribute("actPlanet", "" + pp.getPlanetId());
                    session.setAttribute("actSystem", "" + systemId);
                %>
                <%
            } else {
                
                ArrayList<User> users = MainService.getAllUsers();
                GameUtilities gu = new GameUtilities();

%>
<table border="0" cellspacing="0" cellpadding="0" class="bluetrans" width="100%">
    <tr class="blue2">
        <th>Id - CLICK TO SWITCH</th>
        <th>Username</th>
    </tr>
    
    <%
           for (User uAct : users) {

               UserData udAct = ProfileService.findUserDataByUserId(uAct.getUserId());
               GregorianCalendar gc = new GregorianCalendar();
               if (udAct != null && udAct.getLeaveDays() > 0) {
                   gc.setTimeInMillis(udAct.getLeaveDate());
               }
               boolean aktiv = uAct.getLastUpdate() > gu.getCurrentTick() - 2;
               boolean inaktiv1 = uAct.getLastUpdate() < gu.getCurrentTick() - 720;
               boolean inaktiv2 = uAct.getLastUpdate() < gu.getCurrentTick() - 1440;
               boolean deleteExp = ProfileService.isDeletionExpired(uAct.getUserId());
               boolean delete = uAct.getDeleteDate() > 0;
    %>
    <tr>
        <td align="center"><a href="changeUser?change=<%= uAct.getUserId() %>"><%= uAct.getUserId() %></a></td>
        <td align="center"><%= uAct.getGameName() %></td></tr>
    <%
           }
    %>
</table>
<%
            }
            }
%>