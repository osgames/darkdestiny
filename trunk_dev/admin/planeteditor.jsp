
<%@page import="at.viswars.admin.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.viswars.*" %>

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="at.viswars.database.access.*" %>

<%
        session = request.getSession();
	int userId = Integer.parseInt((String)session.getAttribute("userId"));
	AdminUser adminUser = new AdminUser(userId);
	if (!adminUser.isAdmin()) {
		throw new Exception("You are not ADMIN!");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
    
    <h1> Planeteneditor - beta </h1>
    <br />
    <br />
        
        
 <%      
        // Auslesen der GET Werte aus der Session
        String input = (String)request.getParameter("editor_planetID");
        boolean error = false;
        boolean buffered = true;
        
        PlanetData craftysplanet = null;
        RessCalculation craftyress = null;
        
        
        
         int planetID = 0;
                        
        
 %>
        <form method="post" action="main.jsp">
        <input type="hidden" name="page" value="planeteditor">
	<table>
	<tr><td>PlanetenID:</td>
        <td>
            
            
         <input type="text" name="editor_planetID" value="<% 
         	if(input == null){ 
         		%>-enter-ID-<%
         	} else { 
         		%><%=input %><% 
        	} 
        	%>" />
        </td>
        <td> <input type="submit" name="submit" value="Go for it">
        </tr>
	</table>
	</form>
        
<%  
        if(input != null) {
            
           
            
            
            // check if the input is an valid Integer
            try {
                planetID = Integer.parseInt(input);
                }
            catch (NumberFormatException e) {
                error = true;
            %> <b><i> Illegal Argument - Integer.parseInt() failed! Only valid numbers </i></b> <%       
            }
                    
          
               
        }
      
        // end in case of errors
        if (error){
            return;
        }
        
        craftysplanet = new PlanetData(planetID);
        buffered = craftysplanet.isBuffered() ;
                    
                
        
         // end in case of no planet to display
        if (input == null) {
             return;
        }
        
       
               
  %>

            <form method="post" action="main.jsp">
            <input type="hidden" name="page" value="planeteditor" %>
                   <input type="hidden" name="editor_planetID" value="<%= planetID %>">
            <input type="hidden" name="valid_entry" value="1">
            <p class="red_centered_text"><i> Dont use this tool carelessly </i></p>
            <br >
            <table border="0" width="90%">
                <tr>                
                    <td width=30% valign="top">
                       <table border="1" width="100%">
                          <caption><i>static planet details</i> </caption>
                             <tr>
                                 <td width="60%">planetID </td>
                                 <td width="40%"><%= craftysplanet.getPlanetId() %> </td>
                             </tr>
                             <tr><td></td></tr>
                             <tr>
                                <td width="60%">systemID </td> 
                                <td width="40%"> <input type="text" size="10" name="editor_systemid" value="<%= craftysplanet.getSystemId() %>"></td>
                             </tr> 
                             <tr><td></td></tr>
                             <tr>
                                 <td width="60%"> planettyp </td>
                                 <td width="40%"> <input type="text" size="10" name="editor_typeland" value="<%= craftysplanet.getPlanetClass() %>"></td>
                             </tr>
                             <tr><td></td></tr>
                             <tr>
                                 <td width="60%"> atmosphere </td>
                                 <td width="40%"> <input type="text" size="10" name="editor_typeatmos" value="<%= craftysplanet.getAtmosphereClass()%>"></td>
                             </tr>
                              <tr><td></td></tr>
                             <tr>
                             <td width="60%"> temperature </td>
                             <td width="40%"> <input type="text" size="10" name="editor_averagetemp" value="<%= craftysplanet.getAverageTemp()%>"></td>
                             </tr>
                             <tr><td></td></tr>
                              <tr>
                                  <td width="60%"> orbitlevel </td>
                                  <td> <input type="text" size="10" name="editor_diameter" value="<%= craftysplanet.getOrbitLevel()%>"></td>
                              </tr>
                              <tr><td></td></tr>
                                 <tr>
                                     <td width=60%> diameter </td>
                                     <td width="40%"> <input type="text" size="10" name="editor_diameter" value="<%= craftysplanet.getDiameter()%>"></td>
                                 </tr>
                              <tr><td></td></tr>
                              <tr>
                                  <td width=60%> max population </td>
                                  <td width="40%"><input type="text" size="10" name="editor_population" value="<%= craftysplanet.getPopulation()%>"></td>
                              </tr>
                           </table>
                    </td>
                    <td width="5%"></td>
                    <td width="30%" valign="top">
                            <table border="1" width="100%">
                                <caption><i>variable planet ressources</i></caption>
                                <tr>
                                     <td width=60%> iron </td>
                                     <td width="40%"><input type="text" size="10" name="editor_iron" value="<%= craftysplanet.getIronRess()%>"></td>
                                </tr>
                                <tr><td></td></tr>
                                <tr>
                                    <td width="60%"> ynkelonium </td>
                                    <td width="40%"><input type="text" size="10" name="editor_ynkel" value="<%= craftysplanet.getYnkeloniumRess()%>"></td>
                                </tr>
                                <tr><td></td></tr>
                                <tr>
                                    <td width="60%"> howalgonium </td>
                                    <td width="40%"><input type="text" size="10" name="editor_howa" value="<%= craftysplanet.getHowalgoniumRess()%>"></td>
                                </tr>                       
                            </table>
                    </td>
                    <td width="5%"></td>
                    <td width="30%" valign="top">
                          <table border="1" width="100%">
                                <caption><i>unused fields in "planet"</i></caption>
                                <tr>
                                     <td width=60%> cvEmbinium res </td>
                                     <td width="40%"><input type="text" size="10" name="editor_unused_cvemb_ress" value="disabled" disabled></td>
                                </tr>
                                <tr><td></td></tr>
                                <tr>
                                    <td width="60%"> planet energy </td>
                                    <td width="40%"><input type="text" size="10" name="editor_unused_energy" value="disabled" disabled></td>
                                </tr>
                                <tr><td></td></tr>
                                <tr>
                                    <td width="60%"> planetID </td>
                                    <td width="40%"><input type="text" size="10" name="editor_unused_planetid" value="disabled" disabled></td>
                                </tr>
                                <tr><td></td></tr>
                                <tr>
                                    <td width="60%"> growth </td>
                                    <td width="40%"><input type="text" size="10" name="editor_unused_growth" value="disabled" disabled></td>
                                </tr> 
                                <tr><td></td></tr>
                                <tr>
                                    <td width="60%"> research </td>
                                    <td width="40%"><input type="text" size="10" name="editor_unused_research" value="disabled" disabled></td>
                                </tr>
                                <tr><td></td></tr>
                                <tr>
                                    <td width="60%"> production </td>
                                    <td width="40%"><input type="text" size="10" name="editor_unused_production" value="disabled" disabled></td>
                                </tr>
                                
                                
                            </table>
                    </td>
               </tr>
            </table>
            <br />
            <br />

         <% if(!buffered){ %>
 
           
         <p class="red_centered_test"> This planet does not belong to anyone </p>

         <%   return; }
         
         
         // now lets try to get the Ressources from RessCalculation
         // this is kinda a tough operation because the RessCalculation class is freaky
         
         craftyress = new RessCalculation(craftysplanet);
         craftyress.loadCurrentRess();
         craftyress.calcProduction();
         
         long ironprod[] = craftyress.getProdCons(GameConstants.RES_IRON);
         long steelprod[] = craftyress.getProdCons(GameConstants.RES_STEEL);
         long terkonitprod[] = craftyress.getProdCons(GameConstants.RES_TERKONIT);
         long ynkelprod[] = craftyress.getProdCons(GameConstants.RES_YNKELONIUM);
         long howaprod[] = craftyress.getProdCons(GameConstants.RES_HOWALGONIUM);
         long cvembprod [] = craftyress.getProdCons(GameConstants.RES_CVEMBINIUM);
         
         // switch to the max values of the ressources
         // should be available with craftyress.getIron() etc ...
         craftyress.switchOutputRess(GameConstants.OUTPUT_PLANET_STOCKLIMITMAX);
         
                 
         
          // get Username for the specific UserID - needed for Output in Tables
        Statement stmt = DbConnect.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery("SELECT username FROM user WHERE id="+craftysplanet.getUserId());
        rs.next();
        String temp_playername = rs.getString(1);
        
        
        // get the name of the system - not buffered - has to be saved direct after editing
        rs = stmt.executeQuery("SELECT name FROM system WHERE id="+craftysplanet.getSystemId());
        rs.next();
        String temp_systemname = rs.getString(1);
        
        
        // get the name of the planet - not buffered - has to be saved direct after editing
        rs = stmt.executeQuery("SELECT name FROM playerplanet WHERE planetID="+craftysplanet.getPlanetId());
        rs.next();
        String temp_planetname = rs.getString(1);
        
        stmt.close();
        %>
       <table border="0" width="90%">
            <tr>                
                <td width=30% valign="top">
                    <table border="1" width="100%">
                        <caption><i>planet owner</i></caption>
                        <tr>
                            <td width="60%"> playerID </td>
                            <td width="40%"> <input type="text" size="10" name="editor_playerid" value="<%= craftysplanet.getUserId() %>"> </td>
                       </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> playername </td>
                            <td width="40%"><%= temp_playername %> </td>
                       </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> planetname </td>
                            <td width="40%"> <input type="text" size="10" name="editor_update_manual_planetname" value="<%= temp_planetname %>" /></td>
                       </tr>
                        <tr><td></td></tr>
                         <tr>
                            <td width="60%"> systemname </td>
                            <td width="40%"> <input type="text" size="10" name="editor_update_manual_systemname" value="<%= temp_systemname %>"/> </td>
                       </tr>
                        <tr><td></td></tr>
                         <tr>
                            <td width="60%"> population </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_population" value="<%= craftysplanet.getPopulation() %>" /></td>
                       </tr>
                        <tr><td></td></tr>
                         <tr>
                            <td width="60%"> invisible flag </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_population" value="<%= craftysplanet.getInvisibleFlag() %>" /></td>
                        </tr>
                        <tr><td></td></tr>
                        
                        
                        
                    </table> 
                </td>
                <td width="5%"></td>
                <td width="30%" valign="top">
                    <table border="1" width="100%">
                        <caption><i>player ressources</i></caption>
                        <tr>
                            <td width="60%"> iron </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_iron" value="<%= craftysplanet.getIron() %>"> </td>
                        </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> steel </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_steel" value="<%= craftysplanet.getSteel()%>"></td>
                        </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> terkonit </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_terkonit" value="<%= craftysplanet.getTerkonit()%>"></td>
                        </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> ynkelonium </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_ynkel" value="<%= craftysplanet.getYnkelonium()%>"></td>
                        </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> howalgonium </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_howa" value="<%= craftysplanet.getHowalgonium()%>"></td>
                        </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> cv embinium </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_cvemb" value="<%= craftysplanet.getCvEmbinium()%>"></td>
                        </tr>
                        <tr><td></td></tr>
                        
                    </table>
                </td>
                <td width="5%"></td>
                <td width="30%" valign="top">
                   <table border="1" width="100%">
                       <caption><i>calculated values</i></caption> 
                       <tr>
                            <td width="60%"> moral </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_moral" value="<%= craftysplanet.getMoral() %>"> </td>
                        </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> population growth </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_growth" value="<%= craftysplanet.getPopulationGrowth() %>"> </td>
                        </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> tax level </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_tax" value="<%= craftysplanet.getTax() %>"> </td>
                        </tr>
                        <tr><td></td></tr>
                         <tr>
                            <td width="60%"> migration level </td>
                            <td width="40%"> <%= craftysplanet.getPopulationMigration() %> </td>
                        </tr>
                        <tr><td></td></tr>
                   </table>
               </td>
           </tr>
        </table>

        <br />
        <br />
        
           <table border="0" width="90%">
            <tr>                
                <td width=30% valign="top">
                    <table border="1" width="100%">
                        <caption><i>player storage values</i></caption>
                        <tr>
                            <td width="60%"> min iron </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_iron_storage" value="<%= craftysplanet.getMinIronStorage() %>"> </td>
                       </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> min steel </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_steel_storage" value="<%= craftysplanet.getMinSteelStorage() %>"> </td>
                       </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> min terkonit </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_terkonit_storage" value="<%= craftysplanet.getMinTerkonitStorage() %>"> </td>
                       </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> min ynkelonium </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_ynkel_storage" value="<%= craftysplanet.getMinYnkeloniumStorage() %>"> </td>
                       </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> min howalgonium </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_howa_storage" value="<%= craftysplanet.getMinHowalgoniumStorage() %>"> </td>
                       </tr>
                        <tr><td></td></tr>
                        <tr>
                            <td width="60%"> min cv embinium </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_cvemb_storage" value="<%= craftysplanet.getMinCVEmbiniumStorage()%>"> </td>
                       </tr>
                        <tr><td></td></tr>
                        
                                               
                        
                    </table> 
                </td>
                <td width="5%"></td>
               
                <td width=65% valign="top">
                    <table border="1" width="100%" class="centered_text">
                    <caption><i>player production information</i></caption>
                       <tr>
                           <td width="20%"> ressource </td>
                           <td width="20%"> production </td>
                           <td width="20%"> consumption </td>
                           <td width="20%"> currentvalue </td>
                           <td width="20%"> maxvalue </td>
                       </tr>
                       <tr>
                           <td> iron </td>
                           <td> <%= ironprod[0] %> </td>
                           <td> <%= ironprod[1] %> </td>
                           <td> <%= craftysplanet.getIron() %> </td>
                           <td> <%= craftyress.getIron() %> </td>

                       </tr>
                       <tr>
                           <td> steel </td>
                           <td> <%= steelprod[0] %> </td>
                           <td> <%= steelprod[1] %> </td>
                           <td> <%= craftysplanet.getSteel() %> </td>
                           <td> <%= craftyress.getSteel() %> </td>
                       </tr>
                       <tr>
                           <td> terkonit </td>
                           <td> <%= terkonitprod[0] %> </td>
                           <td> <%= terkonitprod[1] %> </td>
                           <td> <%= craftysplanet.getTerkonit() %> </td>
                           <td> <%= craftyress.getTerkonit() %> </td>
                       </tr>
                       <tr>
                           <td> ynkelonium </td>
                           <td> <%= ynkelprod[0] %> </td>
                           <td> <%= ynkelprod[1] %> </td>
                           <td> <%= craftysplanet.getYnkelonium() %> </td>
                           <td> <%= craftyress.getYnkelonium() %> </td>
                       </tr>
                       <tr>
                           <td> howalgonium </td>
                           <td> <%= howaprod[0] %> </td>
                           <td> <%= howaprod[1] %> </td>
                           <td> <%= craftysplanet.getHowalgonium() %> </td>
                           <td> <%= craftyress.getHowalgonium() %> </td>
                       </tr>
                       <tr>
                           <td> CV embinium </td>
                           <td> <%= cvembprod[0] %> </td>
                           <td> <%= cvembprod[1] %> </td>
                           <td> <%= craftysplanet.getCvEmbinium() %> </td>
                           <td> <%= craftyress.getCVEmbinium() %> </td>
                       </tr>

                    </table> 
                </td>
           </tr>
        </table>
        
         <br />
        <br />
        
   <table border="0" width="90%">
    <tr>    
         <td width="30%" valign="top">
                    <table border="1" width="100%">
                        <caption><i>player priorities</i></caption>
                         <tr>
                            <td width="60%"> industry </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_industry_priority" value="<%= craftysplanet.getPriorityIndustry()%>"> </td>
                       </tr>
                        <tr><td></td></tr>
                           <tr>
                            <td width="60%"> agriculture </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_agriculture_priority" value="<%= craftysplanet.getPriorityAgriculture()%>"> </td>
                       </tr>
                        <tr><td></td></tr>
                           <tr>
                            <td width="60%"> research </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_industry_priority" value="<%= craftysplanet.getPriorityResearch()%>"> </td>
                       </tr>
                        <tr><td></td></tr>
                           <tr>
                            <td width="60%"> energy - unused </td>
                            <td width="40%"> <input type="text" size="10" name="editor_player_industry_priority" value="<%= craftysplanet.getPriorityEnergy()%>" disabled> </td>
                       </tr>
                        <tr><td></td></tr>
                       
                        
                    </table>
                </td>
         <td width="5%"></td>
       
        <td width="30%">
            
        </td>
         <td width="5%"></td>
        <td width="30%" valign="top">
            <table border="1" width="100%">



            </table>
        </td>
   </tr>
</table>


                  
                
                    
        </form>
   
        <br />
        <br />
        
        <div class="blue_text">
        notes and warnings <br />
        
        <ul>   
            
            <%
            if ( craftysplanet.getPopulationGrowth() < 0) {
            %> <li class="red_text"> population growth is currently negativ </li> <%
            }
            
            if ( craftysplanet.getMoral() < 80) {
             %> <li class="red_text"> moral on this planet is very low </li> <%
            }
            
            if ( craftysplanet.getInvisibleFlag() != 0) {
            %> <li class="red_text"> this planet is invisible in the system overview - invisible flag is set </li> <%
            }
            
            
             if ( craftysplanet.getTax() > 40) {
             %> <li class="red_text"> the taxes on this planet are very high </li> <%
            }
            
            // end of red warning  section
            
            if ( ironprod[0] <= ironprod[1]) {
            %> <li class="orange_text"> this planet produces no iron (maybe is the iron consumption too high)  </li> <%
            } 
            
              if ( steelprod[0] <= steelprod[1]) {
            %> <li class="orange_text"> this planet produces no steel (maybe is the steel consumption too high)  </li> <%
            }  
              
            if ( craftysplanet.getMinIronStorage() > craftysplanet.getIron()) {
            %> <li class="orange_text"> the minimum iron storage value is higher than the actual iron ressources on the planet </li> <%
            }                     
            
            if ( craftysplanet.getMinSteelStorage() > craftysplanet.getSteel()) {
            %> <li class="orange_text"> the minimum steel storage value is higher than the actual steel ressources on the planet </li> <%
            }
            
            if ( craftysplanet.getMinTerkonitStorage() > craftysplanet.getTerkonit()) {
            %> <li class="orange_text"> the minimum terkonit storage value is higher than the actual terkonit ressources on the planet </li> <%
            }
            
            if ( craftysplanet.getMinYnkeloniumStorage() > craftysplanet.getYnkelonium()) {
            %> <li class="orange_text"> the minimum ynkelonium storage value is higher than the actual ynkelonium ressources on the planet </li> <%
            }
            
            if ( craftysplanet.getMinHowalgoniumStorage() > craftysplanet.getHowalgonium()) {
            %> <li class="orange_text"> the minimum howalgonium storage value is higher than the actual howalgonium ressources on the planet </li> <%
            }
        
                 
            // end of orange warning section
                 if ( craftysplanet.getPopulationMigration() < 0) {
             %> <li> The people are leaving this planet due to migration to other planets </li> <%
            }     
                                  
             if ( terkonitprod[0] <= terkonitprod[1]) {
            %> <li> this planet produces no terkonit </li> <%
            } 
            
             if ( ynkelprod[0] <= ynkelprod[1]) {
            %> <li> this planet produces no ynkelonium </li> <%
            } 
            
             if ( howaprod[0] <= howaprod[1]) {
            %> <li> this planet produces no howalgonium </li> <%
            } 
            
              if ( craftysplanet.getIron() == craftyress.getIron()) {
            %> <li> the storage space of iron on this planet is full or there is no storage room for iron  </li> <%
            } 
            
               if ( craftysplanet.getSteel() == craftyress.getSteel()) {
            %> <li> the storage space of steel on this planet is full or there is no storage room for steel  </li> <%
            } 
            
               if ( craftysplanet.getTerkonit() == craftyress.getTerkonit()) {
            %> <li> the storage space of terkonit on this planet is full or there is no storage room for terkonit </li> <%
            } 
            
               if ( craftysplanet.getYnkelonium() == craftyress.getYnkelonium()) {
            %> <li> the storage space of ynkelonium on this planet is full or there is no storage room for ynkelonium  </li> <%
            } 
            
               if ( craftysplanet.getHowalgonium() == craftyress.getHowalgonium()) {
            %> <li> the storage space of howalgonium on this planet is full or there is no storage room for howalgonium  </li> <%
            } 
            
                
            if ( craftysplanet.getMinCVEmbiniumStorage() > 0) {
            %> <li> there is a CVembinium storage value set - ressource is not implemented yet </li> <%
            }
            %> 
        </ul>
    </div>
    </body>
</html>
