<%-- 
    Document   : runCombat
    Created on : Jul 7, 2010, 2:57:37 PM
    Author     : Dreloc
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.viswars.spacecombat.*"%>
<%@page import="at.viswars.databuffer.fleet.*"%>
<%@page import="java.util.*"%>
<%@page import="at.viswars.model.PlayerFleet"%>
<%@page import="at.viswars.dao.*"%>
<%@page import="at.viswars.GameConfig"%>

<%@page import="at.viswars.service.Service"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Combat Testing</title>
    </head>
    <body text="#FFFFFF" bgcolor="BLACK" >

<%
    AbsoluteCoordinate ac = new AbsoluteCoordinate(100,100);
    RelativeCoordinate rc = null;
    RelativeCoordinate rc_ = new RelativeCoordinate(954, 6085);
    RelativeCoordinate rc_2 = new RelativeCoordinate(954, 6079);
    ArrayList<PlayerFleet> fleets = new ArrayList<PlayerFleet>();
    ArrayList<PlayerFleet> fleets_ = new ArrayList<PlayerFleet>();
    ArrayList<PlayerFleet> fleets_2 = new ArrayList<PlayerFleet>();

    PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    PlayerFleet pf1 = pfDAO.findById(162);
    PlayerFleet pf2 = pfDAO.findById(247);
    PlayerFleet pf3 = pfDAO.findById(309);
    PlayerFleet pf1_ = pfDAO.findById(162);
    PlayerFleet pf2_ = pfDAO.findById(247);
    PlayerFleet pf1_2 = pfDAO.findById(162);
    PlayerFleet pf2_2 = pfDAO.findById(247);

    fleets.add(pf1);
    fleets.add(pf2);
    fleets.add(pf3);
    fleets_.add(pf1_);
    fleets_.add(pf2_);
    fleets_2.add(pf1_2);
    fleets_2.add(pf2_2);

    CombatReportGenerator crg = new CombatReportGenerator();
    CombatHandler_NEW chNew = new CombatHandler_NEW(CombatReportGenerator.CombatType.SYSTEM_COMBAT,fleets,rc_,ac,crg);
    CombatHandler_NEW chNew_ = new CombatHandler_NEW(CombatReportGenerator.CombatType.ORBITAL_COMBAT,fleets_,rc_,ac,crg);
    CombatHandler_NEW chNew_2 = new CombatHandler_NEW(CombatReportGenerator.CombatType.ORBITAL_COMBAT,fleets_2,rc_2,ac,crg);
    crg.generateReports();

    crg.writeReportsToDatabase();

%>
        <h1>Combat run!</h1>
        <% for(Map.Entry<Integer, String> report : crg.getUserReports().entrySet()){ %>
        ==========================================================================<BR>
        Bericht f�r :  <%= Service.userDAO.findById(report.getKey()).getGameName() %><BR>
        <%= report.getValue() %><BR>

        <% } %>

    </body>
</html>
