<%-- 
    Document   : testGetModules
    Created on : 07.12.2009, 15:12:54
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.viswars.ships.*"%>
<%@page import="at.viswars.dao.*"%>
<%@page import="java.util.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Hello Modules!</h2><BR><BR>
<%
        ChassisDAO cDAO = (ChassisDAO)DAOFactory.get(ChassisDAO.class);
        ArrayList<ShipModule> modules = ShipBuilder.getModulesForChassis(cDAO.findById(4), 157, 1l);                
        
        for (ShipModule sm : modules) {
            out.write("MODUL: " + sm.getName() + " E_CONS: " + ((Weapon)sm).getEnergyConsumption() + "<BR>");
        }
%>
    </body>
</html>
