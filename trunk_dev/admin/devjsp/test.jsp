<%@page import="at.viswars.utilities.LoginUtilities"
%><%@page import="at.viswars.img.charts.GroundCombatChart"
%><%@page import="at.viswars.img.charts.ChartDrawer"
%><%@page import="at.viswars.img.BaseRenderer"
%><%@page import="at.viswars.PlanetData"
%><%@page import="at.viswars.img.BackgroundPainter"
%><%@page import="at.viswars.img.planet.*"
%><%@page import="at.viswars.*"
%><%@page import="at.viswars.img.IModifyImageFunction"
%><%@page import="java.util.List"
%><%@page import="java.util.LinkedList"
%><%@ page contentType="image/png" %><%

LoginUtilities.checkLogin(request, response);

List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();
// Check for DATA_FOR_SCAN_PIC

int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int mode = 1;
int picSize = 200;

if (request.getParameter("planetId") != null) {
    planetId = Integer.parseInt(request.getParameter("planetId"));
}

PlanetData pd = new PlanetData(planetId);

if (request.getParameter("mode") != null) {
    mode = Integer.parseInt(request.getParameter("mode"));
    if (mode == PlanetRenderer.LARGE_PIC) {
        picSize = 200;
    } else if (mode == PlanetRenderer.SMALL_PIC) {
        picSize = 40;
    }           
}

allItems.add(new PlanetRenderer(pd,this.getServletContext().getRealPath(GameConfig.picPath()+"pic/") + "/",mode));
BaseRenderer renderer = new BaseRenderer(picSize,picSize);
// Schreibt den Mime-Type in den Log, Nur Benutzen, wenn 
// genau das gew&uuml;nscht wird
// renderer.getImageMimeType(); 

renderer.modifyImage(new BackgroundPainter(0x000000));

for (IModifyImageFunction mif : allItems) {
	renderer.modifyImage(mif);
}

renderer.sendToUser(response.getOutputStream());
%>