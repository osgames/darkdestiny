<%@page import="at.viswars.admin.*"%><%
session = request.getSession();
if (session.getAttribute("userId") == null) {
%>
    <jsp:forward page="../login.jsp" >
            <jsp:param name="errmsg" value='Session abgelaufen' />
    </jsp:forward>    
<%
}
	TechTreeAdmin tta = new TechTreeAdmin();
	AdminUser adminUser  = null;
	if (!TechTreeAdmin.DEBUG)
	{
		session = request.getSession();
		int userId = Integer.parseInt((String)session.getAttribute("userId"));
		adminUser = new AdminUser(userId);
		tta.checkUser(adminUser);
	}
	else TechTreeAdmin.BASEPATH = "techtree.jsp?";
	
	if (request.getParameter("reload") != null)
	{
		TechTreeAdmin.forceReload();
		TechTreeDisplayAdmin.recreate();
	}
	TechTreeDisplayAdmin.doCalculation();
	
%><%@ page contentType="image/svg+xml" %><?xml version="1.0" encoding="UTF-8"?>
 <!DOCTYPE svg>
<svg xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:ev="http://www.w3.org/2001/xml-events"
      version="1.1" baseProfile="full"
      <%= TechTreeDisplayAdmin.getSize() %>>
      <%= TechTreeDisplayAdmin.showTree() %>
 </svg>