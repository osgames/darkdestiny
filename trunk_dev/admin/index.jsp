<%@page import="at.viswars.*" %>
<%@page import="at.viswars.database.framework.QueryLogging" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.Threading.*" %>
<%@page import="at.viswars.database.access.*" %>
<CENTER>
<%
        
        
        
  Runtime r = Runtime.getRuntime();
  
  // Set user to admin state
  request.getSession();
    
  long oldFreeMem = r.freeMemory();
  
  r.gc();
  
  long freeMem = r.freeMemory();
  long currMem = r.totalMemory();
  long maxMem  = r.maxMemory();

  int users = 0;
  int totalUsers = 0;
  int inactiveUsers = 0;
  int inactiveUsers5 = 0;

  if(request.getParameter("update") != null){
      String shutdownmsg = request.getParameter("shutdownmessage");
      GameUtilities.setShutDownMsg(shutdownmsg);

      }
     
        users = GameUtilities.getUsersOnlineSince(2);
        totalUsers = GameUtilities.getTotalUsers();
        inactiveUsers5 = GameUtilities.getUsersOfflineSince(720);
        inactiveUsers = GameUtilities.getUsersOfflineSince(1440);
            
%>
<FORM action="main.jsp?subCategory=99&page=admin/index&update=1" method="post">
<TABLE>
    <TR>
        <TD colspan="2">Shutdown-Message</TD>
    </TR>
    <TR>
        <TD>
            <INPUT type="text" name="shutdownmessage" value="<%= GameUtilities.getShutDownMsg() %>"/>
        </TD>
        <TD>
            <INPUT type="submit" value="update">
        </TD>
    </TR>
</TABLE>
</FORM>
<TABLE width="60%" class="blue">
<TR class="blue2"><TD align="center" colspan="2"><B>Server Information</B></TD></TR>
<TR><TD align="center">Registered</TD><TD align="center"><%= totalUsers %></TD></TR>
<TR><TD align="center">Users Online (last 20 min)</TD><TD align="center"><%= users %></TD></TR>
<TR><TD align="center">Inactive Users (no login 5 days)</TD><TD align="center"><%= inactiveUsers5 %></TD></TR>
<TR><TD align="center">Inactive Users (no login 10 days)</TD><TD align="center"><%= inactiveUsers %></TD></TR>
<TR><TD align="center">Total buffered planets</TD><TD align="center"><%= Service.playerPlanetDAO.findAll().size() %></TD></TR>
<TR><TD align="center">Max Memory</TD><TD align="center"><%= FormatUtilities.getFormatScaledNumber(maxMem,99) %></TD></TR>
<TR><TD align="center">Assigned Memory</TD><TD align="center"><%= FormatUtilities.getFormatScaledNumber(currMem,99) %></TD></TR>
<TR><TD align="center">Used Memory</TD><TD align="center"><%= FormatUtilities.getFormatScaledNumber(currMem-freeMem,99) %></TD></TR>
<TR><TD align="center">Free'ed memory by GC</TD><TD align="center"><%= FormatUtilities.getFormatScaledNumber(freeMem - oldFreeMem,99) %></TD></TR>
</TABLE>
<BR><BR>
<TABLE width="60%" class="blue">
<TR class="blue2"><TD align="center" colspan="2"><B>Data Access Layer Information (FIND)</B></TD></TR>
<TR><TD align="center">Querymode INDEXED used</TD><TD align="center"><%= FormatUtilities.getFormattedNumber(QueryLogging.indexedQueries) %></TD></TR>
<TR><TD align="center">Querymode INDEXED_MERGED used</TD><TD align="center"><%= FormatUtilities.getFormattedNumber(QueryLogging.mergedQueries) %></TD></TR>
<TR><TD align="center">Querymode INDEXED_PARTIAL used</TD><TD align="center"><%= FormatUtilities.getFormattedNumber(QueryLogging.partialQueries) %></TD></TR>
<TR><TD align="center">Querymode DIRECT used</TD><TD align="center"><%= FormatUtilities.getFormattedNumber(QueryLogging.directQueries) %></TD></TR>
</TABLE>
<BR><BR>
<TABLE width="60%" class="blue">
<TR class="blue2"><TD align="center" colspan="4"><B>Thread Watch</B></TD></TR>
<TR class="blue2"><TD align="center">ThreadId</TD><TD align="center">ThreadName</TD><TD align="center">Timing</TD><TD align="center">CPUTime</TD></TR>
<%
ArrayList<ThreadEntry> tList = ThreadUtilities.getAllThreads();
for (ThreadEntry te : tList) {
%>
<TR><TD align="center"><%= te.getThreadId() %></TD>
<TD align="center"><%= te.getThreadName() %></TD>
<TD align="center"><%= te.isTimingEnabled() %></TD>
<TD align="center"><%= FormatUtilities.getFormattedDecimalCutEndZero(te.getCpuTime() / 1000000000d,2) %> sek</TD></TR>
<%
}
%>
</TABLE>
</CENTER>