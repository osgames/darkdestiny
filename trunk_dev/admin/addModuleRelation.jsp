<%-- 
    Document   : addModuleRelation
    Created on : 14.11.2009, 19:28:33
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="at.viswars.admin.module.*"%>
<%@page import="java.util.*"%>

<%
int moduleId = Integer.parseInt(request.getParameter("id"));
String errorMsg = null;

AdminModule am = null; 
ModuleAttributeRel mar = null;

String mode = "Add";
String buttonMode = "ADD";
boolean editMode = false;

if (request.getParameter("type") != null) {
    if (request.getParameter("type").equalsIgnoreCase("add")) {
        am = ModuleConfiguration.getModule(moduleId);
        try {
            ModuleConfiguration.addModuleAttributeRelation(request.getParameterMap());
            response.sendRedirect("modifyModule.jsp?id="+am.getId());
        } catch (InvalidDataException ide) {
            errorMsg = ide.getMessage();
        }
    } else if (request.getParameter("type").equalsIgnoreCase("edit")) {
        mar = ModuleConfiguration.getModuleAttribute(moduleId);
        am = ModuleConfiguration.getModule(mar.getModuleId());        
        
        try {
            ModuleConfiguration.changeModuleAttributeRelation(request.getParameterMap());
            response.sendRedirect("modifyModule.jsp?id="+am.getId());
        } catch (InvalidDataException ide) {
            errorMsg = ide.getMessage();
        }        
    } else if (request.getParameter("type").equalsIgnoreCase("modify")) {                
        mar = ModuleConfiguration.getModuleAttribute(moduleId);
        am = ModuleConfiguration.getModule(mar.getModuleId());
        mode = "Edit";
        buttonMode = "SAVE";
        editMode = true;
    }
} else {
    am = ModuleConfiguration.getModule(moduleId);
}

ArrayList<AdminChassis> chassis = ModuleConfiguration.getAllChassis();
ArrayList<AdminAttribute> attributes = ModuleConfiguration.getAllAttributes();
ArrayList<AdminResearch> researches = ModuleConfiguration.getAllResearch();

AdminChassis acAll = new AdminChassis();
acAll.setId(0);
acAll.setName("ALL");

chassis.add(0, acAll);

ArrayList<String> referenceTypes = new ArrayList<String>();
for (ReferenceType rt : ReferenceType.values()) {
    referenceTypes.add(rt.name());
}

ArrayList<String> attributeTypes = new ArrayList<String>();
for (AttributeType at : AttributeType.values()) {
    attributeTypes.add(at.name());
}
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title><%= mode %> Module Attribute Relation for Module <%= am.getName() %> [<%= am.getId() %>]</title>
    </head>
    <body>
        <h2><%= mode %> Module Attribute Relation for Module <%= am.getName() %> [<%= am.getId() %>]</h2><BR><BR>
<%
    if (errorMsg != null) {
%>
        <FONT color="red"><B><%= errorMsg%></B></FONT><BR><BR>
<%
    }
%>      
        <FORM METHOD="post" ACTION="addModuleRelation.jsp?type=<%= editMode ? "edit" : "add" %>&id=<%= editMode ? mar.getId() : am.getId() %>">
        <TABLE>
            <TR>
                <TD>Chassis</TD>
                <TD>
                    <SELECT NAME="chassis">
<%
                    for (AdminChassis ac : chassis) {
                        if (editMode && (ac.getId() == mar.getChassis().getId())) {
%>
                            <OPTION SELECTED VALUE="<%= ac.getId() %>"><%= ac.getName() %></OPTION>
<%
                        } else {
%>
                            <OPTION VALUE="<%= ac.getId() %>"><%= ac.getName() %></OPTION>
<%
                        }
                    }
%>
                    </SELECT>                      
                </TD>
            </TR>
            <TR>
                <TD>Research</TD>
                <TD>
                    <SELECT NAME="research">
                        <OPTION SELECTED VALUE="0">No requirement</OPTION>
<%
                    for (AdminResearch ar : researches) {
                        if (editMode && (ar.getId() == mar.getResearch().getId())) {
%>                            
                            <OPTION SELECTED VALUE="<%= ar.getId() %>"><%= ar.getName() %></OPTION>
<%
                        } else {
%>              
                            <OPTION VALUE="<%= ar.getId() %>"><%= ar.getName() %></OPTION>
<%
                        }
                    }
%>
                    </SELECT>                      
                </TD>
            </TR>
            <TR>
                <TD>Attribute</TD>
                <TD>
                    <SELECT name="attribute">
<%
                    for (AdminAttribute aa : attributes) {
                        if (editMode && (aa.getId() == mar.getAttributeId())) {                        
%>
                            <OPTION SELECTED VALUE="<%= aa.getId() %>"><%= aa.getName() %></OPTION>
<%
                        } else {
%>
                            <OPTION VALUE="<%= aa.getId() %>"><%= aa.getName() %></OPTION>
<%
                        }
                    }
%>
                    </SELECT>                    
                </TD>
            </TR>            
            <TR>
                <TD>Attribute Type</TD>
                <TD>
                    <SELECT name="attributeType">
                        <OPTION SELECTED ID="-">-</OPTION>
<%
                    for (String option : attributeTypes) {
                        if (editMode && (mar.getAttributeType() != null) && option.equalsIgnoreCase(mar.getAttributeType().toString())) {
%>
                            <OPTION SELECTED ID="<%= option %>"><%= option %></OPTION>
<%
                        } else {
%>
                            <OPTION ID="<%= option %>"><%= option %></OPTION>
<%
                        }
                    }
%>
                    </SELECT>
                </TD>
            </TR>              
            <TR>
                <TD>Value</TD>
<%
            if (editMode) {
%>
                <TD><INPUT NAME="value" ID="value" SIZE="10" TYPE="test" VALUE="<%= mar.getValue() %>" /></TD>
<%
            } else {
%>
                <TD><INPUT NAME="value" ID="value" SIZE="10" TYPE="test" VALUE="0.0" /></TD>
<%
            }
%>
            </TR>        
            <TR>
                <TD>Reference Id</TD>
<%
            if (editMode) {
%>                
                <TD><INPUT NAME="refId" ID="refId" SIZE="5" TYPE="test" VALUE="<%= mar.getRefId() %>" /></TD>
<%
            } else {
%>
                <TD><INPUT NAME="refId" ID="refId" SIZE="5" TYPE="test" VALUE="0" /></TD>
<%
            }
%>                
            </TR>              
            <TR>
                <TD>Reference Type</TD>
                <TD>
                    <SELECT name="refType">
                        <OPTION SELECTED ID="-">-</OPTION>
<%
                    for (String option : referenceTypes) {
                        if (editMode && (mar.getRefType() != null) && option.equalsIgnoreCase(mar.getRefType().toString())) {
%>
                            <OPTION SELECTED ID="<%= option %>"><%= option %></OPTION>
<%
                        } else {
%>
                            <OPTION ID="<%= option %>"><%= option %></OPTION>
<%
                        }
                    }
%>
                    </SELECT>
                </TD>
            </TR>    
            <TR>
                <TD>Applies To</TD>
<%
            if (editMode) {
%>                
                <TD><INPUT NAME="appliesTo" ID="appliesTo" SIZE="5" VALUE="<%= mar.getAppliesTo() %>" /></TD>
<%
            } else {
%>
                <TD><INPUT NAME="appliesTo" ID="appliesTo" SIZE="5" VALUE="0" /></TD>
<%
            }
%>                
            </TR>                     
        </TABLE>
        <BR>
        <INPUT type="SUBMIT" ID="confirm" NAME="confirm" VALUE="<%= buttonMode %>" />
        </FORM>
        <BR><A HREF="modifyModule.jsp?id=<%= am.getId()%>">Zur�ck</A>
    </body>
</html>