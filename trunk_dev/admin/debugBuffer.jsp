<%@page import="at.viswars.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.DebugBuffer.DebugLevel" %>
<%@page import="at.viswars.DebugBuffer.DebugLine" %>
<%
 /*
  * Hier wird das Menu f&uuml;r den Admin erzeugt
  */
%>
<HTML>
<HEAD>
<TITLE>Dark Destiny - DEBUG</TITLE>
<style type="text/css">
<!--
/*  ... Style-Sheet-Angaben ... */
a:link { color:#C0C0C0; font-weight:bold }
a:visited { color:#C0C0C0; font-weight:bold }
a:active { color:#C0C0C0; font-weight:bold }
a:hover { color:#ffffff; font-weight:bold; }
a { text-decoration:none; }
-->
</style>
</HEAD>
<body text="#FFFF00" BGCOLOR="#000000">
<%
int process = (request.getParameter("process") != null)?Integer.parseInt(request.getParameter("process")):0;
if(process == 1){
        DebugBuffer.clear();
    }
String colors [] = new String [] 
                               	{
		"white",
		"cyan",
		"red",
		"red",
		"white",
                "yellow",
                "green"
                               	};

    ArrayList<DebugLine> debugs = DebugBuffer.readBuffer();
    HashMap<Integer, Integer> typeCount = new HashMap<Integer, Integer>();
    for (DebugBuffer.DebugLine entry : debugs) {
        Integer count = typeCount.get(entry.getLevel().getIntValue());
        if(count == null){
            count = 0;
            }
        count++;
        typeCount.put(entry.getLevel().getIntValue(), count);

    }

TreeMap<Integer, String> lvls = new TreeMap<Integer, String>();
lvls.put(-1, "All");
lvls.put(0, "Trace");
lvls.put(1, "Debug");
lvls.put(2, "Error");
lvls.put(3, "Fatal Error");
lvls.put(4, "Unknown");
lvls.put(5, "Warning");
lvls.put(6, "GroundCombat");
int level = (request.getParameter("level") != null)?Integer.parseInt(request.getParameter("level")):-1;

    %>
    <a href="debugBuffer.jsp?process=1">Clear</a>
    <BR>
    <a href="debugBuffer.jsp">Refresh</a>
    <FORM name="lvlform" method="post">
    <SELECT name="lvlselect" onChange="window.location=document.lvlform.lvlselect.options[document.lvlform.lvlselect.selectedIndex].value">
    <%
for(Map.Entry<Integer, String> entry : lvls.entrySet()){
    Integer count = typeCount.get(entry.getKey());
    if(count == null){
            count = 0;
        }
    %>
    <OPTION <% if(level == entry.getKey()){%> selected <%}%> value="debugBuffer.jsp?level=<%= entry.getKey()%>"><%= entry.getValue()%> (<%= count %>) </OPTION>
    <%
    }
    %>
    </SELECT>
    </FORM>
    <%

for (DebugBuffer.DebugLine entry : debugs) {
	if (level == entry.getLevel().getIntValue() || level == -1)
	{
            if(level == -1 && entry.getLevel().getIntValue() == 6){continue;}
	%>
    <font color="<%= colors[entry.getLevel().getIntValue()] %>">
    	<%= "[" + entry.getTimeStamp() + "] " + entry.getMessage() %>
    	</font><BR>
	<%
	}
}

%>
</BODY>
</HTML>
