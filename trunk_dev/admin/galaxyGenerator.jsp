
<%@page import="at.viswars.GameConfig"%>
<%@page import="at.viswars.model.Galaxy"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.enumeration.EPlanetDensity"%>
<%@page import="at.viswars.enumeration.EGalaxyType"%>
<%@page import="at.viswars.result.BaseResult" %>
<%@page import="at.viswars.service.SetupService" %>
<%@page import="java.util.*" %>
<%
            final int TYPE_STEP0 = 0;
            final int TYPE_STEP1 = 1;
            final int TYPE_STEP2 = 2;
            final int TYPE_STEP3 = 3;
            final int TYPE_STEP8 = 8;
            final int TYPE_STEP9 = 9;
            final int PROCESS_CHANGESTART = 99;
            int type = TYPE_STEP0;
            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }
            int process = 0;
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            ArrayList<BaseResult> results = new ArrayList<BaseResult>();
            switch(process){
                case(PROCESS_CHANGESTART):
                    SetupService.changePlayerStart(Integer.parseInt(request.getParameter("galaxyId")));
                    break;
            }
            
            switch (type) {
                case (TYPE_STEP8):
                    //Generate Database
                    Service.galaxyDAO.deleteGalaxy(Integer.parseInt(request.getParameter("galaxyId")));
                    type = TYPE_STEP0;
                    break;
                case (TYPE_STEP3):
                    //Generate Database
                    SetupService.generateGalaxy(request.getParameterMap());
                    type = TYPE_STEP0;
                    break;
            }
%>
<HTML>
    <HEAD>
        <link rel="stylesheet" href="main.css.jsp" type="text/css">
    </HEAD>
    <BODY bgcolor="white">
        <CENTER>
            <% for (BaseResult br : results) {%>
            <%= br.getFormattedString()%><BR>
            <% }%>
           
            <%
                        switch (type) {
                            case (TYPE_STEP0): 
                                %>
                                 <TABLE class="blu2" align="center">
                                    <TR>
                                        <TD><A href="main.jsp?subCategory=99&page=admin/galaxyGenerator&type=<%= TYPE_STEP1%>">Neue Galaxy Hinzuf�gen</A></TD>
                                    </TR>
                                 </TABLE>
                                    <TABLE class="blu2" align="center">
                                    <TR class="blue2">
                                        <TD>id</TD>
                                        <TD>originX</TD>
                                        <TD>originY</TD>
                                        <TD>startSystem</TD>
                                        <TD>endSystem</TD>
                                        <TD>width</TD>
                                        <TD>height</TD>
                                        <TD>Startable</TD>
                                        <TD colspan="2">Action</TD>
                                    </TR>
                                <%
                    for(Galaxy g : (ArrayList<Galaxy>)Service.galaxyDAO.findAll()){
                        %>
                                
                                    <TR>
                                        <TD><%= g.getId() %></TD>
                                        <TD><%= g.getOriginX() %></TD>
                                        <TD><%= g.getOriginY() %></TD>
                                        <TD><%= g.getStartSystem() %></TD>
                                        <TD><%= g.getEndSystem() %></TD>
                                        <TD><%= g.getWidth() %></TD>
                                        <TD><%= g.getHeight() %></TD>
                                        <TD><A href="main.jsp?subCategory=99&page=admin/galaxyGenerator&process=<%= PROCESS_CHANGESTART %>&galaxyId=<%= g.getId() %>"><%= g.getPlayerStart() %></A></TD>
                                        <TD><A href="main.jsp?subCategory=99&page=admin/galaxyGenerator&type=<%= TYPE_STEP8 %>&galaxyId=<%= g.getId() %>">L�schen</TD>
                                        <TD><A href="main.jsp?subCategory=99&page=admin/galaxyGenerator&type=<%= TYPE_STEP9 %>&galaxyId=<%= g.getId() %>">Anzeigen</TD>
                                    </TR>
                                
                               <% 
                                 }%>
                               </TABLE>
                                <%
                                break;
                            case (TYPE_STEP1):
            %>
            <BR><BR>
            <FORM action="main.jsp?subCategory=99&page=admin/galaxyGenerator&type=<%= TYPE_STEP2%>" method="post">
                <TABLE class="bluetrans">
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            Universe Settings - 1
                        </TD>
                    </TR>
                    <TR>
                        <TD><IMG style="width:250px; height:250px;" src="<%= GameConfig.picPath()%>pic/setup/spiralgalaxy.jpg" ></TD>
                        <TD><IMG style="width:250px; height:250px;" src="<%= GameConfig.picPath()%>pic/setup/spiralgalaxy.jpg" ></TD>
                    </TR>
                    <TR class="blue2">
                        <TD align="center">Spiral-galaxy <INPUT name="universetype" value="<%= EGalaxyType.spiral %>" class="dark"  type="radio" checked /></TD>
                        <TD align="center">Star-Cluster <INPUT DISABLED name="universetype"  value="<%= EGalaxyType.star %>" class="dark" type="radio" /></TD>
                    </TR>
                    <TR>
                        <TD><IMG style="width:250px; height:250px;" src="<%= GameConfig.picPath()%>pic/setup/ringgalaxy.jpg" ></TD>
                        <TD><IMG style="width:250px; height:250px;" src="<%= GameConfig.picPath()%>pic/setup/spiralgalaxy.jpg" ></TD>
                    </TR>
                    <TR class="blue2">
                        <TD align="center">Ring-Cluster<INPUT name="universetype" value="<%= EGalaxyType.ring %>" class="dark" type="radio" /></TD>
                        <TD align="center">Randomized<INPUT DISABLED name="universetype" value="<%= EGalaxyType.randomized %>" class="dark" type="radio" /></TD>
                    </TR>
                    <TR>
                        <TD colspan="2" align="center"><INPUT type="submit" class="dark" value="Continue" /></TD>
                    </TR>
                </TABLE>
            </FORM>
            <%
                                break;
                            case (TYPE_STEP2):
            %>
            <BR><BR>
            <FORM action="main.jsp?subCategory=99&page=admin/galaxyGenerator&type=<%= TYPE_STEP3%>" method="post">
                <TABLE class="bluetrans">
                    <INPUT name="universetype" value="<%= request.getParameter("universetype")%>" type="hidden" />
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            Galaxy Settings - 2
                        </TD>
                    </TR>
                    <TR>
                        <TD>Galaxy-Height/Durchmesser (e.g.: 1000,2000,4000)</TD>
                        <TD align="center"><INPUT name="height" class="dark" style="width:200px;" value="2000"/></TD>
                    </TR>
                    <TR>
                        <TD>Galaxy-Width/Durchmesser (e.g.: 1000,2000,4000)</TD>
                        <TD align="center"><INPUT name="width" class="dark" style="width:200px;" value="2000"/></TD>
                    </TR>
                    <TR>
                        <TD>Ursprung X-Koordinate (Mittelpunkt)</TD>
                        <TD align="center"><INPUT name="originX" class="dark" style="width:200px;" value="2000"/></TD>
                    </TR>
                    <TR>
                        <TD>Ursprung Y-Koordinate (Mittelpunkt)</TD>
                        <TD align="center"><INPUT name="originY" class="dark" style="width:200px;" value="2000"/></TD>
                    </TR>
                    <TR>
                        <TD>Systems per User</TD>
                        <TD align="center"><INPUT name="systemsPerUser" class="dark" style="width:200px;" value="30"/></TD>
                    </TR>
                    <TR>
                        <TD>Galaxy-Density</TD>
                        <TD align="center"><SELECT name="planetDensity" class="dark">
                                <% for(EPlanetDensity ptype : EPlanetDensity.values()){ %>
                                <OPTION value="<%= ptype.toString() %>"><%= ptype.toString() %></OPTION>
                                <% } %>
                            </SELECT>
                        </TD>
                    </TR>
                    <TR>
                        <TD>Planet-TYPES</TD>
                        <TD align="center"><SELECT class="dark">
                                <OPTION>Lots of habitable Planets (M,C,G)</OPTION>
                                <OPTION>Normal distribution of Planets</OPTION>
                                <OPTION>Lots of inhabitable Planets</OPTION>
                            </SELECT>
                        </TD>
                    </TR>
                    <TR>
                        <TD colspan="2" align="center"><INPUT type="submit" class="dark" value="Generate" /></TD>
                    </TR>
                </TABLE>
            </FORM>
            <%
                                    break;

                                      
                            case (TYPE_STEP9):
                                int galaxyId = Integer.parseInt(request.getParameter("galaxyId"));
                                
            %>
            <BR><BR>
                <TABLE class="bluetrans">
                     <TR class="blue2">
                        <TD colspan="2" align="center">  <IMG src="GetGalaxyPic?galaxyId=<%= galaxyId %>" width="1000" height="1000" />
                        </TD>
                    </TR>
                   
                </TABLE>
            <%
                                    break;
                            }%>
        </CENTER>
    </BODY>
</HTML>