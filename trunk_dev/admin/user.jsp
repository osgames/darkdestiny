<%@page import="at.viswars.model.UserSettings"%>
<%@page import="java.util.*" %>
<%@page import="at.viswars.*" %>
<%@page import="at.viswars.admin.*" %>
<%@page import="java.util.GregorianCalendar" %>
<%@page import="at.viswars.model.User" %>
<%@page import="at.viswars.model.PlayerPlanet" %>
<%@page import="at.viswars.model.Planet" %>
<%@page import="at.viswars.model.UserData" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.service.ProfileService" %>
<%
            /*
             * Hier wird das User-Menu f&uuml;r den Admin erzeugt
             */
            session = request.getSession();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));

            if(session.getAttribute("admin") == null){
                //do nothing
                }else{

            if (request.getParameter("change") != null && session.getAttribute("admin") != null) {
                session.setAttribute("userId", (String)request.getParameter("change"));
                PlayerPlanet pp = LoginService.findHomePlanet(Integer.parseInt((String) request.getParameter("change")));
                int systemId = LoginService.findPlanetById(pp.getPlanetId()).getSystemId();

                    session.setAttribute("actPlanet", "" + pp.getPlanetId());
                    session.setAttribute("actSystem", "" + systemId);
                %>
                <%
            } else {
                
                ArrayList<User> users = MainService.getAllUsers();
                GameUtilities gu = new GameUtilities();

%>
    usercount :  <%= users.size() %>
<table border="0" cellspacing="0" cellpadding="0" class="blue" width="100%">
    <tr class="blue2">
        <th>Id</th>
        <th>Username</th>
        <th>Game-Name</th>
        <th>System-Gew�hlt</th>
        <th>Aktivit&auml;t</th>
        <th>Password</th>
        <th>L&ouml;schen</th>
        <th>Andere Parameter einsehen</th>
    </tr>
    
    <%
           for (User uAct : users) {

               UserSettings usAct = Service.userSettingsDAO.findByUserId(uAct.getUserId());
               GregorianCalendar gc = new GregorianCalendar();
               if (usAct != null && usAct.getLeaveDays() > 0) {
                   gc.setTimeInMillis(usAct.getLeaveDate());
               }
               boolean aktiv = uAct.getLastUpdate() > gu.getCurrentTick() - 2;
               boolean inaktiv1 = uAct.getLastUpdate() < gu.getCurrentTick() - 720;
               boolean inaktiv2 = uAct.getLastUpdate() < gu.getCurrentTick() - 1440;
               boolean deleteExp = ProfileService.isDeletionExpired(uAct.getUserId());
               boolean delete = uAct.getDeleteDate() > 0;
    %>
    <tr>
        <td align="center"><a href="changeUser?change=<%= uAct.getUserId() %>"><%= uAct.getUserId() %></a></td>
        <td align="center"><%= uAct.getUserName() %></td>
        <td align="center"><%= uAct.getGameName() %></td>
        <td align="center"><%= uAct.getSystemAssigned() %></td>
        <td><% if (aktiv) {%>
            <font color="#00FF00"><b>ONLINE</b></font>
            <%} else if (deleteExp) {%>            
            <font color="#FF0000"><b>L&Ouml;SCHUNG F&Auml;LLIG</b></font>
            <%} else if (delete) {%>
            <font color="#FF0000"><b>L&Ouml;SCHUNG BEANTRAGT</b></font>
            <%} else if (inaktiv2) {%>
            <font color="#C08000"><b>INAKTIV (2 Wochen)</b></font>
            <%} else if (inaktiv1) {%>
            <font color="#808000"><b>INAKTIV (eine Woche)</b></font>
            <%} else {%>
            <font color="#008000"><b>AKTIV</b></font>
            <%}%><%
        //Urlaub?
        if (usAct != null && usAct.getLeaveDays() != 0) {%><font color="#6666FF"><b>(U: <%= usAct.getLeaveDays()%> D)</b></font><%}
        //Eingestellt am
        if (usAct != null && usAct.getLeaveDays() != 0) {%><font color="#6666FF"><b>(|<%= gc.get(GregorianCalendar.DAY_OF_MONTH)%>.<%= (1 + gc.get(GregorianCalendar.MONTH))%>|)</b></font><%}
        //Sitted by UserName
        if (usAct != null && usAct.getSittedById() > 0) {%><font color="#6666FF"><b>(S: <%= MainService.findUserByUserId(usAct.getSittedById()).getUserName() %>)</b></font><%}
        %></td>
        <td align="center"><a href="adminMain.jsp?page=generatePassword&user=<%= uAct.getUserId() %>">&Auml;ndern</a></td>
        <td align="center"><a href="main.jsp?subCategory=99&page=admin/delUsr&user=<%= uAct.getUserId() %>">L&ouml;schen</a></td>
    <td align="center"><a href="adminMain.jsp?page=changeUser&user=<%= uAct.getUserId() %>">&Auml;ndern</a></td></tr>
    <%
           }
    %>
</table>
<%
            }
            }
%>