<%-- 
    Document   : fixRanks
    Created on : 16.01.2011, 01:44:54
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.viswars.dao.*" %>
<%@page import="at.viswars.model.*" %>
<%@page import="java.util.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
<%
AllianceRankDAO arDAO = (AllianceRankDAO)DAOFactory.get(AllianceRankDAO.class);
ArrayList<AllianceRank> aRanks = arDAO.findAll();

for (AllianceRank ar : aRanks) {
    if (ar.getComparisonRank() == 1) {
        ar.setComparisonRank(4);
    } else if (ar.getComparisonRank() == 2) {
        ar.setComparisonRank(3);
    } else if (ar.getComparisonRank() == 3) {
        ar.setComparisonRank(2);
    } else if (ar.getComparisonRank() == 4) {
        ar.setComparisonRank(1);
    }
    
    arDAO.update(ar);
}
%>
    </body>
</html>
