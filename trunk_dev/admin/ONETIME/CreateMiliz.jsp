<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.database.access.*" %>

<%
Connection con = DbConnect.getConnection();
Statement stmt = con.createStatement();
Statement stmt2 = con.createStatement();

LinkedList<Integer> planets = new LinkedList<Integer>();

ResultSet rs = stmt.executeQuery("SELECT planetID FROM playerplanet WHERE homesystem=1");
while (rs.next()) {
    out.write("FOUNT PLANET " + rs.getInt(1) + "<BR>");
    ResultSet rs2 = stmt2.executeQuery("SELECT number FROM planetconstruction WHERE planetID="+rs.getInt(1)+" AND constructionID=32");
    if (rs2.next()) {
        if (rs2.getInt(1) < 2) {
            out.write("ADDING PLANET " + rs.getInt(1) + "<BR>");
            planets.add(rs.getInt(1));
        }
    } else {
        out.write("ADDING PLANET " + rs.getInt(1) + "<BR>");
        planets.add(rs.getInt(1));        
    }
}

for (Integer i : planets) {
    stmt.execute("DELETE FROM planetconstruction WHERE planetID="+i.intValue()+" AND constructionID=32");
    stmt.execute("INSERT INTO planetconstruction SET planetID="+i.intValue()+", constructionID=32, number=2");

    // Get population
    rs = stmt.executeQuery("SELECT population, userID FROM playerplanet WHERE planetID="+i.intValue());
    long population = 0;
    int playerId = -1;
    if (rs.next()) {
        population = rs.getLong(1);
        playerId = rs.getInt(2);
    }
    stmt.executeUpdate(
            "INSERT INTO playertroops SET planetId=" + i 
            + ", userId=" + playerId + ", troopId=0, number=" 
            + ((int)(population / 100f * 0.5f))
            );
    
    out.write("Generated 2 Kasernen and " + ((int)(population / 100f * 0.5f)) + " Miliz on Planet " + i.intValue()+"<BR>");
}
%>
