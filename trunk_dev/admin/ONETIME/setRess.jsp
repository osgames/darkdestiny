<%@page import="java.sql.*"%>
<%@page import="at.viswars.database.access.*" %>
<%
if (session.getAttribute("REALLY_RUN_THIS_SCRIPT") == null) {
    throw new Exception("Diese Exception entfernen, um dies auszuf&uuml;hren");
}
/*
 * Hiermit werden die Werte f&uuml;r Ynkelonium, howalgonium und eisen 
 * neu berechnet
 */
long timenow = System.currentTimeMillis();

Statement stmt = DbConnect.createStatement();
Statement stmt2 = DbConnect.createStatement();
stmt.executeUpdate("UPDATE planet SET ynkelonium=0, howalgonium=0");
stmt.executeUpdate("UPDATE planet SET iron=0 WHERE (landType='A' OR landType='B')");

ResultSet rs = stmt.executeQuery("SELECT * FROM planet WHERE (landType='A' OR landType='B')");
while (rs.next()) {
    if (rs.getString("landType").equalsIgnoreCase("A")) {
        int randVal = 25000000 + (int)(Math.random()*10000000);
        stmt2.executeUpdate("UPDATE planet SET ynkelonium="+randVal+" WHERE id="+rs.getInt(1));
    } else {
        int randVal = 1000000 + (int)(Math.random()*4000000);
        stmt2.executeUpdate("UPDATE planet SET ynkelonium="+randVal+" WHERE id="+rs.getInt(1));        
    } 
}
%>