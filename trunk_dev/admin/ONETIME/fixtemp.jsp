<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.database.access.*" %>
<%
if (session.getAttribute("REALLY_RUN_THIS_SCRIPT") == null) {
    throw new Exception("Diese Exception entfernen, um dies auszuf&uuml;hren");
}
/*
 * Temperaturen auf sinnvolle Werte setzen, 
 * unbekannt, ob ben&ouml;tigt
 */
 Statement stmt = DbConnect.createStatement();
stmt.executeUpdate("UPDATE planet SET avgTemp=15 WHERE id=2");

ResultSet rs = stmt.executeQuery("SELECT p.id,p.orbitLevel,p.landType FROM planet p, playerplanet pp WHERE p.id<>pp.planetID");

Map<Integer, Integer> tmpData = new HashMap<Integer, Integer>();

while (rs.next()) {
    int orbitLevel = rs.getInt(2);
    int avgTemp = 0;
    
    switch(orbitLevel)
    {
    case 1: 
            avgTemp = 590 + (int)(Math.random()*100);
            break;
    case 2:
            avgTemp = 280 + (int)(Math.random()*80);
            break;
    case 3:
            avgTemp = 130 + (int)(Math.random()*60);
            break;
    case 4:
            avgTemp = 60 + (int)(Math.random()*40);
            break;
    case 5:
            avgTemp = 30 + (int)(Math.random()*20);
            break;
    case 6:
            avgTemp = 10 + (int)(Math.random()*10);
            break;
    case 7:
            avgTemp = 0 + (int)(Math.random()*10);
            break;
    case 8:
            avgTemp = -50 + (int)(Math.random()*20);
            break;
    case 9:
            avgTemp = -95 + (int)(Math.random()*30);
            break;
    case 10:
            avgTemp = -140 + (int)(Math.random()*40);
            break;
    case 11:
            avgTemp = -185 + (int)(Math.random()*50);
            break;
    case 12:
            avgTemp = -230 + (int)(Math.random()*60);
            break;
    }
    
    String landtyp = rs.getString(3);
    
    if (landtyp.equalsIgnoreCase("M")) {
        avgTemp = 10 + (int)(Math.random()*15);
    } else if (landtyp.equalsIgnoreCase("C")) {
        avgTemp = -15 + (int)(Math.random()*25);
    } else if (landtyp.equalsIgnoreCase("G")) {
        avgTemp = 25 + (int)(Math.random()*20);
    }
    
    tmpData.put(rs.getInt(1),avgTemp);
}		

Statement stmt2 = DbConnect.createStatement();

for (Map.Entry<Integer, Integer> entry : tmpData.entrySet()){
    int id = entry.getKey().intValue();
    int temp = entry.getValue().intValue();

    stmt2.executeUpdate("Update planet set avgTemp="+temp+" WHERE id ="+id);
}
%>