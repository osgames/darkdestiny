<%@page import="at.viswars.Logger.Logger"%>
<%@page import="java.sql.*"%>
<%@page import="at.viswars.database.access.*" %>

<%
if (session.getAttribute("REALLY_RUN_THIS_SCRIPT") == null) {
    throw new Exception("Diese Exception entfernen, um dies auszuf&uuml;hren");
}
 /*
  * Da beim Planetenerstellen noch im Vergleich zur aktuellen Struktur der
  * DB Probleme gibt, wird hier einiges ge&auml;ndert
  */

long timenow = System.currentTimeMillis();

Statement Stmt = DbConnect.createStatement();
Statement stmt2 = DbConnect.createStatement();

stmt2.execute("ALTER TABLE planet CHANGE iron iron BIGINT");
stmt2.execute("ALTER TABLE planet CHANGE ynkelonium ynkelonium BIGINT");
stmt2.execute("ALTER TABLE planet CHANGE howalgonium howalgonium BIGINT");

ResultSet rs = Stmt.executeQuery("SELECT * FROM planet");	
while (rs.next()) {
    int diameter = rs.getInt("diameter");
    long volume = (long)((float)(4/3)*Math.PI*(float)(Math.pow((float)(diameter / 2),3)));
    long calcIron = (long)((float)volume / (float)1000);
    
    stmt2.executeUpdate("UPDATE planet SET iron="+calcIron+" WHERE id="+rs.getInt("id"));
    Logger.getLogger().write("Update " + rs.getInt(1)+ " with " + calcIron + " diameter " + diameter + " volume " + volume);
}
out.println("Startzeit gesetzt");
%>