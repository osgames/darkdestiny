<%-- 
    Document   : moduleConfiguration
    Created on : 14.11.2009, 17:57:42
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="at.viswars.admin.module.*"%>
<%@page import="at.viswars.ML"%>
<%@page import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
if (request.getParameter("type") != null) {
    if (request.getParameter("type").equals("deleteAttrib")) {
        int id = Integer.parseInt(request.getParameter("id"));
        ModuleConfiguration.deleteAttribute(id);
        response.sendRedirect("moduleConfiguration.jsp");
    }
}
int userId = 1;
try{
    userId = Integer.parseInt((String)session.getAttribute("userId"));
    }catch(Exception e){


        }

TreeMap<String, ArrayList<AdminModule>> aModules = ModuleConfiguration.getAllModulesSortedByType(userId);
ArrayList<AdminAttribute> aAttributes = ModuleConfiguration.getAllAttributes();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Module Configuration</title>
    </head>
    <body>
        <h2>Module Configuration</h2><BR><BR>
        <B>Module Overview</B><BR><BR>
        <TABLE border=1>
            <TR>
                <TH width="100">Module Id</TH>
                <TH width="300">Module Name</TH>
                <TH width="200">Actions</TH>
            </TR>            
<%
            for (Map.Entry<String, ArrayList<AdminModule>> entry : aModules.entrySet()) {
                %>
                <TR>
                    <TD colspan="3">
                        <%= entry.getKey() %>
                    </TD>
                </TR>
                <%
                ArrayList<AdminModule> ams = entry.getValue();
                for(AdminModule am : ams){
%>
            <TR>
                <TD><%= am.getId() %></TD>
                <TD><%= ML.getMLStr(am.getName(), userId) %></TD>
                <TD><A HREF="modifyModule.jsp?id=<%= am.getId() %>">Modify</A>&nbsp;<A HREF="moduleConfiguration.jsp?id=<%= am.getId() %>&type=deleteModule">Delete</A><BR></TD>
            </TR>
<%                }
            }
%>                         
            <TR>
                <TD></TD>
                <TD></TD>
                <TD><A HREF="insertModule.jsp">Insert new Module</A></TD>                
            </TR>
        </TABLE>
        <BR><BR>
        <B>Attribute Overview</B><BR><BR>
        <TABLE border=1>
            <TR>
                <TH width="100">Attribute Id</TH>
                <TH width="300">Attribute Name</TH>
                <TH width="200">Reference</TH>
                <TH width="200">Actions</TH>
            </TR>
<%
            for (AdminAttribute aa : aAttributes) {
%>            
            <TR>
                <TD><%= aa.getId() %></TD>
                <TD><%= aa.getName() %></TD>
                <TD><%= aa.getRef() %></TD>
                <TD><A HREF="modifyAttribute.jsp?id=<%= aa.getId()%>&type=modify">Modify</A>&nbsp;<A HREF="moduleConfiguration.jsp?id=<%= aa.getId()%>&type=deleteAttrib">Delete</A></TD>
            </TR>
<%                
            }
%>                             
            <TR>
                <TD></TD>
                <TD></TD>
                <TD></TD>
                <TD><A HREF="modifyAttribute.jsp>Insert new Attribute</A></TD>                    
            </TR>
        </TABLE>                       
    </body>
</html>
