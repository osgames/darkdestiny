<%@page import="at.viswars.*"%>
<%@page import="java.sql.*"%>
<%@page import="at.viswars.database.access.DbConnect" %>
<%@page import="at.viswars.Logger.*"%>
<%
session = request.getSession();
int userID = Integer.parseInt((String)session.getAttribute("userId"));

Statement stmt = DbConnect.createStatement();

// Account L&ouml;schung beantragt 
if (request.getParameter("deleteAcc") != null) {    
    // Logger.getLogger().write(request.getParameter("deleteAcc"));
    if (!((String)request.getParameter("deleteAcc")).equals("0")) {
    
        // Set Account to deleted Status 
        stmt.executeUpdate("UPDATE user SET deleteDate="+GameUtilities.getCurrentTick2()+" WHERE id="+userID);
        request.removeAttribute("deleteAcc");
%>    
        <jsp:forward page="main.jsp?page=chooseNewHome">
                <jsp:param name="deleteAcc" value='0' />
        </jsp:forward>                   
<%
    }
}

// Neustart beantragt 
if (request.getParameter("restart") != null) {
    // Delete old Empire and generate new Starting Location
}

// Neuer Hauptplanet gew&auml;hlt
if (request.getParameter("newHome") != null) {
    if (!((String)request.getParameter("newHome")).equals("-1") &&
        !((String)request.getParameter("newHome")).equals("0")) {
        int newPlanetId = Integer.parseInt((String)request.getParameter("newHome"));      

        // Check if planet is valid
        ResultSet rs = stmt.executeQuery("SELECT planetID FROM playerplanet WHERE userID="+userID+" AND planetID="+newPlanetId);
        if (rs.next()) {
            // set new Session variables and go to overview
%>    
            <jsp:forward page="doNewHome.jsp" />            
<%            
        } else {
            // if not reload page
            request.removeAttribute("newHome");
%>
            <jsp:forward page="main.jsp?page=chooseNewHome">
                    <jsp:param name="newHome" value='-1' />
            </jsp:forward>                   
<%            
        }    
    }
}

%>
<TABLE width="80%">
    <TR align="center"><TD colspan=2>
<%
    ResultSet rs = stmt.executeQuery("SELECT planetID FROM playerplanet WHERE userID="+userID);
    if (rs.next()) {
%>
        <FONT style="font-size:13px">Ihr Hauptplanet wurde erobert, bitte w&auml;hlen sie einen neuen Hauptplaneten aus ihren vorhandenen Kolonien!<BR><BR></FONT>
        <%      
        rs = stmt.executeQuery("SELECT name, planetID, population FROM playerplanet WHERE userID="+userID);
        %>
        <FORM method="post" action="main.jsp?page=chooseNewHome" Name="newHomeSel">
            <SELECT size='1' id="newHome" name="newHome">
                <OPTION value="0">--- Hauptplanet w&auml;hlen ---</OPTION>
                <%
                while (rs.next()) {
                %>
                <OPTION value="<%= rs.getInt(2) %>"><%= rs.getString(1) %> (Bev.: <%= FormatUtilities.getFormattedNumber(rs.getLong(3)) %>)</OPTION>
                <%
                } %>
            </SELECT>
            <BR><BR>
            <input type="submit" id="OK" name="OK" value="OK">
        </FORM><BR>
<%
    } else {
%>
        <FONT style="font-size:13px">Ihr Hauptplanet wurde erobert und sie besitzen keine weiteren Kolonien!<BR><BR></FONT>
<%
    }
%>
    </TD></TR>
    <TR align="center" valign="top">
        <TD><FORM method="post" action="resetAccount.jsp" Name="resetAcc"><input type="submit" id="restart" name="restart" value="Neustarten"></FORM></TD>
        <TD><FORM method="post" action="chooseNewHome.jsp" Name="deleteAccF"><input type="submit" id="deleteAcc" name="deleteAcc" value="Account l&ouml;schen"></FORM></TD>        
    </TR>
</TABLE>