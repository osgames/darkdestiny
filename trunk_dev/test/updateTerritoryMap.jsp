<%-- 
    Document   : updateTerritoryPermissions
    Created on : Jun 26, 2012, 7:12:38 PM
    Author     : Admin
--%>

<%@page import="at.viswars.model.TerritoryMapShare"%>
<%@page import="at.viswars.enumeration.ETerritoryMapShareType"%>
<%@page import="java.util.ArrayList"%>
<%@page import="at.viswars.model.TerritoryMap"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.notification.NotificationBuffer"%>
<%@page import="at.viswars.service.TerritoryService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
<%
for(TerritoryMap tm : (ArrayList<TerritoryMap>)Service.territoryMapDAO.findAll()){
   TerritoryMapShare tms = Service.territoryMapShareDAO.findBy(tm.getId(), tm.getRefId(), ETerritoryMapShareType.USER);
   if(tms == null){
       tms = new TerritoryMapShare();
       tms.setTerritoryId(tm.getId());
       tms.setShow(true);
       tms.setRefId(tm.getRefId());
       tms.setType(ETerritoryMapShareType.USER);
       Service.territoryMapShareDAO.add(tms);
   }
}
%>