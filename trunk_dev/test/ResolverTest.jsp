<%-- 
    Document   : diplomacyTest
    Created on : Jan 29, 2011, 5:00:47 PM
    Author     : Dreloc
--%>

<%@page import="at.viswars.diplomacy.DiplomacyRelationExt"%>
<%@page import="at.viswars.ML"%>
<%@page import="at.viswars.utilities.DiplomacyUtilities"%>
<%@page import="at.viswars.model.DiplomacyRelation"%>
<%@page import="at.viswars.service.Service"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
int userId = 48;


%>
<TABLE class="blue2">
    <TR>
        <TD>
            FROM
        </TD>
        <TD>
            TO
        </TD>
        <TD>
            Type
        </TD>
        <TD>
            Relation-Type
        </TD>
    </TR>
    <%
    for(DiplomacyRelation dr : DiplomacyUtilities.findRelationsForUser(userId)){

        DiplomacyRelationExt dre = new DiplomacyRelationExt(dr);
     %>
     <TR>
        <TD>
            <%= dre.getFromName() %>
        </TD>
        <TD>
            <%= dre.getToName() %>
        </TD>
        <TD>
            <%= ML.getMLStr(Service.diplomacyTypeDAO.findById(dr.getDiplomacyTypeId()).getName(), userId) %>
        </TD>
        <TD>
            <%= dr.getType() %>
        </TD>
    </TR>
     <%
     }
     %>
</TABLE>