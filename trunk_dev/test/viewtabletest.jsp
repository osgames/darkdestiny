<%-- 
    Document   : viewtabletest
    Created on : 20.03.2010, 18:07:31
    Author     : Bullet
--%>

<%@page import="at.viswars.dao.*"%>
<%@page import="at.viswars.model.ViewTable"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Trying to get Viewtable for player 1</h1>
        <%

    ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    HashMap<Integer, HashMap<Integer, ViewTable>> vtMap = vtDAO.findByUserCategorized(1);
    for(Map.Entry<Integer, HashMap<Integer, ViewTable>> entry : vtMap.entrySet()){
            //out.write("System : " + entry.getKey() +" <BR>");
            HashMap<Integer, ViewTable> planets = entry.getValue();
            for(Map.Entry<Integer, ViewTable> entry2 : planets.entrySet()){
                if(entry2.getValue().getUserId() != 1){
                    out.write("Planet : " + entry.getKey() +" UserId : " + entry2.getValue().getUserId() + " != 1 <BR>");
                    }
           // out.write("Planet : " + entry.getKey() +" UserId : " + entry2.getValue().getUserId() + " <BR>");
            }
        }
%>
          <h1>Trying to get Viewtable for player 2</h1>
        <%

    vtMap = vtDAO.findByUserCategorized(2);
    for(Map.Entry<Integer, HashMap<Integer, ViewTable>> entry : vtMap.entrySet()){
            //out.write("System : " + entry.getKey() +" <BR>");
            HashMap<Integer, ViewTable> planets = entry.getValue();
            for(Map.Entry<Integer, ViewTable> entry2 : planets.entrySet()){
                if(entry2.getValue().getUserId() != 2){
                    out.write("Planet : " + entry.getKey() +" UserId : " + entry2.getValue().getUserId() + " != 2 <BR>");
                    }
           // out.write("Planet : " + entry.getKey() +" UserId : " + entry2.getValue().getUserId() + " <BR>");
            }
        }
%>
          <h1>Trying to get Viewtable for player 3</h1>
        <%


   vtMap = vtDAO.findByUserCategorized(3);
    for(Map.Entry<Integer, HashMap<Integer, ViewTable>> entry : vtMap.entrySet()){
            //out.write("System : " + entry.getKey() +" <BR>");
            HashMap<Integer, ViewTable> planets = entry.getValue();
            for(Map.Entry<Integer, ViewTable> entry2 : planets.entrySet()){
                if(entry2.getValue().getUserId() != 3){
                    out.write("Planet : " + entry.getKey() +" UserId : " + entry2.getValue().getUserId() + " != 3 <BR>");
                    }
           // out.write("Planet : " + entry.getKey() +" UserId : " + entry2.getValue().getUserId() + " <BR>");
            }
        }
%>
    </body>
</html>
