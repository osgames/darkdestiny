<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="at.viswars.*"%>
<%@page import="java.util.*" %>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.service.*"%>
<%
 /*
  * Hier wird eine Schlacht berechnet.
  */
int attId = 1;
int defId = 1;
if(request.getParameter("attId") != null){
        attId = Integer.parseInt(request.getParameter("attId"));
    }

if(request.getParameter("defId") != null){
        defId = Integer.parseInt(request.getParameter("defId"));
    }
%>
<CENTER>
<FORM method="post" action="calcBattle.jsp" Name="genBattleForm">
<%
ArrayList<ShipData> attackDesigns      = BetaTools.getShipDesignsForUser(attId);
ArrayList<ShipData> defenderDesigns      = BetaTools.getShipDesignsForUser(defId);
// ArrayList<ShipData> availTroops       = BetaTools.getAllTroopTypes();
// ArrayList<ShipData> availPlanetDef    = BetaTools.getAllPDTypes();

boolean firstSD = true;
boolean firstTR = true;
boolean firstPD = true;

%>
<TABLE><TR class="heading"><TD>ATTACKER</TD></TR></TABLE><BR>
<TABLE>
    <TR>
        <TD colspan="3">
            <SELECT id="attId" name="attId" onChange="window.location.href = 'genBattle.jsp?attId='+document.getElementById('attId').value+'&defId='+document.getElementById('defId').value">
<%
for (UserData ud : (ArrayList<UserData>)Service.userDataDAO.findAll()){
    %>
    <OPTION <% if(ud.getUserId() == attId){ %> SELECTED <% } %> value="<%= ud.getUserId() %>"><%= Service.userDAO.findById(ud.getUserId()).getGameName() %></OPTION>
    <%
    }
%>
            </SELECT>
        </TD>
    </TR>
            <%
for (int i=0;i<10;i++) { %>
    <TR><TD width="100">Angreifer <%= (i+1) %></TD>
    <TD width="*">
        <select id="attacker<%= i%>" name="attacker<%= i %>"><option value="0">-- Schiff w&auml;hlen --</option>
        <%
        for (int j=0;j<attackDesigns.size();j++) { %>
        <option value="<%= attackDesigns.get(j).getDesignId() + "_" + i %>"><%= attackDesigns.get(j).getDesignName() %></option>
        <%            
        }
        %>
		</select>
    </TD>
<TD width="100"><INPUT NAME="att<%= i %>Count" VALUE="0" TYPE="text" VALUE="" SIZE="4" MAXLENGTH="7" ALIGN=middle></TD></TR>
<% }
%>    
    </TABLE>
    <BR><BR>
<TABLE><TR class="heading"><TD>DEFENDER</TD></TR></TABLE><BR>
<TABLE>
    <TR>
        <TD colspan="3">
            <SELECT id="defId" name="defId" onChange="window.location.href = 'genBattle.jsp?attId='+document.getElementById('attId').value+'&defId='+document.getElementById('defId').value">
<%
for (UserData ud : (ArrayList<UserData>)Service.userDataDAO.findAll()){
    %>
    <OPTION <% if(ud.getUserId() == defId){ %> SELECTED <% } %> value="<%= ud.getUserId() %>"><%= Service.userDAO.findById(ud.getUserId()).getGameName() %></OPTION>
    <%
    }
%>
            </SELECT>
        </TD>
    </TR>
<%
for (int i=0;i<10;i++) { %>
    <TR><TD width="100">Verteidiger <%= (i+1) %></TD>
    <TD width="*">
        <select id="defender<%= i%>" name="defender<%= i %>"><option value="0">-- Schiff w&auml;hlen --</option>
        <%
        for (int j=0;j<defenderDesigns.size();j++) { %>
        <option value="<%= defenderDesigns.get(j).getDesignId() + "_" + i %>"><%= defenderDesigns.get(j).getDesignName() %></option>
        <%            
        }
        %>
		</select>        
    </TD>
<TD width="100"><INPUT NAME="def<%= i %>Count" VALUE="0" TYPE="text" VALUE="" SIZE="4" MAXLENGTH="7" ALIGN=middle></TD></TR>
<% }
%>    
    </TABLE>
    <BR><BR>
<BR><BR>
<INPUT type="submit" name="submit" value="Engage!">
</FORM>
</CENTER>
