<%--
    Document   : getAffectedUsers
    Created on : 29.01.2011, 16:09:48
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="at.viswars.diplomacy.*"%>
<%@page import="at.viswars.diplomacy.combat.*"%>
<%@page import="at.viswars.utilities.*"%>
<%@page import="at.viswars.dao.*"%>
<%@page import="at.viswars.databuffer.fleet.RelativeCoordinate"%>
<%@page import="at.viswars.model.*"%>
<%@page import="java.util.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Viewtable test</title>
    </head>
    <body>
        <h2>Running tests, retrieve affected Users</h2><BR><BR>
<%
    int testUser = 57;
/*
    UserDAO uDAO = (UserDAO)DAOFactory.get(UserDAO.class);

    for (User u : uDAO.findAllSorted()) {
        if (u.getUserId() == 0) continue;
        int testUser = u.getUserId();
    %>
    <BR><B>TestUser is <%= testUser%></B><BR><BR>
    <%
    /*
        ArrayList<UserShareEntry> useList = DiplomacyUtilities.getAffectedShareUsers(testUser);
        for (UserShareEntry use : useList) {
            if (use.getType() == UserShareEntry.EntryType.ALLIANCE_ENTRY) {
                out.write("Found masterAlliance Id " + use.getId() + "<BR>");
                for (Integer userId : use.getUsers()) {
                    out.write("User " + userId + "<BR>");
                }
            } else if (use.getType() == UserShareEntry.EntryType.SINGLE_ENTRY) {
                out.write("Found single user Id " + use.getId() + "<BR>");
                for (Integer userId : use.getUsers()) {
                    out.write("User " + userId + "<BR>");
                }
            }
        }
    }
    */

    /*
    Logger.getLogger().write("===================================");
    out.write("<BR><BR>");

    ArrayList<UserShareEntry> useList2 = DiplomacyUtilities.getSharingUsers(testUser);
    for (UserShareEntry use : useList2) {
        if (use.getType() == UserShareEntry.EntryType.ALLIANCE_ENTRY) {
            out.write("Found masterAlliance Id " + use.getId() + "<BR>");
            for (Integer userId : use.getUsers()) {
                out.write("User " + userId + "<BR>");
            }
        } else if (use.getType() == UserShareEntry.EntryType.SINGLE_ENTRY) {
            out.write("Found single user Id " + use.getId() + "<BR>");
            for (Integer userId : use.getUsers()) {
                out.write("User " + userId + "<BR>");
            }
        }
    }
    */

    ArrayList<Integer> uList = new ArrayList<Integer>();
    uList.add(58);
    // uList.add(46);
    uList.add(17);
    /*
    uList.add(5);
    uList.add(2);
    uList.add(8);
    uList.add(37);
    uList.add(36);
    */

    RelativeCoordinate rc = new RelativeCoordinate(545,3545);
    CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(uList, rc);
    out.write("CGR = " + cgr + "<BR>");
    out.write("Conflict in system "+rc.getSystemId()+": " + cgr.hasConflictingParties() + "<BR><BR>");
    for (CombatGroupEntry cge : cgr.getCombatGroups()) {
        out.write("Combat group " + cge.getGroupId() + " doesnt like<BR>");       
        for (Integer enemy : cge.getAttackingGroups()) {
            out.write(">> combat group " + enemy + "<BR>");
        }
    }

    out.write("<BR>");
    DiplomacyGroupResult dgr = CombatGroupResolver.resolveUsers(uList);
    for (AllianceEntry ae : dgr.getAlliances()) {
        out.write("Alliance found " + ae.getAllianceId() + "<BR>");
        for (AllianceMemberEntry ame : ae.getMembers()) {
            out.write(">> has member " + ame.getUserId() + "<BR>");
            if (!ame.getRelationsToPlayer().isEmpty()) {
                for (Map.Entry<PlayerEntry,DiplomacyRelation> rtpEntry : ame.getRelationsToPlayer().entrySet()) {
                out.write(">>>> Alliance is neutral: Relation to " + rtpEntry.getKey().getUserId() + ": " + CombatGroupResolver.getNameForRelation(rtpEntry.getValue().getDiplomacyTypeId()) + "<BR>");
                }
            }
        }
        for (Map.Entry<AllianceEntry,DiplomacyRelation> entry : ae.getRelationsToAlliance().entrySet()) {
            out.write(">> has relation to alliance " + entry.getKey().getAllianceId() + ": " + CombatGroupResolver.getNameForRelation(entry.getValue().getDiplomacyTypeId()) + "<BR>");
        }
        for (Map.Entry<PlayerEntry,DiplomacyRelation> entry : ae.getRelationsToPlayer().entrySet()) {
            out.write(">> has relation to player " + entry.getKey().getUserId() + ": " + CombatGroupResolver.getNameForRelation(entry.getValue().getDiplomacyTypeId()) + "<BR>");
        }
        out.write("<BR>");
    }

    for (PlayerEntry pe : dgr.getPlayers()) {
        out.write("Player found " + pe.getUserId() + "<BR>");
        for (Map.Entry<AllianceEntry,DiplomacyRelation> entry : pe.getRelationsToAlliance().entrySet()) {
            out.write(">> has relation to alliance " + entry.getKey().getAllianceId() + ": " + CombatGroupResolver.getNameForRelation(entry.getValue().getDiplomacyTypeId()) + "<BR>");
        }
        for (Map.Entry<PlayerEntry,DiplomacyRelation> entry : pe.getRelationsToPlayer().entrySet()) {
            out.write(">> has relation to player " + entry.getKey().getUserId() + ": " + CombatGroupResolver.getNameForRelation(entry.getValue().getDiplomacyTypeId()) + "<BR>");
        }
        out.write("<BR>");
    }
%>
    </body>
</html>
