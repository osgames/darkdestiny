<%-- 
    Document   : emailTest
    Created on : 20.11.2011, 17:59:51
    Author     : Stefan
--%>

<%@page import="at.viswars.service.EmailService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Sending da mails!</h1>
<%
    String content = "Hallo!\n\nDark Destiny - Runde 8 startet am 16.7.2012!\n\n" + 
            "Am 16. Juli um vorraussichtlich 18:00 startet wieder eine neue Runde" +
            ", da die letzte Runde gro�e Code�nderungen mit sich brachte gab es einige Startschwierigkeiten, die jetzt jedoch alle ausgemerzt sind, " + 
            "auch der letzte Runde stark umgebaute Weltraumkampf wurde weiter verbessert und liefert jetzt sehr gute Ergebnisse.\n" +
            "Ausserdem sind letzte Runde wieder einige nette neue Spieler hinzugestossen, was das ganze wieder etwas interessanter macht :)\n\n" +
            "Hier kurz die wichtigsten �nderungen seit letzter Runde:\n" +
            "* Neues Handelssystem (wesentlich besser angenommen wie das alte)\n" + 
            "* Techtree ist fertiggestellt\n" + 
            "* Abbrechen von Geb�udeabrissen des Gegner nach Invasion m�glich\n" + 
            "* Konfigurierbares Startsystem\n" +           
            "* Verbesserte Sternenkarte\n\n" +            
            "Man kann sich schon f�r die neue Runde registrieren, das Login wird aber erst zum Starttermin freigeschaltet!\n" + 
            "Falls sich am Starttermin noch etwas �ndern sollte, werden alle vorregistrierten Spieler noch informiert." + 
            "\n\nWer mehr wissen will kann gerne im Forum => http://forum.thedarkdestiny.at/ oder \n"
            + "IRC-Chat => http://webchat.euirc.net/ (Channel #darkdestiny) nachfragen." +
            "\nLink zum Spiel => http://www.darkdestiny.at/ !" +
            "\n\nWir w�rden uns jedenfalls wieder sehr �ber rege Beteiligung auch an dieser Runde freuen :).\n\n" +
            "Lg,\nDas DarkDestiny Team";

    EmailService.smtp("bacher.stefan@aon.at", content);
%>
    </body>
</html>
