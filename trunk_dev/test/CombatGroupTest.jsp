<%@page import="at.viswars.diplomacy.combat.CombatGroupResult"%>
<%@page import="at.viswars.diplomacy.combat.CombatGroupResolver"%>
<%@page import="at.viswars.databuffer.fleet.RelativeCoordinate"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="at.viswars.spacecombat.CombatUtilities"%>
<%@page import="java.util.ArrayList"%>
<%

ArrayList<Integer> users = new ArrayList<Integer>();

users.add(7);
users.add(18);

RelativeCoordinate rc = new RelativeCoordinate(594,0);

CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(users, rc);
cgr.print();
HashMap<Integer, ArrayList<Integer>> test = cgr.getAsMap();


%>
Users participating : <BR>
<% for(Integer i : users) { %>
<%= i %><BR>
<% } %>

Groups : <BR>
<% for(Map.Entry<Integer, ArrayList<Integer>> entry : test.entrySet()) { %>
Group : <%= entry.getKey() %><BR>
=> Users:<BR>
<% for(Integer i : entry.getValue()) { %>
=>=>User :<%= i %><BR>
<% } %><BR><BR>
<% } %>