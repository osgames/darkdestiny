<%@page import="at.viswars.*" %>
<%@page import="at.viswars.voting.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.database.access.*" %>
<%
// Read Session Data
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int voteId = Integer.parseInt(request.getParameter("voteId"));
int voted = Integer.parseInt(request.getParameter("voteoption"));

Locale locale = request.getLocale();
if(session.getAttribute("Language") != null){
    locale = (Locale)session.getAttribute("Language");
}

//out.write("<FONT color=\"white\">Vote (Id="+voteId+",userId="+userId+",option="+voted+")</FONT><BR>");

// out.write("You have voted: " + voted + " for vote " + voteId + "<BR>");
// out.write("Access granted? " + VoteUtilities.checkVotePermission(userId, voteId));

// Check for reference
int refMsg = 0;
if (request.getParameter("msgId") != null) {
    refMsg = Integer.parseInt((String)request.getParameter("msgId"));
}

boolean voteClosed = false;
boolean voteNotExistant = false;
boolean acceptedWithThisVote = false;
boolean deniedWithThisVote = false;
boolean alreadyVoted = false;

VoteResult vr = null;

if (VoteUtilities.checkVotePermission(userId,voteId)) {
    //out.write("<FONT color=\"white\">Permission granted</FONT><BR>");
    
    vr = VoteUtilities.vote(userId, voted, voteId, locale);

    if ((vr.getVoteCode() == VoteResult.VOTE_CLOSED_OPTION) ||
        (vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT) ||
        (vr.getVoteCode() == VoteResult.VOTE_CLOSED_DENIED)) {
        voteClosed = true;     
        //out.write("<FONT color=\"white\">Result -> Closed</FONT><BR>");          
    } else if (vr.getVoteCode() == VoteResult.VOTE_NOT_EXISTS) {
        voteNotExistant = true;
        //out.write("<FONT color=\"white\">Result -> Not exists</FONT><BR>");    
    } else if (vr.getVoteCode() == VoteResult.VOTED_ACCEPT) {
        acceptedWithThisVote = true;
        //out.write("<FONT color=\"white\">Result -> Accepted</FONT><BR>");    
    } else if (vr.getVoteCode() == VoteResult.VOTED_DENIED) {
        deniedWithThisVote = true;
        //out.write("<FONT color=\"white\">Result -> Denied</FONT><BR>");    
    } else if (vr.getVoteCode() == VoteResult.VOTE_ALREADY_VOTED) {
        alreadyVoted = true;
        //out.write("<FONT color=\"white\">Result -> Already Voted</FONT><BR>");    
    } else if (vr.getVoteCode() == VoteResult.VOTED) {
        //out.write("<FONT color=\"white\">Result -> Voted</FONT><BR>");    
    }
} else {
    // out.write("<FONT color=\"white\">No Permission</FONT><BR>");    
    return;
}


// Invalid call 
if (!voteNotExistant) { 
    if (voteClosed) {
        if (vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT) {
%>        
        <FONT style="font-size:13px">Diese Abstimmung wurde bereits abgeschlossen und angenommen!</FONT><BR>
<%            
        } else if (vr.getVoteCode() == VoteResult.VOTED_DENIED) {
%>        
        <FONT style="font-size:13px">Diese Abstimmung wurde bereits abgeschlossen und abgelehnt!</FONT><BR>                
<%            
        }
%>        
        <FONT style="font-size:13px"><%= VoteUtilities.getVoteStateHTML(vr,voteId, locale) %></FONT>
<%
    } else if (voteNotExistant) {
%>
            <BR><FONT style="font-size:13px" color="red">Diese Abstimmung existiert nicht mehr!</FONT><BR>
<%
    } else {
        if (acceptedWithThisVote) {
%>
            <BR><FONT style="font-size:13px" color="yellow"><B>Der Antrag wurde mit deiner Stimme angenommen!</B><BR></FONT>
            <FONT style="font-size:13px"><%= VoteUtilities.getVoteStateHTML(vr,voteId,locale) %></FONT>
<%
        } else if (deniedWithThisVote) {
%>
            <BR><FONT style="font-size:13px" color="yellow"><B>Der Antrag wurde mit deiner Stimme abgelehnt!</B><BR></FONT>
            <FONT style="font-size:13px"><%= VoteUtilities.getVoteStateHTML(vr,voteId,locale) %></FONT>
<%            
        } else if (alreadyVoted) {
%>
            <BR><FONT style="font-size:13px" color="yellow"><B>Du hast bereits abgestimmt!</B><BR></FONT>
            <FONT style="font-size:13px"><%= VoteUtilities.getVoteStateHTML(vr,voteId,locale) %></FONT>
<%            
        } else {
%>
            <BR><FONT style="font-size:13px" color="yellow"><B>Deine Stimmabgabe wurde gez&auml;hlt!</B><BR></FONT>
            <FONT style="font-size:13px"><%= VoteUtilities.getVoteStateHTML(vr,voteId,locale) %></FONT>
<%            
        }
    }
} else {
%>
<BR><FONT style="font-size:13px" color="red">Ung&uuml;ltiger Seiten Aufruf!<BR></FONT>
<%    
}

String linkExt = "";
if (refMsg != 0) {
    linkExt = "&type=1&messageId="+refMsg;
}
%>
<BR><BR><A style="font-size:13px" HREF="main.jsp?page=new/messages<%= linkExt %>">Weiter</A>