<%@page import="at.viswars.*" %>
<%@page import="java.util.*" %>

<%
int currentDesign = 0;
int chassisId = 0;
int action = 0;
Map<String, String[]> allPars = request.getParameterMap();

int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));

int relListID[] = (int[])session.getAttribute("relArrayModule");

if (request.getParameter("currentDesign") != null) {
    try {
        currentDesign = Integer.parseInt(request.getParameter("currentDesign"));
    } catch (NumberFormatException e) { }
}

if (request.getParameter("chassisId") != null) {
    try {
        chassisId = Integer.parseInt(request.getParameter("chassisId"));
    } catch (NumberFormatException e) { }
}

// action 1 = delete all modules and recall shipDesign
// action 2 = save Design and show Message 'Design saved' then back to military shipdesign
if (request.getParameter("action") != null) {
    try {
        action = Integer.parseInt(request.getParameter("action"));
    } catch (NumberFormatException e) { }
}

if (action == 1) {
    String errMessage = null;
    ShipUtilitiesNonStatic sun = new ShipUtilitiesNonStatic(userId,planetId);
    sun.changeChassis(currentDesign,chassisId);
    errMessage = sun.outMsg;
    
    if (errMessage == null) errMessage = "";    
    // ShipUtilities.changeChassis(currentDesign,chassisId);
%>
    <jsp:forward page="main.jsp" >
            <jsp:param name="page" value='military' />
            <jsp:param name="shipdesign" value='<%= currentDesign %>' />
            <jsp:param name="action" value='2' />
            <jsp:param name="type" value='6' />
            <jsp:param name="errMsg" value='<%= errMessage %>' />
    </jsp:forward>
<%
} else if (action == 2) {       
    String errMessage = null;
    ShipUtilitiesNonStatic sun = new ShipUtilitiesNonStatic(userId,planetId);
    sun.saveDesign(allPars,relListID);    
    errMessage = sun.outMsg;
    
    if (errMessage == null) errMessage = "";
%>
    <jsp:forward page="main.jsp" >
            <jsp:param name="page" value='military' />
            <jsp:param name="shipdesign" value='<%= currentDesign %>' />
            <jsp:param name="action" value='2' />
            <jsp:param name="type" value='6' />
            <jsp:param name="errMsg" value='<%= errMessage %>' />
    </jsp:forward>
<%
}
%>