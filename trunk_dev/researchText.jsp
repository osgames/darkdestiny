
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.model.Research" %>
<%@page import="at.viswars.GameConfig" %>
<%@page import="at.viswars.ML" %>
<%

            int researchId = Integer.parseInt(request.getParameter("researchId"));
            int userId = 0;
            if (session.getAttribute("userId") != null) {
                userId = Integer.parseInt((String) session.getAttribute("userId"));
            }
            Research r = Service.researchDAO.findById(researchId);
            boolean preview = false;
            if (request.getParameter("preview") != null) {
                preview = true;

            }
            String researchString = ResearchService.getResearchString(researchId, userId);


%>
<html>
    <body>
        <table style="width: 100%; height: 100%;" cellspacing='0' cellpadding='0' align='center' border="1">
            <tr>
                <td>
                    <table cellspacing='0' cellpadding='0' align='center' WIDTH='100%' border='0'>
                        <tr>
                            <td height='41' width='100'                 style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/HeaderLeft.jpg); background-repeat: repeat-x;' border='0'> <FONT COLOR='3399FF' ><center>    </FONT></center>  </td>
                            <td height='41' width='27' align='left'  style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/HeaderSubjectLeft.jpg); background-repeat: repeat-x;'> </td>
                            <td height='41' width='20'               style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/HeaderSubjectBackground.jpg); background-repeat: repeat-x;'> </td>
                            <% if (preview) {%>
                            <td height='41' width='*'  align='left'  style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/HeaderSubjectSpacerLeft.jpg); background-repeat: repeat-x;'> <FONT COLOR='3399FF' ><center><b><%= ML.getMLStr(r.getName(), request.getLocale()) %></b></center> </FONT>  </td>
                            <% } else {%>
                            <td height='41' width='*'  align='left'  style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/HeaderSubjectSpacerLeft.jpg); background-repeat: repeat-x;'> <FONT COLOR='3399FF' ><center><b><%= ML.getMLStr(r.getName(), userId) %></b></center> </FONT>  </td>
                            <% }%>
                            <td height='41' width='30' 		     style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/HeaderSubjectSpacerLeft.jpg); background-repeat: repeat-x;'> </td>
                            <td height='41' width='31' align='right' style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/HeaderSubjectRight.jpg); background-repeat: repeat-x;'> </td>
                        </tr>
                    </table>
                    <table  style="width: 100%; height: 90%" cellspacing='0' cellpadding='0' align='center' WIDTH='100%' border='0'>
                        <tr>
                            <td width='46' height='23' style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/TextTopLeftAge.jpg); background-repeat: none;'> </td>
                            <td width='*' height='23' style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/TextTopSpacer.jpg); background-repeat: repeat-x; text-align: center;'> </td>
                            <td width='46' height='23' style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/TextTopRightAge.jpg); background-repeat: none;'> </td>
                        </tr>
                        <tr>
                            <td background='<%= GameConfig.picPath()%>pic/NewsTable/TextLeftSpacer.jpg'> </td>
                            <% if (preview) {%>
                            <td valign="top" background='<%= GameConfig.picPath()%>pic/NewsTable/TextMid.jpg'> <FONT COLOR='BLACK' style="font-weight:bold;"><P align="justify"><%= ML.getAtlanMLStr(r.getDetailText(), request.getLocale()).replace("_", "&nbsp;").replace("<researches>", researchString)%></P></FONT> </td>
                            <% } else {%>
                            <td valign="top" background='<%= GameConfig.picPath()%>pic/NewsTable/TextMid.jpg'> <FONT COLOR='BLACK' style="font-weight:bold;"><P align="justify"><%= ML.getAtlanMLStr(r.getDetailText(), userId).replace("<researches>", researchString)%></P> </FONT>  </td>
                            <% }%>
                            <td background='<%= GameConfig.picPath()%>pic/NewsTable/TextRightSpacer.jpg'> </td>
                        </tr>
                        <tr>
                            <td width='46' height='37' style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/TextBottomLeftAge.jpg); background-repeat: none;'> </td>
                            <td width='*' height='37' style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/TextBottomSpacerLeft.jpg); background-repeat: repeat-x; text-align: center;'> </td>
                            <td width='46' height='37' style='background-image: url(<%= GameConfig.picPath()%>pic/NewsTable/TextBottomRightAge.jpg); background-repeat: none;'> </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
