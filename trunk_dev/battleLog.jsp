<%@page import="at.viswars.Logger.Logger"%>
<%@page import="at.viswars.enumeration.EShipType"%>
<%@page import="at.viswars.model.Construction"%>
<%@page import="at.viswars.enumeration.EConstructionType"%>
<%@page import="at.viswars.databuffer.RessAmountEntry"%>
<%@page import="com.thoughtworks.xstream.XStream"%>
<%@page import="at.viswars.buildable.ShipDesignExt"%>
<%@page import="at.viswars.ships.ShipRepairCost"%>
<%@page import="at.viswars.model.BattleResult"%>
<%@page import="at.viswars.enumeration.EDamageLevel"%>
<%@page import="at.viswars.spacecombat.SpaceCombatDataLogger"%>
<%@page import="at.viswars.battlelog.TempConfig"%>
<%@page import="at.viswars.model.ShipDesign"%>
<%@page import="at.viswars.model.User"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="at.viswars.*"%>
<%@page import="at.viswars.databuffer.*"%>
<%@page import="java.util.*" %>
<%@page import="at.viswars.result.*" %>
<%@page import="at.viswars.service.*" %>
<%@page import="at.viswars.spacecombat.*" %>
<%@page import="at.viswars.model.BattleLog" %>

<HTML>
    <HEAD>
        <link rel="stylesheet" href="main.css.jsp" type="text/css">
        <script src="java/main.js" type="text/javascript"></script>
    </HEAD>
    <BODY bgcolor="black" style='background-image:url(<%=GameConfig.picPath()%>pic/bg2.jpg); background-repeat: none; color: WHITE'>
        <CENTER>
            <%
                        SpaceCombatDataLogger scdl = null;
                        SpaceCombatDataLogger scdlCompare = null;
                        SpaceCombatDataLogger scdlDiff = null;
                        //######## GENERATOR VALUES
                        final int PLAYER_COUNT = 4;
                        final int PLAYER_FLEET_COUNT = 5;
                        final int PLAYER_FLEET_DESIGN_COUNT = 10;

                        Locale locale = new Locale("de","DE");
                        //#######

                        int userId = 1;
                        try{
                            userId = Integer.parseInt((String)session.getAttribute("userId"));
                            }catch(Exception e){
                        }

                        
                        if (request.getParameter("userId") != null) {
                            userId = Integer.parseInt((String)request.getParameter("userId"));
                        }

                        final int PAGE_OVERVIEW = 0;
                        final int PAGE_GENERATE = 2;
                        final int PAGE_FIGHT = 4;
                        final int PAGE_RESULTS = 5;
                        final int PAGE_VIEW_RESULTS = 7;
                        final int PAGE_SAVE = 6;
                        final int PAGE_BATTLE = 1;

                        boolean comparison = false;

                        int type = PAGE_GENERATE;
                        int configId = 0;
                        if (request.getParameter("config") != null) {
                            configId = Integer.parseInt(request.getParameter("config"));
                        }
                        int battleId = 0;
                        if (request.getParameter("battleId") != null) {
                            battleId = Integer.parseInt(request.getParameter("battleId"));
                            scdl = BattleLogService.getResult(battleId);
                            Logger.getLogger().write("scdl : " + scdl);
                            type = PAGE_BATTLE;
                        }
                        if (request.getParameter("type") != null) {
                            type = Integer.parseInt(request.getParameter("type"));
                        }
                        if (type == PAGE_FIGHT) {
                            int process = Integer.parseInt(request.getParameter("process"));
                            if (process == 1) {
                                scdl = BattleLogService.startConfig(configId);
                                type = PAGE_BATTLE;
                            } else if (process == 2) {
                                BattleLogService.deleteConfig(configId);
                                type = PAGE_GENERATE;
                            } else if (process == 3) {
                                scdl = BattleLogService.generateFight(request.getParameterMap());
                                configId = BattleLogService.getConfigId();
                                type = PAGE_BATTLE;
                            }
                            }


                        if (type == PAGE_RESULTS) {
                            if (request.getParameter("battleResultId") != null) {
                                if (Integer.parseInt(request.getParameter("battleResultId")) > 0) {
                                  BattleResult br =  Service.battleResultDAO.findById(Integer.parseInt(request.getParameter("battleResultId")));
                                  Service.battleResultDAO.remove(br);
                                type = PAGE_RESULTS;
                                }
                            }
                        }
                        if (type == PAGE_VIEW_RESULTS) {
                            if (request.getParameter("battleResultId") != null) {
                                if (Integer.parseInt(request.getParameter("battleResultId")) > 0) {
                                    scdl = BattleLogService.getBattleResult(Integer.parseInt(request.getParameter("battleResultId")));
                                configId = BattleLogService.getConfigId();
                                type = PAGE_BATTLE;
                                }
                            }
                        }
                        if (type == PAGE_SAVE) {
                            BattleLogService.saveResult(request.getParameter("xmlResult"), request.getParameter("resultName"), userId);
                                type = PAGE_RESULTS;
                        }
                        if (type == PAGE_BATTLE) {
                            if (request.getParameter("cbattleResultId") != null) {
                                if (Integer.parseInt(request.getParameter("cbattleResultId")) > 0) {
                                    scdlCompare = BattleLogService.getBattleResult(Integer.parseInt(request.getParameter("cbattleResultId")));
                                    if(scdlCompare != null){

                                comparison = true;
                                }
                                }
                            }
                        }

                        String basePicPath = GameConfig.picPath();
                        String style = "none";
                        String styleDetails = "none";
                        String img2 = "pic/icons/menu_ein.png";
                        String img1 = "pic/icons/menu_aus.png";

            %>
            <TABLE style="width:80%;" class="blue2" align="center">
                <TR class="blue2">
                    <TD align="center">
                        <A href="battleLog.jsp?type=<%= PAGE_GENERATE%>">Generator</A>
                    </TD>
                    <TD align="center">
                        <A href="battleLog.jsp?type=<%= PAGE_RESULTS%>">Results</A>
                    </TD>
                </TR>
            </TABLE>
            <%
                        switch (type) {
                            case (PAGE_OVERVIEW):
            %>
            <!-- #### OVERVIEW ####  -->
            <TABLE class="blue">
                <TR>
                    <TD colspan="2">
                        Konfiguration 1
                    </TD>
                    <TD colspan="2">
                        Konfiguration 2
                    </TD>
                </TR>
                <TR>
                    <TD colspan="2">
                        <SELECT class="dark" style="width:100%;">
                            <OPTION>Config 1</OPTION>
                        </SELECT>
                    </TD>
                    <TD colspan="2">
                        <SELECT class="dark" style="width:100%;">
                            <OPTION>Config 8</OPTION>
                        </SELECT>
                    </TD>
                </TR>
                <TR class="blue2">
                    <TD>
                        Id
                    </TD>
                    <TD>
                        Comment
                    </TD>
                    <TD>
                        Accuracy
                    </TD>
                    <TD>
                        Action
                    </TD>
                </TR>
                <% for (BattleLog bl : (ArrayList<BattleLog>) Service.battleLogDAO.findAll()) {%>
                <TR>
                    <TD>
                        <%= bl.getId()%>
                    </TD>
                    <TD>
                        <%= bl.getComment()%>
                    </TD>
                    <TD>
                        <%= bl.getAccuracy()%>
                    </TD>
                    <TD>
                        <A href="battleLog.jsp?battleId=<%= bl.getId()%>">Fight</A>
                        <INPUT type="checkbox"/>
                    </TD>
                </TR>
                <% }%>
                <TR>
                    <TD align="center" colspan="4">
                        <INPUT type="button" value="Calculate Selected">
                    </TD>
                </TR>
            </TABLE>
            <%
                                            break;
                                        case (PAGE_BATTLE):


                                            if (scdl != null) {
                                                TreeMap<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>> data = BattleLogService.reSort(scdl.getAllLoggedShips());
                                                TreeMap<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>> dataCompare = new TreeMap<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>>();
                                                TreeMap<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>> dataDifferences = new TreeMap<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>>();
                                                if (scdlCompare != null) {
                                                    dataCompare = BattleLogService.reSort(scdlCompare.getAllLoggedShips());
                                                }
                                                

            %>
            <% if(comparison){ %>
            <TABLE class="bluetrans">
                <TR>
                    <TD class="blue">Comparison - Link</TD>
                    <TD>
                        <INPUT  style="width:300px;"class="dark" value="<%= GameConfig.getHostURL() %>/battleLog.jsp?type=<%= PAGE_VIEW_RESULTS %>&cbattleResultId=<%= request.getParameter("cbattleResultId") %>&battleResultId=<%= request.getParameter("battleResultId") %>" />
                    </TD>
                </TR>
            </TABLE>
            <BR>
            <% } %>
            <TABLE class="bluetrans">
                <!-- ConfigurationHeader -->
                <TR class="blue2">
                    <TD>
                        &nbsp;
                    </TD>
                    <TD align="center">
                        Konfiguration 1
                    </TD>
                    <TD align="center">
                        Konfiguration 2
                    </TD>
                    <TD align="center">
                        Unterschiede
                    </TD>
                </TR>
                <!-- Start Data Header -->
                <TR>
                    <!-- First Column - BattleInfo -->
                    <TD>
                        <TABLE cellpadding="0" cellspacing="0" style="font-size: 10px; font-weight: bolder;">
                            <TR class="blue2">
                                <TD class="blue">
                                    2
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <!-- Second Column - First Config -->
                    <TD valign="top">
                        <TABLE class="bluetrans" cellpadding="0" cellspacing="0" style="font-size: 10px; font-weight: bolder;">
                            <TR>
                                <% for (Map.Entry<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>> users : data.entrySet()) {

                                %>
                                <!-- First Column - First Config Result First Player -->
                                <TD valign="top">
                                    <TABLE cellpadding="0" cellspacing="1" style="font-size: 10px; width:100%; font-weight: bolder;">
                                        <TR class="blue2">
                                            <TD align="center" colspan="4">
                                                <%= Service.userDAO.findById(users.getKey()).getGameName()%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD>
                                                Name
                                            </TD>
                                            <TD>
                                                (A)
                                            </TD>
                                            <TD>
                                                (V)
                                            </TD>
                                            <TD>
                                                (Σ)
                                            </TD>
                                        </TR>
                                        <% 
                                        for (Map.Entry<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>> types : users.getValue().entrySet()) { %>
                                        <%
                                         for (Map.Entry<Integer, TreeMap<Integer, SC_DataEntry>> chassis : types.getValue().entrySet()) {%>
                                        <TR>
                                                <%    if(types.getKey().equals(CombatGroupFleet.UnitTypeEnum.SHIP) || types.getKey().equals(CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE)){ %>
                                               <TD align="center" class="blue2" colspan="4">
                                                <%= ML.getMLStr(Service.chassisDAO.findById(chassis.getKey()).getName(), locale)%>
                                                </TD>
                                                <% }else if(types.getKey().equals(CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE)){ %>
                                               <TD align="center" style="background-color: red" colspan="4">
                                                 <%= ML.getMLStr(Service.constructionDAO.findById(chassis.getKey()).getName(), locale)%>
                                                </TD>
                                                <% } %>
                                        </TR>
                                        <% for (Map.Entry<Integer, SC_DataEntry> designs : chassis.getValue().entrySet()) {
                                                 SC_DataEntry de = designs.getValue();

                                                 String popUp = "Defense";
                                                    if(types.getKey().equals(CombatGroupFleet.UnitTypeEnum.SHIP) || types.getKey().equals(CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE)){
                                                     popUp = BattleLogService.buildPopUp(designs.getKey(), locale, BattleLogService.getTotal(de.getDamageDistribution()));
                                                     }
                                        %>

                                        <TR onclick="toogleVisibility('design_1_<%= designs.getKey()%>');" ONMOUSEOVER="doTooltip(event,'<%= popUp %>')" ONMOUSEOUT="hideTip()">
                                            <TD style="color:goldenrod">
                                             <%    if(types.getKey().equals(CombatGroupFleet.UnitTypeEnum.SHIP) || types.getKey().equals(CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE)){ %>

                                                <%= Service.shipDesignDAO.findById(designs.getKey()).getName()%>
                                                <% } %>
                                            </TD>
                                            <TD>
                                                <%= BattleLogService.getTotal(de.getDamageDistribution())%>
                                            </TD>
                                            <TD>
                                                <%= BattleLogService.getLost(de.getDamageDistribution())%>
                                            </TD>
                                            <TD>
                                                <%= (BattleLogService.getTotal(de.getDamageDistribution()) - BattleLogService.getLost(de.getDamageDistribution()))%>
                                            </TD>
                                        </TR>
                                        <TR id="design_1_<%= designs.getKey()%>" style="display:<%= styleDetails%>">
                                            <TD valign="top" colspan="4">
                                                <TABLE  cellpadding="0" cellspacing="0" width="100%" style="font-size: 10px;font-weight: bolder;">
                                                    <TR>
                                                        <TD valign="top">
                                                            <TABLE cellpadding="0" cellspacing="0" width="100%" style="font-size: 10px;font-weight: bolder;">
                                                                <TR>
                                                                    <TD>D-Taken</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getDamageTaken(), 0, GameConstants.VALUE_TYPE_NORMAL)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>D-Eff.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getEffDamageTaken(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>S-Armor</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getST_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>T-Armor</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getTE_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Y-Armor</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getYN_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <%
                                                                Logger.getLogger().write(""+de.getPS_DamagePenetrated_Fail());
                                                                Logger.getLogger().write(""+de.getPS_DamageAbsorbed());
                                                                Logger.getLogger().write(""+de.getPS_DamagePenetrated());
                                                                Logger.getLogger().write(""+de.getPS_ShieldPointsRestored());%>
                                                                <% if(de.getPS_DamageAbsorbed() != 0d || de.getPS_DamagePenetrated() != 0d || de.getPS_ShieldPointsRestored() != 0d){ %>
                                                                <TR>
                                                                    <TD align="center" colspan="2" class="blue">Prallschirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD align="right"><%=FormatUtilities.getFormatScaledNumber(de.getPS_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% } %>
                                                               <% if(de.getHU_DamageAbsorbed() != 0d || de.getHU_DamagePenetrated() != 0d || de.getHU_ShieldPointsRestored() != 0d){ %>
                                                                <TR>
                                                                    <TD align="center" colspan="2" class="blue">HÜ-Schirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                 <% } %>
                                                                <% if(de.getPA_DamageAbsorbed() != 0d || de.getPA_DamagePenetrated() != 0d || de.getPA_ShieldPointsRestored() != 0d){ %>
                                                               <TR>
                                                                    <TD align="center" colspan="2" class="blue">Paratron-Schirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% } %>
                                                            </TABLE>
                                                        <TD>
                                                    </TR>
                                                    <TR>
                                                        <TD colspan="4">
                                                            <TABLE cellpadding="0" cellspacing="0" style="font-size: 10px; width:100%; font-weight: bolder;">

                                                                <TR>
                                                                    <% for (EDamageLevel damageLevel : EDamageLevel.values()) {%>

                                                                    <TD align="center" width="16%" bgcolor="<%= BattleLogService.toColor(damageLevel)%>"><%= de.getDamageDistribution().get(damageLevel)%></TD>
                                                                    <% }%>
                                                                </TR>
                                                            </TABLE>
                                                        <TD>
                                                    </TR>
                                                </TABLE>
                                            </TD>
                                        </TR>
                                        <% }%>
                                        <% }%>
                                        <% }%>

                                         <TR class="blue2">
                                            <TD align="center" colspan="4">
                                                Ressourcen

                                            </TD>
                                        </TR>
                                        <% for(RessAmountEntry rae : BattleLogService.getRessources(users.getValue())){ %>
                                        
                                         <TR>
                                            <TD align="left" colspan="1">
                                                <IMG width=15px height=15px src="<%= Service.ressourceDAO.findRessourceById(rae.getRessId()).getImageLocation() %>" />
                                            </TD>
                                            <TD align="right" colspan="3">
                                                <%=  FormatUtilities.getFormattedDecimal(rae.getQty(), 0) %>
                                            </TD>
                                        </TR>
                                        <% }%>
                                    </TABLE>
                                </TD>
                                <% }%>
                            </TR>
                            <TR>
                                <TD colspan="2">
                                    <FORM method="POST" action="battleLog.jsp?type=<%= PAGE_SAVE%>">
                                        <TABLE class="bluetrans" width="100%">
                                        <% 
                                        XStream xstream = new XStream();
                                        String xml = xstream.toXML(scdl);
                                        %>
                                        <INPUT type="hidden" name="config" value="<%= configId%>"/>
                                        <INPUT type="hidden" name="xmlResult" value='<%= xml %>'/>
                                            <TR>
                                                <TD>
                                                    <INPUT class="dark" value="-Name-" name="resultName" style="width:100%">
                                                </TD>
                                                <TD>
                                                    <INPUT class="dark" type="submit" value="Speichern">
                                                </TD>
                                            </TR>
                                        </TABLE>
                                    </FORM>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <TD valign="top">
                        <TABLE class="bluetrans" cellpadding="0" cellspacing="0" style="font-size: 10px; font-weight: bolder;">
                            <TR>
                                <% for (Map.Entry<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>> users : dataCompare.entrySet()) {

                                %>
                                <!-- First Column - First Config Result First Player -->
                                <TD valign="top">
                                    <TABLE cellpadding="0" cellspacing="1" style="font-size: 10px; width:100%; font-weight: bolder;">
                                        <TR class="blue2">
                                            <TD align="center" colspan="4">
                                                <%= Service.userDAO.findById(users.getKey()).getGameName()%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD>
                                                Name
                                            </TD>
                                            <TD>
                                                (A)
                                            </TD>
                                            <TD>
                                                (V)
                                            </TD>
                                            <TD>
                                                (Σ)
                                            </TD>
                                        </TR>
                                        <% 

                                        for (Map.Entry<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>> types : users.getValue().entrySet()) {                                                                                                  
                                        for (Map.Entry<Integer, TreeMap<Integer, SC_DataEntry>> chassis : types.getValue().entrySet()) {%>
                                        <TR>
                                            <TD class="blue2" colspan="4">
                                                <%= ML.getMLStr(Service.chassisDAO.findById(chassis.getKey()).getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <% for (Map.Entry<Integer, SC_DataEntry> designs : chassis.getValue().entrySet()) {
                                                 SC_DataEntry de = designs.getValue();
                                        %>

                                        <TR onclick="toogleVisibility('design_2_<%= designs.getKey()%>');"  ONMOUSEOVER="doTooltip(event,'<%= BattleLogService.buildPopUp(designs.getKey(), locale, BattleLogService.getTotal(de.getDamageDistribution())) %>')" ONMOUSEOUT="hideTip()">
                                            <TD style="color:goldenrod">
                                                <%= Service.shipDesignDAO.findById(designs.getKey()).getName()%>
                                            </TD>
                                            <TD>
                                                <%= BattleLogService.getTotal(de.getDamageDistribution())%>
                                            </TD>
                                            <TD>
                                                <%= BattleLogService.getLost(de.getDamageDistribution())%>
                                            </TD>
                                            <TD>
                                                <%= (BattleLogService.getTotal(de.getDamageDistribution()) - BattleLogService.getLost(de.getDamageDistribution()))%>
                                            </TD>
                                        </TR>
                                        <TR id="design_2_<%= designs.getKey()%>" style="display:<%= styleDetails%>">
                                            <TD valign="top" colspan="4">
                                                <TABLE  cellpadding="0" cellspacing="0" width="100%" style="font-size: 10px;font-weight: bolder;">
                                                    <TR>
                                                        <TD valign="top">
                                                            <TABLE cellpadding="0" cellspacing="0" width="100%" style="font-size: 10px;font-weight: bolder;">
                                                                <TR>
                                                                    <TD>D-Taken</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getDamageTaken(), 0, GameConstants.VALUE_TYPE_NORMAL)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>D-Eff.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getEffDamageTaken(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>A-Steel</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getST_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>TE-Steel</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getTE_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Y-Steel</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getYN_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% if(de.getPS_DamageAbsorbed() != 0d || de.getPS_DamagePenetrated() != 0d || de.getPS_ShieldPointsRestored() != 0d){ %>
                                                                <TR>
                                                                    <TD colspan="2" class="blue">Prallschirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD align="right"><%=FormatUtilities.getFormatScaledNumber(de.getPS_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% } %>
                                                                <% if(de.getHU_DamageAbsorbed() != 0d || de.getHU_DamagePenetrated() != 0d || de.getHU_ShieldPointsRestored() != 0d){ %>
                                                                <TR>
                                                                    <TD colspan="2" class="blue">HÜ-Schirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% } %>
                                                                <% if(de.getPA_DamageAbsorbed() != 0d || de.getPA_DamagePenetrated() != 0d || de.getPA_ShieldPointsRestored() != 0d){ %>
                                                                <TR>
                                                                    <TD colspan="2" class="blue">Paratron-Schirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% } %>
                                                            </TABLE>
                                                        <TD>
                                                    </TR>
                                                    <TR>
                                                        <TD colspan="4">
                                                            <TABLE cellpadding="0" cellspacing="0" style="font-size: 10px; width:100%; font-weight: bolder;">

                                                                <TR>
                                                                    <% for (EDamageLevel damageLevel : EDamageLevel.values()) {%>

                                                                    <TD align="center" width="16%" bgcolor="<%= BattleLogService.toColor(damageLevel)%>"><%= de.getDamageDistribution().get(damageLevel)%></TD>
                                                                    <% }%>
                                                                </TR>
                                                            </TABLE>
                                                        <TD>
                                                    </TR>
                                                </TABLE>
                                            </TD>

                                        </TR>
                                        <% }%>
                                        <% }%>
                                        <% }%>

                                        
                                         <TR class="blue2">
                                            <TD align="center" colspan="4">
                                                Ressourcen
                                            </TD>
                                        </TR>
                                        <% for(RessAmountEntry rae : BattleLogService.getRessources(users.getValue())){ %>
                                        
                                         <TR>
                                            <TD align="left" colspan="1">
                                                <IMG width=15px height=15px src="<%= Service.ressourceDAO.findRessourceById(rae.getRessId()).getImageLocation() %>" />
                                            </TD>
                                            <TD align="right" colspan="3">
                                                <%=  FormatUtilities.getFormattedDecimal(rae.getQty(), 0) %>
                                            </TD>
                                        </TR>
                                        <% }
                                        %>
                                    </TABLE>
                                </TD>
                                <% }
                                %>
                            </TR>
                        </TABLE>
                    </TD>
                    <%
                                                                    if (scdlCompare != null) {
                                                                        scdlDiff = BattleLogService.substractResult(scdl, scdlCompare);
                                                                        dataDifferences = BattleLogService.reSort(scdlDiff.getAllLoggedShips());
                                                                    }
                    %>
                    <TD valign="top">

                        <TABLE class="bluetrans" cellpadding="0" cellspacing="0" style="font-size: 10px; font-weight: bolder;">
                            <TR>
                                <% 
                                for (Map.Entry<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>> users : dataDifferences.entrySet()) {

                                %>
                                <!-- First Column - First Config Result First Player -->
                                <TD valign="top">
                                    <TABLE cellpadding="0" cellspacing="1" style="font-size: 10px; width:100%; font-weight: bolder;">
                                        <TR class="blue2">
                                            <TD align="center" colspan="4">
                                                <%= Service.userDAO.findById(users.getKey()).getGameName()%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD>
                                                Name
                                            </TD>
                                            <TD>
                                                (A)
                                            </TD>
                                            <TD>
                                                (V)
                                            </TD>
                                            <TD>
                                                (Σ)
                                            </TD>
                                        </TR>
                                     <% for (Map.Entry<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>> types : users.getValue().entrySet()) {
                                         for (Map.Entry<Integer, TreeMap<Integer, SC_DataEntry>> chassis : types.getValue().entrySet()) {%>
                                        <TR>
                                            <TD class="blue2" colspan="4">
                                                <%= ML.getMLStr(Service.chassisDAO.findById(chassis.getKey()).getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <% for (Map.Entry<Integer, SC_DataEntry> designs : chassis.getValue().entrySet()) {
                                                 SC_DataEntry de = designs.getValue();
                                        %>

                                        <TR onclick="toogleVisibility('design_3_<%= designs.getKey()%>');"  ONMOUSEOVER="doTooltip(event,'<%= BattleLogService.buildPopUp(designs.getKey(), locale, BattleLogService.getTotal(de.getDamageDistribution())) %>')" ONMOUSEOUT="hideTip()">
                                            <TD style="color:goldenrod">
                                                <%= Service.shipDesignDAO.findById(designs.getKey()).getName()%>
                                            </TD>
                                            <TD>
                                                <%= BattleLogService.getTotal(de.getDamageDistribution())%>
                                            </TD>
                                            <TD>
                                                <%= BattleLogService.getLost(de.getDamageDistribution())%>
                                            </TD>
                                            <TD>
                                                <%= (BattleLogService.getTotal(de.getDamageDistribution()) - BattleLogService.getLost(de.getDamageDistribution()))%>
                                            </TD>
                                        </TR>
                                        <TR id="design_3_<%= designs.getKey()%>" style="display:<%= styleDetails%>">
                                            <TD valign="top" colspan="4">
                                                <TABLE  cellpadding="0" cellspacing="0" width="100%" style="font-size: 10px;font-weight: bolder;">
                                                    <TR>
                                                        <TD valign="top">
                                                            <TABLE cellpadding="0" cellspacing="0" width="100%" style="font-size: 10px;font-weight: bolder;">
                                                                <TR>
                                                                    <TD>D-Taken</TD>
                                                                    <TD <% if(de.getDamageTaken()< 0){ %> style="color:red" <% }else if(de.getDamageTaken() > 0){ %> style="color:greenyellow" <% } %>
                                                                      align="right"><%= FormatUtilities.getFormatScaledNumber(de.getDamageTaken(), 0, GameConstants.VALUE_TYPE_NORMAL)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>D-Eff.</TD>
                                                                    <TD <% if(de.getEffDamageTaken()< 0){ %> style="color:red" <% }else if(de.getEffDamageTaken() > 0){ %> style="color:greenyellow" <% } %>
                                                                       align="right"><%= FormatUtilities.getFormatScaledNumber(de.getEffDamageTaken(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>A-Steel</TD>
                                                                    <TD <% if(de.getST_DamageAbsorbed()< 0){ %> style="color:red" <% }else if(de.getST_DamageAbsorbed() > 0){ %> style="color:greenyellow" <% } %>
                                                                      align="right"><%= FormatUtilities.getFormatScaledNumber(de.getST_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>TE-Steel</TD>
                                                                    <TD <% if(de.getTE_DamageAbsorbed()< 0){ %> style="color:red" <% }else if(de.getTE_DamageAbsorbed() > 0){ %> style="color:greenyellow" <% } %>
                                                                      align="right"><%= FormatUtilities.getFormatScaledNumber(de.getTE_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Y-Steel</TD>
                                                                    <TD <% if(de.getYN_DamageAbsorbed()< 0){ %> style="color:red" <% }else if(de.getYN_DamageAbsorbed() > 0){ %> style="color:greenyellow" <% } %>
                                                                      align="right"><%= FormatUtilities.getFormatScaledNumber(de.getYN_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% if(de.getPS_DamageAbsorbed() != 0d || de.getPS_DamagePenetrated() != 0d || de.getPS_ShieldPointsRestored() != 0d){ %>
                                                                <TR>
                                                                    <TD colspan="2" class="blue">Prallschirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD align="right"><%=FormatUtilities.getFormatScaledNumber(de.getPS_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD <% if(de.getPS_DamagePenetrated()< 0){ %> style="color:red" <% }else if(de.getPS_DamagePenetrated() > 0){ %> style="color:greenyellow" <% } %>
                                                                       align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD <% if(de.getPS_DamagePenetrated_Fail()< 0){ %> style="color:red" <% }else if(de.getPS_DamagePenetrated_Fail() > 0){ %> style="color:greenyellow" <% } %>
                                                                       align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD <% if(de.getPS_ShieldPointsRestored()< 0){ %> style="color:red" <% }else if(de.getPS_ShieldPointsRestored() > 0){ %> style="color:greenyellow" <% } %>
                                                                       align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPS_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% } %>
                                                                <% if(de.getHU_DamageAbsorbed() != 0d || de.getHU_DamagePenetrated() != 0d || de.getHU_ShieldPointsRestored() != 0d){ %>
                                                                <TR>
                                                                    <TD colspan="2" class="blue">HÜ-Schirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD <% if(de.getHU_DamageAbsorbed()< 0){ %> style="color:red" <% }else if(de.getHU_DamageAbsorbed() > 0){ %> style="color:greenyellow" <% } %>
                                                                       align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD <% if(de.getHU_DamagePenetrated()< 0){ %> style="color:red" <% }else if(de.getHU_DamagePenetrated() > 0){ %> style="color:greenyellow" <% } %>
                                                                        align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD <% if(de.getHU_DamagePenetrated_Fail()< 0){ %> style="color:red" <% }else if(de.getHU_DamagePenetrated_Fail() > 0){ %> style="color:greenyellow" <% } %>
                                                                        align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD <% if(de.getHU_ShieldPointsRestored()< 0){ %> style="color:red" <% }else if(de.getHU_ShieldPointsRestored() > 0){ %> style="color:greenyellow" <% } %>
                                                                        align="right"><%= FormatUtilities.getFormatScaledNumber(de.getHU_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% } %>
                                                                <% if(de.getPA_DamageAbsorbed() != 0d || de.getPA_DamagePenetrated() != 0d || de.getPA_ShieldPointsRestored() != 0d){ %>
                                                                <TR>
                                                                    <TD colspan="2" class="blue">Paratron-Schirm</TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Abs.</TD>
                                                                    <TD <% if(de.getPA_DamageAbsorbed()< 0){ %> style="color:red" <% }else if(de.getPA_DamageAbsorbed() > 0){ %> style="color:greenyellow" <% } %>
                                                                        align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamageAbsorbed(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.</TD>
                                                                    <TD <% if(de.getPA_DamagePenetrated()< 0){ %> style="color:red" <% }else if(de.getPA_DamagePenetrated() > 0){ %> style="color:greenyellow" <% } %>
                                                                        align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamagePenetrated(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Pen.-Failed</TD>
                                                                    <TD <% if(de.getPA_DamagePenetrated_Fail()< 0){ %> style="color:red" <% }else if(de.getPA_DamagePenetrated_Fail() > 0){ %> style="color:greenyellow" <% } %>
                                                                        align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_DamagePenetrated_Fail(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <TR>
                                                                    <TD>Shield-Rest.</TD>
                                                                    <TD <% if(de.getPA_ShieldPointsRestored()< 0){ %> style="color:red" <% }else if(de.getPA_ShieldPointsRestored() > 0){ %> style="color:greenyellow" <% } %>
                                                                        align="right"><%= FormatUtilities.getFormatScaledNumber(de.getPA_ShieldPointsRestored(), 0, GameConstants.VALUE_TYPE_NORMAL, locale)%></TD>
                                                                </TR>
                                                                <% } %>
                                                            </TABLE>
                                                        <TD>
                                                    </TR>
                                                    <TR>
                                                        <TD colspan="4">
                                                            <TABLE cellpadding="0" cellspacing="0" style="font-size: 10px; width:100%; font-weight: bolder;">

                                                                <TR>
                                                                    <% for (EDamageLevel damageLevel : EDamageLevel.values()) {%>

                                                                    <TD <% if(de.getDamageDistribution().get(damageLevel) < 0){ %> style="color:darkred" <% }else if(de.getDamageDistribution().get(damageLevel) > 0){ %> style="color:green" <% } %>
                                                                        align="center" width="16%" bgcolor="<%= BattleLogService.toColor(damageLevel)%>"><%= de.getDamageDistribution().get(damageLevel)%></TD>
                                                                    <% }%>
                                                                </TR>
                                                            </TABLE>
                                                        <TD>
                                                    </TR>
                                                </TABLE>
                                            </TD>

                                        </TR>
                                        <% }%>
                                        <% }%>
                                        <% }%>
                                    </TABLE>
                                </TD>
                                <% }%>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
                <% %>

            </TABLE>


            <%
                                            }
                                            break;

                                        case (PAGE_GENERATE):
                                            int step = 1;
                                            if (request.getParameter("step") != null) {
                                                step = Integer.parseInt(request.getParameter("step"));
                                            }
                                            switch (step) {
                                                case (1):
            %>
            <BR>
            <FORM action="battleLog.jsp?type=<%= PAGE_GENERATE%>&step=2" method="POST">
                <TABLE class="bluetrans">
                    <TR class="blue2">
                        <TD>
                            Spieler
                        </TD>
                        <TD>
                            Flotten
                        </TD>
                        <TD>
                            Planetbesitzer
                        </TD>
                    </TR>
                    <% for (int i = 0; i < PLAYER_COUNT; i++) {%>
                    <TR>
                        <TD>
                            <SELECT name="user_<%= i%>" class="dark">
                                <OPTION selected value="-1" >- None -</OPTION>
                                <% for (User u : Service.userDAO.findAllSortedByName()) {%>
                                <% if(Service.shipDesignDAO.findByUserId(u.getUserId()).size() == 0) continue; %>
                                <OPTION value="<%= u.getUserId()%>"><%= u.getGameName()%> </OPTION>
                                <% }%>
                            </SELECT>
                        </TD>
                        <TD>
                            <SELECT name="fleetCount_<%= i%>" class="dark">
                                <% for (int j = 1; j < PLAYER_FLEET_COUNT; j++) {%>
                                <OPTION><%= j%> </OPTION>
                                <% }%>
                            </SELECT>
                        </TD>
                        <TD align="center" >
                            <INPUT class="dark" type="radio" <% if(i == 0){%>checked<% } %>  name="planetOwner" value="<%= i %>">
                        </TD>
                    </TR>
                    <% }%>
                    <TR class="blue2">
                        <TD colspan="3" align="center">
                            BattleClass
                        </TD>
                    </TR>
                    <TR>
                        <TD colspan="3">
                            <SELECT name="battleClass" class="dark" style="width:100%">
                                <OPTION value="at.viswars.spacecombat.combatcontroller.SpaceCombatNew" SELECTED>- Version 1.5 -</OPTION>
                                <OPTION value="at.viswars.spacecombat.combatcontroller.SpaceCombat" >- Standard -</OPTION>
                            </SELECT>
                        </TD>
                    </TR>
                    <TR class="blue2">
                        <TD colspan="3" align="center">
                            CompareResult
                        </TD>
                    </TR>
                    <TR>
                        <TD colspan="3">
                            <SELECT name="cbattleResultId" class="dark" style="width:100%">
                                <OPTION value="0">- Keine -</OPTION>
                                <% for (BattleResult br : (ArrayList<BattleResult>) Service.battleResultDAO.findByUserId(userId)) {%>
                                <OPTION value="<%= br.getId()%>"><%= br.getName()%></OPTION>
                                <% }%>
                            </SELECT>
                        </TD>
                    </TR>
                    <TR>
                        <TD align="center" colspan="3">
                            <INPUT type="submit" class="dark" value="Continue" />
                        </TD>
                    </TR>
                </TABLE>
            </FORM>
            <TABLE class="bluetrans">
                <TR class="blue2">
                    <TD colspan="4">
                        Saved Config (Deleted periodically)
                    </TD>
                </TR>
                <% for (Map.Entry<Integer, TempConfig> entry : BattleLogService.getTempConfigs().descendingMap().entrySet()) {

                %>
                <TR>
                    <TD>
                        <%= entry.getKey()%>
                    </TD>
                    <TD>
                        <TABLE class="bluetrans">
                            <% for (String s : entry.getValue().getPlayers()) {%>
                            <TR>
                                <TD>
                                    <%= s%>
                                </TD>
                            </TR>
                            <% }%>
                        </TABLE>
                    </TD>
                    <TD>
                        <A href="battleLog.jsp?type=<%= PAGE_FIGHT%>&config=<%= entry.getKey()%>&process=1">Fight</A>
                    </TD>
                    <TD>
                        <A href="battleLog.jsp?type=<%= PAGE_FIGHT%>&config=<%= entry.getKey()%>&process=2">Delete</A>
                    </TD>
                </TR>
                <% }%>
            </TABLE>
            <% break;
                                                      case (2):
                     int planetOwner = -1;
                        try{
                         planetOwner = Integer.parseInt(request.getParameter("planetOwner"));
                      }catch(Exception e){

                      }
            %>
            <BR>
            <FORM action="battleLog.jsp?type=<%= PAGE_FIGHT%>&process=3" method="POST">
                <INPUT type="hidden" name="cbattleResultId" value="<%= request.getParameter("cbattleResultId")%>" />
                <INPUT type="hidden" name="battleClass" value="<%= request.getParameter("battleClass")%>" />
                <TABLE class="bluetrans">
                    <TR>
                        <TD>
                            <TABLE class="bluetrans">
                                <% int count = 0;%>
                                <% for (int i = 0; i < PLAYER_COUNT; i++) {

                                    int uId = Integer.parseInt(request.getParameter("user_" + i));
                                    Logger.getLogger().write("userId : " + uId);
                                    if (uId <= 0) {
                                        continue;
                                    }
                                    boolean isOwner = false;

                                     Logger.getLogger().write("Comparing " + planetOwner + " == " + uId);
                                 if(planetOwner == (i)){
                                     %>
                                     <INPUT type="HIDDEN" name="planetOwner" value="<%= uId %>"
                                     <%
                                     isOwner = true;
                                  }
                                %>
                                <% Logger.getLogger().write("i : " + i);%>
                                <% if ((i % 2) == 0) {%>
                                <TR>
                                    <% }%>
                                    <TD valign="top">
                                        <TABLE class="bluetrans" style="width:100%;">
                                            <%
                                                 count++;
                                            %>
                                            <TR class="blue2">
                                                <TD align="left">
                                                    <%= Service.userDAO.findById(uId).getGameName()%>
                                                </TD>
                                                <TD>
                                                    <SELECT name="group_<%= uId %>" class="dark" style="width: 100%;">
                                                        <% for(int x = 1; x <= 4; x++){ %>
                                                        <OPTION <% if(count == x){%> SELECTED <% } %> value="<%= x %>"><%= x %></OPTION>
                                                        <% } %>
                                                    </SELECT>
                                                </TD>
                                            </TR>
                                            <%
                                                 int fleetCount = Integer.parseInt(request.getParameter("fleetCount_" + (i)));
                                                 
                                                 Logger.getLogger().write("Fleetcount : " + fleetCount);
                                            %>

                                            <% if(isOwner){%>
                                            <TR class="blue2">
                                                <TD colspan="2" align="center">
                                                   Shields
                                                </TD>
                                            </TR>
                                            <TR class="blue2">
                                                <TD align="center">
                                                   PA Shield
                                                </TD>
                                                <TD align="center">
                                                   HU Shield
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD align="center">
                                                    <INPUT type="radio" name="shield" value="<%= Construction.ID_PLANETARY_PARATRONSHIELD %>">
                                                </TD>
                                                <TD align="center">
                                                    <INPUT type="radio" name="shield" value="<%= Construction.ID_PLANETARY_HUESHIELD %>">
                                                </TD>
                                            </TR>
                                            <TR class="blue2">
                                                <TD colspan="2" align="center">
                                                   PlanetDefense
                                                </TD>
                                            </TR>
                                            <% for(Construction c : Service.constructionDAO.findByType(EConstructionType.PLANETARY_DEFENSE)){ %>

                                            <TR>
                                                <TD>
                                                    <%= ML.getMLStr(c.getName(), locale) %>
                                                </TD>
                                                <TD>
                                                    <INPUT name="defense_<%= c.getId() %>" class="dark" type="textfield" value="0" />
                                                </TD>
                                            </TR>
                                            <% } %>
                                                <%} %>
                                                <% for (int k = 1; k <= fleetCount; k++) {%>
                                            <TR class="blue2">
                                                <TD colspan="2" align="center">
                                                    Fleet <%= k%>
                                                </TD>
                                            </TR>
                                            <% if (Service.shipDesignDAO.findByUserId(uId).size() == 0) {%>
                                            <TR>
                                                <TD>
                                                    &nbsp;
                                                </TD>
                                            </TR>
                                            <% } else {%>
                                            <% for (int l = 0; l <= PLAYER_FLEET_DESIGN_COUNT; l++) {%>
                                            <TR>
                                                <TD>
                                                    <SELECT name="fleet_<%= uId%>_<%= k%>_<%= l%>_design" class="dark">
                                                        <OPTION value="-1" >- None - </OPTION>
                                                        <% for (ShipDesign shipDesign : Service.shipDesignDAO.findByUserId(uId)) {%>
                                                        <% if(!isOwner && shipDesign.getType().equals(EShipType.SPACEBASE)){ 
                                                            continue; } %>
                                                        <OPTION value="<%= shipDesign.getId()%>"><%= shipDesign.getName()%></OPTION>
                                                        <% }%>
                                                    </SELECT>
                                                </TD>
                                                <TD>
                                                    <INPUT name="fleet_<%= uId%>_<%= k%>_<%= l%>_count" class="dark" type="textfield" value="0" />
                                                </TD>
                                            </TR>
                                            <% }%>
                                            <% }%>
                                            <% }%>
                                        </TABLE>
                                    </TD>

                                    <% if ((i % 2) != 0) {%>
                                </TR>
                                <% }%>
                                <% }%>
                            </TABLE>
                        </TD>
                    </TR>
                    <TR style="width:100%">
                        <TD colspan="2">
                            <TABLE align="center" style="width:100%;">
                                <TR>
                                    <TD align="center">
                                        <INPUT type="submit" class="dark" value="Continue" />
                                    </TD>
                                </TR>
                            </TABLE>
                        </TD>
                    </TR>
                </TABLE>
            </FORM>

            <%   break;
                                                        }%>
            <% break;
            case(PAGE_RESULTS):%>
                <FORM action="battleLog.jsp?type=<%= PAGE_VIEW_RESULTS%>" method="POST">
                <TABLE>
                    <TR class="blue2">
                        <TD>
                            Config 1
                        </TD>
                        <TD>
                            Config 2
                        </TD>
                    </TR>
                    <TR>
                        <TD>

                            <SELECT name="battleResultId" class="dark">
                                <OPTION selected value="-1" >- None -</OPTION>
                                <%for(BattleResult br : (ArrayList<BattleResult>)Service.battleResultDAO.findByUserIdSorted(userId)){%>
                                <OPTION value="<%= br.getId() %>"><%= br.getName() %> </OPTION>
                                <% }%>
                            </SELECT>
                        </TD>
                        <TD>

                            <SELECT name="cbattleResultId" class="dark">
                                <OPTION selected value="-1" >- None -</OPTION>
                                <% for(BattleResult br : (ArrayList<BattleResult>)Service.battleResultDAO.findByUserIdSorted(userId)){%>
                                <OPTION value="<%= br.getId() %>"><%= br.getName() %> </OPTION>
                                <% }%>
                            </SELECT>
                        </TD>
                    </TR>
                    <TR>
                        <TD align="center" colspan="2">
                            <INPUT type="submit" class="dark" value="Compare" />
                        </TD>
                    </TR>
                </TABLE>
                            <BR>
                            <BR>
                <TABLE class="bluetrans">
                    <TR class="blue2">
                        <TD colspan="6" align="right">
                            <SELECT class="dark" id="userId" name="userId"  onChange="window.location.href='battleLog.jsp?type=<%= PAGE_RESULTS %>&userId='+document.getElementById('userId').value;">
                                <% for(User u : Service.userDAO.findAllSorted()){
                                if(u.getUserId() == 0)continue;
                                %>
                                <OPTION <% if(userId == u.getUserId()){ %>SELECTED<% } %> value="<%= u.getUserId()%>"><%= u.getGameName()%></OPTION>
                                <% } %>
                            </SELECT>
                        </TD>
                    </TR>
                    <TR class="blue2">
                        <TD>
                            Id
                        </TD>
                        <TD>
                            User-Name
                        </TD>
                        <TD>
                            Name
                        </TD>
                        <TD>
                           Version
                        </TD>
                        <TD>
                            Link
                        </TD>
                        <TD>
                            Action
                        </TD>
                    </TR>
                    <% for(BattleResult br : Service.battleResultDAO.findByUserIdSorted(userId)){ %>
                    <TR>
                        <TD>
                            <%= br.getId() %>
                        </TD>
                        <TD>
                            <%= Service.userDAO.findById(br.getUserId()).getGameName() %>
                        </TD>
                        <TD>
                            <%= br.getName() %>
                        </TD>
                        <TD align="center" class="blue">
                            <%= br.getVersion() %>
                        </TD>
                        <TD>
                            <INPUT  style="width:300px;"class="dark" value="<%= GameConfig.getHostURL() %>/battleLog.jsp?type=<%= PAGE_VIEW_RESULTS %>&battleResultId=<%= br.getId() %>" />
                        </TD>
                        <TD>
                        <A href="battleLog.jsp?type=<%= PAGE_VIEW_RESULTS %>&battleResultId=<%= br.getId() %>">View</A>
                        <% if(br.getUserId() == userId){ %>
                        <A href="battleLog.jsp?type=<%= PAGE_RESULTS%>&battleResultId=<%= br.getId() %>&userId=<%= userId %>">Delete</A>
                        <% } %>
                        </TD>
                    </TR>
                    <% } %>
                </TABLE>
            </FORM>
                <% break;

                        }
            %>
        </CENTER>
    </BODY>
</HTML>