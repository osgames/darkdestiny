
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.viswars.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.view.header.*" %>
<%@page import="at.viswars.service.MainService" %>
<%@page import="at.viswars.service.LoginService" %>
<%@page import="at.viswars.service.Service" %>
<%@page import="at.viswars.model.Language" %>
<%@page import="at.viswars.GameUtilities" %>
<%@page import="at.viswars.model.User" %>
<%@page import="at.viswars.Logger.*"%>
<%@page import="at.viswars.model.UserData" %>
<%@page import="at.viswars.viewbuffer.HeaderBuffer" %>
<%@page import="at.viswars.DebugBuffer.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<style type='text/css' >
    #tickTimePositioner{position:relative; top:0px; left:0px; width:0px; height:0px; border:0px; z-index:1; background-repeat:no-repeat; }
    #tickTimeLabel{position:absolute; top:30px; left:12px; width:100px; height:0px; border:0px; z-index:1; background-repeat:no-repeat; }
    #tickTime{position:absolute; top:27px; left:86px; width:37px; height:0px; border:0px; z-index:1; background-repeat:no-repeat; }
</style>
<style type="text/css">
.ie.example {  
  background: inherit;
  background-color: inherit;
  color: inherit;
  height: inherit;
  border: inherit;
  text-align: inherit;
  font-size: 12px;
}
.ie7.example {

}

</style>

<%
            int previousCategoryId = 0;
            int nextCategoryId = 0;
            int previousSystemId = 0;
            int nextSystemId = 0;
            int previousPlanetId = 0;
            int nextPlanetId = 0;


            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
            int systemId = Integer.parseInt((String) session.getAttribute("actSystem"));
            int actCategory = 0;

            User u = Service.userDAO.findById(userId);
            UserData ud = Service.userDataDAO.findByUserId(userId);

            if (session.getAttribute("actCategory") != null) {
                actCategory = Integer.parseInt((String) session.getAttribute("actCategory"));
            } else {
                session.setAttribute("actCategory", "0");
            }

//Logic und Data f&uuml;r den Header           
            HeaderData headerData = null;
            try {
                //dbg Logger.getLogger().write("Loaded Headerdata for user: "+userId);
                headerData = HeaderBuffer.getUserHeaderData(userId);
                if (headerData == null) throw new Exception();
            } catch (Exception e) {
                // Maybe a new user -> reload
                HeaderBuffer.reloadUser(userId);
                headerData = HeaderBuffer.getUserHeaderData().get(userId);                
            }

            boolean planetFound = false;
            /* Wenn im Header nichts gemacht wurde, m&uuml;ssen einfach nur die DropDownBoxen richtig eingestellt werden
             * Es wird nach der richtigen Kategorie und dem richtigen System gesucht
             *
             * */

            for (Map.Entry<Integer, CategoryEntity> me : headerData.getCategorys().entrySet()) {
                for (Map.Entry<Integer, SystemEntity> me2 : me.getValue().getSystems().entrySet()) {
                    for (Map.Entry<Integer, PlanetEntity> me3 : me2.getValue().getPlanets().entrySet()) {
                        if (me3.getKey() == planetId) {
                            planetFound = true;
                            break;
                        }
                    }
                    if (planetFound) {
                        break;
                    }
                }
                if (planetFound) {
                    break;
                }
            }
%>

<CENTER>
    <TABLE border='0' cellpadding=0 cellspacing=0 align="center" width=100% height="71px" >
        <TR>
            <TD bgcolor="BLACK" style='background-image: url(<%=GameConfig.picPath()%>pic/menu/tickerBackground.png);' width="137">
                <div id='tickTimeLabel' style='text-align:center;  width:65px; color: #ffffff; font-size:10px;' >
                    <%= ML.getMLStr("top_lbl_nexttick", userId)%>
                </div>
                <div id='tickTime' align=right>
                    <form name='countdownform'><input size ='4' align='right' style='border: solid 2px #000000; text-align:right; color: #ffffff; background-color: #000000; font-size:11px; font-family:"Courier New"; font-weight:bold;' name='countdowninput'>
                    </form>
                </div>
            </TD>

            <!--     <TD style='background-image: url(pic/menu/headerSpacer.png)'  width="50">&nbsp;</TD> -->
            <TD style='background-image: url(<%=GameConfig.picPath()%>pic/menu/headerSpacer.png)'  width="*">&nbsp;</TD>
            <!--  <TD style='background-image: url(pic/menu/dark.png); background-repeat:no-repeat'  width="*">&nbsp;</TD> -->
            <TD width="23"><IMG src="<%=GameConfig.picPath()%>pic/menu/ltop.png" /></TD>
            <TD nowrap width="574px">
                <CENTER>
                    <TABLE width="574px" height="71px" border='0' cellpadding=0 cellspacing=0 align="center" style='background-image: url(<%=GameConfig.picPath()%>pic/menu/headerImage.png);' >
                        <TR>
                            <TD width='20px'></TD>
<%
            if (LoginService.findHomePlanetByUserId(userId) != 0) {
                            %>
                            <TD width='63px'><A onClick="return popupSwitch(this, 'editor')" target="blank" href="planetPop.jsp"><IMG BORDER="0" onmouseover="doTooltip(event,'<%= ML.getMLStr("top_pop_switchtoplanet", userId)%>')" onmouseout="hideTip()" SRC="<%=GameConfig.picPath()%>pic/menu/homeworldButton.png" ALT="Zum Heimplaneten"/></A> </TD>
                                    <%
                                        } else {
                                    %>
                            <TD width='63px'>&nbsp;</TD>
                            <%            }
                            %>
                            <TD width='85px'>
                                <TABLE width="100%" border = '0' cellspacing='0' cellpadding='0'>
                                    <TR>
                                        <TD style="height:17px; width:72px" width="100%" align="CENTER"><FONT style="font-size:10px; font-weight:bolder"><%= ML.getMLStr("top_lbl_category", userId)%></FONT></TD>
                                    </TR>
                                    <TR>
                                        <TD style="height:17px; width:72px" width="100%" align="CENTER"><FONT style="font-size:10px; font-weight:bolder"><%= ML.getMLStr("top_lbl_system", userId)%></FONT></TD>
                                    </TR>
                                    <TR>
                                        <TD style="height:17px; width:72px" width="100%" align="CENTER"><FONT style="font-size:10px; font-weight:bolder"><%= ML.getMLStr("top_lbl_planet", userId)%></FONT></TD>
                                    </TR>
                                </TABLE>
                            </TD>

                            <TD width="223px">
                                <CENTER>
                                    <TABLE border = '0' height='12px' cellspacing='0' cellpadding='0'>
                                        <TR>

                                            <%
            // DebugBuffer.trace("JSP: Processing Top JSP");


            //dbg Logger.getLogger().write("test 2");

            //dbg Logger.getLogger().write("Category: "+switchCategory+" System: "+switchSystem+" Planet: "+planetId+" switchPlanet: "+switchPlanet);
            //dbg Logger.getLogger().write(" Changes . Category: "+categoryChanged+" System: "+systemChanged+" Planet: "+planetChanged);

            //Wurde kein Planet ausgew&auml;hlt zum anzeigen ist es der Planet in den Session variablen

            // DebugBuffer.trace("JSP: Test 10 JSP");
            // Erstellen der ersten DropDown Box die die Kategorien beinhaltet
            try {
                previousCategoryId = headerData.getCategorys().get(actCategory).getPreviousCategoryId();
                nextCategoryId = headerData.getCategorys().get(actCategory).getNextCategoryId();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in header", e);
                //dbg Logger.getLogger().write("Errrrror : " + e);
            }
                                            %>
                                            <TD valign='top' width="26"><A href='main.jsp?page=switchPlanet&stype=4&switchCategory=<%=previousCategoryId%>'><IMG BORDER="0" src="<%=GameConfig.picPath()%>pic/menu/leftArrow.png" width="26" height="17"></IMG></A></TD>
                                            <TD  valign='top'>
                                                <FORM style='padding:0px; margin:0px; border:0px;'  name="categoryForm">
                                                    <SELECT class="dark example" style="width:170px;" name="category" onChange="window.location=document.categoryForm.category.options[document.categoryForm.category.selectedIndex].value">
<%
            boolean firstCategory = true;
            ArrayList<CategoryEntity> categoriesOrdered = new ArrayList<CategoryEntity>();
            TreeMap<String, CategoryEntity> categoriesTreeMap = new TreeMap<String, CategoryEntity>();
            for (Map.Entry<Integer, CategoryEntity> me : headerData.getCategorys().entrySet()) {
                categoriesTreeMap.put(me.getValue().getCategoryName(), me.getValue());
            }

            categoriesOrdered.addAll(categoriesTreeMap.values());
            //  DebugBuffer.trace("JSP: Test 11 JSP");
            for (CategoryEntity cat : categoriesOrdered) {
                int tmpCategoryId = cat.getCategoryId();

                String categoryName = cat.getCategoryName();

                if (tmpCategoryId == actCategory) {%>
                                                        <OPTION SELECTED value='main.jsp?page=switchPlanet&stype=4&switchCategory=<%= tmpCategoryId%>'> <%=categoryName%> </OPTION>
                                                        <%              } else {%>
                                                        <OPTION value='main.jsp?page=switchPlanet&stype=4&switchCategory=<%= tmpCategoryId%>'> <%=categoryName%> </OPTION>
                                                        <%              }
            }%>
                                                    </SELECT>
                                                </FORM>
                                            </TD>
                                            <TD  valign='top' width="26"><A href='main.jsp?page=switchPlanet&stype=4&switchCategory=<%= nextCategoryId%>'><IMG  BORDER="0" src="<%=GameConfig.picPath()%>pic/menu/rightArrow.png" width="26" height="17"></IMG></A></TD>
                                        </TR>
                                        <%

            // DebugBuffer.trace("JSP: Test 12 JSP");
            try {
                previousSystemId = headerData.getCategorys().get(actCategory).getSystems().get(systemId).getPreviousSystemId();
                nextSystemId = headerData.getCategorys().get(actCategory).getSystems().get(systemId).getNextSystemId();
                // DebugBuffer.trace("JSP: Test 12.3 JSP");
                //Erstellen der zweiten Drop Down Box mit den Systemen
            } catch (Exception e) {
                //dbg Logger.getLogger().write("Errrrror2 : " + e);
            }
                                        %>
                                        <TR>
                                            <TD  valign='top' width="26"><A href='main.jsp?page=switchPlanet&stype=4&switchSystem=<%=previousSystemId%>'><IMG BORDER="0" src="<%=GameConfig.picPath()%>pic/menu/leftArrow.png" width="26" height="17"></IMG></A></TD>
                                            <TD  valign='top'>
                                                <FORM style='padding:0px; margin:0px; border:0px;'  name="systemsForm">
                                                    <SELECT class="dark example" style="width:170px;" name="systems" onChange="window.location=document.systemsForm.systems.options[document.systemsForm.systems.selectedIndex].value"> <%


            // DebugBuffer.trace("JSP: Test 13 JSP");
            //dbg Logger.getLogger().write("test 5");
            for (CategoryEntity cat : categoriesOrdered) {
                //Ist das System nicht in der aktuellen Kategorie => Continue
                if (cat.getCategoryId() != actCategory) {
                    continue;
                }
                ArrayList<SystemEntity> systemsOrdered = new ArrayList<SystemEntity>();
                TreeMap<String, SystemEntity> systemTreeMap = new TreeMap<String, SystemEntity>();
                for (Map.Entry<Integer, SystemEntity> me2 : cat.getSystems().entrySet()) {
                    systemTreeMap.put(me2.getValue().getSystemName(), me2.getValue());

                }
                systemsOrdered.addAll(systemTreeMap.values());
                boolean firstSystem = true;
                for (SystemEntity sys : systemsOrdered) {
                    int tmpSystemId = sys.getSystemId();

                    String tmpSystemName = sys.getSystemName();
                    // Ist das gerade bearbeitete System das in der DropBox gew&auml;hlte wird es als Selected markiert

                    if (systemId == tmpSystemId) {
                                                        %><OPTION SELECTED  value='main.jsp?page=switchPlanet&stype=4&switchSystem=<%=tmpSystemId%>'> <%=tmpSystemName%> </OPTION> <%
                    } else {
                                                        %><OPTION  value='main.jsp?page=switchPlanet&stype=4&switchSystem=<%=tmpSystemId%>'> <%=tmpSystemName%> </OPTION>  <%
                    }
                }
            }
            
            try {%>
                                                    </SELECT>
                                                </FORM>
                                            </TD>
                                            <TD  valign='top' width="26"><A href='main.jsp?page=switchPlanet&stype=4&switchSystem=<%=nextSystemId%>'><IMG BORDER="0" src="<%=GameConfig.picPath()%>pic/menu/rightArrow.png" width="26" height="17"></IMG></A></TD>
                                        </TR>

                                        <%
            } catch (Exception e) {
                //dbg Logger.getLogger().write("Found your Error noob:"+e);
            }
            
            try {
                previousPlanetId = headerData.getCategorys().get(actCategory).getSystems().get(systemId).getPlanets().get(planetId).getPreviousPlanetId();
                nextPlanetId = headerData.getCategorys().get(actCategory).getSystems().get(systemId).getPlanets().get(planetId).getNextPlanetId();
            } catch (Exception e) {
                //dbg Logger.getLogger().write("Errrrror3 : " + e);
            }
            //Erstellen der dritten Drop Down Box mit den Planeten

                                        %>
                                        <TR>
                                            <TD  valign='top'  width="26"><A href='main.jsp?page=switchPlanet&stype=4&switchPlanet=<%=previousPlanetId%>'><IMG BORDER="0" src="<%=GameConfig.picPath()%>pic/menu/leftArrow.png" width="26" height="17"></IMG></A></TD>
                                            <TD valign='top'>
                                                <FORM style='padding:0px; margin:0px; border:0px;' name="planetsForm">
                                                    <SELECT class="dark example" style="width:170px;" name="planets"  onChange="window.location=document.planetsForm.planets.options[document.planetsForm.planets.selectedIndex].value"> <%

            boolean firstPlanet = true;
            int firstPlanetId = 0;
            //dbg Logger.getLogger().write("test 8");
            // DebugBuffer.trace("JSP: Test 15 JSP");
            
            for (CategoryEntity cat : categoriesOrdered) {

                //Ist das System nicht in der aktuellen Kategorie => Continue
                if (cat.getCategoryId() != actCategory) {
                    continue;
                }
                ArrayList<SystemEntity> systemsOrdered = new ArrayList<SystemEntity>();
                TreeMap<String, SystemEntity> systemTreeMap = new TreeMap<String, SystemEntity>();
                for (Map.Entry<Integer, SystemEntity> me2 : cat.getSystems().entrySet()) {
                    systemTreeMap.put(me2.getValue().getSystemName(), me2.getValue());

                }
                systemsOrdered.addAll(systemTreeMap.values());
                for (SystemEntity sys : systemsOrdered) {
                    //Ist der Planet nicht im aktuellen System => Continue
                    if (sys.getSystemId() != systemId) {
                        continue;
                    }
                    ArrayList<PlanetEntity> planetsOrdered = new ArrayList<PlanetEntity>();
                    TreeMap<String, PlanetEntity> planetTreeMap = new TreeMap<String, PlanetEntity>();

                    for (Map.Entry<Integer, PlanetEntity> me3 : sys.getPlanets().entrySet()) {
                        planetTreeMap.put(me3.getValue().getPlanetName(), me3.getValue());
                    }
                    planetsOrdered.addAll(planetTreeMap.values());
                    for (PlanetEntity p : planetsOrdered) {

                        if (firstPlanet) {
                            firstPlanetId = p.getPlanetId();
                            firstPlanet = false;

                        }
                        if (planetId == p.getPlanetId()) {
                                                        %><OPTION SELECTED value='main.jsp?page=switchPlanet&stype=4&switchPlanet=<%= p.getPlanetId()%>'> <%= p.getPlanetName()%> <%
                        } else {
                                                            %><OPTION value='main.jsp?page=switchPlanet&stype=4&switchPlanet=<%= p.getPlanetId()%>'> <%= p.getPlanetName()%> <%
                        }
                    }
                }
            }
            //dbg Logger.getLogger().write("test 9");
%>
                                                    </SELECT>
                                                </FORM>
                                            </TD>
                                            <TD  valign='top' width="26"><A href='main.jsp?page=switchPlanet&stype=4&switchPlanet=<%=nextPlanetId%>'><IMG BORDER="0" src="<%=GameConfig.picPath()%>pic/menu/rightArrow.png" width="26" height="17"></IMG></A></TD>
                                        </TR>
                                    </TABLE>
                                </CENTER>
                            </TD>
                            <TD width="30px"></TD>
                            <TD width='155px'>
                                <TABLE border="0" cellpadding=0 cellspacing=0 >
                                    <TR><TD colspan="6" align="center" style="height:12px"><FONT style="font-size:12px; font-weight:bolder" color="BLACK"><%= ML.getMLStr("top_lbl_coordinates", userId)%></FONT></TD></TR>
                                    <TR>
                                        <TD colspan="2" align="right" style="height:10px"><FONT style="font-size:10px; font-weight:bolder" color="BLACK"><%= ML.getMLStr("top_lbl_system", userId)%></FONT></TD>
                                        <TD style="width:7px"></TD>
                                        <TD style="width:7px"></TD>
                                        <TD colspan="2" align="left" style="height:10px"><FONT style="font-size:10px; font-weight:bolder" color="BLACK"><%= ML.getMLStr("top_lbl_planet", userId)%></FONT></TD>
                                    </TR>
                                    <TR>
                                        <TD style="height:2px" colspan="6"></TD>
                                    </TR>
                                    <TR>
                                        <TD style="width:19px;"></TD>
                                        <TD style="width:34px" align="center"><b><FONT size=2pt color='WHITE'><%=systemId%></FONT></b></TD>
                                        <TD style="width:7px"></TD>
                                        <TD style="width:7px"></TD>
                                        <TD style="width:34px" align="center"><b><FONT size=2pt color='WHITE'><%=planetId%></FONT></b></TD>
                                        <TD style="width:19px"></TD>
                                    </TR>
                                </TABLE>
                            </TD>
                        </TR>
                    </TABLE>
                </CENTER>
            </TD>
            <TD width="23"><IMG src="<%=GameConfig.picPath()%>pic/menu/rtop.png" /></TD>

            <!--  <TD style='background-image: url(pic/menu/destiny.png);  background-repeat:no-repeat'  width="*">&nbsp;</TD> -->
            <TD style='background-image: url(<%=GameConfig.picPath()%>pic/menu/headerSpacer.png)'  width="*">&nbsp;<% //dbg Logger.getLogger().write("TESTTEST"); %></TD>
                 <!--  <TD bgcolor="BLACK" style='background-image: url(<%=GameConfig.picPath()%>pic/menu/languageBackground.png);' width="137">
                <FORM name="tlanguageForm">

                    <TABLE>
                        <TR>
                            <TD height="16px">

                            </TD>
                        </TR>
                        <TR>
                            <TD width="5px">

                            </TD>
                     <TD>
                                <SELECT name="tlanguage" style='background-color: #000000; color: #ffffff; width:113px; height:17px; border:0px; text-align: center;'   onChange="window.location=document.tlanguageForm.tlanguage.options[document.tlanguageForm.tlanguage.selectedIndex].value">
                   <%
                   
                   try {
                    for(Language l : MainService.findAllLanguages()){
                        if(ML.getLocale(userId).toString().startsWith(l.getLanguage())){
                        %>
                         <OPTION SELECTED value="main.jsp?changeLanguage=<%= l.getLanguage() %>_<%= l.getCountry() %>&page=<%= request.getParameter("page")%>"><%= ML.getMLStr(l.getName(), userId) %></OPTION>

                        <%
                        }else{
                        %>
                         <OPTION value="main.jsp?changeLanguage=<%= l.getLanguage() %>_<%= l.getCountry() %>&page=<%= request.getParameter("page")%>"><%= ML.getMLStr(l.getName(), userId) %></OPTION>

                        <%
                        }
                    } 
                    } catch (Exception e) {
                        DebugBuffer.writeStackTrace("EPIC LANGUAGE ERROR", e);
                    }
                    %>
                </SELECT>
                            </TD>
                            <TD>

                            </TD>
                        </TR>
                        <TR>
                            <TD>

                            </TD>
                        </TR>
                    </TABLE>
                </FORM>
            </TD>-->

                   
            <TD align="CENTER" style='background-image: url(pic/menu/headerSpacer.png)'  width="140">
                <TABLE>
                    <TR>
                         <TD align="center">
                        <FONT size="1"><%= ML.getMLStr("top_lbl_usersonline", userId).replace("%COUNT%", String.valueOf(GameUtilities.getUsersOnlineSince(2))) %></FONT>
                    </TD>
                    </TR>
                    <TR>
                         <TD align="center">
                            <% if(!u.isGuest()) {
                            for(Language l : MainService.findAllLanguages()){
                                
                                if(l.getId() != l.getMasterLanguageId()){
                                    continue;
                                }
                                //Language is masterlanguage => check if user has an other country with this language as masterlanguage
                                if(l.getId() == l.getMasterLanguageId()){
                                    Language userLanguage = Service.languageDAO.findBy(Service.userDAO.findById(userId).getLocale());
                                    if(userLanguage.getId() != l.getId() && userLanguage.getMasterLanguageId() == l.getId()){
                                        l = userLanguage;
                                    }
                                }
                            %>
                          <A href='main.jsp?changeLanguage=<%= l.getLanguage() %>_<%= l.getCountry() %>&page=<%= request.getParameter("page")%>'><IMG border="0" alt="Deutsch" src="pic/icons/<%= l.getPic() %>"></A>
                            &nbsp;&nbsp;
                            <% } %>
                            <% } else { %>
                            
                            <IMG border="0" alt="Deutsch" src="pic/icons/german.png">&nbsp;&nbsp;
                            <IMG border="0" alt="English" src="pic/icons/english.png">
                            <% } %>
                        </TD>
                    </TR>
                </TABLE>
        </TR>

    </TABLE>
</CENTER>
