<%-- 
    Document   : pDefJSON
    Created on : 17.03.2009, 22:37:58
    Author     : Stefan
--%>

<%@ page import="at.viswars.json.*" %>
<%@ page import="java.util.*" %>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int sysId = Integer.parseInt(request.getParameter("sId"));
int pId = Integer.parseInt(request.getParameter("pId"));
response.setCharacterEncoding("ISO-8859-1");
out.write(JSONTestSource.getDefenseInformation(sysId,pId,userId));
%>