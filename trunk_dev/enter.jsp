<%@page import="at.viswars.text.FormatTime"%>
<%@page import="at.viswars.enumeration.EGameDataStatus"%>
<%@page import="at.viswars.service.Service"%>
<%@page import="at.viswars.utilities.TitleUtilities"%>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.viswars.GameConstants" %>
<%@page import="at.viswars.ML" %>
<%@page import="at.viswars.DebugBuffer" %>
<%@page import="at.viswars.DebugBuffer.DebugLevel" %>
<%@page import="at.viswars.databuffer.*" %>
<%@page import="at.viswars.database.access.DbConnect" %>
<%@page import="at.viswars.service.LoginService"%>
<%@page import="at.viswars.service.MainService"%>
<%@page import="at.viswars.model.*"%>
<%@page import="at.viswars.GameConfig"%>
<%@page import="at.viswars.GameUtilities"%>
<%@page import="at.viswars.utilities.MD5"%>
<%@page import="at.viswars.Logger.*"%>

<%
    final int max_login_min = 5;
    boolean multi = false;
    Locale locale = request.getLocale();
    int userId = 0;
    String userName = "";
    String password = "";
    String portalPassword = "";

    if (request.getParameter("user") != null) {
        userName = request.getParameter("user");
    }
    if (request.getParameter("portalPassword") != null) {
        portalPassword = request.getParameter("portalPassword");
    }
    if (request.getParameter("password") != null) {
        password = MD5.encryptPassword(request.getParameter("password"));
    }
    GameData gd = Service.gameDataDAO.findById(1);

    session = request.getSession();
    //If not coming from startsystem selection start a new session
    if (session.getAttribute("visJoinMap") == null) {
        session = request.getSession(true);
    } else {
        //Load values from session
        session.removeAttribute("visJoinMap");
        userId = Integer.parseInt((String) session.getAttribute("userId"));
        userName = Service.userDAO.findById(userId).getUserName();
        password = (String) session.getAttribute("password");
        session.removeAttribute("password");
        java.lang.System.out.println("logging in with userName " + userName + " password : " + password);
    }
    try {
        if (!userName.equals("")) {
            User uTmp = LoginService.findUserBy(userName);
            User sitterAcc = null;

            // Check if account is sitted and prepare sitter login
            if (uTmp != null) {
                UserSettings us = Service.userSettingsDAO.findByUserId(uTmp.getUserId());
                if (us != null) {
                    if (us.getSittedById() != 0) {
                        sitterAcc = LoginService.findUserById(us.getSittedById());
                    }
                }
            }

            User u = LoginService.findUserBy(userName, password);

            if (u == null && !portalPassword.equals("")) {
                u = LoginService.findPortalUser(userName, portalPassword);
            }
            // Check login with sitter password
            if ((u == null) && (sitterAcc != null)) {
                User sitter = LoginService.findUserBy(sitterAcc.getUserName(), password);
                if (sitter != null) {
                    u = LoginService.findUserBy(userName, uTmp.getPassword());
                }
            }

            if (u != null) {
                userId = u.getUserId();

                LoginTracker tmp = LoginService.findLatestLoginTrackerByIp(request.getRemoteAddr());
                if (tmp == null) {
                    tmp = new LoginTracker();
                    tmp.setUserId(userId);
                    tmp.setTime(java.lang.System.currentTimeMillis());
                }
                int minutes = max_login_min;
                int seconds = minutes * 60;
                int milliSeconds = seconds * 1000;

                // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Millis : " + (java.lang.System.currentTimeMillis() - tmp.getTime()));
                if (u != null && (gd.getStatus().equals(EGameDataStatus.RUNNING) || gd.getStatus().equals(EGameDataStatus.RUNNING_NOUPDATE) || u.getAdmin())) {
                    if (u.getMulti() || u.getLocked() > 0) {
                        out.write("<HTML>");
                        out.write("<BODY text=\"#FFFF00\" background=\"" + GameConfig.picPath() + "pic/sh.jpg\">");
                        out.write("<CENTER>");
                        out.write("<TABLE width=\"80%\" bgcolor=\"darkred\"><TR align=\"center\"><TD>");
                        if (u.getMulti()) {
                            out.write("<BR><FONT size=\"+2\"><B>" + ML.getMLStr("login_err_multi", userId) + "</B></FONT><BR><BR>");
                            out.write("\n");
                            out.write("<IMG src=\"http://www.thedarkdestiny.at/multiaccount.jpg\" />");
                            out.write("<BR>" + ML.getMLStr("login_err_locked_multi", userId));
                        } else {
                            out.write("<BR><FONT size=\"+2\"><B>" + ML.getMLStr("login_err_notspec", userId) + "</B></FONT><BR><BR>");
                            out.write("\n");
                        }

                        if (u.getLocked() > 0) {
                            String date = FormatTime.formatEndTimeofTimeSpanAsDate(u.getLocked());
                            out.write("<B>" + ML.getMLStr("login_err_locked", userId).replace("%TIME%", date) + "</B><BR><BR>");

                        }

                        out.write("</TD></TR></TABLE>");
                        out.write("</CENTER>");
                        out.write("</BODY>");
                        out.write("</HTML>");
                    } else if (u.getDeleteDate() > 0) {
                        int deleteDate = u.getDeleteDate();
                        int finalDeletion = (u.getDeleteDate() + GameConstants.DELETION_DELAY) - GameUtilities.getCurrentTick2();

                        int gracePeriod = u.getDeleteDate() + (int) (GameConstants.DELETION_DELAY / 2d);
                        int graceLeft = gracePeriod - GameUtilities.getCurrentTick2();

                        out.write("<HTML>");
                        out.write("<BODY text=\"#FFFF00\" background=\"" + GameConfig.picPath() + "pic/sh.jpg\">");
                        out.write("<CENTER>");

                        if (graceLeft > 0) {
%>
<TABLE width="80%" bgcolor="darkred" >
    <TR align="center">
        <TD align="middle" colspan=2>
            <B><BR>You marked your account for deletion, you may still revoke that state for <%= graceLeft%> ticks, if you decide to revoke your deletion state,<BR>
                you will be able to enter the game again in <%= graceLeft%> ticks, otherwise deletion will be locked in and your account is deleted in <%= finalDeletion%> ticks.<BR><BR>
                <INPUT type="button" value="Löschung zurücknehmen" onclick="window.location.href = '<%= GameConfig.getHostURL()%>/RevokeDeletion?uId=<%= u.getUserId()%>&pw=<%= password%>'">
                <BR><BR></B>
        </TD>
    </TR>
</TABLE>
<%
} else {
%>
<TABLE width="80%" bgcolor="darkred" >
    <TR align="center">
        <TD align="middle" colspan=2>
            <B><BR>You did not revoke your deletion and your account will be removed in <%= finalDeletion%> ticks.<BR><BR></B>
        </TD>
    </TR>
</TABLE>
<%
    }

    out.write("</CENTER>");
    out.write("</BODY>");
    out.write("</HTML>");
} else {
    try {
        // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Set session");
        Locale l = null;
        Integer languageId = -1;
        if (session.getAttribute("Language") != null) {
            l = (Locale) session.getAttribute("Language");
        }
        if (session.getAttribute("languageId") != null) {
            languageId = Integer.parseInt((String) session.getAttribute("languageId"));
        }
        if (l != null) {
            session.setAttribute("Language", l);
        }
        if (languageId >= 0) {
            session.setAttribute("languageId", languageId);
        }

        //SaveLogin
        session.setMaxInactiveInterval(GameConstants.INVALIDATE_USER_AFTER);
        session.setAttribute("userId", "" + u.getUserId());
    } catch (Exception e) {
        Logger.getLogger().write("WTF :  " + e);
    }
    if (!u.getSystemAssigned()) {
        session.setAttribute("password", password);
        // DebugBuffer.addLine(DebugLevel.TRACE, "Forwarding join Map (" + u.getUserId() + ")");
%>
<jsp:forward page="visjoinmap.jsp">
    <jsp:param name="userId" value="<%= u.getUserId()%>" />
</jsp:forward>
<%
// IP Protection
            }
            LoginService.checkForMulti(u.getUserId(), request.getRemoteAddr());
            /*
             //Login
             }*/// else {
            // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Process default login");
            LoginTracker loginTracker = new LoginTracker();
            loginTracker.setUserId(u.getUserId());
            loginTracker.setTime(java.lang.System.currentTimeMillis());
            loginTracker.setLoginIP(request.getRemoteAddr());

            // DebugBuffer.addLine(DebugLevel.UNKNOWN,"1");

            LoginService.saveLoginTracker(loginTracker);
            int homePlanetId = LoginService.findHomePlanetByUserId(u.getUserId());
            int systemId = 0;
            if (homePlanetId > 0) {
                PlayerPlanet pp = LoginService.findPlayerPlanetByPlanetId(homePlanetId);
                systemId = LoginService.findPlanetById(pp.getPlanetId()).getSystemId();
            }
            // DebugBuffer.addLine(DebugLevel.UNKNOWN,"2");

            session.setAttribute("actPlanet", "" + homePlanetId);
            session.setAttribute("actSystem", "" + systemId);


            LoginService.setLastUpdateForUserId(GameUtilities.getCurrentTick2(), userId);
            // DebugBuffer.addLine(DebugLevel.DEBUG,"user : " + userId + " logging in => titlecheck");
            TitleUtilities.updateTitles();
            UserData userData = LoginService.findUserDataByUserId(userId);

            // DebugBuffer.warning("[ENTER]: " + request.getRequestedSessionId());
            // DebugBuffer.warning("[ENTER] SessionID: " + session.getId());
            request.setAttribute("JSESSIONID", session.getId());

            UserSettings us = Service.userSettingsDAO.findByUserId(userId);

            if (us.getShowWelcomeSite()) {

                // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Go welcome");
                String redirectURL = "main.jsp?page=welcome";
                String urlWithSessionID = response.encodeRedirectURL(redirectURL.toString());
                response.sendRedirect(urlWithSessionID);
                return;
                // <jsp:forward page="main.jsp?page=welcome" />
            } else {
                // DebugBuffer.addLine(DebugLevel.UNKNOWN,"Go overview");
                String redirectURL = "main.jsp?page=new/overview";
                String urlWithSessionID = response.encodeRedirectURL(redirectURL.toString());
                response.sendRedirect(urlWithSessionID);
                return;
                // <jsp:forward page="main.jsp?page=new/overview" />
            }
            // }
        }
// userId > 0 but u = null
    } else if (u != null && gd.getStatus().equals(EGameDataStatus.REGISTRATION) && !u.getAdmin()) {
        out.write("<HTML>");
        out.write("<BODY text=\"#FFFF00\" background=\"" + GameConfig.picPath() + "pic/sh.jpg\">");
        out.write("<CENTER>");
        out.write("<FONT size=\"+1\"><B>" + "Registration Phase: You are registered" + "</B></FONT><BR><BR>");
        out.write("<FONT size=\"+1\"><B>" + "Players registered: " + Service.userDAO.findAll().size() + "</B></FONT><BR><BR>");
        out.write("\n");
        out.write("</CENTER>");
        out.write("</BODY>");
        out.write("</HTML>");
    } else if (gd.getStatus().equals(EGameDataStatus.STOPPED)) {
        out.write("<HTML>");
        out.write("<BODY text=\"#FFFF00\" background=\"" + GameConfig.picPath() + "pic/sh.jpg\">");
        out.write("<CENTER>");
        out.write("<FONT size=\"+1\"><B>" + "Server Stopped" + "</B></FONT><BR><BR>");
        out.write("\n");
        out.write("</CENTER>");
        out.write("</BODY>");
        out.write("</HTML>");
    } else {
        DebugBuffer.addLine(DebugBuffer.DebugLevel.DEBUG, "USER NOT FOUND");
    }
// userId = 0
} else {
    DebugBuffer.addLine(DebugBuffer.DebugLevel.DEBUG, "USER NOT FOUND");
%>
<jsp:forward page="login.jsp" >
    <jsp:param name="errmsg" value='<%= ML.getMLStr("login_err_invalidlogin", locale)%>' />
</jsp:forward>
<%
    }
} else {
%>
<jsp:forward page="login.jsp" >
    <jsp:param name="errmsg" value='<%= ML.getMLStr("login_err_invalidlogin", locale)%>' />
</jsp:forward>
<%            }
} catch (Exception e) {
    DebugBuffer.writeStackTrace("ERROR ON LOGON: ", e);

    request.setAttribute("password", "XXXX");

    session.setAttribute("errParams", request.getParameterMap());
    session.setAttribute("errException", e);
%>
<jsp:forward page="new/error.jsp" />
<%
    }



%>