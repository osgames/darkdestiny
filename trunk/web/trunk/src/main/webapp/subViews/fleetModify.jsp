<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.buildable.*" %>
<%@page import="at.darkdestiny.core.ships.*" %>
<%@page import="at.darkdestiny.core.model.ShipDesign" %>
<%@page import="at.darkdestiny.core.movable.PlayerFleetExt" %>
<%@page import="at.darkdestiny.core.service.Service" %>
<%@page import="at.darkdestiny.core.service.FleetService" %>
<%@page import="at.darkdestiny.core.service.ShipDesignService" %>
<%@page import="at.darkdestiny.core.view.ShipModifyView" %>
<%@page import="at.darkdestiny.core.view.ShipModifyEntry" %>
<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%@page import="at.darkdestiny.core.utilities.ShipUtilities" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.enumeration.EDamageLevel" %>

<%
final int FLEET_ACTION_MODIFY = 15;
final int FLEET_ACTION_SCRAP = 16;
final int FLEET_ACTION_UPGRADE = 17;
final int FLEET_ACTION_REPAIR = 18;

int userId = Integer.parseInt((String)session.getAttribute("userId"));
int action = Integer.parseInt(request.getParameter("fmaction"));

if ((action == FLEET_ACTION_SCRAP) || (action == FLEET_ACTION_UPGRADE) ||
    (action == FLEET_ACTION_REPAIR)) {
    if (request.getParameter("bufId") != null) {
        ParameterBuffer pb = BufferHandling.getBuffer(userId, Integer.parseInt(request.getParameter("bufId")));        
        
        if (action == FLEET_ACTION_SCRAP) {
            pb.setParameter(ShipScrapBuffer.COUNT, request.getParameter("destroyCount"));
        } else if (action == FLEET_ACTION_UPGRADE) {
            pb.setParameter(UpgradeShipBuffer.TO_DESIGN_ID, request.getParameter("toDesign"));
            pb.setParameter(UpgradeShipBuffer.COUNT, request.getParameter("upgradeCount"));
        } else if (action == FLEET_ACTION_REPAIR) {
            FleetService.addRepairShipsToBuffer(request.getParameterMap(), (RepairShipBuffer)pb);
        }
%>
            <jsp:forward page="../main.jsp" >
                <jsp:param name="bufId" value='<%= pb.getId() %>' />
                <jsp:param name="page" value='confirm/shipModification' />
            </jsp:forward>
<%        
    }
}

int fleetId = Integer.parseInt(request.getParameter("fleetId"));
int designId = 0;

boolean showList = (action == FLEET_ACTION_MODIFY);

ShipModifyView smv = null;
if (showList) smv = FleetService.getModifiableShips(fleetId, userId);

PlayerFleetExt pfe = FleetService.getFleet(fleetId);

ShipDesignExt sde = null;
ShipData sData = null;

int currDesignCount = 0;
if ((action == FLEET_ACTION_SCRAP) || (action == FLEET_ACTION_UPGRADE)) {
    designId = Integer.parseInt(request.getParameter("dId"));
    smv = ShipUtilities.getModifiableShips(fleetId,designId);
    sde = new ShipDesignExt(designId);
    if (action == FLEET_ACTION_SCRAP) {
        currDesignCount = (smv.getShipList().get(0)).getCount();
    } else {
        currDesignCount = (smv.getShipList().get(0)).getCount() - (smv.getShipList().get(0)).getDamagedCount();
    }
    
    /*
    for (ShipData sd : pfe.getShipList()) {
        if (sd.getDesignId() == designId) {
            currDesignCount = sd.getCount();
            break;
        }
    }
    */
} else if (action == FLEET_ACTION_REPAIR) {
    int shipFleetId = Integer.parseInt((String)request.getParameter("sfId"));    
    sData = new ShipData(shipFleetId);
    sde = new ShipDesignExt(sData.getDesignId());
    designId = sData.getDesignId();
    fleetId = sData.getFleetId();
}

ParameterBuffer pb = null;
if ((action == FLEET_ACTION_SCRAP) || (action == FLEET_ACTION_UPGRADE) || 
    (action == FLEET_ACTION_REPAIR)) {
    if (request.getParameter("bufId") == null) {
        if (action == FLEET_ACTION_SCRAP) {
            pb = new ShipScrapBuffer(userId);
            pb.setParameter(ShipScrapBuffer.FLEET_ID, fleetId);
            pb.setParameter(ShipScrapBuffer.DESIGN_ID, designId);            
        } else if (action == FLEET_ACTION_UPGRADE) {
            pb = new UpgradeShipBuffer(userId);
            pb.setParameter(UpgradeShipBuffer.FLEET_ID, fleetId);
            pb.setParameter(UpgradeShipBuffer.DESIGN_ID, designId);
        } else if (action == FLEET_ACTION_REPAIR) {
            pb = new RepairShipBuffer(userId);
            pb.setParameter(RepairShipBuffer.FLEET_ID, fleetId);
            pb.setParameter(RepairShipBuffer.DESIGN_ID, designId);  
            pb.setParameter(RepairShipBuffer.SHIPFLEET_ID,sData.getId());                               
        }
    }
}

if (showList) {
%>
<CENTER><B>Flotte <%= pfe.getName()%> bearbeiten</B></CENTER><BR>
<TABLE class="blue" width="60%">
    <TR>
        <TD class="blue"><B>Designname</B></TD>
        <TD class="blue"><B>Anzahl</B></TD>
        <TD class="blue"><B>Aktionen</B></TD>
    </TR>
<%
    for (ShipModifyEntry sme : smv.getShipList()) {        
%>
    <TR>
        <TD><%= sme.getDesignName() %></TD>
        <TD><%= sme.getCount() %></TD>
        <TD>
            <TABLE cellspacing=0 cellpadding=0><TR>
<%
if (sme.isBlockedByLoading()) { %>
    <TD><IMG alt="Schiffe umr&uuml;sten" src="<%= GameConfig.getInstance().picPath() %>pic/nomodify.jpg" onmouseover="doTooltip(event,'<%= ML.getMLStr("fleetmanagement_pop_nomodifyfleet", userId) %>')" onmouseout="hideTip()"/></TD>
<%
} else { %>
<% if (sme.isCanBeModified()) { %><TD><IMG style="cursor:pointer;" alt="Schiffe umr&uuml;sten" src="<%= GameConfig.getInstance().picPath() %>pic/icons/upgrade.png" onmouseover="doTooltip(event,'Schiffe umr&uuml;sten')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=1&fleetId=<%= fleetId %>&dId=<%= sme.getDesignId() %>&fmaction=<%= FLEET_ACTION_UPGRADE %>'"/></TD><% } %>
<% if (sme.isCanBeRepaired()) { %><TD><IMG style="cursor:pointer;" alt="Schiffe reparieren" src="<%= GameConfig.getInstance().picPath() %>pic/modify.jpg" onmouseover="doTooltip(event,'Schiffe reparieren')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=1&sfId=<%= sme.getId() %>&fmaction=<%= FLEET_ACTION_REPAIR %>'"/></TD><% } %>
<% if (sme.isCanBeScrapped()) { %><TD><IMG style="cursor:pointer;" alt="Schiffe verschrotten" src="<%= GameConfig.getInstance().picPath() %>pic/cancel.jpg" onmouseover="doTooltip(event,'Schiffe verschrotten')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=1&fleetId=<%= fleetId %>&dId=<%= sme.getDesignId() %>&fmaction=<%= FLEET_ACTION_SCRAP %>'"/></TD><% } %>
<% } %>
            </TR></TABLE>
        </TD>
    </TR>    
<%
    }
%>
</TABLE>
<BR/>
<%
} else if (action == FLEET_ACTION_SCRAP) {
        ShipScrapCost ssc = sde.getShipScrapCost();
%>
        <script language="Javascript">
<%
        for (RessAmountEntry rae : ssc.getRessArray()) {
            out.write("var "+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()
                    +"Per = " + rae.getQty() + ";");
        }
%>        
        var maxCount = <%= currDesignCount %>;

        var actCount = 1;

<%
        out.write("var totalCredit = creditPer;");
        for (RessAmountEntry rae : ssc.getRessArray()) {
            out.write("var total" + Service.ressourceDAO.findRessourceById(rae.getRessId()).getName() + " = " + Service.ressourceDAO.findRessourceById(rae.getRessId()).getName() + "Per;");
        }
%>        
        function calcTotal() {
<%            
        out.write("totalCredit = actCount * "+Service.ressourceDAO.findRessourceById(at.darkdestiny.core.model.Ressource.CREDITS).getName()+"Per;");

        for (RessAmountEntry rae : ssc.getRessArray()) {
                out.write("total" + Service.ressourceDAO.findRessourceById(rae.getRessId()).getName() + " = actCount * " + Service.ressourceDAO.findRessourceById(rae.getRessId()).getName() + "Per;");
            }            
%>            

            document.getElementById("credit").innerHTML = number_format(totalCredit,0) + ' Credits';
<%
        for (RessAmountEntry rae : ssc.getRessArray()) {
                if (rae.getQty() != 0) {
                    out.write("document.getElementById(\""+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()+"\").innerHTML = number_format(total"+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()+",0);");
                }              
            }        
%>
        }
    </script>
    <FORM method='post' action='main.jsp?page=new/fleet&type=1&fmaction=<%= FLEET_ACTION_SCRAP %>&bufId=<%= pb.getId() %>' name='confirm' >
    <TABLE width="80%" style="font-size:13px">
    	<TR>
    		<TD colspan=2 align="center">
    			Bitte Anzahl der zu verschrottenden Schiffe eingeben<BR><BR>
    		</TD>
    	</TR>
    	<TR valign="middle">
    		<TD width="200">Schiffsname:</TD>
    		<TD><%= ((ShipDesign)sde.getBase()).getName() %></TD>
    	</TR>
    	<TR valign="middle">
	    	<TD width="200">Typ:</TD>
	    	<TD><%= ML.getMLStr(ShipDesignService.getChassis(((ShipDesign)sde.getBase()).getChassis()).getName(),userId) %></TD>
	    </TR>
    	<TR valign="middle">
    		<TD>Planet:</TD>
    		<TD><%= pfe.getRelativeCoordinate().getPlanetId() %></TD>
    	</TR>
    	<TR valign="middle">
    		<TD>Anzahl:</TD>
    		<TD><INPUT  name="destroyCount"
    					Id="destroyCount" 
    					value="1" 
    					SIZE="3" 
    					MAXLENGTH="6" 
    					ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();"> / <%= currDesignCount %>
    			<BR>
    		</TD>
    	</TR>
    	<TR valign="top">
    		<TD>Kosten:</TD>
                <TD><SPAN id="credit"> <%= FormatUtilities.getFormattedNumber(ssc.getRess(at.darkdestiny.core.model.Ressource.CREDITS)) %></SPAN>
    			<BR>
    		</TD>
    	</TR>
        <TR>
            <TD align="center" colspan=2 class="blue2"><B>Ressourcenr&uuml;ckverg&uuml;tung</B></TD>
        </TR>
        <TR>
            <TD class="blue2"><B>Ressource</B></TD>
            <TD class="blue2"><B>Menge</B></TD>                
        </TR>
<%
        for (RessAmountEntry rae : ssc.getRessArray()) {
            if (rae.getQty() != 0) {
                if(rae.getRessId() == at.darkdestiny.core.model.Ressource.CREDITS){
                        continue;
                    }
                out.write("<TR><TD>"+ML.getMLStr(Service.ressourceDAO.findRessourceById(rae.getRessId()).getName(),userId)+"</TD><TD><SPAN id=\""+Service.ressourceDAO.findRessourceById(rae.getRessId()).getName()+"\">" + FormatUtilities.getFormattedNumber(rae.getQty()) + "</SPAN></TD></TR>");
            }
        }      
%>
    <TR><TD colspan=2 align="CENTER"><BR><INPUT type="submit" name="Ok" value="Ok"></TD></TR>
    </TABLE><BR><BR>                   
    </FORM>    
<%
} else if (action == FLEET_ACTION_UPGRADE) {
%>
<CENTER>
    <FORM method='post' action='main.jsp?page=new/fleet&type=1&fmaction=<%= FLEET_ACTION_UPGRADE %>&bufId=<%= pb.getId() %>' name='confirm' >
    <TABLE width="80%" style="font-size:13px">
    	<TR>
    		<TD colspan=2 align="center">Bitte das Design w&auml;hlen in das das aktuelle Schiff ge&auml;ndert werden soll und die Anzahl der zu &auml;ndernden Schiff eingeben.
    			<BR>
    			<BR>
    		</TD>
    	</TR>
    	<TR valign="middle">
	    	<TD width="200">Schiffsname:</TD>
    		<TD><%= ((ShipDesign)sde.getBase()).getName() %></TD>
    	</TR>
    	<TR valign="middle">
	    	<TD width="200">Typ:</TD>
                <TD><%= ML.getMLStr(ShipDesignService.getChassis(((ShipDesign)sde.getBase()).getChassis()).getName(),userId) %></TD>
    	</TR>
    	<TR valign="middle">
    		<TD>Planet:</TD>
    		<TD><%= pfe.getRelativeCoordinate().getPlanetId() %></TD>
    	</TR>
    	<TR valign="middle">
    		<TD>Anzahl:</TD>
    		<TD><INPUT name="upgradeCount" 
    					id="ugradeCount" 
    					value="1" 
    					SIZE="3" 
    					MAXLENGTH="6" 
    					ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();" /> / <%= currDesignCount %>
    					<BR>
    		</TD>
    	</TR>
    	<TR valign="top">
    		<TD>Upgraden zu Design:</TD>
    		<TD>
    			<SELECT id="toDesign" name="toDesign">
        			<OPTION value=0>-- Design w&auml;hlen --</OPTION><%
                                ArrayList<ShipDesign> sdList = FleetService.getPossibleRefitShips((ShipDesign)sde.getBase(), userId);
        			for (ShipDesign sd : sdList) { 
						%><OPTION value="<%= sd.getId() %>"><%= sd.getName() %></OPTION><%
        			}
				%></SELECT>
    		</TD>
    	</TR>
    	<TR>
    		<TD colspan=2 align="CENTER">
    			<BR><INPUT type="submit" name="Ok" value="Ok" ONCLICK ='if (document.getElementById("toDesign").value == 0) { alert("Neues Design muss angegeben werden!"); return false; }'>
    		</TD>
    	</TR>
    </TABLE><BR><BR>                   
    </FORM>      
</CENTER>
<%
} else if (action == FLEET_ACTION_REPAIR) {    
    ShipRepairCost ssc = sde.getShipRepairCost();
%>
    <FORM method='post' action='main.jsp?page=new/fleet&type=1&fmaction=<%= FLEET_ACTION_REPAIR %>&bufId=<%= pb.getId() %>' name='confirm' >
    <TABLE width="80%" style="font-size:13px">
    	<TR>
    		<TD align="center">
                        Beschädigte Schiffe vom Typ <%= sData.getDesignName() %> reparieren.<BR>
    			Bitte Anzahl der Schiffe eingeben, die repariert werden sollen:<BR><BR>
    		</TD>
    	</TR>
        <TR><TD align="middle">
            <TABLE class="blue"><TR align="middle">
                    <TD width="150" class="blue"><B>Beschädigung</B></TD><TD width="80" class="blue"><B>Verfg.</B></TD><TD width="100" class="blue"><B>Anzahl</B></TD>
            </TR>
<%
        for (ShipStatusEntry sse : sData.getShipDataStatus()) {          
            if (sse.getCount() == 0) continue;
            if (sse.getStatus() == EDamageLevel.NODAMAGE) continue;
%>
            <TR align="middle">
                <TD><%= ML.getMLStr(sse.getStatus().toString(), userId) %></TD><TD><%= sse.getCount()%></TD><TD><INPUT type="text" SIZE="4" name="count_<%= sse.getStatus().toString() %>" value="0"></TD>
            </TR>
<%
        }
%>       
        </TABLE>    
    </TD></TR>
    <TR><TD align="CENTER"><BR><INPUT type="submit" name="Ok" value="Ok"></TD></TR>
    </TABLE>                 
    </FORM>   
<%                  
}
%>