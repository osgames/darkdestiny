<%@page import="at.darkdestiny.core.requestbuffer.BufferHandling"%>
<%@page import="at.darkdestiny.core.requestbuffer.ParameterBuffer"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.text.*" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.result.BaseResult" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.hangar.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.utilities.*" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="at.darkdestiny.core.utilities.MilitaryUtilities" %>
<%@page import="at.darkdestiny.core.movable.*" %>
<%@page import="at.darkdestiny.core.view.*" %>
<%@page import="at.darkdestiny.core.result.FleetListResult" %>
<%@page import="at.darkdestiny.core.result.FleetListResult.ESortType" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.fleet.*" %>

<%
    final int FLEET_ACTION_SHOW_DETAIL = 1;
    final int FLEET_ACTION_RENAME = 2;
    final int FLEET_ACTION_REGROUP = 3;
    final int FLEET_ACTION_SEND = 4;
    final int FLEET_ACTION_COLONIZE = 5;
    final int FLEET_ACTION_SCAN_PANET = 6;
    final int FLEET_ACTION_SCAN_SYSTEM = 69;
    final int FLEET_ACTION_ABORT_SCAN = 68;
    final int FLEET_ACTION_SET_HIDE = 7;
    final int FLEET_ACTION_UNSET_HIDE = 8;
    final int FLEET_ACTION_REROUTE = 9;
    final int FLEET_ACTION_CALLBACK = 10;
    final int FLEET_ACTION_ENTER_RETREAT = 11;
    final int FLEET_ACTION_SET_RETREAT = 12;
    final int FLEET_ACTION_DO_FORMATION = 13;
    final int FLEET_ACTION_UNDO_FORMATION = 14;
    final int FLEET_ACTION_MODIFY = 15;
    final int FLEET_ACTION_SCRAP = 16;
    final int FLEET_ACTION_UPGRADE = 17;
    final int FLEET_ACTION_VIEW_FLEET = 20;
    final int FLEET_ACTION_LOAD_RESS = 21;
    final int FLEET_ACTION_UNLOAD_RESS = 22;
    final int FLEET_ACTION_LOAD_TROOPS = 23;
    final int FLEET_ACTION_UNLOAD_TROOPS = 24;
    final int FLEET_ACTION_LOAD_POPULATION = 25;
    final int FLEET_ACTION_UNLOAD_POPULATION = 26;
    final int FLEET_ACTION_DO_HANGAR = 27;
    final int FLEET_ACTION_ATTACK = 28;
    final int FLEET_ACTION_ATTACK_NOW = 29;
    final int FLEET_ACTION_SET_SORTTYPE = 30;
    final int FLEET_ACTION_CREATEEMBASSY = 31;
    final int FLEET_ACTION_ATTACH_STATION = 40;
    final int FLEET_ACTION_DETACH_STATION = 41;
    final int FLEET_ACTION_LOAD_SCRAP= 110;
    final int OWN = 0;
    final int ALLIED = 1;
    final int ENEMY = 2;

    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
    int systemId = Integer.parseInt((String) session.getAttribute("actSystem"));

    ParameterBuffer pb = null;
    if (request.getParameter("respObj") != null) {
        pb = BufferHandling.getBuffer(userId, Integer.parseInt(request.getParameter("respObj")));
    }

    String viewType = null;

    if (request.getParameter("viewType") != null) {
        viewType = request.getParameter("viewType");
    }
    ESortType sort = ESortType.ID;
    if (request.getParameter("sortType") != null) {
        sort = ESortType.valueOf(request.getParameter("sortType"));
    }
    FleetListResult flr = null;
    String tableHeading = "";
    String tableColor = "blue";
    EFleetViewType parType = EFleetViewType.valueOf(viewType);

    int typeForward = 1;
    boolean transView = false;

    if (parType == EFleetViewType.OWN_SYSTEM) {
        flr = FleetService.getFleetList(userId, systemId, planetId, EFleetViewType.OWN_SYSTEM);
        tableHeading = ML.getMLStr("fleetmanagement_lbl_localfleets", userId);
    } else if (parType == EFleetViewType.ALL_SYSTEM_ALLIED) {
        flr = FleetService.getFleetList(userId, systemId, planetId, EFleetViewType.ALL_SYSTEM_ALLIED);
        tableHeading = ML.getMLStr("fleetmanagement_lbl_alliedfleetsinsystem", userId);
        tableColor = "violet";
    } else if (parType == EFleetViewType.ALL_SYSTEM_ENEMY) {
        flr = FleetService.getFleetList(userId, systemId, planetId, EFleetViewType.ALL_SYSTEM_ENEMY);
        tableHeading = ML.getMLStr("fleetmanagement_lbl_otherfleetsinothersystems", userId);;
        tableColor = "red";
    } else if (parType == EFleetViewType.OWN_NOT_IN_SYSTEM) {
        flr = FleetService.getFleetList(userId, systemId, planetId, EFleetViewType.OWN_NOT_IN_SYSTEM);
        tableHeading = ML.getMLStr("fleetmanagement_lbl_fleetsinothersystems", userId);
    } else if (parType == EFleetViewType.OWN_ALL_WITH_ACTION) {
        flr = FleetService.getFleetList(userId, systemId, planetId, EFleetViewType.OWN_ALL_WITH_ACTION);
        tableHeading = ML.getMLStr("fleetmanagement_lbl_fleetsontheway", userId);
    } else if (parType == EFleetViewType.ALL_SYSTEM_OTHER_USERS) {
        flr = FleetService.getFleetList(userId, systemId, planetId, EFleetViewType.ALL_SYSTEM_OTHER_USERS);
    } else if (parType == EFleetViewType.OWN_ALL_TRANSPORT_SYSTEM) {
        flr = FleetService.getFleetList(userId, systemId, planetId, parType);
        tableHeading = ML.getMLStr("fleetmanagement_lbl_localfleets", userId);
        transView = true;
    } else if (parType == EFleetViewType.OWN_ALL_TRANSPORT_NOT_IN_SYSTEM) {
        flr = FleetService.getFleetList(userId, systemId, planetId, parType);
        tableHeading = ML.getMLStr("fleetmanagement_lbl_fleetsinothersystems", userId);
        transView = true;
    } else if (parType == EFleetViewType.ALLIED_NOT_IN_SYSTEM) {
        flr = FleetService.getFleetList(userId, systemId, planetId, parType);
        tableHeading = "Allierte Flotten in anderen Systemen";
        tableColor = "violet";
    } else if (parType == EFleetViewType.ENEMY_NOT_IN_SYSTEM) {
        flr = FleetService.getFleetList(userId, systemId, planetId, parType);
        tableHeading = "Andere Flotten in anderen Systemen";
        tableColor = "red";
    }

    if (transView) {
        typeForward = 2;
    }

    ArrayList<FleetFormationExt> formations = flr.getFleetFormations();
    ArrayList<PlayerFleetExt> singleFleets = flr.getSingleFleetsSorted(sort, systemId);

    if (!flr.getAllContainedFleets().isEmpty()) {
        if (pb != null) {
            pb.setParameter("found", true);
        }
    }

    boolean entryFound = false;

    if ((formations.size() > 0) || (singleFleets.size() > 0)) { // DO HEADER
        entryFound = true;
%>
<BR>
<TABLE class="<%= tableColor%>" width="90%">
    <TR><TD class="<%= tableColor%>" align="center"><B><%= tableHeading%></B></TD></TR>
</TABLE>
<%
    if (parType != EFleetViewType.OWN_ALL_WITH_ACTION) {
%>
<TABLE class="<%= tableColor%>" cellpadding=0 width="90%">
    <TR>
        <TD class="<%= tableColor%>" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_name", userId)%></B></TD>
        <TD class="<%= tableColor%>" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_location", userId)%></B></TD>
        <TD class="<%= tableColor%>" align="center">&nbsp;</TD>
        <TD class="<%= tableColor%>" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_actions", userId)%></B></TD>
    </TR>
    <%
    } else {
    %>
    <TABLE width="90%" class="blue" cellpadding=0>
        <TR>
            <TD class="blue" width="*" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_name", userId)%></B></TD>
            <TD class="blue" width="100" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_target", userId)%></B></TD>
            <!-- <TD class="blue" bgcolor="#808080" align="center"><B>Auftrag</B></TD> -->
            <TD class="blue" width="60" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_eta", userId)%></B></TD>
            <!-- <TD class="blue" bgcolor="#808080" align="center"><B>ETA(Zeit)</B></TD> -->
            <TD class="blue" width="120" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_arrivalat", userId)%></B></TD>
            <TD class="blue" width="140" bgcolor="#808080" align="center"><B><%= ML.getMLStr("fleetmanagement_lbl_actions", userId)%></B></TD>
        </TR>
        <%
                }
            }

            for (FleetFormationExt ffExt : formations) {
        %>
        <TR>

            <TD align="center"><B><%= ffExt.getBase().getName()%></B></TD>

            <%

                boolean currentPlayerIsOwner = (ffExt.getUserId() == userId);

                String destination = "";
                String destinationPop = "";

                if (ffExt.isMoving()) {
                    destination += String.valueOf(ffExt.getTargetRelativeCoordinate().getSystemId()) + ":";
                    destinationPop += Service.systemDAO.findById(ffExt.getTargetRelativeCoordinate().getSystemId()).getName();
                    if (ffExt.getTargetRelativeCoordinate().getPlanetId() == 0) {
                        destination += "x";
                    } else {
                        destination += String.valueOf(ffExt.getTargetRelativeCoordinate().getPlanetId());
                        PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(ffExt.getTargetRelativeCoordinate().getPlanetId());
                        if (pp != null) {
                            destinationPop += ":" + pp.getName();
                        } else {
                            destinationPop += ":Planet #" + ffExt.getTargetRelativeCoordinate().getPlanetId();
                        }
                    }
                } else {
                    destination += String.valueOf(ffExt.getRelativeCoordinate().getSystemId()) + ":";
                    destinationPop += Service.systemDAO.findById(ffExt.getRelativeCoordinate().getSystemId()).getName();
                    if (ffExt.getRelativeCoordinate().getPlanetId() == 0) {
                        destination += "x";
                    } else {
                        destination += String.valueOf(ffExt.getRelativeCoordinate().getPlanetId());
                        PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(ffExt.getRelativeCoordinate().getPlanetId());
                        if (pp != null) {
                            destinationPop += ":" + pp.getName();
                        } else {
                            destinationPop += ":Planet #" + ffExt.getRelativeCoordinate().getPlanetId();
                        }
                    }

                }
                destination = destination.replace("'", "");
            %>
            <TD align="center" onmouseover="doTooltip(event, '<%= destinationPop%>')" onmouseout="hideTip()"><%= destination%></TD>
             <!-- various column -->
             <%  if (!ffExt.isMoving()) {  %>
             <TD>
                 <TABLE>
                    <TR>
                        <%

                            if (!ffExt.isMoving()) {
                                 ArrayList<ScrapResource> scrap = null;
                                if(ffExt.getRelativeCoordinate().getPlanetId() > 0){
                                        scrap = Service.scrapResourceDAO.findBy(ffExt.getRelativeCoordinate().getPlanetId(), ELocationType.PLANET);
                                   }else{
                                        scrap = Service.scrapResourceDAO.findBy(ffExt.getRelativeCoordinate().getSystemId(), ELocationType.SYSTEM);
                                   }
                        %>
                        <% if(!scrap.isEmpty()){%>
                        <TD>
                             <IMG style="cursor:pointer;" alt="Gehe zu System" src="<%= GameConfig.getInstance().picPath()%>pic/scrap.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_showsystem", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/system&systemId=<%= ffExt.getBase().getSystemId()%>'"/>
                        </TD>
                            <% } %>
                        <% }else{ %>
                        <TD>xx</TD>
                        <% } %>

                    </TR>
                 </TABLE>
             </TD>
             <% } %>
        <!-- /various column -->
                <%
                    if (ffExt.isMoving()) {
                %>
            <TD align="center"><%= ffExt.getETA()%></TD>
            <TD align="center"><%= FormatTime.formatEndTimeofTimeSpanAsDate(ffExt.getETA())%></TD>
                <%
                    }
                %>
            <TD align="left">
                <TABLE cellspacing=0 cellpadding=0><TR>
                        <TD><IMG style="cursor:pointer;" alt="Flottenverband Information" src="<%= GameConfig.getInstance().picPath()%>pic/view.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_showfleetformationinformation", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&isFF=1&fleetId=<%= ffExt.getBase().getId()%>&fmaction=1'"/></TD>
                            <%
         if (parType != EFleetViewType.ALL_SYSTEM_ALLIED && parType != EFleetViewType.ALL_SYSTEM_ENEMY && parType != EFleetViewType.ENEMY_NOT_IN_SYSTEM) {
             if (currentPlayerIsOwner) {%><TD><IMG style="cursor:pointer;" alt="Flottenverband umbenennen" src="<%= GameConfig.getInstance().picPath()%>pic/umbenennen.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_renamefleetformation", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&isFF=1&fleetId=<%= ffExt.getBase().getId()%>&fmaction=2'"/></TD><% }%>
                            <% }%>
                            <%
                                if (transView) {
                                    boolean canLoadTroops = false;
                                    boolean canUnloadTroops = false;
                                    boolean canLoadRess = false;
                                    boolean canUnloadRess = false;
                                }
                            %>
                            <%  if (!ffExt.isMoving() && currentPlayerIsOwner) {%><TD><IMG style="cursor:pointer;" alt="Flottenverband bewegen" src="<%= GameConfig.getInstance().picPath()%>pic/movefleet.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_movefleetformation", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&isFF=1&fleetId=<%= ffExt.getBase().getId()%>&fmaction=4'"/></TD><% }%>
                        <% if (currentPlayerIsOwner) {%><TD><IMG style="cursor:pointer;" alt="Flotten Verband aufl&ouml;sen" src="<%= GameConfig.getInstance().picPath()%>pic/icons/undofleetformation.png" onmouseover="doTooltip(event, '<%=  ML.getMLStr("fleetmanagement_pop_disbandfleetformation", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&isFF=1&fleetId=<%= ffExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_UNDO_FORMATION%>'"/></TD><% }%>
                        <TD><IMG style="cursor:pointer;" alt="Gehe zu System" src="<%= GameConfig.getInstance().picPath()%>pic/goSystem.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_showsystem", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/system&systemId=<%= ffExt.getBase().getSystemId()%>'"/></TD>
                        <%  if (ffExt.isMoving() && ffExt.getSourceRelativeCoordinate().getSystemId() != 0 && currentPlayerIsOwner) {%><TD><IMG style="cursor:pointer;" alt="R�ckruf" src="<%= GameConfig.getInstance().picPath()%>pic/callBack.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_callback", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&isFF=1&fleetId=<%= ffExt.getId()%>&fmaction=10'"/></TD><% }%>

                    </TR></TABLE></TD>
        </TR>


        <%
            for (PlayerFleetExt pfExt : ffExt.getParticipatingFleets()) {
                if (parType == EFleetViewType.ALL_SYSTEM_ENEMY || parType == EFleetViewType.ENEMY_NOT_IN_SYSTEM) {
                    // Enemy must not know anything about structure of this fleet
                    break;
                }

                boolean currentPlayerIsOwnerFleet = (pfExt.getUserId() == userId);

                entryFound = true;

                if (ffExt.isMoving()) {
        %>
        <TR><TD align="center"><%= pfExt.getBase().getName()%></TD>
            <TD align="center">&nbsp;</TD>
            <TD align="center">&nbsp;</TD>
            <TD align="center">&nbsp;</TD>
                <%
                } else {
                %>
        <TR><TD colspan="2" align="center"><%= pfExt.getBase().getName()%></TD>
                <%
                    }
                %>
            <TD align="left"><TABLE cellspacing=0 cellpadding=0><TR>
                        <TD><IMG style="cursor:pointer;" alt="Flotteninformation" src="<%= GameConfig.getInstance().picPath()%>pic/view.jpg" onmouseover="doTooltip(event, 'Flotteninformationen anzeigen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=1'"/></TD>
                            <%
                                if (parType != EFleetViewType.ALL_SYSTEM_ALLIED && parType != EFleetViewType.ALL_SYSTEM_ENEMY && parType != EFleetViewType.ENEMY_NOT_IN_SYSTEM) {
                            %>
                            <% if (currentPlayerIsOwnerFleet) {%><TD><IMG style="cursor:pointer;" alt="Flotten umbenennen" src="<%= GameConfig.getInstance().picPath()%>pic/umbenennen.jpg" onmouseover="doTooltip(event, 'Flotte umbenennen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=2'"/></TD><% }%>

                        <% if (!transView && currentPlayerIsOwnerFleet) {%><TD><IMG style="cursor:pointer;" alt="Flotte aus Verband entfernen" src="<%= GameConfig.getInstance().picPath()%>pic/icons/removefleet.png" onmouseover="doTooltip(event, 'Flotte aus Verband entfernen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_UNDO_FORMATION%>'"/></TD><% }
    }%>
                    </TR></TABLE></TD>
        </TR>
        <%
                }
            }

            for (PlayerFleetExt pfExt : singleFleets) {
                String fleetName = "";
                String name = "";
                String color = "#FFFFFF";
                if (pfExt.getUserId() != userId) {
                    name = "[" + Service.userDAO.findById(pfExt.getUserId()).getGameName() + "]";
                    color = DiplomacyUtilities.getDiplomacyResult(userId, pfExt.getUserId()).getColor();
                }
                fleetName += "<B><FONT color='" + color + "'>" + pfExt.getBase().getName() + " " + name + "</B></FONT>";
        %>
        <TR>
            <TD align="center"><%= fleetName%></TD>
                <%
                    String destination = "";
                    String destinationPop = "";
                    if (pfExt.isMoving()) {
                        destination += String.valueOf(pfExt.getTargetRelativeCoordinate().getSystemId()) + ":";
                        destinationPop += Service.systemDAO.findById(pfExt.getTargetRelativeCoordinate().getSystemId()).getName();
                        if (pfExt.getTargetRelativeCoordinate().getPlanetId() == 0) {
                            destination += "x";
                        } else {
                            destination += String.valueOf(pfExt.getTargetRelativeCoordinate().getPlanetId());
                            PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(pfExt.getTargetRelativeCoordinate().getPlanetId());
                            if (pp != null) {
                                destinationPop += ":" + pp.getName();
                            } else {

                                destinationPop += ":Planet #" + pfExt.getTargetRelativeCoordinate().getPlanetId();
                            }
                        }
                    } else {
                        int sysId = pfExt.getRelativeCoordinate().getSystemId();
                        at.darkdestiny.core.model.System s = Service.systemDAO.findById(sysId);

                        destination += String.valueOf(sysId) + ":";
                        if (s != null) {
                            destinationPop += s.getName();
                        } else {
                            destinationPop += "??";
                        }

                        if (pfExt.getRelativeCoordinate().getPlanetId() == 0) {
                            destination += "x";
                        } else {
                            destination += String.valueOf(pfExt.getRelativeCoordinate().getPlanetId());
                            PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(pfExt.getRelativeCoordinate().getPlanetId());
                            if (pp != null) {
                                destinationPop += ":" + pp.getName();
                            } else {

                                destinationPop += ":Planet #" + pfExt.getRelativeCoordinate().getPlanetId();
                            }
                        }
                    }
                    destination = destination.replace("'", "");
                %>
            <TD align="center"  onmouseover="doTooltip(event, '<%= destinationPop%>')" onmouseout="hideTip()"><%= destination%></TD>
            <!-- various column -->

             <%  if (!pfExt.isMoving()) {  %>
            <TD width="10px">
                <TABLE>
                    <TR>
                        <!-- scrap image (info image. just in management -->
                        <%
                            if (!transView) {
                                if (!pfExt.isMoving()) {
                                    ArrayList<ScrapResource> scrap = null;
                                    if (pfExt.getRelativeCoordinate().getPlanetId() > 0) {
                                        scrap = Service.scrapResourceDAO.findBy(pfExt.getRelativeCoordinate().getPlanetId(), ELocationType.PLANET);
                                    } else {
                                        scrap = Service.scrapResourceDAO.findBy(pfExt.getRelativeCoordinate().getSystemId(), ELocationType.SYSTEM);
                                    }
                        %>

                                    <% if (!scrap.isEmpty()) {
                                            String tooltip = ML.getMLStr("fleetmanagement_pop_scrap", userId);
                                            for (ScrapResource scrapResource : scrap) {
                                                tooltip += "<br>";
                                                Ressource r = Service.ressourceDAO.findById(scrapResource.getResourceId());
                                                tooltip += ML.getMLStr(r.getName(), userId) + " : " + scrapResource.getQuantity();
                                            }

                                    %>
                                    <TD>
                                        <IMG alt="Schrott" src="<%= GameConfig.getInstance().picPath()%>pic/scrap.png" onmouseover="doTooltip(event, '<%= tooltip%>')" onmouseout="hideTip()"/>
                                    </TD>
                                        <% } else {%>
                                            <TD></TD>
                                        <% }%>
                               <% }%>
                        <!-- /scrap image -->
                        <% } else {%>
                        <TD></TD>
                        <% }%>

                    </TR>
                </TABLE>
            </TD>
            <% } %>
            <!-- /various column -->
                <%
                    if (pfExt.isMoving()) {
                %>
            <TD align="center"><%= pfExt.getETA()%></TD>
            <TD align="center"><%= FormatTime.formatEndTimeofTimeSpanAsDate(pfExt.getETA())%></TD>
                <%
                    }
                %>
            <TD align="left"><TABLE cellspacing=0 cellpadding=0><TR>
                        <TD><IMG style="cursor:pointer;" alt="Flotteninformation" src="<%= GameConfig.getInstance().picPath()%>pic/view.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_showfleetinformation", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=1'"/></TD>
                            <%
                                if (parType != EFleetViewType.ALL_SYSTEM_ALLIED && parType != EFleetViewType.ALL_SYSTEM_ENEMY
                                        && parType != EFleetViewType.ENEMY_NOT_IN_SYSTEM && parType != EFleetViewType.ALLIED_NOT_IN_SYSTEM) {
                            %>
                        <TD><IMG style="cursor:pointer;" alt="Flotten umbenennen" src="<%= GameConfig.getInstance().picPath()%>pic/umbenennen.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_renamefleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=2'"/></TD>
                            <%
                                // boolean canInvade =  at.darkdestiny.core.utilities.MilitaryUtilities.invasionPossible(userId, pfExt.getBase().getPlanetId(), null);
                                Integer duration = FleetService.findRunningScan(userId, pfExt.getId());
                                boolean canInvade = pfExt.canInvade();

                                if (transView) {
                                    LoadingInformation li = pfExt.getLoadingInformation();



                                    ArrayList<ScrapResource> scrap = null;
                                    if (pfExt.getRelativeCoordinate().getPlanetId() > 0) {
                                        scrap = Service.scrapResourceDAO.findBy(pfExt.getRelativeCoordinate().getPlanetId(), ELocationType.PLANET);
                                    } else {
                                        scrap = Service.scrapResourceDAO.findBy(pfExt.getRelativeCoordinate().getSystemId(), ELocationType.SYSTEM);
                                    }

                                    boolean positionIsPlanet = !pfExt.isMoving() && (pfExt.getRelativeCoordinate().getPlanetId() != 0);
                                    boolean troopsOnPlanet = false;

                                    if (positionIsPlanet) {
                                        troopsOnPlanet = MilitaryUtilities.loadingTroopsPossible(userId, pfExt.getRelativeCoordinate().getPlanetId());
                                    }

                                    boolean canLoadTroops = ((li.getTroopSpace() - li.getCurrLoadedTroops()) > 0) && positionIsPlanet && troopsOnPlanet;
                                    boolean canUnloadTroops = ((li.getCurrLoadedTroops()) > 0) && positionIsPlanet;
                                    boolean canLoadRess = ((li.getRessSpace() - li.getCurrLoadedRess()) > 0) && positionIsPlanet;
                                    boolean canUnloadRess = ((li.getCurrLoadedRess() > 0)) && positionIsPlanet;
                                    boolean canLoadPopulation = ((li.getTroopSpace() - li.getCurrLoadedTroops() - li.getCurrLoadedPopulation()) > 0) && positionIsPlanet;
                                    boolean canUnloadPopulation = ((li.getCurrLoadedPopulation() > 0)) && positionIsPlanet;
                                    boolean canLoadShips = ((li.getLargeHangarSpace() + li.getMediumHangarSpace() + li.getSmallHangarSpace()) > 0);
                                    boolean canAttachStation = pfExt.getCanDockStations();
                                    boolean canDetachStation = pfExt.getHasDockedStations();
                                    boolean canLoadScrap = pfExt.getHasTractorBeam() && !scrap.isEmpty() && ((li.getRessSpace() - li.getCurrLoadedRess()) > 0);

                                    if (Service.playerPlanetDAO.findByPlanetId(pfExt.getBase().getPlanetId()) != null) {
                                        //debug Logger.getLogger().write("1 : " + pfExt.getBase().getPlanetId());
                                        //debug Logger.getLogger().write("compare : " + pfExt.getUserId() + " to : " + Service.playerPlanetDAO.findByPlanetId(pfExt.getBase().getPlanetId()).getUserId());
                                        if (pfExt.getUserId() != Service.playerPlanetDAO.findByPlanetId(pfExt.getBase().getPlanetId()).getUserId()) {
                                            canLoadPopulation = false;
                                            canUnloadPopulation = false;

                                            if (!pfExt.canInvade() && !MilitaryUtilities.unloadTroopPossible(userId, planetId)) {
                                                canUnloadTroops = false;
                                            }
                                        }
                                    } else {
                                        //debug Logger.getLogger().write("3");
                                        canLoadPopulation = false;
                                        canUnloadPopulation = false;
                                    }

                                    if (canLoadRess) {%><TD><IMG style="cursor:pointer;" alt="Rohstoffe einladen" src="<%= GameConfig.getInstance().picPath()%>pic/ress_unload.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_loadresources", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_LOAD_RESS%>'"/></TD><% }
                                    if (canUnloadRess) {%><TD><IMG style="cursor:pointer;" alt="Rohstoffe entladen" src="<%= GameConfig.getInstance().picPath()%>pic/ress_load.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_unloadresources", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_UNLOAD_RESS%>'"/></TD><% }
                                    if (canLoadTroops) {%><TD><IMG style="cursor:pointer;" alt="Truppen einladen" src="<%= GameConfig.getInstance().picPath()%>pic/military_unload.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_loadtroops", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_LOAD_TROOPS%>'"/></TD><% }
                                    if (canUnloadTroops) {%><TD><IMG style="cursor:pointer;" alt="Truppen entladen" src="<%= GameConfig.getInstance().picPath()%>pic/military_load.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_unloadtroops", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_UNLOAD_TROOPS%>'"/></TD><% }
                                    if (canLoadPopulation) {%><TD><IMG style="cursor:pointer;" alt="Arbeiter einladen" src="<%= GameConfig.getInstance().picPath()%>pic/popload.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_loadworkers", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_LOAD_POPULATION%>'"/></TD><% }
                                    if (canUnloadPopulation) {%><TD><IMG style="cursor:pointer;" alt="Arbeiter entladen" src="<%= GameConfig.getInstance().picPath()%>pic/popunload.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_unloadworkers", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_UNLOAD_POPULATION%>'"/></TD><% }
                                    if (canLoadShips) {%><TD><IMG style="cursor:pointer;" alt="Hangars laden/entladen" src="<%= GameConfig.getInstance().picPath()%>pic/hangar.png" onmouseover="doTooltip(event, 'Hangars laden/entladen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_DO_HANGAR%>'"/></TD><% }
                                    if (canAttachStation) {%><TD><IMG style="cursor:pointer;" alt="Station andocken" src="<%= GameConfig.getInstance().picPath()%>pic/attach_station.png" onmouseover="doTooltip(event, 'Station andocken')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_ATTACH_STATION %>'"/></TD><% }
                                    if (canDetachStation) {%><TD><IMG style="cursor:pointer;" alt="Station l�sen" src="<%= GameConfig.getInstance().picPath()%>pic/detach_station.png" onmouseover="doTooltip(event, 'Station l�sen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_DETACH_STATION %>'"/></TD><% }
                                    if (canLoadScrap) {%><TD><IMG style="cursor:pointer;" alt="Schrott einsammeln" src="<%= GameConfig.getInstance().picPath()%>pic/collect_scrap.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_collect_scrap", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_LOAD_SCRAP %>'"/></TD><% }
            }
                            %>
                            <%  if (!pfExt.isMoving() && !transView && (duration == null)) {%><TD><IMG style="cursor:pointer;" alt="Flotte umgruppieren" src="<%= GameConfig.getInstance().picPath()%>pic/splitFleet.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_regroupfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=3'"/></TD><% }%>
                        <%  if (!pfExt.isInFleetFormation() && !transView && !pfExt.isMoving() && (duration == null)) {%><TD><IMG style="cursor:pointer;" alt="Flotte Formation hinzuf&uuml;gen" src="<%= GameConfig.getInstance().picPath()%>pic/icons/dofleetformation.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_addfleettoformation", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_DO_FORMATION%>'"/></TD><% }%>
                        <%  if (!pfExt.isMoving() && !pfExt.isInFleetFormation()) {%><TD><IMG style="cursor:pointer;" alt="Flotte bewegen" src="<%= GameConfig.getInstance().picPath()%>pic/movefleet.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_movefleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=1&fleetId=<%= pfExt.getBase().getId()%>&fmaction=4'"/></TD><% }%>
                        <%  if (!pfExt.isMoving()) {%><TD><IMG style="cursor:pointer;" alt="Gehe zu System" src="<%= GameConfig.getInstance().picPath()%>pic/goSystem.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_showsystem", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/system&systemId=<%= pfExt.getBase().getSystemId()%>'"/></TD><% }%>
                        <%  if ((pfExt.isMoving() && pfExt.getSourceRelativeCoordinate().getSystemId() != 0) && !pfExt.isInFleetFormation()) {%><TD><IMG style="cursor:pointer;" alt="R�ckruf" src="<%= GameConfig.getInstance().picPath()%>pic/callBack.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_callback", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=10'"/></TD><% }%>
                        <%  if (canInvade) {%> <TD><IMG style="cursor:pointer;" alt="Invadieren')" src="<%= GameConfig.getInstance().picPath()%>pic/invade.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_invade", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=invade&planetId=<%=  pfExt.getBase().getPlanetId()%>&fleetId=<%= pfExt.getBase().getId()%>'"/></TD><% }%>

                        <%
                            boolean used = false;
                            if (used && !transView) {
                                if ((pfExt.getBase().getPlanetId() != 0) && (pfExt.getBase().getStatus() != 2)) {%><TD><IMG style="cursor:pointer;" alt="Pl. Verteidigung" src="<%= GameConfig.getInstance().picPath()%>pic/defense.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_assigntoplanetarydefense", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=7'"/></TD><% }%>
                        <%  if ((pfExt.getBase().getPlanetId() == 0) && (pfExt.getBase().getStatus() != 2)) {%><TD><IMG style="cursor:pointer;" alt="Sys. Verteidigung" src="<%= GameConfig.getInstance().picPath()%>pic/defense_sys.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_addtosystemdefense", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=7'"/></TD><% }%>
                        <%  if ((pfExt.getBase().getPlanetId() != 0) && (pfExt.getBase().getStatus() == 2)) {%><TD><IMG style="cursor:pointer;" alt="Pl. Verteidigung" src="<%= GameConfig.getInstance().picPath()%>pic/defenseRemove.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_removefromplanetarydefense", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=8'"/></TD><% }%>
                        <%  if ((pfExt.getBase().getPlanetId() == 0) && (pfExt.getBase().getStatus() == 2)) {%><TD><IMG style="cursor:pointer;" alt="Sys. Verteidigung" src="<%= GameConfig.getInstance().picPath()%>pic/defense_sysRemove.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_removefromsystemdefense", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=8'"/></TD><% }%>
                        <TD><IMG style="cursor:pointer;" alt="Sys. Verteidigung" src="<%= GameConfig.getInstance().picPath()%>pic/icons/retreat.PNG" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_setretreatoptions", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=11'"/></TD>
                            <%
                                }

                                if (!pfExt.isMoving() && !transView) {

                                    if (FleetService.canColonize(pfExt) && (duration == null)) {
                            %>
                        <TD><IMG style="cursor:pointer;" alt="Planet Kolonisieren" src="<%= GameConfig.getInstance().picPath()%>pic/colonize.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_colonizeplanet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=99'"></IMG></TD>
                            <%
                                }

                                if (FleetService.canScanPlanet(pfExt)) {
                            %>
                        <TD><IMG style="cursor:pointer;" alt="Planet scannen" src="<%= GameConfig.getInstance().picPath()%>pic/scan_planet.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_scanplanet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_SCAN_PANET%>'"></IMG></TD>
                            <%
                                }
                                if (FleetService.canScanSystem(pfExt)) {
                            %>
                        <TD><IMG style="cursor:pointer;" alt="System scannen" src="<%= GameConfig.getInstance().picPath()%>pic/scan_system.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_scansystem", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_SCAN_SYSTEM%>'"></IMG></TD>
                            <%
                                }
                                if (FleetService.canCreateEmbassy(pfExt)) {
                            %>
                        <TD><IMG style="cursor:pointer;" alt="Botschaf errichten" src="<%= GameConfig.getInstance().picPath()%>pic/embassy.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_createembassy", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_CREATEEMBASSY%>'"></IMG></TD>
                            <%
                                }
                                if (duration != null && pfExt.getRelativeCoordinate().getPlanetId() != 0) {
                            %>
                        <TD style="border-style:solid; border-width:thin; border-color:#770000;"><TABLE cellpadding="0" cellspacing="0"><TR><TD><IMG style="cursor:pointer;" alt="Planet scannen" src="<%= GameConfig.getInstance().picPath()%>pic/scan_planet.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_planetscanongoing", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_ABORT_SCAN%>'"/></TD><TD bgcolor="black"><FONT style="font-size: 10px; font-weight: bolder" color="red">&nbsp;<%= duration%>&nbsp;</FONT></TD></TR></TABLE></TD>
                                        <%
                                            }
                                            if (duration != null && pfExt.getRelativeCoordinate().getPlanetId() == 0) {
                                        %>
                        <TD style="border-style:solid; border-width:thin; border-color:#770000;"><TABLE cellpadding="0" cellspacing="0"><TR><TD><IMG style="cursor:pointer;" alt="System scannen" src="<%= GameConfig.getInstance().picPath()%>pic/scan_system.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_systemscanongoing", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_ABORT_SCAN%>'"/></TD><TD bgcolor="black"><FONT style="font-size: 10px; font-weight: bolder" color="red">&nbsp;<%= duration%>&nbsp;</FONT></TD></TR></TABLE></TD>
                                        <%
                                            }

                                            ShipModifyView smv = at.darkdestiny.core.utilities.ShipUtilities.getModifiableShips(pfExt.getId());
                                            boolean cond1 = false;

                                            for (ShipModifyEntry sme : smv.getShipList()) {
                                                cond1 = cond1 || sme.isCanBeModified() || sme.isCanBeRepaired() || sme.isCanBeScrapped();
                                                cond1 = cond1 && (duration == null);
                                            }
                                            if (FleetService.canUseShipyard(pfExt.getUserId(), pfExt.getBase().getPlanetId())) {
        if (cond1) {%>
                        <TD><IMG style="cursor:pointer;" alt="Flotte umr&uuml;sten" src="<%= GameConfig.getInstance().picPath()%>pic/modify.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_modifyfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_MODIFY%>'" /></TD>
                            <%  } else {%>
                        <TD><IMG alt="Flotte umr&uuml;sten" src="<%= GameConfig.getInstance().picPath()%>pic/nomodify.jpg" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_nomodifyfleet", userId)%>')" onmouseout="hideTip()" /></TD>
                            <%  }
                                        }
                                    }
                                }
                                if (!pfExt.isMoving() && !transView) {
                                    if (pfExt.getUserId() == userId && FleetService.canFight(pfExt)) {
                            %>
                        <TD><IMG style="cursor:pointer;" alt="Flotte angreifen" src="<%= GameConfig.getInstance().picPath()%>pic/fight.png" onmouseover="doTooltip(event, '<%= ML.getMLStr("fleetmanagement_pop_attackfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/fleet&type=<%= typeForward%>&fleetId=<%= pfExt.getBase().getId()%>&fmaction=<%= FLEET_ACTION_ATTACK%>'"/></TD>
                            <%
                                    }
                                }
                            %>
                    </TR></TABLE></TD>
        </TR>
        <%
            }

            if (entryFound) {
        %>
    </TABLE>
    <% }%>