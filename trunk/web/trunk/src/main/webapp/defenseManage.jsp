<%--
    Document   : defenseManage
    Created on : 21.03.2009, 19:13:26
    Author     : Stefan
--%>
<%@page import="at.darkdestiny.core.ships.ShipUtilities"%>
<%@page import="at.darkdestiny.core.utilities.MilitaryUtilities"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.darkdestiny.core.hangar.*" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="at.darkdestiny.core.construction.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.interfaces.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.databuffer.fleet.*" %>
<%@page import="at.darkdestiny.core.model.Ressource" %>
<%@page import="at.darkdestiny.core.service.DefenseService" %>
<%@page import="at.darkdestiny.core.service.ShipDesignService" %>
<%@page import="at.darkdestiny.core.service.ConstructionService" %>
<%@page import="at.darkdestiny.core.view.*" %>
<%@page import="at.darkdestiny.core.buildable.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.model.PlanetDefense" %>
<%@page import="at.darkdestiny.core.model.ShipDesign" %>
<%@page import="at.darkdestiny.core.model.Construction" %>
<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int systemId = Integer.parseInt((String)session.getAttribute("actSystem"));

final int ACTION_UPGRADE = 4;
final int ACTION_SCRAP = 5;

int actionType = 0;
int designId = 0;
int detailFleet = 0;

String[] info = new String[3];
int defPlanetId = 0;
int defSystemId = 0;
// int queueIdent = 0;

boolean showDeployWindow = false;
boolean showRedesignWindow = false;
boolean showScrapWindow = false;

String message = "";

if (request.getParameter("showFleetId") != null) {
    detailFleet = Integer.parseInt((String)request.getParameter("showFleetId"));
}

BaseResult br = null;
ParameterBuffer pb = null;

if (request.getParameter("action") != null) {
    try {
        actionType = Integer.parseInt((String)request.getParameter("action"));

    if ((actionType == ACTION_UPGRADE) || (actionType == ACTION_SCRAP)) {
        if (request.getParameter("bufId") != null) {
            pb = BufferHandling.getBuffer(userId, Integer.parseInt(request.getParameter("bufId")));

            if (actionType == ACTION_SCRAP) {
                pb.setParameter(StarbaseScrapBuffer.COUNT, request.getParameter("destroyCount"));
            } else if (actionType == ACTION_UPGRADE) {
                pb.setParameter(UpgradeStarbaseBuffer.TO_DESIGN_ID, request.getParameter("toDesign"));
                pb.setParameter(UpgradeStarbaseBuffer.COUNT, request.getParameter("upgradeCount"));
            }
    %>
                <jsp:forward page="main.jsp" >
                    <jsp:param name="bufId" value='<%= pb.getId() %>' />
                    <jsp:param name="page" value='confirm/shipModification' />
                </jsp:forward>
    <%
        }
    }

        if ((actionType == 1) || (actionType == 4) || (actionType == 5)) {
            StringTokenizer st = null;

            switch (actionType) {
                case(1):
                    st = new StringTokenizer((String)request.getParameter("relocate"),"_");
                    break;
                case(4):
                    st = new StringTokenizer((String)request.getParameter("redesign"),"_");
                    break;
                case(5):
                    st = new StringTokenizer((String)request.getParameter("scrap"),"_");
                    break;
            }

            int elemCnt = 0;
            while (st.hasMoreElements()) {
                info[elemCnt] = (String)st.nextElement();
                elemCnt++;
            }

            designId = Integer.parseInt(info[0]);
            defPlanetId = Integer.parseInt(info[1]);
            defSystemId = Integer.parseInt(info[2]);
        }

        if ((actionType == ACTION_SCRAP) || (actionType == ACTION_UPGRADE)) {
            if (request.getParameter("bufId") == null) {
                if (actionType == ACTION_SCRAP) {
                    pb = new StarbaseScrapBuffer(userId);
                    pb.setParameter(StarbaseScrapBuffer.DESIGN_ID, designId);
                    pb.setParameter(StarbaseScrapBuffer.SYSTEM_ID, defSystemId);
                    pb.setParameter(StarbaseScrapBuffer.PLANET_ID, defPlanetId);
                    showScrapWindow = true;
                } else if (actionType == ACTION_UPGRADE) {
                    pb = new UpgradeStarbaseBuffer(userId);
                    pb.setParameter(UpgradeStarbaseBuffer.DESIGN_ID, designId);
                    pb.setParameter(UpgradeStarbaseBuffer.SYSTEM_ID, defSystemId);
                    pb.setParameter(UpgradeStarbaseBuffer.PLANET_ID, defPlanetId);
                    showRedesignWindow = true;
                }
            }
        }

        if (actionType == 1) {
            if ((String)request.getParameter("relocate") != null) {
                if (designId != 0) {
                    showDeployWindow = true;
                }
            }
        } else if (actionType == 2) {
            // Deploy Stations
            Map<String, String[]> allPars = request.getParameterMap();
            br = DefenseService.deployStation(allPars,systemId,planetId,userId);
        } else if (actionType == 3) {
            Map<String, String[]> parMap = request.getParameterMap();
            if (parMap.containsKey("addShips")) {
                // Show a fleetlist with all local fleets, multiple selection allowed,
                // after ok add all ships to planet defense at requested position
            } else if (parMap.containsKey("removeShips")) {
                // show all ships in defense splitted by system and planet, allow choosing of
                // count and add this ships to a new or defined local fleet.
            }
        }
        /*
        } else if (actionType == 4) {
            if ((String)request.getParameter("redesign") != null) {
                if (designId != 0) {
                    ReconstructionFunctions rf = new ReconstructionFunctions();
                    queueIdent = rf.getQueueIdentStarBase(userId,defPlanetId,defSystemId,designId);
                    showRedesignWindow = true;
                }
            }
        } else if (actionType == 5) {
            if ((String)request.getParameter("scrap") != null) {
                if (designId != 0) {
                    queueIdent = ShipUtilities.getQueueIdentStarBase(userId,defPlanetId,defSystemId,designId);
                    showScrapWindow = true;
                }
            }
        }
        */
    } catch (NumberFormatException e) {

    }
}

if (request.getParameter("fleetId") != null && request.getParameter("fmaction") != null) {
    try {
        if (request.getParameter("fmaction").equalsIgnoreCase("1")) {

            ShipUtilities.restoreFleetFromDef(Integer.parseInt(request.getParameter("fleetId")));
        }
    } catch (NumberFormatException e) { }
}
%>
<BR>
<%
if (br != null) {
    if (br.isError()) {
        message = "1" + br.getMessage();
    } else {
        message = "0" + br.getMessage();
    }
}

if (!message.equalsIgnoreCase("")) {
    if (message.substring(0,1).equalsIgnoreCase("1")) {
%>
    <FONT color=red style="font-size:13px"><B><%= message.substring(1) %></B></FONT><BR><BR>
<%
    } else {
%>
    <FONT color=green style="font-size:13px"><B><%= message.substring(1) %></B></FONT><BR><BR>
<%
    }
}
if (showDeployWindow) {
    String stationName = "DEFAULT";

    ShipDesign sd = ShipDesignService.getShipDesign(designId);
    if (sd != null) {
        stationName = sd.getName();
    }
    int count = DefenseService.getCountFor(defSystemId, defPlanetId, userId, EDefenseType.SPACESTATION, designId);
%>
<TABLE width="80%" cellpadding=0 cellspacing=0 class="blue"><TR style="font-weight: bold"><TD class="blue">Stationieren [<%= stationName %>]</TD></TR>
<TR><TD>
<FORM method="post" action="main.jsp?page=defensemenu&type=1&action=2&relocate=<%= (String)request.getParameter("relocate") %>" Name="Deploy">
<TABLE width="100%"><TR style="font-size:12px; font-weight: bold;">
<TD>System</TD>
<TD>Planet</TD>
<TD colspan=2>Anzahl</TD>
</TR><TR>
<TD><INPUT DISABLED NAME="sId" TYPE="text" VALUE="<%= systemId %>" SIZE="5" ALIGN=middle></TD>
<TD><INPUT NAME="pId" TYPE="text" VALUE="0" SIZE="5" ALIGN=middle></TD>
<TD><INPUT NAME="count" TYPE="text" VALUE="0" SIZE="5" ALIGN=middle> (max. <%= count %>)</TD>
<TD><INPUT type="submit" name="OK" value="OK"></TD>
</TR>
</TABLE>
</FORM>
</TD></TR>
</TABLE>
<BR>
<%
} else if (showRedesignWindow) {
    String stationName = "DEFAULT";
    int maxCount = 0;
    int chassisSize = 0;

    ShipDesign sd = ShipDesignService.getShipDesign(designId);
    // ResultSet rs = stmt.executeQuery("SELECT name, chassis FROM shipdesigns WHERE id="+designId);
    if (sd != null) {
        stationName = sd.getName();
        chassisSize = sd.getChassis();
    }

    maxCount = DefenseService.getCountFor(defSystemId, defPlanetId, userId, EDefenseType.SPACESTATION, designId);
    // rs = stmt.executeQuery("SELECT count FROM planetdefense WHERE planetId="+defPlanetId+" AND systemId="+defSystemId+" AND userId="+userId+" AND type=2 AND unitId="+designId);
%>
    <FORM method='post' action='main.jsp?page=defenseManage&bufId=<%= pb.getId() %>&action=<%= ACTION_UPGRADE %>' name='confirm' >
    <TABLE width="80%" style="font-size:13px"><TR>
    <TD colspan=2 align="center">Bitte das Design w&auml;hlen in das die aktuelle Station ge&auml;ndert werden soll und die Anzahl der zu &auml;ndernden Stationen eingeben.<BR><BR></TD></TR>
    <TR valign="middle">
    <TD width="200">Typ:</TD><TD><%= stationName %></TD></TR>
    <TR valign="middle"><TD>Planet:</TD><TD><%= defPlanetId %></TD></TR>
    <TR valign="middle"><TD>Anzahl:</TD><TD><INPUT name="upgradeCount" id="ugradeCount" value="1" SIZE="3" MAXLENGTH="6" ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();"> / <%= maxCount %><BR></TD></TR>
    <TR valign="top"><TD>Upgraden zu Design:</TD>
    <TD>
    <SELECT id="toDesign" name="toDesign">
        <OPTION value=0>-- Design w&auml;hlen --</OPTION>
<%
        ArrayList<ShipDesign> sdList = DefenseService.getPossibleUpgradeDesigns(userId, designId, chassisSize);
        // rs = stmt.executeQuery("SELECT id, name FROM shipdesigns WHERE chassis='"+chassisSize+"' AND id<>'"+designId+"' AND userId='"+userId+"'");
        for (ShipDesign sdTmp : sdList) {
%>
            <OPTION value="<%= sdTmp.getId() %>"><%= sdTmp.getName() %></OPTION>
<%
        }
%>
    </SELECT>
    </TD></TR>
    <TR><TD colspan=2 align="CENTER"><BR><INPUT type="submit" name="Ok" value="Ok"></TD></TR>
    </TABLE><BR><BR>
    </FORM>
<%
} else if (showScrapWindow) {
    String stationName = "DEFAULT";
    int maxCount = 0;

    ShipDesign sd = ShipDesignService.getShipDesign(designId);
    if (sd != null) {
        stationName = sd.getName();
    }

    maxCount = DefenseService.getCountFor(defSystemId, defPlanetId, userId, EDefenseType.SPACESTATION, designId);
    // rs = stmt.executeQuery("SELECT count FROM planetdefense WHERE planetId="+defPlanetId+" AND systemId="+defSystemId+" AND userId="+userId+" AND type='SPACESTATION' AND unitId="+designId);

    ShipDesignExt sde = new ShipDesignExt(designId);
    IRessourceCost shipCost = sde.getShipScrapCost();
%>
    <script language="Javascript">
        var creditPer = <%= shipCost.getRess(Ressource.CREDITS) %>;
<%
        for (Ressource r : DefenseService.getAllStorableRessources()) {
            out.write("var " + ML.getMLStr(r.getName(), userId) + "Per = " +shipCost.getRess(r.getId()) + ";");
        }
%>
        var maxCount = <%= maxCount %>;

        var actCount = 1;

        var totalCredit = creditPer;
<%
        for (Ressource r : DefenseService.getAllStorableRessources()) {
            out.write("var total" + r.getName() + " = " + r.getName() + "Per;");
        }
%>
        function calcTotal() {
            totalCredit = actCount * creditPer;
<%
            for (Ressource r : DefenseService.getAllStorableRessources()) {
                out.write("total" + r.getName() + " = actCount * " + r.getName() + "Per;");
            }
%>

            document.getElementById("credit").innerHTML = number_format(totalCredit,0) + ' Credits';
<%
            for (Ressource r : DefenseService.getAllStorableRessources()) {
                if (shipCost.getRess(r.getId()) != 0) { out.write("document.getElementById(\"" + r.getName() + "\").innerHTML = number_format(total" + r.getName() + ",0);"); }
            }
%>
        }
    </script>
    <FORM method='post' action='main.jsp?page=defenseManage&bufId=<%= pb.getId() %>&action=<%= ACTION_SCRAP %>' name='confirm' >
    <TABLE width="80%" style="font-size:13px"><TR>
            <TD colspan=2 align="center"><%= ML.getMLStr("defensemanage_msg_deletespacestations", userId) %><BR><BR></TD></TR>
    <TR valign="middle">
    <TD width="200"><%= ML.getMLStr("defensemanage_lbl_type", userId) %>:</TD><TD><%= stationName %></TD></TR>
    <TR valign="middle"><TD><%= ML.getMLStr("defensemanage_lbl_planet", userId) %>:</TD><TD><%= defPlanetId %></TD></TR>
    <TR valign="middle"><TD><%= ML.getMLStr("defensemanage_lbl_number", userId) %>:</TD><TD><INPUT name="destroyCount" id="destroyCount" value="1" SIZE="3" MAXLENGTH="6" ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();"> / <%= maxCount %><BR></TD></TR>
    <TR valign="top"><TD><%= ML.getMLStr("defensemanage_lbl_costs", userId) %>:</TD><TD><DIV id="credit"> <%= FormatUtilities.getFormattedNumber(shipCost.getRess(Ressource.CREDITS)) %></DIV><BR></TD></TR>

        <TR>
            <TD align="center" colspan=2 class="blue2"><B>Ressourcenr&uuml;ckverg&uuml;tung</B></TD>
        </TR>
        <TR>
            <TD class="blue2"><B><%= ML.getMLStr("defensemanage_lbl_ressource", userId) %></B></TD>
            <TD class="blue2"><B><%= ML.getMLStr("defensemenu_lbl_amount", userId) %></B></TD>
        </TR>
<%
        for (Ressource r : DefenseService.getAllStorableRessources()) {
            if (shipCost.getRess(r.getId()) != 0) { out.write("<TR><TD>"+r.getName()+"</TD><TD><DIV id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(shipCost.getRess(r.getId())) + "</DIV></TD></TR>"); }
        }
%>
    <TR><TD colspan=2 align="CENTER"><BR><INPUT type="submit" name="Ok" value=""defensemanage_but_confirm""></TD></TR>
    </TABLE><BR><BR>
    </FORM>
<%
}
if (!((showScrapWindow) || (showRedesignWindow) || (showDeployWindow))) {
%>
    <TABLE width="80%" cellpadding=0 cellspacing=0 class="blue"><TR style="font-weight: bold"><TD align="center" class="blue"><%= ML.getMLStr("defensemanage_lbl_activedefensesystems", userId) %></TD></TR>
    <TR><TD>
    <TABLE style="font-size:12px" width="100%" cellpadding=0 cellspacing=0>
    <TR style="font-size:12px; font-weight: bold;"><TD width="50%" class="blue" valign="top"><%= ML.getMLStr("defensemanage_lbl_orbitplanet", userId) %></TD><TD width="50%" class="blue"><%= ML.getMLStr("defensemanage_lbl_systemwide", userId) %></TD></TR>
    <TR valign="top"><TD>
    <% // Check planetdefense table with planetId - if nothing found print "-"
    boolean entryFound = false;
    boolean firstEntry = true;

    RelativeCoordinate rc = new RelativeCoordinate(systemId,planetId);
    ArrayList<PlanetDefense> pdList = DefenseService.getDefensesForLocByType(userId, rc, EDefenseType.SPACESTATION);

    // ResultSet rs = stmt.executeQuery("SELECT pd.count, sd.name FROM planetdefense pd, shipdesigns sd WHERE pd.unitId=sd.id AND pd.type='SPACESTATION' AND pd.planetId="+planetId+" AND pd.userId="+userId);
    for (PlanetDefense pd : pdList) {
        ShipDesign sd = ShipDesignService.getShipDesign(pd.getUnitId());

        if (!firstEntry) {
    %>
        <BR>
    <%
        }
    %>
        <%= pd.getCount() %>x <%= sd.getName() %>
    <%
        entryFound = true;
        firstEntry = false;
    }

    pdList = DefenseService.getDefensesForLocByType(userId, rc, EDefenseType.TURRET);
    // rs = stmt.executeQuery("SELECT pd.count, c.name FROM planetdefense pd, construction c WHERE pd.unitId=c.id AND pd.type='TURRET' AND pd.planetId="+planetId);
    for (PlanetDefense pd : pdList) {
        Construction c = ConstructionService.findConstructionById(pd.getUnitId());

        if (!firstEntry) {
    %>
        <BR>
    <%
        }
    %>
    <%= pd.getCount() %>x <%= ML.getMLStr(c.getName(), userId) %>
    <%
        entryFound = true;
        firstEntry = false;
    }

    if (!entryFound) {
    %>
        -
    <%
    }
    %>
    </TD>
    <TD>
    <% // Check planetdefense table with systemId - if nothing found print "-"
    entryFound = false;
    firstEntry = true;

    rc = new RelativeCoordinate(systemId,0);
    pdList = DefenseService.getDefensesForLocByType(userId, rc, EDefenseType.SPACESTATION);
    // rs = stmt.executeQuery("SELECT pd.count, sd.name FROM planetdefense pd, shipdesigns sd WHERE pd.unitId=sd.id AND pd.type='SPACESTATION' AND pd.systemId="+systemId+" AND pd.planetId=0 AND pd.userId="+userId);
    for (PlanetDefense pd : pdList) {
        ShipDesign sd = ShipDesignService.getShipDesign(pd.getUnitId());

        if (!firstEntry) {
    %>
        <BR>
    <%
        }
    %>
        <%= pd.getCount() %>x <%= sd.getName() %>
    <%
        entryFound = true;
        firstEntry = false;
    }

    // rs = stmt.executeQuery("SELECT pd.count, c.name FROM planetdefense pd, construction c WHERE pd.unitId=c.id AND pd.type='TURRET' AND pd.systemId="+systemId+" AND pd.planetId=0 AND pd.userId="+userId);
    pdList = DefenseService.getDefensesForLocByType(userId, rc, EDefenseType.TURRET);
    for (PlanetDefense pd : pdList) {
        Construction c = ConstructionService.findConstructionById(pd.getUnitId());

        if (!firstEntry) {
    %>
        <BR>
    <%
        }
    %>
        <%= pd.getCount() %>x <%= c.getName() %>
    <%
        entryFound = true;
        firstEntry = false;
    }

    if (!entryFound) {
    %>
        -
    <%
    }
    %>
    </TD>
    </TR></TABLE></TD></TR></TABLE>
    <TABLE width="80%" class="blue">
        <TR style="font-weight: bold"><TD align="center" class="blue" colspan=3><%= ML.getMLStr("defensemenu_lbl_spacestations", userId) %></TD></TR>
        <TR style="font-weight: bold"><TD width="*" class="blue"><%= ML.getMLStr("defensemanage_lbl_name", userId) %></TD><TD align="center" width="100" class="blue"><%= ML.getMLStr("defensemanage_lbl_position", userId) %></TD><TD width="100" class="blue"><%= ML.getMLStr("defensemanage_lbl_actions", userId) %></TD></TR>
    <% // Check undeployedstations table - print "-" if empty
    pdList = DefenseService.getDefensesForSystemByType(userId, rc.getSystemId(), EDefenseType.SPACESTATION);
    // rs = stmt.executeQuery("SELECT sd.id, pd.count, sd.name, pd.planetId, pd.systemId FROM planetdefense pd, shipdesigns sd WHERE pd.unitId=sd.id AND (pd.systemId="+systemId+" OR pd.systemId=0) AND pd.type='SPACESTATION'");

    entryFound = false;
    firstEntry = true;

    for (PlanetDefense pd : pdList) {
        ShipDesign sd = ShipDesignService.getShipDesign(pd.getUnitId());

        String planetStr = ""+pd.getPlanetId();
        if (pd.getPlanetId() == 0) planetStr = "x";

        if (!firstEntry) {
            // Nur zur info f&uuml;r den, der das geschrieben hat: Dieser Tag ist an dieser Stelle ung&uuml;ltig!
    %>
        <BR>
    <%
        }

        entryFound = true;
        firstEntry = false;
    %>
    <TR><TD><%= pd.getCount() %>x <%= sd.getName() %></TD>
        <TD align="center"><%= systemId %>:<%= planetStr %></TD>
        <TD>
            <%
            // <IMG src="**=GameConfig.getInstance().picPath()**pic/deploy.jpg" onmouseover="doTooltip(event,'Raumstation positionieren')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=military&type=8&orbDefId=**= rs.getInt(1) **&action=1'" />
            %>
            <IMG src="<%=GameConfig.getInstance().picPath()%>pic/relocate.jpg" onmouseover="doTooltip(event,'<%= ML.getMLStr("defensemanage_pop_relocatespacestation", userId) %>')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=defensemenu&type=1&relocate=<%= pd.getUnitId() %>_<%= pd.getPlanetId() %>_<%= pd.getSystemId() %>&action=1';" />
    <%
            ShipModifyEntry sme = DefenseService.getShipModifyEntry(pd.getUnitId(), pd.getPlanetId());
            if (sme.isCanBeModified()) {
    %>
                <IMG src="<%=GameConfig.getInstance().picPath()%>pic/modify.jpg" onmouseover="doTooltip(event,'<%= ML.getMLStr("defensemanage_pop_upgradespacestation", userId) %>')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=defensemenu&type=1&redesign=<%= pd.getUnitId() %>_<%= pd.getPlanetId() %>_<%= pd.getSystemId() %>&action=4';" />
                <IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'<%= ML.getMLStr("defensemanage_pop_disarmspacestation"   , userId) %>')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=defensemenu&type=1&scrap=<%= pd.getUnitId() %>_<%= pd.getPlanetId() %>_<%= pd.getSystemId() %>&action=5';" />
    <%
            }
    %>
    </TD></TR>
    <%
    }
    if (!entryFound) {
    %>
    <TR><TD>-</TD><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
    <%
    }
    %>
    </TABLE>
<%
}
%>