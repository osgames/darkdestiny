<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.core.model.Voting"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.voting.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%
// Read Session Data
session = request.getSession();

int userId = 0;
int voteId = 0;
int voted = 0;

try {
    userId = Integer.parseInt((String)session.getAttribute("userId"));
    voteId = Integer.parseInt(request.getParameter("voteId"));
    voted = Integer.parseInt(request.getParameter("voteoption"));
} catch (NumberFormatException nfe) {
    DebugBuffer.error("Initial Parameter casting failed: " + nfe.getMessage());
}

Locale locale = request.getLocale();
if(session.getAttribute("Language") != null){
    locale = (Locale)session.getAttribute("Language");
}

//out.write("<FONT color=\"white\">Vote (Id="+voteId+",userId="+userId+",option="+voted+")</FONT><BR>");

// out.write("You have voted: " + voted + " for vote " + voteId + "<BR>");
// out.write("Access granted? " + VoteUtilities.checkVotePermission(userId, voteId));

// Check for reference
int refMsg = 0;
if (request.getParameter("msgId") != null) {
    refMsg = Integer.parseInt((String)request.getParameter("msgId"));
}

boolean voteClosed = false;
boolean voteNotExistant = false;
boolean acceptedWithThisVote = false;
boolean deniedWithThisVote = false;
boolean alreadyVoted = false;

VoteResult vr = null;
IVoteWrapper ivw; 
Vote v = VotingFactory.loadVote(voteId);

if (v.getType() == Voting.TYPE_VOTE_RESEARCH) {
    ivw = new ResearchVoteWrapper();
} else {
    ivw = new DefaultVoteWrapper();
}

if (VoteUtilities.checkVotePermission(userId,voteId)) {
    //out.write("<FONT color=\"white\">Permission granted</FONT><BR>");
    
    vr = VoteUtilities.vote(userId, voted, voteId, locale);

    if ((vr.getVoteCode() == VoteResult.VOTE_CLOSED_OPTION) ||
        (vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT) ||
        (vr.getVoteCode() == VoteResult.VOTE_CLOSED_DENIED)) {
        DebugBuffer.debug("[DOVOTEa] VoteCode: " + vr.getVoteCode());
        voteClosed = true;     
        //out.write("<FONT color=\"white\">Result -> Closed</FONT><BR>");          
    } else if (vr.getVoteCode() == VoteResult.VOTE_NOT_EXISTS) {
        DebugBuffer.debug("[DOVOTEb] VoteCode: " + vr.getVoteCode());
        voteNotExistant = true;
        //out.write("<FONT color=\"white\">Result -> Not exists</FONT><BR>");    
    } else if (vr.getVoteCode() == VoteResult.VOTED_ACCEPT) {
        DebugBuffer.debug("[DOVOTEc] VoteCode: " + vr.getVoteCode());
        acceptedWithThisVote = true;
        //out.write("<FONT color=\"white\">Result -> Accepted</FONT><BR>");    
    } else if (vr.getVoteCode() == VoteResult.VOTED_DENIED) {
        DebugBuffer.debug("[DOVOTEd] VoteCode: " + vr.getVoteCode());
        deniedWithThisVote = true;
        //out.write("<FONT color=\"white\">Result -> Denied</FONT><BR>");    
    } else if (vr.getVoteCode() == VoteResult.VOTE_ALREADY_VOTED) {
        DebugBuffer.debug("[DOVOTEe] VoteCode: " + vr.getVoteCode());
        alreadyVoted = true;
        //out.write("<FONT color=\"white\">Result -> Already Voted</FONT><BR>");    
    } else if (vr.getVoteCode() == VoteResult.VOTED) {
        DebugBuffer.debug("[DOVOTEf] VoteCode: " + vr.getVoteCode());
        //out.write("<FONT color=\"white\">Result -> Voted</FONT><BR>");    
    }
} else {
    // out.write("<FONT color=\"white\">No Permission</FONT><BR>");    
    return;
}


// Invalid call 
if (!voteNotExistant) { 
    if (voteClosed) {
        if (vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT) {
            DebugBuffer.debug("[DOVOTE1] VoteCode: " + vr.getVoteCode());
%>        
        <FONT style="font-size:13px">Diese Abstimmung wurde bereits abgeschlossen und angenommen!</FONT><BR>
<%            
        } else if (vr.getVoteCode() == VoteResult.VOTED_DENIED) {
            DebugBuffer.debug("[DOVOTE2] VoteCode: " + vr.getVoteCode());
%>        
        <FONT style="font-size:13px">Diese Abstimmung wurde bereits abgeschlossen und abgelehnt!</FONT><BR>                
<%            
        }
%>        
        <FONT style="font-size:13px"><%= ivw.getVoteStateHTML(vr,voteId, locale) %></FONT>
<%
    } else if (voteNotExistant) {
        DebugBuffer.debug("[DOVOTE3] VoteCode: " + vr.getVoteCode());
%>
            <BR><FONT style="font-size:13px" color="red">Diese Abstimmung existiert nicht mehr!</FONT><BR>
<%
    } else {
        if (acceptedWithThisVote && voteClosed) {
            DebugBuffer.debug("[DOVOTE4] VoteCode: " + vr.getVoteCode());
%>
            <BR><FONT style="font-size:13px" color="yellow"><B>Der Antrag wurde mit deiner Stimme angenommen!</B><BR></FONT>
            <FONT style="font-size:13px"><%= ivw.getVoteStateHTML(vr,voteId,locale) %></FONT>
<%
        } else if (deniedWithThisVote && voteClosed) {
            DebugBuffer.debug("[DOVOTE5] VoteCode: " + vr.getVoteCode());
%>
            <BR><FONT style="font-size:13px" color="yellow"><B>Der Antrag wurde mit deiner Stimme abgelehnt!</B><BR></FONT>
            <FONT style="font-size:13px"><%= ivw.getVoteStateHTML(vr,voteId,locale) %></FONT>
<%            
        } else if (alreadyVoted) {
            DebugBuffer.debug("[DOVOTE6] VoteCode: " + vr.getVoteCode());
%>
            <BR><FONT style="font-size:13px" color="yellow"><B>Du hast bereits abgestimmt!</B><BR></FONT>
            <FONT style="font-size:13px"><%= ivw.getVoteStateHTML(vr,voteId,locale) %></FONT>
<%            
        } else {
            DebugBuffer.debug("[DOVOTE7] VoteCode: " + vr.getVoteCode());
%>
            <BR><FONT style="font-size:13px" color="yellow"><B>Deine Stimmabgabe wurde gez&auml;hlt!</B><BR></FONT>
            <FONT style="font-size:13px"><%= ivw.getVoteStateHTML(vr,voteId,locale) %></FONT>
<%            
        }
    }
} else {
%>
<BR><FONT style="font-size:13px" color="red">Ung&uuml;ltiger Seiten Aufruf!<BR></FONT>
<%    
}

String linkExt = "";
if (refMsg != 0) {
    linkExt = "&type=1&messageId="+refMsg;
}
%>
<BR><BR><A style="font-size:13px" HREF="main.jsp?page=new/messages<%= linkExt %>">Weiter</A>