<%@page import="at.darkdestiny.core.*"%>
<%@page import="java.sql.*"%>
<%@page import="at.darkdestiny.core.databuffer.*"%>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.service.Service" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%
session = request.getSession();
if (session.getAttribute("userId") != null) {
    int userId = Integer.parseInt((String)session.getAttribute("userId"));

    if (request.getParameter("newHome") != null) {        
        Statement stmt = DbConnect.createStatement();
    
        int newHome = Integer.parseInt(request.getParameter("newHome"));
    
        for(PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)){
            int bufferId = pp.getPlanetId();
            if (bufferId != newHome) {
                pp.setHomeSystem(false);
            } else {
                pp.setHomeSystem(true);
            }
            Service.playerPlanetDAO.update(pp);
        }
       
        session.setAttribute("actPlanet",""+Service.playerPlanetDAO.findHomePlanetByUserId(userId).getPlanetId());
        session.setAttribute("actSystem",""+Service.planetDAO.findById(Service.playerPlanetDAO.findHomePlanetByUserId(userId).getPlanetId()).getSystemId());
    }

%>    
    <jsp:forward page="main.jsp?page=new/overview" />
<%
} else {
%>    
    <jsp:forward page="main.jsp?page=new/overview" />
<%    
}
%>