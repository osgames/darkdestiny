<%--
    Document   : noteEditor
    Created on : 04.02.2011, 18:14:46
    Author     : Aion
--%>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.trade.*" %>
<%@page import="at.darkdestiny.core.exceptions.*" %>
<%@page import="at.darkdestiny.core.model.Note" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="at.darkdestiny.core.model.PlayerPlanet" %>
<%


            int userId = 0;
            if (session.getAttribute("userId") != null) {
                userId = Integer.parseInt((String) session.getAttribute("userId"));
            }
            BaseResult br = null;

            int planetId = LoginService.findHomePlanetByUserId(userId);
            if (request.getParameter("submit") != null) {
                int submit = Integer.parseInt(request.getParameter("submit"));
                if(submit == 1){
                    planetId = Integer.parseInt(request.getParameter("planetId"));
                }else{
                    String name = request.getParameter("planetId");
                    try {
                        PlayerPlanet pp = TradeUtilities.resolvePlanetNameId(name, userId, true);
                        if (pp == null) {
                            pp = TradeUtilities.resolvePlanetNameId(name, userId, false);
                        }
                        if (pp != null) {
                        planetId = pp.getPlanetId();
                        }else{

                        br =  new BaseResult(ML.getMLStr("top_err_planetnotfound", userId), true);
                            }
                    } catch (PlanetNameNotUniqueException pnnue) {
                        br =  new BaseResult(ML.getMLStr("transport_err_planetnamenotunique", userId), true);
                    }
                }
                if(br == null){
%>
<script language="Javascript">
window.opener.location.href = 'selectview.jsp?&showPlanet=<%= planetId %>'
    this.close();
</script>
<%
}
            }
%>


<%

%>
<HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    </head>
    <TITLE>
        Planetswitch
    </TITLE>
    <BODY bgcolor="black">
        <TABLE border="0" cellpadding="0" cellspacing="0" style="width:100%;color:white; font-size: 11px; font-weight: bolder;">
            <FORM name="form" method="post" action="planetPop.jsp?submit=1">
                <INPUT class="blue2" type="hidden" name="planetId" value="<%= planetId%>"/>
                <TR>
                    <TD colspan="1" align="center" style="width:100%;">
                        <INPUT style="width:100%;background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: center; font-size: 11px;" type="submit" value="Zum Hauptplaneten"/>
                    </TD>
                </TR>
            </FORM>
            <FORM name="form" method="post" action="planetPop.jsp?submit=2">
                <TR>
                    <TD colspan="1" align="center" style="width:100%;">
                        PlanetName/Id
                    </TD>
                </TR>
                <TR>
                    <TD colspan="1" align="center" style="width:100%;">
                        <INPUT name="planetId" style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: center; font-size: 11px;" type="text"/>
                    </TD>
                </TR>
                <TR>
                    <TD colspan="1" align="center" style="width:100%;">
                        <INPUT style="background-color:#000000;  border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: center; font-size: 11px;" type="submit" value="Absenden"/>
                    </TD>
                </TR>
            </FORM>
                <% if(br != null){ %>

                <TR>
                    <TD style="background-color:#000000; color: red; text-align: center; font-size: 11px;" colspan="1" align="center" style="width:100%;">
                       <%= br.getMessage() %>
                    </TD>
                </TR>
                <% } %>
        </TABLE>
        <%= planetId%>
    </BODY>
</HTML>