<%@page import="at.darkdestiny.core.utilities.DiplomacyUtilities"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel"%>
<%@page import="at.darkdestiny.core.databuffer.*"%>
<%@page import="at.darkdestiny.core.StarMapInfo"%>
<%@page import="at.darkdestiny.core.StarMapInfoData"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.core.xml.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.OutputStreamWriter"%>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.util.*"%>
<%
int userId = -1;
            try {


                try {
                    userId = Integer.parseInt((String) session.getAttribute("userId"));
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.WARNING, "Creating StarMapInfo-Data => No userId set");
                    return;
                }

                //  DebugBuffer.addLine(DebugLevel.WARNING, "Done finding sharing users in " + (System.currentTimeMillis() - time) + " ms");

                XMLMemo starmap = new XMLMemo("Starmap");
                XMLMemo coding = new XMLMemo("Codierung");
                XMLMemo typeEntry = new XMLMemo("LandCode");
                typeEntry.addAttribute("A", "Gasriese, lebensfeindlich");
                typeEntry.addAttribute("B", "Gasriese, lebensfeindlich");
                typeEntry.addAttribute("C", "Kalt, bewohnbar");
                typeEntry.addAttribute("E", "Keine Atmosph\u00E4re, lebensfeindlich");
                typeEntry.addAttribute("G", "Heiss, bewohnbar");
                typeEntry.addAttribute("J", "Geologisch sehr aktiv, lebensfeindlich");
                typeEntry.addAttribute("L", "Fels und Eis, lebensfeindlich");
                typeEntry.addAttribute("M", "Erd\u00E4hnlich, bewohnbar");
                coding.addChild(typeEntry);
                starmap.addChild(coding);

                StarMapInfoData smid = new StarMapInfoData(userId);
                StarMapInfo smi = new StarMapInfo(userId);

                starmap = smi.addConstants(starmap);
                //  DebugBuffer.addLine(DebugLevel.WARNING, "Adding constants in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addObservatory(starmap, smid);
                //   DebugBuffer.addLine(DebugLevel.WARNING, "Adding observatories in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addSystems(starmap, smid);
                // DebugBuffer.addLine(DebugLevel.WARNING, "Adding systems in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addAllFleets(starmap, smid);
                // DebugBuffer.addLine(DebugLevel.WARNING, "Adding fleets in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addHyperScanner(starmap, smid);
                //  DebugBuffer.addLine(DebugLevel.WARNING, "Adding Hyperscanner in " + (System.currentTimeMillis() - time) + " ms");
                starmap = smi.addTradeRoutes(starmap);

                 smi.addSunTransmitter(starmap);
                //   DebugBuffer.addLine(DebugLevel.WARNING, "Adding traderoutes in " + (System.currentTimeMillis() - time) + " ms");
                 smi.addTerritories(starmap, smid);
//Hinzuf&uuml;gen der Handelsrouten Infos

                response.setCharacterEncoding("ISO-8859-1");
                // PrintWriter toJSP = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "ISO-8859-1"));
                PrintWriter toJSP = new PrintWriter(out);
                starmap.write(toJSP);
                //   DebugBuffer.addLine(DebugLevel.WARNING, "Done creating Starmap-Data for User " + userId + " in " + (System.currentTimeMillis() - time) + " ms");
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while generating StarmapInfo (for User "+userId+"): ", e);
            }
%>