<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.model.UserSettings"%>
<%@page import="at.darkdestiny.core.service.ProfileService"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.core.model.UserData"%>
<%
// Determine correct connection path
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    UserSettings us = Service.userSettingsDAO.findByUserId(userId);
    String xmlUrl = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);
%>
<html>
    <title>
    </title>
    <body bgColor="BLACK">
    <CENTER>
        <a href="darkdestiny://<%=xmlUrl.replace("http://", "")%>?session=<%= session.getId()%>&mode=STARMAP">In der App �ffnen</a> (<a href="appInfo.jsp">Info</a>)
        <BR>
        <applet id="starmap" code="at.darkdestiny.vismap.gui.AppletMain" width="<%= us.getStarmapWidth()%>" height="<%= us.getStarmapHeight()%>" alt="" archive="applet/vismap.jar">
            <param name="source" value="<%= xmlUrl%>createStarMapInfoXT.jsp"/>
            <param name="sessionId" value="<%= request.getSession().getId()%>"/>
            <param name="MODE" value="STARMAP">
            <PARAM NAME="cache_option" VALUE="NO">
        </applet>
    </CENTER>

</body>
<script language="javascript" type="text/javascript">
    var disableHTMLScroll = false;

    document.getElementById("starmap").addEventListener('click',
            function (e) {
                disableHTMLScroll = true;
            }
    , false);

    document.getElementById("starmap").addEventListener('mouseover',
            function (e) {
                disableHTMLScroll = true;
            }
    , false);

    document.getElementById("starmap").addEventListener('mouseout',
            function (e) {
                disableHTMLScroll = false;
            }
    , false);

    document.addEventListener('DOMMouseScroll', function (e) {
        if (disableHTMLScroll) {
            e.stopPropagation();
            e.preventDefault();
            e.cancelBubble = false;
            return false;
        } else {
            return true;
        }
    }, false);
</script>
</html>
