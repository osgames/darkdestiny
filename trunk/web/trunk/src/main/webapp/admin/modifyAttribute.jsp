<%-- 
    Document   : modifyAttribute
    Created on : 14.11.2009, 18:52:00
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%
int attributeId = 0;
String errorMsg = null;

AdminAttribute aa = null; 

String mode = "Add new Attribute";
String buttonMode = "ADD";
boolean editMode = false;

if (request.getParameter("type") != null) {        
    if (request.getParameter("type").equalsIgnoreCase("add")) {
        try {
            ModuleConfiguration.modifyAttribute(request.getParameterMap(),true);
            response.sendRedirect("moduleConfiguration.jsp");
        } catch (InvalidDataException ide) {
            errorMsg = ide.getMessage();
        }
    } else if (request.getParameter("type").equalsIgnoreCase("edit")) {
        attributeId = Integer.parseInt(request.getParameter("id"));
        aa = ModuleConfiguration.getAttribute(attributeId);      
        
        try {
            ModuleConfiguration.modifyAttribute(request.getParameterMap(),false);
            response.sendRedirect("moduleConfiguration.jsp");
        } catch (InvalidDataException ide) {
            errorMsg = ide.getMessage();
        }        
    } else if (request.getParameter("type").equalsIgnoreCase("modify")) {  
        attributeId = Integer.parseInt(request.getParameter("id"));                     
        aa = ModuleConfiguration.getAttribute(attributeId);
        mode = "Edit Attribute " + aa.getName() + " [" + aa.getId() + "]";
        buttonMode = "SAVE";
        editMode = true;
    }
}

ArrayList<String> references = new ArrayList<String>();
for (Reference r : Reference.values()) {
    references.add(r.name());
}
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title><%= mode %></title>
    </head>
    <body>
        <h2><%= mode %></h2><BR><BR>
<%
    if (errorMsg != null) {
%>
        <FONT color="red"><B><%= errorMsg%></B></FONT><BR><BR>
<%
    }
%> 
        <FORM METHOD="post" ACTION="modifyAttribute.jsp?type=<%= editMode ? "edit" : "add" %><%= editMode ? "&id="+aa.getId() : "" %>">
        <TABLE>
            <TR>
                <TD>Name</TD>
<%
            if (editMode) {
%>
                <TD><INPUT NAME="name" ID="name" SIZE="50" VALUE="<%= aa.getName() %>" /></TD>
<%
            } else {
%>
                <TD><INPUT NAME="name" ID="name" SIZE="50" VALUE="" /></TD>
<%
            }
%>
            </TR>        
            <TR>
                <TD>Reference</TD>
                <TD>
                    <SELECT name="reference">
<%
                    for (String option : references) {
                        if (editMode && (aa.getRef() != null) && option.equalsIgnoreCase(aa.getRef().toString())) {
%>
                            <OPTION SELECTED ID="<%= option %>"><%= option %></OPTION>
<%
                        } else {
%>
                            <OPTION ID="<%= option %>"><%= option %></OPTION>
<%
                        }
                    }
%>
                    </SELECT>
                </TD>
            </TR>                                                 
        </TABLE>
        <BR>
        <INPUT type="SUBMIT" ID="confirm" NAME="confirm" VALUE="<%= buttonMode %>" />
        </FORM>
        <BR><A HREF="moduleConfiguration.jsp">Zur�ck</A>
    </body>
</html>
