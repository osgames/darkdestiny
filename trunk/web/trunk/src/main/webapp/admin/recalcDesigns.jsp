<%-- 
    Document   : recalcDesigns
    Created on : 30.01.2011, 22:21:52
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.darkdestiny.core.dao.*" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Update designs</title>
    </head>
    <body>
        <h2>Updating ...</h2>
<%
    ShipDesignDAO sdDAO = (ShipDesignDAO)DAOFactory.get(ShipDesignDAO.class);
    ArrayList<ShipDesign> sdList = sdDAO.findAll();

    int nothing = 0;
    int success = 0;
    int fail = 0;

    for (ShipDesign sd : sdList) {
        if (sd.getChassis() < 3) continue;

        ShipDesignService.State st = ShipDesignService.updateDesignCosts(sd.getId());

        if (st == ShipDesignService.State.NOTHING_DONE) nothing++;
        if (st == ShipDesignService.State.SUCCESS) success++;
        if (st == ShipDesignService.State.FAIL) fail++;

        out.write("<BR>Updating: " + sd.getName() + "["+st+"]");                
    }

    out.write("<BR><BR>");
    out.write("NOTHING: " + nothing + "<BR>");
    out.write("SUCCESS: " + success + "<BR>");
    out.write("FAIL: " + fail + "<BR>");
%>
    </body>
</html>
