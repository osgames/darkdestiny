<%@page import="at.darkdestiny.core.ai.AIDebugLevel"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="at.darkdestiny.core.ai.AIUtilities"%>
<%@page import="at.darkdestiny.core.ai.AIDebugBuffer"%>
<%@page import="at.darkdestiny.core.ai.AIThread"%>
<%@page import="at.darkdestiny.core.ai.AIDebugEntry"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel" %>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLine" %>
<%
    /*
     * Hier wird das Menu f&uuml;r den Admin erzeugt
     */
%>
<HTML>
    <HEAD>
        <TITLE>Dark Destiny - DEBUG</TITLE>
        <style type="text/css">
            <!--
            /*  ... Style-Sheet-Angaben ... */
            a:link { color:#C0C0C0; font-weight:bold }
            a:visited { color:#C0C0C0; font-weight:bold }
            a:active { color:#C0C0C0; font-weight:bold }
            a:hover { color:#ffffff; font-weight:bold; }
            a { text-decoration:none; }
            -->
        </style>
    </HEAD>
    <body text="#FFFFFF" BGCOLOR="#000000">

        <%
            String aiThreadName = "";
            String debugLevel = "";
            if (request.getParameter("aithreadname") != null) {
                aiThreadName = request.getParameter("aithreadname");
            }

            if (request.getParameter("debugLevel") != null) {
                debugLevel = request.getParameter("debugLevel");
            }
            System.out.println("aiThreadName : " + aiThreadName);
            System.out.println("debugLevel : " + debugLevel);
        %>
        <a href="debugBufferAI.jsp?process=1">AI erstellen</a><BR>
        <a href="debugBufferAI.jsp">Refresh</a><br/><br/>
        <form>
            <select id="aithreadname" onchange="window.location.href = 'debugBufferAI.jsp?aithreadname=' + document.getElementById('aithreadname').options[document.getElementById('aithreadname').selectedIndex].value + '&debugLevel=' + document.getElementById('debugLevel').options[document.getElementById('debugLevel').selectedIndex].value;">
                <option value="">ALL</option>
                <% for (String ai : AIDebugBuffer.getAIs()) {%>
                <option <% if (ai.equals(aiThreadName)) {%> selected <% }%> value="<%= ai%>"><%= ai%></option>
                <% }%>
            </select>
            <br/>
            <select id="debugLevel" onchange="window.location.href = 'debugBufferAI.jsp?aithreadname=' + document.getElementById('aithreadname').options[document.getElementById('aithreadname').selectedIndex].value + '&debugLevel=' + document.getElementById('debugLevel').options[document.getElementById('debugLevel').selectedIndex].value;">
                <option value="">ALL</option>
                <% for (AIDebugLevel level : AIDebugLevel.values()) {%>
                <option <% if (level.toString().equals(debugLevel)) {%> selected <% }%> value="<%= level.toString()%>"><%= level.toString()%></option>
                <% }%>
            </select>
        </form>
        <%
            if (request.getParameter("process") != null && request.getParameter("process").equals("1")) {
                AIUtilities.createAI();
            }

        %>
        <table>
            <%
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

                for (Map.Entry<Long, TreeMap<String, AIDebugEntry>> entry : AIDebugBuffer.getEntries(aiThreadName, debugLevel).entrySet()) {

                    for (Map.Entry<String, AIDebugEntry> ai : entry.getValue().entrySet()) {


            %>
            <tr>
                <td>
                    [<%= sdf.format(new Date(entry.getKey()))%>][<%= ai.getKey()%>][<%= ai.getValue().getLevel()%>] <%= ai.getValue().getMessage()%>
                </td>
            </tr>
            <%                    }
                }
            %>
        </table>
    </BODY>
</HTML>
