<%@page import="at.darkdestiny.core.admin.*" %>
<%
 /*
  * Hier wird das Menu f&uuml;r den Admin erzeugt
  */
	session = request.getSession();
	int userId = Integer.parseInt((String)session.getAttribute("userId"));
	AdminUser adminUser = new AdminUser(userId);
	
%>
<table>
<tr>
	<td><a href="main.jsp?page=overview&admin=0">Zum Spiel</a></td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
	<td><a href="main.jsp?page=index">Admin &uuml;bersicht</a></td>
</tr>
<tr>
	<td><a href="main.jsp?page=techtree">Der Technologiebaum</a></td>
</tr>
<tr>
	<td><a href="admin/techtreeimg.jsp" target="_blank">Der Technologiebaum als Bild</a></td>
</tr>
<tr>
	<td><a href="main.jsp?page=user">UserAdmin</a></td>
</tr>
<tr>
    <td><b><a href="main.jsp?page=planeteditor">Planeten Editor</a></b></td>
</tr>
<tr>
    <td><b><a href="main.jsp?page=systemeditor">System Editor</a></b></td>
</tr>
<tr>
    <td><b><a href="main.jsp?page=news">News</a></b></td>
</tr>
<tr>
    <td><b><a href="main.jsp?page=statistic">Statistiken</a></b></td>
</tr>
<tr>
	<td><a href="main.jsp?page=betaTools">BetaTool (kleine simulationen)</a></td>
</tr>
<tr>
	<td><a href="main.jsp?page=debugBuffer">DebugMeldungen</a></td>
</tr>
<tr>
	<td><a href="main.jsp?page=update">Status des Update-Threads</a></td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td><b>Einmalige Benutzung?</b></td>
</tr>
<tr>
	<td><a href="main.jsp?page=cleanViewTable">Clean View Tables</a></td>
</tr>
</table>