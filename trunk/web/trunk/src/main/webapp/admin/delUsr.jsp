<%@page import="java.sql.*" %>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.admin.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.utilities.DestructionUtilities" %>
<%
/*
 * Benutzer l&ouml;schen, nachdem eine Sicherheitsabfrage erfolgt ist
 */
	session = request.getSession();

if ((request.getParameter("user") != null) && (request.getParameter("confirmed") == null))
{
	int id = Integer.parseInt(request.getParameter("user"));
	Statement stmt = DbConnect.createStatement();
	ResultSet res = stmt.executeQuery("SELECT * FROM user WHERE id="+id);

	if (!res.next())
		throw new Exception("Benutzer nicht gefunden");
	%>
	Du m&ouml;chtest den Benutzer mit der ID <%=id %> l&ouml;schen?<br><br>
	Hier nochmal eine Zusammenfassung der Benutzerdaten:<br>
	<table>
	<tr><td>User-Name</td><td><%=res.getString("username") %></td></tr>
	<tr><td>Game-Name</td><td><%=res.getString("gamename") %></td></tr>
	<tr><td>EMail</td><td><%=res.getString("email") %></td></tr>
	</table><br>
	<a href="main.jsp?subCategory=99&page=admin/delUsr&user=<%= id%>&confirmed=1">Ja, diesen Benutzer l&ouml;schen</a>
	<%
	stmt.close();
}
else if (request.getParameter("user") != null) {
    String userId = (String)request.getParameter("user");
    if (userId != null) {
        DestructionUtilities.destroyPlayer(Integer.parseInt(userId));
    }
%>
    <jsp:forward page="/main.jsp?subCategory=99&page=admin/user"/>
<%
}

%>