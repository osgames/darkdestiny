<%-- 
    Document   : modifyModule
    Created on : 14.11.2009, 18:51:50
    Author     : Stefan
--%>

<%@page import="at.darkdestiny.core.admin.module.AdminModule"%>
<%@page import="at.darkdestiny.core.admin.module.ModuleConfiguration"%>
<%@page import="at.darkdestiny.core.admin.module.ModuleAttributeRel"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%
int moduleId = 0;

int userId = 1;
try{
    userId = Integer.parseInt((String)session.getAttribute("userId"));
    }catch(Exception e){
 }
if (request.getParameter("type") != null) {
    if (request.getParameter("type").equalsIgnoreCase("delete")) {
        int marId = Integer.parseInt(request.getParameter("id"));
        ModuleAttributeRel mar = ModuleConfiguration.getModuleAttribute(marId);
        moduleId = mar.getModuleId();
        ModuleConfiguration.deleteModuleAttributeRelation(marId);
    }
} else {
    moduleId = Integer.parseInt(request.getParameter("id"));
}

AdminModule am = ModuleConfiguration.getModule(moduleId);
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Modify Module <%= ML.getMLStr(am.getName(), userId) %></title>
    </head>
    <body>
        <h2>Modify Module <%= ML.getMLStr(am.getName(), userId) %> [<%= am.getId() %>]</h2><BR><BR>
        <TABLE border=1>
            <TR>
                <TH>Id</TH>
                <TH>Chassis</TH>
                <TH>Research</TH>
                <TH>Entry Type</TH>
                <TH>Value</TH>
                <TH>Ref Id</TH>
                <TH>Ref Type</TH>
                <TH>Applies To</TH>
                <TH>Actions</TH>
            </TR>
<%
            ArrayList<ModuleAttributeRel> relations = am.getAttributes();
            if (relations != null) {
                for (ModuleAttributeRel mar : relations) {
%>
            <TR>
                <TD><%= mar.getId() %></TD>
                <TD><%= ML.getMLStr(mar.getChassis().getName(), userId) %> [<%= mar.getChassis().getId() %>]</TD>
                <TD><%= mar.getResearch().getName() %> [<%= mar.getResearch().getId() %>]</TD>
                <TD><%= ML.getMLStr("js_" + mar.getAttributeName(), userId) %>&nbsp[<%= mar.getAttributeType() %>]</TD>
                <TD><%= mar.getValue() %></TD>
                <TD><%= mar.getRefId() %></TD>
                <TD><%= mar.getRefType() %></TD>
                <TD><%= mar.getAppliesTo() %></TD>
                <TD><A HREF="addModuleRelation.jsp?type=modify&id=<%= mar.getId() %>">Modify</A>&nbsp;<A HREF="modifyModule.jsp?id=<%= mar.getId() %>&type=delete">Delete</A></TD>
            </TR>        
<%
                }
            }
%>
            <TR>
                <TD colspan=8></TD>
                <TD><A HREF="addModuleRelation.jsp?id=<%= am.getId() %>">Insert new Relation</A></TD>
            </TR>
        </TABLE>
        <BR><BR><A HREF="moduleConfiguration.jsp">Zur�ck</A>
    </body>
</html>
