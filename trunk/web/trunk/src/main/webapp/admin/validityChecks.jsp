<%-- 
    Document   : validityChecks
    Created on : 25.11.2014, 20:02:40
    Author     : Stefan
--%>

<%@page import="at.darkdestiny.core.admin.ValidityChecks"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Validity Checks</title>
    </head>
    <body>
        <h1>Exectuting validity checks -- Please check DebugBuffer</h1>
<%
        out.write("<BR>DiplomacyRelation Result: " + !ValidityChecks.checkDiplomaticRelations() + "<BR>");
        out.write("<BR>SpaceStation Result: " + !ValidityChecks.checkSpaceStations() + "<BR>");
        out.write("<BR>Ship Result: " + !ValidityChecks.checkShips() + "<BR>");
%>        
    </body>
</html>
