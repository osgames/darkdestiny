<%-- 
    Document   : copyModuleRessCost
    Created on : 19.02.2010, 00:24:18
    Author     : Stefan
--%>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.dao.*" %>
<%@page import="at.darkdestiny.core.admin.module.*" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Copy Module Ress Cost</title>
    </head>
    <body>
        Start transferring entries ...
<%
        RessourceCost find = new RessourceCost();
        find.setType(RessourceCost.COST_MODULE);
        ArrayList<RessourceCost> rcList = Service.ressourceCostDAO.find(find);
        
        HashMap<Integer,ArrayList<RessourceCost>> moduleCost = 
                new HashMap<Integer,ArrayList<RessourceCost>>();
        
        for (RessourceCost rc : rcList) {
            if (moduleCost.containsKey(rc.getId())) {
                moduleCost.get(rc.getId()).add(rc);
            } else {
                ArrayList<RessourceCost> costEntries = new ArrayList<RessourceCost>();
                costEntries.add(rc);
                moduleCost.put(rc.getId(), costEntries);
            }
        }
        
        ArrayList<ModuleAttribute> modAttribList = Service.moduleAttributeDAO.findAll();
        HashMap<Integer,ArrayList<Integer>> chassisModMap = 
                new HashMap<Integer,ArrayList<Integer>>();
        
        for (ModuleAttribute ma : modAttribList) {
            if (chassisModMap.containsKey(ma.getModuleId())) {
                if (chassisModMap.get(ma.getModuleId()).contains(ma.getChassisId())) continue;
                chassisModMap.get(ma.getModuleId()).add(ma.getChassisId());
            } else {
                if (ma.getChassisId() != 0) {
                    ArrayList<Integer> chassisList = new ArrayList<Integer>();
                    chassisList.add(ma.getChassisId());
                    chassisModMap.put(ma.getModuleId(), chassisList);
                }
            }
        }
        
        for (Map.Entry<Integer,ArrayList<Integer>> modChassisEntry : chassisModMap.entrySet()) {
            int moduleId = modChassisEntry.getKey();
            ArrayList<Integer> chassisList = modChassisEntry.getValue();
            
            ArrayList<RessourceCost> ressCost = moduleCost.get(moduleId);
            for (Integer chassis : chassisList) {
                for (RessourceCost rcEntry : ressCost) {
                    ModuleAttribute maNew = new ModuleAttribute();
                    maNew.setAttributeId(63);
                    maNew.setModuleId(moduleId);
                    maNew.setChassisId(chassis);
                    maNew.setAttributeType(AttributeType.DEFINE);
                    maNew.setRefType(ReferenceType.RESSOURCE);
                    maNew.setRefId(rcEntry.getRessId());
                    maNew.setValue((double)rcEntry.getQty());
                    Service.moduleAttributeDAO.add(maNew);
                    out.println("Adding Ressource " + rcEntry.getRessId() + " for module " + moduleId + " and chassis " + chassis);
                    out.flush();
                }
            }
        }
%>
    </body>
</html>
