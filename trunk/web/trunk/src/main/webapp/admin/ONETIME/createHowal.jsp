<%@page import="java.sql.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%
if (session.getAttribute("REALLY_RUN_THIS_SCRIPT") == null) {
    throw new Exception("Diese Exception entfernen, um dies auszuf&uuml;hren");
}
Statement stmt = DbConnect.createStatement();
Statement stmt2 = DbConnect.createStatement();

ResultSet rs = stmt.executeQuery("SELECT id FROM planet WHERE (landType<>'A' AND landType<>'B')");
while (rs.next()) {
    boolean hasHowal = Math.random() < 0.2d;
    
    if (hasHowal) {
        boolean hasMuchHowal = Math.random() < 0.1d;
        int howalRess = 0;
        
        if (hasMuchHowal) {
            howalRess = (int)(500000 + Math.random() * 1000000);
        } else {
            howalRess = (int)(Math.random() * 10000);
        }
        
        stmt2.executeUpdate("UPDATE planet SET howalgonium="+howalRess+" WHERE id="+rs.getInt(1));
    }
}
%>