<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="java.sql.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.service.Service" %>
<%@page import="at.darkdestiny.core.service.AdminService" %>
<%@page import="at.darkdestiny.core.admin.*" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.statistics.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="org.jfree.chart.plot.PlotOrientation" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@page import="org.jfree.chart.*"%>
<%
            /*
             * Hier wird das User-Menu f&uuml;r den Admin erzeugt
             */



            session = request.getSession();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            new AdminUser(userId);

            final int TYPE_SHOW = 0;
            final int TYPE_CREATECATEGORY = 1;
            final int TYPE_CREATEENTRY = 2;
            final int TYPE_SHOWCHART = 3;
            final int PROCESS_DELETECATEGORY = 1;
            final int PROCESS_CREATECATEGORY = 2;
            final int PROCESS_CREATEENTRY = 3;
            final int PROCESS_DELETEENTRY = 4;


            int type = 0;
            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }

            int process = 0;
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            int statisticEntryId = 0;

            if (request.getParameter("statisticEntryId") != null) {
                statisticEntryId = Integer.parseInt(request.getParameter("statisticEntryId"));
            }

            if (process > 0) {
                Map params = request.getParameterMap();
                switch (process) {
                    case (PROCESS_CREATECATEGORY):
                        AdminService.createCategory(userId, params);
                        break;
                    case (PROCESS_CREATEENTRY):
                        AdminService.createEntry(userId, params);
                        break;
                    case (PROCESS_DELETEENTRY):
                        AdminService.deleteEntry(userId, params);
                        break;
                    case (PROCESS_DELETECATEGORY):
                        AdminService.deleteCategory(userId, params);
                        break;

                }
            }


            ArrayList<StatisticCategory> cats = Service.statisticCategoryDAO.findAll();
            ArrayList<StatisticEntry> entries = Service.statisticEntryDAO.findAll();

            String myImageMap = "";
            try {
                JFreeChart chart = null;
                if (statisticEntryId > 0) {
                    chart = StatisticChartCreater.createChart(statisticEntryId, userId);
                    if (session.getAttribute("chart") != null) {
                        session.removeAttribute("chart");
                    }
                    session.setAttribute("chart", chart);
                    // get ImageMap
                    ChartRenderingInfo info = new ChartRenderingInfo();
                    // populate the info
                    chart.createBufferedImage(500, 500, info);
                    myImageMap = ChartUtilities.getImageMap("map", info);
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in Statistik jsp", e);
            }

%>
<%= myImageMap%>
<%
            switch (type) {
                case (TYPE_SHOW):

%>
<TABLE align="center">
    <TR>
        <TD>
            <A href="main.jsp?subCategory=99&page=admin/statistic&type=<%= TYPE_CREATECATEGORY%>">Create Category</A>
        </TD>
    </TR>
    <TR>
        <TD>
            <A href="main.jsp?subCategory=99&page=admin/statistic&type=<%= TYPE_CREATEENTRY%>">Create Entry</A>
        </TD>
    </TR>
</TABLE>
<% if (cats.size() > 0) {%>
<TABLE class="blue" style="font-size: 13px;" width ="80%">
    <TR class="blue2">
        <TD>
            ID
        </TD>
        <TD>
            NAME
        </TD>
        <TD>
            ACTION
        </TD>
    </TR>
    <% }%>
    <% for (StatisticCategory sc : cats) {%>
    <TR>
        <TD>
            <%= sc.getId()%>
        </TD>
        <TD>
            <%= ML.getMLStr(sc.getName(), userId)%> <IMG border="0" alt="techtree" onmouseover="doTooltip(event,'<%= sc.getName()%>')" onmouseout="hideTip()" src="<%= GameConfig.getInstance().picPath()%>pic/infoT.png" />
        </TD>
        <TD>
            <IMG src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg" onclick="window.location.href = 'main.jsp?subCategory=99&page=admin/statistic&process=<%= PROCESS_DELETECATEGORY%>&categoryId=<%= sc.getId()%>'" />
        </TD>
    </TR>
    <% }%>

    <% if (cats.size() > 0) {%>
</TABLE>
<% }%>
<BR>
<% if (entries.size() > 0) {%>
<TABLE class="blue" style="font-size: 12px;" width ="80%">
    <TR class="blue2">
        <TD>
            ID
        </TD>
        <TD>
            Name
        </TD>
        <TD>
            Statement/Class
        </TD>
        <TD>
            RefType
        </TD>
        <TD>
            ChartType
        </TD>
        <TD>
            Category
        </TD>
        <TD>
            Vorschau
        </TD>
        <TD>
            Action
        </TD>
    </TR>
    <% }%>
    <% for (StatisticEntry se : entries) {%>
    <TR valign="top">
        <TD>
            <%= se.getId()%> 
        </TD>
        <TD>
            <%= ML.getMLStr(se.getName(), userId)%> <IMG border="0" alt="techtree" onmouseover="doTooltip(event,'<%= se.getName()%>')" onmouseout="hideTip()" src="<%= GameConfig.getInstance().picPath()%>pic/infoT.png" />
        </TD>
        <% if (se.getRefType() == EStatisticRefType.SELECTSTATEMENT) {%>
        <TD>
            <TEXTAREA readonly cols="20"  rows="2"> <%= se.getRef()%></TEXTAREA>
        </TD>
        <% } else {%>
        <TD>
            <%= se.getRef()%>
        </TD>
        <% }%>

        <% if (se.getRefType() == EStatisticRefType.SELECTSTATEMENT) {%>
        <TD>
            <IMG src="<%= GameConfig.getInstance().picPath()%>pic/sql.png">
        </TD>
        <% } else if (se.getRefType() == EStatisticRefType.CLAZZ) {%>
        <TD>
            <B>Clazz</B>
        </TD>
        <% }%>
        <% if (se.getType() == EStatisticType.BARCHART) {%>
        <TD>
            <IMG src="<%= GameConfig.getInstance().picPath()%>pic/barChart.png">
        </TD>
        <% } else if (se.getType() == EStatisticType.PIECHART) {%>
        <TD>
            <IMG src="<%= GameConfig.getInstance().picPath()%>pic/pieChart.png">
        </TD>
        <% }%>
        <TD>
            <%= se.getCategoryId()%>
        </TD>
        <TD>
            <IMG src="<%= GameConfig.getInstance().picPath()%>pic/view.jpg" onclick="window.location.href = 'main.jsp?subCategory=99&page=admin/statistic&type=<%= TYPE_SHOWCHART%>&statisticEntryId=<%= se.getId()%>'" />
        </TD>
        <TD>
            <IMG src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg" onclick="window.location.href = 'main.jsp?subCategory=99&page=admin/statistic&process=<%= PROCESS_DELETEENTRY%>&entryId=<%= se.getId()%>'" />
        </TD>
    </TR>
    <% }%>

    <% if (entries.size() > 0) {%>
</TABLE>
<% }
                    break;

                case (TYPE_CREATECATEGORY):
%>
<FORM method="post" action="main.jsp?subCategory=99&page=admin/statistic&process=<%= PROCESS_CREATECATEGORY%>">
    <TABLE align="center">
        <TR>
            <TD>
                <INPUT name="categoryName" type="text">
            </TD>
        </TR>
        <TR>
            <TD>
                <INPUT type="submit" value="create"/>
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                    break;


                case (TYPE_CREATEENTRY):
%>
<FORM  method="post" action="main.jsp?subCategory=99&page=admin/statistic&process=<%= PROCESS_CREATEENTRY%>">
    <TABLE class="blue" style="font-size: 13px;" width ="80%">
        <TR class="blue2">
            <TD>
                Name
            </TD>
            <TD>
                xTitle
            </TD>
            <TD>
                yTitle
            </TD>
            <TD>
                Statement/Class
            </TD>
            <TD>
                RefType
            </TD>
            <TD>
                ChartType
            </TD>
            <TD>
                Category
            </TD>
        </TR>
        <TR valign="top">
            <TD>
                <INPUT name="name" type="text">
            </TD>
            <TD>
                <INPUT name="xTitle" type="text">
            </TD>
            <TD>
                <INPUT name="yTitle" type="text">
            </TD>
            <TD>
                <TEXTAREA name="ref" rows="20" cols="20"></TEXTAREA>
            </TD>
            <TD>
                <SELECT  name="refType" >
                    <% for (EStatisticRefType srt : EStatisticRefType.values()) {%>
                    <OPTION value="<%= srt.toString()%>"><%= srt.toString()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
            <TD>
                <SELECT name="statType">
                    <% for (EStatisticType st : EStatisticType.values()) {%>
                    <OPTION value="<%= st.toString()%>"><%= st.toString()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
            <TD>
                <SELECT  name="categoryId">
                    <% for (StatisticCategory sc : cats) {%>
                    <OPTION value="<%= sc.getId()%>"><%= sc.getName()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
        </TR>
        <TR>
            <TD colspan="6" align="center">
                <INPUT type="submit" value="create"/>
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                    break;

                case (TYPE_SHOWCHART):
%>
<TABLE align="center">
    <TR>
        <TD align="center">
            <IMG src="StatChartViewer" usemap="#map">
        </TD>
    </TR>
    <TR>
        <TD align="center">
            <A href="main.jsp?subCategory=99&page=admin/statistic" >Zur�ck</A>
        </TD>
    </TR>
</TABLE>
<%
                    break;
            }
%>

