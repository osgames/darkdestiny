<%@page import="org.slf4j.Logger"%>
<%@page import="at.darkdestiny.core.statistic.CalculateImperialOverview"%>
<%@page import="at.darkdestiny.core.statistic.UserStatisticsUpdater"%>
<%@page import="at.darkdestiny.core.enumeration.EPassword"%>
<%@page import="at.darkdestiny.core.utilities.MD5"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel"%>
<%
    DebugBuffer.addLine(DebugLevel.WARNING, "Statistic jsp got called from: " + request.getRemoteAddr());


    try {

        String pass = request.getParameter("password");

        CheckForUpdate.requestUpdate();
        // String pass = request.getParameter("pass");
        // if (pass.equals("cs")) {
        DebugBuffer.addLine(DebugLevel.DEBUG, "Calculation of statistic started");

        UserStatisticsUpdater updater = new UserStatisticsUpdater();
        updater.setName("Statistic Update");
        updater.start();

        /* Interne Statistikberechnung */
        CalculateImperialOverview test = new CalculateImperialOverview();
        test.start();
        test.setName("Imperial Statistic Update");
        out.println("Imperial Statistic Calculation done");

        DebugBuffer.addLine(DebugLevel.DEBUG, "Calculation of statistic ended");
    } catch (Exception e) {
        DebugBuffer.writeStackTrace("Error in statistic: ", e);
        e.printStackTrace();
    }
%>
