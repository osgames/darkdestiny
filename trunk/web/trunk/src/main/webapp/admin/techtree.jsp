<%@page import="at.darkdestiny.core.admin.*" %>
<%@page import="at.darkdestiny.core.admin.techtree.actions.*" %>
<%
	TechTreeAdmin tta = new TechTreeAdmin();
	AdminUser adminUser  = null;
	if (!TechTreeAdmin.DEBUG) {
		session = request.getSession();
		int userId = Integer.parseInt((String)session.getAttribute("userId"));
		adminUser = new AdminUser(userId);
		tta.checkUser(adminUser);
	} else {
	    TechTreeAdmin.BASEPATH = "techtree.jsp?";
	}
	
	
	if (request.getParameter("reload") != null) {
		TechTreeAdmin.forceReload();
	}
	
	IMyPageProducer impp = null;
	
	if (request.getParameter("action") != null) {
		switch (Integer.parseInt((String)request.getParameter("action")))
			{
			case TechTreeAdmin.ACTION_FULL_TREE: {
				impp = new TreeProducer();
				break;
			}
			case TechTreeAdmin.ACTION_ONE_ITEM: {
				int id = Integer.parseInt((String)request.getParameter("id"));
				impp = new TechnoProducer(id,request);
				break;
			}
			case TechTreeAdmin.ACTION_ADD_DEPENDENCY: {
				int id = -1;
				int tid = -1;
				if (request.getParameter("id") != null)
					id = Integer.parseInt((String)request.getParameter("id"));
				if (request.getParameter("tid") != null)
					tid = Integer.parseInt((String)request.getParameter("tid"));
				impp = new AddTechnoProducer(id,tid);
				break;
			}
			case TechTreeAdmin.ACTION_DELETE_DEPENDENCY: {
				int id = -1;
				int tid = -1;
				boolean sure = false;
				if (request.getParameter("id") != null)
					id = Integer.parseInt((String)request.getParameter("id"));
				if (request.getParameter("tid") != null)
					tid = Integer.parseInt((String)request.getParameter("tid"));
				if (request.getParameter("sure") != null)
					sure = (Integer.parseInt((String)request.getParameter("sure")) != 0);
				impp = new DeleteTechnoProducer(id,tid, sure);
				break;
			}
			case TechTreeAdmin.ACTION_DELETE_TECH: {
				int id = -1;
				boolean sure = false;
				if (request.getParameter("id") != null)
					id = Integer.parseInt((String)request.getParameter("id"));
				if (request.getParameter("sure") != null)
					sure = (Integer.parseInt((String)request.getParameter("sure")) != 0);
				DeleteTechnoAction dta = new DeleteTechnoAction(id,sure);
				if (dta.doAction())
					impp = new TreeProducer();
				else impp = dta;
				break;
			}
			}
	}
	if (impp == null) {
		impp = new TreeProducer();
	}
		
	impp.setup(tta);
	
	%>
<table border="1">
	<%= impp.producePage()	%>
</table>