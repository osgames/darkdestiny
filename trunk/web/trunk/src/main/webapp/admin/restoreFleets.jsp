<%-- 
    Document   : restoreFleets
    Created on : Apr 5, 2012, 6:09:01 AM
    Author     : Admin
--%>

<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
  int userId = Integer.parseInt((String) session.getAttribute("userId"));

  ArrayList<BaseResult> results = new ArrayList<BaseResult>();
        int type = 1;
        final int TYPE_SHOWALL = 1;
        final int TYPE_SHOWBATTLELOG = 2;;
        final int TYPE_RESTORE = 3;
        if (request.getParameter("type") != null) {
            type = Integer.parseInt(request.getParameter("type"));
        }
        if(type == TYPE_RESTORE){
              int battleLogId = Integer.parseInt((String) request.getParameter("battleLogId"));
              results.addAll(AdminService.restoreFleets(battleLogId));
            type = TYPE_SHOWALL;
        }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>BattleLog Restore</h1>
        <TABLE>
        <% for (BaseResult br : results) {%>
        <TR><TD>
            <%= br.getMessage() %>
            </TD>
        </TR>
            <% }%>
        </TABLE>
        <% 
        switch(type){
            case(TYPE_SHOWALL):
         %>
        <FORM action="main.jsp?page=admin/restoreFleets&type=<%= TYPE_SHOWBATTLELOG %>" method="post">
        <TABLE class="bluetrans">
            <TR>
                <TD>
                    BattleLog-Id
                </TD>
                <TD>
                    <INPUT type="textfield" name="battleLogId" size="10"/>
                </TD>
            </TR>
            <TR>
                <TD colspan="2" align="center">
                    
                    <INPUT type="submit" size="10" />
                </TD>
            </TR>
        </TABLE>
          </FORM>
        <% 
        break;
        case(TYPE_SHOWBATTLELOG):
              int battleLogId = Integer.parseInt((String) request.getParameter("battleLogId"));
              
             BattleLogRestoreResult blrr = AdminService.getBattleLogRestore(battleLogId);
             HashSet<Integer> users = new HashSet<Integer>(); 
             HashSet<Integer> designs = new HashSet<Integer>();
             int shipCount = 0;
             %>
        <FORM action="main.jsp?page=admin/restoreFleets&type=<%= TYPE_RESTORE %>" method="post">
             <TABLE class="bluetrans">
            <TR>
                <TD>
                    BattleLog-Id
                </TD>
                <TD>
                    <%= blrr.getBatteLogId() %>
                </TD>
            </TR>
            <TR>
                <TD colspan="2" align="center">
                    <TABLE class="bluetrans">
                        <TR class="blue2">
                            <TD>
                                Design-Id
                            </TD>
                            <TD>Design-Name
                            </TD>
                            <TD>Count</TD>
                            <TD>UserId</TD>
                            <TD>User-Name</TD>
                            <TD>HomePlanet-Name</TD>
                            <TD>HomePlanet-Coordinates</TD>
                        </TR>
                        <% for(BattleLogRestoreEntry blre : blrr.getEntries()){ 
                                shipCount += blre.getBattleLogEntry().getCount();
                                if(!designs.contains(blre.getDesign().getId())){
                                    designs.add(blre.getDesign().getId());
                                }
                                if(!users.contains(blre.getUser().getUserId())){
                                    users.add(blre.getUser().getUserId());
                                }
                        %>
                        <TR>
                            <TD><%= blre.getDesign().getId() %></TD>
                            <TD><%= blre.getDesign().getName() %></TD>
                            <TD><%= blre.getBattleLogEntry().getCount() %></TD>
                            <TD><%= blre.getUser().getUserId() %></TD>
                            <TD><%= blre.getUser().getGameName() %></TD>
                            <TD><%= blre.getPlayerPlanet().getName() %></TD>
                            <TD><%= blre.getPlanet().getSystemId() %> : <%= blre.getPlanet().getId() %></TD>
                        </TR>
                        <% } %>
                    </TABLE>
                </TD>
            </TR>
                        <TR class="blue2">
                            <TD colspan="2">To Restore</TD>
                        </TR>
                        <TR>
                            <TD>
                                <%= shipCount %>
                            </TD>
                            <TD>
                                Ships
                            </TD>
                        </TR>
                        <TR>
                            <TD>
                                <%= designs.size() %>
                            </TD>
                            <TD>
                                Designs
                            </TD>
                        </TR>
                        <TR>
                            <TD>
                                <%= users.size() %>
                            </TD>
                            <TD>
                                Users
                            </TD>
                        </TR>
                        <TR>
                            <TD>
                                <INPUT type="hidden" name="battleLogId" value="<%= battleLogId %>" />
                                <INPUT type="submit" value="Restore" />
                            </TD>
                        </TR>
        </TABLE>
    </FORM>
             <%
            break;
         } %>
     
    </body>
</html>
