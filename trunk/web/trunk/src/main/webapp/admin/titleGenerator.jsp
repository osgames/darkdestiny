
<%@page import="at.darkdestiny.core.service.Service" %>
<%@page import="at.darkdestiny.core.service.TitleService" %>
<%@page import="at.darkdestiny.core.model.Language" %>
<%@page import="at.darkdestiny.core.model.Title" %>
<%@page import="at.darkdestiny.core.model.Condition" %>
<%@page import="at.darkdestiny.core.model.ConditionParameter" %>
<%@page import="at.darkdestiny.core.model.Campaign" %>
<%@page import="at.darkdestiny.core.model.ConditionToTitle" %>
<%@page import="at.darkdestiny.core.model.ParameterToTitle" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="at.darkdestiny.core.GameConfig" %>
<%@page import="java.util.*" %>
<%
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            ArrayList<BaseResult> result = new ArrayList<BaseResult>();
            final int TYPE_SHOW_ALL = 0;
            final int TYPE_CREATE_TITLE = 1;
            final int TYPE_SHOW_CONDITIONS = 2;
            final int TYPE_CONDITION_INFO = 3;
            final int TYPE_SHOW_TITLE = 4;
            final int TYPE_ASSIGN_CONDITION = 5;
            final int TYPE_CREATE_CONDITION = 6;
            final int TYPE_ADD_PARAMETER = 7;
            final int TYPE_GENERATE_JAVA_TITLE = 8;
            final int TYPE_GENERATE_JAVA_CONDITION = 9;

            final int PROCESS_CREATE_TITLE = 1;
            final int PROCESS_ASSIGN_CONDITION = 2;
            final int PROCESS_UNASSIGN_CONDITION = 3;
            final int PROCESS_CREATE_CONDITION = 4;
            final int PROCESS_ADD_PARAMETER = 5;
            final int PROCESS_DELETE_PARAMETER = 6;
            final int PROCESS_GENERATE_JAVA_TITLE = 7;
            final int PROCESS_GENERATE_JAVA_CONDITION = 8;
            final int PROCESS_DELETE_TITLE = 9;
            final int PROCESS_DELETE_CONDITION = 10;



            int type = TYPE_SHOW_ALL;
            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }
            int process = 0;
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            int titleId = 0;
            if (request.getParameter("titleId") != null) {
                titleId = Integer.parseInt(request.getParameter("titleId"));
            }
            int conditionId = 0;
            if (request.getParameter("conditionId") != null) {
                conditionId = Integer.parseInt(request.getParameter("conditionId"));
            }

            if (process > 0) {
                switch (process) {
                    case (PROCESS_CREATE_TITLE): {
                        result = TitleService.addTitle(userId, request.getParameterMap());
                        break;
                    }
                    case (PROCESS_ASSIGN_CONDITION): {
                        result = TitleService.assignCondition(userId, request.getParameterMap());
                        type = TYPE_SHOW_TITLE;
                        break;
                    }
                    case (PROCESS_UNASSIGN_CONDITION): {
                        result = TitleService.unAssignCondition(userId, request.getParameterMap());
                        type = TYPE_SHOW_TITLE;
                        break;
                    }
                    case (PROCESS_CREATE_CONDITION): {
                        result = TitleService.createCondition(userId, request.getParameterMap());
                        type = TYPE_SHOW_CONDITIONS;
                        break;
                    }
                    case (PROCESS_ADD_PARAMETER): {
                        result = TitleService.addParameter(userId, request.getParameterMap());
                        type = TYPE_SHOW_CONDITIONS;
                        break;
                    }

                    case (PROCESS_DELETE_PARAMETER): {
                        result = TitleService.deleteParameter(userId, request.getParameterMap());
                        type = TYPE_SHOW_CONDITIONS;
                        break;
                    }
                    case (PROCESS_GENERATE_JAVA_TITLE): {
                        result = TitleService.generateJavaTitle(userId, request.getParameterMap());
                        type = TYPE_SHOW_ALL;
                        break;
                    }
                    case (PROCESS_GENERATE_JAVA_CONDITION): {
                        result = TitleService.generateJavaCondition(userId, request.getParameterMap());
                        type = TYPE_SHOW_CONDITIONS;
                        break;
                    }
                    case (PROCESS_DELETE_TITLE): {
                        result = TitleService.deleteTitle(userId, request.getParameterMap());
                        type = TYPE_SHOW_ALL;
                        break;
                    }
                    case (PROCESS_DELETE_CONDITION): {
                        result = TitleService.deleteCondition(userId, request.getParameterMap());
                        type = TYPE_SHOW_CONDITIONS;
                        break;
                    }

                }
            }

%>
<center><h4>Title Generator v1.0</h4></center>
<% for (BaseResult br : result) {%>
<% if (br.isError()) {%>
<CENTER><B><FONT color="RED"><%= br.getMessage()%></FONT></B></CENTER>
<% } else {%>
<CENTER><B><FONT color="GREEN"><%= br.getMessage()%></FONT></B></CENTER>
<% }%>
<%
            }%>
<TABLE align="center" width="80%">
    <TR  class="blue2">
        <TD width="50%" align="center"><A href="main.jsp?page=admin/titleGenerator&subCategory=99" >Titel</A></TD>
        <TD width="50%" align="center"><A href="main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_SHOW_CONDITIONS%>" >Vorbedingungen</A></TD>
    </TR>
</TABLE>
<BR>
<%
            switch (type) {

                case (TYPE_SHOW_ALL): {
%>
<CENTER><A href="main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_CREATE_TITLE%>" >Neuer Titel</A></CENTER>
<TABLE style="font-size: 12px" align="center" width="80%">
    <TR class="blue2">
        <TD>
            Name
        </TD>
        <TD>
            Type
        </TD>
        <TD>
            Difficulty
        </TD>
        <TD>
            Points
        </TD>
        <TD>
            Sichtbarkeit (f�r User)
        </TD>
        <TD width="40px" colspan="3">
            Actions
        </TD>
    </TR>
    <% for (Title t : (ArrayList<Title>) Service.titleDAO.findAll()) {%>
    <TR>
        <TD>
            <%= ML.getMLStr(t.getName() + "_" + Service.userDAO.findById(userId).getGender().toString(), userId)%>
        </TD>
        <TD>
            <%= t.getType()%>
        </TD>
        <TD>
            <%= t.getDifficulty()%>
        </TD>
        <TD>
            <%= t.getPoints()%>
        </TD>
        <TD>
            <%= t.getVisible()%>
        </TD>
        <TD>
            <IMG style="cursor:pointer;" alt="Details anzeigen" src="<%= GameConfig.getInstance().picPath()%>pic/view.jpg" onmouseover="doTooltip(event,'Details anzeigen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_SHOW_TITLE%>&titleId=<%= t.getId()%>'"/>
        </TD>
        <TD>
            <IMG style="cursor:pointer;" alt="" src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'L�schen des Titels')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_DELETE_TITLE%>&titleId=<%= t.getId()%>'"/>
        </TD>
        <TD>
            <IMG style="cursor:pointer;" alt="" src="<%= GameConfig.getInstance().picPath()%>pic/java.gif" onmouseover="doTooltip(event,'Generiere Java Klasse')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_GENERATE_JAVA_TITLE%>&titleId=<%= t.getId()%>'"/>
        </TD>
    </TR>
    <% }%>
</TABLE>
<%
                    break;
                }
                case (TYPE_SHOW_TITLE): {
%>
<FORM action="main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_CREATE_TITLE%>" method="post">
    <TABLE style="font-size: 12px" align="center" width="80%">
        <TR class="blue2">
            <TD>
                Name
            </TD>
            <TD>
                Type
            </TD>
            <TD>
                Difficulty
            </TD>
            <TD>
                Points
            </TD>
            <TD>
                Sichtbarkeit (f�r User)
            </TD>
            <TD width="40px">
                Actions
            </TD>
        </TR>
        <%
                            Title t = Service.titleDAO.findById(titleId);
        %>
        <TR>
            <TD>
                <%= ML.getMLStr(t.getName() + "_" + Service.userDAO.findById(userId).getGender().toString(), userId)%>
            </TD>
            <TD>
                <%= t.getType()%>
            </TD>
            <TD>
                <%= t.getDifficulty()%>
            </TD>
            <TD>
                <%= t.getPoints()%>
            </TD>
            <TD>
                <%= t.getVisible()%>
            </TD>
            <TD>
                <IMG style="cursor:pointer;" alt="" src="<%= GameConfig.getInstance().picPath()%>pic/addFilter.png" onmouseover="doTooltip(event,'Bedingung hinzuf�gen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_ASSIGN_CONDITION%>&titleId=<%= t.getId()%>'"/>
            </TD>
        </TR>
        <TR>
            <TD class="blue2">
                Beschreibung
            </TD>
        </TR>
        <TR>
            <TD>
                <%= ML.getMLStr(t.getDescription(), userId)%>
            </TD>
        </TR>
    </TABLE>
    <TABLE style="font-size: 13px" width="80%" align="center">
        <TR class="blue2">
            <TD align="center" colspan="6">
                <B>Vorbedingungen</B>
            </TD>
        </TR>
        <% for (ConditionToTitle ctt : (ArrayList<ConditionToTitle>) Service.conditionToTitleDAO.findByTitleId(titleId)) {
                                Condition c = Service.conditionDAO.findById(ctt.getConditionId());
        %>

        <TR class="blue2">
            <TD>
                Id
            </TD>
            <TD>
                Order
            </TD>
            <TD>
                Beschreibung
            </TD>
            <TD>
                Klasse
            </TD>
            <TD>
                Conditions
            </TD>
            <TD width="20px">
                <IMG style="cursor:pointer;" alt="" src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'Bedingung Entfernen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_UNASSIGN_CONDITION%>&conditionToTitleId=<%= ctt.getId()%>&titleId=<%= titleId%>'"/>
            </TD>
        </TR>
        <TR>
            <TD>
                <%= ctt.getId() %>
            </TD>
            <TD>
                <%= ctt.getOrder() %>
            </TD>
            <TD>
                <%= ML.getMLStr(ctt.getDescription(), userId)%>
            </TD>
            <TD>
                <%= c.getClazz().replace("at.darkdestiny.core.title.condition.", "")%>
            </TD>
            <TD colspan="2">
                <TABLE style="font-size: 13px" width="100%">
                    <TR class="blue2">
                        <TD>
                            Id
                        </TD>
                        <TD>
                            Name
                        </TD>
                        <TD>
                            Type
                        </TD>
                        <TD>
                            Value
                        </TD>
                    </TR>
                    <% for (ConditionParameter cp : Service.conditionParameterDAO.findByConditionIdSortedByOrder(c.getId())) {
                             ParameterToTitle ptt = Service.parameterToTitleDAO.findBy(ctt.getId(), cp.getId());
                    %>
                    <TR>
                        <TD>
                            <%= cp.getOrder()%>
                        </TD>
                        <TD>
                            <%= cp.getName()%>
                        </TD>
                        <TD>
                            <%= cp.getType()%>
                        </TD>
                        <TD>
                            <%= ptt.getValue()%>
                        </TD>
                    </TR>
                    <% }%>
                </TABLE>
            </TD>
        </TR>
        <% }%>
    </TABLE>
</FORM>
<%
                    break;
                }
                case (TYPE_CREATE_TITLE): {
%>
<FORM action="main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_CREATE_TITLE%>" method="post">
    <TABLE style="font-size: 12px" align="center" width="80%">
        <TR class="blue2">
            <TD align="center" colspan="2">
                Neuer Titel
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Multilanguage Name
            </TD>
            <TD>
                db_title_<INPUT name="titleName" type="input" />
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Kampagne
            </TD>
            <TD>
                <SELECT name="campaignId">
                    <% for(Campaign c : (ArrayList<Campaign>)Service.campaignDAO.findAll()){ %>
                    <OPTION value="<%= c.getId() %>"><%= ML.getMLStr(c.getName(), userId) %></OPTION>
                    <% } %>
                </SELECT>
            </TD>
        </TR>
        <TR>
            <TD>
                &nbsp;
            </TD>
        </TR>
        <%

                            for (Language l : Service.languageDAO.findAll()) {
                                if(l.getId() != l.getMasterLanguageId()){
                                    continue;
                                }
                                for(EUserGender gender : EUserGender.values()){
        %>
        <TR>
            <TD align="center">
                Name [<%= ML.getMLStr(l.getName(), userId)%>] [<%= gender.toString() %>]
            </TD>
            <TD>
                <INPUT name="lang_<%= l.getId()%>_name_<%= gender.toString() %>" type="input" />
            </TD>
        </TR>
        <%          }
                            }
                            for (Language l : Service.languageDAO.findAll()) {
                                if(l.getId() != l.getMasterLanguageId()){
                                    continue;
                                }
        %>
        <TR>
            <TD align="center">
                Beschreibung [<%= ML.getMLStr(l.getName(), userId)%>]
            </TD>
            <TD>
                <textarea style="width:100%;" rows="5" name="lang_<%= l.getId()%>_description"></textarea>
            </TD>
        </TR>
        <%
                            }
        %>
        <TR>
            <TD align="center">
                Difficulty
            </TD>
            <TD>
                <INPUT type="textfield" name="difficulty" />
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Points
            </TD>
            <TD>
                <INPUT type="textfield" name="points" />
            </TD>
        </TR>
        <TR>
            <TD colspan="2">
                Classname: In der Klasse (Bsp.: at.darkdestiny.core.title.Colonist) werden die n�tigen
                Funktionen von Title geerbt und m�ssen implementiert werden.
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Classname
            </TD>
            <TD>
                at.darkdestiny.core.title.<INPUT type="textfield" name="clazz" />
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Sichbarkeit (f�r User)
            </TD>
            <TD>
                <INPUT type="checkbox" name="visibility" checked />
            </TD>
        </TR>
        <TR>
            <TD align="center" >
                Type
            </TD>
            <TD>
                <SELECT name="titleType">
                    <% for (ETitleType e : ETitleType.values()) {%>

                    <OPTION value="<%= e.toString()%>"><%= e.toString()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
        </TR>
        <TR>
            <TD colspan="2" align="center">
                <INPUT type="submit" value="Weiter" />
            </TD>

        </TR>
    </TABLE>
</FORM>
<%
                    break;
                }
                case (TYPE_ASSIGN_CONDITION): {
                    if (conditionId == 0) {
%>
<FORM action="main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_ASSIGN_CONDITION%>" method="post">
    <TABLE style="font-size: 12px" align="center" width="80%">
        <INPUT type="hidden" name="titleId" value="<%= titleId%>" />
        <TR class="blue2">
            <TD>
                Condition
            </TD>
            <TD>
                <SELECT name="conditionId">
                    <% for (Condition c : (ArrayList<Condition>) Service.conditionDAO.findAll()) {%>
                    <OPTION value="<%= c.getId()%>"><%= c.getDescription()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
        </TR>
        <TR>
            <TD align="center" colspan="4">
                <INPUT type="submit" name="continue" value="Weiter" />
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                    } else {
                        Condition c = Service.conditionDAO.findById(conditionId);
%>
<FORM action="main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_ASSIGN_CONDITION%>" method="post">
    <TABLE style="font-size: 12px" align="center" width="80%">
        <INPUT type="hidden" name="conditionId" value="<%= conditionId%>" />
        <INPUT type="hidden" name="titleId" value="<%= titleId%>" />
        <TR class="blue2">
            <TD colspan="2">
                Condition
            </TD>
            <TD colspan="2">
                <B>
                    <%= c.getDescription()%>
                </B>
            </TD>
        </TR>
        <TR class="blue2">
            <TD colspan="2">
                Position (Bedingungen werden geordnet)
            </TD>
            <TD colspan="2">
                <SELECT name="order">
                    <% for (int i = 1; i < 20; i++) {%>
                    <OPTION value="<%= i%>"><%= i%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
        </TR>
        <TR>
            <TD colspan="2">

            </TD>
            <TD class="blue2" colspan="2">
                Beschreibung (Bsp.: Besiedeln sie 4 Planeten)
            </TD>
        </TR>
        <%
                                for (Language l : Service.languageDAO.findAll()) {
                                if(l.getId() != l.getMasterLanguageId()){
                                    continue;
                                }
        %>
        <TR>
            <TD align="center" colspan="2">
                Beschreibung [<%= ML.getMLStr(l.getName(), userId)%>]
            </TD>
            <TD colspan="2">
                <INPUT style="width:100%" name="lang_<%= l.getId()%>_description" type="input" />
            </TD>
        </TR>
        <% }%>
        <TR>
            <TD colspan="4">
                Diese Vorbedingung verlangt es dass sie folgende�bergabeparameter an den Konstruktor spezifizieren
            </TD>
        </TR>
        <TR>
            <TD>
                &nbsp;
            </TD>
        </TR>
        <TR class="blue2">
            <TD>
                Position
            </TD>
            <TD>
                Name des Parameters
            </TD>
            <TD>
                Type
            </TD>
            <TD>
                Wert
            </TD>
        </TR>
        <% for (ConditionParameter cp : Service.conditionParameterDAO.findByConditionIdSortedByOrder(conditionId)) {%>
        <TR>
            <TD>
                <%= cp.getOrder()%>
            </TD>
            <TD>
                <%= cp.getName()%>
            </TD>
            <TD>
                <%= cp.getType()%>
            </TD>
            <TD>
                <INPUT name="condition_value_<%= cp.getId()%>" type="textfield" style="width:100%"/>
            </TD>
        </TR>
        <% }%>
        <TR>
            <TD align="center" colspan="4">
                <INPUT type="submit" name="continue" value="Weiter" />
            </TD>
        </TR>
    </TABLE>
</FORM>
<%            }
                    break;
                }
                case (TYPE_SHOW_CONDITIONS): {
%>
<CENTER><A href="main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_CREATE_CONDITION%>">Neue Bedingung</A></CENTER>
<TABLE style="font-size: 13px" align="center" width="80%">
    <TR class="blue2">
        <TD colspan="2">
            Action
        </TD>
        <TD>
            Description
        </TD>
        <TD>
            Clazz
        </TD>
        <TD>
            Parameter
        </TD>
    </TR>
    <% for (Condition tc : (ArrayList<Condition>) Service.conditionDAO.findAll()) {%>
    <TR>
        <TD valign="top">
            <IMG style="cursor:pointer;" alt="" src="<%= GameConfig.getInstance().picPath()%>pic/java.gif" onmouseover="doTooltip(event,'Generiere Java Klasse')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_GENERATE_JAVA_CONDITION%>&conditionId=<%= tc.getId()%>'"/>
        </TD>
        <TD valign="top">
            <IMG style="cursor:pointer;" alt="" src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'L�sche Bedingung')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_DELETE_CONDITION%>&conditionId=<%= tc.getId()%>'"/>
        </TD>
        <TD valign="top">
            <%=  tc.getDescription()%>
        </TD>
        <TD valign="top">
            <%= tc.getClazz().replace("at.darkdestiny.core.title.condition.", "")%>
        </TD>
        <TD valign="top">
            <TABLE style="font-size: 13px" width="100%">
                <TR class="blue2">
                    <TD>
                        Id
                    </TD>
                    <TD>
                        Name
                    </TD>
                    <TD>
                        Type
                    </TD>
                    <TD>
                        &nbsp;
                    </TD>
                </TR>
                <% for (ConditionParameter cp : Service.conditionParameterDAO.findByConditionIdSortedByOrder(tc.getId())) {%>
                <TR>
                    <TD>
                        <%= cp.getOrder()%>
                    </TD>
                    <TD>
                        <%= cp.getName()%>
                    </TD>
                    <TD>
                        <%= cp.getType()%>
                    </TD>
                    <TD>
                        <IMG style="cursor:pointer;" alt="" src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'Parameter entfernen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_DELETE_PARAMETER%>&conditionParameterId=<%= cp.getId()%>'"/>
                    </TD>
                </TR>
                <% }%>
                <TR>
                    <TD colspan="4" align="right">
                        <IMG style="cursor:pointer;" alt="" src="<%= GameConfig.getInstance().picPath()%>pic/addFilter.png" onmouseover="doTooltip(event,'Parameter hinzuf�gen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=admin/titleGenerator&subCategory=99&type=<%= TYPE_ADD_PARAMETER%>&conditionId=<%= tc.getId()%>'"/>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
    <%
                        }%>
</TABLE>
<%
                    break;
                }
                case (TYPE_ADD_PARAMETER): {
%>
<FORM action="main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_ADD_PARAMETER%>" method="post">
    <TABLE style="font-size: 13px" align="center" width="80%">
        <INPUT type="hidden" name="conditionId" value="<%= conditionId%>" />
        <TR>
            <TD>
                Order (Position im Konstruktor)
            </TD>
            <TD>
                <SELECT name="order">
                    <% for (int i = 1; i < 20; i++) {%>
                    <OPTION value="<%= i%>"><%= i%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
        </TR>
        <TR>
            <TD>
                Name der Variable (Bsp.: planetCount)
            </TD>
            <TD>
                <INPUT type="text" name="name" />
            </TD>
        </TR>
        <TR>
            <TD>
                Type
            </TD>
            <TD>
                <SELECT name="conditionType">
                    <% for (EConditionType e : EConditionType.values()) {%>
                    <OPTION value="<%= e.toString()%>"><%= e.toString()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
        </TR>
        <TR>
            <TD colspan="2" align="center">
                <INPUT type="submit" value="Weiter" />
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                    break;
                }
                case (TYPE_CONDITION_INFO): {
%>
<CENTER>
    <TABLE style="font-size: 13px" align="center" width="70%">
        <TR>
            <TD>
                Title selbst verf�gen �ber eine Funktion die Auskunft gibt ob ein Spieler diesen Rang bekommt:<BR>
                <B>public boolean check(int userId)</B><BR>

                Neben dieser Funktion k�nnen einem Title auch ein oder mehrere Conditions(Bedingungen)
                zugeordnet werden. Eine Condition erbt von at.darkdestiny.core.title.TitleCondition welche wiederum
                ITitleCondition erbt.<BR><BR>
                Conditions sind dazu gedacht viele Vorraussetzungen von Titeln in viele kleine aufzuteilen und vorallem
                wiederkehrende Vorraussetzungen zu verwenden.<BR>
                Bsp.:<BR><B>
                    Titel Kolonisator => Anzahl der Planeten = 2<BR>
                    Titel Imperator => Anzahl der Planeten = 10<BR>
                    Titel Gro�imperator => Anzahl der Planeten = 50<BR></B>
                <BR><BR>
                Hierf�r hat eine Condition die M�glichkeit einen eigenen Constructor zu spezifizieren mit
                einer Reihe von ConditionParametern. Mit ihnen kann dann in der <B>check(int userId)</B>-Funktion der Condition gearbeitet werden<BR><BR>
                M�gliche Typen : <B><BR><% for (EConditionType ct : EConditionType.values()) {%><%= ct.toString()%><BR><% }%></B>
                Bsp.:<BR>
                <B>at.darkdestiny.core.title.condition.PlanetCount</B> mit dem Cunstroctor:<BR><BR>
                <B>public PlanetCount(ConditionParameter planetCount, ConditionParameter comparator)</B><BR>
            </TD>
        </TR>
    </TABLE>
</CENTER>

<%
                    break;
                }

                case (TYPE_CREATE_CONDITION): {
%>
<FORM action="main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_CREATE_CONDITION%>" method="post">
    <TABLE style="font-size: 13px" align="center" width="80%">
        <TR class="blue2">
            <TD align="center" colspan="3">
                Bedingung
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Description (Bsp: Bev�lkerungsanzahl)
            </TD>
            <TD align="center">
            </TD>
            <TD align="left">
                <INPUT type="text" name="description"/>
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Clazz
            </TD>
            <TD align="right">
                at.darkdestiny.core.title.condition.C
            </TD>
            <TD align="left">
                <INPUT type="text" name="clazz" value="MeineConditionKlasse"/>
            </TD>
        </TR>
        <TR>
            <TD align="center" colspan="3">
                <INPUT type="submit" name="continue" value="Erstellen"/>
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                    break;
                }
                case (TYPE_GENERATE_JAVA_TITLE): {
%>
<FORM action="main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_GENERATE_JAVA_TITLE%>" method="post">
    <INPUT type="hidden" name="titleId" value="<%= titleId%>" />
    <%
                        Title t = Service.titleDAO.findById(titleId);
    %>
    <TABLE style="font-size: 13px" align="center" width="80%">
        <TR>
            <TD align="center">
                Sie sind dabei folgenden Java kot zu generieren
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Klasse: <%= t.getClazz()%>.java<BR>
            </TD>
        </TR>
        <TR>
            <TD align="center">
                <TEXTAREA style="width:100%; height:400px; "><%= TitleService.getJavaForTitle(titleId).toString()%></TEXTAREA>
            </TD>
        </TR>
        <TR>
            <TD align="center">
                <INPUT type="submit" name="continue" value="Erstellen"/>
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                    break;
                }
                case (TYPE_GENERATE_JAVA_CONDITION): {
%>
<FORM action="main.jsp?page=admin/titleGenerator&subCategory=99&process=<%= PROCESS_GENERATE_JAVA_CONDITION%>" method="post">
    <INPUT type="hidden" name="conditionId" value="<%= conditionId%>" />
    <%
                        Condition c = Service.conditionDAO.findById(conditionId);
    %>
    <TABLE style="font-size: 13px" align="center" width="80%">
        <TR>
            <TD align="center">
                Sie sind dabei folgenden Java kot zu generieren
            </TD>
        </TR>
        <TR>
            <TD align="center">
                Klasse: <%= c.getClazz()%>.java<BR>
            </TD>
        </TR>
        <TR>
            <TD align="center">
                <TEXTAREA style="width:100%; height:400px; "><%= TitleService.getJavaForCondition(conditionId).toString()%></TEXTAREA>
            </TD>
        </TR>
        <TR>
            <TD align="center">
                <INPUT type="submit" name="continue" value="Erstellen"/>
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                    break;
                }
            }%>
