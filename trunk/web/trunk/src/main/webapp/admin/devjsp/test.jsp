<%@page import="at.darkdestiny.core.utilities.LoginUtilities"
%><%@page import="at.darkdestiny.core.img.charts.GroundCombatChart"
%><%@page import="at.darkdestiny.core.img.charts.ChartDrawer"
%><%@page import="at.darkdestiny.core.img.BaseRenderer"
%><%@page import="at.darkdestiny.core.PlanetData"
%><%@page import="at.darkdestiny.core.img.BackgroundPainter"
%><%@page import="at.darkdestiny.core.img.planet.*"
%><%@page import="at.darkdestiny.core.*"
%><%@page import="at.darkdestiny.core.img.IModifyImageFunction"
%><%@page import="java.util.List"
%><%@page import="java.util.LinkedList"
%><%@ page contentType="image/png" %><%

LoginUtilities.checkLogin(request, response);

List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();
// Check for DATA_FOR_SCAN_PIC

int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int mode = 1;
int picSize = 200;

if (request.getParameter("planetId") != null) {
    planetId = Integer.parseInt(request.getParameter("planetId"));
}

PlanetData pd = new PlanetData(planetId);

if (request.getParameter("mode") != null) {
    mode = Integer.parseInt(request.getParameter("mode"));
    if (mode == PlanetRenderer.LARGE_PIC) {
        picSize = 200;
    } else if (mode == PlanetRenderer.SMALL_PIC) {
        picSize = 40;
    }           
}

allItems.add(new PlanetRenderer(pd,this.getServletContext().getRealPath(GameConfig.getInstance().picPath()+"pic/") + "/",mode));
BaseRenderer renderer = new BaseRenderer(picSize,picSize);
// Schreibt den Mime-Type in den Log, Nur Benutzen, wenn 
// genau das gew&uuml;nscht wird
// renderer.getImageMimeType(); 

renderer.modifyImage(new BackgroundPainter(0x000000));

for (IModifyImageFunction mif : allItems) {
	renderer.modifyImage(mif);
}

renderer.sendToUser(response.getOutputStream());
%>