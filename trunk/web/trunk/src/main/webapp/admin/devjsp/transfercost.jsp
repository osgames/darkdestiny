<%@page import="at.darkdestiny.framework.access.DbConnect"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>

<%
        Connection conn = DbConnect.getConnection();
        Statement stmt = conn.createStatement();
        Statement stmt2 = conn.createStatement();

        boolean updateConstructions = true;
        boolean updateModules = true;
        boolean updateGroundTroops = true;
        boolean updateShips = true;
        boolean updatePlanets = true;

        if (updateConstructions && updateModules && updateGroundTroops && updateShips) {
            stmt2.execute("TRUNCATE TABLE ressourceCost");
        }
        
        if (updatePlanets) {
            stmt2.execute("TRUNCATE TABLE planetressources");
        }
        
        if (updateConstructions) {
            ResultSet rs = stmt.executeQuery("SELECT id, iron, steel, terkonit, ynkelonium, howalgonium, cvEmbinium FROM construction");

            while (rs.next()) {
                int qty = 0;

                if (rs.getInt(2) > 0) {
                    qty = rs.getInt(2);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_IRON + ", " + qty + ")");
                }
                if (rs.getInt(3) > 0) {
                    qty = rs.getInt(3);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_STEEL + ", " + qty + ")");
                }
                if (rs.getInt(4) > 0) {
                    qty = rs.getInt(4);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_TERKONIT + ", " + qty + ")");
                }
                if (rs.getInt(5) > 0) {
                    qty = rs.getInt(5);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_YNKELONIUM + ", " + qty + ")");
                }
                if (rs.getInt(6) > 0) {
                    qty = rs.getInt(6);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_HOWALGONIUM + ", " + qty + ")");
                }
                if (rs.getInt(7) > 0) {
                    qty = rs.getInt(7);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_CVEMBINIUM + ", " + qty + ")");
                }
            }
        }        
        
        if (updateModules) {
            ResultSet rs = stmt.executeQuery("SELECT id, iron, steel, terkonit, ynkelonium, howalgonium, cvEmbinium FROM module");

            while (rs.next()) {
                int qty = 0;

                if (rs.getInt(2) > 0) {
                    qty = rs.getInt(2);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_IRON + ", " + qty + ")");
                }
                if (rs.getInt(3) > 0) {
                    qty = rs.getInt(3);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_STEEL + ", " + qty + ")");
                }
                if (rs.getInt(4) > 0) {
                    qty = rs.getInt(4);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_TERKONIT + ", " + qty + ")");
                }
                if (rs.getInt(5) > 0) {
                    qty = rs.getInt(5);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_YNKELONIUM + ", " + qty + ")");
                }
                if (rs.getInt(6) > 0) {
                    qty = rs.getInt(6);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_HOWALGONIUM + ", " + qty + ")");
                }
                if (rs.getInt(7) > 0) {
                    qty = rs.getInt(7);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_CVEMBINIUM + ", " + qty + ")");
                }
            }
        }                
        
        if (updateGroundTroops) {
            ResultSet rs = stmt.executeQuery("SELECT id, iron, steel, terkonit, ynkelonium, howalgonium, cvEmbinium FROM groundtroops");

            while (rs.next()) {
                int qty = 0;

                if (rs.getInt(2) > 0) {
                    qty = rs.getInt(2);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_IRON + ", " + qty + ")");
                }
                if (rs.getInt(3) > 0) {
                    qty = rs.getInt(3);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_STEEL + ", " + qty + ")");
                }
                if (rs.getInt(4) > 0) {
                    qty = rs.getInt(4);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_TERKONIT + ", " + qty + ")");
                }
                if (rs.getInt(5) > 0) {
                    qty = rs.getInt(5);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_YNKELONIUM + ", " + qty + ")");
                }
                if (rs.getInt(6) > 0) {
                    qty = rs.getInt(6);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_HOWALGONIUM + ", " + qty + ")");
                }
                if (rs.getInt(7) > 0) {
                    qty = rs.getInt(7);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_CVEMBINIUM + ", " + qty + ")");
                }
            }
        }                
        
        if (updateShips) {
            ResultSet rs = stmt.executeQuery("SELECT id, iron, steel, terkonit, ynkelonium, howalgonium, CVEmbinium FROM shipdesigns");

            while (rs.next()) {
                int qty = 0;

                if (rs.getInt(2) > 0) {
                    qty = rs.getInt(2);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (3, " + rs.getInt(1) + ", " + GameConstants.RES_IRON + ", " + qty + ")");
                }
                if (rs.getInt(3) > 0) {
                    qty = rs.getInt(3);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (3, " + rs.getInt(1) + ", " + GameConstants.RES_STEEL + ", " + qty + ")");
                }
                if (rs.getInt(4) > 0) {
                    qty = rs.getInt(4);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (3, " + rs.getInt(1) + ", " + GameConstants.RES_TERKONIT + ", " + qty + ")");
                }
                if (rs.getInt(5) > 0) {
                    qty = rs.getInt(5);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (3, " + rs.getInt(1) + ", " + GameConstants.RES_YNKELONIUM + ", " + qty + ")");
                }
                if (rs.getInt(6) > 0) {
                    qty = rs.getInt(6);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (3, " + rs.getInt(1) + ", " + GameConstants.RES_HOWALGONIUM + ", " + qty + ")");
                }
                if (rs.getInt(7) > 0) {
                    qty = rs.getInt(7);
                    stmt2.execute("INSERT INTO ressourcecost (type, id, ressId, qty) VALUES (3, " + rs.getInt(1) + ", " + GameConstants.RES_CVEMBINIUM + ", " + qty + ")");
                }
            }
        }

        if (updatePlanets) {
            ResultSet rs = stmt.executeQuery("SELECT id, iron, 0, 0, ynkelonium, howalgonium, cvEmbinium FROM planet");

            while (rs.next()) {
                long qty = 0;

                if (rs.getLong(2) > 0) {
                    qty = rs.getLong(2);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_PLANET_IRON + ", " + qty + ")");
                }
                if (rs.getLong(5) > 0) {
                    qty = rs.getLong(5);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_PLANET_YNKELONIUM + ", " + qty + ")");
                }
                if (rs.getLong(6) > 0) {
                    qty = rs.getLong(6);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (0, " + rs.getInt(1) + ", " + GameConstants.RES_PLANET_HOWALGONIUM + ", " + qty + ")");
                }
            }

            rs.close();
            rs = stmt.executeQuery("SELECT planetID, iron, steel, terkonit, ynkelonium, howalgonium, cvEmbinium, minIronStorage, minSteelStorage, minTerkonitStorage, minYnkeloniumStorage, minHowalgoniumStorage, minCVEmbiniumStorage FROM playerplanet");

            while (rs.next()) {
                long qty = 0;

                if (rs.getLong(2) > 0) {
                    qty = rs.getLong(2);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_IRON + ", " + qty + ")");
                }
                if (rs.getLong(3) > 0) {
                    qty = rs.getLong(3);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_STEEL + ", " + qty + ")");
                }
                if (rs.getLong(4) > 0) {
                    qty = rs.getLong(4);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_TERKONIT + ", " + qty + ")");
                }
                if (rs.getLong(5) > 0) {
                    qty = rs.getLong(5);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_YNKELONIUM + ", " + qty + ")");
                }
                if (rs.getLong(6) > 0) {
                    qty = rs.getLong(6);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_HOWALGONIUM + ", " + qty + ")");
                }
                if (rs.getLong(7) > 0) {
                    qty = rs.getLong(7);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (1, " + rs.getInt(1) + ", " + GameConstants.RES_CVEMBINIUM + ", " + qty + ")");
                }

                if (rs.getLong(8) > 0) {
                    qty = rs.getLong(8);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_IRON + ", " + qty + ")");
                }
                if (rs.getLong(9) > 0) {
                    qty = rs.getLong(9);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_STEEL + ", " + qty + ")");
                }
                if (rs.getLong(10) > 0) {
                    qty = rs.getLong(10);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_TERKONIT + ", " + qty + ")");
                }
                if (rs.getLong(11) > 0) {
                    qty = rs.getLong(11);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_YNKELONIUM + ", " + qty + ")");
                }
                if (rs.getLong(12) > 0) {
                    qty = rs.getLong(12);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_HOWALGONIUM + ", " + qty + ")");
                }
                if (rs.getLong(13) > 0) {
                    qty = rs.getLong(13);
                    stmt2.execute("INSERT INTO planetressources (type, planetId, ressId, qty) VALUES (2, " + rs.getInt(1) + ", " + GameConstants.RES_CVEMBINIUM + ", " + qty + ")");
                }
            }
            rs.close();
        }
%>