<%@page import="at.darkdestiny.core.service.MultiService" %>
<%@page import="at.darkdestiny.core.service.MainService" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.GameUtilities" %>
<%@page import="at.darkdestiny.core.GameConfig" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%

     
final int PROCESS_DELETE_BYIP = 1;
final int PROCESS_DELETE_BYTICK = 2;
final int PROCESS_DELETE_BYENTRY = 3;
int process = 0;
if(request.getParameter("process") != null){
    process = Integer.parseInt(request.getParameter("process"));
    }
if(process > 0){
        switch(process){
             case(PROCESS_DELETE_BYIP):
                        MultiService.deleteByIp(request.getParameterMap());
                 break;
            case(PROCESS_DELETE_BYTICK):
                MultiService.deleteByTick(request.getParameterMap());
                break;
            case(PROCESS_DELETE_BYENTRY):
                MultiService.deleteByEntry(request.getParameterMap());
                break;
            }
    }
       TreeMap<String, TreeMap<Integer, TreeMap<Integer, MultiLog>>> entries = MultiService.findAllSorted();

%>
<TABLE class="blue" width="80%">
    <TR class="blue2">
        <TD width="15%">
            IP Address
        </TD>
        <TD width="5%">

        </TD>
        <TD width="80%">
        </TD>
    </TR>

    <% for (Map.Entry<String, TreeMap<Integer, TreeMap<Integer, MultiLog>>> entry : entries.entrySet()) {%>
    <%

    %>
    <TR>
        <TD valign="top">
            <%= entry.getKey()%>
        </TD>
        <TD valign="top">
            <IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" onclick="window.location.href='main.jsp?subCategory=99&page=admin/multis&ip=<%= entry.getKey() %>&process=<%= PROCESS_DELETE_BYIP %>';" onmouseover="doTooltip(event,'Delete Tick-Entry')" onmouseout="hideTip()" />
        </TD>
        <TD>
            <% if (!entry.getValue().isEmpty()) {%>
            <TABLE class="blue" width="100%">
                <TR class="blue2">
                    <TD width="15%">
                        Tick
                    </TD>
                    <TD width="15%">
                        Date
                    </TD>
                    <TD>

                    </TD>
                    <TD width="70%">

                    </TD>
                </TR>
                <% for (Map.Entry<Integer, TreeMap<Integer, MultiLog>> entry2 : entry.getValue().entrySet()) {%>
                <TR>
                    <TD valign="top">
                        <%= entry2.getKey()%>
                    </TD>
                    <%

                         java.util.Date date = new java.util.Date(entry2.getValue().firstEntry().getValue().getDate());
                         String dateString = java.text.DateFormat.getDateInstance().format(date);
                         String timeString = java.text.DateFormat.getTimeInstance().format(date);
                    %>
                    <TD valign="top">
                        <%= dateString%><BR>
                        <%= timeString%>
                    </TD>
                    <TD valign="top">
                        <IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" onclick="window.location.href='main.jsp?subCategory=99&page=admin/multis&ip=<%= entry.getKey() %>&tick=<%= entry2.getKey() %>&process=<%= PROCESS_DELETE_BYTICK %>';" onmouseover="doTooltip(event,'Delete Tick-Entry')" onmouseout="hideTip()" />
                    </TD>
                 <TD>

                    <% if (!entry2.getValue().isEmpty()) {%>
                    <TABLE class="blue" width="100%">
                        <TR class="blue2">
                            <TD>
                                User
                            </TD>
                            <TD>
                                Count
                            </TD>
                            <TD>
                                Comment
                            </TD>
                            <TD>

                            </TD>
                        </TR>
                        <% for (Map.Entry<Integer, MultiLog> entry3 : entry2.getValue().entrySet()) {%>
                        <TR>
                            <TD>
                                <%= MainService.findUserByUserId(entry3.getKey()).getGameName()%>
                            </TD>
                            <TD>
                                <%= entry3.getValue().getCount()%>
                            </TD>
                            <TD>
                                <%
                                     String comment = entry3.getValue().getComment();
                                     if (comment == null || comment.equals("null")) {
                                         comment = "";
                                     }
                                %>
                                <%= comment%>
                            </TD>
                            <TD valign="top">
                                <IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" onclick="window.location.href='main.jsp?subCategory=99&page=admin/multis&ip=<%= entry.getKey() %>&tick=<%= entry2.getKey() %>&userId=<%= entry3.getKey() %>&process=<%= PROCESS_DELETE_BYENTRY %>';" onmouseover="doTooltip(event,'Delete Single Entry')" onmouseout="hideTip()" />
                            </TD>
                        </TR>
                        <% }%>
                    </TABLE>
                    <% }%>
                </TD>
    </TR>
    <% }%>
</TABLE>
<% }%>
</TD>
</TR>
<% }%>
</TABLE>