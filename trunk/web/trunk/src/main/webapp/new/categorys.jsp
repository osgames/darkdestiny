<%@page import="at.darkdestiny.core.*"%>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.core.utilities.*"%>
<%@page import="at.darkdestiny.core.service.CategoryService"%>
<%@page import="at.darkdestiny.core.model.PlayerCategory"%>
<%@page import="at.darkdestiny.core.viewbuffer.HeaderBuffer"%>
<%
int type = 0;
int action = 0;
int categoryId = 0;


if (request.getParameter("type") != null) {
    type = Integer.parseInt(request.getParameter("type"));
}
if (request.getParameter("catId") != null) {
    categoryId = Integer.parseInt(request.getParameter("catId"));
}
if (request.getParameter("action") != null) {
    action = Integer.parseInt(request.getParameter("action"));
}
  session = request.getSession();

int userId = Integer.parseInt((String) session.getAttribute("userId"));


if (action != 0) {

   String categoryName = "";
   Map<String, String[]> parMap = request.getParameterMap();

    for(final Map.Entry<String, String[]> entry : parMap.entrySet()) {
           String parName = entry.getKey();

            if (parName.startsWith("categoryName")) {
                categoryName = parMap.get("categoryName")[0]; }
           }

    switch (type) {
        // Kategorie &auml;ndern
        case (1):
            CategoryService.changeCategory(userId, categoryId, categoryName);
            break;

        // L&ouml;schen
        case (2):
            CategoryService.deleteCategory(userId, categoryId);
            break;

        // Hinzuf&uuml;gen
        case (3):
            CategoryService.addCategory(userId, categoryName);
            break;
    }
    if(type > 0){

        HeaderBuffer.reloadUser(userId);
        }
    type = 0;

}

switch (type) {
    case 0:

%>
<BR>
<CENTER>
    <A HREF="main.jsp?page=new/categorys&type=3"><%= ML.getMLStr("category_link_add", userId)%></A>
</CENTER>
<BR>
<TABLE class="blue" WIDTH="80%">
    <TR class="heading">
        <TD width='40%'><%= ML.getMLStr("category_lbl_categoryname", userId)%></TD>
        <TD width='30%'><%= ML.getMLStr("category_lbl_change", userId)%></TD>
        <TD width='30%'><%= ML.getMLStr("category_lbl_delete", userId)%></TD>
    </TR>

    <%


          for (PlayerCategory pc : CategoryService.getAllCategorysByUserId(userId)) {

    %>
    <TR>
        <TD><%= pc.getName()%></TD>
        <TD><A HREF="main.jsp?page=new/categorys&type=1&catId=<%= pc.getId()%>"><%= ML.getMLStr("category_lbl_change", userId)%></A>
        <TD><A HREF="main.jsp?page=new/categorys&type=2&catId=<%= pc.getId()%>"><%= ML.getMLStr("category_lbl_delete", userId)%></A>
    </TR>
    <%


          }
    %>

</TABLE>
    <BR>
    <CENTER><A HREF="main.jsp?page=new/management_1&action=4"><%= ML.getMLStr("category_link_back" , userId)%></A></CENTER>
<%
          break;
//Kategorie &Auml;ndern
      case 1:
%>
<TABLE >
    <TR >
        <TD>
            <FORM method="post" action="main.jsp?page=new/categorys&type=1&action=1&catId=<%=categoryId%>" >
                <INPUT name="categoryName" size="20" maxlength='25' value="<%= CategoryService.getCategoryNamefromCategoryID(categoryId) %>" size="5" maxlength='8'>
                <INPUT type="submit" name="change" value="<%= ML.getMLStr("category_but_change", userId)%>">
            </FORM>
        </TD>
    </TR>
    <TR>
        <TD><CENTER><A HREF="main.jsp?page=new/categorys"><%= ML.getMLStr("category_link_back" , userId)%></A></CENTER></TD>
    </TR>
</TABLE>

<%
          break;
//Kategorie L&ouml;schen
      case 2:
%>
<TABLE>
    <TR>
        <TD COLSPAN="2">
            <%= ML.getMLStr("category_msg_del1" , userId)%>
            <B><%= CategoryService.getCategoryNamefromCategoryID(categoryId) %></B>
            <%= ML.getMLStr("category_msg_del2", userId)%>
        </TD>
    </TR>
    <TR>
        <TD width="50%"><CENTER><A HREF="main.jsp?page=new/categorys&type=2&action=1&catId=<%=categoryId%>"><%= ML.getMLStr("category_link_yes" , userId)%></A></CENTER></TD>
        <TD width="50%"><CENTER><A HREF="main.jsp?page=new/categorys"><%= ML.getMLStr("category_link_no" , userId)%></A></CENTER></TD>
    </TR>
</TABLE>

<%

          break;
      //Hinzuf&uuml;gen
      case 3:
%>
<TABLE >
    <TR >
        <TD>
            <FORM method="post" action="main.jsp?page=new/categorys&type=3&action=1" >
                <INPUT name="categoryName" value="<%= ML.getMLStr("category_lbl_newcategory" , userId)%>" size="20" maxlength='25'>
                <INPUT type="submit" name="change" value="<%= ML.getMLStr("category_but_add" , userId)%>">
            </FORM>
        </TD>
    </TR>
    <TR>
        <TD><CENTER><A HREF="main.jsp?page=new/categorys"><%= ML.getMLStr("category_link_back" , userId)%></A></CENTER></TD>
    </TR>
</TABLE>

<%
                    break;
            }
%>

