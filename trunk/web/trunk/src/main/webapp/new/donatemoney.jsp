<%@page import="at.darkdestiny.core.result.BaseResult"%>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.model.UserData" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%
session = request.getSession();
int userId = Integer.parseInt((String) session.getAttribute("userId"));
int process = 0;
if (request.getParameter("process") != null) {
    process = Integer.parseInt(request.getParameter("process"));
}
int type = 0;
if (request.getParameter("type") != null) {
    type = Integer.parseInt(request.getParameter("type"));
}
User user = DiplomacyService.findUserById(userId);
BaseResult br = null;
if(process == 1){
    if(!user.getTrial()){
         br = DiplomacyService.createDonation(userId, request.getParameterMap());
    }
}%>
<% if(br != null) { %>
<CENTER><%= br.getFormattedString() %></CENTER>
<% } %>
<%
if(user.getTrial()){
    %>
    <CENTER><%= ML.getMLStr("diplomacy_err_notavailableintrial", userId) %></CENTER>
    <%
    }else{
switch(type){
    case(0):
%>
<FORM method="post" action="main.jsp?page=new/diplomacy&subpage=donatemoney&type=1">
    <TABLE width="80%">
      <TR>
          <TD align="center" style="font-size: 12px"><BR>
          <%= ML.getMLStr("donatemoney_msg_info",userId) %><BR>
          You may donate maximal <%= FormatUtilities.getFormattedNumber(DiplomacyService.getMaxDonateAmount(userId)) %> credits<BR>
          </TD>
      </TR>
    </TABLE><BR>
  <TABLE width="60%" class="blue">
      <TR class="blue2">
        <TD>
            <%= ML.getMLStr("donatemoney_lbl_doneatemoneyfor",userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("donatemoney_lbl_amount",userId) %>
        </TD>
    </TR>
    <TR>
        <TD>
            <SELECT name="toUserId">
                <%
                 for(User u : DiplomacyService.findAllUsersNotTrial()){
                     if(u.getUserId() == userId)continue;
                    %>
                    <OPTION value="<%= u.getUserId()%>"><%= u.getGameName() %></OPTION>
                    <%
                }
                %>
            </SELECT>
        </TD>
        <TD>
            <INPUT name="amount" type="text">
        </TD>
    </TR>
    <TR>
        <TD align="center" colspan="2">
            <INPUT type="submit" value="<%= ML.getMLStr("donatemoney_but_send",userId) %>">
        </TD>
    </TR>
</TABLE>
</FORM>
<TABLE width="60%" class="blue">
    <TR>
        <TD colspan="3">
            <%= ML.getMLStr("donatemoney_lbl_formertransaction",userId) %>
        </TD>
    </TR>
    <TR class="blue2">
        <TD>
            <%= ML.getMLStr("donatemoney_lbl_date",userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("donatemoney_lbl_from",userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("donatemoney_lbl_to",userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("donatemoney_lbl_amount", userId) %>
        </TD>
    </TR>
    <%
    for(CreditDonation cd : DiplomacyService.findAllDonations()){
           if (cd.getAmount() <= 0) continue;

           java.util.Date msgDate = new java.util.Date(cd.getDate());
           String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate) + " " + java.text.DateFormat.getTimeInstance().format(msgDate);
           User u1 =  DiplomacyService.findUserById(cd.getFromUserId());
           User u2 = DiplomacyService.findUserById(cd.getToUserId());
           
           String fromUser = ML.getMLStr("donatemoney_userdeleted", userId);
           String toUser = ML.getMLStr("donatemoney_userdeleted", userId);
           if(u1 != null){
               fromUser = u1.getGameName();
               }
           if(u2 != null){
               toUser = u2.getGameName();
               }

    %>
    <TR>
        <TD>
            <%= msgDateString %>
        </TD>
        <TD>
            <%= fromUser %>
        </TD>
        <TD>
            <%= toUser %>
        </TD>
        <TD>
            <%= FormatUtilities.getFormattedNumber(cd.getAmount()) %>
        </TD>
    </TR>
    <%
    }
    %>
</TABLE>

<%
break;

case(1):

    try{
    long amount = Long.parseLong(request.getParameter("amount"));

    int toUserId =  Integer.parseInt(request.getParameter("toUserId"));
    %>
    <FORM method="post" action="main.jsp?page=new/diplomacy&subpage=donatemoney&process=1">
        <INPUT type="hidden" name="amount" value="<%= amount %>"> 
        <INPUT type="hidden" name="toUserId" value="<%= toUserId %>"> 
        <TABLE>
            <TR>
                <TD>
                    Wollen sie dem Spieler <%= DiplomacyService.findUserById(toUserId).getGameName() %>
                    wirklich <%= amount %> Credits schenken ?
                </TD>
            </TR>
            <TR>
                <TD>
                    <CENTER><INPUT type="submit" value="bestštigen"></CENTER>
                </TD>
            </TR>
        </TABLE>
    </FORM>
    <%
    }catch(Exception e){
        %>
        <TABLE width="80%">

        <TR>
            <TD colspan="2" align ="center">
                <B><Font color="RED"><%= ML.getMLStr("donatemoney_err_amountinvalid", userId) %></Font></B>
            </TD>
        </TR>
        <TR>
            <TD colspan="2" align ="center">
                <A onClick="javascript:history.go(-1)"><B><BR><%= ML.getMLStr("global_back", userId) %></B></A>
            </TD>
        </TR>
        </TABLE>
        <%
    }
break;
}
}
%>