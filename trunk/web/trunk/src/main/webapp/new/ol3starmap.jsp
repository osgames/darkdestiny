<%-- 
    Document   : ol3starmap
    Created on : 27.04.2016, 18:58:51
    Author     : Stefan
--%>
		<style>
			.rotate-north {
			  top: 80px;
			  left: .5em;
			}
                        
			.exit {
			  top: 115px;
			  left: .5em;
			}                        

			.refresh {
			  top: 150px;
			  left: .5em;
			}                           
                        
			.ol-touch .rotate-north {
			  top: 80px;
			}
                        
                        div {
                           border: 0 !important; 
                        }
		</style>	
<DIV id="darklayer" class="dark" style="display:none">
</DIV>
<DIV id="helper" class="helper">
	<DIV id="starmapdetail" class="starmapdetail" style="display:none"></DIV>                
</DIV>        
<DIV id="actionpanel" class="actionpanel" style="display:none;">    
	<SPAN id="actionpanelheading" class="actionpanelheading">Sende Flotte</SPAN><BR>
	<SPAN id="actionpanelfleetname" class="actionpanelfleetname">Flotte: -</SPAN><BR>
	<SPAN id="actionpaneltargetsys" class="actionpaneltargetsys">System: -</SPAN><BR>
	<SPAN id="actionpaneltargetplanet" class="actionpaneltargetplanet">Planet: -</SPAN><BR>
	<INPUT hidden id="fleetId" value="" />
	<INPUT hidden id="targetType" value="" />
	<INPUT hidden id="targetId" value="" />
	<INPUT hidden id="toSystem" value="" />
	<BR>
	 <button  class="darklarge" type="button" onclick="sendFleet(document.getElementById('fleetId').value,document.getElementById('targetType').value,
	 document.getElementById('targetId').value,document.getElementById('toSystem').value)">OK!</button> 
	 <button  class="darklarge" type="button" onclick="stopDrawMode();">Abbrechen</button> 
</DIV>        
<div class="container-fluid" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; opacity: 1; background-color: black;">

<div class="row-fluid" style="width: 100%; height: 100%;">
  <div class="span12" style="width: 100%; height: 100%;">
    <div id="map" class="map" style="background-color:#000000; width: 100%; height: 100%;""></div>  
  </div>
</div>

</div>
                <link rel="stylesheet" href="css/ol.css" type="text/css">
		<link rel="stylesheet" href="css/starmap.css" type="text/css">
                <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
                <script src="java/starmap/ajax.js"></script>
		<script src="java/starmap/jquery-2.0.0.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>		
                <script src="java/starmap/ol-debug.js"></script>
		<script src="java/starmap/starmap.js"></script>
		<script src="java/starmap/logicSM.js"></script>
