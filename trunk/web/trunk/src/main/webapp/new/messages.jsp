<%@page import="org.jfree.chart.JFreeChart"%>
<%@page import="at.darkdestiny.core.service.IdToNameService"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.model.Message" %>
<%@page import="at.darkdestiny.core.model.User" %>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel" %>
<%@page import="at.darkdestiny.core.service.MessageService" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="at.darkdestiny.util.*" %>
<script src="java/messages.js" type="text/javascript"></script>
<SCRIPT TYPE="text/javascript">
    <!--
    function popup(mylink, windowname)
    {
        if (! window.focus)return true;
        var href;
        if (typeof(mylink) == 'string')
            href=mylink;
        else
            href=mylink.href;
        window.open(href, windowname, "resizable=no,scrollbars=yes,status=no");
        return false;
    }
    //-->
</SCRIPT>
<%
            session = request.getSession();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            User user = MessageService.findUserById(userId);

            String work = request.getParameter("work");
            // Vars for sending messages
            int multiUserLimit = 5;
            boolean sent = false;
            String topic = request.getParameter("topic");
            String text = request.getParameter("text");
            String[] targetUsers = request.getParameterValues("targetUsers");
            String targetUserId = request.getParameter("targetUserId");

            // Check if a message was sent
            if (request.getParameter("sent") != null) {
                if (request.getParameter("sent").equalsIgnoreCase("1")) {
                    sent = true;
                }
            }
            // Check if a message is viewed
            int msgId = -1;
            if (request.getParameter("messageId") != null) {
                try {
                    msgId = Integer.parseInt((String) request.getParameter("messageId"));
                } catch (NumberFormatException nfe) {
                }
            }
            // Read type
            EMessageFolder activeFolder = null;
            EMessageFolder baseFolder = null;

            int actType = 1;

            if (request.getParameter("type") != null) {
                try {
                    int type = Integer.parseInt(request.getParameter("type"));

                    if ((type < 1) || (type > 4)) {
                        type = 1;
                    }

                    actType = type;

                    switch (type) {
                        case(1):
                            activeFolder = EMessageFolder.INBOX;
                            break;
                        case(2):
                            activeFolder = EMessageFolder.OUTBOX;
                            break;
                        case(3):
                            activeFolder = EMessageFolder.SEND;
                            break;
                        case(4):
                            activeFolder = EMessageFolder.ARCHIVE;
                            break;
                    }
                } catch (NumberFormatException nfe) {
                }
            } else {
                activeFolder = EMessageFolder.INBOX;
            }

            baseFolder = activeFolder;

            if (msgId != -1) {
                activeFolder = EMessageFolder.VIEW;
            }

            if (activeFolder == EMessageFolder.INBOX) {
                if (work != null) {
                    ArrayList<Message> messages = new ArrayList<Message>();
                    int workId = Integer.parseInt(work);
                    switch (workId) {
                        case 1: // delete
                            Enumeration<String> e = request.getParameterNames();
                            int paramCnt = 0;
                            int tmpMsgId = 0;
                            int nextMessageId = -1;
                            
                            while (e.hasMoreElements()) {                                
                                String s = e.nextElement();

                                if (s.startsWith("MID_")) {                                
                                    paramCnt++;
                                    
                                    if (paramCnt == 1) {
                                        tmpMsgId = Integer.parseInt(s.substring(4));
                                        nextMessageId = MessageService.nextMessageId(userId,tmpMsgId);
                                        if (nextMessageId == -1) {
                                            nextMessageId = MessageService.previousMessageId(userId,tmpMsgId);
                                        }                                        
                                    }
                                                                        
                                    MessageService.deleteMessage(userId, Integer.parseInt(s.substring(4)), activeFolder);
                                }                            
                            }

                            if (paramCnt == 1) {
                                msgId = nextMessageId;
                                activeFolder = EMessageFolder.VIEW;
                            }
                            
                            break;
                        case 2: // archive
                            e = request.getParameterNames();
                            while (e.hasMoreElements()) {
                                String s = (String) e.nextElement();
                                if (s.startsWith("MID_")) {
                                    Message m = MessageService.findMessageByTargetId(userId, Integer.parseInt(s.substring(4)));
                                    m.setArchiveByTarget(true);
                                    m.setViewed(true);
                                    try {
                                        MessageService.updateMessage(m);
                                    } catch (Exception ex) {

                                        DebugBuffer.addLine(DebugLevel.ERROR, "Error trying to delete MessageId : " + m.getMessageId() + " e : " + ex);
                                    }
                                }
                            }
                            break;
                        case 3: // Delete all message
                            MessageService.deleteAllMessages(userId, activeFolder, null);
                            break;
                        case 4: // delete all system messages
                            MessageService.deleteAllMessages(userId, activeFolder, EMessageType.SYSTEM);
                            break;
                        case 5: // delete player messages
                            MessageService.deleteAllMessages(userId, activeFolder, EMessageType.USER);
                            break;
                        case 6: // delete alliance messages
                            MessageService.deleteAllMessages(userId, activeFolder, EMessageType.ALLIANCE);
                            break;
                        default:
                    }
                }
            } else if (activeFolder == EMessageFolder.OUTBOX) {
                if (work != null) {
                    ArrayList<Message> messages = new ArrayList<Message>();

                    int workId = Integer.parseInt(work);
                    switch (workId) {

                        case 1:
                            Enumeration<String> e = request.getParameterNames();
                            while (e.hasMoreElements()) {
                                String s = e.nextElement();
                                if (s.startsWith("MID_")) {
                                    MessageService.deleteMessage(userId, Integer.parseInt(s.substring(4)), activeFolder);
                                }
                            }
                            break;
                        case 2: // Delete all message
                            MessageService.deleteAllMessages(userId, activeFolder, null);
                            break;
                        default:
                    }
                }
            } else if (activeFolder == EMessageFolder.ARCHIVE) {
                if (work != null) {
                    int workId = Integer.parseInt(work);
                    switch (workId) {

                        case 1:
                            Enumeration<String> e = request.getParameterNames();
                            while (e.hasMoreElements()) {
                                String s = e.nextElement();
                                if (s.startsWith("MID_")) {
                                    MessageService.deleteMessage(userId, Integer.parseInt(s.substring(4)), activeFolder);
                                }
                            }
                            break;
                        case 2: // Delete all message
                            MessageService.deleteAllMessages(userId, activeFolder, null);
                            break;
                        default:
                    }
                }
            }
%>

<TABLE class="trans" width="80%">
    <TR align="center">
        <TD width="25%"><U><A href="main.jsp?page=new/messages&type=1"><%= ML.getMLStr("messages_link_inbox", userId)%></A></U></TD>
        <TD width="25%"><U><A href="main.jsp?page=new/messages&type=2"><%= ML.getMLStr("messages_link_outbox", userId)%></A></U></TD>
        <TD width="25%"><U><A href="main.jsp?page=new/messages&type=3"><%= ML.getMLStr("messages_link_senden", userId)%></A></U></TD>
        <TD width="25%"><U><A href="main.jsp?page=new/messages&type=4"><%= ML.getMLStr("messages_link_archive", userId)%></A></U></TD>
    </TR>
</TABLE><BR>
<%
            if (msgId > -1) {
                Message m = new Message();
                if (baseFolder == EMessageFolder.OUTBOX) {
                    m = MessageService.getMessageForView(user.getUserId(), msgId, MessageService.TYPE_SOURCE_USER);
                } else {
                    m = MessageService.getMessageForView(user.getUserId(), msgId, MessageService.TYPE_TARGET_USER);
                }

                if (m != null) {
                    java.util.Date msgDate = new java.util.Date(m.getTimeSent());
                    String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate) + " " + java.text.DateFormat.getTimeInstance().format(msgDate);

%>
<table width="80%">
    <tr style="font-size:12px"><td width="60" class="blue2" valign="top" bgcolor="#808080">
            <B><%= activeFolder == EMessageFolder.OUTBOX ? "" + ML.getMLStr("messages_lbl_to", userId) + "" : "" + ML.getMLStr("messages_lbl_from", userId) + " : "%></B>
        </td><td><%= activeFolder == EMessageFolder.OUTBOX
                                ? MessageService.findUserById(m.getTargetUserId()).getGameName()
                                : ((m.getSourceUserId() != 0)
                                ? MessageService.findUserById(m.getSourceUserId()).getGameName()
                                : "SYSTEM")%></td></tr>
    <%
        // Add session information for chart visualization
        // ********* BEGIN *********
        JFreeChart chart = MessageService.getChart(userId, m);
        if (chart != null) {
            if (session.getAttribute("chart") != null) {
                session.removeAttribute("chart");
            }
            session.setAttribute("chart", chart);
        }
        // ********** END **********
    %>
    <tr style="font-size:12px"><td class="blue2" valign="top" bgcolor="#808080"><B><%= ML.getMLStr("messages_lbl_date", userId)%>:</B></td><td><%= msgDateString%></td></tr>
    <tr style="font-size:12px"><td class="blue2" valign="top" bgcolor="#808080"><B><%= ML.getMLStr("messages_lbl_subject", userId)%>:</B></td><td><B><%= m.getTopic()%></B></td></tr>
    <tr style="font-size:12px"><td class="blue2" valign="top" bgcolor="#808080"><B><%= ML.getMLStr("messages_lbl_text", userId)%>:</B></td><td><%= (m.getText() + MessageService.getExtendedVoteText(userId, m)) %></td></tr>
    <tr style="font-size:12px"><td colspan="2">
            <%
//            String messageType = ""
                                topic = m.getTopic();
                                topic = topic.replace(' ', '+');
                                if (m.getType() == EMessageType.USER) {
            %>
            <br/>
            <%
                                    if (m.getSourceUserId() != 0 && (activeFolder != EMessageFolder.OUTBOX)) {
                                        if (baseFolder != EMessageFolder.OUTBOX) {
            %>
            <a href="main.jsp?page=new/messages&type=3&topic=<%= topic.replace("\"", "%22") %>&targetUserId=<%= m.getSourceUserId()%>"><%= ML.getMLStr("messages_link_reply", userId)%></a><br/><br/>
            <%              }} else if (activeFolder == EMessageFolder.OUTBOX) {%>
            <a href="main.jsp?page=new/messages&type=3&topic=<%= topic%>&targetUserId=<%= m.getTargetUserId()%>"><%= ML.getMLStr("messages_link_sendmessagetouser", userId)%></a><br/><br/>
            <%              }
                                }%>
            <br><br>
            </a>
            <%
            %>
            <TABLE width="100%">
               <TR>
                   <TD align="middle" colspan="3"><a style="font-size:17px;" href="main.jsp?page=new/messages&type=<%= actType %>&work=1&MID_<%= m.getMessageId()%>"><%= ML.getMLStr("messages_delete_message", userId)%><BR><BR></TD>
                </TR>                
                <TR>
                    <TD width="33%" align="middle">
<%
                        int newMsgId = MessageService.previousMessageId(userId,m);
                        if (newMsgId != -1) {
%>                        
                        <a style="font-size:17px;" href="main.jsp?page=new/messages&type=<%= actType %>&messageId=<%= newMsgId %>"><%= ML.getMLStr("msg_previous_msg", userId)%></a>
<%
                        }
%>                        
                    </TD>
                    <TD width="34%" align="middle"><a style="font-size:17px;" href="main.jsp?page=new/messages&type=<%= actType %>"><%= ML.getMLStr("global_back", userId)%></a></TD>                    
                    <TD width="33%" align="middle">
<%
                        newMsgId = MessageService.nextMessageId(userId,m);
                        if (newMsgId != -1) {
%>                                                
                        <a style="font-size:17px;" href="main.jsp?page=new/messages&type=<%= actType %>&messageId=<%= newMsgId %>"><%= ML.getMLStr("msg_next_msg", userId)%></a>
<%
                        }
%>                                                
                    </TD>
                </TR>
            </TABLE>
        </td></tr>
        <% if (m != null) {%>
</table>
<%}%>
<%
                   if ((!m.getViewed() && (m.getSourceUserId() != userId)) || (baseFolder == EMessageFolder.INBOX)) {
                        m.setViewed(true);
                        MessageService.updateMessage(m);
                    }

                } else {
%>Der Versuch eine nicht zugehoerige Nachricht darzustellen wurde der Dark Destiny-Administration gemeldet.<br/>
<br/>
Bei erneutem Auftreten kann eine Loeschung Ihres Accounts ohne weitere Warnung die Folge sein.
<%  }
                actType = 0;
            }

            if (activeFolder == EMessageFolder.INBOX) {
                ArrayList<Message> messages = new ArrayList<Message>();
                messages = MessageService.findInboxMessages(userId);
                // DebugBuffer.addLine("TARGET USERID = " + userId);

                if (messages.size() > 0) {
%>
<!--
==========================================================================================
                                        INBOX
==========================================================================================
-->
<form action="main.jsp?page=new/messages&type=1" method="post">
    <table class="trans" style="font-size:13px;" width="90%" border='0' cellpadding="0" cellspacing="0">
        <tr style="font-size:12px" class="blue2">
            <th width="38" align="center"><%= ML.getMLStr("messages_lbl_new", userId)%></th>
            <th width="30" align="center"><%= ML.getMLStr("messages_lbl_type", userId)%></th>
            <th width="100" align=left><%= ML.getMLStr("messages_lbl_sender", userId)%></th>
            <th width="*" align=left><%= ML.getMLStr("messages_lbl_subject", userId)%></th>
            <th width="120" align=left><%= ML.getMLStr("messages_lbl_date", userId)%></th>
            <th width="60" align="center"><%= ML.getMLStr("messages_lbl_selection", userId)%></th>
        </tr>
        <%}%>
        <% for (Message m : messages) {
                            String messageType = "";
                            if (m.getType() == EMessageType.SYSTEM) {
                                messageType = "SYS";
                            } else if (m.getType() == EMessageType.USER) {
                                messageType = "P2P";
                            } else if (m.getType() == EMessageType.ALLIANCE) {
                                messageType = "ALL";
                            } else {
                                // DebugBuffer.addLine("Message type set to ???");
                                messageType = "???";
                            }

        %>
        <tr style="font-size:12px" id="ROW_<%= m.getMessageId()%>">
            <td align="center"><%= (!m.getViewed()) ? "<IMG src=\"pic/gstar.png\"/>" : "" %></td>
            <td align="center"><IMG src="pic/<%= MessageService.getMessageTypePic(m) %>"/></td>
            <td><%= (m.getSourceUserId() == 0) ? ML.getMLStr("messages_lbl_system", userId) : MessageService.findUserById(m.getSourceUserId()).getGameName()%></td>
            <td onclick="clickMessage(<%= m.getMessageId()%>,false);"><a style="font-size:12px" href="main.jsp?page=new/messages&type=<%= actType %>&messageId=<%= m.getMessageId()%>"><%= m.getTopic()%></a></td>
            <%
                 java.util.Date msgDate = new java.util.Date(m.getTimeSent());
                 String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate) + " " + java.text.DateFormat.getTimeInstance().format(msgDate);
            %>
            <td nowrap><%= msgDateString%></td>
            <td align="center"><input onclick="clickMessage(<%= m.getMessageId()%>,true);" type="checkbox" id="MID_<%= m.getMessageId()%>" name="MID_<%= m.getMessageId()%>"></td>
        </tr>
        <%}%>
        <% if (messages.size() > 0) {%>
    </table>
    <%}%>
    <br/>
    <select name="work">
        <option selected value="1"><%= ML.getMLStr("messages_opt_deleteselected", userId)%></option>
        <option value="2"><%= ML.getMLStr("messages_opt_moveselected", userId)%></option>
        <option value="3"><%= ML.getMLStr("messages_opt_deleteall", userId)%></option>
        <option value="4"><%= ML.getMLStr("messages_opt_deletesystem", userId)%></option>
        <option value="5"><%= ML.getMLStr("messages_opt_deleteplayer", userId)%></option>
        <option value="6"><%= ML.getMLStr("messages_opt_deletealliance", userId)%></option>
    </select>
    <input type="submit" value="<%= ML.getMLStr("messages_but_confirm", userId)%>">
</form>
<%

            } else if (activeFolder == EMessageFolder.OUTBOX) {
                ArrayList<Message> messages = new ArrayList<Message>();
                messages = MessageService.findOutboxMessages(userId);
                // DebugBuffer.addLine("TARGET USERID = " + userId);

                if (messages.size() > 0) {
%>
<!--
==========================================================================================
                                        OUTBOX
==========================================================================================
-->
<form action="main.jsp?page=new/messages&type=2" method="post">
    <table class="trans" style="font-size:13px;" width="90%" border='0' cellpadding="0" cellspacing="0">
        <tr style="font-size:12px" class="blue2">
            <th width="40" align="center"><%= ML.getMLStr("messages_lbl_type", userId)%></th>
            <th width="100" align="left"><%= ML.getMLStr("messages_lbl_receiver", userId)%></th>
            <th width="*" align=left><%= ML.getMLStr("messages_lbl_subject", userId)%></th>
            <th width="120" align=left><%= ML.getMLStr("messages_lbl_date", userId)%></th>
            <th width="60" align="center"><%= ML.getMLStr("messages_lbl_selection", userId)%></th>
        </tr>
        <% }
                        for (Message m : messages) {
                            String messageType = "";
                            if (m.getType() == EMessageType.SYSTEM) {
                                messageType = "SYS";
                            } else if (m.getType() == EMessageType.USER) {
                                messageType = "P2P";
                            } else if (m.getType() == EMessageType.ALLIANCE) {
                                messageType = "ALL";
                            } else {
                                // DebugBuffer.addLine("Message type set to ???");
                                messageType = "???";
                            }

        %>
        <tr style="font-size:12px" id="ROW_<%= m.getMessageId()%>">
            <td align="center"><IMG src="pic/<%= MessageService.getMessageTypePic(m) %>"/></td>
            <td align="left"><%= IdToNameService.getUserName(m.getTargetUserId()) %></td>
            <td onclick="clickMessage(<%= m.getMessageId()%>,false);"><a style="font-size:12px" href="main.jsp?page=new/messages&type=<%= actType %>&messageId=<%= m.getMessageId()%>"><%= m.getTopic()%></a></td>
            <%
                            java.util.Date msgDate = new java.util.Date(m.getTimeSent());
                            String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate) + " " + java.text.DateFormat.getTimeInstance().format(msgDate);
            %>
            <td nowrap><%= msgDateString%></td>
            <td align="center"><input onclick="clickMessage(<%= m.getMessageId()%>,true);" type="checkbox" id="MID_<%= m.getMessageId()%>" name="MID_<%= m.getMessageId()%>"></td>
        </tr>
        <%}%>
        <% if (messages.size() > 0) {%>
    </table>
    <%}%>
    <br/>
    <select name="work">
        <option selected value="1"><%= ML.getMLStr("messages_opt_deleteselected", userId)%></option>
        <option value="2"><%= ML.getMLStr("messages_opt_deleteall", userId)%></option>
    </select>
    <input type="submit" value="<%= ML.getMLStr("messages_but_confirm", userId)%>">
</form>
<%
            } else if (activeFolder == EMessageFolder.SEND) {
                Set<String> s = new HashSet<String>();
                if (targetUserId != null) {
                    if (topic != null) {
                        topic = topic.replaceAll("\"", "&quot;");

                        if (!topic.startsWith("Re: ")) {
                            topic = "Re: " + topic;
                        }
                        
                        topic.replaceAll("&quot;", "\"");
                    }
                    
                    s.add(MessageService.findUserById(Integer.parseInt(targetUserId)).getGameName());
                }

                if (targetUsers != null) {
                    s = new HashSet<String>(Arrays.asList(targetUsers));
                }
                s.remove("0");

                if (!sent) {
                    ArrayList<User> users = MessageService.findAllUsers();
                    if (topic == null) {
                        topic = "";
                    }
%>
<form action="main.jsp?page=new/messages&type=3&sent=1" method="post">
    <input type="hidden" name="page" value="messageCompose"/>
    <table width="90%" border="0">

        <tr style="font-size:12px"><td rowspan="2"><select multiple size='20' name="targetUsers" onclick=''>
                    <option value="0"><%= ML.getMLStr("messages_msg_selectreceiver", userId)%>:</option>
                    <%
                            for (User u : users) {
                                if (MessageService.findUserDataById(u.getUserId()) == null) {
                                    continue;
                                }
                        %><option <% if (s.contains(u.getGameName())) {
                                                        out.write("selected");
                                                    }%> value='<%= u.getUserId()%>'><%= u.getGameName().toLowerCase()%> </option><%
                                                            }

                    %>
                </select></td>

            <td valign="top"><%= ML.getMLStr("messages_lbl_subject", userId)%>:</td>
            <td valign="top"><input name="topic" value="<%= topic%>" type="text"/></td>
        </tr>

        <tr style="font-size:12px">
            <td valign="top"><%= ML.getMLStr("messages_lbl_text", userId)%>:</td>
            <td valign="top"><textarea name="text" cols="30" rows="10"></textarea><BR>
                <BR>
                <jsp:include page="../subViews/smileys.jsp" />
            </td>
        </tr>
    </table>

    <input type="submit" value="<%= ML.getMLStr("messages_but_send", userId)%>">
</form>
<%
                } else {
                    topic = request.getParameter("topic");
                    text = request.getParameter("text");
                    targetUsers = request.getParameterValues("targetUsers");
                    targetUserId = request.getParameter("targetUserId");

                    if (user.getAdmin()) {
                        // DebugBuffer.addLine("ADMIN LIMIT SET");
                        multiUserLimit = 999;
                    }

                    if (topic == null || text == null || targetUsers == null || s.size() == 0) {
                        if (topic == null) {
                            topic = "";
                        }
                        if (text == null) {
                            text = "";
                        }
                    } else {
                        if (s.size() < multiUserLimit) {
                            //send
                            ArrayList<Integer> receivers = new ArrayList<Integer>();
                            int cnt = 0;
                            for (Iterator<String> it = s.iterator(); it.hasNext();) {
                                receivers.add(Integer.parseInt(it.next()));
                                cnt++;
                            }
                            MessageService.writeMessageToUser(userId, receivers, topic, text, false);

                            out.write(ML.getMLStr("messages_msg_sendsuccess1", userId) + " " + cnt + " " + ML.getMLStr("messages_msg_sendsuccess2", userId));
                            //&Auml;nderung 11.06.04
                            out.write("<br><br><a href='main.jsp?page=new/messages&type=1'>Weiter</a>");
                            if (cnt > 0) {
                                return;
                            }
                        } else {
                            out.write("Nachricht kann nicht an mehr als an " + multiUserLimit + " Mitspieler gesendet werden.");
                        }
                    }
                }
            } else if (activeFolder == EMessageFolder.ARCHIVE) {
                ArrayList<Message> messages = new ArrayList<Message>();
                messages = MessageService.findArchivedMessages(userId);
                if (messages.size() > 0) {
%>
<!--
==========================================================================================
                                        ARCHIVE
==========================================================================================
-->
<form action="main.jsp?page=new/messages&type=4" method="post">
    <table class="trans" style="font-size:13px;" width="90%" border='0' cellpadding="0" cellspacing="0">
        <tr style="font-size:12px" class="blue2">
<!-- Type war fr�her ein oder Ausgang, jetzt ist es Nachrichten Typ -->            
            <th width="40" align="center"><%= ML.getMLStr("messages_lbl_type", userId)%></th>
            <th width="100" align=left><%= ML.getMLStr("messages_lbl_sender", userId)%></th>
            <th width="*" align=left><%= ML.getMLStr("messages_lbl_subject", userId)%></th>
            <th width="120" align=left><%= ML.getMLStr("messages_lbl_date", userId)%></th>
            <th width="60" align="center"><%= ML.getMLStr("messages_lbl_selection", userId)%></th>
        </tr>
        <% }
                        for (Message m : messages) {
                            String gameName = ML.getMLStr("messages_lbl_system", userId);
                            if (m.getSourceUserId() != 0) {
                                gameName = MessageService.findUserById(m.getSourceUserId()).getGameName();
                            }
                            String messageType = "";
                            if (m.getType() == EMessageType.SYSTEM) {
                                messageType = "SYS";
                            } else if (m.getType() == EMessageType.USER) {
                                messageType = "P2P";
                            } else if (m.getType() == EMessageType.ALLIANCE) {
                                messageType = "ALL";
                            } else {
                                // DebugBuffer.addLine("Message type set to ???");
                                messageType = "???";
                            }

        %>
        <tr style="font-size:12px" id="ROW_<%= m.getMessageId()%>">
            <td align="center"><IMG src="pic/<%= MessageService.getMessageTypePic(m) %>"/></td>
            <td><% if (m.getSourceUserId() == userId) {
                                out.write(user.getGameName());
                            } else {
                                out.write(gameName);
                            }%></td>
            <td onclick="clickMessage(<%= m.getMessageId()%>,false);"><a style="font-size:12px" href="main.jsp?page=new/messages&type=<%= actType %>&messageId=<%= m.getMessageId()%>"><%= m.getTopic()%></a></td>
            <%

                            java.util.Date msgDate = new java.util.Date(m.getTimeSent());
                            String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate) + " " + java.text.DateFormat.getTimeInstance().format(msgDate);
            %>
            <td nowrap><%= msgDateString%></td>
            <td align="center"><input type="checkbox" onclick="clickMessage(<%= m.getMessageId()%>,true);" id="MID_<%= m.getMessageId()%>" name="MID_<%= m.getMessageId()%>"></td>
        </tr>
        <%}
                        if (messages.size() > 0) {%>
    </table>
    <%}%>
    <br/>
    <select name="work">
        <option selected value="1"><%= ML.getMLStr("messages_opt_deleteselected", userId)%></option>
        <option value="2"><%= ML.getMLStr("messages_opt_deleteall", userId)%></option>
    </select>
    <input type="submit" value="<%= ML.getMLStr("messages_but_confirm", userId)%>">
</form>
<%

            }
%>



