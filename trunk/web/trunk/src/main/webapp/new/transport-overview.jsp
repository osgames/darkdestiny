<%@ page import="at.darkdestiny.core.service.*" %>
<%@ page import="at.darkdestiny.core.*" %>
<%@ page import="at.darkdestiny.core.model.*" %>
<%@ page import="at.darkdestiny.core.result.*" %>
<%@ page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="at.darkdestiny.core.ships.*" %>
<%@ page import="at.darkdestiny.core.trade.*" %>
<%@ page import="java.util.*" %>
<%@ page import="at.darkdestiny.util.DebugBuffer.DebugLevel" %>

<%
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
            ETradeRouteSorting type = ETradeRouteSorting.SORT_BY_START;
            int action = 0;
            int routeId = 0;
            int detailId = 0;
            int routeType = 1;
            
            String enterKeywords = ML.getMLStr("transport_msg_enterkeywords", userId);
            String filter = "";
            IdToNameService itn = new IdToNameService(userId);

            if (request.getParameter("action") != null) {
                action = Integer.parseInt(request.getParameter("action"));
            }

            if (request.getParameter("routeId") != null) {
                routeId = Integer.parseInt(request.getParameter("routeId"));
            }

            if (request.getParameter("detailId") != null) {
                detailId = Integer.parseInt(request.getParameter("detailId"));
            }

            if (request.getParameter("filter") != null) {
                filter = request.getParameter("filter");
            }else{
                filter = enterKeywords;
                }
            if (request.getParameter("routeType") != null) {
                routeType = Integer.parseInt(request.getParameter("routeType"));
            }

            if (request.getParameter("sort") != null) {
                type = ETradeRouteSorting.valueOf(request.getParameter("sort"));
            }
            final int ROUTE_TYPE_INTERNAL = 1;
            final int ROUTE_TYPE_EXTERNAL = 2;


            final int ACTION_NONE = 0;
            final int ACTION_ADD_SHIP = 1;
            final int ACTION_ADD_SHIP_DO = 10;
            final int ACTION_REMOVE_SHIP = 2;
            final int ACTION_REMOVE_SHIP_DO = 20;
            final int ACTION_SET_TRANSMITTER = 3;
            final int ACTION_SET_TRANSMITTER_DO = 30;
            final int ACTION_MODIFY_ROUTE_DETAIL = 4;
            final int ACTION_MODIFY_ROUTE_DETAIL_DO = 40;
            final int ACTION_DELETE_ROUTE = 5;
            final int ACTION_DELETE_ROUTE_DETAIL = 6;
            final int ACTION_PAUSE_ROUTE_DETAIL = 7;
            final int ACTION_ACCEPT_ROUTE = 15;

            BaseResult result = null;

            switch (action) {
                case (ACTION_ADD_SHIP_DO):
                    result = TransportService.addShipsToRoute(userId, routeId, request.getParameterMap());
                    action = 0;
                    break;
                case (ACTION_SET_TRANSMITTER_DO):
                    int newCap = Integer.parseInt(request.getParameter("ntc"));
                    result = TransportService.setTransmitterCapacity(userId, routeId, newCap);
                    action = 0;
                    break;
                case (ACTION_REMOVE_SHIP_DO):
                    result = TransportService.removeShipsFromRoute(userId, routeId, request.getParameterMap());
                    action = 0;
                    break;
                case (ACTION_MODIFY_ROUTE_DETAIL_DO):
                    action = 0;
                    break;
                case (ACTION_DELETE_ROUTE):
                    result = TransportService.deleteRoute(userId, routeId);
                    action = 0;
                    break;
                case (ACTION_DELETE_ROUTE_DETAIL):
                    result = TransportService.deleteRouteDetail(userId, detailId);
                    action = 0;
                    break;
                case (ACTION_PAUSE_ROUTE_DETAIL):
                    result = TransportService.switchActiveRouteDetail(userId, detailId);
                    action = 0;
                    break;
                case (ACTION_ACCEPT_ROUTE):
                    result = TransportService.acceptRoute(userId, routeId);
                    action = 0;
                    break;
            }

            switch (action) {
                case (ACTION_NONE):
                    TradeRouteResult trr = null;
                    TradeRouteResult trrInt = TransportService.getAllInternalTradeRoutes(userId);
                    TradeRouteResult trrExt = TransportService.getAllExternalTradeRoutes(userId);
                    if (routeType == ROUTE_TYPE_INTERNAL) {
                        trr = trrInt;
                    } else if (routeType == ROUTE_TYPE_EXTERNAL) {
                        trr = trrExt;
                    }

                    TradeRouteResult sortedResult = trr.getSortedResult(type);
%>
<script language = "JavaScript">
    var routes = new Array(<%= sortedResult.getRoutes().size()%>);
    <% for (int i = 0; i < sortedResult.getRoutes().size(); i++) {
                            TradeRouteExt tre = sortedResult.getRoutes().get(i);
    %>
        routes[<%= i%>] = new Array(2);
        //Setting id
        routes[<%= i%>][0] = <%= tre.getBase().getId()%>;
        //Setting metadata
        routes[<%= i%>][1] = new Array(5);
        routes[<%= i%>][1][0] = <%= tre.getBase().getStartPlanet()%>;
        routes[<%= i%>][1][1] = <%= tre.getBase().getTargetPlanet()%>;
        routes[<%= i%>][1][2] = '<%= itn.getPlanetName(tre.getBase().getStartPlanet())%>';
        routes[<%= i%>][1][3] = '<%= itn.getPlanetName(tre.getBase().getTargetPlanet())%>';
        routes[<%= i%>][1][4] = new Array(<%= sortedResult.getRouteDetails(tre).size()%>);
    <% for (int j = 0; j < sortedResult.getRouteDetails(tre).size(); j++) {
             TradeRouteDetail trd = sortedResult.getRouteDetails(tre).get(j);
    %>
                routes[<%= i%>][1][4][<%= j%>] = <%= trd.getId()%>
    <% }%>
    <% }%>

        function toggleTradeRoutes()
        {
            var searchString = getElement('id', 'filter').value;
            var foundEntries = 0;
            var firstEntry = 1;
            if(searchString != '<%= enterKeywords%>'){
                for(var i = 0; i < routes.length; i++){

                    var found = false;
                    for(var j = 0; j < 4; j++){
                        var metaSub = String(routes[i][1][j]).substring(0, searchString.length);
                        if(metaSub == searchString){
                            getElement('id', 'header'+ String(routes[i][0])).style.display = '';
                            
                            try{
                                getElement('id', 'spacer'+ String(routes[i][0])).style.display = '';
                            }catch(err){}
                            try{
                                getElement('id', 'addlink'+ String(routes[i][0])).style.display = '';
                            }catch(err){}
                            found = true;
                            break;
                        }else{
                            getElement('id', 'header'+ String(routes[i][0])).style.display = 'none';
                            try{
                                getElement('id', 'spacer'+ String(routes[i][0])).style.display = 'none';
                            }catch(err){}
                            try{
                                getElement('id', 'addlink'+ String(routes[i][0])).style.display = 'none';
                            }catch(err){}
                        }
                       
                    }
                   
                    for(var k = 0; k < routes[i][1][4].length; k++){
                         
                        if(found){
                            foundEntries = foundEntries + 1;
                            getElement('id', 'entries'+ String(routes[i][1][4][k])).style.display = '';
                        }else{
                            getElement('id', 'entries'+ String(routes[i][1][4][k])).style.display = 'none';
                        }
                    }
                    if(foundEntries > 10 || firstEntry == 1){
                        try{
                            getElement('id', 'labels'+ String(routes[i][0])).style.display = '';
                            foundEntries = 0;
                            firstEntry = 0;
                        }catch(err){}
                    }else{
                        try{
                            getElement('id', 'labels'+ String(routes[i][0])).style.display = 'none';
                        }catch(err){
                        }
                    }
                }
            }
        }
        function setActPlanet()
        {
            getElement('id', 'filter').value = <%= planetId%>;
            toggleTradeRoutes();
        }

</script>

<TABLE align="center" width="40%">
    <TR class="blue2">
        <TD align="center">
            <A href="main.jsp?page=new/transport&type=0&routeType=1"><%= ML.getMLStr("transport_link_internal", userId)%> (<%= trrInt.getRoutes().size()%>)</A>
        </TD>
        <TD align="center">
            <A href="main.jsp?page=new/transport&type=0&routeType=2"><%= ML.getMLStr("transport_link_external", userId)%> (<%= trrExt.getRoutes().size()%>)</A>
        </TD>
    </TR>
</TABLE>
<%
                    if (result != null) {
                        if (result.isError()) {
%>
<FONT color="red"><B><%= result.getMessage()%></B></FONT><BR><BR>
<%
                        } else {
%>
<FONT color="lightgreen"><B><%= result.getMessage()%></B></FONT><BR><BR>
<%
                        }
                    }
%>
<SELECT  style='background-color: #000000; color: #ffffff; width:250px; height:17px; border:0px; text-align: center;' id="type" name="typeSelect" onChange="window.location.href = 'main.jsp?page=new/transport&sort='+document.getElementById('type').value">
    <OPTION <% if (type == ETradeRouteSorting.SORT_BY_START) {%> SELECTED <% }%>  VALUE="<%=  ETradeRouteSorting.SORT_BY_START%>"><%= ML.getMLStr("transport_opt_sortbystartplanet", userId)%></OPTION>
    <OPTION <% if (type == ETradeRouteSorting.SORT_BY_TARGET) {%> SELECTED <% }%>  VALUE="<%= ETradeRouteSorting.SORT_BY_TARGET%>"><%= ML.getMLStr("transport_opt_sortbytargetplanet", userId)%></OPTION>
</SELECT>
<BR><BR>
<%
                    int actEntry = 11;

                    if (sortedResult.getRoutes().size() > 0) {
%>
<!-- FILTER START  -->
<TABLE border="0" style="font-size:11px; color:white;"  align="center" cellpadding="1" width="90%" cellspacing="1">
    <TR>
        <TD width="50px" class="blue2">
            <B><%= ML.getMLStr("transport_lbl_filter", userId)%>:</B>
        </TD>
        <TD width="300px">
            <INPUT id="filter" name="filter" style="background-color: #000000; color: #ffffff; border:1px; border-color: gray; border-width: thin; border-style: solid; width: 300px; font-size: 12px;" type="text" value="<%= filter%>" onkeyup="toggleTradeRoutes()" />
        </TD>
        <TD align="left">
            <INPUT style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;" type="button" value="<%= ML.getMLStr("transport_lbl_actualplanet", userId)%>" onclick="setActPlanet()" />
        </TD>
    </TR>
    <TR>
        <TD>

        </TD>
    </TR>
</TABLE>
<TABLE border="0" class="trade" WIDTH="90%">
    <%
                            for (TradeRouteExt tre : sortedResult.getRoutes()) {
                                int shipCount = tre.getAssignedShipCount();
                                
    %>
    <%if (actEntry > 10) {
                                        actEntry = 0;
    %>
    <TR id="labels<%= tre.getBase().getId()%>" style="font-weight: bold; font-size: 11px; font-weight: bolder;">
        <TD width="20">&nbsp</TD>
        <TD style="background-color:#909090; color: black;"><%= ML.getMLStr("transport_lbl_target", userId)%></TD>
        <TD style="background-color:#909090; color: black;"><%= ML.getMLStr("transport_lbl_resource", userId)%></TD>
        <TD style="background-color:#909090; color: black;"><%= ML.getMLStr("transport_lbl_remainingquantity", userId)%></TD>
        <TD style="background-color:#909090; color: black;"><%= ML.getMLStr("transport_lbl_remainingtime", userId)%></TD>
        <TD style="background-color:#909090; color: black;"><%= ML.getMLStr("transport_lbl_delivered", userId)%></TD>
        <TD style="background-color:#909090; color: black;"><%= ML.getMLStr("transport_lbl_quantitypertick", userId)%></TD>
        <TD style="background-color:#909090; color: black;"><%= ML.getMLStr("transport_lbl_actions", userId)%></TD>
    </TR>
    <%
                                    }
    %>
    <TR id="header<%= tre.getBase().getId()%>" style="font-weight: bold;" style="">
        <TD colspan="7">
            <TABLE width="100%" style="font-weight: bold; font-size: 12px; font-weight: bolder;">
                <TR>
                    <TD align="center" style="background-color:#404040;" width="150px">
                        <%= itn.getPlanetName(tre.getBase().getStartPlanet())%>
                    </TD>
                    <TD align="center" width="20px" colspan="1">
                        <=>
                    </TD>
                    <TD align="center" style="background-color:#404040;" width="150px">
                        <%= itn.getPlanetName(tre.getBase().getTargetPlanet())%>
                    </TD>
                    <% if(shipCount > 0){ %>
                    <TD width="*">
                        &nbsp;
                    </TD>
                    <TD align="center" style="background-color:#404040;" width="200px">
                        <%= ML.getMLStr("transport_lbl_assignedships", userId)%>: <%= shipCount %>
                    </TD>
                    <% } else {%>
                    <TD width="*">
                        &nbsp;
                    </TD>
                    <% } %>
                </TR>
            </TABLE>
        </TD>
        <TD style="background-color: #404040;"><TABLE  cellpadding="0" cellspacing="0" border="0" style="background-color: #000000;"><TR>
                    <% if (tre.isAddingShipsPossible()) {%><TD><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_assignships", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/addShips.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&routeId=<%= tre.getBase().getId()%>&action=<%= ACTION_ADD_SHIP%>'" /></TD><% }%>
                    <% if (tre.isRemovingShipsPossible()) {%><TD><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_removeships", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/removeShips.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&routeId=<%= tre.getBase().getId()%>&action=<%= ACTION_REMOVE_SHIP%>'" /></TD><% }%>
                    <% if (tre.isAddingTransmitterPossible()) {%><TD><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_assigntransmitter", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/icons/transmitter_on.gif" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&routeId=<%= tre.getBase().getId()%>&action=<%= ACTION_SET_TRANSMITTER%>'" /></TD><% }%>
                    <% if (tre.isRemovingTransmitterPossible()) {%><TD><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_removetransmitter", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/icons/transmitter_off.gif" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&routeId=<%= tre.getBase().getId()%>&action=<%= ACTION_SET_TRANSMITTER%>'" /></TD><% }%>
                        <% if (tre.getBase().getStatus() == ETradeRouteStatus.PENDING) {
                                                            if (tre.getBase().getTargetUserId() != userId) {%>
                    <TD><IMG border=0 onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_waitingtobeaccepted", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/vote.jpg" /></TD>
                        <% } else {%>
                    <TD><IMG onclick="window.location.href = 'main.jsp?page=new/transport&type=0&routeType=<%= ROUTE_TYPE_EXTERNAL%>&routeId=<%= tre.getBase().getId()%>&action=<%= ACTION_ACCEPT_ROUTE%>'" border=0 onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_acceptoffer", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/contract.png" /></TD>
                        <% }
                                                        }%>
                    <TD><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_deleteroute", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&filter=' + document.getElementById('filter').value + '&routeId=<%= tre.getBase().getId()%>&action=<%= ACTION_DELETE_ROUTE%>'" /></TD>
                </TR></TABLE></TD>
    </TR>
    <%
                                    boolean firstReversed = false;%>
    <%      if (sortedResult.getRouteDetailsFromTo(tre).size() > 0) {
    %>
    <%


              for (TradeRouteDetail trd : sortedResult.getRouteDetailsFromTo(tre)) {
                  if (!firstReversed) {
                      firstReversed = true;
                  }
                  actEntry++;
                  
                  TradeRoute tr = TradeService.getRoute(trd.getRouteId());
                  String agriDistri = "";
                  
                  if ((trd.getRessId() == Ressource.FOOD) && Service.planetConstructionDAO.isConstructed(tr.getStartPlanet(), 43)) {
                      agriDistri = "<B><FONT color=\"yellow\"> Agrarg&uuml;terzentrum</FONT></B>";
                  }
    %>
    <TR id="entries<%= trd.getId()%>">
        <TD width="20">&nbsp</TD>
        <TD><%= itn.getPlanetName(tre.getBase().getTargetPlanet())%></TD>
        <TD>
            <TABLE style="font-size: 11px; font-weight: bolder; border-spacing: 0px;" cellpadding="0" cellspacing="0" >
                <TR>
                    <TD width="16"><IMG alt="<%= ML.getMLStr(OverviewService.findRessourceById(trd.getRessId()).getName(), userId)%>" height="15" width="15" src="<%= GameConfig.getInstance().picPath()%><%= OverviewService.findRessourceById(trd.getRessId()).getImageLocation()%>"/></TD>
                    <TD><%= itn.getRessourceName(trd.getRessId())%></TD>
                </TR>
            </TABLE>
        </TD>
        <%
                          long restQty = 0;
                          if (trd.getMaxQty() > 0) {
                              restQty = trd.getMaxQty() - trd.getQtyDelivered();
                          }
        %>
        <TD><%= FormatUtilities.getInfForZero((int) restQty)%></TD>
        <TD><%= FormatUtilities.getInfForZero(trd.getRemainingTime(GameUtilities.getCurrentTick2()))%></TD>
        <TD><%= FormatUtilities.getFormattedNumber(trd.getQtyDelivered())%></TD>
        <TD><%= FormatUtilities.getFormattedNumber(trd.getLastTransport())%> (<%= FormatUtilities.getInfForZero(trd.getMaxQtyPerTick())%>)<%= agriDistri %></TD>
        <TD><TABLE cellpadding="0" cellspacing="0" border="0"><TR>
                    <TD><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_deleterouteentry", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&filter=' + document.getElementById('filter').value + '&detailId=<%= trd.getId()%>&action=<%= ACTION_DELETE_ROUTE_DETAIL%>'" /></TD>
                    <% if (!trd.getDisabled()) {%><TD><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_pause", userId)%>')" onmouseout="hideTip()" border=0 src="<%= GameConfig.getInstance().picPath()%>pic/stop.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&detailId=<%= trd.getId()%>&action=<%= ACTION_PAUSE_ROUTE_DETAIL%>'" /></TD><% }%>
                    <% if (trd.getDisabled()) {%><TD><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_resume", userId)%>')" onmouseout="hideTip()" border=0 src="<%= GameConfig.getInstance().picPath()%>pic/start.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&detailId=<%= trd.getId()%>&action=<%= ACTION_PAUSE_ROUTE_DETAIL%>'" /></TD><% }%>
                </TR></TABLE></TD>
    </TR>    
    <%
                                        }
                                    }
                                    if (sortedResult.getRouteDetailsToFrom(tre).size() > 0) {
                                        for (TradeRouteDetail trd : sortedResult.getRouteDetailsToFrom(tre)) {
                                            if (firstReversed) {
                                                firstReversed = false;
    %>
    <TR id="spacer<%= tre.getBase().getId()%>" style="height: 3px;"><TD table-layout: fixed colspan="8" width="100%"><DIV style="height:1px;"></DIV></TD></TR>
    <%
                                    }
    %>
    <TR id="entries<%= trd.getId()%>">
        <TD width="20">&nbsp</TD>
        <TD><%= itn.getPlanetName(tre.getBase().getStartPlanet())%></TD>
        <TD>
            <TABLE style="font-size: 11px; font-weight: bolder;">
                <TR>
                    <TD width="16"><IMG alt="<%= ML.getMLStr(OverviewService.findRessourceById(trd.getRessId()).getName(), userId)%>" height="15" width="15" src="<%= GameConfig.getInstance().picPath()%><%= OverviewService.findRessourceById(trd.getRessId()).getImageLocation()%>"/></TD>
                    <TD><%= itn.getRessourceName(trd.getRessId())%></TD>
                </TR>
            </TABLE>
        </TD>
        <%
                                        long restQty = 0;
                                        if (trd.getMaxQty() > 0) {
                                            restQty = trd.getMaxQty() - trd.getQtyDelivered();
                                        }
        %>
        <TD><%= FormatUtilities.getInfForZero((int) restQty)%></TD>
        <TD><%= FormatUtilities.getInfForZero(trd.getRemainingTime(GameUtilities.getCurrentTick2()))%></TD>
        <TD><%= FormatUtilities.getFormattedNumber(trd.getQtyDelivered())%></TD>
        <TD><%= FormatUtilities.getFormattedNumber(trd.getLastTransport())%> (<%= FormatUtilities.getInfForZero(trd.getMaxQtyPerTick())%>)</TD>
        <TD><TABLE cellpadding="0" cellspacing="0" border="0"><TR>
                    <TD><IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&filter=' + document.getElementById('filter').value + '&detailId=<%= trd.getId()%>&action=<%= ACTION_DELETE_ROUTE_DETAIL%>'" /></TD>
                    <% if (!trd.getDisabled()) {%><TD><IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/stop.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&detailId=<%= trd.getId()%>&action=<%= ACTION_PAUSE_ROUTE_DETAIL%>'" /></TD><% }%>
                    <% if (trd.getDisabled()) {%><TD><IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/start.jpg" onclick="window.location.href = 'main.jsp?page=new/transport&type=0&detailId=<%= trd.getId()%>&action=<%= ACTION_PAUSE_ROUTE_DETAIL%>'" /></TD><% }%>
                </TR></TABLE></TD>
    </TR>    
    <%
                                        }
                                    }
                                    if (routeType == ROUTE_TYPE_INTERNAL) {
    %>
    <TR id="addlink<%= tre.getBase().getId()%>">
        <TD width="20">&nbsp</TD>
        <TD colspan="7" ><A style="font-size: 11px; text-decoration: underline;" href="main.jsp?page=new/transport&type=1&sourcePlanetId=<%= tre.getBase().getStartPlanet()%>&targetPlanetId=<%= tre.getBase().getTargetPlanet()%> ">(+) <%= ML.getMLStr("transport_link_addnewrouteentry", userId)%></A></TD>
    </TR>
    <%
                                }
                            }
    %>
</TABLE>
<%
                    } else {
%>
<%= ML.getMLStr("transport_msg_noroutesexisting", userId)%><A target="new" href="http://www.thedarkdestiny.at/wiki/index.php/Transport"><IMG border="0" alt="techtree" onmouseover="doTooltip(event,'<%= ML.getMLStr("transport_pop_wiki", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.getInstance().picPath()%>pic/infoT.png" /></A>
    <%
                        }
                        break;
                    case (ACTION_ADD_SHIP):
                        ArrayList<ShipData> ships = TransportService.getAllTransportShips(userId, routeId);

    %>
<FORM method="post" action="main.jsp?page=new/transport&type=0&routeId=<%= routeId%>&action=<%= ACTION_ADD_SHIP_DO%>">
    <TABLE class="blue" width="80%" cellpadding=0 cellspacing=0>
        <TR>
            <TD class="blue2" width="35%" bgcolor="#808080" align="center"><B><%= ML.getMLStr("transport_lbl_fleetname", userId)%></B></TD>
            <TD class="blue2" width="35%" bgcolor="#808080" align="center"><B><%= ML.getMLStr("transport_lbl_designname", userId)%></B></TD>
            <TD class="blue2" width="10%" bgcolor="#808080" align="center"><B><%= ML.getMLStr("transport_lbl_maxavailable", userId)%></B></TD>
            <TD class="blue2" width="20%" bgcolor="#808080" align="center"><B><%= ML.getMLStr("transport_lbl_tomove", userId)%></B></TD>
        </TR>
        <%
                                for (ShipData sd : ships) {
        %>
        <TR>
            <TD width="35%" align="center"><%=sd.getFleetName()%></TD>
            <TD width="35%" align="center"><%=sd.getDesignName()%></TD>
            <TD width="10%" align="center"><%=sd.getCount()%></TD>
            <TD width="20%" align="center">
                <INPUT NAME="fid<%=sd.getFleetId()%>did<%=sd.getDesignId()%>" TYPE="text" VALUE="0" SIZE="6" ALIGN=middle>
            </TD>
        </TR>
        <%
                                }
        %>
    </TABLE>
    <BR>    
    <INPUT type="submit" name="Zuteilen" value="<%= ML.getMLStr("transport_but_assign", userId)%>">
</FORM>   
<%
                    break;
                case (ACTION_REMOVE_SHIP):
                    ArrayList<TradeShipData> shipsRemove = TransportService.getShipsInRoute(userId, routeId);
%>
<FORM method="post" action="main.jsp?page=new/transport&type=0&routeId=<%= routeId%>&action=<%= ACTION_REMOVE_SHIP_DO%>">
    <TABLE class="blue" width="80%" cellpadding=0 cellspacing=0>
        <TR>
            <TD class="blue2" width="55%" bgcolor="#808080" align="center"><B><%= ML.getMLStr("transport_lbl_designname", userId)%></B></TD>
            <TD class="blue2" width="10%" bgcolor="#808080" align="center"><B><%= "Home"%></B></TD>
            <TD class="blue2" width="10%" bgcolor="#808080" align="center"><B><%= ML.getMLStr("transport_lbl_maxavailable", userId)%></B></TD>
            <TD class="blue2" width="25%" bgcolor="#808080" align="center"><B><%= ML.getMLStr("transport_lbl_tomove", userId)%></B></TD>
        </TR>
        <%
                            for (TradeShipData sd : shipsRemove) {
        %>
        <TR>
            <TD width="55%" align="center"><%=sd.getDesignName()%></TD>
            <TD width="10%" align="center"><%=sd.getHome().getPlanetId()%></TD>
            <TD width="10%" align="center"><%=sd.getCount()%></TD>
            <TD width="25%" align="center">
                <INPUT NAME="did<%=sd.getDesignId()%>_<%=sd.getHome().getPlanetId()%>" TYPE="text" VALUE="0" SIZE="6" ALIGN=middle>
            </TD>
        </TR>
        <%
                            }
        %>
    </TABLE>
    <BR>    
    <INPUT type="submit" name="Zuteilen" value="<%= ML.getMLStr("transport_but_assign", userId)%>">
</FORM>   
<%
                    break;
                case (ACTION_SET_TRANSMITTER):
                    routeId = Integer.parseInt(request.getParameter("routeId"));
                    TradeRouteTransmitterInfo trti = TradeUtilities.getRouteTransmitterInfo(routeId);

                    String startPlanetName = PlanetService.getPlanetName(trti.getPlanetIdStart()).getName();
                    String destPlanetName = PlanetService.getPlanetName(trti.getPlanetIdTarget()).getName();

                    int freeCapStart = trti.getTotalCapStart() - trti.getUsedCapStart();
                    int freeCapTarget = trti.getTotalCapTarget() - trti.getUsedCapTarget();
%>
<FORM method="post" action="main.jsp?page=new/transport&type=0&routeId=<%= routeId%>&action=<%= ACTION_SET_TRANSMITTER_DO%>" Name="addtransmitter">
    <TABLE width="80%" style="font-size:13px">
        <TR><TD>Freie Kapazit&auml;t auf <%= startPlanetName%></TD><TD><%= freeCapStart%></TD></TR>
        <TR><TD>Freie Kapazit&auml;t auf <%= destPlanetName%></TD><TD><%= freeCapTarget%></TD></TR>
        <TR><TD>Aktuell zugeteilte Kapazit&auml;t</TD><TD><%= trti.getUsageThisRoute()%></TD></TR>
        <TR><TD colspan=2>&nbsp;</TD></TR>
        <TR><TD>Neue Kapazit&auml;tszuteilung</TD>
            <TD>
                <INPUT NAME="ntc" TYPE="text" VALUE="<%= trti.getUsageThisRoute()%>" SIZE="6" ALIGN=middle>&nbsp;&nbsp;<INPUT type="submit" name="Setzen" value="Setzen">
            </TD>
        </TR>
    </TABLE>
</FORM> 
<%
                    break;
                case (ACTION_MODIFY_ROUTE_DETAIL):
%>
Modify detail
<%
                    break;
            }
            if (request.getParameter("filter") != null) {
                %>
                <script language="JavaScript">
                    toggleTradeRoutes();
                    </script>
                <%
            }
%>