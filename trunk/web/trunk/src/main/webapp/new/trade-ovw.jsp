<%@page import="at.darkdestiny.core.model.Construction"%>
<%@page import="at.darkdestiny.core.model.PlanetConstruction"%>
<%@page import="at.darkdestiny.core.ML"%>
<%@page import="at.darkdestiny.core.view.TradeRouteView"%>
<%@page import="at.darkdestiny.core.model.TradeRouteDetail"%>
<%@page import="at.darkdestiny.core.trade.TradeRouteExt"%>
<%@page import="at.darkdestiny.core.enumeration.ETradeOfferType"%>
<%@page import="at.darkdestiny.core.GameConstants"%>
<%@page import="at.darkdestiny.core.FormatUtilities"%>
<%@page import="at.darkdestiny.core.model.PlanetRessource"%>
<%@page import="at.darkdestiny.core.enumeration.EPlanetRessourceType"%>
<%@page import="at.darkdestiny.core.model.Ressource"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.model.TradeOffer"%>
<%@page import="at.darkdestiny.core.model.PriceListEntry"%>
<%@page import="java.util.ArrayList"%>
<%@page import="at.darkdestiny.core.GameConfig"%>
<%@page import="at.darkdestiny.core.service.TradeService2"%>
<%@page import="at.darkdestiny.core.model.TradePost"%>

<script language = "JavaScript">
    
    function hideInput(ressId, type){
        var field = document.getElementById('ress_' + ressId + '_amount');
        if(type == 'MINIMUM'){
            field.setAttribute("style", "visibility:hidden");
        }else{
            field.setAttribute("style", "visibility:none");
        }
    }
    
</script>
<%

session.setAttribute("actPage", "new/trade-main");
            final int SUBTYPE_OVERVIEW = 1;
            final int SUBTYPE_RENAME = 2;
            final int SUBTYPE_SET = 3;

            final int PROCESS_RENAME = 1;
            final int PROCESS_SET = 2;
            int subType = SUBTYPE_OVERVIEW;
            int process = 0;

            if (request.getParameter("subType") != null) {
                subType = Integer.parseInt(request.getParameter("subType"));
            }
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));

            if (process > 0) {

                if (process == PROCESS_RENAME) {
                    TradeService2.renameTradePost(userId, Integer.parseInt(request.getParameter("tradePostId")), request.getParameter("name"));
                }
                if (process == PROCESS_SET) {
                    TradeService2.setTradePostOffer(userId, Integer.parseInt(request.getParameter("tradePostId")), request.getParameterMap());
                }
            }

            switch (subType) {
                case (SUBTYPE_OVERVIEW): {
                     ArrayList<TradeRouteView> outRoutes = TradeService2.getAllOutgoingTrades(userId, planetId);
 if(!outRoutes.isEmpty()){
%>
<TABLE class="bluetrans" width="80%" style="font-size: 11px; font-weight: bolder;">
    <TR class="blue2">
        <TD colspan="5" align="center">
            <%= ML.getMLStr("trade_lbl_outgoingtraderoutes", userId) %>
        </TD>
    </TR>
    
    <TR class="blue2">
        <TD></TD>
        <TD><%= ML.getMLStr("trade_lbl_resource", userId) %></TD>
        <TD><%= ML.getMLStr("trade_lbl_amount", userId) %></TD>
        <TD><%= ML.getMLStr("trade_lbl_amountpertick", userId) %></TD>
        <TD><%= ML.getMLStr("trade_lbl_reason", userId) %></TD>
    </TR>
    <% for(TradeRouteView trv : outRoutes){
    TradeRouteDetail tr = trv.getTrd();
    Ressource r = Service.ressourceDAO.findRessourceById(tr.getRessId());
    %>
    
    <TR>
        <TD><IMG style="width:18px; height:18px" src="<%= GameConfig.getInstance().picPath() + r.getImageLocation()%>"/></TD>
        <TD><%= ML.getMLStr(r.getName(), userId) %></TD>
        <TD><%= FormatUtilities.getFormattedNumber((tr.getMaxQty() - tr.getQtyDelivered())) %> </TD>
        <TD><%= FormatUtilities.getFormattedNumber((tr.getLastTransport())) %></TD>
        <TD><%= ML.getMLStr("TR_" + tr.getCapReason().toString(), userId) %></TD>
    </TR>
    <% } %>
</TABLE> 
<% } %>
      <%              ArrayList<TradeRouteView> routes = TradeService2.getAllIncomingTrades(userId, planetId);
      if(!routes.isEmpty()){
%>
<TABLE class="bluetrans" width="80%" style="font-size: 11px; font-weight: bolder;">
    <TR class="blue2">
        <TD colspan="6" align="center">
            <%= ML.getMLStr("trade_lbl_incomingtraderoutes", userId) %>
        </TD>
    </TR>
    <TR class="blue2">
        <TD></TD>
        <TD><%= ML.getMLStr("trade_lbl_resource", userId) %></TD>
        <TD><%= ML.getMLStr("trade_lbl_amount", userId) %></TD>
        <TD><%= ML.getMLStr("trade_label_target", userId) %></TD>
        <TD><%= ML.getMLStr("trade_lbl_amountpertick", userId) %></TD>
        <TD><%= ML.getMLStr("trade_lbl_reason", userId) %></TD>
    </TR>
    <% for(TradeRouteView trv : routes){
    TradeRouteDetail tr = trv.getTrd();
    Ressource r = Service.ressourceDAO.findRessourceById(tr.getRessId());
    %>
    
    <TR>
        <TD><IMG style="width:18px; height:18px" src="<%= GameConfig.getInstance().picPath() + r.getImageLocation()%>"/></TD>
        <TD><%= ML.getMLStr(r.getName(), userId) %></TD>
        <TD><%= FormatUtilities.getFormattedNumber((tr.getMaxQty() - tr.getQtyDelivered())) %> </TD>
        <TD><%= trv.getTargetPlanet() %></TD>
        <TD><%= FormatUtilities.getFormattedNumber((tr.getLastTransport())) %></TD>
        <TD><%= ML.getMLStr("TR_" + tr.getCapReason().toString(), userId) %></TD>
    </TR>
    <% } %>
</TABLE>
<% } %>
<BR>
<BR>
<TABLE class="bluetrans" width="80%" >
    <TR>
        <TD class="blue2" colspan="3"><%= ML.getMLStr("trade_lbl_availabletradeposts", userId) %></TD>
    </TR>
    <%
                            for (TradePost tp : TradeService2.getAllOwnTradeposts(userId, planetId)) {
                                PlanetConstruction pc = Service.planetConstructionDAO.findBy(tp.getPlanetId(), Construction.ID_INTERSTELLARTRADINGPOST);
    %>
    <TR>
        <TD class="blue2"><%= ML.getMLStr("trade_lbl_tradepost", userId) %> <B><%= tp.getName()%></B></TD>
        <TD class="blue2" align="left"><%= ML.getMLStr("trade_lbl_capacity", userId) %>: <B><%= FormatUtilities.getFormattedNumber((tp.getTransportPerLJ() * pc.getLevel())) %></B>   <%= ML.getMLStr("trade_lbl_boost", userId) %>: <B>x<%= tp.getCapBoost() %></B></TD>
        <TD class="blue2" align="center">
            <TABLE cellspacing ="0" cellpadding="0" width="100%">
                <TR>
                    <TD>
                        <IMG style="width:15px; height:15px" onclick="window.location.href = 'main.jsp?page=new/trade-main&type=1&tradePostId=<%= tp.getId()%>&subType=<%= SUBTYPE_RENAME%>'" onmouseover="doTooltip(event,'<%= ML.getMLStr("trade_pop_rename", userId) %>')" onmouseout="hideTip()"   src="<%= GameConfig.getInstance().picPath()%>pic/umbenennen.jpg"/>
                    </TD>
                    <TD>
                        <IMG style="width:15px; height:15px" onclick="window.location.href = 'main.jsp?page=new/trade-main&type=1&tradePostId=<%= tp.getId()%>&subType=<%= SUBTYPE_SET%>'" onmouseover="doTooltip(event,'<%= ML.getMLStr("trade_pop_setvalues", userId) %>')" onmouseout="hideTip()"   src="<%= GameConfig.getInstance().picPath()%>pic/edit.jpg"/>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
    <TR>
        <TD colspan="3">
            <TABLE class="bluetrans" style="width:100%; font-size: 11px; font-weight: bolder;">
                <TR class="blue2">
                    <TD><%= ML.getMLStr("trade_lbl_resource", userId) %></TD>
                    <TD align="center"><%= ML.getMLStr("trade_lbl_instorage", userId) %></TD>
                    <TD align="center"><%= ML.getMLStr("trade_lbl_tosell", userId) %></TD>
                    <TD align="center" onmouseover="doTooltip(event,'MINIMUM - ab Mindestbestandsmenge <BR> FIXED - Fixer Betrag')" onmouseout="hideTip()" >Typ</TD>
                    <TD align="center"><%= ML.getMLStr("trade_lbl_sold", userId) %></TD>
                </TR>
                <% for (Ressource r : (ArrayList<Ressource>) Service.ressourceDAO.findAllStoreableRessources()) {
                                        TradeOffer to = TradeService2.getOffersByTradePost(tp.getId(), r.getId());
                                        if (to != null) {
                %>
                <TR>
                    <TD>
                        <IMG style="width:18px; height:18px" src="<%= GameConfig.getInstance().picPath() + r.getImageLocation()%>"/></TD>
                    <TD align="center">
                        <% PlanetRessource pr = Service.planetRessourceDAO.findBy(tp.getPlanetId(), r.getId(), EPlanetRessourceType.INSTORAGE);
                                String qty = "0";
                                if (pr != null) {
                                    qty = FormatUtilities.getFormatScaledNumber(pr.getQty(), GameConstants.VALUE_TYPE_NORMAL);
                                }
                        %>
                        <%= qty %>
                    </TD>
                    <% if (to.getQuantity() < 0) {%>
                    <TD align="center">-<%= ML.getMLStr("trade_label_notset", userId) %>-</TD>
                    <TD align="center">-<%= ML.getMLStr("trade_label_notset", userId) %>-</TD>
                    <% } else {%>
                    <TD align="center"><%= FormatUtilities.getFormatScaledNumber(to.getQuantity(), GameConstants.VALUE_TYPE_NORMAL) %>
                    </TD>
                    <TD align="center"><%= ML.getMLStr("trade_enum_"+to.getType().toString(), userId) %></TD>
                    <% }%>
                    <TD align="center"><%= FormatUtilities.getFormattedNumber(to.getSold())%></TD>
                </TR>
                <% }%>
                <% }%>
            </TABLE>
        </TD>
    </TR>
    <% }%>
</TABLE>

<%          }
                break;
                case (SUBTYPE_RENAME): {
                    TradePost tp = TradeService2.getTradePost(userId, Integer.parseInt(request.getParameter("tradePostId")));
%>
<FORM method="post" action="main.jsp?page=new/trade-main&type=1&process=<%= PROCESS_RENAME%>">
    <INPUT type="hidden" name="tradePostId" value="<%= Integer.parseInt(request.getParameter("tradePostId"))%>"/>
    <TABLE class="bluetrans" align="center">
        <TR>
            <TD class="blue"><%= ML.getMLStr("trade_lbl_name", userId) %>:</TD>
            <TD><INPUT name="name" value="<%= tp.getName()%>" type="text" class="dark" style="width:200px"/></TD>
        </TR>
        <TR>
            <TD colspan="2" align="center">
                <INPUT type="submit" class="dark" value="<%= ML.getMLStr("trade_but_change", userId) %>" />
            </TD>
        </TR>
    </TABLE>
</FORM>
<%      }
                break;
                case (SUBTYPE_SET): {
                    TradePost tp = Service.tradePostDAO.findById(Integer.parseInt(request.getParameter("tradePostId")));
%>
<FORM method="POST" action="main.jsp?page=new/trade-main&type=1&process=<%= PROCESS_SET %>">
    <INPUT type="hidden" name="tradePostId" value="<%= Integer.parseInt(request.getParameter("tradePostId"))%>"/>
    <TABLE class="bluetrans" style="width:80%;">
        <TR class="blue2">
            <TD><%= ML.getMLStr("trade_lbl_resource", userId) %></TD>
            <TD align="center"><%= ML.getMLStr("trade_lbl_instorage", userId) %></TD>
            <TD align="center"><%= ML.getMLStr("trade_lbl_tosell", userId) %></TD>
            <TD align="center"><%= ML.getMLStr("trade_lbl_type", userId) %></TD>
            <TD align="center"><%= ML.getMLStr("trade_lbl_sold", userId) %></TD>
        </TR>
    <% for (Ressource r : (ArrayList<Ressource>) Service.ressourceDAO.findAllStoreableRessources()) {
                 TradeOffer to = TradeService2.getOffersByTradePost(tp.getId(), r.getId());
                 if (to != null) {
                     String visibility = "none";
                     if(to.getType().equals(ETradeOfferType.MINIMUM)){
                         visibility = "hidden";
                     }
        %>
        <TR>
            <TD>
                <IMG style="width:18px; height:18px" src="<%= GameConfig.getInstance().picPath() + r.getImageLocation()%>"/></TD>
            <TD align="center">
                <% PlanetRessource pr = Service.planetRessourceDAO.findBy(tp.getPlanetId(), r.getId(), EPlanetRessourceType.INSTORAGE);
                    String qty = "0";
                    if (pr != null) {
                        qty = FormatUtilities.getFormatScaledNumber(pr.getQty(), GameConstants.VALUE_TYPE_NORMAL);
                    }
                %>
                <%= qty%>
            </TD>
            <% if (to.getQuantity() < 0 || to.getType().equals(ETradeOfferType.MINIMUM)) {%>
            <TD align="center"><INPUT style="visibility:<%= visibility %>" id="ress_<%= to.getRessourceId() %>_amount" name="ress_<%= to.getRessourceId() %>_amount" class="dark" value="0" type="INPUT" /></TD>
            <% } else {%>
            <TD align="center"><INPUT style="visibility:<%= visibility %>" id="ress_<%= to.getRessourceId() %>_amount" name="ress_<%= to.getRessourceId() %>_amount"  class="dark" value="<%= to.getQuantity()%>" type="INPUT" /></TD>
            <% }%>
            <TD align="center">
                <SELECT id="ress_<%= to.getRessourceId() %>_type" name="ress_<%= to.getRessourceId() %>_type" class="dark" onChange="hideInput(<%= to.getRessourceId() %>, document.getElementById('ress_<%= to.getRessourceId() %>_type').value);">
                    <% for (ETradeOfferType type : ETradeOfferType.values()) {%>
                    <OPTION value="<%= type.toString() %>" <% if(to.getType().equals(type)){ %>selected <% } %>><%= ML.getMLStr("trade_enum_" + type.toString(),userId) %></OPTION>
                    <% }%>
                </SELECT>
            </TD>
            <TD align="center"><%= FormatUtilities.getFormattedNumber(to.getSold())%></TD>
        </TR>
        <% }%>
        <% }%>
        <TR class="blue2">
            <TD align="center" colspan="5">
                <INPUT class="dark" type="submit" value="<%= ML.getMLStr("trade_but_change", userId) %>" />
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
                                     }
                                     break;
                                 }%>