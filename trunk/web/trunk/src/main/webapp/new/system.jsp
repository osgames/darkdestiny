<%@page import="at.darkdestiny.core.enumeration.EOwner"%>
<%@page import="at.darkdestiny.core.utilities.DiplomacyUtilities"%>
<%@page import="at.darkdestiny.core.utilities.MilitaryUtilities"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.GameConfig" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%
session.setAttribute("actPage", "new/system");
session = request.getSession(true);
int userId = Integer.parseInt((String) session.getAttribute("userId"));
int systemId = -1;

%>
<BR/>
<TABLE WIDTH="80%">
    <TR>
        <TD ALIGN="CENTER" WIDTH="50%"><A href="main.jsp?page=new/system"><%= ML.getMLStr("systemsearch_link_systemview", userId)%></A></TD>
        <TD ALIGN="CENTER" WIDTH="50%"><A href="main.jsp?page=new/system-search"><%= ML.getMLStr("systemsearch_link_search", userId)%></A></TD>
    </TR>
</TABLE>
<BR/>
<%
        // HashMap<Integer, Planet> planets = new HashMap<Integer, Planet>();
        long startTime = java.lang.System.currentTimeMillis();

        int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
        if (request.getParameter("systemId") != null) {
            systemId = Integer.parseInt(request.getParameter("systemId"));
        } else {
            Planet p = SystemService.findPlanetById(planetId);
            systemId = p.getSystemId();
        }

        // DebugBuffer.addLine("PID " + planetId);

        SystemSearchResult ssr = SystemService.findSystemsBySystemId(systemId, systemId, userId);

        // planets = SystemService.findPlanetsBySystemId(systemId);

        // Build info texts
        out.write("<SCRIPT>");
        for (PlanetExt pe : ssr.getSystemList().get(0).getPlanets()) {
            String planetName = "" + ML.getMLStr("system_lbl_planet", userId) + " #" + pe.getBase().getId();
            String infotext = "";

            int userCurrPlanet = 0;
            if (pe != null && pe.getVtEntry() != null) {
                userCurrPlanet = pe.getVtEntry().getUserId();
            }

            PlayerPlanet pp = SystemService.findPlayerPlanetByPlanetId(pe.getBase().getId());
            if (userCurrPlanet == 0) pp = null;
            
            if (pp != null) {
                infotext = ML.getMLStr("system_lbl_planet", userId) + " " + pp.getName();
                infotext += " / ";
                infotext += ML.getMLStr("system_lbl_owner", userId) + " " + SystemService.findUserById(pp.getUserId()).getGameName();
            }else{
                infotext += planetName;
            }
            
            out.write("planet" + pe.getBase().getId() + "text='<U><B>" + infotext + "</B></U>");
            out.write("<BR><BR><B>" + ML.getMLStr("system_pop_coordinates", userId) + ":</B> " + pe.getBase().getSystemId() + ":" + pe.getBase().getId());
            out.write("<BR><B>" + ML.getMLStr("system_pop_type", userId) + ":</B> " + pe.getBase().getLandType());
            out.write("<BR><B>" + ML.getMLStr("system_pop_temperature", userId) + ":</B> " + pe.getBase().getAvgTemp() + " �C");
            out.write("<BR><B>" + ML.getMLStr("system_pop_diameter", userId) + "</B> " + FormatUtilities.getFormattedNumber(pe.getBase().getDiameter()) + " km';");
       }
        out.write("</SCRIPT>");
%>
<TABLE style="border-style:none; margin-left:20px; border-left-color:#000000; border-top-color:#000000; border-bottom-color:#000000; border-right-color:#000000; border-left-width:0px;" frame="lhs" width="300" align="left">
    <TR>
        <TD align="middle" width="100px" class="nb">
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/sun.jpg" />
        </TD>
        <TD align="center" width="150" class="nb">
            <IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("systemsearch_pop_sendfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/sendfleet&systemId=<%= systemId%>'" src="./pic/movefleet.jpg" \>
        </TD>
    </TR>
    <TR><TD colspan="2">&nbsp <BR></TD></TR>
    <%
        boolean found = ssr.getSystemList().get(0).isSystemInViewtable();
        Iterator<PlanetExt> peIterator = ssr.getSystemList().get(0).getPlanetsOrderedByOrbit().iterator();

        PlanetExt actPlanet = null;    
        if(peIterator.hasNext()){
            actPlanet = peIterator.next();
        }
        if (found) {
            //Looping through the Orbit
            for (int i = 0; i <= Planet.MAX_ORBIT_LEVELS; i++) {
                String picURLPlanet = null;
                Planet p = null;
                
                 EOwner owner = EOwner.INHABITATED;
                 if(actPlanet != null){
                     owner = actPlanet.getOwnerAttitude(userId);;
                 }
                 if ((actPlanet == null) || (actPlanet.getBase().getOrbitLevel() > i)) {
                     p = null;
                 } else {
                    p = actPlanet.getBase(); 
                    if (peIterator.hasNext()) {
                        actPlanet = peIterator.next();
                    } else {
                        actPlanet = null;
                    }
                }                
                
                //If no Planet on this orbitlevel
                if (p == null) {
                    picURLPlanet = GameConfig.getInstance().picPath() + "pic/empty.gif";
                } else {
                    picURLPlanet = "GetPlanetPic?mode=2&planetId=" + p.getId();
                }

                int relationType = 0;
                int currUser = userId;
                int userCurrPlanet = 0;
                if (actPlanet != null && actPlanet.getVtEntry() != null) {
                    userCurrPlanet = actPlanet.getVtEntry().getUserId();
                }
                boolean bombardement = false;
                boolean invasion = false;
                boolean scan = false;
                boolean colonize = false;

                // 3 = alliiert
                // 2 = Eigen
                // 1 = Fremd
                // 0 = unbewohnt

                if (userCurrPlanet != 0) {
                    if (userCurrPlanet == currUser) {
                        relationType = 2;
                    } else {
                        if (SystemService.areAllied(currUser, userCurrPlanet)) {
                            relationType = 3;
                        } else {
                            relationType = 1;

                            /**
                             * TODO Updaten
                             *
                             */
                            if(p != null){
                            invasion = at.darkdestiny.core.utilities.MilitaryUtilities.invasionPossible(currUser, p.getId(), null);
                                                       }
                        }
                    }
                }

                if (p != null) {
                        String color = "#FFFFFF";
                        color = DiplomacyUtilities.getColor(owner);
                        if (owner == EOwner.OWN) {
                            out.println("<TR><TD align=\"center\" class=\"nb\"><IMG class=\"pic1\" style=\"color:" + color + "; cursor:pointer;\" alt=\"" + ("" + ML.getMLStr("system_lbl_planet", userId) + "" + p.getId()) + "\" src=\"" + picURLPlanet + "\" onmouseover=\"doTooltip(event,planet" + p.getId() + "text)\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'selectview.jsp?showPlanet=" + p.getId() + "';\" /></TD>");
                        } else {
                            out.println("<TR><TD align=\"center\" class=\"nb\"><IMG class=\"pic1\" style=\"color:" + color + "; cursor:pointer;\" alt=\"" + ("" + ML.getMLStr("system_lbl_planet", userId) + "" + p.getId()) + "\" src=\"" + picURLPlanet + "\" onmouseover=\"doTooltip(event,planet" + p.getId() + "text)\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=new/overview&vPlanet=" + p.getId() + "';\" /></TD>");
                        }
                } else {
                    // out.println("<TR><TD align=\"center\" class=\"nb\"><IMG src=\"" + picURLPlanet + "\"></TD>");
                }

                // Actions Pictures
                // F&uuml;r eigene -> Flotte schicken
                // F&uuml;r unbewohnte -> Flotte schicken, Sonde
                // F&uuml;r feindliche und allierte -> Flotte schicken, Sonde, Spionageinfo
                if (p != null) {
                    if (relationType == 0) {
                        // Empty
                        out.write("<TD align=\"center\" class=\"nb\"><IMG onmouseover=\"doTooltip(event,'"+ML.getMLStr("systemsearch_pop_sendfleet", userId)+"')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=new/sendfleet&planetId=" + p.getId() + "'\" src=\"./pic/movefleet.jpg\" \\>");
                
                    } else if (relationType == 1) {
                        // Enemy
                        // Probe / Spy
                     //   out.write("<TD align=\"center\" class=\"nb\"><IMG onmouseover=\"doTooltip(event,'Flotte senden')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=sendfleet&planetId=" + p.getId() + "'\" src=\"./pic/movefleet.jpg\" \\><IMG onmouseover=\"doTooltip(event,'Sonde senden')\" onmouseout=\"hideTip()\" src=\"./pic/probe.jpg\" onclick=\"window.location.href = 'main.jsp?page=sendprobe&systemId=" + systemId + "&planetId=" + p.getId() + "'\" \\><IMG onmouseover=\"doTooltip(event,'Spion senden')\" onmouseout=\"hideTip()\" src=\"./pic/spy.jpg\" \\>");
                         out.write("<TD align=\"center\" class=\"nb\"><IMG onmouseover=\"doTooltip(event,'" + ML.getMLStr("systemsearch_pop_sendfleet", userId) + "')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=new/sendfleet&planetId=" + p.getId() + "'\" src=\"./pic/movefleet.jpg\" \\>");
                   } else if (relationType == 2) {
                        // Own
                        out.write("<TD align=\"center\" class=\"nb\"><IMG onmouseover=\"doTooltip(event,'" + ML.getMLStr("systemsearch_pop_sendfleet", userId) + "')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=new/sendfleet&planetId=" + p.getId() + "'\" src=\"./pic/movefleet.jpg\" \\></TD>");
                    } else if (relationType == 3) {
                        // Allied
                        // Probe / Spy
                        //out.write("<TD align=\"center\" class=\"nb\"><IMG onmouseover=\"doTooltip(event,'Flotte senden')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=sendfleet&planetId=" + p.getId() + "'\" src=\"./pic/movefleet.jpg\" \\><IMG onmouseover=\"doTooltip(event,'Sonde senden')\" onmouseout=\"hideTip()\" src=\"./pic/probe.jpg\" \\><IMG onmouseover=\"doTooltip(event,'Spion senden')\" onmouseout=\"hideTip()\" src=\"./pic/spy.jpg\" \\>");
                        out.write("<TD align=\"center\" class=\"nb\"><IMG onmouseover=\"doTooltip(event,'" + ML.getMLStr("systemsearch_pop_sendfleet", userId) + "')\" onmouseout=\"hideTip()\" onclick=\"window.location.href = 'main.jsp?page=new/sendfleet&planetId=" + p.getId() + "'\" src=\"./pic/movefleet.jpg\" \\>");
                    }
                } else {
                    out.write("<TR><TD colspan=\"2\">&nbsp<BR><BR></TD></TR>");
                    // out.write("<TD align=\"center\" class=\"nb\">");
                }

                if (scan) {
                    out.write("<IMG onmouseover=\"doTooltip(event,'"+ML.getMLStr("system_pop_scan", userId)+"')\" onmouseout=\"hideTip()\" src=\"./pic/scan.jpg\" \\>");
                }
                if (colonize) {
                    out.write("<IMG onmouseover=\"doTooltip(event,'"+ML.getMLStr("system_pop_colonize", userId)+"')\" onmouseout=\"hideTip()\" src=\"./pic/colonize.jpg\" \\>");
                }
                if (bombardement) {
                    out.write("<IMG onmouseover=\"doTooltip(event,'Bombardieren')\" onmouseout=\"hideTip()\" src=\"./pic/bombard.gif\" \\>");
                }
                if (invasion) {
                    out.write("<IMG onmouseover=\"doTooltip(event,'Invadieren')\" onmouseout=\"hideTip()\" src=\"./pic/invade.png\" onclick=\"window.location.href = 'main.jsp?page=invade&planetId=" + p.getId() + "'\" \\>");
                }

                out.println("</TD>");
                out.println("</TR>");
            }
        } else {%>
        <!-- <TR> <TD colspan=11 align="center" width="100" class="nb"><BR><BR><B><%= ML.getMLStr("system_msg_noinformationaboutsystem", userId)%></B></TD></TR> -->
    <%        }
    %>
</TABLE>
