<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.service.ProfileService"%>
<%@page import="at.darkdestiny.core.service.SystemService"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.statistic.*"%>
<%@page import="at.darkdestiny.core.service.StatisticService"%>
<%@page import="at.darkdestiny.core.service.TitleService"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.core.model.UserData"%>
<%@page import="at.darkdestiny.core.model.Alliance"%>
<%@page import="at.darkdestiny.core.model.Statistic"%>
<%@page import="at.darkdestiny.core.model.PlayerStatistic"%>
<%@page import="at.darkdestiny.core.result.PlayerStatisticResult"%>
<%@page import="org.jfree.chart.*"%>
<%@page import="at.darkdestiny.util.*" %>
<%
            session = request.getSession();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int type = 1;
            if (request.getParameter("type") != null) {
                type = Integer.parseInt((String) request.getParameter("type"));
            }

            Statistik stat = new Statistik();
            int pointsPerMember = 0;
            PlayerStatistic playerStatistic = stat.calcStatistic(userId, Statistik.WRITE_TO_DB_NO);
%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TABLE width="80%">
    <TR align="center">
        <TD width="20%"><U><A href="main.jsp?page=new/statistic&type=1"><%= ML.getMLStr("statistic_link_empires", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=new/statistic&type=2"><%= ML.getMLStr("statistic_link_alliances", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=imperialoverview"><%= ML.getMLStr("statistik_link_internalstatistic", userId)%></A></U></td>
        <TD width="20%"><U><A href="main.jsp?page=variousStatistics"><%= ML.getMLStr("statistic_link_variousstatistics", userId)%></A></U></td>
    </TR>
</TABLE><BR>
<FONT size=-1><%= ML.getMLStr("statistic_msg_statisticinfo", userId)%><BR>
    <TABLE style="font-size: 12px">
        <TR class="blue2">
            <TD colspan="6" align="center">
                <B><%= ML.getMLStr("statistic_lbl_currentpoints", userId) %></B>
            </TD>
        </TR>
        <TR class="blue2">
            <TD>
            </TD>
            <TD width="90" align="center">
                <%= ML.getMLStr("statistic_lbl_ressources", userId)%>
            </TD>
            <TD width="90" align="center">
                <%= ML.getMLStr("statistic_lbl_population", userId)%>
            </TD>
            <TD width="90" align="center">
                <%= ML.getMLStr("statistic_lbl_research", userId)%>
            </TD>
            <TD width="90" align="center">
                <%= ML.getMLStr("statistic_lbl_military", userId)%>
            </TD>
            <TD width="90" align="center" >
                <%= ML.getMLStr("statistic_lbl_sum", userId)%>
            </TD>
        </TR>
        <TR>
            <TD>
                <%= ML.getMLStr("statistic_lbl_points", userId) %>
            </TD>
            <TD align="center">
                <%= FormatUtilities.getFormattedNumber(playerStatistic.getRessourcePoints())%>
            </TD>
            <TD align="center">
                <%= FormatUtilities.getFormattedNumber(playerStatistic.getPopulationPoints())%>
            </TD>
            <TD align="center">
                <%= FormatUtilities.getFormattedNumber(playerStatistic.getResearchPoints())%>
            </TD>
            <TD align="center">
                <%= FormatUtilities.getFormattedNumber(playerStatistic.getMilitaryPoints())%>
            </TD>
            <TD align="center">
                <%= FormatUtilities.getFormattedNumber(playerStatistic.getSum()) %>
            </TD>
        </TR>
    </TABLE>
    <BR></FONT>
    <TABLE width="80%">
        <TR>
            <TD align="right">
                <FONT size="-2" color="#888880"><%= ML.getMLStr("statistic_lbl_inactiveplayer", userId)%></FONT>
            </TD>
        </TR>
        <TR>
            <TD align="right">
                <FONT size="-2" style="text-decoration: underline">(<%= ML.getMLStr("statistic_lbl_player", userId) %>)</FONT><FONT size="-2"> <%= ML.getMLStr("statistic_lbl_playerwithprofile", userId) %></FONT>
            </TD>
        </TR>
        <TR>
            <TD align="right">
                <FONT size="-2" style="color: red;">(<%= ML.getMLStr("statistic_lbl_player", userId) %>)</FONT><FONT size="-2"> <%= ML.getMLStr("statistic_lbl_playerdeletionrequested", userId) %></FONT>
            </TD>
        </TR>
        <TR>
            <TD align="right">
                <FONT size="-2" style="color: red;">(<%= ML.getMLStr("statistic_lbl_player", userId) %>[xx])</FONT><FONT size="-2"> <%= ML.getMLStr("statistic_lbl_playerdeletionrequestedticks", userId) %></FONT>
            </TD>
        </TR>
    </TABLE>
    <% if (type == 1) {
                    int days = -1;
                    if (request.getParameter("days") != null) {
                        days = Integer.parseInt((String) request.getParameter("days"));
                    }
                    boolean collapsed = false;
                    if (days > -1) {
                        collapsed = true;
                    } else {
                        days = 7;
                    }
                    String style = "none";
                    String help = "("+ML.getMLStr("statistik_lbl_expand", userId)+")";
                    String img2 = "pic/icons/menu_ein.png";
                    String img1 = "pic/icons/menu_aus.png";
                    if (collapsed) {
                        style = "";

                        img1 = "pic/icons/menu_ein.png";
                        img2 = "pic/icons/menu_aus.png";
                    }
                    PersonalStatistic ps = new PersonalStatistic(userId, days);
                    String imageMap = "";
                    try{
                    JFreeChart chart = ps.getChart();
                    if (session.getAttribute("chart") != null) {
                        session.removeAttribute("chart");
                    }
                    session.setAttribute("chart", chart);
    // get ImageMap
                    ChartRenderingInfo info = new ChartRenderingInfo();
    // populate the info
                    if (chart != null) {
                        chart.createBufferedImage(640, 400, info);
                        imageMap = ChartUtilities.getImageMap("map", info);
                    }
                    }catch(Exception e){
                            DebugBuffer.writeStackTrace("Error in Statistik jsp", e);
                        }

    %>
    <%= imageMap%>

<CENTER>
    <TABLE class="blue" width="80%">
        <TR>
            <TD>
                <TABLE width="100%">
                    <TR>
            <TD  valign="middle" width="100px" class="blue2">
                <span onclick="toogleVisibility('chartTr');
                    exchangeImgs('img_chart', '<%=GameConfig.getInstance().picPath()%><%= img2%>',
                    '+','<%=GameConfig.getInstance().picPath()%><%= img1%>','-');">
                    <img src="<%=GameConfig.getInstance().picPath()%><%= img1%>" alt="-" border="0" id="img_chart">&nbsp;
                    <FONT id="text" style="font-size: 10; font-weight: bold; color: white;"><%= help %></FONT>
                </span>
            </TD>
            <TD class="blue2" align="center">
                <B><%= ML.getMLStr("statistic_lbl_chart", userId)%></B>
            </TD>
                    </TR>
                </TABLE>
            </TD>

        </TR>
        <TR style="display:<%= style%>" id="chartTr">
            <TD width="100%" colspan="2">
                <TABLE width="100%">
                    <TR>

                        <TD colspan="4" align="center">
                            <IMG src="ChartViewer" usemap="#map" /></TD>
                    </TR>
                    <TR>
                        <TD class="blue3" width="25%" align="center">
                            <A href="main.jsp?page=new/statistic&type=1&days=7"><%= ML.getMLStr("statistic_link_7days", userId) %></A>
                        </TD>
                        <TD class="blue3" width="25%" align="center">
                            <A href="main.jsp?page=new/statistic&type=1&days=31"><%= ML.getMLStr("statistic_link_1month", userId) %></A>
                        </TD>
                        <TD class="blue3" width="25%" align="center">
                            <A href="main.jsp?page=new/statistic&type=1&days=93"><%= ML.getMLStr("statistic_link_3month", userId) %></A>
                        </TD>
                        <TD class="blue3" width="25%" align="center">
                            <A href="main.jsp?page=new/statistic&type=1&days=9999"><%= ML.getMLStr("statistic_link_all", userId) %></A>
                        </TD>
                    </TR>
                </TABLE>
            </TD>
        </TR>

    </TABLE>
</CENTER>
<%



    }%>
<TABLE class="blue" cellpadding=0 cellspacing=0 width="80%">

    <TR align="center">
        <%
                    int rank = 1;

        // Imperiumsansicht
                    if (type == 1) {

        %>
        <!-- Image -->
        <TD class="blue" WIDTH="35">&nbsp;</TD>
        <!-- Direction -->
        <TD class="blue" WIDTH="30">&nbsp;</TD>
        <!-- Rank -->
        <TD class="blue" WIDTH="50"><B><%= ML.getMLStr("statistic_lbl_rank", userId)%></B></TD>
        <!-- Rank -->
        <TD class="blue" WIDTH="50"><B><%= ML.getMLStr("statistic_lbl_galaxy", userId)%></B></TD>
        <!-- Name -->
        <TD class="blue"><B><%= ML.getMLStr("statistic_lbl_name", userId)%> <a href="main.jsp?page=new/tasks&type=2">(- <%= ML.getMLStr("statistic_lbl_title", userId) %>)</a></B></TD>
        <!-- Status -->
        <TD class="blue" width="115"><B><%= ML.getMLStr("statistic_lbl_status", userId)%> </B><A href="http://www.thedarkdestiny.at/wiki/index.php/Trial-Modus" target="new"><IMG border="0" alt="techtree" onmouseover="doTooltip(event,'<%= ML.getMLStr("statistic_pop_status", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.getInstance().picPath()%>pic/infoT.png" /></A></TD>
        <!-- Language -->
        <TD class="blue" width="60"><B><A href="main.jsp?page=new/profile&subpage=2"><%= ML.getMLStr("statistic_lbl_country", userId)%></A></B></TD>
        <!-- Alliance -->
        <TD class="blue" WIDTH="80"><B><%= ML.getMLStr("statistic_lbl_alliance", userId)%></B></TD>
        <!-- Points -->
        <TD class="blue" WIDTH="70"><B><%= ML.getMLStr("statistic_lbl_points", userId)%></B></TD>


        <%
        // Allianz&uuml;bersicht
        } else if (type == 2) {
        %>
        <!-- Image -->
        <TD class="blue"  WIDTH="35">&nbsp;</TD>
        <!-- Rank -->
        <TD class="blue" WIDTH="70"><B><%= ML.getMLStr("statistic_lbl_rank", userId)%></B></TD>
        <!-- Name -->
        <TD class="blue"><B><%= ML.getMLStr("statistic_lbl_name", userId)%></B></TD>
        <!-- Membercount -->
        <TD class="blue" WIDTH="60"><B><%= ML.getMLStr("statistic_lbl_members", userId)%></B></TD>
        <!-- Points -->
        <TD class="blue" WIDTH="85"><B><%= ML.getMLStr("statistic_lbl_points", userId)%></B></TD>
        <!-- Punkte/Spieler-->
        <TD class="blue" WIDTH="85"><B><%= ML.getMLStr("statistic_lbl_pointspermember", userId)%></B></TD>
        <%
        }
        %>
    </TR>

    <%
                int firstthree = 0;
                GameUtilities gu = new GameUtilities();
                int tick = gu.getCurrentTick();
                Statement stmt = DbConnect.createStatement();
                ResultSet rs = null;
                if (type == 1) {
    %>
    <TR>
        <TD>

        </TD>
    </TR>
    <%
        PlayerStatisticResult psr = StatisticService.getPlayerStatisticResult();
        // rs = stmt.executeQuery("SELECT stat.*,u.`lastupdate`, a.tag, a.id FROM statistic stat, user u LEFT OUTER JOIN alliancemembers am ON am.userId=u.id LEFT OUTER JOIN alliance a ON am.allianceId=a.id WHERE (stat.userid<>0 AND stat.userid<5000) AND (stat.userid = u.id) ORDER BY rank ASC");

        for (Map.Entry<Integer, ArrayList<Statistic>> entries : psr.getPlayerStats().entrySet()) {
            for(Statistic s : entries.getValue()){
                if(StatisticService.findUserDataByUserId(s.getUserId()) == null){
                    %>
                    <TR>
                        <TD></TD>
                        <TD></TD>
                        <TD align="center"><%= s.getRank() %></TD>
                        <TD align="center">Gel�scht</TD>
                        <TD></TD>
                        <TD></TD>
                        <TD></TD>
                        <TD></TD>
                    </TR>
                    <%
                    continue;

                }
            boolean actUser = false;
            if (type == 1) {
                actUser = s.getUserId() == userId;
            }

            firstthree++;

    %><TR height="20" align="center"><%

                        String pic = "";

                        switch (firstthree) {
                            case (1):
                                pic = GameConfig.getInstance().picPath() + "pic/first.gif";
                                break;
                            case (2):
                                pic = GameConfig.getInstance().picPath() + "pic/second.gif";
                                break;
                            case (3):
                                pic = GameConfig.getInstance().picPath() + "pic/third.gif";
                                break;
                        }

                        if (!pic.equalsIgnoreCase("")) {
        %><TD><IMG border=0 src="<%= pic%>" /></TD><%
                            } else {
            %><TD>&nbsp;</TD><%                                }

                                pic = "";
                                if (s.getLastRank() == 0) {
                                    pic = GameConfig.getInstance().picPath() + "pic/new.gif";
                                } else if (s.getRank() > s.getLastRank()) {
                                    pic = GameConfig.getInstance().picPath() + "pic/down.gif";
                                } else if (s.getRank() < s.getLastRank()) {
                                    pic = GameConfig.getInstance().picPath() + "pic/up.gif";
                                } else if (s.getRank() == s.getLastRank()) {
                                    pic = GameConfig.getInstance().picPath() + "pic/same.gif";
                                }
                                if (!pic.equalsIgnoreCase("")) {
        %><TD><IMG border=0 src="<%= pic%>" /></TD><%
                                        } else {
            %><TD>&nbsp;</TD><%                                        }


                                        UserData actUserData = StatisticService.findUserDataByUserId(s.getUserId());
                                        String userName = psr.getUserNames().get(s.getUserId());
                                        Alliance alliance = StatisticService.findAllianceByUserId(s.getUserId());


                                        User u = StatisticService.findUserByUserId(s.getUserId());
                                        if(actUserData != null && actUserData.getTitleId() > 0){
                                             userName += " " + TitleService.findTitle(actUserData.getUserId(), userId);
                                        }
                                        if (u != null && tick - u.getLastUpdate() > GameConstants.USER_DEAD_2) {
                                            userName += " *";
                                        }

                                        String galaxy = SystemService.getGalaxy(u.getUserId());


                                        if (actUser) {
        %><!-- Rank -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><%= s.getRank()%></FONT></TD>
        <!-- Galaxy -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><%= galaxy %></FONT></TD>
        <!-- Username -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><%= userName %></FONT></TD>
          <!-- Trial -->
          <% if(u.getTrial()){%><TD><FONT style="font-size:10px;"> (<%= ML.getMLStr("statistic_lbl_trialmode", userId) %>)</FONT></TD><% }else{ %>
        <TD><FONT style="font-size:10px;">Normal</FONT></TD><% } %>
        <!-- Language -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><% if(actUserData != null){ %><IMG alt="language" src="pic/icons/<%= ML.getLanguage(s.getUserId()).getPic() %>"><% } %></FONT></TD>
        <!-- Alliance --><%
                        if (alliance == null) {
        %><TD><FONT style="color:#9ACD32; font-size:14px;">-</FONT></TD><%} else {
        %><TD><FONT style="color:#9ACD32; font-size:14px;"><A HREF="main.jsp?page=showAlliance&allianceid=<%= alliance.getId()%>"><%= alliance.getTag()%></A></FONT></TD><%
                        }
        %><!-- Points -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><%= FormatUtilities.getFormattedNumber(s.getPoints())%></FONT></TD>
        <%
                    } else {
        %><TD><%=s.getRank()%></TD>
        <!-- Galaxy -->
        <TD><FONT style="font-size:14px;"><%= galaxy %></FONT></TD>
        <TD>


            <%
            String styleString = "";
            String color = "#FFFFFF";
            boolean delete = false;
            if(u.getDeleteDate() > 0){
                delete = true;
            }

            UserData ud = Service.userDataDAO.findByUserId(u.getUserId());

            if (tick - u.getLastUpdate() <= GameConstants.USER_DEAD_2) {
                    styleString += "font-size:14px; font-weight:normal;";
                }else{
                    color = "#808080;";

                }
             if(delete && (GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - u.getDeleteDate()) <= GameConstants.DELETION_DELAY / 2)) {
                        color = "#FF0000;";
              }

              if (ud != null && (ud.getPic() != null) || (ud.getDescription() != null)) {
                  styleString += " text-decoration: underline;";
              }
            styleString += " color:" + color + ";";
            if(delete &&  GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - u.getDeleteDate()) <= GameConstants.DELETION_DELAY / 2){
                userName += "(" + ( GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - u.getDeleteDate())) + ")";
            }
            %>
            <% if (ud != null && (ud.getPic() != null) || (ud.getDescription() != null)) { %>
            <A HREF="main.jsp?page=showUser&destUserId=<%= u.getUserId()%>"><FONT style="<%= styleString %>"><%= userName%></FONT></A>
            <% }else{ %>
            <p><font style="<%= styleString %>">
                    <%= userName %>
                    </font>
            </p>

            <% } %>
        </TD>
        <!-- Trial -->
         <% if(u.getTrial()){%><TD><FONT style="font-size:10px;"> (<%= ML.getMLStr("statistic_lbl_trialmode", userId) %>)</FONT></TD><% }else{ %>
        <TD><FONT style="font-size:10px;">Normal</FONT></TD><% } %>
        <!-- Language -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><% if(actUserData != null){ %><IMG alt="language" src="pic/icons/<%= ML.getLanguage(u.getUserId()).getPic() %>"><% } %></FONT></TD>

        <!-- Alliance --><%
                        if (alliance != null) {
        %><TD><FONT style="color:#9ACD32; font-size:14px;"><A HREF="main.jsp?page=showAlliance&allianceid=<%= alliance.getId()%>"><%= alliance.getTag()%></A></FONT></TD><%
                                        } else {
        %><TD><FONT style="color:#FFFFFF; font-size:14px;">-</FONT> </TD><%                                    }
        %><!-- Punkte -->

        <TD><%=FormatUtilities.getFormattedNumber(s.getPoints())%></TD><%
                    }

        %></TR><%
}
            }
} else {
rs = stmt.executeQuery("SELECT a.name, totalPoints.points, a.id, totalPoints.members FROM alliance a, "
        + "(SELECT am.allianceId, SUM(s.points) as points, COUNT(*) as members FROM alliancemembers am, statistic s WHERE s.userId=am.userId GROUP BY am.allianceId) as totalPoints "
        + "WHERE totalPoints.allianceId=a.id ORDER BY totalPoints.points DESC");


while (rs.next()) {
    boolean actUser = false;

    if (type == 1) {
        actUser = rs.getInt(3) == userId;
    }


    firstthree++;

        %><TR height="20" align="center"><%

String pic = "";

switch (firstthree) {
    case (1):
        pic = GameConfig.getInstance().picPath() + "pic/first.gif";
        break;
    case (2):
        pic = GameConfig.getInstance().picPath() + "pic/second.gif";
        break;
    case (3):
        pic = GameConfig.getInstance().picPath() + "pic/third.gif";
        break;
}

if (!pic.equalsIgnoreCase("")) {
        %><TD><IMG border=0 src="<%= pic%>" /></TD><%
        } else {
            %><TD>&nbsp;</TD><%                    }

        if (type == 1) {
            pic = "";
            if (rs.getInt(2) == 0) {
                pic = GameConfig.getInstance().picPath() + "pic/new.gif";
            } else if (rs.getInt(1) > rs.getInt(2)) {
                pic = GameConfig.getInstance().picPath() + "pic/down.gif";
            } else if (rs.getInt(1) < rs.getInt(2)) {
                pic = GameConfig.getInstance().picPath() + "pic/up.gif";
            } else if (rs.getInt(1) == rs.getInt(2)) {
                pic = GameConfig.getInstance().picPath() + "pic/same.gif";
            }
            if (!pic.equalsIgnoreCase("")) {
        %><TD><IMG border=0 src="<%= pic%>" /></TD><%
                    } else {
            %><TD>&nbsp;</TD><%                                }
                }

                if (type == 1) {
                    String userName = rs.getString(4);
                    String alliance = rs.getString(7);
                    UserData actUserData = StatisticService.findUserDataByUserId(rs.getInt(3));

                    User u = StatisticService.findUserByUserId(userId);
                    UserData ud = StatisticService.findUserDataByUserId(userId);

                    if (actUser) {
        %><!-- Rank -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><%= rs.getInt(1)%></FONT></TD>
        <!-- Username -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><%= userName%></FONT></TD>
        <!-- Language -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><IMG alt="language" src="pic/icons/<%= ML.getLanguage(actUserData.getUserId()).getPic() %>"></FONT></TD>
        <!-- Alliance --><%
                    if (alliance == null) {
        %><TD><FONT style="color:#9ACD32; font-size:14px;">-</FONT></TD><%                                        } else {
        %><TD><FONT style="color:#9ACD32; font-size:14px;"><A HREF="main.jsp?page=showAlliance&allianceid=<%= rs.getInt(8)%>"><%= alliance%></A></FONT></TD><%
                    }
        %><!-- Points -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><%= FormatUtilities.getFormattedNumber(rs.getInt(5))%></FONT></TD>
        <%
                } else {
        %><TD><%=rs.getInt(1)%></TD>
        <TD>

            <%if (ud != null && !ud.getPic().equals("") || !ud.getDescription().equals("")) {%>
            <A style="<%
                if (tick - rs.getInt(6) <= GameConstants.USER_DEAD_2) {
               %>font-size:14px; font-weight:normal; <%
                } else if (ud != null && !ud.getPic().equals("") || !ud.getDescription().equals("")) {
               %> font-size:13px; font-weight:normal; color:#808080; <%                                    }%>" HREF="main.jsp?page=showUser&destUserId=<%= rs.getInt(3)%>"><%= userName%></A>
            <%} else {%>
            <p><font style="<%
                if (tick - rs.getInt(6) <= GameConstants.USER_DEAD_2) {
                     %>font-size:14px; font-weight:normal; <%                                                        } else {
                     %> font-size:13px; font-weight:normal; color:#888880; <%                                    }%>"><%= userName%></font></p>

            <%}%>
        </TD>
        <!-- Language -->
        <TD><FONT style="color:#9ACD32; font-size:14px;"><IMG alt="language" src="src="pic/icons/<%= ML.getLanguage(actUserData.getUserId()).getPic() %>"%>"></FONT></TD>

        <!-- Alliance --><%
                    if (alliance != null) {
        %><TD><FONT style="color:#9ACD32; font-size:14px;"><A HREF="main.jsp?page=showAlliance&allianceid=<%= rs.getInt(8)%>"><%= alliance%></A></FONT></TD><%
                                    } else {
        %><TD><FONT style="color:#FFFFFF; font-size:14px;">-</FONT> </TD><%                            }
        %><!-- Punkte -->
        <TD><%=FormatUtilities.getFormattedNumber(rs.getInt(5))%></TD><%
                }
            } else {
                pointsPerMember = Math.round(rs.getInt(2) / rs.getInt(4));
        %><TD><FONT style="font-size:14px;"><%= rank++%></FONT></TD>
        <TD><A HREF="main.jsp?page=showAlliance&allianceid=<%= rs.getInt(3)%>" style="font-size:14px; font-weight:normal;"><%= rs.getString(1)%></A></TD>
        <!-- Members -->
        <TD><%= rs.getInt(4)%></TD>
        <!-- Punkte -->
        <TD><FONT style="font-size:14px;"><%= FormatUtilities.getFormattedNumber(rs.getInt(2))%></FONT></TD>
        <!-- Punkte/Spieler -->
        <TD><FONT style="font-size:14px;"><%= FormatUtilities.getFormattedNumber(pointsPerMember)%></FONT></TD><%
            }
        %></TR><%
                        }
                    }
                    stmt.close();
        %>
</TABLE>
