
<%@page import="at.darkdestiny.core.service.Service" %>
<%@page import="at.darkdestiny.core.service.FilterService" %>
<%@page import="at.darkdestiny.core.model.Filter" %>
<%@page import="at.darkdestiny.core.model.FilterValue" %>
<%@page import="at.darkdestiny.core.model.FilterToValue" %>
<%@page import="at.darkdestiny.core.result.FilterResult" %>
<%@page import="at.darkdestiny.core.utilities.FilterUtilities" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="at.darkdestiny.core.result.BaseResult" %>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="java.util.*" %>
<%

            BaseResult result = null;

            int userId = Integer.parseInt((String) session.getAttribute("userId"));

            final int TYPE_SHOW = 1;
            final int TYPE_DELETE = 2;
            final int TYPE_ADD_FILTER = 3;
            final int TYPE_ADD_VALUE = 4;
            final int PROCESS_ADD_FILTER = 1;
            final int PROCESS_ADD_VALUE = 2;
            final int PROCESS_DELETE_FILTER = 3;
            final int PROCESS_DELETE_VALUE = 4;
            int type = 0;
            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }
            int filterId = 0;
            if (request.getParameter("filterId") != null) {
                filterId = Integer.parseInt(request.getParameter("filterId"));
            }
            int process = 0;
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            int filterValueId = 1;
            if (request.getParameter("filterValueId") != null) {
                filterValueId = Integer.parseInt(request.getParameter("filterValueId"));
            }
            if (process > 0) {
                switch (process) {
                    case (PROCESS_ADD_FILTER): {
                         FilterResult fr = FilterService.addFilter(userId, request.getParameterMap());
                        filterId = fr.getId();
                        result = fr.getResult();
                        if(filterId > 0){
                            type = TYPE_ADD_VALUE;
                        }
                        break;
                    }
                    case (PROCESS_ADD_VALUE): {
                         FilterResult fr = FilterService.addValue(userId, request.getParameterMap());
                        result = fr.getResult();
                        type = TYPE_SHOW;
                        break;
                    }
                    case (PROCESS_DELETE_FILTER): {
                         result = FilterService.deleteFilter(userId, request.getParameterMap());
                        type = 0;
                        break;
                    }
                    case (PROCESS_DELETE_VALUE): {
                         result = FilterService.deleteValue(userId, request.getParameterMap());
                        type = TYPE_SHOW;
                        break;
                    }
                }
            }
            if (result != null) {
                if (result.isError()) {
%>
<CENTER><FONT color="red"><%= result.getMessage()%></FONT></CENTER>
<%
                } else {
%>
<CENTER><FONT color="green"><B><%= result.getMessage()%></B></FONT></CENTER>
<%
                }
            }
            switch (type) {
                case (0): {
%>
<TABLE align="center" style="color: white; font-size: 12px;" width="80%">
    <TR class="blue2">
        <TD colspan="2" align="center">
            Filter
        </TD>
        <TD style="width:20px" align="right">
            <IMG alt="Filter hinzur�gen" src="<%= GameConfig.getInstance().picPath()%>pic/addFilter.png" onmouseover="doTooltip(event,'Filter hinzuf�gen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/filters&type=<%= TYPE_ADD_FILTER%>'"/>
        </TD>
    </TR>
    <TR class="blue2">
        <TD>
            Filter-Name
        </TD>
        <TD>
            Beschreibung
        </TD>
        <TD>
            Aktionen
        </TD>
    </TR>
    <% for (Filter f : (ArrayList<Filter>) Service.filterDAO.findAll()) {
        if(f.getUserId() != userId && f.getUserId() != 0){
            continue;
            }
    %>
    <TR>
        <TD>
            <%= f.getName()%>
        </TD>
        <TD>
            <%= f.getDescription()%>
        </TD>
        <TD>
            <IMG style="cursor:pointer;" alt="Details anzeigen" src="<%= GameConfig.getInstance().picPath()%>pic/view.jpg" onmouseover="doTooltip(event,'Details anzeigen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/filters&filterId=<%= f.getId()%>&type=<%= TYPE_SHOW%>'"/>
            <% if (f.getUserId() == userId) {%>
            <IMG style="cursor:pointer;" alt="Filter l�schen" src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'Filter l�schen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/filters&filterId=<%= f.getId()%>&process=<%= PROCESS_DELETE_FILTER %>'"/>
            <% }%>
        </TD>
    </TR>
    <% }%>
    <TR>
        <TD colspan="3" align="center">
            <A href='main.jsp?page=new/new_administration-body-main'>Zur&uuml;ck</A>
        </TD>
    </TR>
</TABLE>

<% break;
                }

                case (TYPE_SHOW): {
               Filter f = Service.filterDAO.findById(filterId);
%>
<TABLE align="center" style="color: white; font-size: 13px;" width="80%">
    <TR class="blue2">
        <TD colspan="3" align="center">
            Wert
        </TD>
        <TD style="width:17px" align="right">
            <% if(f.getUserId() == userId) {%>
             <IMG style="cursor:pointer;" alt="Filterwert hinzuf�gen" src="<%= GameConfig.getInstance().picPath()%>pic/addValue.jpg" onmouseover="doTooltip(event,'Filterwert hinzuf�gen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/filters&filterId=<%= filterId %>&type=<%= TYPE_ADD_VALUE %>'"/>
             <% } %>
        </TD>
    </TR>
    <TR class="blue2">
        <TD>
           Name
        </TD>
        <TD>
            Vergleichsoperator
        </TD>
        <TD>
            Wert
        </TD>
        <TD>
            Aktionen
        </TD>
    </TR>
    <% for (FilterToValue ftv : Service.filterToValueDAO.findByFilterId(filterId)) {
                            FilterValue fv = Service.filterValueDAO.findById(ftv.getValueId());

    %>
    <TR>
        <TD>
            <%= fv.getName()%>
        </TD>
        <TD>
            <B> <%= ftv.getComparatorString()%></B>
        </TD>
        <TD>
            <%= ftv.getValue()%>
        </TD>

        <TD>
            <% if(f.getUserId() == userId){ %>
               <IMG style="cursor:pointer;" alt="Wert l�schen" src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg" onmouseover="doTooltip(event,'Wert l�schen')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/filters&filterId=<%= f.getId()%>&valueId=<%= ftv.getValueId() %>&process=<%= PROCESS_DELETE_VALUE %>'"/>
        <% } %>
        </TD>
    </TR>
    <% }%>
    <TR>
        <TD colspan="4" align="center">
            <A href='main.jsp?page=new/filters'>Zur&uuml;ck</A>
        </TD>
    </TR>
</TABLE>
<% break;
                }
                case (TYPE_ADD_FILTER): {
%>
<FORM action="main.jsp?page=new/filters&process=<%= PROCESS_ADD_FILTER%>" method="post">
    <TABLE align="center" style="color: white; font-size: 12px;" width="80%">
        <TR class="blue2">
            <TD align="center" colspan="2">
                <B>Neuer Filter</B>
            </TD>
        </TR>
        <TR class="blue2">
            <TD>
                Name
            </TD>
            <TD>
                Beschreibung
            </TD>
        </TR>
        <TR>
            <TD valign="top">
                <INPUT name="name" style="width:100%;" type="text"/>
            </TD>
            <TD>
                <TEXTAREA name="description" style="width:100%;" rows="10"></TEXTAREA>
            </TD>
        </TR>
        <TR>
            <TD colspan="2" align="center">
                <INPUT type="submit" value="Weiter" />
            </TD>
        </TR>
    <TR>
        <TD colspan="2" align="center">
            <A href='main.jsp?page=new/filters'>Zur&uuml;ck</A>
        </TD>
    </TR>
    </TABLE>
</FORM>
<%
                    break;
                }
                case (TYPE_ADD_VALUE): {
                    Filter f = Service.filterDAO.findById(filterId);
%>
<FORM name="valueIdForm" action="main.jsp?page=new/filters&filterId=<%= filterId %>&process=<%= PROCESS_ADD_VALUE %>" method="post">
<TABLE align="center" style="color: white; font-size: 12px;" width="80%">
    <TR class="blue2">
        <TD align="center" colspan="3">
            <B>Filter : <%= f.getName()%></B>
        </TD>
    </TR>
    <TR class="blue2">
        <TD align="center">
            <B>Name</B>
        </TD>
        <TD align="center">
            <B>Vergleichsoperator</B>
        </TD>
        <TD align="center">
            <B>Wert</B>
        </TD>
    </TR>
    <TR>
        <TD>
            <SELECT name="valueIdSelect" style="width:100%;" onChange="window.location=document.valueIdForm.valueIdSelect.options[document.valueIdForm.valueIdSelect.selectedIndex].value">
        <%
           for (FilterValue fv : (ArrayList<FilterValue>)Service.filterValueDAO.findAll()) {
        %>
        <OPTION <% if(filterValueId == fv.getId()){ %>selected <% } %>value="main.jsp?page=new/filters&filterId=<%= filterId %>&type=<%= TYPE_ADD_VALUE %>&filterValueId=<%= fv.getId() %>"><%= fv.getName() %> (<%= fv.getType() %>)</OPTION>
        <% }%>
            </SELECT>
        </TD>
    <INPUT type="hidden" name="filterValueId" value="<%= filterValueId %>"/>
        <TD align="center">
            <SELECT name="comparator" style="width:100%; text-align: center;">
                <%
                FilterValue fv = Service.filterValueDAO.findById(filterValueId);
                for(EFilterComparator efc : EFilterComparator.values()){
                    if(fv.getType() == EFilterValueType.BOOLEAN){
                            if(efc == EFilterComparator.BIGGER  || efc == EFilterComparator.BIGGEREQUALS
                                    || efc == EFilterComparator.SMALLER || efc == EFilterComparator.SMALLEREQUALS){
                                    continue;
                                }
                     }
                %>
                <OPTION><%= FilterUtilities.getComparatorString(efc) %> </OPTION>
                <% } %>
            </SELECT>
        </TD>
        <TD>
            <% if(fv.getType() == EFilterValueType.BOOLEAN){ %>
            <SELECT style="width:100%" name="value">
                <OPTION value="1">Wahr</OPTION>
                <OPTION value="0">Falsch</OPTION>
            </SELECT>
            <% }else{ %>
            <INPUT style="width:100%" name="value">
            <% } %>
        </TD>
    </TR>
    <TR>
        <TD colspan="3" align="center">
            <INPUT type="submit" value="Hinzuf�gen">
        </TD>
    </TR>
    <TR>
        <TD colspan="3" align="center">
            <A href='main.jsp?page=new/filters'>Zur&uuml;ck</A>
        </TD>
    </TR>
</TABLE>
</FORM>
<%


                    break;
                }
            }%>
