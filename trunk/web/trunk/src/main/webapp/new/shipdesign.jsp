<%@page import="at.darkdestiny.core.GameConfig"%>
<%@ page import="at.darkdestiny.core.service.*" %>
<%@ page import="at.darkdestiny.core.model.*" %>
<%@ page import="at.darkdestiny.core.result.*" %>
<%@ page import="at.darkdestiny.core.ML" %>
<%@ page import="at.darkdestiny.core.enumeration.EShipType" %>
<%@page import="at.darkdestiny.util.*" %>

<%
    int designId = Integer.parseInt(request.getParameter("designId"));
    int userId = Integer.parseInt((String) session.getAttribute("userId"));

    ShipDesignResult sdr = null;
    if (request.getParameter("submit") != null) {
        sdr = ShipDesignService.storeDesign(userId, designId, request.getParameterMap());
    }

    ShipDesign sd = ShipDesignService.getShipDesign(designId);

    String basePath = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);

    int shiptype = -1;
    if (sd.getType() == EShipType.BATTLE) {
        shiptype = EShipType.BATTLE.ordinal();
    } else if (sd.getType() == EShipType.TRANSPORT) {
        shiptype = EShipType.TRANSPORT.ordinal();
    } else if (sd.getType() == EShipType.CARRIER) {
        shiptype = EShipType.CARRIER.ordinal();
    } else if (sd.getType() == EShipType.COLONYSHIP) {
        shiptype = EShipType.COLONYSHIP.ordinal();
    } else if (sd.getType() == EShipType.SPACEBASE) {
        shiptype = EShipType.SPACEBASE.ordinal();
    }

    boolean shipExists = ShipDesignService.isBuilt(userId, designId);
    boolean inProduction = ShipDesignService.isInProduction(userId, designId);

%>
<script language="javascript" type="text/javascript">
    var language = '<%= ML.getLocale(userId)%>';
    var actChassis = <%= sd.getChassis()%>;
    var orgChassis = <%= sd.getChassis()%>;
    var shipExists = <%= shipExists%>;
    var inProduction = <%= inProduction%>;
    var existsErrorMessage = 'Das Schiff wurde bereits gebaut oder wird gebaut und kann nicht mehr ver�ndert werden!';

    var shiptype = <%= shiptype%>;

    var ressImgs = new Array();
    <% for (Ressource r : Service.ressourceDAO.findAll()) {%>
    ressImgs[<%= r.getId()%>] = '<%= GameConfig.getInstance().picPath() + r.getImageLocation()%>';
    <% }%>
    ressImgs[999] = '<%= GameConfig.getInstance().picPath()%>pic/icons/empty.png';
    ressImgs[1001] = '<%= GameConfig.getInstance().picPath()%>pic/military.jpg';
    ressImgs[1002] = '<%= GameConfig.getInstance().picPath()%>pic/ress.png';
    ressImgs[1003] = '<%= GameConfig.getInstance().picPath()%>pic/pop.png';
    function initSD() {
        if (shipExists || inProduction) {
            var hidefield = false;

            var appName = navigator.appName;
            var version = navigator.appVersion;

            if ((appName == 'Netscape') &&
                    (version.substring(0, 1) == '5')) {
                hidefield = true;
            }

            if (hidefield) {
                document.getElementById("Save").type = 'HIDDEN';
            } else {
                document.getElementById("Save").disabled = true;
            }
        }

        ajaxCall('<%= basePath%>GetJSONDesignData?userId=<%= userId%>&designId=<%= sd.getId()%>', processAjaxData);

        var shipdetails = document.getElementById('shipdetails');

        while (shipdetails.hasChildNodes()) {
            shipdetails.removeChild(shipdetails.lastChild);
        }

        var loadingTD = document.createElement("TD");
        var loadingImage = document.createElement("IMG");

        loadingImage.border = 0;
        loadingImage.src = './pic/loading.gif';

        loadingTD.appendChild(loadingImage);
        shipdetails.appendChild(loadingTD);
    }

    function callingMain() {
        var leftTD = document.createElement("td");
        var strucSPAN = document.createElement("span");
        var accelSPAN = document.createElement("span");
        var sUsedSPAN = document.createElement("span");
        var sTotalSPAN = document.createElement("span");
        var eUsedSPAN = document.createElement("span");
        var eTotalSPAN = document.createElement("span");
        var attSPAN = document.createElement("span");
        var defSPAN = document.createElement("span");
        var designRangeSPAN = document.createElement("span");

        var middleTD = document.createElement("td");

        var rightTD = document.createElement("td");
        var specSPAN = document.createElement("span");
        var resSPAN = document.createElement("span");


        leftTD.vAlign = 'top';
        leftTD.appendChild(document.createTextNode("Struktur: "));
        strucSPAN.id = 'structure';
        leftTD.appendChild(strucSPAN);
        leftTD.appendChild(document.createElement("br"));
        leftTD.appendChild(document.createTextNode("Beschl.: "));
        accelSPAN.id = 'acceleration';
        accelSPAN.innerHTML = "-";
        leftTD.appendChild(accelSPAN);
        leftTD.appendChild(document.createTextNode(" km/sec�"));
        leftTD.appendChild(document.createElement("br"));
        leftTD.appendChild(document.createTextNode("Reichweite: "));
        designRangeSPAN.id = 'designRange';
        leftTD.appendChild(designRangeSPAN);
        leftTD.appendChild(document.createElement("br"));
        leftTD.appendChild(document.createTextNode("Platz: "));
        sUsedSPAN.id = 'spaceUsed';
        leftTD.appendChild(sUsedSPAN);
        leftTD.appendChild(document.createTextNode(" / "));
        sTotalSPAN.id = 'spaceTotal';
        leftTD.appendChild(sTotalSPAN);
        leftTD.appendChild(document.createElement("br"));
        leftTD.appendChild(document.createTextNode("Energie: "));
        eUsedSPAN.id = 'energyUsed';
        leftTD.appendChild(eUsedSPAN);
        leftTD.appendChild(document.createTextNode(" / "));
        eTotalSPAN.id = 'energyTotal';
        leftTD.appendChild(eTotalSPAN);
        leftTD.appendChild(document.createElement("br"));
        leftTD.appendChild(document.createTextNode("Angriff: "));
        attSPAN.id = 'attack';
        leftTD.appendChild(attSPAN);
        leftTD.appendChild(document.createElement("br"));
        leftTD.appendChild(document.createTextNode("Schild: "));
        defSPAN.id = 'defense';
        leftTD.appendChild(defSPAN);
        leftTD.appendChild(document.createElement("br"));

        middleTD.vAlign = 'top';
        specSPAN.id = "special";
        middleTD.appendChild(specSPAN);

        rightTD.vAlign = 'top';
        resSPAN.id = "resources";
        rightTD.appendChild(resSPAN);

        shipdetails.appendChild(leftTD);
        shipdetails.appendChild(middleTD);
        shipdetails.appendChild(rightTD);
        /*
         shipdetails.innerHTML = '<TD valign=\"top\">Struktur: <SPAN ID="structure">-</SPAN><BR>' +
         'Beschleunigung: <SPAN ID="acceleration">-</SPAN> km/sec�<BR>' +
         'Platz: <SPAN ID="spaceUsed">-</SPAN> / <SPAN ID="spaceTotal">-</SPAN><BR>' +
         'Energie: <SPAN ID="energyUsed">-</SPAN> / <SPAN ID="energyTotal">-</SPAN><BR>' +
         'Angriff: <SPAN ID="attack">-</SPAN><BR>' +
         '<TD valign="top">-</TD>' +
         '<TD valign="top"><SPAN ID="resources"></SPAN></TD>';
         */
    }

    if (window.addEventListener) {
        // See if the browser has the function addEventListener
        window.addEventListener('load', initSD, false);
    } else if (window.attachEvent) {
        // See if the browser has the function attachEvent
        window.attachEvent('onload', initSD);
    } else {
        window.alert('Sorry, but your browser isn\'t supported');
    }
</script>
<TABLE>
    <!-- Header Table -->
    <TR>
        <TD>
            <TABLE>
                <TR>
                    <!-- Chassis Selector -->
                    <TD>
                        <TABLE>
                            <!-- Image Line -->
                            <TR>
                                <TD colspan=2>
                                    <IMG ID="shippic" alt="Chassis" src="./pic/<%= ShipDesignService.getChassis(sd.getChassis()).getImg_design() %>" border=0 />
                                </TD>
                            </TR>
                            <TR>
                                <TD align="left"><IMG alt="left" src="./pic/buttonLeft.JPG" onClick="switchChassisPrevious();" border=0 /></TD>
                                <TD align="right"><IMG alt="right" src="./pic/buttonRight.JPG" onClick="switchChassisNext();" border=0 /></TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <!-- Design Base Information -->
                    <TD valign="top">
                        <TABLE>
                            <TR>
                                <TD valign="top"><FONT size="+2"><%= sd.getName()%> [<SPAN ID="shipname"><%= ML.getMLStr(ShipDesignService.getChassis(sd.getChassis()).getName(), userId)%></SPAN>]</FONT><BR>
                                    <TABLE width="180px" border="0">
                                        <TR valign="middle">
                                            <TD style="border-style: solid; border-width: 1px" valign="middle" width="15px"><IMG src="./pic/icons/left.PNG" border=0 onClick="switchTypePrevious();" /></TD>
                                            <TD style="border-style: solid; border-width: 1px" align="center"><SPAN id="shipTypeName"><%= ShipDesignService.typeIdToNameML(sd.getType(), userId)%></SPAN></TD>
                                            <TD style="border-style: solid; border-width: 1px" valign="middle" width="15px"><IMG src="./pic/icons/right.PNG" border=0 onClick="switchTypeNext();" /></TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD>&nbsp;</TD>
                            </TR>
                            <TR>
                                <TD>
                                    <TABLE style="font-size: 14px; font-weight: bolder;">
                                        <TR align="left" class="blue2">
                                            <TH width="200"><B><%= ML.getMLStr("shipdesign_lbl_basevalues", userId) %></B></TH>
                                            <TH width="200"><B><%= ML.getMLStr("shipdesign_lbl_shiptypebonus", userId) %></B></TH>
                                            <TH width="200"><B><%= ML.getMLStr("shipdesgin_lbl_constructioncost", userId) %></B></TH>
                                        </TR>
                                        <TR style="font-size: 11px;" ID="shipdetails" align="left">
                                            <TD colspan="3" valign="top"></TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
    <%
        if (sdr != null) {
            if (sdr.isSuccess()) { %>
    <TR><TD align="center">
            <%
                        if (sdr.getErrors().size() > 0) {
            %>
            <FONT color="#FFFF00">
            <%
                            for (String error : sdr.getErrors()) {
            %>
            <B><%= error%></B><BR>
                <%
                                }
                %>
            </FONT>
            <%
                        }
            %>
            <FONT color="#7FFF00"><B><%= ML.getMLStr("shipdesign_msg_designverified", userId) %></B></FONT>
        </TD></TR>
        <%      } else { %>
    <TR><TD align="center"><FONT color="#CD5C5C"><B<%= ML.getMLStr("shipdesign_err_missincomponents", userId) %></B></FONT><BR>
                <%
            for (String error : sdr.getErrors()) { %>
            <FONT color="#FFD700"><B><%= error %></B><BR></FONT>
                <%          }
                %>
        </TD></TR>
        <%      }
            } else {
        %>
    <TR>
        <TD>&nbsp;</TD>
    </TR>
    <%
        }
    %>
    <TR>
        <TD>
            <TABLE>
                <TR>
                    <!-- Module Information -->
                    <TD valign="top">
                        <TABLE width="230">
                            <TR><TD width="230">
                                    <TABLE width="100%">
                                        <TR>
                                            <TD><SPAN ID="modulename"></SPAN></TD>
                                            <TD align="left"><IMG style="visibility:hidden;" ID="infopic" src="./pic/info.jpg" border=0 /></TD>
                                        </TR>
                                    </TABLE>
                                </TD></TR>
                            <TR><TD>&nbsp;</TD></TR>
                            <TR><TD><FONT size="-1"><SPAN ID="moduleinfo"></SPAN></FONT></TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <!-- Ship Modules -->
                    <TD valign="top" align="center">
                        <FORM method="post" action="" Name="SaveDesign">
                            <TABLE ID="slotTable" style="font-size: 12px; font-weight: bolder;">
                                <TR>
                                    <TD>
                                        <%= ML.getMLStr("shipdesign_lbl_propulsion", userId) %>:
                                    </TD>
                                    <TD>
                                        <SELECT name="engines" ID="engines">
                                            <OPTION value="-1">--------------- <%= ML.getMLStr("shipdesign_lbl_pleaseselect", userId) %> ---------------</OPTION>
                                        </SELECT>
                                        <INPUT name="engines_count" ID="engines_count" style="visibility:hidden" SIZE="3" VALUE=0 />
                                    </TD>
                                </TR>
                                <TR>
                                    <TD>
                                        <%= ML.getMLStr("shipdesign_lbl_hyperspaceengine", userId) %>:
                                    </TD>
                                    <TD>
                                        <SELECT name="hyperspaceEngines" ID="hyperspaceEngines">
                                            <OPTION value="-1">--------------- <%= ML.getMLStr("shipdesign_lbl_pleaseselect", userId) %> ---------------</OPTION>
                                        </SELECT>
                                        <INPUT name="hyperspaceEngines_count" ID="hyperspaceEngines_count" style="visibility:hidden" SIZE="3" VALUE=0 />
                                    </TD>
                                </TR>
                                <TR>
                                    <TD>
                                        <%= ML.getMLStr("shipdesign_lbl_armor", userId) %>:
                                    </TD>
                                    <TD>
                                        <SELECT name="armor" ID="armor">
                                            <OPTION value="-1">--------------- <%= ML.getMLStr("shipdesign_lbl_pleaseselect", userId) %> ---------------</OPTION>
                                        </SELECT>
                                        <INPUT name="armor_count" ID="armor_count" style="visibility:hidden" SIZE="3" VALUE=0 />
                                    </TD>
                                </TR>
                                <TR>
                                    <TD>
                                        <%= ML.getMLStr("shipdesign_lbl_computersystem", userId) %>:
                                    </TD>
                                    <TD>
                                        <SELECT name="computer" ID="computer">
                                            <OPTION value="-1">--------------- <%= ML.getMLStr("shipdesign_lbl_pleaseselect", userId) %> ---------------</OPTION>
                                        </SELECT>
                                        <INPUT name="computer_count" ID="computer_count" style="visibility:hidden" SIZE="3" VALUE=0 />
                                    </TD>
                                </TR>
                                <TR><TD>&nbsp</TD></TR>
                                <TR ID="en1">
                                    <TD>
                                        <%= ML.getMLStr("shipdesign_lbl_energy", userId) %>:
                                    </TD>
                                    <TD>
                                        <SELECT name="reactors_1" ID="reactors_1">
                                            <OPTION value="-1">--------------- <%= ML.getMLStr("shipdesign_lbl_pleaseselect", userId) %> ---------------</OPTION>
                                        </SELECT>
                                        &nbsp;
                                        <INPUT name="reactors_1_count" ID="reactors_1_count" style="visibility:hidden" SIZE="3" VALUE=0 />
                                    </TD>
                                </TR>
                                <TR ID="enAdd">
                                    <TD>&nbsp;</TD>
                                    <TD ONCLICK="addSlot(1);"><FONT size=-2><U><%= ML.getMLStr("shipdesign_lbl_addslot", userId) %></U></FONT></TD>
                                </TR>
                                <TR><TD>&nbsp</TD></TR>
                                <TR ID="of1">
                                    <TD>
                                        <%= ML.getMLStr("shipdesign_lbl_offensive", userId) %>:
                                    </TD>
                                    <TD>
                                        <SELECT name="weapons_1" ID="weapons_1">
                                            <OPTION value="-1">--------------- <%= ML.getMLStr("shipdesign_lbl_pleaseselect", userId) %> ---------------</OPTION>
                                        </SELECT>
                                        &nbsp;
                                        <INPUT name="weapons_1_count" ID="weapons_1_count" style="visibility:hidden" SIZE="3" VALUE=0 />
                                    </TD>
                                </TR>
                                <TR ID="ofAdd">
                                    <TD>&nbsp;</TD>
                                    <TD ONCLICK="addSlot(2);"><FONT size=-2><U><%= ML.getMLStr("shipdesign_lbl_addslot", userId) %></U></FONT></TD>
                                </TR>
                                <TR><TD>&nbsp</TD></TR>
                                <TR ID="de1">
                                    <TD>
                                        <%= ML.getMLStr("shipdesign_lbl_defensive", userId) %>:
                                    </TD>
                                    <TD>
                                        <SELECT name="shields_1" ID="shields_1">
                                            <OPTION value="-1">--------------- <%= ML.getMLStr("shipdesign_lbl_pleaseselect", userId) %> ---------------</OPTION>
                                        </SELECT>
                                        &nbsp;
                                        <INPUT name="shields_1_count" ID="shields_1_count" style="visibility:hidden" SIZE="3" VALUE=0 />
                                    </TD>
                                </TR>
                                <TR ID="deAdd">
                                    <TD>&nbsp;</TD>
                                    <TD ONCLICK="addSlot(3);"><FONT size=-2><U><%= ML.getMLStr("shipdesign_lbl_addslot", userId) %></U></FONT></TD>
                                </TR>
                                <TR><TD>&nbsp</TD></TR>
                                <TR ID="ot1">
                                    <TD>
                                        <%= ML.getMLStr("shipdesign_lbl_specialmodules", userId) %>:
                                    </TD>
                                    <TD>
                                        <SELECT name="other_1" ID="other_1">
                                            <OPTION value="-1">--------------- <%= ML.getMLStr("shipdesign_lbl_pleaseselect", userId) %> ---------------</OPTION>
                                        </SELECT>
                                        &nbsp;
                                        <INPUT name="other_1_count" ID="other_1_count" style="visibility:hidden" SIZE="3" VALUE=0 />
                                    </TD>
                                </TR>
                                <TR ID="otAdd">
                                    <TD>&nbsp;</TD>
                                    <TD ONCLICK="addSlot(4);"><FONT size=-2><U><%= ML.getMLStr("shipdesign_lbl_addslot", userId) %></U></FONT></TD>
                                </TR>
                            </TABLE>
                            <BR>
                            <INPUT type="HIDDEN" name="cId" ID="cId" VALUE="<%= sd.getChassis() %>" />
                            <INPUT type="HIDDEN" name="shiptype" ID="shiptype" VALUE="<%= shiptype %>" />
                            <INPUT class="dark" ID="Save" NAME="submit" TYPE="SUBMIT" VALUE="Save">
                        </FORM>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
</TABLE>