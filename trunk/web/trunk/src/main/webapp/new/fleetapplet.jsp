<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.core.GameConstants"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            FleetParameterBuffer fpb = new FleetParameterBuffer(userId);

            if (request.getParameter("toSystem") != null) {
                fpb.setParameter("toSystem", request.getParameter("toSystem"));
                fpb.setParameter("targetId", request.getParameter("targetId"));
                fpb.setParameter(FleetParameterBuffer.LOCATION_ID, request.getParameter("targetId"));
            }
            
            if (request.getParameter("targetType") != null) {
                fpb.setParameter(FleetParameterBuffer.LOCATION_TYPE, request.getParameter("targetType"));                
            }
            
            if (request.getParameter("fleetId") != null) {

                fpb.setParameter("fleetId", request.getParameter("fleetId"));
            }

            synchronized (GameConstants.appletSync) {
                BaseResult br = FleetService.checkFlightRequest(userId, fpb);

                if (!br.isError()) {
                    br = FleetService.processFlightRequest(userId, fpb);
                }

                if (!br.isError()) { %>
                    OKAY
<%              } else { %>
                    <%= br.getMessage() %>
<%              }
            }
%>

