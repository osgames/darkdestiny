<%@page import="at.darkdestiny.core.update.ProdOrderBuffer"%>
<%@page import="at.darkdestiny.core.view.ProductionPointsView"%>
<%@page import="at.darkdestiny.core.view.IndustryPointsView"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.core.databuffer.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="at.darkdestiny.core.ships.*"%>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel"%>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.result.*"%>
<%@page import="at.darkdestiny.core.enumeration.*"%>
<%@page import="at.darkdestiny.core.planetcalc.*"%>
<%@page import="at.darkdestiny.core.buildable.*"%>
<%@page import="at.darkdestiny.core.utilities.BonusUtilities"%>
<%@page import="at.darkdestiny.core.enumeration.*"%>
<%@page import="at.darkdestiny.util.*" %>
<%
    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
    int type = 1;
    int detail = 0;

    session.setAttribute("actPage", "new/production");

    EShipType shipType = null;
    int chassisId = 0;

    if (request.getParameter("compress") != null) {
        ProductionService.compressOrders(userId, planetId);
    }

    if (request.getParameter("type") != null) {
        type = Integer.parseInt((String) request.getParameter("type"));
    }
    if (request.getParameter("shipType") != null) {
        if (!((String) request.getParameter("shipType")).equals("0") && !((String) request.getParameter("shipType")).equals("null")) {
            shipType = EShipType.valueOf((String) request.getParameter("shipType"));
        }
    }

    if (request.getParameter("chassisId") != null) {
        chassisId = Integer.parseInt((String) request.getParameter("chassisId"));
    }

    Map<String, String[]> allPars = request.getParameterMap();
    ArrayList<String> errors = new ArrayList<String>();

    if (request.getParameter("submit") != null) {
        if (type == 1) {
            errors = ProductionService.buildShip(userId, planetId, allPars);
        } else if (type == 2) {
            errors = ProductionService.buildGroundTroop(userId, planetId, allPars);
        }
    }

// Change Priority
    if ((request.getParameter("oNo") != null) && (request.getParameter("priority") != null)) {
        int orderNo = Integer.parseInt((String) request.getParameter("oNo"));
        int priority = Integer.parseInt((String) request.getParameter("priority"));
        ProductionService.setNewPriority(orderNo, priority);
    }

    if (request.getParameter("detail") != null) {
        detail = Integer.parseInt((String) request.getParameter("detail"));
    } else {
        detail = 1;
    }

    ProductionBuildResult pbr = null;
    if (type == 1) {
        pbr = ProductionService.getProductionListShips(userId, planetId);
    } else if (type == 2) {
        pbr = ProductionService.getProductionListGroundTroops(userId, planetId);
    }
%>
<TABLE width="90%">
    <TR align="center">
        <TD width="50%"><A href="main.jsp?page=new/production&type=1"><%= ML.getMLStr("production_lbl_ships", userId)%></A></TD>
        <TD width="50%"><A href="main.jsp?page=new/production&type=2"><%= ML.getMLStr("production_lbl_groundtroops", userId)%></A></TD>
    </TR>
    <TR align="center">
        <TD width="50%"><U><A href="main.jsp?page=new/production&type=4"><%= ML.getMLStr("production_lbl_shipdesign", userId)%></A></U></TD>
<TD></TD>
</TR>
</TABLE><BR>
<%
    ProductionPointsView ppv = ProductionService.calcBonus(planetId);
    long modulePoints = ppv.getModulePoints();
    long dockPoints = ppv.getDockPoints();
    long orbDockPoints = ppv.getOrbDockPoints();
    long kasPoints = ppv.getKasPoints();


    if (type == 4) {
%>
<jsp:include page="shipdesignoverview.jsp" />
<%} else {
    if ((type == 1) || (type == 2)) {


        if ((modulePoints + dockPoints + orbDockPoints + kasPoints) > 0) {
%>
<TABLE style="font-size:13px" width="80%">
    <TR class="blue2" align="middle">
        <TD colspan=5><B><%= ML.getMLStr("production_lbl_productionpertick", userId)%></B></TD>
    </TR>
    <TR align="middle">
        <TD><B><%= ML.getMLStr("production_lbl_modulefactory", userId)%></B></TD>
        <TD><B><%= ML.getMLStr("production_lbl_planetaryshipyards", userId)%></B></TD>
        <TD><B><%= ML.getMLStr("production_lbl_orbitalshipyards", userId)%></B></TD>
        <TD><B><%= ML.getMLStr("production_lbl_barracks", userId)%></B></TD>
    </TR>
    <TR align="middle">
        <TD><%= modulePoints%>
            <% if (ppv.getModBonus() > 0) {%><BR><FONT style="color:#FF00FF; font-size: 10px; font-weight: bold;">
            (+ <%= ppv.getModBonus()%>)
            </FONT>
            <% }%></TD>
        <TD><%= dockPoints%>
            <% if (ppv.getDockBonus() > 0) {%><BR><FONT style="color:#FF00FF; font-size: 10px; font-weight: bold;">
            (+ <%= ppv.getDockBonus()%>)
            </FONT>
            <% }%></TD>
        <TD><%= orbDockPoints%>
            <% if (ppv.getOrbDockBonus() > 0) {%><BR><FONT style="color:#FF00FF; font-size: 10px; font-weight: bold;">
            (+ <%= ppv.getOrbDockBonus()%>)
            </FONT>
            <% }%></TD>
        <TD><%= kasPoints%>
            <% if (ppv.getKasBonus() > 0) {%><BR><FONT style="color:#FF00FF; font-size: 10px; font-weight: bold;">
            (+ <%= ppv.getKasBonus()%>)
            </FONT>
            <% }%></TD>
    </TR>
    <TR><TD>&nbsp;</TD></TR>
</TABLE>
<%
        }
    }
    /*
     Statement stmt2 = conn.createStatement();
     ResultSet rs = stmt.executeQuery("select buildingId, timeFinished, number, actionType from actions where (actionType=3 OR actionType=4 or actionType=5) and planetId="+planetId+" order by timeFinished asc");
     */

    InConstructionResult inConsLocal = ProductionService.getInConstructionData(planetId);
    InConstructionResult inConsRemote = ProductionService.getInConstructionDataOtherPlanets(userId, planetId);

    if (!inConsLocal.getEntries().isEmpty()) {
%>
<table cellpadding="0" cellspacing="0" width="90%"><tr><td>
            <TABLE align="center" class="blue" border="0" cellpadding="0" cellspacing="0" width="90%">
                <TR bgcolor="#808080" >
                    <td class="blue" width="*"><B><%= ML.getMLStr("production_lbl_ongoingproduction", userId)%></B></td>
                    <td class="blue" width="50" align="center"><B><%= ML.getMLStr("production_lbl_quantity", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("construction_pop_module", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_module", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("construction_pop_shipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_shipyard", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("construction_pop_orbitalshipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_orbitalshipyard", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("construction_pop_barracks", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_barracks", userId)%></B></td>
                    <td class="blue" width="73">
                        <TABLE cellpadding="0" cellspacing="0" width="100%">
                            <TR valign="center">
                                <TD align="middle">
                                    <B><%= ML.getMLStr("construction_lbl_priority", userId)%></B>
                                </TD><TD align="right">
                                    <IMG src="<%=GameConfig.getInstance().picPath()%>pic/icons/compress.png" onclick="window.location.href = 'main.jsp?page=new/production&type=<%= type%>&compress=1';" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("queue_compress", userId)%>')" ONMOUSEOUT="hideTip()" style="cursor:pointer" />
                                </TD>
                            </TR>
                        </TABLE>
                    </td>
                    <td class="blue" width="25"><B>&nbsp;</B></td>
                </TR>
                <%  for (InConstructionEntry ice : inConsLocal.getEntries()) {
                        String name = "N/A";
                        if (ice.getOrder().getType() == EProductionOrderType.GROUNDTROOPS) {
                            name = ML.getMLStr(((GroundTroop) ice.getUnit()).getName(), userId);
                        } else if ((ice.getOrder().getType() == EProductionOrderType.PRODUCE)
                                || (ice.getOrder().getType() == EProductionOrderType.SCRAP)
                                || (ice.getOrder().getType() == EProductionOrderType.UPGRADE)
                                || (ice.getOrder().getType() == EProductionOrderType.REPAIR)) {
                            name = ((ShipDesign) ice.getUnit()).getName();
                        }

                        String extensionTag = "";

                        if (ice.getOrder().getType() == EProductionOrderType.SCRAP) {
                            extensionTag = " " + ML.getMLStr("production_lbl_abbr_scrapship", userId) + "";
                        }
                        if (ice.getOrder().getType() == EProductionOrderType.UPGRADE) {
                            extensionTag = " " + ML.getMLStr("production_lbl_abbr_refitship", userId) + "";
                        }
                        if (ice.getOrder().getType() == EProductionOrderType.REPAIR) {
                            extensionTag = " " + ML.getMLStr("production_lbl_abbr_repairship", userId) + "";
                        }
                %>
                <tr>
                    <%
                        if (ice.getAction().getUserId() == userId) {
                    %>
                    <td><%= name + extensionTag%></td>
                    <%
                    } else {
                        User u = MainService.findUserByUserId(ice.getAction().getUserId());
                    %>
                    <td><FONT color="lightblue"><%= name + extensionTag + " [" + u.getGameName() + "]"%></FONT></td>
                        <%
                            }
                        %>
                    <td align="center"><%= ice.getAction().getNumber()%></td>
                    <%
                        if (ice.getOrder().getModuleNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%} else {
                    %>
                    <td align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("production_lbl_modLeft", userId) + (ice.getOrder().getModuleNeed() - ice.getOrder().getModuleProc())%>')" ONMOUSEOUT="hideTip()"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getModulePerc(), 0)%> %</td>
                    <%
                        }
                        if (ice.getOrder().getDockNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%} else {
                    %>
                    <td align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("production_lbl_dockLeft", userId) + (ice.getOrder().getDockNeed() - ice.getOrder().getDockProc())%>')" ONMOUSEOUT="hideTip()"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getDockPerc(), 0)%> %</td>
                    <%
                        }
                        if (ice.getOrder().getOrbDockNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%} else {
                    %>
                    <td align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("production_lbl_orbdockLeft", userId) + (ice.getOrder().getOrbDockNeed() - ice.getOrder().getOrbDockProc())%>')" ONMOUSEOUT="hideTip()"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getOrbDockPerc(), 0)%> %</td>
                    <%
                        }
                        if (ice.getOrder().getKasNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%} else {
                    %>
                    <td align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("production_lbl_kasLeft", userId) + (ice.getOrder().getKasNeed() - ice.getOrder().getKasProc())%>')" ONMOUSEOUT="hideTip()"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getKasPerc(), 0)%> %</td>
                    <%
                        }

                    %>
                    <td>
                        <SELECT id="priority<%= ice.getOrder().getId()%>" name="priority<%= ice.getOrder().getId()%>" onchange="window.location.href = 'main.jsp?page=new/production&type=<%= type%>&detail=<%= detail%>&oNo=<%= ice.getOrder().getId()%>&priority=' + document.getElementById('priority<%= ice.getOrder().getId()%>').value;">
                            <%
                                for (int i = 0; i < ProdOrderBuffer.PRIORITY_COUNT; i++) {
                                    boolean marked = false;

                                    if (ice.getOrder().getPriority() == i) {
                                        marked = true;
                                    }

                                    String selStr = !marked ? "" : " selected";
                                    String prioTxt = "";

                                    if (i == 0) {
                                        prioTxt = " (" + ML.getMLStr("construction_opt_priority_high", userId) + ")";
                                    }
                                    if (i == 5) {
                                        prioTxt = " (" + ML.getMLStr("construction_opt_priority_average", userId) + ")";
                                    }
                                    if (i == 10) {
                                        prioTxt = " (" + ML.getMLStr("cosntruction_opt_priority_low", userId) + ")";
                                    }
                            %>
                            <option value="<%= i%>"<%= selStr%>><%= i%><%= prioTxt%></option>
                            <%
                                }
                            %>
                        </SELECT>
                    </td>
                    <%
                        if (ice.getOrder().getType() == EProductionOrderType.PRODUCE) {
                        }
                    %></tr><%
                            /*
                             }
                             */
                        }%>
            </TABLE></td></tr></table>
<BR>
<%
    }

    if (!inConsRemote.getEntries().isEmpty()) {
%>
<table cellpadding="0" cellspacing="0" width="90%"><tr><td>
            <TABLE class="blue" border="0" cellpadding="0" cellspacing="0" width="100%">
                <TR bgcolor="#808080" >
                    <td class="blue" width="*"><B><%= ML.getMLStr("production_lbl_ongoingproductionotherplanets", userId)%></B></td>
                    <td class="blue" width="50" align="center"><B><%= ML.getMLStr("production_lbl_quantity", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("construction_pop_module", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_module", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("construction_pop_shipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_shipyard", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("construction_pop_orbitalshipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_orbitalshipyard", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("construction_pop_barracks", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_barracks", userId)%></B></td>
                </TR>
                <%  for (InConstructionEntry ice : inConsRemote.getEntries()) {

                        String name = "N/A";
                        if (ice.getOrder().getType() == EProductionOrderType.GROUNDTROOPS) {
                            name = ML.getMLStr(((GroundTroop) ice.getUnit()).getName(), userId);
                        } else if ((ice.getOrder().getType() == EProductionOrderType.PRODUCE)
                                || (ice.getOrder().getType() == EProductionOrderType.SCRAP)
                                || (ice.getOrder().getType() == EProductionOrderType.UPGRADE)) {
                            name = ((ShipDesign) ice.getUnit()).getName();
                        }

                        String extensionTag = "";

                        if (ice.getOrder().getType() == EProductionOrderType.SCRAP) {
                            extensionTag = " (A)";
                        }
                        if (ice.getOrder().getType() == EProductionOrderType.UPGRADE) {
                            extensionTag = " (U)";
                        }

                        PlayerPlanet pp = PlanetService.getPlanetName(ice.getAction().getPlanetId());
                        if (pp == null) {
                            DebugBuffer.warning("Found invalid playerplanet for a production order (Order Id: " + ice.getOrder().getId() + ")");
                            continue;
                        }
                %>
                <tr>
                    <%
                        if (pp.getUserId() != userId) {%>
                    <td><FONT color="lightblue"><%= name + extensionTag + " [Planet: " + pp.getName() + " (" + pp.getPlanetId() + ")]"%></FONT></td>
                        <%              } else {%>
                    <td><%= name + extensionTag + " [Planet: " + pp.getName() + " (" + pp.getPlanetId() + ")]"%></td>
                    <%
                        }
                    %>
                    <td align="center"><%= ice.getAction().getNumber()%></td>
                    <%
                        if (ice.getOrder().getModuleNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%} else {
                    %>
                    <td align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("production_lbl_modLeft", userId) + (ice.getOrder().getModuleNeed() - ice.getOrder().getModuleProc())%>')" ONMOUSEOUT="hideTip()"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getModulePerc(), 0)%> %</td>
                    <%
                        }
                        if (ice.getOrder().getDockNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%} else {
                    %>
                    <td align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("production_lbl_dockLeft", userId) + (ice.getOrder().getDockNeed() - ice.getOrder().getDockProc())%>')" ONMOUSEOUT="hideTip()"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getDockPerc(), 0)%> %</td>
                    <%
                        }
                        if (ice.getOrder().getOrbDockNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%} else {
                    %>
                    <td align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("production_lbl_orbdockLeft", userId) + (ice.getOrder().getOrbDockNeed() - ice.getOrder().getOrbDockProc())%>')" ONMOUSEOUT="hideTip()"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getOrbDockPerc(), 0)%> %</td>
                    <%
                        }
                        if (ice.getOrder().getKasNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%} else {
                    %>
                    <td align="center" ONMOUSEOVER="doTooltip(event, '<%= ML.getMLStr("production_lbl_kasLeft", userId) + (ice.getOrder().getKasNeed() - ice.getOrder().getKasProc())%>')" ONMOUSEOUT="hideTip()"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getKasPerc(), 0)%> %</td>
                    <%
                        }
                    %></tr><%
                        }%>
            </TABLE>
        </td>
    </tr>
</table>
<BR>
<%
    }

// Major check checks if needed buildings are built on this planet
    boolean modulefactory = true;
    boolean shipyard = true;
    boolean orbitalShipyard = true;
    boolean barracks = true;

    modulefactory = ProductionService.isConstructed(planetId, Construction.ID_MODULEFACTORY);
    shipyard = ProductionService.isConstructed(planetId, Construction.ID_PLANETARYSHIPYARD);
    orbitalShipyard = ProductionService.isConstructed(planetId, Construction.ID_ORBITAL_SHIPYARD);
    barracks = ProductionService.isConstructed(planetId, Construction.ID_BARRACKS);

    boolean blockedByEnemy = false;
    blockedByEnemy = ProductionService.isBlockedByEnemy(planetId);

    boolean showExtHashMap = false;

    if (errors.size() > 0) {
        for (String error : errors) {
            out.write("<FONT color=\"red\"><B>" + error + "</B></FONT><BR>");
        }
        out.write("<BR>");

    }
    if (!((type == 0) || ((type == 1) && !shipyard) || ((type == 2) && !barracks) || ((type > 2) && !modulefactory))) {
        if ((detail != 0) || (type < 4)) {
            if (pbr.getUnitsWithPossibleTech().size() == 0) {%>
<CENTER><FONT style="font-size:13px;">Kein Schiffsdesign verf&uuml;gbar</FONT></CENTER>
    <%      } else {
        String appendToLink = "";
        if (shipType != null) {
            appendToLink += "&shipType=" + shipType;
        }
        if (chassisId != 0) {
            appendToLink += "&chassisId=" + chassisId;
        }
    %>

<FORM method="post" action="main.jsp?page=new/production&type=<%= type%>&detail=<%= detail%><%= appendToLink%>" Name="Production">
    <INPUT type="hidden" name="shipType" value="<%= shipType%>"/>
    <INPUT type="hidden" name="chassisId" value="<%= chassisId%>"/>
    <INPUT <% if (blockedByEnemy && type == 1) {%>disabled style="color: #777777;" <% }%>   type="submit" id="submit" name="submit" value="<%= ML.getMLStr("production_but_build", userId)%>">
    <% if (type == 1 && blockedByEnemy) {%>
    <BR>
    <BR>
    <FONT color="FF0000" size="2"><B><%= ML.getMLStr("production_msg_blockedByEnemy", userId)%></B></FONT>
        <% }%>
        <%      if (type == 2) {%>
    <BR><BR>
    <CENTER><FONT style="font-size:13px; color:red;"><B>ACHTUNG!</B></FONT><FONT style="font-size:13px;">  Beim Bau von Bodentruppen werden Arbeiter in Infantrie umgewandelt und stehen nicht mehr f�r die Arbeit zur Verf�gung</FONT></CENTER>
            <%      }%>
            <% if (type == 2) {%>
    <BR>
    <BR>
    <FONT size="2" color="FFFF00"><B><%= ML.getMLStr("production_lbl_effective", userId)%></B></FONT>&nbsp;
    <FONT color="FF0000" size="2"><B><%= ML.getMLStr("production_lbl_ineffective", userId)%></B></FONT>
        <% }%>
    <P>
        <%
            if (type == 1 && shipyard) {
        %>
    <TABLE width="80%">
        <TD align="left" class="blue2">
            <%= ML.getMLStr("production_lbl_shiptype", userId)%> :
        </TD>
        <TD align="left">
            <SELECT id="shipType" name="shipType" style='background-color: #000000; color: #ffffff; border:0px; width: 150px; font-size: 12px;' onChange="window.location.href = 'main.jsp?page=new/production&shipType=' + document.getElementById('shipType').value + '&chassisId=' + document.getElementById('chassisId').value">
                <OPTION value="<%= 0%>"><%= ML.getMLStr("production_opt_all", userId)%></OPTION>
                    <% for (EShipType t : EShipType.values()) {%>
                <OPTION <% if (t.equals(shipType)) {%> SELECTED <% }%> value="<%= t%>"><%= ML.getMLStr("db_shipdesign_shiptype_" + t.toString(), userId)%></OPTION>
                    <% }%>
            </SELECT>
        </TD>
        <TD align="left" class="blue2">
            <%= ML.getMLStr("production_lbl_chassis", userId)%> :
        </TD>
        <TD align="left">
            <SELECT id="chassisId" name="chassisId" style='background-color: #000000; color: #ffffff; border:0px; width: 150px; font-size: 12px;' onChange="window.location.href = 'main.jsp?page=new/production&shipType=' + document.getElementById('shipType').value + '&chassisId=' + document.getElementById('chassisId').value">
                <OPTION value="<%= 0%>"><%= ML.getMLStr("production_opt_all", userId)%></OPTION>
                    <% for (Chassis c : ProductionService.findAllChassisOrdered()) {%>
                <OPTION <% if (c.getId() == chassisId) {%> SELECTED <% }%> value="<%= c.getId()%>"><%= ML.getMLStr(c.getName(), userId)%></OPTION>
                    <% }%>
            </SELECT>
        </TD>
    </TABLE>
    <%
        }%>
    <%
            }
        }
        boolean collapsed = !ConstructionService.isShowProductionDetails(userId);
        String style = "";
        String img1 = "pic/icons/menu_ein.png";
        String img2 = "pic/icons/menu_aus.png";
        if (collapsed) {
            img2 = "pic/icons/menu_ein.png";
            img1 = "pic/icons/menu_aus.png";
            style = "none";
        }

        for (Buildable c : pbr.getUnitsWithPossibleTech()) {
            RessourcesEntry cost = c.getRessCost();
            ShipDesignDetailed sdd = null;
            String name = "N/A";
            String image = "";
            int crew = 0;
            int id = 0;
            int range = 0;

            BuildableResult br = pbr.getBuildInfo(c);
            double buildTime = Double.MIN_VALUE;
            ShipDesignExt sde = null;

            if (type == 1) {
                ShipDesign sd = (ShipDesign) c.getBase();
                sdd = new ShipDesignDetailed(sd.getId());
                sde = (ShipDesignExt) c;
                range = sd.getRange();

                if (shipType != null && !sd.getType().equals(shipType)) {
                    continue;
                }

                if (chassisId > 0 && chassisId != sd.getChassis()) {
                    continue;
                }
                if (sde.getChassis().getMinBuildQty() > 1) {
                    name = sdd.getName() + "-Geschwader" + " [&aacute; " + sde.getChassis().getMinBuildQty() + " " + ML.getMLStr(sde.getChassis().getName(), userId) + "]";
                } else {
                    name = sdd.getName() + " [" + ML.getMLStr(sde.getChassis().getName(), userId) + "]";
                }

                crew = sd.getCrew();
                id = sd.getId();

                showExtHashMap = true;
            } else if (type == 2) {
                GroundTroop gt = (GroundTroop) c.getBase();
                name = ML.getMLStr(gt.getName(), userId);
                crew = gt.getCrew();
                image = gt.getImage();
                id = gt.getId();
            }

            // calc Buildtime
            if (dockPoints > 0) {
                buildTime = Math.max(0, ((double) c.getBuildTime().getDockPoints() / (double) dockPoints));
            }
            if (orbDockPoints > 0) {
                buildTime = Math.max(buildTime, ((double) c.getBuildTime().getOrbDockPoints() / (double) orbDockPoints));
            }
            if (kasPoints > 0) {
                buildTime = Math.max(buildTime, ((double) c.getBuildTime().getKasPoints() / (double) kasPoints));
            }
            if (modulePoints > 0) {
                buildTime = Math.max(buildTime, ((double) c.getBuildTime().getModulePoints() / (double) modulePoints));
            }
            if ((orbDockPoints == 0) && (c.getBuildTime().getOrbDockPoints() > 0)) {
                buildTime = Double.MAX_VALUE;
            }
            if ((dockPoints == 0) && (c.getBuildTime().getDockPoints() > 0)) {
                buildTime = Double.MAX_VALUE;
            }
            if ((modulePoints == 0) && (c.getBuildTime().getModulePoints() > 0)) {
                buildTime = Double.MAX_VALUE;
            }
            if ((kasPoints == 0) && (c.getBuildTime().getKasPoints() > 0)) {
                buildTime = Double.MAX_VALUE;
            }

            boolean disabled = br.getPossibleCount() == 0;
            if (blockedByEnemy && type == 1) {
                disabled = true;
            }
    %>
    <TABLE class="blue" border=0 width="80%">
        <TR bgcolor="#808080">


            <TD class="blue" width="60%">

                <span onclick="toogleVisibility('production<%= id%>');
                            exchangeImgs('img_system<%= id%>', '<%= GameConfig.getInstance().picPath()%><%= img2%>',
                                    '+', '<%=GameConfig.getInstance().picPath()%><%= img1%>', '-');">
                    <img src="<%=GameConfig.getInstance().picPath()%><%= img1%>" alt="-" border="0" id="img_system<%= id%>">&nbsp;
                </span>
                <B><%= name%><% if (type == 2) {%> (<%= FormatUtilities.getFormattedNumber(ProductionService.findGroundsTroopBy(id, planetId, userId))%>)<% }%></B>
            </TD>
            <TD class="blue" width="40%" align="right">(max: <%=FormatUtilities.getFormattedNumber(br.getPossibleCount())%>) <%= ML.getMLStr("production_lbl_quantity", userId)%>:
                <INPUT <% if (disabled) {
                            out.write("DISABLED");%> style="color: #777777;"<% }%> NAME="build<%= id%>" TYPE="text" VALUE="0" SIZE="5" ALIGN=middle>
            </TD>
        </TR>
    </TABLE>
    <TABLE class="blue" style="font-size:13px;" width="80%">
        <% if ((type == 1) && ((sde.getBuildTime().getOrbDockPoints() > 0) && !orbitalShipyard)) {%>
        <TR valign="top">
            <TD colspan=3 align=left width="100%"><B><FONT COLOR="yellow">Keine Orbitalwerft zur Fertigstellung vorhanden!</FONT></B>
            </TD>
        </TR>
        <% }%>
        <TR style="display:<%= style%>" id="production<%= id%>">
            <TD>
                <!-- Production entry Details -->
                <TABLE width="100%">
                    <TR valign="top">
                        <% if (!image.equals("")) {%>
                        <TD width="180px">
                            <IMG alt="<%= name%>" src="<%= GameConfig.getInstance().picPath()%>pic/production/<%= image%>"/>
                        </TD>
                        <% }%>
                        <% if (showExtHashMap) {%>
                        <TD valign="TOP" align=left width="25%">
                            <% } else {%>
                        <TD valign="TOP" align=left width="*">
                            <% }%>

                            <% if (type == 2) {
                                    GroundTroop gt = (GroundTroop) c.getBase();%>
                            <%=  ML.getMLStr(gt.getDescription(), userId)%><BR><BR>
                            <% }%>
                            <% if (buildTime == Double.MAX_VALUE) {%>
                            <FONT size="2"><%= ML.getMLStr("construction_lbl_constructiontime", userId)%>: &#8734;<BR></FONT>
                            <% } else if (buildTime < 1) {%>
                            <FONT size="2"><%= ML.getMLStr("construction_lbl_constructiontime", userId)%>: <1 [<%= (int) Math.floor(1 / buildTime)%> / Tick]<BR></FONT>
                            <% } else {%>
                            <FONT size="2"><%= ML.getMLStr("construction_lbl_constructiontime", userId)%>: <%= (int) Math.ceil(buildTime)%><BR></FONT>
                            <% }%>
                            <% if (range > 0) {%>
                            <FONT size="2"><%= ML.getMLStr("construction_lbl_range", userId)%>: <%= FormatUtilities.getFormattedNumber(range)%> <BR></FONT>
                            <% }%>
                            <% if (crew > 0) {%>
                            Crew: <%= FormatUtilities.getFormattedNumber(crew)%> <BR>
                            <% }%>
                            <% if (type == 2) {%>
                            Hitpoints : <%=  FormatUtilities.getFormattedNumber(((GroundTroop) c.getBase()).getHitpoints(), ML.getLocale(userId))%>
                            <% }%>
                            <%
                                ArrayList<RessAmountEntry> ress = cost.getSortedRessArray();
                                if (ress.size() > 0) {
                            %>
                            <TABLE width="100%" style="font-size:13px" valign="TOP">
                                <%
                                    for (RessAmountEntry rce : cost.getSortedRessArray()) {
                                %>
                                <TR>
                                    <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(ProductionService.findRessource(rce.getRessId()).getName(), userId)%>" height="20" width="20" src="<%= GameConfig.getInstance().picPath()%><%= ProductionService.findRessource(rce.getRessId()).getImageLocation()%>"/></TD>
                                        <% if (rce.getQty() <= 0) {
                                                continue;
                                            }%>
                                    <TD align="LEFT"><%= ML.getMLStr(ProductionService.findRessource(rce.getRessId()).getName(), userId)%>: <%= FormatUtilities.getFormattedDecimalCutEndZero(rce.getQty(), 2)%></TD>
                                </TR>
                                <% }%>
                            </TABLE>
                            <% }%>
                        </TD>
                        <%
                            if (showExtHashMap) {
                        %>
                        <TD width="200" valign="TOP" align="middle" >
                            <TABLE width="200" style="font-size:13px" valign="TOP">
                                <TR>
                                    <TD><B>Module</B></TD>
                                </TR>
                                <% for (Map.Entry<Integer, Integer> me : sdd.getModuleCount().entrySet()) {
                                        Module mData = ProductionService.getModule(me.getKey());

                                        name = "";

                                        if (mData == null) {
                                            name = "<FONT color=\"red\">Ung&uuml;ltiges Modul (" + me.getKey() + ")</FONT>";
                                        } else {
                                            name = ML.getMLStr(mData.getName(), userId);
                                        }

                                        int count = me.getValue();%>
                                <TR>
                                    <TD align="LEFT"><%= count%>x <%= name%></TD>
                                </TR>
                                <% }%>
                            </TABLE>
                        </TD>
                        <% /* New code showing buildpoints of all kinds for ships */%>
                        <TD width="200" valign="TOP" align="middle">
                            <TABLE width="200" style="font-size:13px" valign="TOP">
                                <TR>
                                    <TD colspan="2"><B><%= ML.getMLStr("production_lbl_pointscost", userId)%></B></TD>
                                </TR>
                                <% if (c.getBuildTime().getModulePoints() > 0) {%>
                                <TR>
                                    <TD align="LEFT"><%= ML.getMLStr("production_lbl_modulefactory", userId)%>&nbsp;</TD>
                                    <TD align="RIGHT">&nbsp;<%= FormatUtilities.getFormattedNumber(c.getBuildTime().getModulePoints())%></TD>
                                </TR>
                                <% }%>
                                <% if (c.getBuildTime().getDockPoints() > 0) {%>
                                <TR>
                                    <TD align="LEFT"><%= ML.getMLStr("production_lbl_planetaryshipyards", userId)%>&nbsp;</TD>
                                    <TD align="RIGHT">&nbsp;<%= FormatUtilities.getFormattedNumber(c.getBuildTime().getDockPoints())%></TD>
                                </TR>
                                <% }%>
                                <% if (c.getBuildTime().getOrbDockPoints() > 0) {%>
                                <TR>
                                    <TD align="LEFT"><%= ML.getMLStr("production_lbl_orbitalshipyards", userId)%>&nbsp;</TD>
                                    <TD align="RIGHT">&nbsp;<%= FormatUtilities.getFormattedNumber(c.getBuildTime().getOrbDockPoints())%></TD>
                                </TR>
                                <% }%>
                                <% if (c.getBuildTime().getKasPoints() > 0) {%>
                                <TR>
                                    <TD align="LEFT"><%= ML.getMLStr("production_lbl_barracks", userId)%>&nbsp;</TD>
                                    <TD align="RIGHT">&nbsp;<%= FormatUtilities.getFormattedNumber(c.getBuildTime().getKasPoints())%></TD>
                                </TR>
                                <% }%>
                            </TABLE>
                        </TD>
                        <% }%>
                    </TR>
                    <%
                        if (type == 2) {

                            ArrayList<GroundTroop> gts = ProductionService.findAllGroundTroops();
                            GTRelationResult gtrr = ProductionService.findGTRelationsBy(id);
                    %>
                    <TR>
                        <TD colspan="2">
                            <TABLE class="blue2" border="0"  style="font-size:13px;" width="100%">
                                <%
                                    for (int decisioner = 0; decisioner <= 1; decisioner++) {
                                %>
                                <TR>
                                    <%
                                        boolean first = false;
                                        for (GroundTroop gt : gts) {
                                    %>


                                    <%
                                        GTRelation gtr = gtrr.getGtRelations().get(gt.getId());
                                        boolean effective = false;
                                        boolean nonEffective = false;
                                        Double damage = gtrr.getDamage().get(gt.getId());
                                        if (damage == null) {
                                            damage = 0d;
                                        }

                                        if (damage > (gtrr.getMeanDamage() * 1.4d)) {
                                            effective = true;
                                        }

                                        if (damage < (gtrr.getMeanDamage() * 0.2d)) {
                                            nonEffective = true;
                                        }
                                        String attackPoints = "~0";
                                        String attackProb = "~0";
                                        String hitProb = "~0";
                                        if (gtr != null) {
                                            attackPoints = String.valueOf(gtr.getAttackPoints());
                                            attackProb = String.valueOf(gtr.getAttackProbab());
                                            hitProb = String.valueOf(gtr.getHitProbab());
                                        }

                                        switch (decisioner) {
                                            case (1):
                                                if (first) {
                                    %>
                                    <TD>Name</TD>
                                        <%
                                                first = false;
                                            }
                                            if (effective) {
                                        %>
                                    <TD align="center" width="10%" class="blue2"><FONT color="FFFF00"><B><%= ML.getMLStr(gt.getName(), userId)%></B></FONT></TD>
                                            <%
                                            } else if (nonEffective) {
                                            %>
                                    <TD align="center" width="10%" class="blue2"><FONT color="FF0000"><B><%= ML.getMLStr(gt.getName(), userId)%></B></FONT></TD>
                                            <%
                                            } else {
                                            %>
                                    <TD align="center" width="10%" class="blue2"><%= ML.getMLStr(gt.getName(), userId)%></TD>
                                        <%
                                            }
                                        %>

                                    <%

                                            break;
                                        case (2):
                                            if (first) {
                                    %>
                                    <TD>AngriffsPunkte</TD>
                                        <%
                                                first = false;
                                            }
                                        %>
                                    <TD><%= attackPoints%></TD>
                                        <%
                                                break;
                                            case (3):
                                                if (first) {
                                        %>
                                    <TD>Angriffswahrscheinlichkeit</TD>
                                        <%
                                                first = false;
                                            }
                                        %>
                                    <TD><%= attackProb%></TD>
                                        <%
                                                break;
                                            case (4):
                                                if (first) {
                                        %>
                                    <TD>Trefferwahrscheinlichkeit</TD>
                                        <%
                                                first = false;
                                            }
                                        %>
                                    <TD><%= hitProb%></TD>
                                        <%
                                                        break;
                                                }
                                            }
                                        %>
                                </TR>
                                <%
                                    }
                                %>
                            </TABLE></TD></TR>
                            <%
                                }%>

                </TABLE>
            </TD>
        </TR>


    </TABLE>
    <BR>
    <%

        }
        if ((detail != 0) || (type < 4)) {
            if (pbr.getUnitsWithPossibleTech().size() > 0) {
    %>
    <BR><INPUT <% if (blockedByEnemy && type == 1) {%>disabled style="color: #777777;" <% }%> type="submit" id="submit" name="submit" value="<%= ML.getMLStr("production_but_build", userId)%>">
</FORM>
<%
        }
    }
} else {
%> <FONT style="font-size:13px;"> <%
    switch (type) {
    case 1:%><%= ML.getMLStr("production_no_shipyard", userId)%><%
        break;
        case 2:%><%= ML.getMLStr("production_no_barracks", userId)%><%
            break;
        default:%>Momentan deaktiviert<%
        }
%> </FONT> <%

        // <FONT style="font-size:13px;">Keine Modulfabrik vorhanden</FONT>
    }

    allPars.clear();
}
// stmt2.close();
%>

