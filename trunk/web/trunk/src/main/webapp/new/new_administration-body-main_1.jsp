<%@page import="at.darkdestiny.core.utilities.ShippingUtilities"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.core.databuffer.*"%>
<%@page import="at.darkdestiny.core.construction.InConstructionData"%>
<%@page import="java.util.*"%>
<%@page import="java.io.IOException"%>
<%@page import="at.darkdestiny.core.view.header.*"%>
<%@page import="at.darkdestiny.core.planetcalc.*"%>
<%@page import="at.darkdestiny.core.result.*"%>
<%@page import="at.darkdestiny.core.service.AdministrationService"%>
<%@page import="at.darkdestiny.core.utilities.PopulationUtilities"%>
<%@page import="at.darkdestiny.core.utilities.ConstructionUtilities"%>
<%@page import="at.darkdestiny.core.viewbuffer.HeaderBuffer"%>
<%@page import="at.darkdestiny.core.enumeration.*"%>
<%@page import="at.darkdestiny.core.result.*"%>
<%@page import="at.darkdestiny.core.service.*"%>
<%@page import="at.darkdestiny.core.model.*"%>
<%@page import="at.darkdestiny.core.filter.*"%>
<%@page import="at.darkdestiny.core.model.Note"%>
<%@page import="at.darkdestiny.core.model.Filter"%>
<%@page import="at.darkdestiny.core.model.Ressource"%>
<%

    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int actCategory = Integer.parseInt((String) session.getAttribute("actCategory"));

%>
<script  type="text/javascript">

    var language = '<%= ML.getLocale(userId) %>';
    function name(){
        return "administration";
    }
</script>

<%
    int process = 0;
    final int PROCESS_BUILDBUILDING = 1;
    final int PROCESS_CHANGETAXES = 2;
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }

    if (process > 0) {
        if (process == PROCESS_BUILDBUILDING) {
            ConstructionService.buildBuilding(userId, request.getParameterMap());
        }
        if (process == PROCESS_CHANGETAXES) {
            ManagementService.changeTaxes(request.getParameterMap(), userId, Integer.parseInt(request.getParameter("planetId")));
        }
    }


    String basePicPath = GameConfig.getInstance().picPath();

    HeaderData hData = HeaderBuffer.getUserHeaderData(userId);
    HashMap<Integer, SystemEntity> allActSystems = hData.getCategorys().get(actCategory).getSystems();
    HashMap<Integer, HashMap<Integer, HashMap<Boolean, Long>>> tradeBalances = ShippingUtilities.findTradeBalanceForUserSorted(userId);


// Filter START
    int filterId = 0;
    if (request.getParameter("filterId") != null) {
        filterId = Integer.parseInt(request.getParameter("filterId"));
    }
// Filter ENDE
// Variablen zum verwalten der Seitenanzeige START
    int firstItem = 0;
    int fromItem = 0;
    int toItem = 15;
    int itemsPerPage = 15;

    if (request.getParameter("fromItem") != null) {
        fromItem = Integer.parseInt(request.getParameter("fromItem"));
    }
    if (request.getParameter("toItem") != null) {
        toItem = Integer.parseInt(request.getParameter("toItem"));
    }

    PlanetFilter pf = new PlanetFilter(allActSystems, fromItem, toItem, filterId, userId);
    fromItem = fromItem + 1;
    PlanetFilterResult pfr = pf.getResult();
    int lastItem = pfr.getItems();
    if (toItem > lastItem) {
        toItem = lastItem;
    }
    if (lastItem == 0) {
        fromItem = 0;
    }
// Variablen zum verwalten der Seitenanzeige ENDE

%>
<BR>
<BR>
<TABLE id="planetData" border="0" style="font-size:13px" width="90%"  align="center" cellpadding="0" cellspacing="0">    
    <SCRIPT type="text/javascript">    
        var doJustOnce = true;
    </SCRIPT>
    <%

String basePath = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);


        for (PlanetFilterResultEntry pfre : pfr.getResult()) {

            Planet p = pfre.getP();
            PlayerPlanet pp = pfre.getPp();

%>
   <TR><TD>
            <!-- Planeteneintrag-->
            <TABLE>
                <TR>
                    
                    <!-- Rohstoffe -->
                    <TD>Ressources
                        <TABLE id="<%= p.getId()%>_ressources"></TABLE>
                    </TD>
                    <TD>RawRessources
                        <TABLE style="font-size:10px; height:14px; font-weight:bold;  border-style: solid; border-width: thin;">
                <TBODY id="<%= p.getId()%>_rawressources"></TBODY>
                </TABLE>                   </TD>
    </TR>
    
</TABLE>
                        <script type="text/javascript">
                            buildTables(<%= userId %>, <%= p.getId()%>, planetData);
                            if(doJustOnce){
                            ajaxCall('<%= basePath %>GetJSONManagementData?userId=<%= userId %>&planetId=<%= p.getId() %>',processManagementData);
                            doJustOnce = true;
                            }
                        </script>
</TD></TR>
<%

    }
%>
</TABLE><%

%>
