<%@page import="at.darkdestiny.core.ML"%>
<%
int type = 1;

            int userId = Integer.parseInt((String) session.getAttribute("userId"));
if (request.getParameter("type") != null) {
    type = Integer.parseInt(request.getParameter("type"));
}
%>
<BR>
<TABLE width="80%">
    <TR align="center">
        <TD width="34%"><U><A href="main.jsp?page=new/intelligence-main&type=1"><%= ML.getMLStr("intelligence_link_overview", userId) %></A></U></TD>
        <TD width="33%"><U><A href="main.jsp?page=new/intelligence-main&type=2"><%= ML.getMLStr("intelligence_link_reports", userId) %></A></U></TD>
    </TR>
</TABLE><BR>
<%
switch (type) {
    case(1):
%>
        <jsp:include page="../new/intelligence-overview.jsp" />
<%
        break;
    case(2):
%>
        <jsp:include page="../new/intelligence-reports.jsp" />
<%
        break;
}
%>