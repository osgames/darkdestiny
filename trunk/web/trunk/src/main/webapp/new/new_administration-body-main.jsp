<%@page import="at.darkdestiny.core.utilities.ShippingUtilities"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.core.databuffer.*"%>
<%@page import="at.darkdestiny.core.construction.InConstructionData"%>
<%@page import="java.util.*"%>
<%@page import="java.io.IOException"%>
<%@page import="at.darkdestiny.core.view.header.*"%>
<%@page import="at.darkdestiny.core.planetcalc.*"%>
<%@page import="at.darkdestiny.core.result.*"%>
<%@page import="at.darkdestiny.core.service.AdministrationService"%>
<%@page import="at.darkdestiny.core.utilities.PopulationUtilities"%>
<%@page import="at.darkdestiny.core.utilities.ConstructionUtilities"%>
<%@page import="at.darkdestiny.core.viewbuffer.HeaderBuffer"%>
<%@page import="at.darkdestiny.core.enumeration.*"%>
<%@page import="at.darkdestiny.core.result.*"%>
<%@page import="at.darkdestiny.core.service.*"%>
<%@page import="at.darkdestiny.core.model.*"%>
<%@page import="at.darkdestiny.core.filter.*"%>
<%@page import="at.darkdestiny.core.model.Note"%>
<%@page import="at.darkdestiny.core.model.Filter"%>
<%@page import="at.darkdestiny.core.model.Ressource"%>
<%

    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    int actCategory = Integer.parseInt((String) session.getAttribute("actCategory"));

%>
<script  type="text/javascript">

    var language = '<%= ML.getLocale(userId) %>';
    function name(){
        return "administration";
    }
</script>

<%
    int process = 0;
    final int PROCESS_BUILDBUILDING = 1;
    final int PROCESS_CHANGETAXES = 2;
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }

    if (process > 0) {
        if (process == PROCESS_BUILDBUILDING) {
            ConstructionService.buildBuilding(userId, request.getParameterMap());
        }
        if (process == PROCESS_CHANGETAXES) {
            ManagementService.changeTaxes(request.getParameterMap(), userId, Integer.parseInt(request.getParameter("planetId")));
        }
    }


    String basePicPath = GameConfig.getInstance().picPath();

    HeaderData hData = HeaderBuffer.getUserHeaderData(userId);
    HashMap<Integer, SystemEntity> allActSystems = hData.getCategorys().get(actCategory).getSystems();
    HashMap<Integer, HashMap<Integer, HashMap<Boolean, Long>>> tradeBalances = ShippingUtilities.findTradeBalanceForUserSorted(userId);


// Filter START
    int filterId = 0;
    if (request.getParameter("filterId") != null) {
        filterId = Integer.parseInt(request.getParameter("filterId"));
    }
// Filter ENDE
// Variablen zum verwalten der Seitenanzeige START
    int firstItem = 0;
    int fromItem = 0;
    int toItem = 15;
    int itemsPerPage = 15;

    if (request.getParameter("fromItem") != null) {
        fromItem = Integer.parseInt(request.getParameter("fromItem"));
    }
    if (request.getParameter("toItem") != null) {
        toItem = Integer.parseInt(request.getParameter("toItem"));
    }

    PlanetFilter pf = new PlanetFilter(allActSystems, fromItem, toItem, filterId, userId);
    fromItem = fromItem + 1;
    PlanetFilterResult pfr = pf.getResult();
    int lastItem = pfr.getItems();
    if (toItem > lastItem) {
        toItem = lastItem;
    }
    if (lastItem == 0) {
        fromItem = 0;
    }
// Variablen zum verwalten der Seitenanzeige ENDE

%>
<BR>
<!-- FILTER START  -->
<FORM name="filterForm" method="post">
    <TABLE  border="0" style="font-size:11px; color:white;"  align="center" cellpadding="0" cellspacing="0">
        <TR>
            <TD width="100px" class="blue2">
                <%= ML.getMLStr("administration_label_filter", userId)%>:
            </TD>
            <TD class="blue" width="300px" align="center">
                <SELECT name="filterSelect" style='background-color: #000000; color: #ffffff; border:0px; width: 100%; font-size: 11px;' onChange="window.location=document.filterForm.filterSelect.options[document.filterForm.filterSelect.selectedIndex].value">
                    <OPTION value="main.jsp?page=new/new_administration-body-main&filterId=0&fromItem=<%= firstItem%>&toItem=<%= itemsPerPage%>"><%= ML.getMLStr("administration_opt_none", userId)%></OPTION>
                    <% for (Filter f : FilterService.findAllFilters()) {
                            if (f.getUserId() != 0 && f.getUserId() != userId) {
                                continue;
                            }
                    %>
                    <OPTION value="main.jsp?page=new/new_administration-body-main&filterId=<%= f.getId()%>&fromItem=<%= firstItem%>&toItem=<%= itemsPerPage%>"  <% if (f.getId() == filterId) {%>SELECTED<%}%>><%= f.getName()%></OPTION>
                    <% }%>
                </SELECT>
            </TD>
            <TD width="160px" align="center" style="width:150px;">
                <A href="main.jsp?page=new/filters"><%= ML.getMLStr("administration_lbl_configurefilter", userId)%></A>
            </TD>
        </TR>
    </TABLE>
</FORM>
<BR>
<!-- FILTER END -->
<!-- PageWeiterschaltor START -->
<TABLE border="0" style="font-size:11px; font-weight: bolder; color:white;" width="150px"  align="center" cellpadding="0" cellspacing="0">
    <TR class="blue2">
        <TD align="center" width="10px">
            <% if (fromItem > 1) {%>
            <A href="main.jsp?page=new/new_administration-body-main&fromItem=0&toItem=15&filterId=<%= filterId%>">
                |<<
            </A>
            <% }%>
        </TD>
        <TD align="center"  width="10px">
            <% if (fromItem > 1) {%>
            <A href="main.jsp?page=new/new_administration-body-main&fromItem=<%= ((fromItem - 1) - itemsPerPage)%>&toItem=<%= (fromItem - 1)%>&filterId=<%= filterId%>">
                <
            </A>
            <% }%>
        </TD>
        <TD  width="20px" align="center">
            <%= fromItem%> - <%= toItem%>
        </TD>
        <TD align="center"  width="10px">
            <% if (toItem < lastItem) {%>
            <A href="main.jsp?page=new/new_administration-body-main&fromItem=<%= toItem%>&toItem=<%= (toItem + itemsPerPage)%>&filterId=<%= filterId%>">
                >
            </A>
            <% }%>
        </TD>
        <TD align="center"  width="10px">
            <% if (toItem < lastItem) {
                    int subtractor = pfr.getTotalItems() % itemsPerPage;
            %>
            <A href="main.jsp?page=new/new_administration-body-main&fromItem=<%= (pfr.getTotalItems() - subtractor)%>&toItem=<%= pfr.getTotalItems()%>&filterId=<%= filterId%>">
                >>|
            </A>
            <%
                }

            %>
        </TD>
    </TR>
</TABLE>
<!-- PageWeiterschaltor ENDE -->
<BR>

<TABLE id="planetData" border="0" style="font-size:13px" width="90%"  align="center" cellpadding="0" cellspacing="0">

    <%

String basePath = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);


        // for (Map.Entry<Integer, PlanetFilterResultEntry> pfres : pfr.getResult().entrySet()) {
        for (PlanetFilterResultEntry pfre : pfr.getResult()) {


            // PlanetFilterResultEntry pfre = pfres.getValue();
            Planet p = pfre.getP();
            PlayerPlanet pp = pfre.getPp();

                            java.lang.System.out.println("processing planet : " + pfre.getP().getId());

            // Content
%>
   <TR><TD align="center">
            <!-- Planeteneintrag-->
            <TABLE border="0" style="padding-bottom:10px;" cellspacing="0">
                <TR style="background-color:#4A4A4A;" valign="top">
                    <!-- Planetpic and general stuff -->
                    <TD width="200px">
                        <TABLE width="100%" cellspacing="0" border="0">
                            <TR>
                                <TD style="background-color:#000000; padding:5px;" align="middle" valign="middle"><IMG alt="Planet" onmouseover="doTooltip(event,'<%= pp.getName()%>')" onmouseout="hideTip()"  onclick="window.location.href = 'selectview.jsp?showPlanet=<%= pp.getPlanetId()%>';"  src="<%= basePicPath + "pic/" + p.getPlanetPic()%>" /></TD>
                            </TR>
                            <TR style="height:3px;"><TD></TD></TR>
                            <TR>
                                <TD align="center" style="background-color:#000000; font-weight: bold; font-size: 13px; padding:3px;"><%= pp.getName()%></TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <!-- Possible actions -->
                    <TD width="30">
                        <TABLE style="background-color:#2E2E2E;" cellpadding="3px" cellspacing="0" border="0">
                            <TR>
                                <TD>
                                    <IMG alt="Planet" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_showsystem", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/system&systemId=<%= p.getSystemId()%>'" src="<%= basePicPath%>pic/goSystem.jpg" />
                                </TD>
                            </TR>
                            <TR>
                                <TD>
                                    <IMG alt="Planet" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_pop_showplanet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'selectview.jsp?showPlanet=<%= pp.getPlanetId()%>';" src="<%= basePicPath%>pic/goPlanet.jpg" />
                                </TD>
                            </TR>
                            <TR>
                                <TD>
                                    <IMG alt="Planet" onmouseover="doTooltip(event,'<%= ML.getMLStr("administration_lbl_sendfleet", userId)%>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/sendfleet&planetId=<%= pp.getPlanetId()%>'" src="<%= basePicPath%>pic/movefleet.jpg" />
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <!-- Rohstoffe -->
                    <TD>
                        <TABLE id="<%= p.getId()%>_ressources"></TABLE>
                    </TD>
                    <TD>
                        <!-- Hier wirds eingesetzt -->
                        <TABLE id="<%= p.getId()%>_rawressources"></TABLE>
                    </TD>
                <!-- Verwaltung -->
                <TD valign="top">
                    <TABLE id="<%= p.getId()%>_management" cellspacing ="0" cellpadding="0">
                    </TABLE>
                </TD>
    </TR>
    <TR>
        <TD colspan="1" align="right" valign="top">
            <FORM method="post" action="main.jsp?page=new/new_administration-body-main&fromItem=<%= (fromItem - 1)%>&toItem=<%= toItem%>&filterId=<%= filterId%>&process=<%= PROCESS_BUILDBUILDING%>">
                <TABLE width="100%" cellpadding="0" cellspacing="0">
                    <TR>
                        <TD colspan="1">
                            <TABLE id="<%= p.getId()%>_construction" cellpadding="0" cellspacing="0"></TABLE>
                        </TD>

                    </TR>
                </TABLE>
            </FORM>
        </TD>
        <TD colspan="4" align="right" valign="top">
            <TABLE id="<%= p.getId()%>_runningconstructions"></TABLE>
        </TD>
    </TR>
</TABLE>
                        <script type="text/javascript">

                           //Hier werden die Tabellen als erst aufgebaut
                           buildTables(<%= userId %>, <%= p.getId()%>, planetData);
                            // Hier werden die werte geladen und refreshed
                            ajaxCall('<%= basePath %>GetJSONManagementData?userId=<%= userId %>&planetId=<%= p.getId() %>',processManagementData);


                        </script>
</TD></TR>
<%

    }
%>
</TABLE><%

%>

<!-- PageWeiterschaltor START -->
<TABLE border="0" style="font-size:11px; font-weight: bolder; color:white;" width="150px"  align="center" cellpadding="0" cellspacing="0">
    <TR class="blue2">
        <TD align="center" width="10px">
            <% if (fromItem > 1) {%>
            <A href="main.jsp?page=new/new_administration-body-main&fromItem=0&toItem=15&filterId=<%= filterId%>">
                |<<
            </A>
            <% }%>
        </TD>
        <TD align="center"  width="10px">
            <% if (fromItem > 1) {%>
            <A href="main.jsp?page=new/new_administration-body-main&fromItem=<%= ((fromItem - 1) - itemsPerPage)%>&toItem=<%= (fromItem - 1)%>&filterId=<%= filterId%>">
                <
            </A>
            <% }%>
        </TD>
        <TD  width="20px" align="center">
            <%= fromItem%> - <%= toItem%>
        </TD>
        <TD align="center"  width="10px">
            <% if (toItem < lastItem) {%>
            <A href="main.jsp?page=new/new_administration-body-main&fromItem=<%= toItem%>&toItem=<%= (toItem + itemsPerPage)%>&filterId=<%= filterId%>">
                >
            </A>
            <% }%>
        </TD>
        <TD align="center"  width="10px">
            <% if (toItem < lastItem) {
                    int subtractor = pfr.getTotalItems() % itemsPerPage;
            %>
            <A href="main.jsp?page=new/new_administration-body-main&fromItem=<%= (pfr.getTotalItems() - subtractor)%>&toItem=<%= pfr.getTotalItems()%>&filterId=<%= filterId%>">
                >>|
            </A>
            <%
                }

            %>
        </TD>
    </TR>
</TABLE>
<!-- PageWeiterschaltor ENDE -->