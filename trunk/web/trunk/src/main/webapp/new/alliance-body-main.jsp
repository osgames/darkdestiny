<%@page import="at.darkdestiny.core.utilities.AllianceUtilities"%>
<%@page import="at.darkdestiny.core.enumeration.ELeadership"%>
<%@page import="at.darkdestiny.core.service.AllianceBoardService"%>
<%@page import="at.darkdestiny.core.enumeration.EAllianceBoardType"%>
<%@page import="at.darkdestiny.core.result.AllianceStructureResult"%>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="at.darkdestiny.core.GenerateMessage" %>
<%@page import="at.darkdestiny.core.GameConfig" %>
<%@page import="at.darkdestiny.core.GameUtilities" %>
<%@page import="at.darkdestiny.core.service.AllianceService" %>
<%@page import="at.darkdestiny.core.FormatUtilities" %>
<%@page import="at.darkdestiny.util.DebugBuffer" %>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel" %>
<%@page import="at.darkdestiny.core.result.BaseResult" %>
<%@page import="java.awt.Point" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
  <script type="text/javascript">

   function setANameField() {
    var selectBox = document.getElementById("receiver");
    var selectedValue = parseInt(selectBox.options[selectBox.selectedIndex].value);

    switch (selectedValue) {
        case(0):
        case(1):
        case(2):
        case(3):
            document.getElementById("aname").disabled = true;
            break;
        default:
            document.getElementById("aname").disabled = false;
            break;
    }
   }

  </script>
<%
// Logger.getLogger().write("DEBUG 1");

session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int alliancesSize = 0;
boolean showDescription = true;

// Logger.getLogger().write("DEBUG 2");

// VIEW CONSTANTS
final int TYPE_SHOWMEMBERS = 1;
final int TYPE_EDITPROFILE = 2;
final int TYPE_LEAVEALLIANCE = 3;
final int TYPE_CREATEMESSAGE = 4;
final int TYPE_CREATEALLIANCE = 5;
final int TYPE_DELETEMESSAGE = 6;
final int TYPE_DELETEALLIANCE = 7;
final int TYPE_CREATETREATY = 8;
final int TYPE_LEAVETREATY = 9;
final int TYPE_EDITRANKS = 10;
final int TYPE_SHOWALLIANCEBOARD = 11;
final int TYPE_MESSAGES = 12;
final int TYPE_RECRUITING = 13;
final int TYPE_MANAGEALLIANCEBOARD = 14;
final int TYPE_CREATEALLIANCEBOARD = 15;
final int TYPE_EDITUSERRANK = 16;
final int TYPE_DELETEUSER = 17;
final int TYPE_JOINALLIANCE = 19;
final int TYPE_DELETETREATY = 20;
final int TYPE_SHOWSTRUCTURE = 21;
final int TYPE_SHOWDESCRIPTION = 22;
final int TYPE_SETBOARDPERMISSIONS = 23;
final int TYPE_CHANGELEADERSHIP = 24;

// PROCESS CONSTANTS
final int PROCESS_SAVEPROFILE = 1;
final int PROCESS_CREATETREATY = 2;
final int PROCESS_LEAVETREATY = 3;
final int PROCESS_CREATEALLIANCE = 4;
final int PROCESS_DELETEALLIANCE = 5;
final int PROCESS_EDITRANKS = 6;
final int PROCESS_SETUSERRANK = 7;
final int PROCESS_SENDMESSAGES = 8;
final int PROCESS_SENDMESSAGETOUSER = 9;
final int PROCESS_DELETEUSER = 10;
final int PROCESS_EDITUSERRANK = 11;
final int PROCESS_DELETERANK = 12;
final int PROCESS_CREATEALLIANCEMESSAGE = 13;
final int PROCESS_DELETEALLIANCEMESSAGE = 14;
final int PROCESS_SWITCHRECRUITING = 15;
final int PROCESS_CREATEALLIANCEBOARD = 16;
final int PROCESS_LEAVEALLIANCE = 17;
final int PROCESS_DELETEALLIANCEBOARD = 18;
final int PROCESS_JOINALLIANCE = 19;
final int PROCESS_DELETETREATY = 20;
final int PROCESS_CHANGELEADERSHIP = 24;

// Logger.getLogger().write("DEBUG 3");
// int boardId = AllianceBoard.ALLIANCE_BOARD_ID;
Integer boardId = AllianceUtilities.getDefaultBoard(userId);
int type = 0;
int process = 0;
int messagesPerSite = 15;
int from = 0;
int to = 15;
int targetUserId = 0;

ArrayList<String> errors = new ArrayList<String>();
ArrayList<String> affirms = new ArrayList<String>();
ArrayList<BaseResult> result = new ArrayList<BaseResult>();

// Logger.getLogger().write("DEBUG 4");

if (request.getParameter("type") != null) {
    type = Integer.parseInt(request.getParameter("type"));

    // This user has no board so surpress display
    if ((boardId == null) && (type == TYPE_SHOWALLIANCEBOARD)) {
        type = 0;
    }
}

if(request.getParameter("boardId") != null){
    boardId = Integer.parseInt(request.getParameter("boardId"));
}

if(type == TYPE_SETBOARDPERMISSIONS){%>
 <jsp:include page="alliance-board-permissions.jsp">
       <jsp:param name="boardId" value='<%= boardId %>' />
 </jsp:include>
<% }

if (request.getParameter("targetUserId") != null) {
            targetUserId = Integer.parseInt(request.getParameter("targetUserId"));
}

if (request.getParameter("process") != null) {
    process = Integer.parseInt(request.getParameter("process"));
}



// Logger.getLogger().write("DEBUG 5");

switch(process){
    case(PROCESS_SAVEPROFILE):
        result =  AllianceService.updateAllianceProfile(userId, request.getParameterMap());
        process = 0;
        showDescription = true;
            break;
   case(PROCESS_JOINALLIANCE):

        result =  AllianceService.joinAlliance(userId, request.getParameterMap());
        process = 0;
        showDescription = true;
        break;

    case(PROCESS_CREATEALLIANCE):
        result = AllianceService.createAlliance(userId, request.getParameterMap());
        AllianceMember allianceMember = AllianceService.findAllianceMemberByUserId(userId);

        process = 0;
        if(allianceMember != null) {
            Alliance alliance = AllianceService.findAllianceByAllianceId(allianceMember.getAllianceId());
            showDescription = true;
        }
        break;

    case(PROCESS_CREATEALLIANCEMESSAGE):
        result = AllianceBoardService.createAllianceMessage(userId, request.getParameterMap());
        process = 0;
        type = TYPE_SHOWALLIANCEBOARD;
        showDescription = false;

        break;
   case(PROCESS_DELETEALLIANCEMESSAGE):
       result = AllianceBoardService.deleteAllianceMessage(userId, request.getParameterMap());
        type = TYPE_SHOWALLIANCEBOARD;
        showDescription = false;
        process = 0;
       break;

    case(PROCESS_CREATETREATY):
        // Logger.getLogger().write("CREATE TREATY");
        result = AllianceService.createTreatyRequest(userId, request.getParameterMap());
        process = 0;
        showDescription = true;
        // Logger.getLogger().write("GOT HERE AS WELL");
       break;

      case(PROCESS_LEAVETREATY):
    //case(PROCESS_DELETETREATY):
        result = AllianceService.deleteTreaty(userId);

        type = TYPE_SHOWMEMBERS;
        showDescription = false;
        process = 0;
            break;

    case(PROCESS_LEAVEALLIANCE):
        AllianceService.leaveAlliance(userId);
        type = 0;
        process = 0;
       break;

    case(PROCESS_CREATEALLIANCEBOARD):
        result = AllianceBoardService.createAllianceBoard(userId, request.getParameterMap());

        showDescription = false;
        type = TYPE_MANAGEALLIANCEBOARD;
        break;
    case(PROCESS_DELETEALLIANCEBOARD):

        result = AllianceBoardService.deleteAllianceBoard(userId, request.getParameterMap());

        process = 0;
        showDescription = false;
        type = TYPE_MANAGEALLIANCEBOARD;
        break;

    case(PROCESS_SWITCHRECRUITING):
        result = AllianceService.switchRecruiting(userId);
        process = 0;
        showDescription = true;

        break;
    case(PROCESS_DELETERANK):
        result =  AllianceService.deleteRank(userId, request.getParameterMap());
        process = 0;
        showDescription = false;
        type = TYPE_EDITRANKS;

        break;
    case(PROCESS_EDITRANKS):
        result = AllianceService.createRank(userId, request.getParameterMap());
        process = 0;
        showDescription = false;
        type = TYPE_EDITRANKS;

        break;
   case(PROCESS_SETUSERRANK):
        result = AllianceService.setUserRank(userId, request.getParameterMap());

        process = 0;
        type = TYPE_SHOWMEMBERS;
        showDescription = false;
       break;
   case(PROCESS_DELETEUSER):
       result = AllianceService.deleteUserFromAlliance(userId, request.getParameterMap());
       process = 0;
       showDescription = false;
       type = TYPE_SHOWMEMBERS;
       break;
   case(PROCESS_DELETEALLIANCE):
       AllianceService.deleteAlliance(userId);
       process = 0;
       showDescription = false;
       type = 0;
       break;
   case(PROCESS_CHANGELEADERSHIP):
       String targetLeadership = request.getParameter("newLeadership");
       AllianceService.changeLeaderShip(userId, targetLeadership);
       break;
   case(PROCESS_SENDMESSAGES):
       result = AllianceService.sendAllianceMessage(userId, request.getParameterMap());
       break;       
}

AllianceMember allianceMember = AllianceService.findAllianceMemberByUserId(userId);
Alliance alliance = null;
if(allianceMember != null){
    alliance = AllianceService.findAllianceByAllianceId(allianceMember.getAllianceId());
}

if (type > 0){
    showDescription = false;
} else {
    if (allianceMember != null) {
        type = TYPE_SHOWMEMBERS;
    }
    showDescription = true;
}

if(!result.isEmpty()){ %>
<TABLE>
    <% for(BaseResult br : result){ %>
    <TR>
        <%
        String color = "#00FF00";
        if(br.isError()){
            color = "#FF0000";
            }
        %>
    	<TD align="center">
            <B><FONT color="<%= color %>"> <%= br.getMessage() %></FONT></B>
    	</TD>
    </TR>
    <% } %>
</TABLE>
 <%
    }

// Logger.getLogger().write("DEBUG 6");

if (request.getParameter("from") != null) {
    from = Integer.parseInt(request.getParameter("from"));
}
if (request.getParameter("to") != null) {
    to = Integer.parseInt(request.getParameter("to"));
}

/*
 *################
 *#Errors
 *##############
 * */

if(errors.size() > 0){
    for(String err : errors){
    %>
   <B><FONT COLOR="RED">Error: <%= err%></FONT><BR/></B>
    <%
            }

}else{
if(affirms.size() > 0){
    for(String msg : affirms){
    %>
   <B><FONT COLOR="YELLOW">Benachrichtigung <%= msg %></FONT><BR/></B>
    <%
            }
    }
}
%>

   <%

// Logger.getLogger().write("DEBUG 7");

if(type == TYPE_SHOWDESCRIPTION){
    showDescription = true;
}
/*
 *################
 *#Decription
 *##############
 * */
if(showDescription){

if(alliance != null){
    // Logger.getLogger().write("DEBUG 8");

%>
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script language="Javascript">
    function setPic() {
        var availSpace = (Weite-355) * 0.8;
        if(availSpace > 500){
            availSpace = 800;
        }
        document.getElementById("bild").setAttribute("width", availSpace, 0);
    }
</script>
<%
    AllianceStructureResult asr = AllianceService.getAllianceStructure(alliance.getId());
%>
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['orgchart']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addRows([
<%
        if (allianceMember.getAllianceId() == alliance.getId()) {
        for (Alliance a : asr.getTopLevel()) {
%>
            [{v:'<%= a.getTag() %>', f:'<div style="color:black;"><%= a.getTag() %></div>'}, ''],
<%


            for (Alliance a2 : asr.getSubAlliancesFor(a.getId())) {
%>
            [{v:'<%= a2.getTag() %>', f:'<div style="color:black;"><%= a2.getTag() %></div>'}, '<%= a.getTag() %>'],
<%
            }
        }
        }
%>
        ]);
        var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
        chart.draw(data, {allowHtml:true});
      }
    </script>

<TABLE class="bluetrans" width="80%" align="center">
    <TR bgcolor="#708090">
    	<TD align="center" class="blue2">
    		<B><%= alliance.getName() %>&nbsp;[<%= alliance.getTag() %>]</B>
    	</TD>
    </TR>
<%
    if (asr.display() && (allianceMember.getAllianceId() == alliance.getId())) {
%>
    <TR bgcolor="#708090">
    	<TD align="center" class="blue2">
            <div id='chart_div'></div>
        </TD>
   </TR>
    <%
    }
    if ((alliance.getPicture() != null) && !alliance.getPicture().equalsIgnoreCase("")) {
    %>
    <TR>
    	<TD align="center" class="center">
    		<IMG id="bild" src="<%= alliance.getPicture() %>" />
    	</TD>
    </TR>
    <%
    }
    %>
    <TR>
    	<TD align="center">
    		<%= alliance.getDescription() %>
    	</TD>
    </TR>
    <TR>
        <TD class="blue" align="center">
            <B><%= ML.getMLStr("alliance_leadership_type_lbl",userId) %>: <%= ML.getMLStr(alliance.getLeadership().toString(),userId) %></B>
        </TD>
    </TR>
</TABLE>
   <TABLE class='blue' cellspacing=0 cellpadding=0 width="80%">
     <TR align="center">
                	<TD COLSPAN="4" bgcolor="#708090" width="100%" style="font-size:12px" class="blue2">
                        <B><%= ML.getMLStr("alliance_lbl_members", userId)%></B>
                	</TD>

     </TR>
    <TR>
        <TD class="blue2" width="*"><B><%= ML.getMLStr("alliance_lbl_name", userId)%></B></TD>
        <TD class="blue2" width="30%"><B><%= ML.getMLStr("alliance_lbl_status", userId)%></B></TD>
        <TD class="blue2" width="15%"><B><%= ML.getMLStr("alliance_lbl_action", userId)%></B></TD>
        <TD class="blue2" width="10%" align="center"><B><%= ML.getMLStr("alliance_lbl_online", userId)%></B></TD>
    </TR>
    <%

    ArrayList<AllianceMember> allianceMembers = AllianceService.findAllianceMemberByAllianceId(allianceMember.getAllianceId());

    for(AllianceMember tmpAllianceMember : allianceMembers){

    %>
    <TR>
        <TD style="font-size:12px"><%= AllianceService.findUserById(tmpAllianceMember.getUserId()).getGameName() %></TD>
        <TD style="font-size:12px">
            <%
            String userStatus = ML.getMLStr(AllianceRank.RANK_TRIAL, userId);
            if (!tmpAllianceMember.getIsTrial()) {
                userStatus = ML.getMLStr(AllianceRank.RANK_MEMBER, userId);
            }
            if (tmpAllianceMember.getIsCouncil()) {
                userStatus = ML.getMLStr(AllianceRank.RANK_COUNCIL, userId);
            }
            if (tmpAllianceMember.getIsAdmin()) {
                userStatus = ML.getMLStr(AllianceRank.RANK_ADMIN, userId);
            }
            if (tmpAllianceMember.getSpecialRankId() != 0) {
                AllianceRank ar = AllianceService.findAllianceRankBy(tmpAllianceMember.getSpecialRankId(), tmpAllianceMember.getAllianceId());
                userStatus = ar.getRankName();

            }

            HashSet<Integer> selectableRanks = AllianceService.getSelectableRank(allianceMember.getUserId(), tmpAllianceMember.getUserId());
            %>
        <%= userStatus %></TD>
        <TD style="font-size:12px">
            <A HREF="main.jsp?page=new/messages&targetUserId=<%=tmpAllianceMember.getUserId()%>&type=3"><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_sendmessage",userId) %>')" onmouseout="hideTip()" border=0 src="<%=GameConfig.getInstance().picPath()%>pic/message.jpg"/></A>
            <% if (!selectableRanks.isEmpty()) { %>
            <A HREF="main.jsp?page=new/alliance&type=<%= TYPE_EDITUSERRANK%>&targetUserId=<%= tmpAllianceMember.getUserId() %>"><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_edituserrank",userId) %>')" onmouseout="hideTip()" border=0 src="<%=GameConfig.getInstance().picPath()%>pic/edit.jpg"/></A>
            <% } if (allianceMember.getIsAdmin() && !tmpAllianceMember.getIsAdmin()){ %>
            <A HREF="main.jsp?page=new/alliance&type=<%= TYPE_DELETEUSER%>&targetUserId=<%= tmpAllianceMember.getUserId() %>"><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_kickuser",userId) %>')" onmouseout="hideTip()" border=0 src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg"/></A>
            <% } %>
        </TD>
        <TD style="font-size:12px" align="center">
            <%
            if (AllianceService.findUserById(tmpAllianceMember.getUserId()).getLastUpdate() >= (GameUtilities.getCurrentTick2() - 1)) { %>
            <IMG src="<%=GameConfig.getInstance().picPath()%>pic/online.jpg"/>
            <% } else { %>
            <FONT color="red"><B><%= (GameUtilities.getCurrentTick2() - AllianceService.findUserById(tmpAllianceMember.getUserId()).getLastUpdate()) %></B></FONT>
            <% } %>
        </TD>
    </TR>
    <%
    }
    %>
</TABLE>

<%
}
}

// Logger.getLogger().write("DEBUG 9");

/*
 * ###################
 * # Mitgliederansicht
 * ######################
 *
 * */

if(type == TYPE_SHOWSTRUCTURE){
%>

<%
if(AllianceService.findSubAlliancesOf(alliance).size() > 0 || alliance.getMasterAlliance() != alliance.getId()){
%>
<TABLE class='blue' cellspacing=0 cellpadding=0 width="80%">
    <TR>
        <TD ALIGN="center" class="blue2" COLSPAN="3" width="*"><B><%= ML.getMLStr("alliance_lbl_alliedalliances", userId)%></B></TD>
    </TR>
    <TR>
        <TD class="blue2" width="50%"><B><%= ML.getMLStr("alliance_lbl_alliancename", userId)%></B></TD>
        <TD class="blue2" width="25%"><B><%= ML.getMLStr("alliance_lbl_members", userId)%></B></TD>
        <TD class="blue2" width="25%"><B><%= ML.getMLStr("alliance_lbl_action", userId)%></B></TD>
      </TR>
    <%
    for(Alliance a : AllianceService.findAllAlliancesByMasterId(alliance.getMasterAlliance())){
            int asize = AllianceService.findAllianceMemberByAllianceId(a.getId()).size();
            alliancesSize += asize;

        %>
    <TR>
         <%  if(a.getId() == alliance.getId()){%>
         <TD><FONT COLOR="GREEN" STYLE="font-size:11px; font-weight:bold;"><%= a.getName() %></FONT></TD>
            <%}else{%>
         <TD style="font-size:12px"><B><A HREF="main.jsp?page=showAlliance&allianceid=<%= a.getId() %>"><%= a.getName() %></A></B></TD>
            <%}%>

        <TD style="font-size:12px"><%= asize%></TD>
        <%  if(a.getId() == alliance.getId()){%>
         <TD>-</TD>
            <%}else{%>
            <TD><%if (allianceMember.getIsAdmin() && a.getIsSubAllianceOf() == alliance.getId()){%><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_DELETETREATY %>&destAlliance=<%= a.getId() %>"><IMG onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_deletetreaty", userId)%>')" onmouseout="hideTip()" border=0 src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg"/><%}%></A></TD>
            <%}%>
    </TR>
    <% } %>
      <TR>
        <TD class="blue2"><%= ML.getMLStr("alliance_lbl_sum", userId)%>:</TD>
        <TD style="font-size:12px"><%= alliancesSize%></TD>
        <TD></TD>

    </TR>
</TABLE>
<BR/><BR/>
    <%
}
%>
<%
// Logger.getLogger().write("DEBUG 10");

if(AllianceService.findSubAlliancesOf(alliance).size() > 0 || alliance.getMasterAlliance() != alliance.getId()){
    // Logger.getLogger().write("DEBUG 11");
%>
 <TABLE  class='blue' BGCOLOR="#222222"  cellspacing=0 cellpadding=0 BORDER="0" width="80%">
<%

HashMap<Integer,Integer> allysColspan = AllianceService.getAllianceStructure(alliance);
HashMap<Integer, Integer> allysStructure = AllianceService.breadthFirstSearch(alliance);
HashMap<Point, Integer> allyPositions = new HashMap<Point, Integer>();
Alliance masterAlliance = AllianceService.findAllianceByAllianceId(alliance.getMasterAlliance());
Queue<Alliance> aqueue = new LinkedList<Alliance>();
int x = 0;
int y = 0;

aqueue.add(masterAlliance);

for(int i = 0; i < allysColspan.get(masterAlliance.getId()); i++){
allyPositions.put(new Point(x,y), masterAlliance.getId());
x++;
}
x = 0;
y++;
                    %>
                <TR>
                        <TD  class="blue2" ALIGN ="CENTER" COLSPAN="<%= allysColspan.get(masterAlliance.getId())%>"><B><%= ML.getMLStr("alliance_lbl_alliancetreaties", userId)%></B></TD></TR>
                <TR>
               <TR>
<% if(masterAlliance.getId() != alliance.getId())
{
%>
               <TD BGCOLOR="BLACK"  ALIGN ="CENTER" COLSPAN="<%= allysColspan.get(masterAlliance.getId())%>">
                  <A HREF="main.jsp?page=showAlliance&allianceid=<%= masterAlliance.getId() %>">
                      <FONT style="font-size:13"><%= masterAlliance.getTag()%></FONT>
                  </A>
               </TD>
               <%
            }else{
                   %>
               <TD BGCOLOR="BLACK"  ALIGN ="CENTER" COLSPAN="<%= allysColspan.get(masterAlliance.getId())%>">
                       <FONT COLOR="GREEN" STYLE="font-size:13px; font-weight:bold;"><%= masterAlliance.getTag()%></FONT>
               </TD>
                <%
                }%>
                  <TR>
            <%
        while(aqueue.size() > 0){
            Alliance a = aqueue.poll();

            ArrayList<Alliance> childs =  AllianceService.findSubAlliancesOf(a);

            for(int i = 0; i < childs.size(); i++){

                Alliance atemp = childs.get(i);
                //Check if Father is really the father
                if(atemp.getIsSubAllianceOf() == allyPositions.get(new Point(x,(y-1)))){

                     if(atemp.getId() != alliance.getId()){
                      %>
                        <TD BGCOLOR="BLACK"  ALIGN ="CENTER" COLSPAN="<%= allysColspan.get(atemp.getId())%>">
                           <A HREF="main.jsp?page=showAlliance&allianceid=<%= atemp.getId() %>">
                                <FONT style="font-size:13"><%= atemp.getTag()%></FONT>
                           </A>
                        </TD>
                       <%
                      }else{
                       %>
                         <TD BGCOLOR="BLACK"  ALIGN ="CENTER" COLSPAN="<%= allysColspan.get(atemp.getId())%>">
                               <FONT COLOR="GREEN" STYLE="font-size:13px; font-weight:bold;"><%= atemp.getTag()%></FONT>
                          </TD>
                       <%
                      }
                        aqueue.add(atemp);
                        for(int k = 0; k < allysColspan.get(atemp.getId()); k++){
                                allyPositions.put(new Point(x,y), atemp.getId());
                                x++;
                            }
                 }else{
                        %>
                        <TD></TD>
                        <%
                        x++;
                        i--;
                 }

            }
            x = 0;
           if(aqueue.size() > 0){
               if(allysStructure.get(aqueue.peek().getId()) != allysStructure.get(a.getId())){
                   y++;
                   %>
                   <TR>
                   <%
                  }
              }
        }
%>
 </TABLE>

<%
    // Logger.getLogger().write("DEBUG 12");

    }
}

if (type == TYPE_CHANGELEADERSHIP) {
%>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_CHANGELEADERSHIP %>">
    <TABLE width="250" align="center">
        <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_changeleadership", userId) %><BR><BR></TD></TR>
            <TR><TD align="center"><INPUT align="middle" type="radio" name="newLeadership" value="<%= ELeadership.DEMOCRATIC %>" /></TD><TD><%= ML.getMLStr("alliance_opt_leadership_democratic", userId) %><BR></TD></TR>
            <TR><TD align="center"><INPUT align="middle" type="radio" name="newLeadership" value="<%= ELeadership.ARISTOCRATIC %>" /></TD><TD><%= ML.getMLStr("alliance_opt_leadership_aristocratic", userId) %><BR></TD></TR>
            <TR><TD align="center"><INPUT align="middle" type="radio" name="newLeadership" value="<%= ELeadership.DICTORIAL %>" /></TD><TD><%= ML.getMLStr("alliance_opt_leadership_dictorial", userId) %><BR></TD></TR>
            <TR><TD align="center" colspan="2">&nbsp;<BR></TD></TR>
        <TR><TD align="center" colspan=2><input value="&Auml;ndern" type=submit></TD></TR>
    </TABLE>
</FORM>
<%
}
// Logger.getLogger().write("DEBUG 13");

if(type == TYPE_JOINALLIANCE){
         String preTag = null;
        if (request.getParameter("tag") != null) {
            preTag = request.getParameter("tag");
        }
%>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_JOINALLIANCE %>">
    <TABLE width="80%">
        <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_joinmessage", userId) %><BR><BR></TD></TR>
        <%          if (preTag != null) { %>
        <TR><TD><%= ML.getMLStr("alliance_lbl_name", userId) %></TD><TD><input value="<%= preTag %>" type="text" name="aname" size=20 maxlength="30"></TD></TR>
        <%          } else { %>
        <TR><TD><%= ML.getMLStr("alliance_lbl_name", userId) %></TD>
            <TD>
                <SELECT name="aname">
                    <% for(Alliance a : AllianceService.findAllAlliances()) { %>
                    <OPTION value="<%= a.getName() %>"><%= a.getName()%></OPTION>
                    <% } %>
                </SELECT>
            </TD>
        </TR>
        <%          } %>
        <TR><TD valign="top"><%= ML.getMLStr("alliance_lbl_message", userId) %></TD><TD><textarea name="message" rows=6 cols=40></textarea><BR><BR></TD></TR>
        <TR><TD align="center" colspan=2><input value="Anfrage stellen" type=submit></TD></TR>
    </TABLE>
</FORM>
<%
}

// Logger.getLogger().write("DEBUG 14");
/*
 * ###################
 * # Leave Alliance
 * ######################
 *
 * */
if(type == TYPE_EDITUSERRANK){
    // &Uuml;berpr&uuml;fen ob Aufrufender User Admin oder Konzil ist -- falls NEIN keine Kick m&ouml;glich
// &Uuml;berpr&uuml;fen ob dieser User Trial ist -- falls JA instant kick m&ouml;glich
// Falls normales Mitglied ebenfalls sofortkick
// Falls Konzil oder Admin wird eine Abstimmung &uuml;ber das gesamte Konzil gestartet (einfache Mehrheit - Admin 2/3)


        ArrayList<AllianceRank> allianceRanks = AllianceService.findAllianceRankByAllianceId(allianceMember.getAllianceId());
        AllianceMember destUser = AllianceService.findAllianceMemberByUserId(targetUserId);

%>
<BR>
<CENTER><form method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_SETUSERRANK%>&targetUserId=<%=targetUserId%>">
    <TABLE CLASS="blue" width="80%" style="font-size:13px">
        <TR  CLASS="blue2"><TD width="150"><B><%= ML.getMLStr("alliance_lbl_name", userId)%>:</B></TD>
                            <TD width="*"><%= AllianceService.findUserById(destUser.getUserId()).getGameName() %></TD>
        </TR>
        <%
        String userStatus = ML.getMLStr(AllianceRank.RANK_TRIAL, userId);
        if (!destUser.getIsTrial()) {
            userStatus = ML.getMLStr(AllianceRank.RANK_MEMBER, userId);
        }
        if (destUser.getIsCouncil()) {
            userStatus = ML.getMLStr(AllianceRank.RANK_COUNCIL, userId);
        }
        if (destUser.getIsAdmin()) {
            userStatus = ML.getMLStr(AllianceRank.RANK_ADMIN, userId);
        }
        %>
        <TR CLASS="blue2"><TD><B><%= ML.getMLStr("alliance_lbl_currentstatus", userId)%></B></TD><TD><%= userStatus %></TD></TR>
        <TR><TD></TD><TD>
            <%
            HashSet<Integer> possibleSelections = AllianceService.getSelectableRank(userId, destUser.getUserId());

            if (possibleSelections.contains(AllianceRank.RANK_TRIAL_INT)) {
            %>
                <TABLE  CLASS="blue" width="100%" style="font-size:13px">
                    <TR CLASS="blue2"><TD COLSPAN="2"><%= ML.getMLStr("alliance_lbl_trialranks", userId)%></TD></TR>
                <TR><TD width="30%"><%= ML.getMLStr("alliance_lbl_trialstandard", userId)%></TD><TD><input align="middle" type="radio" name="Status" value="1"/></TD></TR>
<%
                for (AllianceRank ar : allianceRanks) {
                    if (ar.getComparisonRank() == AllianceRank.RANK_TRIAL_INT) { %>
                        <TR><TD width="30%"><%= ar.getRankName() %></TD><TD><input align="middle" type="radio" name="Status" value="x<%= ar.getRankId() %>"/></TD></TR>
<%                  }
                }
            }

            if (possibleSelections.contains(AllianceRank.RANK_MEMBER_INT)) {
%>
                </TABLE>
                <TABLE  CLASS="blue" width="100%" style="font-size:13px">
                <TR CLASS="blue2"><TD COLSPAN="2"><%= ML.getMLStr("alliance_lbl_memberranks", userId)%></TD></TR>
                    <TR><TD width="30%"><%= ML.getMLStr("alliance_lbl_memberstandard", userId)%></TD><TD><input align="middle" type="radio" name="Status" value="2"/></TD></TR>
<%
                for (AllianceRank ar : allianceRanks) {
                    if (ar.getComparisonRank() == AllianceRank.RANK_MEMBER_INT) { %>
                          <TR><TD width="30%"><%= ar.getRankName() %></TD><TD><input align="middle" type="radio" name="Status" value="x<%= ar.getRankId() %>"/></TD></TR>
<%                  }
                }
            }

            if (possibleSelections.contains(AllianceRank.RANK_COUNCIL_INT)) {
%>
                </TABLE>
                <TABLE CLASS="blue" width="100%" style="font-size:13px">
                <TR CLASS="blue2"><TD COLSPAN="2"><%= ML.getMLStr("alliance_lbl_councilranks", userId)%></TD></TR>
                    <TR><TD width="30%"><%= ML.getMLStr("alliance_lbl_councilstandard", userId)%></TD><TD><input align="middle" type="radio" name="Status" value="3"/></TD></TR>
<%
                for (AllianceRank ar : allianceRanks) {
                    if (ar.getComparisonRank() == AllianceRank.RANK_COUNCIL_INT) { %>
                        <TR><TD width="30%"><%= ar.getRankName() %></TD><TD><input align="middle" type="radio" name="Status" value="x<%= ar.getRankId() %>"/></TD></TR>
<%
                    }
                }

%>
               </TABLE>
               <%
            }

            if (possibleSelections.contains(AllianceRank.RANK_ADMIN_INT)) {
               %>
               <TABLE CLASS="blue" width="100%" style="font-size:13px">
                    <TR CLASS="blue2"><TD COLSPAN="2"><%= ML.getMLStr("alliance_lbl_adminranks", userId)%></TD></TR>
                    <TR><TD width="30%"><%= ML.getMLStr("alliance_lbl_adminstandard", userId)%></TD><TD><input align="middle" type="radio" name="Status" value="4"/></TD></TR>
<%
                for (AllianceRank ar : allianceRanks) {
                    if (ar.getComparisonRank() == AllianceRank.RANK_ADMIN_INT) { %>
                        <TR><TD width="30%"><%= ar.getRankName() %></TD><TD><input align="middle" type="radio" name="Status" value="x<%= ar.getRankId() %>"/></TD></TR>
<%                  }
                }
            }
%>
</TABLE></TD></TR>
    <%

    %>
    <TR><TD align="center" colspan=2><P><BR><INPUT value="Status setzen" type=submit></TD></TR>
    <TR><TD colspan=2 align="center"><A onClick="javascript:history.go(-1)"><B><BR>Zur&uuml;ck</B></A></TD></TR>
    </TABLE>
</form>
</CENTER>
<%




    }

// Logger.getLogger().write("DEBUG 15");
/*
 * ###################
 * # Leave Alliance
 * ######################
 *
 * */
if(type == TYPE_LEAVEALLIANCE){

    %>

<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_LEAVEALLIANCE%>">
    <TABLE width="80%">
    <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_leavewarning", userId)%><BR/><BR/></TD></TR>
    <TR><TD align="center" colspan=2><input value="<%= ML.getMLStr("alliance_but_leavealliance", userId)%>" type=submit></TD></TR>
    </TABLE>
</FORM>
<%
}

// Logger.getLogger().write("DEBUG 16");
    /*
 * ###################
 * # Profil Editieren
 * ######################
 *
 * */
if(type == TYPE_EDITPROFILE){

%>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_SAVEPROFILE%>">
    <TABLE width="80%">
        <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_create",userId)%><P><BR></TD></TR>
        <TR><TD><%= ML.getMLStr("alliance_lbl_name",userId)%></TD><TD><INPUT type="text" name="aname" size=20 maxlength="30" value="<%= alliance.getName()%>" /></TD></TR>
        <TR><TD><%= ML.getMLStr("alliance_lbl_tag",userId)%></TD><TD><INPUT type="text" name="atag" size=10 maxlength="10" value="<%= alliance.getTag()%>" /></TD></TR>
        <TR><TD><%= ML.getMLStr("alliance_lbl_image",userId)%></TD><TD><INPUT type="text" name="apic" size=30 maxlength="100" value="<%= alliance.getPicture()%>" /></TD></TR>
        <TR><TD><%= ML.getMLStr("alliance_lbl_description",userId)%><BR>HTML erlaubt</TD><TD><TEXTAREA name="desc" rows=12 cols=55><%= alliance.getDescription()%></TEXTAREA></TD></TR>
        <TR><TD align="center" colspan=2><P><BR><input value="<%= ML.getMLStr("alliance_but_update", userId)%>" type=submit></TD></TR>
    </TABLE>
</FORM>
<%
}

/*
 * ###################
 * # Allianz erstellen
 * ######################
 *
 * */

// Logger.getLogger().write("DEBUG 17");
if(type == TYPE_CREATEALLIANCE){
    if(AllianceService.findAllianceMemberByUserId(userId) != null){

    %>
        <TABLE width="80%">
            <TR>
                <TD ALIGN="CENTER"><B><FONT COLOR="RED"><%= ML.getMLStr("alliance_err_alreadyinalliance", userId)%></FONT></B></TD>
            </TR>
        </TABLE>
    <%
    }else{
    %>

<FORM method="post" action="main.jsp?page=new/alliance&process=<%=PROCESS_CREATEALLIANCE%>">
    <TABLE width="80%">
        <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_create",userId)%><P/><BR/></TD></TR>
        <TR><TD><%= ML.getMLStr("alliance_lbl_name",userId)%></TD><TD><input type="text" name="aname" size=20 maxlength="30"></TD></TR>
        <TR><TD><%= ML.getMLStr("alliance_lbl_tag",userId)%></TD><TD><input type="text" name="atag" size=10 maxlength="10"></TD></TR>
        <TR><TD><%= ML.getMLStr("alliance_lbl_image",userId)%></TD><TD><input type="text" name="apic" size=30 maxlength="100"></TD></TR>
        <TR><TD><%= ML.getMLStr("alliance_lbl_description",userId)%></TD><TD><textarea name="desc" rows=6 cols=40></textarea></TD></TR>
        <TR><TD align="center" colspan=2><P/><BR><input value="<%= ML.getMLStr("alliance_but_create", userId)%>" type=submit></TD></TR>
    </TABLE>
</FORM>
<%
    }
}

/*
 * ###################
 * # Allianznachricht erstellen
 * ######################
 *
 * */
// Logger.getLogger().write("DEBUG 18");
if(type == TYPE_CREATEMESSAGE){
    boolean test = true;
if(test){
    %>
 <jsp:include page="alliance-board.jsp">
                        <jsp:param name="boardId" value='<%= boardId %>' />
 </jsp:include>
<%
}else{
%>
        <FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_CREATEALLIANCEMESSAGE%>">
            <TABLE width="80%" style="font-size:13px">
                <TR><TD align="center" colspan=4><%= ML.getMLStr("alliance_lbl_htmlallowed",userId)%><P><BR></TD></TR>
                <TR>
                    <TD><%= ML.getMLStr("alliance_lbl_topic",userId)%></TD><TD><input type="text" name="topicAMessage" size=50 maxlength="50"></TD>
                    <TD><%= ML.getMLStr("alliance_lbl_allianceboard",userId)%>:
                        <SELECT NAME="boardId">
                            <OPTION <% if(boardId == AllianceBoardService.alliance_board_internal.getId()){%> SELECTED <%}%>
                                VALUE="<%=AllianceBoardService.alliance_board_internal.getId()%>">
                                <%= ML.getMLStr(AllianceBoardService.alliance_board_internal.getName(), userId)%>
                            </OPTION>
                             <% if(AllianceService.findAllAlliancesByMasterId(alliance.getMasterAlliance()).size() > 1){
                            %>
                            <OPTION <% if(boardId == AllianceBoardService.alliance_board_structure.getId()){%> SELECTED <%}%>
                                VALUE="<%=AllianceBoardService.alliance_board_structure.getId()%>">
                                <%= ML.getMLStr(AllianceBoardService.alliance_board_structure.getName(), userId)%>
                            </OPTION>
                            <%}
                            %>
                            <% for(AllianceBoard ab : AllianceService.findAllianceBoardByAllianceId(alliance.getId())){ %>
                            <OPTION <% if(boardId == ab.getId()){%> SELECTED <%}%>
                            VALUE="<%=ab.getId()%>"><%= ab.getName()%></OPTION>
                            <%}%>
                        </SELECT>
                    </TD>

                </TR>
                <TR><TD><%= ML.getMLStr("alliance_lbl_message",userId)%></TD><TD><p><textarea rows="4" name="message" cols="38"></textarea></p></TD></TR>
                <TR><TD align="center" colspan=4><P><BR><input value=<%= ML.getMLStr("alliance_but_submit",userId)%> type=submit></TD></TR>
            </TABLE>
        </FORM>
<%
}
}
/*
 * ###################
 * # Allianznachrichten anzeigen
 * ######################
 *
 * */

// Logger.getLogger().write("DEBUG 19");
if(type == TYPE_SHOWALLIANCEBOARD){

    %>
 <jsp:include page="alliance-board.jsp">

       <jsp:param name="type" value='<%= type %>' />
       <jsp:param name="boardId" value='<%= boardId %>' />
 </jsp:include>
<%
}
/*
 * #################
 * # Allianzb�ndniss eingehen
 * ####################
 * */

// Logger.getLogger().write("DEBUG 20");
    if(type == TYPE_CREATETREATY){
        // Logger.getLogger().write("DEBUG 20a");
%>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_CREATETREATY%>">
    <TABLE width="80%">
    <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_createtreaty", userId)%><BR><BR></TD></TR>
    <TR><TD><%= ML.getMLStr("alliance_lbl_name", userId)%></TD>
        <TD><SELECT name="aname">
                <%
                // Logger.getLogger().write("DEBUG 20b");
                for(Alliance a : AllianceService.findJoinableAlliances(alliance.getId())){
                    // Logger.getLogger().write("DEBUG 20c - " + a.getName());
                    if(a.getId() == alliance.getId()) continue;
                %>
                <OPTION value="<%= a.getId() %>"><%= a.getName()%></OPTION>
                <%
                    // Logger.getLogger().write("DEBUG 20d");
                }
                %>
            </SELECT>
        </TD></TR>
        <TR><TD valign="top"><%= ML.getMLStr("alliance_lbl_message", userId)%></TD><TD><TEXTAREA name="message" rows=6 cols=40></TEXTAREA><BR><BR></TD></TR>
        <TR><TD align="center" colspan=2><INPUT value="<%= ML.getMLStr("alliance_but_submitrequest", userId)%>" type=submit /></TD></TR>
    </TABLE>
</FORM>
<%
}
/*
 * #################
 * # Allianzb�ndniss verlassen
 * ####################
 * */
// Logger.getLogger().write("DEBUG 21");

if(type == TYPE_LEAVETREATY){
%>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_LEAVETREATY%>">
    <TABLE width="80%">
    <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_leavetreaty", userId)%><BR><BR></TD></TR>
    <TR><TD align="center" colspan=2><input value="<%= ML.getMLStr("alliance_but_leave", userId)%>" type=submit></TD></TR>
    </TABLE>
</FORM>

    <%
    }
/*
 * #################
 * # User kicken
 * ####################
 * */
// Logger.getLogger().write("DEBUG 22");
        if(type == TYPE_DELETEUSER){
%>
<BR><BR>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_DELETEUSER%>&targetUserId=<%= targetUserId%>">
    <TABLE width="80%">
        <TR><TD><%= ML.getMLStr("alliance_lbl_member", userId)%></TD><TD><%= AllianceService.findUserById(targetUserId).getGameName()%></TD></TR>
    <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_deletuser", userId)%><BR><BR></TD></TR>
    <TR><TD align="center" colspan=2><input value="<%= ML.getMLStr("alliance_but_kickuser", userId)%>" type=submit></TD></TR>
    </TABLE>
</FORM>

    <%
    }
/*
 * #################
 * # Allianz l�schen
 * ####################
 * */
// Logger.getLogger().write("DEBUG 23");
   if(type == TYPE_DELETEALLIANCE){
%>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_DELETEALLIANCE%>">
    <TABLE width="80%">
    <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_leavealliance", userId)%><BR><BR></TD></TR>
    <TR><TD align="center" colspan=2><INPUT value="<%= ML.getMLStr("alliance_but_disbandalliance", userId) %>" type=submit></TD></TR>
    </TABLE>
</FORM>
    <%
    }
/*
 * #################
 * # R�nge editieren
 * ####################
 * */
// Logger.getLogger().write("DEBUG 24");
 if(type == TYPE_EDITRANKS){

%>
        <FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_EDITRANKS%>">
            <TABLE width="80%" style="font-size:13px">
                <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_ranks", userId)%><P><BR></TD></TR>
                <TR><TD><%= ML.getMLStr("alliance_lbl_rankname", userId)%></TD><TD><INPUT type="text" name="rankName" size=20 maxlength="50"></TD></TR>
                <TR><TD><%= ML.getMLStr("alliance_lbl_equalrank", userId)%></TD><TD>
                    <SELECT id="compRank" name="compRank">
                        <OPTION value="<%= AllianceRank.RANK_ADMIN_INT %>"><%= ML.getMLStr("alliance_lbl_rank_admin", userId)%></OPTION>
                        <OPTION value="<%= AllianceRank.RANK_COUNCIL_INT %>"><%= ML.getMLStr("alliance_lbl_rank_council", userId)%></OPTION>
                        <OPTION value="<%= AllianceRank.RANK_MEMBER_INT %>" selected><%= ML.getMLStr("alliance_lbl_rank_member", userId)%></OPTION>
                        <OPTION value="<%= AllianceRank.RANK_TRIAL_INT %>"><%= ML.getMLStr("alliance_lbl_rank_trial", userId)%></OPTION>
                    </SELECT>
                </TD></TR>
                <TR><TD align="center" colspan=2><P><BR><input value="<%= ML.getMLStr("alliance_but_addrank", userId)%>" type="submit"></TD></TR>
            </TABLE>
        </FORM>
    <%

        ArrayList<AllianceRank> ranks = AllianceService.findAllianceRankByAllianceId(alliance.getId());
        if (ranks.size() > 0) {
%>
        <TABLE class="blue" width="80%" style="font-size:13px">
            <TR><TD class="blue" colspan=3 align="center"><B><%= ML.getMLStr("alliance_lbl_alliancespecificranks",userId) %></B></TD></TR>
            <TR><TD class="blue"><B>Name</B></TD><TD class="blue"><B><%= ML.getMLStr("alliance_lbl_equalrank",userId) %></B></TD><TD class="blue">&nbsp;</TD>
<%
            for (AllianceRank ar : ranks) { %>
            <TR><TD><%= ar.getRankName() %></TD><TD><%= ML.getMLStr(AllianceRank.getComparisonRankName(ar.getComparisonRank()),userId) %></TD><TD><IMG alt="Rang l&ouml;schen" src="<%= GameConfig.getInstance().picPath() %>pic/cancel.jpg" onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_deleterank",userId) %>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=new/alliance&rankId=<%= ar.getRankId() %>&process=<%= PROCESS_DELETERANK%>'"/></TD></TR>
<%          }
%>
        </TABLE>
<%
        }
  }
/*
 * #################
 * # Nachrichten
 * ####################
 * */
// Logger.getLogger().write("DEBUG 25");
if(type == TYPE_MESSAGES){
%>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_SENDMESSAGES%>">
    <TABLE style="font-size:13px" width="80%">
        <TR><TD align="center" colspan=2><%= ML.getMLStr( "alliance_msg_chooserecipient", userId)%><BR><BR></TD></TR>
        <TR><TD valign="top"><%= ML.getMLStr( "alliance_lbl_receiver", userId)%></TD><TD>
                <select id="receiver" name="receiver" onchange="setANameField();">
                    <option value="0"><%= ML.getMLStr( "alliance_lbl_wholealliance", userId)%></option>
                    <option value="1"><%= ML.getMLStr( "alliance_lbl_ownalliance", userId)%></option>
                    <option value="2"><%= ML.getMLStr( "alliance_lbl_owncouncil", userId)%></option>
                    <option value="3"><%= ML.getMLStr( "alliance_lbl_allcouncilsadmins", userId)%></option>
                    <option value="4"><%= ML.getMLStr( "alliance_lbl_alladmins", userId)%></option>
                    <option value="5"><%= ML.getMLStr( "alliance_lbl_specificalliance", userId)%></option>
                    <option value="6"><%= ML.getMLStr( "alliance_lbl_adminsofalliance", userId)%></option>
                    <option value="7"><%= ML.getMLStr( "alliance_lbl_councilsadminsalliance", userId)%></option>
                </select><P><%= ML.getMLStr("alliance_lbl_specifyalliance", userId)%><input type="text" id="aname"  name="aname" size=20 maxlength="30"><BR><BR>
        </TD></TR>
        <TR><TD><%= ML.getMLStr( "alliance_lbl_topic", userId)%></TD><TD><input type="text" name="topic" size=20 maxlength="30"></TD></TR>
        <TR><TD><%= ML.getMLStr( "alliance_lbl_message", userId)%></TD><TD><textarea name="comment" rows=6 cols=40></textarea></TD></TR>
        <TR><TD align="center" colspan=2><input value="<%= ML.getMLStr("alliance_but_submit", userId)%>" type=submit></TD></TR>
    </TABLE>
</FORM>
    <%
    }
/*
 * #################
 * # B�ndniss verlassen
 * ####################
 * */
// Logger.getLogger().write("DEBUG 26");
 if(type == TYPE_DELETETREATY){

int destAlliance = Integer.parseInt(request.getParameter("destAlliance"));
%>
<FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_DELETETREATY %>&destAlliance=<%= destAlliance %>">
    <TABLE width="80%">
        <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_deletetreaty" , userId)%><BR><BR></TD></TR>
        <TR><TD align="center" colspan=2><input value="<%= ML.getMLStr("alliance_but_deletetreaty" , userId)%>" type=submit></TD></TR>
    </TABLE>
</FORM>
    <%
    }

 if(type == TYPE_RECRUITING){
%>
        <FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_SWITCHRECRUITING%>">
            <TABLE width="80%">
                <TR><TD align="center" colspan=2><%= ML.getMLStr("alliance_msg_recruiting", userId)%><BR><BR></TD></TR>
<%
                if (alliance.getIsRecruiting()) {
%>
                    <TR><TD align="center" colspan=2><INPUT value="<%= ML.getMLStr("alliance_but_recruitingoff", userId)%>" type=submit></TD></TR>
<%
                } else {
%>
                    <TR><TD align="center" colspan=2><INPUT value="<%= ML.getMLStr("alliance_but_recruitingon", userId)%>" type=submit></TD></TR>
<%
                }
%>
            </TABLE>
        </FORM>
<%
}

/*
 * #################
 * # Allianzbrett erstellen
 * ####################
 * */
// Logger.getLogger().write("DEBUG 27");
if(type == TYPE_CREATEALLIANCEBOARD){
%>
        <FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_CREATEALLIANCEBOARD%>">
            <TABLE width="80%" style="font-size:13px">
                <TR><TD colspan="2"><%= ML.getMLStr("alliance_lbl_name",userId)%></TD><TD><input type="text" name="boardName" size=50 maxlength="50"></TD></TR>
                <TR><TD><%= ML.getMLStr("alliance_lbl_type",userId)%></TD>
                    <TD>
                        <IMG border="0" alt="techtree" onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_boarddesc", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.getInstance().picPath()%>pic/infoT.png" />
                    </TD>
                    <TD>
                        <SELECT name="boardType">
                            <% for(EAllianceBoardType boardType : EAllianceBoardType.values()){ %>
                                <OPTION value="<%= boardType.toString() %>"><%= ML.getMLStr("alliance_opt_" + boardType.toString(), userId) %></OPTION>
                            <% } %>
                        </SELECT>
                    </TD></TR>
                <TR><TD colspan="2"><%= ML.getMLStr("alliance_lbl_descriptionallianceboard",userId)%></TD><TD><p><textarea rows="9" name="boardDescription" cols="48"></textarea></p></TD></TR>
                <TR><TD align="center" colspan=2><P><BR><input value=<%= ML.getMLStr("alliance_but_create",userId)%> type=submit></TD></TR>
            </TABLE>
        </FORM>
<%

}
/*
 * #################
 * # Allianzbretter
 * ####################
 * */
// Logger.getLogger().write("DEBUG 28");
if(type == TYPE_MANAGEALLIANCEBOARD){
%>
<BR>
<TABLE><TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%=TYPE_CREATEALLIANCEBOARD%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_createallianceboard",userId)%></A></TD></TR></TABLE>
<BR>
<%

boolean opened = false;
if(AllianceService.findAllianceBoardByAllianceId(alliance.getId()).size() > 0){
    opened = true;
    %>
    <TABLE width ="80%">
        <TR>
        <TD ALIGN="CENTER" CLASS="blue2" COLSPAN="4"><%= ML.getMLStr("alliance_lbl_existingallianceboards", userId)%></TD>
        </TR>
        <TR>
            <TD ALIGN="CENTER" CLASS="blue2"><%= ML.getMLStr("alliance_lbl_type", userId)%></TD>
            <TD ALIGN="CENTER" CLASS="blue2"><%= ML.getMLStr("alliance_lbl_name", userId)%></TD>
            <TD ALIGN="CENTER" CLASS="blue2"><%= ML.getMLStr("alliance_lbl_descriptionallianceboard", userId)%></TD>
            <TD ALIGN="CENTER" CLASS="blue2"><%= ML.getMLStr("alliance_lbl_action", userId)%></TD>
        </TR>
    <%
    }
    for(AllianceBoard ab : AllianceService.findAllianceBoardByAllianceId(alliance.getId())){
        %>
        <TR>
            <TD width="20%"><FONT STYLE="font-size:13px;">[<%= ML.getMLStr("alliance_opt_" + ab.getType().toString(), userId) %>] </FONT></TD>
            <TD width="20%"><FONT STYLE="font-size:13px;"><%= ab.getName()%></FONT></TD>
            <TD width="50%"><FONT STYLE="font-size:13px;"><%= ab.getDescription()%></FONT></TD>
            <TD width="10%"><A href='main.jsp?page=new/alliance&process=<%= PROCESS_DELETEALLIANCEBOARD %>&boardId=<%= ab.getId()%>'><IMG border='0' onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_deleteallianceboard",userId) %>')" onmouseout="hideTip()" src='<%= GameConfig.getInstance().picPath() + "pic/cancel_message.gif"%>' width='18' height='18'></A>
                <% if(ab.getType().equals(EAllianceBoardType.CUSTOM)){ %>
                <A href='main.jsp?page=new/alliance&type=<%= TYPE_SETBOARDPERMISSIONS %>&boardId=<%= ab.getId()%>'><IMG border='0' onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_deleteallianceboard",userId) %>')" onmouseout="hideTip()" src='<%= GameConfig.getInstance().picPath() + "pic/edit.jpg"%>' width='18' height='18'></A>
                <% } %>
            </TD>
        </TR>
        <%
     }

    if(opened){
%>
    </TABLE>
<%
    }
}

/*
 * ##############
 * #LINKS
 * ###########
 * */
// Logger.getLogger().write("DEBUG 29");
if(type != TYPE_SHOWALLIANCEBOARD && type != TYPE_CREATEMESSAGE){
%>
<P><TABLE class="blue" width="80%">
    <TR bgcolor="#708090" align="center"><TD class="blue2"><B><%= ML.getMLStr("alliance_link_possibleactions", userId)%></B></TD></TR>
    <%
    if ((alliance != null) && allianceMember.getAllianceId() == alliance.getId()) {
    %>
    <!-- �bersicht anzeigen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_SHOWDESCRIPTION%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_showdescription", userId)%></A></TD></TR>
    <%
    }
    if (alliance != null || AllianceBoardService.hasAllianceBoardPermissions(userId)) {
    %>
    <!-- Allianznachricht erstellen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%=TYPE_CREATEMESSAGE%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_createalliancemessage",userId)%></A></TD></TR>
    <%
    }
    %>
    <%
    if (alliance != null || AllianceBoardService.hasAllianceBoardPermissions(userId)) {
        if (!AllianceBoardService.newMessagesAvailable(userId)) {
        %>
        <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_SHOWALLIANCEBOARD%>&from=<%=from%>&to=<%=to%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_readallianceboard", userId)%></A></TD></TR>
    <%  } else { %>
            <TR align="center"><TD>
            <A HREF="main.jsp?page=new/alliance&type=<%= TYPE_SHOWALLIANCEBOARD%>&from=<%=from%>&to=<%=to%>">
            <FONT color="#FF0000" style="font-size:12px"><%= ML.getMLStr("alliance_link_readallianceboardnew", userId)%></FONT>
            </A></TD></TR>
    <%
    }
    }
    %>
    <%
    if (alliance == null) {
    %>
    <!-- Allianz erstellen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_CREATEALLIANCE%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_create",userId)%></A></TD></TR>
    <%
    }
    %>
    <%
    if (alliance == null) {
    %>
    <!-- Rekrutierende Allianzen anzeigen -->
    <TR align="center"><TD><A HREF="main.jsp?page=showAlliance&type=1" style="font-size:12px"><%= ML.getMLStr("alliance_link_showrecruiting",userId)%></A></TD></TR>
    <%
    }
    if(false){
   // if ((alliance != null) && allianceMember.getAllianceId() == alliance.getId()) {
    %>
    <!-- Mitglieder anzeigen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_SHOWMEMBERS%>" style="font-size:12px"><%= ML.getMLStr("alliance_lbl_members", userId)%></A></TD></TR>
    <%
    }
    if ((alliance != null) && allianceMember.getAllianceId() == alliance.getId()) {
    %>
    <!-- Allianz Struktur anzeigen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_CHANGELEADERSHIP %>" style="font-size:12px"><%= ML.getMLStr("alliance_link_changeleadership", userId)%></A></TD></TR>
    <%
    }
    %>
    <TR><TD ALIGN="center">-------------------</TD></TR>
    <%
    if ((alliance != null) &&  allianceMember.getIsAdmin() && (alliance.getIsSubAllianceOf() <= 0)) {
    %>
    <!-- B�ndniss erstellen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_CREATETREATY%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_createtreaty",userId)%></A></TD></TR>
    <%
    }
    %>
    <%
    if ((alliance != null) && allianceMember.getAllianceId()==alliance.getId() && allianceMember.getIsAdmin() && (alliance.getIsSubAllianceOf() > 0)) {
    %>
    <!-- B�ndniss aufl�sen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_LEAVETREATY%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_canceltreaty",userId)%></A></TD></TR>
    <%
    }
    %>
    <%
    if (alliance == null) {
        ArrayList<Voting> existingVotes = AllianceService.getExistingVotes(userId);
        if(existingVotes.size() == 0){
    %>
    <!-- Allianz beitreten -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_JOINALLIANCE %>" style="font-size:12px"><%= ML.getMLStr("alliance_link_joinalliance",userId)%></A></TD></TR>
    <%
        }else{
            %>
    <!-- Allianz beitreten -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_JOINALLIANCE %>" style="font-size:12px"><%= ML.getMLStr("alliance_link_joinalliance",userId)%></A> (<%= existingVotes.size() %> Abstimmungen ausstehend)</TD></TR>
            <%
        for(Voting v : existingVotes){
        %>
        <TR><TD align="center"><%= AllianceService.getAllianceFromVote(v.getVoteId()).getName() %> [<%= AllianceService.getAllianceFromVote(v.getVoteId()).getTag() %>]</TD></TR>
        <%
        }
        }
    }
    %>
    <%
    if (alliance != null) {
    %>
    <!-- Nachrichten -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_MESSAGES%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_messages",userId)%></A></TD></TR>
    <%
    }
    %>
    <%
    if ((alliance != null) && allianceMember.getIsAdmin()) {
    %>
    <!-- Profil editieren -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_EDITPROFILE%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_editprofile",userId)%></A></TD></TR>
    <%
    }
    %>
    <%
    if ((alliance != null) && allianceMember.getIsAdmin()) {
    %>

    <!-- R�nge definieren -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_EDITRANKS%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_defineranks",userId)%></A></TD></TR>
    <%
    }
    %>
    <%
    if ((alliance != null) && allianceMember.getIsAdmin()) {
    %>
    <!-- Rektrutierung -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%=TYPE_RECRUITING%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_recruiting",userId)%> << <% if (!alliance.getIsRecruiting()){%><%= ML.getMLStr("alliance_link_recruitingon",userId)%><%}else{%><%= ML.getMLStr("alliance_link_recruitingoff",userId)%><%}%> >> </A></TD></TR>
    <%
    }
    %>
        <%
    if ((alliance != null) && (allianceMember.getIsAdmin() || allianceMember.getIsCouncil())) {
    %>
    <!-- Allianzbretter verwalten -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%=TYPE_MANAGEALLIANCEBOARD%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_manageallianceboards",userId)%></A></TD></TR>
    <%
    }
    %>

    <%
    %><TR><TD ALIGN="center">-------------------</TD></TR>
    <!-- Alle Allianzen anzeigen -->
    <TR align="center"><TD><A HREF="main.jsp?page=showAlliance&type=0" style="font-size:12px"><%= ML.getMLStr("alliance_link_showallalliances",userId)%></A></TD></TR>
    <%
    %>
    <%
    if ((alliance != null) && allianceMember.getIsAdmin() && alliance.getUserId() == allianceMember.getUserId()) {
    %>
    <!-- Allianz aufl�sen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_DELETEALLIANCE%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_disbandalliance",userId)%></A></TD></TR>
    <%
    }
    %>
    <%
    if (alliance != null) {
    %>
    <!-- Allianz verlassen -->
    <TR align="center"><TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_LEAVEALLIANCE%>" style="font-size:12px"><%= ML.getMLStr("alliance_link_leavealliance", userId)%></A></TD></TR>
    <%
    }
    %>
</TABLE><%
}
%>

