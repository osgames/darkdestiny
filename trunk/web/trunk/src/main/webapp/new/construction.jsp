<%@page import="at.darkdestiny.core.update.ProdOrderBuffer"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.databuffer.*"%>
<%@page import="at.darkdestiny.core.service.ConstructionService" %>
<%@page import="at.darkdestiny.core.service.Service" %>
<%@page import="at.darkdestiny.core.result.BuildableResult" %>
<%@page import="at.darkdestiny.core.result.InConstructionResult" %>
<%@page import="at.darkdestiny.core.result.InConstructionEntry" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.view.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.utilities.*" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%

            session = request.getSession();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
            boolean idle = false;
            if(request.getParameter("idle") != null){
                idle = Boolean.valueOf(request.getParameter("idle"));
                }

            if (request.getParameter("compress") != null) {
                ConstructionService.compressOrders(userId, planetId);
            }
            
            BaseResult result = null;
            String basePicPath = GameConfig.getInstance().picPath();
            ArrayList<String> errorList = new ArrayList<String>();
            ArrayList<EConstructionType> types = new ArrayList<EConstructionType>();
            EConstructionType type = null;
                    
            String typeString = "";
            if (request.getParameter("type") != null) {
                if(!request.getParameter("type").equalsIgnoreCase("null")){
                    typeString = request.getParameter("type");
                    StringTokenizer st = new StringTokenizer(typeString, "|");
                    while(st.hasMoreElements()){
                        String token = st.nextToken();
                            if(type == null){
                                type = EConstructionType.valueOf(token);
                            }
                            types.add(EConstructionType.valueOf(token));
                        }
                }
            }
            
            typeString = "";
            for (EConstructionType ect : types) {
                if (typeString.isEmpty()) {
                    typeString = ect.name();
                } else {
                    typeString = typeString + "|";
                    typeString = typeString + ect.name();
                }
            }
            
            session.setAttribute("actPage", "new/construction");
            
            Map<String, String[]> pmap = request.getParameterMap();
            int number = 0;
            int cId = 0;
                    int count = 0;
            for (final Map.Entry<String, String[]> entry : pmap.entrySet()) {
                String parName = entry.getKey();
                if (parName.equalsIgnoreCase("build")) {
                    number = Integer.parseInt(entry.getValue()[0]);
                } else if (parName.equalsIgnoreCase("bid")) {
                    cId = Integer.parseInt(entry.getValue()[0]);
                } else if (parName.equalsIgnoreCase("cid")) {
                    cId = Integer.parseInt(entry.getValue()[0]);
                } else if (parName.equalsIgnoreCase("count")) {
                    count = Integer.parseInt(entry.getValue()[0]);
                }
            }

            ArrayList<String> errors = new ArrayList<String>();
            if (cId > 0) {
                if(idle){
                    result = ConstructionService.idleBuilding(userId, planetId, cId, count);
                    if (result.isError()) errorList.add(result.getMessage());
                }else{
                    errorList.addAll(ConstructionService.buildBuilding(userId, planetId, cId, number));
                }
            }
// Change Priority
            if ((request.getParameter("oNo") != null) && (request.getParameter("priority") != null)) {
                int orderNo = Integer.parseInt((String) request.getParameter("oNo"));
                int priority = Integer.parseInt((String) request.getParameter("priority"));
                ConstructionService.setNewPriority(orderNo, priority);
            }

            
%>

<!-- NEW CODE -->

<TABLE class="trans" width="80%">
    <TR align="center">
         <TD width="13%"><U><A href="main.jsp?page=new/construction"><%= ML.getMLStr("construction_lbl_overview", userId)%></A></U></TD>
        <TD width="13%"><U><A href="main.jsp?page=new/construction&type=<%= EConstructionType.ADMINISTRATION %>"><%= ML.getMLStr("construction_lbl_administration", userId)%></A></U></TD>
        <TD width="13%"><U><A href="main.jsp?page=new/construction&type=<%= EConstructionType.ZIVIL %>"><%= ML.getMLStr("construction_lbl_civil", userId)%></A></U></TD>
        <TD width="13%"><U><A href="main.jsp?page=new/construction&type=<%= EConstructionType.ENERGY %>"><%= ML.getMLStr("construction_lbl_energy", userId)%></A></U></TD>
        <TD width="13%"><U><A href="main.jsp?page=new/construction&type=<%= EConstructionType.INDUSTRY %>"><%= ML.getMLStr("construction_lbl_industry", userId)%></A></U></TD>
        <TD width="13%"><U><A href="main.jsp?page=new/construction&type=<%= EConstructionType.MILITARY %>|<%= EConstructionType.PLANETARY_DEFENSE %>"><%= ML.getMLStr("construction_lbl_military", userId)%></A></U></TD>
</TR>
</TABLE><BR/>
<%
    for (String s : errorList) {
%>
<CENTER><FONT color="red"><%= s %></FONT></CENTER>
<%
    }

    if (!errorList.isEmpty()) {
        out.write("<BR><BR>");
    }

    InConstructionResult icr = ConstructionService.findRunnigConstructions(planetId);
    ArrayList<InConstructionEntry> constructions = icr.getEntries();
    if (constructions.size() > 0) {
%>
<table cellpadding="0" cellspacing="0" width="80%"><tr><td>
            <TABLE class="blue" border="0" cellpadding="0" cellspacing="0" width="100%">
                <TR bgcolor="#808080" >
                    <td class="blue" width="*"><B><%= ML.getMLStr("construction_lbl_runningconstructions", userId)%></B></td>
                    <td class="blue" width="50" align="center"><B><%= ML.getMLStr("construction_lbl_quantity", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_industry", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_industry", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_module", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_module", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_shipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_shipyard", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_orbitalshipyard", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_orbitalshipyard", userId)%></B></td>
                    <td class="blue" width="41" align="center" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_barracks", userId)%>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_abbr_barracks", userId)%></B></td>
                    <td class="blue" width="73">
                        <TABLE cellpadding="0" cellspacing="0" width="100%">
                            <TR valign="center">
                                <TD align="middle">
                                    <B style="font-size: 13px;"><%= ML.getMLStr("construction_lbl_priority", userId)%></B>
                                </TD><TD align="right">
                                    <IMG src="<%=GameConfig.getInstance().picPath()%>pic/icons/compress.png" onclick="window.location.href='main.jsp?page=new/construction&type=<%= typeString %>&compress=1';" ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("queue_compress", userId)%>')" ONMOUSEOUT="hideTip()" style="cursor:pointer" />
                                </TD>
                            </TR>
                        </TABLE>
                    </td>
                    <td class="blue" width="25"><B>&nbsp;</B></td>
                </TR>
                <%  for (InConstructionEntry ice : constructions) {

                    String name = "DEFAULT";
                    String extensionTag = "";

                    if ((ice.getOrder().getType() == EProductionOrderType.C_SCRAP) || (ice.getOrder().getType() == EProductionOrderType.SCRAP)) {
                        extensionTag = " " + ML.getMLStr("construction_lbl_abbr_scrapbuilding", userId) + "";
                    }
                    if ((ice.getOrder().getType() == EProductionOrderType.C_IMPROVE)) {
                        extensionTag = " " + ML.getMLStr("construction_lbl_abbr_improve",userId) + "";
                    }
                    if ((ice.getOrder().getType() == EProductionOrderType.C_UPGRADE) || (ice.getOrder().getType() == EProductionOrderType.UPGRADE)) {
                        extensionTag = " " + ML.getMLStr("construction_lbl_abbr_upgrade", userId) + "";
                    }
                    name = ML.getMLStr(((Construction)ice.getUnit()).getName(), userId);

                %>
                <tr>
                    <td><%= name + extensionTag%></td>
                    <td align="center"><%= ice.getAction().getNumber()%></td>
                    <%
      // Rebuild to Industry
      if (ice.getOrder().getIndNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%                } else {
                    %>
                    <td align="center"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getIndPerc(), 0)%> %</td>
                    <%
      }
      if (ice.getOrder().getModuleNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%                } else {
                    %>
                    <td align="center"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getModulePerc(), 0)%> %</td>
                    <%
      }
      if (ice.getOrder().getDockNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%                } else {
                    %>
                    <td align="center"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getDockPerc(), 0)%> %</td>
                    <%
      }
      if (ice.getOrder().getOrbDockNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%                } else {
                    %>
                    <td align="center"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getOrbDockPerc(), 0)%> %</td>
                    <%
      }
      if (ice.getOrder().getKasNeed() == 0) {
                    %>
                    <td align="center">-</td>
                    <%                } else {
                    %>
                    <td align="center"><%= FormatUtilities.getFormattedDecimal((float) ice.getOrder().getKasPerc(), 0)%> %</td>
                    <%
      }
%>         
                    <td>
                        <SELECT id="priority<%= ice.getOrder().getId()%>" name="priority<%= ice.getOrder().getId()%>" onchange="window.location.href='main.jsp?page=new/construction&type=<%= typeString %>&oNo=<%= ice.getOrder().getId()%>&priority='+document.getElementById('priority<%= ice.getOrder().getId()%>').value;">
<%
      for (int i=0;i<ProdOrderBuffer.PRIORITY_COUNT;i++) {
          boolean marked = false;

          if (ice.getOrder().getPriority() == i) {
              marked = true;
          }      

          String selStr = !marked ? "" : " selected";
          String prioTxt = "";
          
          if (i == 0) prioTxt = " (" + ML.getMLStr("construction_opt_priority_high", userId) + ")";
          if (i == 5) prioTxt = " (" + ML.getMLStr("construction_opt_priority_average", userId) + ")";
          if (i == 10) prioTxt = " (" + ML.getMLStr("cosntruction_opt_priority_low", userId) + ")";
%>
                            <option value="<%= i %>"<%= selStr %>><%= i %><%= prioTxt %></option>
<%
      }
%>
                        </SELECT>
                    </td> 
<%
      
            if (ice.getOrder().getType() == EProductionOrderType.C_BUILD) {
                    %>
                    <td align="middle">
                        <IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" style="cursor:pointer" onclick="window.location.href='main.jsp?page=abortBuilding&timeFinished=<%= ice.getAction().getProductionOrderId() %>&type=<%= typeString %>';" onmouseover="doTooltip(event,'Bauauftrag abbrechen')" onmouseout="hideTip()" />
                    </td>
                    <%
            } else if (ice.getOrder().getType() == EProductionOrderType.C_SCRAP) {
                    %>
                    <td align="middle">
                        <IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg" style="cursor:pointer" onclick="window.location.href='main.jsp?page=abortDeconstruction&orderNo=<%= ice.getAction().getProductionOrderId() %>&type=<%= typeString %>';" onmouseover="doTooltip(event,'Abriss abbrechen')" onmouseout="hideTip()" />
                    </td>
                    <%                
            }
                    /*
                    }
                     */
                }
                    %>
                </tr>
            </TABLE></td></tr></table><BR>
            <%
    }
    IndustryPointsView ipv = ConstructionService.getIndustryPointsView(planetId);
    %>
<TABLE style="font-size:13px" width="200px">
    <TR class="blue2" align="middle"><TD colspan='1'><B><%= ML.getMLStr("construction_lbl_buildingpointspertick", userId)%></B></TD>
    </TR>
    <TR align="middle">
        <TD ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_industryinfo", userId) %>')" ONMOUSEOUT="hideTip()"><B><%= ML.getMLStr("construction_lbl_industry", userId) %></B></TD>
        <TR align="middle"><TD><%= ipv.getIndustryPoints() %>
            <% if(ipv.getIndustryPointsBonus() > 0) { %><FONT style="color:#FF00FF; font-size: 10px; font-weight: bold;">
                (+ <%= ipv.getIndustryPointsBonus() %>)
            </FONT> 
            <% } %></TD>
    </TR>
    <TR><TD>&nbsp;</TD></TR>
</TABLE>
<BR>
<%
    if (errors.size() > 0) {
        for (String error : errors) {
%>
        <%= error %><BR>
<%
        }
    }

    if(type == null){
%>

<TABLE width="80%" class="bluetrans">
<TR><TD colspan=1 align=left class="blue"><B><%= ML.getMLStr("overview_lbl_building", userId)%></B></TD>
    <TD class="blue">Anzahl</TD>
    <TD class="blue" colspan="3">&nbsp;</TD></TR>
<%
boolean first = true;
for (Map.Entry<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>> entry : ConstructionService.findPlanetConstructions(planetId).entrySet()) {
    
%><TR><TD class="blue2" colspan="5" align="center" style="font-size:10px; font-weight: bolder"><%= ML.getMLStr("construction_type_" + entry.getKey().toString(), userId) %></TD></TR><%
   for (Map.Entry<Integer, ArrayList<PlanetConstruction>> entry1 : entry.getValue().entrySet()) {
        for(PlanetConstruction pc : entry1.getValue()) {
            Construction c = ConstructionService.findConstructionById(pc.getConstructionId());
              boolean canBeImproved = ConstructionUtilities.isImproveable(c, userId, planetId).isBuildable();
              boolean canBeUpgraded = ConstructionUtilities.canBeUpgraded(c, userId, planetId).isBuildable();
              boolean isDeactivated = ConstructionUtilities.isDeactivated(c, userId, planetId);
              //tempor�r
              canBeUpgraded = false;
              String isDeactivatedString = "";
              if(isDeactivated){
                  isDeactivatedString = "<FONT size=1 color=#888888>(Deaktiviert - Besseres Geb�ude vorhanden)</FONT>";
              }

                if(c.getType() == EConstructionType.ADMINISTRATION){

                    // Verwaltungsgeb&auml;ude k&ouml;nnen nicht abgerissen werden
        %>
                    <TR>
                    <TD width="60%"><FONT color="lightblue"><%= ML.getMLStr(c.getName(), userId)%> <%= isDeactivatedString %></FONT></TD>
                    <TD width="30%"></TD>
               <%}else{%>
                    <TR>
                    <TD width="60%"><%= ML.getMLStr(c.getName(), userId)%> <%= isDeactivatedString %>
                        <% if(c.getLevelable()){ %><FONT style="font-size: 10px;" color="yellow"><B>(Level: <%= pc.getLevel() %><%if(ConstructionUtilities.isBeingImproved(c.getId(), userId, planetId)){%> => <%= (pc.getLevel() + 1) %><%}%>)</B></FONT> <%}%>
                    </TD>
                    <TD width="30%"><%= pc.getActiveCount() %> / <%= pc.getNumber() %></TD>

        <%
                }
                   if (canBeImproved || canBeUpgraded) {
                       if(canBeImproved){
                            %>
                            <TD align="middle">
                               <IMG src="<%=GameConfig.getInstance().picPath()%>pic/spyUpgrade.jpg"
                            onmouseover="doTooltip(event,'Improve')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=improveBuilding&constructionId=<%= c.getId() %>';" />
                            </TD>
                            <%
                           }else if(canBeUpgraded){ %>
                            <TD align="middle">
                               <IMG src="<%=GameConfig.getInstance().picPath()%>pic/modify.jpg"
                            onmouseover="doTooltip(event,'Upgrade')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=upgradeBuilding&constructionId=<%= c.getId() %>';" />
                            </TD>
                           <% }
                     }else{
                            %><TD>&nbsp;</TD><%
                            }
                        if (c.isIdleable()) {
                            %>
                            <TD align="middle">
                               <IMG src="<%=GameConfig.getInstance().picPath()%>pic/stop.jpg"
                            onmouseover="doTooltip(event,'<%= ML.getMLStr("construction_pop_pause", userId)%>')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=new/pauseConstruction&constructionId=<%= c.getId() %>';" />
                            </TD>
                            <%
                            }else{
                                   %><TD>&nbsp;</TD><%
                            }
                         if (c.isDestructible()) {
                            %>
                            <TD align="middle">
                                   <IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg"
                            onmouseover="doTooltip(event,'<%= ML.getMLStr("construction_pop_destroy", userId)%>')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=destroyBuilding&cId=<%= c.getId() %>';" />
                            </TD>
                            <%
                                }else{
                             %><TD>&nbsp;</TD><%
                                }%>
                             </TR>
                            <%

        }
      }
   }
ArrayList<PlanetDefense> defenses = Service.planetDefenseDAO.findByPlanet(planetId, EDefenseType.TURRET);
if(!defenses.isEmpty()){
%><TR><TD class="blue2" colspan="5" align="center" style="font-size:10px; font-weight: bolder"><%= ML.getMLStr("construction_type_" + EConstructionType.PLANETARY_DEFENSE.toString(), userId) %></TD></TR><%
}
        for(PlanetDefense pd : defenses){
            Construction defense = ConstructionService.findConstructionById(pd.getUnitId());

            %>

            <TR>
                <TD width="60%"><%= ML.getMLStr(defense.getName(), userId)%></TD>
            <TD width="30%"><%= pd.getCount() %></TD>
            <%
            if (defense.isIdleable()) {
                    %>
                    <TD align="middle">
                       <IMG src="<%=GameConfig.getInstance().picPath()%>pic/stop.jpg"
                    onmouseover="doTooltip(event,'<%= ML.getMLStr("construction_pop_pause", userId)%>')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=new/pauseConstruction&constructionId=<%= defense.getId() %>';" />
            </TD>
                    <%
                    }else{
                           %><TD></TD><%
                    }
           if (!defense.isDestructible()) { %>
            <TD width="10%"></TD>
            <% }else{ %>
            <TD align="middle">
             <IMG src="<%=GameConfig.getInstance().picPath()%>pic/cancel.jpg"
                    onmouseover="doTooltip(event,'<%= ML.getMLStr("construction_pop_destroy", userId)%>')" onmouseout="hideTip()" onclick="window.location.href='main.jsp?page=destroyBuilding&cId=<%= defense.getId() %>';" />
             </TD>
            <%  } %>
            </TR> <%
            }
%>
</TABLE>
<%

}else{
  //  ProductionBuildResult pbr = ConstructionService.getConstructionList(userId, planetId, type);
    ProductionBuildResult pbr = ConstructionService.getConstructionListSorted(userId, planetId, types);

   TreeMap <Object, HashMap <Buildable, BuildableResult>> cons1 = pbr.getAvailConstructionsSorted().get(Boolean.FALSE);
   if(cons1 != null){
    // for(Buildable b : pbr.getUnitsWithPossibleTech()) { 
    for(Map.Entry<Object, HashMap <Buildable, BuildableResult>> entries1 : pbr.getAvailConstructionsSorted().get(Boolean.FALSE).entrySet()) {

        for(Map.Entry<Buildable, BuildableResult> entries2 : entries1.getValue().entrySet()){

        Construction c = (Construction)entries2.getKey().getBase();

          BuildableResult br = entries2.getValue();


            boolean collapsed = !ConstructionService.isShowConstructionDetails(userId);
            String style = "";
            String img1 = "pic/icons/menu_ein.png";
            String img2 = "pic/icons/menu_aus.png";
            if (collapsed) {
             img2 = "pic/icons/menu_ein.png";
             img1 = "pic/icons/menu_aus.png";
                style = "none";
            }

%>
<FORM  method="post" action="main.jsp?page=new/construction&type=<%= typeString %>" Name="Construction">
    <TABLE border="0" class="blue" width="80%">
        <TR valign="middle" class="blue2">
            <TD>
                <span onclick="toogleVisibility('construction<%= c.getId()%>');
                    exchangeImgs('img_system<%= c.getId()%>', '<%=basePicPath%><%= img2 %>',
                    '+','<%=GameConfig.getInstance().picPath()%><%= img1 %>','-');">
                    <img src="<%=GameConfig.getInstance().picPath()%><%= img1 %>" alt="-" border="0" id="img_system<%= c.getId()%>">&nbsp;
                    <B><%= ML.getMLStr(c.getName(), userId)%></B>

                </span>
            </TD>
            <TD align="right">
                <% if (br.isBuildable()) {%>
                (max: <%= br.getPossibleCount()%>x) <%= ML.getMLStr("construction_lbl_count", userId)%>&nbsp
                <INPUT NAME="build" TYPE="text" VALUE="1" SIZE="2" ALIGN=middle>
                <INPUT NAME="bid" TYPE="HIDDEN" VALUE="<%= c.getId()%>">
                <INPUT NAME="submit" TYPE="SUBMIT" VALUE="<%= ML.getMLStr("construction_but_build", userId)%>">

                <% } else {%>
                <B><%= ML.getMLStr("construction_err_notpossible", userId)%>  </B>
                <%}%>
            </TD>
        </TR>
    </TABLE>    
    <TABLE class="deftransb2" border=0 width="80%">
        <TR valign="top" style="display:<%= style %>" id="construction<%= c.getId()%>">
            <% if(!c.getImage().equals("")){ %>
            <TD align="left" width="150px" height="150px"><IMG  alt="<%= c.getName() %>" src="<%= GameConfig.getInstance().picPath() %>pic/construction/<%= c.getImage() %>"/></TD>
            <% } else { %>
            <TD align="left" width="150px" height="150px"><IMG  alt="<%= c.getName() %>" src="<%= GameConfig.getInstance().picPath() %>pic/construction/placeholder.PNG"/></TD>
            <% } %>
            <TD align="left" valign="top">
                <TABLE style="font-size:13px" border="0" width="100%">
                    <TR valign="top"><TD colspan=2><%= ML.getMLStr(c.getInfotext(), userId)%><BR><BR></TD></TR>
<%
                  if (br.isBuildable() == false) { %>
                    <TR valign="top"><TD colspan=2>
<%                    ArrayList<String> consErrors = br.getMissingRequirements();
                      for (String actError : consErrors) { %>
                        <B><FONT color="yellow"><%= actError %></FONT></B><BR>
<%                    } %>
                    <BR></TD><TR>
<%                                    
                  }
%>                    
                    <TR valign="top">
                        <TD valign="top" align=left width="50%">
                            <TABLE  cellpadding="0" style="font-size:12px;" cellspacing="0" border="0" width="100%">
                              <TR>
                                  <TD style="font-size:15px" colspan="2">
                                      <P style="margin-bottom:5px"><B><%= ML.getMLStr("construction_lbl_constructioncosts", userId)%></B></P>
                                  </TD>
                              </TR>
                      <TR>
                          <TD colspan="2">
                              <%= ML.getMLStr("construction_lbl_industrypoints", userId) %> <%= FormatUtilities.getFormattedNumber(c.getBuildtime())%>
                          </TD>
                      </TR><%
                if (c.getWorkers() > 0) {%>
                      <TR><TD ONMOUSEOVER="doTooltip(event,'<%= ML.getMLStr("construction_pop_populationpoints", userId) %>')" ONMOUSEOUT="hideTip()" colspan="2"><%= ML.getMLStr("construction_lbl_populationpoints", userId)%> <%= FormatUtilities.getFormattedNumber(c.getWorkers())%></TD></TR><%
                }
                if (c.getSurface() > 0) {%>
                     <TR><TD colspan="2"><%= ML.getMLStr("construction_lbl_buildingground", userId)%> <%= FormatUtilities.getFormattedNumber(c.getSurface())%></TD></TR><%
                }

                ArrayList<at.darkdestiny.core.model.RessourceCost> rcList = ConstructionService.findRessourceCostForConstructionId(c.getId());
                if(rcList.size() > 0){
                %>

                <%
                for (at.darkdestiny.core.model.RessourceCost rc : rcList) {
                    at.darkdestiny.core.model.Ressource r = ConstructionService.findRessourceById(rc.getRessId());
                    %>
                    <TR>
                        <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                        <TD align="left" valign="middle">&nbsp;<FONT style="font-size:12px; font-weight:bold;"><%= FormatUtilities.getFormattedNumber(rc.getQty()) %> </FONT> (<%= ML.getMLStr(r.getName(), userId) %> )</TD>
                    </TR>
                    <%
                   }
                }
                    %>
                </TABLE>
                </TD>
                <% ArrayList<Production> production = ConstructionService.findProductionForConstructionId(planetId, c.getId()); %>
                        <TD width="50%">
                            <TABLE  cellpadding="0" style="font-size:12px;" cellspacing="0" border="0" width="100%">
                                <TR>
                                 <TD style="font-size:15px" colspan="3">
                                     <% if(production.size() > 0){ %><P style="margin-bottom:5px"><B><%= ML.getMLStr("construction_lbl_productionconsumptionstore", userId)%></B></P><% } %>
                                 </TD>
                            </TR>
                            <%
                for (Production p : production) {
                    at.darkdestiny.core.model.Ressource r = ConstructionService.findRessourceById(p.getRessourceId());
                    %>
 
                    <%
                    if (p.getDecinc() == Production.PROD_DECREASE) {
                           
                            %>
                            <TR>
                                <TD width="3" align="left"  valign="middle"><IMG  alt="decrese"  src="<%= GameConfig.getInstance().picPath() %>pic/menu/icons/emigration_minus.png"/></TD>
                            <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                            <TD align="left" valign="middle"> <FONT style="font-size:12px; font-weight:bold;">&nbsp;<%= FormatUtilities.getFormattedNumber(p.getQuantity())%> </FONT><%= ML.getMLStr(r.getName(), userId)%> </TD>
                            </TR>
                            <% } else if (p.getDecinc() == Production.PROD_INCREASE) {
                                 
                            %>
                            <TR>
                           <TD width="3" align="left"  valign="middle"><IMG  alt="increase"  src="<%= GameConfig.getInstance().picPath() %>pic/menu/icons/emigration_plus.png"/> </TD>
                            <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                            <TD align="left" valign="middle"> <FONT style="font-size:12px; font-weight:bold;">&nbsp;<%= FormatUtilities.getFormattedNumber(p.getQuantity())%> </FONT><%= ML.getMLStr(r.getName(), userId)%> </TD>
                            </TR>
                            <% } else if (p.getDecinc() == Production.PROD_STORAGE){
                                 
                            %>
                            <TR>
                           <TD width="3" align="left"  valign="middle"><IMG  alt="store"  src="<%= GameConfig.getInstance().picPath() %>pic/menu/icons/store.png"/> </TD>
                            <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                            <TD align="left" valign="middle"> <FONT style="font-size:12px; font-weight:bold;">&nbsp;<%= FormatUtilities.getFormattedNumber(p.getQuantity())%> </FONT><%= ML.getMLStr(r.getName(), userId)%> </TD>
                            </TR>
                            <% }
                }
                            %>
                            </TABLE>
                            </TD>
                    </TR>
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</FORM>    
<%
    }
        }
        }
   TreeMap <Object, HashMap <Buildable, BuildableResult>> cons2 = pbr.getAvailConstructionsSorted().get(Boolean.TRUE);
   if(cons2 != null){
    for(Map.Entry<Object, HashMap <Buildable, BuildableResult>> entries1 :cons2.entrySet()) {

        for(Map.Entry<Buildable, BuildableResult> entries2 : entries1.getValue().entrySet()){

        Construction c = (Construction)entries2.getKey().getBase();

          BuildableResult br = entries2.getValue();

            boolean collapsed = ConstructionService.isShowConstructionDetails(userId);
                    collapsed = true;
            String style = "";
            String img1 = "pic/icons/menu_ein.png";
            String img2 = "pic/icons/menu_aus.png";
            if (collapsed) {
             img2 = "pic/icons/menu_ein.png";
             img1 = "pic/icons/menu_aus.png";
                style = "none";
            }
%>
<FORM  method="post" action="main.jsp?page=new/construction&type=<%= typeString %>" Name="Construction">
    <TABLE border="0" class="gray" width="80%">
        <TR valign="middle" class="gray2">
            <TD>
                <span onclick="toogleVisibility('construction<%= c.getId()%>');
                    exchangeImgs('img_system<%= c.getId()%>', '<%=basePicPath%><%= img2 %>',
                    '+','<%=GameConfig.getInstance().picPath()%><%= img1 %>','-');">
                    <img src="<%=GameConfig.getInstance().picPath()%><%= img1 %>" alt="-" border="0" id="img_system<%= c.getId()%>">&nbsp;
                    <B><%= ML.getMLStr(c.getName(), userId)%></B>

                </span>
            </TD>
            <TD align="right">
                <% if (br.isBuildable()) {%>
                (max: <%= br.getPossibleCount()%>x) <%= ML.getMLStr("construction_lbl_count", userId)%>&nbsp
                <INPUT NAME="build" TYPE="text" VALUE="1" SIZE="2" ALIGN=middle>
                <INPUT NAME="bid" TYPE="HIDDEN" VALUE="<%= c.getId()%>">
                <INPUT NAME="submit" TYPE="SUBMIT" VALUE="<%= ML.getMLStr("construction_but_build", userId)%>">

                <% } else {%>
                <B><%= ML.getMLStr("construction_err_notpossible", userId)%>  </B>
                <%}%>
            </TD>
        </TR>
    </TABLE>
    <TABLE style="font-size:13px" border=0 width="80%">
        <TR valign="top" style="display:<%= style %>" id="construction<%= c.getId()%>">
            <% if(!c.getImage().equals("")){ %>
            <TD align="left" width="150px" height="150px"><IMG  alt="<%= c.getName() %>" src="<%= GameConfig.getInstance().picPath() %>pic/construction/<%= c.getImage() %>"/></TD>
            <% } else { %>
            <TD align="left" width="150px" height="150px"><IMG  alt="<%= c.getName() %>" src="<%= GameConfig.getInstance().picPath() %>pic/construction/placeholder.PNG"/></TD>
            <% } %>
            <TD align="left">
                <TABLE style="font-size:13px" border="0" width="100%">
                    <TR valign="top"><TD colspan=2><%= ML.getMLStr(c.getInfotext(), userId)%><BR><BR></TD></TR>
<%
                  if (br.isBuildable() == false) { %>
                    <TR valign="top"><TD colspan=2>
<%                    ArrayList<String> consErrors = br.getMissingRequirements();
                      for (String actError : consErrors) { %>
                        <B><FONT color="yellow"><%= actError %></FONT></B><BR>
<%                    } %>
                    <BR></TD><TR>
<%
                  }
%>
                   <TR valign="top">
                        <TD valign="middle" align=left width="50%">
                            <TABLE  cellpadding="0" style="font-size:12px;" cellspacing="0" border="0" width="100%">
                              <TR>
                                  <TD style="font-size:14px" colspan="2">
                                      <P style="margin-bottom:5px"><B><%= ML.getMLStr("construction_lbl_constructioncosts", userId)%></B></P>
                                  </TD>
                              </TR>
                      <TR><TD colspan="2"> <%= ML.getMLStr("construction_lbl_buildingpoints", userId)%> <%= FormatUtilities.getFormattedNumber(c.getBuildtime())%></TD></TR><%
                if (c.getWorkers() > 0) {%>
                        
                      <TR><TD colspan="2">     <%= ML.getMLStr("construction_lbl_populationpoints", userId)%> <%= FormatUtilities.getFormattedNumber(c.getWorkers())%></TD></TR><%
                }
                if (c.getSurface() > 0) {%>
                         
                      <TR><TD colspan="2">    <%= ML.getMLStr("construction_lbl_buildingground", userId)%> <%= FormatUtilities.getFormattedNumber(c.getSurface())%></TD></TR><%
                }

                ArrayList<at.darkdestiny.core.model.RessourceCost> rcList = ConstructionService.findRessourceCostForConstructionId(c.getId());
                if(rcList.size() > 0){
                %>

                <%
                for (at.darkdestiny.core.model.RessourceCost rc : rcList) {
                    at.darkdestiny.core.model.Ressource r = ConstructionService.findRessourceById(rc.getRessId());
                    %>
                    <TR>
                    <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                    <TD align="left" valign="middle">&nbsp;<FONT style="font-size:12px; font-weight:bold;"><%= FormatUtilities.getFormattedNumber(rc.getQty()) %> </FONT> (<%= ML.getMLStr(r.getName(), userId) %> )</TD>

                    </TR>
                    <%
                }
                }
                            %>
                </TABLE>
                </TD>
                <% ArrayList<Production> production = ConstructionService.findProductionForConstructionId(planetId, c.getId()); %>
                         <TD width="50%">
                            <TABLE  cellpadding="0" style="font-size:12px;" cellspacing="0" border="0" width="100%">
                                <TR>
                                 <TD style="font-size:14px" colspan="3">
                                     <% if(production.size() > 0){ %><P style="margin-bottom:5px"><B><%= ML.getMLStr("construction_lbl_productionconsumptionstore", userId)%></B></P><% } %>
                                     
                                 </TD>
                            </TR>
                            <%
                for (Production p : production) {
                    at.darkdestiny.core.model.Ressource r = ConstructionService.findRessourceById(p.getRessourceId());
                    %>

                    <%
                    if (p.getDecinc() == Production.PROD_DECREASE) {

                            %>
                            <TR>
                                <TD width="3" align="left"  valign="middle"><IMG  alt="decrese"  src="<%= GameConfig.getInstance().picPath() %>pic/menu/icons/emigration_minus.png"/></TD>
                            <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                            <TD align="left" valign="middle"> <FONT style="font-size:12px; font-weight:bold;">&nbsp;<%= FormatUtilities.getFormattedNumber(p.getQuantity())%> </FONT><%= ML.getMLStr(r.getName(), userId)%> </TD>
                            </TR>
                            <% } else if (p.getDecinc() == Production.PROD_INCREASE) {

                            %>
                            <TR>
                           <TD width="3" align="left"  valign="middle"><IMG  alt="increase"  src="<%= GameConfig.getInstance().picPath() %>pic/menu/icons/emigration_plus.png"/> </TD>
                            <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                            <TD align="left" valign="middle"> <FONT style="font-size:12px; font-weight:bold;">&nbsp;<%= FormatUtilities.getFormattedNumber(p.getQuantity())%> </FONT><%= ML.getMLStr(r.getName(), userId)%> </TD>
                            </TR>
                            <% } else if (p.getDecinc() == Production.PROD_STORAGE){

                            %>
                            <TR>
                           <TD width="3" align="left"  valign="middle"><IMG  alt="store"  src="<%= GameConfig.getInstance().picPath() %>pic/menu/icons/store.png"/> </TD>
                            <TD width="20" align="left"  valign="middle"><IMG  alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                            <TD align="left" valign="middle"> <FONT style="font-size:12px; font-weight:bold;">&nbsp;<%= FormatUtilities.getFormattedNumber(p.getQuantity())%> </FONT><%= ML.getMLStr(r.getName(), userId)%> </TD>
                            </TR>
                            <% }
                        }%>
                            </TABLE>
                            </TD>
                    </TR>
                </TABLE>
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
    }
        }
    }
    }
%>