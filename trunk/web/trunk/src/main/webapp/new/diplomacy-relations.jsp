<%@page import="at.darkdestiny.core.result.DiplomacyResult"%>
<%@page import="at.darkdestiny.core.diplomacy.relations.IRelation"%>
<%@page import="java.lang.reflect.Constructor"%>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="at.darkdestiny.core.GameConfig" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.service.DiplomacyService" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>


<%

            final int TYPE_SHOWALLINACERELATIONS = 1;
            final int TYPE_SHOWUSERRELATIONS = 4;
            final int TYPE_MANAGERELATIONS = 2;
            final int TYPE_SHOWOVERVIEW = 9;
            final int OPTION_ALLALLIANCES = 0;
            final int OPTION_ALLUSERS = 0;
            session.setAttribute("actPage", "new/diplomacy");
            session = request.getSession();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            DiplomacyService.preparePossibleRelations(userId);
            int dUserId = -1;
            int type = TYPE_SHOWOVERVIEW;
            int process = 0;
            int allianceId = 0;
            int userAllianceId = -1;

            AllianceMember am = DiplomacyService.findAllianceMemberByUserId(userId);
            if (am != null) {
                allianceId = am.getAllianceId();
            }
            if (DiplomacyService.findAllianceMemberByUserId(userId) != null) {
                userAllianceId = DiplomacyService.findAllianceMemberByUserId(userId).getAllianceId();
            }


            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }

            if (request.getParameter("alliance") != null) {
                allianceId = Integer.parseInt(request.getParameter("alliance"));
            }
            if (request.getParameter("userId") != null) {
                dUserId = Integer.parseInt(request.getParameter("userId"));
            }
            ArrayList<Alliance> allys = new ArrayList<Alliance>();
            ArrayList<User> users = new ArrayList<User>();
            if (allianceId > 0) {
                allys.add(DiplomacyService.findAllianceById(userId, allianceId));
            } else {
                for(Integer i : DiplomacyService.findAlliancesToDisplay(userId)){
                    allys.add(DiplomacyService.findAllianceById(i));
                    }
            }
            if (dUserId == -1) {
                users.add(DiplomacyService.findUserById(userId, userId));
                dUserId = userId;
            } else if (dUserId == OPTION_ALLUSERS) {
               users = DiplomacyService.findAllUsersDisplayAble(userId);

            } else {
                users.add(DiplomacyService.findUserById(userId, dUserId));
            }

%>
<TABLE width="80%">
    <TR ALIGN="Center">
        <TD width="50%"><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWOVERVIEW%>"><%= ML.getMLStr("diplomacy_link_main", userId)%></A></TD>
       <!-- <TD width="25%"><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWALLINACERELATIONS%>"><%= ML.getMLStr("diplomacy_link_alliances", userId)%></A></TD> -->
        <!-- <TD width="25%"><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWUSERRELATIONS%>"><%= ML.getMLStr("diplomacy_link_players", userId)%></A></TD> -->
        <TD width="50%"><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_MANAGERELATIONS%>"><%= ML.getMLStr("diplomacy_link_management", userId)%></A></TD>
    </TR>
</TABLE><BR/>
<%
            if (type == TYPE_SHOWOVERVIEW) {
%>
<TABLE width="80%"><TR><TD ALIGN="LEFT" style="font-size:13px;">

            <%= ML.getMLStr("diplomacy_msg_diplomacydescription", userId)%>
        </TD></TR>
    <TR><TD><BR>
            <A href="http://www.thedarkdestiny.at/wiki/index.php?title=Sternenkarte" target="new"><%= ML.getMLStr("diplomacy_link_starmapsharing", userId)%></A>
        </TD></TR></TABLE><BR>

    <TABLE class="blue" style="" width="80%">

    <TR>
        <TD align="center"></TD>
        <TD align="center"></TD>
        <TD CLASS="blue2" align="center" colspan="2"><%= ML.getMLStr("diplomacy_lbl_fightinsystem", userId) %></TD>
        <TD align="center" colspan="5"></TD>
    </TR>
    <TD></TD>
    <TR><TD CLASS="blue2"></TD>
        <TD CLASS="blue2" align="left"><%= ML.getMLStr("diplomacy_lbl_name", userId) %></TD>
        <TD CLASS="blue2" align="center"><%= ML.getMLStr("diplomacy_lbl_neutral", userId) %></TD>
        <TD CLASS="blue2" align="center"><%= ML.getMLStr("diplomacy_lbl_own", userId) %></TD>
        <TD CLASS="blue2" align="center"><%= ML.getMLStr("diplomacy_lbl_helsinfight", userId) %></TD>
        <TD CLASS="blue2" align="center"><%= ML.getMLStr("diplomacy_lbl_attackstradefleets", userId) %></TD>
        <TD CLASS="blue2" align="center"><%= ML.getMLStr("diplomacy_lbl_usageofdocks", userId) %></TD>
        <TD CLASS="blue2" align="center"><%= ML.getMLStr("diplomacy_lbl_sharestarmap", userId) %></TD>
        <TD CLASS="blue2" align="center"><%= ML.getMLStr("diplomacy_lbl_color", userId) %></TD>
    </TR>
    <%
                for (DiplomacyType dt : DiplomacyService.findAllDiplomacyType()) {

            //Holen der Klasse die die diplomatische Beziehung beschreibt
            Class clzz = Class.forName(dt.getClazz());
            //Get Constructor
            Constructor con = clzz.getConstructor();

            //Cast Relation
            IRelation sbe = (IRelation) con.newInstance();

            //Get specific settings for this Relation
            DiplomacyResult diplomacyResult = sbe.getDiplomacyResult();
    %>
    <TR>
        <TD><img width="20" height="20" onmouseout="hideTip()" onmouseover="doTooltip(event,'<%= ML.getMLStr(dt.getDescription(), userId) %>')"  alt="Forschung" src="<%= GameConfig.getInstance().picPath()%>pic/info.jpg"></TD>
        <TD><%= ML.getMLStr(dt.getName(), userId) %></TD>
        <TD align="center"><%= ML.getMLStr("diplomacy_EAttackType_" + diplomacyResult.getBattleInNeutral().toString(), userId) %></TD>
        <TD align="center"><%= ML.getMLStr("diplomacy_EAttackType_" + diplomacyResult.getBattleInOwn().toString(), userId) %></TD>
        <TD align="center"><% if(diplomacyResult.isHelps()){%>X<% }  %></TD>
        <TD align="center"><% if(diplomacyResult.isAttackTradeFleets()){%>X<% }  %></TD>
        <TD align="center"><% if(diplomacyResult.isAbleToUseShipyard()){%>X<% }  %></TD>
        <TD align="center"><%= DiplomacyService.SharingTypeToString(diplomacyResult.getSharingStarmapInfo(), userId) %></TD>
        <TD bgcolor="<%= diplomacyResult.getColor() %>">&nbsp;&nbsp;&nbsp;</TD>
    </TR>
    <%
                }
    %>
    </TABLE>
<%
            }
%>
<%
            if (type == TYPE_SHOWALLINACERELATIONS) {
                ArrayList<DiplomacyRelation> allianceRelations = new ArrayList<DiplomacyRelation>();
                ArrayList<DiplomacyRelation> allianceUserRelations = new ArrayList<DiplomacyRelation>();

%>
<FORM METHOD="POST" name="allianceForm">
    <SELECT id="alliances" name="alliances" onChange="window.location=document.allianceForm.alliances.options[document.allianceForm.alliances.selectedIndex].value">
        <OPTION value="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWALLINACERELATIONS%>&alliance=<%= OPTION_ALLALLIANCES%>">< <%= ML.getMLStr("diplomacy_opt_all", userId)%> ></OPTION>
        <%

                for (Integer i : DiplomacyService.findAlliancesToDisplay(userId)) {
                    Alliance a = DiplomacyService.findAllianceById(i);
                    allianceRelations = DiplomacyService.findRelationByFromId(a.getId(), EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE, false);
                    allianceUserRelations = DiplomacyService.findRelationByFromId(a.getId(), EDiplomacyRelationType.ALLIANCE_TO_USER, false);
                    if (allianceRelations.size() <= 0 && allianceUserRelations.size() <= 0) {
                        continue;
                    }

                    if (a.getId() == allianceId) {
        %>
        <OPTION SELECTED value="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWALLINACERELATIONS%>&alliance=<%= a.getId()%>"><%= a.getName()%></OPTION>
        <%} else {
        %>
        <OPTION value="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWALLINACERELATIONS%>&alliance=<%= a.getId()%>"><%= a.getName()%></OPTION>
        <%
                    }

                }
        %>
    </SELECT>
</FORM>
<BR/>
<%
                for (Alliance a : allys) {
                       allianceRelations = DiplomacyService.findRelationByFromId(a.getId(), EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE, false);
                    allianceUserRelations = DiplomacyService.findRelationByFromId(a.getId(), EDiplomacyRelationType.ALLIANCE_TO_USER, false);

                    if (allianceRelations.size() <= 0 && allianceUserRelations.size() <= 0) {
                        continue;
                    } else {
%>
<TABLE  class="blue" width ="80%">
    <TR>
        <TD ALIGN="CENTER" width="33%"class="blue2" COLSPAN="6"><B><%= a.getName()%> [<%= a.getTag()%>]</B></TD>
    </TR>
    <%
        }


        if (allianceRelations.size() > 0) {

    %>
    <TR>
        <TD class="blue" ALIGN="CENTER" width="20%"><%= ML.getMLStr("diplomacy_lbl_alliance", userId)%></TD>
        <TD class="blue" ALIGN="CENTER"  BGCOLOR="GRAY"  width="5%"><B></B></TD>
        <TD class="blue" ALIGN="CENTER" width="25%"><%= ML.getMLStr("diplomacy_lbl_status", userId)%> </TD>
        <TD class="blue" ALIGN="CENTER"  BGCOLOR="GRAY"  width="5%"><B></B></TD>
        <TD class="blue" ALIGN="CENTER" width="20%"><%= ML.getMLStr("diplomacy_lbl_alliance", userId)%></TD>
        <TD class="blue" ALIGN="CENTER" width="25%"><%= ML.getMLStr("diplomacy_lbl_higherrelation", userId) %></TD>
    </TR>


    <%
            for (DiplomacyRelation ar : allianceRelations) {


    %>
    <TR>
        <TD ALIGN="CENTER" ><%= a.getName()%>
        </TD>
        <TD ALIGN="CENTER" ><B><=</B></TD>

        <TD ALIGN="CENTER" ><%= ML.getMLStr(DiplomacyService.findDiplomacyTypeById(ar.getDiplomacyTypeId()).getName(), userId)%>
        </TD>
        <TD ALIGN="CENTER" ><B>=></B></TD>
        <TD ALIGN="CENTER" >
            <%= DiplomacyService.findAllianceNameById(ar.getToId())%>
        </TD>
        <TD>

        </TD>
    </TR>

    <%
            }

        }
        if (allianceUserRelations.size() > 0) {
    %>

    <TR class="blue2">

        <TD class="blue" ALIGN="CENTER" width="20%"><%= ML.getMLStr("diplomacy_lbl_alliance", userId)%></TD>
        <TD class="blue" ALIGN="CENTER"  BGCOLOR="GRAY"  width="5%"><B></B></TD>
        <TD class="blue" ALIGN="CENTER" width="25%"><%= ML.getMLStr("diplomacy_lbl_status", userId)%> </TD>
        <TD class="blue" ALIGN="CENTER"  BGCOLOR="GRAY"  width="5%"><B></B></TD>
        <TD class="blue" ALIGN="CENTER" width="20%"><%= ML.getMLStr("diplomacy_lbl_player", userId)%></TD>
        <TD class="blue" ALIGN="CENTER" width="25%"><%= ML.getMLStr("diplomacy_lbl_higherrelation", userId) %> </TD>
    </TR>
    <%

            for (DiplomacyRelation uar : allianceUserRelations) {

    %>
    <TR>
        <TD ALIGN="CENTER" ><%= a.getName()%>
        </TD>
        <TD ALIGN="CENTER" ><B><=</B></TD>

        <TD ALIGN="CENTER" ><%= ML.getMLStr(DiplomacyService.findDiplomacyTypeById(uar.getDiplomacyTypeId()).getName(), userId)%>
        </TD>
        <TD ALIGN="CENTER" ><B>=></B></TD>
        <TD ALIGN="CENTER" >
            <%= DiplomacyService.findUserById(uar.getToId()).getGameName()%>
        </TD>
        <TD>
        </TD>
    </TR>
    <%
                        }
             %>
             <BR>
        <%

                    }

    %>
</TABLE>
<BR>
    <% } %>
    <BR/><BR/><BR/>
<%
            }


%>
<%
            /*
             * USER TO USER
             *
             *
             * */

            if (type == TYPE_SHOWUSERRELATIONS) {
                String allianceTag = "";

                ArrayList<DiplomacyRelation> userRelations = new ArrayList<DiplomacyRelation>();
                ArrayList<DiplomacyRelation> userAllianceRelations = new ArrayList<DiplomacyRelation>();
%>
<FORM METHOD="POST" name="userForm">
    <SELECT id="alliances" name="users" onChange="window.location=document.userForm.users.options[document.userForm.users.selectedIndex].value">
        <OPTION value="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWUSERRELATIONS%>&userId=<%= OPTION_ALLUSERS%>">< <%= ML.getMLStr("diplomacy_opt_all", userId)%> ></OPTION>
        <%
                for (Integer i : DiplomacyService.findUsersToDisplay(userId)) {
                    User u = DiplomacyService.findUserById(i);
                    allianceTag = "";
                    if (DiplomacyService.findAllianceMemberByUserId(u.getUserId()) != null) {
                        allianceTag = "[" +
                                DiplomacyService.findAllianceById(DiplomacyService.findAllianceMemberByUserId(u.getUserId()).getAllianceId()).getTag() +
                                "]";
                    }
                    userRelations = DiplomacyService.findRelationByFromId(u.getUserId(), EDiplomacyRelationType.USER_TO_USER, false);
                     userAllianceRelations = DiplomacyService.findRelationByFromId(u.getUserId(), EDiplomacyRelationType.USER_TO_ALLIANCE, false);
                    if (userRelations.size() <= 0 && userAllianceRelations.size() <= 0) {
                        continue;
                    }

                    if (u.getUserId() == dUserId) {
        %>
        <OPTION SELECTED value="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWUSERRELATIONS%>&userId=<%= u.getUserId()%>"><%= u.getGameName()%></OPTION>
        <%} else {
        %>
        <OPTION value="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWUSERRELATIONS%>&userId=<%= u.getUserId()%>"><%= u.getGameName()%></OPTION>
        <%
                    }

                }

        %>
    </SELECT>
</FORM>
<BR/>
<%

                for (User u : users) {

                    userRelations = DiplomacyService.findRelationByFromId(u.getUserId(), EDiplomacyRelationType.USER_TO_USER, false);

                    userAllianceRelations = DiplomacyService.findRelationByFromId(u.getUserId(), EDiplomacyRelationType.USER_TO_ALLIANCE, false);
                    allianceTag = "";
                    if (DiplomacyService.findAllianceMemberByUserId(u.getUserId()) != null) {
                        allianceTag = "[" +
                                DiplomacyService.findAllianceById(DiplomacyService.findAllianceMemberByUserId(u.getUserId()).getAllianceId()).getTag() +
                                "]";
                    }
                    if (userRelations.size() <= 0 && userAllianceRelations.size() <= 0) {
                        continue;

                    } else {
%>
<TABLE  class="blue" width ="80%">
    <TR>
        <TD ALIGN="CENTER" width="80%"class="blue2" COLSPAN="6"><B><%= u.getGameName()%> <%= allianceTag%></B></TD>
    </TR>
    <%
                    }

                    if (userRelations.size() > 0) {

    %>
    <TR>
        <TD class="blue" ALIGN="CENTER" width="20%"><%= ML.getMLStr("diplomacy_lbl_player", userId)%></TD>
        <TD class="blue" ALIGN="CENTER"  BGCOLOR="GRAY"  width="5%"><B></B></TD>
        <TD class="blue" ALIGN="CENTER" width="25%"><%= ML.getMLStr("diplomacy_lbl_status", userId)%> </TD>
        <TD class="blue" ALIGN="CENTER"  BGCOLOR="GRAY"  width="5%"><B></B></TD>
        <TD class="blue" ALIGN="CENTER" width="20%"><%= ML.getMLStr("diplomacy_lbl_player", userId)%></TD>
        <TD class="blue" ALIGN="CENTER" width="25%"><%= ML.getMLStr("diplomacy_lbl_higherrelation", userId) %> </TD>
    </TR>


    <%

            for (DiplomacyRelation ur : userRelations) {
                allianceTag = "";
                if (DiplomacyService.findAllianceMemberByUserId(ur.getToId()) != null) {
                    allianceTag = "[" +
                            DiplomacyService.findAllianceById(DiplomacyService.findAllianceMemberByUserId(ur.getToId()).getAllianceId()).getTag() +
                            "]";
                }

    %>  <TR>


        <TD ALIGN="CENTER" >
            <%= u.getGameName()%>
        </TD>
        <TD ALIGN="CENTER" ><B><=</B>
        </TD>
        <TD ALIGN="CENTER">
            <%= ML.getMLStr(DiplomacyService.findDiplomacyTypeById(ur.getDiplomacyTypeId()).getName(), userId)%>
        </TD>
        <TD ALIGN="CENTER" ><B>=></B>
        <TD ALIGN="CENTER" >
            <%= DiplomacyService.findUserById(ur.getToId()).getGameName()%> <%= allianceTag%>
        </TD>
        <TD ALIGN="center">
            <%= DiplomacyService.findHigherThan(ur, userId)%>
        </TD>
    </TR>
    <%
                        }

                    }

//#####
//#####
//##### UserToAlliance / AllianceTOUser
//#####
//#####
                    if (userAllianceRelations.size() > 0) {
    %>
</TR>
<TR>
    <TD class="blue" ALIGN="CENTER" width="20%"><%= ML.getMLStr("diplomacy_lbl_player", userId)%></TD>
    <TD class="blue" ALIGN="CENTER"  BGCOLOR="GRAY"  width="5%"><B></B></TD>
    <TD class="blue" ALIGN="CENTER" width="25%"><%= ML.getMLStr("diplomacy_lbl_status", userId)%> </TD>
    <TD class="blue" ALIGN="CENTER"  BGCOLOR="GRAY"  width="5%"><B></B></TD>
    <TD class="blue" ALIGN="CENTER" width="30%"><%= ML.getMLStr("diplomacy_lbl_alliance", userId)%> </TD>
    <TD class="blue" ALIGN="CENTER" width="30%"><%= ML.getMLStr("diplomacy_lbl_higherrelation", userId) %> </TD>
</TR>
<%

            for (DiplomacyRelation uar : userAllianceRelations) {
%>  <TR>
    <%

    %>

    <TD ALIGN="CENTER" >
        <%= u.getGameName()%>
    </TD>
    <TD ALIGN="CENTER" ><B><= </B>
    </TD>
    <TD ALIGN="CENTER" ><%= ML.getMLStr(DiplomacyService.findDiplomacyTypeById(uar.getDiplomacyTypeId()).getName(), userId)%>
    </TD>
    <TD ALIGN="CENTER" ><B>=></B>

    <TD ALIGN="CENTER">
        <%= DiplomacyService.findAllianceById(uar.getToId()).getName()%>
    </TD>
    <TD ALIGN="center">
        <%= DiplomacyService.findHigherThan(uar, userId)%>
    </TD>
</TR>
<%
                        }
                    }
%>
</TABLE><BR/><BR/><BR/>
<%
                }%>
<BR/><BR/>
<%
            }
%>
