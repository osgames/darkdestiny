<%@page import="at.darkdestiny.core.development.DevelopmentUtilities"%>
<%@page import="at.darkdestiny.core.planetcalc.EconomyChart"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="org.jfree.chart.ChartUtilities"%>
<%@page import="org.jfree.chart.ChartRenderingInfo"%>
<%@page import="org.jfree.chart.JFreeChart"%>
<%@page import="at.darkdestiny.core.enumeration.EPlanetRessourceType"%>
<%@page import="at.darkdestiny.core.model.Ressource"%>
<%@page import="at.darkdestiny.core.html.OptionField"%>
<%@page import="at.darkdestiny.core.model.PlayerCategory"%>
<%@page import="at.darkdestiny.core.service.CategoryService"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.core.FormatUtilities"%>
<%@page import="at.darkdestiny.core.GameConfig"%>
<%@page import="at.darkdestiny.core.enumeration.EColonyType"%>
<%@page import="at.darkdestiny.core.planetcalc.ExtPlanetCalcResult"%>
<%@page import="at.darkdestiny.core.planetcalc.ExtPlanetCalcResult"%>
<%@page import="at.darkdestiny.core.planetcalc.ProductionResult"%>
<%@page import="at.darkdestiny.core.planetcalc.PlanetCalculation"%>
<%@page import="at.darkdestiny.core.databuffer.fleet.RelativeCoordinate"%>
<%@page import="at.darkdestiny.core.utilities.PlanetUtilities"%>
<%@page import="at.darkdestiny.core.model.PlayerPlanet"%>
<%@page import="at.darkdestiny.core.service.OverviewService"%>
<%@page import="at.darkdestiny.core.model.Planet"%>
<%@page import="at.darkdestiny.core.ML"%>
<%@page import="at.darkdestiny.core.service.ManagementService"%>
<%@page import="at.darkdestiny.core.result.BaseResult"%>
<%@page import="java.util.*"%>
<%@page import="java.text.NumberFormat"%>
<%
final int PROCESS_RENAME = 1;
final int PROCESS_ABANDON = 2;
final int PROCESS_TRANSFERPLANET = 3;
final int PROCESS_SETRESSLIMITS = 4;
final int PROCESS_SETPRIORITIES = 5;
final int PROCESS_SETTAXES = 6;
final int PROCESS_SETTINGS = 7;

ArrayList<String> results = new ArrayList<String>();

session.setAttribute("actPage", "new/management_1");
session = request.getSession();
String planetID = (String) session.getAttribute("actPlanet");

int userId = Integer.parseInt((String) session.getAttribute("userId"));
int systemId = Integer.parseInt((String) session.getAttribute("actSystem"));
int planetId = Integer.parseInt(planetID);

// *********************************************************************
// ***************************** PROCESS ACTIONS START
// *********************************************************************
int process = 0;
if (request.getParameter("process") != null) {
    process = Integer.parseInt((String)request.getParameter("process"));
}

BaseResult br = null;

switch (process) {
    case(PROCESS_RENAME):
        br = ManagementService.renamePlanSys(request.getParameterMap(), userId, planetId, systemId);
%>
<jsp:forward page="/main.jsp?page=new/management_1">
    <jsp:param name="process" value="0"/>
    <jsp:param name="result" value="<%= br.getFormattedString() %>"/>
</jsp:forward>
<%        
        break;
    case(PROCESS_ABANDON):
        ManagementService.dismantleColony(userId, planetId);
        break;
    case(PROCESS_TRANSFERPLANET):
        int targetUserId = 0;
        if (request.getParameter("transferTo") != null) {
            targetUserId = Integer.parseInt(request.getParameter("transferTo"));
        }

        br = PlanetUtilities.createTransferPlanetVote(userId, targetUserId, planetId);
        break;
    case(PROCESS_SETRESSLIMITS):
        ManagementService.changeLimits(request.getParameterMap(), userId, planetId);
        break;
    case(PROCESS_SETPRIORITIES):
        ManagementService.updatePriorities(request.getParameterMap(), planetId);
        break;          
    case(PROCESS_SETTAXES):
        ManagementService.changeTaxes(request.getParameterMap(), userId, planetId);
        break;  
    case(PROCESS_SETTINGS):
        System.out.println(ManagementService.changePlanetProperties(request.getParameterMap(), userId, planetId).getMessage());
        break;          
}

// *********************************************************************
// ***************************** PROCESS ACTIONS END
// *********************************************************************

Locale loc = ML.getLocale(userId);

Planet planet = OverviewService.findPlanetById(planetId);
PlayerPlanet pPlanet = OverviewService.findPlayerPlanetByPlanetId(planetId);
String planetName = pPlanet.getName();

at.darkdestiny.core.model.System system = ManagementService.getSystemForPlanet(planetId);
RelativeCoordinate rc = new RelativeCoordinate(system.getId(),planet.getId());

PlanetCalculation pc = null;
ProductionResult productionResult = null;
ExtPlanetCalcResult epcr = null;
if (pPlanet != null) {
    pc = OverviewService.PlanetCalculation(planetId);
    productionResult = pc.getPlanetCalcData().getProductionData();
    epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
}

// CHART TEST
String imageMap = "";
String imageMap2 = "";

java.lang.System.out.println("GOT HERE 1");

try {
    EconomyChart ec = new EconomyChart(epcr,userId);
    
    JFreeChart chart1 = ec.getSurfaceChart();
    if (session.getAttribute("chart1") != null) {
        session.removeAttribute("chart1");
    }
    session.setAttribute("chart1", chart1);
    // get ImageMap
    ChartRenderingInfo info = new ChartRenderingInfo();
    // populate the info
    if (chart1 != null) {
        chart1.createBufferedImage(480, 320, info);
        imageMap = ChartUtilities.getImageMap("map", info);
    } else {
        
    }
    
    JFreeChart chart2 = ec.getWorkerChart();
    if (session.getAttribute("chart2") != null) {
        session.removeAttribute("chart2");
    }
    session.setAttribute("chart2", chart2);
    // get ImageMap
    ChartRenderingInfo info2 = new ChartRenderingInfo();
    // populate the info
    if (chart2 != null) {
        chart2.createBufferedImage(480, 320, info2);
        imageMap2 = ChartUtilities.getImageMap("map2", info2);
    }    
} catch(Exception e){
    DebugBuffer.writeStackTrace("Error in Statistik jsp", e);
}

java.lang.System.out.println("GOT HERE 2");

%>
<%= imageMap%>
<%
// CHART TEST

int action = 0;
if (request.getParameter("action") != null) {
    action = Integer.parseInt((String)request.getParameter("action"));
}

String MLPlanetTypeStr = "overview_lbl_planet";
if (pPlanet.getColonyType() == EColonyType.MINING_COLONY) {
    MLPlanetTypeStr = "overview_lbl_miningplanet";
}
%>
<BR>
<TABLE class="blue" width="80%" cellpadding="0" cellspacing="0">
<TR><TD>
<TABLE width="100%" cellpadding="0" cellspacing="0">
    <TR><TD width="50%" align=center class="blue2"><B><%=ML.getMLStr(MLPlanetTypeStr, userId)%> <%= planetName %> (#<%= planet.getId() %>)</B></TD></TR>
<% if ((pPlanet != null) && pPlanet.isHomeSystem()) { %>
<TR><TD width="50%" align=center class="blue2"><B><%= ML.getMLStr("overview_lbl_homesystem", userId)%></B></TD></TR>
<% } %>
</TABLE>
<TABLE style="font-size:13px;" width="100%" cellpadding="0" cellspacing="0">
<TR>
<TD width="200" rowspan="2"><IMG src="GetPlanetPic?planetId=<%= planetId %>" width="150" height="150" /></TD>
<TD valign="top">
<%
String pop = "systemsearch_pop_"+planet.getLandType();
%>
<FONT size="+3"><%=ML.getMLStr(MLPlanetTypeStr, userId)%> <%= planetName %></FONT><BR>
<FONT size="+2">System <%= system.getName() %></FONT><BR>
Koordinate <%= rc.getSystemId() %>:<%= rc.getPlanetId() %><BR>
</TD>
</TR>
<TR><TD>
<TABLE width="100%" cellpadding="0" cellspacing="0">
    <TR>
        <TD valign="bottom" width="150">
            <TABLE width="100%" cellpadding="0" cellspacing="0">
                <TR style="cursor:pointer;" onClick="window.location = 'main.jsp?page=new/management_1&action=1'"><TD valign="middle">
                        <IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/abandonColony.jpg"/>
                    </TD>
                    <TD valign="middle">
                        <%= ML.getMLStr("management_lbl_disband",userId) %>
                    </TD>
                </TR>
            </TABLE>
        </TD>
        <TD valign="bottom" width="150">
            <TABLE width="100%" cellpadding="0" cellspacing="0">
                <TR style="cursor:pointer;" onClick="window.location = 'main.jsp?page=new/management_1&action=2'"><TD valign="middle">
                        <IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/renameColony.jpg"/>
                    </TD>
                    <TD valign="middle">
                        <%= ML.getMLStr("management_lbl_rename",userId) %>
                    </TD></A>
                </TR>
            </TABLE>            
        </TD>
        <TD valign="bottom" width="175">
            <TABLE width="100%" cellpadding="0" cellspacing="0">
<%
if (PlanetUtilities.getAllFormerPlanetOwners(userId, planetId).size() > 0) {
%>              
                <TR style="cursor:pointer;" onClick="window.location = 'main.jsp?page=new/management_1&action=3'"><TD valign="middle">                        
                        <IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/freeColony.jpg"/>
                    </TD>
                    <TD valign="middle">
                        Planet übergeben
                    </TD>
                </TR>
<%
} else {
%>
                <TR><TD valign="middle">                        
                        <IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/freeColony_gray.jpg"/>
                    </TD>
                    <TD valign="middle">
                        <%= ML.getMLStr("management_lbl_transfer",userId) %>
                    </TD>
                </TR>
<%
} 
%>             
            </TABLE>
        </TD>
        <TD valign="bottom" width="170">
            <TABLE width="100%" cellpadding="0" cellspacing="0">
                <TR style="cursor:pointer;" onClick="window.location = 'main.jsp?page=new/management_1&action=4'"><TD valign="middle">
                        <IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/settingsColony.jpg"/>
                    </TD>
                    <TD valign="middle">
                        <%= ML.getMLStr("management_lbl_settings",userId) %>
                    </TD>
                </TR>
            </TABLE>
        </TD>        
        <TD width="*">
            &nbsp;
        </TD>
    </TR>
</TABLE>
</TD></TR>
</TABLE>
</TD>
</TR>
</TABLE>
<BR>
<!-- START ACTIONS -->
<%
if (br != null) {
    if (br.isError()) {
%>
<TABLE width="80%" style="font-size: 13px;background-color:lightred;">
    <TR><TD align="center"><FONT color="black"><B><%= br.getMessage() %></B></FONT><BR></TD></TR>
</TABLE>
<%
    } else {
%>
<TABLE width="80%" style="font-size: 13px;background-color:lightgreen;">
    <TR><TD align="center"><FONT color="black"><B><%= br.getMessage() %></B></FONT><BR></TD></TR>
</TABLE>
<%
    }
}

switch (action) {
    case(1): // Aufgeben
        if (pPlanet.getPopulation() > 0) {
%>
<TABLE class="gold" width="300" style="font-size: 12px;">
    <FORM method="post" action="main.jsp?page=new/management_1&process=<%= PROCESS_ABANDON %>" Name="Production">
        <TR>
            <TD class="gold" align="center" colspan="2"><B><%= ML.getMLStr("overview_lbl_dismantlecolony", userId)%></B></TD>
        </TR>
        <TR>
            <TD align="center" colspan="2"><B><%= ML.getMLStr("overview_lbl_dismantlemsg", userId)%></B></TD>
        </TR>        
        <TR>
            <TD align="center">
                <%
                        if ((ManagementService.userDAO.findById(userId).isGuest()) || pPlanet.isHomeSystem()) {%>
                <INPUT class="dark" <% if (pPlanet.getDismantle()) {%>CHECKED<% }%> disabled name="dismantle" TYPE="checkbox" />
                <%      } else {%>
                <INPUT class="dark" <% if (pPlanet.getDismantle()) {%>CHECKED<% }%> name="dismantle" TYPE="checkbox" />
                <%      }
                %>
            </TD>
            <TD align="center">
                <% if ((ManagementService.userDAO.findById(userId).isGuest()) || pPlanet.isHomeSystem()) {%>
                <INPUT class="dark" style="color:gray;" disabled type="submit" name="Ok" value="Ok">
                <% } else {%>
                <INPUT class="dark" type="submit" name="Ok" value="Ok">
                <% }%>
            </TD>
    </FORM>
    <% if (ManagementService.abadonable(userId, planetId)) {%>
    <FORM method="post" action="main.jsp?page=new/management_1&process=<%= PROCESS_ABANDON %>" Name="Production">
        <TR>
            <TD colspan="4">
                <INPUT class="darklarge" type="submit" name="Ok" value="<%= ML.getMLStr("management_abadoncolony", userId)%>" />
            </TD>
        </TR>
    </FORM>
    <% }
                    }%>
</TABLE>
<%
        break;        
    case(2): // Umbenennen
%>        
<FORM method="post" action="main.jsp?page=new/management_1&process=<%= PROCESS_RENAME %>" Name="Rename">
    <TABLE class="gold" width="300" style="font-size: 12px;">
        <TR><TD colspan=5 class="gold" align=center><B><%= ML.getMLStr("management_lbl_renameplanetsystem", userId)%></B></TD></TR>
        <TR><TD width="90"><%= ML.getMLStr("management_lbl_planet", userId)%></TD><TD><INPUT class="darklarge" name="planetname" value="<%= ManagementService.getPlanetName(planetId)%>" size="25" maxlength='25'></TD></TR>
        <TR><TD><%= ML.getMLStr("management_lbl_system", userId)%></TD><TD><INPUT class="darklarge" name="systemname" value="<%= ManagementService.getSystemNamefromPlanetId(planetId)%> "  size="25" maxlength='25'></TD></TR>

        <TR><TD colspan="2" align=center height="100%"><BR><CENTER><INPUT class="dark" type="submit" name="Speichern" value="<%= ML.getMLStr("management_but_change", userId)%>"></CENTER></TD></TR>
    </TABLE>
</FORM>        
<%                            
        break;
    case(3): // Befreien
%>        
<FORM method="post" action="main.jsp?page=new/management_1&process=<%= PROCESS_TRANSFERPLANET %>" Name="Rename">
    <TABLE class="gold" width="300" style="font-size: 12px;">
        <TR><TD class="gold" align=center><B><%= ML.getMLStr("management_lbl_transferplanet", userId)%></B></TD></TR>
        <TR>
            <TD align=center>
                <%= ML.getMLStr("management_lbl_transferplanet_msg", userId)%>
            </TD>        
        </TR>
        <TR>
            <TD align=center>
                <BR><SELECT id="transferTo" name="transferTo" class="dark">
                    <%
                    for (User u : PlanetUtilities.getAllFormerPlanetOwners(userId, planetId)) {
                    %>
                    <OPTION value='<%= u.getUserId() %>'> <%= u.getGameName() %></OPTION>
                    <%
                    }
                    %>
                </SELECT>                
            </TD>
        </TR>
        <TR><TD align=center height="100%"><BR><CENTER><INPUT class="dark" type="submit" name="&Uuml;bergeben" value="<%= ML.getMLStr("management_but_moveplanet", userId)%>"></CENTER></TD></TR>        
    </TABLE>
</FORM>              
<%    
        break;
    case(4): // Einstellungen
%>
<FORM method="post" action="main.jsp?page=new/management_1&process=<%= PROCESS_SETTINGS %>" Name="Rename">
    <TABLE class="gold" width="400" style="font-size: 12px;">
        <TR><TD colspan=3 class="gold" align=center><B><%= ML.getMLStr("management_lbl_categorisation", userId)%></B></TD></TR>
        <TR><TD>
            <TABLE width="100%" style="font-size: 12px;">
                <TR><TD colspan="3"><CENTER>
                            <SELECT name="category" class="dark">
                                <OPTION value='0'/> <%= ML.getMLStr("category_opt_nocategory", userId)%>

                                <%
                                                    int tmpCategoryId = CategoryService.getCategoryIdFromPlanetId(planetId);
                                                    for (PlayerCategory pcTmp : CategoryService.getAllCategorysByUserId(userId)) {
                                %>
                                <OPTION <%= (pcTmp.getId() == tmpCategoryId) ? "SELECTED" : ""%> value='<%= pcTmp.getId()%>'/> <%=pcTmp.getName()%>
                                <%
                                                    }
                                %>
                            </SELECT>
                        </CENTER></TD></TR>
                <TR>
                    <TD align=center>
                        <A href="main.jsp?page=new/categorys"><FONT style="font-size:13"><%= ML.getMLStr("management_link_manage", userId)%></FONT></A>
                    </TD>
                </TR>
            </TABLE>                
            </TD>
            <TD width="20">&nbsp</TD>
            <TD>                
                    <TABLE style="font-size: 12px;">

                        <TR>
                            <TD><%= ML.getMLStr("overview_lbl_setinvisible", userId)%></TD>
                        </TR><TR>
                            <TD>
                                <%
                                        if (!pPlanet.getInvisibleFlag()) {%>
                                <INPUT id="visibleState" name="visibleState" TYPE="checkbox" />
                                <%      } else {%>
                                <INPUT id="visibleState" name="visibleState" CHECKED TYPE="checkbox" />
                                <%      }%>
                            </TD>
                        </TR>
                    </TABLE>              
            </TD></TR>
                <TR>
                    <TD colspan="3" align=center><BR><INPUT class="dark" type="submit" name="change" value="<%= ML.getMLStr("management_but_change", userId)%>"></TD>
                </TR>        
    </TABLE>
</FORM>           
<%    
        break;
}
%>
<!-- END ACTIONS -->
<BR>
<TABLE width="80%" cellpadding="0" cellspacing="0">
<TR>
    <TD>
        <TABLE width="100%" cellpadding="2" cellspacing="5">
            <TR>
                <TD width="50%" height="100%" valign="top">
                    <TABLE class="blue" width="100%" height="100%" cellpadding="0" cellspacing="0">
                        <TR class="blue2"><TD height="20px" style="padding-left:5px;">
                        <FONT style="font-size:18px;">Wirtschaft</FONT>
                        </TD></TR>
                        <TR><TD>
                            <FORM method="post" action="main.jsp?page=new/management_1&process=<%= PROCESS_SETPRIORITIES %>" Name="Production">                                
                            <FONT style="font-size:16px;">Produktionsprioriäten<BR></FONT>
                            <TABLE style="font-size:13px;" width="100%" cellpadding="0" cellspacing="0">
                                <THEAD>
                                <TD>Ind. Zweig</TD>
                                <TD>Priorität</TD>
                                <TD>Akt. Effizienz</TD>
                                </THEAD>
                                <TR><TD><%= ML.getMLStr("management_lbl_industry", userId)%></TD><TD>
                                        <%
                                                            OptionField optionField;
                                                            optionField = new OptionField(out, "priorityI");
                                                            optionField.addEntry(0, ML.getMLStr("management_opt_high", userId));
                                                            optionField.addEntry(1, ML.getMLStr("management_opt_average", userId));
                                                            optionField.addEntry(2, ML.getMLStr("management_opt_low", userId));

                                                            optionField.setSelected(pPlanet.getPriorityIndustry());
                                                            optionField.setClass("dark");
                                                            optionField.output();
                                        %>
                                    </TD>
                                    <TD>
                                        <%= FormatUtilities.getFormattedNumber(epcr.getEffIndustry() * 100d) %> %
                                    </TD>
                                </TR><TR><TD><%= ML.getMLStr("management_lbl_agrar", userId)%></TD><TD>
                                    <%
                                                        optionField = new OptionField(out, "priorityA");
                                                        optionField.addEntry(0, ML.getMLStr("management_opt_high", userId));
                                                        optionField.addEntry(1, ML.getMLStr("management_opt_average", userId));
                                                        optionField.addEntry(2, ML.getMLStr("management_opt_low", userId));

                                                        optionField.setSelected(pPlanet.getPriorityAgriculture());
                                                            optionField.setClass("dark");
                                                        optionField.output();
                                    %>
                                    </TD>
                                    <TD>
                                        <%= FormatUtilities.getFormattedNumber(epcr.getEffAgriculture() * 100d) %> %
                                    </TD>
                                </TR><TR><TD><%= ML.getMLStr("management_lbl_research", userId)%></TD><TD>
                                    <%
                                                        optionField = new OptionField(out, "priorityF");
                                                        optionField.addEntry(0, ML.getMLStr("management_opt_high", userId));
                                                        optionField.addEntry(1, ML.getMLStr("management_opt_average", userId));
                                                        optionField.addEntry(2, ML.getMLStr("management_opt_low", userId));

                                                        optionField.setSelected(pPlanet.getPriorityResearch());
                                                            optionField.setClass("dark");
                                                        optionField.output();
                                    %>
                                    </TD>
                                    <TD>
                                        <%= FormatUtilities.getFormattedNumber(epcr.getEffResearch() * 100d) %> %
                                    </TD>
                                </TR>
                                <TR>
                                    <TD colspan="3" align="center"><BR><INPUT class="dark" type="submit" name="Speichern" value="<%= ML.getMLStr("management_but_change", userId)%>"></TD>
                                </TR>
                            </TABLE>
                            </FORM>
                            <FORM id="taxes" method="post" action="main.jsp?page=new/management_1&process=<%= PROCESS_SETTAXES %>" Name="taxes">
                            <TABLE cellspacing="0" cellpadding="0" border="0">
                                <TR valign="middle">
                                    <TD valign="middle"><FONT style="font-size:16px;">Steuern (</FONT></TD>
                                    <TD valign="middle"><INPUT class="dark" name="tax" value="<%= FormatUtilities.getFormattedNumber(pPlanet.getTax()) %>" size="2" maxlength='3'></TD>
                                    <TD valign="middle"><FONT style="font-size:16px;">%)&nbsp</FONT></TD>
                                    <TD valign="middle"><IMG style="cursor:pointer;" onClick="document.getElementById('taxes').submit();" src="<%= GameConfig.getInstance().picPath() %>pic/refresh.png" /></TD>
                                </TR>
                            </TABLE>
                            </FORM>
                            <TABLE style="font-size:13px;" width="60%" cellpadding="0" cellspacing="0">
                                <TR>
                                    <TD><%= ML.getMLStr("management_lbl_base", userId) %> (+)</TD><TD align="right"><%= FormatUtilities.getFormattedNumber(epcr.getBaseTaxIncome(),loc) %></TD>
                                </TR>
                                <TR>
                                    <TD><%= ML.getMLStr("management_lbl_riots", userId) %> (-)</TD><TD align="right"><%= FormatUtilities.getFormattedNumber(epcr.getTaxRiotDiscount(),loc) %></TD>
                                </TR>
                                <TR>
                                    <TD><%= ML.getMLStr("management_lbl_crime", userId) %> (-)</TD><TD align="right"><%= FormatUtilities.getFormattedNumber(epcr.getTaxCrimDiscount(),loc) %></TD>
                                </TR>
                                <TR>
                                    <TD>Total</TD><TD align="right"><%= FormatUtilities.getFormattedNumber(epcr.getTaxIncome(),loc) %></TD>
                                </TR>
                            </TABLE>
                        </TD></TR>
                    </TABLE>
                </TD>
                <TD width="50%" valign="top">
                    <TABLE class="blue" width="100%" height="100%" cellpadding="0" cellspacing="0">
                        <TR class="blue2"><TD height="20px" style="padding-left:5px;">
                        <FONT style="font-size:18px;"><%= ML.getMLStr("management_lbl_population", userId) %></FONT>
                        </TD></TR>
                        <TR valign="top"><TD>                            
                            <TABLE width="100%" cellpadding="0" cellspacing="0" style="font-size:13px;">
                                <TR>
                                    <TD width="150" valign="top"><%= ML.getMLStr("management_lbl_population", userId) %></TD><TD width="150" valign="top" align="right"><%= FormatUtilities.getFormattedNumber(pPlanet.getPopulation(),loc) %></TD><TD width="*">&nbsp;</TD>
                                </TR>
<%
long migrationValue = pPlanet.getMigration();
String migrationStr = "Einwanderung";

if (migrationValue < 0) {
    migrationStr = "Auswanderung";
    migrationValue = Math.abs(migrationValue);
}

double absGrowth = (pPlanet.getPopulation() / 100d * pPlanet.getGrowth()) / 360d;
%>
                                <TR>
                                    <TD valign="top">Bevölkerungswachstum</TD><TD valign="top" align="right"><%= FormatUtilities.getFormattedDecimal(absGrowth,0,loc) %><BR></TD><TD>&nbsp;</TD>
                                </TR>
                                <TR>
                                    <TD valign="top"><%= migrationStr %></TD><TD valign="top" align="right"><%= FormatUtilities.getFormattedNumber(migrationValue,loc) %><BR><BR></TD><TD>&nbsp;</TD>
                                </TR>
                                <TR>
                                    <TD>Moral</TD><TD align="right"><%= FormatUtilities.getFormattedNumber(epcr.getMorale()) %> % (Max. <%= FormatUtilities.getFormattedNumber(epcr.getMaxMorale()) %>)</TD><TD>&nbsp;</TD>
                                </TR>
                                <TR>
                                    <TD>Moral (Bonus)</TD><TD align="right"><%= FormatUtilities.getFormattedNumber(epcr.getMoraleBonus()) %> %</TD><TD>&nbsp;</TD>
                                </TR>
                                <TR>
                                    <TD>Moral (Nahrung)</TD><TD align="right"><%= FormatUtilities.getFormattedNumber(epcr.getMoralFood()) %> %</TD><TD>&nbsp;</TD>
                                </TR>                                
                                <TR>
                                    <TD valign="top">Moral (Steuern)</TD><TD valign="top" align="right"><%= FormatUtilities.getFormattedNumber(epcr.getMoraleBonusTax()) %> %<BR><BR></TD><TD>&nbsp;</TD>
                                </TR>
                                <TR>
                                    <TD><%= ML.getMLStr("management_lbl_crime", userId) %></TD><TD align="right"><%= FormatUtilities.getFormattedDecimal(epcr.getCriminalityValue(),1,loc) %> %</TD><TD>&nbsp;</TD>
                                </TR>
<%
String vorzeichen = "";
if (Math.signum(epcr.getLoyalityChange().doubleValue()) >= 0) {
    vorzeichen = "+";
}
%>                                               
                                <TR>
                                    <TD>Loyalität</TD><TD align="right"><%= FormatUtilities.getFormattedDecimal(epcr.getLoyalityValue().doubleValue(),1,loc) %> % (<%= vorzeichen + FormatUtilities.getFormattedDecimal(epcr.getLoyalityChange().doubleValue(),2,loc) %>)</TD><TD>&nbsp;</TD>
                                </TR>    
<%
vorzeichen = "";
if (Math.signum(epcr.getRiotValueChange()) >= 0) {
    vorzeichen = "+";
}
%>                              
                                <TR>
                                    <TD>Unruhen</TD><TD align="right"><%= FormatUtilities.getFormattedDecimal(epcr.getRiotValue(),1,loc) %> % (<%= vorzeichen + FormatUtilities.getFormattedDecimal(epcr.getRiotValueChange(),2,loc) %>)</TD><TD>&nbsp;</TD>
                                </TR>
                            </TABLE>
                        </TD>
                        </TR>
                        <TR><TD height="*"><BR/></TD></TR>
                    </TABLE>
                </TD>
            </TR>
            <TR>
                <TD>
<%
ArrayList<Ressource> ress = ManagementService.getMinStockRessources(planetId, productionResult);
int colspanFactor = 1;
if (ress.size() > 1) {
    colspanFactor = 2;
}
%>                    
                    <FONT size="+2"><BR>Lager Mindestmengen<BR><BR></FONT>
                    <FORM method="post" action="main.jsp?page=new/management_1&process=<%= PROCESS_SETRESSLIMITS %>" Name="Production">
                        <TABLE cellpadding="0" cellspacing="0" class="blue" width="100%" style="font-size: 12px;">

                            <TR>
                                <TD class="blue2"></TD>
                                <TD valign="left"  class="blue2"><B><%= ML.getMLStr("management_lbl_ressource", userId)%></B></TD>
                                <TD colspan="3" valign="middle" align="center" class="blue2"><%= ML.getMLStr("management_lbl_minimumstock", userId)%><IMG alt="minimumStorage" onmouseover="doTooltip(event,'<%= ML.getMLStr("management_pop_minimumstorage", userId)%>')" onmouseout="hideTip()" src="<%= GameConfig.getInstance().picPath()%>pic/infoT.png" /></TD>
                            </TR>
                            <TR>
                                <TD colspan="2" class="blue2"></TD>
                                <TD valign="middle" align="center" class="blue2">Produktion</TD>
                                <TD valign="middle" align="center" class="blue2">Transport</TD>
                                <TD valign="middle" align="center" class="blue2">Handel</TD>
                            </TR>
        <%
                            for (Ressource r : ress) {
        %>
                            <TR>
                                <TD width="20px" ><IMG alt="<%= ML.getMLStr(r.getName(), userId)%>" height="20" width="20" src="<%= GameConfig.getInstance().picPath()%><%= r.getImageLocation()%>"/></TD>
                                <TD align="left"><%= ML.getMLStr(r.getName(), userId)%></TD>
                                <TD align="center"><INPUT class="dark" name="r_prod_<%= r.getId()%>" value="<%= ManagementService.findMinStock(planetId, r.getId(), EPlanetRessourceType.MINIMUM_PRODUCTION)%>" size="10" maxlength='10'></TD>
                                <TD align="center"><INPUT class="dark" name="r_trans_<%= r.getId()%>" value="<%= ManagementService.findMinStock(planetId, r.getId(),  EPlanetRessourceType.MINIMUM_TRANSPORT)%>" size="10" maxlength='10'></TD>
                                <TD align="center"><INPUT class="dark" name="r_trade_<%= r.getId()%>" value="<%= ManagementService.findMinStock(planetId, r.getId(), EPlanetRessourceType.MINIMUM_TRADE)%>" size="10" maxlength='10'></TD>

                            </TR>
                            <% }%>
                            <TR align="center"><TD colspan="8" align="center"><BR><INPUT class="dark" type="submit" name="Speichern" value="<%= ML.getMLStr("management_but_change", userId)%>"></TD></TR>
                        </TABLE>
                    </FORM>
                <TD>
            </TR>
<TR>
    <TD>
        <TABLE class="blue">
            <TR>
    <TD colspan="2" class="blue">
        <FONT size="+1"><%= ML.getMLStr("db_ressource_consumables", userId)%></FONT>
    </TD>
</TR>
<%
float coverage = DevelopmentUtilities.getConsSupplyRatio(pc.getPlanetCalcData().getProductionData());
String covStr = FormatUtilities.getFormattedDecimal(coverage * 100, 1, ML.getLocale(userId));
float devPointsByConsumable = pc.getPlanetCalcData().getProductionData().getRessBaseProduction(Ressource.DEV_POINTS) * DevelopmentUtilities.getConsSupplyRatio(pc.getPlanetCalcData().getProductionData());

%>    
<TR>
    <TD width="300" valign="middle">
        <TABLE style="font-size:12px;"><TR><TD>
        <IMG src="./pic/menu/icons/happiness.png"/>
                </TD>
                <TD align="middle">
                    <%= ML.getMLStr("management_lbl_devpoints_text1", userId)%><BR>
                    <%= FormatUtilities.getFormattedDecimal(devPointsByConsumable,2,ML.getLocale(userId)) %> 
                </TD>
            </TR>
        </TABLE>
    </TD>
    <TD align="left" valign="top">
        <%= ML.getMLStr("management_lbl_consumables_needed", userId)%> <%= FormatUtilities.getFormattedNumber(pc.getPlanetCalcData().getProductionData().getRessBaseConsumption(Ressource.CONSUMABLES),ML.getLocale(userId)) %><BR>
        <%= ML.getMLStr("management_lbl_consumables_provided", userId)%> <%= FormatUtilities.getFormattedNumber(pc.getPlanetCalcData().getProductionData().getRessConsumption(Ressource.CONSUMABLES),ML.getLocale(userId)) %><BR>
        <%= ML.getMLStr("management_lbl_coverage", userId)%> <%= covStr %>%<BR>
    </TD>
</TR>
        </TABLE>
    </TD>
</TR>            
        </TABLE>
    </TD>
</TR>
<TR><TD>&nbsp;</TD></TR>
<TR>
    <TD>
        <IMG src="ChartViewer?chartAttributeName=chart1&x=480&y=300" usemap="#map" />&nbsp;&nbsp;
        <IMG src="ChartViewer?chartAttributeName=chart2&x=480&y=300" usemap="#map2" /></TD>
    </TD>    
</TR>
</TABLE>

