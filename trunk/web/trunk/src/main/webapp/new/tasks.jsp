<%@page import="at.darkdestiny.core.model.Title" %>
<%@page import="at.darkdestiny.core.model.Campaign" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.view.*" %>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="at.darkdestiny.core.GameConfig" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="java.util.*" %>
<%
            int userId = Integer.parseInt((String) session.getAttribute("userId"));

            final int TYPE_SHOW_TASKS = 1;
            final int TYPE_SHOW_TITLES = 2;
            final int TYPE_SHOW_INFO = 3;

            final int PROCESS_SET_TITLE = 1;

            int type = TYPE_SHOW_TASKS;
            int process = 0;
            int titleStatus = 1;
            int campaignId = 0;
            campaignId = TaskService.findLastCampaign(userId);
            ETitleType titleType = null;

            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            if (request.getParameter("titleType") != null && !request.getParameter("titleType").equals("null")
                    && !request.getParameter("titleType").equals("")) {
                titleType = ETitleType.valueOf(request.getParameter("titleType"));
            }
            if (request.getParameter("titleStatus") != null) {
                titleStatus = Integer.parseInt(request.getParameter("titleStatus"));
            }
            if (request.getParameter("campaignId") != null) {
                campaignId = Integer.parseInt(request.getParameter("campaignId"));
            }
            if(process > 0){
                switch(process){
                    case(PROCESS_SET_TITLE):{
                 int titleId = Integer.parseInt(request.getParameter("titleId"));
                        TaskService.setTitle(userId, titleId);
                        type = TYPE_SHOW_TITLES;
                        break;
                        }
                    }
                }


%>
<TABLE width="80%">
    <TR align="center">
        <TD width="33%"><U><A href="main.jsp?page=new/tasks&type=<%= TYPE_SHOW_TASKS%>"><%= ML.getMLStr("tasks_link_tasks", userId)%></A></U></TD>
        <TD width="33%"><U><A href="main.jsp?page=new/tasks&type=<%= TYPE_SHOW_TITLES%>"><%= ML.getMLStr("tasks_link_titles", userId)%></A></U></TD>
        <TD width="33%"><U><A href="main.jsp?page=new/tasks&type=<%= TYPE_SHOW_INFO%>"><%= ML.getMLStr("tasks_link_info", userId)%></A></U></TD>

    </TR>
</TABLE><BR>
<%
            switch (type) {
                case (TYPE_SHOW_TASKS): {

%>
<TABLE border="0" align="center" style="font-size: 13px" width="80%">
    <TR>
        <TD>
            <TABLE cellpadding="0" cellspacing="0" style="font-size: 13px" align="left" width="100%">
                <TR>
                    <TD width="60%" align="left">
                        <TABLE cellpadding="0" cellspacing="0" style="font-size: 13px" width="100%">
                            <TD align="left" class="blue2">
                                <%= ML.getMLStr("tasks_lbl_campaign", userId)%> :
                            </TD>
                            <TD align="left">
                                <SELECT id="campaignId" name="campaignId" style='background-color: #000000; color: #ffffff; border:0px; width: 250px; font-size: 12px;' onChange="window.location.href = 'main.jsp?page=new/tasks&campaignId='+document.getElementById('campaignId').value+'&titleStatus='+document.getElementById('titleStatus').value+'&titleType='+document.getElementById('titleType').value">
                                    <OPTION value="<%= 0%>"><%= ML.getMLStr("tasks_opt_all", userId)%></OPTION>
                                    <% for (Campaign c : (ArrayList<Campaign>) Service.campaignDAO.findAllOrdered()) {%>
                                    <OPTION <% if (c.getId() == campaignId) {%> SELECTED <% }%> value="<%= c.getId()%>"><%= ML.getMLStr(c.getName(), userId)%></OPTION>
                                    <% }%>
                                </SELECT>
                            </TD>
                        </TABLE>
                    </TD>
                    <TD width="20%" align="left">
                        <TABLE cellpadding="0" cellspacing="0" style="font-size: 13px" width="100%">
                            <TD align="left" class="blue2">
                                <%= ML.getMLStr("task_lbl_type", userId)%> :
                            </TD>
                            <TD align="left">
                                <SELECT id="titleType" name="titleType" style='background-color: #000000; color: #ffffff; border:0px; width: 100px; font-size: 12px;' onChange="window.location.href = 'main.jsp?page=new/tasks&titleStatus=<%= titleStatus%>&titleType='+document.getElementById('titleType').value+'&campaignId='+document.getElementById('campaignId').value">
                                    <OPTION value="null"><%= ML.getMLStr("task_opt_all", userId)%></OPTION>
                                    <% for (ETitleType e : ETitleType.values()) {%>
                                    <OPTION <% if (e == titleType) {%> SELECTED <% }%> value="<%= e.toString()%>"><%= ML.getMLStr("task_opt_" + e.toString(), userId)%></OPTION>
                                    <% }%>
                                </SELECT>
                            </TD>
                        </TABLE>
                    </TD>
                    <TD width="20%" align="left">
                        <TABLE cellpadding="0" cellspacing="0" style="font-size: 13px" width="100%">
                            <TD align="left" class="blue2">
                                <%= ML.getMLStr("tasks_lbl_status", userId)%> :
                            </TD>
                            <TD align="left">
                                <SELECT id="titleStatus" name="titleStatus" style='background-color: #000000; color: #ffffff; border:0px; width: 100px; font-size: 12px;' onChange="window.location.href = 'main.jsp?page=new/tasks&titleType=<%= titleType%>&titleStatus='+document.getElementById('titleStatus').value+'&campaignId='+document.getElementById('campaignId').value">
                                    <OPTION <% if (titleStatus == 0) {%> SELECTED <% }%> value="0"><%= ML.getMLStr("task_opt_all", userId)%></OPTION>
                                    <OPTION <% if (titleStatus == 1) {%> SELECTED <% }%> value="1"><%= ML.getMLStr("task_opt_open", userId)%></OPTION>
                                    <OPTION <% if (titleStatus == 2) {%> SELECTED <% }%> value="2"><%= ML.getMLStr("task_opt_done", userId)%></OPTION>
                                </SELECT>
                            </TD>
                        </TABLE>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
</TABLE>
<TABLE style="font-size: 13px" align="center" width="80%">
    <TR class="blue2">
        <TD width="20px">
        </TD>
        <TD align="center" width="100px">
            <B><%= ML.getMLStr("task_lbl_difficulty", userId)%></B>
        </TD>
        <TD align="center" width="130px">
            <B><%= ML.getMLStr("task_lbl_title", userId)%></B>
        </TD>
        <TD align="center">
            <B><%= ML.getMLStr("task_lbl_description", userId)%></B>
        </TD>
        <% %>
        <TD align="center" width="250px">
            <B><%= ML.getMLStr("task_lbl_conditions", userId)%></B>
        </TD>
    </TR>
    <%
    boolean showInvisible = false;
    if(request.getParameter("invisible") != null){
        showInvisible = true;
    }
                        for (Title title : (ArrayList<Title>) TaskService.getTitlesSorted(userId, campaignId, titleType, titleStatus, showInvisible)) {
                            String color = "#000000";
                            color = TaskService.TypeToColor(title.getType());
                            ArrayList<ViewCondition> conds = TaskService.getConditionsOrdered(title.getId());
    %>
    <TR style="background-color: <%= color%>;">
        <TD style="border-top: 1px solid #222222;" valign="top">
            <% if (TaskService.hasTitle(userId, title.getId())) {%>
            <IMG alt="Check" onmouseover="doTooltip(event,'<%= ML.getMLStr("tasks_pop_fulfilled", userId) %>')" onmouseout="hideTip()"  src="<%= GameConfig.getInstance().picPath()%>pic/checked.png" />
            <% } else {%>
            <IMG alt="Check" onmouseover="doTooltip(event,'<%= ML.getMLStr("tasks_pop_notfulfilled", userId) %>')" onmouseout="hideTip()"  src="<%= GameConfig.getInstance().picPath()%>pic/unchecked.png" />
            <% }%>
        </TD>
        <TD style="border-top: 1px solid #222222;" valign="top">
            <%= TaskService.difficultyToStars(title.getDifficulty())%>
        </TD>
        <TD style="border-top: 1px solid #222222;" align="center" valign="top">
            <B><%= ML.getMLStr((title.getName() + "_" + Service.userDAO.findById(userId).getGender().toString()), userId)%></B>
        </TD>
        <% if (conds.size() > 0) {%>
        <TD style="border-top: 1px solid #222222;" valign="top">
            <%= ML.getMLStr(title.getDescription(), userId)%>
        </TD>
        <TD style="border-top: 1px solid #222222;" valign="top">
            <TABLE style="font-size: 12px" align="center" width="100%">
                <% for (ViewCondition vc : conds) {%>
                <TR>
                    <TD width="20px">
                        <% if (TaskService.hasCondition(userId, title.getId(), vc.getConditionToTitle().getId())) {%>
                        <IMG alt="Check" onmouseover="doTooltip(event,'<%= ML.getMLStr("tasks_pop_fulfilled", userId) %>')" onmouseout="hideTip()"  src="<%= GameConfig.getInstance().picPath()%>pic/checked.png" />
                        <% } else {%>
                        <IMG alt="Check" onmouseover="doTooltip(event,'<%= ML.getMLStr("tasks_pop_notfulfilled", userId) %>')" onmouseout="hideTip()"  src="<%= GameConfig.getInstance().picPath()%>pic/unchecked.png" />
                        <% }%>
                    </TD>
                    <TD>
                        <%= ML.getMLStr(vc.getConditionToTitle().getDescription(), userId)%>
                    </TD>
                </TR>
                <% }%>
            </TABLE>
        </TD>
        <% } else {%>
        <TD style="border-top: 1px solid #222222;" valign="top" colspan="2">
            <%= ML.getMLStr(title.getDescription(), userId)%>
        </TD>
        <% }%>

    </TR>
    <% }%>
</TABLE>

<% break;
                }
                case (TYPE_SHOW_TITLES): {
%>
<TABLE class="blue" style="font-size: 13px" width="80%">
    <TR>
        <TD class="blue2" align="center">
            <%= ML.getMLStr("tasks_lbl_actualtitle", userId) %> :
        </TD>
        <TD align="center">
            <B><%= TitleService.findTitle(userId, userId) %></B>
        </TD>
        <TD align="center" colspan="2">
            <A href="main.jsp?page=new/tasks&process=<%= PROCESS_SET_TITLE %>&titleId=0" ><%= ML.getMLStr("tasks_link_deletetitle", userId)%></A>
        </TD>
    </TR>
    <TR class="blue2">
        <TD align="center">
            <B><%= ML.getMLStr("tasks_lbl_title", userId)%></B>
        </TD>
        <TD align="center">
            <B><%= ML.getMLStr("tasks_lbl_difficulty", userId)%></B>
        </TD>
        <TD>
            <B><%= ML.getMLStr("task_lbl_actions", userId)%></B>
        </TD>
    </TR>
    <% for (Title t : TaskService.findTitlesByUserId(userId)) {%>
    <TR>
        <TD align="center">
            <%= ML.getMLStr((t.getName() + "_" + Service.userDAO.findById(userId).getGender().toString()), userId)%>
        </TD>
        <TD align="left">
            <%= TaskService.difficultyToStars(t.getDifficulty())%>
        </TD>
        <TD>
            <A href="main.jsp?page=new/tasks&process=<%= PROCESS_SET_TITLE %>&titleId=<%= t.getId()%>" ><%= ML.getMLStr("tasks_link_settitle", userId)%></A>
        </TD>
    </TR>
    <% }%>
</TABLE>

<% break;
                }

                case (TYPE_SHOW_INFO): {

%>
<TABLE width="80%">
    <TR>
        <TD style="font-size: 13px;" align="center">
          Das Aufgaben- und Titelsystem stellt eine Art Guideline f�r Anf�nger dar. Mit jedem Tick werden
          die Vorraussetzungen f�r einen Titel �berpr�ft und upgedatet. Titel und Aufgaben selbst haben
          keinen Einflu� auf den Spielverlauf, das hei�t sie bringen dem Spieler keinen Bonus, Vorteil oder
          Sonstiges.
          <BR><BR>
          Der Titel den der Spieler setzt wird in der Statistik angezeigt
        </TD>
    </TR>
</TABLE>
<%




                    break;
                    }

            }%>