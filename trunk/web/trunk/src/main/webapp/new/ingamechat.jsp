<%-- 
    Document   : ingamechat
    Created on : 06.11.2014, 18:27:58
    Author     : Stefan
--%>
<%@page import="at.darkdestiny.core.chat.JSChatUtilities"%>
<%@page import="java.util.ArrayList"%>
<%
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    String basePath = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);    
    JSChatUtilities.setUnreadTimeStamp(userId);        
%>
<SCRIPT language="javascript" type="text/javascript">
    var basePath = '<%= basePath %>';
    var refresh = setInterval("refreshChat(basePath);", 5000);
    
    document.getElementById("unread").innerHTML = '0';
    document.getElementById("unread").style.color = "lightgrey";
</SCRIPT>
<BR>
<TABLE width="80%" style="table-layout:fixed;" class="trans">
    <TR>
        <TD style="width:150px;white-space:nowrap;overflow: hidden;" valign="top">
            <P id="users">
<%
            ArrayList<String> users = JSChatUtilities.getUsers();
            out.write("<B>Benutzer:</B><BR><BR>");
            
            for (String name : users) {
                out.write(name + "<BR>");
            }
%>                
            </P>
        </TD>
        <TD style="width:10px;">
            &nbsp;            
        </TD>        
        <TD valign="top">
            <P id="textcontent">
<%
            ArrayList<String> messages = JSChatUtilities.getLast30Messages();
            for (String msg : messages) {
                out.write(msg + "<BR>");
            }
%>
            </P>
        </TD>
    </TR>
    <TR>
        <TD>
            &nbsp;
        </TD>
    </TR>    
    <TR>
        <TD colspan="3">
            Text:  <INPUT onkeypress="document.getElementById('newmsg').onkeypress = function(e) {
                if (!e) e = window.event;
                var keyCode = e.keyCode || e.which;
                if (keyCode == '13') {
                // Enter pressed
                // alert('Before send');
                sendMsg(document.getElementById('newmsg'),basePath);
                return false;
            }
  }" value="" id="newmsg" type="text" size="100" maxlength="150" />
        </TD>
    </TR>
</TABLE>
