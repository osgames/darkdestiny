
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.result.AllianceBoardResult"%>
<%@page import="at.darkdestiny.core.GenerateMessage"%>
<%@page import="at.darkdestiny.core.model.AllianceMember"%>
<%@page import="at.darkdestiny.core.service.AllianceBoardService"%>
<%@page import="at.darkdestiny.core.model.AllianceMessage"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="at.darkdestiny.core.service.IdToNameService"%>
<%@page import="at.darkdestiny.core.GameConfig"%>
<%@page import="at.darkdestiny.core.ML"%>
<%@page import="at.darkdestiny.core.enumeration.EAllianceBoardPermissionType"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.core.model.Alliance"%>
<%@page import="at.darkdestiny.core.model.AllianceBoardPermission"%>
<%@page import="at.darkdestiny.core.result.AlliancePermissionResult"%>
<%@page import="at.darkdestiny.core.model.AllianceBoard"%>
<%@page import="at.darkdestiny.core.service.AllianceService"%>
<%

    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));


// VIEW CONSTANTS
final int TYPE_SHOWMEMBERS = 1;
final int TYPE_EDITPROFILE = 2;
final int TYPE_LEAVEALLIANCE = 3;
final int TYPE_CREATEMESSAGE = 4;
final int TYPE_CREATEALLIANCE = 5;
final int TYPE_DELETEMESSAGE = 6;
final int TYPE_DELETEALLIANCE = 7;
final int TYPE_CREATETREATY = 8;
final int TYPE_LEAVETREATY = 9;
final int TYPE_EDITRANKS = 10;
final int TYPE_SHOWALLIANCEBOARD = 11;
final int TYPE_MESSAGES = 12;
final int TYPE_RECRUITING = 13;
final int TYPE_MANAGEALLIANCEBOARD = 14;
final int TYPE_CREATEALLIANCEBOARD = 15;
final int TYPE_EDITUSERRANK = 16;
final int TYPE_DELETEUSER = 17;
final int TYPE_JOINALLIANCE = 19;
final int TYPE_DELETETREATY = 20;
final int TYPE_SHOWSTRUCTURE = 21;
final int TYPE_SHOWDESCRIPTION = 22;
final int TYPE_SETBOARDPERMISSIONS = 23;
// PROCESS CONSTANTS
final int PROCESS_SAVEPROFILE = 1;
final int PROCESS_CREATETREATY = 2;
final int PROCESS_LEAVETREATY = 3;
final int PROCESS_CREATEALLIANCE = 4;
final int PROCESS_DELETEALLIANCE = 5;
final int PROCESS_EDITRANKS = 6;
final int PROCESS_SETUSERRANK = 7;
final int PROCESS_SENDMESSAGES = 8;
final int PROCESS_SENDMESSAGETOUSER = 9;
final int PROCESS_DELETEUSER = 10;
final int PROCESS_EDITUSERRANK = 11;
final int PROCESS_DELETERANK = 12;
final int PROCESS_CREATEALLIANCEMESSAGE = 13;
final int PROCESS_DELETEALLIANCEMESSAGE = 14;
final int PROCESS_SWITCHRECRUITING = 15;
final int PROCESS_CREATEALLIANCEBOARD = 16;
final int PROCESS_LEAVEALLIANCE = 17;
final int PROCESS_DELETEALLIANCEBOARD = 18;
final int PROCESS_JOINALLIANCE = 19;
final int PROCESS_DELETETREATY = 20;


    int boardId = -1;
    if (request.getParameter("boardId") != null) {
        boardId = Integer.parseInt(request.getParameter("boardId"));
    }

    System.out.println("Board Id is " + boardId);

    int process = 0;
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }
    if (process == 999) {
        AllianceBoardService.updatePermissions(userId, boardId, request.getParameterMap());
    }



    int type = 0;
    if (request.getParameter("type") != null) {
        type = Integer.parseInt(request.getParameter("type"));
    }
    AllianceMember allianceMember = AllianceService.findAllianceMemberByUserId(userId);
    boolean userHasAlliance = false;
    Alliance alliance = null;
    if (allianceMember != null) {
        userHasAlliance = true;
        alliance = AllianceService.findAllianceByAllianceId(allianceMember.getAllianceId());
    }
    /*
     * ###################
     * # Allianznachrichten anzeigen
     * ######################
     *
     * */
    int messagesPerSite = 15;
    int from = 0;
    int to = 15;

    if (type == TYPE_SHOWALLIANCEBOARD) {

        if (request.getParameter("from") != null) {
            from = Integer.parseInt(request.getParameter("from"));
        }
        if (request.getParameter("to") != null) {
            to = Integer.parseInt(request.getParameter("to"));
        }

        AllianceBoardResult abr = AllianceBoardService.getBoardViewData(userId, boardId);
        AllianceBoardPermission userPermission = abr.getUserPermission();
        boardId = userPermission.getAllianceBoardId();
        ArrayList<AllianceMessage> messages = abr.getMessages();
%>
<TABLE width="80%">
    <TR>
        <TD valign="top">
            <TABLE class="bluetrans"  width="100%">
                <TR>
                    <TD WIDTH="33%" ALIGN="LEFT"><% if(userPermission.getWrite()){ %><A HREF='main.jsp?page=new/alliance&type=<%= TYPE_CREATEMESSAGE%>&boardId=<%= boardId%>'><%= ML.getMLStr("alliance_link_createalliancemessage", userId)%></A> <% }else{ %>[Keine Schreibrechte]<% } %></TD>
                    <TD WIDTH="33%" ALIGN="CENTER"><A HREF='main.jsp?page=new/alliance'><%= ML.getMLStr("alliance_link_back", userId)%></A></TD>
                    <TD WIDTH="33%" ALIGN="CENTER"><FONT STYLE="font-size:13px; font-weight:bold;"><%= ML.getMLStr("alliance_lbl_allianceboard", userId)%>:</FONT></TD>
                </TR>
                <TR>
                    <TD COLSPAN="3" ALIGN="RIGHT"><FORM METHOD="POST" NAME="boardForm">
                        <TABLE width="100%"><TR><TD width="*">&nbsp</TD>
<%
                            if (from == 0) {
                                AllianceBoardService.updateLastRead(userId, boardId);
                            }
                            if (AllianceBoardService.newMessagesAvailable(userId)) {
%>
                            <TD width="70"><IMG alt="new message" src="<%= GameConfig.getInstance().picPath() %>pic/newBoardMessage.gif" /></TD>
<%
                            } else { %>
                            <TD width="70">&nbsp;</TD>
<%                          }
%>
                            <TD width="150"><SELECT style='background-color: #000000; color: #ffffff; width:350px; height:17px; border:0px; text-align: center;' name="boardSelect" onChange="window.location=document.boardForm.boardSelect.options[document.boardForm.boardSelect.selectedIndex].value">

                                  <%
                    for (AllianceBoard ab : abr.getBoards().values()) {

                %>
                <OPTION <% if(boardId == ab.getId()){ %>SELECTED<% } %> VALUE='main.jsp?page=new/alliance&type=<%= TYPE_SHOWALLIANCEBOARD%>&boardId=<%= ab.getId() %>'>
                            <% if (ab.getId() == AllianceBoard.TREATY_BOARD_ID){%>
                            <%= ML.getMLStr(AllianceBoardService.findAllianceBoardById(ab.getId()).getName(), userId)%> (<%= abr.getUnreadBoardMsgCount().get(AllianceBoard.TREATY_BOARD_ID) %>)
                            <%} else if(ab.getId() == AllianceBoard.ALLIANCE_BOARD_ID) {%>
                            <%= ML.getMLStr(AllianceBoardService.findAllianceBoardById(ab.getId()).getName(), userId)%> (<%= abr.getUnreadBoardMsgCount().get(AllianceBoard.ALLIANCE_BOARD_ID) %>)
                            <%} else {%>
                            <%= ab.getName()%> (<%= abr.getUnreadBoardMsgCount().get(ab.getId()) %>)
                            <%}%>

                </OPTION>
                <% }%>
                            </SELECT>
                            </TD>
                            </TR>
                        </TABLE>
                        </FORM>
                    </TD>
                </TR>
                <TR>
                    <TD colspan="3">
                        <FONT  style="font-size:18px;"><B>
                            <% if (boardId == AllianceBoard.TREATY_BOARD_ID || boardId == AllianceBoard.ALLIANCE_BOARD_ID) {%>
                            <%= ML.getMLStr(AllianceBoardService.findAllianceBoardById(boardId).getName(), userId)%>
                            <%} else {%>
                            <%= AllianceBoardService.findAllianceBoardById(boardId).getName()%>
                            <%}%>
                        </B></FONT>
                    </TD>
                </TR>
                <TR>
                    <TD COLSPAN="3"><% if (!AllianceBoardService.findAllianceBoardById(boardId).getDescription().equals("")) {%>
                        <%= ML.getMLStr("alliance_lbl_content", userId)%>:<%}%> <FONT STYLE="font-size:14px; font-weight:bold;"><%= AllianceBoardService.findAllianceBoardById(boardId).getDescription()%></FONT></TD>
                </TR>
                <TR>
                    <TD colspan="3" width="100%" align="left"><B>
                        <%
                        boolean first = true;
                        for(AllianceBoardPermission abp : abr.getBoardPermissions()){
                        String name = "Unbekannt";
                        if(abp.getType().equals(EAllianceBoardPermissionType.USER)){
                            User u = Service.userDAO.findById(abp.getRefId());
                            if(u != null){
                                name = u.getGameName();;
                            }
                                name.trim();
                            if(first){
                                first = false;
                            }else{
                                name = ", " + name;
                            }
                        }
                        %><%= name %><% } %></B>
                    </TD>
                </TR>
                <%
                    for (int i = 0; i < messages.size(); i++) {
                        if (i < from) {
                            continue;
                        }
                        if (i > to) {
                            break;
                        }

                        java.util.Date msgDate = new java.util.Date(messages.get(i).getPostTime());
                        String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate) + " " + java.text.DateFormat.getTimeInstance().format(msgDate);

                        String userName = "";
                        String allianceTag = "";
                        if (messages.get(i).getUserId() == null || messages.get(i).getUserId() == 0) {

                            userName = ML.getMLStr("alliance_msg_senderdeleted", userId);
                        } else {
                            try {
                                userName = AllianceService.findUserById(messages.get(i).getUserId()).getGameName();
                                AllianceMember tmpAm = AllianceService.findAllianceMemberByUserId(messages.get(i).getUserId());
                                if(tmpAm != null){
                                allianceTag = AllianceService.findAllianceByAllianceId(tmpAm.getAllianceId()).getTag();
                                                               }
                            } catch (Exception e) {
                                userName = ML.getMLStr("alliance_msg_senderdeleted", userId);
                            }
                        }

                        String color = "white";
                        if (userHasAlliance && (messages.get(i).getAllianceId() != alliance.getId())) {
                            color = "#B9B9FD";
                        }

                %>

                <TR>
                    <TD colspan="3">
                        <TABLE class='blue' style='font-size:13px' border='0' cellpadding='0' cellspacing='2' width='100%'>
                            <TR>
                                <TD class='blue2'>
                                    <TABLE style='font-size:13px' border='0' cellpadding='0' cellspacing='0' width='100%'>
                                        <TR valign="middle">
                                            <TD ALIGN="LEFT" nowrap width='20%'><%= ML.getMLStr("alliance_lbl_writtenby", userId)%> : <B><%= userName%>&nbsp;

                                                    <%
                               if (AllianceService.findAllianceMemberByUserId(messages.get(i).getUserId()) != null) {
                                   if (userHasAlliance && (alliance.getId() != AllianceService.findAllianceMemberByUserId(messages.get(i).getUserId()).getAllianceId())) {%>
                                                    <FONT color="<%= color%>"> [<%= allianceTag%>]</FONT>
                                                    <% }
                                                        }
                                                    %>
                                                </B></TD>

                                            <TD ALIGN="CENTER" nowrap width='65%'><B><%= messages.get(i).getTopic()%></B></TD>
                                            <TD align="right" valign="middle" width="15px">
                                                <% if (messages.get(i).getUserId() != userId) {%>
                                                <a href="main.jsp?page=new/messages&type=3&topic=<%= messages.get(i).getTopic()%>&targetUserId=<%= messages.get(i).getUserId()%>">
                                                    <IMG style="vertical-align:middle" border="0" SRC="<%= GameConfig.getInstance().picPath() + "pic/message.jpg"%>" ALT="alliance_alt_sendprivatemessage"/>
                                                </a>
                                                <% }%>
                                            </TD>
                                            <TD nowrap width='10%' align='right' valign='center'>&nbsp;<%= msgDateString%></TD>
                                            <%

                                            %>
                                            <TD width='5pt' valign='middle'>
                                                <%

                                                %> <% if(userPermission.getDelete()){ %><A href='main.jsp?page=new/alliance&process=<%= PROCESS_DELETEALLIANCEMESSAGE%>&from=<%= from%>&to=<%= to%>&boardId=<%= boardId%>&messageId=<%= messages.get(i).getPostTime()%>'><IMG border='0' onmouseover="doTooltip(event,'<%= ML.getMLStr("alliance_pop_deletemessage", userId)%>')" onmouseout="hideTip()" src='<%= GameConfig.getInstance().picPath() + "pic/cancel_message.gif"%>' width='18' height='18'></A> <% } %>

                                            </TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD><%= GenerateMessage.parseText(messages.get(i).getMessage())%>
                                    <BR><BR>
                                </TD>
                            </TR>
                        </TABLE>
                        <BR/>
                        <%
                            }
                            //  @TODO RÜckschreiben
                        %>

                <TR>
                    <TD align="center" colspan="3">

                        <!-- LINKS RECHTS NAVIGATION -->
                        <% if(messages.size() > 15){ %>
                        <TABLE width="100%">
                            <TR ALIGN="center">
                                <%
                                    if (from > 1) {
                                %>
                                <TD><A HREF="main.jsp?page=new/alliance&type=<%= TYPE_SHOWALLIANCEBOARD%>&boardId=<%= boardId%>&from=<%=(from - messagesPerSite)%>&to=<%= (to - messagesPerSite)%>"><<<%=(from - messagesPerSite)%>-<%= from%></A></TD>
                                <%}
                                %>
                                <TD>&nbsp;&nbsp;&nbsp;</TD>
                                <TD> <A HREF="main.jsp?page=new/alliance&type=<%= TYPE_SHOWALLIANCEBOARD%>&boardId=<%= boardId%>&from=<%=(from + messagesPerSite)%>&to=<%= (to + messagesPerSite)%>"><%= (to)%>-<%= (to + messagesPerSite)%>>></A></TD>
                            </TR>
                        </TABLE>
                            <% } %>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
</TABLE>
<% }

/*
 * ###################
 * # Allianznachricht erstellen
 * ######################
 *
 * */
// Logger.getLogger().write("DEBUG 18");
if(type == TYPE_CREATEMESSAGE){
        AllianceBoardResult abr = AllianceBoardService.getBoardViewData(userId, boardId);
        AllianceBoardPermission userPermission = abr.getUserPermission();
        boardId = userPermission.getAllianceBoardId();
%>
        <FORM method="post" action="main.jsp?page=new/alliance&process=<%= PROCESS_CREATEALLIANCEMESSAGE%>">
            <TABLE width="80%" style="font-size:13px">
                <TR><TD align="center" colspan=4><%= ML.getMLStr("alliance_lbl_htmlallowed",userId)%><P><BR></TD></TR>
                <TR>
                    <TD><%= ML.getMLStr("alliance_lbl_topic",userId)%></TD><TD><input type="text" name="topicAMessage" size=50 maxlength="50"></TD>
                    <TD><%= ML.getMLStr("alliance_lbl_allianceboard",userId)%>:
                        <SELECT NAME="boardId">

                            <%
                           for(Map.Entry<Integer, AllianceBoard> ab : abr.getBoards().entrySet()){
                                String name = ab.getValue().getName();
                                if(ab.getKey() == AllianceBoard.ALLIANCE_BOARD_ID || ab.getKey() == AllianceBoard.TREATY_BOARD_ID){
                                    name = ML.getMLStr(ab.getValue().getName(), userId);
                                }
                            %>
                            <OPTION <% if(boardId == ab.getValue().getId()){%> SELECTED <%}%>
                            VALUE="<%= ab.getValue().getId() %>"><%= name %></OPTION>
                            <%}

                           %>
                        </SELECT>
                    </TD>

                </TR>
                <TR><TD><%= ML.getMLStr("alliance_lbl_message",userId)%></TD><TD><p><textarea rows="4" name="message" cols="38"></textarea></p></TD></TR>
                <TR><TD align="center" colspan=4><P><BR><input value=<%= ML.getMLStr("alliance_but_submit",userId)%> type=submit></TD></TR>
            </TABLE>
        </FORM>
<%
}
    %>

