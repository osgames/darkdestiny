<%@page import="at.darkdestiny.core.view.DiplomacyManagementView"%>
<%@page import="at.darkdestiny.core.result.BaseResult"%>
<%@page import="at.darkdestiny.core.diplomacy.DiplomacyRelationExt"%>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="at.darkdestiny.core.GameConfig" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.service.DiplomacyService" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>


<%

            session = request.getSession();
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int dUserId = -1;
            int uaUserId = -1;
            int type = 0;
            int process = 0;
            int allianceId = 0;
            int aaAllianceId = -1;
            int uaAllianceId = -1;
            int userAllianceId = -1;

            DiplomacyManagementView dmv = DiplomacyService.getDiplomacyManagementView(userId);

            EDiplomacyRelationType dipType = null;

            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }

            if (request.getParameter("alliance") != null) {
                allianceId = Integer.parseInt(request.getParameter("alliance"));
            }
            if (request.getParameter("UAalliance") != null) {
                uaAllianceId = Integer.parseInt(request.getParameter("UAalliance"));
            }
            if (request.getParameter("AAallianceId") != null) {
                aaAllianceId = Integer.parseInt(request.getParameter("AAallianceId"));
            }
            if (request.getParameter("userId") != null) {
                dUserId = Integer.parseInt(request.getParameter("userId"));
            }
            if (request.getParameter("userIdUA") != null) {
                uaUserId = Integer.parseInt(request.getParameter("userIdUA"));
            }
            if (request.getParameter("dipType") != null) {
                dipType = EDiplomacyRelationType.valueOf(request.getParameter("dipType"));
            }
            final int PROCESS_CREATEUSERRELATION = 25;
            final int PROCESS_CREATEUSERALLIANCERELATION = 36;
            final int PROCESS_CREATEALLIANCERELATION = 84;
            final int PROCESS_CREATEALLIANCEUSERRELATION = 88;
            final int PROCESS_UPDATERELATIONS = 2451;

            final int TYPE_MANAGERELATIONS = 2;

            final int TYPE_SHOWALLINACERELATIONS = 1;
            final int TYPE_SHOWUSERRELATIONS = 4;
            final int TYPE_SHOWOVERVIEW = 9;
            final int TYPE_CREATEALLIANCERELATION = 13;
            final int TYPE_CREATEUSERRELATION = 10;
            final int TYPE_CREATEUSERALLIANCERELATION = 11;
            final int TYPE_CREATEALLIANCEUSERRELATION = 12;
            final int TYPE_UPDATEUSERRELATION = 21;
            final int TYPE_UPDATEUSERALLIANCERELATION = 21;
            final int TYPE_UPDATEALLIANCEUSERRELATION = 22;
            final int TYPE_UPDATEALLIANCERELATION = 23;

            AllianceMember am = DiplomacyService.findAllianceMemberByUserId(userId);
            if (am != null) {
                allianceId = am.getAllianceId();
            }
 BaseResult br = null;
            String message = "";
            switch (process) {
                case (PROCESS_UPDATERELATIONS):
                    br = DiplomacyService.updateRelations(userId, request.getParameterMap());
%>

<%
                    type = TYPE_MANAGERELATIONS;
                    break;

                case (PROCESS_CREATEUSERRELATION):
                    int diplomacyTypeU = 0;
                    int toUser = 0;
                    Map<String, String[]> pmap = request.getParameterMap();
                    for (final Map.Entry<String, String[]> entry : pmap.entrySet()) {
                        String parName = entry.getKey();
                        if (parName.equalsIgnoreCase("dPlayer")) {
                            toUser = Integer.parseInt(entry.getValue()[0]);
                        }
                        if (parName.equalsIgnoreCase("dType")) {
                            diplomacyTypeU = Integer.parseInt(entry.getValue()[0]);
                        }
                        if (parName.equalsIgnoreCase("message")) {
                            message = String.valueOf(entry.getValue()[0]);
                        }
                    }
                    if (message == null) {
                        message = "";
                    }
                    DiplomacyService.createUserRelationVote(userId, toUser, diplomacyTypeU, message);

                    type = TYPE_MANAGERELATIONS;
                    break;
                case (PROCESS_CREATEUSERALLIANCERELATION):
                    int diplomacyTypeA = 0;
                    int toAllianceA = 0;
                    Map<String, String[]> pmapa = request.getParameterMap();
                    for (final Map.Entry<String, String[]> entry : pmapa.entrySet()) {
                        String parName = entry.getKey();
                        if (parName.equalsIgnoreCase("dAlliance")) {
                            toAllianceA = Integer.parseInt(entry.getValue()[0]);
                        }
                        if (parName.equalsIgnoreCase("dType")) {
                            diplomacyTypeA = Integer.parseInt(entry.getValue()[0]);
                        }
                        if (parName.equalsIgnoreCase("message")) {
                            message = String.valueOf(entry.getValue()[0]);
                        }
                    }
                    if (message == null) {
                        message = "";
                    }

                    DiplomacyService.createUserAllianceRelationVote(userId, toAllianceA, diplomacyTypeA, message);
                    type = TYPE_MANAGERELATIONS;
                    break;
                case (PROCESS_CREATEALLIANCERELATION):
                    int diplomacyTypeAA = 0;
                    int toAllianceAA = 0;
                    Map<String, String[]> pmapAA = request.getParameterMap();
                    for (final Map.Entry<String, String[]> entry : pmapAA.entrySet()) {
                        String parName = entry.getKey();
                        if (parName.equalsIgnoreCase("dAlliance")) {
                            toAllianceAA = Integer.parseInt(entry.getValue()[0]);
                        }
                        if (parName.equalsIgnoreCase("dType")) {
                            diplomacyTypeAA = Integer.parseInt(entry.getValue()[0]);
                        }
                        if (parName.equalsIgnoreCase("message")) {
                            message = String.valueOf(entry.getValue()[0]);
                        }
                    }
                    if (message == null) {
                        message = "";
                    }

                    DiplomacyService.createAllianceRelationVote(userId, allianceId, toAllianceAA, diplomacyTypeAA, message);
                    type = TYPE_MANAGERELATIONS;
                    break;
                case (PROCESS_CREATEALLIANCEUSERRELATION):

                    diplomacyTypeU = 0;
                    toUser = 0;
                    pmap = request.getParameterMap();
                    for (final Map.Entry<String, String[]> entry : pmap.entrySet()) {
                        String parName = entry.getKey();
                        if (parName.equalsIgnoreCase("dPlayer")) {
                            toUser = Integer.parseInt(entry.getValue()[0]);
                        }
                        if (parName.equalsIgnoreCase("dType")) {
                            diplomacyTypeU = Integer.parseInt(entry.getValue()[0]);
                        }
                        if (parName.equalsIgnoreCase("message")) {
                            message = String.valueOf(entry.getValue()[0]);
                        }
                    }
                    if (message == null) {
                        message = "";
                    }
                    DiplomacyService.createAllianceUserRelationVote(allianceId, userId, toUser, diplomacyTypeU, message);

                    type = TYPE_MANAGERELATIONS;
                    break;


            }

%>
<TABLE width="80%">
    <TR ALIGN="Center">
        <TD width="25%"><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWOVERVIEW%>"><%= ML.getMLStr("diplomacy_link_main", userId)%></A></TD>
        <TD width="25%"><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWALLINACERELATIONS%>"><%= ML.getMLStr("diplomacy_link_alliances", userId)%></A></TD>
        <TD width="25%"><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relations&type=<%= TYPE_SHOWUSERRELATIONS%>"><%= ML.getMLStr("diplomacy_link_players", userId)%></A></TD>
        <TD width="25%"><A href="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_MANAGERELATIONS%>"><%= ML.getMLStr("diplomacy_link_management", userId)%></A></TD>
    </TR>
</TABLE>
    <% if(br != null){ %>
    <B>
        <% if(br.isError()){ %>
        <FONT color="RED"><%= br.getMessage()%></FONT>
        <% }else{ %>
        <FONT color="GREEN"><%= br.getMessage()%></FONT>
        <% } %>
    </B>
<%}
//### FORM USER TO USER
            if (type == TYPE_CREATEUSERRELATION) {
%>

<FORM  method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&process=<%= PROCESS_CREATEUSERRELATION%>">
    <INPUT type="hidden" name="dType" value="<%= Integer.parseInt(request.getParameter("dType"))%>">
    <INPUT type="hidden" name="dPlayer" value="<%= Integer.parseInt(request.getParameter("dPlayer"))%>">
    <TABLE width="80%"><TR>
            <TD valign="top">An:</TD>
            <TD valign="top"><input disabled size="30" name="playername" value="<%= DiplomacyService.findUserById(Integer.parseInt(request.getParameter("dPlayer"))).getGameName()%>" type="text"/></TD>
        </TR>
        <TR>
            <TD valign="top"><%= ML.getMLStr("messages_lbl_subject", userId)%>:</TD>
            <TD valign="top"><input disabled size="30" name="subject" value="Betreff: <%= ML.getMLStr(DiplomacyService.findDiplomacyTypeById(Integer.parseInt(request.getParameter("dType"))).getName(), userId)%>" type="text"/></TD>
        </TR>

        <TR style="font-size:12px">
            <TD valign="top"><%= ML.getMLStr("messages_lbl_text", userId)%>:</TD>
            <TD valign="top"><textarea name="message" cols="30" rows="10"></textarea><BR>
                <BR><TABLE class="blue" width="100%">
                    <TR><TD width="50%">:) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_smile.gif"/></TD><TD>;) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_wink.gif"/></TD></TR>
                    <TR><TD width="50%">:p <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_razz.gif"/></TD><TD>:D <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_mrgreen.gif"/></TD></TR>
                    <TR><TD width="50%">8) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_cool.gif"/></TD><TD>&nbsp;</TD></TR>
                </TABLE>
            </TD>
        <TR>
            <TD colspan="2" align="center">
                <input type="submit" value="<%= ML.getMLStr("messages_but_send", userId)%>">
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
            }


//### FORM USER TO ALLIANCE
            if (type == TYPE_CREATEUSERALLIANCERELATION) {
%>
<FORM  method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&process=<%= PROCESS_CREATEUSERALLIANCERELATION%>">
    <INPUT type="hidden" name="dType" value="<%= Integer.parseInt(request.getParameter("dType"))%>">
    <INPUT type="hidden" name="dAlliance" value="<%= Integer.parseInt(request.getParameter("dAlliance"))%>">
    <%
    %>
    <TABLE width="80%"><TR>
            <TD valign="top">An:</TD>
            <TD valign="top"><input disabled size="30" name="allianceName" value="<%= DiplomacyService.findAllianceById(Integer.parseInt(request.getParameter("dAlliance"))).getName()%>" type="text"/></TD>
        </TR>
        <TR>
            <TD valign="top"><%= ML.getMLStr("messages_lbl_subject", userId)%>:</TD>
            <TD valign="top"><input disabled size="30" name="subject" value="Betreff: <%= ML.getMLStr(DiplomacyService.findDiplomacyTypeById(Integer.parseInt(request.getParameter("dType"))).getName(), userId)%>" type="text"/></TD>
        </TR>

        <TR style="font-size:12px">
            <TD valign="top"><%= ML.getMLStr("messages_lbl_text", userId)%>:</TD>
            <TD valign="top"><textarea name="message" cols="30" rows="10"></textarea><BR>
                <BR><TABLE class="blue" width="100%">
                    <TR><TD width="50%">:) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_smile.gif"/></TD><TD>;) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_wink.gif"/></TD></TR>
                    <TR><TD width="50%">:p <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_razz.gif"/></TD><TD>:D <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_mrgreen.gif"/></TD></TR>
                    <TR><TD width="50%">8) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_cool.gif"/></TD><TD>&nbsp;</TD></TR>
                </TABLE>
            </TD>
        <TR>
            <TD colspan="2" align="center">
                <input type="submit" value="<%= ML.getMLStr("messages_but_send", userId)%>">
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
            }
//### FORM ALLIANCE TO USER
            if (type == TYPE_CREATEALLIANCEUSERRELATION) {
%>

<FORM  method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&process=<%= PROCESS_CREATEALLIANCEUSERRELATION%>">
    <INPUT type="hidden" name="dType" value="<%= Integer.parseInt(request.getParameter("dType"))%>">
    <INPUT type="hidden" name="dPlayer" value="<%= Integer.parseInt(request.getParameter("uaPlayer"))%>">
    <TABLE width="80%"><TR>
            <TD valign="top">An:</TD>
            <TD valign="top"><input disabled size="30" name="playername" value="<%= DiplomacyService.findUserById(Integer.parseInt(request.getParameter("uaPlayer"))).getGameName()%>" type="text"/></TD>
        </TR>
        <TR>
            <TD valign="top"><%= ML.getMLStr("messages_lbl_subject", userId)%>:</TD>
            <TD valign="top"><input disabled size="30" name="subject" value="Betreff: <%= ML.getMLStr(DiplomacyService.findDiplomacyTypeById(Integer.parseInt(request.getParameter("dType"))).getName(), userId)%>" type="text"/></TD>
        </TR>

        <TR style="font-size:12px">
            <TD valign="top"><%= ML.getMLStr("messages_lbl_text", userId)%>:</TD>
            <TD valign="top"><textarea name="message" cols="30" rows="10"></textarea><BR>
                <BR><TABLE class="blue" width="100%">
                    <TR><TD width="50%">:) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_smile.gif"/></TD><TD>;) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_wink.gif"/></TD></TR>
                    <TR><TD width="50%">:p <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_razz.gif"/></TD><TD>:D <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_mrgreen.gif"/></TD></TR>
                    <TR><TD width="50%">8) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_cool.gif"/></TD><TD>&nbsp;</TD></TR>
                </TABLE>
            </TD>
        <TR>
            <TD colspan="2" align="center">
                <input type="submit" value="<%= ML.getMLStr("messages_but_send", userId)%>">
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
            }

//### FORM ALLIANCE TO ALLIANCE
            if (type == TYPE_CREATEALLIANCERELATION) {
%>
<FORM  method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&process=<%= PROCESS_CREATEALLIANCERELATION%>">
    <INPUT type="hidden" name="dType" value="<%= Integer.parseInt(request.getParameter("dType"))%>">
    <INPUT type="hidden" name="dAlliance" value="<%= Integer.parseInt(request.getParameter("AAallianceId"))%>">
    <%
    %>
    <TABLE width="80%"><TR>
            <TD valign="top">An:</TD>
            <TD valign="top"><input disabled size="30" name="allianceName" value="<%= DiplomacyService.findAllianceById(Integer.parseInt(request.getParameter("AAallianceId"))).getName()%>" type="text"/></TD>
        </TR>
        <TR>
            <TD valign="top"><%= ML.getMLStr("messages_lbl_subject", userId)%>:</TD>
            <TD valign="top"><input disabled size="30" name="subject" value="Betreff: <%= ML.getMLStr(DiplomacyService.findDiplomacyTypeById(Integer.parseInt(request.getParameter("dType"))).getName(), userId)%>" type="text"/></TD>
        </TR>

        <TR style="font-size:12px">
            <TD valign="top"><%= ML.getMLStr("messages_lbl_text", userId)%>:</TD>
            <TD valign="top"><textarea name="message" cols="30" rows="10"></textarea><BR>
                <BR><TABLE class="blue" width="100%">
                    <TR><TD width="50%">:) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_smile.gif"/></TD><TD>;) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_wink.gif"/></TD></TR>
                    <TR><TD width="50%">:p <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_razz.gif"/></TD><TD>:D <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_mrgreen.gif"/></TD></TR>
                    <TR><TD width="50%">8) <IMG border=0 src="http://forum.thedarkdestiny.at/images/smiles/icon_cool.gif"/></TD><TD>&nbsp;</TD></TR>
                </TABLE>
            </TD>
        <TR>
            <TD colspan="2" align="center">
                <input type="submit" value="<%= ML.getMLStr("messages_but_send", userId)%>">
            </TD>
        </TR>
    </TABLE>
</FORM>
<%
            }
            if (type == TYPE_UPDATEALLIANCERELATION || type == TYPE_UPDATEALLIANCEUSERRELATION
                    || type == TYPE_UPDATEUSERALLIANCERELATION || type == TYPE_UPDATEUSERRELATION) {

                ArrayList<DiplomacyRelation> drs = DiplomacyService.findRelsToUpdate(userId, request.getParameterMap());

%>
<%
%>
<FONT color="red"><%= ML.getMLStr("diplomacy_msg_updatewarning", userId)%></FONT> <BR><BR>
   <A href="http://www.thedarkdestiny.at/wiki/index.php/Sternenkarte"  target="new"><%= ML.getMLStr("diplomacy_link_starmapsharing", userId)%></A>
        <BR>
<FORM method="POST" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&process=<%= PROCESS_UPDATERELATIONS%>">

    <BR>
    <BR>
    <TABLE class="blue">
        <% ArrayList<DiplomacyRelation> rels = DiplomacyService.findChangeable(userId, dipType); %>
        <% if(!rels.isEmpty()) { %>
        <TR class="blue2">
            <TD>
                <%= ML.getMLStr("diplomacy_lbl_user", userId) %>
            </TD>
            <TD>
                <%= ML.getMLStr("diplomacy_lbl_option", userId) %>
            </TD>
        </TR>
        <% } %>
        <% for (DiplomacyRelation dr : rels) {%>
        
        <%
        DiplomacyRelationExt dre = new DiplomacyRelationExt(dr);
        boolean found = false;
             for (DiplomacyRelation dr2 : drs) {
             if(dr2.getFromId() == dr.getFromId() &&
                     dr2.getToId() == dr.getToId()){
                 found = true;
                 break;
                 }
             }
             if(dr.getSharingMap()){
                 if(!found){
                     %><INPUT type="hidden" name="<%= dre.getAsToken() %>"/><%
                     %><TR><TD><%= dre.getToName() %> </TD><TD align="center" bgcolor="#444444"><%= ML.getMLStr("diplomacy_sharing_" + !dr.getSharingMap(), userId) %></TD></TR> <%
                     }
             }else{
                    if(found){
                     %><INPUT type="hidden" name="<%= dre.getAsToken() %>"/><%
                     %><TR><TD><%= dre.getToName() %> </TD><TD align="center" bgcolor="#444444"><%= ML.getMLStr("diplomacy_sharing_" + !dr.getSharingMap(), userId) %></TD></TR> <%
                   }
                 }


     %>
        <% }%>
<% if(!rels.isEmpty()){ %>
        <TR>
            <TD colspan="2" align="center">
                <INPUT type="submit" value="<%= ML.getMLStr("diplomacy_but_update", userId)%>"  name="UPDATE"/>
            </TD>
        </TR>
<% } else{ %>
        <TR>
            <TD colspan="2" align="center">
                <%= ML.getMLStr("diplomacy_err_nothingtoupdate", userId) %>
            </TD>
        </TR>
        <TR>
            <TD colspan="2" align ="center">
                <A onClick="javascript:history.go(-1)"><B><BR>Zur&uuml;ck</B></A>
            </TD>
        </TR>
<% } %>
    </TABLE>
</FORM>
<%
            }
//#####
//#####
//##### UserToUser
//#####
//#####
            if (type == TYPE_MANAGERELATIONS) {

%>
<BR/><BR/>
<TABLE class="blue" width="80%">
    <FORM method="post" name="uuform" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_CREATEUSERRELATION%>">

        <TR>
            <TD COLSPAN="7" ALIGN="CENTER" CLASS="blue2"><B><%= ML.getMLStr("diplomacy_lbl_playertoplayer", userId)%></B></TD>
        </TR>
        <TR>
            <TD width="33%">
                <!-- <SELECT id="dPlayer" name="dPlayer"  onChange="window.location=document.uuform.dPlayer.options[document.uuform.dPlayer.selectedIndex].value"> -->
                <SELECT id="dPlayer" name="dPlayer"  onChange="window.location.href='main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_MANAGERELATIONS%>&userId='+document.getElementById('dPlayer').value;">
                    <OPTION value="-1">-<%= ML.getMLStr("diplomacy_opt_pleaseselect", userId) %>-</OPTION>
                    <%
                                    boolean first = true;
                                    for (User u : DiplomacyService.findAllUsersSorted()) {
                                        if (u.getUserId() == userId || !u.getSystemAssigned() || u.getUserId() == 0) {
                                            continue;
                                        }

                                        String allianceTag = "";
                                        if (DiplomacyService.findAllianceMemberByUserId(u.getUserId()) != null) {


                                            allianceTag += "[" + DiplomacyService.findAllianceTagById(DiplomacyService.findAllianceMemberByUserId(u.getUserId()).getAllianceId()) + "]";
                                        }
                    %>
                    <OPTION <% if (u.getUserId() == dUserId) {%> selected <% }%> value=<%= u.getUserId()%>><%= u.getGameName()%> <%= allianceTag%></OPTION>
                    <%
                                    }
                    %>
                </SELECT>
            </TD>
            <TD width="33%">
<%
                int count = 0;
                if (dUserId != -1) {
%>              
                <SELECT id="dType" name="dType">
                    <%                                    
                                    for (DiplomacyType dt : DiplomacyService.getPossibleForUser(userId, dUserId)) {
                                        String voteRequired = "";
                                        if (dt.isVoteRequired()) {
                                            voteRequired = "(V*)";
                                        }
                                        count++;
                    %>
                    <OPTION value="<%= dt.getId()%>"><%= ML.getMLStr(dt.getName(), userId)%> <%= voteRequired%> </OPTION>
                    <%
                                    }
                    %>
                </SELECT>
<%
               }
%>              
            </TD>
            <TD colspan="4" width="33%">
                <% if (count > 0) {%>
                <input value="<%= ML.getMLStr("diplomacy_but_create", userId)%>" type=submit>
                <%}%>
            </TD>

        </TR>
        <TR>
            <TD COLSPAN="7" class="blue2"><B><%= ML.getMLStr("diplomacy_lbl_existingrelations", userId)%></B></TD>
        </TR>
        <TR>
            <TD class="blue"><%= ML.getMLStr("diplomacy_lbl_player", userId)%></TD>
            <TD class="blue"><%= ML.getMLStr("diplomacy_lbl_diplomacytype", userId)%></TD>
            <TD colspan="2" width="10px" class="blue"><%= ML.getMLStr("diplomacy_lbl_starmapsharing", userId)%></TD>
            <TD colspan="3" class="blue"><%= ML.getMLStr("diplomacy_lbl_action", userId)%></TD>
        </TR>
    </FORM>
    <FORM  method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_UPDATEUSERRELATION%>&dipType=<%= EDiplomacyRelationType.USER_TO_USER%>">
        <%
                        ArrayList<DiplomacyRelation> rels = DiplomacyService.findRelationByFromId(userId, EDiplomacyRelationType.USER_TO_USER);
                        for (DiplomacyRelation u : rels) {
                            String allianceTag = "";
                            if (DiplomacyService.findAllianceMemberByUserId(u.getToId()) != null) {


                                allianceTag += "[" + DiplomacyService.findAllianceTagById(DiplomacyService.findAllianceMemberByUserId(u.getToId()).getAllianceId()) + "]";
                            }

                            int voteId = DiplomacyService.findCorrespondingVote(u, userId);

        %>
        <TR>
            <TD><%=  DiplomacyService.findUserById(u.getToId()).getGameName()%> <%= allianceTag%></TD>
            <TD><%=  ML.getMLStr(DiplomacyService.findDiplomacyTypeById(u.getDiplomacyTypeId()).getName(), userId)%></TD>

            <%
                            DiplomacyRelation otherRel = DiplomacyService.findRelationBy(u.getToId(), userId, EDiplomacyRelationType.USER_TO_USER, u.getPending());
            %>
            <% if (!u.getPending() && u.getDiplomacyTypeId() == DiplomacyType.TREATY) {%>
           
            <TD width="15px" align="center"><INPUT name="uu;<%= u.getFromId()%>;<%= u.getToId()%>" type="checkbox" <% if (u.getSharingMap()) {%> checked <% }%> >

                <% } else {%>
            <TD>

                <% }%>
                <% if (!u.getPending() && u.getDiplomacyTypeId() == DiplomacyType.TREATY) {%>
                <INPUT type="checkbox" disabled  <% if (otherRel.getSharingMap()) {%> checked <% }%> ></TD>
                <% } else {%>
            </TD>

            <%  }%>
            <TD>
            <% if(DiplomacyService.hasEmbassy(userId, u.getToId()) && u.getDiplomacyTypeId() == DiplomacyType.TREATY){ %>
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/embassy.png"  onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_embassy", userId)%>')" onmouseout="hideTip()"><% }else if(u.getDiplomacyTypeId() == DiplomacyType.TREATY){ %>
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/noembassy.png"  onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_noembassy", userId)%>')" onmouseout="hideTip()"><% } %>
            </TD>
            <TD>
             </TD>
            <% if (!u.getPending()) {%>
            <% } else {%>
            <TD><IMG border=0 onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_vote", userId)%>')" onmouseout="hideTip()"  src="<%=GameConfig.getInstance().picPath()%>pic/vote.jpg" <% if(voteId > 0){%> style="cursor:pointer;" onclick="window.location.href='main.jsp?page=vote&voteId=<%= voteId %>';"<% } %>/></TD>
            <% }%>
            <% if (u.getInitialiser()) {%>
            <TD><IMG border="0" onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_starter", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/exclamation.jpg"></IMG></TD>
                <% } else {%>

            <TD></TD>
            <% }%>
        </TR>
        <%}
        %>

        <% if (rels.size() > 0) {%>
        <TR>
            <TD></TD>
            <TD></TD>
            <TD><input value="<%= ML.getMLStr("diplomacy_but_update", userId)%>" type=submit></TD>
            <TD></TD>
        </TR>
        <%}%>
    </FORM>
</TABLE>
<BR/><BR/>
<%
//##### UserToAlliance
%>
<TABLE class="blue" width="80%">
    <FORM method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_CREATEUSERALLIANCERELATION%>">
        <TR>
            <TD COLSPAN="7" ALIGN="CENTER" CLASS="blue2"><B><%= ML.getMLStr("diplomacy_lbl_playertoalliance", userId)%></B></TD>
        </TR>
        <TR>
            <TD width="33%">
                <SELECT id="dAlliance" name="dAlliance" onChange="window.location.href='main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_MANAGERELATIONS%>&UAalliance='+document.getElementById('dAlliance').value;">
                <OPTION value="-1">-<%= ML.getMLStr("diplomacy_opt_pleaseselect", userId) %>-</OPTION>
                    <%
                                    first = true;
                                    for (Alliance a : DiplomacyService.findAllAlliancesSortedByName()) {
                                        if (a.getId() == userAllianceId) {
                                            continue;
                                        }
                                        if (a.getId() == allianceId) {
                                            continue;
                                        }
                    %>
                    <OPTION <% if(a.getId() == uaAllianceId ){ %>selected <% } %> value="<%= a.getId()%>"><%= a.getName()%></OPTION>
                    <%
                                    }
                    %>
                </SELECT>
            </TD>
            <TD width="33%">
<%
                count = 0;
                if (uaAllianceId > -1) {
%>              
                <SELECT  id="dType" name="dType">
                    <%
                                    for (DiplomacyType dt : DiplomacyService.getPossibleForUserAlliance(userId, uaAllianceId)) {
                                        count++;

                                        String voteRequired = "";
                                        if (dt.isVoteRequired()) {
                                            voteRequired = "(V*)";
                                        }
                    %>
                    <OPTION value="<%= dt.getId()%>"><%= ML.getMLStr(dt.getName(), userId)%> <%= voteRequired%></OPTION>
                    <%
                                    }
                    %>
                </SELECT>
<%
               }
%>              
            </TD>
            <TD colspan="4" width="33%">
                <% if (count > 0) {%>
                <input value="<%= ML.getMLStr("diplomacy_but_create", userId)%>" type=submit>
                <% }%>
            </TD>
        </TR>
        <TR>
            <TD COLSPAN="7" class="blue2"><B><%= ML.getMLStr("diplomacy_lbl_existingrelations", userId)%></B></TD>
        </TR>
        <TR>
            <TD class="blue"><%= ML.getMLStr("diplomacy_lbl_alliance", userId)%></TD>
            <TD class="blue"><%= ML.getMLStr("diplomacy_lbl_diplomacytype", userId)%></TD>
            <TD colspan="2" class="blue"><%= ML.getMLStr("diplomacy_lbl_starmapsharing", userId)%></TD>
            <TD colspan="3" class="blue"><%= ML.getMLStr("diplomacy_lbl_action", userId)%></TD>
        </TR>
        <%
                        rels.clear();
                        rels = DiplomacyService.findRelationByFromId(userId, EDiplomacyRelationType.USER_TO_ALLIANCE);
                        for (DiplomacyRelation ua : rels) {
                            int voteId = DiplomacyService.findCorrespondingVote(ua, userId);
        %>
    </FORM>
    <FORM   method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_UPDATEALLIANCEUSERRELATION%>&dipType=<%= EDiplomacyRelationType.USER_TO_ALLIANCE%>">
        <TR>
            <TD><%=  DiplomacyService.findAllianceNameById(ua.getToId())%></TD>
            <TD><%=  ML.getMLStr(DiplomacyService.findDiplomacyTypeById(ua.getDiplomacyTypeId()).getName(), userId)%></TD>

            <%
                            DiplomacyRelation otherRel = DiplomacyService.findRelationBy(ua.getToId(), userId, EDiplomacyRelationType.ALLIANCE_TO_USER, ua.getPending());
            %>
            <% if (!ua.getPending() && ua.getDiplomacyTypeId() == DiplomacyType.TREATY) {%>
            <TD><INPUT type="checkbox"  name="ua;<%= ua.getFromId()%>;<%= ua.getToId()%>" <% if (ua.getSharingMap()) {%> checked <% }%> >
                <% } else {%>
            <TD>

                <% }%>
                <% if (!ua.getPending() && ua.getDiplomacyTypeId() == DiplomacyType.TREATY) {%>
                <INPUT type="checkbox" disabled <% if (otherRel.getSharingMap()) {%> checked <% }%> ></TD>
                <% } else {%>
            </TD>

            <TD>
            <% if(DiplomacyService.hasEmbassy(userId, ua.getToId()) && ua.getDiplomacyTypeId() == DiplomacyType.TREATY){ %>
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/embassy.png"  onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_embassy", userId)%>')" onmouseout="hideTip()"><% }else if(ua.getDiplomacyTypeId() == DiplomacyType.TREATY){ %>
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/noembassy.png"  onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_noembassy", userId)%>')" onmouseout="hideTip()"><% } %>
            </TD>
            <% }%>
            <% if (!ua.getPending()) {%>
            <% } else {%>
            <TD><IMG border=0 onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_vote", userId)%>')" onmouseout="hideTip()"  src="<%=GameConfig.getInstance().picPath()%>pic/vote.jpg" <% if(voteId > 0){%> style="cursor:pointer;" onclick="window.location.href='main.jsp?page=vote&voteId=<%= voteId %>';"<% } %>/></TD>
            <% }%>
            <% if (ua.getInitialiser()) {%>
            <TD><IMG border="0" onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_starter", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/exclamation.jpg"></IMG></TD>
                <% } else {%>
            <TD></TD>
            <% }%>
        </TR>
        <%}
        %>
        <% if (rels.size() > 0) {%>
        <TR>
            <TD></TD>
            <TD></TD>
            <TD align="center"><input value="<%= ML.getMLStr("diplomacy_but_update", userId)%>" type=submit></TD>
            <TD></TD>
        </TR>
        <%}%>
    </FORM>
</TABLE><BR/><BR/>
<%
//########## ALLIANCE TO USER
                if (am != null && am.getIsAdmin()) {
%>
<TABLE class="blue" width="80%">
    <FORM method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_CREATEALLIANCEUSERRELATION%>">
        <TR>
            <TD COLSPAN="7" ALIGN="CENTER" CLASS="blue2"><B><%= ML.getMLStr("diplomacy_lbl_alliancetoplayer", userId)%></B></TD>
        </TR>
        <TR>
            <TD width="33%">
                <SELECT id="uaPlayer" name="uaPlayer" onChange="window.location.href='main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_MANAGERELATIONS%>&userIdUA='+document.getElementById('uaPlayer').value;">
                    <OPTION value="-1">-<%= ML.getMLStr("diplomacy_opt_pleaseselect", userId) %>-</OPTION>
                    <%
                                    first = true;
                                    for (User u : DiplomacyService.findAllUsersSorted()) {
                                        if (u.getUserId() == userId || !u.getSystemAssigned() || u.getUserId() == 0) {
                                            continue;
                                        }
                                        ArrayList<Integer> allys = DiplomacyService.findAllys(userId);

                                        if (allys.contains(u.getUserId())) {
                                            continue;
                                        }

                                        if (DiplomacyService.findAllianceMemberByUserId(u.getUserId()) != null) {
                                            if (DiplomacyService.findAllianceMemberByUserId(u.getUserId()).getAllianceId() == allianceId) {
                                                continue;
                                            }
                                        }

                                        String allianceTag = "";
                                        try {
                                            allianceTag += "[" + DiplomacyService.findAllianceTagById(DiplomacyService.findAllianceMemberByUserId(u.getUserId()).getAllianceId()) + "]";
                                        } catch (Exception e) {
                                        }
                                        if (u.getUserId() == uaUserId) {
                    %>
                    <OPTION selected value="<%= u.getUserId()%>"><%= u.getGameName()%> <%= allianceTag%></OPTION>
                    <%
                                        } else {
                    %>
                    <OPTION value="<%= u.getUserId()%>"><%= u.getGameName()%> <%= allianceTag%></OPTION>
                    <%
                                        }
                                    }
                    %>
                </SELECT>
            </TD>
            <TD width="33%">
<%
                count = 0;
                if (uaUserId > -1) {
%>              
                <SELECT id="dType" name="dType">
                    <%
                                    for (DiplomacyType dt : DiplomacyService.getPossibleForAllianceUser(allianceId, uaUserId)) {
                                        String voteRequired = "";
                                        if (dt.isVoteRequired()) {
                                            voteRequired = "(V*)";
                                        }
                                        count++;
                    %>
                    <OPTION value="<%= dt.getId()%>"><%= ML.getMLStr(dt.getName(), userId)%> <%= voteRequired%> </OPTION>
                    <%


                                    }
                    %>
                </SELECT>
<%
               }
%>               
            </TD>
            <TD colspan="4" width="33%">
                <% if (count > 0) {%>
                <input value="<%= ML.getMLStr("diplomacy_but_create", userId)%>" type=submit>
                <% }%>
            </TD>
        </TR>
        <TR>
            <TD COLSPAN="7" class="blue2"><B><%= ML.getMLStr("diplomacy_lbl_existingrelations", userId)%></B></TD>
        </TR>
        <TR>
            <TD class="blue"><%= ML.getMLStr("diplomacy_lbl_player", userId)%></TD>
            <TD class="blue"><%= ML.getMLStr("diplomacy_lbl_diplomacytype", userId)%></TD>
            <TD colspan="2" class="blue"><%= ML.getMLStr("diplomacy_lbl_starmapsharing", userId)%></TD>
            <TD colspan="3" class="blue"><%= ML.getMLStr("diplomacy_lbl_action", userId)%></TD>
        </TR>
    </FORM>
    <FORM  method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_UPDATEALLIANCEUSERRELATION %>&dipType=<%= EDiplomacyRelationType.ALLIANCE_TO_USER%>">
        <%
                        rels.clear();
                        rels = DiplomacyService.findRelationByFromId(am.getAllianceId(), EDiplomacyRelationType.ALLIANCE_TO_USER);
                        for (DiplomacyRelation au : rels) {
                            int voteId = DiplomacyService.findCorrespondingVote(au, userId);
        %>
        <TR>
            <TD><%=  DiplomacyService.findUserById(au.getToId()).getGameName()%></TD>
            <TD><%=  ML.getMLStr(DiplomacyService.findDiplomacyTypeById(au.getDiplomacyTypeId()).getName(), userId)%></TD>

            <%
                                DiplomacyRelation otherRel = DiplomacyService.findRelationBy(au.getToId(), am.getAllianceId(), EDiplomacyRelationType.USER_TO_ALLIANCE, au.getPending());
            %>
            <% if (!au.getPending() && au.getDiplomacyTypeId() == DiplomacyType.TREATY) {%>
            <TD><INPUT name="au;<%= au.getFromId()%>;<%= au.getToId()%>" type="checkbox" <% if (au.getSharingMap()) {%> checked <% }%> >
                <% } else {%>
            <TD>
                <% }%>
                <% if (!au.getPending() && au.getDiplomacyTypeId() == DiplomacyType.TREATY) {%>
                <INPUT disabled type="checkbox"  <% if (otherRel.getSharingMap()) {%> checked <% }%> ></TD>
                <% } else {%>
            </TD>

            <% }%>
            <TD>
            <% if(DiplomacyService.hasEmbassy(userId, au.getToId()) && au.getDiplomacyTypeId() == DiplomacyType.TREATY){ %>
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/embassy.png"  onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_embassy", userId)%>')" onmouseout="hideTip()"><% }else if(au.getDiplomacyTypeId() == DiplomacyType.TREATY){ %>
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/noembassy.png" onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_noembassy", userId)%>')" onmouseout="hideTip()"><% } %>
            </TD>
            <% if (!au.getPending()) {%>
<!-- <TD><A href="main.jsp?page=new/diplomacy&subpage=relations&process=<% /* PROCESS_DELETEU2U %>&fromUser=<% userId%>&toUser=<% u.getToId()%>"><!--<IMG border=0 src="<% GameConfig.getInstance().picPath() */%>pic/cancel.jpg"></IMG></A></TD>-->
            <% } else {%>
            <TD><IMG border=0 onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_vote", userId)%>')" onmouseout="hideTip()"  src="<%=GameConfig.getInstance().picPath()%>pic/vote.jpg" <% if(voteId > 0){%> style="cursor:pointer;" onclick="window.location.href='main.jsp?page=vote&voteId=<%= voteId %>';"<% } %>/></TD>
            <% }%>
            <% if (au.getInitialiser()) {%>
            <TD><IMG border="0" onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_starter", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/exclamation.jpg"></IMG></TD>
                <% } else {%>
            <TD></TD>
            <% }%>
        </TR>
        <%}
        %>
        <% if (rels.size() > 0) {%>
        <TR>
            <TD></TD>
            <TD></TD>
            <TD><input value="<%= ML.getMLStr("diplomacy_but_update", userId)%>" type=submit></TD>
            <TD></TD>
        </TR>
        <%}%>
    </FORM>
</TABLE>
<BR/><BR/>


<%

//####### ALLIANCE TO ALLIANCE
%>
<TABLE class="blue" width="80%">
    <FORM method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_CREATEALLIANCERELATION%>&dipType=<%= EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE%>">
        <TR>
            <TD COLSPAN="7" ALIGN="CENTER" CLASS="blue2"><B><%= ML.getMLStr("diplomacy_lbl_alliancetoalliance", userId)%></B></TD>
        </TR>
        <TR>
            <TD width="33%">
                <SELECT id="AAallianceId" name="AAallianceId" onChange="window.location.href='main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_MANAGERELATIONS%>&AAallianceId='+document.getElementById('AAallianceId').value;">
                    <OPTION value="-1">-<%= ML.getMLStr("diplomacy_opt_pleaseselect", userId) %>-</OPTION>
                    <%
                                    first = true;
                                    for (Alliance a : DiplomacyService.findAllAlliancesSortedByName()) {
                                        if (DiplomacyService.findAllianceMemberByUserId(userId) != null) {
                                            if (a.getId() == am.getAllianceId()) {
                                                continue;
                                            }
                                        }
                                        if (a.getId() == aaAllianceId) {
                    %>
                    <OPTION selected value="<%= a.getId()%>"><%= a.getName()%></OPTION>
                    <%
                                        } else {

                    %>
                    <OPTION value="<%= a.getId()%>"><%= a.getName()%></OPTION>
                    <%
                                        }
                                    }
                    %>
                </SELECT>
            </TD>
            <TD width="33%">
<%
                count = 0;
                if (aaAllianceId > -1) {
%>              
                <SELECT id="dType" name="dType">
                    <%                                  
                                    for (DiplomacyType dt : DiplomacyService.getPossibleForAlliance(allianceId, aaAllianceId)) {


                                        count++;
                                        String voteRequired = "";
                                        if (dt.isVoteRequired()) {
                                            voteRequired = "(V*)";
                                        }
                    %>
                    <OPTION value="<%= dt.getId()%>"><%= ML.getMLStr(dt.getName(), userId)%><%= voteRequired%></OPTION>
                    <%
                                    }
                    %>
                </SELECT>
<%
               }
%>              
            </TD>
            <TD colspan="4" width="33%">
                <% if (count > 0) {%>
                <input value="<%= ML.getMLStr("diplomacy_but_create", userId)%>" type=submit>
                <% }%>
            </TD>
        </TR>

        <TR>
            <TD COLSPAN="7" class="blue2"><B><%= ML.getMLStr("diplomacy_lbl_existingrelations", userId)%></B></TD>
        </TR>
        <TR>
            <TD class="blue"><%= ML.getMLStr("diplomacy_lbl_alliance", userId)%></TD>
            <TD class="blue"><%= ML.getMLStr("diplomacy_lbl_diplomacytype", userId)%></TD>
            <TD colspan="2" class="blue"><%= ML.getMLStr("diplomacy_lbl_starmapsharing", userId)%></TD>
            <TD colspan="3" class="blue"><%= ML.getMLStr("diplomacy_lbl_action", userId)%></TD>
        </TR>
    </FORM>
    <FORM method="post" action="main.jsp?page=new/diplomacy&subpage=diplomacy-relationmanagement&type=<%= TYPE_UPDATEALLIANCERELATION %>&dipType=<%= EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE%>">
        <%
                        rels.clear();
                        rels = DiplomacyService.findRelationByFromId(am.getAllianceId(), EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
                        for (DiplomacyRelation a : rels) {
                            int voteId = DiplomacyService.findCorrespondingVote(a, userId);
        %>
        <TR>
            <TD><%=  DiplomacyService.findAllianceNameById(a.getToId())%></TD>
            <TD><%=  ML.getMLStr(DiplomacyService.findDiplomacyTypeById(a.getDiplomacyTypeId()).getName(), userId)%></TD>

            <%
                                DiplomacyRelation otherRel = DiplomacyService.findRelationBy(a.getToId(), a.getFromId(), EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE, a.getPending());
            %>
            <% if (!a.getPending() && a.getDiplomacyTypeId() == DiplomacyType.TREATY) {%>
            <TD><INPUT  name="aa;<%= a.getFromId()%>;<%= a.getToId()%>" type="checkbox" <% if (a.getSharingMap()) {%> checked <% }%> >
                <% } else {%>
            <TD>

                <% }%>
                <% if (!a.getPending() && a.getDiplomacyTypeId() == DiplomacyType.TREATY) {%>
                <INPUT type="checkbox" disabled <% if (otherRel.getSharingMap()) {%> checked <% }%> ></TD>
                <% } else {%>
            </TD>

            <TD>
            <% if(DiplomacyService.hasEmbassy(userId, a.getToId()) && a.getDiplomacyTypeId() == DiplomacyType.TREATY){ %>
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/embassy.png" onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_embassy", userId)%>')" onmouseout="hideTip()"><% }else if(a.getDiplomacyTypeId() == DiplomacyType.TREATY){ %>
            <IMG src="<%= GameConfig.getInstance().picPath() %>pic/noembassy.png" onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_noembassy", userId)%>')" onmouseout="hideTip()"><% } %>
            </TD>
            <% }%>
            <% if (!a.getPending()) {%>
            <% } else {%>
            <TD><IMG border=0 onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_vote", userId)%>')" onmouseout="hideTip()"  src="<%=GameConfig.getInstance().picPath()%>pic/vote.jpg" <% if(voteId > 0){%> style="cursor:pointer;" onclick="window.location.href='main.jsp?page=vote&voteId=<%= voteId %>';"<% } %>/></TD>
            <% }%>
            <% if (a.getInitialiser()) {%>
            <TD><IMG border="0" onmouseover="doTooltip(event,'<%= ML.getMLStr("diplomacy_pop_starter", userId)%>')" onmouseout="hideTip()" src="<%=GameConfig.getInstance().picPath()%>pic/exclamation.jpg"></IMG></TD>
                <% } else {%>
            <TD></TD>
            <%}%>
        </TR>
        <%}
        %>

        <% if (rels.size() > 0) {%>
        <TR>
            <TD></TD>
            <TD></TD>
            <TD><input value="<%= ML.getMLStr("diplomacy_but_update", userId)%>" type=submit></TD>
            <TD></TD>
        </TR>
        <%}%>
    </FORM>
</TABLE>


<%

                }
%>
<%
            }
%>