
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.core.model.UserSettings"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.model.User" %>
<%@page import="at.darkdestiny.core.model.UserData" %>
<%@page import="at.darkdestiny.core.service.ProfileService" %>
<%
// Determine correct connection path
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    String xmlUrl = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);
            
    int[] xySize = ProfileService.getStarMapSize(userId);    
    
    int cookieStarMapType = 0;
    int cookieStarMapWidth = 0;
    int cookieStarMapHeight = 0;        
    
    try {
    Cookie cookie = null;
    Cookie[] cookies = null;
    // Get an array of Cookies associated with this domain
    cookies = request.getCookies();
    if( cookies != null ){
       // out.println("<h2> Found Cookies Name and Value</h2>");
       for (int i = 0; i < cookies.length; i++){
          cookie = cookies[i];
          if (cookie.getName().equalsIgnoreCase("map_type")) {
            cookieStarMapType = Integer.parseInt(cookie.getValue());
          }
          if (cookie.getName().equalsIgnoreCase("map_xsize")) {
            cookieStarMapWidth = Integer.parseInt(cookie.getValue());  
          }
          if (cookie.getName().equalsIgnoreCase("map_ysize")) {
            cookieStarMapHeight = Integer.parseInt(cookie.getValue());                        
          }                  
       }
       
       if ((cookieStarMapWidth > 0) && (cookieStarMapHeight > 0)) {
           xySize[0] = cookieStarMapWidth;
           xySize[1] = cookieStarMapHeight;
       }       
    }else{
       // out.println("<h2>No cookies founds</h2>");
    }            
    } catch (Exception e) {
        DebugBuffer.error("Cookie crash");
    }    
%>
    <applet id="starmap"  code="at.darkdestiny.vismap.gui.AppletMain" width="<%= xySize[0] %>" height="<%= xySize[1] %>" alt="" archive="applet/vismap.jar">
        <param name="source" value="<%= xmlUrl%>createStarMapInfoXT.jsp"/>
        <param name="sessionId" value="<%= request.getSession().getId()%>"/>
        <param name="MODE" value="STARMAP">
        <PARAM NAME="cache_option" VALUE="NO">

    </applet>
<script language="javascript" type="text/javascript">
    var disableHTMLScroll = false;

    document.getElementById("starmap").addEventListener('click',
    function(e){
        disableHTMLScroll = true;
    }
    , false);

    document.getElementById("starmap").addEventListener('mouseover',
    function(e){
        disableHTMLScroll = true;
    }
    , false);

    document.getElementById("starmap").addEventListener('mouseout',
    function(e){
        disableHTMLScroll = false;
    }
    , false);

    document.addEventListener('DOMMouseScroll', function(e){
        if (disableHTMLScroll) {
            e.stopPropagation();
            e.preventDefault();
            e.cancelBubble = false;
            return false;
        } else {
            return true;
        }
    }, false);
</script>
</BODY>
