<%@page import="java.security.InvalidParameterException"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.core.utilities.RessTransfer"%>
<%@page import="at.darkdestiny.core.GameConstants"%>

<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int actionType = 0;
int fleetId = 0;

// Read FleetId and Type
if (request.getParameter("fleetId") != null) {
    fleetId = Integer.parseInt(request.getParameter("fleetId"));
}

if (request.getParameter("type") != null) {
    actionType = Integer.parseInt(request.getParameter("type"));
}

if ((fleetId == 0) || (actionType == 0)) {
%>
    <BR>Ung&uuml;ltige Anfrage
<%
} else {
    RessTransfer rt = new RessTransfer();
    try {
        RessTransfer.Direction direction = RessTransfer.Direction.LOAD;
        int ressType = 0;
        switch (actionType) {
        case 1: ressType = GameConstants.LOADING_RESS;
                direction = RessTransfer.Direction.LOAD;
                break;
        case 2: ressType = GameConstants.LOADING_RESS;
                direction = RessTransfer.Direction.UNLOAD;
                break;
        case 3: ressType = GameConstants.LOADING_TROOPS;
                direction = RessTransfer.Direction.LOAD;
                break;
        case 4: ressType = GameConstants.LOADING_TROOPS;
                direction = RessTransfer.Direction.UNLOAD;
                break;
        case 5: ressType = GameConstants.LOADING_POPULATION;
                direction = RessTransfer.Direction.LOAD;
                break;
        case 6: ressType = GameConstants.LOADING_POPULATION;
                direction = RessTransfer.Direction.UNLOAD;
                break;
        case 7: ressType = GameConstants.LOADING_SCRAP;
                direction = RessTransfer.Direction.LOAD;
                break;
        }
        rt.preparePlanetFleetTransfer(fleetId, userId, ressType, direction, request.getParameterMap());
        rt.transact();
        if (((rt.getErrMsg()).substring(0,1)).equalsIgnoreCase("0")) { %>
            <BR><FONT color="green" style="font-size:12px"><B><%= (rt.getErrMsg()).substring(1,(rt.getErrMsg()).length() - 1) %></B></FONT><BR><BR>
            <A HREF="main.jsp?page=new/fleet&type=2">Zur�ck zur �bersicht</A>&nbsp;&nbsp;&nbsp;&nbsp;<A HREF="main.jsp?page=new/fleet&type=1&fleetId=<%= fleetId %>&fmaction=4">Versenden</A>
<%      } else { %>
            <BR><FONT color="red" style="font-size:12px"><B>FEHLER: <%= (rt.getErrMsg()).substring(1,(rt.getErrMsg()).length() - 1) %></B></FONT><BR><BR>
            <A HREF="main.jsp?page=new/fleet&type=2">Weiter</A>
<%      }
    } catch (Throwable e) { 
        if (!(e instanceof InvalidParameterException)) {
            DebugBuffer.writeStackTrace("Error in RessTransfer: ",e);
        }
%>            
        <BR><FONT color="red" style="font-size:12px"><B>FEHLER: <%= e.getMessage() %></B></FONT>
<%
        // DebugBuffer.error(e);
    }
}
%>
