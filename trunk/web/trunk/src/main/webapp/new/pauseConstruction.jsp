<%@page import="at.darkdestiny.core.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.databuffer.*"%>
<%@page import="at.darkdestiny.core.service.ConstructionService" %>
<%@page import="at.darkdestiny.core.service.Service" %>
<%@page import="at.darkdestiny.core.result.BuildableResult" %>
<%@page import="at.darkdestiny.core.result.InConstructionResult" %>
<%@page import="at.darkdestiny.core.result.InConstructionEntry" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.view.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.utilities.*" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>

<%

            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
            int constructionId = Integer.parseInt((String) request.getParameter("constructionId"));

            Construction c = ConstructionService.findConstructionById(constructionId);
            PlanetConstruction pc = Service.planetConstructionDAO.findBy(planetId, constructionId);
%>
<BR><BR>
<FORM method="post" action="main.jsp?page=new/construction&cId=<%= constructionId %>&idle=true">
<TABLE align="center" style="width:300px; font-size: 12px; font-weight: bolder">
    <TR>
        <TD align="center" colspan="2">
            Bitte die Anzahl der Geb&auml;ude w&auml;hlen, die stillgelegt werden sollen.<BR><BR>
        </TD>
    </TR>
    <TR class="blue2">
        <TD align="center" colspan="2">
            <%= ML.getMLStr(c.getName(), userId) %>
        </TD>
    </TR>
    <TR>
        <TD align="left" style="width:60%;" align="center">
            Anzahl
        </TD>
        <TD align="right">
            <INPUT type="text" name="count" size="3" value="<%= pc.getIdle() %>" /> / <%= pc.getNumber() %>
        </TD>
    </TR>
    <TR>
        <TD align="center" colspan="2">
            <INPUT value="Update" type="submit"/>
        </TD>
    </TR>
</TABLE>
</FORM>