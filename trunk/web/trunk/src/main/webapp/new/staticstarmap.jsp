<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.core.service.ProfileService"%>

<script src="java/starmap.js" type="text/javascript"></script>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));  
    
int[] xySize = ProfileService.getStarMapSize(userId);
String basePath = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);

String op = request.getParameter("op");
String opPar = "";

if (op != null) {
    opPar = "?op=" + op;
}

int cookieStarMapType = 0;
int cookieStarMapWidth = 0;
int cookieStarMapHeight = 0;  

try {
    Cookie cookie = null;
    Cookie[] cookies = null;
    // Get an array of Cookies associated with this domain
    cookies = request.getCookies();
    if( cookies != null ){
       // out.println("<h2> Found Cookies Name and Value</h2>");
       for (int i = 0; i < cookies.length; i++){
          cookie = cookies[i];
          if (cookie.getName().equalsIgnoreCase("map_type")) {
            cookieStarMapType = Integer.parseInt(cookie.getValue());
          }
          if (cookie.getName().equalsIgnoreCase("map_xsize")) {
            cookieStarMapWidth = Integer.parseInt(cookie.getValue());  
          }
          if (cookie.getName().equalsIgnoreCase("map_ysize")) {
            cookieStarMapHeight = Integer.parseInt(cookie.getValue());                        
          }                  
       }

       if ((cookieStarMapWidth > 0) && (cookieStarMapHeight > 0)) {
           xySize[0] = cookieStarMapWidth;
           xySize[1] = cookieStarMapHeight;
       }
    }else{
       // out.println("<h2>No cookies founds</h2>");
    }            
} catch (Exception e) {
    DebugBuffer.error("Cookie crash");
}   

if (!opPar.equalsIgnoreCase("")) {
    opPar += "&x="+xySize[0]+"&y="+xySize[1];
} else {
    opPar = "?x="+xySize[0]+"&y="+xySize[1];
}
%>
<script language="javascript" type="text/javascript">
    var x = <%= xySize[0] %>;
    var y = <%= xySize[1] %>;
    var basePath = '<%= basePath %>';
    
    function loadAreaMap() {
        // alert('LOADED');
        htmlCall('<%= basePath %>GetStarMapArea',setAreaMap);
    }
    
    function setAreaMap(content) {
        // alert('CONENT: ' + content);
        document.getElementById('descriptionMap').innerHTML = content;
    }
</script>
<BR>

<TABLE class="bluetrans">
    <TR class="blue2">
        <TD colspan="2" align="center">
            <IMG id="starmapwindow" onclick="loadObjectInfo(event.pageX - this.getBoundingClientRect().left - window.pageXOffset,event.pageY - this.getBoundingClientRect().top - window.pageYOffset,'<%= basePath %>');" src="GetStarMap<%= opPar %>" width="<%= xySize[0] %>" height="<%= xySize[1] %>" onLoad="loadAreaMap();" usemap="#description" />             
            <MAP id="descriptionMap" name="description">
                
            </MAP>
        </TD>
    </TR>
</TABLE>
