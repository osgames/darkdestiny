<%@page import="at.darkdestiny.core.development.BonusEntry"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="at.darkdestiny.core.trade.TradeRouteInfo"%>
<%@page import="at.darkdestiny.core.model.*"%>
<%@page import="at.darkdestiny.core.model.Ressource"%>
<%@page import="at.darkdestiny.core.model.PlanetRessource"%>
<%@page import="at.darkdestiny.core.model.Planet"%>
<%@page import="at.darkdestiny.core.result.*"%>
<%@page import="at.darkdestiny.core.ML"%>
<%@page import="at.darkdestiny.core.planetcalc.*"%>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.service.TransportService" %>
<%@page import="at.darkdestiny.core.construction.ConstructCount"%>
<%@page import="at.darkdestiny.core.planetcalc.ExtPlanetCalcResult"%>
<%@page import="at.darkdestiny.core.enumeration.*"%>
<%@page import="at.darkdestiny.core.utilities.*"%>
<%@page import="at.darkdestiny.core.html.*"%>
<%@page import="at.darkdestiny.util.DebugBuffer.*"%>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel"%>
<%
session.setAttribute("actPage", "new/overview");
session = request.getSession();

ArrayList<BaseResult> result = new ArrayList<BaseResult>();

int externalViewId = 0;
if (request.getParameter("vPlanet") != null) {
    externalViewId = Integer.parseInt((String)request.getParameter("vPlanet"));
}

int userId = Integer.parseInt((String)session.getAttribute("userId"));
Locale loc = ML.getLocale(userId);
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int systemId = Integer.parseInt((String)session.getAttribute("actSystem"));

if (externalViewId != 0) {
    planetId = externalViewId;
}

Planet planet = OverviewService.findPlanetById(planetId);
systemId = planet.getSystemId();

int process = 0;

final int PROCESS_BUYBONUS = 1;
if(request.getParameter("process") != null){
       process = Integer.parseInt(request.getParameter("process"));
    }

UserData ud = OverviewService.findUserData(userId);
if(process > 0){
    switch(process){
        case(PROCESS_BUYBONUS):
            result = OverviewService.buyBonus(userId, planetId, request.getParameterMap());
            break;
        }

    }

PlayerPlanet pp = OverviewService.findPlayerPlanetByPlanetId(planetId);
SurfaceResult sr = planet.getSurface();

PlanetCalculation pc = null;
ProductionResult productionResult = null;
ExtPlanetCalcResult epcr = null;
if (pp != null) {
    pc = OverviewService.PlanetCalculation(planetId);
    productionResult = pc.getPlanetCalcData().getProductionData();
    epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
}

boolean currentUserIsOwner = false;

if (externalViewId != 0) {
    if (pp != null) {
        currentUserIsOwner = pp.getUserId() == userId;
    } else {
        currentUserIsOwner = false;
    }
} else {
    if (pp != null) {
        currentUserIsOwner = pp.getUserId() == userId;
    }
}

int inConsUsedPop = 0;
int inConsUsedSpace = 0;
long maxPopulation = planet.getMaxPopulation();

if (epcr != null) {
    inConsUsedPop = epcr.getInConsUsedPopulation();
    inConsUsedSpace = epcr.getInConsUsedSpace();
    maxPopulation = epcr.getMaxPopulation();
}

if (request.getParameter("delete") != null) { %>
            <jsp:forward page="main.jsp" >
                <jsp:param name="buildingID" value='<%= request.getParameter("delete") %>' />
                <jsp:param name="page" value='destroyBuilding' />
            </jsp:forward>
<%
}
// LinkedList<TradeRouteInfo> tradeRoutes = TradeUtilities.getAllPlanetRoutes(planetId);
String MLPlanetTypeStr = "overview_lbl_planet";
if (externalViewId == 0) {
    if (pp.getColonyType() == EColonyType.MINING_COLONY) {
        MLPlanetTypeStr = "db_development_mining";
    } else if (pp.getColonyType() == EColonyType.AGRICULTURAL_COLONY) {
        MLPlanetTypeStr = "db_development_agricultural";
    } else if (pp.getColonyType() == EColonyType.ADMINISTRATIVE_COLONY) {
        MLPlanetTypeStr = "db_development_administrative";        
    } else if (pp.getColonyType() == EColonyType.POPULATION_COLONY) {
        MLPlanetTypeStr = "db_development_population";        
    } else if (pp.getColonyType() == EColonyType.MILITARY_OUTPOST) {
        MLPlanetTypeStr = "db_development_military";        
    } else if (pp.getColonyType() == EColonyType.INDUSTRIAL_COLONY) {
        MLPlanetTypeStr = "db_development_industrial";                
    } else if (pp.isHomeSystem()) {
        MLPlanetTypeStr = "overview_lbl_homeplanet";
    }
}

Planet p = ManagementService.getPlanetById(planetId);
String planetName = "Planet #" + p.getId();
boolean viewerPermission = false;

if (pp != null) {        
    planetName = pp.getName();
    
    if (userId == pp.getUserId()) {
        viewerPermission = true;
    } else {
        DiplomacyResult dr = DiplomacyUtilities.getDiplomacyResult(userId, pp.getUserId());
        if (dr.isSharingMap()) viewerPermission = true;
    }
}


%>
<CENTER>
<% for(BaseResult br : result){ %>
<%= br.getFormattedString() %>
<% }

%>
</CENTER>
<TABLE class="blue" width="80%" cellpadding="0" cellspacing="0">
<TR><TD>
<TABLE width="100%" cellpadding="0" cellspacing="0">
    <TR><TD width="50%" align=center class="blue2"><B><FONT size="+2"><%=ML.getMLStr(MLPlanetTypeStr, userId)%> <%= planetName %> (#<%= planet.getId() %>)</FONT></B></TD></TR>
<% if ((pp != null) && pp.isHomeSystem() && viewerPermission) { %>
<TR><TD width="50%" align=center class="blue2"><B><%= ML.getMLStr("overview_lbl_homesystem", userId)%></B></TD></TR>
<% } %>
</TABLE>
<TABLE style="font-size:13px;" width="100%" cellpadding="0" cellspacing="0">
<TR>
<TD width="200"><IMG src="GetPlanetPic?planetId=<%= planetId %>" width="200" height="200" /></TD>
<TD valign="top">
    <U><%= ML.getMLStr("overview_lbl_common", userId)%></U><BR><BR>
<%
String pop = "systemsearch_pop_"+planet.getLandType();
%>
<%= ML.getMLStr("overview_lbl_planettype", userId)%>: <%= ML.getMLStr(pop, userId)%> <BR>
<%= ML.getMLStr("overview_lbl_diameter", userId)%> <%= FormatUtilities.getFormattedNumber(planet.getDiameter(),loc) %> km<BR>
<%= ML.getMLStr("overview_lbl_averagetemperature", userId)%>: <%= planet.getAvgTemp() %> �C<BR><BR>

<U><%= ML.getMLStr("overview_lbl_specificdata", userId)%></U><BR><BR>
<%= ML.getMLStr("overview_lbl_constructiblesurface",userId)%>: <%= FormatUtilities.getFormatScaledNumber(sr.getSurface(),GameConstants.VALUE_TYPE_NORMAL,loc) %> km� (<%= planet.getSurfaceSquares() %> <%= ML.getMLStr("overview_lbl_buildingsite",userId)%>)<BR>
    <%= ML.getMLStr("overview_lbl_maxpopulation",userId)%>: <%= FormatUtilities.getFormatScaledNumber(maxPopulation,GameConstants.VALUE_TYPE_NORMAL,loc) %><BR>
</TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
<%
if (currentUserIsOwner) {
if (pp.getDismantle()) { %>
<BR><TABLE width="80%" style="background-color: red"><TR><TD align="middle"><FONT color="black" size="+1">Kolonie wird aufgegeben</FONT></TD><TR><TABLE>
<% } %>
<P>
<TABLE width="80%">
    <TR>
        <TD>
            <TABLE width="100%">
                <TR align="center" valign="top">
                    <TD width="*">
                            <TABLE class="blue" width="100%">
                                <TR class="heading"><TD COLSPAN='2' align="center"><%= ML.getMLStr("manamgement_lbl_common", userId)%></TD></TR>
                                <TR><TD width="150"><%= ML.getMLStr("management_lbl_population", userId)%> :</TD><TD>
                                <%= FormatUtilities.getFormatScaledNumber(pp.getPopulation(), GameConstants.VALUE_TYPE_NORMAL,loc)%></TR>
                                <%
                                boolean migrationWarning = PlanetAdministrationService.showMigrationWarning(userId, pp.getPlanetId());
                                String migWarnStr = "<FONT color=\"yellow\">("+ML.getMLStr("management_lbl_migwarn", userId)+")</FONT>";

                                if (pp.getMigration() >= 0) {
                                %>
                                <TR><TD><%= ML.getMLStr("management_lbl_immigration", userId)%>:</TD>
                                    <TD><%= FormatUtilities.getFormattedNumber(pp.getMigration(),loc)%></TD></TR>
                                    <%  } else {%>
                                <TR><TD><%= ML.getMLStr("management_lbl_emigration", userId)%>:</TD>
                                    <TD><%= FormatUtilities.getFormattedNumber(-pp.getMigration(),loc)%> <% if (migrationWarning) { out.write(migWarnStr); }%></TD></TR>
                                    <%  }

                                long freePop = pp.getPopActionPoints()  - inConsUsedPop;
                                if (freePop < 0) {
                                    freePop = 0;
                                }
                                int freeSurface = sr.getFreeSurface() - inConsUsedSpace;
                                BonusResult br = BonusUtilities.findBonus(planetId, EBonusType.POPULATION_GROWTH);
                                double perc = br.getPercBonus();
                                if(perc == 0){
                                    perc = 1d;
                                    }
                                double growthBonus = Math.round((epcr.getGrowth() + br.getNumericBonus()) * (perc) * 100d)/100d;

                                    %>
                                <TR><TD><%= ML.getMLStr("management_lbl_freeactionpoints", userId)%>:</TD><TD><%= FormatUtilities.getFormattedNumber(pp.getPopActionPoints() - (epcr.getUsedPopulation() + inConsUsedPop),loc) %> / <%= FormatUtilities.getFormattedNumber(pp.getPopActionPoints(),loc)%> (<%= ML.getMLStr("overview_lbl_used", userId) %> <%= FormatUtilities.getFormattedNumber(epcr.getUsedPopulation() + inConsUsedPop,loc)%>)</TD></TR>
                                <TR><TD><%= ML.getMLStr("management_lbl_growth", userId)%>:</TD><TD>
                                        <FONT color="<%
                                 if (epcr.getGrowth() < 0) {
                                              %>red<%                              } else {
                                              %>#33FF00<%    }%>">
                                        <%= FormatUtilities.getFormattedDecimal(epcr.getGrowth(), 2)%> %</FONT> (max. <%= FormatUtilities.getFormattedDecimal(epcr.getMaxGrowth() - epcr.getMaxGrowthBonus(), 2) %>% 
                                        <% if (epcr.getMaxGrowthBonus() > 0) { %>
                                            <FONT style="color:#FF00FF">[+ <%= FormatUtilities.getFormattedDecimal(epcr.getMaxGrowthBonus(), 2) %>%]</FONT>                                        
                                        <% } %>
                                        )
                                        </TD></TR>
                                        <%  if (!(p.getLandType().equalsIgnoreCase("A") || p.getLandType().equalsIgnoreCase("B"))) {%>
                                <TR><TD><%= ML.getMLStr("management_lbl_undevelopedarea", userId)%>:</TD><TD><%= (int) ((float) 100 / p.getSurfaceSquares() * (freeSurface))%> % (<%= FormatUtilities.getFormattedNumber((long)sr.getFreeSurface() - inConsUsedSpace)%> / <%= p.getSurfaceSquares() %> <%= ML.getMLStr("overview_lbl_buildingsites", userId) %>)</TD></TR>
                                <%  }%>
                                <TR><TD><%= ML.getMLStr("management_lbl_morale", userId)%>:</TD><TD><FONT color="<%
                                if (epcr.getMorale() < 50) {
                                                              %>red<%   } else if ((epcr.getMorale() < 80) && (epcr.getMorale() >= 50)) {
                                                              %>yellow<%  } else {
                                                              %>#33FF00<%  }%>">
                                        <%= FormatUtilities.getFormattedDecimal(epcr.getMorale(), 0) %></FONT>
                                       
                                        <%= epcr.getMoraleChangeStr() %>(max. <%= FormatUtilities.getFormattedDecimal(epcr.getMaxMorale() - epcr.getMoraleBonus(), 0)%> + <%= epcr.getMoraleBonus() %>)</TR>
                                <TR><TD><%= ML.getMLStr("management_lbl_efficiency", userId)%>:</TD><TD>
                                        <%
                // DebugBuffer.addLine("Trying to get effAgri");
                int effAgri = (int) (epcr.getEffAgriculture() * 100);
                // DebugBuffer.addLine("Trying to get effIndu");
                int effIndu = (int) (epcr.getEffIndustry() * 100);
                // DebugBuffer.addLine("Trying to get effResearch");
                int effRes = (int) (epcr.getEffResearch() * 100);%>
                                        <FONT color="#<%=HTMLColorCodes.getColorCode(effAgri, 60, 100)%>"><%= effAgri%> %</FONT>&nbsp;/
                                        <FONT color="#<%=HTMLColorCodes.getColorCode(effIndu, 60, 100)%>"><%= effIndu%> %</FONT>&nbsp;/
                                        <FONT color="#<%=HTMLColorCodes.getColorCode(effRes, 60, 100)%>"><%= effRes%> %</FONT>
                                    </TD>
                                </TR>
                                <%
                // Special upgrades having effect on planet
                ArrayList<Integer> upgrades = ManagementService.getUpgrades(userId, planetId);
                                %>
                            </TABLE>
                        </TD>
                </TR>
            </TABLE>
<%
ArrayList<ActiveBonus> activeBonus = OverviewService.findActiveBonus(planetId);
ArrayList<BonusEntry> bonus = OverviewService.getBonusList(userId,planetId);
    if (upgrades.size() > 0 || activeBonus.size() > 0 || bonus.size() > 0) { %>
            <BR>
            <FORM action="main.jsp?page=new/overview&process=<%= PROCESS_BUYBONUS %>" method="post">
            <TABLE class="blue" width="100%" style="font-size: 12px;">
                <TR class="heading"><TD colspan=3 align="center"><B><%= ML.getMLStr("management_lbl_bonusextensions", userId)%></B></TD></TR>
                <%
for (Integer upgradeType : upgrades) {
switch (upgradeType) {
                        case (GameConstants.UPDATE_HOMEWORLD):%>
                <TR valign="middle"><TD width="30"><IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/homeworld.gif"/></TD><TD><%= ML.getMLStr("management_lbl_bonus_homeworld", userId)%></TD></TR>
                        <%                  break;
                case (GameConstants.UPDATE_LOCAL_ADMINISTRATION):%>
                <TR valign="middle"><TD><IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/money.jpg"/></TD><TD><%= ML.getMLStr("management_lbl_bonus_localadministration", userId)%></TD></TR>
                        <%                  break;
                case (GameConstants.UPDATE_PLANETARY_ADMINISTRATION):%>
                <TR valign="middle"><TD><IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/money.jpg"/></TD><TD><%= ML.getMLStr("management_lbl_bonus_planetaryadministration", userId)%></TD></TR>
                        <%                  break;
                case (GameConstants.UPDATE_PLANETARY_GOVERNMENT):%>
                <TR valign="middle"><TD><IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/money.jpg"/></TD><TD><%= ML.getMLStr("management_lbl_bonus_planetarygovernment", userId)%></TD></TR>
                        <%                  break;
                case (GameConstants.UPDATE_MINING_DESINTEGRATOR):%>
                <TR valign="middle"><TD><IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/desintegrator.jpg"/></TD><TD><%= ML.getMLStr("management_lbl_bonus_desintegrator", userId)%></TD></TR>
                        <%                  break;
                case (GameConstants.UPDATE_VULCAN_ACTIVITY):%>
                <TR valign="middle"><TD><IMG border=0 src="<%= GameConfig.getInstance().picPath()%>pic/icons/vulcanic.gif"/></TD><TD><%= ML.getMLStr("management_lbl_bonus_vulcanactivity", userId)%></TD></TR>
                        <%                  break;
}
}

        for(ActiveBonus ab : activeBonus){
                Bonus b = OverviewService.findBonusById(ab.getBonusId());
    %>
                <TR valign="middle">
                    <TD>
                        <IMG style="height:20px; width: 20px;" border=0 src="<%= GameConfig.getInstance().picPath()%>pic/menu/icons/happiness.png"/>
                    </TD>
<%
                    if (b.getBonusClass() != null) {
                        for (String description : OverviewService.getActiveBonusDescription(ab.getBonusId(), planetId, userId)) {
%>
                        <TD><%= description %></TD>
<%
                        }
                    } else {
%>                                                          
                    <TD><%= ML.getMLStr(OverviewService.findBonusById(ab.getBonusId()).getName(), userId) %> (<%= ML.getMLStr(OverviewService.findBonusById(ab.getBonusId()).getDescription(), userId) %>)
                        <% if(ab.getDuration() > 0){
                            if(ab.getDuration() < 20) { %>
                            (<FONT style="color:red;"><%= ab.getDuration() %> Ticks</FONT> )
                            <%  }else{ %>
                            (<%= ab.getDuration() %> Ticks)
                            <% }
                            } %>
                    </TD>
<%
                   }
%>                  
                </TR>

                <% }

    %>
               <TR valign="middle"><TD><IMG style="height:20px; width: 20px;" border=0 src="<%= GameConfig.getInstance().picPath()%>pic/menu/icons/happiness.png"/></TD><TD>
                       <SELECT name="bonusId" style='background-color: #000000; color: #ffffff; border:0px; width: 300px; font-size: 12px;'>
                           <%
                   
                    if(bonus.size() > 0){
                    for(BonusEntry be : bonus){ %>
                    <OPTION <% if(!be.isEnabled()) %>disabled<% %> value="<%= be.getId() %>"><%= ML.getMLStr(be.getName(), userId) %> (<%= FormatUtilities.getFormattedNumber(be.getCost()) %> <%= ML.getMLStr("overview_lbl_points", userId) %>)</OPTION>
                <% }
                    }else{ %>
                    <OPTION value="-1" selected><%= ML.getMLStr("overview_opt_noneavailable", userId) %></OPTION>
                       <% }
                        %>
                       </SELECT>
                       <INPUT type="submit" value="<%= ML.getMLStr("overview_but_buy", userId) %>" />
                   </TD>
                   <TD class="blue2" align="center">
                       <A style="font-size:12px;" href="main.jsp?page=new/bonuslist"><%= ML.getMLStr("overview_link_showboni", userId) %></A>
                   </TD>
               </TR>

            </TABLE>
            </FORM>
<%
    }
%>
        </TD>
    </TR>
</TABLE><BR>
<TABLE class="blue" width="80%">
    <TR><TD colspan=3 class="blue" align=center><B><%= ML.getMLStr("overview_lbl_ressources", userId)%></B></TD></TR>
<%
for(PlanetRessource pr : OverviewService.findMineableRessources(planetId)) {
    if ((pr.getRessId() == Ressource.PLANET_YNKELONIUM) && !ResearchService.isResearched(userId, Research.YNKELONIUM)) continue;
    if ((pr.getRessId() == Ressource.PLANET_HOWALGONIUM) && !ResearchService.isResearched(userId, Research.HOWALGONIUMSCANNER)) continue;
%>
<TR>
    <TD><IMG alt="<%= ML.getMLStr(OverviewService.findRessourceById(pr.getRessId()).getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= OverviewService.findRessourceById(pr.getRessId()).getImageLocation() %>"/></TD>
    <TD width="50%"><%= ML.getMLStr(OverviewService.findRessourceById(pr.getRessId()).getName(), userId)%>:</TD>
    <TD width="50%"><%= FormatUtilities.getFormatScaledNumber(pr.getQty(),GameConstants.VALUE_TYPE_NORMAL) %></TD>
</TR>

<%
}
%>
</TABLE>
<P>
<TABLE cellpadding=0 border="0" cellspacing=0 class="blue" width="80%"><TR><TD>
<TABLE cellpadding=0 border="0" cellspacing=0 class="blue" width="100%">
    <TR><TD colspan=8 align="center" class="blue2"><B><%= ML.getMLStr("overview_lbl_production", userId)%></B></TD></TR>
    <TR align="center">
        <TD width="*" class="blue2"></TD>
        <TD width="15%" align="left" class="blue2"><%= ML.getMLStr("overview_lbl_ressource", userId)%></TD>
    <TD class="blue2" align="center" width="15%"><%= ML.getMLStr("overview_lbl_baseproduction", userId) %></TD>
    <TD class="blue2" align="center" width="15%"><%= ML.getMLStr("overview_lbl_effectiveproduction", userId) %></TD>
    <TD class="blue2" align="center"  width="15%"><%= ML.getMLStr("overview_lbl_consumption", userId) %></TD>
    <TD class="blue2" align="center"  width="15%"><%= ML.getMLStr("overview_lbl_preservation", userId)%></TD>
    <TD class="blue2" align="center"  width="15%"><%= ML.getMLStr("overview_lbl_trade", userId)%></TD>
    <TD class="blue2" align="center"  width="15%"><%= ML.getMLStr("lbl_overview_overspill", userId)%></TD></TR>
</TABLE><TABLE cellpadding="0"  border="0" cellspacing="0"  width="100%" style="font-size:13px">
<%
    ProductionResult pr = productionResult;
    HashMap<Integer, Long> trades = TransportService.findShippingBalanceForPlanet(planetId);
    for (at.darkdestiny.core.model.Ressource r : OverviewService.findAllRessources()) {                
            long stock = 0;
            String stockS = "-";

            if (((pr.getRessStock(r.getId()) > 0) && (!r.getMineable())) ||
                (pr.getRessProduction(r.getId()) > 0) ||
                (pr.getRessConsumption(r.getId()) > 0)) {                   
                stock = pr.getRessMaxStock(r.getId());        
%>
            <TR align="center">
                <TD style="background-color: #101010;" align="left" width="*"><IMG alt="<%= ML.getMLStr(r.getName(), userId) %>" height="20" width="20" src="<%= GameConfig.getInstance().picPath() %><%= r.getImageLocation() %>"/></TD>
                 <TD style="background-color: #101010;" align="left" width="15%"><%= ML.getMLStr(r.getName(),userId) %></TD>
            
<%

            long totalIndustry = pr.getRessProduction(r.getId());
            long totalConsumption = pr.getRessConsumption(r.getId());
            long totalIndustryPossible = 0;
            if(r.getId() == Ressource.ENERGY){
                 totalIndustryPossible = (long)(pr.getRessBaseProduction(r.getId()));
            }else if(r.getId() == Ressource.FOOD){
                 totalIndustryPossible = (long)(pr.getRessBaseProduction(r.getId()) * epcr.getEffAgriculture());
            }else{
                 totalIndustryPossible = (long)(pr.getRessBaseProduction(r.getId()) * epcr.getEffIndustry());
            }
            String color = "";
            if (totalIndustryPossible >= 0) {
                color = "#33FF33";
            } else {
                color = "#999933";
            }
%>
             <TD style="background-color: #101010;" width="15%"><FONT color="<%= color %>"><%= FormatUtilities.getFormatScaledNumber(Math.abs(totalIndustryPossible),GameConstants.VALUE_TYPE_NORMAL) %></FONT></TD>
             <%
            color = "";
            if (totalIndustry >= 0) {
                color = "#33FF33";
            } else {
                color = "#999933";
            }
             %>
             <TD style="background-color: #303030;" width="15%"><FONT color="<%= color %>"><%= FormatUtilities.getFormatScaledNumber(Math.abs(totalIndustry),GameConstants.VALUE_TYPE_NORMAL) %></FONT></TD>
             <%

            color = "";
            if (totalConsumption >= 0) {
                color = "#33FF33";
            } else {
                color = "#999933";
            }
             %>
             <TD style="background-color: #303030;" width="15%"><FONT color="#999933"><%= FormatUtilities.getFormatScaledNumber(Math.abs(totalConsumption),GameConstants.VALUE_TYPE_NORMAL) %></FONT></TD>
             <TD style="background-color: #303030;" align="center" width="15%">-</TD>
<%
            // TODO
            Long tradeBalance = trades.get(r.getId());
            if(tradeBalance == null){
                    tradeBalance = 0l;
            }

            
            color = "";
            if (tradeBalance >= 0) {
                color = "#33FF33";
            } else {
                color = "#999933";
            } 
%>
            <TD style="background-color: #303030;" align="center" width="15%"><FONT color="<%= color %>"><%= FormatUtilities.getFormatScaledNumber(Math.abs(tradeBalance),GameConstants.VALUE_TYPE_NORMAL) %></FONT></TD>
<%
            long totalQty = totalIndustry + tradeBalance - totalConsumption;
            color = "";
            if (totalQty >= 0) {
                color = "#33FF33";
            } else {
                color = "#999933";
            }
%>
            <TD style="background-color: #303030;" width="15%"><FONT color="<%= color %>"><%= FormatUtilities.getFormatScaledNumber(Math.abs(totalQty),GameConstants.VALUE_TYPE_NORMAL) %></FONT></TD>
            </TR>
<%
        }
    }
%>
</TABLE>
</TD></TR></TABLE>
<P></P>

<P>
<%
}
%>