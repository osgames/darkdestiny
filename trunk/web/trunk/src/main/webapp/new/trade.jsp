
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="at.darkdestiny.core.GameConfig" %>
<%


int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
                session.setAttribute("actPage", "new/trade");                                              
%>
<TABLE  cellpadding="2" border="0" cellspacing="0">
    <% for(int i = 0; i < RessourceService.getAllStorableRessources().length; i++){
        Ressource r = RessourceService.getAllStorableRessources()[i];
        %>
    <TR>
        <!-- Ressource -->
        <TD valign="top" align="left" width="80px" style="padding-bottom:10px;">
            <TABLE bgcolor="#2E2E2E" width="100%" style=" border-style: solid; border-width: thin;  font-size: 11px; font-weight:bold;">
                <TR>
                    <TD align="center">
                                Rohstoff
                    </TD>
                </TR>
                <TR>
                    <TD align="center" onmouseover="doTooltip(event,'<%= ML.getMLStr(r.getName(), userId) %>')" onmouseout="hideTip()"  >
                                    <IMG style="width:25px; height:25px" src="<%= GameConfig.getInstance().picPath() + r.getImageLocation()%>"/>
                    </TD>
                </TR>
            </TABLE>
        </TD>
        <TD valign="top">
            <TABLE style="font-size:11px; font-weight:bold;" cellpadding="0" cellspacing="0">
                <TR>
                    <TD colspan="2">
                        <TABLE cellpadding="0" cellspacing="0">
                            <TR>
                                <!-- PRICE -->
                                <TD align="left" valign="top">
                                    <TABLE style=" border-style: solid; border-width: thin; font-size: 11px; font-weight:bold;">
                                        <TR  style="background-color:#2E2E2E;">
                                            <TD align="right">
                                                        Preis / 1000 Einheiten
                                            </TD>
                                            <TD align="right">
                                                        Preis / 1 Einheit
                                            </TD>
                                        </TR>
                                        <TR style="background-color:#2E2E2E;">
                                            <TD align="right">
                                                <INPUT style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;" type="textfield" value="0"/>
                                            </TD>
                                            <TD align="right">
                                                <INPUT style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;" type="textfield" value="0"/>
                                            </TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                                <TD width="10px">
                                    
                                </TD>
                                <!-- Amount -->
                                <TD align="left" valign="top">
                                    <TABLE style=" border-style: solid; border-width: thin; ">
                                        <TR style="background-color:#2E2E2E; font-size: 11px; font-weight:bold;">
                                            <TD align="right">
                                                        Verkauf ab
                                            </TD>
                                            <TD align="right">
                                                        Fixe Menge reservieren
                                            </TD>
                                        </TR>
                                        <TR style="background-color:#2E2E2E; font-size: 11px;">
                                            <TD align="left">
                                                <INPUT style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;" type="textfield" value="0"/> Einheiten
                                            </TD>
                                            <TD align="left">
                                                <INPUT style="background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;" type="textfield" value="0"/> Einheiten
                                            </TD>
                                        </TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
                <% if(r.getId() == Ressource.TERKONIT){ %>
                <TR>
                    <TD colspan="2" align="right">
                        R�ckgestellte Mengen
                    </TD>
                </TR>
                <TR>
                    <!-- R�ckgestellte Menge -->
                    <TD bgcolor="#2E2E2E" align="right">
                        800.000 Einheiten 
                    </TD>
                    <TD bgcolor="#2E2E2E" width="18px" align="center">
                        <IMG style="width:15px; height:15px" src="<%= GameConfig.getInstance().picPath()%>pic/cancel.jpg"/>
                    </TD>
                </TR>
                <TR>
                    <TD colspan="2" align="right">
                        Einheiten verkauft
                    </TD>
                </TR>
                <TR>
                    <!-- R�ckgestellte Menge -->
                    <TD colspan="2" bgcolor="#2E2E2E" align="right">
                        123.000 Einheiten
                    </TD>
                </TR>
                        <% } %>
            </TABLE>
        </TD>
    </TR>
    <% } %>
</TABLE>