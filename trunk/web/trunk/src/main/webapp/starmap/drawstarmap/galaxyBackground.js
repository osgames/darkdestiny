

var bgdirector = new CAAT.Director().initialize(mywindowWidth,mywindowHeight,document.getElementById('galaxybg')); 
var bgscene=  bgdirector.createScene(); 

var bgcircle= new CAAT.ShapeActor()
        .setLocation(0,0)
        .setSize(mywindowWidth,mywindowHeight)
        .setFillStyle('#000033')
        .setStrokeStyle('#000000');
 
bgscene.addChild(bgcircle); 
bgdirector.loop(1);   
