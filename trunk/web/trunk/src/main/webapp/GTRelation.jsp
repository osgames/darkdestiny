
<%@page import="at.darkdestiny.core.FormatUtilities"%>
<%@page import="at.darkdestiny.core.enumeration.ERessourceCostType"%>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.utilities.*" %>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="java.util.*" %>
<%

    Locale l = request.getLocale();
    try {
        int userId = Integer.parseInt((String) session.getAttribute("userId"));
        l = ML.getLocale(userId);

    } catch (Exception e) {
    }
    TreeMap<Integer, TreeMap<Integer, GTRelation>> sorted = new TreeMap<Integer, TreeMap<Integer, GTRelation>>();
    for (GTRelation gt : Service.gtRelationDAO.findAll()) {

        TreeMap<Integer, GTRelation> entry1 = sorted.get(gt.getSrcId());
        if (entry1 == null) {
            entry1 = new TreeMap<Integer, GTRelation>();
        }

        entry1.put(gt.getTargetId(), gt);

        sorted.put(gt.getSrcId(), entry1);

    }

    TreeMap<Integer, GroundTroop> sortedTroops = new TreeMap<Integer, GroundTroop>();
    for (GroundTroop gt : Service.groundTroopDAO.findAll()) {
        sortedTroops.put(gt.getId(), gt);
    }
    ArrayList<GroundTroop> troops = new ArrayList<GroundTroop>();
    troops.addAll(sortedTroops.values());
    boolean values = false;
    if (request.getParameter("details") != null) {
        int show = Integer.valueOf(request.getParameter("details"));
        if(show == 1){
            values = true;
        }else{
            values = false;
        }
    }
%>

<CENTER> <a href="GTRelation.jsp?details=<% if (values) {%>0<% } else {%>1<% }%>"><% if (values) {%>Details ausblenden <% } else {%>Details einblenden <% }%></a>
    <TABLE bgcolor="darkgray" style="font-size: 12px; color: black; font-weight: bolder">

        <TR class="blue2">
            <TD></TD>
            <TD></TD>
            <% for (GroundTroop gt1 : troops) {%>
            <TD>
                <%= ML.getMLStr(gt1.getName(), l)%>
            </TD>
            <% }%>

        </TR>

        <%
            int counter = 0;
            for (GroundTroop gt1 : troops) {
                counter++;
        %>
        <%
            if ((counter % 2) != 0) {
        %>

        <TR>
            <% } else {%>

        <TR bgcolor="gray">
            <% }%>
            <TD valign="top">
                <%= ML.getMLStr(gt1.getName(), l)%><BR>
                <%= Service.groundTroopDAO.findById(gt1.getId()).getHitpoints()%>HP
            </TD>
            <%
                boolean first = true;
                for (GroundTroop gt2 : troops) {
                    GTRelation gt = sorted.get(gt1.getId()).get(gt2.getId());

                    double attackPoints = 0;
                    double attackProb = 0;
                    double hitProb = 0;
                    if (gt != null) {
                        attackPoints = gt.getAttackPoints();
                        float total = Service.gtRelationDAO.findTotalAttackprob(gt1.getId());
                        attackProb = Math.round(gt.getAttackProbab() / total * 100d * 1000d) / 1000d;
                        hitProb = gt.getHitProbab();
                    }
                    hitProb = Math.round(hitProb * 1000d) / 1000d;
                    double gt1Credits = Service.ressourceCostDAO.findBy(ERessourceCostType.GROUNDTROOPS, gt1.getId(), Ressource.CREDITS).getQty();
                    double gt2Credits = Service.ressourceCostDAO.findBy(ERessourceCostType.GROUNDTROOPS, gt2.getId(), Ressource.CREDITS).getQty();

                    if (gt1Credits == 0) {
                        gt1Credits = 0;
                    } else if (gt2Credits == 0) {
                        gt2Credits = 0;
                    } else if (gt1Credits < gt2Credits) {
                        gt2Credits = gt2Credits / gt1Credits;
                        gt1Credits = 1;
                    } else if (gt2Credits < gt1Credits) {
                        gt1Credits = gt1Credits / gt2Credits;
                        gt2Credits = 1;

                    } else {
                        gt1Credits = 1;
                        gt2Credits = 1;
                    }
                    gt1Credits = Math.round(gt1Credits * 10d) / 10d;
                    gt2Credits = Math.round(gt2Credits * 10d) / 10d;
                    double sum = Math.round(attackPoints * attackProb * hitProb * 100d) / 100d;

                    double gt1Damage = sum;
                    double gt2Hitpoints = gt2.getHitpoints();
                    double kills = 0d;
                    if (gt1Damage > 0) {
                        kills = gt2Hitpoints / gt1Damage;
                    }
                    double amount = 1;

                    if (kills < 1d && gt1Damage > 0) {
                        amount = amount / kills;
                        kills = 1d;
                    }

                    kills = Math.round(kills * 10d) / 10d;
                    amount = Math.round(amount * 10d) / 10d;




            %>
            <TD valign="top">
                <% if (first) {
                        first = false;
                %>

                <TABLE style="font-size: 12px; color: black;">
                    <% if (values) {%>
                    <TR>
                        <TD>
                            Angriffswahscheinlichkeit
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            Schaden
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            Trefferwahrscheinlichkeit
                        </TD>
                    </TR>
                    <% }%>
                    <TR>
                        <TD>
                            Schaden (summe)
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            Credit-Faktor
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            Enemy-Kills
                        </TD>
                    </TR>
                </TABLE>
            </TD><TD>
                <%
                }%>
                <TABLE style="font-size: 12px; color: black;">

                    <% if (values) {%>
                    <TR>
                        <TD>
                            <%= attackPoints%>
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            <%= attackProb%>%
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            <%= hitProb%>
                        </TD>
                    </TR>
                    <% }%>
                    <TR>
                        <TD>
                            <%= sum%>
                        </TD>
                    </TR>
                    <%if (gt1Credits > 0 && gt2Credits > 0) {%>
                    <TR>
                        <TD>
                            <%= gt1Credits%> : <%= gt2Credits%>
                        </TD>
                    </TR><% } else {%>

                    <TR>
                        <TD>
                            &nbsp;
                        </TD>
                    </TR>
                    <% }%>
                    <TR>
                        <TD>
                            <%= kills%> : <%= amount%>
                        </TD>
                    </TR>
                </TABLE>
            </TD>
            <% }%>
        </TR>
        <% }%>
    </TABLE></CENTER>