<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel"%>
<%@page import="at.darkdestiny.core.databuffer.*"%>
<%@page import="at.darkdestiny.core.StarMapInfo"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.core.xml.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.OutputStreamWriter"%>
<%@page import="at.darkdestiny.framework.access.*" %>
<%
        Locale locale = request.getLocale();

if(session.getAttribute("Language") != null){
    locale = (Locale)session.getAttribute("Language");
}
        session = request.getSession();
        int userID = Integer.parseInt((String) session.getAttribute("userId"));

        XMLMemo techtree = new XMLMemo("Techtree");

        techtree = TechTreeInfo.addConstants(techtree, userID, locale);
        techtree = TechTreeInfo.addUnlockedTechnologies(techtree, userID);
        techtree = TechTreeInfo.addResearches(techtree, userID);
        techtree = TechTreeInfo.addTechRelation(techtree, userID);
        
        // PrintWriter toJSP = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "ISO-8859-1"));
        response.setCharacterEncoding("ISO-8859-1");
        // PrintWriter toJSP = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "ISO-8859-1"));
        PrintWriter toJSP = new PrintWriter(out);
        techtree.write(toJSP);
%>