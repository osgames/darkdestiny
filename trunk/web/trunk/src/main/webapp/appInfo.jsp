<%-- 
    Document   : appInfo
    Created on : 26.09.2014, 18:20:40
    Author     : Jean-R�my
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Informationen zur App</title>
    </head>
    <body>
        <h1>Informationen zur App</h1>
        Die App ist als Ersatz f�r das Applet auf mobilen Ger�ten gedacht. Die Wichtigsten Features sind vorhanden, aber leider noch nicht alle.

        <ul>
            <li><a href="https://play.google.com/store/apps/details?id=at.darkdestiny.vismapgdx.android">Android (Google Play)</a></li>
            <li>iOS: falls jemand einen Apple Developer Account hat, k�nnte er diese Version �bernehmen. Es ist ganz einfach und gibt nicht viel zu tun. Bitte im Forum melden: <a href="http://www.thedarkdestiny.at/board/index.php">Forum</a></li>
        </ul>
    </body>
</html>
