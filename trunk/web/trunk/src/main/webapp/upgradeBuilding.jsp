<%@page import="java.util.ArrayList"%>
<%@page import="at.darkdestiny.core.utilities.ConstructionUtilities"%>
<%@page import="at.darkdestiny.core.buildable.ConstructionExt"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.util.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.construction.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%@page import="at.darkdestiny.core.model.Ressource" %>
<%@page import="at.darkdestiny.core.model.PlayerPlanet" %>
<%@page import="at.darkdestiny.core.model.Construction" %>
<%



session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int constructionId = Integer.parseInt(request.getParameter("constructionId"));


DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"ConstructionId = "+constructionId);

//AbortConstructionResult acr = ConstructionService.getAbortRefund(orderId);
//ConstructionAbortRefund car = acr.getConsAbortRefund();
int count = 1;

            if (request.getParameter("count") != null) {
                count = Integer.parseInt(request.getParameter("count"));
            }
ArrayList<Integer> targetConstructions = ConstructionUtilities.getPossibleUpgradeConstructions(constructionId);

int targetConstructionId = targetConstructions.get(0);

            if (request.getParameter("targetConstructionId") != null) {
                targetConstructionId = Integer.parseInt(request.getParameter("targetConstructionId"));
            }
                    Construction c = Service.constructionDAO.findById(constructionId);
                    Construction cNew = Service.constructionDAO.findById(targetConstructionId);
                    ConstructionUpgradeResult cur = ConstructionUtilities.canBeUpgraded(c, userId, planetId, count, cNew);
                    ConstructionUpgradeCost cuc = cur.getConstructionUpgradeCost();

if (request.getParameter("qi") != null) {
                    int qIdent = Integer.parseInt(request.getParameter("qi"));
                    ConstructionUpgradeBuffer cub = (ConstructionUpgradeBuffer)BufferHandling.getBuffer(userId, qIdent);
                    cub.setParameter(ConstructionUpgradeBuffer.CONSTRUCTION_ID_TARGET, targetConstructionId);
                    cub.setParameter(ConstructionUpgradeBuffer.COUNT, count);
%>
    <jsp:forward page="main.jsp?page=confirm/confirmBuildingUpgrade" />
<%
}

ConstructionUpgradeBuffer coub = new ConstructionUpgradeBuffer(userId);
coub.setParameter(ConstructionUpgradeBuffer.CONSTRUCTION_ID, constructionId);
coub.setParameter(ConstructionUpgradeBuffer.PLANET_ID, planetId);
%>
        <script language="Javascript">
<%
    for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (cuc.getRess(r.getId()) > 0) {
            out.write("var " + r.getName() + "Per = " + cuc.getRess(r.getId()) + ";");
            out.write("\n");
        }else if( cuc.getRess(r.getId()) < 0){
            out.write("var " + r.getName() + "Per = " + (-cuc.getRess(r.getId())) + ";");
            out.write("\n");
            }
    }
%>
            var maxCount = <%= cur.getPossibleCount() %>;

            var actCount = 1;

          //  var totalCredit = db_ressource_creditsPer;
<%
    for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (cuc.getRess(r.getId()) > 0||
                cuc.getRess(r.getId()) < 0) {
            out.write("var total" + r.getName() + " = " + r.getName() + "Per;");
            out.write("\n");
        }
    }
%>

            function calcTotal() {
                if(actCount > maxCount){
                    actCount = maxCount;
                              <%
                              out.write("document.getElementById(\"count\").value = number_format(maxCount,0);");
                              %>

                }
                totalCredit = actCount * db_ressource_creditsPer;
<%
        for (Ressource r : OverviewService.findAllStoreableRessources()) {
            if (cuc.getRess(r.getId()) > 0||
                cuc.getRess(r.getId()) < 0) {
                out.write("total" + r.getName() + " = actCount * " + r.getName() + "Per;");
                out.write("\n");
            }
        }
%>
           //     document.getElementById("db_ressource_creditsPer").innerHTML = number_format(totalCredit,0) + ' Credits';
<%
        for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (cuc.getRess(r.getId()) > 0||
                cuc.getRess(r.getId()) < 0) {
            out.write("document.getElementById(\""+r.getName()+"\").innerHTML = number_format(total"+r.getName()+",0);");
            out.write("\n");
        }
    }
%>
            }

        </script><BR>
        <FORM method='post' action='main.jsp?page=upgradeBuilding&qi=<%= coub.getId() %>' name='confirm'>
            <INPUT type="hidden" name="constructionId" value="<%= constructionId %>">
            <TABLE class="bluetrans" width="80%" style="font-size:13px"><TR>
        <TD colspan=2 align="center"><%= ML.getMLStr("construction_msg_upgradeconstruction", userId) %><BR></TD></TR>
        <TR valign="middle">
            <TD width="200"><%= ML.getMLStr("construction_lbl_constructionType", userId) %>:</TD><TD><%= ML.getMLStr(Service.constructionDAO.findById(constructionId).getName(), userId) %></TD></TR>
        <TR valign="middle">
            <TD width="200"><%= ML.getMLStr("construction_lbl_constructiontargettype", userId) %>:
            </TD><TD>
                <SELECT class="dark" id="targetConId" name="targetConId" onchange="window.location.href='main.jsp?page=upgradeBuilding&constructionId=<%= constructionId %>&targetConstructionId='+document.getElementById('targetConId').value;">>
                    <% for(Integer i : targetConstructions){
                        Construction cTmp = Service.constructionDAO.findById(i);
                        int maxCount = cur.getPossibleCount();
                        if(targetConstructionId != cTmp.getId()){
                            maxCount = ConstructionUtilities.canBeUpgraded(c, userId, planetId, count, cTmp).getPossibleCount();
                        }
                        %>
                        <OPTION <% if(targetConstructionId == cTmp.getId()){ %>SELECTED <% } %>
                            value="<%= cTmp.getId() %>"><%= ML.getMLStr(cTmp.getName(), userId) %>(<%= maxCount %>)</OPTION>
                        <% } %>
                </SELECT>
            </TD></TR>
        <TR valign="middle"><TD><%= ML.getMLStr("construction_lbl_planet", userId)%>:</TD><TD><%= OverviewService.findPlayerPlanetByPlanetId(planetId).getName() %></TD></TR>
        <TR valign="middle"><TD><%= ML.getMLStr("construction_lbl_count",userId)%></TD><TD><INPUT class="dark" name="count" id="count" value="1" SIZE="1" MAXLENGTH="3" ONKEYUP="actCount=document.getElementById('count').value;calcTotal();" /> / <%= cur.getPossibleCount()  %><BR></TD></TR>
        <TR valign="top">
                      <% if (cuc.getRess(Ressource.CREDITS) < 0){
            %>
            <TD><%= ML.getMLStr("construction_lbl_costs", userId)%>:</TD>
            <TD><SPAN id="db_ressource_credits"> <%= FormatUtilities.getFormattedNumber(-cuc.getRess(Ressource.CREDITS)) %></SPAN><BR></TD>

            <%
            }else{
            %>
             <TD><%= ML.getMLStr("construction_lbl_creditrefund", userId)%></TD>
             <TD><SPAN id="db_ressource_credits"> <%= FormatUtilities.getFormattedNumber(cuc.getRess(Ressource.CREDITS)) %></SPAN><BR></TD>

            <%
            }
            %>
        </TR>

            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcecost",userId) %></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
            </TR>
<%
        for (Ressource r : RessourceService.getAllStorableRessources()) {
            if(r.getId() == Ressource.CREDITS)continue;
        if (cuc.getRess(r.getId()) > 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(), userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(cuc.getRess(r.getId())) + "</SPAN></TD></TR>");
        }
    }
%>
        <TR><TD colspan=2 align="CENTER"><BR><INPUT type="submit" name="Ok" value="Ok"></TD></TR>
        </TABLE>
        </FORM>
