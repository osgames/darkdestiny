<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.core.chat.ChatHandler"%>
<%
    session = request.getSession();   
    
    String userId = (String)session.getAttribute("userId");
    
    if (userId != null) {
        ChatHandler.allowAutoLogin(userId, session.getId());
    }
%>