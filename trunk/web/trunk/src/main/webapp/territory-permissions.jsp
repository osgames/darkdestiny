
<%@page import="at.darkdestiny.core.enumeration.ETerritoryType"%>
<%@page import="at.darkdestiny.core.model.TerritoryMap"%>
<%@page import="at.darkdestiny.core.enumeration.ETerritoryMapShareType"%>
<%@page import="at.darkdestiny.core.model.TerritoryMapShare"%>
<%@page import="at.darkdestiny.core.service.TerritoryService"%>
<%@page import="at.darkdestiny.core.result.TerritoryPermissionResult"%>
<%@page import="at.darkdestiny.core.service.AllianceBoardService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="at.darkdestiny.core.service.IdToNameService"%>
<%@page import="at.darkdestiny.core.GameConfig"%>
<%@page import="at.darkdestiny.core.ML"%>
<%@page import="at.darkdestiny.core.enumeration.EAllianceBoardPermissionType"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.core.model.Alliance"%>
<%@page import="at.darkdestiny.core.model.AllianceBoardPermission"%>
<%@page import="at.darkdestiny.core.result.AlliancePermissionResult"%>
<%@page import="at.darkdestiny.core.model.AllianceBoard"%>
<%@page import="at.darkdestiny.core.service.AllianceService"%>
<%

    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));


    int territoryId = 0;
    if (request.getParameter("territoryId") != null) {
        territoryId = Integer.parseInt(request.getParameter("territoryId"));
    }

    int process = 0;
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }

    if (process == 999) {
        TerritoryService.updatePermissions(userId, territoryId, request.getParameterMap());
    }
    TerritoryPermissionResult tpr = TerritoryService.getTerritoryPermissions(userId, territoryId);

%>
<script language="javascript" type="text/javascript">
    var allianceMember = new Array(<%= tpr.getAllianceMembers().size()%>);
    <% for (Map.Entry<Integer, ArrayList<Integer>> entry : tpr.getAllianceMembers().entrySet()) {%>
        allianceMember[<%= entry.getKey()%>] = new Array(<%= entry.getValue().size()%>)
    <% for (Integer i : entry.getValue()) {%>
            allianceMember[<%= entry.getKey()%>][<%= i%>] = <%= i%>;
    <% }%>
    <% }%>
      
        var users = new Array(<%= tpr.getUsers().size()%>);  
    <% for (Map.Entry<Integer, User> entry : tpr.getUsers().entrySet()) {%>
            users[<%= entry.getKey()%>] = "<%= entry.getValue().getGameName()%>";
        
    <% }%>
        var allys = new Array(<%= tpr.getAlliances().size()%>);  
    <% for (Map.Entry<Integer, Alliance> entry : tpr.getAlliances().entrySet()) {%>
            allys[<%= entry.getKey()%>] = "<%= entry.getValue().getName()%>";
        
    <% }%>
        function updatePermissions(){
        
            
        
            var table = document.getElementById("permissionTable");
            var select = document.getElementById('targetEntities');
        
            for (var i=0; i < select.options.length;i++) {
                if (select.options[i].selected == true) {
                    // do something
                    var value = select.options[i].value;
                    var refIdValue = value.substr(0, value.indexOf("-"));
                    var typeValue = value.substr(value.indexOf("-")+1, value.length);
                
                    appendPermission(table,value,refIdValue, typeValue, false, false);
                
                }
            }
    
    
        }
        function appendPermission(table,value, refIdValue, typeValue){
     
                
            var name = "UNKNOWN";
            var typeString = "UNKNOWN";
            if(typeValue == '<%= ETerritoryMapShareType.ALLIANCE.toString()%>'){
                name = allys[refIdValue];
                typeString = 'Allianzberechtigung';
            }else if(typeValue == '<%= ETerritoryMapShareType.USER.toString()%>' || typeValue == '<%= ETerritoryMapShareType.USER_BY_ALLIANCE.toString()%>'
                || typeValue == '<%= ETerritoryMapShareType.USER_BY_ALL.toString()%>'){
                name = users[refIdValue];
                if(typeValue == '<%= ETerritoryMapShareType.USER.toString()%>'){
                    typeString = "Spielerberechtigung";
                }else  if(typeValue == '<%= ETerritoryMapShareType.USER_BY_ALLIANCE.toString()%>'){
                    typeString = 'Berechtigung durch Allianz';
                }else  if(typeValue == '<%= ETerritoryMapShareType.USER_BY_ALL.toString()%>'){
                    typeString = "Berechtigung durch 'Alle'";
                }
            }else if(typeValue == '<%= ETerritoryMapShareType.ALL.toString()%>'){
                name = 'Alle Spieler';
                typeString = '';
            }
                
                
            var tr = document.createElement("tr");
            tr.id = "'" + value + "'";
            tr.setAttribute("id", value);
            var refId = document.createElement("td");
            refId.name = "refId";
            refId.appendChild(document.createTextNode(name));
                
            var idParam = document.createElement("input");
            idParam.setAttribute("type", "hidden");
            idParam.name = "perm_" + value + "-id";
            idParam.value = refIdValue;
                
            var type = document.createElement("td");
            type.name = value;
            type.appendChild(document.createTextNode(typeString));
          //  type.appendChild(document.createTextNode(typeValue));
               
            var typeParam = document.createElement("input");
            typeParam.setAttribute("type", "hidden");
            typeParam.name = "perm_" + value + "-permType";
            typeParam.value = typeValue;
                
            type.appendChild(idParam);
            type.appendChild(typeParam);
                
            /*      var show = document.createElement("td");
                var showSelect =  document.createElement("input");
                showSelect.name = "perm_" + value + "-write";
                showSelect.setAttribute("type", "checkbox");
                if(isShow == "true"){
                    showSelect.setAttribute("checked", "true");
                }
                show.appendChild(writeSelect);*/
                
                
            var action = document.createElement("td");
            if(typeValue == '<%= ETerritoryMapShareType.USER.toString()%>' || typeValue == '<%= ETerritoryMapShareType.ALLIANCE.toString()%>'
                || typeValue == '<%= ETerritoryMapShareType.ALL.toString()%>'){
                var delImage = document.createElement("img");
                var image = "pic/cancel_message.gif";
                delImage.setAttribute("style", "width:17px; height:17px;");
                delImage.setAttribute("onclick", "deletePermission('" + value+ "')");
                delImage.setAttribute("src", image);
                action.appendChild(delImage);
                
            }
            tr.appendChild(refId);
            tr.appendChild(type);
            tr.appendChild(action);
                
                
            var changeButton = document.getElementById('changeButton');
              
            table.appendChild(tr);
            if(typeValue == '<%= ETerritoryMapShareType.ALLIANCE.toString()%>'){
                            
            }
                
            table.appendChild(changeButton);
        }

        function deletePermission(value){
            var table = document.getElementById("permissionTable");
            var tr = document.getElementById(value);
            for(var i = 0; i < table.childNodes.length; i++) {
            
            }
    
            table.removeChild(tr);
            
        }
</script>
<%
    TerritoryMap tm = TerritoryService.getTerritory(userId, territoryId);
%>
<TABLE width="80%">
    <TR>
        <TD align="right">
            <TABLE>
                <TR>
                    <TD>
                        <SELECT multiple size='30' name="targetEntities" id="targetEntities" onclick=''>
                            <OPTION style="background-color: black; color: white;" value="0-<%= ETerritoryMapShareType.ALL%>">Alle Spieler</OPTION>
                            <OPTION value="0">Allianzen selektieren</OPTION>
                            <%

                                for (Alliance a : tpr.getAlliancesSorted()) {
                            %><OPTION style="background-color: black; color: white;" value="<%= a.getId()%>-<%= EAllianceBoardPermissionType.ALLIANCE%>"><%= a.getName()%></OPTION><%
                                      }
                            %>

                            <OPTION value="0">User selektieren</OPTION>
                            <%
                                for (User u : tpr.getUsersSorted()) {
                            %><OPTION style="background-color: black; color: white;" value="<%= u.getUserId()%>-<%= EAllianceBoardPermissionType.USER%>"><%= u.getGameName()%></OPTION><%
                                      }

                            %>
                        </SELECT>
                    </TD>
                </TR>
                <TR>
                    <TD>
                        <INPUT class="dark" type="button" value="Hinzufügen" onclick="updatePermissions();"/>
                    </TD>
                </TR>
            </TABLE>
        </TD>
        <TD align="left" valign="top">
            <FORM action="main.jsp?page=defensemenu&type=4&subType=2&process=999&territoryId=<%= territoryId%>" method="post">
            <TABLE width="300">
                <TR>
                    <TD width="100">
                        Territoriumstyp
                    </TD>
                    <TD width="200" align="middle">
                        <SELECT id="terrType" name="terrType" class="dark">
<%
                            for (ETerritoryType tt : ETerritoryType.values()) {
                                String selected = "";
                                
                                if (tt.equals(tm.getTerritoryType())) {
                                    selected = " SELECTED";
                                }
%>                          
                                <OPTION <%= selected %> value="<%= tt.toString() %>"><%= tt.toString() %></OPTION>
<%
                           }
%>                          
                        </SELECT>
                    </TD>
                </TR>
            </TABLE>
            <BR><BR>            
                <TABLE name="permissions" class="bluetrans" width="600px" id="permissionTable">
                    <TR class="blue2">
                        <TD>
                            Name
                        </TD>
                        <TD>
                            Type
                        </TD>
                        <TD>
                            Aktion
                        </TD>
                    </TR>

                    <TR id="changeButton">
                        <TD valign="bottom" colspan="5" align="center">
                            <BR>
                            <INPUT class="dark" type="submit" value="Übernehmen"/>
                        </TD>
                    </TR>

                </TABLE>
            </FORM>
        </TD>
    </TR>
</TABLE>
<%
    for (TerritoryMapShare tms : tpr.getPermissions()) {
        String id = tms.getRefId() + "-" + tms.getType();
        String name = "UNKNOWN";
        if (tms.getType().equals(ETerritoryMapShareType.ALLIANCE)) {
            name = tpr.getAlliances().get(tms.getRefId()).getName();
        } else if (tms.getType().equals(ETerritoryMapShareType.USER) || tms.getType().equals(ETerritoryMapShareType.USER_BY_ALL) || tms.getType().equals(ETerritoryMapShareType.USER_BY_ALLIANCE)) {
            name = tpr.getUsers().get(tms.getRefId()).getGameName();
        }
%>
<script language="javascript" type="text/javascript">
    appendPermission(document.getElementById("permissionTable"), '<%= id%>', '<%= tms.getRefId()%>', '<%= tms.getType().toString()%>');
</script>
<%
    }
%>

