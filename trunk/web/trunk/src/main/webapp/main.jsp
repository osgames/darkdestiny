
<%@page import="at.darkdestiny.core.ai.ThreadSharedObjects"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.service.FleetService"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.utilities.Menu" %>
<%@page import="at.darkdestiny.core.view.html.table" %>
<%@page import="at.darkdestiny.core.service.LoginService"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.core.model.UserData"%>
<%@page import="at.darkdestiny.core.model.UserSettings"%>
<%@page import="at.darkdestiny.core.notification.NotificationBuffer"%>
<%@page import="at.darkdestiny.core.service.MainService"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.framework.access.DbConnect" %>
<%@page import="at.darkdestiny.core.viewbuffer.*" %>
<%@page buffer="100kb" autoFlush="true" %>
<HTML>
    <HEAD>
        <META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <!-- <META http-equiv="Content-Type" content="text/html; charset=UTF-8" /> -->
        <TITLE>Dark Destiny - The ultimate online space game</TITLE>
        <style type="text/css"></style>
        <link rel="stylesheet" href="main.css.jsp" type="text/css">
        <link rel="shortcut icon" type="image/x-icon" href="<%= GameConfig.getInstance().picPath()%>pic/icons/favicon.ico">
        <script src="java/main.js" type="text/javascript"></script>
        <script src="java/format.js" type="text/javascript"></script>
        <script src="java/translate.js" type="text/javascript"></script>
        <script src="java/translate_static.js" type="text/javascript"></script>
        <script src="java/sorttable.js" type="text/javascript"></script>
        <!-- <script src="java/json.js" type="text/javascript"></script> -->
        <script src="java/shipdesign.js" type="text/javascript"></script>
        <script src="java/management.js" type="text/javascript"></script>
        <script src="java/quest.js" type="text/javascript"></script>
        <script src="java/chat.js" type="text/javascript"></script>
        <script src="java/ajax.js" type="text/javascript"></script>
    </HEAD>
    <%
        GameInit.initGame();
    %>
    <script language="javascript" type="text/javascript">

        var timeServer =<%= GameUtilities.getMillisNextTick()%>
        var adjustMillis = 0;
        var timeAtStart = 0;
        var tickDuration =<%= GameConfig.getInstance().getTicktime()%>;

        function init() {
            var d = new Date();
            timeAtStart = d.getTime();

            countdown();
        }

        var funcStrGlobal = null;

        //SwitchPlanet - Start
        function popupSwitch(mylink, windowname)
        {
            if (!window.focus)
                return true;
            var href;
            if (typeof(mylink) == 'string')
                href = mylink;
            else
                href = mylink.href;

            var h = 100;
            var w = 150;
            var winl = (screen.width - w) / 2;
            var wint = (screen.height - h) / 4;
            if (winl < 0)
                winl = 0;
            if (wint < 0)
                wint = 0;
            var settings = 'height=' + h + ',';
            settings += 'width=' + w + ',';
            settings += 'top=' + wint + ',';
            settings += 'left=' + winl + ',';
            settings += 'resizable=no,';
            settings += 'scrollbars=no,';
            settings += 'status=no';
            window.open(href, windowname, settings);
            return false;
        }
        //Notefunctions - Start
        function popupNote(mylink, windowname)
        {
            if (!window.focus)
                return true;
            var href;
            if (typeof(mylink) == 'string')
                href = mylink;
            else
                href = mylink.href;
            window.open(href, windowname, "width=600,height=400,resizable=no,scrollbars=yes,status=no");
            return false;
        }
        function refreshParent() {
            window.location.reload();

        }
        //Notefunctions - End
        function countdown() {
            var d = new Date();
            var timegone = d.getTime() - timeAtStart;

            if (timeServer < timegone) {
                timeServer += tickDuration;
            }
            var tillTick = ((timeServer - timegone) % tickDuration)

            var minuten = Math.floor(tillTick / 60 / 1000);
            var sekunden = Math.floor(tillTick / 1000 - minuten * 60);
            var hundertstel = Math.floor(tillTick - (minuten * 60 * 1000 + sekunden * 1000));

            document.countdownform.countdowninput.value = minuten + ":" + PadDigits(sekunden, 2);

            setTimeout("countdown();", 250);
        }

        function PadDigits(n, totalDigits)
        {
            nStr = n.toString();
            var pd = '';
            if (totalDigits > nStr.length)
            {
                for (var z = 0; z < (totalDigits - nStr.length); z++)
                {
                    pd += '0';
                }
            }
            return pd + nStr;
        }
    </script>
    <%
        session = request.getSession();

        if (session.getAttribute("userId") != null) {
            CheckForUpdate.requestUpdate();
        }

        String language = "";
        if (request.getParameter("changeLanguage") != null) {
            language = request.getParameter("changeLanguage");
            if (!language.equals("")) {
                MainService.changeLanguage(Integer.parseInt((String) session.getAttribute("userId")), language);
            }
        }


        User u = MainService.findUserByUserId(Integer.parseInt((String) session.getAttribute("userId")));
        int userId = u.getId();

        ThreadSharedObjects.registerThreadSharedObject(ThreadSharedObjects.KEY_LOCALE, ML.getLocale(userId));

        if (!u.getAcceptedUserAgreement()) {
    %>
    <jsp:forward page="userAgreementRequest.jsp" />
    <%        }
        if (u.getAdmin()) {
            session.setAttribute("admin", true);
        }

        String errReqPage = "";
        try {

            Date dat = new Date();
            long time1 = dat.getTime();

            if (request.getParameter("designId") != null) {

                int type = 0;
                boolean pageIsFleet = false;

                if (request.getParameter("type") != null) {
                    type = Integer.parseInt(request.getParameter("type"));
                }

                if (request.getParameter("page") != null) {
                    if (request.getParameter("page").contains("production")) {
                        pageIsFleet = true;
                    }
                }
                if (pageIsFleet && (type == 4)) {
    %>
    <BODY onload="init();" style="margin: 0; padding: 0;" topmargin="0" leftmargin="0" rightmargin="0" BGCOLOR="#000000">
        <!-- safeCall('initSD'); -->
        <%
            }
        } else {
            if (request.getParameter("page") != null) {
                errReqPage = request.getParameter("page");

                if (request.getParameter("page").equalsIgnoreCase("new/alliance") || request.getParameter("page").equalsIgnoreCase("showAlliance")) {
        %>
    <BODY onload="init();" style="margin: 0; padding: 0;" topmargin="0" leftmargin="0" rightmargin="0" BGCOLOR="#000000">
        <!-- safeCall('setPic'); -->
        <%} else {
        %>
    <BODY onload="init()" style="margin: 0; padding: 0;" topmargin="0" leftmargin="0" rightmargin="0" BGCOLOR="#000000">
        <%    }
        } else {
        %>
    <BODY onload="init()" style="margin: 0; padding: 0;" topmargin="0" leftmargin="0" rightmargin="0" BGCOLOR="#000000">
        <%        }
            }

            int subCategory = 0;
            String reqPage = null;
            if (request.getParameter("subCategory") != null) {
                subCategory = Integer.parseInt((String) request.getParameter("subCategory"));

            }
            if (request.getParameter("page") != null) {
                reqPage = (String) (request.getParameter("page")) + ".jsp";

            }

            boolean isAdmin = false;
            if (session.getAttribute("admin") != null) {
                isAdmin = true;
            }

            if ((reqPage != null) && (reqPage.contains("admin/") && !u.getAdmin() && !isAdmin)) {
                reqPage = null;
            }

            if (session.getAttribute("userId") == null) {
        %>
        <jsp:forward page="login.jsp" >
            <jsp:param name="errmsg" value='<%= ML.getMLStr("main_err_sessionexpired", userId)%>' />
        </jsp:forward>
        <%
            } else {
                userId = Integer.parseInt((String) session.getAttribute("userId"));
                User us = LoginService.findUserById(userId);
                if (!us.getAdmin() && session.getAttribute("admin") != null) {
                    //Admin is logged with a non admin acc
                } else {
                    LoginService.setLastUpdateForUserId(GameUtilities.getCurrentTick2(), userId);
                    NotificationBuffer.writeNotifications(u.getUserId());
                    LoginService.addTrackingEntry(userId, request.getRemoteAddr());
                }
            }

            String actPlanet = "0";
            String planetToShow = "0";

            if ((reqPage != null) && (reqPage.equalsIgnoreCase("logout"))) {
        %>
        <jsp:forward page="logout.jsp" />
        <%                }

            if (LoginService.findHomePlanetByUserId(userId) == 0) {
                // session.setAttribute("actPlanet", "" + LoginService.findHomePlanetByUserId(userId));
            }

            if (reqPage == null) {
                reqPage = "empty.jsp";
            }
            // reqPage += ".jsp";
            // DebugBuffer.addLine("Reqpage is " + reqPage);
            if (session.getAttribute("userId") != null) {
                if (session.getAttribute("actPlanet") != null) {
                    actPlanet = (String) session.getAttribute("actPlanet");
                    planetToShow = actPlanet;
                }
            } else {
        %>        
        <jsp:forward page="login.jsp" >
            <jsp:param name="errmsg" value='<%= ML.getMLStr("main_err_invalidsession", userId)%>' />
        </jsp:forward>
        <%
            }

            if (session.getAttribute("homePlanetId") == null) {
                session.setAttribute("homePlanetId", LoginService.findHomePlanetByUserId(userId));
            }
            Menu menu = new Menu(subCategory, userId, Integer.parseInt(actPlanet), isAdmin);
            table leftMenu = menu.getLeftMenuTable();
            String leftMenuString = leftMenu.toString();
            if (subCategory != 99) {
        %>
        <DIV id="darklayer" class="dark" style="display:none">
        </DIV>
        <DIV id="helper" class="helper">
            <DIV id="starmapdetail" class="starmapdetail" style="display:none"></DIV>                
        </DIV>        
        <jsp:include page="top.jsp" flush="false" />
        <% }%>
        <TABLE border="0px"  cellspacing="0" cellpadding="0" width="100%" height="100%">
            <%
                if (!u.getUserConfigured() && session.getAttribute("adminLogin") == null) {
            %>
            <tr><td align="center" valign="top">
                    <jsp:include page='homesysSetup.jsp'/>
                </td></tr>
                <%} else if ((LoginService.findHomePlanetByUserId(userId) == 0) && session.getAttribute("admin") == null && session.getAttribute("adminLogin") != null) {
                %>
            <tr><td align="center" valign="top">
                    <jsp:include page='chooseNewHome.jsp'/>
                </td></tr>
                <%} else if (u.getDeleteDate() != 0) {
                %>
            <tr><td align="center" valign="top">
                    <jsp:include page='accInDeletion.jsp'/>
                </td></tr>
                <%} else {
                %>
            <tr>
                <td align='left' valign='top'  width=137px ><%= leftMenuString%></td>
                <td align='center' valign='top' >
                    <%
                        if (session.getAttribute("admin") != null) {
                            ArrayList<Integer> fleets = FleetService.fleetsInSystemZeroWithStatus0();
                            if (fleets.size() > 0) {
                    %>
                    <TABLE class="bluetrans">
                        <TR>
                            <TD><FONT style="color:red;">
                                Following fleets are in System 0 with status 0, that's bad mkay?
                                <% for (Integer i : fleets) {%>
                                <%= i%> |
                                <% }%>
                                </FONT>
                            </TD>
                        </TR>
                    </TABLE>
                    <% }%>
                    <% }%>
                    <%
                        if (session.getAttribute("admin") != null) {
                            ArrayList<Integer> fleets = FleetService.fleetsInSystemZeroWithoutFleetDetails();
                            if (fleets.size() > 0) {
                    %>
                    <TABLE class="bluetrans">
                        <TR>
                            <TD><FONT style="color:red;">
                                Following fleets are in System 0 with status 1, but have NO FLEET order, that's bad mkay?
                                <% for (Integer i : fleets) {%>
                                <%= i%> |
                                <% }%>
                                </FONT>
                            </TD>
                        </TR>
                    </TABLE>
                    <% }%>
                    <% }%>
                    <%
                        if (!GameUtilities.getShutDownMsg().equals("")) {
                    %><CENTER><FONT size="3" COLOR="FFFF33"><B><%= GameUtilities.getShutDownMsg()%></B></FONT></CENTER><BR><%
                        }
                        if (u.isGuest()) {
                    %><CENTER><FONT size="2" COLOR="FFFF33"><B><%= ML.getMLStr("main_msg_guest", userId)%></B></FONT></CENTER><BR><%
                        }
                        if (!u.getAdmin() && session.getAttribute("admin") != null) {
                    %><CENTER><FONT size="4" COLOR="FF0000"><B>Admin ist geloggt auf einem Account der kein Admin ist</B></FONT></CENTER><BR><%                         }

                        UserSettings us = Service.userSettingsDAO.findByUserId(userId);
                        if (us != null && us.getLeaveDays() > 0) {
                    %><CENTER><FONT COLOR="CC0000"><B><%= ML.getMLStr("main_msg_holidaymode", userId)%></B></FONT></CENTER><BR><%
                        }

                        if (us != null && us.getSittedById() > 0) {
                    %><CENTER><FONT COLOR="CC0000"><B><%= ML.getMLStr("main_msg_sittedBy", userId)%> : <%= MainService.findUserByUserId(us.getSittedById()).getUserName()%></B></FONT></CENTER><BR>
                    <% }

                        if (u.getDeleteDate() > 0) {
                    %><CENTER><FONT COLOR="CC0000"><B><%= ML.getMLStr("main_msg_deletionrequested", userId)%>(<%= GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - u.getDeleteDate())%>)</B></FONT></CENTER><BR>
                    <% }
                        try {
                            if (!reqPage.equals("")) {
                    %><jsp:include page='<%= reqPage%>'/>
                    <% }
                    } catch (Exception e) {
                        reqPage = "new/error.jsp";
                        session.setAttribute("errParams", request.getParameterMap());
                        session.setAttribute("errException", e);
                        System.out.println("E :" + e);
                        e.printStackTrace();
                    %>
                    <jsp:include page='<%= reqPage%>'/>
                    <%                   }
                    %>
            </td>
            <%

                if (!actPlanet.equals("0")) {
                    menu.buildStatusBar();

                    table StatusBar = menu.getStatusBarTable();
                    String statusBarString = StatusBar.toString();
            %>
            <td align='right' valign='top' width=194px ><%= statusBarString%></td>
            <% } else {%>
            <td align='right' valign='top' width=194px >&nbsp;</td>
            <% }%>
        </tr>
        <%
            }

            Date dat2 = new Date();
            long time2 = dat2.getTime();
            float diff = (float) (time2 - time1) / (float) 1000;
        %>
        <TR><TD align="center" style="font-size:9px;" colspan=3><BR>Seite wurde in <%= diff%> Sekunden erstellt
                <BR>Last Restart : <%= GameUtilities.getLastRestart()%></TD></TR>
                <%
                    } catch (IllegalStateException ise) {
                        // No special handling required mostly related to invalid session
                        // will cause logout anyway, even it should not happen
                    } catch (Exception e) {
                        String parameters = "";

                        for (Object me : request.getParameterMap().entrySet()) {
                            Map.Entry mapEntry = (Map.Entry) me;
                            parameters += (String) mapEntry.getKey() + "=[";

                            String[] mapValue = (String[]) mapEntry.getValue();
                            boolean firstLoop = true;
                            for (String tmpStr : mapValue) {
                                if (!firstLoop) {
                                    parameters += ",";
                                }
                                parameters += tmpStr;
                                firstLoop = false;
                            }

                            parameters += "] ";
                        }

                        e.printStackTrace();
                        DebugBuffer.writeStackTrace("Error in Main.jsp:<BR>Requested page: " + errReqPage + "<BR>Parameters: " + parameters + "<BR>", e);
                    }
                %>
    </TABLE>    
</BODY>
</HTML>
<%
// out.flush();

        ThreadSharedObjects.removeThreadSharedObject(ThreadSharedObjects.KEY_LOCALE);
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
%>
