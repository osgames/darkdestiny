<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.construction.*" %>
<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%@page import="at.darkdestiny.core.service.OverviewService" %>
<%@page import="at.darkdestiny.core.model.Ressource" %>
<%@page import="at.darkdestiny.core.model.PlayerPlanet" %>
<%@page import="at.darkdestiny.core.model.Construction" %>
<%@page import="at.darkdestiny.core.model.User" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.util.DebugBuffer" %>
<%
            session = request.getSession();

            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int planetId = Integer.parseInt((String) session.getAttribute("actPlanet"));
            String pass = "";
            if (request.getParameter("pass") != null) {
                pass = request.getParameter("pass");
            }

            int constructionId = 0;
            if (request.getParameter("cId") != null) {
                constructionId = Integer.parseInt(request.getParameter("cId"));
            } else {
                if (request.getParameter("qi") != null) {
                    int qIdent = Integer.parseInt(request.getParameter("qi"));
                    ConstructionScrapBuffer csb = (ConstructionScrapBuffer)BufferHandling.getBuffer(userId, qIdent);
                    csb.setParameter(csb.COUNT, Integer.parseInt(request.getParameter("destroyCount")));
%>
                <jsp:forward page="main.jsp?page=confirm/constructionDestruction" />
<%
                }
            }

            User u = Service.userDAO.findById(userId);

            ScrapConstructionResult scr = ConstructionService.getScrapResult(constructionId, planetId);
            ConstructionScrapCost css = scr.getConsScrapCost();

            ConstructionScrapBuffer csb = new ConstructionScrapBuffer(userId);
            csb.setParameter(csb.CONSTRUCTION_ID, constructionId);
            csb.setParameter(csb.PLANET_ID, planetId);
%>
<script language="Javascript">
    <%
                for (Ressource r : OverviewService.findAllStoreableRessources()) {
                    if (css.getRess(r.getId()) > 0) {
                        out.write("var " + r.getName() + "Per = " + css.getRess(r.getId()) + ";");
                        out.write("\n");
                    }
                }
    %>
        var maxCount = <%= scr.getCount()%>;

        var actCount = 1;

        //  var totalCredit = db_ressource_creditsPer;
    <%
                for (Ressource r : OverviewService.findAllStoreableRessources()) {
                    if (css.getRess(r.getId()) > 0) {
                        out.write("var total" + r.getName() + " = " + r.getName() + "Per;");
                        out.write("\n");
                    }
                }
    %>

        function calcTotal() {
            if(actCount > maxCount){
                actCount = maxCount;
    <%
                out.write("document.getElementById(\"destroyCount\").value = number_format(maxCount,0);");
    %>

            }
            if(actCount < 0 || (!((parseFloat(actCount) == parseInt(actCount)) && !isNaN(actCount)))){
                actCount = 0;
                document.getElementById("destroyCount").value = 0;
            }
            totalCredit = actCount * db_ressource_creditsPer;
    <%
                for (Ressource r : OverviewService.findAllStoreableRessources()) {
                    if (css.getRess(r.getId()) > 0) {
                        out.write("total" + r.getName() + " = actCount * " + r.getName() + "Per;");
                        out.write("\n");
                    }
                }
    %>
            //     document.getElementById("db_ressource_creditsPer").innerHTML = number_format(totalCredit,0) + ' Credits';
    <%
                for (Ressource r : OverviewService.findAllStoreableRessources()) {
                    if (css.getRess(r.getId()) > 0) {
                        out.write("document.getElementById(\"" + r.getName() + "\").innerHTML = number_format(total" + r.getName() + ",0);");
                        out.write("\n");
                    }
                }
    %>
        }
</script>
<FORM method='post' action='main.jsp?page=destroyBuilding&qi=<%= csb.getId() %>' name='confirm' >
    <TABLE width="80%" style="font-size:13px"><TR>
            <% DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "BuildID3 = " + constructionId);%>
        <INPUT type="hidden" name="constructionId" value="<%= constructionId%>" />
        <TD colspan=2 align="center"><%= ML.getMLStr("construction_msg_confirmdestruction", userId)%><BR><BR></TD></TR>
        <TR valign="middle">
            <TD width="200px"><%= ML.getMLStr("construction_lbl_constructionType", userId)%>:</TD><TD><%= ML.getMLStr(scr.getCons().getName(), userId)%></TD></TR>
        <TR valign="middle"><TD><%= ML.getMLStr("construction_lbl_planet", userId)%>:</TD><TD><%= OverviewService.findPlayerPlanetByPlanetId(planetId).getName()%></TD></TR>
        <TR valign="middle"><TD><%= ML.getMLStr("construction_lbl_count", userId)%>:</TD><TD><INPUT name="destroyCount" id="destroyCount" value="1" SIZE="1" MAXLENGTH="3" ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();" /> / <%= scr.getCount()%><BR></TD></TR>
        <TR valign="top"><TD><%= ML.getMLStr("construction_lbl_costs", userId)%>:</TD><TD><SPAN id="db_ressource_credits"> <%= FormatUtilities.getFormattedNumber((long) css.getRess(Ressource.CREDITS))%></SPAN><BR></TD></TR>
        <TR>
            <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcerefund", userId)%>:</B></TD>
        </TR>
        <TR>
            <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
            <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
        </TR>
        <%
                    for (Ressource r : RessourceService.getAllStorableRessources()) {
                        if (r.getId() == Ressource.CREDITS) {
                            continue;
                        }
                        if (css.getRess(r.getId()) > 0) {
                            out.write("<TR><TD>" + ML.getMLStr(r.getName(), userId) + "</TD><TD><SPAN id=\"" + r.getName() + "\">" + FormatUtilities.getFormattedNumber((long) css.getRess(r.getId())) + "</SPAN></TD></TR>");
                        }
                    }
        %>
        <% if ((u.isGuest()) && !pass.equals("one")) {%>
        <TR>
            <TD colspan=2 align="CENTER"><BR>
                <%= ML.getMLStr("global_msg_notavailableasguest", userId)%>
                <INPUT disabled type="submit" name="Ok" value="Ok" />
            </TD>
        </TR>
        <% } else {%>
        <TR>
            <TD colspan=2 align="CENTER"><BR>
                <INPUT type="submit" name="Ok" value="Ok" />
            </TD>
        </TR>
        <% }%>
    </TABLE>
</FORM>
