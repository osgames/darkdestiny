<%@page import="at.darkdestiny.core.requestbuffer.StartLocationBuffer"%>
<%@page import="at.darkdestiny.core.requestbuffer.BufferHandling"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.core.model.Planet"%>
<%@page import="at.darkdestiny.core.service.LoginService"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="java.sql.*" %>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.framework.access.DbConnect" %>
<%
int userId = 0;
int systemId = 0;
session = request.getSession();
Locale locale = request.getLocale();
if(session.getAttribute("Language") != null){
    locale = (Locale)session.getAttribute("Language");
}
try {
    userId = Integer.parseInt((String)session.getAttribute("userId"));
    
    if (request.getParameter("systemId") == null) {
        DebugBuffer.warning("Use Buffer Mode");
        int bufferId = Integer.parseInt(request.getParameter("buf"));    
        DebugBuffer.warning("Buffer Id is " + bufferId);
        StartLocationBuffer slb = (StartLocationBuffer)BufferHandling.getBuffer(userId, bufferId);
        DebugBuffer.warning("SLB is " + slb);
        systemId = slb.getSystemId();
        DebugBuffer.warning("SLB system id is " + systemId);
    } else {
        DebugBuffer.warning("Use system parameter");
        systemId = Integer.parseInt(request.getParameter("systemId"));
        DebugBuffer.warning("System id is " + systemId);
    }
} catch (Exception e) {
    DebugBuffer.error(e);
    DebugBuffer.warning("Invalid call on chooseStartLoc by IP: " + request.getRemoteHost());
    if (request.getRemoteHost().equalsIgnoreCase("83.216.244.30")) {
        response.sendError(response.SC_BAD_REQUEST, "Geh bitte jemand anders auf an Sack und hoer auf diese Seite ohne Parameter aufzurufen ;)");
        return;
    }    
}

if ((userId == 0) || (systemId == 0)) {
    DebugBuffer.fatalError("FATAL ERROR > userId="+userId+" systemId="+systemId);
    DebugBuffer.fatalError("FATAL ERROR Remotehost : " + request.getRemoteHost());
    String ErrorMsg = ML.getMLStr("login_err_creationerror", locale);
%>
        <jsp:forward page="login.jsp" >
                <jsp:param name="errmsg" value='<%= ErrorMsg %>' />
        </jsp:forward>
<%
}

Planet p = LoginService.findMPlanetBySystemId(systemId);
boolean occupied = false;
boolean startable = true;

if (p != null) {

    if(LoginService.findPlayerPlanetByPlanetId(p.getId()) != null){
        occupied = true;
    }
    at.darkdestiny.core.model.System s = Service.systemDAO.findById(p.getSystemId());
    if(!s.getGalaxy().getPlayerStart()){
        
        startable = false;
    }
}

if (occupied || !startable) {
%>
        <jsp:forward page="login.jsp" >
                <jsp:param name="errmsg" value='<%= ML.getMLStr("login_err_systemalreadyinuse", userId) %>' />
        </jsp:forward>
<%
} else {
            
    String ErrorMsg = LoginService.createNewUserEntries(userId,systemId, locale.toString());
    if (ErrorMsg.equalsIgnoreCase("")) {
            session.setAttribute("visJoinMap", "true");
        %>
        <jsp:forward page="enter.jsp"/>
        <%
    } else {
        %>
        <jsp:forward page="login.jsp" >
                <jsp:param name="errmsg" value='<%= ErrorMsg %>' />
        </jsp:forward>
        <%
    }
}

try {
    session.invalidate();
} catch (IllegalStateException ise) {

}
%>