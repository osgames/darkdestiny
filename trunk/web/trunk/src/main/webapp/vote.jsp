<%@page import="at.darkdestiny.core.voting.VoteUtilities"%>
<%@page import="at.darkdestiny.core.voting.Vote"%>
<%@page import="at.darkdestiny.core.service.VotingService"%>
<%@page import="at.darkdestiny.core.model.UserData" %>
<%@page import="at.darkdestiny.core.model.Voting"%>
<%@page import="at.darkdestiny.core.ML"%>
<%@page import="at.darkdestiny.util.DebugBuffer" %>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel" %>

<%

Integer voteId = Integer.parseInt(request.getParameter("voteId"));
Vote v = VoteUtilities.getVote(voteId);

 int userId = Integer.parseInt((String) session.getAttribute("userId"));

%>
<%= VoteUtilities.getHtmlCode(v, voteId, userId) %>
