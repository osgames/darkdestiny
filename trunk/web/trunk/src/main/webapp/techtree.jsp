
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.model.UserSettings"%>
<%@page import="at.darkdestiny.core.model.User" %>
<%@page import="at.darkdestiny.core.model.UserData" %>
<%@page import="at.darkdestiny.core.service.ProfileService" %>
<%
// Determine correct connection path
    int userId = Integer.parseInt((String) session.getAttribute("userId"));
    UserSettings us = Service.userSettingsDAO.findByUserId(userId);
    String xmlUrl = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);
%>
<br><br><CENTER>
    <META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>
    <a href="darkdestiny://<%=xmlUrl.replace("http://", "")%>?session=<%= session.getId()%>&mode=TECHTREE">In der App �ffnen</a> (<a href="appInfo.jsp">Info</a>)
    <BR>

    <applet id="techtree" code="at.darkdestiny.vismap.gui.AppletMain" width="<%= us.getTtvWidth()%>" height="<%= us.getTtvHeight()%>" alt="" archive="applet/vismap.jar">
        <param name="source" value="<%= xmlUrl%>createTechTreeInfo.jsp"/>
        <param name="sessionId" value="<%= request.getSession().getId()%>"/>
        <param name="MODE" value="TECHTREE"/>
        <PARAM NAME="cache_option" VALUE="NO">

    </applet>
</CENTER>
<script language="javascript" type="text/javascript">
    var disableHTMLScroll = false;

    document.getElementById("techtree").addEventListener('click',
            function(e) {
                disableHTMLScroll = true;
            }
    , false);

    document.getElementById("techtree").addEventListener('mouseover',
            function(e) {
                disableHTMLScroll = true;
            }
    , false);

    document.getElementById("techtree").addEventListener('mouseout',
            function(e) {
                disableHTMLScroll = false;
            }
    , false);

    document.addEventListener('DOMMouseScroll', function(e) {
        if (disableHTMLScroll) {
            e.stopPropagation();
            e.preventDefault();
            e.cancelBubble = false;
            return false;
        } else {
            return true;
        }
    }, false);
</script>
