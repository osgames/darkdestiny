<%@page import="at.darkdestiny.core.viewbuffer.HeaderBuffer"%>
<%@page import="at.darkdestiny.core.service.PlanetService"%>
<%@page import="at.darkdestiny.core.result.BaseResult"%>
<%@page import="at.darkdestiny.core.service.SetupService"%>
<%@page import="java.util.Map"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="java.sql.*"%>
<%@page import="at.darkdestiny.framework.access.DbConnect" %>
<%
    session = request.getSession();
    int userId = Integer.parseInt((String) session.getAttribute("userId"));

    Map<String, String[]> allPars = request.getParameterMap();
    if (request.getParameter("submit2") != null) {
        BaseResult br = SetupService.buildParametrizedStartingSystem(userId,allPars);
        if (!br.isError()) {
            session.setAttribute("actPlanet", ""+PlanetService.getHomePlanetForUser(userId));
            HeaderBuffer.reloadUser(userId);
%>
<jsp:forward page="main.jsp?page=welcome" />
<%
       } else {
%>
<jsp:forward page="error.jsp" />
<%
       }
    }
%>

<SCRIPT language="javascript" type="text/javascript">

    function adjustPoints(){
        var basePoints = 10;

        //Homeplanet
        var homePlanetField = document.getElementsByName("homeplanet");
        var homePlanetValue;

        for(i=0; i<homePlanetField.length; i++)
            if(homePlanetField[i].checked == true)
                homePlanetValue = parseInt(homePlanetField[i].value);

        //Population
        var addPopField = document.getElementById("addPop");
        var addPopValue = parseInt(addPopField.options[addPopField.options.selectedIndex].value);


        //Ressources
        var ironRessField = document.getElementsByName("ironress");
        var ironRessValue;

        for(i=0; i<ironRessField.length; i++)
            if(ironRessField[i].checked == true)
                ironRessValue = parseInt(ironRessField[i].value);

        //rare Ressources
        var rareRessField = document.getElementsByName("rareress");
        var rareRessValue;

        for(i=0; i<rareRessField.length; i++)
            if(rareRessField[i].checked == true)
                rareRessValue = parseInt(rareRessField[i].value);


        //Update points
        var points = document.getElementById("points");
        if ( points.hasChildNodes() )
        {
            while ( points.childNodes.length >= 1 )
            {
                points.removeChild( points.firstChild );
            }
        }
        var newPoints = basePoints;
        newPoints += homePlanetValue;
        newPoints += addPopValue;
        newPoints += ironRessValue;
        newPoints += rareRessValue;


        var color="#ffffff";
        if(newPoints > 0){
            color="#00ff00";
        }else if(newPoints < 0){
            color="#ff0000";
        }
        points.setAttribute("color", color);
        var newPointsString = 'Punkte: ' + newPoints + '';
        points.appendChild(document.createTextNode(newPointsString));
        updateSplitPoints(homePlanetValue, addPopValue, ironRessValue, rareRessValue);

    }

    function updateSplitPoints(homePlanetValue, addPopValue, ironRessValue, rareRessValue){

        //Update points
        var splitPointsField = document.getElementById("splitPoints");
        if ( splitPointsField.hasChildNodes() )
        {
            while ( splitPointsField.childNodes.length >= 1 )
            {
                splitPointsField.removeChild( splitPointsField.firstChild );
            }
        }
        if(homePlanetValue > 0 || homePlanetValue < 0)
            appendSplitPoints(splitPointsField, 'Gr��e des Heimatplanetens: ', homePlanetValue + '');
        if(addPopValue > 0 || addPopValue < 0)
            appendSplitPoints(splitPointsField, 'Zus�tzliche Bev�lkerung: ', addPopValue + '');
        if(ironRessValue > 0 || ironRessValue < 0)
            appendSplitPoints(splitPointsField, 'Rohstoffe: ', ironRessValue + '');
        if(rareRessValue > 0 || rareRessValue < 0)
            appendSplitPoints(splitPointsField, 'Besondere Rohstoffe: ', rareRessValue + '');

    }
    function appendSplitPoints(tbody, name, value){


        //Update points
        var tr = document.createElement("TR");
        var td = document.createElement("TD");
        td.appendChild(document.createTextNode(name + value));
        tr.appendChild(td);
        tbody.appendChild(tr);
    }
</SCRIPT>


<FORM method="post" action="main.jsp?page=homesysSetup" Name="Production">
    <TABLE align="center" width="800px" style="font-size:13px">
        <TR>
            <TD>
                <TABLE  class="bluetrans" width="100%">
                    <TR>
                        <TD>
                            Du kannst dir jetzt dein Heimatsystem konfigurieren, du hast 10 Punkte zur Verf�gung, wenn du dir bei den Ressourcen schlechtere Werte einstellst,
                            bekommst du zus�tzliche Punkte mit denen du die maximale Bev�lkerung in deinem Heimatsystem erh�hen kannst.<BR>
                            Falls du ein abgelegenes System gew�hlt hast, ist es ratsam eher mit vielen Rohstoffen zu starten, da es sonst m�glichweise aufwendig werden kann,
                            Rohstoffe zu deinem Hauptplanet zu transportieren!<BR><BR></TD>
                    </TR>
                </TABLE>
            </TD>
        </TR>
        <TR>
            <TD>
                <!-- Punkteberechnung -->
                <TABLE class="bluetrans" style="width:100%;">
                    <TR>
                        <TD>
                            <B><FONT style="font-size:17px">Basispunkte: 10</FONT></B><BR><BR>
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            <TABLE>
                                <TBODY id="splitPoints">

                                </TBODY>
                            </TABLE>
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            <B><FONT style="font-size:17px" id="points" color="#00ff00">Punkte: 10</FONT></B><BR><BR>
                        </TD>
                    </TR>
                </TABLE>
            </TD>
        </TR>
        <TR>
            <TD>
                <!-- Size of homeplanet -->
                <TABLE class="bluetrans" style="font-size:13px; width:100%;"><TR>
                        <TD>
                            <FONT style="font-size:15px"><B>Gr��e des Heimatplanets einstellen (7,5 - 9,5 Mrd. Einwohner)</B></FONT>
                        </td>
                    </TR><TR>
                        <TD>
                            <TABLE style="font-size:13px"><TR valign="middle">
                                    <TD>
                                        <IMG src="<%= GameConfig.getInstance().picPath()%>pic/M/ns1k.png"/>
                                    </TD>
                                    <TD>
                                        <input type="radio" onclick="adjustPoints();" name="homeplanet" value="0" checked>Wenig [ca. 7,5 Mrd.] (0 Punkte)<br>
                                    </td>
                                </tr>
                            </TABLE>
                        </td>
                    </TR><TR>
                        <TD>
                            <TABLE style="font-size:13px"><TR valign="middle">
                                    <TD>
                                        <IMG src="<%= GameConfig.getInstance().picPath()%>pic/M/nm1k.png"/>
                                    </td>
                                    <TD>
                                        <input type="radio" onclick="adjustPoints();" name="homeplanet" value="-6">Normal [ca. 8,5 Mrd.] (-6 Punkte)<br>
                                    </td>
                                </tr>
                            </TABLE>
                        </td>
                    </TR><TR>
                        <TD>
                            <TABLE style="font-size:13px"><TR valign="middle">
                                    <TD>
                                        <IMG src="<%= GameConfig.getInstance().picPath()%>pic/M/nb1k.png"/>
                                    </td>
                                    <TD>
                                        <input type="radio" onclick="adjustPoints();" name="homeplanet" value="-14">Viel [ca. 9,5 Mrd.] (-14 Punkte)<br>
                                    </td>
                                </tr>
                            </TABLE>
                        </td>
                    </TR>
                </TABLE>
            </TD>
        </TR>
        <TR>
            <TD>
                <!-- Additional Population -->
                <TABLE class="bluetrans" style="width:100%;">
                    <TR>
                        <TD>
                            <FONT style="font-size:15px"><B>Zus�tzliche Bev�lkerung auf anderen Planeten im System</B></FONT>
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            <SELECT  onchange="adjustPoints();" size='1' id="addPop" name="addPop">
                                <OPTION value="0" SELECTED>Keine (0 Punkte)</OPTION>
                                <OPTION value="-2">+1 Milliarde (-2 Punkte)</OPTION>
                                <OPTION value="-4">+2 Milliarde (-4 Punkte)</OPTION>
                                <OPTION value="-6">+3 Milliarde (-6 Punkte)</OPTION>
                                <OPTION value="-8">+4 Milliarde (-8 Punkte)</OPTION>
                                <OPTION value="-10">+5 Milliarde (-10 Punkte)</OPTION>
                            </SELECT>
                        </TD>
                    </TR>
                </TABLE>
            </TD>
        </TR>

        <TR>
            <TD>
                <!-- Amout of Ore -->
                <TABLE class="bluetrans"style="font-size:15px; width:100%;">
                    <TR valign="middle">
                        <TD align="left" style="width:320px;"><B>Menge an Eisenerz im System</B></TD>
                        <TD align="left" style="width:25px;"><IMG src="<%= GameConfig.getInstance().picPath()%>pic/menu/icons/ore.png"/></TD>
                        <TD align="left" width="%">&nbsp;</TD>
                    </TR>
                    <TR>
                        <TD colspan="3">
                            <input onclick="adjustPoints();" type="radio" name="ironress" value="2">Wenig [50%] (+2 Punkte)<br>
                            <input onclick="adjustPoints();" type="radio" name="ironress" value="0" checked>Normal (0 Punkte)<br>
                            <input onclick="adjustPoints();" type="radio" name="ironress" value="-4">Viel [200%] (-4 Punkte)
                        </td>
                    </TR>
                </TABLE>
            </TD>
        </TR>
        <TR>
            <TD>
                <!-- Amout of rare Ore -->
                <TABLE  class="bluetrans"style="font-size:15px; width:100%;">
                    <TR valign="middle">
                        <TD align="left" style="width:320px;"><B>Menge an seltenen Rohstoffen im System</B></TD>
                        <TD align="left" style="width:25px;"><IMG src="<%= GameConfig.getInstance().picPath()%>pic/menu/icons/ynkeore.png"/></TD>
                        <TD align="left" style="width:25px;"><IMG src="<%= GameConfig.getInstance().picPath()%>pic/menu/icons/howalore.png"/></TD>
                        <TD align="left" width="%">&nbsp;</TD>
                    </TR>
                    <TR>
                        <TD colspan="4">
                            <input onclick="adjustPoints();"  type="radio" name="rareress" value="4">Keine seltenen Rohstoffe (+4 Punkte)<br>
                            <input onclick="adjustPoints();"  type="radio" name="rareress" value="0" checked>Normal (0 Punkte)<br>
                            <input onclick="adjustPoints();"  type="radio" name="rareress" value="-6">Viel [300%] (-6 Punkte)
                        </TD>
                    </TR>
                </TABLE>
            </TD>
        </TR>
        <TR>

            <TD align="center">
                <BR><input type="submit" name="submit2" value="Los geht's!">
            </TD>
        </TR>
    </TABLE>
</FORM>
