<%--
    Document   : fleetJSON
    Created on : 22.03.2009, 01:43:25
    Author     : Stefan
--%>
<%@page import="at.darkdestiny.core.fleet.FleetData"%>
<%@ page import="at.darkdestiny.core.json.*" %>
<%@ page import="at.darkdestiny.core.*" %>

<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int fleetId = Integer.parseInt(request.getParameter("fId"));

// Security Check
FleetData fd = new FleetData(fleetId);
if (fd.getUserId() != userId) {
    throw new Exception("Invalid Call");
}

response.setCharacterEncoding("ISO-8859-1");
out.write(JSONTestSource.getFleetData(fleetId));
%>