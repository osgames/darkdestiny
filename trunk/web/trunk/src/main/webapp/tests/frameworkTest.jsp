<%@page import="at.darkdestiny.framework.*" %>
<%@page import="at.darkdestiny.core.log.LogEntry" %>
<%@page import="java.util.*" %>

<%
Runtime rt = Runtime.getRuntime();
ReadOnlyTable rot = new ReadOnlyTable<LogEntry>(LogEntry.class);

Logger.getLogger().write("Execute Query");

QueryKeySet qks = new QueryKeySet();
qks.addKey(new QueryKey("locationId",14109));
qks.addKey(new QueryKey("userid",55));
qks.addKey(new QueryKey("time",335166l));

// Testing search on primary key
ArrayList<LogEntry> results = rot.find("primary",qks);
Logger.getLogger().write("RES SIZE = " + results.size());

// Testing get 
qks.addKey(new QueryKey("internalId",0));
qks.addKey(new QueryKey("entryType",1));
LogEntry result = (LogEntry)rot.get(qks);
Logger.getLogger().write("Found entry " + result.getRemark());

// Test custom index
QueryKeySet qks2 = new QueryKeySet();
qks2.addKey(new QueryKey("count",1l));
results = rot.find("countIndex",qks2);
Logger.getLogger().write("RES SIZE = " + results.size());
%>
