<%-- 
    Document   : RessTest
    Created on : 14.07.2009, 19:18:50
    Author     : Stefan
--%>
<%@page import="at.darkdestiny.core.*" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
<%
    RessCalcTest rc = new RessCalcTest();
    
    double totalTime = 0d;
    double totalTime2 = 0d;
            
    out.write("Starting test ...<BR><BR>");
    out.flush();        
    
    for (int i=0;i<500;i++) {   
        if (i > 0) {                        
            totalTime += Double.parseDouble(rc.testRun().replace(",", "."));
            totalTime2 += Double.parseDouble(rc.testRun2().replace(",", "."));
        }
    }            
       
    rc.rc.switchOutputRess(GameConstants.OUTPUT_PLANET_STOCK);
    
    out.write("Average TestRun 1 time was: " + FormatUtilities.getFormattedDecimal(totalTime / 499, 2) + " ms<BR>");
    out.write("Average TestRun 2 time was: " + FormatUtilities.getFormattedDecimal(totalTime2 / 499, 2) + " ms<BR><BR>");    
    
    Logger.getLogger().write("TEST RC = " + rc + " RC.RC="+rc.rc + " RC.RPT="+rc.rpt);
    Logger.getLogger().write("RC.RPT.NODE1="+rc.rpt.getNode(1));
    Logger.getLogger().write("RC.RPT.NODE2="+rc.rpt.getNode(2));
    Logger.getLogger().write("RC.RPT.NODE3="+rc.rpt.getNode(2));
%>
    <table>
        <tr><td colspan="3">ALT</td></tr>
        <tr><td>Storage</td><td>Production</td><td>Consumption</td></tr>
        <tr><td>Iron: <%= rc.rc.getOutput(1) %></td><td>Iron: <%= rc.rc.getProdCons(1)[0] %></td><td>Iron: <%= rc.rc.getProdCons(1)[1] %></td></tr>
        <tr><td>Steel: <%= rc.rc.getOutput(2) %></td><td>Steel: <%= rc.rc.getProdCons(2)[0] %></td><td>Steel: <%= rc.rc.getProdCons(2)[1] %></td></tr>
        <tr><td>Terkonit: <%= rc.rc.getOutput(3) %></td><td>Terkonit: <%= rc.rc.getProdCons(3)[0] %></td><td>Terkonit: <%= rc.rc.getProdCons(3)[1] %></td></tr>
    </table>
    <table>
        <tr><td colspan="3">NEU</td></tr>
        <tr><td>Storage</td><td>Production</td><td>Consumption</td></tr>
        <tr><td>Iron: <%= rc.rpt.getNode(1).getActStorage() %></td><td>Iron: <%= rc.rpt.getNode(1).getActProduction() %></td><td>Iron: <%= rc.rpt.getNode(1).getActConsumption() %></td></tr>
        <tr><td>Steel: <%= rc.rpt.getNode(2).getActStorage() %></td><td>Steel: <%= rc.rpt.getNode(2).getActProduction() %></td><td>Steel: <%= rc.rpt.getNode(2).getActConsumption() %></td></tr>
        <tr><td>Terkonit: <%= rc.rpt.getNode(3).getActStorage() %></td><td>Terkonit: <%= rc.rpt.getNode(3).getActProduction() %></td><td>Terkonit: <%= rc.rpt.getNode(3).getActConsumption() %></td></tr>      
    </table>    
    </body>
</html>
