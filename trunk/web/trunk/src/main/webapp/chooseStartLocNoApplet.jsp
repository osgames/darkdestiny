<%@page import="at.darkdestiny.core.requestbuffer.BufferHandling"%>
<%@page import="at.darkdestiny.core.requestbuffer.StartLocationBuffer"%>
<%@page import="at.darkdestiny.core.FormatUtilities"%>
<%@page import="at.darkdestiny.core.result.StartLocationResult"%>
<%@page import="at.darkdestiny.core.result.BaseResult"%>
<%@page import="at.darkdestiny.core.JoinableGalaxyEntry"%>
<%@page import="at.darkdestiny.core.service.StartService"%>
<%@page import="at.darkdestiny.core.view.JoinableGalaxyView"%>
<%@page import="at.darkdestiny.core.GameConstants"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="java.util.Locale" %>
<%@page import="at.darkdestiny.core.ML" %><%
// Determine correct connection path
    session = request.getSession();

    Locale locale = request.getLocale();
    if (session.getAttribute("Language") != null) {
        locale = (Locale) session.getAttribute("Language");
    }
    int userId = 0;
    try {
        userId = Integer.parseInt((String) session.getAttribute("userId"));
    } catch (Exception e) {
    }


    if ((userId == 0) || ((session.getAttribute("actSystem") != null && (session.getAttribute("adminLogin") == null)))) {

        java.lang.System.out.println("userId" + userId);
        java.lang.System.out.println("session.getAttribute()" + session.getAttribute("actSystem"));
        java.lang.System.out.println(session.getAttribute("adminLogin"));
        try {
            session.invalidate();
        } catch (IllegalStateException ise) {
        }
%>
<jsp:forward page="login.jsp">
    <jsp:param name="errmsg" value='Alte Session gel�scht bitte erneut einloggen!' />
</jsp:forward>
<%    }

    StringBuffer xmlUrlBuffer = request.getRequestURL();
    String xmlUrl = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);

    int bufferId = 0;
    if (request.getParameter("buf") != null) {
        bufferId = Integer.parseInt(request.getParameter("buf"));
    }
    
    if (BufferHandling.getBuffer(userId,bufferId) != null) {
        BufferHandling.getBuffer(userId, bufferId).invalidate();
        bufferId = 0;
    }
    
    StartLocationResult slr = null;    
    
    if (request.getParameter("galaxy") != null) {
        int galaxyId = Integer.parseInt(request.getParameter("galaxy"));
        slr = StartService.setStartingLocation(userId, request.getParameterMap()); 
        
        if (!slr.isError() && !slr.isInternalError()) {
            StartLocationBuffer slb = new StartLocationBuffer(userId);       
            slb.setParameter(StartLocationBuffer.GALAXY_ID, slr.getChoosenGalaxy());
            slb.setParameter(StartLocationBuffer.SYSTEM_ID, slr.getChoosenSystem());
            bufferId = slb.getId();
            
            DebugBuffer.warning("Created Buffer " + bufferId + " with system ID " + slr.getChoosenSystem()) ;
        }
    }
%>
<html>
    <head>
        <title>
            Dark Destiny - The ultimate online strategy game
        </title>
    </head>
    <body bgcolor="black" background="./pic/bg2.jpg" >

    <center>
        <FORM method="post" action="chooseStartLocNoApplet.jsp">
            <TABLE width="60%" align="center">
<%
if ((slr != null) && slr.isError()) {
%>                
                <TR>
                    <TD>
                        <TABLE width="100%" bgcolor="#FFFF00">                                                               
                            <TR><TD><IMG src="./pic/warning.png" height="100" width="100" /><TD><TD>
                                <FONT color="#000000"><B><%= ML.getMLStr(slr.getMessage(), locale) %></B></FONT><BR/><BR/>
                            </TD></TR>
                        </TABLE>                                         
                    </TD>
                </TR>
<%
}
%>                
                <TR>
                    <TD>
                        <P><BR/><font color="yellow" size="+1"><B><%= ML.getMLStr("altjoinmap_msg_1", locale)%><BR /><BR/>
                                <%= ML.getMLStr("altjoinmap_msg_2", locale)%><BR/><P/>
<%
                                if ((slr == null) || (slr.isError())) {
%>                                  
                                <TABLE style="color:yellow;" align="center">
                                    <TR><TD><%= ML.getMLStr("alt_startloc_galaxy", userId) %>:</TD>
                                        <TD>
                                            <SELECT name="galaxy">
<%
                                                JoinableGalaxyView jgv = StartService.getJoinableGalaxies();
                                                boolean first = true;
                                                for (JoinableGalaxyEntry jge : jgv.getAllEntries()) {                                                    
%>
                                                    <OPTION value="<%= jge.getGalaxyId() %>" <%= first ? "SELECTED" : "" %>>Galaxie <%= jge.getGalaxyId() %> (<%= jge.getxSize() %>x<%= jge.getySize() %>) [<%= jge.getPlayerCount() %> Spieler]</OPTION>
<%
                                                    first = false;
                                                }
%>
                                            </SELECT>                                            
                                        </TD></TR>
                                    <TR><TD><%= ML.getMLStr("alt_startloc_distancetoother", userId) %>:</TD>
                                        <TD>
                                            <SELECT name="distance">
                                                <OPTION value="1" SELECTED><%= ML.getMLStr("alt_startloc_opt_random", userId) %></OPTION>
                                                <OPTION value="2"><%= ML.getMLStr("alt_startloc_opt_maximized", userId) %></OPTION>
                                                <OPTION value="3"><%= ML.getMLStr("alt_startloc_opt_locationcode", userId) %></OPTION>
                                            </SELECT>
                                        </TD>
                                    </TR>
                                    <TR><TD><%= ML.getMLStr("alt_startloc_locationcode", userId) %>:</TD><TD><INPUT Name="locationcode" TYPE="text" VALUE="" SIZE="15"</TD></TR>
                                    <TR><TD colspan="2" align="middle"><BR/><INPUT type="submit" name="submit" value="<%= ML.getMLStr("alt_startloc_findsystem", userId) %>"></TD></TR>                                    
                                </TABLE>
<%
                                } else {
%>                                            
                                <TABLE style="color:white; font-weight: bold;" align="center">
                                    <TR>
                                        <TD width="300"><%= ML.getMLStr("alt_startloc_galaxy", userId) %>:</TD>
                                        <TD>Galaxie #<%= slr.getChoosenGalaxy() %></TD>
                                    </TR>
                                    <TR>
                                        <TD><%= ML.getMLStr("alt_startloc_selsystem", userId) %>:</TD>
                                        <TD>System #<%= slr.getChoosenSystem() %></TD>
                                    </TR>
                                    <TR>
                                        <TD><%= ML.getMLStr("alt_startloc_distancetoother", userId) %>:</TD>
                                        <TD><%= slr.getDistanceToNextPlayer() == 0d ? "-" : FormatUtilities.getFormattedDecimal(slr.getDistanceToNextPlayer(), 2) %> LJ</TD>
                                    </TR>         
                                    <TR>
                                        <TD>&nbsp;</TD>
                                        <TD>&nbsp;</TD>
                                    </TR>                                        
                                    <TR>
                                        <TD><INPUT onclick="window.location.href = 'chooseStartLocNoApplet.jsp?buf=<%= bufferId %>'" type="button" name="reload" value="<%= ML.getMLStr("alt_startloc_backtoselect", userId) %>"></TD>
                                        <TD><INPUT onclick="window.location.href = 'chooseStartLoc.jsp?buf=<%= bufferId %>'" type="button" name="submit" value="<%= ML.getMLStr("alt_startloc_go", userId) %>"></TD>
                                    </TR>                                          
                                </TABLE>
<%
                                }
%>            
                            </B></font>
                        </P>
                    </TD>
                </TR>
            </TABLE>
        </FORM>
    </center>
</body>
</html>