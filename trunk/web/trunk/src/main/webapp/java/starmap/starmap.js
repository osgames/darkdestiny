var basePath = baseUrl;
var joinmap = true;

function loadStartLocInfoJSON(x, y, callBack) {
	$.ajax({
		dataType: 'json',
		url: basePath + 'api/StarMapJSON/joinselection',
                data: {x: x, y: y},
		xhrFields: {
			withCredentials: true
		}
	}).done(function(data) {
			callBack(data,x,y);
	});
}

function exitMap() {
	$.ajax({
		dataType: 'json',
		url: basePath + 'api/StarMapJSON/exit',
		xhrFields: {
			withCredentials: true
		}
	}).done(function(data) {
            console.log('Exit Map: ' + data);
	});
}

function loadStarsJSON(callBack) {
	$.ajax({
		dataType: 'json',
		url: basePath + 'api/StarMapJSON/systems',
		xhrFields: {
			withCredentials: true
		}
	}).done(function(data) {
			callBack(data);
	});
}

function loadFleetsJSON(callBack) {
        // console.log('Load fleets');
    
	if (!joinmap) {
                // console.log('Load fleets 2');
		$.ajax({
			dataType: 'json',
			url: basePath + 'api/StarMapJSON/fleets',
			xhrFields: {
				withCredentials: true
			}
		}).done(function(data) {
				callBack(data);
		});
	}
}

function loadObjectInfo(x, y, zoom) {	
    htmlCall(basePath + 'GetStarMapData?x='+Math.round(x)+'&y='+Math.round(y)+'&z='+zoom,displayDIV);
}

function sendFleet(fleetId, targetType, targetId, toSystem) {
    // console.log(fleetId + ',' + targetType + ',' + targetId + ',' + toSystem);

    $.ajax({
        url: basePath + 'api/StarMapJSON/sendfleet',
	// url: basePath + 'new/fleetapplet.jsp',
	xhrFields: {
		withCredentials: true
	},	
	data: {fleetId: fleetId, targetType: targetType, targetId: targetId, toSystem: toSystem}
	}).done(function(data) {
                if (!data.success) {
                    alert('FEHLER: ' + data.errorMessage);
                }
		processFleetSendResponse(data,fleetId,data.success);                
		// alert( "Response:" + data + " DataLength: " + data.length);    
	});
}

function removeStartMessage() {
    document.getElementById("darklayer").style.display = "none";
    document.getElementById("starmapdetail").style.display = "none";    
}

function sendJoinRequest(identifier,x,y) {
    window.location.href = basePath + '/chooseStartLocByTileNew.jsp?sId=' + identifier + '&x=' + x + '&y=' + y;
}

function displayStartMessage(data,x,y) {
    // console.log(data);
    if (data.reqSuccess) {
        document.getElementById("starmapdetail").innerHTML = '';

        var content = "<TABLE>";
        content += "<TR><TD class=\"joinmsg\" colspan=\"2\">"+data.message+"<BR><BR></TD></TR>";
        content += "<TR class=\"joinbuttons\"><TD><button onclick=\"sendJoinRequest('"+data.selectedTile+"',"+x+","+y+");\" type=\"button\">OK</button></TD><TD><button onclick=\"removeStartMessage();\" type=\"button\">Abbrechen</button></TD></TR>";
        content += "</TABLE>";

        $('#starmapdetail').append(content);
        document.getElementById("darklayer").style.display = "";
        document.getElementById("starmapdetail").style.display = "";
    }
}

function processFleetSendResponse(data,fleetId,success) {
	// Ends drawing and removes all moving 
	if (success) {
            addFinalMovement(fleetId);
        }
	stopDrawMode();
}

function startSendFleet(fleetId) {
        var fleetData = getFleetById(fleetId);
    
	document.getElementById("actionpanelfleetname").innerHTML = fleetData.name;
	document.getElementById("fleetId").value = fleetId;
	
	document.getElementById("actionpanel").style.display = "";
        document.getElementById("darklayer").style.display = "none";
        document.getElementById("starmapdetail").style.display = "none";	
	
        globalFleetRange = Number(fleetData.range);
        
	for (var i = 0; i < starFeatures.length; i++) {		
		var currFeature = starFeatures[i];
		
		// console.log('Compare ' + currFeature.get('id') + ' to ' + fleetId + '(Type: ' + currFeature.get('type') + ')');
		
		if ((currFeature.get('id') == fleetId) && (currFeature.get('type') == 'Fleet')) {
			console.log('Found feature ' + fleetId);
			
			var fCoord = [];
			fCoord[0] = currFeature.get('locX');
			fCoord[1] = currFeature.get('locY');
			
			addStartMarkerAt(fCoord);
		}
	}
}

function selectTarget(isPlanet,targetId) {
	console.log('Target is Planet? ' + isPlanet + ' targetId? ' + targetId); 
	
	var rangeY = 20037300; // 20026376.39;
	var rangeX = 20037300;	
	
	if (isPlanet) {
		$.each( stars, function( key, value ) {
			$.each( value.planets, function( key2, value2 ) {
				if (value2.id === targetId) {
					document.getElementById("actionpaneltargetsys").innerHTML = 'System: ' + value.name + ' ['+value.id+']';
					document.getElementById("actionpaneltargetplanet").innerHTML = 'Planet: ' + value2.name + ' ['+value2.id+']';
					
					document.getElementById("targetType").value = 'PLANET';
					document.getElementById("targetId").value = targetId;
					document.getElementById("toSystem").value = 'false';
					
					var fCoord = [];

					var xNew = (((rangeX * 2) * (value.x - 0)) / 10000) - rangeX;
					var yNew = (((rangeY * 2) * (value.y - 0)) / -10000) + rangeY;					
					
					fCoord[0] = xNew;
					fCoord[1] = yNew;
					
					addTargetMarkerAt(fCoord);					
				}		
			});
		});	
	} else {
		$.each( stars, function( key, value ) {
			if (value.id === targetId) {
				document.getElementById("actionpaneltargetsys").innerHTML = 'System: ' + value.name + ' ['+targetId+']';
				document.getElementById("actionpaneltargetplanet").innerHTML = 'Planet: -';
				
				document.getElementById("targetType").value = 'SYSTEM';
				document.getElementById("targetId").value = targetId;
				document.getElementById("toSystem").value = 'true';				
				
				var fCoord = [];

				var xNew = (((rangeX * 2) * (value.x - 0)) / 10000) - rangeX;
				var yNew = (((rangeY * 2) * (value.y - 0)) / -10000) + rangeY;					
					
				fCoord[0] = xNew;
				fCoord[1] = yNew;
					
				addTargetMarkerAt(fCoord);					
			}
		});
	}
}

function displayDIV(content) {
    document.getElementById("darklayer").style.display = "";
    
    document.getElementById("darklayer").onclick = function() {
        document.getElementById("darklayer").style.display = "none";
        document.getElementById("starmapdetail").style.display = "none";
    };
    
    document.getElementById("helper").onclick = function() {
        document.getElementById("darklayer").style.display = "none";
        document.getElementById("starmapdetail").style.display = "none";
    };   
    
    document.getElementById("starmapdetail").onclick = function(event) {
        event.stopPropagation();
    };
    
    document.getElementById("starmapdetail").style.display = "";    
    document.getElementById("starmapdetail").innerHTML = content;        
}

function reloadStarMap(op) {
    //alert('Execute operation ' + op + ' and load image into element ' + document);
    smImage = document.getElementById("starmapwindow");
    smImage.src = basePath + 'GetStarMap?op='+op+'&x='+x+'&y='+y+'&anticache='+makeid();
    
}

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function showDetails(id) {
    htmlCall(basePath + 'GetStarMapData?id='+id,setDetailContent);
    
}

function setDetailContent(content) {
    document.getElementById("smdetailinfo").innerHTML = content;
}

function getFleetById(fleetId) {
    var currFleet;
    
    $.each(fleets, function(key, value) {
        console.log('Compare ' + Number(value.id) + '('+value.name+') vs ' + Number(fleetId));
        if (Number(value.id) === Number(fleetId)) {        
            console.log('equal');
            currFleet = value;
        } else {
            console.log('Not equal');
        }
    });
    
    return currFleet;
}