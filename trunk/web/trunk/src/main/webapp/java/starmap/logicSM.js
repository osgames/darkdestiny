/**
 * Define a namespace for the application.
 */
window.app = {};
var app = window.app;
var globalFleetRange = 0;
// Define the buttons
/**
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 */
app.GoToHome = function(opt_options) {

    var options = opt_options || {};

    var button = document.createElement('button');
    button.innerHTML = 'H';

    var this_ = this;
    var handleGoToHome = function(e) {
        map.getView().setCenter(homeCoord);
        map.getView().setZoom(7);
    };

    button.addEventListener('click', handleGoToHome, false);
    button.addEventListener('touchstart', handleGoToHome, false);


    var element = document.createElement('div');
    element.className = 'rotate-north ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

app.ExitButton = function(opt_options) {

    var options = opt_options || {};

    var button = document.createElement('button');
    button.innerHTML = '<IMG src="./pic/starmapJS/pic/exiticon.png" />';

    var this_ = this;
    var handleExit = function(e) {
        exitMap();
        history.go(-1);
    };

    button.addEventListener('click', handleExit, false);
    button.addEventListener('touchstart', handleExit, false);


    var element = document.createElement('div');
    element.className = 'exit ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

app.RefreshButton = function(opt_options) {

    var options = opt_options || {};

    var button = document.createElement('button');
    button.innerHTML = '<IMG src="./pic/starmapJS/pic/refreshicon.png" />';

    var this_ = this;
    var handleRefresh = function(e) {
         generation++;
         baseSource.setTileUrlFunction(function (tileCoord) {
         return urlTemplate.replace('{z}', (tileCoord[0] - 1).toString())
         .replace('{x}', tileCoord[1].toString())
         .replace('{y}', (-tileCoord[2] - 1).toString()) + '&g=' + generation;
         });        
    };

    button.addEventListener('click', handleRefresh, false);
    button.addEventListener('touchstart', handleRefresh, false);


    var element = document.createElement('div');
    element.className = 'refresh ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

// Inherit the buttons
ol.inherits(app.GoToHome, ol.control.Control);
ol.inherits(app.ExitButton, ol.control.Control);
ol.inherits(app.RefreshButton, ol.control.Control);

// Map logic
var homeCoord = [];
var sendStartCoord = [];

var attribution = new ol.Attribution({
    html: 'Aurora Starmap Prototype'
});

var projection = ol.proj.get('EPSG:3857');

// The tile size supported by the ArcGIS tile service.
var tileSize = 512;

var generation = 0;

console.log('BASEURL: ' + baseUrl);

var urlTemplate = baseUrl +'/GetTile' +
        '?z={z}&y={y}&x={x}';
var urlFunction = function(tileCoord) {
    return urlTemplate.replace('{z}', (tileCoord[0] - 1).toString())
            .replace('{x}', tileCoord[1].toString())
            .replace('{y}', (-tileCoord[2] - 1).toString());
};

var restUrl = 'http://localhost:8084/darkdestiny/tiles/features';
var starFeatures = [];
var fleetFeatures = [];
var actionFeatures = [];

var rangeY = 20037300; // 20026376.39;
var rangeX = 20037300;

var iconStyles = [];
var styles = [];
var baseScale = 0.28;

function getIconScale(currZoomValue) {
    if (currZoomValue > 9000) {
        return baseScale;
    } else if (currZoomValue > 2000) {
        return 0.5;
    } else if (currZoomValue > 1000) {
        return 0.75;
    } else {
        return 1;
    }
}

var iconStyleGray = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/gStar.gif',
    scale: baseScale
}));

var iconStyleYellow = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/yStar.gif',
    scale: baseScale
}));

var iconStyleHome = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/homeStar.gif',
    scale: baseScale
}));

var iconStartMarker = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/marker_start.png',
    scale: 1
}));

var iconTargetMarker = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/marker_target.png',
    scale: 1
}));

var iconFleetMarkerTop = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 1],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/marker_top.svg',
    imgSize: [100, 100],
    scale: baseScale
}));

var iconFleetMarkerRight = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.2, 0.55],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/marker_right.svg',
    imgSize: [100, 100],
    scale: baseScale
}));

var iconFleetMarkerLeft = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 0.55],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/marker_left.svg',
    imgSize: [100, 100],
    scale: baseScale
}));

var iconFleetGoRight = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/Fleet_Outline_Mirr.png',
    scale: 1
}));

var iconFleetGoLeft = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    opacity: 1,
    src: '/darkdestiny/pic/starmapJS/Fleet_Outline.png',
    scale: 1
}));

var i = -1;

var sourcePoly = new ol.source.Vector({
    // features: starFeatures,
    projection: 'EPSG:3857',
    // url: 'dd.geojson',		
    format: new ol.format.GeoJSON(),
    wrapX: false
});

var sourceAction = new ol.source.Vector({
    features: actionFeatures,
    projection: 'EPSG:3857',
    format: new ol.format.GeoJSON(),
    wrapX: false
});

// alert('Loading stars');
loadStarsJSON(loadStars);
joinmap = false;
loadFleetsJSON(loadFleets);

// Add fleets to map
function loadFleets(data) {
    // console.log('Load fleets');
    fleets = data;

    $.each(fleets, function(key, value) {
        // console.log('Iterating fleet ' + i);
        i++;

        var xNew = (((rangeX * 2) * (value.x - 0)) / 10000) - rangeX;
        var yNew = (((rangeY * 2) * (value.y - 0)) / -10000) + rangeY;
        var xNew2;
        var yNew2;

        if (value.moving) {
            xNew2 = (((rangeX * 2) * (value.targetX - 0)) / 10000) - rangeX;
            yNew2 = (((rangeY * 2) * (value.targetY - 0)) / -10000) + rangeY;
        }

        var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point([xNew, yNew]),
            // geometry: new ol.geom.Point(ol.proj.transform([180 - (Math.random() * 360), 90 - (Math.random() * 180)], 'EPSG:4326', 'EPSG:3857')),
            id: value.id,
            locX: xNew,
            locY: yNew,
            type: 'Fleet'
        });

        var iconStyle2 = null;

        // console.log('[' + i + '] Add fleet with status ' + value.status);

        if (!value.moving) {
            if (value.status == 1) {
                // Adjusted
                iconStyle2 = new ol.style.Style({
                    image: iconFleetMarkerTop,
                });
            }

            if (iconStyle2 == null) {
                if ((value.status == 2) || (value.status == 10) || (value.status == 11)) {
                    // Adjusted
                    iconStyle2 = new ol.style.Style({
                        image: iconFleetMarkerRight,
                    });
                } else {
                    iconStyle2 = new ol.style.Style({
                        image: iconFleetMarkerLeft,
                    });
                }
            }
        } else {
            if (value.x < value.targetX) {
                iconStyle2 = new ol.style.Style({
                    image: iconFleetGoRight,
                });
            } else {
                iconStyle2 = new ol.style.Style({
                    image: iconFleetGoLeft,
                });
            }

            var points = [
                [xNew, yNew], [xNew2, yNew2]
            ];

            var lineStyle = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: [0, 255, 0, 1],
                    width: 1
                })
            });

            var featureLine = new ol.Feature({
                geometry: new ol.geom.LineString(points),
            });


            featureLine.setStyle(lineStyle);

            styles.push(lineStyle);
            starFeatures[i] = featureLine;
            sourcePoly.addFeature(featureLine);
            i++;
        }

        styles.push(iconStyle2);

        iconFeature.setStyle(iconStyle2);
        starFeatures[i] = iconFeature;
        sourcePoly.addFeature(iconFeature);
    });
}

// Add stars to map
function loadStars(data) {
    stars = data;
    $.each(stars, function(key, value) {

        i++;

        var xNew = (((rangeX * 2) * (value.x - 0)) / 10000) - rangeX;
        var yNew = (((rangeY * 2) * (value.y - 0)) / -10000) + rangeY;

        var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point([xNew, yNew]),
            // geometry: new ol.geom.Point(ol.proj.transform([180 - (Math.random() * 360), 90 - (Math.random() * 180)], 'EPSG:4326', 'EPSG:3857')),
            id: value.id,
            locX: xNew,
            locY: yNew,
            type: 'Star'
        });

        var iconStyle2;
        var ownHomeSys = false;

        if (value.homeSystem) {
            $.each(value.planets, function(key2, value2) {
                if (value2.homePlanet && (value2.diplomacyId == 1)) {
                    ownHomeSys = true;
                }
            });
        }

        if (value.homeSystem) {
            if (ownHomeSys) {
                // console.log('Set home sys at ' + xNew + ':' + yNew);
                homeCoord = [
                    xNew,
                    yNew
                ];
            }
            iconStyle2 = new ol.style.Style({
                image: iconStyleHome,
            });
        } else if (value.sysStatus == 0) {
            iconStyle2 = new ol.style.Style({
                image: iconStyleYellow,
            });
        } else {
            iconStyle2 = new ol.style.Style({
                image: iconStyleGray,
            });
        }


        styles.push(iconStyle2);

        iconFeature.setStyle(iconStyle2);
        starFeatures[i] = iconFeature;

        sourcePoly.addFeature(iconFeature);
    });

    map.getView().setCenter(homeCoord);
    map.getView().setZoom(7);
}

/*
 var source = new ol.source.Vector({
 format: new ol.format.GeoJSON(),
 wrapX: false
 });
 */

var vector = new ol.layer.Vector({
    source: sourcePoly
});

var actionVector = new ol.layer.Vector({
    source: sourceAction
});

// var transformFn = ol.proj.getTransform('EPSG:4326','EPSG:3857');
/*
 var featureGeometry = new ol.geom.Polygon(
 [[[6868325.613592796, -6418264.39104968],
 [1193640.6337013096, -665307.8941941746], 
 [5811660.134578526, -1526294.5807983996], 
 [6516103.787254706, 3952711.6066830344], 
 [8590298.986801252, -1917652.1656185016], 
 [11447209.355987996, 2191602.4749925733], 
 [11055851.771167897, -3013453.4031147882], 
 [15321649.445707016, -1682837.614726441], 
 [9294742.63947744, -6653078.941941742], 
 [9451285.673405476, -10253568.722286683], 
 [6985732.889038831, -9823075.37898457], 
 [6868325.613592796, -6418264.39104968]]]
 );
 
 // var featureGeometryTf = featureGeometry.applyTransform(transformFn);
 var featurePoly = new ol.Feature({
 geometry: featureGeometry
 });
 sourceAction.addFeature(featurePoly);			
 */
/*
 var vector = new ol.layer.Vector({
 source: source,
 style: new ol.style.Style({
 fill: new ol.style.Fill({
 color: 'rgba(255, 255, 255, 0.2)'
 }),
 stroke: new ol.style.Stroke({
 color: '#ffcc33',
 width: 2
 }),
 image: new ol.style.Circle({
 radius: 7,
 fill: new ol.style.Fill({
 color: '#ffcc33'
 })
 })
 })
 });
 */

var baseSource = new ol.source.XYZ({
    wrapX: false,
    noWrap: true,
    attributions: [attribution],
    maxZoom: 9,
    projection: projection,
    tileSize: tileSize,
    tileUrlFunction: urlFunction
});

var map = new ol.Map({
    controls: ol.control.defaults({
        attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
            collapsible: false
        })
    }).extend([
        new app.GoToHome()
    ]).extend([
        new app.ExitButton()
    ]).extend([
        new app.RefreshButton()
    ]),
    target: 'map',
    layers: [
        new ol.layer.Tile({
            source: baseSource
        }),
        vector,
        actionVector
                // vector
    ],
    view: new ol.View({
        center: [0, 0],
        projection: projection,
        zoom: 2,
        minZoom: 2,
        maxZoom: 11
    })
});

map.getView().setCenter(homeCoord);
map.getView().setZoom(7);
var currScale = getIconScale(1200);

iconStyleGray.setScale(currScale);
iconStyleYellow.setScale(currScale);
iconStyleHome.setScale(currScale);
iconFleetMarkerTop.setScale(currScale);
iconFleetMarkerRight.setScale(currScale);
iconFleetMarkerLeft.setScale(currScale);

map.getView().on('change:resolution', function(e) {
    // console.log(e.target.get(e.key));
    var newValue = e.target.get(e.key);
    var newScale = getIconScale(newValue);

    iconStyleGray.setScale(newScale);
    iconStyleYellow.setScale(newScale);
    iconStyleHome.setScale(newScale);
    iconFleetMarkerTop.setScale(newScale);
    iconFleetMarkerRight.setScale(newScale);
    iconFleetMarkerLeft.setScale(newScale);

});

var element = document.getElementById('popup');

var popup = new ol.Overlay({
    element: element,
    positioning: 'bottom-center',
    stopEvent: false
});
map.addOverlay(popup);

function displayFeatureMessage(feature) {
    if (feature) {
        // console.log('It is a me, a feature at ' + feature.get('locX') + ':' + feature.get('locY'));
    }
}

// Add a start marker at this location
function addStartMarkerAt(coordinate) {
    // console.log('Add marker at ' + coordinate[0] + ':' + coordinate[1]);

    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point([coordinate[0], coordinate[1]]),
        // geometry: new ol.geom.Point(ol.proj.transform([180 - (Math.random() * 360), 90 - (Math.random() * 180)], 'EPSG:4326', 'EPSG:3857')),
        locX: coordinate[0],
        locY: coordinate[1]
    });

    var iconStyle2 = new ol.style.Style({
        image: iconStartMarker,
    });

    iconFeature.setStyle(iconStyle2);
    actionFeatures[1] = iconFeature;

    iconFeature.setId('sm');
    sourceAction.addFeature(iconFeature);

    sendStartCoord[0] = coordinate[0];
    sendStartCoord[1] = coordinate[1];

    addInteraction();
}

// Add a target marker at this location
function addTargetMarkerAt(coordinate) {
    // console.log('Add marker at ' + coordinate[0] + ':' + coordinate[1]);

    var oldFeat = sourceAction.getFeatureById('tm');
    if (oldFeat !== null) {
        sourceAction.removeFeature(oldFeat);
    }

    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point([coordinate[0], coordinate[1]]),
        // geometry: new ol.geom.Point(ol.proj.transform([180 - (Math.random() * 360), 90 - (Math.random() * 180)], 'EPSG:4326', 'EPSG:3857')),
        // locX: xNew,
        // locY: yNew
    });

    var iconStyle2 = new ol.style.Style({
        image: iconTargetMarker,
    });

    iconFeature.setStyle(iconStyle2);
    actionFeatures[1] = iconFeature;

    iconFeature.setId('tm');
    sourceAction.addFeature(iconFeature);

    sendStartCoord[0] = coordinate[0];
    sendStartCoord[1] = coordinate[1];

    // Draw line from start to target marker
    var oldPath = sourceAction.getFeatureById('path');
    if (oldPath !== null) {
        sourceAction.removeFeature(oldPath);
    }    
    
    var startFeat = sourceAction.getFeatureById('sm');
    if (startFeat !== null) {
        var points = [
            [startFeat.get('locX'), startFeat.get('locY')], [coordinate[0], coordinate[1]]
        ];

        var lineStyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: [255, 0, 0, 1],
                width: 2
            })
        });

        var featureLine = new ol.Feature({
            geometry: new ol.geom.LineString(points),
            sX: startFeat.get('locX'),
            sY: startFeat.get('locY'),
            tX: coordinate[0],
            tY: coordinate[1]
        });
        featureLine.setId('path');
        featureLine.setStyle(lineStyle);

        sourceAction.addFeature(featureLine);
    }

}

// display popup on click
map.on('click', function(evt) {
    evt.stopPropagation();

    var realCoord = transformProjectionToPixel(evt.coordinate);

    // console.log('zoom=>' + map.getView().getZoom());
    loadObjectInfo(realCoord[0], realCoord[1], map.getView().getZoom());
    // loadStartLocInfoJSON(Math.round(realCoord[0]),Math.round(realCoord[1]),displayStartMessage);
    /*
     var feature = map.forEachFeatureAtPixel(evt.pixel,
     function(feature, layer) {
     // displayFeatureMessage(feature);
     
     
     if (typeof feature.get('locX') !== 'undefined') {				
     console.log('Loading Object Info (' + feature + ') at ' + feature.get('locX') + ':' + feature.get('locY'));
     loadObjectInfo(realCoord[0],realCoord[1],null);
     }
     // displayDIV("<FONT style=\"color: #FFFFFF;\">Hello</FONT>");
     });
     
     /*
     var geometry = feature.getGeometry();
     var coord = geometry.getCoordinates();
     popup.setPosition(coord);
     $(element).popover({
     'placement': 'top',
     'html': true,
     'content': feature.get('name')
     });
     $(element).popover('show');
     } else {
     $(element).popover('destroy');
     }
     */
});

map.on('pointermove', function(evt) {
    if (evt.dragging) {
        $(element).popover('destroy');
        return;
    }
    // var pixel = map.getEventPixel(evt.originalEvent);
    // var hit = map.hasFeatureAtPixel(pixel);
    // map.getTarget().style.cursor = hit ? 'pointer' : '';		

    // transformProjectionToPixel(evt.coordinate);
    // var pixel = map.getEventPixel(evt.originalEvent);   
    // console.log('Mouseover: ' + pixel + ' C: ' + evt.coordinate);
    /*
     var element = popup.getElement();
     var coordinate = evt.coordinate;
     var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
     coordinate, 'EPSG:3857', 'EPSG:4326'));
     
     $(element).popover('destroy');
     popup.setPosition(coordinate);
     // the keys are quoted to prevent renaming in ADVANCED mode.
     $(element).popover({
     'placement': 'top',
     'animation': false,
     'html': true,
     'content': '<p>The location you clicked was:</p><code>' + hdms + '</code>'
     });
     $(element).popover('show');
     */
});

function transformProjectionToPixel(coord) {
    // console.log("X: " + coord[0] + " Y: " + coord[1]);  

    // X 2.000.000 => 10000
    // X -2.000.000 => 0
    // Y 2.000.000 => 0
    // Y -2.000.000 => 10000  
    var realCoord = [];

    realCoord[0] = 10000 / (rangeX * 2) * (coord[0] + rangeX);
    realCoord[1] = 10000 / (rangeY * 2) * ((coord[1] - rangeY) * -1);

    // console.log(coord[0] + "/" + coord[1] + " => " + realCoord[0] + "/" + realCoord[1]);
    return realCoord;
}

// var typeSelect = document.getElementById('type');

var draw; // global so we can remove it later

var circleStyleRed = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: 'red',
        width: 3
    }),
    fill: new ol.style.Fill({
        color: 'rgba(255, 0, 0, 0.1)'
    })
})

var circleStyleGreen = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: 'green',
        width: 3
    }),
    fill: new ol.style.Fill({
        color: 'rgba(0, 255, 0, 0.1)'
    })
})

function addInteraction() {
    //x var value = typeSelect.value;
    //x if (value !== 'None') {
    /*
     var geometryFunction, maxPoints;
     if (value === 'Square') {
     value = 'Circle';
     geometryFunction = ol.interaction.Draw.createRegularPolygon(4);
     } else if (value === 'Box') {
     value = 'LineString';
     maxPoints = 2;
     geometryFunction = function (coordinates, geometry) {
     if (!geometry) {
     geometry = new ol.geom.Polygon(null);
     }
     var start = coordinates[0];
     var end = coordinates[1];
     geometry.setCoordinates([
     [start, [start[0], end[1]], end, [end[0], start[1]], start]
     ]);
     return geometry;
     };
     }
     */
    var ct = 0;

    draw = new ol.interaction.Draw({
        source: sourceAction,
        maxPoints: 2,
        type: 'LineString',
        geometryFunction: function(c, g) {
            // console.log('TRIGGER THE NIGGER');

            if (typeof g !== 'undefined') {
                g.setCoordinates(c);
            } else {
                c[0][0] = sendStartCoord[0];
                c[0][1] = sendStartCoord[1];
                g = new ol.geom.LineString(c);
            }
            if (c[0].length > ct) {
                // console.log('click coord : ' + c[0][c[0].length - 1]);
                ct = c[0].length;
            } else {
                var startX = c[0][0];
                var startY = c[0][1];

                var startCoord = [];
                startCoord[0] = c[0][0];
                startCoord[1] = c[0][1];

                var currX = c[c.length - 1][0];
                var currY = c[c.length - 1][1];

                var endCoord = [];
                endCoord[0] = c[c.length - 1][0];
                endCoord[1] = c[c.length - 1][1];

                var pxStartCoord = transformProjectionToPixel(startCoord);
                var pxEndCoord = transformProjectionToPixel(endCoord);
                // console.log('move coord : ' + currX + ':' + currY);
                // console.log('start coord : ' + startX + ':' + startY);

                var radiusPx = Math.sqrt(Math.pow(pxStartCoord[0] - pxEndCoord[0], 2) + Math.pow(pxStartCoord[1] - pxEndCoord[1], 2));
                var radius = Math.sqrt(Math.pow(startX - currX, 2) + Math.pow(startY - currY, 2));

                // console.log('Radius: ' + radius + ' PxRadius: ' + radiusPx);

                // var oldFeat = null;																							
                var oldFeat = sourceAction.getFeatureById('rc');
                // console.log(oldFeat);
                if (oldFeat !== null) {
                    sourceAction.removeFeature(oldFeat);
                }

                var feat = new ol.Feature(
                        new ol.geom.Circle([(startX), (startY)], radius)
                        );

                if (radiusPx < globalFleetRange) {
                    feat.setStyle(circleStyleGreen);
                } else {
                    feat.setStyle(circleStyleRed);
                }
                feat.setId('rc');
                sourceAction.addFeature(feat);
            }

            return g;
        }
    });

    draw.on('drawstart', function(evt) {
        console.info('drawstart');
    });

    draw.on('drawend', function(evt) {
        console.info('drawend');
        //console.info(evt);
        var currGeom = evt.feature.getGeometry();
        // console.log(evt.feature.getGeometry());

        var wkt = evt.feature.getGeometry().toString();
        // console.log(wkt);

        var geoJSON = new ol.format.GeoJSON();
        var geoJSONText = geoJSON.writeFeatureObject(evt.feature);

        // var oldFeat = null;																							
        var oldFeat = sourceAction.getFeatureById('rc');
        // console.log(oldFeat);
        if (oldFeat !== null) {
            sourceAction.removeFeature(oldFeat);
        }

        /*
         generation++;
         baseSource.setTileUrlFunction(function (tileCoord) {
         return urlTemplate.replace('{z}', (tileCoord[0] - 1).toString())
         .replace('{x}', tileCoord[1].toString())
         .replace('{y}', (-tileCoord[2] - 1).toString()) + '&g=' + generation;
         });
         */
    });

    map.addInteraction(draw);

    var event = $.Event('click'); //create a click event in your draw method using JQuery 
    event.coordinate = [sendStartCoord[0], sendStartCoord[1]];// set your starting coordinate
    draw.startDrawing_(event);
    // }
}

/**
 * Let user change the geometry type.
 * @param {Event} e Change event.
 */
/*
typeSelect.onchange = function(e) {
    map.removeInteraction(draw);
    addInteraction();
};
*/

// addInteraction();
function stopDrawMode() {
    var oldFeat = sourceAction.getFeatureById('rc');
    if (oldFeat !== null) {
        sourceAction.removeFeature(oldFeat);
    }

    oldFeat = sourceAction.getFeatureById('sm');
    if (oldFeat !== null) {
        sourceAction.removeFeature(oldFeat);
    }

    oldFeat = sourceAction.getFeatureById('tm');
    if (oldFeat !== null) {
        sourceAction.removeFeature(oldFeat);
    }

    oldFeat = sourceAction.getFeatureById('path');
    if (oldFeat !== null) {
        sourceAction.removeFeature(oldFeat);
    }

    console.log('Element: ' + document.getElementById("actionpanel").style);
    document.getElementById("actionpanel").style = 'display: none;';
    document.getElementById("actionpanel").style.display = 'none';

    map.removeInteraction(draw);
}

function addFinalMovement(fleetId) {
    var lineFeat = sourceAction.getFeatureById('path');
    if (lineFeat !== null) {
        var points = [
            [lineFeat.get('sX'), lineFeat.get('sY')], [lineFeat.get('tX'), lineFeat.get('tY')]
        ];

        var lineStyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: [0, 255, 0, 1],
                width: 1
            })
        });

        var featureLine = new ol.Feature({
            geometry: new ol.geom.LineString(points),
        });

        featureLine.setStyle(lineStyle);

        sourceAction.addFeature(featureLine);

        // Find old feature and remove
        $.each(sourcePoly.getFeatures(), function(key, value) {
            // console.log('Looping Object ' + value.type + ' -- ' + value.id);
            if (Number(value.get('id')) === Number(fleetId)) {
                // console.log('Found matching number');
                if (value.get('type') === 'Fleet') {
                    // console.log('Found matching type');
                    sourcePoly.removeFeature(value);
                }
            }
        });
        
        var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point([lineFeat.get('sX'), lineFeat.get('sY')]),
        });

        if (lineFeat.get('sX') < lineFeat.get('tX')) {
            var iconStyle2 = new ol.style.Style({
                image: iconFleetGoRight,
            });
            iconFeature.setStyle(iconStyle2);
        } else {
            var iconStyle2 = new ol.style.Style({
                image: iconFleetGoLeft,
            });
            iconFeature.setStyle(iconStyle2);
        }

        // actionFeatures[1] = iconFeature;							
        // iconFeature.setId('sm');
        sourceAction.addFeature(iconFeature);
    }
}