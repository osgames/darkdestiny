function ajaxCall(ajaxLink, callbackMethod){
    //erstellen des requests
    var req = null;

    try{
        req = new XMLHttpRequest();
    }
    catch (ms){
        try{
            req = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (nonms){
            try{
                req = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (failed){
                req = null;
            }
        }
    }

    if (req == null) alert("Error creating request object!");

      //anfrage erstellen (GET, url ist localhost,
      //request ist asynchron
      req.open("GET", ajaxLink, true);
      var responseTxt = new String();

    //Beim abschliessen des request wird diese Funktion ausgeführt
      req.onreadystatechange = function(responseTxt){
        switch(req.readyState) {
                case 4:
                if(req.status!=200) {
                    // alert("Fehler:"+req.status);
                    return false;
                }else{
                    // alert(req.responseText);
                    //schreibe die antwort in den div container mit der id content
                    var defenseData = eval('('+req.responseText+')');
                    callbackMethod(defenseData);
                }
                break;

                default:
                    return false;
                break;
            }
        };

      req.setRequestHeader("Content-Type",
                          "application/x-www-form-urlencoded");
      req.send(null);

      return responseTxt;
}

function htmlCall(htmlLink, callbackMethod){
    //erstellen des requests
    var req = null;

    try{
        req = new XMLHttpRequest();
    }
    catch (ms){
        try{
            req = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (nonms){
            try{
                req = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (failed){
                req = null;
            }
        }
    }

    if (req == null) alert("Error creating request object!");
	  req.withCredentials = true;
      //anfrage erstellen (GET, url ist localhost,
      //request ist asynchron
      req.open("GET", htmlLink, true);
      var responseTxt = new String();

    //Beim abschliessen des request wird diese Funktion ausgeführt
      req.onreadystatechange = function(responseTxt){
        switch(req.readyState) {
                case 4:
                if(req.status!=200) {				
                    alert("Fehler:"+req.status+" Text: "+req.statusText);
                }else{
                    // alert(req.responseText);
                    //schreibe die antwort in den div container mit der id content
                    callbackMethod(req.responseText);
                }
                break;

                default:
                    return false;
                break;
            }
        };
	  
	  console.log(window.location.origin);
	  
      req.setRequestHeader("Content-Type",
                          "text/plain");				 
      req.send("");

      return responseTxt;
}