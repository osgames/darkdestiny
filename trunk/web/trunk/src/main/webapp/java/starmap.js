function loadObjectInfo(x, y, basePath) {
    htmlCall(basePath + 'GetStarMapData?x='+Math.round(x)+'&y='+Math.round(y),displayDIV);
}

function displayDIV(content) {
    document.getElementById("darklayer").style.display = "";
    
    document.getElementById("darklayer").onclick = function() {
        document.getElementById("darklayer").style.display = "none";
        document.getElementById("starmapdetail").style.display = "none";
    };
    
    document.getElementById("helper").onclick = function() {
        document.getElementById("darklayer").style.display = "none";
        document.getElementById("starmapdetail").style.display = "none";
    };   
    
    document.getElementById("starmapdetail").onclick = function(event) {
        event.stopPropagation();
    };
    
    document.getElementById("starmapdetail").style.display = "";    
    document.getElementById("starmapdetail").innerHTML = content;        
}

function reloadStarMap(op) {
    //alert('Execute operation ' + op + ' and load image into element ' + document);
    smImage = document.getElementById("starmapwindow");
    smImage.src = basePath + 'GetStarMap?op='+op+'&x='+x+'&y='+y+'&anticache='+makeid();
    
}

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function showDetails(id) {
    htmlCall(basePath + 'GetStarMapData?id='+id,setDetailContent);
    
}

function setDetailContent(content) {
    document.getElementById("smdetailinfo").innerHTML = content;
}