function refreshPage(messageList) {
    // alert('Refreshing: ' + messageList);

    var usercontainer = document.getElementById("users");
    var userContent = '';        
    
    userContent += '<B>Benutzer:</B><BR><BR>';
    
    for (i=0;i<messageList.users.length;i++) {
        if (messageList.users[i].inactive === 'true') {
            userContent += '<FONT color=\"gray\">';
            userContent += messageList.users[i].name;
            userContent += '</FONT>';
        } else {
            userContent += messageList.users[i].name;
        }
        userContent += '<BR>';
    }    
    
    usercontainer.innerHTML = userContent;    
    
    var textcontainer = document.getElementById("textcontent");
    var chatContent = '';        
    
    for (i=0;i<messageList.messages.length;i++) {
        chatContent += messageList.messages[i].timestamp;
        chatContent += ' ' + messageList.messages[i].line;
        chatContent += '<BR>';
    }    
    
    textcontainer.innerHTML = chatContent;
}

function sendMsg(elem,basePath) {
    var text = elem.value;
    ajaxCall(basePath + 'SendChatMsg?msg='+text, refreshPage);
    elem.value = '';
}

function refreshChat(basePath) {  
    ajaxCall(basePath + 'SendChatMsg', refreshPage);
}