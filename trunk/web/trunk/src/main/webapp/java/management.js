var picPath = "";
var globalUserId = 0;

function myLittleFunction(i){
    alert(i);
}

function processManagementData(managementData) {
    var planetId = managementData.planetId;


    updateRessources(planetId, managementData);
    updateRawMaterials(planetId, managementData);
    updateManagementValues(planetId, managementData);
    updateRunningConstructions(planetId, managementData);
    updateConstructionTable(planetId, managementData)
    updateWarningsTable(planetId, managementData);
    updateNotesTable(planetId, managementData);
}

function updateRessources(planetId, planetData){
    var field = document.getElementById(planetId + "_ressources");
    if ( field.hasChildNodes() )
    {
        while ( field.childNodes.length >= 1 )
        {
            field.removeChild( field.firstChild );
        }
    }
    for(var i = 0; i < planetData.ressources.length; i++){
        var ressourceId = planetData.ressources[i].id;
        var stock = planetData.ressources[i].value;
        if(stock != null){
            var ressourceTbody = document.createElement("tbody");
            var ressourceRow = document.createElement("tr");
            var ressourceImageCell = document.createElement("td");
            ressourceImageCell.valign = "top";
            ressourceImageCell.align= "center";
            ressourceImageCell.width= "17px";
            //IE
            ressourceImageCell.style.borderStyle = "solid";
            ressourceImageCell.style.borderWidth = "0.1pt";
            ressourceImageCell.style.borderColor = "#999999";
            //Other Browsers
            ressourceImageCell.setAttribute("style", "border-style:solid; border-width: thin;");



            var img = document.createElement("img");

            img.style.width = "17px";
            img.style.height = "17px";
            img.id = planetId + "_ressources_img" + "_" + ressourceId;
            img.setAttribute("style", "width:17px; height:17px;");
            img.setAttribute("src", picPath + planetData.ressources[i].image);
            ressourceImageCell.appendChild(img);


            var ressourceTextCell = document.createElement("td");
            ressourceTextCell.appendChild(document.createTextNode(stock));
            ressourceTextCell.align = "right";
            ressourceTextCell.valign = "top";

            //IE
            ressourceTextCell.style.borderStyle = "solid";
            ressourceTextCell.style.borderWidth = "thin";
            ressourceTextCell.style.borderWidth = "0.1pt";
            ressourceTextCell.style.borderColor = "#999999";

            ressourceTextCell.setAttribute("style", "border-style: solid; border-width: thin;");
            ressourceTextCell.id = planetId + "_ressources_" + ressourceId;
            var appName = navigator.appName;
            if (appName == 'Microsoft Internet Explorer') {
                ressourceImageCell.onmouseover = doProductionToolTip(planetId, ressourceId, globalUserId);
                ressourceImageCell.onmouseout =  function(){
                    hideTip();
                };

            } else {
                ressourceRow.setAttribute("onmouseover", "doTooltip(event,getProductionContent("+planetId+","+planetData.ressources[i].id+","+globalUserId+"))");
                ressourceRow.setAttribute("onmouseout", "hideTip()");

            }
            ressourceRow.appendChild(ressourceImageCell);
            ressourceRow.appendChild(ressourceTextCell);
            ressourceTbody.appendChild(ressourceRow);
            field.appendChild(ressourceTbody);

        }
    }
}

var actTooltip = "Loading ...";

function setActTooltip(prodData) {
    // document.getElementById("toolTip").innerHTML = dynHTML.content;
    var appName = navigator.appName;

    // Delete old content

    var ttDiv = document.getElementById("toolTip");
    if(ttDiv != null){
        while (ttDiv.firstChild) {
            ttDiv.removeChild(ttDiv.firstChild);
        }
    }else{
        return;
    }


    // Add new content
    var ressNameElem = document.createTextNode(prodData.ressName);
    var spacerElem = document.createElement('br');
    var outerTableElem = document.createElement('table');
    outerTableElem.setAttribute('border', '0');
    var headerTBodyElem = document.createElement('tbody');
    var headerRowElem = document.createElement('tr');
    var productionRowElem = document.createElement('tr');
    var tradeRowElem = document.createElement('tr');
    var totalRowElem = document.createElement('tr');

    // Header Row
    var column1 = document.createElement('td');
    var column2 = document.createElement('td');
    var column3 = document.createElement('td');
    var column4 = document.createElement('td');

    var fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    var txt = document.createTextNode('Produktion');
    fontElem.appendChild(txt);
    column2.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    txt = document.createTextNode('Verbrauch');
    fontElem.appendChild(txt);
    column3.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    txt = document.createTextNode('Gesamt');
    fontElem.appendChild(txt);
    column4.appendChild(fontElem);

    headerRowElem.appendChild(column1);
    headerRowElem.appendChild(column2);
    headerRowElem.appendChild(column3);
    headerRowElem.appendChild(column4);

    // Planet Row
    column1 = document.createElement('td');
    column2 = document.createElement('td');
    column3 = document.createElement('td');
    column4 = document.createElement('td');

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    var rowLblElem = document.createTextNode('Planet');
    fontElem.appendChild(rowLblElem);
    column1.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    var value = document.createTextNode(prodData.production.increase.value);
    fontElem.appendChild(value);
    if (appName == 'Microsoft Internet Explorer') {
        column2.style.backgroundColor = prodData.production.increase.color;
        column2.style.textAlign = "right";
    }else{
        column2.setAttribute('bgcolor', prodData.production.increase.color);
        column2.setAttribute('align', 'right');
    }
    column2.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    value = document.createTextNode(prodData.production.decrease.value);
    fontElem.appendChild(value);
    if (appName == 'Microsoft Internet Explorer') {
        column3.style.backgroundColor = prodData.production.decrease.color;
        column3.style.textAlign = "right";
    }else{
        column3.setAttribute('bgcolor', prodData.production.decrease.color);
        column3.setAttribute('align', 'right');
    }
    column3.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    value = document.createTextNode(prodData.production.total.value);
    fontElem.appendChild(value);
    column4.setAttribute('bgcolor', prodData.production.total.color);
    if (appName == 'Microsoft Internet Explorer') {
        column4.style.backgroundColor = prodData.production.total.color;
        column4.style.textAlign = "right";
    }else{
        column4.setAttribute('bgcolor', prodData.production.total.color);
        column4.setAttribute('align', 'right');
    }
    column4.appendChild(fontElem);

    productionRowElem.appendChild(column1);
    productionRowElem.appendChild(column2);
    productionRowElem.appendChild(column3);
    productionRowElem.appendChild(column4);

    // Trade Row
    column1 = document.createElement('td');
    column2 = document.createElement('td');
    column3 = document.createElement('td');
    column4 = document.createElement('td');

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    rowLblElem = document.createTextNode('Handel');
    fontElem.appendChild(rowLblElem);
    column1.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    value = document.createTextNode(prodData.trade.increase.value);
    fontElem.appendChild(value);
    if (appName == 'Microsoft Internet Explorer') {
        column2.style.backgroundColor = prodData.trade.increase.color;
        column2.style.textAlign = "right";
    }else{
        column2.setAttribute('bgcolor', prodData.trade.increase.color);
        column2.setAttribute('align', 'right');
    }
    column2.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    value = document.createTextNode(prodData.trade.decrease.value);
    fontElem.appendChild(value);
    if (appName == 'Microsoft Internet Explorer') {
        column3.style.backgroundColor = prodData.trade.decrease.color;
        column3.style.textAlign = "right";
    }else{
        column3.setAttribute('bgcolor', prodData.trade.decrease.color);
        column3.setAttribute('align', 'right');
    }
    column3.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    value = document.createTextNode(prodData.trade.total.value);
    fontElem.appendChild(value);
    if (appName == 'Microsoft Internet Explorer') {
        column4.style.backgroundColor = prodData.trade.total.color;
        column4.style.textAlign = "right";
    }else{
        column4.setAttribute('bgcolor', prodData.trade.total.color);
        column4.setAttribute('align', 'right');
    }
    column4.appendChild(fontElem);

    tradeRowElem.appendChild(column1);
    tradeRowElem.appendChild(column2);
    tradeRowElem.appendChild(column3);
    tradeRowElem.appendChild(column4);

    // Total Row
    column1 = document.createElement('td');
    column2 = document.createElement('td');
    column3 = document.createElement('td');
    column4 = document.createElement('td');

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    rowLblElem = document.createTextNode('Gesamt');
    fontElem.appendChild(rowLblElem);
    column1.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    value = document.createTextNode(prodData.total.increase.value);
    fontElem.appendChild(value);
    column2.setAttribute('align', 'right');
    if (appName == 'Microsoft Internet Explorer') {
        column2.style.backgroundColor =  prodData.total.increase.color;
        column2.style.textAlign = "right";
    }else{
        column2.setAttribute('bgcolor',  prodData.total.increase.color);
        column2.setAttribute('align', 'right');
    }
    column2.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    value = document.createTextNode(prodData.total.decrease.value);
    fontElem.appendChild(value);
    column3.setAttribute('align', 'right');
    if (appName == 'Microsoft Internet Explorer') {
        column3.style.backgroundColor =  prodData.total.decrease.color;
        column3.style.textAlign = "right";
    }else{
        column3.setAttribute('bgcolor',  prodData.total.decrease.color);
        column3.setAttribute('align', 'right');
    }
    column3.appendChild(fontElem);

    fontElem = document.createElement('font');
    fontElem.setAttribute('size', '1pt');
    fontElem.setAttribute('color', '#000000');
    value = document.createTextNode(prodData.total.total.value);
    fontElem.appendChild(value);
    column4.setAttribute('align', 'right');
    if (appName == 'Microsoft Internet Explorer') {
        column4.style.backgroundColor =  prodData.total.total.color;
        column4.style.textAlign = "right";
    }else{
        column4.setAttribute('bgcolor',  prodData.total.total.color);
        column4.setAttribute('align', 'right');
    }
    column4.appendChild(fontElem);

    totalRowElem.appendChild(column1);
    totalRowElem.appendChild(column2);
    totalRowElem.appendChild(column3);
    totalRowElem.appendChild(column4);

    // Finish
    headerTBodyElem.appendChild(headerRowElem)
    headerTBodyElem.appendChild(productionRowElem)
    headerTBodyElem.appendChild(tradeRowElem)
    headerTBodyElem.appendChild(totalRowElem)

    outerTableElem.appendChild(headerTBodyElem)

    ttDiv.appendChild(ressNameElem);
    ttDiv.appendChild(spacerElem);
    ttDiv.appendChild(outerTableElem);
}

function getMyProductionContent(planetId,ressId,userId) {

    return function () {
        getProductionContent(planetId,ressId,userId);
    };
}


function doProductionToolTip(planetId,ressId,userId) {
    return function () {
        doTooltip(this, getProductionContent(planetId, ressId, userId));
    };
}
function getProductionContent(planetId,ressId,userId) {
    actTooltip = "Loading ...";
    ajaxCall('GetResourceDetail?userId='+userId+'&planetId='+planetId+'&ressId='+ressId,setActTooltip);
    return actTooltip;
}

function doMyTooltip (event, tip) {
    return function () {
        doTooltip(event, tip);
    };
}
function doRefreshManagement(planetId, userId){
    globalUserId = userId;
    ajaxCall('GetJSONManagementData?userId='+ userId +'&planetId='+ planetId +'',processManagementData);

}
function updateConstructionTable(planetId, planetData){

    var field = document.getElementById(planetId + "_construction_select");
    if ( field.hasChildNodes() )
    {
        while ( field.childNodes.length >= 1 )
        {
            field.removeChild( field.firstChild );
        }
    }
    var button = document.getElementById(planetId + "_construction_button");
    button.value = planetData.constructionbuttonvalue;
    var lastConstruction = parseInt(planetData.lastconstruction);
    var defaultValue = false;
    if(lastConstruction == null || lastConstruction == 0){
        defaultValue = true;
    }
    for(var i = 0; i < planetData.constructions.length; i++){

        var option = document.createElement("option");
        if(planetData.constructions[i].id == lastConstruction && (lastConstruction > 0)){
            option.setAttribute("selected", "");
        }
        if(defaultValue){

            option.setAttribute("selected", "");
            defaultValue = false;
        }
        option.setAttribute("value", planetData.constructions[i].id)
        option.appendChild(document.createTextNode(planetData.constructions[i].entry));
        field.appendChild(option);
    }

    var constructionCount = document.getElementById(planetId + "_construction_amount");
    constructionCount.value = 1;
}

function updateRawMaterials(planetId, planetData){

    var field = document.getElementById(planetId + "_rawressources");
    if ( field.hasChildNodes() )
    {
        while ( field.childNodes.length >= 1 )
        {
            field.removeChild( field.firstChild );
        }
    }
    var found = false;



    for(var i = 0; i < planetData.rawmaterials.length; i++){
        var ressourceId = planetData.rawmaterials[i].id;
        var stock =  planetData.rawmaterials[i].value;
        if(stock != ""){


            var ressourceTbody = document.createElement("tbody");
            var ressourceRow = document.createElement("tr");
            var ressourceImageCell = document.createElement("td");

            ressourceImageCell.id = planetId + "_ressources" + "_" + ressourceId;
            ressourceImageCell.valign = "top";
            ressourceImageCell.align= "center";
            ressourceImageCell.style.width= "17px";
            ressourceImageCell.style.borderStyle = "solid";
            ressourceImageCell.style.borderWidth = "thin";
            ressourceImageCell.style.borderWidth = "0.1pt";
            ressourceImageCell.style.borderColor = "#999999";
            ressourceImageCell.setAttribute("style", "border-style: solid; border-width: thin;");


            var appName = navigator.appName;

            if (appName == 'Microsoft Internet Explorer') {

                var tip = planetData.rawmaterials[i].name + ": "+ stock;

                ressourceImageCell.onmouseover =  doMyTooltip(this, tip);
                ressourceImageCell.onmouseout =  function(){
                    hideTip();
                };


            } else {
                ressourceImageCell.setAttribute("onmouseover", "doTooltip(event,'" + planetData.rawmaterials[i].name + ": "+ stock + "')");
                ressourceImageCell.setAttribute("onmouseout", "hideTip()");
            }

            var img = document.createElement("img");
            img.style.width = "17px";
            img.style.height = "17px";
            img.id = planetId + "_rawressources_img_" + "_" + ressourceId;
            img.setAttribute("style", "width:17px; height:17px;");
            img.setAttribute("src", picPath + planetData.rawmaterials[i].image);

            /*
            if(planetId == 754){
                alert('getting : ' + planetId + '-' + i + " :" + ressources[planetId][i]);
            }
            var tip = String(ressources[planetId][i]);
            createSafeListener(img, "mouseover", function(){doTooltip(event, tip);});
            createSafeListener(img, "mouseout", function(){hideTip();});
           */
            ressourceImageCell.appendChild(img);

            ressourceRow.appendChild(ressourceImageCell);
            ressourceTbody.appendChild(ressourceRow)
            field.appendChild(ressourceTbody);

            found = true;
        }
    }
    if(!found){
        ressourceTbody = document.createElement("tbody");
        ressourceRow = document.createElement("tr");
        var ressourceCell = document.createElement("td");
        ressourceRow.appendChild(ressourceCell);
        ressourceTbody.appendChild(ressourceRow);
        field.appendChild(ressourceTbody);
    }


}

function updateNotesTable(planetId, planetData){
    var field = document.getElementById(planetId + "_note");

    if ( field.hasChildNodes() )
    {
        while ( field.childNodes.length >= 1 )
        {
            field.removeChild( field.firstChild );
        }
    }
    if(planetData.note != null){
        field.appendChild(document.createTextNode(planetData.note));
    }

}

function updateWarningsTable(planetId, planetData){

    if(planetData.warnings != null && planetData.warnings.length > 0){

        var warningsRow = document.getElementById(planetId + "_management_warningrow");
        var managementTable = document.getElementById(planetId + "_management");
        buildWarningsTable(planetId, planetData);
        try{
            managementTable.removeChild(warningsRow);
        }catch(Exception){

        }

        //remove xisting warnigs
        var field = document.getElementById(planetId + "_warning");
        if ( field.hasChildNodes() )
        {
            while ( field.childNodes.length >= 1 )
            {
                field.removeChild( field.firstChild );
            }
        }
        var thead = document.createElement("tbody");
        thead.setAttribute("cellspacing", "0");
        thead.setAttribute("cellpadding", "0");
        var row = document.createElement("tr");
        var spacer = document.createElement("td");
        spacer.width = "2px";
        spacer.setAttribute("style", "width:2px");
        spacer.appendChild(document.createTextNode(" "));
        row.appendChild(spacer);

        var warningCell = document.createElement("td");
        warningCell.align="left";
        if(planetData.warnings != null && planetData.warnings.length > 0){


            buildWarningsTable(planetId, planetData);

            warningCell.style.backgroundColor = planetData.warningcolor;
            warningCell.setAttribute("style", "background-color:" + planetData.warningcolor + ";");
            for(var i = 0; i < planetData.warnings.length; i++){
                var warningImage = document.createElement("img");
                warningCell.appendChild(document.createTextNode(" "));
                warningImage.setAttribute("src", picPath + planetData.warnings[i].image);
                warningImage.setAttribute("vspace", "1");
                warningImage.style.width="17px";
                warningImage.style.height="17px";
                warningImage.setAttribute("style", "width:17px; height:17px");
                warningImage.setAttribute("onmouseover", "doTooltip(event,'" + planetData.warnings[i].msg + "')");
                warningImage.setAttribute("onmouseout", "hideTip()");
                warningCell.appendChild(warningImage);
                row.appendChild(warningCell);
            }
        }else{

        }
        thead.appendChild(row);
        field.appendChild(thead);
    }
}

function updateRunningConstructions(planetId, planetData){
    var field = document.getElementById(planetId + "_runningConstructionsTable");
    if (field != null && field.hasChildNodes() )
    {
        while ( field.childNodes.length >= 1 )
        {
            field.removeChild( field.firstChild );
        }
    }

    var runningTbody = document.createElement("tbody");
    runningTbody.id = (planetId + "_runningConstructionsTable_runningTbody");
    if(planetData.runningconstructions != null && planetData.runningconstructions.length > 0){


        var runningRowLabel = document.createElement("tr");
        var runningCellLabel = document.createElement("td");
        runningCellLabel .setAttribute("style", "font-size:12px; font-weight:bold;");
        var bold = document.createElement("b");
        var underline = document.createElement("u");
        underline.appendChild(document.createTextNode(planetData.runningconstructionslabel));


        bold.appendChild(underline);
        runningCellLabel.align="right";
        runningCellLabel.valign="top";
        runningCellLabel.appendChild(bold);

        var spacer = document.createElement("td");
        spacer.appendChild(document.createTextNode('\u00A0'));

        var runningCompressImageTd = document.createElement("td");
        var compressImage = document.createElement("img");

        var popUp = "Queue komprimieren"
        compressImage.setAttribute("onmouseover", "doTooltip(event,'" + popUp + "')");
        compressImage.setAttribute("onmouseout", "hideTip()");
        compressImage.style.width = "14px";
        compressImage.style.height = "14px";
        ;
        compressImage.setAttribute("style", "width:15px; height:18px; bottom:0px");
        compressImage.setAttribute("src", picPath + "pic/icons/compress.png");
        compressImage.id = "compress_ " + planetId + "_img";
        createSafeListener(compressImage, "click", function(){
            compressQueue(planetData.userId, planetId)
            });

        runningCompressImageTd.appendChild(compressImage);

        runningRowLabel.appendChild(runningCellLabel);
        runningRowLabel.appendChild(spacer);
        runningRowLabel.appendChild(runningCompressImageTd);
        runningTbody.appendChild(runningRowLabel);

        var highestPriority = 0;

        for(var i = 0; i < planetData.runningconstructions.length; i++){
            addRunningConstruction(runningTbody, planetData.runningconstructions[i].entry);
            if (planetData.runningconstructions[i].priority > highestPriority) {
                highestPriority = parseInt(planetData.runningconstructions[i].priority);
            }
        }

        //Update the priority
        var selectPriority = document.getElementById(planetId + "_construction_priority");

        if ( selectPriority.hasChildNodes() )
        {
            while ( selectPriority.childNodes.length >= 1 )
            {
                selectPriority.removeChild( selectPriority.firstChild );
            }
        }

        // var lastPriority = parseInt(planetData.lastpriority);
        var newPriority = highestPriority + 1;

        if(newPriority > 10){
            newPriority = 10;
        }

        for(var i = 0; i <= 10; i++){
            var selectOption = document.createElement("option");
            if(i == newPriority){
                selectOption.setAttribute("selected", "");
            }
            selectOption.align="center";
            selectOption.value = i;
            selectOption.appendChild(document.createTextNode(i));
            selectPriority.appendChild(selectOption);
        }
    } else {
        //Update the priority
        var selectPriority = document.getElementById(planetId + "_construction_priority");

        if ( selectPriority.hasChildNodes() )
        {
            while ( selectPriority.childNodes.length >= 1 )
            {
                selectPriority.removeChild( selectPriority.firstChild );
            }
        }

        // var lastPriority = parseInt(planetData.lastpriority);
        var newPriority = 0;

        for(var i = 0; i <= 10; i++){
            var selectOption = document.createElement("option");
            if(i == newPriority){
                selectOption.setAttribute("selected", "");
            }
            selectOption.align="center";
            selectOption.value = i;
            selectOption.appendChild(document.createTextNode(i));
            selectPriority.appendChild(selectOption);
        }
    }

    field.appendChild(runningTbody);
}

function compressQueue(userId, planetId){

    var basePath = "";
    ajaxCall(basePath + "GetJSONManagementData?userId=" + userId + "&planetId=" +  planetId + "&compress=1",processManagementData);
    var image = document.getElementById("compress_ " + planetId + "_img");

    image.setAttribute("src", picPath + "pic/points.gif");
}

function addRunningConstruction(table, constructionEntry){
    var thead = document.createElement("tbody");
    var row = document.createElement("tr");
    var cell = document.createElement("td");
    cell.align = "right";
    cell.appendChild(document.createTextNode(decodeEntities(constructionEntry)));
    row.appendChild(cell);
    thead.appendChild(row);
    table.appendChild(thead);
}


function updateManagementValues(planetId, planetData){
    fields = new Array(5);

    for(var i = 0; i < fields.length; i++){
        fields[i] = new Array(5);
    }
    /*
     * 0 = id
     * 1 = value 1
     * 2 = value 2
     * 3 = color 1
     * 4 = color 2
     */
    fields[0][0] = "_populationValue";
    fields[1][0] = "_foodValue";
    fields[2][0] = "_energyValue";
    fields[3][0] = "_incomeValue";
    fields[4][0] = "_moraleValue";

    fields[0][1] = planetData.populationvalue + "(" + planetData.populationfreepopulation + "%)";

    fields[1][1] = planetData.foodvalue;
    fields[1][2] = planetData.foodtrade;
    fields[1][3] = planetData.foodcolor;
    fields[1][4] = planetData.foodtradecolor;

    fields[2][1] = planetData.energyvalue;
    fields[2][3] = planetData.energycolor;

    fields[3][1] = planetData.incomevalue

    fields[4][1] = planetData.moralevalue;
    fields[4][3] = planetData.moralecolor;
    //Update moralepic seperatly
    var moraleImage = planetData.moraleimage;
    var moraleImageCell = document.getElementById(planetId + "_moraleValue_img");
    moraleImageCell.setAttribute("src", moraleImage);

    for(var i = 0; i < fields.length; i++){
        var value1 = fields[i][1];
        var value2 = fields[i][2];
        var field = fields[i][0];
        var color1 = fields[i][3];
        if(i == 1){
            updateManagementValue(planetId, field, value1, value2, color1, fields[i][4]);
        }else{
            updateManagementValue(planetId, field, value1, null, color1, null);
        }
    }

    var value = planetData.taxvalue;
    var taxField = document.getElementById(planetId + "_taxValue");

    taxField.value = value;
}

function updateManagementValue(planetId, fieldId, value1, value2, color1, color2){
    var field = document.getElementById(planetId + fieldId);
    if ( field.hasChildNodes() )
    {
        while ( field.childNodes.length >= 1 )
        {
            field.removeChild( field.firstChild );
        }
    }
    if(color1 == null && color2 == null){
        var textValue = value1;
        field.appendChild(document.createTextNode(textValue));
    }else if(color1 != null && color2 == null){

        var colorField1 = document.createElement("FONT");
        colorField1.setAttribute("color", color1);
        colorField1.appendChild(document.createTextNode(value1));

        field.appendChild(colorField1);
    }
    else if(color1 != null && color2 != null){
        var colorField1 = document.createElement("FONT");
        colorField1.setAttribute("color", color1);
        colorField1.appendChild(document.createTextNode(value1));
        field.appendChild(colorField1);

        var colorField2 = document.createElement("FONT");
        colorField2.setAttribute("color", color2);
        colorField2.appendChild(document.createTextNode("(" + value2 + ")"));
        field.appendChild(colorField2);
    }
}
function buildTables(userId, planetId, planetData){
    globalUserId = userId;
    buildRessourcesTable(planetId, planetData);
    buildConstructionTable(userId, planetId, planetData);
    buildManagementTable(userId, planetId, planetData);
    buildNotesTable(planetId, planetData);
    buildRunningConstructionsTable(planetId, planetData);
}

function addManagementRow(table, fieldId, label, popUp, imgPath, value1, value2){

    var row = document.createElement("tr");
    row.style.backgroundColor = "#2E2E2E";
    row.setAttribute("style", "background-color:#2E2E2E;");

    var labelCell = document.createElement("td");
    labelCell.appendChild(document.createTextNode(label));
    row.appendChild(labelCell);

    var rowValue = document.createElement("td");
    var textValue = value1;
    if(value2 != null){
        textValue += " (" + value2 + ")";
    }
    rowValue.appendChild(document.createTextNode(textValue));

    rowValue.id = fieldId;
    rowValue.style.textAlign = "right";
    if(imgPath == null){
        rowValue.colSpan = "2";
        rowValue.setAttribute("colspan", "2");
    }
    row.appendChild(rowValue);

    if(imgPath != null){
        var imageCell = document.createElement("td");
        imageCell.valign = "center";
        var image = document.createElement("img");
        image.setAttribute("onmouseover", "doTooltip(event,'" + popUp + "')");
        image.setAttribute("onmouseout", "hideTip()");
        image.style.width = "14px";
        image.style.height = "14px";
        image.setAttribute("style", "width:14px; height:14px");
        image.setAttribute("src", picPath + imgPath);
        image.id = fieldId + "_img";

        imageCell.appendChild(image);
        row.appendChild(imageCell);
    }

    table.appendChild(row);
}
function buildManagementTable(userId, planetId, planetData){
    // alert("planetId : " + planetId+ "_management");
    var managementTable = document.getElementById(planetId + "_management");
    var managementTbody = document.createElement("tbody");
    managementTbody.id = planetId + "_management_thead";
    managementTable.width="350px";
    managementTable.setAttribute("cellpadding","0");
    managementTable.border = "0";
    managementTable.setAttribute("cellspacing","0");
    var managementRow = document.createElement("tr");
    var managementCell1 = document.createElement("td");
    managementCell1.valign = "top";

    var firstTable = document.createElement("table");
    var firstTbody = document.createElement("tbody");
    firstTable.width = "175px";
    firstTable.setAttribute("style", "width:175px;");
    firstTable.style.fontSize = "10px";
    firstTable.style.fontWeight = "bold";
    firstTable.style.borderStyle = "solid";
    firstTable.style.borderWidth = "thin";
    firstTable.style.borderWidth = "0.1pt";
    firstTable.style.borderColor = "#999999";
    firstTable.setAttribute("style", "font-size:10px; font-weight:bold; width:100%;  border-style: solid; border-width: thin;");
    firstTable.border = "0";

    //######POPULATION

    var stock = "???";
    addManagementRow(firstTbody, planetId + "_populationValue", ML('administration_lbl_population',language), ML('administration_lbl_population',language), "pic/icons/population.jpg", stock);

    //######FOOD
    stock = 0;
    var trade = "???";
    addManagementRow(firstTbody, planetId + "_foodValue", ML('administration_lbl_food',language), ML('administration_lbl_food',language), "pic/icons/food.jpg", stock, trade);

    //######ENERGY

    stock = "???";
    addManagementRow(firstTbody, planetId + "_energyValue", ML('administration_lbl_energy',language), ML('administration_lbl_energy',language), "pic/icons/energy.jpg", stock);


    firstTable.appendChild(firstTbody);
    managementCell1.appendChild(firstTable);
    managementRow.appendChild(managementCell1);

    //SECOND COLUMN

    var managementCell2 = document.createElement("td");
    var secondTable = document.createElement("table");
    secondTable.width = "175px";
    secondTable.setAttribute("style", "width:175px;");
    secondTable.style.fontSize = "10px";
    secondTable.style.fontWeight = "bold";
    secondTable.style.borderStyle = "solid";
    secondTable.style.borderWidth = "thin";
    secondTable.style.borderWidth = "0.1pt";
    secondTable.style.borderColor = "#999999";
    var secondTbody = document.createElement("tbody");
    secondTable.setAttribute("style", "font-size:10px; font-weight:bold; border-style: solid; border-width: thin;");

    //######INCOME


    stock = "???";
    addManagementRow(secondTbody, planetId + "_incomeValue", ML('administration_lbl_income',language), ML('administration_lbl_income',language), null, stock);



    //######TAXES

    var taxRow = document.createElement("tr");
    taxRow.style.backgroundColor = "#2E2E2E";
    taxRow.setAttribute("style", "background-color:#2E2E2E;");

    var taxLabel = ML('administration_lbl_taxes',language);
    var taxLabelCell = document.createElement("td");
    taxLabelCell.appendChild(document.createTextNode(taxLabel));
    taxRow.appendChild(taxLabelCell);

    stock = "???";
    var taxValueCell = document.createElement("td");
    taxValueCell.align = "right";

    var taxValue = document.createElement("input");

    taxValue.setAttribute("name", "tax");
    taxValue.style.height = "14px";
    taxValue.style.fontSize = "10px";
    taxValue.style.width = "25px";
    taxValue.setAttribute("style", "height: 14px; font-size:10px; width:25px;");
    taxValue.setAttribute("type", "text");
    taxValue.setAttribute("value", stock);
    taxValue.id = planetId + "_taxValue";

    taxValueCell.appendChild(taxValue);

    taxRow.appendChild(taxValueCell);
    //Tax Refresh Image
    var taxImageCell = document.createElement("td");
    taxImageCell.align = "right";
    taxImageCell.width = "15px";
    var taxImage = document.createElement("input");

    var basePath = "";

    taxImage.style.height = "14px";
    taxImage.style.fontSize = "15px";
    taxImage.style.width = "14px";
    taxImage.setAttribute("style", "height: 14px; font-size:15px; width:14px;");
    taxImage.setAttribute("type", "image");
    taxImage.setAttribute("value", stock);
    taxImage.setAttribute("src", picPath + "pic/refresh.png");

    createSafeListener(taxImage, "click", function(){
        ajaxCall(basePath + "GetJSONManagementData?userId=" + userId + "&planetId=" +  planetId + "&taxes=" + document.getElementById(planetId + "_taxValue").value,processManagementData);
    });
    //createSafeListener(taxImage, "click", function(){ alert("Alarm: " + document.getElementById(planetId + "_taxValue").value);});

    //  taxImage.setAttribute("onclick", "ajaxCall('" + basePath + "GetJSONManagementData?userId=" + userId + "&planetId=" +  planetId + "&taxes=' + document.getElementById('" + planetId + "_taxValue').value + '',processManagementData);");

    taxImageCell.appendChild(taxImage);

    taxRow.appendChild(taxImageCell);

    secondTbody.appendChild(taxRow);


    //#####MORALE

    stock = 0;
    addManagementRow(secondTbody, planetId + "_moraleValue",  ML('administration_lbl_morale',language), ML('administration_lbl_morale',language), "pic/menu/icons/mood_good.png", stock);



    secondTable.appendChild(secondTbody);
    managementCell2.appendChild(secondTable);
    managementRow.appendChild(managementCell2);
    managementTbody.appendChild(managementRow)
    managementTable.appendChild(managementTbody);


}
function createSafeListener(toField, listenerType, functionToDo) {
    var appName = navigator.appName;

    if (appName == 'Microsoft Internet Explorer') {
        toField.attachEvent('on' + listenerType, functionToDo);
    } else {
        toField.addEventListener(listenerType, functionToDo, false);
    }
}
function buildWarningsTable(planetId, planetData){

    var managementTbody = document.getElementById(planetId + "_management_thead");
    var managementRow = document.createElement("tr");
    managementRow.id = planetId + "_management_warningrow";
    managementRow.valign="top";

    var managementCell = document.createElement("td");
    managementCell.valign = "top";
    managementCell.colSpan = "2";
    managementCell.setAttribute("colspan", "2");

    managementCell.style.height="14px";
    managementCell.style.width="100%";
    managementCell.setAttribute("style", "height:'14px'; width='100%'");

    var warningTable = document.createElement("table");
    warningTable.setAttribute("cellpadding", "1");
    warningTable.setAttribute("cellspacing", "1");
    warningTable.height="14px";
    warningTable.style.fontSize="10px";
    warningTable.style.fontWeight="bold";
    warningTable.style.borderStyle="solid";
    warningTable.style.borderWidth="thin";
    warningTable.style.borderWidth = "0.1pt";
    warningTable.style.borderColor = "#999999";
    warningTable.setAttribute("style", "font-size:10px; height:14px; font-weight:bold;  border-style: solid; border-width: thin;");
    warningTable.width="100%";
    warningTable.id = planetId + "_warning";

    managementCell.appendChild(warningTable);
    managementRow.appendChild(managementCell);
    managementTbody.appendChild(managementRow);


}
function buildRunningConstructionsTable(planetId, planetData){

    var constructionTable = document.getElementById(planetId + "_runningconstructions");
    var constructionTbody = document.createElement("tbody");
    var constructionRow = document.createElement("tr");
    var constructionCell = document.createElement("td");
    constructionCell.align="right";
    constructionCell.valign="top";

    var runningTable = document.createElement("table");
    runningTable.style.fontSize = "10px";
    runningTable.style.fontWeight = "bold";
    runningTable.setAttribute("style", "font-size:10px; font-weight: bold;");
    runningTable.width = "100%";
    runningTable.setAttribute("cellpadding", "0");
    runningTable.setAttribute("cellspacing", "0");
    runningTable.id = planetId + "_runningConstructionsTable";
    constructionCell.appendChild(runningTable);

    constructionRow.appendChild(constructionCell);
    constructionTbody.appendChild(constructionRow);
    constructionTable.appendChild(constructionTbody);
//Todo constructions


}
function buildInvoked(userId, planetId){
    var basePath = "";

    var field = document.getElementById(planetId + "_runningConstructionsTable_runningTbody");

    var row = document.createElement("tr");
    var cell = document.createElement("td");
    cell.align = "right";
    cell.style.textAlign = "right";
    var img = document.createElement("img");
    img.setAttribute("src", picPath + "pic/points.gif");

    cell.appendChild(img);
    row.appendChild(cell);
    field.appendChild(row);
    ajaxCall(basePath + "GetJSONManagementData?userId=" + userId + "&planetId=" +  planetId + "&constructionId=" + document.getElementById(planetId + "_construction_select").value + "&priority=" + document.getElementById(planetId + "_construction_priority").value + "&amount=" + document.getElementById(planetId + "_construction_amount").value,processManagementData);

}

function buildConstructionTable(userId, planetId, planetData){

    var basePath = "";


    var constructionTable = document.getElementById(planetId + "_construction");
    var constructionSelectTbody = document.createElement("tbody");
    var constructionSelectRow = document.createElement("tr");
    var constructionSelectCell = document.createElement("td");
    constructionSelectCell.align = "left";
    var select = document.createElement("select");
    select.style.backgroundColor = "#000000";
    select.style.color = "#ffffff";
    select.style.border = "0px";
    //  select.width = "200px";
    select.style.fontSize = "11px";
    select.style.borderWidth = "0.1pt";
    select.style.borderColor = "#999999";
    select.setAttribute("style", "background-color: #000000; color: #ffffff; border:0px; width: 100%; font-size: 11px;");
    select.id = planetId + "_construction_select";

    constructionSelectCell.appendChild(select);
    constructionSelectRow.appendChild(constructionSelectCell);

    var constructionSelectPriorityCell = document.createElement("td");
    constructionSelectPriorityCell.align = "left";
    var selectPriority = document.createElement("select");
    selectPriority.id = planetId + "_construction_priority";
    selectPriority.style.backgroundColor = "#000000";
    selectPriority.style.color = "#ffffff";
    selectPriority.style.border = "0px";
    selectPriority.style.borderWidth = "0.1pt";
    selectPriority.style.borderColor = "#999999";
    selectPriority.width = "35px";
    selectPriority.style.fontSize = "11px";
    //
    selectPriority.setAttribute("style", "background-color: #000000; color: #ffffff; border:0px; width: 35px; font-size: 11px;");
    for(var i = 0; i <= 10; i++){
        var selectOption = document.createElement("option");
        if(i == 5){
            selectOption.setAttribute("selected", "");
        }
        selectOption.align="center";
        selectOption.value = i;
        selectOption.appendChild(document.createTextNode(i));
        selectPriority.appendChild(selectOption);
    }
    constructionSelectPriorityCell.appendChild(selectPriority);
    constructionSelectRow.appendChild(constructionSelectPriorityCell);
    constructionSelectTbody.appendChild(constructionSelectRow);
    constructionTable.appendChild(constructionSelectTbody);



    var constructionSubmitTbody = document.createElement("tbody");
    var constructionSubmitRow = document.createElement("tr");
    var constructionSubmitCell = document.createElement("td");

    var submitTable = document.createElement("table");
    selectPriority.style.backgroundColor = "#000000";
    submitTable.style.color = "#4A4A4A";
    submitTable.style.width = "100%";
    submitTable.setAttribute("style", "width:100%; background-color:#4A4A4A;");
    var submitTbody = document.createElement("tbody");
    var submitRow = document.createElement("tr");
    var submitCellValue = document.createElement("td");
    submitCellValue.align = "center";

    submitCellValue.setAttribute("style", "style='background-color:#4A4A4A;'");
    var value = document.createElement("input");
    value.style.backgroundColor = "#000000";
    value.style.color = "#ffffff";
    value.width = "100%";
    value.style.fontSize = "11px";
    value.style.textAlign = "center";
    value.setAttribute("style", "background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;");
    //value="1" NAME="number" type="text" size="5"
    value.setAttribute("value", "1");
    value.setAttribute("type", "text");
    value.setAttribute("size", "5");
    value.id = planetId + "_construction_amount";
    submitCellValue.appendChild(value);
    submitRow.appendChild(submitCellValue);

    var constructionSubmitButton = document.createElement("td");
    constructionSubmitButton.style.color = "#4A4A4A";
    constructionSubmitButton.style.textAlign = "center";
    constructionSubmitButton.setAttribute("style", "style='background-color:#4A4A4A;'");
    constructionSubmitButton.align = "center";
    var button = document.createElement("input");

    button.style.backgroundColor = "#000000";
    button.style.color = "#ffffff";
    button.style.fontSize = "11px";
    button.style.textAlign = "right";
    button.style.borderStyle = "solid";
    button.style.borderWidth = "0.1pt";
    button.style.borderColor = "#999999";
    button.setAttribute("style", "background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px;");
    //value="1" NAME="number" type="text" size="5"
    button.setAttribute("value", "??????");
    button.id = planetId + "_construction_button";


    createSafeListener(button, "click", function(){
        buildInvoked(userId, planetId);
    });

    // button.setAttribute("onclick", "ajaxCall('" + basePath + "GetJSONManagementData?userId=" + userId + "&planetId=" +  planetId + "&constructionId=' + document.getElementById('" + planetId + "_construction_select').value + '&priority=' + document.getElementById('" + planetId + "_construction_priority').value + '&amount=' + document.getElementById('" + planetId + "_construction_amount').value + '',processManagementData);");
    //"
    button.setAttribute("type", "button");
    button.setAttribute("size", "25");
    constructionSubmitButton.appendChild(button);
    submitRow.appendChild(constructionSubmitButton);
    submitTbody.appendChild(submitRow);

    submitTable.appendChild(submitTbody);

    constructionSubmitCell.appendChild(submitTable);
    constructionSubmitRow.appendChild(constructionSubmitCell);
    constructionSubmitTbody.appendChild(constructionSubmitRow);

    constructionTable.appendChild(constructionSubmitTbody);

}


function buildNotesTable(planetId, planetData){

    var managementTable = document.getElementById(planetId + "_management");
    var managementTbody = document.getElementById(planetId + "_management_thead");
    var managementRow = document.createElement("tr");
    managementRow.valign="top";

    var managementCell = document.createElement("td");
    managementCell.valign = "top";
    managementCell.colSpan = "2";
    managementCell.setAttribute("colspan", "2");

    var noteTable = document.createElement("table");
    noteTable.width = "100%";
    noteTable.setAttribute("cellpadding", "2");
    noteTable.setAttribute("cellspacing", "0");
    noteTable.setAttribute("border", "0");


    var noteTbody = document.createElement("tbody");
    var noteRow = document.createElement("tr");
    var noteLink = document.createElement("a");
    //onClick="return popupNote(this, 'editor')" target="blank" href="noteEditor.jsp?planetId=<%= p.getId()
    noteLink.setAttribute("onClick", "return popupNote(this, 'editor')");
    noteLink.setAttribute("target", "blank");
    noteLink.setAttribute("href", "noteEditor.jsp?planetId=" + planetId);
    var noteImage = document.createElement("img");
    var noteImageCell = document.createElement("td");
    noteImageCell.setAttribute("onmouseover", "doTooltip(event,'Notiz �ndern')");
    noteImageCell.setAttribute("onmouseout", "hideTip()");
    noteImageCell.align="left";
    noteImageCell.width="15px";
    noteImageCell.style.fontSize="10px";
    noteImageCell.style.fontWeight="bold";
    noteImageCell.style.borderStyle="solid";
    noteImageCell.style.borderWidth = "0.1pt";
    noteImageCell.style.borderColor = "#999999";
    noteImageCell.style.fontSize="10px";
    noteImageCell.style.borderWith="thin";
    noteImageCell.setAttribute("style", "font-size:10px; font-weight:bold;  border-style: solid; border-width: thin;");

    noteImageCell.height="18px";
    noteImageCell.style.fontSize="15px";
    noteImageCell.width="18px";
    noteImage.setAttribute("style", "height: 18px; font-size:15px; width:18px;");
    noteImage.setAttribute("type", "image");
    noteImage.setAttribute("border", "0");
    noteImage.setAttribute("src", picPath + "pic/edit.jpg");
    noteLink.appendChild(noteImage);
    noteImageCell.appendChild(noteLink);
    noteRow.appendChild(noteImageCell);

    var noteText = document.createElement("td");
    noteText.appendChild(document.createTextNode(""));
    noteText.style.fontSize = "10px";
    noteText.style.fontWeight = "bold";
    noteText.style.borderStyle = "solid";
    noteText.style.borderWidth = "thin";
    noteText.style.borderWidth = "0.1pt";
    noteText.style.borderColor = "#999999";
    noteText.setAttribute("style", "font-size:10px; font-weight:bold;  border-style: solid; border-width: thin;");
    noteText.id = planetId + "_note";
    noteRow.appendChild(noteText);
    noteTbody.appendChild(noteRow);
    noteTable.appendChild(noteTbody);

    managementCell.appendChild(noteTable);
    managementRow.appendChild(managementCell);
    managementTbody.appendChild(managementRow);


    managementTable.appendChild(managementTbody);


}
function buildRessourcesTable(planetId, planetData){
    // alert("planetId : " + planetId+ "_ressources");
    var ressourceTable = document.getElementById(planetId + "_ressources");
    ressourceTable.style.fontSize = "11px";
    ressourceTable.style.fontWeight = "bold";
    ressourceTable.style.borderStyle = "solid";
    ressourceTable.style.borderWidth = "0.1pt";
    ressourceTable.style.borderColor = "#999999";
    ressourceTable.style.width = "100px";

    ressourceTable.setAttribute("style", "font-size:11px; font-weight:bold; border-style: solid; border-width: thin; width:100px");




    //Raw Materials
    var ressourcerawTable = document.getElementById(planetId + "_rawressources");
    ressourcerawTable.border = 0;
    ressourcerawTable.style.fontSize = "11px";
    ressourcerawTable.style.fontWeight = "bold";
    ressourcerawTable.style.borderStyle = "solid";
    ressourcerawTable.style.borderWidth = "thin";
    ressourcerawTable.style.borderWidth = "0.1pt";
    ressourcerawTable.style.borderColor = "#999999";
    ressourcerawTable.setAttribute("style", "font-size:10px; font-weight:bold;  border-style: solid; border-width: thin;");


}

