    var designDataGlobal = null;
    
    var reactorCount = 1;
    var noOfReactor = 1;
    var shieldCount = 1;
    var noOfShield = 1;
    var weaponCount = 1;
    var noOfWeapon = 1;
    var otherCount = 1;
    var noOfOther = 1;

    var creditCost = 0;
    var ironCost = 0;
    var steelCost = 0;
    var terkonitCost = 0;
    var ynkeloniumCost = 0;
    var howalgoniumCost = 0;
    var cvEmbiniumCost = 0;

    var spaceAvail = 0;
    var spaceConsumption = 0;
    var energyProduction = 0;
    var energyConsumption = 0;
    var attackStrength = 0;
    var defenseStrength = 0;
    var acceleration = 0;   
    var chassisRange = 0;
    var engineRange = 0;

    
    var armorList = null;
    var weaponList = null;
    var engineList = null;
    var reactorList = null;
    var shieldList = null;
    var otherList = null; 
    
    var actRessFactor = 1;
    var actStructureFactor = 1;
    var actShieldFactor = 1;
    var actAttackFactor = 1;
    var moduleAttackFactor = 1;
    var actTroopFactor = 1;
    var actCostFactor = 1;
    var actHangarFactor = 1;
    
    var chassisMapping = new Array();
    var shipTypes = new Array();
    var shipTypeMapping = new Array();

    function createSafeListener(toField, listenerType, functionToDo) {
        var appName = navigator.appName;
        
        if (appName == 'Microsoft Internet Explorer') {
            toField.attachEvent('on' + listenerType, functionToDo);
        } else {
            toField.addEventListener(listenerType, functionToDo, false);
        }
    }
    
    function processAjaxData(designData) {
        designDataGlobal = designData;
        
        // create chassis mapping
        for (var i=0;i<designData.chassis.length;i++) {
            chassisMapping[i] = designData.chassis[i].id;
        }
        
        // Convert initial id to javascript id
        for (i=0;i<designData.chassis.length;i++) {
            if (chassisMapping[i] == actChassis) {
                actChassis = i;
                orgChassis = i;
                break;
            }
        }
        
        armorList = designData.chassis[actChassis].modules.armors;
        weaponList = designData.chassis[actChassis].modules.weapons;
        engineList = designData.chassis[actChassis].modules.engine;
        reactorList = designData.chassis[actChassis].modules.reactors;
        shieldList = designData.chassis[actChassis].modules.shields;
        computerList = designData.chassis[actChassis].modules.computers;
        otherList = designData.chassis[actChassis].modules.other;
        
        
        //Shiptypes
       for(i=0;i<designData.shiptypes.length;i++){
           chassis = new Object();
           chassis.id = designData.shiptypes[i].id;
           chassis.typeId = designData.shiptypes[i].typeId;
           chassis.name = designData.shiptypes[i].name;
           chassis.range = designData.shiptypes[i].range;
           chassis.ressFactor = designData.shiptypes[i].ressFactor;
           chassis.structureFactor = designData.shiptypes[i].structureFactor;
           chassis.shieldFactor = designData.shiptypes[i].shieldFactor;
           chassis.attackFactor = designData.shiptypes[i].attackFactor;
           chassis.troopFactor = designData.shiptypes[i].troopFactor;
           chassis.costFactor = designData.shiptypes[i].costFactor;
           chassis.hangarFactor = designData.shiptypes[i].hangarFactor;
           chassis.range = designData.shiptypes[i].range;
           shipTypes[designData.shiptypes[i].typeId] = chassis;
       }

        var noField = document.getElementById('other_1_count');
        createSafeListener(noField,'change',function() {setCount(this);});
        createSafeListener(noField,'keyup',function() {setCount(this);});
        noField = document.getElementById('weapons_1_count');
        createSafeListener(noField,'change',function() {setCount(this);});
        createSafeListener(noField,'keyup',function() {setCount(this);});
        noField = document.getElementById('reactors_1_count');
        createSafeListener(noField,'change',function() {setCount(this);});
        createSafeListener(noField,'keyup',function() {setCount(this);});
        noField = document.getElementById('shields_1_count');
        createSafeListener(noField,'change',function() {setCount(this);});
        createSafeListener(noField,'keyup',function() {setCount(this);});

        var actField = document.getElementById('engines');       
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        actField = document.getElementById('hyperspaceEngines');       
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        actField = document.getElementById('armor');       
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        actField = document.getElementById('computer');
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});

        actField = document.getElementById('weapons_1');         
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        for (i=0;i<weaponList.length;i++) {
            var newOption = document.createElement('option');
            newOption.text = weaponList[i].name;
            newOption.value = weaponList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('armor'); 
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        for (i=0;i<armorList.length;i++) {
            newOption = document.createElement('option');
            newOption.text = armorList[i].name;
            newOption.value = armorList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('computer');
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        for (i=0;i<computerList.length;i++) {
            newOption = document.createElement('option');
            newOption.text = computerList[i].name;
            newOption.value = computerList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('other_1'); 
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        for (i=0;i<otherList.length;i++) {
            newOption = document.createElement('option');
            newOption.text = otherList[i].name;
            newOption.value = otherList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('engines'); 
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        for (i=0;i<engineList.length;i++) {
            // Check if engine can be used in normal space
            var attributes = engineList[i].Attributes;
            var allowed = false;

            for (j=0;j<attributes.length;j++) {
                var attrib = attributes[j];
                if (attrib.name == 'SPEED_ACCELERATION') {
                    allowed = true;
                    break;
                }
                 
           }
            
            if (!allowed) continue;
            
            var newOption = document.createElement('option');
            newOption.text = engineList[i].name;
            newOption.value = engineList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('hyperspaceEngines'); 
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        for (i=0;i<engineList.length;i++) {
            // Check if engine can be used in hyperspace
            var attributes = engineList[i].Attributes;
            var allowed = false;
            
            for (j=0;j<attributes.length;j++) {
                var attrib = attributes[j];
                if (attrib.name == 'SPEED_HYPER') {
                    if (attrib.value >= 1) {
                        allowed = true;
                        break;
                    }
                }
            }
            
            if (!allowed) continue;            
            
            var newOption = document.createElement('option');
            newOption.text = engineList[i].name;
            newOption.value = engineList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('reactors_1'); 
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        for (i=0;i<reactorList.length;i++) {
            var newOption = document.createElement('option');
            newOption.text = reactorList[i].name;
            newOption.value = reactorList[i].id;            

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }
        
        actField = document.getElementById('shields_1'); 
        createSafeListener(actField,'mousemove',function() {showModuleData(this);});
        createSafeListener(actField,'change',function() {changeModuleData(this);});
        for (i=0;i<shieldList.length;i++) {
            var newOption = document.createElement('option');
            newOption.text = shieldList[i].name;
            newOption.value = shieldList[i].id;            

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }        
    
        calculateRessources();        

        var shipdetails = document.getElementById('shipdetails');
        shipdetails.removeChild(shipdetails.lastChild);                 
        
        try {
            callingMain();
        } catch (error) {
            alert('Der Internet Explorer macht mich fertig! Bitte verwenden sie einen normalen Browser! Aber ich arbeite dran ...');
        }
                                         
       var space = document.getElementById("spaceTotal");
       spaceAvail = parseInt(designData.chassis[actChassis].basicValues.space);
       space.innerHTML = number_format(designData.chassis[actChassis].basicValues.space,0);
       var structure = document.getElementById("structure");       
       structure.innerHTML = "<FONT color='" + getColor(actStructureFactor, parseInt(designData.chassis[actChassis].basicValues.structure)) + "'>" + number_format(parseInt(designData.chassis[actChassis].basicValues.structure) * actStructureFactor,0) + "</FONT>";      
       
       resetDesign();
       displayShipTypeInfo();
       displayRessourceCost();
    }
    
    function updateShipType(){        
        actRessFactor = shipTypes[shiptype].name;
        actRessFactor = shipTypes[shiptype].ressFactor;
        actStructureFactor = shipTypes[shiptype].structureFactor;
        actShieldFactor = shipTypes[shiptype].shieldFactor;
        actAttackFactor = shipTypes[shiptype].attackFactor;
        actTroopFactor = shipTypes[shiptype].troopFactor;
        actCostFactor = shipTypes[shiptype].costFactor;
        actHangarFactor = shipTypes[shiptype].hangarFactor;    
    }
    
    function displayShipTypeInfo(){
        updateShipType();
        displayRessourceCost()
        
        var specialStr = '';
    //    shipTypes = designData.shiptypes;
        var specField = document.getElementById("special");
        var factor = parseFloat(actCostFactor);
        if(factor > 1.0 || factor < 1.0){
            if(factor > 1.0 ){
                specialStr += "+"
            }
        specialStr += number_format((parseFloat(actCostFactor) * 100.0) - 100.0) + '% Kosten <BR>';
        }
        factor = parseFloat(actRessFactor);
        if(factor > 1.0 || factor < 1.0){
            if(factor > 1.0 ){
                specialStr += "+"
            }
        specialStr += number_format((parseFloat(actRessFactor) * 100.0) - 100.0) + '% Ressourcenkapazität <BR>';
        }
        factor = parseFloat(actAttackFactor);
        if(factor > 1.0 || factor < 1.0){
            if(factor > 1.0 ){
                specialStr += "+"
            }
        specialStr += number_format((parseFloat(actAttackFactor) * 100.0) - 100.0) + '% Angriffsstärke <BR>';
        }
        factor = parseFloat(actShieldFactor);
        if(factor > 1.0 || factor < 1.0){
            if(factor > 1.0 ){
                specialStr += "+"
            }
        specialStr += number_format((parseFloat(actShieldFactor) * 100.0) - 100.0) + '% Schildstärke <BR>';
        }
        factor = parseFloat(actTroopFactor);
        if(factor > 1.0 || factor < 1.0){
            if(factor > 1.0 ){
                specialStr += "+"
            }
        specialStr += number_format((parseFloat(actTroopFactor) * 100.0) - 100.0) + '% Truppenkapazität <BR>';
        }
        factor = parseFloat(actHangarFactor);
        if(factor > 1.0 || factor < 1.0){
            if(factor > 1.0 ){
                specialStr += "+"
            }
        specialStr += number_format((parseFloat(actHangarFactor) * 100.0) - 100.0) + '% Hangarkapazität <BR>';
        }
        factor = parseFloat(actStructureFactor);
        if(factor > 1.0 || factor < 1.0){
            if(factor > 1.0 ){
                specialStr += "+"
            }
        specialStr += number_format((parseFloat(actStructureFactor) * 100.0) - 100.0)  + '% Strukturfaktor <BR>';
        }
        specField.innerHTML = specialStr;
    // 
    }
    
    function displayRessourceCost() {  
        var ressourceStr =  '';
        

        if (creditCost > 0) {            
            ressourceStr += getImage(20) + number_format(creditCost * actCostFactor,0) + ' Credits<BR>';
        }
        if (ironCost > 0) {       
            ressourceStr += getImage(1) + number_format(ironCost,3,true) + ' Eisen</SPAN><BR>';
        }
        if (steelCost > 0) {       
            ressourceStr += getImage(2) + number_format(steelCost,3,true) + ' Stahl</SPAN><BR>';
        }
        if (terkonitCost > 0) {       
            ressourceStr += getImage(3) + number_format(terkonitCost,3,true) + ' Terkonit</SPAN><BR>';
        }
        if (ynkeloniumCost > 0) {       
            ressourceStr += getImage(4) + number_format(ynkeloniumCost,3,true) + ' Ynkelonium</SPAN><BR>';
        }
        if (howalgoniumCost > 0) {       
            ressourceStr += getImage(5) + number_format(howalgoniumCost,3,true) + ' Howalgonium</SPAN><BR>';
        }
        if (cvEmbiniumCost > 0) {       
            ressourceStr += getImage(6) + number_format(cvEmbiniumCost,3,true) + ' CV-Embinium</SPAN><BR>';
        }
         
        var ressourceSpan = document.getElementById("resources");
        ressourceSpan.innerHTML = ressourceStr;
        
        var elemSpaceUsed = document.getElementById("spaceUsed");
        elemSpaceUsed.innerHTML = number_format(spaceConsumption,0);
        var elemSpaceTotal = document.getElementById("spaceTotal");
        elemSpaceTotal.innerHTML = number_format(designDataGlobal.chassis[actChassis].basicValues.space,0);                
        var elemEnergyUsed = document.getElementById("energyUsed");
        elemEnergyUsed.innerHTML = number_format(energyConsumption,0);
        var elemEnergyTotal = document.getElementById("energyTotal");
        elemEnergyTotal.innerHTML = number_format(energyProduction,0);
        
        
        var elemAttack = document.getElementById("attack");      
        elemAttack.innerHTML = "<FONT color='" + getColor(actAttackFactor, attackStrength) + "'>" + number_format(attackStrength * actAttackFactor * moduleAttackFactor,0) + "</FONT>";          
        var elemDefense = document.getElementById("defense");
        elemDefense.innerHTML = "<FONT color='" + getColor(actShieldFactor, defenseStrength) + "'>" + number_format(defenseStrength * actShieldFactor,0) + "</FONT>";       
        var elemAcceleration = document.getElementById("acceleration");
        elemAcceleration.innerHTML = number_format(acceleration,3);  
        var structure = document.getElementById("structure");
        structure.innerHTML = "<FONT color='" + getColor(actStructureFactor, parseInt(designDataGlobal.chassis[actChassis].basicValues.structure)) + "'>" + number_format(parseInt(designDataGlobal.chassis[actChassis].basicValues.structure) * actStructureFactor,0) + "</FONT>";      
        var elemRange = document.getElementById("designRange");
        chassisRange =  number_format(designDataGlobal.chassis[actChassis].basicValues.range,0);  
        var engineRangeString = '';
        if(engineRange > 0){
            engineRangeString = ' (<FONT color='+ getColor(1.1,engineRange) +"'>+" + engineRange + "</FONT>)";
        }
        elemRange.innerHTML = chassisRange + engineRangeString;

     
    }
    function getColor(factor, value){
        var color = '#FFFFFF';
            if(factor > 1.0){
                color = '#00FF00';
            }else if(factor < 1.0){
                color = '#FF0000';
            }
        return color;
    }
    
    function calculateRessources() {
        creditCost = 0;
        ironCost = 0;
        steelCost = 0;
        terkonitCost = 0;
        ynkeloniumCost = 0;
        howalgoniumCost = 0;
        cvEmbiniumCost = 0;        
        
        var resEntries = designDataGlobal.chassis[actChassis].Ressources;
        for (i=0;i<resEntries.length;i++) {
            var ressEntry = resEntries[i];
            
            if (ressEntry.id == 1) {
                ironCost += parseFloat(ressEntry.qty);
            }
            if (ressEntry.id == 2) {
                steelCost += parseFloat(ressEntry.qty);
            }
            if (ressEntry.id == 3) {
                terkonitCost += parseFloat(ressEntry.qty);
            }
            if (ressEntry.id == 4) {
                ynkeloniumCost += parseFloat(ressEntry.qty);
            }
            if (ressEntry.id == 5) {
                howalgoniumCost += parseFloat(ressEntry.qty);
            }
            if (ressEntry.id == 6) {
                cvEmbiniumCost += parseFloat(ressEntry.qty);
            }            
            if (ressEntry.id == 20) {
                creditCost += parseFloat(ressEntry.qty);
            }                        
        }
    }
    
    // Shipdesign Javascript Kot
    function addSlot(type,me) {                
        var genSlot;
        
        if (type == 1) { // energy
            genSlot = addRowFor('en',designDataGlobal.chassis[actChassis].modules.reactors);
        } else if (type == 2) { // offensive
            genSlot = addRowFor('of',designDataGlobal.chassis[actChassis].modules.weapons);
        } else if (type == 3) { // defense 
            genSlot = addRowFor('de',designDataGlobal.chassis[actChassis].modules.shields);
        } else if (type == 4) { // other
            genSlot = addRowFor('ot',designDataGlobal.chassis[actChassis].modules.other);
        }
        
        return genSlot;
    }
    
    function addRowFor(typeName,optionData) {
        var slotTable = document.getElementById("slotTable");
        var elements = slotTable.getElementsByTagName("TR");

        lastElement = null;
        var count = 1;
        var rowIndex = 0;
        var insertIndex = 0;

        for (i=0;i<elements.length;i++) {                
            if (elements[i].getAttribute("ID") != null) {
                if (elements[i].getAttribute("ID").substring(0, 2) == typeName) {
                    count++;
                    insertIndex = rowIndex;
                    lastElement = elements[i];
                } 
            }

            rowIndex++;
        }

        var newSlot = slotTable.insertRow(insertIndex);
        // var newSlot = document.createElement("tr");        
        var leftCol = document.createElement("td");
        var rightCol = document.createElement("td");
        newSlot.appendChild(leftCol);
        newSlot.appendChild(rightCol);

        leftCol.innerHTML = "&nbsp;";

        var sel = document.createElement("select");
        var currCount = 0;
        
        if (typeName == 'en') {
            reactorCount++;
            noOfReactor++;
            currCount = reactorCount;
            sel.setAttribute("id", "reactors_" + reactorCount);
            sel.id = "reactors_" + reactorCount;
            sel.name = "reactors_" + reactorCount;
        } else if (typeName == 'of') {
            weaponCount++;
            noOfWeapon++;
            currCount = weaponCount;
            sel.setAttribute("id", "weapons_" + weaponCount);
            sel.id = "weapons_" + weaponCount;
            sel.name = "weapons_" + weaponCount;
        } else if (typeName == 'ot') {
            otherCount++;
            noOfOther++;
            currCount = otherCount;
            sel.setAttribute("id", "other_" + otherCount);
            sel.id = "other_" + otherCount;
            sel.name = "other_" + otherCount;
        } else if (typeName == 'de') {
            shieldCount++;
            noOfShield++;
            currCount = shieldCount;
            sel.setAttribute("id", "shields_" + shieldCount);
            sel.id = "shields_" + shieldCount;
            sel.name = "shields_" + shieldCount;
        }
        
        newSlot.setAttribute("id", typeName+currCount);
        createSafeListener(sel,'mousemove',function() {showModuleData(this);});
        createSafeListener(sel,'change',function() {changeModuleData(this);});
        rightCol.appendChild(sel);
        var opt = document.createElement("option");
        opt.text = "--------------- Bitte auswählen ---------------";
        opt.value = -1;
        try {
            sel.add(opt, null); // standards compliant; doesn't work in IE
        }
        catch(ex) {
            sel.add(opt); // IE only
        }                 
        for (i=0;i<optionData.length;i++) {        
            var newOption = document.createElement('option');
            newOption.text = optionData[i].name;         
            newOption.value = optionData[i].id;

            try {
                sel.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                sel.add(newOption); // IE only
            }            
        }        
        var space = document.createTextNode('\u00a0\u00a0\u00a0');
        var space2 = document.createTextNode('\u00a0\u00a0\u00a0');
        
        rightCol.appendChild(space);
        
        var inputField = document.createElement("input");
        inputField.id = sel.id + '_count';
        inputField.name = sel.id + '_count';
        inputField.style.visibility = 'hidden';
        inputField.setAttribute("size", "3");
        inputField.setAttribute("value", "0");     
        createSafeListener(inputField,'change',function() {setCount(this);});
        createSafeListener(inputField,'keyup',function() {setCount(this);});        
        
        rightCol.appendChild(inputField);
        
        var imgField = document.createElement("img");
        imgField.setAttribute("id", 'delete_' + typeName + currCount);
        imgField.setAttribute("src", './pic/cancel.jpg');
        createSafeListener(imgField,'click',function() {removeSlot(this);}); 
        rightCol.appendChild(space2);
        rightCol.appendChild(imgField);
        
        return sel;
    }   
    
    function resetDesign() {
        // clear all 
        
        // set chassis
        
        // go through all select fields and set data
        var setModulesTmp = designDataGlobal.storedDesign.modules;
        var setModules = new Array;
        
        for (i=0;i<setModulesTmp.length;i++) {
            setModules.push(setModulesTmp[i]);
        }
        
        var elements = document.getElementsByTagName("SELECT");
                
        for (var i=0;i<elements.length;i++) {                           
            var selField = elements[i];

            if (selField.name == null) continue;
            var modules = getModulesForSlot(elements[i]);
            var currCounter = 1;
            
            if (modules != null) {
                for (var j=0;j<modules.length;j++) {         
                    var removeIndex = new Array;
                    
                    for (var k=0;k<setModules.length;k++) {                                                  
                        var modSelect = modules[j];
                        var modBuiltIn = setModules[k];
                        
                        if (modSelect.id == modBuiltIn.id) {
                            var actSlot = selField;
                            
                            if (currCounter > 1) {                                                                
                                if (selField.id.match('reactors')) {
                                    actSlot = addSlot(1,null);
                                } else if (selField.id.match('weapons')) {
                                    actSlot = addSlot(2,null);
                                } else if (selField.id.match('shields')) {
                                    actSlot = addSlot(3,null);
                                } else if (selField.id.match('other')) {
                                    actSlot = addSlot(4,null);
                                }
                            }                           

                            // Add module into list                                                        
                            for (var l=0;l<actSlot.options.length;l++) {                                
                                var actOption = actSlot.options[l];
                                
                                if (actOption.value == modBuiltIn.id) {
                                    // setSlotToModule(elements[i],actOption);
                                    // var i2 = i;
                                    actSlot.selectedIndex = l;
                                    changeModuleData(actSlot);
                                    var countField = document.getElementById(actSlot.id + '_count');
                                    countField.value = modBuiltIn.count;
                                    setCount(countField);                                
                                    removeIndex.push(k);
                                }
                            }
                                                        
                            currCounter++;                                 
                        }
                    }                    
                    
                    while (removeIndex.length > 0) {
                        var remIdx = removeIndex.pop();
                        setModules.splice(remIdx,1);
                    }                                           
                }
            }
        }
    }   
    
    function getModulesForSlot(elem) {
        var modules = null;
        if (elem.id.indexOf('shields') == 0) {
            modules = designDataGlobal.chassis[actChassis].modules.shields;
        } else if (elem.id.indexOf('weapons') == 0) {
            modules = designDataGlobal.chassis[actChassis].modules.weapons;
        } else if (elem.id.indexOf('reactors') == 0) {
            modules = designDataGlobal.chassis[actChassis].modules.reactors;
        } else if (elem.id.indexOf('engine') == 0) {
            modules = designDataGlobal.chassis[actChassis].modules.engine;
        } else if (elem.id.indexOf('hyperspaceEngine') == 0) {
            modules = designDataGlobal.chassis[actChassis].modules.engine;
        } else if (elem.id.indexOf('armor') == 0) {
            modules = designDataGlobal.chassis[actChassis].modules.armors;
        } else if (elem.id.indexOf('computer') == 0) {
            modules = designDataGlobal.chassis[actChassis].modules.computers;
        } else if (elem.id.indexOf('other') == 0) {
            modules = designDataGlobal.chassis[actChassis].modules.other;
        }    
        
        return modules;
    }
    
    function changeModuleData(elem) {
        var appName = navigator.appName;
        if (appName == 'Microsoft Internet Explorer') {
            if (window.event.srcElement) {
              elem = window.event.srcElement;
            }
        }                

        var modules = getModulesForSlot(elem);
        var option = elem.options[elem.selectedIndex];       
        
        var countElem = document.getElementById(elem.id + '_count');
        countElem.value = 0;

        for (i=0;i<modules.length;i++) {
            if (modules[i].id == option.value) {
                var attribs = modules[i].Attributes;
                
                var uniqueByType = false;
                var uniqueByShip = false;
                
                for (j=0;j<attribs.length;j++) {                    
                    if (attribs[j].name == 'UNIQUE_PER_TYPE') {
                        uniqueByType = true;
                    } 
                    if (attribs[j].name == 'UNIQUE_PER_SHIP') {
                        uniqueByShip = true;
                    }
                }                
                
                // Make number field visible, else set to default 0
                if (!uniqueByShip) {
                    var numberField = document.getElementById(elem.id+'_count');
                    numberField.style.visibility = 'visible';
                } else {
                    var numberField = document.getElementById(elem.id+'_count');
                    numberField.value = 1;
                }
            }   
        }
        
        if (elem.selectedIndex == 0) {
            var numberField = document.getElementById(elem.id+'_count');
            numberField.style.visibility = 'hidden';            
        }
        
        if (elem.id.match('ngine')) {
            checkEngine(elem);
        }
        
        sumModules();
    }
    function Numsort (a, b) {
    return a - b;
    }

    
    function showModuleData(elem) {
        var moduleName = document.getElementById("modulename");
        var moduleInfo = document.getElementById("moduleinfo");

        var appName = navigator.appName;
        if (appName == 'Microsoft Internet Explorer') {
            elem = window.event.srcElement;
        }
        
        var option = elem.options[elem.selectedIndex];
        var info = "-";
        
        var modules = getModulesForSlot(elem);
        if (modules == null) return;

        for (i=0;i<modules.length;i++) {
            if (modules[i].id == option.value) {
                var firstInfo = true;
                info = '';
                var sorted = new Array();
                var attribs = modules[i].Attributes;
                for (var t=0;t<attribs.length;t++) {
                    if(attribs[t].id == 0){
                        continue;
                    }
                    sorted.push(attribs[t].id);
                }
                sorted.sort(Numsort);
                for (x=0;x<sorted.length;x++) {                
                    for (j=0;j<attribs.length;j++) {

                    if(sorted[x] != attribs[j].id){
                        continue;
                    }
                        
                    if (!firstInfo) {
                        info += '<BR>';                        
                    }
                    if (attribs[j].id == 0) continue;
                    
                    firstInfo = false;         
                    var value = attribs[j].value
                    var name = attribs[j].name;
                    var color = '#FFFFFF';
                    if(attribs[j].name == 'TROOP_STORAGE'){
                        value = Math.round(value * actTroopFactor);
                        color = getColor(actTroopFactor, value);
                    }
                    if(attribs[j].name == 'RESS_STORAGE'){
                        value = Math.round(value * actRessFactor);
                        color = getColor(actRessFactor, value);
                    }
                    if(attribs[j].name == 'HANGAR_CAPACITY'){
                        value = Math.round(value * actHangarFactor);
                        color = getColor(actHangarFactor, value);
                    }
                    if(attribs[j].name == 'ORB_DOCK_CONS_POINTS'){
                        value = Math.round(value);
                    }
                    if(attribs[j].name == 'DOCK_CONS_POINTS'){
                        value = Math.round(value);
                    }
                    if(attribs[j].name == 'MOD_CONS_POINTS'){
                        value = Math.round(value);
                    }
                    if(attribs[j].name == 'SPACE_CONSUMPTION'){
                        value = Math.round(value);
                    }
                    value = "<FONT color='" + color + "'>" + value + "</FONT>";
                    info += name + ': ' + value;     
                    }
                }
            }
        }         
    
        try {
          info = info.replace('SPACE_CONSUMPTION',getImageModule(999) + ML('SPACE_CONSUMPTION',language));
          info = info.replace('ENERGY_PRODUCTION',getImageModule(8) + ML('ENERGY',language) + ' (+)');
          info = info.replace('ENERGY_CONSUMPTION',getImageModule(8) + ML('ENERGY',language) + ' (-)');
          info = info.replace('ATTACK_STRENGTH',getImageModule(999) + ML('ATTACK_STRENGTH',language));
          info = info.replace('DEFENSE_STRENGTH',getImageModule(999) + ML('DEFENSE_STRENGTH',language));
          info = info.replace('SPEED_ACCELERATION',getImageModule(999) + ML('SPEED_ACCELERATION',language));
          info = info.replace('SPEED_HYPER',getImageModule(999) + ML('SPEED_HYPER',language));
          info = info.replace('TROOP_STORAGE',getImageModule(1001) + ML('TROOP_STORAGE',language));
          info = info.replace('RESS_STORAGE',getImageModule(1002) + ML('RESS_STORAGE',language));
          info = info.replace('MOD_CONS_POINTS',getImageModule(30) + ML('MOD_CONS_POINTS',language));
          info = info.replace('ORB_DOCK_CONS_POINTS',getImageModule(32) + ML('ORB_DOCK_CONS_POINTS',language));          
          info = info.replace('DOCK_CONS_POINTS',getImageModule(31) + ML('DOCK_CONS_POINTS',language));          
          info = info.replace('RANGE',getImageModule(999) + ML('RANGE',language));
          info = info.replace('ACCURACY',getImageModule(999) + ML('ACCURACY',language));
          info = info.replace('HANGAR_CAPACITY',getImageModule(999) + ML('HANGAR_CAPACITY',language));
          info = info.replace('MULTIFIRE',getImageModule(999) + ML('MULTIFIRE',language));
          info = info.replace('ATTACK_BONUS',getImageModule(999) + ML('ATTACK_BONUS',language));
          info = info.replace('DEFENSE_BONUS',getImageModule(999) + ML('DEFENSE_BONUS',language));
          info = info.replace('TARGET_FIRE_EFF',getImageModule(999) + ML('TARGET_FIRE_EFF',language));
        } catch (err) {

        }

        if (elem.selectedIndex == 0) {
            moduleName.innerHTML = '';
            moduleInfo.innerHTML = '';
            document.getElementById("infopic").style.visibility = 'hidden';            
        } else {
            moduleName.innerHTML = option.text;
            moduleInfo.innerHTML = info;      
            document.getElementById("infopic").style.visibility = 'visible';
        }                 
    }        
    
    function removeSlot(elem) {
        var appName = navigator.appName;
        if (appName == 'Microsoft Internet Explorer') {
            elem = window.event.srcElement;
        }
        
        var removeName = elem.id.replace('delete_','');         
        var elementToRemove = document.getElementById(removeName);              
        
        if (removeName.indexOf('ot') == 0) {
            if (noOfOther <= 1) {
                alert('You can\'t do that!');
                return;
            }
            
            noOfOther--;
        } else if (removeName.indexOf('en') == 0) {
            if (noOfReactor <= 1) {
                alert('You can\'t do that!');
                return;
            }
            
            noOfReactor--;
        } else if (removeName.indexOf('of') == 0) {
            if (noOfWeapon <= 1) {
                alert('You can\'t do that!');
                return;
            }
            
            noOfWeapon--;
        } else if (removeName.indexOf('de') == 0) {
            if (noOfShield <= 1) {
                alert('You can\'t do that!');
                return;
            }
            
            noOfShield--;
        }       

        var slotTable = document.getElementById("slotTable");
        var elements = slotTable.getElementsByTagName("TR");        

        for (i=0;i<elements.length;i++) {                
            if (elements[i].getAttribute("ID") == elementToRemove.id) {
                slotTable.deleteRow(i);
            }
        }        
        
        sumModules();
    }
    
    function sumModules() {
        spaceConsumption = 0;
        energyProduction = 0;
        energyConsumption = 0;
        attackStrength = 0;
        defenseStrength = 0;
        acceleration = 0;           
        moduleAttackFactor = 1;        
        range = 0;           
        
        var elements = document.getElementsByTagName("SELECT");
        calculateRessources();
                
        var checkedMod = [];
                
        for (i=0;i<elements.length;i++) {
            var modules = getModulesForSlot(elements[i]);
            if (modules != null) {
                elem = elements[i];
                var option = elem.options[elem.selectedIndex];            
                var countElem = document.getElementById(elem.id + '_count');                                                
                if (countElem == null) continue;
                                                
                var count = countElem.value;                
                
                for (j=0;j<modules.length;j++) {
                    if (modules[j].id == option.value) {                                                                                                
                        var firstInfo = true;
                        info = '';

                        if (!checkedMod.indexOf) {
                            for (var z = 0; z < checkedMod.length; z++) {
                                if (checkedMod[z] == option.value) {
                                  continue;
                                }
                            }
                        } else {
                            if (checkedMod.indexOf(option.value) >= 0) continue;
                        }

                        var attribs = modules[j].Attributes;
                        for (k=0;k<attribs.length;k++) {
                            var attrib = attribs[k];
                            
                            if (attrib.name == 'SPACE_CONSUMPTION') {
                                spaceConsumption += attrib.value * count;
                            } else if (attrib.name == 'ENERGY_PRODUCTION') {
                                energyProduction += attrib.value * count;
                            } else if (attrib.name == 'ENERGY_CONSUMPTION') {
                                energyConsumption += attrib.value * count;
                            } else if (attrib.name == 'ATTACK_STRENGTH') {
                                attackStrength += attrib.value * count;
                            } else if (attrib.name == 'DEFENSE_STRENGTH') {
                                defenseStrength += attrib.value * count;
                            } else if (attrib.name == 'SPEED_ACCELERATION') {
                                acceleration += attrib.value;
                            } else if (attrib.name == 'ATTACK_BONUS') {
                                moduleAttackFactor += attrib.value / 100;
                            }
                        }
                        
                        var resEntries = modules[j].Ressources;
                        for (k=0;k<resEntries.length;k++) {
                            var ressEntry = resEntries[k];

                            if (ressEntry.id == 1) {
                                ironCost += parseFloat(ressEntry.qty * count);
                            }
                            if (ressEntry.id == 2) {
                                steelCost += parseFloat(ressEntry.qty * count);
                            }
                            if (ressEntry.id == 3) {
                                terkonitCost += parseFloat(ressEntry.qty * count);
                            }
                            if (ressEntry.id == 4) {
                                ynkeloniumCost += parseFloat(ressEntry.qty * count);
                            }
                            if (ressEntry.id == 5) {
                                howalgoniumCost += parseFloat(ressEntry.qty * count);
                            }
                            if (ressEntry.id == 6) {
                                cvEmbiniumCost += parseFloat(ressEntry.qty * count);
                            }           
                            if (ressEntry.id == 20) {
                                creditCost += parseInt(ressEntry.qty * count);
                            }
                        }                

                        if ((!(elements[i].id.match('engine') == null)) || (!(elements[i].id.match('hyperspace') == null))) {
                            checkedMod.push(option.value);
                        }
                    }
                }                  
            }
        }                        

        displayRessourceCost();
    } 
    
    function setCount(elem) {
        var appName = navigator.appName;
        if (appName == 'Microsoft Internet Explorer') {
            if (window.event.srcElement) {
              elem = window.event.srcElement;
            }
        }                 
        
        sumModules();
        if (spaceConsumption > spaceAvail) {
            // Calculate space consumed by this module and calculate maximum possible modules
            var exzess = spaceConsumption - spaceAvail;            
            var modules = getModulesForSlot(elem);
            var spacePerModule = 0;
            
            var selectFld = document.getElementById(elem.id.replace('_count',''));
            var selOption = selectFld.options[selectFld.selectedIndex];
            for (i=0;i<modules.length;i++) {
                var module = modules[i];
                if (module.id == selOption.value) {
                    attribs = module.Attributes;
                    for (j=0;j<attribs.length;j++) {
                        var attrib = attribs[j];
                        if (attrib.name == 'SPACE_CONSUMPTION') {
                            spacePerModule = attrib.value;
                        }
                    }
                }
            }
            
            if (spacePerModule > 0) {
                var reduceBy = 0;
                while (exzess > 0) {
                    reduceBy++;
                    exzess -= spacePerModule;
                }
                
                elem.value = elem.value - reduceBy;
                sumModules();
            } else {
                elem.value = 0;
                sumModules();
            }
            
            alert('Zuwenig Platz verfügbar (max. '+elem.value+')!');
        }
        
        return false;
    }
    
    function checkEngine(elem) {
        var selectedOption = elem.options[elem.selectedIndex];
        
        var engineSelect = document.getElementById('engines');
        var hyperEngineSelect = document.getElementById('hyperspaceEngines');
        
        var selectedEngine = engineSelect.options[engineSelect.selectedIndex];
        var selectedHSEngine = hyperEngineSelect.options[hyperEngineSelect.selectedIndex];
        
        var engineIsMulti = false;
        var hsEngineIsMulti = false;
        
        var modules = getModulesForSlot(elem);
        
        var rangeNormalEngine = 0;
        var rangeHyperEngine = 0;
        
        for (i=0;i<modules.length;i++) {
            var module = modules[i];
            if (module.id == selectedEngine.value) {
                var attribs = module.Attributes;
                var isNormal = false;
                var isHyper = false;
                
                for (j=0;j<attribs.length;j++) {
                    var attrib = attribs[j];
                    if (attrib.name == 'SPEED_ACCELERATION') {
                        isNormal = true;
                    }
                    if (attrib.name == 'SPEED_HYPER') {
                        if (attrib.value >= 1) {                        
                            isHyper = true;
                        }
                    }                    
                    if (attrib.name == 'RANGE') {
                        rangeNormalEngine = attrib.value;
                    }
                }
                
                if (isNormal && isHyper) {
                    engineIsMulti = true;
                    break;
                }
            }
        }
        
        for (i=0;i<modules.length;i++) {
            var module = modules[i];
            if (module.id == selectedHSEngine.value) {
                var attribs = module.Attributes;
                var isNormal = false;
                var isHyper = false;
                
                for (j=0;j<attribs.length;j++) {
                    var attrib = attribs[j];
                    if (attrib.name == 'SPEED_ACCELERATION') {
                        isNormal = true;
                    }
                    if (attrib.name == 'SPEED_HYPER') {
                        isHyper = true;                        
                    }                  
                    if (attrib.name == 'RANGE') {
                        rangeHyperEngine = attrib.value;
                    }
                }
                
                if (isNormal && isHyper) {
                    hsEngineIsMulti = true;
                    break;
                }
            }
        }        
        engineRange = Math.max(rangeHyperEngine, rangeNormalEngine);
        
        if (engineIsMulti) {
            if (engineSelect.id == elem.id) {
                // Set hyperspace Engine to same value
                setSlotToModule(hyperEngineSelect,selectedOption);
            } else {
                // Set engine to same value
                setSlotToModule(engineSelect,selectedOption);
            }
        }
        
        if (hsEngineIsMulti) {
            if (hyperEngineSelect.id == elem.id) {
                // Set engine to same value
                setSlotToModule(engineSelect,selectedOption);
            } else {
                // Set hsEngine to same value
                setSlotToModule(hyperEngineSelect,selectedOption);
            }
        }        
        
        // alert('EM: ' + engineIsMulti + ' HSEM: ' + hsEngineIsMulti);
        if (!engineIsMulti && hsEngineIsMulti && (elem.id == engineSelect.id)) {
            hyperEngineSelect.selectedIndex = 0;
        }
        
        if (engineIsMulti && !hsEngineIsMulti && (elem.id == hyperEngineSelect.id)) {
            engineSelect.selectedIndex = 0;
        }        
    }      
    
    function setSlotToModule(sel,option) {
        var availOpt = sel.options;
        for (i=0;i<availOpt.length;i++) {
            if (availOpt[i].value == option.value) {
                sel.selectedIndex = i;
                break;
            }
        }
    }
    
    function clearSlots() {
        var elements = document.getElementsByTagName("IMG");
        for (i=0;i<elements.length;i++) {
            var elem = elements[i];
            if (elem.id.match('delete')) {
                removeSlot(elem);
            }
        }
        
        engineRange = 0;
        // clear all default slots
        document.getElementById('engines').selectedIndex = 0;
        document.getElementById('hyperspaceEngines').selectedIndex = 0;
        document.getElementById('armor').selectedIndex = 0;
        document.getElementById('computer').selectedIndex = 0;
        document.getElementById('reactors_1').selectedIndex = 0;
        document.getElementById('reactors_1_count').value = 0;
        document.getElementById('reactors_1_count').style.visibility = 'hidden';
        document.getElementById('shields_1').selectedIndex = 0;
        document.getElementById('shields_1_count').value = 0;
        document.getElementById('shields_1_count').style.visibility = 'hidden';
        document.getElementById('weapons_1').selectedIndex = 0;
        document.getElementById('weapons_1_count').value = 0;
        document.getElementById('weapons_1_count').style.visibility = 'hidden';
        document.getElementById('other_1').selectedIndex = 0;
        document.getElementById('other_1_count').value = 0;
        document.getElementById('other_1_count').style.visibility = 'hidden';
    }

    function fillBaseSlots() {
        armorList = designDataGlobal.chassis[actChassis].modules.armors;
        weaponList = designDataGlobal.chassis[actChassis].modules.weapons;
        engineList = designDataGlobal.chassis[actChassis].modules.engine;
        reactorList = designDataGlobal.chassis[actChassis].modules.reactors;
        shieldList = designDataGlobal.chassis[actChassis].modules.shields;
        otherList = designDataGlobal.chassis[actChassis].modules.other;        
        
        actField = document.getElementById('weapons_1');       
        actField.options.length = 1;        
        for (i=0;i<weaponList.length;i++) {
            var newOption = document.createElement('option');
            newOption.text = weaponList[i].name;
            newOption.value = weaponList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('armor'); 
        actField.options.length = 1; 
        for (i=0;i<armorList.length;i++) {
            newOption = document.createElement('option');
            newOption.text = armorList[i].name;
            newOption.value = armorList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('other_1');  
        actField.options.length = 1; 
        for (i=0;i<otherList.length;i++) {
            newOption = document.createElement('option');
            newOption.text = otherList[i].name;
            newOption.value = otherList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('engines');  
        actField.options.length = 1; 
        for (i=0;i<engineList.length;i++) {
            // Check if engine can be used in normal space
            var attributes = engineList[i].Attributes;
            var allowed = false;
            for (j=0;j<attributes.length;j++) {
                var attrib = attributes[j];
                if (attrib.name == 'SPEED_ACCELERATION') {
                    allowed = true;
                    break;
                }
            }
            
            if (!allowed) continue;
            
            var newOption = document.createElement('option');
            newOption.text = engineList[i].name;
            newOption.value = engineList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('hyperspaceEngines'); 
        actField.options.length = 1;         
        for (i=0;i<engineList.length;i++) {
            // Check if engine can be used in hyperspace
            var attributes = engineList[i].Attributes;
            var allowed = false;
            
            for (j=0;j<attributes.length;j++) {
                var attrib = attributes[j];
                if (attrib.name == 'SPEED_HYPER') {
                    if (attrib.value >= 1) {
                        allowed = true;
                        break;
                    }
                }
            }
            
            if (!allowed) continue;            
            
            var newOption = document.createElement('option');
            newOption.text = engineList[i].name;
            newOption.value = engineList[i].id;

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }

        actField = document.getElementById('reactors_1');  
        actField.options.length = 1; 
        for (i=0;i<reactorList.length;i++) {
            var newOption = document.createElement('option');
            newOption.text = reactorList[i].name;
            newOption.value = reactorList[i].id;            

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }
        
        actField = document.getElementById('shields_1'); 
        actField.options.length = 1; 
        for (i=0;i<shieldList.length;i++) {
            var newOption = document.createElement('option');
            newOption.text = shieldList[i].name;
            newOption.value = shieldList[i].id;            

            try {
                actField.add(newOption, null); // standards compliant; doesn't work in IE
            }
            catch(ex) {
                actField.add(newOption); // IE only
            }
        }                
    }
    
    function switchTypePrevious() {
        if (shipExists) {
            alert(existsErrorMessage);
            return;
        }
        
        if (shiptype == 0) {
            shiptype = 4;
        } else {
            shiptype -= 1;
        }
        
        document.getElementById("shiptype").value = shiptype;
        setTypeName();
        displayShipTypeInfo()
    }
        
    function switchTypeNext() {
        if (shipExists) {
            alert(existsErrorMessage);
            return;
        }        
        
        if (shiptype == 4) {
            shiptype = 0;
        } else {
            shiptype += 1;
        }      
        
        document.getElementById("shiptype").value = shiptype;
        setTypeName();
        displayShipTypeInfo()
    }
    
    function setTypeName() {
        switch (shiptype) {
            case 0:
                document.getElementById("shipTypeName").innerHTML = "Kampfschiff";
                break;
            case 1:
                document.getElementById("shipTypeName").innerHTML = "Transporter";
                break;
            case 2:
                document.getElementById("shipTypeName").innerHTML = "Träger";
                break;
            case 3:
                document.getElementById("shipTypeName").innerHTML = "Kolonieschiff";
                break;
            case 4:
                document.getElementById("shipTypeName").innerHTML = "Raumbasis";
                break;                
        }
    }
    
    function switchChassisPrevious() {
        if (shipExists) {
            alert(existsErrorMessage);
            return;
        }        
        
        if (actChassis == 0) return;
        
        clearSlots();
        actChassis--;
        fillBaseSlots();        
        
        var chassisFld = document.getElementById('cId');
        chassisFld.value = chassisMapping[actChassis];
        var pic = document.getElementById('shippic');
        pic.src = './pic/' + designDataGlobal.chassis[actChassis].picLink;
        var name = document.getElementById('shipname');
        name.innerHTML = designDataGlobal.chassis[actChassis].name;
        
        var space = document.getElementById("spaceTotal");
       
        spaceAvail = parseInt(designDataGlobal.chassis[actChassis].basicValues.space);
        space.innerHTML = number_format(designDataGlobal.chassis[actChassis].basicValues.space,0);
        var structure = document.getElementById("structure");
        structure.innerHTML = "<FONT color='" + getColor(actStructureFactor, parseInt(designDataGlobal.chassis[actChassis].basicValues.structure)) + "'>" + number_format(parseInt(designDataGlobal.chassis[actChassis].basicValues.structure) * actStructureFactor,0) + "</FONT>";      
        if (actChassis == orgChassis) {
            resetDesign();
        }
        sumModules();            
    }
    function getImage(ressId){
        var img = "<IMG style='width:15px; height:15px;' src='" + ressImgs[ressId] + "' /> ";
        return img;
    }
    function getImageModule(ressId, border){
        var borderStr = '';
        if(border){
        borderStr = 'border-style:solid; border-color:gray; border-width:1px;';
        }
        var img = "<IMG  style='width:15px; height:15px;" + borderStr + "' src='" + ressImgs[ressId] + "' /> ";
        return img;
    }
    
    function switchChassisNext() {
        if (shipExists) {
            alert(existsErrorMessage);
            return;
        }        
        
        if (actChassis == (designDataGlobal.chassis.length - 1)) return;
        
        clearSlots();
        actChassis++;
        fillBaseSlots();        
        
        var chassisFld = document.getElementById('cId');
        chassisFld.value = chassisMapping[actChassis];        
        var pic = document.getElementById('shippic');
        pic.src = './pic/' + designDataGlobal.chassis[actChassis].picLink;
        var name = document.getElementById('shipname');
        name.innerHTML = designDataGlobal.chassis[actChassis].name;

        var space = document.getElementById("spaceTotal");

        spaceAvail = parseInt(designDataGlobal.chassis[actChassis].basicValues.space);
        space.innerHTML = number_format(designDataGlobal.chassis[actChassis].basicValues.space,0);
        var structure = document.getElementById("structure");
        structure.innerHTML = "<FONT color='" + getColor(actStructureFactor, parseInt(designDataGlobal.chassis[actChassis].basicValues.structure)) + "'>" + number_format(parseInt(designDataGlobal.chassis[actChassis].basicValues.structure) * actStructureFactor,0) + "</FONT>";              
        if (actChassis == orgChassis) {
            resetDesign();
        }
        sumModules();  
    }