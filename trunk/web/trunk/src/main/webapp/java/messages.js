function clickMessage(msgId,fromBox) {
    var wasChecked = document.getElementById('MID_' + msgId).checked;
    
    if (fromBox) wasChecked = !wasChecked;
    
    var row = document.getElementById('ROW_' + msgId);
    
    if (!wasChecked) {
        row.style = 'font-size:12px; background-color:rgba(88,88,88,0.5);'; 
    } else {
        row.style = 'font-size:12px; background-color:rgba(0,0,0,0);'; 
    }
    
    document.getElementById('MID_' + msgId).checked = !wasChecked;
}