/**************************************************
 * Hiermit werden die Tooltips angezeigt.         *
 * und globale Dinge wie die Fenstergr��e         *
 * verwaltet                                      *
 **************************************************
 *
 * autor Martin
 * autor Rayden
 */

//Die globale ToolTip-Instanz
//(Ich finde das Konzept nicht sch�n
// aber mir ist es derzeit nicht gelungen,
// dem DIV-Element immer die passenden Style-
// Einstellungen zuzuweisen, au�erdem werden dann
// nicht mehrere ToolTips gleichzeitig angezeigt)
var globalToolTipElement = new createToolTip();
var visibleToolTip = 0;

//Eine neue globale ToolTip-Instanz erzeugen
if(document.captureEvents) {
    document.captureEvents(Event.MOUSEMOVE);
}
document.onmousemove = followMouse;

function replace_html(el, html) {
    if(el) {
        var oldEl = (typeof el === "string" ? document.getElementById(el) : el);
        var newEl = document.createElement(oldEl.nodeName);

        // Preserve any properties we care about (id and class in this example)
        newEl.id = oldEl.id;
        newEl.className = oldEl.className;

        //set the new HTML and insert back into the DOM
        newEl.innerHTML = html;
        if(oldEl.parentNode)
            oldEl.parentNode.replaceChild(newEl, oldEl);
        else
            oldEl.innerHTML = html;

        //return a reference to the new element in case we need it
        return newEl;
    }
        
    return '';
};

function followMouse(e) {       
    if (!e) e = window.event;

    notIE = navigator.appName.search(/icrosoft.+/) == -1;
    if (notIE) {
        globalToolTipElement.placetip(e.pageX,e.pageY);
    } else {
        globalToolTipElement.placetip(e.clientX + document.body.scrollLeft,e.clientY + document.body.scrollTop);
    }
}

function createToolTip()
{
    //Hier werden die zeitgesteuerten Sachen verwaltet (Um Flackern zu vermeiden)
    this.timer = null;	
	
    //Wie hei�t das Element, das den Tooltip anzeigt?
    this.elementID = "toolTip";

    //Ist dieses Element schon initialisiert?
    //So spar ich mir einen OnLoad Handler
    this.initOK = false;
	
    //Das Element, in dem der ToolTip angezeigt wird
    this.element = null;
    
    //Die Initialisierung:
    //DIV - Tag erzeugen um den Code aufzunehmen
    //ACHTUNG: Stylesheet muss richtig gesetzt sein;-)
    this.init = function() 
    {
        if (!document.getElementById(this.elementID)) 
        {
            this.element=document.createElement("DIV");
            this.element.style.pos = 'absolute';
            this.element.id=this.elementID;
            document.body.appendChild(this.element);
        }
        this.initOK = true; 
    };
	
    //Den ToolTip verstecken
    this.hide = function()
    {
        this.timer = setTimeout("globalToolTipElement.makeInVisible('"+this.elementID+"')",300); 
    };
	

    this.placetip = function(xMouse,yMouse) {
        if (visibleToolTip == 0) return;
        // Die Breite des Tooltips f�r die Positionierung berechnen
        var width = document.getElementById(this.elementID).scrollWidth;
        if ((!width)  || (width > 500)) {
            // Dies ist ein Workaround, f�r tooltips, f�r die noch keine Breite berechnet wurde 
            width = 200;
        }
        var appName = navigator.appName;
        
        if (appName == 'Microsoft Internet Explorer') {
            
            // Wenn der Tooltip �ber den rechten Seitenrand rausgehen w�rde, dann ihn vor dem Mauscursor anzeigen
            // Die zus�tzlichen 30 Bildpunke sollen Fehler mit der genauen Randpositionierung beheben.  
            if (xMouse + width + 30 > Fensterweite()) {
                
                if(xMouse != null){
                    document.getElementById(this.elementID).style.left = (xMouse - width - 10)+"px";
                }
            } else {
                if(xMouse != null){
                    document.getElementById(this.elementID).style.left = (xMouse+10);
                }
            }
            if(yMouse != null){
                document.getElementById(this.elementID).style.top = (yMouse+10);
            }
        } else {
            // Wenn der Tooltip �ber den rechten Seitenrand rausgehen w�rde, dann ihn vor dem Mauscursor anzeigen
            // Die zus�tzlichen 30 Bildpunke sollen Fehler mit der genauen Randpositionierung beheben.  
            if (xMouse + width + 30 > Fensterweite()) {
                
                if(xMouse != null){
                    document.getElementById(this.elementID).style.left = (xMouse - width - 10)+"px";
                }
            } else {
                
                if(xMouse != null){
                    document.getElementById(this.elementID).style.left = (xMouse+10)+"px";
                }
            }
            
            if(yMouse != null){
                document.getElementById(this.elementID).style.top = (yMouse+10)+"px";
            }
        }
    	
    };

    //Den ToolTip anzeigen 
    //Parameter: Der Event, und der anzuzeigende Text
    this.show = function(e, toolTip) { 
        if (!this.initOK)
            this.init();
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer=0;
        }
      
        //Ich wei� nicht wieso, aber es ist notwendig, erst den
        //Inhalt zu l�schen, und dann einen neuen zu setzen
        this.element.style.width = "auto";
        this.element.innerHTML = "";
        this.element.innerHTML = toolTip;

        //Position des ToolTips = position der Maus
        if (e.pageX)
        {
            this.placetip(e.pageX, e.pageY)
        }
        else	{
            this.placetip(e.x, e.y)
        }
      
        //Nach ca. 300 ms sichtar machen
        this.timer = setTimeout("globalToolTipElement.makeVisible('"+this.elementID+"')",300); 
    };
      
    //ToolTip sichtbar machen (alle Parameter m�ssen schon gesetzt sein)
    this.makeVisible = function(id) 
    {
        var el=document.getElementById(id);
        if(el)
            el.style.visibility='visible';
    }
      
    //Tooltip verstecken
    this.makeInVisible = function(id) 
    {
        var el=document.getElementById(id);
        if(el)
            el.style.visibility='hidden';
    }
};

//ToolTip anzeigen
//Kapselt den Aufruf an das globale Objekt
function doTooltip(e,tip)
{
if(e == null)return;
    globalToolTipElement.show(e,tip);
    visibleToolTip = 1;
}

//ToolTip verstecken
//Kapselt den Aufruf an das globale Objekt
function hideTip()
{
    globalToolTipElement.hide();
    visibleToolTip = 0;
}

var Weite = 0;
var Hoehe = 0;

//Alles f�r die Fenstergr��en�berwachung
//Wie breit ist das Fenster?
function Fensterweite () {
    if (window && window.innerWidth) {
        return window.innerWidth;
    } else if (document.body && document.body.offsetWidth) {
        return document.body.offsetWidth;
    } else {
        return 0;
    }
}

//Wie hoch ist das Fenster?
function Fensterhoehe () {
    if (window && window.innerHeight) {
        return window.innerHeight;
    } else if (document.body && document.body.offsetHeight) {
        return document.body.offsetHeight;
    } else {
        return 0;
    }
}

//Seite neu darstellen
function neuAufbau () {
    if (Weite != Fensterweite() || Hoehe != Fensterhoehe()) {
        Weite = Fensterweite();
        Hoehe = Fensterhoehe();  
        if (typeof setPic === 'function') {
            setPic();
        };
    }
}

/* �berwachung von Netscape initialisieren */
function bodyLoaded(){  
    if(document && document.body){           
        return (!document.body.offsetWidth);
    } else{  
        window.setTimeout( function(){
            bodyLoaded();
        }, 100);  
    }

    return false;
}  

if (!window.Weite && window.innerWidth) {  
    window.onresize = neuAufbau;
  
    Weite = Fensterweite();
    Hoehe = Fensterhoehe();
} else if (!window.Weite && bodyLoaded) { // document.body.offsetWidth   
    window.onresize = neuAufbau;
  
    Weite = Fensterweite();
    Hoehe = Fensterhoehe();
} 

/* Hiermit k�nnen Elemente immer wieder angezeigt werden bzw. verschwinden*/
function toogleVisibility(name)
{
    if (getElement('id', name).style.display == 'none')
    {
        getElement('id', name).style.display = '';
    }
    else 
    {
        getElement('id', name).style.display = 'none';
    }
}

/* Hiermit kann ein Bild ausgetauscht werden*/
function exchangeImgs(name, bild1, alt1, bild2, alt2)
{
    if (getElement('id', name).alt == alt1)
    {
        getElement('id', name).src = bild2;
        getElement('id', name).alt = alt2;
    }
    else 
    {
        getElement('id', name).src = bild1;
        getElement('id', name).alt = alt1;
    }
}

/* DHTML-Bibliothek */
/* Kopiert von de.Selfhtml.org */

var DHTML = false, DOM = false, MSIE4 = false, NS4 = false, OP = false;

if (document.getElementById) {
    DHTML = true;
    DOM = true;
} else {
    if (document.all) {
        DHTML = true;
        MSIE4 = true;
    } else {
        if (document.layers) {
            DHTML = true;
            NS4 = true;
        }
    }
}
if (window.opera) {
    OP = true;
}

function getElement (Mode, Identifier, ElementNumber) {
    var Element, ElementList;
    if (DOM) {
        if (Mode.toLowerCase() == "id") {
            Element = document.getElementById(Identifier);
            if (!Element) {
                Element = false;
            }
            return Element;
        }
        if (Mode.toLowerCase() == "name") {
            ElementList = document.getElementsByName(Identifier);
            Element = ElementList[ElementNumber];
            if (!Element) {
                Element = false;
            }
            return Element;
        }
        if (Mode.toLowerCase() == "tagname") {
            ElementList = document.getElementsByTagName(Identifier);
            Element = ElementList[ElementNumber];
            if (!Element) {
                Element = false;
            }
            return Element;
        }
        return false;
    }
    if (MSIE4) {
        if (Mode.toLowerCase() == "id" || Mode.toLowerCase() == "name") {
            Element = document.all(Identifier);
            if (!Element) {
                Element = false;
            }
            return Element;
        }
        if (Mode.toLowerCase() == "tagname") {
            ElementList = document.all.tags(Identifier);
            Element = ElementList[ElementNumber];
            if (!Element) {
                Element = false;
            }
            return Element;
        }
        return false;
    }
    if (NS4) {
        if (Mode.toLowerCase() == "id" || Mode.toLowerCase() == "name") {
            Element = document[Identifier];
            if (!Element) {
                Element = document.anchors[Identifier];
            }
            if (!Element) {
                Element = false;
            }
            return Element;
        }
        if (Mode.toLowerCase() == "layerindex") {
            Element = document.layers[Identifier];
            if (!Element) {
                Element = false;
            }
            return Element;
        }
        return false;
    }
    return false;
}

function getAttribute (Mode, Identifier, ElementNumber, AttributeName) {
    var Attribute;
    var Element = getElement(Mode, Identifier, ElementNumber);
    if (!Element) {
        return false;
    }
    if (DOM || MSIE4) {
        Attribute = Element.getAttribute(AttributeName);
        return Attribute;
    }
    if (NS4) {
        Attribute = Element[AttributeName]
        if (!Attribute) {
            Attribute = false;
        }
        return Attribute;
    }
    return false;
}

function getContent (Mode, Identifier, ElementNumber) {
    var Content;
    var Element = getElement(Mode, Identifier, ElementNumber);
    if (!Element) {
        return false;
    }
    if (DOM && Element.firstChild) {
        if (Element.firstChild.nodeType == 3) {
            Content = Element.firstChild.nodeValue;
        } else {
            Content = "";
        }
        return Content;
    }
    if (MSIE4) {
        Content = Element.innerText;
        return Content;
    }
    return false;
}

function setContent (Mode, Identifier, ElementNumber, Text) {
    var Element = getElement(Mode, Identifier, ElementNumber);
    if (!Element) {
        return false;
    }
    if (DOM && Element.firstChild) {
        Element.firstChild.nodeValue = Text;
        return true;
    }
    if (MSIE4) {
        Element.innerText = Text;
        return true;
    }
    if (NS4) {
        Element.document.open();
        Element.document.write(Text);
        Element.document.close();
        return true;
    }
}