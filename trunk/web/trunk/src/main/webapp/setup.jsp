
<%@page import="at.darkdestiny.core.result.PropertiesCheckResult"%>
<%@page import="at.darkdestiny.core.enumeration.EPlanetDensity"%>
<%@page import="at.darkdestiny.core.enumeration.EGalaxyType"%>
<%@page import="at.darkdestiny.core.result.BaseResult" %>
<%@page import="at.darkdestiny.core.service.SetupService" %>
<%@page import="java.util.*" %>
<HTML>
    <HEAD>
        <link rel="stylesheet" href="main.css.jsp" type="text/css">
        <script src="java/main.js" type="text/javascript"></script>
    </HEAD>
    <BODY bgcolor="black">
<%
            final int TYPE_GENERATE_PROPERTY_FILES = 1;
            final int TYPE_OVERVIEW = 0;
            final int TYPE_GENERATE_CONSTANTS_STEP1 = 4;
            final int TYPE_GENERATE_CONSTANTS_STEP2 = 5;
            final int PROCESS_GENERATE_CONSTANTS = 6;
            final int PROCESS_GENERATE_PROPERTY_FILES = 1;
            int type = TYPE_OVERVIEW;
            if (request.getParameter("type") != null) {
                type = Integer.parseInt(request.getParameter("type"));
            }
            int process = 0;
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            ArrayList<BaseResult> results = new ArrayList<BaseResult>();
            switch (process) {
                case (PROCESS_GENERATE_PROPERTY_FILES):
                    //Generate Property Files
                    results.addAll(SetupService.generatePropertyFiles(request.getParameterMap()));
                case (PROCESS_GENERATE_CONSTANTS):
                    //Generate Constants
                    results.addAll(SetupService.generateConstants(request.getParameterMap()));
                    break;
            }
%>

        <CENTER>
            <% for (BaseResult br : results) {%>
            <%= br.getFormattedString()%><BR>
            <% }%>
            <%
                        switch (type) {
                            
                            case(TYPE_OVERVIEW):
                                %>
                                
                                <CENTER>
                                    <A href="setupcheck.jsp">System-Check</A><BR><BR>
                                    <A href="setup.jsp?type=<%= TYPE_GENERATE_PROPERTY_FILES %>">Generate Property-Files</A><BR>
                                    <A href="setup.jsp?type=<%= TYPE_GENERATE_CONSTANTS_STEP1 %>">Insert constants.sql</A><BR>
                                
                                </CENTER>
                                <%
                                break;
                            
                            case (TYPE_GENERATE_PROPERTY_FILES):
                                
                                PropertiesCheckResult pcr = SetupService.checkProperties();
                                if(pcr.isAllPropertiesFilesOkay()){
                                    %>
                                    <CENTER><FONT color="red"><B>The game.properties and db.properties file are already existing.<BR>
                                        Please delete them and restart your server before generating new ones</B></FONT><BR>
                                    <A href="setupcheck.jsp">Back</A></CENTER>
                                    <%
                                }else if(pcr.isIsDbPropertiesFileOkay()){
                                    %>
                                    <CENTER><FONT color="red"><B>The db.properties file is already existing.<BR>
                                        Please delete it and restart your server before generating a new one</B></FONT><BR>
                                    <A href="setupcheck.jsp">Back</A></CENTER>
                                    <%
                                    
                                }else if(pcr.isIsGamePropertiesFileOkay()){
                                    %>
                                    <CENTER><FONT color="red"><B>The game.properties file is already existing.<BR>
                                        Please delete it and restart your server before generating a new one</B></FONT><BR>
                                    <A href="setupcheck.jsp">Back</A></CENTER>
                                    <%
                                    
                                }else{
            %>
            <BR>

            <TABLE class="bluetrans" style="font-size: 10px; font-weight: bolder;"><TR><TD>

            Please note that this setup is not done in the best and fastest way possible and there are some steps that might take time.<BR>
            If you encounter any problems you cant fix, let us know on our forum.<BR>
            Due to the fact that we have hardly people hosting the game, we don't properly support self-installation
            and English as a language, but focus on programming and bugfixing.<BR>
            <FONT color="red">If there is a need to do this. Let us know and we will focus more on that parts.</FONT>
                    </TD>
                </TR>
            </TABLE>
            <BR>
            <CENTER>
                <FONT size="6">SETUP - Generating Game-, Universe- and Database-Properties<BR>&nbsp;</FONT>
            </CENTER>
            <FORM action="setup.jsp?process=<%=PROCESS_GENERATE_PROPERTY_FILES %>" method="post">
                <TABLE class="bluetrans">
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            Game-Settings
                        </TD>
                    </TR>
                    <TR>
                        <TD>Gamehost-Url</TD>
                        <TD><INPUT name="gamehostURL" class="dark" style="width:200px;" value="http://www.thedarkdestiny.at/dd"/></TD>
                    </TR>
                    <TR>
                        <TD>Startpage (after logout)</TD>
                        <TD><INPUT name="startURL" class="dark" style="width:200px;" value="http://www.thedarkdestiny.at"/></TD>
                    </TR>
                    <TR>
                        <TD>Source-Path (for programming)</TD>
                        <TD><INPUT name="srcPath" class="dark" style="width:200px;" value="c://dd/trunk_dev"/></TD>
                    </TR>
                    <TR>
                        <TD>Class-Folder</TD>
                        <TD><INPUT name="classFolder" class="dark" style="width:200px;" value="c:/ddprog/trunk_dev/build/web/WEB-INF/classes"/></TD>
                    </TR>
                    <TR>
                        <TD>Ticktime in ms (600000 recommended)</TD>
                        <TD><INPUT name="tickTime" class="dark" style="width:200px;" value="600000"/></TD>
                    </TR>
                </TABLE>
                <BR>
                <TABLE class="bluetrans">
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            Datenbase-Settings
                        </TD>
                    </TR>
                    <TR>
                        <TD>Host-Ip:Port</TD>
                        <TD><INPUT name="hostip" class="dark" style="width:200px;" value="127.0.0.1:3306"/></TD>
                    </TR>
                    <TR>
                        <TD>Database-Name</TD>
                        <TD><INPUT name="dbname" class="dark" style="width:200px;" value="dd_database"/></TD>
                    </TR>
                    <TR>
                        <TD>User</TD>
                        <TD><INPUT name="user" class="dark" style="width:200px;" value="user" /></TD>
                    </TR>
                    <TR>
                        <TD>Password</TD>
                        <TD><INPUT name="password" class="dark" style="width:200px;" value="password"/></TD>
                    </TR>
                </TABLE><BR>
                <TABLE class="bluetrans">
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            Universe Size
                        </TD>
                    </TR>
                    <TR>
                        <TD>Universe-Height (e.g.: 1000,2000,4000)</TD>
                        <TD align="center"><INPUT name="height" class="dark" style="width:200px;" value="2000"/></TD>
                    </TR>
                    <TR>
                        <TD>Universe-Width (e.g.: 1000,2000,4000)</TD>
                        <TD align="center"><INPUT name="width" class="dark" style="width:200px;" value="2000"/></TD>
                    </TR>
                    <TR>
                        <TD colspan="2" align="center"><INPUT type="submit" class="dark" value="Continue" /></TD>
                    </TR>
                </TABLE>
            </FORM>


            <%
                       }
                                    break;

                           

                            case (TYPE_GENERATE_CONSTANTS_STEP1):

                                //insert constants
            %>
            <BR><BR>
            <FORM action="setup.jsp?type=<%= TYPE_GENERATE_CONSTANTS_STEP2 %>" method="post">
                <TABLE class="bluetrans" style="font-weight: bolder">
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            Constants
                        </TD>
                    </TR>
                    <TR>
                        <TD>
                            Please set the path of the file with the constants (e.g.: Constants.sql)
                        </TD>
                    </TR>
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            <INPUT name="insertFile" value="C:/Constants.sql" class="dark" />
                        </TD>
                    </TR>

                    <TR>
                        <TD colspan="2" align="center"><INPUT type="submit" class="dark" value="Continue" /></TD>
                    </TR>
                </TABLE>
            </FORM>
            <%
                                    break;
                            case (TYPE_GENERATE_CONSTANTS_STEP2):

            HashMap<String, ArrayList<String>> inserts = SetupService.getInserts(request.getParameter("insertFile"));
                                //insert constants
            %>
            <BR><BR>
            Displaying and Inserting the constants may take some time. Check the System.out for the status
            <FORM action="setup.jsp?process=<%= PROCESS_GENERATE_CONSTANTS %>" method="post">
                <TABLE class="bluetrans" width="80%" style="font-weight: bolder">
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            Insert Game-Constants
                        </TD>
                    </TR>
                    <%
                    for(Map.Entry<String, ArrayList<String>> entry1 : inserts.entrySet()){
                        String insert = "";
                        for(String s : entry1.getValue()){
                                insert += s + "\n";
                            }
                    %>
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            Table - <%= entry1.getKey() %>
                        </TD>
                    </TR>
                    <TR class="blue2">
                        <TD colspan="2" align="center">
                            <TEXTAREA name="table_<%= entry1.getKey() %>" style="width:100%; font-size: 10px;  height: 100px;"><%= insert %></TEXTAREA>
                        </TD>
                    </TR>
                    <% } %>
                    <TR>
                        <TD colspan="2" align="center"><INPUT type="submit" class="dark" value="Generate" /></TD>
                    </TR>
                </TABLE>
            </FORM>
            <%
                                    break;
                            }%>
        </CENTER>
    </BODY>
</HTML>