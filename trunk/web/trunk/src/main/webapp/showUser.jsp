<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.service.ProfileService" %>
<%@page import="at.darkdestiny.core.model.UserData" %>
<%@page import="at.darkdestiny.core.model.User" %>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));

int destUserId = 0;
UserData ud = null;
User u = null;
String desc = "";

if (request.getParameter("destUserId") != null) {
    destUserId = Integer.parseInt(request.getParameter("destUserId"));
    ud = ProfileService.findUserDataByUserId(destUserId);
    u = ProfileService.findUserByUserId(destUserId);
    desc = ud.getDescription();
}

if (ud.getDescription() == null) desc  = "<BR>Keine Beschreibung vorhanden";
%>
<script language="Javascript">        
function setPic() {
    var availSpace = (Weite-355) * 0.8;

    document.getElementById("bild").setAttribute("width", availSpace, 0);
}
</script>

<TABLE width="80%" align="center" style="font-size:13px">
    <TR bgcolor="#708090" CLASS="blue2"><TD align="center"><B><%= u.getGameName() %></B></TD></TR>
<% 
    if ((ud.getPic() != null) && (!ud.getPic().equals(""))) {
%>
        <TR><TD align="center"><IMG id="bild" src="<%= ud.getPic() %>"/></TD></TR>
<%  
    } 
%>
    
    <TR><TD align="center"><%= desc %></TD></TR>
   <TR><TD align="center"> <A onClick="javascript:history.go(-1)"><B><BR><%= ML.getMLStr("global_back" , userId)%></B></A></TD></TR>
</TABLE>