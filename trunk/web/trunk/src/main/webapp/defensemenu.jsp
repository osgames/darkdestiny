<%-- 
    Document   : defensemenu
    Created on : 21.03.2009, 17:12:44
    Author     : Stefan
--%>
<%@ page import="java.util.*" %>
<%@ page import="at.darkdestiny.core.*" %>
<%@ page import="at.darkdestiny.core.military.*" %>
<%@ page import="at.darkdestiny.core.utilities.*" %>
<%@ page import="at.darkdestiny.core.databuffer.*" %>
<%@ page import="at.darkdestiny.core.model.UserData" %>
<%@ page import="at.darkdestiny.core.service.DefenseService" %>

<%
            int showType = 2;

            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            final int TYPE_TERRITORY = 4;
            
            if (request.getParameter("type") != null) {
                showType = Integer.parseInt(request.getParameter("type"));
            }
%> 


<TABLE width="80%">
    <TR align="center">
        <TD width="20%"><U><A href="main.jsp?page=defensemenu&type=1"><%= ML.getMLStr("defensemenu_link_acutalplanet", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=defensemenu&type=2"><%= ML.getMLStr("defensemenu_link_defenseoverview", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=defensemenu&type=3"><%= ML.getMLStr("defensemenu_link_groundcombat", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=defensemenu&type=<%= TYPE_TERRITORY %>"><%= ML.getMLStr("defensemenu_link_territory", userId)%></A></U></TD>
        <% /* if(warTable.findAllWarsForPlayer(usserData)).size() > 0){*/%>
        <% /*}else{*/%>
        <!-- <TD width="20%"><U><FONT color="#888888"></FONT></U></TD>-->
        <%
        //}
        %>
    </TR>
</TABLE><BR>
<%
    switch (showType) {
            case (1):%>
                <jsp:include page="defenseManage.jsp" />
                <%  break;
            case (2):%>
                <jsp:include page="defenseOverview.jsp" />
            <%      break;
            case (3): %>
                <jsp:include page="battlereport.jsp" />
            <%
                    break;
            case (TYPE_TERRITORY): %>
                <jsp:include page="territory.jsp" />
            <%
                    break;
                }
    %>

