<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.core.model.Planet"%>
<%@page import="at.darkdestiny.core.service.LoginService"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="java.sql.*" %>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.framework.access.DbConnect" %>
<%
    int userId = 0;
    int x = -1;
    int y = -1;
    session = request.getSession();
    Locale locale = request.getLocale();
    if (session.getAttribute("Language") != null) {
        locale = (Locale) session.getAttribute("Language");
    }
    try {
        userId = Integer.parseInt((String) session.getAttribute("userId"));
        x = Integer.parseInt(request.getParameter("tileCoordinateX"));
        y = Integer.parseInt(request.getParameter("tileCoordinateY"));
    } catch (Exception e) {
        DebugBuffer.warning("Invalid call on chooseStartLoc by IP: " + request.getRemoteHost());
        if (request.getRemoteHost().equalsIgnoreCase("83.216.244.30")) {
            response.sendError(response.SC_BAD_REQUEST, "Geh bitte jemand anders auf an Sack und hoer auf diese Seite ohne Parameter aufzurufen ;)");
            return;
        }
    }

    if ((userId == 0) || (x == -1) || (y == -1)) {
        DebugBuffer.fatalError("FATAL ERROR > userId=" + " x=" + x + " y=" + y);
        DebugBuffer.fatalError("FATAL ERROR Remotehost : " + request.getRemoteHost());
        String ErrorMsg = ML.getMLStr("login_err_creationerror", locale);
%>
<jsp:forward page="login.jsp" >
    <jsp:param name="errmsg" value='<%= ErrorMsg%>' />
</jsp:forward>
<%
    }
    boolean foundPlanet = false;
    for (at.darkdestiny.core.model.System s : Service.systemDAO.findAll()) {

        if (s.getX() < x || s.getX() > x + GameConstants.TILE_SIZE
                || s.getY() < y || s.getY() > y + GameConstants.TILE_SIZE) {
            continue;
        }

        boolean occupied = false;
        boolean startable = true;
        boolean hasMType = false;

        if (!s.getGalaxy().getPlayerStart()) {
            startable = false;
        }
        for (Planet p : Service.planetDAO.findBySystemId(s.getId())) {
            if (p.getLandType().equals(Planet.LANDTYPE_M)) {
                hasMType = true;
            }
            if (LoginService.findPlayerPlanetByPlanetId(p.getId()) != null) {
                occupied = true;
            }


        }
        if (!hasMType || occupied || !startable) {
            continue;
        }


        String ErrorMsg = LoginService.createNewUserEntries(userId, s.getId(), locale.toString());
        if (ErrorMsg.equalsIgnoreCase("")) {
            session.setAttribute("visJoinMap", "true");
            %>
            <jsp:forward page="enter.jsp"/>
            <%
        }
    }
%>
        <jsp:forward page="login.jsp" >
            <jsp:param name="errmsg" value='<%= ML.getMLStr("login_err_systemalreadyinuse", userId)%>' />
        </jsp:forward>
<%
    try {
        session.invalidate();
    } catch (IllegalStateException ise) {
    }
%>