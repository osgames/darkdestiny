<%@page import="at.darkdestiny.core.hangar.*"%>
<%@page import="java.util.*"%>

<%
HangarUtilities hu = new HangarUtilities();
hu.initializeFleet(14);
ArrayList<ShipTypeEntry> newFleet = hu.getNestedHangarStructure();
out.write("FINAL LIST CONTAINS " + newFleet.size() + " DESIGN ENTRIES<BR><BR>");
for (int i=0;i<newFleet.size();i++) {
    ShipTypeEntry ste = (ShipTypeEntry)newFleet.get(i);
    
    for (int j=0;j<ste.getNestedLevel();j++) {
        out.write("---");
    }
    
    out.write("DESIGN="+ste.getId()+" COUNT="+ste.getCount()+"<BR>");
}
%> 
