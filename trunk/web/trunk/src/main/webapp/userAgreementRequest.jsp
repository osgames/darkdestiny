<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.core.model.UserData"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.service.LoginService" %>
<%@page import="java.util.Locale"%>
<%@page import="at.darkdestiny.core.model.Language"%>
<%@page import="at.darkdestiny.core.enumeration.*"%>
<%@page import="at.darkdestiny.core.*"%>
<script src="java/main.js" type="text/javascript"></script>
<link rel="stylesheet" href="main.css.jsp" type="text/css">
<HTML>
    <HEAD>
        <TITLE>Dark Destiny - User Agreement</TITLE>

    </HEAD>
    <body text="#FFFF00" BGCOLOR="#000000" background="<%=GameConfig.getInstance().picPath()%>pic/sh.jpg">
        <script language="javascript" type="text/javascript">
            
            
            var appName = navigator.appName;
            function changeSubmitButton(checked)
            {
                document.getElementById("submit").disabled = !checked;
                updateButton();
            }
            function updateButton(){
                
                if (appName != 'Microsoft Internet Explorer') {
                    if(document.getElementById("submit").disabled){
                        document.getElementById("submit").setAttribute("style", "color:gray;");
                    }else{
                        document.getElementById("submit").setAttribute("style", "color:black;");
                    }
                }
            }
        </script>
        <TABLE align="center" border="0" cellpadding="0" cellspacing="0">
            <%
                int userId = Integer.parseInt((String) session.getAttribute("userId"));
                boolean accepted = false;
                if (request.getParameter("accepted") != null) {
                    int acceptedValue = Integer.parseInt(request.getParameter("accepted"));
                    if(acceptedValue == 1){
                        accepted = true;
                    }

                    User u = Service.userDAO.findById(userId);
                    u.setAcceptedUserAgreement(accepted);
                    Service.userDAO.update(u);
                    if (accepted) {
                        String forwardPage = request.getParameter("page");
            %>

            <jsp:forward page="main.jsp" >
                <jsp:param name="page" value="<%= forwardPage%>" />
            </jsp:forward>    
            <%
                    }
                }
                Locale locale = request.getLocale();
                if (session.getAttribute("Language") != null) {
                    locale = (Locale) session.getAttribute("Language");

                }

                String forwardPage = request.getParameter("page");
            %>
            <TR>
                <TD></TD>
                <TD width="*" align="center">
                    <FORM name="anmelden" method="post" action="userAgreementRequest.jsp?accepted=1<% if (forwardPage != null) {%>&page=<%= forwardPage%><% }%>">
                        <TABLE align="center" width="100%" style="font-size: 13px; color: yellow;">
                            <TR>
                                <TD>Sie haben die Nutzungs- und Datenschutzbestimmungen noch nicht akzeptiert oder sie wurden gešndert</TD>

                            </TR>
                            <TR>
                                <TD>Ich habe die <a href="UsageAgreement.htm" target="new">Nutzungs- und Datenschutzbestimmungen</a> gelesen und bin mit ihnen einverstanden</TD>
                                <TD>
                                    <INPUT type="checkbox" id="checkbox" onClick="changeSubmitButton(document.getElementById('checkbox').checked);" />
                                </TD>
                            </TR>
                        </TABLE>
                        <P>
                        <TABLE width="50%">
                            <TR align="center">
                                <TD>
                                    <INPUT disabled="true" id="submit" type=submit name="submit" value="Bestštigen">
                                </TD>
                            </TR>
                        </TABLE>
                    </FORM>
                </TD>
            </TR>
        </TABLE>
        <script language="javascript" type="text/javascript">
            
            
            updateButton();
        </script>
    </BODY>
</HTML>