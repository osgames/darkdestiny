<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.buildable.*" %>
<%@page import="at.darkdestiny.core.ships.*" %>
<%@page import="at.darkdestiny.core.FormatUtilities" %>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="at.darkdestiny.core.service.RessourceService" %>
<%@page import="at.darkdestiny.core.service.ShipDesignService" %>
<%@page import="at.darkdestiny.core.service.DefenseService" %>
<%@page import="at.darkdestiny.core.model.ShipDesign" %>
<%@page import="at.darkdestiny.core.model.Ressource" %>
<%@page import="at.darkdestiny.core.result.BaseResult" %>
<%@page import="at.darkdestiny.core.enumeration.EDamageLevel" %>
<%@page import="java.util.*" %>

<%
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int bufferId = Integer.parseInt(request.getParameter("bufId"));

ParameterBuffer pb = BufferHandling.getBuffer(userId, bufferId);

if (request.getParameter("c") != null) {
    BaseResult br = null;

    if (pb instanceof ShipScrapBuffer) {
        br = ShipDesignService.scrapShip(userId, (ShipScrapBuffer)pb);
    } else if (pb instanceof UpgradeShipBuffer) {
        br = ShipDesignService.upgradeShip(userId, (UpgradeShipBuffer)pb);
    } else if (pb instanceof RepairShipBuffer) {
        br = ShipDesignService.repairShips(userId, (RepairShipBuffer)pb);
    } else if (pb instanceof StarbaseScrapBuffer) {
        br = DefenseService.scrapStarbase(userId, (StarbaseScrapBuffer)pb);
    } else if (pb instanceof UpgradeStarbaseBuffer) {
        br = DefenseService.upgradeStarbase(userId, (UpgradeStarbaseBuffer)pb);
    }

    if (br.isError()) {
        %><jsp:forward page="../main.jsp" >
        <jsp:param name="msg" value='<%=br.getMessage()%>' />
        <jsp:param name="page" value='showerror' />
        </jsp:forward><%
    } else {
        %><jsp:forward page="../main.jsp" >
        <jsp:param name="page" value='new/fleet' />
        </jsp:forward><%
    }
}

if (pb instanceof ShipScrapBuffer) {
        ShipScrapBuffer ssb = (ShipScrapBuffer)pb;

        ShipDesignExt sde = new ShipDesignExt(ssb.getDesignId());
        ShipDesign sd = (ShipDesign)sde.getBase();

        ShipScrapCost ssc = sde.getShipScrapCost(ssb.getCount());
%>
        <FONT size="-1"><%= ML.getMLStr("lbl_ship_modification_scrap_txt", userId) %><BR />
        <%= ssb.getCount() %>x <%= sd.getName() %><BR /><BR />
        <%= ML.getMLStr("lbl_ship_modification_scrap_txt2_p1", userId) %>&nbsp;<FONT color=yellow><B><%= FormatUtilities.getFormattedNumber(ssc.getRess(Ressource.CREDITS)) %></B></FONT>
        <%= ML.getMLStr("lbl_ship_modification_scrap_txt2_p2", userId) %><BR/>
        <%= ML.getMLStr("lbl_ship_modification_scrap_gain", userId) %><BR/><BR/></FONT>
        <TABLE border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_scrap_gain_short", userId) %></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_resource", userId) %></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_quantity", userId) %></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (ssc.getRess(r.getId()) > 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(),userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(ssc.getRess(r.getId())) + "</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />
        <FORM method='post' action='main.jsp?page=confirm/shipModification&bufId=<%= ssb.getId() %>&c=1' name='confirm' %>
            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%
} else if (pb instanceof UpgradeShipBuffer) {
        UpgradeShipBuffer usb = (UpgradeShipBuffer)pb;

        ShipDesignExt sde = new ShipDesignExt(usb.getDesignId());
        ShipDesign sd = (ShipDesign)sde.getBase();
        ShipDesignExt sdeNew = new ShipDesignExt(usb.getToDesignId());
        ShipDesign sdNew = (ShipDesign)sdeNew.getBase();

        ShipUpgradeCost suc = sde.getShipUpgradeCost(sdeNew, usb.getCount());

        String oldName = sd.getName();
        String newName = sdNew.getName();
%>
        <FONT size="-1"><%= ML.getMLStr("lbl_ship_modification_rebuild_txt", userId) %><BR/>
                <%= oldName %> --&gt; <%= newName %> (<%= usb.getCount() %>x)<BR/><BR/>
                <%= ML.getMLStr("lbl_ship_modification_rebuild_txt2_p1", userId) %>&nbsp;<FONT color=yellow><B><%= FormatUtilities.getFormattedDecimal(suc.getRess(Ressource.CREDITS),0) %></B></FONT>
        <%= ML.getMLStr("lbl_ship_modification_rebuild_txt2_p2", userId) %><BR/>
        <%= ML.getMLStr("lbl_ship_modification_costgain", userId) %><BR/><BR/></FONT>
        <TABLE border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_costgain_short", userId) %></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_resource", userId) %></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_quantity", userId) %></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (suc.getRess(r.getId()) < 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(),userId)+"</TD><TD><SPAN style=\"color:#00FF00\" id=\""+r.getName()+"\">" + FormatUtilities.getFormattedDecimal(Math.abs(suc.getRess(r.getId())),0) +"</SPAN></TD></TR>");
        } else if (suc.getRess(r.getId()) > 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(),userId)+"</TD><TD><SPAN style=\"color:#FF0000\" id=\""+r.getName()+"\">" + FormatUtilities.getFormattedDecimal(suc.getRess(r.getId()),0) +"</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />
        <FORM method='post' action='main.jsp?page=confirm/shipModification&bufId=<%= pb.getId() %>&c=1' name='confirm' %>
            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%
} else if (pb instanceof RepairShipBuffer) {
        RepairShipBuffer rsb = (RepairShipBuffer)pb;

        ShipDesignExt sde = new ShipDesignExt(rsb.getDesignId());
        ShipDesign sd = (ShipDesign)sde.getBase();

        ShipRepairCost src = new ShipRepairCost(sde);
        RessourcesEntry re = src.getTotalCosts(rsb.getCount());

        String name = sd.getName();
%>
        <BR><FONT size="-1"><%= ML.getMLStr("lbl_ship_modification_repair_txt3_p1", userId) %>&nbsp;<FONT color=yellow><B><%= name %></B></FONT>&nbsp;
        <%= ML.getMLStr("lbl_ship_modification_repair_txt3_p2", userId) %><BR/><BR/>
            <TABLE style="font-size:12px"><TR><TD width="120"><B><%= ML.getMLStr("lbl_ship_modification_damagelevel", userId) %></B></TD><TD align="center" width="60"><B><%= ML.getMLStr("lbl_ship_modification_count", userId) %></B></TD></TR>
<%
        for (Map.Entry<EDamageLevel,Integer> entry : rsb.getCount().entrySet()) {
%>
            <TR><TD><%= entry.getKey().toString()%></TD><TD align="center"><%= entry.getValue()%></TD></TR>
<%
        }
%>
        </TABLE><BR/>
                <%= ML.getMLStr("lbl_ship_modification_repair_txt2_p1", userId) %>&nbsp;<FONT color=yellow><B><%= FormatUtilities.getFormattedNumber(re.getRess(Ressource.CREDITS)) %></B></FONT>
        <%= ML.getMLStr("lbl_ship_modification_repair_txt2_p2", userId) %><BR/>
        <%= ML.getMLStr("lbl_ship_modification_cost", userId) %><BR/><BR/></FONT>
        <TABLE border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_cost_short", userId) %></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_resource", userId) %></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_quantity", userId) %></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (re.getRess(r.getId()) > 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(),userId)+"</TD><TD><SPAN style=\"color:#FF0000\" id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(re.getRess(r.getId())) +"</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />
        <FORM method='post' action='main.jsp?page=confirm/shipModification&bufId=<%= pb.getId() %>&c=1' name='confirm' %>
            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%
} else if (pb instanceof UpgradeStarbaseBuffer) {
        UpgradeStarbaseBuffer usb = (UpgradeStarbaseBuffer)pb;

        ShipDesignExt sde = new ShipDesignExt(usb.getDesignId());
        ShipDesign sd = (ShipDesign)sde.getBase();
        ShipDesignExt sdeNew = new ShipDesignExt(usb.getToDesignId());
        ShipDesign sdNew = (ShipDesign)sdeNew.getBase();

        ShipUpgradeCost suc = sde.getShipUpgradeCost(sdeNew, usb.getCount());

        String oldName = sd.getName();
        String newName = sdNew.getName();
%>
        <FONT size="-1"><%= ML.getMLStr("lbl_ship_modification_rebuild_ss_txt", userId) %><BR/>
                <%= oldName %> --&gt; <%= newName %> (<%= usb.getCount() %>x)<BR/><BR/>
                <%= ML.getMLStr("lbl_ship_modification_rebuild_txt2_p1", userId) %>&nbsp;<FONT color=yellow><B><%= FormatUtilities.getFormattedNumber(suc.getRess(Ressource.CREDITS)) %></B></FONT>
        <%= ML.getMLStr("lbl_ship_modification_rebuild_txt2_p2", userId) %><BR/>
        <%= ML.getMLStr("lbl_ship_modification_costgain", userId) %><BR/><BR/></FONT>
        <TABLE border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_costgain_short", userId) %></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_resource", userId) %></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_quantity", userId) %></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (suc.getRess(r.getId()) < 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(),userId)+"</TD><TD><SPAN style=\"color:#00FF00\" id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(Math.abs(suc.getRess(r.getId()))) +"</SPAN></TD></TR>");
        } else if (suc.getRess(r.getId()) > 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(),userId)+"</TD><TD><SPAN style=\"color:#FF0000\" id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(suc.getRess(r.getId())) +"</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />
        <FORM method='post' action='main.jsp?page=confirm/shipModification&bufId=<%= pb.getId() %>&c=1' name='confirm' %>
            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%
} else if (pb instanceof StarbaseScrapBuffer) {
        StarbaseScrapBuffer ssb = (StarbaseScrapBuffer)pb;

        ShipDesignExt sde = new ShipDesignExt(ssb.getDesignId());
        ShipDesign sd = (ShipDesign)sde.getBase();

        ShipScrapCost ssc = sde.getShipScrapCost(ssb.getCount());
%>
        <FONT size="-1"><%= ML.getMLStr("lbl_ship_modification_scrap_ss_txt", userId) %><BR />
        <%= ssb.getCount() %>x <%= sd.getName() %><BR /><BR />
        <%= ML.getMLStr("lbl_ship_modification_scrap_txt2_p1", userId) %>&nbsp;<FONT color=yellow><B><%= FormatUtilities.getFormattedNumber(ssc.getRess(Ressource.CREDITS) * ssb.getCount()) %></B></FONT>
        <%= ML.getMLStr("lbl_ship_modification_scrap_txt2_p2", userId) %><BR/>
        <%= ML.getMLStr("lbl_ship_modification_scrap_gain", userId) %><BR/><BR/></FONT>
        <TABLE border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_scrap_gain_short", userId) %></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_resource", userId) %></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("lbl_ship_modification_quantity", userId) %></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (ssc.getRess(r.getId()) > 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(),userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(ssc.getRess(r.getId()) * ssb.getCount()) + "</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />
        <FORM method='post' action='main.jsp?page=confirm/shipModification&bufId=<%= ssb.getId() %>&c=1' name='confirm' %>
            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%
}
%>