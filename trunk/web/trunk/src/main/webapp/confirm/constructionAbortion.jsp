<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.construction.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.model.Ressource" %>
<%@page import="at.darkdestiny.core.service.RessourceService" %>
<%@page import="at.darkdestiny.core.service.ConstructionService" %>
<%@page import="at.darkdestiny.core.model.Construction" %>
<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.framework.access.DbConnect" %>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));

int bufferId = Integer.parseInt(request.getParameter("qi"));
ConstructionOrderAbortBuffer coab = (ConstructionOrderAbortBuffer)BufferHandling.getBuffer(userId, bufferId);
int count = coab.getCount();

int execute = 0;
if (request.getParameter("execute") != null) {
    execute = Integer.parseInt(request.getParameter("execute"));
}
if(execute == 1) {
        ArrayList<String> errors = ConstructionService.abortConstruction(coab.getOrderId(), userId, count);
        session.setAttribute("actPage", "new/construction");
        try {
            request.removeAttribute("timeFinished");
        } catch(Exception e) {
            DebugBuffer.warning("Couldnt remove timeFinished");
        }

        if (errors.size() > 0) {
            String msg = null;
            for (String err : errors) {
                if (msg != null) msg += "<BR>";
                msg += err;
            }
        %>
        <jsp:forward page="/main.jsp?page=showerror&msg=<%= msg %>"/>
 <%
        } else {
        %>
        <jsp:forward page="/main.jsp?page=new/construction"/>
 <%
        }
 }
        AbortConstructionResult acr = ConstructionService.getAbortRefund(coab.getOrderId());
        ConstructionAbortRefund car = acr.getConsAbortRefund();
%>

      <FORM method="post" action='main.jsp?page=confirm/constructionAbortion&qi=<%= coab.getId() %>&execute=1' name="confirm">
          <INPUT type="hidden" name="timeFinished" value="<%= coab.getOrderId() %>">
          <INPUT type="hidden" name="destroyCount" value="<%= count %>">
    <FONT size="-1"><%= ML.getMLStr("construction_msg_abortionmsg1", userId)%><BR />
            <%= count %>x <%= ML.getMLStr(acr.getCons().getName(), userId) %><BR /><BR />
             <BR><%= ML.getMLStr("construction_msg_abortionmsg2", userId)%><BR/><BR/></FONT>
        <TABLE border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcerefund", userId)%></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
            </TR>
<%
    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (car.getRess(r.getId()) > 0) {
            if(r.getId() == Ressource.CREDITS)continue;
            out.write("<TR><TD>"+ML.getMLStr(r.getName(), userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(car.getRess(r.getId())*count) + "</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />

            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%

%>
