<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.construction.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.model.Ressource" %>
<%@page import="at.darkdestiny.core.service.RessourceService" %>
<%@page import="at.darkdestiny.core.service.ConstructionService" %>
<%@page import="at.darkdestiny.core.model.Construction" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.darkdestiny.util.DebugBuffer" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%@page import="at.darkdestiny.framework.access.DbConnect" %>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int bufferId = Integer.parseInt(request.getParameter("qi"));

ConstructionScrapBuffer scb = (ConstructionScrapBuffer)BufferHandling.getBuffer(userId, bufferId);

int count = scb.getCount();
int cId = scb.getConstructionId();

int execute = 0;
if (request.getParameter("execute") != null) {
    execute = Integer.parseInt(request.getParameter("execute"));
}
if (execute == 1) {
        ConstructionService.deconstructConstruction(scb.getPlanetId(), scb.getConstructionId(), userId, scb.getCount());
        scb.invalidate();
        %>
        <jsp:forward page="/main.jsp?page=new/construction"/>
 <%
 }

        ScrapConstructionResult scr = ConstructionService.getScrapResult(cId, scb.getPlanetId());
        ConstructionScrapCost css = scr.getConsScrapCost();
%>
    
<FORM method="post" action='main.jsp?page=confirm/constructionDestruction&qi=<%= bufferId %>&execute=1' name="confirm">
          <INPUT type="hidden" name="constructionId" value="<%= scb.getConstructionId() %>">
          <INPUT type="hidden" name="destroyCount" value="<%= scb.getCount() %>">
    <FONT size="-1"><%= ML.getMLStr("construction_msg_confirmdestruction2", userId)%><BR/>
            <%= count %>x <%= ML.getMLStr(scr.getCons().getName(), userId) %><BR /><BR />
            <%= ML.getMLStr("construction_msg_destructionmsg1", userId) %> <FONT color=yellow><B><%= FormatUtilities.getFormattedNumber(css.getRess(Ressource.CREDITS)*count) %></B></FONT>
            <%= ML.getMLStr("construction_msg_deconstructionmsg2", userId)%> <FONT color=yellow><B><%= FormatUtilities.getFormattedNumber((int) Math.round(scr.getCons().getBuildtime() * count * SetupValues.BUILDTIME_MULTI)) %></B></FONT> <%= ML.getMLStr("construction_msg_deconstructionmsg3", userId)%>
        <BR /><%= ML.getMLStr("construction_msg_deconstructionmsg4", userId)%><BR/><BR/></FONT>
        <TABLE border="0" width="80%" style="font-size:13px">
            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcerefund", userId)%></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
            </TR>
<%

    for (Ressource r : RessourceService.getAllStorableRessources()) {
        if (css.getRess(r.getId()) > 0) {
            if(r.getId() == Ressource.CREDITS)continue;
            out.write("<TR><TD>"+ML.getMLStr(r.getName(), userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber((long)css.getRess(r.getId())*count) + "</SPAN></TD></TR>");
        }
    }
%>
        </TABLE><BR />

            <INPUT type="submit" name="Ok" value="Ok">
        </FORM>
<%

%>
