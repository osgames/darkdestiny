<%-- 
    Document   : quest
    Created on : Apr 2, 2012, 12:49:46 PM
    Author     : Admin
--%>

<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.Locale"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
    int slashCounter = 0;

    StringBuffer requestUrl = request.getRequestURL();

    Locale locale = ML.getLocale(1);
    Logger.getLogger().write("Locale : "  + locale.toString());
    String basePath = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);
%>
<html>
    <head>
        <META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <style type="text/css"></style>      
        <link rel="stylesheet" href="../main.css.jsp" type="text/css">
        <script src="../java/main.js" type="text/javascript"></script>
        <script src="../java/format.js" type="text/javascript"></script>
        <script src="../java/translate.js" type="text/javascript"></script>
        <script src="../java/json.js" type="text/javascript"></script>
        <script src="../java/quest.js" type="text/javascript"></script>
        <script src="../java/ajax.js" type="text/javascript"></script>
        <title>JSP Page</title>
    </head>
    <body style="background-color: black; color:white;">
    <CENTER><h1>Questbuilder gammay</h1></CENTER>
    <%


        int userId = 0;

        int type = 1;
        final int TYPE_SHOWALL = 1;
        final int TYPE_SHOWQUEST = 2;
        final int TYPE_SHOWQUESTSTEP = 5;
        final int TYPE_SHOWQUESTSTEPRELATION = 4;
        final int TYPE_CREATEQUESTSTEP = 6;
        final int TYPE_CREATEQUEST = 3;
        final int TYPE_CREATESTEPRELATION = 7;
        final int TYPE_CREATEREQUIREMENT = 8;
        final int TYPE_CREATEQUESTEFFECT = 9;

        if (request.getParameter("type") != null) {
            type = Integer.parseInt(request.getParameter("type"));
        }

        int process = 0;
        final int PROCESS_CREATEQUEST = 1;
        final int PROCESS_CREATEQUESTSTEP = 2;
        final int PROCESS_CREATEQUESTREQUIREMENT = 3;
        final int PROCESS_CREATEQUESTEFFECT = 4;
        final int PROCESS_DELETEQUESTSTEP = 5;
        final int PROCESS_CREATEQUESTRELATION = 6;
        
        
        if (request.getParameter("process") != null) {
            process = Integer.parseInt(request.getParameter("process"));
        }
        if (process == PROCESS_CREATEQUEST) {
            QuestService.createQuest(userId, request.getParameterMap());
        }
        if (process == PROCESS_CREATEQUESTSTEP) {
            QuestService.createQuestStep(userId, request.getParameterMap());
            type = TYPE_SHOWQUEST;
        }
        if (process == PROCESS_CREATEQUESTREQUIREMENT) {
            QuestService.createQuestRequirement(userId, request.getParameterMap());
            type = TYPE_CREATESTEPRELATION;
        }
        if (process == PROCESS_CREATEQUESTEFFECT) {
            QuestService.createQuestResult(userId, request.getParameterMap());
            type = TYPE_CREATESTEPRELATION;
        }
        if (process == PROCESS_DELETEQUESTSTEP) {
            QuestService.deleteQuestStep(userId, request.getParameterMap());
            type = TYPE_SHOWQUEST;
        }
        if (process == PROCESS_CREATEQUESTRELATION) {
            QuestService.createQuestRelation(userId, request.getParameterMap());
            type = TYPE_SHOWQUEST;
        }

    %>
    <TABLE alling="center" class="blue2" id="quests"></TABLE>
</body>
<%
    switch (type) {
        case (TYPE_SHOWALL):
%>

<TABLE align="center">
    <TR>
        <TD><A href="questBuilder.jsp?type=<%= TYPE_CREATEQUEST%>" >Neue Quest</A></TD>
    </TR>
</TABLE>
<TABLE align="center" class="bluetrans">
    <TR class="blue2">
        <TD>�d</TD>
        <TD>Name</TD>
        <TD width="250px">Description</TD>
        <TD>Steps</TD>            
        <TD>Author</TD>
        <TD>Bewilligt</TD>
        <TD>Aktionen</TD>
    </TR>
    <% for (Quest q : QuestService.getQuests()) {%>
    <TR>
        <TD><%= q.getId()%></TD>
        <TD><B><%= ML.getQuestMLStr(q.getName(), locale)%></B></TD>
        <TD><%= ML.getQuestMLStr(q.getDescription(), locale)%></TD>
        <TD><%= QuestService.getQuestSteps(q.getId()).size() %></TD>
        <TD><%= q.getAuthor()%></TD>
        <TD><%= q.getAccepted()%></TD>
        <TD><A href="questBuilder.jsp?type=<%= TYPE_SHOWQUEST%>&questId=<%= q.getId()%>">Anzeigen</A></TD>
    </TR>
    <% }%>

</TABLE>
<%
        break;

    case (TYPE_SHOWQUEST):
               {
        int questId = 0;
        if (request.getParameter("questId") != null) {
            questId = Integer.parseInt(request.getParameter("questId"));
        }


        Quest quest = QuestService.getQuest(questId);
        ArrayList<QuestStep> questSteps = QuestService.getQuestSteps(questId);
        
%>
<SCRIPT type="text/javascript">
     var questSteps = new Array(<%= questSteps.size()%>);
     <% for(int i = 0; i < questSteps.size(); i++){
         QuestStep qs = questSteps.get(i);
         ArrayList<QuestStepRelation> qsrs = QuestService.findQuestRelations(qs.getId());
         ArrayList<QuestStepRelation> qsrsPrev = QuestService.findQuestRelationsByPrevId(qs.getId());
         %>
             questSteps[<%= i %>] = new Array(4);
             questSteps[<%= i %>][0] = '<%= ML.getQuestMLStr(qs.getName(), locale) %>';
             questSteps[<%= i %>][3] = <%= qs.getId() %>;
             questSteps[<%= i %>][1] = new Array(<%= qsrs.size() %>);
             questSteps[<%= i %>][2] = new Array(<%= qsrsPrev.size() %>);
             <% for(int j = 0; j < qsrs.size(); j++){ 
             QuestStepRelation qsr = qsrs.get(j);
                %>
                    <% if(qsr.getPreviousStepId() != null && qsr.getPreviousStepId() > 0){ %>
                    questSteps[<%= i %>][1][<%= j %>] = new Array(2);
                    questSteps[<%= i %>][1][<%= j %>][0] = '<%= ML.getQuestMLStr(QuestService.getQuestStep(qsr.getPreviousStepId()).getName(), locale) %>';
                    questSteps[<%= i %>][1][<%= j %>][1] = <%= qsr.getId() %>;
                    <% } %>
                 <% } %>
             <% for(int j = 0; j < qsrsPrev.size(); j++){ 
             QuestStepRelation qsr = qsrsPrev.get(j);
                %>
                    questSteps[<%= i %>][2][<%= j %>] = new Array(2);
                    questSteps[<%= i %>][2][<%= j %>][0] = '<%= ML.getQuestMLStr(QuestService.getQuestStep(qsr.getStepId()).getName(), locale) %>';
                    questSteps[<%= i %>][2][<%= j %>][1] = <%= qsr.getId() %>;
            
                 <% } %>
         <% } %>
</SCRIPT>
<%

%>
<TABLE align="center" class="bluetrans">
    <TR class="blue2">
        <TD>Name
        </TD>
        <TD>
            <%= ML.getQuestMLStr(quest.getName(), locale)%>
        </TD>
    </TR>
    <TR>

        <TD>Beschreibung
        </TD>
        <TD>
            <%= ML.getQuestMLStr(quest.getDescription(), locale)%>
        </TD>
    </TR>
</TABLE>
<TABLE align="center" class="bluetrans">
    <TR><TD><TABLE class="bluetrans">
                <TR class="blue2">
                    <TD colspan="3">
                        Hinzugef�gte Questschritte
                    </TD>
                </TR>
                <TR>
                    <TD colspan="4" width="100%">
                        <SELECT style="width:100%;" id="queststeps" multiple="true" size="14" onclick="updateQuestStepDetails(document.getElementById('queststeps').options[document.getElementById('queststeps').selectedIndex].value);">
                            <% for (QuestStep qs : questSteps) {%>
                            <OPTION value="<%= qs.getId()%>"><%= ML.getQuestMLStr(qs.getName(), locale)%></OPTION>
                            <% }%>
                        </SELECT>
                    </TD>
                </TR>
                <TR>
                    <TD>
                        <INPUT onclick="window.location.href = 'questBuilder.jsp?type=<%= TYPE_SHOWQUESTSTEP%>&questStepId=' + document.getElementById('queststeps').options[document.getElementById('queststeps').selectedIndex].value" type="button" value="Edit" />
                    </TD>
                    <TD>
                        <INPUT onclick="window.location.href = 'questBuilder.jsp?type=<%= TYPE_CREATESTEPRELATION%>&questId=<%= questId%>&questStepId=' + document.getElementById('queststeps').options[document.getElementById('queststeps').selectedIndex].value" type="button" value="Verlinke QuestSchritt" />
                    </TD>
                    <TD>
                        <INPUT onclick="window.location.href = 'questBuilder.jsp?type=<%= TYPE_CREATEQUESTSTEP%>&questId=<%= questId%>'" type="button" value="Hinzuf�gen" />
                    </TD>
                    <TD>
                        <INPUT onclick="window.location.href = 'questBuilder.jsp?process=<%= PROCESS_DELETEQUESTSTEP%>&questId=<%= questId%>&questStepId=' + document.getElementById('queststeps').options[document.getElementById('queststeps').selectedIndex].value" type="button" value="Delete" />
                    </TD>
                </TR>
            </TABLE>
        </TD>
        <TD valign="top">
            <TABLE width="500px" id="queststepdetails">
            </TABLE>
        </TD>
    </TR>
</TABLE>
<%


   }
        break;

    case (TYPE_CREATEQUEST):
               {
%>
<FORM action="questBuilder.jsp?process=1" method="post">
    <TABLE align="center" class="bluetrans">
        <TR class="blue2">
            <TD>Author
            </TD>
            <TD>
                <INPUT name="author" type="textfield" class="dark"/>

            </TD>
        </TR>
        <TR class="blue2">
            <TD>
                Typ
            </TD>
            <TD>
                <TABLE class="bluetrans">
                    <% for (EQuestType qt : EQuestType.values()) {%>
                    <TR>
                        <TD>                       
                            <INPUT id="typeSelect_<%= qt.name()%>" name="questType_<%= qt.name()%>" type="radio" value="<%= qt.name()%>"><%= qt.name()%></INPUT>
                        </TD>
                    </TR>
                    <% }%>
                </TABLE>
            </TD>
        </TR>
        <TR class="blue2">
            <TD>Name
            </TD>
            <TD>
                <TABLE class="bluetrans">
                    <% for (Language l : Service.languageDAO.findAll()) {
                            if (l.getMasterLanguageId() != l.getId()) {
                                continue;
                            }
                    %>
                    <TR>
                        <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                        </TD>
                    </TR>
                    <TR>
                        <TD><INPUT name="name_<%= l.getId()%>" type="textfield" class="dark"/>
                        </TD>
                    </TR>
                    <% }%>
                </TABLE>
            </TD>
        </TR>
        <TR>

            <TD class="blue2">Beschreibung
            </TD>
            <TD>
                <TABLE class="bluetrans">
                    <% for (Language l : Service.languageDAO.findAll()) {
                            if (l.getMasterLanguageId() != l.getId()) {
                                continue;
                            }
                    %>
                    <TR>
                        <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                        </TD>
                    </TR>
                    <TR>
                        <TD><TEXTAREA name="description_<%= l.getId()%>" rows="10" cols="40" class="dark" ></TEXTAREA>
                        </TD>
                    </TR>
                    <% }%>
                </TABLE>
            </TD>
        </TR>
        <TR>
            <TD colspan="2" align="center"><INPUT type="submit" value="Erstellen" /></TD>
        </TR>
    </TABLE>
</FORM>
<%

    }
        break;

    case (TYPE_SHOWQUESTSTEP):
               {
        int questStepId = 0;
        if (request.getParameter("questStepId") != null) {
            questStepId = Integer.parseInt(request.getParameter("questStepId"));
        }
       
        QuestStep qs = QuestService.getQuestStep(questStepId);
%>

<TABLE align="center" class="bluetrans">
    <TR>
        <TD class="blue2">Id
        </TD>
        <TD>
           <%= qs.getId() %>
        </TD>
    </TR>
    <TR>
        <TD class="blue2">Name
        </TD>
        <TD>
             <%= ML.getQuestMLStr(qs.getName(), locale) %>
        </TD>
    </TR>
    <TR>
        <TD class="blue2">User-Nachricht
        </TD>
        <TD>
             <%= ML.getQuestMLStr(qs.getMessage(), locale) %>
        </TD>
    </TR>
</TABLE>

<%
    }
    
        break;

    case (TYPE_SHOWQUESTSTEPRELATION):
               {
        int questStepRelationId = 0;
        if (request.getParameter("questStepRelationId") != null) {
            questStepRelationId = Integer.parseInt(request.getParameter("questStepRelationId"));
        }
%>

<TABLE align="center" class="bluetrans">
    <TR>
        <TD class="blue2">Id
        </TD>
        <TD>
            <%= questStepRelationId%>
        </TD>
    </TR>
    <TR>
        <TD class="blue2">Name
        </TD>
        <TD>
            Druuf-Invasion
        </TD>
    </TR>
    <TR>
        <TD class="blue2">Typ
        </TD>
        <TD>
            <TABLE>
                <% for (EQuestStepRelationType qsrt : EQuestStepRelationType.values()) {
                        String popUp = "Andere Vorbedingungen";
                        if (qsrt.equals(EQuestStepRelationType.USER_DRIVEN)) {
                            popUp = "Der User darf diesen Schritt<BR> unter den anderen ausw�hlen";
                        } else if (qsrt.equals(EQuestStepRelationType.RANDOM_DRIVEN)) {
                            popUp = "Das Ereigniss tritt zu der angegebenen Teilwahrscheinlichkeit<BR> unter den anderen m�glichen Folgeschritten ein";
                        }
                %>
                <TR>
                    <TD>
                        <INPUT id="typeSelect" onclick="updateTypeSelect('<%= qsrt.name()%>');" name="type" type="radio" value="<%= qsrt.name()%>"><%= qsrt.name()%></INPUT><IMG src="../pic/infoT.png" onmouseover="doTooltip(event,'<%= popUp%>');" onmouseout="hideTip();" /></TD>
                </TR>
                <% }%>
            </TABLE>
        </TD>
        <TD id="selectTextField">
        </TD>
    </TR>
    <TR>

        <TD class="blue2">Vorbedingungen
        </TD>
        <TD  colspan="2">
            <TABLE class="bluetrans">
                <TR><TD>
                        <SELECT multiple="true" size="12">

                            <OPTION>Questschritt abgeschlossen
                            </OPTION>
                            <OPTION>Quest abgeschlossen
                            </OPTION>
                            <OPTION>Minimum-Ticks
                            </OPTION>
                            <OPTION>Forschungsanzahl
                            </OPTION>


                            <OPTION>Bev�lkerungsanzahl (Hauptplanet)
                            </OPTION>


                            <OPTION>Bev�lkerungsanzahl (Hauptsystem)
                            </OPTION>


                            <OPTION>Bev�lkerungsanzahl (System)
                            </OPTION>


                            <OPTION>Bev�lkerungsanzahl (Imperial)
                            </OPTION>

                            <OPTION>Ressourcenzahlung
                            </OPTION>

                            <OPTION>Ereignis - Scan
                            </OPTION>


                            <OPTION>Ereignis - Weltraumkampf
                            </OPTION>


                            <OPTION>Ereignis - Weltraumkampf (Sieg)
                            </OPTION>


                            <OPTION>Ereignis - Weltraumkampf (Verlust)
                            </OPTION>


                            <OPTION>Ereignis - Steuererh�hung
                            </OPTION>


                            <OPTION>Ereignis - Bodenkampf
                            </OPTION>


                            <OPTION>Ereignis - Bodenkampf(Verlust)
                            </OPTION>


                            <OPTION>Ereignis - Bodenkampf(Gewinn)
                            </OPTION>
                        </SELECT>
                    </TD>
                    <TD valign="top">
                        <TABLE id="requirements">
                            <TR>
                                <TD class="blue2">
                                    Questschritt abgeschlossen
                                </TD>
                                <TD>2</TD>
                            </TR>
                            <TR>
                                <TD class="blue2">
                                    Forschungsanzahl
                                </TD>
                                <TD> > 50</TD>
                            </TR>
                            <TR>
                                <TD class="blue2">
                                    Ressourcenzahlung
                                </TD>
                                <TD> 5.000 Howalgonium</TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
    <TR>

        <TD class="blue2">Effekt
        </TD>
        <TD colspan="2">
            <TABLE class="bluetrans">
                <TR><TD>
                        <SELECT multiple="true" size="12">

                            <OPTION>Neue Forschung
                            </OPTION>
                            <OPTION>Quest abgeschlossen
                            </OPTION>
                            <OPTION>Entwicklungspunkte (Anzahl)
                            </OPTION>
                            <OPTION>Credits
                            </OPTION>
                            <OPTION>Ressourcen
                            </OPTION>
                            <OPTION>Produktionsbonus (Imperium)
                            </OPTION>
                            <OPTION>Produktionsbonus (Hauptplanet
                            </OPTION>
                        </SELECT>
                    </TD>

                    <TD valign="top">
                        <TABLE id="effects">
                            <TR>
                                <TD class="blue2">
                                    Ressourcengeschenk
                                </TD>
                                <TD>2.000.000 Eisen</TD>
                            </TR>
                            <TR>
                                <TD class="blue2">
                                    Ressourcengeschenk
                                </TD>
                                <TD>10.000 Howalgonium</TD>
                            </TR>
                            <TR>
                                <TD class="blue2">
                                    Entwicklungspunkte
                                </TD>
                                <TD> 5.000</TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
</TABLE>   
<%
   }
        break;
    case (TYPE_CREATESTEPRELATION):
               {
        int questStepId = 0;
        if (request.getParameter("questStepId") != null) {
            questStepId = Integer.parseInt(request.getParameter("questStepId"));
        }
        int questId = 0;
        if (request.getParameter("questId") != null) {
            questId = Integer.parseInt(request.getParameter("questId"));
        }
        QuestStep questStep = QuestService.getQuestStep(questStepId);
%>
<FORM action="questBuilder.jsp" method="post">
    <INPUT type="hidden" name="questStepId" value="<%= questStepId %>"/>
    <INPUT type="hidden" name="questId" value="<%= questId %>"/>
    <INPUT type="hidden" name="process" value="<%= PROCESS_CREATEQUESTRELATION %>"/>
    <TABLE class="bluetrans" align="center">
        <TR>
            <TD>Eine Questschritt Verlinkung ist ein Verlinkung zwischen dem hier ausgew�hlten Questschritt<BR>
                mit einem zuvor abgeschlossenen Questschritt. Zus�tzlich k�nnen mehrere andere Bedingungen ausgew�hlt werden.<BR>
                <B>ACHTUNG: Es kann f�r eine Verlinkung immer nur 1! anderer Vorschritt gew�hlt werden<BR>
                    Hat eine Verlinkung keinen Vorschritt wird sie als Startschritt angesehen</TD>
                    </TR>
                    </TABLE>
                    <TABLE align="center" class="bluetrans">
                        <TR>
                            <TD class="blue2">Id
                            </TD>
                            <TD>
                                <%= questStepId%>
                            </TD>
                        </TR>
                        <TR>
                            <TD class="blue2">Name
                            </TD>
                            <TD>
                                <%= ML.getQuestMLStr(questStep.getName(), locale)%>
                            </TD>
                        </TR>
                        <TR>
                            <TD class="blue2">Nachricht
                            </TD>
                            <TD>
                                <%= ML.getQuestMLStr(questStep.getMessage(), locale)%>
                            </TD>
                        </TR>
                        <TR>
                            <TD class="blue2">Typ
                            </TD>
                            <TD>
                                <TABLE>
                                    <% for (EQuestStepRelationType qsrt : EQuestStepRelationType.values()) {
                                            String popUp = "Andere Vorbedingungen";
                                            String selected = "Selected";
                                            if (qsrt.equals(EQuestStepRelationType.USER_DRIVEN)) {
                                                selected = "";
                                                popUp = "Der User darf diesen Schritt<BR> unter den anderen ausw�hlen";
                                            } else if (qsrt.equals(EQuestStepRelationType.RANDOM_DRIVEN)) {
                                                selected = "";
                                                popUp = "Das Ereigniss tritt zu der angegebenen Teilwahrscheinlichkeit<BR> unter den anderen m�glichen Folgeschritten ein";
                                            }
                                    %>
                                    <TR>
                                        <TD>
                                            <INPUT id="typeSelect" onclick="updateTypeSelect('<%= qsrt.name()%>');" name="questType" type="radio" value="<%= qsrt.name() %>"><%= qsrt.name()%></INPUT><IMG src="../pic/infoT.png" onmouseover="doTooltip(event,'<%= popUp%>');" onmouseout="hideTip();" />
                                        </TD>
                                    </TR>
                                    <% }%>
                                </TABLE>
                            </TD>
                            <TD id="selectTextField">
                            </TD>
                        </TR>
                        <TR>
                            <TD class="blue2">Vorschritt
                            </TD>
                            <TD>
                                <SELECT name="previousStepId">
                                    <OPTION value="0">-StartSchritt-</OPTION>
                                    <% for(QuestStep qs : QuestService.getQuestSteps(questId)){ %>
                                    <OPTION value="<%= qs.getId() %>"><%= ML.getQuestMLStr(qs.getName(), locale) %></OPTION>
                                    <%  } %>
                                </SELECT>
                            </TD>
                        </TR>
                        <TR>

                            <TD class="blue2">Vorbedingungen
                            </TD>
                            <TD  colspan="2">
                                <TABLE class="bluetrans">
                                    <TR><TD>
                                            <SELECT id="questrequirements" multiple="true" size="12">
                                                <% for (QuestRequirement qr : QuestService.findQuestRequirements()) {
                                                        String working = "";
                                                        if (!qr.getWorking()) {
                                                            working = "[notWorkingYet]";
                                                        }
                                                %>
                                                <OPTION onmouseover="doTooltip(event,'<%= ML.getQuestMLStr(qr.getDescription(), locale)%>')" onmouseout="hideTip()" value="<%= qr.getId()%>" <% if (!qr.getWorking()) {%>Disabled <% }%>><%= ML.getQuestMLStr(qr.getName(), locale)%><%= working%>
                                                </OPTION>
                                                <% }%>

                                            </SELECT>
                                        </TD>
                                        <TD valign="top">
                                            <TABLE>
                                                <TBODY name="requirements" id="requirements"></TBODY>
                                            </TABLE>
                                        </TD>
                                    </TR>
                                </TABLE>
                            </TD>
                            <TD>
                                <TABLE>
                                    <TR><TD><INPUT onclick="addQuestRequirement(document.getElementById('questrequirements').options[document.getElementById('questrequirements').selectedIndex].value, document.getElementById('questrequirements').options[document.getElementById('questrequirements').selectedIndex].innerHTML);" type="button" value="Hinzuf�gen" /></TD></TR>
                                    <TR><TD><INPUT onclick="window.location.href = 'questBuilder.jsp?type=<%= TYPE_CREATEREQUIREMENT%>&questStepId=<%= questStepId%>'" type="button" value="Neue Bedingung" /></TD></TR>
                                </TABLE>
                            </TD>
                        </TR>
                        <TR>

                            <TD class="blue2">Effekt
                            </TD>
                            <TD colspan="2">
                                <TABLE class="bluetrans">
                                    <TR><TD>
                                            <SELECT id="questresult" multiple="true" size="12">
                                                <% for (QuestResult qr : QuestService.findQuestResults()) {
                                                        String working = "";
                                                        if (!qr.getWorking()) {
                                                            working = "[notWorkingYet]";
                                                        }
                                                %>
                                                <OPTION onmouseover="doTooltip(event,'<%= ML.getQuestMLStr(qr.getDescription(), locale)%>')" onmouseout="hideTip()"  value="<%= qr.getId() %>" <% if (!qr.getWorking()) {%>Disabled <% }%>><%= ML.getQuestMLStr(qr.getName(), locale)%><%= working%>
                                                </OPTION>
                                                <% }%>
                                            </SELECT>
                                        </TD>

                                        <TD valign="top">
                                            <TABLE name="effects" id="effects">

                                            </TABLE>
                                        </TD>
                                    </TR>
                                </TABLE>
                            </TD>
                            <TD>
                                <TABLE>
                                    <TR><TD><INPUT onclick="addQuestResult(document.getElementById('questresult').options[document.getElementById('questresult').selectedIndex].value, document.getElementById('questresult').options[document.getElementById('questresult').selectedIndex].innerHTML);" type="button" value="Hinzuf�gen" /></TD></TR>
                                    <TR><TD><INPUT onclick="window.location.href = 'questBuilder.jsp?type=<%= TYPE_CREATEQUESTEFFECT%>&questStepId=<%= questStepId%>'" type="button" value="Neuer Effekt" /></TD></TR>
                                </TABLE>
                            </TD>
                        </TR>
                        <TR>
                            <TD colspan="2" align="center">
                                <INPUT type="submit" value="Erstellen" />
                            </TD>
                        </TR>
                    </TABLE>

                    </FORM>
                    <%
                                       }
                            break;

                        case (TYPE_CREATEQUESTSTEP):
                        {
                            int questId = 0;
                            if (request.getParameter("questId") != null) {
                                questId = Integer.parseInt(request.getParameter("questId"));
                            }
                    %>
                    <TABLE class="bluetrans" align="center">
                        <TR>
                            <TD>Ein Questschritt beinhaltet einen <B>Namen</B> sowie eine <B>Nachricht</B> die der User bekommt<BR>
                                Die <B>Beschreibung</B> dient den Programmieren den gr��eren Zusammenhang des Schrittes im Quest zu erkennen
                                <BR>
                                Solche Schritte werden danach mit einer Relation Vor oder Hinter andere Questschritte geh�ngt. Ein Questschritt
                                kann in einem Quest �fters auftreten</TD>
                        </TR>
                    </TABLE>
                    <FORM action="questBuilder.jsp?process=<%= PROCESS_CREATEQUESTSTEP%>" method="post">
                        <INPUT type="hidden" name="questId" value=<%= questId%> />
                        <TABLE align="center" class="bluetrans">
                            <TR class="blue2">
                                <TD>Name
                                </TD>
                                <TD>
                                    <TABLE class="bluetrans">
                                        <% for (Language l : Service.languageDAO.findAll()) {
                                                if (l.getMasterLanguageId() != l.getId()) {
                                                    continue;
                                                }
                                        %>
                                        <TR>
                                            <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD><INPUT name="name_<%= l.getId()%>" type="textfield" class="dark"/>
                                            </TD>
                                        </TR>
                                        <% }%>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD class="blue2">Nachricht
                                </TD>
                                <TD>
                                    <TABLE class="bluetrans">
                                        <% for (Language l : Service.languageDAO.findAll()) {
                                                if (l.getMasterLanguageId() != l.getId()) {
                                                    continue;
                                                }
                                        %>
                                        <TR>
                                            <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD><TEXTAREA name="message_<%= l.getId()%>" rows="10" cols="40" class="dark" ></TEXTAREA>
                                            </TD>
                                        </TR>
                                        <% }%>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD class="blue2">Beschreibung
                                </TD>
                                <TD>
                                    <TABLE class="bluetrans">
                                        <% for (Language l : Service.languageDAO.findAll()) {
                                                if (l.getMasterLanguageId() != l.getId()) {
                                                    continue;
                                                }
                                        %>
                                        <TR>
                                            <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD><TEXTAREA name="description_<%= l.getId()%>" rows="10" cols="40" class="dark" ></TEXTAREA>
                                            </TD>
                                        </TR>
                                        <% }%>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD colspan="2" align="center"><INPUT type="submit" value="Erstellen" /></TD>
                            </TR>
                        </TABLE>
                    </FORM>

                    <%

                                           }
                        break;
                            
                        case (TYPE_CREATEREQUIREMENT):
                                                       {
                           int questStepId = 0;
                            if (request.getParameter("questStepId") != null) {
                                questStepId = Integer.parseInt(request.getParameter("questStepId"));
                            }
                    %>
                    <TABLE class="bluetrans" align="center">
                        <TR>
                            <TD>Hierbei handelt es sich um eine Vorbedingung wie: <BR>
                                -Minimum-Bev�lkerung (Hauptplanet) [Der Spieler muss mindestens eine bestimmte Bev�lkerung auf seinem Hauptplanet haben dass dieser Schritt eintritt]<BR>
                                -Bodenkampf (Gewinn) [Der Spieler musse inen Bodenkampf gewonnen haben dass dieser Schritt eintritt]
                                <BR><BR>
                                <B>Die hier erstellten Schritte m�ssen nachdem sie angelegt wurden erst von den Programmierern ausprogrammiert werden.</B>
                            </TD>
                        </TR>
                    </TABLE>
                    <FORM action="questBuilder.jsp?process=<%= PROCESS_CREATEQUESTREQUIREMENT%>" method="post">
                        <INPUT type="hidden" name="questStepId" value=<%= questStepId%> />
                        <TABLE align="center" class="bluetrans">
                            <TR class="blue2">
                                <TD>Name
                                </TD>
                                <TD>
                                    <TABLE class="bluetrans">
                                        <% for (Language l : Service.languageDAO.findAll()) {
                                                if (l.getMasterLanguageId() != l.getId()) {
                                                    continue;
                                                }
                                        %>
                                        <TR>
                                            <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD><INPUT name="name_<%= l.getId()%>" type="textfield" class="dark"/>
                                            </TD>
                                        </TR>
                                        <% }%>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD class="blue2">Beschreibung
                                </TD>
                                <TD>
                                    <TABLE class="bluetrans">
                                        <% for (Language l : Service.languageDAO.findAll()) {
                                                if (l.getMasterLanguageId() != l.getId()) {
                                                    continue;
                                                }
                                        %>
                                        <TR>
                                            <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD><TEXTAREA name="description_<%= l.getId()%>" rows="10" cols="40" class="dark" ></TEXTAREA>
                                            </TD>
                                        </TR>
                                        <% }%>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD colspan="2" align="center"><INPUT type="submit" value="Erstellen" /></TD>
                            </TR>
                        </TABLE>
                    </FORM>

                    <%
                        }
                            break;


                        case (TYPE_CREATEQUESTEFFECT):
                        {
                            int questStepId = 0;
                            if (request.getParameter("questStepId") != null) {
                                questStepId = Integer.parseInt(request.getParameter("questStepId"));
                            }
                    %>
                    <TABLE class="bluetrans" align="center">
                        <TR>
                            <TD>Hierbei handelt es sich um Effekte oder Ergebnisse einer Quest wie: <BR>
                                -Ressourcengeschenk (Eisen) [Der Spieler bekommt eine bestimmte Anzahl an Eisen zur Verf�gung gestellt]<BR>
                                -Entwicklungspunkte [Der Spieler bekommt eine bestimmte Anzahl von Entwicklungspunkte]
                                <BR><BR>
                                <B>Die hier erstellten Effekte m�ssen nachdem sie angelegt wurden erst von den Programmierern ausprogrammiert werden.</B>
                            </TD>
                        </TR>
                    </TABLE>
                    <FORM action="questBuilder.jsp?process=<%= PROCESS_CREATEQUESTEFFECT%>" method="post">
                        <INPUT type="hidden" name="questStepId" value=<%= questStepId%> />
                        <TABLE align="center" class="bluetrans">
                            <TR class="blue2">
                                <TD>Name
                                </TD>
                                <TD>
                                    <TABLE class="bluetrans">
                                        <% for (Language l : Service.languageDAO.findAll()) {
                                                if (l.getMasterLanguageId() != l.getId()) {
                                                    continue;
                                                }
                                        %>
                                        <TR>
                                            <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD><INPUT name="name_<%= l.getId()%>" type="textfield" class="dark"/>
                                            </TD>
                                        </TR>
                                        <% }%>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD class="blue2">Beschreibung
                                </TD>
                                <TD>
                                    <TABLE class="bluetrans">
                                        <% for (Language l : Service.languageDAO.findAll()) {
                                                if (l.getMasterLanguageId() != l.getId()) {
                                                    continue;
                                                }
                                        %>
                                        <TR>
                                            <TD><%= ML.getQuestMLStr(l.getName(), locale)%>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD><TEXTAREA name="description_<%= l.getId()%>" rows="10" cols="40" class="dark" ></TEXTAREA>
                                            </TD>
                                        </TR>
                                        <% }%>
                                    </TABLE>
                                </TD>
                            </TR>
                            <TR>
                                <TD colspan="2" align="center"><INPUT type="submit" value="Erstellen" /></TD>
                            </TR>
                        </TABLE>
                    </FORM>

                    <%
                                       }

                                break;
                        }



                    %>
                    </html>
