<%@page import="at.darkdestiny.core.GameConstants"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="java.util.Locale" %>
<%@page import="at.darkdestiny.core.ML" %><%
// Determine correct connection path
    session = request.getSession();

    Locale locale = request.getLocale();
    if (session.getAttribute("Language") != null) {
        locale = (Locale) session.getAttribute("Language");
    }
    int userId = 0;
    try {
        userId = Integer.parseInt((String) session.getAttribute("userId"));
    } catch (Exception e) {
    }

    if (1 == 2) {
    if ((userId == 0) || ((session.getAttribute("actSystem") != null && (session.getAttribute("adminLogin") == null)))) {

        java.lang.System.out.println("userId" + userId);
        java.lang.System.out.println("session.getAttribute()" + session.getAttribute("actSystem"));
        java.lang.System.out.println(session.getAttribute("adminLogin"));
        try {
            session.invalidate();
        } catch (IllegalStateException ise) {
        }
%>
<jsp:forward page="login.jsp">
    <jsp:param name="errmsg" value='Alte Session gel�scht bitte erneut einloggen!' />
</jsp:forward>
<%    } }

String xmlUrl = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/") + 1);
%>    
<script type="text/javascript">
    var baseUrl = '<%= xmlUrl %>';
</script>    
<%    
    String systemIdStr = "";
    if (request.getParameter("marker") != null) {
        try {
            int systemId = Integer.parseInt(request.getParameter("marker"));
            if (systemId > 9999) {
                systemId = 0;
            } else {
                systemIdStr = "" + systemId;
            }
        } catch (NumberFormatException nfe) {
            DebugBuffer.addLine(nfe);
        }
    }
%>
<html>
<head>
<title>Aurora Starmap Prototype</title>
		<style>
			.rotate-north {
			  top: 65px;
			  left: .5em;
			}
			.ol-touch .rotate-north {
			  top: 80px;
			}
		</style>	
</head>
<body style="background-color: black">
<DIV id="darklayer" class="dark" style="display:none">
</DIV>
<DIV id="helper" class="helper">
	<DIV id="starmapdetail" class="starmapdetail" style="display:none"></DIV>                
</DIV>        
<DIV id="actionpanel" class="actionpanel" style="display:none;">    
	<SPAN id="actionpanelheading" class="actionpanelheading">Sende Flotte</SPAN><BR>
	<SPAN id="actionpanelfleetname" class="actionpanelfleetname">Flotte: -</SPAN><BR>
	<SPAN id="actionpaneltargetsys" class="actionpaneltargetsys">System: -</SPAN><BR>
	<SPAN id="actionpaneltargetplanet" class="actionpaneltargetplanet">Planet: -</SPAN><BR>
	<INPUT hidden id="fleetId" value="" />
	<INPUT hidden id="targetType" value="" />
	<INPUT hidden id="targetId" value="" />
	<INPUT hidden id="toSystem" value="" />
	<BR>
	 <button type="button" onclick="sendFleet(document.getElementById('fleetId').value,document.getElementById('targetType').value,
	 document.getElementById('targetId').value,document.getElementById('toSystem').value)">OK!</button> 
	 <button type="button" onclick="stopDrawMode();">Abbrechen</button> 
</DIV>        
<div class="container-fluid">

<div class="row-fluid">
    <div>
        <TABLE align="center"><TR><TD align="center">
        <B><FONT color="yellow" size="+1">Bitte den Startplatz in der Galaxie w�hlen, dazu einfach auf ein eingef�rbtes Quadrat in der Karte klicken und best�tigen. Rotumrandente und durchgestrichene Bereiche sind f�r Neuanmeldung gesperrt.<BR>
            Umso st�rker ein Gebiet gelb eingef�rbt ist, umso h�her ist dort bereits die Besiedelung bzw. das Technologie-Level der Spieler.</FONT></B><BR><BR>
                </TD></</TR></table>
    </div>
  <div class="span12">
    <div id="map" class="map"></div>  
  </div>
</div>

</div>
                <link rel="stylesheet" href="css/ol.css" type="text/css">
		<link rel="stylesheet" href="css/starmap.css" type="text/css">
                <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
                <script src="java/starmap/ajax.js"></script>
		<script src="java/starmap/jquery-2.0.0.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>		
                <script src="java/starmap/ol-debug.js"></script>
		<script src="java/starmap/starmap.js"></script>
		<script src="java/starmap/logic.js"></script>
</body>
</html>