<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%
   long timeNow = System.currentTimeMillis();
   Statistics statistics = new Statistics();
   statistics.calculateStatistics();

   long after = System.currentTimeMillis();
   DebugBuffer.addLine("Statistikupdate in "+(after-timeNow)+" ms durchgeführt");
   
   
   %>
