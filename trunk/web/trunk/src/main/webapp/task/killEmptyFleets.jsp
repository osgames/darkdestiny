<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.dao.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="java.util.*" %>

<%
            PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
            ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);

            ArrayList<PlayerFleet> pfList = pfDAO.findAll();
            for (PlayerFleet pf : pfList) {
                ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(pf.getId());
                for (ShipFleet sf : sfList) {
                    if (Service.shipDesignDAO.findById(sf.getDesignId()) == null) {
                        Service.shipFleetDAO.remove(sf);
                    }
                }
                sfList.clear();
                sfList = sfDAO.findByFleetId(pf.getId());
                if (sfList.size() == 0) {
%>Remove fleet : <%= pf.getId()%> <%
                pfDAO.remove(pf);
            }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Kill empty fleets</title>
    </head>
    <body>
        <h2>Hopefully killed now ;)</h2>
    </body>
</html>
