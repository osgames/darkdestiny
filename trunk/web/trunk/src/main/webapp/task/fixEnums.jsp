
<%@page import="at.darkdestiny.core.service.Service" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%

for(Action a : (ArrayList<Action>)Service.actionDAO.findAll()){
    int actionType = a.getActionType();
/*
    public static final int TYPE_BUILDING = 1;
    public static final int TYPE_RESEARCH = 2;
    public static final int TYPE_MODULE = 3;
    public static final int TYPE_SHIP = 4;
    public static final int TYPE_GROUNDTROOPS = 5;
    public static final int TYPE_FLIGHT = 6;
    public static final int TYPE_DEFENSE = 7;
    public static final int TYPE_SPYRELATED = 8;
    public static final int TYPE_TRADE = 9;
    public static final int TYPE_DECONSTRUCT = 10;
    public static final int TYPE_PROBE = 11;
 * */
    switch(actionType){
            case(1):
                a.setType(EActionType.BUILDING);
                break;
            case(2):
                a.setType(EActionType.RESEARCH);
                break;
            case(3):
                a.setType(EActionType.MODULE);
                break;
            case(4):
                a.setType(EActionType.SHIP);
                break;
            case(5):
                a.setType(EActionType.GROUNDTROOPS);
                break;
            case(6):
                a.setType(EActionType.FLIGHT);
                break;
            case(7):
                a.setType(EActionType.DEFENSE);
                break;
            case(8):
                a.setType(EActionType.SPYRELATED);
                break;
            case(9):
                a.setType(EActionType.TRADE);
                break;
            case(10):
                a.setType(EActionType.DECONSTRUCT);
                break;
            case(11):
                a.setType(EActionType.PROBE);
                break;

        }
                Service.actionDAO.update(a);
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
