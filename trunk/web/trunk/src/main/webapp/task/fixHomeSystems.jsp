<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>

<%
    Logger.getLogger().write("START PROCESSING");

            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT s.id, p.id FROM playerplanet pp LEFT OUTER JOIN planet p ON p.id=pp.planetID LEFT OUTER JOIN system s ON s.id=p.systemId WHERE pp.homesystem=1");
            while (rs.next()) {
                int systemId = rs.getInt(1);
                
                if ((systemId == 583) ||
                    (systemId == 1677) ||
                    (systemId == 1935) ||
                    (systemId == 2189)) {
                    out.write("FOUND SYSTEM " + rs.getInt(1) + " AND PLANET "+rs.getInt(2)+" FOR BALANCING<BR>");                    
                    
                    while (LoginService.balanceStartingSystem(rs.getInt(2),systemId)) {
                        Thread.sleep(100);
                    }
                }
                
                

                out.write("Analyzing ...<BR>");
                                        
                PlanetData p = new PlanetData(rs.getInt(2));
                
                long volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (p.getDiameter() / 2), 3)));
                long iron = (long) ((float) volume / (float) 1000);                
                
                Statement stmt2 = DbConnect.createStatement();
                stmt2.executeUpdate("UPDATE planetressources SET type=" + PlanetTable.PLANET_RESSOURCE + ", ressId=" + GameConstants.RES_PLANET_IRON + ", qty=" + iron + " WHERE planetId=" + rs.getInt(2) + " AND type=" + PlanetTable.PLANET_RESSOURCE + " AND ressId=" + GameConstants.RES_PLANET_IRON);                    
                stmt2.close();
                
                boolean hasYnke = false;
                int ynkeAmount = 0;
                boolean hasHowal = false;
                int howalAmount = 0;
                int habitableCount = 0;
                int mCount = 0;
                long totalAddPopulation = 0;
                long homePopulation = 0;

                HashMap<Integer,Long> colonies = new HashMap<Integer,Long>();
                
                stmt2 = DbConnect.createStatement();
                ResultSet rs2 = stmt2.executeQuery("SELECT id, landType FROM planet WHERE systemId=" + rs.getInt(1));
                while (rs2.next()) {
                    PopulationFunctions pf = new PopulationFunctions(rs2.getInt(1));
                    pf.loadValues();
                    
                    if (rs2.getString(2).equalsIgnoreCase("M")) {
                        mCount++;
                        habitableCount++;
                    }

                    if (rs2.getString(2).equalsIgnoreCase("C")) {
                        habitableCount++;
                    }

                    if (rs2.getString(2).equalsIgnoreCase("G")) {
                        habitableCount++;
                    }

                    Statement stmt3 = DbConnect.createStatement();
                    ResultSet rs3 = stmt3.executeQuery("SELECT ressId, qty FROM planetressources WHERE planetId=" + rs2.getInt(1) + " AND type=0");
                    while (rs3.next()) {
                        if (rs3.getInt(1) == 12) {
                            hasYnke = true;
                            ynkeAmount += rs3.getInt(2);
                        }

                        if (rs3.getInt(1) == 13) {
                            hasHowal = true;
                            howalAmount += rs3.getInt(2);
                        }
                    }

                    if (rs.getInt(2) != rs2.getInt(1)) {
                        totalAddPopulation += pf.getMaxPopulation();
                        if (pf.getMaxPopulation() > 0) colonies.put(rs2.getInt(1), pf.getMaxPopulation());
                    } else {
                        homePopulation += pf.getMaxPopulation();
                    }
                }

                out.write("TOTAL ADD POPULATION: " + FormatUtilities.getFormattedNumber(totalAddPopulation) + "<BR>");
                out.write("YNKE AMOUNT: " + ynkeAmount + "<BR>");
                out.write("HOWAL AMOUNT; " + howalAmount + "<BR>");
                out.write("HABITABLE PLANETS: " + habitableCount + "<BR>");
                out.write("M PLANETS: " + mCount + "<BR><BR>");                               
                
                out.write("Proposed actions to be taken:<BR><BR>");
                
                boolean reduceAddPop = false;
                boolean increasePopOnHP = false;
                boolean increasePopOnColony = false;
                boolean removeM = false;
                            
                long increasePopBy = 0;
                long decreasePopBy = 0;
                                 
                int bonus = 0;
                
                if (!hasYnke) bonus += 500000000;
                if (!hasHowal) bonus += 500000000;
                
                if (totalAddPopulation > (3500000000l + bonus)) {
                    out.write("Reduce Population of additional planets!<BR>");
                    long tmpDec = totalAddPopulation - (3500000000l + bonus);
                    long decValue = tmpDec - 100000000 + (int)(Math.random() * 200000000);
                    if (decValue > 0) {
                        decreasePopBy = decValue;
                        reduceAddPop = true;
                    }                    
                }
                
                if (mCount > 1) {
                    out.write("Convert additional M planets to C or G!<BR>");
                    removeM = true;
                }      
                
                if (!hasYnke && !hasHowal) {
                    if (totalAddPopulation < 4000000000l) {
                        long tmpValue = 4000000000l - totalAddPopulation;
                        if (habitableCount > 1) {
                            long incValue = tmpValue - 100000000 + (int)(Math.random() * 200000000);
                            if (incValue > 0) {
                                out.write("Increase population on colony by " + FormatUtilities.getFormattedNumber(incValue) + "<BR>");
                                increasePopOnColony = true;
                                increasePopBy = incValue;
                            }
                        } else {                            
                            if (tmpValue > 2500000000l) tmpValue = 2500000000l;
                            long incValue = tmpValue - 100000000 + (int)(Math.random() * 200000000);
                            
                            if (incValue > 0) {
                                out.write("Increase population on homeworld by " + FormatUtilities.getFormattedNumber(incValue) + "<BR>");
                                increasePopOnHP = true;
                                increasePopBy = incValue;
                            }
                        }
                    }
                } else if (!hasYnke) {
                    if (totalAddPopulation < 3000000000l) {
                        long tmpValue = 3000000000l - totalAddPopulation;
                        if (habitableCount > 1) {
                            long incValue = tmpValue - 100000000 + (int)(Math.random() * 200000000);
                            if (incValue > 0) {
                                out.write("Increase population on colony by " + FormatUtilities.getFormattedNumber(incValue) + "<BR>");
                                increasePopOnColony = true;
                                increasePopBy = incValue;
                            }
                        } else {
                            if (tmpValue > 1700000000l) tmpValue = 1700000000l;
                            long incValue = tmpValue - 100000000 + (int)(Math.random() * 200000000);
                            
                            if (incValue > 0) {
                                out.write("Increase population on homeworld by " + FormatUtilities.getFormattedNumber(incValue) + "<BR>");
                                increasePopOnHP = true;
                                increasePopBy = incValue;
                            }
                        }
                    }                    
                } else if (!hasHowal) {
                    if (totalAddPopulation < 3000000000l) {
                        long tmpValue = 3000000000l - totalAddPopulation;
                        if (habitableCount > 1) {
                            long incValue = tmpValue - 100000000 + (int)(Math.random() * 200000000);
                            if (incValue > 0) {
                                out.write("Increase population on colony by " + FormatUtilities.getFormattedNumber(incValue) + "<BR>");
                                increasePopOnColony = true;
                                increasePopBy = incValue;
                            }
                        } else {
                            if (tmpValue > 170000000l) tmpValue = 1700000000l;
                            long incValue = tmpValue - 100000000 + (int)(Math.random() * 200000000);
                            
                            if (incValue > 0) {
                                out.write("Increase population on homeworld by " + FormatUtilities.getFormattedNumber(incValue) + "<BR>");
                                increasePopOnHP = true;
                                increasePopBy = incValue;
                            }
                        }
                    }                    
                } else {
                    if (totalAddPopulation < 2000000000l) {
                        long tmpValue = 2000000000l - totalAddPopulation;
                        if (habitableCount > 1) {
                            long incValue = tmpValue - 100000000 + (int)(Math.random() * 200000000);
                            if (incValue > 0) {
                                out.write("Increase population on colony by " + FormatUtilities.getFormattedNumber(incValue) + "<BR>");
                                increasePopOnColony = true;
                                increasePopBy = incValue;
                            }
                        } else {
                            if (tmpValue > 1400000000) tmpValue = 1400000000;
                            long incValue = tmpValue - 100000000 + (int)(Math.random() * 200000000);
                            
                            if (incValue > 0) {
                                out.write("Increase population on homeworld by " + FormatUtilities.getFormattedNumber(incValue) + "<BR>");
                                increasePopOnHP = true;
                                increasePopBy = incValue;
                            }
                        }
                    }                    
                }
                
                // Recheck homeworld
                if (habitableCount == 1) {
                    boolean reduce = false;
                    long reduceTo = 0;
                    
                    if (!(hasYnke ^ hasHowal)) {
                        reduce = true;
                        reduceTo = 9200000000l - (long)(Math.random() * 200000000);
                    } else if (!hasYnke && !hasHowal) {
                        reduce = true;
                        reduceTo = 10100000000l - (long)(Math.random() * 200000000);
                    } else {
                        reduce = true;
                        reduceTo = 8900000000l - (long)(Math.random() * 200000000);
                    }
                    
                    PlanetData homeP = new PlanetData(rs.getInt(2));
                    
                    if (homeP.getMaxPopulation() > reduceTo) {
                        long targetPop = reduceTo;
                                                
                        while (homeP.getMaxPopulation() > targetPop) {                            
                            homeP.setDiameter(homeP.getDiameter() - 100);
                            homeP.calcSurface();
                            homeP.calcMaxPopulation();
                        }
                        
                        out.write("<BR>TESTACTION: FIX HP DIAMETER TO " + homeP.getDiameter());                      
                        Statement stmt4 = DbConnect.createStatement();
                        stmt4.execute("UPDATE planet SET diameter="+homeP.getDiameter()+" WHERE id="+rs.getInt(2));  
                        stmt4.close();                        
                    }
                }
                
                increasePopOnHP = false;
                
                out.write("<BR>");

                if (removeM) {
                    // rescan for planets and switch all planets not homeworld and M to C or G
                    Statement stmt3 = DbConnect.createStatement();
                    ResultSet rs3 = stmt3.executeQuery("SELECT p.id, p.landType, pp.homesystem FROM planet p LEFT OUTER JOIN playerplanet pp ON pp.planetID=p.id WHERE p.systemId=" + rs.getInt(1));                    
                    while (rs3.next()) {
                        if ((rs3.getString(3) == null) && (rs3.getString(2).equalsIgnoreCase("M"))) {
                            String newType = "";
                            int newTemp = 0;
                            
                            if (Math.random() > 0.5d) {
                                newType = "C";
                                newTemp = 0;
                            } else {
                                newType = "G";
                                newTemp = 30;
                            }
                            
                            out.write("TESTACTION: TRANSFORM PLANET " + rs3.getInt(1) + " FROM M TO " + newType + " (NEW TEMP="+newTemp+")");
                            Statement stmt4 = DbConnect.createStatement();
                            stmt4.execute("UPDATE planet SET landType='"+newType+"' AND avgTemp="+newTemp+" WHERE id="+rs3.getInt(1));
                            stmt4.close();
                            PlanetData pdTmp = new PlanetData(rs3.getInt(1));
                            pdTmp.setPlanetClass(newType);
                            pdTmp.setAverageTemp(newTemp);                            
                        }
                    }
                } else if (reduceAddPop) {
                    // reduce diameter of largest colony till required pop reached
                    int largestPlanet = 0;
                    long largestPop = 0;
                    
                    for (Map.Entry<Integer,Long> colony : colonies.entrySet()) {
                        if (largestPop < colony.getValue()) {
                            largestPop = colony.getValue();
                            largestPlanet = colony.getKey();                            
                        }
                    }
                    
                    out.write("TESTACTION: REDUCE POPULATION ON " + largestPlanet + " BY " + decreasePopBy);
                    
                        PlanetData pdTmp = new PlanetData(largestPlanet);
                        long targetPop = pdTmp.getMaxPopulation() - decreasePopBy;
                        int oldDiameter = pdTmp.getDiameter();
                                                
                        while (pdTmp.getMaxPopulation() > targetPop) {                            
                            pdTmp.setDiameter(pdTmp.getDiameter() - 100);
                            pdTmp.calcSurface();
                            pdTmp.calcMaxPopulation();
                        }
                        
                        out.write("<BR>TESTACTION: REDUCE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());                      
                        Statement stmt4 = DbConnect.createStatement();
                        stmt4.execute("UPDATE planet SET diameter="+pdTmp.getDiameter()+" WHERE id="+largestPlanet);  
                        stmt4.close();
                } else if (increasePopOnHP || increasePopOnColony) {
                    if (increasePopOnHP) {
                        // Increase diameter of HP till required pop is reached
                        out.write("TESTACTION: INCREASE POPULATION ON " + rs.getInt(2) + " BY " + increasePopBy);
                        
                        PlanetData pdTmp = new PlanetData(rs.getInt(2));
                        long targetPop = pdTmp.getMaxPopulation() + increasePopBy;
                        int oldDiameter = pdTmp.getDiameter();
                                                
                        while (pdTmp.getMaxPopulation() < targetPop) {                            
                            pdTmp.setDiameter(pdTmp.getDiameter() + 100);
                            pdTmp.calcSurface();
                            pdTmp.calcMaxPopulation();
                        }
                                                
                        out.write("<BR>TESTACTION: INCREASE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());                          
                        Statement stmt4 = DbConnect.createStatement();
                        stmt4.executeUpdate("UPDATE planet SET diameter="+pdTmp.getDiameter()+" WHERE id="+rs.getInt(2));   
                        
                        volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (pdTmp.getDiameter() / 2), 3)));
                        iron = (long) ((float) volume / (float) 1000);     

                        stmt4.executeUpdate("UPDATE planetressources SET qty=" + iron + " WHERE planetId = " + rs.getInt(2) + " AND type=" + PlanetTable.PLANET_RESSOURCE + " AND ressId=" + GameConstants.RES_PLANET_IRON);                    
                        stmt4.close();                                                                     
                    } else {
                        // Increase diameter of smallest colony till required pop is reached
                        int smallestPlanet = 0;
                        long smallestPop = 99999999999l;

                        for (Map.Entry<Integer,Long> colony : colonies.entrySet()) {
                            if (smallestPop > colony.getValue()) {
                                smallestPop = colony.getValue();
                                smallestPlanet = colony.getKey();                            
                            }
                        }           

                        out.write("TESTACTION: INCREASE POPULATION ON " + smallestPlanet + " BY " + increasePopBy);                                                           
                        
                        PlanetData pdTmp = new PlanetData(smallestPlanet);
                        long targetPop = pdTmp.getMaxPopulation() + increasePopBy;
                        int oldDiameter = pdTmp.getDiameter();
                        int oldTemp = pdTmp.getAverageTemp();
                                                
                        while (pdTmp.getMaxPopulation() < targetPop) {                            
                            pdTmp.setDiameter(pdTmp.getDiameter() + 100);
                            if (pdTmp.getPlanetClass().equalsIgnoreCase("C")) {
                                if (pdTmp.getAverageTemp() < 5) pdTmp.setAverageTemp(pdTmp.getAverageTemp() + 1);
                            } else {
                                if (pdTmp.getAverageTemp() > 25) pdTmp.setAverageTemp(pdTmp.getAverageTemp() - 1);
                            }
                            
                            pdTmp.calcSurface();
                            pdTmp.calcMaxPopulation();
                        }
                        
                        out.write("<BR>TESTACTION: INCREASE DIAMETER FROM " + oldDiameter + " TO " + pdTmp.getDiameter());   
                        Statement stmt4 = DbConnect.createStatement();
                        stmt4.execute("UPDATE planet SET avgTemp="+pdTmp.getAverageTemp()+", diameter="+pdTmp.getDiameter()+" WHERE id="+smallestPlanet);
                        stmt4.close();
                    }
                }
                
                out.write("<BR><BR>");
            }
%>
