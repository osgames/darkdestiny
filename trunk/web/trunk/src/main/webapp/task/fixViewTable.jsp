<%@page import="at.darkdestiny.framework.QueryKey"%>
<%@page import="at.darkdestiny.framework.QueryKeySet"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.sql.*" %>
<%@page import="at.darkdestiny.core.utilities.*" %>
<%@page import="at.darkdestiny.core.dao.*" %>
<%@page import="at.darkdestiny.core.model.ViewTable" %>
<%@page import="at.darkdestiny.core.model.Planet" %>
<%@page import="java.util.*" %>
<%
    ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

    ArrayList<ViewTable> allEntries = vtDAO.findAll();    
    HashMap<Integer,ArrayList<Integer>> rebuild = new HashMap<Integer,ArrayList<Integer>>();   
    HashMap<Integer,ArrayList<Integer>> reverseCheck = new HashMap<Integer,ArrayList<Integer>>();    
    
    for (ViewTable vt : allEntries) {
        if (vt.getPlanetId() == 0) continue;

        Planet p = pDAO.findById(vt.getPlanetId());
        if (p == null) {
            ArrayList<Integer> sysList;
            
            if (!rebuild.containsKey(vt.getUserId())) {
                sysList = new ArrayList<Integer>();
                sysList.add(vt.getSystemId());
                rebuild.put(vt.getUserId(),sysList);
            } else {
                sysList = rebuild.get(vt.getUserId());
                if (!sysList.contains(vt.getSystemId())) {
                    sysList.add(vt.getSystemId());
                }
            }                        
        }        
    }                  
    
    for (ViewTable vt : allEntries) {
        if (vt.getPlanetId() == 0) continue;
        
        vt.setLastTimeVisited(GameUtilities.getCurrentTick2());
        
        /*

        */
        
        if (!reverseCheck.containsKey(vt.getUserId())) {
            ArrayList<Planet> pList = pDAO.findBySystemId(vt.getSystemId());
            ArrayList<ViewTable> vtList = vtDAO.findByUserAndSystem(vt.getUserId(),vt.getSystemId());
            
            if (pList.size() != vtList.size()) {
                out.write("[LOG] " + vt.getSystemId() + " PlanetCount: "+pList.size()+" ViewTableCount: "+vtList.size()+" User: "+vt.getUserId()+"<BR>");
                ArrayList<Integer> sysList = new ArrayList<Integer>();
                sysList.add(vt.getSystemId());
                reverseCheck.put(vt.getUserId(), sysList);
                if (rebuild.containsKey(vt.getUserId())) {
                    if (rebuild.get(vt.getUserId()).contains(vt.getSystemId())) {
                        rebuild.get(vt.getUserId()).remove(vt.getSystemId());
                    }
                }
            }
        } else {
            ArrayList<Integer> sysList = reverseCheck.get(vt.getUserId());
            if (!sysList.contains(vt.getSystemId())) {
                ArrayList<Planet> pList = pDAO.findBySystemId(vt.getSystemId());
                ArrayList<ViewTable> vtList = vtDAO.findByUserAndSystem(vt.getUserId(),vt.getSystemId());

                if (pList.size() != vtList.size()) {
                    out.write("[LOG] " + vt.getSystemId() + " PlanetCount: "+pList.size()+" ViewTableCount: "+vtList.size()+" User: "+vt.getUserId()+"<BR>");
                    sysList.add(vt.getSystemId());
                    reverseCheck.put(vt.getUserId(), sysList);
                    if (rebuild.containsKey(vt.getUserId())) {
                        if (rebuild.get(vt.getUserId()).contains(vt.getSystemId())) {
                            rebuild.get(vt.getUserId()).remove(vt.getSystemId());
                        }
                    }
                }                
            }
        }
    }
    
    // Update time of all viewtable entries
    vtDAO.updateAll(allEntries);
    
    // Start deleting found entries
    for (Map.Entry<Integer,ArrayList<Integer>> entry : rebuild.entrySet()) {
        ArrayList<Integer> sysList = entry.getValue();
        for (Integer sys : sysList) {
            out.write("[Inconsistency 1] Rebuild system " + sys + " for user "+entry.getKey()+"<BR>");
            
            QueryKeySet qks = new QueryKeySet();
            qks.addKey(new QueryKey("systemId",sys));
            qks.addKey(new QueryKey("userId",entry.getKey()));
            
            vtDAO.removeAll(qks);
            ViewTableUtilities.addOrRefreshSystemForUser(entry.getKey(), sys, GameUtilities.getCurrentTick2());
        }
    }
    
    for (Map.Entry<Integer,ArrayList<Integer>> entry : reverseCheck.entrySet()) {
        ArrayList<Integer> sysList = entry.getValue();
        for (Integer sys : sysList) {
            out.write("[Inconsistency 2] Rebuild system " + sys + " for user "+entry.getKey()+"<BR>");
            
            QueryKeySet qks = new QueryKeySet();
            qks.addKey(new QueryKey("systemId",sys));
            qks.addKey(new QueryKey("userId",entry.getKey()));            
            
            vtDAO.removeAll(qks);
            ViewTableUtilities.addOrRefreshSystemForUser(entry.getKey(), sys, GameUtilities.getCurrentTick2());
        }
    }
%>