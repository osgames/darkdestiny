<%@page import="at.darkdestiny.core.service.ShipDesignService"%>
<%@page import="at.darkdestiny.framework.access.DbConnect"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@ page import="at.darkdestiny.core.*" %>
<%@ page import="java.sql.*" %>

<%
    Statement stmt = DbConnect.createStatement();
    ResultSet rs = stmt.executeQuery("SELECT id FROM shipdesigns");

    while (rs.next()) {
        ShipDesignService.updateDesignCosts(rs.getInt(1));

        out.println("Recalculated " + rs.getInt(1));
    }

    rs.close();
    stmt.close();
%>