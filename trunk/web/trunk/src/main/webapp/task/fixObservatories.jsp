
<%@page import="at.darkdestiny.core.model.Construction"%>
<%@page import="at.darkdestiny.core.utilities.ViewTableUtilities"%>
<%@page import="at.darkdestiny.core.model.PlayerPlanet"%>
<%@page import="at.darkdestiny.core.scanner.SystemDesc"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="java.util.ArrayList"%>
<%@page import="at.darkdestiny.core.scanner.StarMapQuadTree"%>
<%@page import="javax.swing.text.View"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.util.*"%>
<%

    StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
    ArrayList<at.darkdestiny.core.model.System> systems = Service.systemDAO.findAll();

    for (at.darkdestiny.core.model.System s : systems) {
        SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
        quadtree.addItemToTree(sd);
    }

    for (PlayerPlanet pp : (ArrayList<PlayerPlanet>) Service.playerPlanetDAO.findAll()) {
        ViewTableUtilities.addOrRefreshSystem(pp.getUserId(), Service.planetDAO.findById(pp.getPlanetId()).getSystemId(), GameUtilities.getCurrentTick2());

        if (Service.planetConstructionDAO.isConstructed(pp.getPlanetId(), Construction.ID_OBSERVATORY)) {

            at.darkdestiny.core.model.System s = Service.systemDAO.findById(Service.planetDAO.findById(pp.getPlanetId()).getSystemId());

            ArrayList<SystemDesc> tmpSys = quadtree.getItemsAround(s.getX(), s.getY(), (int) 100);
            for (SystemDesc sd : tmpSys) {

                ViewTableUtilities.addOrRefreshSystem(pp.getUserId(), sd.id, GameUtilities.getCurrentTick2());
            }
        }
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
