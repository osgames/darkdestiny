<%--
    Document   : createDB
    Created on : 02.05.2011, 19:02:40
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="at.darkdestiny.framework.utilities.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Autocreate database</title>
    </head>
    <body> /* CREATE-DB Statements */<BR/>

        <%
            Class[] classes = CreateFromModel.getClasses("at.darkdestiny.core.model");
        %>        /* [<%= classes.length%>] Tables */
        <%

            for (Class c : classes) {
                out.write("/*Found class: " + c.getName() + "<BR>");
                out.write("Generated SQL:*/<BR><BR>");

                // Generate
                out.write(CreateFromModel.createSQLForClass(c));

                out.write("<BR>/*---------------------------------------------*/<BR>");
            }
        %>
    </body>
</html>
