<%@page import="at.darkdestiny.core.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.framework.access.*" %>
<%
Connection con = DbConnect.getConnection();
Statement stmt = con.createStatement();

LinkedList<TroopData> allPlanetaryTroops = new LinkedList<TroopData>();
LinkedList<TroopData> allLoadedTroops = new LinkedList<TroopData>();

out.write("<BR><BR>GROUND<BR><BR>");

ResultSet rs = stmt.executeQuery("SELECT * FROM playertroops");
while (rs.next()) {
    TroopData tmp = new TroopData();
    tmp.setPlanetId(rs.getInt(1));
    tmp.setFleetId(rs.getInt(2));
    tmp.setTrooptype(rs.getInt(3));    
    tmp.setCount(rs.getInt(4));
    
    // out.write("Added ("+tmp.getPlanetId()+","+tmp.getFleetId()+","+tmp.getTrooptype()+","+tmp.getCount()+") to Planetary Buffer<BR>");
    
    allPlanetaryTroops.add(tmp);
}

out.write("<BR><BR>FLEETLOADING<BR><BR>");

rs = stmt.executeQuery("SELECT * FROM fleetloading WHERE loadtype=2");
while (rs.next()) {
    TroopData tmp = new TroopData();
    tmp.setFleetId(rs.getInt(1));
    tmp.setTrooptype(rs.getInt(3));
    tmp.setCount(rs.getInt(4));
    
    // out.write("Added ("+tmp.getFleetId()+",2,"+tmp.getTrooptype()+","+tmp.getCount()+") to Fleet Buffer<BR>");
    
    allLoadedTroops.add(tmp);
}

out.write("<BR><BR>WRITING<BR><BR>");

// Delete Tables
Statement stmt2 = con.createStatement();

stmt2.execute("DELETE FROM playertroops");
stmt2.execute("DELETE FROM fleetloading WHERE loadtype=2");

PreparedStatement pstmt1 = con.prepareStatement("INSERT INTO playertroops (planetId,fleetId,troopId,number) VALUES (?,?,?,?)");
PreparedStatement pstmt2 = con.prepareStatement("INSERT INTO fleetloading (fleetId,loadtype,id,count) VALUES (?,2,?,?)");
PreparedStatement pstmt11 = con.prepareStatement("UPDATE playertroops SET number=number+? WHERE planetId=? AND fleetId=? AND troopId=?");
PreparedStatement pstmt22 = con.prepareStatement("UPDATE fleetloading SET count=count+? WHERE fleetId=? AND loadtype=2 AND id=?");

out.write("<BR><BR>GROUND<BR><BR>");

for (TroopData tmp : allPlanetaryTroops) {
    TroopData newEntry = new TroopData();        
    
    switch(tmp.getTrooptype()) {
        case(20):            
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(50);
            newEntry.setCount(tmp.getCount());
            break;
        case(30):
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(60);
            newEntry.setCount(tmp.getCount());            
            break;
        case(11):
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(20);
            newEntry.setCount(tmp.getCount());            
            break;
        case(40):
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(80);
            newEntry.setCount(tmp.getCount());            
            break;
        case(41):
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(90);
            newEntry.setCount(tmp.getCount());            
            break;
        case(50):
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(60);
            newEntry.setCount(tmp.getCount() * 10);            
            break;
        case(60):
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(60);
            newEntry.setCount(tmp.getCount() * 30);            
            break;
        case(61):
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(60);
            newEntry.setCount(tmp.getCount() * 50);            
            break;           
        default:
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(tmp.getTrooptype());
            newEntry.setCount(tmp.getCount());                  
    }
    
    // out.write("CHANGING "+tmp.getTrooptype()+"-->"+newEntry.getTrooptype()+" WITH COUNT "+tmp.getCount()+"-->"+newEntry.getCount()+"<BR>");
    
    ResultSet rs2 = stmt.executeQuery("SELECT planetId FROM playertroops WHERE planetId="+newEntry.getPlanetId()+" AND fleetId="+newEntry.getFleetId()+" AND troopId="+newEntry.getTrooptype());
    if (rs2.next()) {
        pstmt11.setInt(2,newEntry.getPlanetId());
        pstmt11.setInt(3,newEntry.getFleetId());
        pstmt11.setInt(4,newEntry.getTrooptype());
        pstmt11.setInt(1,newEntry.getCount());
        pstmt11.execute();            
    } else {    
        pstmt1.setInt(1,newEntry.getPlanetId());
        pstmt1.setInt(2,newEntry.getFleetId());
        pstmt1.setInt(3,newEntry.getTrooptype());
        pstmt1.setInt(4,newEntry.getCount());
        pstmt1.execute();    
    }
    
    // stmt.execute("INSERT INTO playertroops (planetId,fleetId,troopId,number) VALUES ("+newEntry.getPlanetId()+","+newEntry.getFleetId()+","+newEntry.getTrooptype()+","+newEntry.getCount()+")");
}

out.write("<BR><BR>FLEETLOADING<BR><BR>");

for (TroopData tmp : allLoadedTroops) {
    TroopData newEntry = new TroopData();
    
    switch(tmp.getTrooptype()) {
        case(20):            
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(50);
            newEntry.setCount(tmp.getCount());
            break;
        case(30):
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(60);
            newEntry.setCount(tmp.getCount());            
            break;
        case(11):
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(20);
            newEntry.setCount(tmp.getCount());            
            break;
        case(40):
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(80);
            newEntry.setCount(tmp.getCount());            
            break;
        case(41):
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(90);
            newEntry.setCount(tmp.getCount());            
            break;
        case(50):
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(60);
            newEntry.setCount(tmp.getCount() * 10);            
            break;
        case(60):
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(60);
            newEntry.setCount(tmp.getCount() * 30);            
            break;
        case(61):
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(60);
            newEntry.setCount(tmp.getCount() * 50);            
            break;            
        default:
            newEntry.setPlanetId(tmp.getPlanetId());
            newEntry.setFleetId(tmp.getFleetId());
            newEntry.setTrooptype(tmp.getTrooptype());
            newEntry.setCount(tmp.getCount());                   
    }
    
    // out.write("(Fleet) CHANGING "+tmp.getTrooptype()+"-->"+newEntry.getTrooptype()+" WITH COUNT "+tmp.getCount()+"-->"+newEntry.getCount()+"<BR>");
    
    ResultSet rs2 = stmt.executeQuery("SELECT fleetId FROM fleetloading WHERE fleetId="+newEntry.getFleetId()+" AND loadtype=2 AND id="+newEntry.getTrooptype());
    if (rs2.next()) {   
        pstmt2.setInt(2,newEntry.getFleetId());
        pstmt2.setInt(3,newEntry.getTrooptype());
        pstmt2.setInt(1,newEntry.getCount());
        pstmt2.execute();        
    } else {
        pstmt2.setInt(1,newEntry.getFleetId());
        pstmt2.setInt(2,newEntry.getTrooptype());
        pstmt2.setInt(3,newEntry.getCount());
        pstmt2.execute();
    }
    // stmt.execute("INSERT INTO fleetloading (fleetId,loadtype,id,count) VALUES ("+newEntry.getFleetId()+",2,"+newEntry.getTrooptype()+","+newEntry.getCount()+")");
}
%>
