<%
  Runtime r = Runtime.getRuntime();
  
  long freeMem = r.freeMemory();
  long currMem = r.totalMemory();
  long maxMem  = r.maxMemory();
%>
<HTML>
    <BODY>
        Free Memory: <%= freeMem %><BR>
        Current Memory: <%= currMem %><BR>
        Maximum Memory: <%= maxMem %><BR>
    </BODY>
</HTML>