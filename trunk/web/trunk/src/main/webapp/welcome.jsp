<%@page import="at.darkdestiny.core.model.UserSettings"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%@page import="at.darkdestiny.core.model.UserData" %>
<%@page import="at.darkdestiny.core.service.ProfileService"%>
<%@page import="at.darkdestiny.core.model.UserData"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.core.ML"%>
<%@page import="at.darkdestiny.util.DebugBuffer" %>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
DebugBuffer.addLine(DebugLevel.UNKNOWN,"Start welcome page");    

session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));              

UserSettings us = Service.userSettingsDAO.findByUserId(userId);
User u = ProfileService.findUserByUserId(userId);

DebugBuffer.addLine(DebugLevel.UNKNOWN,"UserSettings="+us+" User="+u);
   
String deactivate = "false";
if (request.getParameter("deactivate") != null) {
    deactivate = (String)request.getParameter("deactivate");

    if (deactivate.equals("true")) {
        us.setShowWelcomeSite(false);
        Service.userSettingsDAO.update(us);
%>

        <jsp:forward page="main.jsp?page=new/overview" />
<%
    } else {
        us.setShowWelcomeSite(true);
        Service.userSettingsDAO.update(us);
%>
        <jsp:forward page="main.jsp?page=new/overview" />
<%
    }
}
   
   %>
   <h1><%= ML.getMLStr("welcome_msg_header", userId) %></h1>
   <br>
   <% if (!u.isGuest()){ %>
       <a href="main.jsp?page=welcome&deactivate=true" >>><%= ML.getMLStr("welcome_link_disablewelcomepage", userId) %><<</a>
    <br>  <br><% } %>
</center>
   <table class="bluetrans" width="600px">
       <TR>
           <TD>Jahrtausende lange erstrahlte das glorreiche Imperium der Lemurer in seinem ganzen Glanz.
Wissenschaft und Technik waren in ihrer vollen Bl�te. Die f�hrenden K�pfe wagten sich an die letzten R�tsel des Universums. Sie hatten den Flug zwischen den Sternen geb�ndigt, nichts schien f�r sie unm�glich.
<BR><BR>
Doch vor 50.000 Jahren fiel eine unvorstellbar gewaltt�tige und grausame Spezies aus den Tiefen des Leeraums zwischen den Sterneninseln �ber ihr Reich her. Der Krieg wogte Jahrzehnte, doch so sehr sich die Lemurer auch wehrten, der Kampf schien verloren zu gehen. Unz�hlige Kolonien wurden ausgel�scht, Milliarden Lemurer starben,. Der Grausamkeit des Gegners hatten sie nichts entgegenzusetzen. Schon bald, nach den ersten hohen Verlusten, eilte ein Name den gegnerischen Flotten voraus, Die Bestien.
<BR><BR>
Als sich das Ende der Lemurer abzeichnete, gelang den f�higsten verbliebenen K�pfen, des einstmals ruhmreichen Imperiums noch ein Durchbruch. In einem letzten aufopfernden  Kommandounternehmen gelang es den tapferen Lemurern, auf dem Hauptst�tzpunkt der Bestien ihre neuste Entwicklung zum Einsatz zu bringen n�mlich Psycho-Regeneratoren. Diese bewirkten �ber Jahrhunderte eine zunehmende Abnahme des Aggressionspotenzials der Bestien. Dadurch entgingen die Lemurer ihrer v�lligen Ausrottung. Jedoch waren fast alle wichtigen Welten zerst�rt und das Chaos griff um sich.
<BR><BR>
50.000 Jahre sp�ter sind die Lemurer vergessen, die Reste des einstmals alles beherrschenden Imperiums fielen in die Barbarei zur�ck, es dauerte �onen bis sich einzelne ehemalige Kolonien wieder zu industriellen Gesellschaften entwickelt haben. Die Abschottung und das Vergessen ihrer Herkunft und Geschichte, machte die Nachfahren der Lemurer zu eigenen V�lkern.
<BR><BR>
Diese V�lker stehen nun an der Schwelle zur planetaren Raumfahrt, �bernimm die uneingeschr�nkte Herrschaft �ber eine dieser Zivilisationen, f�hre sie hinaus in die unbekannten Tiefen des Universums. Bilde schlagkr�ftige Armeen aus und entwerfe individuelle Raumschiffe die du zu m�chtigen Flottenverb�nden ausbaust. K�mpfe um Ressourcen, Ruhm und Ehre, treibe Handel mit anderen Zivilisationen oder schlie�e dich mit anderen V�lkern zu gro�en Allianzen zusammen, doch vergiss nie, sei ein guter Herrscher, den du bist auf deine Bev�lkerung angewiesen.

<BR><BR>
Dunkle Zeiten stehen bevor! Dein Volk braucht dich!</TD>
       <TR>
       <tr>
   <!--     <td height="600" width="600" style='background-image: url(http://stud3.tuwien.ac.at/~e0400242/darkdestiny/logo/10.jpg); background-repeat: none;'> -->
          <td><BR><BR><HR>
  <font color="WHITE"> <%= ML.getMLStr("welcome_msg_1", userId) %>
         <br>
         <BR>
        <%= ML.getMLStr("welcome_msg_2", userId) %>
        <br>
            <br>
        <%= ML.getMLStr("welcome_msg_3", userId) %>
        <br>
        <br>
        - Dark Wiki mit einem <a href="http://www.thedarkdestiny.at/wiki/index.php/Tutorial" target="_blank"><%= ML.getMLStr("welcome_link_tutorial", userId) %></a>(German only)(Thanx to smallone)
        <br>
        - <a href="Chat_1.jsp" target="_blank"><b><%= ML.getMLStr("welcome_link_ingamechat", userId) %></b></a>
        <br>
        - <a href="http://forum.thedarkdestiny.at/" target="_blank"><b><%= ML.getMLStr("welcome_link_forum", userId) %></b></a><%= ML.getMLStr("welcome_msg_forum", userId) %>
    
        <br>
        <br><center>
        <h2> <%= ML.getMLStr("welcome_msg_4", userId) %></h2><br><br>
    </center>
            <center>
        <img src="http://upload.wikimedia.org/wikipedia/commons/7/78/Dark_destiny_game_log_250x250.jpg"></img>
    </center>
    
    </font>
    
</td>
</tr>
</table>
