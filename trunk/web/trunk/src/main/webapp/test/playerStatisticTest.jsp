<%-- 
    Document   : EnumAnnotationTest
    Created on : 25.04.2010, 23:33:00
    Author     : Aion
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="at.darkdestiny.core.model.PlayerStatistic" %>
<%@page import="at.darkdestiny.core.dao.*" %>
<%@page import="java.util.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <%

        PlayerStatistic ps = new PlayerStatistic();
        ps.setUserId(1);
        PlayerStatisticDAO psDAO = (PlayerStatisticDAO) DAOFactory.get(PlayerStatisticDAO.class);
        ArrayList<PlayerStatistic> result = psDAO.find(ps);


        %>
        <h1>result size : <%= result.size() %></h1>
    </body>
</html>
