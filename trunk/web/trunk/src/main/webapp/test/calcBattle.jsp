<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%
 /*
  * Hier wird eine Schlacht berechnet.
  * ACHTUNG: geht von Festen SpielerIDs aus: 9998 und 9999 (Angreifer bzw. Verteidiger)
  */
	
%>
<%
Map<String, Object> allPars = (Map<String, Object>)request.getParameterMap(); %>
<% 
TestCombatResult tcr = BetaTools.generateUnits(allPars);
%>
<%= tcr.getMessage() %>
<%
Logger.getLogger().write("TCR: " + tcr);
Logger.getLogger().write("TCR2: " + tcr.getCombatReport());
        
if (tcr.isError()) {
%>
    ERROR: <%= tcr.getMessage() %>
<% } else { %>
<h1>Combat run!</h1>
        <h2>Costs</h2>
        AttackerCost: <%= tcr.getAttackerCost() %><BR>
        DefenderCost: <%= tcr.getDefenderCost() %>
        <h2>Reports</h2>
<% for(Map.Entry<Integer, StringBuilder> report : ((HashMap<Integer,StringBuilder>)tcr.getCombatReport()).entrySet()){ %>
==========================================================================<BR>
Bericht für :  <%= Service.userDAO.findById(report.getKey()).getGameName() %><BR>
<%= report.getValue().toString() %><BR>

<% } %>
<% } %>
