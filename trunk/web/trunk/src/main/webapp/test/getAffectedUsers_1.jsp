<%-- 
    Document   : getAffectedUsers
    Created on : 29.01.2011, 16:09:48
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Viewtable test</title>
    </head>
    <body>
        <h2>Running tests, retrieve affected Users</h2><BR><BR>
<%
    int testUser = 48;

    UserDAO uDAO = (UserDAO)DAOFactory.get(UserDAO.class);

    %>
    <BR><B>TestUser is <%= testUser%></B><BR><BR>
    <%
        ArrayList<UserShareEntry> useList = DiplomacyUtilities.getAffectedShareUsers(testUser);
        for (UserShareEntry use : useList) {
            if (use.getType() == UserShareEntry.EntryType.ALLIANCE_ENTRY) {
                out.write("Found masterAlliance Id " + use.getId() + "<BR>");
                for (Integer userId : use.getUsers()) {
                    out.write("User " + userId + "<BR>");
                }
            } else if (use.getType() == UserShareEntry.EntryType.SINGLE_ENTRY) {
                out.write("Found single user Id " + use.getId() + "<BR>");
                for (Integer userId : use.getUsers()) {
                    out.write("User " + userId + "<BR>");
                }            
            }
        }
    
%>
    </body>
</html>
