<%-- 
    Document   : performanceGet
    Created on : 18.03.2010, 17:08:43
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="at.darkdestiny.framework.*" %>
<%@page import="java.lang.reflect.*" %>
<%@page import="at.darkdestiny.core.model.CombatRound" %>
<%@page import="at.darkdestiny.core.dao.*" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.FormatUtilities" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>GET vs FIND Performance Test</title>
    </head>
    <body>
        <h2>GET vs FIND Performance Test</h2><BR><BR>
<%
/*
    RessourceDAO rDAO = (RessourceDAO)DAOFactory.get(RessourceDAO.class);
    Ressource r = new Ressource();
    r.setId(5);
    
    long start = System.currentTimeMillis();
    int testSize = 50000;
    
    for (int i=0;i<testSize;i++) {
        rDAO.get(r);
    }
    
    long end = System.currentTimeMillis();
    long timeForGet = (end - start);
    
    out.write(FormatUtilities.getFormattedNumber(testSize) + " GET queries took " + (end - start) + "ms ("+((double)(end - start) / (double)testSize)+"ms per Operation)<BR>");    
    
    start = System.currentTimeMillis();
    
    for (int i=0;i<testSize;i++) {
        rDAO.find(r);
    }
    
    end = System.currentTimeMillis();
    long timeForFind = (end - start);
    out.write(FormatUtilities.getFormattedNumber(testSize) + " FIND queries took " + (end - start) + "ms ("+((double)(end - start) / (double)testSize)+"ms per Operation)<BR>");   
    out.write("GET is " + FormatUtilities.getFormattedDecimal((((double)timeForFind / (double)timeForGet) * 100) - 100,1) + "% faster<BR>");
 * */
%>
    <!-- <BR><BR>Test on invalid ID<BR><BR> -->
<%
/*
    r.setId(999);
    start = System.currentTimeMillis();
    
    for (int i=0;i<testSize;i++) {
        rDAO.get(r);
    }
    
    end = System.currentTimeMillis();
    timeForGet = (end - start);
    out.write(FormatUtilities.getFormattedNumber(testSize) + " GET queries took " + (end - start) + "ms ("+((double)(end - start) / (double)testSize)+"ms per Operation)<BR>"); 

    start = System.currentTimeMillis();
    
    for (int i=0;i<testSize;i++) {
        rDAO.find(r);
    }
    
    end = System.currentTimeMillis();
    timeForFind = (end - start);
    out.write(FormatUtilities.getFormattedNumber(testSize) + " FIND queries took " + (end - start) + "ms ("+((double)(end - start) / (double)testSize)+"ms per Operation)<BR>");
    out.write("GET is " + FormatUtilities.getFormattedDecimal((((double)timeForFind / (double)timeForGet) * 100) - 100,1) + "% faster<BR>");
    */

    CombatRoundDAO crDAO = (CombatRoundDAO)DAOFactory.get(CombatRoundDAO.class);
    CombatRound cr = (CombatRound)crDAO.findAll().get(0);
    
    long start = System.currentTimeMillis();
    int testSize = 50;
          
    for (int i=0;i<testSize;i++) {
        cr.setRound((int)Math.floor(Math.random() * 10));
        crDAO.update(cr);
    }
    
    long end = System.currentTimeMillis();
    long timeForGet = (end - start);
    
    out.write(FormatUtilities.getFormattedNumber(testSize) + " UPDATE queries took " + (end - start) + "ms ("+((double)(end - start) / (double)testSize)+"ms per Operation)<BR>");  
    
    ArrayList<CombatRound> combatRounds = new ArrayList<CombatRound>();
    for (int i=0;i<testSize;i++) {
        cr.setRound((int)Math.floor(Math.random() * 10));
        combatRounds.add(cr);
    }       
       
    start = System.currentTimeMillis();    
        
    crDAO.updateAll(combatRounds);
    
    end = System.currentTimeMillis();
    timeForGet = (end - start);
    
    out.write("UPDATEALL took " + (end - start) + "ms ("+((double)(end - start) / (double)testSize)+"ms per Operation)<BR>");     
%>
    </body>
</html>
