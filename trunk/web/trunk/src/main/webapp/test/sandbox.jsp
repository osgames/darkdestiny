<%-- 
    Document   : sandbox
    Created on : 14.02.2015, 23:41:46
    Author     : Stefan
--%>

<%@page import="at.darkdestiny.core.statistic.CalculateImperialOverview"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="at.darkdestiny.framework.dao.DAOFactory"%>
<%@page import="at.darkdestiny.core.dao.UserDAO"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            CalculateImperialOverview cio = new CalculateImperialOverview();
            cio.start();
        %>        
    </body>
</html>
