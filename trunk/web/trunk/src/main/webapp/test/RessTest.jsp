<%-- 
    Document   : RessTest
    Created on : 14.07.2009, 19:18:50
    Author     : Stefan
--%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*"%><%@page import="at.darkdestiny.util.*"%>
<%@page import="at.darkdestiny.core.*" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
<%
    PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    ArrayList<PlayerPlanet> ppList = ppDAO.findAll();

    double bestInitial = 1d;
    double bestInc = 1d;
    double bestIterations = 999999d;

    for (double initalAmp = 1d;initalAmp < 3d;initalAmp+=0.1d) {
        for (double incAmp = 0.1d;incAmp < 2d;incAmp+=0.1d) {
            PlanetService.interpolationStepsTotal = 0;
            
            for (PlayerPlanet pp : ppList) {
                IdToNameService itns = new IdToNameService(pp.getUserId());
                // PlanetService.getFreePopulation(pp.getPlanetId(),initalAmp,incAmp);
                // out.write("Maximum extractable Population for Planet " + itns.getPlanetName(pp.getPlanetId()) + " is " + FormatUtilities.getFormattedNumber(PlanetService.getFreePopulation(pp.getPlanetId(),initalAmp,incAmp)) + "/" + FormatUtilities.getFormattedNumber(pp.getPopulation())+"<BR>");
            }

            Logger.getLogger().write("Total Steps: " + PlanetService.interpolationStepsTotal + " ["+initalAmp+"/"+incAmp+"]");

            if (PlanetService.interpolationStepsTotal < bestIterations) {
                bestInitial = initalAmp;
                bestInc = incAmp;
                bestIterations = PlanetService.interpolationStepsTotal;
            }
        }
    }

    Logger.getLogger().write("Best Result: " + bestIterations + " ["+bestInitial+"/"+bestInc+"]");
%>
    </body>
</html>
