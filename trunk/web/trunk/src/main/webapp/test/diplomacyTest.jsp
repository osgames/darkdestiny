<%-- 
    Document   : diplomacyTest
    Created on : Jan 29, 2011, 5:00:47 PM
    Author     : Dreloc
--%>

<%@page import="at.darkdestiny.core.service.IdToNameService"%>
<%@page import="at.darkdestiny.core.service.DiplomacyService"%>
<%@page import="at.darkdestiny.core.diplomacy.combat.DiplomacyGroupResult"%>
<%@page import="at.darkdestiny.core.diplomacy.combat.CombatGroupResult"%>
<%@page import="at.darkdestiny.core.diplomacy.combat.CombatGroupResolver"%>
<%@page import="at.darkdestiny.core.databuffer.fleet.RelativeCoordinate"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.util.*"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
ArrayList<Integer> players = new ArrayList<Integer>();
players.add(10);
players.add(13);
players.add(15);

DiplomacyGroupResult dgr = CombatGroupResolver.resolveUsers(players);

RelativeCoordinate rc = new RelativeCoordinate(202,0);
CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(players, rc);
%>
<HTML>
    <body>
        <H1>DiplomacyGroupResult</H1>
<%
for (Integer i : players) {
    for (Integer i2 : players) {
        if (i.equals(i2)) continue;
        out.println(IdToNameService.getUserName(i) + " <=> " + IdToNameService.getUserName(i2) + " => " + DiplomacyService.getDiplomacyTypeForId(dgr.getRelationBetween(i, i2).getDiplomacyTypeId()).getName() + "<BR>");        
    }
}
%>                   
        <H1>CombatGroupResolver Result</H1>        
<%
    cgr.print();
%>
    </body>
</HTML>