<%-- 
    Document   : ML_JS
    Created on : 20.07.2010, 11:58:12
    Author     : Stefan
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
    </head>
    <body>
<script language="javascript" type="text/javascript">
    var ML_DE_de = [];
    var ML_EN_gb = [];
    var ML_EN_us = [];
    
    ML_DE_de["lbl_Test"] = "Deutscher Test";
    ML_EN_gb["lbl_Test"] = "British Test";
    ML_EN_us["lbl_Test"] = "American Test";
    
    function ML(tag, language) {
        return eval('ML_' + language + '["' + tag + '"]');
    }    
    
    alert(ML('lbl_Test','DE_de'));
    alert(ML('lbl_Test','EN_gb'));
    alert(ML('lbl_Test','EN_us'));
</script>    
        <h2>Hello World!</h2>
    </body>
</html>
