<%@page import="at.darkdestiny.core.utilities.RessTransfer"%>
<%@page import="at.darkdestiny.core.utilities.*" %>
<%@page import="at.darkdestiny.core.utilities.MilitaryUtilities" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.GameConstants" %>
<%@page import="java.util.*" %>
<%@page import="at.darkdestiny.core.service.InvadeService"%>
<%
session = request.getSession(true);

final int FLEET_ACTION_INVADE = 28;
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetToInvade = Integer.parseInt((String)request.getParameter("planetId"));
String planetName = InvadeService.findPlayerPlanetByPlanetId(planetToInvade).getName();

Map<String, String[]> parMap = request.getParameterMap();
%>
<BR>
<B>Invasion von Planet #<%= planetToInvade %> (<%= planetName %>)</B><BR><BR>
<%

if (!MilitaryUtilities.invasionPossible(userId,planetToInvade, null)) {
    %>
    <B>Es sind keine Flotten vorhanden, um eine Invasion durchzuf&uuml;hren!</B>
    <%
} else {
    int fleetId = 0;
    //Einzelausladen einer Flotte aus dem Transfermenü
    if(request.getParameter("fleetId") != null){
        fleetId = Integer.parseInt(request.getParameter("fleetId"));
       }

    //Einzelausladen
    if(fleetId > 0){
        MilitaryUtilities.invade(fleetId);
        //Alle Flotten im Orbit (Aus der Systemansicht)
    }else{
        ArrayList<PlayerFleet> fleets = MilitaryUtilities.getInvadeableFleets(userId, planetToInvade);
        for(PlayerFleet pf : fleets){
            MilitaryUtilities.invade(pf.getId());
        }
    }
    %>
    <jsp:forward page="main.jsp?page=new/fleetloading">
        <jsp:param name="fmaction" value="<%= FLEET_ACTION_INVADE %>" />
    </jsp:forward>
    <%
}
%>