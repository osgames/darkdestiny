<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.core.view.*"%>
<%@page import="at.darkdestiny.core.view.header.*"%>
<%@page import="at.darkdestiny.core.viewbuffer.*"%>
<%@page import="at.darkdestiny.core.service.Service"%>

<%
session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));
int systemId = Integer.parseInt((String)session.getAttribute("actSystem"));
String actPage = "";
if((String)session.getAttribute("actPage") != null){
    actPage = (String)session.getAttribute("actPage");
}
Map<String, String[]> allPars = request.getParameterMap();

int switchType = 0;
String forwardLink = null;

if (request.getParameter("stype") != null) {
    switchType = Integer.parseInt((String)request.getParameter("stype"));
}

if (request.getParameter("toLink") != null) {
    if (!request.getParameter("toLink").equalsIgnoreCase("")) {
        forwardLink = request.getParameter("toLink");
    }
}

String parStr = "";
boolean firstPar = true;

switch (switchType) {
/*    case(GameConstants.SWITCH_NEXT_LOWER):
        int[] locInfo = gd.getNextLowerPlanet(planetId);
        session.setAttribute("actPlanet",""+locInfo[1]);
        session.setAttribute("actSystem",""+locInfo[0]);

        for (Map.Entry<String, String[]> me : allPars.entrySet()) {
            String key = me.getKey();
            if (key.equalsIgnoreCase("stype")) continue;
            String data = me.getValue()[0];

            if (firstPar) {
                firstPar = false;
                parStr += key + "=" + data;
            } else {
                parStr += "&" + key + "=" + data;
            }
        }
%>
     <!--   <jsp:forward page="main.jsp?<%= parStr %>" /> !>
<%
        // break;*/
    case(GameConstants.SWITCH_HOMESYSTEM):

        session.setAttribute("actPlanet",""+Service.playerPlanetDAO.findHomePlanetByUserId(userId).getPlanetId());
        session.setAttribute("actSystem",""+Service.planetDAO.findById(Service.playerPlanetDAO.findHomePlanetByUserId(userId).getPlanetId()).getSystemId());

        for (Map.Entry<String, String[]> me : allPars.entrySet()) {
            String key = me.getKey();
            if (key.equalsIgnoreCase("stype")) continue;
            String data = me.getValue()[0];

            if (firstPar) {
                firstPar = false;
                parStr += key + "=" + data;
            } else {
                parStr += "&" + key + "=" + data;
            }
        }
%>
        <jsp:forward page="main.jsp?<%= parStr %>" />
<%
        break;
    /* case(GameConstants.SWITCH_NEXT_HIGHER):
        locInfo = gd.getNextHigherPlanet(planetId);
        session.setAttribute("actPlanet",""+locInfo[1]);
        session.setAttribute("actSystem",""+locInfo[0]);
        for (Map.Entry<String, String[]> me : allPars.entrySet()) {
            String key = me.getKey();
            if (key.equalsIgnoreCase("stype")) continue;
            String data = me.getValue()[0];

            if (firstPar) {
                firstPar = false;
                parStr += key + "=" + data;
            } else {
                parStr += "&" + key + "=" + data;
            }
        }

        <jsp:forward page="main.jsp?<%= parStr " />

        break;*/
    case(GameConstants.SWITCH_BY_PARAMETER):
        int actCategory = 0;

        if (session.getAttribute("actCategory") != null) {
            actCategory = Integer.parseInt((String)session.getAttribute("actCategory"));
        }

        int switchTypeHeader = 0;
        int switchId = 0;

        try {
            if (request.getParameter("switchCategory") != null) {
                switchId = Integer.parseInt((String)request.getParameter("switchCategory"));
                switchTypeHeader = HeaderBuffer.SWITCH_TYPE_CATEGORY;
            }

            if (request.getParameter("switchSystem") != null) {
                switchId = Integer.parseInt((String)request.getParameter("switchSystem"));
                switchTypeHeader = HeaderBuffer.SWITCH_TYPE_SYSTEM;
            }

            if (request.getParameter("switchPlanet") != null) {
                switchId = Integer.parseInt((String)request.getParameter("switchPlanet"));
                switchTypeHeader = HeaderBuffer.SWITCH_TYPE_PLANET;
            }
        } catch (NumberFormatException nfe) {
            DebugBuffer.error("Wrong datatype for switching supplied: " + nfe);
        }

        HeaderData hData = null;

        if (switchTypeHeader != 0) {
            hData = HeaderBuffer.getUserHeaderData(userId);
        }

        if (hData != null) {
            switch (switchTypeHeader) {
                case(HeaderBuffer.SWITCH_TYPE_CATEGORY):
                    // Check if this is a valid category
                    if (hData.getCategorys().containsKey(switchId)) {
                        session.setAttribute("actCategory", ""+switchId);

                        int firstSystem = hData.getCategorys().get(switchId).getFirstSystemId();
                        session.setAttribute("actSystem", ""+firstSystem);

                        int firstPlanet = hData.getCategorys().get(switchId).getSystems().get(firstSystem).getFirstPlanetId();
                        session.setAttribute("actPlanet", ""+firstPlanet);

                    }

                    break;
                case(HeaderBuffer.SWITCH_TYPE_SYSTEM):
                    // Check if this system is in active Category
                    if (hData.getCategorys().get(actCategory).getSystems().containsKey(switchId)) {
                        session.setAttribute("actSystem", ""+switchId);

                        int firstPlanet = hData.getCategorys().get(actCategory).getSystems().get(switchId).getFirstPlanetId();
                        session.setAttribute("actPlanet", ""+firstPlanet);

                     }

                    break;
                case(HeaderBuffer.SWITCH_TYPE_PLANET):
                    if (hData.getCategorys().get(actCategory).getSystems().get(systemId) == null) {
                        // System does not exist
                        break;
                    }
                    
                    // Check if this planet is in active System
                    if (hData.getCategorys().get(actCategory).getSystems().get(systemId).getPlanets().containsKey(switchId)) {
                        session.setAttribute("actPlanet", ""+switchId);

                    } else {
                        // Search systems for planetId
                        HashMap<Integer,SystemEntity> allSystems = hData.getCategorys().get(actCategory).getSystems();

                        boolean found = false;

                        for (SystemEntity se : allSystems.values()) {
                            if (se.getPlanets().containsKey(switchId)) {
                                found = true;

                                session.setAttribute("actSystem", ""+se.getSystemId());
                                session.setAttribute("actPlanet", ""+switchId);
                            }
                        }

                        // Switch to all category and start system search again
                        if (!found) {
                            allSystems = hData.getCategorys().get(0).getSystems();

                            for (SystemEntity se : allSystems.values()) {
                                if (se.getPlanets().containsKey(switchId)) {
                                    session.setAttribute("actCategory", "0");
                                    session.setAttribute("actSystem", ""+se.getSystemId());
                                    session.setAttribute("actPlanet", ""+switchId);
                                }
                            }
                        }
                    }
                    break;
            }
        }

        if (forwardLink != null) {
%>
            <jsp:forward page="main.jsp" >
                <jsp:param name="page" value="<%= forwardLink %>" />
                <jsp:param name="toLink" value="" />
            </jsp:forward>
<%
        } else {
            if(!actPage.equals("")){

%>
           <jsp:forward page="main.jsp" >
                <jsp:param name="page" value="<%= actPage %>" />
            </jsp:forward>
<%
            }else{
%>
            <jsp:forward page="main.jsp?page=new/overview" />
<%          }
        }
        try{

            session.removeAttribute("actPage");
        }catch(Exception e){
        }
        break;
}
%>