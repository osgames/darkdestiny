<%--
    Document   : noteEditor
    Created on : 04.02.2011, 18:14:46
    Author     : Aion
--%>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.model.Note" %>
<%


    int userId = 0;
    if (session.getAttribute("userId") != null) {
        userId = Integer.parseInt((String) session.getAttribute("userId"));
    }

    int planetId = 0;
    if (request.getParameter("planetId") != null) {
        planetId = Integer.parseInt((String) request.getParameter("planetId"));
    }
    if (request.getParameter("submit") != null) {
        AdministrationService.changeNote(userId, planetId, request.getParameter("msg"));
%>
<script language="Javascript">
    if(window.opener.name() = "administration"){
    window.opener.doRefreshManagement(<%= planetId %>,<%= userId %>);
    }else{
    window.opener.refreshParent();
    }
    this.close();
</script>
<%
    }
%>
<script language = "JavaScript">
    function docounter()
    {
        var length = document.form.msg.value.length;
        if(length > <%= AdministrationService.NOTE_MAX_LENGTH %>){
            length = <%= AdministrationService.NOTE_MAX_LENGTH %>;
            var txt = document.form.msg.value;
            document.form.msg.value = txt.substring(0, <%= AdministrationService.NOTE_MAX_LENGTH %>);
        }
        document.getElementById("zeichen").innerHTML = length;

    }

</script>

<%


    Note n = AdministrationService.findNote(userId, planetId);
    String msg = "";
    if (n != null) {
        msg = n.getMessage();
    }
%>
<HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    </head>
    <TITLE>
        Notiz Planet #<%= planetId%>
    </TITLE>
    <BODY bgcolor="black">
        <FORM name="form" method="post" action="noteEditor.jsp?submit=true">
            <INPUT type="hidden" name="planetId" value="<%= planetId%>"/>
            <TABLE border="0" style="width:100%;height:100%;">
                <TR>
                    <TD style="width:100%;" align="right">
                        &nbsp;
                    </TD>
                    <TD style="width:20px;" align="right">
                        <FONT style="color: white;">Zeichen:
                        </FONT>
                    </TD>
                    <TD style="width:20px;" align="right">
                        <FONT style="color: white;"><DIV style="white-space:nowrap; width:20px" id="zeichen"><%= msg.length()%></DIV></FONT>
                    </TD>
                    <TD style="width:20px;" align="left">
                        <FONT style="color: white;">/<%= AdministrationService.NOTE_MAX_LENGTH %>
                        </FONT>
                    </TD>
                </TR>
                <TR>
                    <TD style="width:20px; height: 100%;" colspan="4">
                        <textarea name="msg" style="width:100%; height: 100%;" onBlur="docounter()" onkeyup="docounter()" onChange="docounter()" ><%= msg.replace("<BR>", "\n")%></textarea>
                    </TD>
                </TR>
                <TR>
                    <TD colspan="4" align="center" style="width:10%">
                        <INPUT  type="submit" value="�ndern"/>
                    </TD>
                </TR>
            </TABLE>
        </FORM>
        <%= planetId%>
    </BODY>
</HTML>