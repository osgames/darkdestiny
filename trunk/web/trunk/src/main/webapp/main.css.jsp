<%@ page contentType="text/css" %>
<%@page import="at.darkdestiny.core.*"%>
/*  ... Style-Sheet-Angaben ... */
a:link { color:#C0C0C0; font-weight:bold; font-size:13; }
a:visited { color:#C0C0C0; font-weight:bold; font-size:13; }
a:active { color:#C0C0C0; font-weight:bold; font-size:13; }
a:hover { color:#ffffff; font-weight:bold; font-size:13; }
a { text-decoration:none; font-size:13; }
BODY { color:#ffffff; font-size:13; font-family: Helvetica; background-image:url(<%=GameConfig.getInstance().picPath()%>pic/bg2.jpg);
       background-attachment:fixed; padding:0px;}
img.pic1 { border-width:2px; background-color:#000000; border-style:solid; padding:4px; margin:0px; }
tr.heading { font-weight:bold; background-color:#3366FF; }
td.nb { border-width:0px; }
td.blue { border-style:solid; border-width:thin; border-color:#330099; background-color:#3366FF; }
td.gold { border-style:solid; border-width:thin; border-color:#daa520; background-color:#ff8c00; }
td.violet { border-style:solid; border-width:thin; border-color:#770077; background-color:#990099; }
td.red { border-style:solid; border-width:thin; border-color:#770000; background-color:#990000; }
td.blue2 { border-style:hidden; border-width:0px; background-color:#3366FF; font-size:12px; }
td.violet2 { border-style:hidden; border-width:0px; background-color:#990099; font-size:12px; }
td.red2 { border-style:hidden; border-width:0px; background-color:#990000; font-size:12px; }
td.blue3 { border-style:hidden; border-width:0px; background-color:#3366FF; font-size:13px; }
td.bluebottomline {  border-bottom-style: solid; border-bottom-width: thin; border-bottom-color: #330099; }
td.lightblue { border-style:solid; border-width:thin; border-color:#330099; background-color:#6666CC; }
td.red { font-weight:bold; border-style:solid; border-width:thin; border-color:#660000; background-color:#990033; }
tr.blue2 { border-style:hidden; border-width:0px; background-color:#3366FF; }
tr.gray2 { border-style:hidden; border-width:0px; background-color:#999999; }
tr.lightblue { border-style:hidden; border-width:0px; background-color:#6666CC; }
tr.red { border-style:hidden; border-width:0px; background-color:#990033; }
span.dark {background: #000000; background-color: #000000; color: #ffffff; height:17px; border:0px; text-align: center;}
select.dark {background: #000000; background-color: #000000; color: #ffffff; height:17px; border:0px; text-align: center;}
option {border:0px; }
input.dark { background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 11px; }
input.darklarge { background-color:#000000; border-style: solid; border-width:thin; border-color:#999999;  color: white; text-align: right; font-size: 14px; }
table.blue { 
    border-style:solid;
    border-width:1px;
    border-color:#330099;
    font-size:13px;
    border-spacing:0px;
    padding:0px;
    spacing:0px; }
table.gold { 
    border-style:solid;
    border-width:1px;
    border-color:#daa520;
    font-size:13px;
    border-spacing:0px;
    padding:0px;
    spacing:0px; }    
table.trans { 
    font-size:13px; 
    border-spacing:0px;
    cell-spacing:0px;
    cell-padding:0px;
    background-image: url(http://www.thedarkdestiny.at/tableBgd.png);background-repeat: repeat; }
table.bluetrans { 
    border-style:solid; 
    border-width:thin; 
    border-color:#330099; 
    font-size:13px; 
    border-spacing:0px;
    cell-spacing:0px;
    cell-padding:0px;
    background-image: url(http://www.thedarkdestiny.at/tableBgd.png);background-repeat: repeat; }
table.deftrans { 
    font-size:13px; 
    border-spacing:0px; 
    background-image: url(http://www.thedarkdestiny.at/tableBgd.png);background-repeat: repeat; }       
table.deftransb2 { 
    font-size:13px; 
    border-spacing:2px; 
    background-image: url(http://www.thedarkdestiny.at/tableBgd.png);background-repeat: repeat; }   
table.sortable thead {
 border-style:solid; 
    border-width:thin; 
    border-color:#330099; 
    font-size:13px; 
    border-spacing:0px; 
    background-image: url(http://www.thedarkdestiny.at/tableBgd.png);background-repeat: repeat; 
}
table.violet { border-style:solid; border-width:thin; border-color:#770077; font-size:13px; border-spacing:0px; }
table.red { border-style:solid; border-width:thin; border-color:#992222; font-size:13px; border-spacing:0px; }
table.trade { border-style:solid; border-width:thin; border-color:#999999; font-size:11px; font-weight:bolder; border-spacing:0px; }
table.gray { border-style:solid; border-width:thin; border-color:#999999; font-size:13px; border-spacing:0px; }
table.lightblue { border-style:solid; border-width:thin; border-color:#330099; font-size:13px; border-spacing:0px; }
table.red { border-style:solid; border-width:thin; border-color:#660000; font-size:13px; border-spacing:0px; }
font.normal { font-size:13; }
#data0 { margin-left:55px; }
#data1 { color:#FFFFFF; margin-left:50px; }
#data2 { color:#FFFFFF; margin-left:50px; }
#data3 { color:#FFFFFF; margin-left:50px; }
#data4 { color:#FFFFFF; margin-left:50px; }
#data5 { color:#FFFFFF; margin-left:50px; }
#data6 { color:#FFFFFF; margin-left:50px; }
#data7 { color:#FFFFFF; margin-left:50px; }
#data8 { color:#FFFFFF; margin-left:50px; }
#data9 { color:#FFFFFF; margin-left:50px; }
#data10 { color:#FFFFFF; margin-left:50px; }
#data11 { color:#FFFFFF; margin-left:50px; }
#data12 { color:#FFFFFF; margin-left:50px; }
#data13 { color:#FFFFFF; margin-left:50px; }
 
/*Das Menu der linken Seite*/
#menu1 { margin-left:18px;}
#menu2 { margin-left:18px; }
#menu3 { margin-left:18px; }
#menu4 { margin-left:18px; }
#menu5 { margin-left:18px; }
#menu6 { margin-left:18px; }
#menu7 { margin-left:18px; }
#menu8 { margin-left:18px; }
#menu9 { margin-left:18px; }
#menu10 { margin-left:18px; }
#menu11 { margin-left:18px; }
#menu12 { margin-left:18px; }
#menu13 { margin-left:18px; }
#menu14 { margin-left:18px; }
#menu15 { margin-left:18px; }
#menu16 { margin-left:18px; }
#menu17 { margin-left:41px; }
#menu18 { margin-left:26px; }
 
/* Wie sieht der ToolTip aus? */
div#toolTip {
  position:absolute; visibility:hidden; left:0; top:0; z-index:10000;
  background-color:#6666FF; border:1px solid #000000; 
  padding:3px;
  color:#FFFFFF; font-size:11px; line-height:1.0;
}
/* Der Inhalt des ToolTips wird wie folgt dargestellt */
div#toolTip div.img { text-align:center }
div#toolTip div.txt { text-align:center; margin-top:4px }


/* added by Craft4 for <input> Fields mit dem dunklen DD Hintergrund */
input {background-color: lightblue; color: black; }
option.light {background-color: lightblue; color: black; }
select.light {background-color: lightblue; color: black; }

/* some other useful stuff for html formatting */
.red_centered_text { text-align: center; color: red;}

.right_text { text-align: right }
.centered_text { text-align: center }

.blue_text { text-align: left; color: lightblue;}
.red_text { color: red;}
.orange_text { color: orange;}

div.dark {
    background-color: black;
    filter:alpha(opacity=50);
    opacity: 0.5;
    -moz-opacity:0.50;
    z-index: 30;
    height: 100%;
    width: 100%;
    background-repeat:no-repeat;
    background-position:center;
    position:absolute;
    text-align:center;
    top:0px;
    left:0px;
    border:0px;
}

div.helper {
    top: 100;
    width: 100%;
    margin: 0;
    border: none;
    position:absolute;
    text-align:center;
    z-index: 40;
}

div.starmapdetail {
    background-color: black;
    filter:alpha(opacity=90);
    display: inline-block;
    opacity: 0.9;
    -moz-opacity:0.9;
    z-index: 50;
    margin: 0 auto;
    border: none;
    position: relative;
}