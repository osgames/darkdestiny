
<%@page import="at.darkdestiny.core.GameConfig"%>
<%@page import="at.darkdestiny.util.DebugBuffer.DebugLevel"%>
<%@page import="at.darkdestiny.util.DebugBuffer"%>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.model.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.utilities.*" %>
<%@page import="at.darkdestiny.core.GameUtilities" %>
<%@page import="at.darkdestiny.core.enumeration.*" %>
<%@page import="at.darkdestiny.core.ML" %>
<%@page import="java.util.*" %>
<%@page import="org.jfree.chart.*"%>
<%@page import="at.darkdestiny.util.*"%>

<%

            final int TYPE_SHOWALL = 0;
            final int TYPE_SHOWARCHIVED = 2;
            final int TYPE_SHOWGROUNDCOMBAT = 1;
            final int PROCESS_ARCHIVE = 1;
            final int PROCESS_DELETE = 2;
            int userId = Integer.parseInt((String) session.getAttribute("userId"));

            int type = 0;
            if (request.getParameter("subType") != null) {
                type = Integer.parseInt(request.getParameter("subType"));
            }
            int process = 0;
            if (request.getParameter("process") != null) {
                process = Integer.parseInt(request.getParameter("process"));
            }
            BaseResult br = null;
            if(process > 0){
                switch(process){
                    case(PROCESS_ARCHIVE):
                        br = GroundCombatService.archiveReport(userId, request.getParameterMap());
                        break;
                    case(PROCESS_DELETE):
                        br = GroundCombatService.deleteReport(userId, request.getParameterMap());
                        break;
                    }
                }
            if(br != null){ %>

                <CENTER><%= br.getFormattedString() %></CENTER>

                <%
                }
            if(true){
            %>
         <h3><%= ML.getMLStr("battlereport_lbl_groundcombats", userId)%></h3>
         <TABLE class="blue2" width="60%" align="center">
                <TR>
                    <TD class="blue2" align="center">
                        <a href="main.jsp?page=defensemenu&type=3&subType=<%= TYPE_SHOWALL %>">Reports</a>
                    </TD>
                    <TD class="blue2" align="center">
                        <a href="main.jsp?page=defensemenu&type=3&subType=<%= TYPE_SHOWARCHIVED %>">Archiv</a>
                    </TD>
                </TR>
                <TR>
                     <TD>
                      &nbsp;
                     </TD>
                </TR>
         </TABLE>

            <%
            }

            if(type == TYPE_SHOWALL || type == TYPE_SHOWARCHIVED){
%>

<TABLE width="80%" class="bluetrans" style="font-size: 13px">
    <TR class="blue2">
        <TD>
            <%= ML.getMLStr("battlereport_lbl_starttime", userId)%>
        </TD>
        <TD>
            <%= ML.getMLStr("battlereport_lbl_status", userId)%>
        </TD>
        <TD>
            <%= ML.getMLStr("battlereport_lbl_planet", userId)%>
        </TD>
        <TD>
            <%= ML.getMLStr("battlereport_lbl_rounds", userId)%>
        </TD>
        <TD colspan="2">
            <%= ML.getMLStr("battlereport_lbl_players", userId)%>
        </TD>
        <TD>
            <%= ML.getMLStr("battlereport_lbl_actions", userId)%>
        </TD>

    </TR>
    <%
                        for (GroundCombat gc : (ArrayList<GroundCombat>) Service.groundCombatDAO.findAllSorted()) {
                            ArrayList<CombatPlayer> players = Service.combatPlayerDAO.findByCombatId(gc.getId());
                            boolean dontShow = false;
                            if (!Service.userDAO.findById(userId).getAdmin()) {
                                boolean participating = false;
                                for (CombatPlayer player : players) {
                                    if (player.getUserId() == userId) {
                                        if(player.getReportStatus().equals(ECombatReportStatus.DELETED)
                                           || (type == TYPE_SHOWALL && player.getReportStatus().equals(ECombatReportStatus.ARCHIVED))
                                           || (type == TYPE_SHOWARCHIVED && player.getReportStatus().equals(ECombatReportStatus.SHOWING))){
                                            dontShow = true;
                                            break;
                                            }
                                        participating = true;
                                        break;
                                    }

                                }
                                if (!participating || dontShow) {
                                    continue;
                                }
                            }
                            java.util.Date combatDate = new java.util.Date(gc.getStartTime());
                            String combatString = java.text.DateFormat.getDateInstance().format(combatDate) + " " + java.text.DateFormat.getTimeInstance().format(combatDate);
    %>
    <TR>
        <TD valign="top"  class="bluebottomline">
            <%= combatString%>
        </TD>
        <TD valign="top"  class="bluebottomline">
            <%= ML.getMLStr("groundcombat_ECombatStatus_" + gc.getCombatStatus().toString(), userId) %>
        </TD>
        <TD valign="top"  class="bluebottomline ">
            <% if(Service.playerPlanetDAO.findByPlanetId(gc.getPlanetId()) != null){ %>
            <%= Service.playerPlanetDAO.findByPlanetId(gc.getPlanetId()).getName()%>(<%= gc.getPlanetId()%>)
            <% }else{ %>
            Planet #<%= gc.getPlanetId() %>
            <% } %>
        </TD>
        <TD valign="top"  class="bluebottomline">
            <%= Service.combatRoundDAO.findByCombatId(gc.getId()).size()%>
        </TD>
        <TD colspan="2"  class="bluebottomline">
            <TABLE width="100%"  class="blue2" style="font-size: 13px">
                <TR class="blue2">
                    <TD><%= ML.getMLStr("battlereport_lbl_name", userId)%></TD>
                    <TD>
                        <%= ML.getMLStr("battlereport_lbl_starttime", userId)%>
                    </TD>
                    <%
                    CombatPlayer userCombatPlayer = null;
                                                for (CombatPlayer cp : players) {
                                                    if(cp.getUserId() == userId){
                                                        userCombatPlayer = cp;
                                                        }
                                                    java.util.Date playerDate = new java.util.Date(gc.getStartTime());
                                                    String playerString = java.text.DateFormat.getDateInstance().format(playerDate) + " " + java.text.DateFormat.getTimeInstance().format(playerDate);

                    %>
                <TR>
                    <TD>
                        <% if (Service.userDAO.findById(cp.getUserId()) != null) {%>
                        <%= Service.userDAO.findById(cp.getUserId()).getGameName()%><%
                        } else {%>
                        User gel�scht
                        <% }%>

                    </TD>
                    <TD>
                        <%= playerString%>
                    </TD>
                </TR>
                <%
                                            }
                %>

            </TABLE>
        </TD>
        <TD  class="bluebottomline">
            <TABLE>
                <TR>
                    <TD>
                        <% if (Service.combatRoundDAO.findByCombatId(gc.getId()).size() > 0) {%>
                        <A style="font-size: 12px" href="main.jsp?page=defensemenu&type=3&subType=<%= TYPE_SHOWGROUNDCOMBAT%>&gcId=<%= gc.getId()%>"><%= ML.getMLStr("battlereport_link_show", userId)%></A>
                        <% }%>
                    </TD>
                </TR>
                <TR>
                    <TD>
                        <% if(userCombatPlayer != null && !userCombatPlayer.getReportStatus().equals(ECombatReportStatus.ARCHIVED)) { %>
                         <A style="font-size: 12px" href="main.jsp?page=defensemenu&type=3&subType=<%= type %>&process=<%= PROCESS_ARCHIVE %>&gcId=<%= gc.getId()%>"><%= ML.getMLStr("battlereport_link_archive", userId)%></A>
                         <% } %>
                    </TD>
                </TR>
                <TR>
                    <TD>
                        <% if(gc.getCombatStatus().equals(ECombatStatus.FINISHED)) { %>
                        <A style="font-size: 12px" href="main.jsp?page=defensemenu&type=3&subType=<%= type %>&process=<%= PROCESS_DELETE %>&gcId=<%= gc.getId()%>"><%= ML.getMLStr("battlereport_link_delete", userId)%></A>
                         <% } %>
                    </TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
    <%
                        }
    %>
</TABLE>
<%

                }
                if (type == TYPE_SHOWGROUNDCOMBAT) {



                    boolean collapsed = false;
            EGroundCombatChart chartType = EGroundCombatChart.TOTAL;
            if (request.getParameter("chartType") != null) {
                collapsed = true;
                chartType = EGroundCombatChart.valueOf(request.getParameter("chartType"));
            }
                    int gcId = Integer.parseInt(request.getParameter("gcId"));
                    GroundCombat gc = Service.groundCombatDAO.findById(gcId);
                    boolean foundPlayer = false;
                    for(CombatPlayer cp : Service.combatPlayerDAO.findByCombatId(gc.getId())){
                            if(cp.getUserId() == userId){
                                foundPlayer = true;
                                break;
                            }
                        }
                    if(foundPlayer || Service.userDAO.findById(userId).getAdmin()){

                    if (gc.getStartTime() == GameUtilities.getCurrentTick2()) {
%>
Noch keine Runde berechnet
<%        } else {
                        GroundCombatResult gcr = GroundCombatUtilities.getReport(gcId, userId, chartType);

                    String style = "none";
                    String help = "("+ML.getMLStr("statistik_lbl_expand", userId)+")";
                    String img2 = "pic/icons/menu_ein.png";
                    String img1 = "pic/icons/menu_aus.png";

                    if (collapsed) {
                        style = "";

                        img1 = "pic/icons/menu_ein.png";
                        img2 = "pic/icons/menu_aus.png";
                    }
                    String imageMap = "";
                    try{
                    JFreeChart chart = gcr.getChart();
                    if (session.getAttribute("chart") != null) {
                        session.removeAttribute("chart");
                    }
                    session.setAttribute("chart", chart);
    // get ImageMap
                    ChartRenderingInfo info = new ChartRenderingInfo();
    // populate the info
                    if (chart != null) {
                        chart.createBufferedImage(640, 400, info);
                        imageMap = ChartUtilities.getImageMap("map", info);
                    }
                    }catch(Exception e){
                            DebugBuffer.writeStackTrace("Error in battlereport jsp", e);
                        }
                          java.util.Date battleDate = new java.util.Date(gcr.getStartTime());
                                                    String battleString = java.text.DateFormat.getDateInstance().format(battleDate) + " " + java.text.DateFormat.getTimeInstance().format(battleDate);

%>
<TABLE class="bluetrans">
    <TR>
        <TD class="blue2"><%= ML.getMLStr("battlereport_lbl_planet", userId)%></TD><TD> Planet #<%= gcr.getPlanetId() %></TD>
    </TR>
    <TR>
        <TD class="blue2"><%= ML.getMLStr("battlereport_lbl_starttime", userId)%> </TD><TD><%= battleString%></TD>
    </TR>
    <TR>
        <TD class="blue2"><%= ML.getMLStr("battlereport_lbl_status", userId)%></TD><TD> <%= ML.getMLStr("groundcombat_ECombatStatus_" + gcr.getStatus().toString(), userId)%></TD>
    </TR>
    <TR>
        <TD class="blue2"><%= ML.getMLStr("battlereport_lbl_rounds", userId)%></TD><TD> <%= gcr.getRounds()%></TD>
</TR>
</TABLE>
<%

%>
<BR><h3>Short Report</h3><BR>
<TABLE width="80%">

    <TR>
            <TD colspan="2">
                <TABLE width="100%">
                    <TR>
            <TD  valign="middle" width="100px" class="blue2">
                <span onclick="toogleVisibility('chartTr');
                    exchangeImgs('img_chart', '<%=GameConfig.getInstance().picPath()%><%= img2%>',
                    '+','<%=GameConfig.getInstance().picPath()%><%= img1%>','-');">
                    <img src="<%=GameConfig.getInstance().picPath()%><%= img1%>" alt="-" border="0" id="img_chart">&nbsp;
                    <FONT id="text" style="font-size: 10; font-weight: bold; color: white;"><%= help %></FONT>
                </span>
            </TD>
            <TD class="blue2" align="center">
                <B><%= ML.getMLStr("statistic_lbl_chart", userId)%></B>
            </TD>
                    </TR>
                </TABLE>
            </TD>

        </TR>
        <TR style="display:<%= style%>" id="chartTr">
            <TD width="100%" colspan="2">
                <TABLE width="100%">
                    <TR>

                        <TD colspan="4" align="center">
                            <IMG src="ChartViewer" usemap="#map"></TD>
                    </TR>
                    <TR>
                        <TD class="blue3" width="25%" align="center">
                            <A href="main.jsp?page=defensemenu&type=3&subType=1&gcId=<%= gc.getId() %>&chartType=<%= EGroundCombatChart.TOTAL %>"><%= EGroundCombatChart.TOTAL %></A>
                        </TD>
                        <TD class="blue3" width="25%" align="center">
                            <A href="main.jsp?page=defensemenu&type=3&subType=1&gcId=<%= gc.getId() %>&chartType=<%= EGroundCombatChart.UNITS %>"><%= EGroundCombatChart.UNITS %></A>
                        </TD>
                        <TD class="blue3" width="25%" align="center">
                            <A href="main.jsp?page=defensemenu&type=3&subType=1&gcId=<%= gc.getId() %>&chartType=<%= EGroundCombatChart.ATTACKSTRENGTH %>"><%= EGroundCombatChart.ATTACKSTRENGTH %></A>
                        </TD>
                        <TD class="blue3" width="25%" align="center">
                            <A href="main.jsp?page=defensemenu&type=3&subType=1&gcId=<%= gc.getId() %>&chartType=<%= EGroundCombatChart.HITPOINTS %>"><%= EGroundCombatChart.HITPOINTS %></A>
                        </TD>
                    </TR>
                </TABLE>
            </TD>
        </TR>
    <TR>
          <%
                                int user = 0;
                                for (Map.Entry<Integer, TreeMap<Integer, RoundEntry>> startUnits : gcr.getResult().entrySet()) {
                                    user++;
        %>

        <% if ((user % 2) != 0) {%>
    <TR>
        <% }%>
        <TD valign="top">
            <TABLE style="border-style:solid; border-width:thin; border-color:#330099; font-size: 13px" width="100%" >
                <TR>
                    <TD bgcolor="gray" colspan="4">
                        <% if (Service.userDAO.findById(startUnits.getKey()) != null) {%>
                        <%= Service.userDAO.findById(startUnits.getKey()).getGameName()%><%
                        } else {%>
                        User gel�scht
                        <% }%>
                        <% if (gcr.getWinner() == startUnits.getKey()) {%>
                        <B><font color="orange">Gewinner</font></B>
                        <% }%>
                    </TD>
                </TR>
                <TR class="blue2">
                    <TD>
                        Typ
                    </TD>
                    <TD>
                        Anzahl
                    </TD>
                    <TD>
                        �berlebend
                    </TD>
                    <TD>
                        Verloren
                    </TD>
                </TR>
                <%
                                                    for (Integer unitId : gcr.getParticipatingTypesTotal()) {
                                                        RoundEntry startUnit = startUnits.getValue().get(unitId);
                                                        if(startUnit == null){%>
                                                        <TR><TD>&nbsp;</TD><TD></TD><TD></TD></TR>
                                                            <%
                                                        continue;
                                                        }

                %>
                <TR>
                    <TD>
                        <%= ML.getMLStr(Service.groundTroopDAO.findById(startUnit.getTroopId()).getName(), userId)%>
                    </TD>
                    <TD>
                        <%= (startUnit.getNumber())%>
                    </TD>
                    <%

                                                                long dead = startUnit.getDead();
                                                                long survived = startUnit.getNumber() - startUnit.getDead();
                                                                if(survived < 0){
                                                                    dead += survived;
                                                                    survived = 0;
                                                                }
                    %>
                    <TD>
                        <%= survived%>
                    </TD>
                    <TD>
                        <%= dead %>
                    </TD>
                </TR>
                <% }
                %>
            </TABLE>
        </TD>
        <% if (user % 2 == 0) {%>
    </TR>
    <% }%>
    <%
                            }
                                boolean show = true;
    %>
</TR>
</TABLE>

<% if (show) {%>
<h3>Report</h3><BR>
<TABLE width="80%">
    <%
                            int round = 0;
                            for (Map.Entry<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> rEntry : gcr.getHistory().entrySet()) {
                                round++;
                                TreeMap<Integer, TreeMap<Integer,RoundEntry>> re2 = rEntry.getValue();
                                float multiplicator = (float) Math.ceil(((float) round / 10f));
    %>

    <TR class="blue2">
        <TD align="center" colspan="<%= (4 * re2.size())%>">
            Round <%= round%> Schadensmultiplikator = <%= Math.round(multiplicator)%>x
        </TD>
    </TR>
    <TR>
        <%
                    user = 0;
                    for (Map.Entry<Integer, TreeMap<Integer, RoundEntry>> re : re2.entrySet()) {
                        user++;
        %>

        <% if ((user % 2) != 0) {%>
    <TR>
        <% }%>
        <TD valign="top">
            <TABLE width="100%" style="font-size: 13px;">

                <TR>
                    <%
                              double bonus = Math.round((gcr.getTerritoryUser().get(rEntry.getKey()).get(re.getKey()).getTerritorySize() - gcr.getTerritoryUser().get(rEntry.getKey()).get(re.getKey()).getChange()) * 100d);

                    %>
                    <TD bgcolor="gray" colspan="4">
                        <% if (Service.userDAO.findById(re.getKey()) != null) {%>
                        <%= Service.userDAO.findById(re.getKey()).getGameName()%><%
                        } else {%>
                        User gel�scht
                        <% }%>
                        (<%= bonus%>% Bonus)
                    </TD>
                </TR>
                <TR class="blue2">
                    <TD>
                        Typ
                    </TD>
                    <TD>
                        Anzahl
                    </TD>
                    <TD>
                        �berlebend
                    </TD>
                    <TD>
                        Verloren
                    </TD>
                </TR>
                <%
            for (Map.Entry<Integer, RoundEntry> entry : re.getValue().entrySet()) {
                long survived = entry.getValue().getNumber();
                long dead = entry.getValue().getDead();
                if (survived < 0) {
                    dead += survived;
                    survived = 0;
                }
                %>
                <TR>
                    <TD>
                        <%= ML.getMLStr(Service.groundTroopDAO.findById(entry.getValue().getTroopId()).getName(), userId)%>
                    </TD>
                    <TD>
                        <%= (entry.getValue().getNumber() + entry.getValue().getDead())%>
                    </TD>
                    <TD>
                        <%= survived%>
                    </TD>
                    <TD>

                        <%= dead%>
                    </TD>
                </TR>
                <% }
                //   Logger.getLogger().write("Wanna acces : " + rEntry + " => " + re);
                //    Logger.getLogger().write("Wanna acces : " + rEntry.getKey() + " => " + re);
                //   Logger.getLogger().write("Wanna acces : " + rEntry + " => " + re.getKey());
                double terr = 0d;

                try {
                    terr = Math.round(gcr.getTerritoryUser().get(rEntry.getKey()).get(re.getKey()).getChange() * 100d);
                } catch (Exception e) {
                }

                %>
                <TR class="blue2">
                    <% if (terr >= 0) {%>
                    <TD align="center" colspan="2">
                        Gewinn Territorium
                    </TD>
                    <% } else {%>
                    <TD align="center" colspan="2">
                        Verlust Territorium
                    </TD>
                    <% }%>
                    <TD align="center" colspan="2">
                        <%= terr%> %
                    </TD>
                </TR>
            </TABLE>
        </TD>
        <% if (user % 2 == 0) {%>
    </TR>
    <% }%>
    <%
                                    }
    %>
</TR>
<TR>
    <TD>
        &nbsp;
    </TD>
</TR>

<%}%>

</TABLE>
<%
                        }

%>

<% if (session.getAttribute("admin") != null) {%>
<h3>Long Report</h3><BR>
<%= gcr.getReport().toString()%>

<%
                        }
                                }

                }
            }
%>


