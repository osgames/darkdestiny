<%@page import="at.darkdestiny.core.service.IdToNameService"%>
<%@page import="at.darkdestiny.core.ML"%>
<%@page import="at.darkdestiny.core.enumeration.ETerritoryMapShareType"%>
<%@page import="at.darkdestiny.core.enumeration.ERestrictionReason"%>
<%@page import="at.darkdestiny.core.model.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="at.darkdestiny.core.model.TerritoryMapShare"%>
<%@page import="at.darkdestiny.core.GameConfig"%>
<%@page import="at.darkdestiny.core.service.TerritoryService"%>
<%@page import="at.darkdestiny.core.model.TerritoryMap"%>
<%@page import="at.darkdestiny.core.enumeration.ETerritoryMapType"%>
<%@page import="at.darkdestiny.core.service.Service"%>
<%
    int userId = Integer.parseInt((String) session.getAttribute("userId"));

    final int TYPE_SHOW_TERRITORYS = 1;
    final int TYPE_SHARE_TERRITORY = 2;
    final int TYPE_RENAME_TERRITORY = 3;
    final int TYPE_DELETE_TERRITORY = 4;
    
    final int PROCESS_RENAME_TERRITORY = 1;
    final int PROCESS_DELETE_TERRITORY = 2;
    final int PROCESS_CHANGE_SHOW_TERRITORY = 3;

    int type = TYPE_SHOW_TERRITORYS;
    int process = 0;

    if (request.getParameter("subType") != null) {
        type = Integer.parseInt(request.getParameter("subType"));
    }
    if (request.getParameter("process") != null) {
        process = Integer.parseInt(request.getParameter("process"));
    }
    switch(process){
        case(PROCESS_RENAME_TERRITORY):
            TerritoryService.renameTerritory(userId, request.getParameterMap());
            break;
        case(PROCESS_DELETE_TERRITORY):
            TerritoryService.deleteTerritory(userId, request.getParameterMap());
            break;
        case(PROCESS_CHANGE_SHOW_TERRITORY):
            TerritoryService.changeShow(userId, request.getParameterMap());
            break;
    }

    switch (type) {
        case (TYPE_SHOW_TERRITORYS):
%>

<CENTER>

    <B><FONT color="yellow" size="1">Teststadium :) Das Territorium kann auf der Sternenkarte gew�hlt und gee�ndert werden.<BR>
        Hier gel�scht und mit andern Spielern geshared werden. Einige Werte sind noch nicht aktuell das kommt aber noch :)</FONT></B>
</CENTER>
<TABLE width="80%" align="center" class="bluetrans">
    <TR style="background-color: green;">
        <TD align="center" colspan="6">
            <B><%= ML.getMLStr("territory_lbl_ownterritories", userId) %></B>
        </TD>
    </TR>
    <TR class="blue2">
        <TD>
            <%= ML.getMLStr("territory_lbl_name", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_description", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_sharedwith", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_showonstarmap", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_territorytype", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_actions", userId) %>
        </TD>
    </TR>
    <% for (TerritoryMap tm : Service.territoryMapDAO.findBy(userId, ETerritoryMapType.USER)) {
        ArrayList<User> users = Service.territoryMapShareDAO.findUsersByTerritoryIdWithAlliance(tm.getId());
        int amount = users.size();
        amount--;
    
        TerritoryMapShare tms = Service.territoryMapShareDAO.findBy(tm.getId(), userId, ETerritoryMapShareType.USER);
        boolean checked  = false;
        if(tms != null){
         checked = tms.getShow();
               }
        %>
    <TR class="blue">
        <TD>
           <%= tm.getName() %>
        </TD>
        <TD>
           <%= tm.getDescription() %>
        </TD>
        <TD>
            <%= amount %><BR>
        </TD>
        <TD>
            <INPUT type="checkbox" <% if(checked){ %>checked<% } %> onchange="window.location.href = 'main.jsp?page=defensemenu&type=4&process=<%= PROCESS_CHANGE_SHOW_TERRITORY %>&territoryId=<%= tm.getId() %>&change=<%= !checked %>'" />
        </TD>
        <TD>
            <%= tm.getTerritoryType() %>
        </TD>
        <TD>
            <IMG style="cursor:pointer;" alt="Territorium umbenennen" src="<%= GameConfig.getInstance().picPath() %>pic/umbenennen.jpg" onmouseover="doTooltip(event,'<%= ML.getMLStr("territory_pop_rename", userId) %>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=defensemenu&type=4&subType=<%= TYPE_RENAME_TERRITORY %>&territoryId=<%= tm.getId() %>'"/>
            <IMG style="cursor:pointer;" alt="Territorium l�schen" src="<%= GameConfig.getInstance().picPath() %>pic/cancel.jpg" onmouseover="doTooltip(event,'<%= ML.getMLStr("territory_pop_delete", userId) %>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=defensemenu&type=4&subType=<%= TYPE_DELETE_TERRITORY %>&territoryId=<%= tm.getId() %>'"/>
            <IMG style="cursor:pointer;" alt="Territorium teilen" src="<%= GameConfig.getInstance().picPath() %>pic/edit.jpg" onmouseover="doTooltip(event,'<%= ML.getMLStr("territory_pop_share", userId) %>')" onmouseout="hideTip()" onclick="window.location.href = 'main.jsp?page=defensemenu&type=4&subType=<%= TYPE_SHARE_TERRITORY %>&territoryId=<%= tm.getId() %>'"/>
            
        </TD>
    </TR>
    <% }%>
    
</TABLE>
<TABLE width="80%" align="center" class="bluetrans">
    <TR style="background-color: black;">
        <TD align="center" colspan="4">
            <B><%= ML.getMLStr("territory_lbl_otherterritorys", userId) %></B>
        </TD>
    </TR>
    <TR class="blue2">
        <TD>
            <%= ML.getMLStr("territory_lbl_player", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_name", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_description", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_sharedwith", userId) %>
        </TD>
        <TD>
            <%= ML.getMLStr("territory_lbl_showonstarmap", userId) %>
        </TD>
    </TR>
    <% for (TerritoryMapShare tms: Service.territoryMapShareDAO.findByUserId(userId)) {
        TerritoryMap tm = Service.territoryMapDAO.findById(tms.getTerritoryId());
        if(tm.getType().equals(ETerritoryMapType.USER) && tm.getRefId() == userId)continue;
        
        ArrayList<User> users = Service.territoryMapShareDAO.findUsersByTerritoryIdWithAlliance(tm.getId());
        int amount = users.size();
        amount--;
        
        boolean disabled = false;
        if(tms.getType().equals(ETerritoryMapShareType.ALLIANCE)){
            if(!Service.allianceMemberDAO.findByUserId(userId).getIsAdmin()){
                disabled = true;
            }
        }
    
        boolean checked = tms.getShow();
        %>
    <TR class="blue">
        <TD>
            <%= IdToNameService.getUserName(tm.getRefId()) %>
        </TD>
        <TD>
           <%= tm.getName() %>
        </TD>
        <TD>
           <%= tm.getDescription() %>
        </TD>
        <TD>
            <%= amount %>
        </TD>
        <TD>
            <INPUT type="checkbox" disable="<%= disabled %>" <% if(checked){ %>checked<% } %> onchange="window.location.href = 'main.jsp?page=defensemenu&type=4&process=<%= PROCESS_CHANGE_SHOW_TERRITORY %>&territoryId=<%= tm.getId() %>&change=<%= !checked %>'" />
        </TD>
    </TR>
    <% }%>
    
</TABLE>
<% break;
    case (TYPE_RENAME_TERRITORY):
           {
    int territoryId = Integer.parseInt(request.getParameter("territoryId"));
    TerritoryMap tm = Service.territoryMapDAO.findById(territoryId);
%>

<FORM action="main.jsp?page=defensemenu&type=4&process=<%= PROCESS_RENAME_TERRITORY %>&territoryId=<%= territoryId %>"  method="post">
<TABLE align="center" class="bluetrans">
    <TR style="background-color: green;">
        <TD align="center" colspan="2">
            <B><%= ML.getMLStr("territory_lbl_renameterritory", userId) %></B>
        </TD>
    </TR>
    <TR>
        <TD class="blue2">
            <%= ML.getMLStr("territory_lbl_name", userId) %>
        </TD>
        <TD>
            <INPUT name="name" value="<%= tm.getName() %>" />
        </TD>       
    </TR>
    <TR>
        <TD class="blue2">
            <%= ML.getMLStr("territory_lbl_description", userId) %>
        </TD>
        <TD>
            <INPUT name="description" value="<%= tm.getDescription() %>" />
        </TD>       
    </TR>    
    <TR>
        <TD colspan="2">
            <INPUT type="submit" value="<%= ML.getMLStr("territory_but_change", userId) %>" />
        </TD>       
    </TR>  
</TABLE>
</form>
<% }

break;
case (TYPE_DELETE_TERRITORY):
       {
    int territoryId = Integer.parseInt(request.getParameter("territoryId"));
    TerritoryMap tm = Service.territoryMapDAO.findById(territoryId);
    
%>

<FORM action="main.jsp?page=defensemenu&type=4&process=<%= PROCESS_DELETE_TERRITORY %>&territoryId=<%= territoryId %>"  method="post">
<TABLE align="center" class="bluetrans">
    <TR style="background-color: green;">
        <TD align="center">
            <B><%= ML.getMLStr("territory_lbl_deleteterritory", userId) %></B>
        </TD>
    </TR>
    <TR>
        <TD class="blue2">
            <B><%= ML.getMLStr("territory_msg_delete", userId).replace("%NAME%", tm.getName()) %></B>
            
        </TD>      
    </TR>
    
        <TD align="center">
            <INPUT type="submit" value="<%= ML.getMLStr("territory_but_delete", userId) %>" />
        </TD>       
    </TR>  
</TABLE>
</form>
<% 
}
break;

case(TYPE_SHARE_TERRITORY):
    int territoryId = Integer.parseInt(request.getParameter("territoryId"));
    %>
     <jsp:include page="territory-permissions.jsp" >
                    <jsp:param name="territoryId" value='<%= territoryId %>' />
     </jsp:include>
    <%
    
    break;
    }%>