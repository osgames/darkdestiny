<%@page import="at.darkdestiny.core.*" %>
<%@page import="at.darkdestiny.core.databuffer.*" %>
<%@page import="at.darkdestiny.core.construction.*" %>
<%@page import="at.darkdestiny.core.service.*" %>
<%@page import="at.darkdestiny.core.result.*" %>
<%@page import="at.darkdestiny.core.requestbuffer.*" %>
<%@page import="at.darkdestiny.core.model.Ressource" %>
<%@page import="at.darkdestiny.core.model.PlayerPlanet" %>
<%@page import="at.darkdestiny.core.model.Construction" %>
<%@page import="at.darkdestiny.util.*"%>
<%
session = request.getSession();

int userId = Integer.parseInt((String)session.getAttribute("userId"));;
int planetId = Integer.parseInt((String)session.getAttribute("actPlanet"));;
int orderId = 0;
String type = (String)request.getParameter("type");

AbortDeconstructionResult adr = null;
ConstructionScrapAbortCost csac = null;

if (request.getParameter("qi") != null) {
    int qIdent = Integer.parseInt(request.getParameter("qi"));
    DeconstructionOrderAbortBuffer coab = (DeconstructionOrderAbortBuffer)BufferHandling.getBuffer(userId, qIdent);

    coab.setParameter(coab.COUNT, Integer.parseInt(request.getParameter("destroyCount")));
%>
    <jsp:forward page="main.jsp?page=confirm/deconstructionAbortion" />
<%
} else {
    orderId = Integer.parseInt(request.getParameter("orderNo"));

    if (!ConstructionService.orderIdIsValid(orderId)) {
        String url = "main.jsp?page=new/construction&type="+type;
        System.out.println("URL = " + url);
    %>
        <jsp:forward page="<%= url %>" />
    <%
    }    
    
    DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE,"BuildID = "+orderId);

    adr = ConstructionService.getDeconstructionAbortCost(orderId);
    csac = adr.getConsScrapAbortCost();
}

DeconstructionOrderAbortBuffer coab = new DeconstructionOrderAbortBuffer(userId);
coab.setParameter(coab.CONSTRUCTION_ID, adr.getCons().getId());
coab.setParameter(coab.ORDER_ID, orderId);
coab.setParameter(coab.PLANET_ID, planetId);
%>
        <script language="Javascript">
<%
    for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (csac.getRess(r.getId()) > 0) {
            out.write("var " + r.getName() + "Per = " + csac.getRess(r.getId()) + ";");
            out.write("\n");
        }else if( csac.getRess(r.getId()) < 0){
            out.write("var " + r.getName() + "Per = " + (-csac.getRess(r.getId())) + ";");
            out.write("\n");
            }
    }
%>
            var maxCount = <%= adr.getActionEntry().getNumber() %>;

            var actCount = 1;

          //  var totalCredit = db_ressource_creditsPer;
<%
    for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (csac.getRess(r.getId()) > 0||
                csac.getRess(r.getId()) < 0) {
            out.write("var total" + r.getName() + " = " + r.getName() + "Per;");
            out.write("\n");
        }
    }
%>

            function calcTotal() {
                if(actCount > maxCount){
                    actCount = maxCount;
                              <%
                              out.write("document.getElementById(\"destroyCount\").value = number_format(maxCount,0);");
                              %>

                }
            if(actCount < 0 || (!((parseFloat(actCount) == parseInt(actCount)) && !isNaN(actCount)))){
                actCount = 0;
                document.getElementById("destroyCount").value = 0;
            }
                totalCredit = actCount * db_ressource_creditsPer;
<%
        for (Ressource r : OverviewService.findAllStoreableRessources()) {
            if (csac.getRess(r.getId()) > 0||
                csac.getRess(r.getId()) < 0) {
                out.write("total" + r.getName() + " = actCount * " + r.getName() + "Per;");
                out.write("\n");
            }
        }
%>
           //     document.getElementById("db_ressource_creditsPer").innerHTML = number_format(totalCredit,0) + ' Credits';
<%
        for (Ressource r : OverviewService.findAllStoreableRessources()) {
        if (csac.getRess(r.getId()) > 0||
                csac.getRess(r.getId()) < 0) {
            out.write("document.getElementById(\""+r.getName()+"\").innerHTML = number_format(total"+r.getName()+",0);");
            out.write("\n");
        }
    }
%>
            }
        </script>
        <FORM method='post' action='main.jsp?page=abortDeconstruction&qi=<%= coab.getId() %>' name='confirm' >
            <INPUT type="hidden" name="timeFinished" value="<%= orderId %>">
        <TABLE width="80%" style="font-size:13px"><TR>
        <TD colspan=2 align="center"><%= ML.getMLStr("construction_msg_abortdeconstruction", userId)%>.<BR><BR></TD></TR>
        <TR valign="middle">
            <TD width="200"><%= ML.getMLStr("construction_lbl_constructionType", userId) %>:</TD><TD><%= ML.getMLStr(adr.getCons().getName(), userId) %></TD></TR>
        <TR valign="middle"><TD><%= ML.getMLStr("construction_lbl_planet", userId)%>:</TD><TD><%= OverviewService.findPlayerPlanetByPlanetId(planetId).getName() %></TD></TR>
        <TR valign="middle"><TD><%= ML.getMLStr("construction_lbl_count", userId)%></TD><TD><INPUT name="destroyCount" id="destroyCount" value="1" SIZE="1" MAXLENGTH="3" ONKEYUP="actCount=document.getElementById('destroyCount').value;calcTotal();"> / <%= adr.getActionEntry().getNumber() %><BR></TD></TR>
        <TR valign="top">
                      <% if (csac.getRess(Ressource.CREDITS) > 0){
            %>
            <TD><%= ML.getMLStr("construction_lbl_costs", userId)%>:</TD>
            <TD><SPAN id="db_ressource_credits"> <%= FormatUtilities.getFormattedNumber(csac.getRess(Ressource.CREDITS)) %></SPAN><BR></TD>

            <%
            }else{
            %>
             <TD><%= ML.getMLStr("construction_lbl_creditrefund", userId)%></TD>
             <TD><SPAN id="db_ressource_credits"> <%= FormatUtilities.getFormattedNumber(-csac.getRess(Ressource.CREDITS)) %></SPAN><BR></TD>

            <%
            }
            %>
        </TR>

            <TR>
                <TD align="center" colspan=2 class="blue2"><B><%= ML.getMLStr("construction_lbl_ressourcecost", userId)%></B></TD>
            </TR>
            <TR>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_ressource", userId)%></B></TD>
                <TD class="blue2"><B><%= ML.getMLStr("credits_lbl_amount", userId)%></B></TD>
            </TR>
<%
        for (Ressource r : RessourceService.getAllStorableRessources()) {
            if(r.getId() == Ressource.CREDITS)continue;
        if (csac.getRess(r.getId()) > 0) {
            out.write("<TR><TD>"+ML.getMLStr(r.getName(), userId)+"</TD><TD><SPAN id=\""+r.getName()+"\">" + FormatUtilities.getFormattedNumber(csac.getRess(r.getId())) + "</SPAN></TD></TR>");
        }
    }
%>
        <TR><TD colspan=2 align="CENTER"><BR><INPUT type="submit" name="Ok" value="Ok"></TD></TR>
        </TABLE>
        </FORM>
