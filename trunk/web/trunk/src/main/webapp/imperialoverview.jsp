<%@page import="at.darkdestiny.core.enumeration.EConstructionType"%>
<%@page import="at.darkdestiny.core.service.ConstructionService"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="at.darkdestiny.core.*"%>
<%@page import="at.darkdestiny.core.statistic.*"%>
<%@page import="at.darkdestiny.core.model.PlayerStatistic"%>
<%@page import="at.darkdestiny.core.model.GroundTroop"%>
<%@page import="at.darkdestiny.core.model.Chassis"%>
<%@page import="at.darkdestiny.core.model.Ressource"%>
<%@page import="at.darkdestiny.core.model.Construction"%>
<%@page import="at.darkdestiny.core.service.StatisticService"%>
<%@page import="at.darkdestiny.framework.access.*" %>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<%
	session = request.getSession();
int userId = Integer.parseInt((String)session.getAttribute("userId"));
Statistik stat = new Statistik();
PlayerStatistic playerStatistic = stat.calcStatistic(userId, Statistik.WRITE_TO_DB_NO);

Imperialoverview imperial = new Imperialoverview(userId);

float temp = 0;
%>  
<TABLE width="80%">
    <TR align="center">
        <TD width="20%"><U><A href="main.jsp?page=new/statistic&type=1"><%= ML.getMLStr("statistic_link_empires", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=new/statistic&type=2"><%= ML.getMLStr("statistic_link_alliances", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=imperialoverview"><%= ML.getMLStr("statistik_link_internalstatistic", userId)%></A></U></TD>
        <TD width="20%"><U><A href="main.jsp?page=variousStatistics"><%= ML.getMLStr("statistic_link_variousstatistics", userId)%></A></U></TD>
    </TR>
</TABLE>
<BR />
<FONT size=-1><%= ML.getMLStr("statistic_msg_statisticinfo", userId)%><BR>
 
        
<table class="blue" width="80%">
    <thead><tr><td width="100%" class="blue" align="center" colspan=5><B><%= ML.getMLStr("statistic_lbl_management", userId) %></B></td></tr></thead>
<tbody>
 	<tbody>
	<tr>
		<td width="35%" class="blue"> &nbsp; </td>
           	<td width="15%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_empire", userId)%></td>
		<td width="20%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_alliance", userId)%></td>
		<td width="30%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_percentage", userId)%></td>
           
	</tr>			
	<tr>
		<td class="centered_text"><%= ML.getMLStr("statistic_lbl_totalpopulation", userId)%></td>
		<td class="right_text"><%=FormatUtilities.getFormattedNumber(imperial.getUserPopulation()) %></td>
		<td class="right_text"><%=FormatUtilities.getFormattedNumber(imperial.getAllyPopulation()) %></td>
		<td class="centered_text">
                <%   if (imperial.getAllyPopulation() == 0) { temp = 0; }
                     else { temp = 100* (float)imperial.getUserPopulation()/ (float) imperial.getAllyPopulation();}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));                  
                 %>    
                </td>
	</tr>
	<tr>
		<td class="centered_text"><%= ML.getMLStr("statistic_lbl_income", userId)%></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getUserIncome()) %> </td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getAllyIncome()) %></td>
		<td class="centered_text">
                     <%  
                     
                     if (imperial.getAllyIncome() == 0) { temp = 0; }
                     else { temp = 100 * (float)imperial.getUserIncome()/ (float) imperial.getAllyIncome();}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));              
                     //out.write(Float.toString(temp));
                    
                 %>
                    
                </td>
	</tr>
	<tr>
		<td class="centered_text"><%= ML.getMLStr("statistic_lbl_numberofsystems", userId)%></td>
		<td class="right_text" ><%= FormatUtilities.getFormattedNumber(imperial.getUserSystems()) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getAllySystems()) %></td>
		<td class="centered_text">
                     <%   if (imperial.getAllySystems() == 0) { temp = 0; }
                     else { temp = 100 * (float)imperial.getUserSystems()/ (float)imperial.getAllySystems();}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));            
                 %>
                </td>
	</tr>
	<tr>
		<td class="centered_text"><%= ML.getMLStr("statistic_lbl_numberofplanets", userId)%></td>
		<td class="right_text"> <%= FormatUtilities.getFormattedNumber(imperial.getUserPlanets()) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getAllyPlanets()) %></td>
		<td class="centered_text">
                     <%   if (imperial.getAllyPlanets() == 0) { temp = 0; }
                     else { temp = 100* (float)imperial.getUserPlanets()/ (float)imperial.getAllyPlanets();}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));            
                 %>
                </td>
	</tr>
<%
        ArrayList<Ressource> ressources = StatisticService.findAllRessources();
        for(Ressource r : ressources) {
%>        
	<tr>
		<td class="centered_text"><%= ML.getMLStr(r.getName(), userId) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getUserRessource(r.getId())) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getAllyRessource(r.getId())) %></td>
		<td class="centered_text">
                     <%   if (imperial.getAllyRessource(r.getId()) == 0) { temp = 0; }
                     else { temp =100 *(float) imperial.getUserRessource(r.getId())/(float)imperial.getAllyRessource(r.getId());}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));            
                 %>
                    
                </td>
	</tr>
<%
        }
%>        
	</tbody>
</table>
<br />

<table width="80%" class="blue">
        <THEAD><TR><TD width="100%" class="blue" align="center" colspan=5><B><%= ML.getMLStr("statistic_lbl_production", userId)%></B></TD></TR></THEAD>
 	<tbody>
	<tr>
		<td width="35%" class="blue"> &nbsp; </td>
           	<td width="15%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_empire", userId)%></td>
		<td width="20%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_alliance", userId)%></td>
		<td width="30%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_percentage", userId)%></td>
	</tr>
<%
        ressources = StatisticService.findAllRessources();
        for(Ressource r : ressources) {
%>        
	<tr>
		<td class="centered_text"><%= ML.getMLStr(r.getName(), userId) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getUserProduction(r.getId())) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getAllyProduction(r.getId())) %></td>
		<td class="centered_text">
                     <%   if (imperial.getAllyProduction(r.getId()) == 0) { temp = 0; }
                     else { temp =100 *(float) imperial.getUserProduction(r.getId())/(float)imperial.getAllyProduction(r.getId());}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));            
                 %>
                    
                </td>
	</tr>
<%
        }
%>                
	</tbody>
</table>

<br />        

<table width="80%" class="blue">
        <THEAD><TR><TD width="100%" class="blue" align="center" colspan=5><B><%= ML.getMLStr("statistic_lbl_fleet", userId)%></B></TD></TR></THEAD>
 	<tbody>
	<tr>
		<td width="35%" class="blue"> &nbsp; </td>
           	<td width="15%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_empire", userId)%></td>
		<td width="20%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_alliance", userId)%></td>
		<td width="30%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_percentage", userId)%></td>

	</tr>
<%
        ArrayList<Chassis> chassis = StatisticService.findAllChassis();
        for(Chassis c : chassis) {
%>        
	<tr>
            <td class="centered_text"><%= ML.getMLStr(c.getName(), userId) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getUserShipcount(c.getId())) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getAllyShipcount(c.getId())) %></td>
		<td class="centered_text">
                     <%   if (imperial.getAllyShipcount(c.getId()) == 0) { temp = 0; }
                     else { temp = 100*(float)imperial.getUserShipcount(c.getId())/(float)imperial.getAllyShipcount(c.getId());}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));            
                 %>                
                </td>
	</tr>
<%
        }
%>        
	</tbody>
</table>

<br />

<table width="80%"   class="blue">
    <THEAD><TR><TD width="100%" class="blue" align="center" colspan="5"><B><%= ML.getMLStr("statistic_lbl_groundtroops", userId)%></B></TD></TR></THEAD>
 	<tbody>
	<tr>
		<td width="35%" class="blue"> &nbsp; </td>
           	<td width="15%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_empire", userId)%></td>
		<td width="20%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_alliance", userId)%></td>
		<td width="30%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_percentage", userId)%></td>

	</tr>
<%
        ArrayList<GroundTroop> groundTroops = StatisticService.findAllGroundTroops();
        for(GroundTroop gt : groundTroops) {
            
%>        
	<tr>
            <td class="centered_text"><%= ML.getMLStr(gt.getName(), userId) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getUserTroops(gt.getId())) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getAllyTroops(gt.getId())) %></td>
		<td class="centered_text">
                     <%   if (imperial.getAllyTroops(gt.getId()) == 0) { temp = 0; }
                     else { temp = 100*(float) imperial.getUserTroops(gt.getId())/(float)imperial.getAllyTroops(gt.getId());}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));            
                 %>                
                </td>
	</tr>
<%
        }                
%>        
	</tbody>
</table>

<br />

<table width="80%"  class="blue">
  <THEAD><TR><TD width="100%" class="blue" align="center" colspan="5"><B><%= ML.getMLStr("statistic_lbl_defense", userId)%></B></TD></TR></THEAD>
 	<tbody>
	<tr>
		<td width="35%" class="blue"> &nbsp; </td>
           	<td width="15%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_empire", userId)%></td>
		<td width="20%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_alliance", userId)%></td>
		<td width="30%"  class="blue" align="center"><%= ML.getMLStr("statistic_lbl_percentage", userId)%></td>

	</tr>	
<%
    ArrayList<Integer> def = new ArrayList<Integer>();
    ArrayList<Construction> consDefList = ConstructionService.findConstructionByType(EConstructionType.PLANETARY_DEFENSE);
    for (Construction c : consDefList) {
        if (c.getVisible()) {
            def.add(c.getId());
        }
    }  
    
        for(Integer i : def) {
%>        
	<tr>
            <td class="centered_text"><%= ML.getMLStr(StatisticService.findConstructionById(i).getName(), userId) %></td>
        <td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getUserDefence(i)) %></td>
		<td class="right_text"><%= FormatUtilities.getFormattedNumber(imperial.getAllyDefence(i)) %></td>
		<td class="centered_text">
                     <%   if (imperial.getAllyDefence(i) == 0) { temp = 0; }
                     else { temp =  100*(float)imperial.getUserDefence(i)/(float)imperial.getAllyDefence(i);}
                     out.write(FormatUtilities.getFormattedDecimal(temp,2));            
                 %>                
                </td>
	</tr>
<%
        }
%>        
	
	</tbody>
</table>
