/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.view.html.div;
import at.darkdestiny.core.view.html.style;
import at.darkdestiny.core.view.html.styleDivCont;
import at.darkdestiny.core.view.html.table;
import at.darkdestiny.core.view.html.td;
import at.darkdestiny.core.view.html.tr;
import at.darkdestiny.core.view.menu.StatusBarData;
import at.darkdestiny.core.view.menu.statusbar.*;

/**
 *
 * @author Aion
 */
public class PopulationBar extends StatusBarEntity implements IStatusBarEntity {

    public PopulationBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        //Population DivContainers
        styleDivCont positionerPopulation = new styleDivCont("positionerPopulation", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(positionerPopulation);
        String imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_equal.png";
        if (statusBarData.getPopulationFood().getMoral() >= 100) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_verygood.png";
        } else if (statusBarData.getPopulationFood().getMoral() >= 75 && statusBarData.getPopulationFood().getMoral() < 100) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_good.png";
        } else if (statusBarData.getPopulationFood().getMoral() >= 50 && statusBarData.getPopulationFood().getMoral() < 75) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_equal.png";
        } else {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_bad.png";
        }

        String emigrationIconImage = "";
        long migrationChange = statusBarData.getPopulationFood().getGrowthPopulation();
        migrationChange += statusBarData.getPopulationFood().getEmigration();
        if (migrationChange > 0) {
            emigrationIconImage = GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_plus.png";
        } else {
            emigrationIconImage = GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_minus.png";
        }

        styleDivCont planetPopulation = new styleDivCont("planetPopulation", "absolute", 9, 63, 95, 1, 0, 1, "", "");
        styleDivCont emigration = new styleDivCont("emigrationPopulation", "absolute", 22, 63, 95, 1, 0, 1, "", "");
        styleDivCont populationIcon = new styleDivCont("populationIcon", "absolute", 8, 17, 30, 30, 0, 1, imageIcon, "no-repeat");
        styleDivCont emigrationIcon = new styleDivCont("emigrationIcon", "absolute", 25, 58, 10, 10, 0, 1, emigrationIconImage, "no-repeat");

        rootNode.addDivCont(planetPopulation);
        rootNode.addDivCont(populationIcon);
        rootNode.addDivCont(emigration);
        rootNode.addDivCont(emigrationIcon);


        return rootNode;
    }

    @Override
    public table addTr(table table) {

        int height = 50;
        String imageUrl = GameConfig.getInstance().picPath() + "pic/menu/statusbar/stb_population_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ");  position:relative; background-repeat: repeat-x;'";

        td1.setProperties(properties);

        long migrationChange = statusBarData.getPopulationFood().getGrowthPopulation();
        migrationChange += statusBarData.getPopulationFood().getEmigration();
        div positionerPopulation = new div();
        positionerPopulation.setProperties("id='positionerPopulation'");

        div planetPopulation = new div();
        planetPopulation.setProperties("id='planetPopulation' align=right  onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_population", userId) + "')\" onmouseout=\"hideTip()\"");
        planetPopulation.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormatScaledNumber(statusBarData.getPopulationFood().getPlanetPopulation(), 2, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "</FONT>");

        div emigration = new div();
        emigration.setProperties("id='emigrationPopulation' align=right  onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_migration", userId) + " : " + FormatUtilities.getFormatScaledNumber(statusBarData.getPopulationFood().getEmigration(), 2, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "<BR>"
                + "" + ML.getMLStr("statusbar_pop_growth", userId) + " : " + FormatUtilities.getFormatScaledNumber(statusBarData.getPopulationFood().getGrowthPopulation(), 2, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "')\" onmouseout=\"hideTip()\"");
        emigration.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormatScaledNumber(Math.abs(migrationChange), 2, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "</FONT>");


        //Hier wird der ToolTip aufgebaut der produktion verbrauch handel usw anzeigt
        div populationIcon = new div();
        populationIcon.setProperties("id='populationIcon' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_population_morale", userId) + " : " + statusBarData.getPopulationFood().getMoral() + "')\" onmouseout=\"hideTip()\" ");
        positionerPopulation.addDiv(populationIcon);
        div emigrationIcon = new div();
        emigrationIcon.setProperties("id='emigrationIcon' ");
        positionerPopulation.addDiv(emigrationIcon);

        positionerPopulation.addDiv(planetPopulation);
        positionerPopulation.addDiv(emigration);



        td1.addDiv(positionerPopulation);


        tr1.addTd(td1);

        table.addTr(tr1);

        return table;
    }
}
