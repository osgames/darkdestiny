/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.view.html.a;
import at.darkdestiny.core.view.html.div;
import at.darkdestiny.core.view.html.style;
import at.darkdestiny.core.view.html.styleDivCont;
import at.darkdestiny.core.view.html.table;
import at.darkdestiny.core.view.html.td;
import at.darkdestiny.core.view.html.tr;
import at.darkdestiny.core.view.menu.StatusBarData;
import at.darkdestiny.core.view.menu.statusbar.*;

/**
 *
 * @author Aion
 */
public class NoteBar_Small extends StatusBarEntity implements IStatusBarEntity {

    public NoteBar_Small(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        //Population DivContainers
        styleDivCont positionerNote = new styleDivCont("positionerNoteSmall", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(positionerNote);


        styleDivCont privateString = new styleDivCont("noteStringSmall", "absolute", 15, 32, 120, 10, 0, 1, "", "");

        rootNode.addDivCont(privateString);

        String imageIcon = GameConfig.getInstance().picPath() + "pic/menu/statusbar/stb_notessmall_button.png";
        styleDivCont noteIcon = new styleDivCont("noteIconSmall", "absolute", 14, 8, 30, 30, 0, 1, imageIcon, "no-repeat");
        // >>>>>>>>>>>>>

        rootNode.addDivCont(noteIcon);


        //>>>>>>>>><
        return rootNode;
    }

    @Override
    public table addTr(table table) {

        int height = 30;
        String imageUrl = GameConfig.getInstance().picPath() + "pic/menu/statusbar/stb_notessmall_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + ");  position:relative; background-repeat: repeat-x;'";

        td1.setProperties(properties);

        div positionerNote = new div();
        positionerNote.setProperties("id='positionerNoteSmall'");

        div privateString = new div();

        if (statusBarData.getNote() != null && !statusBarData.getNote().getMessage().equals("")) {
            privateString.setProperties("id='noteStringSmall' style='overflow:hidden;width:120px; height:10px;' onmouseover=\"doTooltip(event,'" + statusBarData.getNote().getMessage() + "')\" onmouseout=\"hideTip()\" align=left");
        } else {
            privateString.setProperties("id='noteStringSmall' style='overflow:hidden;width:120px; height:10px;' align=left");
        }
        if (statusBarData.getNote() != null && !statusBarData.getNote().getMessage().equals("")) {
            String message = statusBarData.getNote().getMessage();
            if (message.length() > 18) {
                message = message.substring(0, 18) + "...";
            }
            privateString.setData("<FONT color=#FFFFFF size='-2'><B>" + message + "</B></FONT>");
        } else {
            privateString.setData("<FONT color=#FFFFFF size='-2'><B> </B></FONT>");
        }

        positionerNote.addDiv(privateString);

        div noteIcon = new div();
        noteIcon.setProperties("id='noteIconSmall' onmouseover=\"doTooltip(event,'" + ML.getMLStr("administration_pop_editnote", userId) + "')\" onmouseout=\"hideTip()\" ");


        a privateHref = new a("noteEditor.jsp?planetId=" + statusBarData.getPlanetId());
        privateHref.setPropertes(" onClick='return popupNote(this, \"editor\")' ");
        noteIcon.setHyperlink(privateHref);
        positionerNote.addDiv(noteIcon);
        td1.addDiv(positionerNote);
        td1.setProperties(properties);
        td1.setData("");
        tr1.addTd(td1);

        // =>>>>>>>>>><
        table.addTr(tr1);

        return table;
    }
}
