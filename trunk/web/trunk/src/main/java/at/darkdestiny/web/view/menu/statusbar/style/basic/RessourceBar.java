/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.view.html.div;
import at.darkdestiny.core.view.html.style;
import at.darkdestiny.core.view.html.styleDivCont;
import at.darkdestiny.core.view.html.table;
import at.darkdestiny.core.view.html.td;
import at.darkdestiny.core.view.html.tr;
import at.darkdestiny.core.view.menu.ProductionUsageEntity;
import at.darkdestiny.core.view.menu.RessourceEntity;
import at.darkdestiny.core.view.menu.StatusBarData;
import at.darkdestiny.core.view.menu.StorageEntity;
import at.darkdestiny.core.view.menu.statusbar.*;

/**
 *
 * @author Bullet
 */
public class RessourceBar extends StatusBarEntity implements IStatusBarEntity {

    private static int minLength = 0;
    private static int maxLength = 88;
    private static int barLength;
    String storageImage = "pic/menu/storage";
    String tmpStorageImage = "";

    public RessourceBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    public table addTr(table table) {
        for (int i = 0; i < statusBarData.getRessources().size(); i++) {

            // Logger.getLogger().write("Searching tha traderoutes for this Planet");

            //Adding <TR> for every Ressource
            if (statusBarData.getRessources().get(i).getProductionUsage().getProductionNow() != 0 || statusBarData.getRessources().get(i).getStorage().getStorageStock() != 0 || statusBarData.getRessources().get(i).getTrade().getIncoming() != 0 || statusBarData.getRessources().get(i).getTrade().getOutgoing() != 0) {
                table.addTr(buildRessourcePanel(statusBarData.getRessources().get(i)));
            }

            // Logger.getLogger().write("TradeSize4:  "+tradeRoutes.size());

        }
        return table;
    }

    private tr buildRessourcePanel(RessourceEntity ressource) {

        int height = 50;
        String imageUrl = GameConfig.getInstance().picPath() + "pic/menu/statusbar/stb_ressources_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + "); position:relative; background-repeat: repeat-x;'";



        td1.setProperties(properties);
        div ressPositioner = new div();
        ressPositioner.setProperties("id='" + ressource.getName() + "positioner'");



        //Hier wird der ToolTip aufgebaut der produktion verbrauch handel usw anzeigt
        div ressIcon = new div();

        String ressMouseOver = "id='" + ressource.getName() + "icon' onmouseover=\"doTooltip(event,'";
        ressMouseOver += ML.getMLStr(ressource.getName(), userId) + " <BR> ";
        ressMouseOver += "<TABLE border=0><TR><TD> </TD><TD><FONT size=1pt>" + ML.getMLStr("statusbar_pop_production", userId) + "</FONT></TD><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_usage", userId) + "</FONT></TD><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_total", userId) + "</FONT></TD></TR>";

        String color;
        //##### PRODUCTION
        if (ressource.getProductionUsage().getProductionNow() != 0 || ressource.getProductionUsage().getUsageNow() != 0) {
            ressMouseOver += "<TR><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_planet", userId) + "</FONT></TD>";

            if (ressource.getProductionUsage().getProductionNow() == 0) {
                color = "#000000";
            } else {
                color = "#33FF00";
            }

            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getProductionUsage().getProductionNow() + "</B></FONT></TD>";
            if (ressource.getProductionUsage().getUsageNow() == 0) {
                color = "#000000";
            } else {
                color = "#FFCC00";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + ressource.getProductionUsage().getUsageNow() + "</B></FONT></TD>";

            if (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow() > 0) {
                color = "#33FF00";
            } else if (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow() < 0) {
                color = "#FFCC00";
            } else {
                color = "#000000";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD></TR>";
        }

        //### TRADE
        if (ressource.getTrade().getIncoming() != 0 || ressource.getTrade().getOutgoing() != 0) {
            ressMouseOver += "<TR><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_trade", userId) + "</FONT></TD>";
            if (ressource.getTrade().getIncoming() == 0) {
                color = "#000000";
            } else {
                color = "#33FF00";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + ressource.getTrade().getIncoming() + "</B></FONT></TD>";


            if (ressource.getTrade().getOutgoing() == 0) {
                color = "#000000";
            } else {
                color = "#FFCC00";
            }
            ressMouseOver += " <TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getTrade().getOutgoing() + "</B></FONT></TD>";
            if (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing() > 0) {
                color = "#33FF00";
            } else if (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing() < 0) {
                color = "#FFCC00";
            } else {
                color = "#000000";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing()) + "</B></FONT></TD></TR>";

        }
        //### TRANSPORT
        if (ressource.getTransport().getIncoming() != 0 || ressource.getTransport().getOutgoing() != 0) {
            ressMouseOver += "<TR><TD> <FONT size=1pt> " + "Transport" + "</FONT></TD>";
            if (ressource.getTransport().getIncoming() == 0) {
                color = "#000000";
            } else {
                color = "#33FF00";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + ressource.getTransport().getIncoming() + "</B></FONT></TD>";


            if (ressource.getTransport().getOutgoing() == 0) {
                color = "#000000";
            } else {
                color = "#FFCC00";
            }
            ressMouseOver += " <TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getTransport().getOutgoing() + "</B></FONT></TD>";
            if (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing() > 0) {
                color = "#33FF00";
            } else if (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing() < 0) {
                color = "#FFCC00";
            } else {
                color = "#000000";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing()) + "</B></FONT></TD></TR>";

        }

        //### TOTAL
        ressMouseOver += "<TR><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_total", userId) + "</FONT></TD> ";

        if (ressource.getProductionUsage().getProductionNow() + ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming() == 0) {
            color = "#000000";
        } else {
            color = "#33FF00";
        }

        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + (ressource.getProductionUsage().getProductionNow() + ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming()) + "</B></FONT></TD>";

        if (ressource.getTrade().getOutgoing() + ressource.getProductionUsage().getUsageNow() + ressource.getTransport().getOutgoing() == 0) {
            color = "#000000";
        } else {
            color = "#FFCC00";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getOutgoing() + ressource.getProductionUsage().getUsageNow() + ressource.getTransport().getOutgoing()) + "</B></FONT></TD>";
        if (ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getTransport().getOutgoing() - ressource.getProductionUsage().getUsageNow() > 0) {
            color = "#33FF00";
        } else if (ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getTransport().getOutgoing() - ressource.getProductionUsage().getUsageNow() < 0) {
            color = "#FFCC00";
        } else {
            color = "#000000";
        }


        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getTransport().getOutgoing() - ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD></TR></TABLE>";


        ressMouseOver += "')\" onmouseout=\"hideTip()\" ";
        ressIcon.setProperties(ressMouseOver);
        /*  Properties(
         "id='" + ressource.getName() + "icon' onmouseover=\"doTooltip(event,'" + ressource.getName() + "')\" onmouseout=\"hideTip()\" ");
         */


        /**
         * Lagerbestand + ToolTip
         *
         */
        ressPositioner.addDiv(ressIcon);

        div ressStock = new div();
        ressStock.setProperties("id='" + ressource.getName() + "stock'  align=right");
        ressStock.setData(
                "<FONT color=#FFFFFF size='-2'>"
                + (FormatUtilities.getFormatScaledNumber(
                ressource.getStorage().getStorageStock(), 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId))) + "</FONT>");

        ressPositioner.addDiv(ressStock);

        div minStock = new div();
        minStock.setProperties("id='" + ressource.getName() + "minStock'  align=left");
        minStock.setData("");

        ressPositioner.addDiv(minStock);


        div minStock_Trade = new div();
        minStock_Trade.setProperties("id='" + ressource.getName() + "minStock_Trade'  align=left");
        minStock_Trade.setData("");

        ressPositioner.addDiv(minStock_Trade);

        div minStock_Transport = new div();
        minStock_Transport.setProperties("id='" + ressource.getName() + "minStock_Transport'  align=left");
        minStock_Transport.setData("");

        ressPositioner.addDiv(minStock_Transport);

        div ressStorage = new div();
        ressStorage.setProperties(
                "id='" + ressource.getName() + "storage' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_storeusage", userId) + ": <BR>" + (ressource.getStorage().getPercent() + "% von " + FormatUtilities.getFormatScaledNumber(ressource.getStorage().getStorageCapicity(), 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId))) + "')\" onmouseout=\"hideTip()\" ");
        ressPositioner.addDiv(ressStorage);





        td1.addDiv(ressPositioner);


        tr1.addTd(td1);

        return tr1;

    }

    public style addStyle(style rootNode) {
        for (int i = 0; i < statusBarData.getRessources().size(); i++) {

            //Length of StorageBarImage

            RessourceEntity ressource = statusBarData.getRessources().get(i);

            styleDivCont ressPositioner = new styleDivCont(
                    ressource.getName() + "positioner", "relative", -23, 0, 1, 1, 0, 1, "", "");
            rootNode.addDivCont(ressPositioner);



            styleDivCont icon = new styleDivCont(
                    ressource.getName() + "icon", "absolute", 8, 17, 30, 30, 0, 1,
                    GameConfig.getInstance().picPath() + ressource.getRessource().getImageLocation(), "no-repeat");
            rootNode.addDivCont(icon);

            // If Production > Usage GreenImage
            tmpStorageImage = changeStorageImage(storageImage, ressource);

            // Calculating StorageBarImageLength X_x

            barLength = calculateStorageBarLength(minLength, maxLength, ressource.getStorage());

            styleDivCont stock = new styleDivCont(
                    ressource.getName() + "stock", "absolute", 9, 63, 95, 1, 0, 1, "", "");
            rootNode.addDivCont(stock);

            styleDivCont storage = new styleDivCont(
                    ressource.getName() + "storage", "absolute", 26, 58, barLength, 9, 0, 1,
                    GameConfig.getInstance().picPath() + tmpStorageImage, "no-repeat");
            rootNode.addDivCont(storage);

            /**
             *
             * Berechnung der Position f�r den MindestBestand einer Ressource
             */
            int minStockPositionShift = 58;
            int maxStockPositionShift = 86;
            int positionShift = 0;
            if (ressource.getStorage().getStorageMin_Production() > ressource.getStorage().getStorageCapicity()) {
                positionShift = maxStockPositionShift;
            } else if (ressource.getStorage().getStorageCapicity() > 0) {
                positionShift = (int) (maxStockPositionShift * ressource.getStorage().getStorageMin_Production() / ressource.getStorage().getStorageCapicity());

            }

            int minStockPosition_production = minStockPositionShift + positionShift;

            styleDivCont minStock_Production = new styleDivCont(
                    ressource.getName() + "minStock", "absolute", 26, minStockPosition_production, 2, 6, 0, 4,
                    GameConfig.getInstance().picPath() + "pic/menu/minStock.png", "no-repeat");
            rootNode.addDivCont(minStock_Production);

            positionShift = 0;
            if (ressource.getStorage().getStorageMin_Trade() > ressource.getStorage().getStorageCapicity()) {
                positionShift = maxStockPositionShift;
            } else if (ressource.getStorage().getStorageCapicity() > 0) {
                positionShift = (int) (maxStockPositionShift * ressource.getStorage().getStorageMin_Trade() / ressource.getStorage().getStorageCapicity());

            }

            int minStockPosition_Trade = minStockPositionShift + positionShift;

            styleDivCont minStock_Trade = new styleDivCont(
                    ressource.getName() + "minStock_Trade", "absolute", 26, minStockPosition_Trade, 2, 6, 0, 3,
                    GameConfig.getInstance().picPath() + "pic/menu/minStock_Trade.png", "no-repeat");
            rootNode.addDivCont(minStock_Trade);

            positionShift = 0;
            if (ressource.getStorage().getStorageMin_Transport() > ressource.getStorage().getStorageCapicity()) {
                positionShift = maxStockPositionShift;
            } else if (ressource.getStorage().getStorageCapicity() > 0) {
                positionShift = (int) (maxStockPositionShift * ressource.getStorage().getStorageMin_Transport() / ressource.getStorage().getStorageCapicity());

            }

            int minStockPosition_Transport = minStockPositionShift + positionShift;

            styleDivCont minStock_Transport = new styleDivCont(
                    ressource.getName() + "minStock_Transport", "absolute", 26, minStockPosition_Transport, 2, 6, 0, 2,
                    GameConfig.getInstance().picPath() + "pic/menu/minStock_Transport.png", "no-repeat");
            rootNode.addDivCont(minStock_Transport);
        }
        return rootNode;
    }

    private String changeStorageImage(String storageImage, RessourceEntity ressource) {

        ProductionUsageEntity productionUsage = ressource.getProductionUsage();

        // Logger.getLogger().write("TradeSize4:  "+tradeRoutes.size());
        if (productionUsage.getProductionNow() + ressource.getTransport().getIncoming() + ressource.getTrade().getIncoming() > productionUsage.getUsageNow() + ressource.getTrade().getOutgoing() + ressource.getTransport().getOutgoing()) {
            storageImage += "Plus";
        }
        // If Production < Usage RedImage
        if (productionUsage.getProductionNow() + ressource.getTransport().getIncoming() + ressource.getTrade().getIncoming() < productionUsage.getUsageNow() + ressource.getTrade().getOutgoing() + ressource.getTransport().getOutgoing()) {
            storageImage += "Minus";
        }
        // Else normale picture
        storageImage += ".png";

        return storageImage;
    }

    public int calculateStorageBarLength(int minLength, int maxLength, StorageEntity storage) {

        if (storage.getStorageCapicity() > 0) {
            double stock = (double) storage.getStorageStock();
            double capicity = (double) storage.getStorageCapicity();

            double multiplicator = (stock / capicity);
            minLength = minLength + (int) Math.round(multiplicator * maxLength);
            storage.setPercent(Math.round(100 * multiplicator));
        }
        return Math.min(minLength, maxLength);
    }
}
