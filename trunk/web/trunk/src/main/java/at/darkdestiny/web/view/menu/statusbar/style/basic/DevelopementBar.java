/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.view.html.div;
import at.darkdestiny.core.view.html.style;
import at.darkdestiny.core.view.html.styleDivCont;
import at.darkdestiny.core.view.html.table;
import at.darkdestiny.core.view.html.td;
import at.darkdestiny.core.view.html.tr;
import at.darkdestiny.core.view.menu.StatusBarData;
import at.darkdestiny.core.view.menu.statusbar.*;
import at.darkdestiny.util.DebugBuffer;

/**
 *
 * @author Aion
 */
public class DevelopementBar extends StatusBarEntity implements IStatusBarEntity {

    public DevelopementBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        //Population DivContainers
        styleDivCont positionerPopulation = new styleDivCont("positionerDevelopement", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(positionerPopulation);
        String imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/happiness.png";

        String emigrationIconImage = "";
        if (statusBarData.getDevelopement().getPlanetPoints() >= 0) {
            emigrationIconImage = GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_plus.png";
        } else {
            emigrationIconImage = GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_minus.png";
        }

        styleDivCont globalCredits = new styleDivCont("globalDev", "absolute", 9, 63, 95, 1, 0, 1, "", "");
        styleDivCont changeCredits = new styleDivCont("changeDev", "absolute", 22, 63, 95, 1, 0, 1, "", "");
        styleDivCont creditsIcon = new styleDivCont("devIcon", "absolute", 8, 17, 30, 30, 0, 1, imageIcon, "no-repeat");
        styleDivCont creditsChangeIcon = new styleDivCont("devChangeIcon", "absolute", 25, 58, 10, 10, 0, 1, emigrationIconImage, "no-repeat");

        rootNode.addDivCont(globalCredits);
        rootNode.addDivCont(changeCredits);
        rootNode.addDivCont(creditsIcon);
        rootNode.addDivCont(creditsChangeIcon);


        return rootNode;
    }

    @Override
    public table addTr(table table) {

        int height = 50;
        String imageUrl = GameConfig.getInstance().picPath() + "pic/menu/statusbar/stb_credit_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + "); position:relative; background-repeat: repeat-x;'";

        td1.setProperties(properties);

        div positionerPopulation = new div();
        positionerPopulation.setProperties("id='positionerDevelopement'");

        div gloabalCredits = new div();
        gloabalCredits.setProperties("id='globalDev' align=right  onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_developementglobal", userId) + "')\" onmouseout=\"hideTip()\"");
        gloabalCredits.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormatScaledNumber(statusBarData.getDevelopement().getGlobalPoints(), 2, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "</FONT>");

        div planetaryincome = new div();
        planetaryincome.setProperties("id='changeDev' align=right  onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_developementplanetary", userId) + "')\" onmouseout=\"hideTip()\"");
        planetaryincome.setData("<FONT color=#FFFFFF size='-2'>" + FormatUtilities.getFormattedDecimal(statusBarData.getDevelopement().getPlanetPoints(), 2, ML.getLocale(userId)) + "</FONT>");


        //Hier wird der ToolTip aufgebaut der produktion verbrauch handel usw anzeigt
        div creditsIcon = new div();
        creditsIcon.setProperties("id='devIcon' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_developementpoints", userId) + "')\" onmouseout=\"hideTip()\" ");
        positionerPopulation.addDiv(creditsIcon);
        div creditsChangeIcon = new div();
        creditsChangeIcon.setProperties("id='devChangeIcon' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_developementplanetary", userId) + "')\" onmouseout=\"hideTip()\" ");
        positionerPopulation.addDiv(creditsChangeIcon);

        positionerPopulation.addDiv(gloabalCredits);
        positionerPopulation.addDiv(planetaryincome);



        td1.addDiv(positionerPopulation);


        tr1.addTd(td1);

        table.addTr(tr1);

        return table;
    }
}
