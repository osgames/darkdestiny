/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.view.html.div;
import at.darkdestiny.core.view.html.style;
import at.darkdestiny.core.view.html.styleDivCont;
import at.darkdestiny.core.view.html.table;
import at.darkdestiny.core.view.html.td;
import at.darkdestiny.core.view.html.tr;
import at.darkdestiny.core.view.menu.ProductionUsageEntity;
import at.darkdestiny.core.view.menu.RessourceEntity;
import at.darkdestiny.core.view.menu.StatusBarData;
import at.darkdestiny.core.view.menu.StorageEntity;
import at.darkdestiny.core.view.menu.statusbar.*;

/**
 *
 * @author Aion
 */
public class FoodBar extends StatusBarEntity implements IStatusBarEntity {

    public FoodBar(StatusBarData statusBarData, int userId, int width) {
        super(statusBarData, userId, width);
    }

    @Override
    public style addStyle(style rootNode) {
        int minLength = 0;
        int maxLength = 104;
        int maxLengthStorage = 88;
        int barLength;
        /**
         * L?nge der Foodanzeige Storage
         *
         */
        styleDivCont positionerFood = new styleDivCont("positionerFood", "relative", -23, 0, 1, 1, 0, 1, "", "");
        rootNode.addDivCont(positionerFood);
        String storageImage = "pic/menu/storage";
        String tmpStorageImage = "";
        styleDivCont foodicon = new styleDivCont(statusBarData.getPopulationFood().getFood().getRessource().getName() + "icon", "absolute", 8, 17, 30, 30, 0, 1, GameConfig.getInstance().picPath() + "pic/menu/foodicon.png", "no-repeat");

        rootNode.addDivCont(foodicon);

        tmpStorageImage = changeStorageImage(storageImage, statusBarData.getPopulationFood().getFood());
        /**
         * Calculating StorageBarImageLength X_x
         */
        barLength = calculateStorageBarLength(minLength, maxLengthStorage, statusBarData.getPopulationFood().getFood().getStorage());
        styleDivCont foodStorage = new styleDivCont("foodStorage", "absolute", 26, 58, barLength, 9, 0, 1, GameConfig.getInstance().picPath() + tmpStorageImage, "no-repeat");
        rootNode.addDivCont(foodStorage);


        String foodUsageImage = "pic/menu/foodRed.png";
        String foodProductionImage = "pic/menu/foodProduction.png";
        String foodImage = "pic/menu/foodEqual.png";
        int usageBarLength = 0;
        int productionBarLength = 0;

        int productionBarZIndex = 0;

        ProductionUsageEntity productionUsage = statusBarData.getPopulationFood().getFood().getProductionUsage();

        if (productionUsage.getProductionNow() + statusBarData.getPopulationFood().getFood().getTrade().getIncoming() > productionUsage.getUsageNow() + statusBarData.getPopulationFood().getFood().getTrade().getOutgoing()) {

            productionBarLength = maxLength;

            foodUsageImage = foodImage;

            double production = (double) productionUsage.getProductionNow() + (double) +statusBarData.getPopulationFood().getFood().getTrade().getIncoming();
            double usage = (double) productionUsage.getUsageNow() + (double) +statusBarData.getPopulationFood().getFood().getTrade().getOutgoing();
            double multiplicator = (usage / production);
            usageBarLength = minLength + (int) Math.round(multiplicator * maxLength);


        } else if (productionUsage.getProductionNow() + statusBarData.getPopulationFood().getFood().getTrade().getIncoming() < productionUsage.getUsageNow() + statusBarData.getPopulationFood().getFood().getTrade().getOutgoing()) {

            foodProductionImage = foodImage;

            double production = (double) productionUsage.getProductionNow() + (double) +statusBarData.getPopulationFood().getFood().getTrade().getIncoming();
            double usage = (double) productionUsage.getUsageNow() + (double) +statusBarData.getPopulationFood().getFood().getTrade().getOutgoing();
            double multiplicator = (production / usage);

            productionBarLength = minLength + (int) Math.round(multiplicator * maxLength);
            productionBarZIndex = 2;

            usageBarLength = maxLength;
        } else {
            productionBarZIndex = 2;
            foodProductionImage = foodImage;
            productionBarLength = maxLength;
        }
        styleDivCont foodProduction = new styleDivCont("foodProduction", "absolute", 11, 57, productionBarLength, 10, 0, productionBarZIndex, GameConfig.getInstance().picPath() + foodProductionImage, "no-repeat");
        rootNode.addDivCont(foodProduction);

        styleDivCont foodUsage = new styleDivCont("foodUsage", "absolute", 11, 57, usageBarLength, 10, 0, 1, GameConfig.getInstance().picPath() + foodUsageImage, "no-repeat");
        rootNode.addDivCont(foodUsage);
        /**
         *
         * Berechnung der Position f�r den MindestBestand einer Ressource
         */
        int minStockPositionShift = 58;
        int maxStockPositionShift = 86;
        int positionShift = 0;
        if (statusBarData.getPopulationFood().getFood().getStorage().getStorageMin_Production() > statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity()) {
            positionShift = maxStockPositionShift;
        } else if (statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity() > 0) {
            positionShift = (int) (maxStockPositionShift * statusBarData.getPopulationFood().getFood().getStorage().getStorageMin_Production() / statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity());

        }
        int minStockPosition = minStockPositionShift + positionShift;


        styleDivCont minStock = new styleDivCont(
                statusBarData.getPopulationFood().getFood().getName() + "minStock", "absolute", 26, minStockPosition, 2, 6, 0, 4,
                GameConfig.getInstance().picPath() + "pic/menu/minStock.png", "no-repeat");
        rootNode.addDivCont(minStock);
        /////////////////////
        positionShift = 0;
        if (statusBarData.getPopulationFood().getFood().getStorage().getStorageMin_Trade() > statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity()) {
            positionShift = maxStockPositionShift;
        } else if (statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity() > 0) {
            positionShift = (int) (maxStockPositionShift * statusBarData.getPopulationFood().getFood().getStorage().getStorageMin_Trade() / statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity());

        }
        int minStockPosition_Trade = minStockPositionShift + positionShift;


        styleDivCont minStock_Trade = new styleDivCont(
                statusBarData.getPopulationFood().getFood().getName() + "minStock_Trade", "absolute", 26, minStockPosition_Trade, 2, 6, 0, 3,
                GameConfig.getInstance().picPath() + "pic/menu/minStock_Trade.png", "no-repeat");
        rootNode.addDivCont(minStock_Trade);



        /////////////
        positionShift = 0;
        if (statusBarData.getPopulationFood().getFood().getStorage().getStorageMin_Transport() > statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity()) {
            positionShift = maxStockPositionShift;
        } else if (statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity() > 0) {
            positionShift = (int) (maxStockPositionShift * statusBarData.getPopulationFood().getFood().getStorage().getStorageMin_Transport() / statusBarData.getPopulationFood().getFood().getStorage().getStorageCapicity());

        }
        int minStockPosition_Transport = minStockPositionShift + positionShift;


        styleDivCont minStock_Transport = new styleDivCont(
                statusBarData.getPopulationFood().getFood().getName() + "minStock_Transport", "absolute", 26, minStockPosition_Transport, 2, 6, 0, 2,
                GameConfig.getInstance().picPath() + "pic/menu/minStock_Transport.png", "no-repeat");
        rootNode.addDivCont(minStock_Transport);
        return rootNode;
    }

    @Override
    public table addTr(table table) {

        long productionNow = statusBarData.getPopulationFood().getFood().getProductionUsage().getProductionNow() + statusBarData.getPopulationFood().getFood().getTrade().getIncoming();
        long usageNow = statusBarData.getPopulationFood().getFood().getProductionUsage().getUsageNow() + statusBarData.getPopulationFood().getFood().getTrade().getOutgoing();
        String foodProductionToolTip = "'" + ML.getMLStr("statusbar_pop_foodproduction", userId) + ":" + FormatUtilities.getFormatScaledNumber(productionNow, 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + " <br> "
                + "" + ML.getMLStr("statusbar_pop_foodusage", userId) + ":" + FormatUtilities.getFormatScaledNumber(usageNow, 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "'";
        String foodUsageToolTip = "'" + ML.getMLStr("statusbar_pop_foodproduction", userId) + ":" + FormatUtilities.getFormatScaledNumber(productionNow, 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + " <br> "
                + "" + ML.getMLStr("statusbar_pop_foodusage", userId) + ":" + FormatUtilities.getFormatScaledNumber(usageNow, 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "'";
        if (statusBarData.getPopulationFood().getFood().getProductionUsage().getProductionNow()
                == statusBarData.getPopulationFood().getFood().getProductionUsage().getUsageNow()) {

            foodProductionToolTip = "'" + ML.getMLStr("statusbar_pop_usage", userId) + " = " + ML.getMLStr("statusbar_pop_production", userId) + ":" + FormatUtilities.getFormatScaledNumber(productionNow, 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "'";
            foodUsageToolTip = "'" + ML.getMLStr("statusbar_pop_usage", userId) + " = " + ML.getMLStr("statusbar_pop_production", userId) + ":" + FormatUtilities.getFormatScaledNumber(usageNow, 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "'";

        }

        int height = 50;
        String imageUrl = GameConfig.getInstance().picPath() + "pic/menu/statusbar/stb_food_background.png";

        String properties = "";

        tr tr1 = new tr();
        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + imageUrl + "); position:relative; background-repeat: repeat-x;'";

        td1.setProperties(properties);

        div positionerFood = new div();
        positionerFood.setProperties("id='positionerFood'");

        // Nahrungsmouse Over

        RessourceEntity ressource = statusBarData.getPopulationFood().getFood();

        //Hier wird der ToolTip aufgebaut der produktion verbrauch handel usw anzeigt
        div ressIcon = new div();

        String ressMouseOver = "id='" + ressource.getName() + "icon' onmouseover=\"doTooltip(event,'";
        ressMouseOver += ML.getMLStr(ressource.getName(), userId) + " <BR> ";
        ressMouseOver += "<TABLE border=0><TR><TD> </TD><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_production", userId) + "</FONT></TD><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_usage", userId) + "</FONT></TD><TD> <FONT size=1pt>" + ML.getMLStr("statusbar_pop_total", userId) + "</FONT></TD></TR>";

        ressMouseOver += "<TR><TD><FONT size=1pt> " + ML.getMLStr("statusbar_pop_planet", userId) + "</FONT></TD>";

        String color;

        if (ressource.getProductionUsage().getProductionNow() == 0) {
            color = "#000000";
        } else {
            color = "#33FF00";
        }

        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getProductionUsage().getProductionNow() + "</B></FONT></TD>";
        if (ressource.getProductionUsage().getUsageNow() == 0) {
            color = "#000000";
        } else {
            color = "#FFCC00";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right ><FONT  align=right size=1pt color=#000000 ><B>" + ressource.getProductionUsage().getUsageNow() + "</B></FONT></TD>";

        if (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow() > 0) {
            color = "#33FF00";
        } else if (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow() < 0) {
            color = "#FFCC00";
        } else {
            color = "#000000";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getProductionUsage().getProductionNow() - ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD></TR>";

        //#### TRADE
        if (ressource.getTrade().getIncoming() != 0 || ressource.getTrade().getOutgoing() != 0) {
            ressMouseOver += "<TR><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_trade", userId) + "</FONT></TD>";
            if (ressource.getTrade().getIncoming() == 0) {
                color = "#000000";
            } else {
                color = "#33FF00";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + ressource.getTrade().getIncoming() + "</B></FONT></TD>";

            if (ressource.getTrade().getOutgoing() == 0) {
                color = "#000000";
            } else {
                color = "#FFCC00";
            }
            ressMouseOver += " <TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getTrade().getOutgoing() + "</B></FONT></TD>";
            if (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing() > 0) {
                color = "#33FF00";
            } else if (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing() < 0) {
                color = "#FFCC00";
            } else {
                color = "#000000";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getIncoming() - ressource.getTrade().getOutgoing()) + "</B></FONT></TD></TR>";

        }
        //#### TRANSPORT
        if (ressource.getTransport().getIncoming() != 0 || ressource.getTransport().getOutgoing() != 0) {
            ressMouseOver += "<TR><TD> <FONT size=1pt> " + "Transport" + "</FONT></TD>";
            if (ressource.getTransport().getIncoming() == 0) {
                color = "#000000";
            } else {
                color = "#33FF00";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + ressource.getTransport().getIncoming() + "</B></FONT></TD>";

            if (ressource.getTransport().getOutgoing() == 0) {
                color = "#000000";
            } else {
                color = "#FFCC00";
            }
            ressMouseOver += " <TD bgcolor=" + color + " align=right><FONT size=1pt  align=right color=#000000><B>" + ressource.getTransport().getOutgoing() + "</B></FONT></TD>";
            if (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing() > 0) {
                color = "#33FF00";
            } else if (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing() < 0) {
                color = "#FFCC00";
            } else {
                color = "#000000";
            }
            ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTransport().getIncoming() - ressource.getTransport().getOutgoing()) + "</B></FONT></TD></TR>";

        }
        //## total
        ressMouseOver += "<TR><TD> <FONT size=1pt> " + ML.getMLStr("statusbar_pop_total", userId) + "</FONT></TD> ";

        if (ressource.getProductionUsage().getProductionNow() + ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming() == 0) {
            color = "#000000";
        } else {
            color = "#33FF00";
        }

        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT align=right size=1pt color=#000000><B>" + (ressource.getProductionUsage().getProductionNow() + ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming()) + "</B></FONT></TD>";

        if (ressource.getTrade().getOutgoing() + ressource.getProductionUsage().getUsageNow() + ressource.getTransport().getOutgoing() == 0) {
            color = "#000000";
        } else {
            color = "#FFCC00";
        }
        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getOutgoing() + ressource.getProductionUsage().getUsageNow() + ressource.getTransport().getOutgoing()) + "</B></FONT></TD>";
        if (ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getTransport().getOutgoing() - ressource.getProductionUsage().getUsageNow() > 0) {
            color = "#33FF00";
        } else if (ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getTransport().getOutgoing() - ressource.getProductionUsage().getUsageNow() < 0) {
            color = "#FFCC00";
        } else {
            color = "#000000";
        }


        ressMouseOver += "<TD bgcolor=" + color + " align=right><FONT  align=right size=1pt color=#000000 ><B>" + (ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming() + ressource.getProductionUsage().getProductionNow() - ressource.getTrade().getOutgoing() - ressource.getTransport().getOutgoing() - ressource.getProductionUsage().getUsageNow()) + "</B></FONT></TD></TR></TABLE>";


        ressMouseOver += "')\" onmouseout=\"hideTip()\" ";
        ressIcon.setProperties(ressMouseOver);

        //NahrungsmouseOver Ende

        positionerFood.addDiv(ressIcon);


        div minStock = new div();
        minStock.setProperties("id='" + ressource.getName() + "minStock'  align=left");
        minStock.setData("");

        positionerFood.addDiv(minStock);

        div minStock_Trade = new div();
        minStock_Trade.setProperties("id='" + ressource.getName() + "minStock_Trade'  align=left");
        minStock_Trade.setData("");

        positionerFood.addDiv(minStock_Trade);

        div minStock_Transport = new div();
        minStock_Transport.setProperties("id='" + ressource.getName() + "minStock_Transport'  align=left");
        minStock_Transport.setData("");

        positionerFood.addDiv(minStock_Transport);
        div ressStorage = new div();
        ressStorage.setProperties("id='foodStorage' onmouseover=\"doTooltip(event,'" + ML.getMLStr("statusbar_pop_storeusage", userId) + ": " + statusBarData.getPopulationFood().getFood().getStorage().getPercent() + "% " + ML.getMLStr("menu_percentage_of", userId) + " " + FormatUtilities.getFormatScaledNumber(ressource.getStorage().getStorageCapicity(), 0, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId)) + "')\" onmouseout=\"hideTip()\"");
        positionerFood.addDiv(ressStorage);

        div foodProduction = new div();
        foodProduction.setProperties("id='foodProduction' onmouseover=\"doTooltip(event," + foodProductionToolTip + ")\" onmouseout=\"hideTip()\" ");
        positionerFood.addDiv(foodProduction);

        div foodUsage = new div();
        foodUsage.setProperties("id='foodUsage' onmouseover=\"doTooltip(event," + foodUsageToolTip + ")\" onmouseout=\"hideTip()\"");
        positionerFood.addDiv(foodUsage);

        td1.addDiv(positionerFood);


        tr1.addTd(td1);

        table.addTr(tr1);
        return table;
    }

    private double getMultiplicator(double higherValue, double lowerValue) {
        double multiplicator = (lowerValue / higherValue);
        return multiplicator;

    }

    private String changeStorageImage(String storageImage, RessourceEntity ressource) {
        ProductionUsageEntity productionUsage = ressource.getProductionUsage();

        // Logger.getLogger().write("TradeSize4:  "+tradeRoutes.size());
        long increase = productionUsage.getProductionNow() + ressource.getTrade().getIncoming() + ressource.getTransport().getIncoming();
        long decrease = productionUsage.getUsageNow() + ressource.getTrade().getOutgoing() + ressource.getTransport().getOutgoing();

        if (increase > decrease) {
            storageImage += "Plus";
        } else if (increase < decrease) {
            storageImage += "Minus";
        }

        storageImage += ".png";

        return storageImage;
    }

    public int calculateStorageBarLength(int minLength, int maxLength, StorageEntity storage) {

        if (storage.getStorageCapicity() > 0) {
            double stock = (double) storage.getStorageStock();
            double capicity = (double) storage.getStorageCapicity();

            double multiplicator = (stock / capicity);
            minLength = minLength + (int) Math.round(multiplicator * maxLength);
            storage.setPercent(Math.round(100 * multiplicator));
        }
        return Math.min(minLength, maxLength);
    }
}
