/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.util;

import java.util.Properties;

/**
 *
 * @author LAZYPAD
 */
public interface IGameConfig {

    public String loadValue(Properties propertiesFile, String key);

    public boolean loadBooleanValue(Properties propertiesFile, String key);

    public String getDriverClassName();

    public java.lang.String getDatabase();

    public java.lang.String getUsername();

    public java.lang.String getPassword();

    public java.lang.String getHostURL();

    /**
     * @return the duration of a tick in milliseconds!
     */
    public int getTicktime();

    public long getStarttime();

    public String picPath();

    public void rereadValues();

    /**
     * @return the startURL
     */
    public String getStartURL();

    /**
     * @return the srcPath
     */
    public String getSrcPathWeb();

    /**
     * @return the srcPath
     */
    public String getSrcPathClasses();

    /**
     * @return the srcPath
     */
    public String getSrcPathResources();

    /**
     * @return the webserviceURL
     */
    public String getWebserviceURL();


    public boolean isAssignDeletedEmpireToAI();
    
    public boolean isDeleteInactive();
}
