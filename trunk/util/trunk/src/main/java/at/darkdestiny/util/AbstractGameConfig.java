package at.darkdestiny.util;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractGameConfig implements IGameConfig {

    private static final Logger log = LoggerFactory.getLogger(AbstractGameConfig.class);

    /**
     * Parameter der Datenbank-Anbindung:
     */
    protected String driver;
    protected String database;
    protected String username;
    protected String password;
    protected String gamehostURL;
    protected String webserviceURL;
    protected String startURL;
    // For development purposes
    protected String srcPathWeb = "";
    protected String srcPathClasses = "";
    protected String srcPathResources = "";
    protected boolean assignDeletedEmpireToAI = false;
    protected boolean updateTickOnStartup = false;
    protected boolean deleteInactive = false;

    protected String modelPackagePath;
    /**
     * Basis URL der Bilder:
     */
    protected static String picPath = "";
    protected int tickTime = -1;
    protected long startTime = -1;

    public static AbstractGameConfig getInstance() {
        return null;
    }

    public String loadValue(Properties propertiesFile, String key) {
        try {
            String value = propertiesFile.getProperty(key);
            System.out.println("Load [" + key + " => " + value + "] from [" + propertiesFile + "]");
            return value;
        } catch (Exception e) {
            DebugBuffer.error("Error loading setup properties key : " + key + " from file " + propertiesFile);
        }
        return null;
    }

    public boolean loadBooleanValue(Properties propertiesFile, String key) {
        try {
            if (propertiesFile.getProperty(key.toString()).equalsIgnoreCase("true")) {
               System.out.println("Load [" + key + " => true]");
                return true;
            }
        } catch (Exception e) {
            DebugBuffer.error("Error loading setup properties key : " + key + " from file " + propertiesFile);
        }
        DebugBuffer.debug("Load [" + key + " => false]");
        return false;
    }

    public abstract String getDriverClassName();

    public abstract java.lang.String getDatabase();

    public abstract java.lang.String getUsername();

    public abstract java.lang.String getPassword();

    public abstract java.lang.String getHostURL();

    public abstract long getStarttime();

    public abstract String picPath();

    public abstract void rereadValues();

    /**
     * @return the startURL
     */
    public abstract String getStartURL();

    /**
     * @return the srcPath
     */
    public abstract String getSrcPathWeb();

    /**
     * @return the srcPath
     */
    public abstract String getSrcPathClasses();

    /**
     * @return the srcPath
     */
    public abstract String getSrcPathResources();

    /**
     * @return the webserviceURL
     */
    public abstract String getWebserviceURL();

    /**
     * @return the webserviceURL
     */
    public abstract boolean isAssignDeletedEmpireToAI();

    /**
     * @return the webserviceURL
     */
    public abstract boolean isUpdateTickOnStartup();
    
    /**
     * @return the webserviceURL
     */
    public abstract boolean isDeleteInactive();    
}
