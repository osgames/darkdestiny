package at.darkdestiny.util;

import java.text.SimpleDateFormat;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dieser Puffer für log meldungen
 * dabei werden mindesten MIN_SIZE meldungen aufbewahrt,
 * Meldungen werden nach TIMEOUT*(1 + logLevel)
 * wieder entfernt. Damit bleiben schwerwiegendere
 * Fehler länger im log.
 *
 * Der Basis-Timeout ist eine Minute
 *
 *
 * @author martin
 * @author Rayden
 */
public final class DebugBuffer {
    private static final Logger log = LoggerFactory.getLogger(DebugBuffer.class);

    public static DebugLevel logLevel = DebugLevel.TRACE;
    public static int MIN_SIZE = 20000;
    public static int TIMEOUT = 60 * 1000; // = 1 min
    private static long lastSearched = 0;

    public static enum DebugLevel {

        TRACE(0),
        DEBUG(1),
        ERROR(2),
        FATAL_ERROR(3),
        UNKNOWN(4),
        WARNING(5),
        GROUNDCOMBAT(6);
        private final int value;

        private DebugLevel(int value_) {
            value = value_;
        }

        public int getIntValue() {
            return value;
        }
    }

    public static final class DebugLine {

        final String message;
        final String timeStamp;
        final DebugLevel level;
        final boolean isStackTrace;
        final long errorTime;

        public DebugLine(DebugLevel level2, String message2) {
            this(level2, message2, false);
        }

        public DebugLine(DebugLevel error, String string, boolean b) {
            this.level = error;
            this.timeStamp = getTime();
            this.message = string;
            this.isStackTrace = b;
            errorTime = System.currentTimeMillis();
        }

        public DebugLevel getLevel() {
            return level;
        }

        public String getMessage() {
            return message;
        }

        public String getTimeStamp() {
            return timeStamp;
        }

        public long getErrorTime() {
            return errorTime;
        }

        public boolean isTimedOut() {
            return System.currentTimeMillis()
                    > (1 + level.getIntValue()) * TIMEOUT;
        }
    }
    private static final transient ArrayList<DebugLine> entries = new ArrayList<DebugLine>();

    /** Creates a new instance of DebugBuffer */
    private DebugBuffer() {
        // Empty constructor for Utility class
    }

    public static void addLine(Throwable emessage) {
        writeStackTrace("", emessage);
    }

    /**
     *
     * @param msg
     * @deprecated
     */
    public static void addLine(String msg) {
        addLine(new DebugLine(DebugLevel.UNKNOWN, msg));
    }

    public static void addLine(DebugLevel level, String message) {
        if ((level == DebugLevel.ERROR) || (level == DebugLevel.FATAL_ERROR)) {
            writeStackTrace(message, new Exception());
        } else {
            addLine(new DebugLine(level, message));
        }        
    }

    private static String getTime() {
        Date todaysDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = formatter.format(todaysDate);

        return formattedDate;
    }

    public static ArrayList<DebugLine> readBuffer() {
        ArrayList<DebugLine> debugEntries = new ArrayList<DebugLine>();

        synchronized (entries) {
            debugEntries.addAll(entries);
        }

        return debugEntries;
    }

    public static void clear() {
        entries.clear();
    }

    public static void writeStackTrace(String string, Exception e) {
        StringBuffer sb = new StringBuffer(string + e.getMessage() + " (" + e.getClass().getName() + ")");
        for (StackTraceElement ste : e.getStackTrace()) {
            sb.append("<br>" + ste.toString());
        }
        if (e.getCause() != null) {
            sb.append(produceStackTrace(e.getCause()));
        }
        addLine(new DebugLine(DebugLevel.ERROR, sb.toString(), true));
    }

    public static void writeStackTrace(String string, Throwable t) {
        StringBuffer sb = new StringBuffer(string + t.getMessage() + " (" + t.getClass().getName() + ")");
        for (StackTraceElement ste : t.getStackTrace()) {
            sb.append("<br>" + ste.toString());
        }
        if (t.getCause() != null) {
            sb.append(produceStackTrace(t.getCause()));
        }
        addLine(new DebugLine(DebugLevel.ERROR, "[" + getTime() + "] " + sb.toString(), true));
    }

    private static void addLine(DebugLine line) {
        synchronized (entries) {
            try {
                if (line.getLevel().getIntValue() < logLevel.getIntValue()) {
                    return;
                }

                // Wenn mehr einträge da sind, als min. gespeichert werden, und vor mehr als 100 ms
                // nach überflüssigen gesucht wurde, dann entferne überflüssige.
                if ((entries.size() > MIN_SIZE) && (System.currentTimeMillis() - lastSearched > 100)) {
                    for (Iterator<DebugLine> e = entries.iterator(); e.hasNext();) {
                        DebugLine dl = e.next();

                        if (dl.isTimedOut()
                                && ((dl.getLevel() != DebugLevel.ERROR)
                                && (dl.getLevel() != DebugLevel.FATAL_ERROR))) {
                            e.remove();
                        }

                        if (entries.size() < MIN_SIZE) {
                            break;
                        }
                    }

                    lastSearched = System.currentTimeMillis();
                }

                entries.add(line);

                if ((line.getLevel() == DebugLevel.ERROR) || (line.getLevel() == DebugLevel.FATAL_ERROR)) {
                    log.error(line.getMessage());
                }
            } catch (Exception e) {
                log.error("DebugBuffer encountered Exception: " + e.getMessage());
            }
        }
    }

    public static StringBuffer produceStackTrace(Throwable e) {
        StringBuffer sb = new StringBuffer();
        for (StackTraceElement ste : e.getStackTrace()) {
            sb.append("<br>" + ste.toString());
        }
        if (e.getCause() != null) {
            sb.append("<br><b>Caused by</b>" + e.getCause().getMessage() + " (" + e.getCause().getClass().getName() + " )");
            sb.append(produceStackTrace(e.getCause()));
        }
        return sb;
    }

    public static void error(String message) {
        // addLine(DebugLevel.ERROR, message);
        writeStackTrace(message, new Exception());
    }

    public static void warning(String message) {
        addLine(DebugLevel.WARNING, message);
    }

    public static void error(Throwable e) {
        addLine(e);
    }

    public static void error(String message, Throwable e) {
        writeStackTrace(message, e);
    }

    public static void trace(String message) {
        addLine(DebugLevel.TRACE, message);
    }

    public static void debug(String message) {
        addLine(DebugLevel.DEBUG, message);
    }

    public static void fatalError(String message) {
        addLine(DebugLevel.FATAL_ERROR, message);
    }
}
