/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.core;

import at.darkdestiny.framework.QueryKeySet;
import at.darkdestiny.framework.exception.InternalFrameworkException;
import at.darkdestiny.framework.exception.InvalidIndexException;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
interface DBTableBufferedInterface<T> extends DBTableInterface<T> {
    public ArrayList<T> getEntries(QueryKeySet qks, String indexName) throws InternalFrameworkException, InvalidIndexException;
}
