/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author Stefan
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface OneToMany {
    public String baseField();
    public String refTable();
    public String refField();
    public String mappingTable() default("");
    public String update() default("CASCADE");
    public String delete() default("CASCADE");
}
