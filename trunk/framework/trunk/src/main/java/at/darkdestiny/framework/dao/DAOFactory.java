/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.dao;

import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.util.DebugBuffer;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Eobane
 */
public class DAOFactory {

    private static final Logger log = LoggerFactory.getLogger(DAOFactory.class);

    private static HashMap<Class, GenericDAO> instanceList = new HashMap<Class, GenericDAO>();
    private static HashMap<Class, GenericDAO> virtualList = new HashMap<Class, GenericDAO>();
    private static boolean virtualMode = false;

    /*
    public static <T> T getVirtual(Class clazz) {

    }
    */

    public static <T> T get(Class clazz) {
        return get(clazz, null);
    }

    public static <T> T get(Class clazz, Boolean virtualDAO) {
        HashMap<Class, GenericDAO> sourceList;
        if(virtualDAO != null){
            if(virtualDAO){
                sourceList = virtualList;
            }else{
                sourceList = instanceList;
            }
        }else if (virtualMode) {
            sourceList = virtualList;
        } else {
            sourceList = instanceList;
        }

        if (!sourceList.containsKey(clazz)) {
            try {

                GenericDAO genericDAO = null;
                Constructor constructor;
                constructor = clazz.getDeclaredConstructor();
                constructor.setAccessible(true);
                genericDAO = (GenericDAO) constructor.newInstance();
                sourceList.put(clazz, genericDAO);

                return (T) genericDAO;
            } catch (NoSuchMethodException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            } catch (SecurityException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            } catch (InstantiationException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            } catch (IllegalAccessException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            } catch (InvocationTargetException ex) {
                DebugBuffer.writeStackTrace(DAOFactory.class.getName(), ex);
            }
        } else {
            return (T) sourceList.get(clazz);
        }

        return null;
    }

    public static void dropAll() {
        HashMap<Class, GenericDAO> sourceList;
        
        // Leads to error if the contents of static fields are not garbage collected
        // clearDatasource();
        
        if (virtualMode) {            
            sourceList = virtualList;            
        } else {
            sourceList = instanceList;
        }        
        
        sourceList.clear();
    }

    private static void clearDatasource() {
        HashMap<Class, GenericDAO> sourceList;
        if (virtualMode) {
            sourceList = virtualList;
        } else {
            sourceList = instanceList;
        }

        for (GenericDAO gDAO : sourceList.values()) {
            Class clazz = gDAO.getClass();
            try {
                Class clazz2 = clazz;
                while (!(clazz2.getName().equals(ReadOnlyTable.class.getName()))) {
                    clazz2 = clazz2.getSuperclass();
                }

                Method reloadMethod = clazz2.getDeclaredMethod("clearDataSource");
                reloadMethod.setAccessible(true);
                reloadMethod.invoke(gDAO);
            } catch (NoSuchMethodException nsme) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", nsme);
                log.debug( "Method reloadDataSource not found for " + clazz);
                nsme.printStackTrace();
            } catch (IllegalAccessException iae) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", iae);
                iae.printStackTrace();
            } catch (InvocationTargetException ite) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", ite);
                ite.printStackTrace();
            }
        }

        System.gc();
    }    
    
    public static void reloadAll() {
        HashMap<Class, GenericDAO> sourceList;
        if (virtualMode) {
            sourceList = virtualList;
        } else {
            sourceList = instanceList;
        }

        for (GenericDAO gDAO : sourceList.values()) {
            Class clazz = gDAO.getClass();
            try {
                Class clazz2 = clazz;
                while (!(clazz2.getName().equals(ReadOnlyTable.class.getName()))) {
                    clazz2 = clazz2.getSuperclass();
                }

                Method reloadMethod = clazz2.getDeclaredMethod("reloadDataSource");
                reloadMethod.setAccessible(true);
                reloadMethod.invoke(gDAO);
            } catch (NoSuchMethodException nsme) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", nsme);
                log.debug( "Method reloadDataSource not found for " + clazz);
                nsme.printStackTrace();
            } catch (IllegalAccessException iae) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", iae);
                iae.printStackTrace();
            } catch (InvocationTargetException ite) {
                DebugBuffer.writeStackTrace("Error while reloading Table: ", ite);
                ite.printStackTrace();
            }
        }

        System.gc();
    }

    public static ArrayList<Exception> gatherAllLocks() {
        ArrayList<Exception> eList = new ArrayList<Exception>();

        for (Map.Entry<Class, GenericDAO> daoEntry : instanceList.entrySet()) {
            Exception e = daoEntry.getValue().getWriteLock();
            if (e != null) {
                eList.add(e);
            } else {
                log.info("No lock in " + daoEntry.getValue());
            }
        }

        return eList;
    }

    /**
     * @return the virtualMode
     */
    public static boolean isVirtualMode() {
        return virtualMode;
    }

    /**
     * @param aVirtualMode the virtualMode to set
     */
    public static void setVirtualMode(boolean aVirtualMode) {
        virtualMode = aVirtualMode;
    }
}
