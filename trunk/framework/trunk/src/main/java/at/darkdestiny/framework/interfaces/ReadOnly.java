/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.interfaces;

import at.darkdestiny.framework.QueryKeySet;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface ReadOnly<T> {
    public T get(QueryKeySet qks);
    public ArrayList<T> find(String name, QueryKeySet qks);
    public ArrayList<T> findAll();
    public boolean contains(QueryKeySet qks);
}
