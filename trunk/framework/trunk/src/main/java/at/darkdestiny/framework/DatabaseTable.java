/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework;

 import at.darkdestiny.framework.access.DbConnect;import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.OrderByAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.exception.InternalFrameworkException;
import at.darkdestiny.framework.exception.InvalidIndexException;
import at.darkdestiny.framework.exception.PrimaryKeyInvalidException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
abstract class DatabaseTable<T> {

    private static final Logger log = LoggerFactory.getLogger(DatabaseTable.class);

    protected final Class<T> dataClass;
    private final String tableName;
    private HashMap<TableIndex, IndexedTableData<T>> indices = new HashMap<TableIndex, IndexedTableData<T>>();
    protected HashMap<String, TableIndex> availIndices = new HashMap<String, TableIndex>();
    private HashMap<Integer, ArrayList<T>> buffer = new HashMap<Integer, ArrayList<T>>();
    private final static int QUERYMODE_INDEXED = 0;
    private final static int QUERYMODE_INDEXED_MERGED = 1;
    private final static int QUERYMODE_INDEXED_PARTIAL = 2;
    private final static int QUERYMODE_DIRECT = 3;
    boolean debug = false;

    public DatabaseTable() {
        dataClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        if (debug) {
            log.debug( "DataClass: " + dataClass.getName());
        }

        tableName = ((TableNameAnnotation) dataClass.getAnnotation(TableNameAnnotation.class)).value();
        initTable();
    }

    private void initTable() {
        String sqlSelect = "SELECT ";

        boolean firstField = true;

        HashMap<String, ArrayList<TableIndexField>> tmpIndices = new HashMap<String, ArrayList<TableIndexField>>();

        for (Field f : dataClass.getDeclaredFields()) {
            if (debug) {
                log.debug( "Check Field: " + f.getName());
            }

            for (Annotation a : f.getDeclaredAnnotations()) {
                if (debug) {
                    log.debug( "Check Annotation: " + a.toString());
                }

                if (a instanceof FieldMappingAnnotation) { // Build select statement
                    if (debug) {
                        log.debug( "Found FieldMappingAnnotation");
                    }
                    if (firstField) {
                        sqlSelect += ((FieldMappingAnnotation) a).value();
                        firstField = false;
                    } else {
                        sqlSelect += ", " + ((FieldMappingAnnotation) a).value();
                    }

                    if (debug) {
                        log.debug( "New SQL String: " + sqlSelect);
                    }
                } else if (a instanceof IdFieldAnnotation) { // Create primary key
                    if (debug) {
                        log.debug( "Adding " + f.getAnnotation(FieldMappingAnnotation.class).value() + " to primary key");
                    }

                    if (tmpIndices.containsKey("primary")) {
                        tmpIndices.get("primary").add(new TableIndexField(f.getAnnotation(FieldMappingAnnotation.class).value(), f.getType()));
                    } else {
                        ArrayList<TableIndexField> idxFieldList = new ArrayList<TableIndexField>();
                        idxFieldList.add(new TableIndexField(f.getAnnotation(FieldMappingAnnotation.class).value(), f.getType()));

                        tmpIndices.put("primary", idxFieldList);
                    }
                } else if (a instanceof IndexAnnotation) { // Create indices
                    IndexAnnotation ifa = (IndexAnnotation) a;
                    if (debug) {
                        log.debug( "Adding " + f.getAnnotation(FieldMappingAnnotation.class).value() + " to " + ifa.indexName() + " index");
                    }

                    if (tmpIndices.containsKey(ifa.indexName())) {
                        tmpIndices.get(ifa.indexName()).add(new TableIndexField(f.getAnnotation(FieldMappingAnnotation.class).value(), f.getType()));
                    } else {
                        ArrayList<TableIndexField> idxFieldList = new ArrayList<TableIndexField>();
                        idxFieldList.add(new TableIndexField(f.getAnnotation(FieldMappingAnnotation.class).value(), f.getType()));

                        tmpIndices.put(ifa.indexName(), idxFieldList);
                    }
                } else if (a instanceof OrderByAnnotation) { // Create Order By String
                    FieldMappingAnnotation fma = f.getAnnotation(FieldMappingAnnotation.class);
                }
            }
        }

        // Build indices
        for (Map.Entry<String, ArrayList<TableIndexField>> me : tmpIndices.entrySet()) {
            if (me.getKey().equalsIgnoreCase("primary")) {
                TableIndex primaryTI = new TableIndex("primary", IndexType.PRIMARY_INDEX, me.getValue());
                availIndices.put(primaryTI.getName(), primaryTI);
                indices.put(primaryTI, new IndexedTableData<T>(primaryTI));
            } else {
                TableIndex index = new TableIndex(me.getKey(), IndexType.PRIMARY_INDEX, me.getValue());
                availIndices.put(index.getName(), index);
                indices.put(index, new IndexedTableData<T>(index));
            }
        }

        try {
            Statement stmt = DbConnect.createStatement();
            if (debug) {
                log.debug( "SQL: " + sqlSelect + " FROM " + tableName);
            }

            ResultSet rs = stmt.executeQuery(sqlSelect + " FROM " + tableName);

            int i = 0;

            while (rs.next()) {
                Constructor<T> constructor = dataClass.getConstructor();
                constructor.setAccessible(true);
                T dataEntry = constructor.newInstance();

                // TableIndex ti2 = availIndices.get("primary");
                Collection<TableIndex> tblIndices = availIndices.values();

                for (Field field : dataClass.getDeclaredFields()) {
                    field.setAccessible(true);
                    FieldMappingAnnotation fma = field.getAnnotation(FieldMappingAnnotation.class);

                    if (fma == null) {
                        continue;
                    }

                    if ((field.getType() == Integer.class) || (field.getType() == int.class)) {
                        field.set(dataEntry, rs.getInt(fma.value()));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == Long.class) || (field.getType() == long.class)) {
                        field.set(dataEntry, rs.getLong(fma.value()));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == Double.class) || (field.getType() == double.class)) {
                        field.set(dataEntry, rs.getDouble(fma.value()));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == Float.class) || (field.getType() == float.class)) {
                        field.set(dataEntry, rs.getFloat(fma.value()));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == Boolean.class) || (field.getType() == boolean.class)) {
                        field.set(dataEntry, rs.getInt(fma.value()) == 1);
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if ((field.getType() == String.class)) {
                        field.set(dataEntry, rs.getString(fma.value()));
                        for (TableIndex ti : tblIndices) {
                            indices.get(ti).addEntry(dataEntry, field, field.getType());
                        }
                    } else if (field.getType().isEnum()) {
                        Class fEnumType = field.getType();
                        HashSet<String> eValues = new HashSet<String>();  // enum constants

                        Enum[] eConstants = (Enum[]) fEnumType.getEnumConstants();
                        for (Enum e : eConstants) {
                            eValues.add(null);
                            eValues.add(e.toString());
                        }

                        if (eValues.contains(rs.getString(fma.value()))) {
                            if (rs.getString(fma.value()) == null) {
                                field.set(dataEntry, null);
                            } else {
                                field.set(dataEntry, Enum.valueOf(fEnumType, rs.getString(fma.value())));
                            }

                            for (TableIndex ti : tblIndices) {
                                indices.get(ti).addEntry(dataEntry, field, field.getType());
                            }
                        } else {
                            throw new InternalFrameworkException("Database Enumeration " + rs.getString(fma.value()) + " does not exist in Enumeration "
                                    + field.getType().getName());
                        }
                    }

                    i++;

                    if ((i % 500) == 0) {
                        if (debug) {
                            log.debug( "Indexed " + i + " entries!");
                        }
                    }
                }
            }

            stmt.close();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        } catch (NoSuchMethodException nsme) {
            nsme.printStackTrace();
        } catch (InstantiationException ie) {
            ie.printStackTrace();
        } catch (InvocationTargetException ite) {
            ite.printStackTrace();
        } catch (InternalFrameworkException ife) {
            ife.printStackTrace();
        }
    }

    protected ArrayList<T> mergeAll(T entry, QueryKeySet qks) throws InternalFrameworkException {
        throw new InternalFrameworkException("Not supported!");
    }

    private T insertOrMerge(T entry) throws InternalFrameworkException {
        throw new InternalFrameworkException("Not supported!");
    }

    protected T insert(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        try {
            // Build insert statment from entry
            String fieldDecStr = "";
            String fieldValueStr = "";

            boolean firstField = true;

            for (Field f : entry.getClass().getDeclaredFields()) {
                if (f.getAnnotation(FieldMappingAnnotation.class) == null) {
                    if (debug) {
                        log.debug( "Continuing field : " + f.getName() + " Reason : No FieldMappingAnnotation");
                    }
                    continue;
                } else {
                    //   log.debug( "Not Continuing field : " + f.getAnnotation(FieldMappingAnnotation.class) + " annotation :" + f.getAnnotation(FieldMappingAnnotation.class).value());
                }
                f.setAccessible(true);
                if (f.get(entry) == null) {
                    continue;
                }

                if (!firstField) {
                    fieldDecStr += ", ";
                    fieldValueStr += ", ";
                }

                firstField = false;
                fieldDecStr += f.getAnnotation(
                        FieldMappingAnnotation.class).value();

                if (f.getType() == String.class) {
                    String value = (String) f.get(entry);
                    fieldValueStr += "'" + value + "'";
                } else {
                    fieldValueStr += f.get(entry);
                }
            }

            String stmtStr = "INSERT INTO " + tableName + " (" + fieldDecStr + ") VALUES (" + fieldValueStr + ")";
            Statement stmt = DbConnect.createStatement();

            Integer generatedKey = null;

            try {
                stmt.execute(stmtStr);

                ResultSet keys = stmt.getGeneratedKeys();
                if (keys.next()) {
                    generatedKey = keys.getInt(1);
                    if (debug) {
                        log.debug( "Generated Key 1 = " + generatedKey);
                    }
                }

                stmt.close();
            } catch (SQLException sqle) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }

                throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtStr);
            }

            T newEntry = cloneResult(entry);

            Collection<TableIndex> tblIndices = availIndices.values();
            for (Field f : entry.getClass().getDeclaredFields()) {

                f.setAccessible(true);
                if (f.getAnnotation(FieldMappingAnnotation.class) == null) {
                    if (debug) {
                        log.debug( "Continuing field : " + f.getName() + " Reason : No FieldMappingAnnotation");
                    }
                    continue;
                } else {
                    //  log.debug( "Not Continuing field : " + f.getAnnotation(FieldMappingAnnotation.class) + " annotation :" + f.getAnnotation(FieldMappingAnnotation.class).value());
                }
                if ((f.get(entry) == null) && (f.getAnnotation(IdFieldAnnotation.class) == null)) {
                    continue;
                }

                if (debug) {
                    log.debug( "Field name: " + f.getName() + " Annoation: " + f.getAnnotation(IdFieldAnnotation.class) + " Generated Key: " + generatedKey);
                }
                if ((f.getAnnotation(IdFieldAnnotation.class) != null) && (generatedKey != null)) {
                    if (debug) {
                        log.debug( "Generated Key 2 = " + generatedKey);
                    }
                    f.set(newEntry, generatedKey);
                    f.set(entry, generatedKey);
                }

                for (TableIndex ti : tblIndices) {
                    indices.get(ti).addEntry(newEntry, f, f.getType());
                }
            }

            QueryKeySet qks = buildQueryKeySetFromPrimary(entry);
            IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));
            // Check if all primary fields was specified
            checkPrimaryKey(qks);
            ArrayList<T> res = tableData.getEntry(qks);
            return cloneResult(res.get(0));
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    protected void delete(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        QueryKeySet qks = buildQueryKeySetFromPrimary(entry);

        IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));

        // Check if all primary fields was specified
        checkPrimaryKey(qks);

        ArrayList<T> res = tableData.getEntry(qks);

        if (res.size() == 0) {
            throw new PrimaryKeyInvalidException("Primary Key not available");
        }
        T result = res.get(0);

        // log.debug( "RESULT FOUND?: " + result);

        Statement stmt = DbConnect.createStatement();
        String stmtStr = "DELETE FROM " + this.tableName + " WHERE ";

        try {
            boolean firstEntry = true;

            for (QueryKey qk : qks.getKeys()) {
                if (!firstEntry) {
                    stmtStr += " AND ";
                }
                firstEntry = false;

                stmtStr += qk.getFieldName() + "=" + qk.getFieldValue();
            }

            //log.debug( "Generated Statement: " + stmtStr);

            stmt.execute(stmtStr);
        } catch (SQLException sqle) {
            throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtStr);
        }

        for (Map.Entry<TableIndex, IndexedTableData<T>> idxSet : indices.entrySet()) {
            TableIndex tblIdx = idxSet.getKey();
            IndexedTableData itd = idxSet.getValue();

            //log.debug( "Cleaning up Index " + tblIdx.getName());
            itd.deleteEntry(result);
        }
    }

    protected T merge(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException {
        try {
            QueryKeySet qks = buildQueryKeySetFromPrimary(entry);

            /*
             for (QueryKey qk : qks.getKeys()) {
             log.debug( "KEY FIELD = " + qk.getFieldName() + " VALUE = " + qk.getFieldValue());
             }
             */

            IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));

            // Check if all primary fields was specified
            checkPrimaryKey(qks);

            ArrayList<T> res = tableData.getEntry(qks);

            if (res.size() == 0) {
                throw new PrimaryKeyInvalidException("Primary Key not available");
            }

            T result = res.get(0);

            // log.debug( "RESULT REF: " + result);
            // Build statement and update
            Statement stmt = DbConnect.createStatement();
            String stmtStr = "UPDATE " + tableName + " SET";
            String whereStr = " WHERE";

            try {
                boolean firstEntry = true;
                boolean firstWhereEntry = true;

                for (Field f : entry.getClass().getDeclaredFields()) {
                    f.setAccessible(true);

                    if (f.getAnnotation(FieldMappingAnnotation.class) == null) {
                        if (debug) {
                            log.debug( "Continuing field : " + f.getName() + " Reason : No FieldMappingAnnotation");
                        }
                        continue;
                    } else {
                        //        log.debug( "Not Continuing field : " + f.getAnnotation(FieldMappingAnnotation.class) + " annotation :" + f.getAnnotation(FieldMappingAnnotation.class).value());
                    }

                    Field targetField = result.getClass().getDeclaredField(f.getName());

                    targetField.setAccessible(true);
                    if (qks.getKeyFields().contains(f.getName())) {
                        if (firstWhereEntry) {
                            firstWhereEntry = false;
                        } else {
                            whereStr += " AND";
                        }

                        if (f.getType() == String.class) {
                            whereStr += " " + ((FieldMappingAnnotation) f.getAnnotation(FieldMappingAnnotation.class)).value() + "='" + f.get(entry) + "'";
                        } else {
                            whereStr += " " + ((FieldMappingAnnotation) f.getAnnotation(FieldMappingAnnotation.class)).value() + "=" + f.get(entry);
                        }

                        continue;
                    }

                    if (firstEntry) {
                        firstEntry = false;
                    } else {
                        stmtStr += ",";
                    }

                    if (f.getType() == String.class) {
                        stmtStr += " " + ((FieldMappingAnnotation) f.getAnnotation(FieldMappingAnnotation.class)).value() + "='" + f.get(entry) + "'";
                    } else {
                        stmtStr += " " + ((FieldMappingAnnotation) f.getAnnotation(FieldMappingAnnotation.class)).value() + "=" + f.get(entry);
                    }
                }

                stmt.executeUpdate(stmtStr + whereStr);
                stmt.close();
            } catch (SQLException sqle) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    log.debug( "Error in Update: " + e);

                }
                log.debug( "Error in Update: " + sqle.getMessage());

                throw new InternalFrameworkException(sqle.getMessage() + " >> Statement: " + stmtStr + whereStr);
            }

            for (Field f : entry.getClass().getDeclaredFields()) {
                Field targetField = result.getClass().getDeclaredField(f.getName());

                targetField.setAccessible(true);
                if (f.getAnnotation(FieldMappingAnnotation.class) == null) {
                    if (debug) {
                        log.debug( "Continuing field : " + f.getName() + " Reason : No FieldMappingAnnotation");
                    }
                    continue;
                } else {
                    //        log.debug( "Not Continuing field : " + f.getAnnotation(FieldMappingAnnotation.class) + " annotation :" + f.getAnnotation(FieldMappingAnnotation.class).value());
                }
                f.setAccessible(true);

                TableIndex ti = null;

                if (targetField.get(result) != targetField.get(entry)) {
                    // If this field is an index sorting needs to be done
                    if (targetField.getAnnotation(IndexAnnotation.class) != null) {
                        IndexAnnotation ia = (IndexAnnotation) targetField.getAnnotation(IndexAnnotation.class);
                        ti = availIndices.get(ia.indexName());
                        indices.get(ti).deleteEntry(result);
                    }
                }

                targetField.set(result, f.get(entry));

                if (ti != null) {
                    indices.get(ti).addEntry(result, f, f.getType());
                }
            }

            // log.debug( "RESULT REF: " + result);

            return cloneResult(result);
        } catch (NoSuchFieldException nsfe) {
            throw new InternalFrameworkException(nsfe.getMessage());
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    protected T getEntry(T entry) throws PrimaryKeyInvalidException, InvalidIndexException, InternalFrameworkException {
        IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));

        // Check if all primary fields was specified
        QueryKeySet qks = buildQueryKeySetFromPrimary(entry);
        checkPrimaryKey(qks);

        ArrayList<T> res = cloneResult(tableData.getEntry(qks));

        if (res.size() == 0) {
            return null;
        } else {
            return res.get(0);
        }
    }

    protected T getEntry(QueryKeySet qks) throws PrimaryKeyInvalidException, InvalidIndexException, InternalFrameworkException {
        IndexedTableData<T> tableData = indices.get(availIndices.get("primary"));

        // Check if all primary fields was specified
        checkPrimaryKey(qks);

        ArrayList<T> res = cloneResult(tableData.getEntry(qks));

        if (res.size() == 0) {
            return null;
        } else {
            return res.get(0);
        }
    }

    private QueryKeySet buildQueryKeySetFromPrimary(T entry) throws InternalFrameworkException {
        try {
            TableIndex primary = availIndices.get("primary");

            QueryKeySet qks = new QueryKeySet();
            for (TableIndexField tif : primary.getTableIndexFields()) {
                qks.addKey(new QueryKey(
                        tif.getFieldName(),
                        getFieldValue(
                        entry,
                        entry.getClass().getDeclaredField(tif.getFieldName()),
                        entry.getClass().getDeclaredField(tif.getFieldName()).getType())));
            }

            return qks;
        } catch (NoSuchFieldException nsfe) {
            throw new InternalFrameworkException(nsfe.getMessage());
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    private QueryKeySet buildQueryKeySetFromEntry(T entry) throws InternalFrameworkException {
        try {
            QueryKeySet qks = new QueryKeySet();

            for (Field f : entry.getClass().getDeclaredFields()) {
                f.setAccessible(true);

                if (f.getAnnotation(FieldMappingAnnotation.class) == null) {
                    continue;
                }

                if (f.get(entry) == null) {
                    continue;
                }
                qks.addKey(new QueryKey(f.getName(), f.get(entry)));
            }

            return qks;
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    private QueryKeySet buildPrimaryQueryKeySetFromEntry(T entry) throws InternalFrameworkException {
        try {
            QueryKeySet qks = new QueryKeySet();
            ArrayList<TableIndexField> indexfields = availIndices.get("primary").getTableIndexFields();

            for (Field f : entry.getClass().getDeclaredFields()) {
                f.setAccessible(true);

                if (f.getAnnotation(FieldMappingAnnotation.class) == null) {
                    continue;
                }
                if (f.get(entry) == null) {
                    continue;
                }

                boolean found = false;
                for (TableIndexField tif : indexfields) {
                    if (tif.getFieldName().equalsIgnoreCase(f.getName())) {
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    continue;
                }
                qks.addKey(new QueryKey(f.getName(), f.get(entry)));
            }

            return qks;
        } catch (IllegalAccessException iae) {
            throw new InternalFrameworkException(iae.getMessage());
        }
    }

    private void checkPrimaryKey(QueryKeySet qks) throws PrimaryKeyInvalidException {
        TableIndex ti = availIndices.get("primary");

        for (TableIndexField tif : ti.getTableIndexFields()) {
            boolean found = false;
            boolean typeValid = false;

            for (QueryKey qk : qks.getKeys()) {
                if (qk.getFieldName().equals(tif.getFieldName())) {
                    found = true;
                    if (qk.getClass() != tif.getDataType()) {
                        typeValid = true;
                    }
                }
            }

            if (!found) {
                throw new PrimaryKeyInvalidException("Not all fields specified");
            }
            if (!typeValid) {
                throw new PrimaryKeyInvalidException("Field " + tif.getFieldName() + " has invalid type should be " + tif.getDataType());
            }
        }
    }

    protected ArrayList<T> getEntries(T entry) throws InternalFrameworkException, InvalidIndexException {
        // QueryKeySet qks = buildQueryKeySetFromEntry(entry);
        QueryKeySet qks = buildPrimaryQueryKeySetFromEntry(entry);
        String indexName = null;

        HashSet<String> queryFields = new HashSet<String>();

        for (QueryKey qk : qks.getKeys()) {
            queryFields.add(qk.getFieldName());
            if (debug) {
                log.debug( "Query contains field " + qk.getFieldName());
            }
        }

        for (Map.Entry<String, TableIndex> index : availIndices.entrySet()) {
            TableIndex ti = index.getValue();
            if (debug) {
                log.debug( "Check Index " + ti.getName());
            }
            HashSet<String> indexFields = new HashSet<String>();

            for (TableIndexField tif : ti.getTableIndexFields()) {
                if (debug) {
                    log.debug( "Index contains field " + tif.getFieldName());
                }
                indexFields.add(tif.getFieldName());
            }

            if (queryFields.containsAll(indexFields)) {
                if (debug) {
                    log.debug( "FOUND!");
                }
                indexName = ti.getName();
                break;
            }
        }

        /*
         if (buffer.containsKey(qks.hashCode())) {
         // log.debug( "Retrieve from Buffered Results ("+qks.hashCode()+")");
         return buffer.get(qks.hashCode());
         } else {
         // log.debug( "Read from Buffer ("+qks.hashCode()+")");
         }
         */

        ArrayList<T> result = getEntries(qks, indexName);
        /*
         char c = 13;

         try {
         // Create file
         FileWriter fstream = new FileWriter("C:\\hash.csv", true);
         BufferedWriter out = new BufferedWriter(fstream);
         out.write(qks.hashCode()+";"+qks.getHashBase()+c);
         //Close the output stream
         out.close();
         } catch (Exception e) {//Catch exception if any
         System.err.println("Error: " + e.getMessage());
         }
         */
        // buffer.put(qks.hashCode(), result);

        return result;
    }

    protected ArrayList<T> getEntriesNew(T entry) throws InternalFrameworkException {
        // Build query key set
        QueryKeySet qks = buildQueryKeySetFromEntry(entry);
        TableIndex index = null;

        HashSet<String> queryFields = new HashSet<String>();

        int queryValue = 0;
        for (QueryKey qk : qks.getKeys()) {
            if (debug) {
                log.debug( "Query contains field " + qk.getFieldName());
            }
            queryFields.add(qk.getFieldName());
            queryValue++;
        }

        TreeMap<Integer, TableIndex> ratedIndices = new TreeMap<Integer, TableIndex>();

        for (Map.Entry<String, TableIndex> _index : availIndices.entrySet()) {
            TableIndex ti = _index.getValue();
            // log.debug( "Check Index " + ti.getName());
            HashSet<String> indexFields = new HashSet<String>();

            int indexValue = 0;
            for (TableIndexField tif : ti.getTableIndexFields()) {
                // log.debug( "Index contains field " + tif.getFieldName());
                indexFields.add(tif.getFieldName());

                if (queryFields.contains(tif.getFieldName())) {
                    indexValue++;
                }
            }

            if (indexValue == 0) {
                continue;
            }
            ratedIndices.put(indexValue, ti);

            if (indexFields.containsAll(queryFields)) {
                if (debug) {
                    log.debug( "FOUND!");
                }
                index = ti;
                break;
            }
        }

        int queryMode = QUERYMODE_DIRECT;

        if (index != null) {
            queryMode = QUERYMODE_INDEXED;
        } else {
            if (ratedIndices.size() > 0) {
                queryMode = QUERYMODE_INDEXED_PARTIAL;
                index = ratedIndices.lastEntry().getValue();
                // log.debug( "USE PARTIAL INDEX: " + index.getName());
            } else {
                queryMode = QUERYMODE_DIRECT;
            }
        }

        ArrayList<T> resultTmp = null;
        ArrayList<T> result = null;

        switch (queryMode) {
            case (QUERYMODE_INDEXED):
                try {
                    result = getEntries(qks, index.getName());
                } catch (InvalidIndexException iie) {
                }
                break;
            case (QUERYMODE_INDEXED_MERGED):
                throw new InternalFrameworkException("Query mode INDEXED_MERGED not implemented!");
            case (QUERYMODE_INDEXED_PARTIAL):
                try {
                    // Determine uncovered Fields
                    HashMap<String, Object> uncoveredFields = new HashMap<String, Object>();
                    for (QueryKey qk : qks.getKeys()) {
                        uncoveredFields.put(qk.getFieldName(), qk.getFieldValue());
                    }

                    QueryKeySet finalQKS = new QueryKeySet();

                    for (TableIndexField tif : index.getTableIndexFields()) {
                        finalQKS.addKey(new QueryKey(tif.getFieldName(), uncoveredFields.get(tif.getFieldName())));

                        // log.debug( "Uncovered field " + tif.getFieldName());

                        uncoveredFields.remove(tif.getFieldName());
                    }

                    resultTmp = getEntries(finalQKS, index.getName());
                    result = cloneResult(resultTmp);

                    ArrayList<T> toDelete = new ArrayList<T>();

                    for (T resEntry : result) {
                        for (Map.Entry<String, Object> condField : uncoveredFields.entrySet()) {
                            // log.debug( "Check field " + condField.getKey() + " with Value " + condField.getValue());

                            Field f = dataClass.getDeclaredField(condField.getKey());
                            f.setAccessible(true);

                            // log.debug( "Compare " + f.get(resEntry) + " to " + condField.getValue());
                            if (DatabaseHelper.compareFields(f, resEntry, condField.getValue()) != 0) {
                                // log.debug( "REMOVE");
                                toDelete.add(resEntry);
                            } else {
                                // log.debug( "KEEP");
                            }
                        }
                    }

                    if (debug) {
                        log.debug( "RESULT SIZE = " + result.size() + " TODELETE SIZE = " + toDelete.size());
                    }
                    result.removeAll(toDelete);
                } catch (InvalidIndexException iie) {
                    iie.printStackTrace();
                } catch (NoSuchFieldException nsfe) {
                    nsfe.printStackTrace();
                    /*
                     } catch (IllegalAccessException iae) {
                     iae.printStackTrace();
                     */
                }
                break;
            case (QUERYMODE_DIRECT):
                throw new InternalFrameworkException("Query mode DIRECT not implemented!");
        }

        return result;
    }

    protected ArrayList<T> getEntries(QueryKeySet qks, String indexName) throws InternalFrameworkException, InvalidIndexException {
        TableIndex ti = availIndices.get(indexName);

        if (ti == null) {
            throw new InvalidIndexException("Index " + indexName + " unknown");
        }
        IndexedTableData<T> tableData = indices.get(ti);

        int bufferHash = (int) Math.floor((qks.hashCode() + indexName.hashCode()) / 2);
        /*
         log.debug( "=========================");
         log.debug( "bufferHash " + bufferHash);
         log.debug( "------- Details --------");
         log.debug( "Index name: " + indexName);
         for (QueryKey qk : qks.getKeys()) {
         log.debug( "KEY="+qk.getFieldName()+ " VALUE="+qk.getFieldValue());
         }
         */

        if (buffer.containsKey(bufferHash)) {
            // log.debug( "Retrieve from Buffered Results ("+qks.hashCode()+") -> Result size " + buffer.get(bufferHash).size());
            return buffer.get(bufferHash);
        }

        ArrayList<T> result = cloneResult(tableData.getEntry(qks));

        /*
         char c = 13;

         try {
         // Create file
         FileWriter fstream = new FileWriter("C:\\hash.csv", true);
         BufferedWriter out = new BufferedWriter(fstream);
         out.write(qks.hashCode()+";"+qks.getHashBase()+c);
         //Close the output stream
         out.close();
         } catch (Exception e) {//Catch exception if any
         System.err.println("Error: " + e.getMessage());
         }
         */

        // buffer.put(bufferHash, result);

        return result;
    }

    protected ArrayList<T> getAllEntries() {
        TableIndex ti = availIndices.get("primary");

        IndexedTableData<T> tableData = indices.get(ti);

        return cloneResult(tableData.getAllEntries());
    }

    private Object getFieldValue(T entry, Field f, Class c) throws IllegalAccessException {
        Object obj = null;

        f.setAccessible(true);

        if ((c == int.class) || (c == Integer.class)) {
            obj = f.get(entry);
        } else if ((c == double.class) || (c == Double.class)) {
            obj = f.get(entry);
        } else if ((c == long.class) || (c == Long.class)) {
            obj = f.get(entry);
        } else if ((c == float.class) || (c == Float.class)) {
            obj = f.get(entry);
        } else if ((c == boolean.class) || (c == Boolean.class)) {
            obj = (Boolean) f.get(entry) ? (int) 1 : (int) 0;
        } else if (c == char.class) {
            obj = f.get(entry);
        } else if (c == Enum.class) {
            obj = f.get(entry);
        }

        return obj;
    }

    private T cloneResult(T in) {
        try {
            Constructor<T> constructor = dataClass.getConstructor();
            constructor.setAccessible(true);
            T out = constructor.newInstance();

            Class<T> clazz = (Class<T>) in.getClass();
            Method mtd = clazz.getMethod("clone", new Class[0]);
            T clone = (T) mtd.invoke(in, new Object[0]);
            out = clone;

            return out;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private ArrayList<T> cloneResult(ArrayList<T> in) {
        ArrayList<T> out = new ArrayList<T>();

        try {
            for (T item : in) {
                Class<T> clazz = (Class<T>) item.getClass();
                Method mtd = clazz.getMethod("clone", new Class[0]);
                T clone = (T) mtd.invoke(item, new Object[0]);
                out.add(clone);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return out;
    }

    class IndexedTableData<T> {

        private final String indexName;
        private HashMap<String, HashMap<Object, HashSet<T>>> indexedTable = new HashMap<String, HashMap<Object, HashSet<T>>>();

        public IndexedTableData(TableIndex tblIdx) {
            // log.debug( "Creating Index " + tblIdx.getName() + " for table " + tableName);
            indexName = tblIdx.getName();

            for (TableIndexField idxField : tblIdx.getTableIndexFields()) {
                HashMap<Object, HashSet<T>> values = new HashMap<Object, HashSet<T>>();
                indexedTable.put(idxField.getFieldName(), values);

                // log.debug( "Added field " + idxField.getFieldName() + " to indexTableData");
            }
        }

        public void deleteEntry(T entry) {
            for (HashMap<Object, HashSet<T>> indexEntries : indexedTable.values()) {
                // log.debug( "Looping values");

                for (HashSet<T> content : indexEntries.values()) {
                    // log.debug( "HashSet contains?" + content.contains(entry));
                    if (content.remove(entry)) {
                        if (debug) {
                            log.debug( "DELETED " + entry + " FROM " + indexName);
                        }
                        break;
                    }
                }
            }
        }

        public void addEntry(T entry, Field f, Class c) throws IllegalAccessException {
            HashMap<Object, HashSet<T>> values = indexedTable.get(f.getAnnotation(FieldMappingAnnotation.class).value());
            if (values == null) {
                return;
            }
            Object obj = null;

            if ((c == int.class) || (c == Integer.class)) {
                obj = f.get(entry);
            } else if ((c == double.class) || (c == Double.class)) {
                obj = f.get(entry);
            } else if ((c == long.class) || (c == Long.class)) {
                obj = f.get(entry);
            } else if ((c == float.class) || (c == Float.class)) {
                obj = f.get(entry);
            } else if ((c == boolean.class) || (c == Boolean.class)) {
                // obj = f.getBoolean(entry) ? (int) 1 : (int) 0;
                obj = f.get(entry);
            } else if (c == char.class) {
                obj = f.get(entry);
            } else if (c == String.class) {
                obj = (String) f.get(entry);
            } else if (c == Enum.class) {
                obj = Enum.valueOf(c, (String) f.get(entry));
            }

            if (values.containsKey(obj)) {
                // log.debug( "VALUE " + obj + " EXISTS, ADDING " + entry + " TO INDEX " + indexName);
                values.get(obj).add(entry);
            } else {
                // log.debug( "VALUE " + obj + " NOT EXISTS, CREATING " + entry + " IN " + indexName);
                HashSet<T> entries = new HashSet<T>();
                entries.add(entry);
                values.put(obj, entries);
            }
        }

        public ArrayList<T> getEntry(QueryKeySet keyset) throws InternalFrameworkException, InvalidIndexException {
            ArrayList<T> result = new ArrayList<T>();
            HashSet<T> resBuffer = null;

            for (QueryKey qk : keyset.getKeys()) {
                if (!indexedTable.containsKey(qk.getFieldName())) {
                    throw new InvalidIndexException("Field " + qk.getFieldName() + " could nto be found in index " + indexName);
                }
                HashMap<Object, HashSet<T>> entries = indexedTable.get(qk.getFieldName());

                // log.debug( "Check for fieldValue " + qk.getFieldValue() + " in field " + qk.getFieldName());

                if (entries.containsKey(qk.getFieldValue())) {
                    // log.debug( "Found " + entries.get(qk.getFieldValue()).size() + " matches for " + qk.getFieldName());

                    if (resBuffer == null) {
                        resBuffer = new HashSet<T>();

                        /*
                         for (T resClass : entries.get(qk.getFieldValue())) {
                         log.debug( "Found Result Ref: " + resClass);
                         }
                         */

                        resBuffer.addAll(entries.get(qk.getFieldValue()));
                    } else {
                        resBuffer.retainAll(entries.get(qk.getFieldValue()));
                    }
                } else {
                    // log.debug( "Clear");
                    resBuffer = null;
                    break;
                }
            }

            if (resBuffer == null) {
                return result;
            }

            Collection<T> medResult = null;

            try {
                if (!keyset.getOrderKeys().isEmpty()) {
                    for (OrderKey ok : keyset.getOrderKeys()) {
                        TreeMap<Object, LinkedList<T>> tm = new TreeMap<Object, LinkedList<T>>();

                        if (medResult == null) {
                            for (T entry : resBuffer) {
                                Field f = entry.getClass().getDeclaredField(ok.getFieldName());
                                f.setAccessible(true);

                                // log.debug( "Order Field " + f.getName());

                                if (tm.containsKey(f.get(entry))) {
                                    tm.get(f.get(entry)).add(entry);
                                } else {
                                    LinkedList<T> ll = new LinkedList<T>();
                                    ll.add(entry);
                                    tm.put(f.get(entry), ll);
                                }
                            }

                            medResult = new LinkedList<T>();

                            if (ok.getOrder() == OrderKey.ORDER_ASC) {
                                // log.debug( "Get Ascending result");
                                for (LinkedList<T> list : tm.values()) {
                                    medResult.addAll(list);
                                }
                            } else if (ok.getOrder() == OrderKey.ORDER_DESC) {
                                // log.debug( "Get Descending result");
                                for (LinkedList<T> list : tm.descendingMap().values()) {
                                    medResult.addAll(list);
                                }
                            }
                        } else {
                            for (T entry : medResult) {
                                Field f = entry.getClass().getDeclaredField(ok.getFieldName());
                                f.setAccessible(true);

                                if (tm.containsKey(f.get(entry))) {
                                    tm.get(f.get(entry)).add(entry);
                                } else {
                                    LinkedList<T> ll = new LinkedList<T>();
                                    ll.add(entry);

                                    tm.put(f.get(entry), ll);
                                }
                            }

                            if (ok.getOrder() == OrderKey.ORDER_ASC) {
                                // log.debug( "Get Ascending result");
                                for (LinkedList<T> list : tm.values()) {
                                    medResult.addAll(list);
                                }
                            } else if (ok.getOrder() == OrderKey.ORDER_DESC) {
                                // log.debug( "Get Descending result");
                                for (LinkedList<T> list : tm.descendingMap().values()) {
                                    medResult.addAll(list);
                                }
                            }
                        }
                    }
                }
            } catch (NoSuchFieldException nsfe) {
                throw new InternalFrameworkException(nsfe.getMessage());
            } catch (IllegalAccessException iae) {
                throw new InternalFrameworkException(iae.getMessage());
            }

            if (medResult != null) {
                result.addAll(medResult);
            } else {
                result.addAll(resBuffer);
            }

            return result;
        }

        public ArrayList<T> getAllEntries() {
            ArrayList<T> result = new ArrayList<T>();

            for (HashMap<Object, HashSet<T>> indexEntry : indexedTable.values()) {
                for (HashSet<T> indexContent : indexEntry.values()) {
                    result.addAll(indexContent);
                }
                break;
            }

            return result;
        }
    }
}
