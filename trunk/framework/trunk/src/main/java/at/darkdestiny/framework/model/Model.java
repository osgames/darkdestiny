/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.model;

import java.io.Serializable;

/**
 *
 * @author Eobane
 */
public class Model<T> implements Cloneable, Serializable {

    @Override
    @SuppressWarnings("unchecked")
    public T clone() {
        try {
            T tmp = (T) super.clone();
            return tmp;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public void debugPrint() {
    }
}
