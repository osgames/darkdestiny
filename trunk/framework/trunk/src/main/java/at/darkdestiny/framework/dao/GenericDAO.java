/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.dao;

public interface GenericDAO<T> {
    public Exception getWriteLock();
}
