/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.transaction;

import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.exception.TransactionException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Stefan
 */
public class DatabaseTransaction extends BaseTransaction {
    private final String statement;
    private final String reverseStatement;
    
    private int generatedKey = -1;
    
    public DatabaseTransaction(String statement, String reverseStatement) {
        this.statement = statement;
        this.reverseStatement = reverseStatement;
    }
       
    protected Object execute() throws TransactionException {        
        Statement stmt = DbConnect.createStatement();
        try {
            stmt.execute(statement);       
            
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                generatedKey = rs.getInt(1);
            }
        } catch (SQLException e) {
            throw new TransactionException("DB Transaction failed",e);
        }
        
        return null;
    }
    
    protected void rollback() throws TransactionException {
        Statement stmt = DbConnect.createStatement();
        try {
            String reverseStmt = reverseStatement.replace("%GENKEY%", ""+generatedKey);
            
            stmt.execute(reverseStmt);   
        } catch (SQLException e) {
            throw new TransactionException("DB Transaction failed",e);
        }        
    }
}
