/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.transaction;

import at.darkdestiny.framework.exception.TransactionException;

/**
 *
 * @author Stefan
 */
public abstract class BaseTransaction {
    public final static int TRANSACTION_TYPE_UPDATE = 0;
    public final static int TRANSACTION_TYPE_INSERT = 1;
    public final static int TRANSACTION_TYPE_DELETE = 2;
    public final static int TRANSACTION_TYPE_INSERT_OR_UPDATE = 3;
    
    protected abstract Object execute() throws TransactionException;
    protected abstract void rollback() throws TransactionException;
        
    protected TransactionHandler getTransaction() {
        return TransactionHandler.getCurrentTransaction();
    }    
}
