/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DataScope.EScopeType;
import at.darkdestiny.framework.core.DatabaseTableBuffered;
import at.darkdestiny.framework.core.DatabaseTablePrimitive;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.interfaces.ReadOnly;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.util.DebugBuffer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class ReadOnlyTable<T extends Model> implements ReadOnly<T> {
    private static final Logger log = LoggerFactory.getLogger(ReadOnlyTable.class);

    protected at.darkdestiny.framework.core.DatabaseTable datasource;
    private boolean debug = true;

    protected ReadOnlyTable() {                       
        if (DAOFactory.isVirtualMode()) {
            // Check if table is constant, if yes do not virtualize
            Class<T> dataClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            Annotation dataScopeAnnotation = dataClass.getAnnotation(DataScope.class);
            if ((dataScopeAnnotation != null) && (((DataScope)dataScopeAnnotation).type() == EScopeType.CONSTANTS)) {
                log.debug(dataClass.getName() + " is constant => do not virtualize");
                initTable();
            } else {            
                initVirtualTable();
            }
        } else {
            initTable();
        }
    }

    protected void clearDataSource() {
        datasource = null;
    }
    
    protected void reloadDataSource() {
        if (DAOFactory.isVirtualMode()) {
            // Check if table is constant, if yes do not virtualize
            Class<T> dataClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            Annotation dataScopeAnnotation = dataClass.getAnnotation(DataScope.class);
            if ((dataScopeAnnotation != null) && (((DataScope)dataScopeAnnotation).type() == EScopeType.CONSTANTS)) {
                log.debug(dataClass.getName() + " is constant => do not virtualize");
                initTable();
            } else {            
                initVirtualTable();
            }
        } else {
            initTable();
        }
    }
    
    private void initVirtualTable() {
        Class<T> dataClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        String srcClass = DatabaseTableBuffered.class.getName();
        try {
            Class rc = Class.forName(srcClass);
            Constructor<at.darkdestiny.framework.core.DatabaseTable> constructor = rc.getConstructor(Class.class,boolean.class);
            constructor.setAccessible(true);
            datasource = constructor.newInstance(dataClass,true);
            // Logger.getLogger().write("Set datasource to " + datasource);
        } catch (Exception e) {
            log.error("Loading of " + dataClass.getName() + " failed: " + e.toString());
            e.printStackTrace();
        }        
    }
    
    private void initTable() {
        boolean readOnlyClass = false;
        if (getClass().getSuperclass().equals(ReadOnlyTable.class)) {
            readOnlyClass = true;
        }

        // DBCoreUtils.inspect(this);
        Class<T> dataClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        String srcClass = DataSourceConfig.getInstance().getClassForModel(dataClass);
        
        try {
            Class rc = Class.forName(srcClass);            
            Constructor<at.darkdestiny.framework.core.DatabaseTable> constructor;
            if (rc == DatabaseTablePrimitive.class) {
                constructor = rc.getConstructor(Class.class);
                constructor.setAccessible(true);
                datasource = constructor.newInstance(dataClass);
            } else {
                constructor = rc.getConstructor(Class.class,boolean.class);
                constructor.setAccessible(true);
                datasource = constructor.newInstance(dataClass,false);
            }
                                    
            // log.debug( "Set datasource to " + datasource);
        } catch (Exception e) {
            log.error("Loading of " + dataClass.getName() + " failed: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public T get(QueryKeySet qks) {
        try {
            T result = (T) datasource.getEntry(qks);

            return result;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in get: ", e);
            return null;
        }
    }

    public ArrayList<T> get(ArrayList<T> entries) {
        try {
            ArrayList<T> result = datasource.getEntries(entries);
            return result;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in get: ", e);
            return null;
        }
    }

    @Override
    public boolean contains(QueryKeySet qks) {
        try {
            long startTime = System.currentTimeMillis();
            T result = (T) datasource.getEntry(qks);
            //// log.debug( "Contains time was " + (System.currentTimeMillis() - startTime) + "ms");

            return (result != null);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in contains: ", e);
            return false;
        }
    }

    public T get(T entry) {
        try {
            // long startTime = System.currentTimeMillis();
            T result = (T) datasource.getEntry(entry);
            // log.debug( "GetQuery time was " + (System.currentTimeMillis() - startTime) + "ms");

            return result;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in get: ", e);
            return null;
        }
    }

    public ArrayList<T> find(T entry) {
        ArrayList<T> result = null;

        try {
            long startTime = System.currentTimeMillis();
            result = datasource.getEntries(entry);
            // log.debug( "FindQuery time was " + (System.currentTimeMillis() - startTime) + "ms");
        } catch (Exception iie) {
            DebugBuffer.writeStackTrace("Error in find: ", iie);
            return null;
        }

        return result;
    }

    @Override
    public ArrayList<T> find(String indexName, QueryKeySet qks) {
        ArrayList<T> result = null;

        try {
            long startTime = System.currentTimeMillis();
            result = datasource.getEntries(qks, indexName);
            // // log.debug( "FindQuery time was " + (System.currentTimeMillis() - startTime) + "ms");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in find: ", e);
            return null;
        }

        return result;
    }

    @Override
    public ArrayList<T> findAll() {
        ArrayList<T> result = null;

        try {
            long startTime = System.currentTimeMillis();
            result = datasource.getAllEntries();
            //  // log.debug( "FindAllQuery time was " + (System.currentTimeMillis() - startTime) + "ms");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in findAll: ", e);
            return null;
        }

        return result;
    }

    public Exception getWriteLock() {
        return datasource.getWriteLock();
    }
}
