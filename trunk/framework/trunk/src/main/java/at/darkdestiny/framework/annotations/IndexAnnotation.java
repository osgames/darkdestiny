/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author Stefan
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface IndexAnnotation {    
    public String indexName();
}
