/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used to annotate data which is created by the user, are constants or both.
 * This is used to automatically create constants form a db or delete all user
 * generated content
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DataScope {

    public EScopeType type();

    public enum EScopeType {

        USER_GENERATED, CONSTANTS, MIXED
    }
}
