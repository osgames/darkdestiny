/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.core;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public abstract class DatabaseTable<T> implements DBTableInterface<T> {          
    protected HashMap<String,String> fieldToDBName = new HashMap<String,String>();
    protected HashSet<String> mappedFields = new HashSet<String>();
    protected HashMap<String,String> indexedFields = new HashMap<String,String>();
    protected HashSet<String> primaryFields = new HashSet<String>();
    protected HashMap<String,ForeignKey> foreignKeys = new HashMap<String,ForeignKey>();
    
    public Exception getWriteLock() {
        return null;
    }    
}
