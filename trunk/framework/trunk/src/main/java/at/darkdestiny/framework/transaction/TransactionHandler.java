/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.transaction;

import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class TransactionHandler {
    // private static ArrayList<TransactionHandler> transactions = new ArrayList<TransactionHandler>();

    private static HashMap<String, TransactionHandler> transactions =
            new HashMap<String, TransactionHandler>();
    private ArrayList<BaseTransaction> transactionList = new ArrayList<BaseTransaction>();
    private String transactionLog = "";
    private ArrayList<BaseTransaction> procTransactions = new ArrayList<BaseTransaction>();
    /*
     private HashMap<TransactionHandler, ArrayList<ReadWriteTable>> participants =
     new HashMap<TransactionHandler, ArrayList<ReadWriteTable>>();
     */
    private TransactionException transactionError = null;

    private TransactionHandler() {
    }

    public static TransactionHandler getTransactionHandler() {
        TransactionHandler th = new TransactionHandler();
        return th;
    }

    public void startTransaction() {
        transactions.put(Thread.currentThread().toString(), this);
    }

    public void endTransaction() {
        transactions.remove(Thread.currentThread().toString());
    }

    public static TransactionHandler getCurrentTransaction() {
        return transactions.get(Thread.currentThread().toString());
    }

    public void setTransactionError(TransactionException transactionError) {
        this.transactionError = transactionError;
    }

    public TransactionException getTransactionError() {
        return this.transactionError;
    }

    public boolean isError() {
        return (!(transactionError == null));
    }

    /*
     public void addParticipant(ReadWriteTable rwt) {
     if (participants.containsKey(this)) {
     participants.get(this).add(rwt);
     } else {
     ArrayList<ReadWriteTable> rwtList = new ArrayList<ReadWriteTable>();
     rwtList.add(rwt);
     participants.put(this, rwtList);
     }

     rwt.setTransactionMode(this);
     }

     public void unregisterParticipants() {
     for (ReadWriteTable rwt : participants.get(this)) {
     rwt.resetTransactionMode();
     }

     participants.remove(this);
     }
     */
    public void addTransaction(BaseTransaction t) {
        transactionList.add(t);
    }

    public ArrayList<Model> executeTransactionMultiple(TransactionMultiple t) throws TransactionException {
        ArrayList<Model> result = null;

        try {
            transactionLog += "Try to execute TransactionMultiple " + t + "<BR>";
            result = (ArrayList<Model>) t.execute();
            transactionLog += "TransactionMultiple " + t + " executed successfully<BR>";
            procTransactions.add(0, t);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Execute TransactionMultiple failed: ", e);
            transactionLog += "TransactionMultiple " + t + " failed<BR>";
            throw new TransactionException("TransactionMultiple failed", e);
        }

        return result;
    }

    public Model executeTransaction(Transaction t) throws TransactionException {
        Model result = null;

        try {
            transactionLog += "Try to execute Transaction " + t + "<BR>";
            result = (Model) t.execute();
            transactionLog += "Transaction " + t + " executed successfully<BR>";
            procTransactions.add(0, t);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Execute Transaction failed: ", e);
            transactionLog += "Transaction " + t + " failed<BR>";
            throw new TransactionException("Transaction failed", e);
        }

        return result;
    }

    public void executeTransaction(DatabaseTransaction t) throws TransactionException {
        try {
            transactionLog += "Try to execute DatabaseTransaction " + t + "<BR>";
            t.execute();
            transactionLog += "DatabaseTransaction " + t + " executed successfully<BR>";
            procTransactions.add(0, t);
        } catch (Exception e) {
            transactionLog += "DatabaseTransaction " + t + " failed<BR>";
            throw new TransactionException("DatabaseTransaction failed", e);
        }
    }

    public void rollback() throws TransactionException {
        String transactionName = "?";

        try {
            for (BaseTransaction t : procTransactions) {
                if (t instanceof DatabaseTransaction) {
                    transactionName = "DatabaseTransaction";
                } else if (t instanceof Transaction) {
                    transactionName = "Transaction";
                } else if (t instanceof TransactionMultiple) {
                    transactionName = "TransactionMultiple";
                }

                transactionLog += "Try to rollback " + transactionName + " " + t + "<BR>";
                t.rollback();
                transactionLog += "Rolled back " + transactionName + " " + t + "<BR>";
            }
        } catch (TransactionException e2) {
            DebugBuffer.writeStackTrace("Rollback failed: ", e2);
            // rollback failed
            transactionLog += "Error (" + e2.getMessage() + "): Rollback of " + transactionName + " failed!<BR>";
            if (e2.getUnderlyingException() != null) {
                transactionLog += "Underlying Exception: " + e2.getUnderlyingException().getMessage() + "<BR>";
            }
            throw new TransactionException("Rollback of Transaction failed", e2);
        }
    }

    public Model execute() throws TransactionException {
        // Lock all tables for this transaction

        // Execute transactions
        try {
            for (BaseTransaction t : transactionList) {
                transactionLog += "Try to execute transaction " + t + "<BR>";
                t.execute();
                procTransactions.add(0, t);
                transactionLog += "Executed transaction " + t + "<BR>";
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Transaction failed: ", e);
            // On error roll back all transactions
            transactionLog += "Error (" + e.getMessage() + "): Rolling back " + procTransactions.size() + " transaction(s)<BR>";

            try {
                for (BaseTransaction t : procTransactions) {
                    transactionLog += "Try to rollback transaction " + t + "<BR>";
                    t.rollback();
                    transactionLog += "Rolled back transaction " + t + "<BR>";
                }
            } catch (Exception e2) {
                DebugBuffer.writeStackTrace("Rollback failed: ", e2);
                // rollback failed
                transactionLog += "Error (" + e2.getMessage() + "): Rollback of transaction failed!<BR>";
                throw new TransactionException("Rollback of Transaction failed", e2);
            }

            // transaction failed
            throw new TransactionException("Transaction failed", e);
        }

        return null;
    }

    public String getTransactionLog() {
        return transactionLog;
    }
}
