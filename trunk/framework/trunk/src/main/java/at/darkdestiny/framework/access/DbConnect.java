package at.darkdestiny.framework.access;

 import at.darkdestiny.util.AbstractGameConfig;import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.sql.Connection;
import java.util.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class DbConnect {

    private static final Logger log = LoggerFactory.getLogger(DbConnect.class);

    private Connection conn = null;
    private static HashMap<Integer, DbConnect> connBuffer = new HashMap<Integer, DbConnect>();
    private final String dbName;
    private static final int CONN_VALIDATION_TIME = 1000 * 60 * 60;
    private Stack<TimedConnection> writeConnectionStack = new Stack<TimedConnection>();
    private HashMap<Thread, TimedConnection> usedConnections = new HashMap<Thread, TimedConnection>();
    private TimedConnection readConnection;
    private static DbConnect instance = null;
    private AbstractGameConfig config;

    private static DbConnect getInstanceById(int id) {
        Integer connId = Integer.valueOf(id);

        if (connBuffer.get(connId) != null) {
            return connBuffer.get(connId);
        } else {
            connBuffer.put(connId, new DbConnect());
            return connBuffer.get(connId);
        }
    }

    public static DbConnect getInstance(AbstractGameConfig aConfig, boolean forceNewConfig) {
        if (instance == null || forceNewConfig) {
            instance = new DbConnect(aConfig);
        }

        return instance;
    }
    public static DbConnect getInstance(AbstractGameConfig aConfig) {
        if (instance == null) {
            instance = new DbConnect(aConfig);
        }

        return instance;
    }

    public static DbConnect getInstance() {
        if (instance == null) {
            System.err.println("Instance is null, no config set use the constructor getInstance(AbstractGameConfig aConfig) once first");
            System.exit(1);
        }

        return instance;
    }

    public Connection getNewConnection(boolean readOnly) {
        try {
            log.debug( "Recreating a timed out connection");

            Connection c = DriverManager.getConnection(config.getDatabase(), config.getUsername(), config.getPassword());
            if (readOnly) {
                c.setReadOnly(true);
            }

            return c;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in DBConnect: ", e);
            throw new RuntimeException(e);
        }
    }

    public synchronized static void assignWriteConnection() {
        if (!getInstance().usedConnections.containsKey(Thread.currentThread())) {
            while (getInstance().writeConnectionStack.isEmpty()) {
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                }
            }

            TimedConnection tc = getInstance().writeConnectionStack.pop();

            String cName = tc.getName();
            // DebugBuffer.trace("Pop " + cName);

            try {
                Connection c = tc.getConn();

                // Revalidate Connection
                if (tc.getLastCheck() < (System.currentTimeMillis() - CONN_VALIDATION_TIME)) {
                    if (!c.isValid(5000)) {
                        try {
                            c.close();
                        } catch (Exception e) {
                        }

                        tc = new TimedConnection(getInstance().getNewConnection(false));
                        tc.setName(cName);
                        // DebugBuffer.trace("Replace " + cName);
                    } else {
                        // DebugBuffer.trace(cName + " successfully verified");
                        tc.setLastCheck(System.currentTimeMillis());
                        // DebugBuffer.trace("Reset Timer on " + cName);
                    }
                } else {
                    if (c.isClosed()) {
                        tc = new TimedConnection(getInstance().getNewConnection(false));
                        tc.setName(cName);
                        DebugBuffer.trace("Replace " + cName);
                    }
                }

                getInstance().usedConnections.put(Thread.currentThread(), tc);
                // DebugBuffer.trace("Assign " + cName + " to " + Thread.currentThread().getName());
            } catch (AbstractMethodError ame) {
                DebugBuffer.warning("Connection.isValid() is not supported, fallback to old procedure");

                Connection c = tc.getConn();
                try {
                    Statement check = c.createStatement();
                    check.executeQuery("SELECT VERSION()");
                    // DebugBuffer.trace(cName + " successfully verified");
                } catch (SQLException eInner) {
                    try {
                        c.close();
                    } catch (Exception e) {
                    }

                    tc = new TimedConnection(getInstance().getNewConnection(false));
                    tc.setName(cName);
                    // DebugBuffer.trace("Replace " + cName);
                } finally {
                    tc.setLastCheck(System.currentTimeMillis());
                    // DebugBuffer.trace("Reset Timer on " + cName);
                }

                getInstance().usedConnections.put(Thread.currentThread(), tc);
                // DebugBuffer.trace("Assign " + cName + " to " + Thread.currentThread().getName());
            } catch (SQLException e) {
                DebugBuffer.writeStackTrace("Error in DBConnect: ", e);
                throw new RuntimeException(e);
            }

            // log.debug( "Assigned Write Connection [LEFT: "+getInstance().writeConnectionStack.size()+"]");
        }
    }

    public synchronized static void releaseWriteConnection() {
        if (getInstance().usedConnections.containsKey(Thread.currentThread())) {
            getInstance().writeConnectionStack.push(getInstance().usedConnections.remove(Thread.currentThread()));
            // log.debug( "Released Write Connection [LEFT: "+getInstance().writeConnectionStack.size()+"]");
        }
    }

    private synchronized static DbConnect getInstanceByTime() {
        int currTime = (int) (System.currentTimeMillis() / 600000);
        Integer connId = Integer.valueOf(currTime);

        // If there is a timeout connection delete it
        for (int i = (currTime - 144); i < (currTime - 1); i++) {
            Integer oldConnId = Integer.valueOf(i);
            if (connBuffer.containsKey(oldConnId)) {
                try {
                    ((DbConnect) connBuffer.remove(oldConnId)).closeConnection();
                    connBuffer.remove(oldConnId);
                } catch (Exception e) {
                    DebugBuffer.writeStackTrace("Connection error ", e);
                }
            }
        }

        if (connBuffer.get(connId) != null) {
            return connBuffer.get(connId);
        } else {
            connBuffer.put(connId, new DbConnect());
            DebugBuffer.addLine(DebugLevel.TRACE, "Open connections: " + connBuffer.size());
            return (DbConnect) connBuffer.get(connId);
        }
    }

    private DbConnect(AbstractGameConfig aConfig) {
        this.config = aConfig;
        try {

            if (config == null) {
                System.err.print("GameConfig has to be set to DBConnect at this point!");
                System.exit(1);
            }

            if (config.getDatabase() == null) {
                System.err.print("Databasename has to be set");
                System.exit(1);
            }
            String dbUrl = config.getDatabase();
            String dbUser = config.getUsername();
            String dbPass = config.getPassword();

            log.debug( "Get Connection : " + dbUrl);
            log.debug( "User : " + dbUser);
            log.debug( "Password : " + dbPass);

            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(dbUrl, dbUser, dbPass);

            // Fill stacks
            for (int i = 0; i < 10; i++) {
                writeConnectionStack.push(new TimedConnection(DriverManager.getConnection(dbUrl, dbUser, dbPass)));
            }
            readConnection = new TimedConnection(conn);

            int lastIndex = dbUrl.lastIndexOf("/");
            int stopAt = dbUrl.indexOf("?");

            if (stopAt != -1) {
                log.debug( "DB SOURCE=" + dbUrl.substring(lastIndex + 1, stopAt));
                dbName = dbUrl.substring(lastIndex + 1, stopAt);
            } else {
                log.debug( "DB SOURCE=" + dbUrl.substring(lastIndex + 1));
                dbName = dbUrl.substring(lastIndex + 1);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in DBConnect: ", e);
            throw new RuntimeException(e);
        }
    }

    private DbConnect() {

        this(null);
    }

    public Connection getConn() {
        TimedConnection tmpConn = usedConnections.get(Thread.currentThread());
        if (tmpConn == null) {
            tmpConn = readConnection;
        }

        Connection outConn = tmpConn.getConn();

        try {
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in DBConnect.getConn(): ", e);
        }
        return outConn;
    }

    public static String getDbName() {
        return getInstanceByTime().dbName;
    }

    public static Connection getConnection() {
        return getInstance().getConn();
    }

    public static Connection getConnectionById(int id) {
        DbConnect db2 = getInstanceById(id);
        return db2.getConn();
    }

    public static void removeConnection(int id) {
        Integer connId = Integer.valueOf(id);
        if (connBuffer.get(connId) != null) {
            ((DbConnect) connBuffer.get(connId)).closeConnection();
            connBuffer.remove(connId);
        }
    }

    private void closeConnection() {
        try {
            DebugBuffer.addLine(DebugLevel.DEBUG, "CLOSING A TIMED OUT CONNECTION");
            conn.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while closing Connection ", e);
        }
    }

    public static Statement createStatement() {
        try {
            if (!getInstance().usedConnections.containsKey(Thread.currentThread())) {
                TimedConnection tc = getInstance().readConnection;
                if (tc.getConn().isClosed()) {
                    getInstance().readConnection = new TimedConnection(getInstance().getNewConnection(true));
                }

                return getInstance().readConnection.getConn().createStatement();
            } else {
                return getInstance().usedConnections.get(Thread.currentThread()).getConn().createStatement();
            }
        } catch (SQLException e) {
            DebugBuffer.writeStackTrace("SQL-Exception:", e);
            return null;
        }
    }

    public static void forceConnectionReset() {
        for (Map.Entry<Integer, DbConnect> dbConns : connBuffer.entrySet()) {
            dbConns.getValue().closeConnection();
        }

        connBuffer.clear();
    }

    public static PreparedStatement prepareStatement(String pstmt, boolean getAutogeneratedKeys) {
        try {
            if (!getInstance().usedConnections.containsKey(Thread.currentThread())) {
                Connection c = getInstance().readConnection.getConn();
                if (c.isClosed()) {
                    getInstance().readConnection = new TimedConnection(getInstance().getNewConnection(true));
                }

                if (getAutogeneratedKeys) {
                    return getInstance().readConnection.getConn().prepareStatement(pstmt, PreparedStatement.RETURN_GENERATED_KEYS);
                } else {
                    return getInstance().readConnection.getConn().prepareStatement(pstmt);
                }
            } else {
                if (getAutogeneratedKeys) {
                    return getInstance().usedConnections.get(Thread.currentThread()).getConn().prepareStatement(pstmt, PreparedStatement.RETURN_GENERATED_KEYS);
                } else {
                    return getInstance().usedConnections.get(Thread.currentThread()).getConn().prepareStatement(pstmt);
                }
            }
        } catch (SQLException e) {
            DebugBuffer.writeStackTrace("SQL-Exception:", e);
            return null;
        }
    }
}
