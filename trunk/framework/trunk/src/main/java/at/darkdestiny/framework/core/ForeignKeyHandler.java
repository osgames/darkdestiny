/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.core;

import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class ForeignKeyHandler {

    private static final Logger log = LoggerFactory.getLogger(ForeignKeyHandler.class);

    private HashMap<Class, HashMap<String, ForeignKey>> fkList = new HashMap<Class, HashMap<String, ForeignKey>>();
    private HashMap<Model, HashMap<String, ArrayList<Model>>> loadedChilds = new HashMap<Model, HashMap<String, ArrayList<Model>>>();
    private static ForeignKeyHandler instance;

    protected static ForeignKeyHandler getInstance() {
        if (instance == null) {
            instance = new ForeignKeyHandler();
        }

        return instance;
    }

    protected void addForeignKey(Class c, String variableName, ForeignKey fk) {
        HashMap<String, ForeignKey> keyList = fkList.get(c);
        if (keyList == null) {
            keyList = new HashMap<String, ForeignKey>();
            keyList.put(variableName, fk);
            fkList.put(c, keyList);
        } else {
            keyList.put(variableName, fk);
        }
    }

    protected void loadChilds(LazyArrayList list, Model enclosingModel, String variableName) {
        log.debug( "ArrayList is requesting loading");

        try {
            ForeignKey fk = fkList.get(enclosingModel.getClass()).get(variableName);
            log.debug( "ForeignKey Found");

            Class mappingClass = null;
            Class childClass;

            childClass = Class.forName("at.darkdestiny.web.dao." + fk.getChildTableName() + "DAO");
            if (fk.getMappingTable().equalsIgnoreCase("")) {
                mappingClass = Class.forName("at.darkdestiny.web.dao." + fk.getMappingTable() + "DAO");
            }

            Field arrayList = enclosingModel.getClass().getDeclaredField(variableName);

            ReadOnlyTable mappingTable = null;
            ReadOnlyTable childTable = (ReadOnlyTable) DAOFactory.get(childClass);
            log.debug( "Child DAO Class found successfully");
            if (mappingClass != null) {
                mappingTable = (ReadOnlyTable) DAOFactory.get(mappingClass);
                log.debug( "Mapping DAO Class found successfully");
            }

            Field idField = enclosingModel.getClass().getDeclaredField(fk.getBaseFieldName());
            idField.setAccessible(true);
            log.debug( "IdField Found");

            Class mappingModel = null;
            Class childModel = Class.forName("at.darkdestiny.web.model." + fk.getChildTableName());
            if (mappingClass != null) {
                mappingModel = Class.forName("at.darkdestiny.web.model." + fk.getMappingTable());
            }

            Model mappingModelInstance = null;
            Constructor childConstructor = childModel.getConstructor();
            Model childModelInstance = (Model) childConstructor.newInstance();
            log.debug( "Child Model instantiated");
            if (mappingClass != null) {
                Constructor mappingConstructor = mappingModel.getConstructor();
                mappingModelInstance = (Model) mappingConstructor.newInstance();
                log.debug( "Mapping Model instantiated");
            }

            ArrayList<Model> mappingList = null;
            ArrayList<Model> tmpResultList = new ArrayList<Model>();

            if (mappingClass != null) {
                Field baseRefField = mappingModelInstance.getClass().getDeclaredField(enclosingModel.getClass().getSimpleName() + "_" + fk.getBaseFieldName() + "_id");
                baseRefField.setAccessible(true);
                baseRefField.set(mappingModelInstance, idField.get(enclosingModel));
                mappingList = mappingTable.find(mappingModelInstance);

                for (Model resEntry : mappingList) {
                    Field targetRefField = childModelInstance.getClass().getDeclaredField(fk.getChildTableName() + "_" + fk.getChildFieldName() + "_id");
                    targetRefField.setAccessible(true);
                    targetRefField.set(childModelInstance, idField.get(resEntry));
                    tmpResultList.addAll(childTable.find(childModelInstance));
                }
            } else {
                Field childField = childModelInstance.getClass().getDeclaredField(fk.getChildFieldName());
                childField.setAccessible(true);
                childField.set(childModelInstance, idField.get(enclosingModel));
                tmpResultList = childTable.find(childModelInstance);
            }

            // Move loaded Childs to Model (otherwise memory will fill up)
            if (!loadedChilds.containsKey(enclosingModel)) {
                HashMap<String, ArrayList<Model>> elements = new HashMap<String, ArrayList<Model>>();
                loadedChilds.put(enclosingModel, elements);
            }

            if (!loadedChilds.get(enclosingModel).containsKey(variableName)) {
                loadedChilds.get(enclosingModel).put(variableName, new ArrayList<Model>());
            }

            loadedChilds.get(enclosingModel).get(variableName).clear();
            for (Object tmpObj : tmpResultList) {
                loadedChilds.get(enclosingModel).get(variableName).add((Model) ((Model) tmpObj).clone());
            }

            list.addAllInternal(tmpResultList);
            log.debug( "ArrayList loaded >> ItemsFound: " + tmpResultList.size() + " for foreignKey " + idField.get(enclosingModel) + " on field " + fk.getChildFieldName());
        } catch (NoSuchFieldException nsfe) {
            log.error("Field " + variableName + " does not exist in class " + enclosingModel.getClass());
        } catch (ClassNotFoundException cnfe) {
            log.error("Class \"at.darkdestiny.web.dao." + enclosingModel.getClass().getName() + "DAO\" does not exist");
        } catch (NoSuchMethodException nsme) {
            nsme.printStackTrace();
        } catch (InstantiationException ie) {
            ie.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        } catch (InvocationTargetException iae) {
            iae.printStackTrace();
        }
    }

    // Compare childs to current list and do according actions
    protected void saveChilds(Model enclosingModel, String variableName) {
    }

    // Check if the ForeignKey field has valid values, if values are null fill them
    private void checkForeignKeyField(ArrayList<Model> elements, Model enclosingModel, ForeignKey fk) {
    }
}
