/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework;

/**
 *
 * @author Stefan
 */
public class TableIndexField {
    private final String fieldName;   
    private final Class dataType;
    private Object value;  
    
    public TableIndexField(String fieldName, Class dataType) {
        this.fieldName = fieldName;
        this.dataType = dataType;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Class getDataType() {
        return dataType;
    }
}
