/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.core;

import at.darkdestiny.framework.TableIndex;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class QueryMode {
    protected final static int QUERYMODE_INDEXED = 0;
    protected final static int QUERYMODE_INDEXED_MERGED = 1;
    protected final static int QUERYMODE_INDEXED_PARTIAL = 2;
    protected final static int QUERYMODE_DIRECT = 3;
    
    private final int queryMode;
    private final HashSet<TableIndex> indices;
    private final HashMap<String,Object> uncovered;
    
    public QueryMode(int queryMode, HashSet<TableIndex> indices, HashMap<String,Object> uncovered) {
        this.queryMode = queryMode;
        this.indices = indices;
        this.uncovered = uncovered;
    }

    public int getQueryMode() {
        return queryMode;
    }

    public HashSet<TableIndex> getIndices() {
        return indices;
    }

    public HashMap<String,Object> getUncovered() {
        return uncovered;
    }
}
