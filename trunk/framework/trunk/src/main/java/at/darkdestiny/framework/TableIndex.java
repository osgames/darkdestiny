/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TableIndex {
    private final String name;
    private final IndexType type;
    private final ArrayList<TableIndexField> tif;
    
    public TableIndex(String indexName, IndexType type, ArrayList<TableIndexField> tif) {
        this.name = indexName;
        this.type = type;
        this.tif = tif;
    }

    public String getName() {
        return name;
    }

    public IndexType getType() {
        return type;
    }

    public ArrayList<TableIndexField> getTableIndexFields() {
        return tif;
    }
}
