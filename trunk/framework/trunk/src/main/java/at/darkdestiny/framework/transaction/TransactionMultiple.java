/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.transaction;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.core.DatabaseTable;
import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.model.Model;
import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TransactionMultiple extends BaseTransaction {

    private final ReadWriteTable g;
    private final ArrayList<Model> mList;
    private final int type;
    private ArrayList<TransactionMultiple> reverseTransactions = new ArrayList<TransactionMultiple>();

    public TransactionMultiple(ReadWriteTable g, ArrayList<Model> mList, int type) {
        this.g = g;
        this.mList = mList;
        this.type = type;
    }

    protected ArrayList<Model> execute() throws TransactionException {
        ArrayList<Model> result = null;

        switch (type) {
            case (TRANSACTION_TYPE_UPDATE):
                TransactionMultiple rb = new TransactionMultiple(g, getCurrentEntries(mList), Transaction.TRANSACTION_TYPE_UPDATE);
                reverseTransactions.add(rb);
                try {
                    result = getDataSource().mergeAll(mList);
                } catch (Exception e) {
                    e.printStackTrace();
                    getTransaction().setTransactionError(new TransactionException("Internal Transaction Error", e));
                }
                // result = g.updateAll(mList);
                if (getTransaction().isError()) {
                    throw getTransaction().getTransactionError();
                }
                return result;
            case (TRANSACTION_TYPE_INSERT):
                try {
                    result = getDataSource().insertAll(mList);
                } catch (Exception e) {
                    e.printStackTrace();
                    getTransaction().setTransactionError(new TransactionException("Internal Transaction Error", e));
                }
                // result = g.insertAll(mList);
                if (getTransaction().isError()) {
                    throw getTransaction().getTransactionError();
                }
                rb = new TransactionMultiple(g, result, TransactionMultiple.TRANSACTION_TYPE_DELETE);
                reverseTransactions.add(rb);
                return result;
            case (TRANSACTION_TYPE_DELETE):
                rb = new TransactionMultiple(g, getCurrentEntries(mList), TransactionMultiple.TRANSACTION_TYPE_INSERT);
                reverseTransactions.add(rb);
                // int resp = g.removeAll(mList);
                try {
                    getDataSource().deleteAll(mList);
                } catch (Exception e) {
                    e.printStackTrace();
                    getTransaction().setTransactionError(new TransactionException("Internal Transaction Error", e));
                }
                if (getTransaction().isError()) {
                    throw getTransaction().getTransactionError();
                }
                return null;
            case (TRANSACTION_TYPE_INSERT_OR_UPDATE):
                return null;
        }

        return result;
    }

    protected void rollback() {
        for (TransactionMultiple tm : reverseTransactions) {
            switch (tm.type) {
                case (TRANSACTION_TYPE_UPDATE):
                    g.updateAll(tm.mList);
                    break;
                case (TRANSACTION_TYPE_INSERT):
                    g.insertAll(tm.mList);
                    break;
                case (TRANSACTION_TYPE_DELETE):
                    g.removeAll(tm.mList);
                    break;
            }
        }
    }

    private ArrayList<Model> getCurrentEntries(ArrayList<Model> mList) {
        return g.get(mList);
    }

    private DatabaseTable getDataSource() throws TransactionException {
        try {
            Field f = g.getClass().getSuperclass().getSuperclass().getDeclaredField("datasource");
            f.setAccessible(true);
            return (DatabaseTable) f.get(g);
        } catch (Exception e) {
            e.printStackTrace();
            throw new TransactionException("Internal Transaction Error", e);
        }
    }
}
