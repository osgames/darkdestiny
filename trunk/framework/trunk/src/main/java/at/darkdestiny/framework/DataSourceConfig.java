/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework;

import at.darkdestiny.framework.core.DatabaseTableBuffered;
import at.darkdestiny.framework.core.DatabaseTablePrimitive;
import java.util.HashMap;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class DataSourceConfig {
    private static final Logger log = LoggerFactory.getLogger(DataSourceConfig.class);

    private static DataSourceConfig instance;
    private final HashMap<String, String> tableType = new HashMap<String, String>();
    private String modelPackagePath = "at.darkdestiny.core.model.";

    public static DataSourceConfig getInstance() {
        if (instance == null) {
            instance = new DataSourceConfig();
        }
        return instance;
    }

    private DataSourceConfig() {
        ResourceBundle rb = ResourceBundle.getBundle("tableconfig");

        for (String key : rb.keySet()) {
            TableType tt = TableType.valueOf(rb.getString(key));

            if (tt == TableType.SOURCE_BUFFERED) {
                tableType.put(key, DatabaseTableBuffered.class.getName());
            } else if (tt == TableType.SOURCE_DATABASE) {
                tableType.put(key, DatabaseTablePrimitive.class.getName());
            }
        }
    }

    public String getClassForModel(Class m) {
        String mName = m.getName().replace(modelPackagePath, "");
        if (tableType.get(mName) == null) {
            log.error("Could not find table config entry for " + m);
            return null;
        }
        return tableType.get(mName);
    }

    /**
     * @param modelPackagePath the modelPackagePath to set
     */
    public void setModelPackagePath(String modelPackagePath) {
        this.modelPackagePath = modelPackagePath;
    }
}
