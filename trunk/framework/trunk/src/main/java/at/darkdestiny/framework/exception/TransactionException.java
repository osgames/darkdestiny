/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.exception;

/**
 *
 * @author Stefan
 */
public class TransactionException extends RuntimeException {
    private final Exception underlyingException;
    
    public TransactionException(String msg) {
        super(msg);
        underlyingException = null;
    }
    
    public TransactionException(String msg, Exception e) {
        super(msg);
        underlyingException = e;
    }

    public Exception getUnderlyingException() {
        return underlyingException;
    }
}
