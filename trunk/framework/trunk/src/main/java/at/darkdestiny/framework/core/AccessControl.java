/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.core;

 import org.slf4j.Logger;import org.slf4j.LoggerFactory;
;

/**
 *
 * @author Stefan
 */
public class AccessControl {

    private static final Logger log = LoggerFactory.getLogger(AccessControl.class);

    private int givenLocks;
    private int waitingWriters;
    public static boolean TRACE = false;
    private Object mutex;

    private Exception lastObtainedWriteLock = null;
    private final DatabaseTable owner;

    public AccessControl(DatabaseTable dt) {
        owner = dt;
        mutex = new Object();
        givenLocks = 0;
        waitingWriters = 0;
    }

    public Exception getLocks() {
        if (givenLocks == -1) {
            return lastObtainedWriteLock;
        } else {
            return null;
        }
    }

    public void getReadLock() {
        synchronized (mutex) {

            try {
                while ((givenLocks == -1) || (waitingWriters != 0)) {
                    if (TRACE) {
                        log.debug( Thread.currentThread().toString() + "waiting for readlock");
                    }
                    mutex.wait();
                }
            } catch (java.lang.InterruptedException e) {
                log.debug( e.getMessage());
            }

            givenLocks++;

            if (TRACE) {
                log.debug( Thread.currentThread().toString() + " got readlock, GivenLocks = " + givenLocks);
            }
        }
    }

    public void getWriteLock() {
        synchronized (mutex) {
            waitingWriters++;
            try {
                while (givenLocks != 0) {
                    if (TRACE) {
                        String tableName = "";

                        if (owner instanceof DatabaseTableBuffered) {
                            tableName = ((DatabaseTableBuffered)owner).tableName;
                        } else if (owner instanceof DatabaseTablePrimitive) {
                            tableName = ((DatabaseTablePrimitive)owner).tableName;
                        }
                        log.info(Thread.currentThread().toString() + "waiting for writelock @ " + tableName);
                    }
                    mutex.wait();
                }
            } catch (java.lang.InterruptedException e) {
                log.debug( e.getMessage());
            }

            waitingWriters--;
            givenLocks = -1;

            if (TRACE) {
                lastObtainedWriteLock = new Exception("This function caused another thread to wait for writelock");
                log.debug( Thread.currentThread().toString() + " got writelock, GivenLocks = " + givenLocks);
            }
        }
    }

    public void releaseLock() {
        synchronized (mutex) {
            if (givenLocks == 0) {
                return;
            }
            if (givenLocks == -1) {
                // log.debug( "Release writelock");
                givenLocks = 0;
            } else {
                givenLocks--;
            }
            if (TRACE) {
                log.debug( Thread.currentThread().toString() + " released lock, GivenLocks = " + givenLocks);
            }
            mutex.notifyAll();
        }
    }
}
