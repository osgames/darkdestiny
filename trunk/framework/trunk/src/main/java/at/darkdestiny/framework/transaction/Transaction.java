/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.transaction;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.core.DatabaseTable;
import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.model.Model;
import java.lang.reflect.Field;

/**
 *
 * @author Stefan
 */
public class Transaction extends BaseTransaction {

    private final ReadWriteTable g;
    private final Model m;
    private final int type;
    private Transaction reverseTransaction = null;

    public Transaction(ReadWriteTable g, Model m, int type) {
        this.g = g;
        this.m = m;
        this.type = type;
    }

    protected Object execute() throws TransactionException {
        Model result = null;

        switch (type) {
            case (TRANSACTION_TYPE_UPDATE):
                Transaction rb = new Transaction(g, (Model) getCurrentEntry(m), Transaction.TRANSACTION_TYPE_UPDATE);
                this.reverseTransaction = rb;
                try {
                    result = (Model) getDataSource().merge(m);
                } catch (Exception e) {
                    e.printStackTrace();
                    getTransaction().setTransactionError(new TransactionException("Internal Transaction Error", e));
                }
                // result = (Model)g.update(m);
                if (getTransaction().isError()) {
                    throw getTransaction().getTransactionError();
                }
                return result;
            case (TRANSACTION_TYPE_INSERT):
                try {
                    result = (Model) getDataSource().insert(m);
                    // result = (Model)g.add(m);
                } catch (Exception e) {
                    e.printStackTrace();
                    getTransaction().setTransactionError(new TransactionException("Internal Transaction Error", e));
                }
                if (getTransaction().isError()) {
                    throw getTransaction().getTransactionError();
                }
                rb = new Transaction(g, result, Transaction.TRANSACTION_TYPE_DELETE);
                this.reverseTransaction = rb;
                return result;
            case (TRANSACTION_TYPE_DELETE):
                rb = new Transaction(g, (Model) getCurrentEntry(m), Transaction.TRANSACTION_TYPE_INSERT);
                this.reverseTransaction = rb;
                try {
                    // g.remove(m);
                    getDataSource().delete(m);
                } catch (Exception e) {
                    e.printStackTrace();
                    getTransaction().setTransactionError(new TransactionException("Internal Transaction Error", e));
                }
                if (getTransaction().isError()) {
                    throw getTransaction().getTransactionError();
                }
                return null;
        }

        return result;
    }

    protected void rollback() {
        switch (reverseTransaction.type) {
            case (TRANSACTION_TYPE_UPDATE):
                g.update(reverseTransaction.m);
                break;
            case (TRANSACTION_TYPE_INSERT):
                g.add(reverseTransaction.m);
                break;
            case (TRANSACTION_TYPE_DELETE):
                g.remove(reverseTransaction.m);
                break;
        }
    }

    private Object getCurrentEntry(Model m) {
        return g.get(m);
    }

    private DatabaseTable getDataSource() throws TransactionException {
        try {
            Field f = g.getClass().getSuperclass().getSuperclass().getDeclaredField("datasource");
            f.setAccessible(true);
            return (DatabaseTable) f.get(g);
        } catch (Exception e) {
            e.printStackTrace();
            throw new TransactionException("Internal Transaction Error", e);
        }
    }
}
