/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.core;

import at.darkdestiny.framework.QueryKeySet;
import at.darkdestiny.framework.exception.InternalFrameworkException;
import at.darkdestiny.framework.exception.InvalidIndexException;
import at.darkdestiny.framework.exception.PrimaryKeyInvalidException;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
interface DBTableInterface<T> {
    /**
     * Retrieves a record based on primary index
     *
     * @param entry A model with values in all primary fields
     *
     * @return Record which meets the primary key values or null
     *
     * @throws PrimaryKeyInvalidException if not all fields are specified
     * @throws InvalidIndexException if there is no primary key specified at all for this record
     * @throws InternalFrameworkException on any other errors
     */
    public T getEntry(T entry) throws PrimaryKeyInvalidException, InvalidIndexException, InternalFrameworkException;    
        
    /**
     * Retrieves a record based on primary index
     *
     * @param qks A QueryKeySet object containing all fields and values for the primary index
     *
     * @return Record which meets the primary key values or null
     *
     * @throws PrimaryKeyInvalidException if not all fields are specified
     * @throws InvalidIndexException if there is no primary key specified at all for this record
     * @throws InternalFrameworkException on any other errors
     */
    @Deprecated
    public T getEntry(QueryKeySet qks) throws PrimaryKeyInvalidException, InvalidIndexException, InternalFrameworkException;

    /**
     * Retrieves an ArrayList containing all records matching the specified
     * values in the given model, this operation is fast if there exists an
     * index for the queried fields, otherwise the most appropriate index will be
     * used. If none index is available the response time maybe bad.
     *
     * @param entry A model object containing all fields and values which shall be filtered
     *
     * @return An ArrayList with all records matching the given values
     *
     * @throws InternalFrameworkException if any error occures
     */
    public ArrayList<T> getEntries(T entry) throws InternalFrameworkException;

    /**
     * Retrieves a list of records base on all primary keys given.
     *
     * @param entries An ArrayList of models with the requested primary keys
     *
     * @return An ArrayList with all records matching the given primary keys
     *
     * @throws PrimaryKeyInvalidException if not all fields for primary key are specified
     * @throws InvalidIndexException if there is no primary key specified at all for this table
     * @throws InternalFrameworkException on any other errors
     */
    public ArrayList<T> getEntries(ArrayList<T> entries) throws PrimaryKeyInvalidException, InvalidIndexException, InternalFrameworkException;

    /**
     * Retrieves all records contained in this table.
     *
     * @return An ArrayList with all records
     *
     * @throws InternalFrameworkException if any other errors
     */
    public ArrayList<T> getAllEntries() throws InternalFrameworkException;
    public T merge(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException;
    public ArrayList<T> mergeAll(T entry, QueryKeySet qks) throws InternalFrameworkException;
    public ArrayList<T> mergeAll(ArrayList<T> entries) throws InternalFrameworkException;
    public T insert(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException;
    public ArrayList<T> updateOrInsert(ArrayList<T> entries) throws InternalFrameworkException;
    public ArrayList<T> insertAll(ArrayList<T> entries) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException;
    public void delete(T entry) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException;                         
    public int deleteAll(T entry) throws InternalFrameworkException;
    public int deleteAll(ArrayList<T> entries) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException;
    public int deleteAll(QueryKeySet qks) throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException;
    public int deleteAll() throws InternalFrameworkException, InvalidIndexException, PrimaryKeyInvalidException;
    public ArrayList<T> getEntries(QueryKeySet qks, String indexName) throws InternalFrameworkException, InvalidIndexException;
}
