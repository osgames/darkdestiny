/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.model;

import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;

/**
 *
 * @author HorstRabe
 */
@TableNameAnnotation(value = "testtable")
public class TestTable extends Model<TestTable> {

    @FieldMappingAnnotation("field1")
    @IdFieldAnnotation
    private Integer field1;
    @FieldMappingAnnotation("field2")
    @IndexAnnotation(indexName = "field2")
    private Double field2;
    @FieldMappingAnnotation("field3")
    @IndexAnnotation(indexName = "field3")
    private Integer field3;
    @FieldMappingAnnotation("field4")
    private Integer field4;

    public Integer getField1() {
        return field1;
    }

    public void setField1(Integer field1) {
        this.field1 = field1;
    }

    public Double getField2() {
        return field2;
    }

    public void setField2(Double field2) {
        this.field2 = field2;
    }

    public Integer getField3() {
        return field3;
    }

    public void setField3(Integer field3) {
        this.field3 = field3;
    }

    public Integer getField4() {
        return field4;
    }

    public void setField4(Integer field4) {
        this.field4 = field4;
    }
}
