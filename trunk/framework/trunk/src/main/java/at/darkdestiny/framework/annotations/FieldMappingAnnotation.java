/**
 * 
 */
package at.darkdestiny.framework.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author martin
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldMappingAnnotation {

	public String value();
}
