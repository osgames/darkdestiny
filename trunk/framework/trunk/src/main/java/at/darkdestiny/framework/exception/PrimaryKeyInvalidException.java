/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.framework.exception;

/**
 *
 * @author Stefan
 */
public class PrimaryKeyInvalidException extends Exception {
    public PrimaryKeyInvalidException(String msg) {
        super(msg);
    }
}
