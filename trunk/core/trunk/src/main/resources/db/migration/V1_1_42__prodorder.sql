DROP TABLE IF EXISTS `productionorderprogress`;
CREATE TABLE `productionorderprogress` (
  `productionOrderId` int(11) NOT NULL,
  `resourceId` mediumint(9) NOT NULL,
  `needed` int(11) NOT NULL,
  `processed` int(11) NOT NULL,
  PRIMARY KEY (`productionOrderId`,`resourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE planetressource CHANGE type type ENUM('PLANET','INSTORAGE','MINIMUM_PRODUCTION','MINIMUM_TRADE','MINIMUM_TRANSPORT','ORBITAL_SCRAP','SYSTEM_SCRAP','MINIMUM_CONSTRUCTION');
