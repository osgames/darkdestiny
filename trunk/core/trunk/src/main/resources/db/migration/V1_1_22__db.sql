INSERT IGNORE INTO `constructionrestriction` VALUES ('0', '82', 'ScientificAcademy');

DROP TABLE IF EXISTS `votestyle`;
CREATE TABLE `votestyle` (
  `voteId` mediumint(9) NOT NULL,
  `showVotings` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `overrideVotedText` varchar(250) DEFAULT NULL,
  `overrideNotVotedText` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`voteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

