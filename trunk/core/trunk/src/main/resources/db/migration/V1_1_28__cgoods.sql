DROP TABLE IF EXISTS `construction`;
CREATE TABLE `construction` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `infotext` varchar(50) DEFAULT NULL,
  `typplanet` varchar(50) DEFAULT NULL,
  `buildtime` int(11) DEFAULT NULL,
  `type` enum('ADMINISTRATION','ZIVIL','ENERGY','INDUSTRY','MILITARY','PLANETARY_DEFENSE') DEFAULT NULL,
  `orderNo` int(11) DEFAULT NULL,
  `population` bigint(20) DEFAULT NULL,
  `prodType` enum('COMMON','AGRAR','INDUSTRY','ENERGY','RESEARCH') DEFAULT NULL,
  `hp` int(11) DEFAULT NULL,
  `designId` int(11) DEFAULT NULL,
  `workers` bigint(20) DEFAULT NULL,
  `surface` int(11) DEFAULT NULL,
  `uniquePerPlanet` tinyint(3) DEFAULT NULL,
  `uniquePerSystem` tinyint(3) DEFAULT NULL,
  `isMine` int(11) DEFAULT NULL,
  `specialBuildingId` int(11) DEFAULT NULL,
  `minPop` int(11) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `destructible` tinyint(3) DEFAULT NULL,
  `idleable` tinyint(3) DEFAULT NULL,
  `levelable` tinyint(3) DEFAULT '0',
  `econType` enum('NOT_SPECIFIED','INDUSTRY','AGRICULTURE','RESEARCH','MILITARY','RECREATIONAL') NOT NULL DEFAULT 'NOT_SPECIFIED',
  `isVisible` tinyint(3) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `construction` VALUES ('2', 'db_construction_colonybase', 'db_construction_colonybase_desc', 'CEGJLM', '870', 'ADMINISTRATION', '0', '30000', 'COMMON', '5000', '0', '1', '10', '1', '0', '0', '0', '0', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('3', 'db_construction_localadministration', 'db_construction_localadministration_desc', 'CEGJLM', '2233', 'ADMINISTRATION', '10', '0', 'COMMON', '99999', '0', '0', '0', '1', '0', '0', '0', '1000000', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('4', 'db_construction_planetadministration', 'db_construction_planetadministration_desc', 'CEGJLM', '12760', 'ADMINISTRATION', '20', '0', 'COMMON', '99999', '0', '0', '0', '1', '0', '0', '0', '500000000', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('5', 'db_construction_planetgovernment', 'db_construction_planetgovernment_desc', 'CEGJLM', '26100', 'ADMINISTRATION', '30', '0', 'COMMON', '99999', '0', '0', '0', '1', '0', '0', '0', '1500000000', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('12', 'db_construction_hydroponicfarm', 'db_construction_hydroponicfarm_desc', 'EJL', '3190', 'ZIVIL', '40', '0', 'AGRAR', '3000', '0', '5', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('13', 'db_construction_smallagriculturefarm', 'db_construction_smallagriculturefarm_desc', 'CGM', '870', 'ZIVIL', '10', '0', 'AGRAR', '1000', '0', '1', '1', '0', '0', '0', '0', '0', 'smallagriculture.jpg', '1', '1', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('14', 'db_construction_bigagriculturefarm', 'db_construction_bigagriculturefarm_desc', 'CGM', '1450', 'ZIVIL', '20', '0', 'AGRAR', '3000', '0', '3', '3', '0', '0', '0', '0', '0', '', '1', '1', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('15', 'db_construction_agriculturestorage', 'db_construction_agriculturestorage_desc', 'CEGJLM', '2610', 'ZIVIL', '65', '0', 'COMMON', '8000', '0', '100', '10', '0', '0', '0', '0', '0', '', '1', '0', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('16', 'db_construction_agrocomplex', 'db_construction_agrocomplex_desc', 'CGM', '7250', 'ZIVIL', '30', '0', 'AGRAR', '8000', '0', '100', '30', '0', '0', '0', '0', '0', 'agrarcomplex.jpg', '1', '1', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('17', 'db_construction_smallironmine', 'db_construction_smallironmine_desc', 'CEGJLM', '890', 'INDUSTRY', '10', '0', 'INDUSTRY', '1500', '0', '20', '1', '0', '0', '1', '0', '0', 'smallironmine.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('18', 'db_construction_bigironmine', 'db_construction_bigironmine_desc', 'CEGJLM', '1740', 'INDUSTRY', '20', '0', 'INDUSTRY', '4000', '0', '300', '3', '0', '0', '1', '0', '0', 'bigironmine.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('19', 'db_construction_complexironmine', 'db_construction_complexironmine_desc', 'CEGJLM', '3260', 'INDUSTRY', '30', '0', 'INDUSTRY', '9000', '0', '1000', '9', '0', '0', '1', '0', '0', 'ironcomplex.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('20', 'db_construction_steelfactory', 'db_construction_steelfactory_desc', 'CEGJLM', '6230', 'INDUSTRY', '40', '0', 'INDUSTRY', '9000', '0', '500', '5', '0', '0', '0', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('21', 'db_construction_steelcomplex', 'db_construction_steelcomplex_desc', 'CEGJLM', '10230', 'INDUSTRY', '50', '0', 'INDUSTRY', '13000', '0', '1500', '10', '0', '0', '0', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('22', 'db_construction_smallressourcestorage', 'db_construction_smallressourcestorage_desc', 'CEGJLM', '1740', 'INDUSTRY', '60', '0', 'COMMON', '1500', '0', '25', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('23', 'db_construction_bigressourcestorage', 'db_construction_bigressourcestorage_desc', 'CEGJLM', '3190', 'INDUSTRY', '70', '0', 'COMMON', '3000', '0', '125', '18', '0', '0', '0', '0', '0', '', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('24', 'db_construction_nuclearpowerplant', 'db_construction_nuclearpowerplant_desc', 'CEGJLM', '3480', 'ENERGY', '10', '0', 'ENERGY', '3000', '0', '1', '2', '0', '0', '0', '0', '0', 'nuclearplant.jpg', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('25', 'db_construction_fusionpowerplant', 'db_construction_fusionpowerplant_desc', 'CEGJLM', '10150', 'ENERGY', '20', '0', 'ENERGY', '3000', '0', '2', '4', '0', '0', '0', '0', '0', 'fusionplant.jpg', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('26', 'db_construction_steelcompacter', 'db_construction_steelcompacter_desc', 'CEGJLM', '19320', 'INDUSTRY', '52', '0', 'INDUSTRY', '11000', '0', '2000', '10', '0', '0', '0', '0', '0', 'steelcompressor.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('27', 'db_construction_researchlaboratory', 'db_construction_researchlaboratory_desc', 'CEGJLM', '6300', 'ZIVIL', '0', '0', 'RESEARCH', '2500', '0', '4000', '3', '0', '0', '0', '0', '0', 'researchcenter.jpg', '1', '1', '0', 'RESEARCH', '1');
INSERT INTO `construction` VALUES ('28', 'db_construction_planetarylasercannon', 'db_construction_planetarylasercannon_desc', 'CEGJLM', '2150', 'PLANETARY_DEFENSE', '42', '0', 'COMMON', '25000', '9000', '1', '2', '0', '0', '0', '0', '0', 'planetarylaser.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('29', 'db_construction_planetarymisslebase', 'db_construction_planetarymisslebase_desc', 'CEGJLM', '3500', 'PLANETARY_DEFENSE', '44', '0', 'COMMON', '19000', '8999', '1', '2', '0', '0', '0', '0', '0', 'planetaryrocket.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('30', 'db_construction_planetaryshipyard', 'db_construction_planetaryshipyard_desc', 'CEGJLM', '8350', 'MILITARY', '30', '0', 'COMMON', '14000', '0', '5000', '20', '0', '0', '0', '0', '0', 'planetaryshipyard.jpg', '1', '1', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('31', 'db_construction_modulefactory', 'db_construction_modulefactory_desc', 'CEGJLM', '4350', 'MILITARY', '20', '0', 'COMMON', '8000', '0', '3000', '10', '0', '0', '0', '0', '0', 'modulefactory.jpg', '1', '1', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('32', 'db_construction_barracks', 'db_construction_barracks_desc', 'CEGJLM', '1392', 'MILITARY', '10', '0', 'COMMON', '6000', '0', '20', '5', '0', '0', '0', '0', '0', 'barracks.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('33', 'db_construction_orbitalynkeloniummine', 'db_construction_orbitalynkeloniummine_desc', 'AB', '8700', 'INDUSTRY', '58', '0', 'INDUSTRY', '15000', '0', '10', '0', '0', '0', '1', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('34', 'db_construction_orbitalcolony', 'db_construction_orbitalcolony_desc', 'AB', '7830', 'ADMINISTRATION', '40', '30000', 'COMMON', '20000', '0', '0', '0', '1', '0', '0', '0', '0', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('35', 'db_construction_orbitalshipyard', 'db_construction_orbitalshipyard_desc', 'CEGJLM', '18250', 'MILITARY', '40', '0', 'COMMON', '15000', '0', '2000', '0', '0', '0', '0', '0', '0', 'orbitalshipyard.jpg', '1', '1', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('36', 'db_construction_orbitalaccomodationunit', 'db_construction_orbitalaccomodationunit_desc', 'ABCEGJLM', '5220', 'ZIVIL', '110', '50000', 'COMMON', '15000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('37', 'db_construction_orbitalressourcestorage', 'db_construction_orbitalressourcestorage_desc', 'ABCEGJLM', '3335', 'INDUSTRY', '75', '0', 'COMMON', '10000', '0', '1', '0', '0', '0', '0', '0', '0', 'orbitalressourcestorage.jpg', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('38', 'db_construction_orbitalhydroponicfarm', 'db_construction_orbitalhydroponicfarm_desc', 'ABCEGJLM', '6090', 'ZIVIL', '50', '0', 'AGRAR', '10000', '0', '5', '0', '0', '0', '0', '0', '0', '', '1', '0', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('39', 'db_construction_howalgoniummine', 'db_construction_howalgoniummine_desc', 'CEGJLM', '11020', 'INDUSTRY', '53', '0', 'INDUSTRY', '3000', '0', '5', '5', '0', '0', '1', '0', '0', 'howalgoniummine.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('40', 'db_construction_accomodationunit', 'db_construction_accomodationunit_desc', 'EJL', '2030', 'ZIVIL', '60', '100000', 'COMMON', '3500', '0', '0', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('41', 'db_construction_scannerphalanx', 'db_construction_scannerphalanx_desc', 'CEGJLM', '5800', 'MILITARY', '70', '0', 'COMMON', '2000', '0', '5', '2', '1', '0', '0', '0', '0', 'scannerphalanx.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('42', 'db_construction_hyperspacescanner', 'db_construction_hyperspacescanner_desc', 'CEGJLM', '7250', 'MILITARY', '80', '0', 'COMMON', '2500', '0', '1', '5', '1', '0', '0', '0', '0', 'hyperscanner.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('43', 'db_construction_hyperressourcestorage', 'db_construction_hyperressourcestorage_desc', 'CEGJLM', '4060', 'INDUSTRY', '80', '0', 'COMMON', '1500', '0', '50', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('44', 'db_construction_orbitalhyperressourcestorage', 'db_construction_orbitalhyperressourcestorage_desc', 'ABCEGJLM', '3480', 'INDUSTRY', '90', '0', 'COMMON', '12000', '0', '1', '0', '0', '0', '0', '0', '0', 'orbitalhyperressourcestorage.jpg', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('45', 'db_construction_orbitalhowalgoniummine', 'db_construction_orbitalhowalgoniummine_desc', 'AB', '8555', 'INDUSTRY', '57', '0', 'INDUSTRY', '15000', '0', '10', '0', '0', '0', '1', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('46', 'db_construction_planetarypositroncomputer', 'db_construction_planetarypositroncomputer_desc', 'CEGJLM', '67300', 'ZIVIL', '1', '0', 'RESEARCH', '20000', '0', '500', '20', '1', '1', '0', '0', '1500000000', '', '1', '1', '0', 'RESEARCH', '1');
INSERT INTO `construction` VALUES ('47', 'db_construction_planetaryintervalcannon', 'db_construction_planetaryintervalcannon_desc', 'CEGJLM', '8555', 'PLANETARY_DEFENSE', '46', '0', 'COMMON', '35000', '9001', '1', '2', '0', '0', '0', '0', '0', 'plasmacannon.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('48', 'db_construction_orbitalfoodstorage', 'db_construction_orbitalfoodstorage_desc', 'ABCEGJLM', '3335', 'ZIVIL', '70', '0', 'COMMON', '10000', '0', '1', '0', '0', '0', '0', '0', '0', '', '1', '0', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('49', 'db_construction_CVEmbinium_facility', 'db_construction_CVEmbinium_facility_desc', 'CEGJLM', '198200', 'INDUSTRY', '59', '0', 'INDUSTRY', '26000', '0', '1500', '30', '0', '0', '0', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('50', 'db_construction_militaryoutpost', 'db_construction_militaryoutpost_desc', 'CEGJLM', '98700', 'MILITARY', '105', '0', 'COMMON', '300000', '0', '10000', '250', '1', '1', '0', '0', '0', '', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('61', 'db_construction_observatory', 'db_construction_observatory_desc', 'CEGJLM', '4640', 'ADMINISTRATION', '60', '0', 'COMMON', '5000', '0', '1', '2', '1', '0', '0', '3', '0', '', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('80', 'db_construction_megacity', 'db_construction_megacity_desc', 'CGM', '65000', 'ZIVIL', '5', '200000000', 'COMMON', '50000', '0', '0', '100', '0', '0', '0', '0', '0', 'megacity.png', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('81', 'db_construction_consumables_industry', 'db_construction_consumables_industry_desc', 'CEGJLM', '11000', 'INDUSTRY', '100', '0', 'INDUSTRY', '13000', '0', '2000', '10', '0', '0', '0', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('82', 'db_construction_scientific_academy', 'db_construction_scientific_academy_desc', 'CGM', '32000', 'ZIVIL', '32', '0', 'RESEARCH', '7000', '0', '15000', '20', '1', '1', '0', '0', '0', '', '1', '0', '0', 'RESEARCH', '1');
INSERT INTO `construction` VALUES ('83', 'db_construction_planetary_fortress', 'db_contruction_planetary_fortress_desc', 'CEGJLM', '55000', 'PLANETARY_DEFENSE', '106', '0', 'COMMON', '150000', '0', '100', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('84', 'db_construction_agri_distributioncenter', 'db_construction_agri_distributioncenter_desc', 'M', '45000', 'ZIVIL', '35', '0', 'AGRAR', '10000', '0', '3000', '30', '1', '0', '0', '0', '0', '', '1', '0', '1', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('90', 'db_construction_leisurefacilities', 'db_construction_leisurefacilities_desc', 'M', '1170', 'ZIVIL', '120', '0', 'COMMON', '1000', '0', '50', '50', '0', '0', '0', '0', '0', 'recreational.jpg', '1', '0', '0', 'RECREATIONAL', '1');
INSERT INTO `construction` VALUES ('100', 'db_construction_civilspaceport', 'db_construction_civilspaceport_desc', 'CEGJLM', '4350', 'ZIVIL', '130', '0', 'COMMON', '8000', '0', '1000', '10', '0', '0', '0', '0', '0', '', '1', '1', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('101', 'db_construction_interstellartradingpost', 'db_construction_interstellartradingpost_desc', 'CEGJLM', '5510', 'ZIVIL', '80', '0', 'COMMON', '6000', '0', '5000', '10', '1', '0', '0', '1', '0', '', '1', '0', '1', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('103', 'db_construction_transmitter', 'db_construction_transmitter_desc', 'CEGJLM', '32150', 'INDUSTRY', '110', '0', 'COMMON', '9000', '0', '750', '15', '0', '0', '0', '0', '0', '', '1', '1', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('200', 'db_construction_planetaryhueshield', 'db_construction_planetaryhueshield_desc', 'CEGJLM', '68950', 'MILITARY', '90', '0', 'COMMON', '120000', '0', '250', '120', '1', '0', '0', '0', '0', 'schirmhue.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('201', 'db_construction_planetaryparatronshield', 'db_construction_planetaryparatronshield_desc', 'CEGJLM', '95300', 'MILITARY', '100', '0', 'COMMON', '150000', '0', '250', '120', '1', '0', '0', '0', '0', 'schirmparatron.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('202', 'db_construction_hypertrop', 'db_construction_hypertrop_desc', 'CEGJLM', '123400', 'ENERGY', '30', '0', 'ENERGY', '115000', '0', '1500', '150', '1', '0', '0', '0', '0', 'sonnenzapfer.jpg', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('300', 'db_construction_solarplanet', 'db_construction_solarplanet_desc', 'JCEGLM', '1950', 'ENERGY', '15', '0', 'ENERGY', '1000', '0', '1', '40', '0', '0', '0', '0', '0', 'solarplant.jpg', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('1000', 'db_construction_transmittercontroller', 'db_construction_transmittercontroller_desc', 'CEGJLM', '0', 'ADMINISTRATION', '0', '0', 'COMMON', '0', '0', '0', '20', '1', '1', '0', '0', '0', '', '0', '0', '0', 'NOT_SPECIFIED', '0');
INSERT INTO `construction` VALUES ('1001', 'db_construction_transmitterfortress', 'db_construction_transmitterfortress_desc', 'CEGJLM', '0', 'PLANETARY_DEFENSE', '0', '0', 'COMMON', '200000', '0', '0', '10', '0', '0', '0', '0', '0', '', '1', '0', '0', 'MILITARY', '0');

DROP TABLE IF EXISTS `production`;
CREATE TABLE `production` (
  `constructionId` int(11) NOT NULL DEFAULT '0',
  `ressourceId` int(11) NOT NULL DEFAULT '0',
  `quantity` float DEFAULT NULL,
  `incdec` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`constructionId`,`ressourceId`,`incdec`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `production` VALUES ('2', '1', '50', '0');
INSERT INTO `production` VALUES ('2', '1', '50000', '2');
INSERT INTO `production` VALUES ('2', '2', '50000', '2');
INSERT INTO `production` VALUES ('2', '8', '1000', '0');
INSERT INTO `production` VALUES ('2', '9', '20', '0');
INSERT INTO `production` VALUES ('2', '9', '2000', '2');
INSERT INTO `production` VALUES ('12', '8', '500', '1');
INSERT INTO `production` VALUES ('12', '9', '100', '0');
INSERT INTO `production` VALUES ('13', '8', '25', '1');
INSERT INTO `production` VALUES ('13', '9', '50', '0');
INSERT INTO `production` VALUES ('13', '9', '50', '2');
INSERT INTO `production` VALUES ('14', '8', '250', '1');
INSERT INTO `production` VALUES ('14', '9', '1000', '0');
INSERT INTO `production` VALUES ('14', '9', '1000', '2');
INSERT INTO `production` VALUES ('15', '9', '5e+006', '2');
INSERT INTO `production` VALUES ('16', '8', '3000', '1');
INSERT INTO `production` VALUES ('16', '9', '100000', '0');
INSERT INTO `production` VALUES ('16', '9', '100000', '2');
INSERT INTO `production` VALUES ('17', '1', '70', '0');
INSERT INTO `production` VALUES ('17', '1', '3500', '2');
INSERT INTO `production` VALUES ('17', '8', '200', '1');
INSERT INTO `production` VALUES ('17', '11', '210', '1');
INSERT INTO `production` VALUES ('18', '1', '200', '0');
INSERT INTO `production` VALUES ('18', '1', '10000', '2');
INSERT INTO `production` VALUES ('18', '8', '500', '1');
INSERT INTO `production` VALUES ('18', '11', '600', '1');
INSERT INTO `production` VALUES ('19', '1', '600', '0');
INSERT INTO `production` VALUES ('19', '1', '30000', '2');
INSERT INTO `production` VALUES ('19', '8', '850', '1');
INSERT INTO `production` VALUES ('19', '11', '1800', '1');
INSERT INTO `production` VALUES ('20', '1', '225', '1');
INSERT INTO `production` VALUES ('20', '1', '2250', '2');
INSERT INTO `production` VALUES ('20', '2', '200', '0');
INSERT INTO `production` VALUES ('20', '2', '10000', '2');
INSERT INTO `production` VALUES ('20', '8', '1400', '1');
INSERT INTO `production` VALUES ('21', '1', '550', '1');
INSERT INTO `production` VALUES ('21', '1', '5500', '2');
INSERT INTO `production` VALUES ('21', '2', '500', '0');
INSERT INTO `production` VALUES ('21', '2', '25000', '2');
INSERT INTO `production` VALUES ('21', '8', '2400', '1');
INSERT INTO `production` VALUES ('22', '1', '150000', '2');
INSERT INTO `production` VALUES ('22', '2', '150000', '2');
INSERT INTO `production` VALUES ('22', '3', '100000', '2');
INSERT INTO `production` VALUES ('22', '4', '50000', '2');
INSERT INTO `production` VALUES ('22', '7', '30000', '2');
INSERT INTO `production` VALUES ('22', '8', '50', '1');
INSERT INTO `production` VALUES ('23', '1', '750000', '2');
INSERT INTO `production` VALUES ('23', '2', '750000', '2');
INSERT INTO `production` VALUES ('23', '3', '500000', '2');
INSERT INTO `production` VALUES ('23', '4', '250000', '2');
INSERT INTO `production` VALUES ('23', '7', '90000', '2');
INSERT INTO `production` VALUES ('23', '8', '175', '1');
INSERT INTO `production` VALUES ('24', '8', '2000', '0');
INSERT INTO `production` VALUES ('25', '8', '10000', '0');
INSERT INTO `production` VALUES ('26', '2', '900', '1');
INSERT INTO `production` VALUES ('26', '2', '9000', '2');
INSERT INTO `production` VALUES ('26', '3', '500', '0');
INSERT INTO `production` VALUES ('26', '3', '25000', '2');
INSERT INTO `production` VALUES ('26', '8', '3500', '1');
INSERT INTO `production` VALUES ('27', '8', '2000', '1');
INSERT INTO `production` VALUES ('27', '10', '3', '0');
INSERT INTO `production` VALUES ('27', '14', '1', '0');
INSERT INTO `production` VALUES ('30', '8', '1300', '1');
INSERT INTO `production` VALUES ('30', '31', '250', '0');
INSERT INTO `production` VALUES ('31', '8', '800', '1');
INSERT INTO `production` VALUES ('31', '30', '500', '0');
INSERT INTO `production` VALUES ('32', '8', '300', '1');
INSERT INTO `production` VALUES ('32', '33', '300', '0');
INSERT INTO `production` VALUES ('33', '4', '50', '0');
INSERT INTO `production` VALUES ('33', '12', '80', '1');
INSERT INTO `production` VALUES ('34', '1', '50000', '2');
INSERT INTO `production` VALUES ('34', '2', '50000', '2');
INSERT INTO `production` VALUES ('34', '3', '50000', '2');
INSERT INTO `production` VALUES ('34', '4', '50000', '2');
INSERT INTO `production` VALUES ('34', '8', '1000', '0');
INSERT INTO `production` VALUES ('34', '8', '1000', '1');
INSERT INTO `production` VALUES ('34', '9', '30', '0');
INSERT INTO `production` VALUES ('34', '9', '2000', '2');
INSERT INTO `production` VALUES ('35', '32', '250', '0');
INSERT INTO `production` VALUES ('37', '1', '50000', '2');
INSERT INTO `production` VALUES ('37', '2', '50000', '2');
INSERT INTO `production` VALUES ('37', '3', '50000', '2');
INSERT INTO `production` VALUES ('37', '4', '50000', '2');
INSERT INTO `production` VALUES ('38', '9', '50', '0');
INSERT INTO `production` VALUES ('39', '5', '5', '0');
INSERT INTO `production` VALUES ('39', '5', '200', '2');
INSERT INTO `production` VALUES ('39', '8', '2000', '1');
INSERT INTO `production` VALUES ('39', '13', '5', '1');
INSERT INTO `production` VALUES ('41', '8', '400', '1');
INSERT INTO `production` VALUES ('43', '5', '10000', '2');
INSERT INTO `production` VALUES ('43', '6', '10000', '2');
INSERT INTO `production` VALUES ('43', '8', '300', '1');
INSERT INTO `production` VALUES ('44', '5', '8000', '2');
INSERT INTO `production` VALUES ('44', '6', '8000', '2');
INSERT INTO `production` VALUES ('45', '5', '3', '0');
INSERT INTO `production` VALUES ('45', '5', '200', '2');
INSERT INTO `production` VALUES ('45', '13', '3', '1');
INSERT INTO `production` VALUES ('46', '8', '27500', '1');
INSERT INTO `production` VALUES ('46', '14', '100', '0');
INSERT INTO `production` VALUES ('48', '9', '120000', '2');
INSERT INTO `production` VALUES ('49', '5', '9', '1');
INSERT INTO `production` VALUES ('49', '5', '300', '2');
INSERT INTO `production` VALUES ('49', '6', '3', '0');
INSERT INTO `production` VALUES ('49', '6', '100', '2');
INSERT INTO `production` VALUES ('49', '8', '19500', '1');
INSERT INTO `production` VALUES ('80', '7', '250', '1');
INSERT INTO `production` VALUES ('80', '99', '1', '0');
INSERT INTO `production` VALUES ('81', '1', '500', '1');
INSERT INTO `production` VALUES ('81', '2', '200', '1');
INSERT INTO `production` VALUES ('81', '7', '100', '0');
INSERT INTO `production` VALUES ('81', '7', '10000', '2');
INSERT INTO `production` VALUES ('81', '8', '1250', '1');
INSERT INTO `production` VALUES ('82', '8', '7500', '1');
INSERT INTO `production` VALUES ('82', '10', '20', '0');
INSERT INTO `production` VALUES ('82', '14', '20', '0');
INSERT INTO `production` VALUES ('102', '8', '3500', '1');
INSERT INTO `production` VALUES ('202', '8', '1e+006', '0');
INSERT INTO `production` VALUES ('300', '8', '1000', '0');

INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '81', '1', '120000');
INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '81', '2', '40000');
INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '81', '20', '3250000');