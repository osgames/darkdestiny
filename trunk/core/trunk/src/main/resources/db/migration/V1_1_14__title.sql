DROP TABLE IF EXISTS `title`;
CREATE TABLE `title` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `campaignId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `type` enum('IMPERIAL','RESEARCH','MILITARY','VARIOUS') DEFAULT NULL,
  `difficulty` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `clazz` varchar(50) DEFAULT NULL,
  `visible` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of title
-- ----------------------------
INSERT INTO `title` VALUES ('7', '1', 'db_title_trialpresident', 'db_title_trialpresident_desc', 'IMPERIAL', '0', '0', 'at.darkdestiny.core.title.TrialPresident', '0');
INSERT INTO `title` VALUES ('8', '1', 'db_title_president', 'db_title_president_desc', 'IMPERIAL', '1', '1', 'at.darkdestiny.core.title.President', '1');
INSERT INTO `title` VALUES ('9', '1', 'db_title_thecurious', 'db_title_thecurious_desc', 'RESEARCH', '1', '1', 'at.darkdestiny.core.title.TheCurious', '1');
INSERT INTO `title` VALUES ('10', '1', 'db_title_thefolkhero', 'db_title_thefolkhero_desc', 'IMPERIAL', '5', '5', 'at.darkdestiny.core.title.TheFolkHero', '1');
INSERT INTO `title` VALUES ('11', '2', 'db_title_systemlord', 'db_title_systemlord_desc', 'IMPERIAL', '7', '7', 'at.darkdestiny.core.title.SystemLord', '0');
INSERT INTO `title` VALUES ('12', '2', 'db_title_emperor', 'db_title_emperor_desc', 'IMPERIAL', '9', '9', 'at.darkdestiny.core.title.Emperor', '1');
INSERT INTO `title` VALUES ('13', '1', 'db_title_thegeneral', 'db_title_thegeneral_desc', 'MILITARY', '1', '1', 'at.darkdestiny.core.title.TheGeneral', '1');
INSERT INTO `title` VALUES ('14', '1', 'db_title_thedetermined', 'db_title_thedetermined_desc', 'RESEARCH', '3', '3', 'at.darkdestiny.core.title.TheDetermined', '1');
INSERT INTO `title` VALUES ('15', '100', 'db_title_thecommunicative', 'db_title_thecommunicative_desc', 'VARIOUS', '0', '0', 'at.darkdestiny.core.title.TheCommunicative', '0');
INSERT INTO `title` VALUES ('16', '100', 'db_title_thechatterbox', 'db_title_thechatterbox_desc', 'VARIOUS', '0', '0', 'at.darkdestiny.core.title.ChatterBox', '0');
INSERT INTO `title` VALUES ('17', '1', 'db_title_thegenerous', 'db_title_thegenerous_desc', 'IMPERIAL', '3', '3', 'at.darkdestiny.core.title.TheGenerous', '1');
INSERT INTO `title` VALUES ('18', '100', 'db_title_theradiated', 'db_title_theradiated_desc', 'VARIOUS', '0', '0', 'at.darkdestiny.core.title.TheRadiated', '0');
INSERT INTO `title` VALUES ('19', '1', 'db_title_thetinkerer', 'db_title_thetinkerer_desc', 'MILITARY', '2', '2', 'at.darkdestiny.core.title.TheTinkerer', '1');
INSERT INTO `title` VALUES ('20', '1', 'db_title_theexplorer', 'db_title_theexplorer_desc', 'MILITARY', '4', '7', 'at.darkdestiny.core.title.TheExplorer', '1');
INSERT INTO `title` VALUES ('21', '1', 'db_title_theresearcher', 'db_title_theresearcher_desc', 'RESEARCH', '2', '2', 'at.darkdestiny.core.title.TheResearcher', '1');
INSERT INTO `title` VALUES ('22', '100', 'db_title_thetyrant', 'db_title_thetyrant_desc', 'VARIOUS', '0', '0', 'at.darkdestiny.core.title.TheTyrant', '0');
INSERT INTO `title` VALUES ('23', '100', 'db_title_thestingy', 'db_title_thestingy_desc', 'VARIOUS', '0', '0', 'at.darkdestiny.core.title.TheStingy', '0');
INSERT INTO `title` VALUES ('24', '100', 'db_title_theefficient', 'db_title_theefficient_desc', 'IMPERIAL', '3', '4', 'at.darkdestiny.core.title.TheEfficient', '0');
INSERT INTO `title` VALUES ('25', '1', 'db_title_theminer', 'db_title_theminer_desc', 'IMPERIAL', '2', '2', 'at.darkdestiny.core.title.TheMiner', '1');
INSERT INTO `title` VALUES ('26', '1', 'db_title_thevisionary', 'db_title_thevisionary_desc', 'IMPERIAL', '4', '4', 'at.darkdestiny.core.title.TheVisionary', '1');
INSERT INTO `title` VALUES ('27', '1', 'db_title_thesmart', 'db_title_thesmart_desc', 'RESEARCH', '4', '4', 'at.darkdestiny.core.title.TheSmart', '1');
INSERT INTO `title` VALUES ('28', '1', 'db_title_theengineer', 'db_title_theengineer_desc', 'MILITARY', '3', '3', 'at.darkdestiny.core.title.TheEngineer', '1');
INSERT INTO `title` VALUES ('30', '1', 'db_title_thecolonist', 'db_title_thecolonist_desc', 'MILITARY', '5', '5', 'at.darkdestiny.core.title.Colonist', '1');
INSERT INTO `title` VALUES ('31', '1', 'db_title_theambitious', 'db_title_theambitious_desc', 'RESEARCH', '5', '5', 'at.darkdestiny.core.title.TheAmbitious', '1');
INSERT INTO `title` VALUES ('32', '3', 'db_title_theomnicient', 'db_title_theomnicient_desc', 'RESEARCH', '15', '15', 'at.darkdestiny.core.title.TheOmniscient', '1');
INSERT INTO `title` VALUES ('33', '2', 'db_title_themerchant', 'db_title_themerchant_desc', 'IMPERIAL', '6', '6', 'at.darkdestiny.core.title.TheMerchant', '1');
INSERT INTO `title` VALUES ('34', '2', 'db_title_theelementalist', 'db_title_theelementalist_desc', 'RESEARCH', '6', '6', 'at.darkdestiny.core.title.TheElementalist', '1');
INSERT INTO `title` VALUES ('35', '2', 'db_title_thedimensionalengrossed', 'db_title_thedimensionalengrossed_desc', 'RESEARCH', '8', '8', 'at.darkdestiny.core.title.TheEngrossed', '1');
INSERT INTO `title` VALUES ('36', '2', 'db_title_thefortuneteller', 'db_title_thefortuneteller_desc', 'MILITARY', '8', '8', 'at.darkdestiny.core.title.TheFortuneteller', '1');
INSERT INTO `title` VALUES ('37', '1', 'db_title_theflexible', 'db_title_theflexible_desc', 'VARIOUS', '1', '1', 'at.darkdestiny.core.title.TheFlexible', '0');
INSERT INTO `title` VALUES ('38', '3', 'db_title_maniac', 'db_title_maniac_desc', 'RESEARCH', '12', '12', 'at.darkdestiny.core.title.TheManiac', '1');
INSERT INTO `title` VALUES ('39', '2', 'db_title_thedwarf', 'db_title_thedwarf_desc', 'MILITARY', '9', '9', 'at.darkdestiny.core.title.TheDwarf', '1');
INSERT INTO `title` VALUES ('42', '3', 'db_title_thecolossus', 'db_title_thecolossus_desc', 'MILITARY', '13', '13', 'at.darkdestiny.core.title.TheColossus', '1');
INSERT INTO `title` VALUES ('43', '1', 'db_title_thestinker', 'db_title_thestinker_desc', 'VARIOUS', '1', '1', 'at.darkdestiny.core.title.TheStinker', '0');
INSERT INTO `title` VALUES ('44', '1', 'db_title_thelucky', 'db_title_thelucky_desc', 'IMPERIAL', '1', '1', 'at.darkdestiny.core.title.TheLucky', '0');
INSERT INTO `title` VALUES ('45', '3', 'db_title_thefanatic', 'db_title_thefanatic_desc', 'IMPERIAL', '14', '14', 'at.darkdestiny.core.title.TheFanatic', '1');
INSERT INTO `title` VALUES ('46', '2', 'db_title_thebeneficiary', 'db_title_thebeneficiary_desc', 'VARIOUS', '1', '1', 'at.darkdestiny.core.title.TheBenificiary', '0');
INSERT INTO `title` VALUES ('47', '3', 'db_title_thebillionaire', 'db_title_thebillionaire_desc', 'IMPERIAL', '12', '12', 'at.darkdestiny.core.title.TheBillionaire', '1');
INSERT INTO `title` VALUES ('48', '2', 'db_title_thecautious', 'db_title_thecautious_desc', 'MILITARY', '10', '10', 'at.darkdestiny.core.title.TheCautious', '1');
INSERT INTO `title` VALUES ('49', '3', 'db_title_theslaughterer', 'db_title_theslaughterer_desc', 'MILITARY', '12', '12', 'at.darkdestiny.core.title.TheSlaughterer', '1');
INSERT INTO `title` VALUES ('50', '3', 'db_title_theexecutor', 'db_title_theexecutor_desc', 'MILITARY', '15', '15', 'at.darkdestiny.core.title.TheExecutor', '1');
INSERT INTO `title` VALUES ('51', '2', 'db_title_thepolitician', 'db_title_thepolitician_desc', 'IMPERIAL', '9', '9', 'at.darkdestiny.core.title.ThePolitician', '0');
INSERT INTO `title` VALUES ('52', '3', 'db_title_theconquerer', 'db_title_theconquerer_desc', 'MILITARY', '14', '14', 'at.darkdestiny.core.title.TheConquerer', '1');
INSERT INTO `title` VALUES ('53', '3', 'db_title_theinvincible', 'db_title_theinvincible_desc', 'MILITARY', '11', '11', 'at.darkdestiny.core.title.TheInvincible', '1');