

DROP TABLE IF EXISTS `viewsystem`;
CREATE TABLE `viewsystem` (
  `locationId` int(20) NOT NULL,
  `locationType` enum('SYSTEM') NOT NULL,
  `userId` int(11) NOT NULL,
  `date` bigint(20) NOT NULL,
  PRIMARY KEY (`locationId`,`locationType`,`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
