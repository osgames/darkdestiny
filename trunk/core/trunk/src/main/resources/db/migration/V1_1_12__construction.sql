ALTER TABLE construction ADD COLUMN isVisible tinyint(3) DEFAULT 1;

UPDATE construction SET isVisible=0 WHERE id=81;
UPDATE construction SET isVisible=0 WHERE id=1000;
UPDATE construction SET isVisible=0 WHERE id=1001;