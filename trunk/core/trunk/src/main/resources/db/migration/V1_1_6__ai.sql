
DROP TABLE IF EXISTS `ai`;
CREATE TABLE `ai` (
  `userId` int(11) NOT NULL,
  `aiType` enum('FREE_WORLDS_AI','NORMAL') NOT NULL DEFAULT 'NORMAL',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `aigoal`
-- ----------------------------
DROP TABLE IF EXISTS `aigoal`;
CREATE TABLE `aigoal` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aigoal
-- ----------------------------
INSERT INTO `aigoal` VALUES ('1', 'PlanetDevelopmentGoal');
INSERT INTO `aigoal` VALUES ('2', 'PlanetFoodGoal');
INSERT INTO `aigoal` VALUES ('3', 'PlanetMoraleGoal');
INSERT INTO `aigoal` VALUES ('4', 'PlanetResourceGoal');
INSERT INTO `aigoal` VALUES ('5', 'PlanetConstructionGoal');
-- ----------------------------
-- Table structure for `aigoalparameter`
-- ----------------------------
DROP TABLE IF EXISTS `aigoalparameter`;
CREATE TABLE `aigoalparameter` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `goalId` mediumint(9) NOT NULL,
  `parameterType` enum('RESOURCE','POPGROWTH','MORALE','TAX','FOOD','ENERGY','CONSTRUCT') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aigoalparameter
-- ----------------------------
INSERT INTO `aigoalparameter` VALUES ('2', '4', 'RESOURCE');
INSERT INTO `aigoalparameter` VALUES ('4', '3', 'TAX');
INSERT INTO `aigoalparameter` VALUES ('6', '5', 'CONSTRUCT');

-- ----------------------------
-- Table structure for `aigoaluser`
-- ----------------------------
DROP TABLE IF EXISTS `aigoaluser`;
CREATE TABLE `aigoaluser` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `userId` mediumint(9) NOT NULL,
  `goalId` mediumint(9) NOT NULL,
  `priority` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aigoaluser
-- ----------------------------
INSERT INTO `aigoaluser` VALUES ('1', '8', '1', '0');
INSERT INTO `aigoaluser` VALUES ('2', '8', '2', '0');
INSERT INTO `aigoaluser` VALUES ('3', '8', '3', '0');
INSERT INTO `aigoaluser` VALUES ('4', '8', '4', '0');
INSERT INTO `aigoaluser` VALUES ('5', '9', '1', '0');
INSERT INTO `aigoaluser` VALUES ('6', '9', '2', '0');
INSERT INTO `aigoaluser` VALUES ('7', '9', '3', '0');
INSERT INTO `aigoaluser` VALUES ('8', '9', '4', '0');
INSERT INTO `aigoaluser` VALUES ('9', '8', '5', '0');
INSERT INTO `aigoaluser` VALUES ('10', '9', '5', '0');
