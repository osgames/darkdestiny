DROP TABLE IF EXISTS `specialtrade`;
CREATE TABLE `specialtrade` (
  `planetId` mediumint(8) unsigned NOT NULL,
  `production` int(10) unsigned NOT NULL,
  `consumption` int(10) unsigned NOT NULL,
  `incomingRoutes` int(10) unsigned NOT NULL,
  `outgoingRoutes` int(10) unsigned NOT NULL,
  `routedCapacity` int(10) unsigned NOT NULL,
  `incomingGoods` int(11) NOT NULL,
  `outgoingGoods` int(11) NOT NULL,
  PRIMARY KEY (`planetId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM production WHERE constructionId=80 AND ressourceId=2 AND quantity=500 AND incdec=1;