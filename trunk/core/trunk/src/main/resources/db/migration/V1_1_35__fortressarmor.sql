DROP TABLE IF EXISTS `constructionmodule`;
CREATE TABLE `constructionmodule` (
  `constructionId` int(11) NOT NULL DEFAULT '0',
  `moduleId` int(11) NOT NULL DEFAULT '0',
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`constructionId`,`moduleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `constructionmodule` VALUES ('28', '900', '1');
INSERT INTO `constructionmodule` VALUES ('29', '901', '1');
INSERT INTO `constructionmodule` VALUES ('47', '902', '1');
INSERT INTO `constructionmodule` VALUES ('1001', '22', '1');
INSERT INTO `constructionmodule` VALUES ('1001', '51', '10');
INSERT INTO `constructionmodule` VALUES ('1001', '2000', '20');
