CREATE TABLE `devroute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `srcPlanet` int(11) NOT NULL,
  `targetPlanet` int(11) NOT NULL,
  `ressId` mediumint(9) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE traderoute MODIFY COLUMN type ENUM('TRANSPORT','TRADE','ECONOMY') DEFAULT NULL;

DROP TABLE IF EXISTS `bonus`;
CREATE TABLE `bonus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `value` double DEFAULT NULL,
  `valueType` enum('DEFINE','INCREASE','DECREASE','INCREASE_PERC','DECREASE_PERC') DEFAULT NULL,
  `bonusType` enum('POPULATION_GROWTH','PRODUCTION','CONSTRUCTION','RESEARCH','TRADE','CREDIT') DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `bonusRange` enum('PLANET','EMPIRE') DEFAULT NULL,
  `cost` bigint(20) DEFAULT NULL,
  `reqResearch` int(11) DEFAULT NULL,
  `bonusClass` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bonus
-- ----------------------------
INSERT INTO `bonus` VALUES ('2', 'db_bonus_production_perc_1', 'db_bonus_production_perc_1_desc', '0.2', 'INCREASE_PERC', 'PRODUCTION', '1008', 'PLANET', '10000', '0', null);
INSERT INTO `bonus` VALUES ('3', 'db_bonus_construction_perc_1', 'db_bonus_construction_perc_1_desc', '150', 'INCREASE', 'CONSTRUCTION', '1008', 'PLANET', '10000', '0', null);
INSERT INTO `bonus` VALUES ('4', 'db_bonus_research_perc_1', 'db_bonus_research_perc_1_desc', '0.1', 'INCREASE_PERC', 'RESEARCH', '1008', 'PLANET', '15000', '0', null);
INSERT INTO `bonus` VALUES ('5', 'db_development_industrial', 'db_development_industrial_desc', null, null, null, '0', 'PLANET', '50000', '0', 'IndustrialColony');
INSERT INTO `bonus` VALUES ('6', 'db_development_mining', 'db_development_mining_desc', null, null, null, '0', 'PLANET', '5000', '0', 'MiningColony');
INSERT INTO `bonus` VALUES ('7', 'db_development_agricultural', 'db_development_agricultural_desc', null, null, null, '0', 'PLANET', '35000', '0', 'AgriculturalColony');
INSERT INTO `bonus` VALUES ('8', 'db_development_population', 'db_development_population_desc', null, null, null, '0', 'PLANET', '50000', '0', 'PopulationColony');
INSERT INTO `bonus` VALUES ('9', 'db_development_research', 'db_development_research_desc', null, null, null, '0', 'PLANET', '50000', '0', 'ResearchColony');
INSERT INTO `bonus` VALUES ('10', 'db_development_trade', 'db_development_trade_desc', null, null, null, '0', 'PLANET', '35000', '0', 'TradeColony');
INSERT INTO `bonus` VALUES ('11', 'db_development_military', 'db_development_military_desc', null, null, null, '0', 'PLANET', '50000', '0', 'MilitaryColony');
INSERT INTO `bonus` VALUES ('12', 'db_development_administrative', 'db_development_administrative_desc', null, null, null, '0', 'PLANET', '30000', '0', 'AdministrativeColony');