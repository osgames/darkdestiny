-- ----------------------------
-- Table structure for `ressource`
-- ----------------------------
DROP TABLE IF EXISTS `ressource`;
CREATE TABLE `ressource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `transportable` tinyint(3) DEFAULT NULL,
  `mineable` tinyint(3) DEFAULT NULL,
  `storeable` tinyint(3) DEFAULT NULL,
  `ressourcecost` tinyint(3) DEFAULT NULL,
  `baseRess` int(11) DEFAULT NULL,
  `researchRequired` int(11) DEFAULT NULL,
  `displayLocation` int(11) DEFAULT NULL,
  `imageLocation` varchar(50) DEFAULT NULL,
  `styleClazz` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ressource
-- ----------------------------
INSERT INTO `ressource` VALUES ('1', 'db_ressource_iron', '1', '0', '1', '1', '1', '0', '1', 'pic/menu/icons/iron.png', 'resourcebar_iron_image');
INSERT INTO `ressource` VALUES ('2', 'db_ressource_steel', '1', '0', '1', '1', '1', '0', '2', 'pic/menu/icons/steel.png', 'resourcebar_steel_image');
INSERT INTO `ressource` VALUES ('3', 'db_ressource_terkonit', '1', '0', '1', '1', '1', '6', '3', 'pic/menu/icons/terkonit.png', 'resourcebar_terkonit_image');
INSERT INTO `ressource` VALUES ('4', 'db_ressource_ynkelonium', '1', '0', '1', '1', '1', '51', '4', 'pic/menu/icons/ynkelonium.png', 'resourcebar_ynkelonium_image');
INSERT INTO `ressource` VALUES ('5', 'db_ressource_howalgonium', '1', '0', '1', '1', '1', '73', '5', 'pic/menu/icons/howalgonium.png', 'resourcebar_howalgonium_image');
INSERT INTO `ressource` VALUES ('6', 'db_ressource_cvembinium', '1', '0', '1', '1', '1', '150', '6', 'pic/menu/icons/embinium.png', 'resourcebar_embinium_image');
INSERT INTO `ressource` VALUES ('7', 'db_ressource_consumables', '1', '0', '1', '0', '0', '0', '7', 'pic/menu/icons/consumable.png', '');
INSERT INTO `ressource` VALUES ('8', 'db_ressource_energy', '0', '0', '0', '0', '0', '0', '-1', 'pic/menu/icons/energy.png', '');
INSERT INTO `ressource` VALUES ('9', 'db_ressource_food', '1', '0', '0', '0', '1', '0', '-1', 'pic/icons/food.jpg', null);
INSERT INTO `ressource` VALUES ('10', 'db_ressource_researchpoints', '0', '0', '0', '0', '0', '0', '-1', 'pic/menu/icons/research.png', null);
INSERT INTO `ressource` VALUES ('11', 'db_ressource_ore', '0', '1', '0', '0', '0', '0', '-1', 'pic/menu/icons/ore.png', 'rawresourcebar_ore_image');
INSERT INTO `ressource` VALUES ('12', 'db_ressource_ynkeloniumore', '0', '1', '0', '0', '0', '0', '-1', 'pic/menu/icons/ynkeore.png', 'rawresourcebar_ynkeore_image');
INSERT INTO `ressource` VALUES ('13', 'db_ressource_howalgoniumcrystals', '0', '1', '0', '0', '0', '0', '-1', 'pic/menu/icons/howalore.png', 'rawresourcebar_howalore_image');
INSERT INTO `ressource` VALUES ('14', 'db_ressource_computerresearchpoints', '0', '0', '0', '0', '0', '0', '-1', 'pic/menu/icons/cresearch.png', null);
INSERT INTO `ressource` VALUES ('20', 'db_ressource_credits', '0', '0', '1', '1', '0', '0', '0', 'pic/icons/money.jpg', null);
INSERT INTO `ressource` VALUES ('30', 'db_ressource_moduleproduction', '0', '0', '0', '0', '0', '0', '-1', 'pic/icons/buildpoints.gif', null);
INSERT INTO `ressource` VALUES ('31', 'db_ressource_buildingpointsspaceport', '0', '0', '0', '0', '0', '0', '-1', 'pic/icons/buildpoints.gif', null);
INSERT INTO `ressource` VALUES ('32', 'db_ressource_buildingpointsorbitalspaceport', '0', '0', '0', '0', '0', '0', '-1', 'pic/icons/buildpoints.gif', null);
INSERT INTO `ressource` VALUES ('33', 'db_ressource_buildingpointsbarracks', '0', '0', '0', '0', '0', '0', '-1', 'pic/icons/buildpoints.gif', null);
INSERT INTO `ressource` VALUES ('99', 'db_ressource_developmentpoints', '0', '0', '1', '1', '0', '0', '-1', 'pic/icons/devpoints.png', null);