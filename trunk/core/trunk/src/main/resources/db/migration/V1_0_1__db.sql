 /* CREATE-DB Statements */
/* [143] Tables */ /*Found class: at.darkdestiny.core.model.Action
Generated SQL:*/

CREATE TABLE `action` (`type` enum('BUILDING','RESEARCH','MODULE','SHIP','GROUNDTROOPS','FLIGHT','DEFENSE','SPYRELATED','TRADE','DECONSTRUCT','PROBE','SPACESTATION','SCAN_SYSTEM','SCAN_PLANET') COLLATE utf8_general_ci, `refTableId` int(11) unsigned NOT NULL DEFAULT '0', `userId` int(11) unsigned NOT NULL, `refEntityId` int(11) unsigned NOT NULL DEFAULT '0', `number` int(11) unsigned NOT NULL DEFAULT '0', `planetId` int(11) unsigned NOT NULL DEFAULT '0', `systemId` int(11) unsigned NOT NULL DEFAULT '0', `onHold` tinyint(3) DEFAULT '0', PRIMARY KEY (`refTableId`,`userId`,`refEntityId`,`planetId`,`systemId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ActiveBonus
Generated SQL:*/

CREATE TABLE `activebonus` (`refId` int(11) unsigned NOT NULL, `bonusId` int(11) unsigned NOT NULL, `bonusRange` enum('PLANET','EMPIRE') COLLATE utf8_general_ci DEFAULT 'PLANET', `duration` int(11) unsigned NOT NULL, PRIMARY KEY (`refId`,`bonusId`,`bonusRange`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AddOn
Generated SQL:*/

CREATE TABLE `addon` (`name` varchar(50), `version` varchar(50), `packageName` varchar(50), `interfaceName` varchar(50), `installed` tinyint(3), `active` tinyint(3), PRIMARY KEY (`name`,`version`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AIMessage
Generated SQL:*/

CREATE TABLE `aimessage` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `type` enum('CONSTRUCT','TAX','RESOURCE') COLLATE utf8_general_ci, `senderId` int(11), `receiverId` int(11), `planetId` int(11), `time` bigint(20), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AIMessageParameter
Generated SQL:*/

CREATE TABLE `aimessagepar` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `messageId` int(11), `parName` varchar(50), `parValue` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Alliance
Generated SQL:*/

CREATE TABLE `alliance` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `userId` int(11), `name` varchar(50), `picture`text (400) NOT NULL, `description`text (1000) NOT NULL, `leadership` enum('DEMOCRATIC','ARISTOCRATIC','DICTORIAL') COLLATE utf8_general_ci DEFAULT 'DEMOCRATIC', `tag` varchar(50), `isSubAllianceOf` int(11), `masterAllianceId` int(11), `isRecruiting` tinyint(3), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AllianceBoard
Generated SQL:*/

CREATE TABLE `allianceboard` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `allianceId` int(11), `name` varchar(50), `description`text (500) NOT NULL, `type` enum('ALLIANCE_INTERNAL','ALLIANCE_STRUCTURE','CUSTOM') COLLATE utf8_general_ci, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AllianceBoardPermission
Generated SQL:*/

CREATE TABLE `allianceboardpermission` (`refId` int(11), `allianceBoardId` int(11), `type` enum('ALLIANCE','USER') COLLATE utf8_general_ci, `isWrite` tinyint(3), `isDelete` tinyint(3), `isConfirmed` tinyint(3), PRIMARY KEY (`refId`,`allianceBoardId`,`type`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AllianceLastRead
Generated SQL:*/

CREATE TABLE `alliancelastread` (`allianceBoardId` int(11), `id` int(11), `lastRead` bigint(20), PRIMARY KEY (`allianceBoardId`,`id`,`lastRead`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AllianceMember
Generated SQL:*/

CREATE TABLE `alliancemembers` (`allianceId` int(11), `userId` int(11), `joinedOn` bigint(20), `isTrial` tinyint(3), `isAdmin` tinyint(3), `isCouncil` tinyint(3), `specialRankId` int(11) DEFAULT '0', `lastRead` bigint(20) DEFAULT '0', PRIMARY KEY (`allianceId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AllianceMessage
Generated SQL:*/

CREATE TABLE `alliancemessage` (`allianceId` int(11), `userId` int(11), `boardId` int(11), `topic`text (500) NOT NULL, `message`text (500) NOT NULL, `postTime` bigint(20), PRIMARY KEY (`allianceId`,`boardId`,`postTime`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AllianceMessageRead
Generated SQL:*/

CREATE TABLE `alliancemessageread` (`allianceId` int(11), `boardId` int(11), `userId` int(11), `lastRead` bigint(20), PRIMARY KEY (`allianceId`,`boardId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.AllianceRank
Generated SQL:*/

CREATE TABLE `alliancerank` (`allianceId` int(11), `rankId` int(11), `rankName` varchar(50), `comparisonRank` int(11), PRIMARY KEY (`allianceId`,`rankId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Attribute
Generated SQL:*/

CREATE TABLE `attribute` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `reference` enum('NOT_ALLOWED','OPTIONAL','REQUIRED') COLLATE utf8_general_ci, `displayOrder` int(11) DEFAULT '-1', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.BattleLog
Generated SQL:*/

CREATE TABLE `battlelog` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `comment` varchar(50), `accuracy` float, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.BattleLogEntry
Generated SQL:*/

CREATE TABLE `battlelogentry` (`id` int(11), `designId` int(11), `count` int(11), PRIMARY KEY (`id`,`designId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.BattleResult
Generated SQL:*/

CREATE TABLE `battleresult` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `data` varchar(50), `version` varchar(50), `userId` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Bonus
Generated SQL:*/

CREATE TABLE `bonus` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `description` varchar(50), `value` double, `valueType` enum('DEFINE','INCREASE','DECREASE','INCREASE_PERC','DECREASE_PERC') COLLATE utf8_general_ci, `bonusType` enum('POPULATION_GROWTH','PRODUCTION','CONSTRUCTION','RESEARCH','TRADE','CREDIT') COLLATE utf8_general_ci, `duration` int(11), `bonusRange` enum('PLANET','EMPIRE') COLLATE utf8_general_ci, `cost` bigint(20), `reqResearch` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Campaign
Generated SQL:*/

CREATE TABLE `campaign` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `campaignOrder` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.CampaignToUser
Generated SQL:*/

CREATE TABLE `campaigntouser` (`userId` int(11), `campaignId` int(11), PRIMARY KEY (`userId`,`campaignId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ChangeEntry
Generated SQL:*/

CREATE TABLE `changeentry` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `date` bigint(20), `text` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Chassis
Generated SQL:*/

CREATE TABLE `chassis` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `minBuildQty` int(11), `relModuleId` int(11), `img_design` varchar(50), `starbase` tinyint(3), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ChassisWeaponRel
Generated SQL:*/

CREATE TABLE `chassisweaponrel` (`chassisId` int(11), `moduleId` int(11), `cost_factor` double, `size_factor` double, `strength_factor` double, `energy_factor` double, PRIMARY KEY (`chassisId`,`moduleId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.CombatPlayer
Generated SQL:*/

CREATE TABLE `combatplayer` (`combatId` int(11), `userId` int(11), `startTime` bigint(20), `reportStatus` enum('SHOWING','ARCHIVED','DELETED') COLLATE utf8_general_ci, PRIMARY KEY (`combatId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.CombatRound
Generated SQL:*/

CREATE TABLE `combatround` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `combatId` int(11), `round` int(11), `report` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Condition
Generated SQL:*/

CREATE TABLE `titlecondition` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `description` varchar(50), `clazz` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ConditionParameter
Generated SQL:*/

CREATE TABLE `conditionparameter` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `conditionId` int(11), `paramOrder` int(11), `name` varchar(50), `type` enum('COMPARATOR','LONG','INTEGER','DOUBLE','FLOAT','BOOLEAN','STRING','LOCATION') COLLATE utf8_general_ci, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ConditionToTitle
Generated SQL:*/

CREATE TABLE `conditiontotitle` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `titleId` int(11), `conditionId` int(11), `description` varchar(50), `conditionOrder` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ConditionToUser
Generated SQL:*/

CREATE TABLE `conditiontouser` (`userId` int(11), `conditionToTitleId` int(11), `value` varchar(50), PRIMARY KEY (`userId`,`conditionToTitleId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Construction
Generated SQL:*/

CREATE TABLE `construction` (`id` int(11) unsigned NOT NULL, `name` varchar(50), `infotext` varchar(50), `typplanet` varchar(50), `buildtime` int(11), `type` enum('ADMINISTRATION','ZIVIL','ENERGY','INDUSTRY','MILITARY','PLANETARY_DEFENSE') COLLATE utf8_general_ci, `orderNo` int(11), `population` bigint(20), `prodType` enum('COMMON','AGRAR','INDUSTRY','ENERGY','RESEARCH') COLLATE utf8_general_ci, `hp` int(11), `designId` int(11), `workers` bigint(20), `surface` int(11), `uniquePerPlanet` tinyint(3), `uniquePerSystem` tinyint(3), `isMine` int(11), `specialBuildingId` int(11), `minPop` int(11), `image` varchar(50), `destructible` tinyint(3), `idleable` tinyint(3), `levelable` tinyint(3) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ConstructionModule
Generated SQL:*/

CREATE TABLE `constructionmodule` (`constructionId` int(11), `moduleId` int(11), `count` int(11), PRIMARY KEY (`constructionId`,`moduleId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ConstructionRestriction
Generated SQL:*/

CREATE TABLE `constructionrestriction` (`type` int(11), `consId` int(11), `restrictionClass` varchar(50), PRIMARY KEY (`type`,`consId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ConstructionUpgrade
Generated SQL:*/

CREATE TABLE `constructionupgrade` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `baseConstruction` int(11) unsigned NOT NULL, `targetConstruction` int(11) unsigned NOT NULL, `baseConsConsumption` int(11) unsigned NOT NULL, `targetConsCreation` int(11) unsigned NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.CreditDonation
Generated SQL:*/

CREATE TABLE `creditdonation` (`date` bigint(20), `fromUserId` int(11), `toUserId` int(11), `amount` bigint(20), PRIMARY KEY (`date`,`fromUserId`,`toUserId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.DamagedShips
Generated SQL:*/

CREATE TABLE `damagedships` (`shipFleetId` int(11), `refTable` enum('SHIPFLEET','HANGARRELATION') COLLATE utf8_general_ci, `damageLevel` enum('NODAMAGE','MINORDAMAGE','LIGHTDAMAGE','MEDIUMDAMAGE','HEAVYDAMAGE','DESTROYED') COLLATE utf8_general_ci, `count` int(11), PRIMARY KEY (`shipFleetId`,`refTable`,`damageLevel`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.DeletedUser
Generated SQL:*/

CREATE TABLE `deleteduser` (`userName`text (15) NOT NULL, `password`text (25) NOT NULL, `gameName`text (15) NOT NULL, `email` varchar(50), PRIMARY KEY (`email`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.DesignModule
Generated SQL:*/

CREATE TABLE `designmodule` (`designId` int(11), `moduleId` int(11), `isConstruction` tinyint(3), `count` int(11), PRIMARY KEY (`designId`,`moduleId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.DiplomacyRelation
Generated SQL:*/

CREATE TABLE `diplomacyrelation` (`fromId` int(11), `toId` int(11), `diplomacyTypeId` int(11), `type` enum('USER_TO_USER','USER_TO_ALLIANCE','ALLIANCE_TO_USER','ALLIANCE_TO_ALLIANCE') COLLATE utf8_general_ci, `date` bigint(20), `initialiser` tinyint(3), `acknowledged` tinyint(3), `pending` tinyint(3), `sharingMap` tinyint(3) DEFAULT '0', PRIMARY KEY (`fromId`,`toId`,`diplomacyTypeId`,`type`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.DiplomacyType
Generated SQL:*/

CREATE TABLE `diplomacytype` (`id` int(11) NOT NULL, `name` varchar(50), `description` varchar(50), `clazz` varchar(50), `voteRequired` tinyint(3), `requestString` varchar(50), `weight` int(11), `userDefineable` tinyint(3), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Embassy
Generated SQL:*/

CREATE TABLE `embassy` (`userId` int(11), `remUserId` int(11), `synced` tinyint(3), PRIMARY KEY (`userId`,`remUserId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Filter
Generated SQL:*/

CREATE TABLE `filter` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `description`text (550) NOT NULL, `userId` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.FilterToValue
Generated SQL:*/

CREATE TABLE `filtertovalue` (`filterId` int(11), `valueId` int(11), `comparator` enum('SMALLER','SMALLEREQUALS','EQUALS','BIGGEREQUALS','BIGGER') COLLATE utf8_general_ci, `value` varchar(50), PRIMARY KEY (`filterId`,`valueId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.FilterValue
Generated SQL:*/

CREATE TABLE `filtervalue` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `value` enum('POPULATION_VALUE','MIGRATION_VALUE','MORAL_VALUE','VISIBLE_FLAG','ENERGY_PERCENTAGE_USAGE','ENERGY_PERCENTAGE_FREE','HAS_IN_CONSTRUCTION','HABITABLE','HOSTILE_COMPACT','HOSTILE_GASEOUS') COLLATE utf8_general_ci, `type` enum('LONG','INTEGER','DOUBLE','FLOAT','BOOLEAN','STRING') COLLATE utf8_general_ci, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.FkChildTest
Generated SQL:*/

CREATE TABLE `fk_testtable_childs` (`id` int(11), `fk_testtable_ref` int(11), `name` varchar(50), PRIMARY KEY (`id`,`fk_testtable_ref`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.FkTest
Generated SQL:*/

CREATE TABLE `fk_testtable` (`id` int(11), `name` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.FleetDetail
Generated SQL:*/

CREATE TABLE `fleetdetails` (`Id` int(11) unsigned NOT NULL AUTO_INCREMENT, `fleetId` int(11), `startSystem` int(11), `startPlanet` int(11), `destSystem` int(11), `destPlanet` int(11), `commandTypeId` int(11), `commandType` int(11), `startTime` int(11), `flightTime` int(11), `speed` float, `startX` int(11), `startY` int(11), `destX` int(11), `destY` int(11), PRIMARY KEY (`Id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.FleetFormation
Generated SQL:*/

CREATE TABLE `fleetformation` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `userId` int(11), `name` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.FleetLoading
Generated SQL:*/

CREATE TABLE `fleetloading` (`fleetId` int(11), `loadtype` int(11), `id` int(11), `count` int(11), PRIMARY KEY (`fleetId`,`loadtype`,`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.FleetOrder
Generated SQL:*/

CREATE TABLE `fleetorders` (`fleetId` int(11), `fleetType` enum('FLEET','FLEET_FORMATION') COLLATE utf8_general_ci, `fleetorder` int(11), `retreatFactor` int(11), `retreatToType` enum('SYSTEM','PLANET','TRANSIT') COLLATE utf8_general_ci, `retreatTo` int(11), PRIMARY KEY (`fleetId`,`fleetType`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Galaxy
Generated SQL:*/

CREATE TABLE `galaxy` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `width` int(11), `height` int(11), `originX` int(11), `originY` int(11), `startSystem` int(11), `endSystem` int(11), `playerStart` tinyint(3) DEFAULT '0', `allowIntergalacticFlight` tinyint(3) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.GameData
Generated SQL:*/

CREATE TABLE `gamedata` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `prago` int(11), `daArk` int(11), `startTime` bigint(20), `tickTime` int(11), `lastUpdatedTick` int(11), `status` enum('STOPPED','REGISTRATION','RUNNING','RUNNING_NOUPDATE') COLLATE utf8_general_ci, `version` varchar(50), `width` int(11), `height` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.GroundCombat
Generated SQL:*/

CREATE TABLE `groundcombat` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `planetId` int(11), `startTime` bigint(20), `winner` int(11), `planetOwner` int(11), `combatStatus` enum('FINISHED','LANDING','ONGOING') COLLATE utf8_general_ci, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.GroundTroop
Generated SQL:*/

CREATE TABLE `groundtroops` (`id` int(11) NOT NULL, `hitpoints` int(11), `name` varchar(50), `description` varchar(50), `credits` bigint(20), `crew` int(11), `size` int(11), `modulPoints` int(11), `image` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.GTRelation
Generated SQL:*/

CREATE TABLE `gtrelation` (`srcId` int(11), `targetId` int(11), `factor` float, `researchRequired` tinyint(3), `attackPoints` float, `attackProbab` float, `hitProbab` float, PRIMARY KEY (`srcId`,`targetId`,`researchRequired`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.HangarRelation
Generated SQL:*/

CREATE TABLE `hangarrelation` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `fleetId` int(11), `designId` int(11), `count` int(11), `relationToDesign` int(11), `relationToSF` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ImperialStatistic
Generated SQL:*/

CREATE TABLE `imperialstatistic` (`userid` int(11), `allyid` int(11), `typeid` enum('STATISTIC','TROOPS','SHIPS','DEFENCE','RESSOURCE','RANK') COLLATE utf8_general_ci, `sourceid` int(11), `value` bigint(20), PRIMARY KEY (`userid`,`allyid`,`typeid`,`sourceid`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Improvement
Generated SQL:*/

CREATE TABLE `improvement` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `description` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Intelligence
Generated SQL:*/

CREATE TABLE `intelligence` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `userId` int(11) unsigned NOT NULL, `refDocumentId` int(11) unsigned NOT NULL, `refEntity` enum('PLANET','SYSTEM') COLLATE utf8_general_ci NOT NULL, `refEntityId` int(11) unsigned NOT NULL, `type` enum('RESOURCE','STORAGE','GROUNDTROOP','SHIP','POPULATION','DEFENSE','CONSTRUCTION') COLLATE utf8_general_ci NOT NULL, `refId` int(11) unsigned NOT NULL, `value` int(11) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ITechFunctions
Generated SQL:*/


/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Language
Generated SQL:*/

CREATE TABLE `language` (`id` int(11) NOT NULL, `name` varchar(50), `language` varchar(50), `country` varchar(50), `propertiesfile` varchar(50), `pic` varchar(50), `masterLanguageId` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.LoginTracker
Generated SQL:*/

CREATE TABLE `logintracker` (`userId` int(11), `loginIP` varchar(50), `time` bigint(20), PRIMARY KEY (`userId`,`loginIP`,`time`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.MenuImage
Generated SQL:*/

CREATE TABLE `menuimage` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `type` int(11), `description` varchar(50), `imageUrl` varchar(50), `width` int(11), `height` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.MenuLink
Generated SQL:*/

CREATE TABLE `menulink` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `linkOrder` int(11), `linkName`text (400) NOT NULL, `linkUrl`text (400) NOT NULL, `linkDest` varchar(50), `menuLinkId` int(11), `accessLevel` int(11), `adminOnly` tinyint(3), `subCategory` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Message
Generated SQL:*/

CREATE TABLE `message` (`messageId` int(11) unsigned NOT NULL AUTO_INCREMENT, `sourceUserId` int(11) unsigned NOT NULL DEFAULT '0', `targetUserId` int(11) unsigned NOT NULL DEFAULT '0', `topic`text (50), `text`text (500) NOT NULL, `timeSent` bigint(20) unsigned NOT NULL DEFAULT '0', `type` enum('SYSTEM','USER','ALLIANCE') COLLATE utf8_general_ci DEFAULT 'SYSTEM', `viewed` tinyint(3) DEFAULT '0', `delBySource` tinyint(3) DEFAULT '0', `delByTarget` tinyint(3) DEFAULT '0', `archiveBySource` tinyint(3) DEFAULT '0', `archiveByTarget` tinyint(3) DEFAULT '0', `outMaster` tinyint(3) DEFAULT '0', `refType` enum('NOTHING','VOTE','ALLIANCEBOARD_ID_ADD','ALLIANCEBOARD_ID_DELETE','TERRITORY_ID_ADD','TERRITORY_ID_DELETE') COLLATE utf8_general_ci DEFAULT 'NOTHING', `refId` int(11) DEFAULT '0', PRIMARY KEY (`messageId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Module
Generated SQL:*/

CREATE TABLE `module` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `type` enum('CHASSIS','WEAPON','SHIELD','ARMOR','ENGINE','REACTOR','TRANSPORT_RESSOURCE','TRANSPORT_POPULATION','HANGAR','ENERGY_STORAGE','SPECIAL','COMPUTER','COLONISATION') COLLATE utf8_general_ci, `isChassis` tinyint(3), `uniquePerShip` tinyint(3), `freeForDesign` tinyint(3), `uniquePerType` tinyint(3), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ModuleAttribute
Generated SQL:*/

CREATE TABLE `moduleattributes` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `moduleId` int(11), `chassisId` int(11), `researchId` int(11), `attributeId` int(11), `attributeType` enum('DEFINE','INCREASE','DECREASE','INCREASE_PERC','DECREASE_PERC') COLLATE utf8_general_ci, `value` double, `refId` int(11), `refType` enum('MODULE','CHASSIS','RESSOURCE') COLLATE utf8_general_ci, `appliesTo` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.MultiLog
Generated SQL:*/

CREATE TABLE `multilog` (`tick` int(11), `ip` varchar(50), `userId` int(11), `date` bigint(20), `count` int(11), `comment` varchar(50), PRIMARY KEY (`tick`,`ip`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.News
Generated SQL:*/

CREATE TABLE `news` (`newsId` int(11) unsigned NOT NULL AUTO_INCREMENT, `subject` varchar(50), `message` varchar(50), `isArchived` tinyint(3), `date` bigint(20), `submittedById` int(11), `languageId` int(11), PRIMARY KEY (`newsId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Newsletter
Generated SQL:*/

CREATE TABLE `newsletter` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `email` varchar(50), `allowEmail` tinyint(3), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Note
Generated SQL:*/

CREATE TABLE `note` (`planetId` int(11), `userId` int(11), `message` varchar(50), PRIMARY KEY (`planetId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.NotificationToUser
Generated SQL:*/

CREATE TABLE `notificationtouser` (`notificationId` int(11), `userId` int(11), `enabled` tinyint(3), `awayMode` tinyint(3), PRIMARY KEY (`notificationId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.NotificationType
Generated SQL:*/

CREATE TABLE `notification` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `description` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ParameterToTitle
Generated SQL:*/

CREATE TABLE `parametertotitle` (`conditionToTitleId` int(11), `parameterId` int(11), `value`text (500) NOT NULL, PRIMARY KEY (`conditionToTitleId`,`parameterId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Password
Generated SQL:*/

CREATE TABLE `password` (`password` enum('TASK_TICKUPDATE','TASK_CALCSTATISTICS','TASK_DELETETRIAL','TASK_DELETEUSER','WEBSERVICE') COLLATE utf8_general_ci, `value` varchar(50), PRIMARY KEY (`password`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Planet
Generated SQL:*/

CREATE TABLE `planet` (`id` int(11) unsigned NOT NULL, `systemId` int(11), `landType` varchar(50), `atmosphereType` varchar(50), `avgTemp` int(11), `diameter` int(11), `orbitLevel` int(11), `growthBonus` float, `researchBonus` float, `productionBonus` float, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlanetCategory
Generated SQL:*/

CREATE TABLE `planetcategory` (`planetId` int(11), `categoryId` int(11), PRIMARY KEY (`planetId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlanetConstruction
Generated SQL:*/

CREATE TABLE `planetconstruction` (`planetId` int(11), `constructionId` int(11), `number` int(11), `idle` int(11) DEFAULT '0', `level` int(11) DEFAULT '-1', PRIMARY KEY (`planetId`,`constructionId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlanetDefense
Generated SQL:*/

CREATE TABLE `planetdefense` (`planetId` int(11), `systemId` int(11), `fleetId` int(11), `unitId` int(11), `userId` int(11), `type` enum('TURRET','SPACESTATION') COLLATE utf8_general_ci, `count` int(11), PRIMARY KEY (`planetId`,`systemId`,`fleetId`,`unitId`,`userId`,`type`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlanetLog
Generated SQL:*/

CREATE TABLE `planetlog` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `planetId` int(11), `userId` int(11), `time` int(11), `type` enum('COLONIZED','INVADED','ABANDONED','INVADED_HELP','TRANSFER') COLLATE utf8_general_ci DEFAULT 'COLONIZED', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlanetLoyality
Generated SQL:*/

CREATE TABLE `planetloyality` (`planetId` int(11), `userId` int(11), `value` double, PRIMARY KEY (`planetId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlanetRessource
Generated SQL:*/

CREATE TABLE `planetressource` (`planetId` int(11), `ressId` int(11), `type` enum('PLANET','INSTORAGE','MINIMUM_PRODUCTION','MINIMUM_TRADE','MINIMUM_TRANSPORT') COLLATE utf8_general_ci, `qty` bigint(20), PRIMARY KEY (`planetId`,`ressId`,`type`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlayerCategory
Generated SQL:*/

CREATE TABLE `playercategory` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `userId` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlayerFleet
Generated SQL:*/

CREATE TABLE `playerfleet` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `userId` int(11), `planetId` int(11), `systemId` int(11), `locationId` int(11), `locationType` enum('SYSTEM','PLANET','TRANSIT') COLLATE utf8_general_ci, `status` int(11) DEFAULT '0', `fleetFormationId` int(11) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlayerPlanet
Generated SQL:*/

CREATE TABLE `playerplanet` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `userId` int(11), `planetId` int(11), `homeSystem` tinyint(3), `colonyType` enum('DEFAULT','MINING_COLONY','NPC_TRANSMITTER') COLLATE utf8_general_ci DEFAULT 'DEFAULT', `population` bigint(20), `moral` int(11), `unrest` double, `growth` float, `specialPoints` float, `specialGrowth` float, `specialResearch` float, `specialProduction` float, `tax` int(11), `growthStatus` float, `researchStatus` float, `productionStatus` float, `priorityIndustry` int(11), `priorityAgriculture` int(11), `priorityResearch` int(11), `invisibleFlag` tinyint(3) DEFAULT '0', `migration` int(11), `dismantle` tinyint(3) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlayerResearch
Generated SQL:*/

CREATE TABLE `playerresearch` (`userId` int(11), `researchId` int(11), `resTime` bigint(20), PRIMARY KEY (`userId`,`researchId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlayerStatistic
Generated SQL:*/

CREATE TABLE `playerstatistics` (`userId` int(11), `date` bigint(20), `researchPoints` bigint(20), `ressourcePoints` bigint(20), `populationPoints` bigint(20), `militaryPoints` bigint(20), `rank` int(11), PRIMARY KEY (`userId`,`date`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PlayerTroop
Generated SQL:*/

CREATE TABLE `playertroops` (`planetId` int(11), `userId` int(11), `troopId` int(11), `number` bigint(20), PRIMARY KEY (`planetId`,`userId`,`troopId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PriceList
Generated SQL:*/

CREATE TABLE `pricelist` (`id` int(11) NOT NULL AUTO_INCREMENT, `userId` int(11), `listType` enum('PUBLIC','ALLIED','ALLY','USER') COLLATE utf8_general_ci DEFAULT 'PUBLIC', `refId` int(11) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.PriceListEntry
Generated SQL:*/

CREATE TABLE `pricelistentry` (`id` int(11) NOT NULL AUTO_INCREMENT, `priceListId` int(11), `ressId` int(11), `price` int(11) DEFAULT '0', `sold` bigint(20) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Production
Generated SQL:*/

CREATE TABLE `production` (`constructionId` int(11), `ressourceId` int(11), `quantity` bigint(20), `incdec` int(11), PRIMARY KEY (`constructionId`,`ressourceId`,`incdec`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ProductionOrder
Generated SQL:*/

CREATE TABLE `productionorder` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `planetId` int(11) DEFAULT '0', `moduleNeed` int(11) DEFAULT '0', `moduleProc` int(11) DEFAULT '0', `dockNeed` int(11) DEFAULT '0', `dockProc` int(11) DEFAULT '0', `orbDockNeed` int(11) DEFAULT '0', `orbDockProc` int(11) DEFAULT '0', `kasNeed` int(11) DEFAULT '0', `kasProc` int(11) DEFAULT '0', `indNeed` int(11) DEFAULT '0', `indProc` int(11) DEFAULT '0', `priority` int(11) DEFAULT '1', `type` enum('PRODUCE','SCRAP','UPGRADE','GROUNDTROOPS','C_BUILD','C_BUILD_STATIONS','C_SCRAP','C_UPGRADE','REPAIR','C_IMPROVE') COLLATE utf8_general_ci, `toId` int(11) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Quiz
Generated SQL:*/

CREATE TABLE `quiz` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `topic` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.QuizAnswer
Generated SQL:*/

CREATE TABLE `quizanswer` (`quizEntryId` int(11), `answer` varchar(50), PRIMARY KEY (`quizEntryId`,`answer`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.QuizEntry
Generated SQL:*/

CREATE TABLE `quizentry` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `quizId` int(11), `difficulty` int(11), `author` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.QuizQuestion
Generated SQL:*/

CREATE TABLE `quizquestion` (`quizEntryId` int(11), `question` varchar(50), PRIMARY KEY (`quizEntryId`,`question`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ResAlignment
Generated SQL:*/

CREATE TABLE `resalignment` (`id` int(11), `x` int(11), `y` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Research
Generated SQL:*/

CREATE TABLE `research` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `rp` int(11), `rpComputer` int(11), `rpSpecial` int(11), `shortText`text (100) NOT NULL, `detailText`text (100) NOT NULL, `smallPic`text (500) NOT NULL, `largePic` varchar(50), `bonus` double, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ResearchProgress
Generated SQL:*/

CREATE TABLE `researchprogress` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `userId` int(11), `researchId` int(11), `rpLeft` int(11), `rpLeftComputer` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Ressource
Generated SQL:*/

CREATE TABLE `ressource` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `transportable` tinyint(3), `mineable` tinyint(3), `storeable` tinyint(3), `ressourcecost` tinyint(3), `baseRess` int(11), `researchRequired` int(11), `displayLocation` int(11), `imageLocation` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.RessourceCost
Generated SQL:*/

CREATE TABLE `ressourcecost` (`type` enum('CONSTRUCTION','MODULE','GROUNDTROOPS','SHIP') COLLATE utf8_general_ci, `id` int(11), `ressId` int(11), `qty` bigint(20), PRIMARY KEY (`type`,`id`,`ressId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.RoundEntry
Generated SQL:*/

CREATE TABLE `roundentry` (`combatRoundId` int(11), `userId` int(11), `troopId` int(11), `number` bigint(20), `dead` bigint(20), PRIMARY KEY (`combatRoundId`,`userId`,`troopId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ShipDesign
Generated SQL:*/

CREATE TABLE `shipdesigns` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `chassis` int(11), `userId` int(11), `name` varchar(50), `designTime` bigint(20), `primaryTarget` int(11) DEFAULT '0', `type` enum('BATTLE','TRANSPORT','CARRIER','COLONYSHIP','SPACEBASE') COLLATE utf8_general_ci, `crew` int(11) DEFAULT '0', `buildable` tinyint(3) DEFAULT '0', `standardspeed` float DEFAULT '0', `hyperspeed` float DEFAULT '0', `troopSpace` int(11) DEFAULT '0', `ressSpace` int(11) DEFAULT '0', `isConstruct` tinyint(3) DEFAULT '0', `smallHangar` int(11) DEFAULT '0', `mediumHangar` int(11) DEFAULT '0', `largeHangar` int(11) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ShipFleet
Generated SQL:*/

CREATE TABLE `shipfleet` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `fleetId` int(11), `designId` int(11), `count` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ShipTypeAdjustment
Generated SQL:*/

CREATE TABLE `shiptypeadjustment` (`type` int(11) unsigned NOT NULL AUTO_INCREMENT, `type_E` enum('BATTLE','TRANSPORT','CARRIER','COLONYSHIP','SPACEBASE') COLLATE utf8_general_ci, `attackFactor` float, `structureFactor` float, `shieldFactor` float, `ressFactor` float, `troopFactor` float, `hangarFactor` float, `costFactor` float, PRIMARY KEY (`type`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.SizeRelation
Generated SQL:*/

CREATE TABLE `sizerelation` (`chassisId` int(11), `engineFactor` int(11), `propulsionFactor` double, `hyperFactor` int(11), `creditFactor` int(11), `hyperCrystalFactor` int(11), PRIMARY KEY (`chassisId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Spy
Generated SQL:*/

CREATE TABLE `spy` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `xp` int(11), `attachedToType` enum('NOHTING','PLANET','FLEET') COLLATE utf8_general_ci DEFAULT 'PLANET', `attachedToId` int(11), `systemId` int(11), `planetId` int(11), `currAction` enum('DEFENSE','INACTION','MOVING') COLLATE utf8_general_ci DEFAULT 'DEFENSE', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.SpyAction
Generated SQL:*/

CREATE TABLE `spyaction` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `spyId` int(11), `spyActionId` int(11), `durationLeft` int(11), `targetEntityType` enum('NOTHING','SHIP','CONSTRUCTION') COLLATE utf8_general_ci, `targetEntityId` int(11), `successProbability` int(11), `startSystem` int(11), `startPlanet` int(11), `targetSystem` int(11), `targetPlanet` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.SpyActionType
Generated SQL:*/

CREATE TABLE `spyactiontype` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `duration` int(11), `description` varchar(50), `xpNeed` int(11), `descriptionClass` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Statistic
Generated SQL:*/

CREATE TABLE `statistic` (`userid` int(11), `rank` int(11), `lastrank` int(11), `userName` varchar(50), `points` int(11), PRIMARY KEY (`userid`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.StatisticCategory
Generated SQL:*/

CREATE TABLE `statisticcategory` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.StatisticEntry
Generated SQL:*/

CREATE TABLE `statisticentry` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `ref`text (450) NOT NULL, `refType` enum('CLAZZ','SELECTSTATEMENT') COLLATE utf8_general_ci, `type` enum('PIECHART','BARCHART') COLLATE utf8_general_ci, `categoryId` int(11), `xTitle` varchar(50), `yTitle` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Style
Generated SQL:*/

CREATE TABLE `style` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(50), `description` varchar(50), `creator` varchar(50), `version` varchar(50), `width` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.StyleElement
Generated SQL:*/

CREATE TABLE `styleelement` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `styleId` int(11), `clazz`text (450) NOT NULL, `name` varchar(50), `description` varchar(50), `core` tinyint(3), `template` varchar(50), PRIMARY KEY (`id`,`styleId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.StyleToUser
Generated SQL:*/

CREATE TABLE `styletouser` (`styleId` int(11), `elementId` int(11), `userId` int(11), `rank` int(11), PRIMARY KEY (`styleId`,`elementId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.SunTransmitter
Generated SQL:*/

CREATE TABLE `suntransmitter` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `systemId` int(11), `planetId` int(11), `userId` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.SunTransmitterRoute
Generated SQL:*/

CREATE TABLE `suntransmitterroute` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `startId` int(11), `endId` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.System
Generated SQL:*/

CREATE TABLE `system` (`id` int(11) unsigned NOT NULL, `name` varchar(50), `posx` int(11), `posy` int(11), `visibility` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TechRelation
Generated SQL:*/

CREATE TABLE `techrelation` (`sourceID` int(11), `reqConstructionID` int(11), `reqResearchID` int(11), `treeType` int(11), PRIMARY KEY (`sourceID`,`reqConstructionID`,`reqResearchID`,`treeType`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TechType
Generated SQL:*/

CREATE TABLE `techtype` (`id` int(11) unsigned NOT NULL, `name` varchar(50), `refDAO` varchar(50), `refModel` varchar(50), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TerritoryEntry
Generated SQL:*/

CREATE TABLE `territoryentry` (`combatRoundId` int(11), `userId` int(11), `territorySize` double, `territoryChange` double, PRIMARY KEY (`combatRoundId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TerritoryMap
Generated SQL:*/

CREATE TABLE `territorymap` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `refId` int(11), `name` varchar(50), `description`text (500) NOT NULL, `type` enum('USER','ALLIANCE') COLLATE utf8_general_ci, `territoryType` enum('NORMAL','EMPIRE') COLLATE utf8_general_ci, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TerritoryMapPoint
Generated SQL:*/

CREATE TABLE `territorymappoint` (`id` int(11), `territoryId` int(11), `x` int(11), `y` int(11), PRIMARY KEY (`id`,`territoryId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TerritoryMapShare
Generated SQL:*/

CREATE TABLE `territorymapshare` (`territoryId` int(11), `refId` int(11), `type` enum('ALL','ALLIANCE','USER','USER_BY_ALLIANCE','USER_BY_ALL') COLLATE utf8_general_ci, `showOnStarmap` tinyint(3), PRIMARY KEY (`territoryId`,`refId`,`type`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Title
Generated SQL:*/

CREATE TABLE `title` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `campaignId` int(11), `name` varchar(50), `description` varchar(50), `type` enum('IMPERIAL','RESEARCH','MILITARY','VARIOUS') COLLATE utf8_general_ci, `difficulty` int(11), `points` int(11), `clazz` varchar(50), `visible` tinyint(3), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TitleToUser
Generated SQL:*/

CREATE TABLE `titletouser` (`userId` int(11), `titleId` int(11), PRIMARY KEY (`userId`,`titleId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TradeFleet
Generated SQL:*/

CREATE TABLE `tradefleets` (`tradeId` int(11), `designId` int(11), `tradePostId` int(11), `count` int(11), PRIMARY KEY (`tradeId`,`designId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TradeLog
Generated SQL:*/

CREATE TABLE `tradelog` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `fromUserId` int(11), `date` bigint(20), `toUserId` int(11), `ressourceId` int(11), `quantity` bigint(20), `price` bigint(20), `total` bigint(20), `fromPlanetId` int(11), `toPlanetId` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TradeOffer
Generated SQL:*/

CREATE TABLE `tradeoffer` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `tradePostId` int(11), `ressourceId` int(11), `quantity` bigint(20), `type` enum('NONE','MINIMUM','FIXED') COLLATE utf8_general_ci, `sold` bigint(20), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TradePost
Generated SQL:*/

CREATE TABLE `tradepost` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `planetId` int(11), `name` varchar(50), `transportPerLJ` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TradePostShip
Generated SQL:*/

CREATE TABLE `tradepostship` (`tradePostId` int(11), `designId` int(11), `count` int(11), PRIMARY KEY (`tradePostId`,`designId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TradeRoute
Generated SQL:*/

CREATE TABLE `traderoute` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `userId` int(11), `startPlanet` int(11), `targetPlanet` int(11), `distance` double, `tradePostId` int(11) DEFAULT '0', `targetUserId` int(11) DEFAULT '0', `efficiency` double DEFAULT '0', `capacity` int(11) DEFAULT '0', `status` enum('ACTIVE','PENDING','PAUSED','BLOCKED') COLLATE utf8_general_ci, `type` enum('TRANSPORT','TRADE') COLLATE utf8_general_ci, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TradeRouteDetail
Generated SQL:*/

CREATE TABLE `traderoutedetail` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `routeId` int(11), `direction` tinyint(3) DEFAULT '0', `ressId` int(11), `maxQty` int(11) DEFAULT '0', `maxQtyPerTick` int(11) DEFAULT '0', `maxDuration` int(11) DEFAULT '0', `qtyDelivered` bigint(20) DEFAULT '0', `lastTransport` int(11) DEFAULT '0', `timeStarted` int(11) DEFAULT '0', `disabled` tinyint(3) DEFAULT '0', `capReason` enum('NONE','TARGET_FULL','SOURCE_LOW','TARGET_FULL_SERVE') COLLATE utf8_general_ci DEFAULT 'NONE', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.TradeRouteShip
Generated SQL:*/

CREATE TABLE `traderouteship` (`traderouteId` int(11), `shiptype` int(11), `addLocation` enum('IGNORE','START','TARGET') COLLATE utf8_general_ci, `designId` int(11), `number` int(11), PRIMARY KEY (`traderouteId`,`shiptype`,`addLocation`,`designId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.User
Generated SQL:*/

CREATE TABLE `user` (`id` int(11) NOT NULL AUTO_INCREMENT, `userName`text (15) NOT NULL, `userType` enum('SYSTEM','HUMAN','AI') COLLATE utf8_general_ci DEFAULT 'HUMAN', `password`text (25) NOT NULL, `gameName`text (15) NOT NULL, `locale` varchar(50), `joinDate` bigint(20), `lastUpdate` int(11) DEFAULT '0', `activationCode` varchar(50), `active` tinyint(3), `admin` tinyint(3), `betaAdmin` tinyint(3), `guest` tinyint(3) DEFAULT '0', `locked` int(11), `ip`text (150) NOT NULL, `deleteDate` int(11), `trial` tinyint(3), `systemAssigned` tinyint(3) DEFAULT '0', `userConfigured` tinyint(3) DEFAULT '0', `multi` tinyint(3), `email`text (500) NOT NULL, `gender` enum('MALE','FEMALE') COLLATE utf8_general_ci, `trusted` tinyint(3) DEFAULT '0', `acceptedUserAgreement` tinyint(3) DEFAULT '0', `registrationToken` varchar(50) DEFAULT '', `portalPassword` varchar(50) DEFAULT '', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.UserData
Generated SQL:*/

CREATE TABLE `userdata` (`userId` int(11), `credits` bigint(20), `pic` varchar(50) NOT NULL DEFAULT '', `description` varchar(50) NOT NULL DEFAULT '', `rating` int(11), `titleId` int(11) DEFAULT '0', `developementPoints` double DEFAULT '0', `temporary` int(11) DEFAULT '0', `weeklyIncome` bigint(20) DEFAULT '0', PRIMARY KEY (`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.UserSettings
Generated SQL:*/

CREATE TABLE `usersettings` (`userId` int(11), `starmapHeight` int(11) DEFAULT '600', `starmapWidth` int(11) DEFAULT '600', `starmapExternalBrowser` tinyint(3) DEFAULT '0', `ttvHeight` int(11) DEFAULT '600', `ttvWidth` int(11) DEFAULT '600', `ttvExternalBrowser` tinyint(3) DEFAULT '0', `leaveDays` int(11) DEFAULT '0', `maxLeaveTicks` int(11) DEFAULT '0', `leaveDate` bigint(20) DEFAULT '0', `sittedById` int(11) DEFAULT '0', `showWelcomeSite` tinyint(3) DEFAULT '0', `showConstructionDetails` tinyint(3) DEFAULT '0', `showProductionDetails` tinyint(3) DEFAULT '0', `newsletter` tinyint(3) DEFAULT '0', `protectaccount` tinyint(3) DEFAULT '0', PRIMARY KEY (`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.ViewTable
Generated SQL:*/

CREATE TABLE `viewtable` (`systemId` int(11), `planetId` int(11), `userId` int(11), `lastTimeVisited` int(11) DEFAULT '0', `mostRecent` tinyint(3) DEFAULT '0', PRIMARY KEY (`systemId`,`planetId`,`userId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.VoteMetadata
Generated SQL:*/

CREATE TABLE `votemetadata` (`voteId` int(11), `dataName` varchar(50), `dataValue`text (500) NOT NULL, `dataType` enum('INTEGER','STRING','DOUBLE') COLLATE utf8_general_ci DEFAULT 'INTEGER', PRIMARY KEY (`voteId`,`dataName`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.VoteOption
Generated SQL:*/

CREATE TABLE `voteoptions` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `voteId` int(11), `voteOption` varchar(50), `ML` tinyint(3), `preSelect` tinyint(3), `type` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.VotePermission
Generated SQL:*/

CREATE TABLE `votepermission` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `voteId` int(11), `type` int(11), `typeId` int(11), `minRank` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.Voting
Generated SQL:*/

CREATE TABLE `voting` (`voteId` int(11) unsigned NOT NULL AUTO_INCREMENT, `minVotes` int(11), `totalVotes` int(11), `expire` int(11), `type` int(11), `votetext` varchar(50), `closed` tinyint(3) DEFAULT '0', `result` int(11) DEFAULT '0', PRIMARY KEY (`voteId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.VotingDetail
Generated SQL:*/

CREATE TABLE `votingdetails` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `voteId` int(11), `userId` int(11), `optionVoted` int(11), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
/*Found class: at.darkdestiny.core.model.WeaponFactor
Generated SQL:*/

CREATE TABLE `weaponfactor` (`weaponId` int(11), `steelarmor` double, `terkonitarmor` double, `ynkenitarmor` double, `soloniumarmor` double, `prallschirm_penetration` double, `huschirm_penetration` double, `paratron_penetration` double, `small_penalty` double, `atmosphere_penalty` double, `multifire` int(11), `weaponrange` int(11), PRIMARY KEY (`weaponId`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*---------------------------------------------*/
