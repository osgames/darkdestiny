DROP TABLE IF EXISTS `diplomacytype`;
CREATE TABLE `diplomacytype` (
  `id` int(11) NOT NULL,
  `relationType` enum('EMPIRE_RELATION','TRADE_AGREEMENT') NOT NULL DEFAULT 'EMPIRE_RELATION',
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `clazz` varchar(50) DEFAULT NULL,
  `voteRequired` tinyint(3) DEFAULT NULL,
  `requestString` varchar(50) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `userDefineable` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of diplomacytype
-- ----------------------------
INSERT INTO `diplomacytype` VALUES ('0', 'EMPIRE_RELATION', 'db_diplomacy_ally', 'db_diplomacy_ally_desc', 'at.darkdestiny.core.diplomacy.relations.Ally', '0', 'db_diplomacy_ally_reqStr', '120', '0');
INSERT INTO `diplomacytype` VALUES ('1', 'EMPIRE_RELATION', 'db_diplomacy_treaty', 'db_diplomacy_treaty_desc', 'at.darkdestiny.core.diplomacy.relations.Treaty', '1', 'db_diplomacy_treaty_reqStr', '100', '1');
INSERT INTO `diplomacytype` VALUES ('2', 'EMPIRE_RELATION', 'db_diplomacy_nap', 'db_diplomacy_nap_desc', 'at.darkdestiny.core.diplomacy.relations.Nap', '1', 'db_diplomacy_nap_reqStr', '90', '1');
INSERT INTO `diplomacytype` VALUES ('3', 'TRADE_AGREEMENT', 'db_diplomacy_trade', 'db_diplomacy_trade_desc', 'at.darkdestiny.core.diplomacy.relations.Trade', '1', 'db_diplomacy_trade_reqStr', '80', '1');
INSERT INTO `diplomacytype` VALUES ('4', 'EMPIRE_RELATION', 'db_diplomacy_friendly', 'db_diplomacy_friendly_desc', 'at.darkdestiny.core.diplomacy.relations.Friendly', '1', 'db_diplomacy_friendly_reqStr', '70', '1');
INSERT INTO `diplomacytype` VALUES ('5', 'EMPIRE_RELATION', 'db_diplomacy_neutral', 'db_diplomacy_neutral_desc', 'at.darkdestiny.core.diplomacy.relations.Neutral', '1', 'db_diplomacy_neutral_reqStr', '60', '1');
INSERT INTO `diplomacytype` VALUES ('6', 'EMPIRE_RELATION', 'db_diplomacy_aggressive', 'db_diplomacy_aggressive_desc', 'at.darkdestiny.core.diplomacy.relations.Aggressive', '0', 'db_diplomacy_aggressive_reqStr', '50', '1');
INSERT INTO `diplomacytype` VALUES ('7', 'EMPIRE_RELATION', 'db_diplomacy_war', 'db_diplomacy_war_desc', 'at.darkdestiny.core.diplomacy.relations.War', '0', 'db_diplomacy_war_reqStr', '40', '1');
