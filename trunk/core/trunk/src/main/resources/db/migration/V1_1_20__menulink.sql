ALTER TABLE menulink ADD url varchar(200);


UPDATE `menulink` SET url='messages/inbox' WHERE id=120;

DROP TABLE IF EXISTS `styleelement`;
CREATE TABLE `styleelement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `styleId` int(11) NOT NULL DEFAULT '0',
  `clazz` text NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `core` tinyint(3) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`styleId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
-- ----------------------------
-- Records of styleelement
-- ----------------------------
INSERT INTO `styleelement` VALUES ('1', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.MessageSpacer', 'styleelement_messagespacer', 'styleelement_messagespacer_desc', '1', '');
INSERT INTO `styleelement` VALUES ('2', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.MessageBar', 'styleelement_messagebar', 'styleelement_messagebar_desc', '1', '');
INSERT INTO `styleelement` VALUES ('3', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.CreditSpacer', 'styleelement_creditspacer', 'styleelement_creditspacer_desc', '1', 'basic/creditSpacer.xhtml');
INSERT INTO `styleelement` VALUES ('4', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.CreditBar', 'styleelement_creditbar', 'styleelement_creditbar_desc', '1', 'basic/creditBar.xhtml');
INSERT INTO `styleelement` VALUES ('5', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.DevelopementSpacer', 'styleelement_developementspacer', 'styleelement_developementspacer_desc', '1', 'basic/developementSpacer.xhtml');
INSERT INTO `styleelement` VALUES ('6', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.DevelopementBar', 'styleelement_developementbar', 'styleelement_developementbar', '1', 'basic/developementBar.xhtml');
INSERT INTO `styleelement` VALUES ('7', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.EnergySpacer', 'styleelement_energyspacer', 'styleelement_energyspacer_desc', '1', 'basic/energySpacer.xhtml');
INSERT INTO `styleelement` VALUES ('8', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.EnergyBar', 'styleelement_energybar', 'styleelement_energybar_desc', '1', 'basic/energyBar.xhtml');
INSERT INTO `styleelement` VALUES ('9', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.PopulationSpacer', 'styleelement_populationspacer', 'styleelement_populationspacer_desc', '1', 'basic/populationSpacer.xhtml');
INSERT INTO `styleelement` VALUES ('10', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.PopulationBar', 'styleelement_populationbar', 'styleelement_populationbar_desc', '1', 'basic/populationBar.xhtml');
INSERT INTO `styleelement` VALUES ('11', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.FoodSpacer', 'styleelement_foodspacer', 'styleelement_foodspacer_desc', '1', 'basic/foodSpacer.xhtml');
INSERT INTO `styleelement` VALUES ('12', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.FoodBar', 'styleelement_foodbar', 'styleelement_foodbar_desc', '1', 'basic/foodBar.xhtml');
INSERT INTO `styleelement` VALUES ('13', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.RessourceSpacer', 'styleelement_ressourcespacer', 'styleelement_ressourcespacer_desc', '1', 'basic/resourceSpacer.xhtml');
INSERT INTO `styleelement` VALUES ('14', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.RessourceBar', 'styleelement_ressourcebar', 'styleelement_ressourcebar_desc', '1', 'basic/resourceBar.xhtml');
INSERT INTO `styleelement` VALUES ('15', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.RawmaterialBar', 'styleelement_rawmaterialbar', 'styleelement_rawmaterialbar_desc', '0', '');
INSERT INTO `styleelement` VALUES ('16', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.NoteBar_Small', 'styleelement_notebar_small', 'styleelement_notebar_small_desc', '0', '');
INSERT INTO `styleelement` VALUES ('17', '1', 'at.darkdestiny.web.view.menu.statusbar.style.basic.NoteBar_Big', 'styleelement_notebar_big', 'styleelement_notebar_big_desc', '0', '');
