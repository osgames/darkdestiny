DELETE FROM activebonus WHERE bonusId=11;

DROP TABLE IF EXISTS construction;
DROP TABLE IF EXISTS constructionrestriction;
DROP TABLE IF EXISTS bonus;

CREATE TABLE `bonus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `value` double DEFAULT NULL,
  `valueType` enum('DEFINE','INCREASE','DECREASE','INCREASE_PERC','DECREASE_PERC') DEFAULT NULL,
  `bonusType` enum('POPULATION_GROWTH','PRODUCTION','CONSTRUCTION','RESEARCH','TRADE','CREDIT','MINING') DEFAULT NULL,
  `priceIncreasePerUsage` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `planetUnique` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `duration` int(11) DEFAULT NULL,
  `bonusRange` enum('PLANET','EMPIRE') DEFAULT NULL,
  `cost` bigint(20) DEFAULT NULL,
  `reqResearch` int(11) DEFAULT NULL,
  `bonusClass` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

INSERT INTO `bonus` VALUES ('2', 'db_bonus_production_perc_1', 'db_bonus_production_perc_1_desc', '0.2', 'INCREASE_PERC', 'PRODUCTION', '1', '0', '1008', 'PLANET', '10000', '0', null);
INSERT INTO `bonus` VALUES ('3', 'db_bonus_construction_perc_1', 'db_bonus_construction_perc_1_desc', '150', 'INCREASE', 'CONSTRUCTION', '1', '0', '1008', 'PLANET', '10000', '0', null);
INSERT INTO `bonus` VALUES ('4', 'db_bonus_research_perc_1', 'db_bonus_research_perc_1_desc', '0.1', 'INCREASE_PERC', 'RESEARCH', '1', '0', '1008', 'PLANET', '10000', '0', null);
INSERT INTO `bonus` VALUES ('5', 'db_development_industrial', 'db_development_industrial_desc', null, null, null, '0', '1', '0', 'PLANET', '50000', '0', 'IndustrialColony');
INSERT INTO `bonus` VALUES ('6', 'db_development_mining', 'db_development_mining_desc', null, null, null, '0', '1', '0', 'PLANET', '5000', '0', 'MiningColony');
INSERT INTO `bonus` VALUES ('7', 'db_development_agricultural', 'db_development_agricultural_desc', null, null, null, '0', '1', '0', 'PLANET', '35000', '0', 'AgriculturalColony');
INSERT INTO `bonus` VALUES ('8', 'db_development_population', 'db_development_population_desc', null, null, null, '0', '1', '0', 'PLANET', '50000', '0', 'PopulationColony');
INSERT INTO `bonus` VALUES ('9', 'db_development_research', 'db_development_research_desc', null, null, null, '0', '1', '0', 'PLANET', '50000', '0', 'ResearchColony');
INSERT INTO `bonus` VALUES ('10', 'db_development_trade', 'db_development_trade_desc', null, null, null, '0', '1', '0', 'PLANET', '35000', '0', 'TradeColony');
INSERT INTO `bonus` VALUES ('12', 'db_development_administrative', 'db_development_administrative_desc', null, null, null, '0', '1', '0', 'PLANET', '30000', '0', 'AdministrativeColony');
INSERT INTO `bonus` VALUES ('13', 'db_bonus_mining_perc_1', 'db_bonus_mining_perc_1_desc', '0.75', 'INCREASE_PERC', 'MINING', '1', '0', '1008', 'PLANET', '5000', '0', null);

CREATE TABLE `construction` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `infotext` varchar(50) DEFAULT NULL,
  `typplanet` varchar(50) DEFAULT NULL,
  `buildtime` int(11) DEFAULT NULL,
  `type` enum('ADMINISTRATION','ZIVIL','ENERGY','INDUSTRY','MILITARY','PLANETARY_DEFENSE') DEFAULT NULL,
  `orderNo` int(11) DEFAULT NULL,
  `population` bigint(20) DEFAULT NULL,
  `prodType` enum('COMMON','AGRAR','INDUSTRY','ENERGY','RESEARCH') DEFAULT NULL,
  `hp` int(11) DEFAULT NULL,
  `designId` int(11) DEFAULT NULL,
  `workers` bigint(20) DEFAULT NULL,
  `surface` int(11) DEFAULT NULL,
  `uniquePerPlanet` tinyint(3) DEFAULT NULL,
  `uniquePerSystem` tinyint(3) DEFAULT NULL,
  `isMine` int(11) DEFAULT NULL,
  `specialBuildingId` int(11) DEFAULT NULL,
  `minPop` int(11) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `destructible` tinyint(3) DEFAULT NULL,
  `idleable` tinyint(3) DEFAULT NULL,
  `levelable` tinyint(3) DEFAULT '0',
  `econType` enum('NOT_SPECIFIED','INDUSTRY','AGRICULTURE','RESEARCH','MILITARY','RECREATIONAL') NOT NULL DEFAULT 'NOT_SPECIFIED',
  `isVisible` tinyint(3) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `construction` VALUES ('2', 'db_construction_colonybase', 'db_construction_colonybase_desc', 'CEGJLM', '870', 'ADMINISTRATION', '0', '30000', 'COMMON', '5000', '0', '1', '10', '1', '0', '0', '0', '0', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('3', 'db_construction_localadministration', 'db_construction_localadministration_desc', 'CEGJLM', '2233', 'ADMINISTRATION', '10', '0', 'COMMON', '99999', '0', '0', '0', '1', '0', '0', '0', '1000000', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('4', 'db_construction_planetadministration', 'db_construction_planetadministration_desc', 'CEGJLM', '12760', 'ADMINISTRATION', '20', '0', 'COMMON', '99999', '0', '0', '0', '1', '0', '0', '0', '500000000', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('5', 'db_construction_planetgovernment', 'db_construction_planetgovernment_desc', 'CEGJLM', '26100', 'ADMINISTRATION', '30', '0', 'COMMON', '99999', '0', '0', '0', '1', '0', '0', '0', '1500000000', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('12', 'db_construction_hydroponicfarm', 'db_construction_hydroponicfarm_desc', 'EJL', '3190', 'ZIVIL', '40', '0', 'AGRAR', '3000', '0', '5', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('13', 'db_construction_smallagriculturefarm', 'db_construction_smallagriculturefarm_desc', 'CGM', '870', 'ZIVIL', '10', '0', 'AGRAR', '1000', '0', '1', '1', '0', '0', '0', '0', '0', 'smallagriculture.jpg', '1', '1', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('14', 'db_construction_bigagriculturefarm', 'db_construction_bigagriculturefarm_desc', 'CGM', '1450', 'ZIVIL', '20', '0', 'AGRAR', '3000', '0', '3', '3', '0', '0', '0', '0', '0', '', '1', '1', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('15', 'db_construction_agriculturestorage', 'db_construction_agriculturestorage_desc', 'CEGJLM', '2610', 'ZIVIL', '65', '0', 'COMMON', '8000', '0', '100', '10', '0', '0', '0', '0', '0', '', '1', '0', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('16', 'db_construction_agrocomplex', 'db_construction_agrocomplex_desc', 'CGM', '7250', 'ZIVIL', '30', '0', 'AGRAR', '8000', '0', '100', '30', '0', '0', '0', '0', '0', 'agrarcomplex.jpg', '1', '1', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('17', 'db_construction_smallironmine', 'db_construction_smallironmine_desc', 'CEGJLM', '890', 'INDUSTRY', '10', '0', 'INDUSTRY', '1500', '0', '20', '1', '0', '0', '1', '0', '0', 'smallironmine.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('18', 'db_construction_bigironmine', 'db_construction_bigironmine_desc', 'CEGJLM', '1740', 'INDUSTRY', '20', '0', 'INDUSTRY', '4000', '0', '300', '3', '0', '0', '1', '0', '0', 'bigironmine.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('19', 'db_construction_complexironmine', 'db_construction_complexironmine_desc', 'CEGJLM', '3260', 'INDUSTRY', '30', '0', 'INDUSTRY', '9000', '0', '1000', '9', '0', '0', '1', '0', '0', 'ironcomplex.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('20', 'db_construction_steelfactory', 'db_construction_steelfactory_desc', 'CEGJLM', '6230', 'INDUSTRY', '40', '0', 'INDUSTRY', '9000', '0', '500', '5', '0', '0', '0', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('21', 'db_construction_steelcomplex', 'db_construction_steelcomplex_desc', 'CEGJLM', '10230', 'INDUSTRY', '50', '0', 'INDUSTRY', '13000', '0', '1500', '10', '0', '0', '0', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('22', 'db_construction_smallressourcestorage', 'db_construction_smallressourcestorage_desc', 'CEGJLM', '1740', 'INDUSTRY', '60', '0', 'COMMON', '1500', '0', '25', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('23', 'db_construction_bigressourcestorage', 'db_construction_bigressourcestorage_desc', 'CEGJLM', '3190', 'INDUSTRY', '70', '0', 'COMMON', '3000', '0', '125', '18', '0', '0', '0', '0', '0', '', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('24', 'db_construction_nuclearpowerplant', 'db_construction_nuclearpowerplant_desc', 'CEGJLM', '3480', 'ENERGY', '10', '0', 'ENERGY', '3000', '0', '1', '2', '0', '0', '0', '0', '0', 'nuclearplant.jpg', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('25', 'db_construction_fusionpowerplant', 'db_construction_fusionpowerplant_desc', 'CEGJLM', '10150', 'ENERGY', '20', '0', 'ENERGY', '3000', '0', '2', '4', '0', '0', '0', '0', '0', 'fusionplant.jpg', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('26', 'db_construction_steelcompacter', 'db_construction_steelcompacter_desc', 'CEGJLM', '19320', 'INDUSTRY', '52', '0', 'INDUSTRY', '11000', '0', '2000', '10', '0', '0', '0', '0', '0', 'steelcompressor.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('27', 'db_construction_researchlaboratory', 'db_construction_researchlaboratory_desc', 'CEGJLM', '6300', 'ZIVIL', '0', '0', 'RESEARCH', '2500', '0', '4000', '3', '0', '0', '0', '0', '0', 'researchcenter.jpg', '1', '1', '0', 'RESEARCH', '1');
INSERT INTO `construction` VALUES ('28', 'db_construction_planetarylasercannon', 'db_construction_planetarylasercannon_desc', 'CEGJLM', '2150', 'PLANETARY_DEFENSE', '42', '0', 'COMMON', '25000', '9000', '1', '2', '0', '0', '0', '0', '0', 'planetarylaser.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('29', 'db_construction_planetarymisslebase', 'db_construction_planetarymisslebase_desc', 'CEGJLM', '3500', 'PLANETARY_DEFENSE', '44', '0', 'COMMON', '19000', '8999', '1', '2', '0', '0', '0', '0', '0', 'planetaryrocket.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('30', 'db_construction_planetaryshipyard', 'db_construction_planetaryshipyard_desc', 'CEGJLM', '8350', 'MILITARY', '30', '0', 'COMMON', '14000', '0', '5000', '20', '0', '0', '0', '0', '0', 'planetaryshipyard.jpg', '1', '1', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('31', 'db_construction_modulefactory', 'db_construction_modulefactory_desc', 'CEGJLM', '4350', 'MILITARY', '20', '0', 'COMMON', '8000', '0', '3000', '10', '0', '0', '0', '0', '0', 'modulefactory.jpg', '1', '1', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('32', 'db_construction_barracks', 'db_construction_barracks_desc', 'CEGJLM', '1392', 'MILITARY', '10', '0', 'COMMON', '6000', '0', '20', '5', '0', '0', '0', '0', '0', 'barracks.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('33', 'db_construction_orbitalynkeloniummine', 'db_construction_orbitalynkeloniummine_desc', 'AB', '8700', 'INDUSTRY', '58', '0', 'INDUSTRY', '15000', '0', '10', '0', '0', '0', '1', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('34', 'db_construction_orbitalcolony', 'db_construction_orbitalcolony_desc', 'AB', '7830', 'ADMINISTRATION', '40', '30000', 'COMMON', '20000', '0', '0', '0', '1', '0', '0', '0', '0', '', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('35', 'db_construction_orbitalshipyard', 'db_construction_orbitalshipyard_desc', 'CEGJLM', '18250', 'MILITARY', '40', '0', 'COMMON', '15000', '0', '2000', '0', '0', '0', '0', '0', '0', 'orbitalshipyard.jpg', '1', '1', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('36', 'db_construction_orbitalaccomodationunit', 'db_construction_orbitalaccomodationunit_desc', 'ABCEGJLM', '5220', 'ZIVIL', '110', '50000', 'COMMON', '15000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('37', 'db_construction_orbitalressourcestorage', 'db_construction_orbitalressourcestorage_desc', 'ABCEGJLM', '3335', 'INDUSTRY', '75', '0', 'COMMON', '10000', '0', '1', '0', '0', '0', '0', '0', '0', 'orbitalressourcestorage.jpg', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('38', 'db_construction_orbitalhydroponicfarm', 'db_construction_orbitalhydroponicfarm_desc', 'ABCEGJLM', '6090', 'ZIVIL', '50', '0', 'AGRAR', '10000', '0', '5', '0', '0', '0', '0', '0', '0', '', '1', '0', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('39', 'db_construction_howalgoniummine', 'db_construction_howalgoniummine_desc', 'CEGJLM', '11020', 'INDUSTRY', '53', '0', 'INDUSTRY', '3000', '0', '5', '5', '0', '0', '1', '0', '0', 'howalgoniummine.jpg', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('40', 'db_construction_accomodationunit', 'db_construction_accomodationunit_desc', 'EJL', '2030', 'ZIVIL', '60', '100000', 'COMMON', '3500', '0', '0', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('41', 'db_construction_scannerphalanx', 'db_construction_scannerphalanx_desc', 'CEGJLM', '5800', 'MILITARY', '70', '0', 'COMMON', '2000', '0', '5', '2', '1', '0', '0', '0', '0', 'scannerphalanx.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('42', 'db_construction_hyperspacescanner', 'db_construction_hyperspacescanner_desc', 'CEGJLM', '7250', 'MILITARY', '80', '0', 'COMMON', '2500', '0', '1', '5', '1', '0', '0', '0', '0', 'hyperscanner.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('43', 'db_construction_hyperressourcestorage', 'db_construction_hyperressourcestorage_desc', 'CEGJLM', '4060', 'INDUSTRY', '80', '0', 'COMMON', '1500', '0', '50', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('44', 'db_construction_orbitalhyperressourcestorage', 'db_construction_orbitalhyperressourcestorage_desc', 'ABCEGJLM', '3480', 'INDUSTRY', '90', '0', 'COMMON', '12000', '0', '1', '0', '0', '0', '0', '0', '0', 'orbitalhyperressourcestorage.jpg', '1', '0', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('45', 'db_construction_orbitalhowalgoniummine', 'db_construction_orbitalhowalgoniummine_desc', 'AB', '8555', 'INDUSTRY', '57', '0', 'INDUSTRY', '15000', '0', '10', '0', '0', '0', '1', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('46', 'db_construction_planetarypositroncomputer', 'db_construction_planetarypositroncomputer_desc', 'CEGJLM', '67300', 'ZIVIL', '1', '0', 'RESEARCH', '20000', '0', '500', '20', '1', '1', '0', '0', '1500000000', '', '1', '1', '0', 'RESEARCH', '1');
INSERT INTO `construction` VALUES ('47', 'db_construction_planetaryintervalcannon', 'db_construction_planetaryintervalcannon_desc', 'CEGJLM', '8555', 'PLANETARY_DEFENSE', '46', '0', 'COMMON', '35000', '9001', '1', '2', '0', '0', '0', '0', '0', 'plasmacannon.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('48', 'db_construction_orbitalfoodstorage', 'db_construction_orbitalfoodstorage_desc', 'ABCEGJLM', '3335', 'ZIVIL', '70', '0', 'COMMON', '10000', '0', '1', '0', '0', '0', '0', '0', '0', '', '1', '0', '0', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('49', 'db_construction_CVEmbinium_facility', 'db_construction_CVEmbinium_facility_desc', 'CEGJLM', '198200', 'INDUSTRY', '59', '0', 'INDUSTRY', '26000', '0', '1500', '30', '0', '0', '0', '0', '0', '', '1', '1', '0', 'INDUSTRY', '1');
INSERT INTO `construction` VALUES ('50', 'db_construction_militaryoutpost', 'db_construction_militaryoutpost_desc', 'CEGJLM', '98700', 'MILITARY', '105', '0', 'COMMON', '300000', '0', '10000', '250', '1', '1', '0', '0', '0', '', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('61', 'db_construction_observatory', 'db_construction_observatory_desc', 'CEGJLM', '4640', 'ADMINISTRATION', '60', '0', 'COMMON', '5000', '0', '1', '2', '1', '0', '0', '3', '0', '', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('80', 'db_construction_megacity', 'db_construction_megacity_desc', 'CGM', '65000', 'ZIVIL', '5', '200000000', 'COMMON', '50000', '0', '0', '100', '0', '0', '0', '0', '0', 'megacity.png', '0', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('81', 'db_construction_consumables_industry', 'db_construction_consumables_industry_desc', 'CEGJLM', '11000', 'INDUSTRY', '100', '0', 'INDUSTRY', '13000', '0', '2000', '10', '0', '0', '0', '0', '0', '', '1', '0', '0', 'INDUSTRY', '0');
INSERT INTO `construction` VALUES ('82', 'db_construction_scientific_academy', 'db_construction_scientific_academy_desc', 'CGM', '32000', 'ZIVIL', '32', '0', 'RESEARCH', '7000', '0', '10000', '10', '1', '1', '0', '0', '0', '', '1', '0', '0', 'RESEARCH', '1');
INSERT INTO `construction` VALUES ('83', 'db_construction_planetary_fortress', 'db_contruction_planetary_fortress_desc', 'CEGJLM', '55000', 'MILITARY', '106', '0', 'COMMON', '150000', '0', '100', '5', '0', '0', '0', '0', '0', '', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('84', 'db_construction_agri_distributioncenter', 'db_construction_agri_distributioncenter_desc', 'CGM', '45000', 'ZIVIL', '35', '0', 'AGRAR', '10000', '0', '3000', '30', '1', '0', '0', '0', '0', '', '1', '0', '1', 'AGRICULTURE', '1');
INSERT INTO `construction` VALUES ('90', 'db_construction_leisurefacilities', 'db_construction_leisurefacilities_desc', 'M', '1170', 'ZIVIL', '120', '0', 'COMMON', '1000', '0', '50', '50', '0', '0', '0', '0', '0', 'recreational.jpg', '1', '0', '0', 'RECREATIONAL', '1');
INSERT INTO `construction` VALUES ('100', 'db_construction_civilspaceport', 'db_construction_civilspaceport_desc', 'CEGJLM', '4350', 'ZIVIL', '130', '0', 'COMMON', '8000', '0', '1000', '10', '0', '0', '0', '0', '0', '', '1', '1', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('101', 'db_construction_interstellartradingpost', 'db_construction_interstellartradingpost_desc', 'CEGJLM', '5510', 'ZIVIL', '80', '0', 'COMMON', '6000', '0', '5000', '10', '1', '0', '0', '1', '0', '', '1', '0', '1', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('103', 'db_construction_transmitter', 'db_construction_transmitter_desc', 'CEGJLM', '32150', 'INDUSTRY', '110', '0', 'COMMON', '9000', '0', '750', '15', '0', '0', '0', '0', '0', '', '1', '1', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('200', 'db_construction_planetaryhueshield', 'db_construction_planetaryhueshield_desc', 'CEGJLM', '68950', 'MILITARY', '90', '0', 'COMMON', '120000', '0', '250', '120', '1', '0', '0', '0', '0', 'schirmhue.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('201', 'db_construction_planetaryparatronshield', 'db_construction_planetaryparatronshield_desc', 'CEGJLM', '95300', 'MILITARY', '100', '0', 'COMMON', '150000', '0', '250', '120', '1', '0', '0', '0', '0', 'schirmparatron.jpg', '1', '0', '0', 'MILITARY', '1');
INSERT INTO `construction` VALUES ('202', 'db_construction_hypertrop', 'db_construction_hypertrop_desc', 'CEGJLM', '123400', 'ENERGY', '30', '0', 'ENERGY', '115000', '0', '1500', '150', '1', '0', '0', '0', '0', 'sonnenzapfer.jpg', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('300', 'db_construction_solarplanet', 'db_construction_solarplanet_desc', 'JCEGLM', '1950', 'ENERGY', '15', '0', 'ENERGY', '1000', '0', '1', '40', '0', '0', '0', '0', '0', 'solarplant.jpg', '1', '0', '0', 'NOT_SPECIFIED', '1');
INSERT INTO `construction` VALUES ('1000', 'db_construction_transmittercontroller', 'db_construction_transmittercontroller_desc', 'CEGJLM', '0', 'ADMINISTRATION', '0', '0', 'COMMON', '0', '0', '0', '20', '1', '1', '0', '0', '0', '', '0', '0', '0', 'NOT_SPECIFIED', '0');
INSERT INTO `construction` VALUES ('1001', 'db_construction_transmitterfortress', 'db_construction_transmitterfortress_desc', 'CEGJLM', '0', 'PLANETARY_DEFENSE', '0', '0', 'COMMON', '200000', '0', '0', '10', '0', '0', '0', '0', '0', '', '1', '0', '0', 'MILITARY', '0');

CREATE TABLE `constructionrestriction` (
  `type` int(11) NOT NULL DEFAULT '0',
  `consId` int(11) NOT NULL DEFAULT '0',
  `restrictionClass` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`type`,`consId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `constructionrestriction` VALUES ('0', '2', 'ColonyBase');
INSERT INTO `constructionrestriction` VALUES ('0', '3', 'LocalAdministration');
INSERT INTO `constructionrestriction` VALUES ('0', '4', 'PlanetaryAdministration');
INSERT INTO `constructionrestriction` VALUES ('0', '5', 'PlanetaryGovernment');
INSERT INTO `constructionrestriction` VALUES ('0', '46', 'PlanetarySupercomputer');
INSERT INTO `constructionrestriction` VALUES ('0', '61', 'Observatory');
INSERT INTO `constructionrestriction` VALUES ('0', '80', 'Megacity');
INSERT INTO `constructionrestriction` VALUES ('0', '84', 'AgriculturalDistributionCenter');
INSERT INTO `constructionrestriction` VALUES ('0', '101', 'RTradePost');
INSERT INTO `constructionrestriction` VALUES ('0', '200', 'RPlanetaryHUEShield');

DROP TABLE IF EXISTS `techrelation`;
CREATE TABLE `techrelation` (
  `sourceID` int(11) NOT NULL DEFAULT '0',
  `reqConstructionID` int(11) NOT NULL DEFAULT '0',
  `reqResearchID` int(11) NOT NULL DEFAULT '0',
  `treeType` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sourceID`,`reqConstructionID`,`reqResearchID`,`treeType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `techrelation` VALUES ('1', '0', '27', '1');
INSERT INTO `techrelation` VALUES ('4', '0', '28', '1');
INSERT INTO `techrelation` VALUES ('5', '0', '4', '1');
INSERT INTO `techrelation` VALUES ('6', '0', '5', '1');
INSERT INTO `techrelation` VALUES ('6', '0', '32', '2');
INSERT INTO `techrelation` VALUES ('7', '0', '6', '1');
INSERT INTO `techrelation` VALUES ('7', '0', '24', '1');
INSERT INTO `techrelation` VALUES ('7', '0', '48', '2');
INSERT INTO `techrelation` VALUES ('8', '0', '6', '1');
INSERT INTO `techrelation` VALUES ('9', '0', '8', '1');
INSERT INTO `techrelation` VALUES ('9', '0', '22', '2');
INSERT INTO `techrelation` VALUES ('10', '0', '85', '2');
INSERT INTO `techrelation` VALUES ('11', '0', '10', '1');
INSERT INTO `techrelation` VALUES ('14', '0', '37', '1');
INSERT INTO `techrelation` VALUES ('15', '0', '16', '1');
INSERT INTO `techrelation` VALUES ('16', '0', '19', '1');
INSERT INTO `techrelation` VALUES ('19', '0', '17', '1');
INSERT INTO `techrelation` VALUES ('19', '0', '18', '1');
INSERT INTO `techrelation` VALUES ('19', '0', '116', '2');
INSERT INTO `techrelation` VALUES ('20', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('21', '0', '9', '2');
INSERT INTO `techrelation` VALUES ('21', '0', '20', '1');
INSERT INTO `techrelation` VALUES ('22', '0', '24', '1');
INSERT INTO `techrelation` VALUES ('22', '0', '58', '2');
INSERT INTO `techrelation` VALUES ('23', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('24', '0', '3', '1');
INSERT INTO `techrelation` VALUES ('25', '0', '22', '1');
INSERT INTO `techrelation` VALUES ('25', '0', '41', '0');
INSERT INTO `techrelation` VALUES ('26', '0', '7', '0');
INSERT INTO `techrelation` VALUES ('26', '0', '25', '1');
INSERT INTO `techrelation` VALUES ('28', '0', '3', '1');
INSERT INTO `techrelation` VALUES ('28', '0', '42', '0');
INSERT INTO `techrelation` VALUES ('29', '0', '15', '0');
INSERT INTO `techrelation` VALUES ('29', '0', '31', '1');
INSERT INTO `techrelation` VALUES ('30', '0', '30', '0');
INSERT INTO `techrelation` VALUES ('31', '0', '26', '1');
INSERT INTO `techrelation` VALUES ('32', '0', '31', '1');
INSERT INTO `techrelation` VALUES ('33', '0', '31', '1');
INSERT INTO `techrelation` VALUES ('33', '0', '32', '1');
INSERT INTO `techrelation` VALUES ('33', '0', '51', '0');
INSERT INTO `techrelation` VALUES ('33', '0', '69', '0');
INSERT INTO `techrelation` VALUES ('34', '0', '29', '1');
INSERT INTO `techrelation` VALUES ('34', '0', '66', '0');
INSERT INTO `techrelation` VALUES ('35', '0', '29', '1');
INSERT INTO `techrelation` VALUES ('35', '0', '67', '0');
INSERT INTO `techrelation` VALUES ('36', '0', '11', '1');
INSERT INTO `techrelation` VALUES ('36', '0', '68', '0');
INSERT INTO `techrelation` VALUES ('37', '0', '11', '1');
INSERT INTO `techrelation` VALUES ('37', '0', '22', '1');
INSERT INTO `techrelation` VALUES ('37', '0', '66', '0');
INSERT INTO `techrelation` VALUES ('38', '0', '37', '1');
INSERT INTO `techrelation` VALUES ('38', '0', '82', '0');
INSERT INTO `techrelation` VALUES ('39', '0', '14', '1');
INSERT INTO `techrelation` VALUES ('39', '0', '79', '0');
INSERT INTO `techrelation` VALUES ('40', '0', '20', '1');
INSERT INTO `techrelation` VALUES ('40', '0', '95', '3');
INSERT INTO `techrelation` VALUES ('41', '0', '21', '1');
INSERT INTO `techrelation` VALUES ('42', '0', '37', '1');
INSERT INTO `techrelation` VALUES ('42', '0', '78', '0');
INSERT INTO `techrelation` VALUES ('43', '0', '40', '1');
INSERT INTO `techrelation` VALUES ('43', '0', '73', '0');
INSERT INTO `techrelation` VALUES ('44', '0', '22', '1');
INSERT INTO `techrelation` VALUES ('44', '0', '40', '1');
INSERT INTO `techrelation` VALUES ('44', '0', '73', '0');
INSERT INTO `techrelation` VALUES ('45', '0', '44', '1');
INSERT INTO `techrelation` VALUES ('45', '0', '74', '0');
INSERT INTO `techrelation` VALUES ('46', '0', '32', '1');
INSERT INTO `techrelation` VALUES ('46', '0', '45', '1');
INSERT INTO `techrelation` VALUES ('46', '0', '139', '0');
INSERT INTO `techrelation` VALUES ('47', '0', '41', '1');
INSERT INTO `techrelation` VALUES ('47', '0', '143', '0');
INSERT INTO `techrelation` VALUES ('48', '0', '32', '1');
INSERT INTO `techrelation` VALUES ('48', '0', '47', '1');
INSERT INTO `techrelation` VALUES ('48', '0', '65', '0');
INSERT INTO `techrelation` VALUES ('49', '0', '31', '1');
INSERT INTO `techrelation` VALUES ('49', '0', '151', '0');
INSERT INTO `techrelation` VALUES ('50', '0', '6', '1');
INSERT INTO `techrelation` VALUES ('50', '0', '56', '2');
INSERT INTO `techrelation` VALUES ('51', '0', '5', '1');
INSERT INTO `techrelation` VALUES ('51', '0', '50', '1');
INSERT INTO `techrelation` VALUES ('51', '0', '107', '2');
INSERT INTO `techrelation` VALUES ('52', '0', '50', '1');
INSERT INTO `techrelation` VALUES ('52', '0', '112', '2');
INSERT INTO `techrelation` VALUES ('53', '0', '52', '1');
INSERT INTO `techrelation` VALUES ('54', '0', '50', '1');
INSERT INTO `techrelation` VALUES ('55', '0', '54', '1');
INSERT INTO `techrelation` VALUES ('56', '0', '53', '1');
INSERT INTO `techrelation` VALUES ('57', '0', '51', '1');
INSERT INTO `techrelation` VALUES ('58', '0', '57', '1');
INSERT INTO `techrelation` VALUES ('59', '0', '8', '1');
INSERT INTO `techrelation` VALUES ('59', '0', '33', '1');
INSERT INTO `techrelation` VALUES ('59', '0', '53', '1');
INSERT INTO `techrelation` VALUES ('60', '0', '59', '1');
INSERT INTO `techrelation` VALUES ('60', '0', '77', '2');
INSERT INTO `techrelation` VALUES ('61', '0', '60', '1');
INSERT INTO `techrelation` VALUES ('61', '0', '124', '2');
INSERT INTO `techrelation` VALUES ('62', '0', '61', '1');
INSERT INTO `techrelation` VALUES ('63', '0', '8', '1');
INSERT INTO `techrelation` VALUES ('63', '0', '65', '1');
INSERT INTO `techrelation` VALUES ('64', '0', '63', '1');
INSERT INTO `techrelation` VALUES ('66', '0', '68', '1');
INSERT INTO `techrelation` VALUES ('66', '0', '82', '1');
INSERT INTO `techrelation` VALUES ('67', '0', '65', '1');
INSERT INTO `techrelation` VALUES ('68', '0', '65', '1');
INSERT INTO `techrelation` VALUES ('69', '0', '57', '1');
INSERT INTO `techrelation` VALUES ('69', '0', '65', '1');
INSERT INTO `techrelation` VALUES ('70', '0', '53', '3');
INSERT INTO `techrelation` VALUES ('70', '0', '93', '1');
INSERT INTO `techrelation` VALUES ('71', '0', '93', '1');
INSERT INTO `techrelation` VALUES ('72', '0', '54', '1');
INSERT INTO `techrelation` VALUES ('73', '0', '5', '1');
INSERT INTO `techrelation` VALUES ('73', '0', '72', '1');
INSERT INTO `techrelation` VALUES ('74', '0', '73', '1');
INSERT INTO `techrelation` VALUES ('75', '0', '74', '1');
INSERT INTO `techrelation` VALUES ('76', '0', '71', '1');
INSERT INTO `techrelation` VALUES ('77', '0', '90', '1');
INSERT INTO `techrelation` VALUES ('77', '0', '91', '1');
INSERT INTO `techrelation` VALUES ('77', '0', '108', '1');
INSERT INTO `techrelation` VALUES ('78', '0', '105', '1');
INSERT INTO `techrelation` VALUES ('79', '0', '74', '1');
INSERT INTO `techrelation` VALUES ('80', '0', '6', '0');
INSERT INTO `techrelation` VALUES ('80', '0', '43', '2');
INSERT INTO `techrelation` VALUES ('80', '0', '48', '1');
INSERT INTO `techrelation` VALUES ('80', '0', '105', '1');
INSERT INTO `techrelation` VALUES ('81', '0', '76', '1');
INSERT INTO `techrelation` VALUES ('81', '0', '80', '1');
INSERT INTO `techrelation` VALUES ('81', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('82', '0', '65', '1');
INSERT INTO `techrelation` VALUES ('82', '0', '81', '2');
INSERT INTO `techrelation` VALUES ('83', '0', '33', '1');
INSERT INTO `techrelation` VALUES ('83', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('83', '50', '0', '0');
INSERT INTO `techrelation` VALUES ('84', '0', '80', '2');
INSERT INTO `techrelation` VALUES ('85', '0', '86', '1');
INSERT INTO `techrelation` VALUES ('85', '0', '109', '2');
INSERT INTO `techrelation` VALUES ('86', '0', '40', '1');
INSERT INTO `techrelation` VALUES ('87', '0', '85', '1');
INSERT INTO `techrelation` VALUES ('88', '0', '1', '1');
INSERT INTO `techrelation` VALUES ('89', '0', '88', '1');
INSERT INTO `techrelation` VALUES ('90', '0', '51', '1');
INSERT INTO `techrelation` VALUES ('90', '0', '154', '2');
INSERT INTO `techrelation` VALUES ('91', '0', '53', '1');
INSERT INTO `techrelation` VALUES ('92', '0', '97', '1');
INSERT INTO `techrelation` VALUES ('93', '0', '55', '1');
INSERT INTO `techrelation` VALUES ('94', '0', '92', '1');
INSERT INTO `techrelation` VALUES ('95', '0', '94', '1');
INSERT INTO `techrelation` VALUES ('96', '0', '54', '1');
INSERT INTO `techrelation` VALUES ('97', '0', '96', '1');
INSERT INTO `techrelation` VALUES ('98', '0', '19', '1');
INSERT INTO `techrelation` VALUES ('98', '0', '92', '1');
INSERT INTO `techrelation` VALUES ('98', '0', '100', '1');
INSERT INTO `techrelation` VALUES ('99', '0', '98', '1');
INSERT INTO `techrelation` VALUES ('100', '0', '18', '1');
INSERT INTO `techrelation` VALUES ('101', '0', '70', '1');
INSERT INTO `techrelation` VALUES ('101', '0', '88', '1');
INSERT INTO `techrelation` VALUES ('102', '0', '70', '1');
INSERT INTO `techrelation` VALUES ('103', '0', '70', '1');
INSERT INTO `techrelation` VALUES ('103', '0', '115', '0');
INSERT INTO `techrelation` VALUES ('104', '0', '93', '1');
INSERT INTO `techrelation` VALUES ('105', '0', '74', '1');
INSERT INTO `techrelation` VALUES ('105', '0', '104', '1');
INSERT INTO `techrelation` VALUES ('105', '0', '141', '1');
INSERT INTO `techrelation` VALUES ('106', '0', '102', '1');
INSERT INTO `techrelation` VALUES ('106', '0', '105', '1');
INSERT INTO `techrelation` VALUES ('107', '0', '105', '1');
INSERT INTO `techrelation` VALUES ('108', '0', '105', '1');
INSERT INTO `techrelation` VALUES ('109', '0', '91', '1');
INSERT INTO `techrelation` VALUES ('109', '0', '92', '1');
INSERT INTO `techrelation` VALUES ('109', '0', '118', '1');
INSERT INTO `techrelation` VALUES ('109', '0', '135', '1');
INSERT INTO `techrelation` VALUES ('110', '0', '130', '1');
INSERT INTO `techrelation` VALUES ('110', '0', '150', '1');
INSERT INTO `techrelation` VALUES ('111', '0', '117', '1');
INSERT INTO `techrelation` VALUES ('112', '0', '111', '1');
INSERT INTO `techrelation` VALUES ('112', '0', '131', '1');
INSERT INTO `techrelation` VALUES ('113', '0', '111', '1');
INSERT INTO `techrelation` VALUES ('114', '0', '113', '1');
INSERT INTO `techrelation` VALUES ('115', '0', '114', '1');
INSERT INTO `techrelation` VALUES ('116', '0', '115', '1');
INSERT INTO `techrelation` VALUES ('117', '0', '108', '1');
INSERT INTO `techrelation` VALUES ('118', '0', '123', '1');
INSERT INTO `techrelation` VALUES ('119', '0', '131', '1');
INSERT INTO `techrelation` VALUES ('120', '0', '119', '1');
INSERT INTO `techrelation` VALUES ('120', '0', '121', '1');
INSERT INTO `techrelation` VALUES ('121', '0', '80', '1');
INSERT INTO `techrelation` VALUES ('121', '0', '86', '1');
INSERT INTO `techrelation` VALUES ('122', '0', '93', '1');
INSERT INTO `techrelation` VALUES ('122', '0', '139', '1');
INSERT INTO `techrelation` VALUES ('123', '0', '105', '1');
INSERT INTO `techrelation` VALUES ('123', '0', '117', '1');
INSERT INTO `techrelation` VALUES ('123', '0', '122', '1');
INSERT INTO `techrelation` VALUES ('124', '0', '123', '1');
INSERT INTO `techrelation` VALUES ('125', '0', '126', '1');
INSERT INTO `techrelation` VALUES ('126', '0', '123', '1');
INSERT INTO `techrelation` VALUES ('127', '0', '126', '1');
INSERT INTO `techrelation` VALUES ('128', '0', '84', '1');
INSERT INTO `techrelation` VALUES ('128', '0', '89', '1');
INSERT INTO `techrelation` VALUES ('129', '0', '94', '1');
INSERT INTO `techrelation` VALUES ('129', '0', '128', '1');
INSERT INTO `techrelation` VALUES ('130', '0', '123', '1');
INSERT INTO `techrelation` VALUES ('130', '0', '131', '1');
INSERT INTO `techrelation` VALUES ('131', '0', '105', '1');
INSERT INTO `techrelation` VALUES ('131', '0', '108', '1');
INSERT INTO `techrelation` VALUES ('131', '0', '139', '1');
INSERT INTO `techrelation` VALUES ('134', '0', '130', '1');
INSERT INTO `techrelation` VALUES ('134', '0', '150', '1');
INSERT INTO `techrelation` VALUES ('135', '0', '117', '1');
INSERT INTO `techrelation` VALUES ('135', '0', '131', '1');
INSERT INTO `techrelation` VALUES ('139', '0', '74', '1');
INSERT INTO `techrelation` VALUES ('139', '0', '99', '1');
INSERT INTO `techrelation` VALUES ('141', '0', '103', '1');
INSERT INTO `techrelation` VALUES ('142', '0', '119', '1');
INSERT INTO `techrelation` VALUES ('143', '0', '142', '1');
INSERT INTO `techrelation` VALUES ('144', '0', '146', '1');
INSERT INTO `techrelation` VALUES ('145', '0', '112', '1');
INSERT INTO `techrelation` VALUES ('145', '0', '146', '1');
INSERT INTO `techrelation` VALUES ('146', '0', '107', '1');
INSERT INTO `techrelation` VALUES ('147', '0', '53', '1');
INSERT INTO `techrelation` VALUES ('148', '0', '131', '1');
INSERT INTO `techrelation` VALUES ('149', '0', '130', '1');
INSERT INTO `techrelation` VALUES ('150', '0', '123', '1');
INSERT INTO `techrelation` VALUES ('151', '0', '150', '1');
INSERT INTO `techrelation` VALUES ('152', '0', '119', '1');
INSERT INTO `techrelation` VALUES ('152', '0', '130', '1');
INSERT INTO `techrelation` VALUES ('152', '0', '150', '1');
INSERT INTO `techrelation` VALUES ('152', '0', '153', '1');
INSERT INTO `techrelation` VALUES ('153', '0', '123', '1');
INSERT INTO `techrelation` VALUES ('154', '0', '53', '1');
INSERT INTO `techrelation` VALUES ('200', '0', '36', '2');
INSERT INTO `techrelation` VALUES ('200', '0', '144', '0');
INSERT INTO `techrelation` VALUES ('201', '0', '38', '2');
INSERT INTO `techrelation` VALUES ('201', '0', '145', '0');
INSERT INTO `techrelation` VALUES ('202', '0', '125', '0');
INSERT INTO `techrelation` VALUES ('202', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('203', '0', '38', '2');
INSERT INTO `techrelation` VALUES ('300', '0', '25', '2');
INSERT INTO `techrelation` VALUES ('301', '0', '26', '2');
INSERT INTO `techrelation` VALUES ('310', '0', '33', '2');
INSERT INTO `techrelation` VALUES ('315', '0', '59', '2');
INSERT INTO `techrelation` VALUES ('320', '0', '60', '2');
INSERT INTO `techrelation` VALUES ('325', '0', '63', '2');
INSERT INTO `techrelation` VALUES ('330', '0', '64', '2');
INSERT INTO `techrelation` VALUES ('340', '0', '61', '2');
INSERT INTO `techrelation` VALUES ('350', '0', '62', '2');
INSERT INTO `techrelation` VALUES ('351', '0', '31', '2');
INSERT INTO `techrelation` VALUES ('400', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('401', '0', '121', '2');
INSERT INTO `techrelation` VALUES ('402', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('403', '0', '152', '2');
INSERT INTO `techrelation` VALUES ('404', '0', '119', '2');
INSERT INTO `techrelation` VALUES ('405', '0', '120', '2');
INSERT INTO `techrelation` VALUES ('500', '0', '44', '2');
INSERT INTO `techrelation` VALUES ('501', '0', '46', '2');
INSERT INTO `techrelation` VALUES ('502', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('503', '0', '46', '2');
INSERT INTO `techrelation` VALUES ('600', '0', '39', '2');
INSERT INTO `techrelation` VALUES ('601', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('700', '0', '101', '2');
INSERT INTO `techrelation` VALUES ('701', '0', '110', '2');
INSERT INTO `techrelation` VALUES ('800', '0', '92', '2');
INSERT INTO `techrelation` VALUES ('801', '0', '17', '2');
INSERT INTO `techrelation` VALUES ('802', '0', '134', '2');
INSERT INTO `techrelation` VALUES ('900', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('1000', '0', '34', '2');
INSERT INTO `techrelation` VALUES ('1010', '0', '35', '2');
INSERT INTO `techrelation` VALUES ('1020', '0', '49', '2');
INSERT INTO `techrelation` VALUES ('1030', '0', '83', '2');
INSERT INTO `techrelation` VALUES ('1031', '0', '83', '2');
INSERT INTO `techrelation` VALUES ('1032', '0', '83', '2');
INSERT INTO `techrelation` VALUES ('1033', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('1034', '0', '999', '2');
INSERT INTO `techrelation` VALUES ('1035', '0', '999', '2');

INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '50', '1', '230000');
INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '50', '2', '1275000');
INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '50', '3', '610000');
INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '50', '20', '231500000');
INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '50', '99', '35000');

INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '83', '2', '390000');
INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '83', '3', '210000');
INSERT IGNORE INTO `ressourcecost` VALUES ('CONSTRUCTION', '83', '20', '98100000');