INSERT IGNORE INTO techrelation SET sourceID=80, reqConstructionID=0, reqResearchId=6, treeType=0;

DROP TABLE IF EXISTS `techtype`;
CREATE TABLE `techtype` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `refDAO` varchar(50) DEFAULT NULL,
  `refModel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of techtype
-- ----------------------------
INSERT INTO `techtype` VALUES ('0', 'db_researchtype_construction', 'at.darkdestiny.core.dao.ConstructionDAO', 'at.darkdestiny.core.model.Construction');
INSERT INTO `techtype` VALUES ('1', 'db_researchtype_research', 'at.darkdestiny.core.dao.ResearchDAO', 'at.darkdestiny.core.model.Research');
INSERT INTO `techtype` VALUES ('2', 'db_researchtype_module', 'at.darkdestiny.core.dao.ModuleDAO', 'at.darkdestiny.core.model.Module');
INSERT INTO `techtype` VALUES ('3', 'db_researchtype_groundtroop', 'at.darkdestiny.core.dao.GroundTroopDAO', 'at.darkdestiny.core.model.GroundTroop');
INSERT INTO `techtype` VALUES ('4', 'db_researchtype_improvement', 'at.darkdestiny.core.dao.ImprovementDAO', 'at.darkdestiny.core.model.Improvement');
INSERT INTO `techtype` VALUES ('5', 'db_researchtype_ressource', 'at.darkdestiny.core.dao.RessourceDAO', 'at.darkdestiny.core.model.Ressource');
