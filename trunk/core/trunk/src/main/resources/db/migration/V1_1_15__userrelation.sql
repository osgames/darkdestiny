DROP TABLE IF EXISTS `userrelation`;
CREATE TABLE `userrelation` (
  `userId1` mediumint(9) NOT NULL,
  `userId2` mediumint(9) NOT NULL,
  `relationValue` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId1`,`userId2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;