
DROP TABLE IF EXISTS `scrapresource`;
CREATE TABLE `scrapresource` (
  `planetId` int(11) DEFAULT NULL,
  `systemId` int(11) DEFAULT NULL,
  `locationId` int(11) NOT NULL DEFAULT '0',
  `locationType` enum('PLANET','SYSTEM') NOT NULL,
  `resourceId` int(11) NOT NULL DEFAULT '0',
  `quantity` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`locationId`,`locationType`,`resourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
