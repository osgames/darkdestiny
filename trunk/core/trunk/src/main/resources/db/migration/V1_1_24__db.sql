UPDATE construction SET typplanet='M' WHERE id=84;

DROP TABLE IF EXISTS `bonus`;
CREATE TABLE `bonus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `value` double DEFAULT NULL,
  `valueType` enum('DEFINE','INCREASE','DECREASE','INCREASE_PERC','DECREASE_PERC') DEFAULT NULL,
  `bonusType` enum('POPULATION_GROWTH','PRODUCTION','CONSTRUCTION','RESEARCH','TRADE','CREDIT','MINING') DEFAULT NULL,
  `priceIncreasePerUsage` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `planetUnique` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `duration` int(11) DEFAULT NULL,
  `bonusRange` enum('PLANET','EMPIRE') DEFAULT NULL,
  `cost` bigint(20) DEFAULT NULL,
  `reqResearch` int(11) DEFAULT NULL,
  `bonusClass` varchar(50) DEFAULT NULL,
  `enabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bonus
-- ----------------------------
INSERT INTO `bonus` VALUES ('2', 'db_bonus_production_perc_1', 'db_bonus_production_perc_1_desc', '0.2', 'INCREASE_PERC', 'PRODUCTION', '1', '0', '1008', 'PLANET', '10000', '0', null, '1');
INSERT INTO `bonus` VALUES ('3', 'db_bonus_construction_perc_1', 'db_bonus_construction_perc_1_desc', '150', 'INCREASE', 'CONSTRUCTION', '1', '0', '1008', 'PLANET', '10000', '0', null, '1');
INSERT INTO `bonus` VALUES ('4', 'db_bonus_research_perc_1', 'db_bonus_research_perc_1_desc', '0.1', 'INCREASE_PERC', 'RESEARCH', '1', '0', '1008', 'PLANET', '10000', '0', null, '1');
INSERT INTO `bonus` VALUES ('5', 'db_development_industrial', 'db_development_industrial_desc', null, null, null, '0', '1', '0', 'PLANET', '50000', '0', 'IndustrialColony', '1');
INSERT INTO `bonus` VALUES ('6', 'db_development_mining', 'db_development_mining_desc', null, null, null, '0', '1', '0', 'PLANET', '5000', '0', 'MiningColony', '0');
INSERT INTO `bonus` VALUES ('7', 'db_development_agricultural', 'db_development_agricultural_desc', null, null, null, '0', '1', '0', 'PLANET', '35000', '0', 'AgriculturalColony', '1');
INSERT INTO `bonus` VALUES ('8', 'db_development_population', 'db_development_population_desc', null, null, null, '0', '1', '0', 'PLANET', '50000', '0', 'PopulationColony', '0');
INSERT INTO `bonus` VALUES ('9', 'db_development_research', 'db_development_research_desc', null, null, null, '0', '1', '0', 'PLANET', '50000', '0', 'ResearchColony', '0');
INSERT INTO `bonus` VALUES ('10', 'db_development_trade', 'db_development_trade_desc', null, null, null, '0', '1', '0', 'PLANET', '35000', '0', 'TradeColony', '1');
INSERT INTO `bonus` VALUES ('12', 'db_development_administrative', 'db_development_administrative_desc', null, null, null, '0', '1', '0', 'PLANET', '30000', '0', 'AdministrativeColony', '0');
INSERT INTO `bonus` VALUES ('13', 'db_bonus_mining_perc_1', 'db_bonus_mining_perc_1_desc', '0.75', 'INCREASE_PERC', 'MINING', '1', '0', '1008', 'PLANET', '5000', '0', null, '1');
