/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.TechTypeDAO;
import at.darkdestiny.core.enumeration.ETechListType;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class TechListView {

    private final TreeMap<ETechListType, ArrayList<TechListViewEntry>> list = new TreeMap<ETechListType, ArrayList<TechListViewEntry>>();
    private HashMap<ETechListType, String> typeToName = new HashMap<ETechListType, String>();
    public static TechTypeDAO techTypeDAO = (TechTypeDAO) DAOFactory.get(TechTypeDAO.class);
    private final TreeMap<ETechListType, TreeMap<String, TechListViewEntry>> sortedList = new TreeMap<ETechListType, TreeMap<String, TechListViewEntry>>();
 private int userId;

    public TechListView(int userId) {
        this.userId = userId;
    }

    public void addTechListViewEntry(TechListViewEntry tlve) {

        String techTypeName = typeToName.get(tlve.getTechType());
        if (techTypeName == null) {
            techTypeName = ML.getMLStr(tlve.getTechType().toString(), userId);
            typeToName.put(tlve.getTechType(), techTypeName);
        }
        TreeMap<String, TechListViewEntry> results = sortedList.get(tlve.getTechType());
        if (results == null) {
            results = new TreeMap<String, TechListViewEntry>();
        }

        results.put(tlve.getName(), tlve);
        sortedList.put(tlve.getTechType(), results);

    }

    /**
     * @return the list
     */
    public TreeMap<ETechListType, ArrayList<TechListViewEntry>> getList() {
        for (Map.Entry<ETechListType, TreeMap<String, TechListViewEntry>> entry : sortedList.entrySet()) {
            for (Map.Entry<String, TechListViewEntry> entry1 : entry.getValue().entrySet()) {
                ArrayList<TechListViewEntry> tlves = list.get(entry.getKey());
                if (tlves == null) {
                    tlves = new ArrayList<TechListViewEntry>();
                }
                tlves.add(entry1.getValue());
                list.put(entry.getKey(), tlves);
            }

        }
        return list;
    }
}