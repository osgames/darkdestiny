/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.update.transport;

/**
 *
 * @author Stefan
 */
public class RessCapacityMultiEdge extends AbstractCapacity {
    private final int ressId;
    
    public RessCapacityMultiEdge(int ressId, int capacity) {
        super(capacity);
        this.ressId = ressId;        
    }

    /**
     * @return the ressId
     */
    public int getRessId() {
        return ressId;
    }
}
