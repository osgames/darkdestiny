/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ships;

/**
 *
 * @author Stefan
 */
public class Computer extends SimpleModule {
    private double damageBonus;
    private double targetFireAccBonus;
    
    public Computer(ModInitParameter mip, int id) {
        super(mip,id);
    }

    /**
     * @return the damageBonus
     */
    public double getDamageBonus() {
        return damageBonus;
    }

    /**
     * @param damageBonus the damageBonus to set
     */
    public void setDamageBonus(double damageBonus) {
        this.damageBonus = damageBonus;
    }

    /**
     * @return the targetFireAccBonus
     */
    public double getTargetFireAccBonus() {
        return targetFireAccBonus;
    }

    /**
     * @param targetFireAccBonus the targetFireAccBonus to set
     */
    public void setTargetFireAccBonus(double targetFireAccBonus) {
        this.targetFireAccBonus = targetFireAccBonus;
    }
}
