/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.diplomacy.combat;

 import at.darkdestiny.core.dao.UserDAO;import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.UserDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Stefan
 */
public class CombatGroupResult {

    private static final Logger log = LoggerFactory.getLogger(CombatGroupResult.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    private final ArrayList<CombatGroupEntry> combatGroups = new ArrayList<CombatGroupEntry>();

    protected CombatGroupResult() {
    }

    protected void addCombatGroup(CombatGroupEntry cge) {
        combatGroups.add(cge);
    }

    public ArrayList<CombatGroupEntry> getCombatGroups() {
        return combatGroups;
    }

    public boolean hasConflictingParties() {
        for (CombatGroupEntry cge : combatGroups) {
            if (cge.getAttackingGroups().size() > 0) {
                return true;
            }
        }

        return false;
    }

    public void removeCombatGroupFromCompare(int groupId) {
        for (Iterator<CombatGroupEntry> cgeIt = combatGroups.iterator(); cgeIt.hasNext();) {
            CombatGroupEntry cge = cgeIt.next();
            if (cge.getGroupId() != groupId) {
                continue;
            }

            for (CombatGroupEntry cgeInner : combatGroups) {
                if (cgeInner.getGroupId() != cge.getGroupId()) {
                    for (Iterator<Integer> grpIdIt = cgeInner.getAttackingGroups().iterator(); grpIdIt.hasNext();) {
                        Integer id = grpIdIt.next();
                        if (id == groupId) {
                            grpIdIt.remove();
                        }
                    }
                }
            }

            cgeIt.remove();
        }
    }

    public HashMap<Integer, ArrayList<Integer>> getAsMap() {
        HashMap<Integer, ArrayList<Integer>> groupPlayerMap = new HashMap<Integer, ArrayList<Integer>>();

        for (CombatGroupEntry cge : getCombatGroups()) {
            groupPlayerMap.put(cge.getGroupId(), cge.getPlayers());
        }
        return groupPlayerMap;
    }

    public void print() {
        log.debug("=================== START ==================");
        for (CombatGroupEntry cge : combatGroups) {
            log.debug("====================================================");
            log.debug("Combatgroup " + cge.getGroupId());
            log.debug("====================================================");
            for (Integer cgPlayerId : cge.getPlayers()) {
                log.debug(uDAO.findById(cgPlayerId).getGameName());
            }

            log.debug("This group attacks:");
            for (Integer attacks : cge.getAttackingGroups()) {
                log.debug("Group " + attacks);
            }
        }
        log.debug("==================== END ===================");
    }
}
