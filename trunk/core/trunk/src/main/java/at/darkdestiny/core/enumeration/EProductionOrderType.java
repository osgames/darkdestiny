/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Bullet
 */
public enum EProductionOrderType {

   PRODUCE, SCRAP, UPGRADE, GROUNDTROOPS, C_BUILD,
   C_BUILD_STATIONS, C_SCRAP, C_UPGRADE, REPAIR, C_IMPROVE
}
