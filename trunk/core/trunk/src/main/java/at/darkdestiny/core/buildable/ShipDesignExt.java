/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.buildable;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.admin.module.AttributeTree;
import at.darkdestiny.core.admin.module.ModuleAttributeResult;
import at.darkdestiny.core.dao.ChassisDAO;
import at.darkdestiny.core.dao.DesignModuleDAO;
import at.darkdestiny.core.dao.RessourceCostDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.ERessourceCostType;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.DesignModule;
import at.darkdestiny.core.model.RessourceCost;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.ships.BuildTime;
import at.darkdestiny.core.ships.ShipRepairCost;
import at.darkdestiny.core.ships.ShipScrapCost;
import at.darkdestiny.core.ships.ShipUpgradeCost;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ShipDesignExt implements Buildable {

    private static DesignModuleDAO dmDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    private static ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private final ShipDesign ship;
    private final RessourcesEntry re;
    private final BuildTime bt;
    private final Chassis c;

    public ShipDesignExt(ShipDesign shipDesign) {
        ship = shipDesign;
        ArrayList<RessAmountEntry> raeList = new ArrayList<RessAmountEntry>();
        ArrayList<RessourceCost> rcList = rcDAO.find(ERessourceCostType.SHIP, ship.getId());

        for (RessourceCost rc : rcList) {
            RessAmountEntry rae = new RessAmountEntry(rc.getRessId(), rc.getQty());
            raeList.add(rae);
        }

        re = new RessourcesEntry(raeList);

        c = cDAO.findById(ship.getChassis());
        AttributeTree aTree = new AttributeTree(ship.getUserId(), 0l);
        ModuleAttributeResult mar = aTree.getAllAttributes(c.getId(), c.getRelModuleId());

        bt = new BuildTime();

        int orbDockPoints = 0;
        int dockPoints = 0;
        int modPoints = 0;

        dockPoints = (int) mar.getAttributeValue(at.darkdestiny.core.enumeration.EAttribute.DOCK_CONS_POINTS) * c.getMinBuildQty();
        orbDockPoints = (int) mar.getAttributeValue(at.darkdestiny.core.enumeration.EAttribute.ORB_DOCK_CONS_POINTS) * c.getMinBuildQty();
        // bt.setDockPoints((int)mar.getAttributeValue(at.darkdestiny.core.Attribute.DOCK_CONS_POINTS) * c.getMinBuildQty());
        // bt.setOrbDockPoints((int)mar.getAttributeValue(at.darkdestiny.core.Attribute.ORB_DOCK_CONS_POINTS) * c.getMinBuildQty());

        for (DesignModule dm : dmDAO.findByDesignId(ship.getId())) {
            mar = aTree.getAllAttributes(c.getId(), dm.getModuleId());
            modPoints += (int) mar.getAttributeValue(at.darkdestiny.core.enumeration.EAttribute.MOD_CONS_POINTS) * c.getMinBuildQty() * dm.getCount();
            dockPoints += (int) mar.getAttributeValue(at.darkdestiny.core.enumeration.EAttribute.DOCK_CONS_POINTS) * c.getMinBuildQty() * dm.getCount();
            orbDockPoints += (int) mar.getAttributeValue(at.darkdestiny.core.enumeration.EAttribute.ORB_DOCK_CONS_POINTS) * c.getMinBuildQty() * dm.getCount();
        }

        bt.setModulePoints(modPoints);
        bt.setDockPoints(dockPoints);
        bt.setOrbDockPoints(orbDockPoints);
    }

    public ShipDesignExt(int id) {
        this(sdDAO.findById(id));
    }

    @Override
    public RessourcesEntry getRessCost() {
        return re;
    }

    @Override
    public Model getBase() {
        return ship;
    }

    public Class getModelClass(){
        return ShipDesign.class;
    }

    @Override
    public BuildTime getBuildTime() {
        return bt;
    }

    public ShipScrapCost getShipScrapCost() {
        return new ShipScrapCost(this);
    }

    public ShipScrapCost getShipScrapCost(int count) {
        return new ShipScrapCost(this, count);
    }

    public ShipUpgradeCost getShipUpgradeCost(ShipDesignExt sdeTo) {
        return new ShipUpgradeCost(this, sdeTo);
    }

    public ShipUpgradeCost getShipUpgradeCost(ShipDesignExt sdeTo, int count) {
        return new ShipUpgradeCost(this, sdeTo, count);
    }

    public ShipRepairCost getShipRepairCost() {
        return new ShipRepairCost(this);
    }

    public Chassis getChassis() {
        return c;
    }
}
