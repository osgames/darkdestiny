/**
 * 
 */
package at.darkdestiny.core.admin.db;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author martin
 *
 */
public class IntegerField extends IDatabaseColumn {

	private final int column;

	/**
	 * @param comment 
	 * @param readonly 
	 * @param name 
	 * @param column 
	 * 
	 */
	public IntegerField(int column, String name, boolean readonly, String comment) {
		super(name,comment, readonly);
		this.column = column;
	}

	public Object getFromResultSet(ResultSet rs) throws SQLException {
		return rs.getInt(column);
	}

	public String makeEdit(Object object) {
		
		if (object == null)
			object = 0;
		
		return "<input type=\"text\" name=\"f_"+name+"\" size=\"80\" value=\""+
			((Integer)object).intValue()+"\" "+
			(readonly?"readonly ":"")+"/>";
	}

	protected String getValueOnEmpty()
	{
		return "'0'";
	}
}
