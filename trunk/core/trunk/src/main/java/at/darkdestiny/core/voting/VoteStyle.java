/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

/**
 *
 * @author Rudi
 */
public class VoteStyle {
    private boolean displayVotings = true;  
    private String overrideNotVotedML = null;
    private String overrideVotedML = null;

    /**
     * @return the displayVotings
     */
    public boolean isDisplayVotings() {
        return displayVotings;
    }

    /**
     * @param displayVotings the displayVotings to set
     */
    public void setDisplayVotings(boolean displayVotings) {
        this.displayVotings = displayVotings;
    }

    /**
     * @return the overrideNotVotedML
     */
    public String getOverrideNotVotedML() {
        return overrideNotVotedML;
    }

    /**
     * @param overrideNotVotedML the overrideNotVotedML to set
     */
    public void setOverrideNotVotedML(String overrideNotVotedML) {
        this.overrideNotVotedML = overrideNotVotedML;
    }

    /**
     * @return the overrideVotedML
     */
    public String getOverrideVotedML() {
        return overrideVotedML;
    }

    /**
     * @param overrideVotedML the overrideVotedML to set
     */
    public void setOverrideVotedML(String overrideVotedML) {
        this.overrideVotedML = overrideVotedML;
    }
}
