/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.development.conditions;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.ActiveBonusDAO;
import at.darkdestiny.core.dao.BonusDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.core.enumeration.EEconomyType;
import at.darkdestiny.core.model.Bonus;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class AgriculturalColony implements IBonusCondition {
    private static BonusDAO bDAO = (BonusDAO)DAOFactory.get(BonusDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    private static ActiveBonusDAO abDAO = (ActiveBonusDAO)DAOFactory.get(ActiveBonusDAO.class);
    
    @Override
    public boolean isPossible(int planetId) {
        // Population larger than 500 Mio.
        // 75% work in agriculture
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
                
        if (pp.getPopulation() < 1000000) {
            return false;
        }                
        if (abDAO.findByPlanetAndBonusId(pp.getPlanetId(), 5) != null) return false;
        
        try {                   
            /*
            PlanetCalculation pc = new PlanetCalculation(planetId);
            ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
            
            double unused = epcr.getPlanetEconomyWorkerPerc().get(EEconomyType.NOT_USED);
            double blowUp = 100d / (100d - unused);
            
            if (epcr.getPlanetEconomyWorkerPerc().get(EEconomyType.AGRICULTURE) == null) return false;
            if ((epcr.getPlanetEconomyWorkerPerc().get(EEconomyType.AGRICULTURE) * blowUp) < 75d) {
                return false;
            } 
            */
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in checking bonus condition: ", e);
            return false;
        }
        
        return true;
    }
    
    @Override
    public boolean isPossible() {
        return false;
    }    
    
    @Override 
    public ArrayList<String> activeBoni(int bonusId, int planetId, int userId) {
        Bonus b = bDAO.findById(bonusId);
        
        ArrayList<String> boniList = new ArrayList<String>();
                
        boniList.add("["+ML.getMLStr(b.getName(),userId)+"] +30% Nahrungsproduktion");
        
        return boniList;
    }
    
    @Override 
    public void onActivation(int bonusId, int planetId, int userId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        pp.setColonyType(EColonyType.AGRICULTURAL_COLONY);
        ppDAO.update(pp);            
    }    
}
