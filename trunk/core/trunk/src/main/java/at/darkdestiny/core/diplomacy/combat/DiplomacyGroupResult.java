/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy.combat;

import at.darkdestiny.core.model.DiplomacyRelation;
import at.darkdestiny.core.model.DiplomacyType;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class DiplomacyGroupResult {
    private final ArrayList<AllianceEntry> alliances;
    private final ArrayList<PlayerEntry> players;

    private HashMap<Integer,CombatParticipent> userCPRel = new HashMap<Integer,CombatParticipent>();

    public DiplomacyGroupResult(ArrayList<AllianceEntry> alliances, ArrayList<PlayerEntry> players) {
        this.alliances = alliances;
        this.players = players;
    }

    public ArrayList<AllianceEntry> getAlliances() {
        return alliances;
    }

    public ArrayList<PlayerEntry> getPlayers() {
        return players;
    }

    public DiplomacyRelation getRelationBetween(int user1, int user2) {
        CombatParticipent cp1 = null;
        CombatParticipent cp2 = null;

        // find user1
        if (!userCPRel.containsKey(user1)) {
            for (PlayerEntry pe : players) {
                if (pe.getUserId() == user1) {
                    cp1 = pe;
                    break;
                }
            }

            if (cp1 == null) {
                for (AllianceEntry ae : alliances) {
                    for (Integer user : ae.getAllMemberIds()) {
                        if (user.equals(user1)) {
                            cp1 = ae;
                            break;
                        }
                    }

                    if (cp1 != null) break;
                }
            }

            if (cp1 == null) DebugBuffer.addLine(DebugLevel.ERROR, "User " + user1 + " not found!");
            userCPRel.put(user1, cp1);
        } else {
            cp1 = userCPRel.get(user1);
        }

        // find user2
        if (!userCPRel.containsKey(user2)) {
            for (PlayerEntry pe : players) {
                if (pe.getUserId() == user2) {
                    cp2 = pe;
                    break;
                }
            }

            if (cp2 == null) {
                for (AllianceEntry ae : alliances) {
                    for (Integer user : ae.getAllMemberIds()) {
                        if (user.equals(user2)) {
                            cp2 = ae;
                            break;
                        }
                    }

                    if (cp2 != null) break;
                }
            }

            if (cp2 == null) DebugBuffer.addLine(DebugLevel.ERROR, "User " + user2 + " not found!");
            userCPRel.put(user2, cp2);
        } else {
            cp2 = userCPRel.get(user2);
        }

        if ((cp1 instanceof AllianceEntry) && (cp2 instanceof AllianceEntry) && ((cp1.getRelationTo(cp2)).getDiplomacyTypeId() == DiplomacyType.NEUTRAL)) {
            for (AllianceMemberEntry ame : ((AllianceEntry)cp1).getMembers()) {
                if (ame.getUserId() == user1) {
                    for (AllianceMemberEntry ame2 : ((AllianceEntry)cp2).getMembers()) {
                        if (ame2.getUserId() == user2) {
                            // log.debug("1: " + ame + "/" + ame2 + " >> " + ame.getUserId() + "/"+ame2.getUserId());
                            return ame.getRelationTo(ame2);
                        }
                    }
                }
            }

            // log.debug("2: " + cp1 + "/" + cp2 + " >> " + cp1.getId() + "/"+cp2.getId());
            return cp1.getRelationTo(cp2);
        } else {
            // log.debug("3:" + cp1 + "/" + cp2 + " >> " + cp1.getId() + "/"+cp2.getId());
            if ((cp1 instanceof PlayerEntry) && (cp2 instanceof AllianceEntry)) {
                if (cp1.getRelationTo(cp2).getDiplomacyTypeId() == DiplomacyType.NEUTRAL) {
                    // log.debug("Try to override neutral");
                    // Find corresponding AllianceMember Entry for cp2
                    AllianceEntry aeTmp = (AllianceEntry)cp2;
                    ArrayList<AllianceMemberEntry> amList = aeTmp.getMembers();
                    for (AllianceMemberEntry ameTmp : amList) {
                        // log.debug("Looping user " + ameTmp.getUserId());
                        if (ameTmp.getUserId() == user2) {
                            if (cp1.getRelationTo(ameTmp) != null) return cp1.getRelationTo(ameTmp);
                        }
                    }
                }
            } else if ((cp1 instanceof AllianceEntry) && (cp2 instanceof PlayerEntry)) {
                if (cp1.getRelationTo(cp2).getDiplomacyTypeId() == DiplomacyType.NEUTRAL) {
                    // log.debug("Try to override neutral");
                    // Find corresponding AllianceMember Entry for cp1
                    AllianceEntry aeTmp = (AllianceEntry)cp1;
                    ArrayList<AllianceMemberEntry> amList = aeTmp.getMembers();
                    for (AllianceMemberEntry ameTmp : amList) {
                        // log.debug("Looping user " + ameTmp.getUserId());
                        if (ameTmp.getUserId() == user1) {
                            if (ameTmp.getRelationTo(cp2) != null) return ameTmp.getRelationTo(cp2);
                        }
                    }
                }
            }

            return cp1.getRelationTo(cp2);
        }
    }
}
