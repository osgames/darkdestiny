/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.WeaponModule;
import at.darkdestiny.core.admin.module.ReferenceType;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.exceptions.InvalidArgumentException;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.spacecombat.helper.ArmorShieldFactor;
import at.darkdestiny.core.spacecombat.helper.ShieldDefenseDetail;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import at.darkdestiny.core.spacecombat.helper.TargetListController;
import at.darkdestiny.core.spacecombat.helper.TargetListEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.WeaponModule;
import at.darkdestiny.core.admin.module.ReferenceType;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.exceptions.InvalidArgumentException;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.spacecombat.helper.ArmorShieldFactor;
import at.darkdestiny.core.spacecombat.helper.ShieldDefenseDetail;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import at.darkdestiny.core.spacecombat.helper.TargetListController;
import at.darkdestiny.core.spacecombat.helper.TargetListEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */


public class CombatUnit extends AbstractCombatUnit {

    private static final Logger log = LoggerFactory.getLogger(CombatUnit.class);

    private HashSet<CombatUnit> alreadyFiredOn = new HashSet<CombatUnit>();

    public CombatUnit(int id, int shipFleetId, int orgCount, CombatGroupFleet.UnitTypeEnum ute) {
        super(id, shipFleetId, orgCount, ute);

        if (ute != CombatGroupFleet.UnitTypeEnum.SHIP) {
            throw new InvalidArgumentException("This constructor only supports UnitTypeEnum SHIP");
        }
    }

    public CombatUnit(int id, int orgCount, CombatGroupFleet.UnitTypeEnum ute) {
        super(id, orgCount, ute);

        if ((ute != CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE)
                && (ute != CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE)) {
            throw new InvalidArgumentException("This constructor only supports UnitTypeEnum SURFACE_DEFENSE or PLATTFORM_DEFENSE");
        }
    }

    @Override
    public boolean fire(boolean allowReturnFire) {
        // ATTACKER - Retrieve all possible targets for this unit
        TargetListController tlc = (TargetListController) CombatContext.getRegisteredObject("TL_MODULE");

        log.debug("Load TLC for " + this + " = " + tlc);
        log.debug(" " + this + " = " + tlc);

        ArrayList<TargetListEntry> targetLists = tlc.getList(this);

        // ATTACKER - Sump up absolute targeting value
        double totalValue = 0f;
        for (TargetListEntry tle : targetLists) {
            totalValue += tle.getRelativeAttackProbability();
        }

        // ATTACKER - Assign weapons on targets
        HashMap<Integer, ArmorShieldFactor> asfMap = CombatContext.getRegisteredObject("ASFMAP");
        HashMap<TargetListEntry, HashMap<Integer, Integer>> fireWeaponOnTargetCount = getWeaponsOnTarget(targetLists, totalValue);

        // ATTACKER - Loop through all Targets and process firing
        for (TargetListEntry tle : targetLists) {
            // ATTACKER - If no weapon shoots at this target
            //            SKIP
            if (!fireWeaponOnTargetCount.containsKey(tle)) {
                continue;
            }

            // ATTACKER - If attacker has already shot on this target
            //            SKIP
            if (getAlreadyFiredOn().contains((CombatUnit) tle.getTarget())) {
                continue;
            }

            // Get defending unit
            ICombatUnit icu = tle.getTarget();

            // Only informative: Percentage of attackpower of attacker goes to current target
            double attackShare = 100d / totalValue * tle.getRelativeAttackProbability();

            ShieldDefinition enemyShields;
            int armorType = 0;

            AbstractCombatUnit enemy = (AbstractCombatUnit) icu;
            // If defender is already destroyed skip this calculation
            // TODO: It may be questionable why this case can happen anyway
            //       I suppose this happens if this target was destroyed by some other attacker
            //       but targeting values are still there
            if (((CombatUnit) enemy).getTempDestroyed() >= 1d) {
                continue;
            }

            enemyShields = enemy.getShields();

            // Store the original destroyed values of attacker and defender
            double orgTempDestroyed_Enemy = enemy.getTempDestroyed();
            double orgTempDestroyed = this.getTempDestroyed();

            log.debug("=================================================================================");
            log.debug(this.getName() + " attacks " + enemy.getName());
            log.debug("=================================================================================");

            // TODO: What can there be else than a CombatUnit??
            if (icu instanceof CombatUnit) {
                ArmorShieldFactor asf = null;
                AbstractBattleShip abs = null;

                // Load customized ArmorShieldFactors for Planetary defense or Ships
                if (icu instanceof DefenseBuilding) {
                    asf = ((HashMap<Integer, ArmorShieldFactor>) CombatContext.getRegisteredObject("ASFMAP_PLANETARY")).get(0);
                } else {
                    abs = (AbstractBattleShip) icu;
                    asf = asfMap.get(abs.getShipDesign().getId());
                }

                // Load and Buffer absorbtion and damage reduction relations between weapons and defense
                // This is buffered for every ship object separately
                // TODO: Check if this may cause inconsistencies if different ship sizes have different penetration values
                //       Dependant on the ship firing the weapon
                for (WeaponModule wm : getWeapons()) {
                    if (!enemy.paraPenetration.containsKey(wm.getWeaponId())) {
                        enemy.paraPenetration.put(wm.getWeaponId(), asf.getPTShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                    if (!enemy.huPenetration.containsKey(wm.getWeaponId())) {
                        enemy.huPenetration.put(wm.getWeaponId(), asf.getHUShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                    if (!enemy.prallPenetration.containsKey(wm.getWeaponId())) {
                        enemy.prallPenetration.put(wm.getWeaponId(), asf.getPSShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }

                    if (!enemy.ynkeAbsorb.containsKey(wm.getWeaponId())) {
                        enemy.ynkeAbsorb.put(wm.getWeaponId(), asf.getYnkeArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                    if (!enemy.terkAbsorb.containsKey(wm.getWeaponId())) {
                        enemy.terkAbsorb.put(wm.getWeaponId(), asf.getTerkArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                    if (!enemy.steelAbsorb.containsKey(wm.getWeaponId())) {
                        enemy.steelAbsorb.put(wm.getWeaponId(), asf.getSteelArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                }

                for (WeaponModule wm : enemy.getWeapons()) {
                    if (!this.paraPenetration.containsKey(wm.getWeaponId())) {
                        this.paraPenetration.put(wm.getWeaponId(), asf.getPTShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                    if (!this.huPenetration.containsKey(wm.getWeaponId())) {
                        this.huPenetration.put(wm.getWeaponId(), asf.getHUShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                    if (!this.prallPenetration.containsKey(wm.getWeaponId())) {
                        this.prallPenetration.put(wm.getWeaponId(), asf.getPSShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }

                    if (!this.ynkeAbsorb.containsKey(wm.getWeaponId())) {
                        this.ynkeAbsorb.put(wm.getWeaponId(), asf.getYnkeArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                    if (!this.terkAbsorb.containsKey(wm.getWeaponId())) {
                        this.terkAbsorb.put(wm.getWeaponId(), asf.getTerkArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                    if (!this.steelAbsorb.containsKey(wm.getWeaponId())) {
                        this.steelAbsorb.put(wm.getWeaponId(), asf.getSteelArmor().getAttributeValue(EAttribute.DAMAGE_REDUCTION, wm.getWeaponId(), ReferenceType.MODULE));
                    }
                }

                // Ground defense does not have Armor so we can skip this check if its not a battleship
                if (icu instanceof BattleShipNew) {
                    armorType = abs.getArmor();
                } else if (icu instanceof DefenseBuilding) {
                    DefenseBuilding db = (DefenseBuilding) icu;
                    armorType = db.getArmor();
                }
            }

            // TODO: Is this the effect of planetary weapons on ships??
            //       If yes, why is there no buffering of armor values??
            HashMap<Integer, ArmorShieldFactor> asfMapPlanetary = CombatContext.getRegisteredObject("ASFMAP_PLANETARY");
            ArmorShieldFactor asfPlanetary = asfMapPlanetary.get(0);

            for (WeaponModule wm : enemy.getWeapons()) {
                if (!this.paraPenetration.containsKey(wm.getWeaponId())) {
                    this.paraPenetration.put(wm.getWeaponId(), asfPlanetary.getPTShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                }
                if (!this.huPenetration.containsKey(wm.getWeaponId())) {
                    this.huPenetration.put(wm.getWeaponId(), asfPlanetary.getHUShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                }
                if (!this.prallPenetration.containsKey(wm.getWeaponId())) {
                    this.prallPenetration.put(wm.getWeaponId(), asfPlanetary.getPSShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, wm.getWeaponId(), ReferenceType.MODULE));
                }
            }

            log.debug("[" + this.getName() + "] Firing " + attackShare + " on target " + enemy.getName());
            log.debug("Enemy HP = " + enemy.getHp() + " Enemy Count = " + enemy.getCount());

            // Get damage value of attacker
            double totalDamageAtt = 0d;
            double totalDamageDef = 0d;
            HashMap<Integer, Double> attWeapStr = new HashMap<Integer, Double>();
            HashMap<Integer, Double> defWeapStr = new HashMap<Integer, Double>();

            // Sum up all damage which attacker deals
            // This is needed for shield calculation
            double damageAttacker = 0d;

            // Loop all weapons of the attacker
            for (WeaponModule wm : weapons) {
                // There are 4 conditions which lead to skipping the current attack calculation
                // 1. There is no Weapon firing on this target
                if (fireWeaponOnTargetCount.get(tle) == null) {
                    break;
                }
                // 2. The specific current weapon does not fire on this target
                if (fireWeaponOnTargetCount.get(tle).get(wm.getWeaponId()) == null) {
                    continue;
                }
                // 3. The defender got already destroyed
                if (((CombatUnit) enemy).getTempDestroyed() >= 1d) {
                    continue;
                }
                // 4. The attacking ship is already destroyed
                // TODO: How can i get here then anyway???
                if (this.tempDestroyed >= 1d) {
                    continue;
                }

                // Get the number of weapons and the module which is firing at the defender
                int weaponCount = fireWeaponOnTargetCount.get(tle).get(wm.getWeaponId());
                Module m = mDAO.findById(wm.getWeaponId());

                // Store weapons with their current attack power in a HashMap
                // and sum up their total attack value
                // This is need for shield calculations
                attWeapStr.put(wm.getWeaponId(), (double) wm.getAttackPower() * this.damageBonus * enemy.damageDefenseBonus * weaponCount * (1d - this.tempDestroyed));
                totalDamageAtt += wm.getAttackPower() * this.damageBonus * enemy.damageDefenseBonus * weaponCount * (1d - this.tempDestroyed);

                log.debug("ATTACKER1 [" + this.getName() + "] ATTACKBONUS: " + this.damageBonus + "DEFENSEBONUS: " + this.damageDefenseBonus);

                int groupingMultiplier = 1;
                if (enemy instanceof BattleShipNew) {
                    groupingMultiplier = ((BattleShipNew) enemy).getGroupingMultiplier();
                }
                log.debug("WEAPONCOUNT=" + weaponCount + " ENEMYCOUNT=" + enemy.getCount());
                int maxTargets = Math.min(weaponCount, enemy.getCount() * groupingMultiplier);

                // If enemy shields are down ignore fireindicator gradually
                log.debug("TLE TFI: " + tle.getTargetFireIndicator() + " TLE TFIgnore: " + targetFireIgnore(tle.getTarget()));
                maxTargets = Math.min(maxTargets, (int) Math.ceil(maxTargets / Math.max(1d, tle.getTargetFireIndicator() * targetFireIgnore(tle.getTarget()))));

                log.debug("Current weapon [" + m.getName() + "] attackpower of " + attWeapStr.get(wm.getWeaponId()) + " is distributed on " + maxTargets + " targets");
                log.debug("Current destroyed Level of target is " + ((CombatUnit) enemy).getTempDestroyed());

                double hpDefenderNew = enemy.getHp() * enemy.getCount() * (1d - ((CombatUnit) enemy).getTempDestroyed());
                // log.debug("HPDefenderNew = " + hpDefenderNew + " = " + enemy.getHp() + " * " + enemy.getCount() + " * (1d - " + ((CombatUnit) enemy).getTempDestroyed() + ")");
                double lossDefenderTheoretically = (100d / hpDefenderNew) * attWeapStr.get(wm.getWeaponId());
                // log.debug("lossDefenderTheoretically = " + lossDefenderTheoretically + " = (100d / " + hpDefenderNew + ") * " + attWeapStr.get(wm.getWeaponId()));
                double lossDefenderNew = (100d / hpDefenderNew) * ((enemy.getHp() / groupingMultiplier) * maxTargets);
                // log.debug("lossDefenderNew = " + hpDefenderNew + " = (100d / " + hpDefenderNew + ") * (" + enemy.getHp() + " / " + groupingMultiplier + ") * " + maxTargets);

                log.debug("This results in maximum losses of " + maxTargets + " (respective a tempDestroyed of " + Math.min(lossDefenderTheoretically, lossDefenderNew) + "%)");
                log.debug("Unnormalized losses: " + lossDefenderTheoretically + "%)");

                // *********************************************
                // Planetary Shield
                // *********************************************
                if (enemy.getPlanetaryShields() != null) {
                    log.debug("So how does the planetary shield perform?");
                    ShieldDefenseDetail sddDefTmp = new ShieldDefenseDetail();
                    double multiplier = 1d - getReductionThroughShieldsNew(enemy, this, wm, maxTargets, attWeapStr, enemy.getPlanetaryShields(), sddDefTmp);

                    lossDefenderTheoretically = lossDefenderTheoretically * multiplier;
                    log.debug("Unnormalized losses AFTER PLANETARY SHIELD: " + lossDefenderTheoretically + "%)");

                    this.sc_LoggingEntry.incPA_DamageAbsorbed(sddDefTmp.getAbsPA());
                    this.sc_LoggingEntry.incPA_DamagePenetrated(sddDefTmp.getPenPA());
                    this.sc_LoggingEntry.incHU_DamageAbsorbed(sddDefTmp.getAbsHU());
                    this.sc_LoggingEntry.incHU_DamagePenetrated(sddDefTmp.getPenHU());
                    this.sc_LoggingEntry.incPS_DamageAbsorbed(sddDefTmp.getAbsPS());
                    this.sc_LoggingEntry.incPS_DamagePenetrated(sddDefTmp.getPenPS());
                }                         
                
                // *********************************************
                // Unit Shields
                // *********************************************                     
                if (enemy.getShields() != null) {
                    log.debug("So how does the shield of this poor ships perform?");
                    ShieldDefenseDetail sddDefTmp = new ShieldDefenseDetail();
                    double multiplier = 1d - enemy.getReductionThroughShieldsNew(enemy, this, wm, maxTargets, attWeapStr, enemy.getShields(), sddDefTmp);

                    lossDefenderTheoretically = lossDefenderTheoretically * multiplier;
                    log.debug("Unnormalized losses AFTER SHIELD: " + lossDefenderTheoretically + "%)");

                    enemy.sc_LoggingEntry.incPA_DamageAbsorbed(sddDefTmp.getAbsPA());
                    enemy.sc_LoggingEntry.incPA_DamagePenetrated(sddDefTmp.getPenPA());
                    enemy.sc_LoggingEntry.incHU_DamageAbsorbed(sddDefTmp.getAbsHU());
                    enemy.sc_LoggingEntry.incHU_DamagePenetrated(sddDefTmp.getPenHU());
                    enemy.sc_LoggingEntry.incPS_DamageAbsorbed(sddDefTmp.getAbsPS());
                    enemy.sc_LoggingEntry.incPS_DamagePenetrated(sddDefTmp.getPenPS());
                }
                log.debug("Well, doesnt look to good, thank god we have armor as well");

                double lossDefTheoPreArmor = lossDefenderTheoretically;

                int tmpArmorType = 0;

                if (enemy instanceof AbstractCombatUnit) {
                    AbstractCombatUnit acu = (AbstractCombatUnit) enemy;          
                    
                    System.out.println("Found armortype " + acu.getArmorType() + " for unit " + acu.getName());

                    switch (acu.getArmorType()) {
                        case (1):
                            lossDefenderTheoretically = lossDefenderTheoretically * (1d - enemy.steelAbsorb.get(wm.getWeaponId()));
                            double damagePostArmorDef = (lossDefenderTheoretically / 100d) * hpDefenderNew;
                            enemy.sc_LoggingEntry.incST_DamageAbsorbed((lossDefenderTheoretically / lossDefTheoPreArmor) * hpDefenderNew);
                            break;
                        case (2):
                            lossDefenderTheoretically = lossDefenderTheoretically * (1d - enemy.terkAbsorb.get(wm.getWeaponId()));
                            damagePostArmorDef = (lossDefenderTheoretically / 100d) * hpDefenderNew;
                            enemy.sc_LoggingEntry.incTE_DamageAbsorbed((lossDefenderTheoretically / lossDefTheoPreArmor) * hpDefenderNew);
                            break;
                        case (3):
                            lossDefenderTheoretically = lossDefenderTheoretically * (1d - enemy.ynkeAbsorb.get(wm.getWeaponId()));
                            damagePostArmorDef = (lossDefenderTheoretically / 100d) * hpDefenderNew;
                            log.debug("LOSSDEFPREARMOR=" + lossDefTheoPreArmor + " LOSSDEFAFTERARMOR=" + lossDefenderTheoretically + " HPDEFNEW=" + hpDefenderNew);
                            enemy.sc_LoggingEntry.incYN_DamageAbsorbed((lossDefenderTheoretically / lossDefTheoPreArmor) * hpDefenderNew);
                            break;
                    }

                    log.debug("Unnormalized losses AFTER ARMOR: " + lossDefenderTheoretically + "%)");                
                }
                
                // calculate slow down of targeting adjustment
                double factor = Math.max(1d,1d + (enemy.getTargetFireDefenseBonus() - 1d) - (this.getTargetFireBonus() - 1d));
                
                if (lossDefenderTheoretically > lossDefenderNew) {
                    tle.setTargetFireIndicator(tle.getTargetFireIndicator() / (((lossDefenderTheoretically / lossDefenderNew) / factor)));
                    log.debug(lossDefenderTheoretically + " > " + lossDefenderNew + " - Adjusted TFI [" + enemy.getName() + "=>" + tle.getTarget().getName() + "] to " + tle.getTargetFireIndicator());
                } else {
                    tle.setTargetFireIndicator(tle.getTargetFireIndicator() * (2d / factor));
                    log.debug(lossDefenderTheoretically + " < " + lossDefenderNew + " - Adjusted TFI to " + tle.getTargetFireIndicator());
                }

                double effectiveLoss = 0d;
                if (lossDefenderTheoretically > lossDefenderNew) {
                    effectiveLoss = lossDefenderNew;
                } else {
                    effectiveLoss = lossDefenderTheoretically;
                }

                double currState = 1d - ((CombatUnit) enemy).getTempDestroyed();
                currState -= currState * (effectiveLoss / 100d);
                double destroyedDefender = (double) (Math.ceil((1d - currState) * 100d) / 100d);

                log.debug("Set destroyed defender to " + (1d - currState));

                log.debug("Adding to damage Eff Def: " + (hpDefenderNew * (effectiveLoss / 100d)));
                // enemy.sc_LoggingEntry.incEffDamageTaken((hpDefenderNew * (effectiveLoss / 100d)));
                ((CombatUnit) enemy).setTempDestroyed(destroyedDefender);

            }

            this.getAlreadyFiredOn().add((CombatUnit) enemy);

            if (allowReturnFire) {
                log.debug("=================================================================================");
                log.debug(enemy.getName() + " returns fire on " + this.getName());
                log.debug("=================================================================================");
                ArrayList<TargetListEntry> targetLists_Enemy = tlc.getList(enemy);

                double totalValue_Enemy = 0f;
                TargetListEntry currObj = null;

                for (TargetListEntry tle_Enemy : targetLists_Enemy) {
                    totalValue_Enemy += tle_Enemy.getRelativeAttackProbability();
                    if (tle_Enemy.getTarget().equals(this)) {
                        currObj = tle_Enemy;
                    }
                }

                HashMap<TargetListEntry, HashMap<Integer, Integer>> fireWeaponOnTargetCount_Enemy = enemy.getWeaponsOnTarget(targetLists_Enemy, totalValue_Enemy);
                attWeapStr.clear();

                for (WeaponModule wm : enemy.weapons) {
                    if (((CombatUnit) this).getTempDestroyed() >= 1d) {
                        continue;
                    }
                    if (orgTempDestroyed_Enemy >= 1d) {
                        continue;
                    }

                    if (fireWeaponOnTargetCount_Enemy.get(currObj) == null) {
                        break;
                    }
                    if (fireWeaponOnTargetCount_Enemy.get(currObj).get(wm.getWeaponId()) == null) {
                        continue;
                    }
                    int weaponCount = fireWeaponOnTargetCount_Enemy.get(currObj).get(wm.getWeaponId());
                    Module m = mDAO.findById(wm.getWeaponId());

                    attWeapStr.put(wm.getWeaponId(), (double) wm.getAttackPower() * enemy.damageBonus * this.damageDefenseBonus * weaponCount * (1d - orgTempDestroyed_Enemy));
                    totalDamageAtt += wm.getAttackPower() * enemy.damageBonus * this.damageDefenseBonus * weaponCount * (1d - orgTempDestroyed_Enemy);

                    log.debug("ATTACKER2 [" + enemy.getName() + "] ATTACKBONUS: " + enemy.damageBonus + "DEFENSEBONUS: " + enemy.damageDefenseBonus);

                    int groupingMultiplier = 1;
                    if (this instanceof BattleShipNew) {
                        groupingMultiplier = ((BattleShipNew) this).getGroupingMultiplier();
                    }
                    int maxTargets = Math.min(weaponCount, this.getCount() * groupingMultiplier);

                    log.debug("MAXTARGETS [" + weaponCount + "," + this.getCount() * groupingMultiplier + "]");
                    // If enemy shields are down ignore fireindicator gradually
                    log.debug("TLE TFI: " + currObj.getTargetFireIndicator() + " TLE TFIgnore: " + targetFireIgnore(currObj.getTarget()));
                    maxTargets = Math.min(maxTargets, (int) Math.ceil(maxTargets / Math.max(1d, currObj.getTargetFireIndicator() * targetFireIgnore(currObj.getTarget()))));
                    log.debug("Current weapon [" + m.getName() + "] attackpower of " + attWeapStr.get(wm.getWeaponId()) + " is distributed on " + maxTargets + " targets");
                    log.debug("Current destroyed Level of target is " + ((CombatUnit) this).getTempDestroyed());

                    double hpDefenderNew = this.getHp() * this.getCount() * (1d - ((CombatUnit) this).getTempDestroyed());
                    double lossDefenderTheoretically = (100d / hpDefenderNew) * attWeapStr.get(wm.getWeaponId());
                    double lossDefenderNew = (100d / hpDefenderNew) * ((this.getHp() / groupingMultiplier) * maxTargets);

                    /*
                     double hpDefenderNew = this.getHp() * this.getCount() * (1d - ((CombatUnit) this).getTempDestroyed());
                     double lossDefenderTheoretically = (100d / hpDefenderNew) * attWeapStr.get(wm.getWeaponId());
                     double lossDefenderNew = (100d / hpDefenderNew) * (this.getHp() * maxTargets);
                     */
                    log.debug("This results in maximum losses of " + maxTargets + " (respective a tempDestroyed of " + Math.min(lossDefenderTheoretically, lossDefenderNew) + "%)");
                    log.debug("Unnormalized losses: " + lossDefenderTheoretically + "%)");

                    // *********************************************
                    // Planetary Shield
                    // *********************************************
                    if (this.getPlanetaryShields() != null) {
                        log.debug("So how does the planetary shield perform?");
                        ShieldDefenseDetail sddDefTmp = new ShieldDefenseDetail();
                        double multiplier = 1d - getReductionThroughShieldsNew(this, enemy, wm, maxTargets, attWeapStr, this.getPlanetaryShields(), sddDefTmp);

                        lossDefenderTheoretically = lossDefenderTheoretically * multiplier;
                        log.debug("Unnormalized losses AFTER PLANETARY SHIELD: " + lossDefenderTheoretically + "%)");

                        this.sc_LoggingEntry.incPA_DamageAbsorbed(sddDefTmp.getAbsPA());
                        this.sc_LoggingEntry.incPA_DamagePenetrated(sddDefTmp.getPenPA());
                        this.sc_LoggingEntry.incHU_DamageAbsorbed(sddDefTmp.getAbsHU());
                        this.sc_LoggingEntry.incHU_DamagePenetrated(sddDefTmp.getPenHU());
                        this.sc_LoggingEntry.incPS_DamageAbsorbed(sddDefTmp.getAbsPS());
                        this.sc_LoggingEntry.incPS_DamagePenetrated(sddDefTmp.getPenPS());
                    }                    
                    
                    // *********************************************
                    // Unit Shields
                    // *********************************************                    
                    if (this.getShields() != null) {
                        log.debug("So how does the shield of this poor ships perform?");
                        ShieldDefenseDetail sddDefTmp = new ShieldDefenseDetail();
                        double multiplier = 1d - getReductionThroughShieldsNew(this, enemy, wm, maxTargets, attWeapStr, this.getShields(), sddDefTmp);

                        lossDefenderTheoretically = lossDefenderTheoretically * multiplier;
                        log.debug("Unnormalized losses AFTER SHIELD: " + lossDefenderTheoretically + "%)");

                        this.sc_LoggingEntry.incPA_DamageAbsorbed(sddDefTmp.getAbsPA());
                        this.sc_LoggingEntry.incPA_DamagePenetrated(sddDefTmp.getPenPA());
                        this.sc_LoggingEntry.incHU_DamageAbsorbed(sddDefTmp.getAbsHU());
                        this.sc_LoggingEntry.incHU_DamagePenetrated(sddDefTmp.getPenHU());
                        this.sc_LoggingEntry.incPS_DamageAbsorbed(sddDefTmp.getAbsPS());
                        this.sc_LoggingEntry.incPS_DamagePenetrated(sddDefTmp.getPenPS());
                    }

                    log.debug("Well, doesnt look to good, thank god we have armor as well");

                    double lossDefTheoPreArmor = lossDefenderTheoretically;
                    
                if (enemy instanceof AbstractCombatUnit) {
                    AbstractCombatUnit acu = (AbstractCombatUnit) enemy;          
                    
                    System.out.println("Found armortype " + acu.getArmorType() + " for unit " + acu.getName());
                        switch (acu.getArmorType()) {
                            case (1):
                                lossDefenderTheoretically = lossDefenderTheoretically * (1d - this.steelAbsorb.get(wm.getWeaponId()));
                                double damagePostArmorDef = (lossDefenderTheoretically / 100d) * hpDefenderNew;
                                this.sc_LoggingEntry.incST_DamageAbsorbed((lossDefenderTheoretically / lossDefTheoPreArmor) * hpDefenderNew);
                                break;
                            case (2):
                                lossDefenderTheoretically = lossDefenderTheoretically * (1d - this.terkAbsorb.get(wm.getWeaponId()));
                                damagePostArmorDef = (lossDefenderTheoretically / 100d) * hpDefenderNew;
                                this.sc_LoggingEntry.incTE_DamageAbsorbed((lossDefenderTheoretically / lossDefTheoPreArmor) * hpDefenderNew);
                                break;
                            case (3):
                                lossDefenderTheoretically = lossDefenderTheoretically * (1d - this.ynkeAbsorb.get(wm.getWeaponId()));
                                damagePostArmorDef = (lossDefenderTheoretically / 100d) * hpDefenderNew;
                                this.sc_LoggingEntry.incYN_DamageAbsorbed((lossDefenderTheoretically / lossDefTheoPreArmor) * hpDefenderNew);
                                break;
                        }

                        log.debug("Unnormalized losses AFTER ARMOR: " + lossDefenderTheoretically + "%)");
                    }

                    double factor = Math.max(1d,1d + (this.getTargetFireDefenseBonus() - 1d) - (enemy.getTargetFireBonus() - 1d));
                
                    if (lossDefenderTheoretically > lossDefenderNew) {
                        currObj.setTargetFireIndicator(currObj.getTargetFireIndicator() / ((lossDefenderTheoretically / lossDefenderNew) / factor));
                        log.debug(lossDefenderTheoretically + " > " + lossDefenderNew + " - Adjusted TFI [" + enemy.getName() + "=>" + currObj.getTarget().getName() + "] to " + currObj.getTargetFireIndicator());
                    } else {
                        currObj.setTargetFireIndicator(currObj.getTargetFireIndicator() * (2d / factor));
                        log.debug(lossDefenderTheoretically + " < " + lossDefenderNew + " - Adjusted TFI to " + currObj.getTargetFireIndicator());
                    }

                    double effectiveLoss = 0d;
                    if (lossDefenderTheoretically > lossDefenderNew) {
                        effectiveLoss = lossDefenderNew;
                    } else {
                        effectiveLoss = lossDefenderTheoretically;
                    }

                    double currState = 1d - ((CombatUnit) this).getTempDestroyed();
                    currState -= currState * (effectiveLoss / 100d);
                    double destroyedDefender = (double) (Math.ceil((1d - currState) * 100d) / 100d);

                    log.debug("Set destroyed defender to " + (1d - currState));

                    log.debug("Adding to damage Eff Def: " + (hpDefenderNew * (effectiveLoss / 100d)));
                    // enemy.sc_LoggingEntry.incEffDamageTaken((hpDefenderNew * (effectiveLoss / 100d)));
                    ((CombatUnit) this).setTempDestroyed(destroyedDefender);

                }

                ((CombatUnit) enemy).getAlreadyFiredOn().add(this);
            }

            // Both sides have fired set effective Damage taken
            double damageDiffAct = (this.getHp() * this.getCount() * this.getTempDestroyed()) - (this.getHp() * this.getCount() * orgTempDestroyed);
            double damageDiffEnemy = (enemy.getHp() * enemy.getCount() * enemy.getTempDestroyed()) - (enemy.getHp() * enemy.getCount() * orgTempDestroyed_Enemy);
            this.sc_LoggingEntry.incEffDamageTaken(damageDiffAct);
            enemy.sc_LoggingEntry.incEffDamageTaken(damageDiffEnemy);
        }

        log.debug("I'm firing ma laza");
        return false;
    }

    @Override
    protected double getReductionThroughShieldsNew(AbstractCombatUnit abs, AbstractCombatUnit absAtt, WeaponModule wm, int maxHitable, HashMap<Integer, Double> detailDamageAtt, ShieldDefinition shield, ShieldDefenseDetail sdd) {
        double reduction = 0d;
        double damageAttacker = detailDamageAtt.get(wm.getWeaponId());
        double damageAttOrg = damageAttacker;

        HashMap<Double, Double> weightedReductionMap = new HashMap<Double, Double>();

        double attackerDistraction = absAtt.getDistraction();

        double defenderCount = abs.getCount();
        double attackerCount = absAtt.getCount();

        if (abs instanceof BattleShipNew) {
            defenderCount *= ((BattleShipNew) abs).getGroupingMultiplier();
        }
        if (absAtt instanceof BattleShipNew) {
            attackerCount *= ((BattleShipNew) absAtt).getGroupingMultiplier();
        }

        defenderCount *= abs.targetFireDefenseBonus;
        attackerCount *= absAtt.targetFireBonus;

        
        
        log.debug("DEFENDER [" + abs.getName() + "] TARGETFIREBONUS: " + abs.targetFireDefenseBonus + " ATTACKER [" + absAtt.getName() + "] TARGETFIREBONUS:" + absAtt.targetFireBonus);

        log.debug("Average WR (" + absAtt.getName() + "): " + absAtt.averageWeaponRange + " Average WR (" + abs.getName() + "):" + abs.averageWeaponRange);
        double maxConcentrationPerc = (absAtt.averageWeaponRange / abs.averageWeaponRange) * absAtt.targetFireBonus;
        if (maxConcentrationPerc > 1d) {
            maxConcentrationPerc = 1d;
        }
        double concAdjust = Math.max(0d, 0.8d - (0.8d * maxConcentrationPerc));

        log.debug("VALUES concAdjust=" + concAdjust + " defenderCount=" + defenderCount + " attackerCount=" + attackerCount);
        log.debug("LEFT SIDE IS " + (Math.max(0d, (0.8d - concAdjust))) + " RIGHT SIDE IS " + (1d * Math.min(1d, defenderCount / attackerCount)));
        
        // Concentration modifier - lower is better for attacker
        double concentrationModifier = (0.2d + concAdjust) + (Math.max(0d, (0.8d - concAdjust)) / 1d * Math.min(1d, (defenderCount * abs.averageWeaponRange) / (attackerCount * absAtt.averageWeaponRange)));
        log.debug("Concentration Modifier is " + concentrationModifier + " >> defenderCount: " + defenderCount + " attackerCount: " + attackerCount);

        log.debug("PARATRON (Shared): " + shield.getSharedShield_PA() + " COUNT: " + abs.getCount() + " MaxHitable: " + maxHitable);
        log.debug("HU (Shared): " + shield.getSharedShield_HU() + " COUNT: " + abs.getCount() + " MaxHitable: " + maxHitable);
        log.debug("PRALL (Shared): " + shield.getSharedShield_PS() + " COUNT: " + abs.getCount() + " MaxHitable: " + maxHitable);

        float groupingMultiplier = 1f;
        if (abs instanceof BattleShipNew) {
            groupingMultiplier = ((BattleShipNew)abs).getGroupingMultiplier();
        }
        
        float psShieldStr = (shield.getSharedShield_PS() / (abs.getCount() * groupingMultiplier)) * maxHitable;
        float huShieldStr = (shield.getSharedShield_HU() / (abs.getCount() * groupingMultiplier)) * maxHitable;
        float paShieldStr = (shield.getSharedShield_PA() / (abs.getCount() * groupingMultiplier)) * maxHitable;

        double relShieldStrPA = paShieldStr * concentrationModifier;
        double relShieldStrHU = huShieldStr * concentrationModifier;
        double relShieldStrPS = psShieldStr * concentrationModifier;

        double currReduction = 0d;

        double absorbPS = abs.prallPenetration.get(wm.getWeaponId());
        double absorbHU = abs.huPenetration.get(wm.getWeaponId());
        double absorbPA = abs.paraPenetration.get(wm.getWeaponId());

        double wastedDamage = 0d;

        if (paShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbPA;

            if (absorbedDamage > relShieldStrPA) {
                // abs.sc_LoggingEntry.incPA_DamagePenetrated(absorbedDamage - relShieldStrPA);
                absorbedDamage = relShieldStrPA;
            }

            // abs.sc_LoggingEntry.incPA_DamageAbsorbed(absorbedDamage);
            log.debug("[PA] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrPA + " (Abs. Damage: " + absorbedDamage + " / absorbPA: " + absorbPA + ")");

            relShieldStrPA -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsPA(sdd.getAbsPA() + damageAttacker);
                shield.setSharedShield_PA((float) shield.getSharedShield_PA() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, currReduction + adjust * absorbPA);
                log.debug("CurrReduction: " + currReduction + " Adjust: " + adjust);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                shield.setSharedShield_PA((float) shield.getSharedShield_PA() - effAbsorbedDamage);
                // sdd.setAbsPA(sdd.getAbsPA() + effAbsorbedDamage);
                sdd.setAbsPA(sdd.getAbsPA() + absorbedDamage);
                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenPA(sdd.getPenPA() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // abs.sc_LoggingEntry.incPA_DamagePenetrated(damageAttacker);
            }
        }

        if (huShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbHU;

            if (absorbedDamage > relShieldStrHU) {
                // abs.sc_LoggingEntry.incHU_DamagePenetrated(absorbedDamage - relShieldStrHU);
                absorbedDamage = relShieldStrHU;
            }

            log.debug("[HU] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrHU + " (Abs. Damage: " + absorbedDamage + " / absorbHU: " + absorbHU + ")");

            // abs.sc_LoggingEntry.incHU_DamageAbsorbed(absorbedDamage);
            relShieldStrHU -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsHU(sdd.getAbsHU() + damageAttacker);
                shield.setSharedShield_HU((float) shield.getSharedShield_HU() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, currReduction + adjust * absorbHU);
                // currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbHU);
                log.debug("CurrReduction: " + currReduction + " Adjust: " + adjust);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                shield.setSharedShield_HU((float) shield.getSharedShield_HU() - effAbsorbedDamage);
                // sdd.setAbsHU(sdd.getAbsHU() + effAbsorbedDamage);
                sdd.setAbsHU(sdd.getAbsHU() + absorbedDamage);
                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenHU(sdd.getPenHU() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // sdd.setPenHU(sdd.getPenHU() + damageAttacker);

                // abs.sc_LoggingEntry.incHU_DamagePenetrated(damageAttacker);
            }
        }

        if (psShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbPS;

            if (absorbedDamage > relShieldStrPS) {
                // abs.sc_LoggingEntry.incPS_DamagePenetrated(absorbedDamage - relShieldStrPS);
                absorbedDamage = relShieldStrPS;
            }

            // abs.sc_LoggingEntry.incPS_DamageAbsorbed(absorbedDamage);
            relShieldStrPS -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsPS(sdd.getAbsPS() + damageAttacker);
                shield.setSharedShield_PS((float) shield.getSharedShield_PS() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, adjust * absorbHU);
                // currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbPS);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                shield.setSharedShield_PS((float) shield.getSharedShield_PS() - effAbsorbedDamage);
                // sdd.setAbsPS(sdd.getAbsPS() + effAbsorbedDamage);
                sdd.setAbsPS(sdd.getAbsPS() + absorbedDamage);
                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenPS(sdd.getPenPS() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // sdd.setPenPS(sdd.getPenPS() + damageAttacker);
                // abs.sc_LoggingEntry.incPS_DamagePenetrated(damageAttacker);
            }
        }

        return currReduction;
    }

    protected double targetFireIgnore(ICombatUnit enemy) {
        if (enemy instanceof BattleShipNew) {
            BattleShipNew bsn = (BattleShipNew) enemy;

            float getOrgShieldPoints = bsn.getShields().getShield_PA() * bsn.getCount()
                    + bsn.getShields().getSharedShield_HU() * bsn.getCount()
                    + bsn.getShields().getShield_PS() * bsn.getCount();
            float getShieldPointsLeft = bsn.getShields().getSharedShield_PA() * bsn.getCount()
                    + bsn.getShields().getSharedShield_HU() * bsn.getCount() + bsn.getShields().getSharedShield_PS() * bsn.getCount();

            log.debug("OrgShieldPoints=" + getOrgShieldPoints + " left=" + getShieldPointsLeft);
            if (getShieldPointsLeft < 0) {
                getShieldPointsLeft = 0;
            }

            if (getOrgShieldPoints > 0f) {
                return (double) (1d / getOrgShieldPoints * getShieldPointsLeft);
            } else {
                return 1d;
            }
        } else {
            return 1d;
        }
    }

    /**
     * @return the alreadyFiredOn
     */
    public HashSet<CombatUnit> getAlreadyFiredOn() {
        return alreadyFiredOn;
    }
}
