package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.framework.dao.DAOFactory;

public class CHasResearched extends AbstractCondition {

    private ParameterEntry researchValue;
    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);

    public CHasResearched(ParameterEntry researchValue) {
        this.researchValue = researchValue;
    }

    public boolean checkCondition(int userId, int conditionToTitleId) {
        int researchId = Integer.parseInt(researchValue.getParamValue().getValue());
        if (prDAO.findBy(userId, researchId) != null) {
            return true;
        } else {
            return false;
        }
    }
}
