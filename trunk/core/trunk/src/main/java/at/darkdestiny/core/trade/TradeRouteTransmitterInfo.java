/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.trade;

/**
 *
 * @author Stefan
 */
public class TradeRouteTransmitterInfo {
    private int totalCapStart;
    private int totalCapTarget;
    private int planetIdStart;
    private int planetIdTarget;
    private int usedCapStart;
    private int usedCapTarget;
    private int usageThisRoute;

    public int getTotalCapStart() {
        return totalCapStart;
    }

    public void setTotalCapStart(int totalCapStart) {
        this.totalCapStart = totalCapStart;
    }

    public int getTotalCapTarget() {
        return totalCapTarget;
    }

    public void setTotalCapTarget(int totalCapTarget) {
        this.totalCapTarget = totalCapTarget;
    }

    public int getPlanetIdStart() {
        return planetIdStart;
    }

    public void setPlanetIdStart(int planetIdStart) {
        this.planetIdStart = planetIdStart;
    }

    public int getPlanetIdTarget() {
        return planetIdTarget;
    }

    public void setPlanetIdTarget(int planetIdTarget) {
        this.planetIdTarget = planetIdTarget;
    }

    public int getUsedCapStart() {
        return usedCapStart;
    }

    public void setUsedCapStart(int usedCapStart) {
        this.usedCapStart = usedCapStart;
    }

    public int getUsedCapTarget() {
        return usedCapTarget;
    }

    public void setUsedCapTarget(int usedCapTarget) {
        this.usedCapTarget = usedCapTarget;
    }

    public int getUsageThisRoute() {
        return usageThisRoute;
    }

    public void setUsageThisRoute(int usageThisRoute) {
        this.usageThisRoute = usageThisRoute;
    }
}
