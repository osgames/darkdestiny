/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.core.enumeration.EConstructionProdType;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.enumeration.EEconomyType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;
import com.google.common.base.Strings;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "construction")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Construction extends Model<Construction> implements ITechFunctions {

    public static final int PROD_TYPE_GENERAL = 0;
    public static final int PROD_TYPE_AGRICULTURE = 1;
    public static final int PROD_TYPE_INDUSTRY = 2;
    public static final int PROD_TYPE_ENERGY = 3;
    public static final int PROD_TYPE_RESEARCH = 4;
    public static final int ID_COLONYBASE = 2;
    public static final int ID_LOCALADMINISTRATION = 3;
    public static final int ID_PLANETADMINISTRATION = 4;
    public static final int ID_PLANETGOVERNMENT = 5;
    public static final int ID_HYDROPONICFARM = 12;
    public static final int ID_SMALLAGRICULTUREFARM = 13;
    public static final int ID_BIGAGRICULTUREFARM = 14;
    public static final int ID_AGRICULTURESTORAGE = 15;
    public static final int ID_AGROCOMPLEX = 16;
    public static final int ID_SMALLIRONMINE = 17;
    public static final int ID_BIGIRONMINE = 18;
    public static final int ID_COMPLEXIRONMINE = 19;
    public static final int ID_STEELFACTORY = 20;
    public static final int ID_STEELCOMPLEX = 21;
    public static final int ID_SMALLRESSOURCESTORAGE = 22;
    public static final int ID_BIGRESSOURCESTORAGE = 23;
    public static final int ID_NUCLEARPOWERPLANT = 24;
    public static final int ID_FUSIONPOWERPLANT = 25;
    public static final int ID_STEELCOMPACTOR = 26;
    public static final int ID_RESEARCHLABORATORY = 27;
    public static final int ID_PLANETARYLASERCANNON = 28;
    public static final int ID_PLANETARYMISSLEBASE = 29;
    public static final int ID_PLANETARYSHIPYARD = 30;
    public static final int ID_MODULEFACTORY = 31;
    public static final int ID_BARRACKS = 32;
    public static final int ID_ORBITAL_YNKELONIUMMINE = 33;
    public static final int ID_ORBITAL_COLONY = 34;
    public static final int ID_ORBITAL_SHIPYARD = 35;
    public static final int ID_ORBITAL_ACCOMODATIONUNIT = 36;
    public static final int ID_ORBITAL_RESSOURCESTORAGE = 37;
    public static final int ID_ORBITAL_HYDROPONDICFARM = 38;
    public static final int ID_HOWALGONIUMMINE = 39;
    public static final int ID_ACCOMODATIONUNIT = 40;
    public static final int ID_SCANNERPHALANX = 41;
    public static final int ID_HYPERSPACESCANNER = 42;
    public static final int ID_HYPERRESSOURCESTORAGE = 43;
    public static final int ID_ORBITAL_HYPERRESSOURCESTORAGE = 44;
    public static final int ID_ORBITAL_HOWALGONIUMMINE = 45;
    public static final int ID_PLANETARY_POSITRONCOMPUTER = 46;
    public static final int ID_PLANETARY_INTERVALCANNON = 47;
    public static final int ID_ORBITAL_FOODSTORAGE = 48;
    public static final int ID_INTELLIGENCEHEADQUATERS = 60;
    public static final int ID_OBSERVATORY = 61;
    public static final int ID_RECREATIONAL_PARK = 90;
    public static final int ID_CIVILSPACEPORT = 100;
    public static final int ID_INTERSTELLARTRADINGPOST = 101;
    public static final int ID_MILITARYSPACEPORT = 102;
    public static final int ID_TRANSMITTER = 103;
    public static final int ID_PLANETARY_HUESHIELD = 200;
    public static final int ID_PLANETARY_PARATRONSHIELD = 201;
    public static final int ID_HYPERTROP = 202;
    public static final int ID_SOLARPLANT = 300;
    public static final int ID_SUNTRANSMITTER_CONTROLLER = 1000;
    public static final int ID_SUNTRANSMITTER_FORTRESS = 1001;
    public static final int ID_MEGACITY = 80;
    public static final int ID_PLANETARY_FORTRESS = 83;
    public static final int ID_AGRICULTURAL_DISTRIBUTION = 84;
    public static final int ID_MILITARY_OUTPOST = 50;

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("infotext")
    private String infotext;
    @FieldMappingAnnotation("typplanet")
    private String typplanet;
    @FieldMappingAnnotation("buildtime")
    private Integer buildtime;
    @FieldMappingAnnotation("type")
    @IndexAnnotation(indexName = "type")
    private EConstructionType type;
    /*
     @FieldMappingAnnotation("type")
     @IndexAnnotation(indexName = "type")
     private Integer type;*/
    @FieldMappingAnnotation("orderNo")
    private Integer orderNo;
    @FieldMappingAnnotation("population")
    private Long population;
    @FieldMappingAnnotation("prodType")
    private EConstructionProdType prodType;
    @FieldMappingAnnotation("hp")
    private Integer hp;
    @FieldMappingAnnotation("designId")
    private Integer designId;
    @FieldMappingAnnotation("workers")
    private Long workers;
    @FieldMappingAnnotation("surface")
    private Integer surface;
    @FieldMappingAnnotation("uniquePerPlanet")
    private Boolean uniquePerPlanet;
    @FieldMappingAnnotation("uniquePerSystem")
    private Boolean uniquePerSystem;
    @FieldMappingAnnotation("isMine")
    private Integer isMine;
    @FieldMappingAnnotation("specialBuildingId")
    private Integer specialBuildingId;
    @FieldMappingAnnotation("minPop")
    private Integer minPop;
    @FieldMappingAnnotation("image")
    private String image;
    @FieldMappingAnnotation("destructible")
    private Boolean destructible;
    @FieldMappingAnnotation("idleable")
    private Boolean idleable;
    @FieldMappingAnnotation("levelable")
    @DefaultValue(value = "0")
    private Boolean levelable;
    @FieldMappingAnnotation("econType")
    private EEconomyType econType;
    @FieldMappingAnnotation("isVisible")
    private Boolean visible;


    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the infotext
     */
    public String getInfotext() {
        return infotext;
    }

    /**
     * @param infotext the infotext to set
     */
    public void setInfotext(String infotext) {
        this.infotext = infotext;
    }

    /**
     * @return the typplanet
     */
    public String getTypplanet() {
        return typplanet;
    }

    /**
     * @param typplanet the typplanet to set
     */
    public void setTypplanet(String typplanet) {
        this.typplanet = typplanet;
    }

    /**
     * @return the buildtime
     */
    public Integer getBuildtime() {
        return buildtime;
    }

    /**
     * @param buildtime the buildtime to set
     */
    public void setBuildtime(Integer buildtime) {
        this.buildtime = buildtime;
    }

    /**
     * @return the type
     */
    public EConstructionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EConstructionType type) {
        this.type = type;
    }

    /**
     * @return the orderNo
     */
    public Integer getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo the orderNo to set
     */
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return the population
     */
    public Long getPopulation() {
        return population;
    }

    /**
     * @param population the population to set
     */
    public void setPopulation(Long population) {
        this.population = population;
    }

    /**
     * @return the prodType
     */
    public EConstructionProdType getProdType() {
        return prodType;
    }

    /**
     * @param prodType the prodType to set
     */
    public void setProdType(EConstructionProdType prodType) {
        this.prodType = prodType;
    }

    /**
     * @return the hp
     */
    public Integer getHp() {
        return hp;
    }

    /**
     * @param hp the hp to set
     */
    public void setHp(Integer hp) {
        this.hp = hp;
    }

    /**
     * @return the designId
     */
    public Integer getDesignId() {
        return designId;
    }

    /**
     * @param designId the designId to set
     */
    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    /**
     * @return the workers
     */
    public Long getWorkers() {
        return workers;
    }

    /**
     * @param workers the workers to set
     */
    public void setWorkers(Long workers) {
        this.workers = workers;
    }

    /**
     * @return the surface
     */
    public Integer getSurface() {
        return surface;
    }

    /**
     * @param surface the surface to set
     */
    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    /**
     * @return the uniquePerPlanet
     */
    public Boolean isUniquePerPlanet() {
        return uniquePerPlanet;
    }

    /**
     * @param uniquePerPlanet the uniquePerPlanet to set
     */
    public void setUniquePerPlanet(Boolean uniquePerPlanet) {
        this.uniquePerPlanet = uniquePerPlanet;
    }

    /**
     * @return the uniquePerSystem
     */
    public Boolean isUniquePerSystem() {
        return uniquePerSystem;
    }

    /**
     * @param uniquePerSystem the uniquePerSystem to set
     */
    public void setUniquePerSystem(Boolean uniquePerSystem) {
        this.uniquePerSystem = uniquePerSystem;
    }

    /**
     * @return the isMine
     */
    public Integer getIsMine() {
        return isMine;
    }

    /**
     * @param isMine the isMine to set
     */
    public void setIsMine(Integer isMine) {
        this.isMine = isMine;
    }

    /**
     * @return the specialBuildingId
     */
    public Integer getSpecialBuildingId() {
        return specialBuildingId;
    }

    /**
     * @param specialBuildingId the specialBuildingId to set
     */
    public void setSpecialBuildingId(Integer specialBuildingId) {
        this.specialBuildingId = specialBuildingId;
    }

    /**
     * @return the minPop
     */
    public Integer getMinPop() {
        return minPop;
    }

    /**
     * @param minPop the minPop to set
     */
    public void setMinPop(Integer minPop) {
        this.minPop = minPop;
    }

    public int getTechId() {
        return id;
    }

    public String getTechName() {
        return name;
    }

    public String getTechDescription() {
        return infotext;
    }

    /**
     * @return the image
     */
    public String getImage() {
        if(Strings.isNullOrEmpty(image)){
            return "placeholder.PNG";
        }
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the destructible
     */
    public Boolean isDestructible() {
        return destructible;
    }

    /**
     * @return the idleable
     */
    public Boolean isIdleable() {
        return idleable;
    }

    /**
     * @param destructible the destructible to set
     */
    public void setDestructible(Boolean destructible) {
        this.destructible = destructible;
    }

    /**
     * @param idleable the idleable to set
     */
    public void setIdleable(Boolean idleable) {
        this.idleable = idleable;
    }

    /**
     * @return the levelable
     */
    public Boolean getLevelable() {
        return levelable;
    }

    /**
     * @param levelable the levelable to set
     */
    public void setLevelable(Boolean levelable) {
        this.levelable = levelable;
    }

    /**
     * @return the econType
     */
    public EEconomyType getEconType() {
        return econType;
    }

    /**
     * @param econType the econType to set
     */
    public void setEconType(EEconomyType econType) {
        this.econType = econType;
    }

    /**
     * @return the visible
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
}
