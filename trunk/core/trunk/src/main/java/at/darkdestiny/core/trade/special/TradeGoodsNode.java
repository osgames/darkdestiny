/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.trade.special;

import at.darkdestiny.core.tools.graph.DefaultNode;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class TradeGoodsNode extends DefaultNode {

    private int systemId;
    List<Integer> planetIds = Lists.newArrayList();
    Map<Integer, ETradeNodeType> planetTypes = Maps.newHashMap();
    private int productionValue = 0;
    private int consumptionValue = 0;
    private ArrayList<TransferringEntry> routes = new ArrayList<TransferringEntry>();
    private Color overrideColor = null;
    
    public TradeGoodsNode(int systemId, int x, int y) {
        super(String.valueOf(systemId), x, y);
        this.systemId = systemId;
    }    
    
    /**
     * @return the systemId
     */
    public int getSystemId() {
        return systemId;
    }

    public void addRoute(TransferringEntry te) {
        getRoutes().add(te);
    }
    
    public int getFreeProduction() {
        int used = 0;
        
        for (TransferringEntry te : getRoutes()) {
            if (this.equals(te.getStartNode())) {
                used += te.getQuantity();
            }
        }
        
        return productionValue - used;
    }
    
    public int getFreeConsumption() {
        int used = 0;
        
        for (TransferringEntry te : getRoutes()) {
            // System.out.println("Check consumption " + this.toString() + " vs " + te.getTargetNode().toString());
            if (this.equals(te.getTargetNode())) {
                used += te.getQuantity();
            }
            // System.out.println("USED: " + used);
        }
        
        return consumptionValue - used;        
    }
    
    public void addPlanetId(int planetId, ETradeNodeType type) {
        if (!planetIds.contains(planetId)) {
            planetIds.add(planetId);
        }
        if (!planetTypes.containsKey(planetId)) {
            planetTypes.put(planetId, type);
        }
    }

    /**
     * @return the productionValue
     */
    public int getProductionValue() {
        return productionValue;
    }

    /**
     * @param productionValue the productionValue to set
     */
    public void setProductionValue(int productionValue) {
        this.productionValue = productionValue;
    }

    /**
     * @return the consumptionValue
     */
    public int getConsumptionValue() {
        return consumptionValue;
    }

    /**
     * @param consumptionValue the consumptionValue to set
     */
    public void setConsumptionValue(int consumptionValue) {
        this.consumptionValue = consumptionValue;
    }

    /**
     * @return the overrideColor
     */
    public Color getOverrideColor() {
        return overrideColor;
    }

    /**
     * @param overrideColor the overrideColor to set
     */
    public void setOverrideColor(Color overrideColor) {
        this.overrideColor = overrideColor;
    }

    /**
     * @return the routes
     */
    public ArrayList<TransferringEntry> getRoutes() {
        return routes;
    }

    public enum ETradeNodeType {

        INDUSTRIAL, TRADE, MEGACITY;
    }

    public boolean isTradePlanet() {
        boolean foundTrade = false;
        
        for (Map.Entry<Integer, ETradeNodeType> entry : planetTypes.entrySet()) {
            if (entry.getValue().equals(ETradeNodeType.INDUSTRIAL)) {
                // foundIndustrial = true;
            } else if (entry.getValue().equals(ETradeNodeType.MEGACITY)) {
                // foundMegaCity = true;
            } else if (entry.getValue().equals(ETradeNodeType.TRADE)) {
                foundTrade = true;
            }                
        }    
        
        return foundTrade;
    }
    
    @Override
    public String toString() {
        boolean foundIndustrial = false;
        boolean foundTrade = false;
        boolean foundMegaCity = false;

        for (Map.Entry<Integer, ETradeNodeType> entry : planetTypes.entrySet()) {
            if (entry.getValue().equals(ETradeNodeType.INDUSTRIAL)) {
                foundIndustrial = true;
            } else if (entry.getValue().equals(ETradeNodeType.MEGACITY)) {
                foundMegaCity = true;
            } else if (entry.getValue().equals(ETradeNodeType.TRADE)) {
                foundTrade = true;
            }
        }        
        
        StringBuffer buffer = new StringBuffer();
        
        if (foundIndustrial) buffer.append("I");
        if (foundMegaCity) buffer.append("M");
        if (foundTrade) buffer.append("T");
        
        if (productionValue > 0) {
            buffer.append(" +" + productionValue);
        }
        if (consumptionValue > 0) {
            buffer.append(" -" + consumptionValue);
        }        
        
        /*
        buffer.append("s:");
        buffer.append(systemId);
        for (Map.Entry<Integer, ETradeNodeType> entry : planetTypes.entrySet()) {
            buffer.append(" ");
            buffer.append(entry.getKey());
            buffer.append("-");
            buffer.append(entry.getValue().toString().subSequence(0, 1));
        }
        */
        return buffer.toString();
    }

    public String toColor() {
        if (overrideColor != null) {
            return "white";
        }
        
        String color = "#EFEFEF";
        boolean foundIndustrial = false;
        boolean foundTrade = false;
        boolean foundMegaCity = false;

        for (Map.Entry<Integer, ETradeNodeType> entry : planetTypes.entrySet()) {
            if (entry.getValue().equals(ETradeNodeType.INDUSTRIAL)) {
                foundIndustrial = true;
            } else if (entry.getValue().equals(ETradeNodeType.MEGACITY)) {
                foundMegaCity = true;
            } else if (entry.getValue().equals(ETradeNodeType.TRADE)) {
                foundTrade = true;
            }
        }
        if (foundIndustrial && !foundMegaCity && !foundTrade) {
            color = "#FFFFAA";
        }
        if (foundMegaCity && !foundTrade && !foundIndustrial) {
            color = "FFAAAA";
        }
        if (foundTrade && !foundMegaCity && !foundIndustrial) {
            color = "AAAAAA";
        }
        if (foundIndustrial && foundMegaCity && !foundTrade) {
            color = "orange";
        }
        return color;
    }
}
