/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.scanner;

/**
 *
 * @author Stefan
 */
public class SystemDesc extends AbstractCoordinate {
    public final int id;
    public final long popscore;
    public final int statscore;
    
    public SystemDesc(int id, int x, int y, long popscore, int statscore) {
        super(x,y);
        this.id = id;
        this.popscore = popscore;
        this.statscore = statscore;
    }
}
