/*
 * UniverseCreation.java
 *
 * Created on 27. April 2007, 11:14
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.enumeration.EGalaxyType;
import at.darkdestiny.core.enumeration.EPlanetDensity;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.ERessourceCostType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.result.GalaxyCreationResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.setup.AbstractGalaxy;
import at.darkdestiny.core.setup.IrregularGalaxy;
import at.darkdestiny.core.setup.RingGalaxy;
import at.darkdestiny.core.setup.Spiralgalaxy;
import at.darkdestiny.framework.QueryKey;
import at.darkdestiny.framework.QueryKeySet;
import at.darkdestiny.framework.access.DbConnect;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class UniverseCreation{
    // Do not change: code is not properly adjusted to handle other values

    private static int WIDTH;
    private static int HEIGHT;

    public static GalaxyCreationResult createNewGalaxy(int width, int height, boolean destroyDBEntries, EPlanetDensity pDensity, EGalaxyType gType, int sysPerUser) {
        return createNewGalaxy(width, height, destroyDBEntries, pDensity, gType, sysPerUser, width / 2, height / 2);
    }

    /**
     * Creates a new instance of UniverseCreation
     */
    public UniverseCreation(int width, int height) {
        WIDTH = width;
        HEIGHT = height;
    }

    public static void destroyUniverse() {
        Service.allianceDAO.removeAll();
        Service.allianceMessageDAO.removeAll();
        Service.allianceMessageReadDAO.removeAll();
        Service.allianceMemberDAO.removeAll();
        Service.allianceRankDAO.removeAll();
        Service.userDAO.removeAll();
        Service.viewTableDAO.removeAll();
        Service.allianceBoardDAO.removeAll();
        Service.productionOrderDAO.removeAll();

        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("type", ERessourceCostType.SHIP));
        Service.ressourceCostDAO.removeAll(qks);

        Service.planetRessourceDAO.removeAll();
        Service.playerResearchDAO.removeAll();
        Service.planetDAO.removeAll();
        Service.planetConstructionDAO.removeAll();
        Service.userDataDAO.removeAll();
        Service.playerPlanetDAO.removeAll();
        Service.actionDAO.removeAll();
        Service.systemDAO.removeAll();
        Service.playerCategoryDAO.removeAll();
        Service.planetCategoryDAO.removeAll();
        Service.statisticDAO.removeAll();
        Service.researchProgressDAO.removeAll();
        Service.tradeOfferDAO.removeAll();
        Service.tradeRouteDAO.removeAll();
        Service.tradeRouteShipDAO.removeAll();
        Service.playerTroopDAO.removeAll();
        Service.loginTrackerDAO.removeAll();
        Service.planetLoyalityDAO.removeAll();
        Service.priceListDAO.removeAll();
        Service.priceListEntryDAO.removeAll();

        Service.territoryMapDAO.removeAll();
        Service.territoryMapPointDAO.removeAll();
        Service.territoryMapShareDAO.removeAll();

        Service.tradeLogDAO.removeAll();


        Service.messageDAO.removeAll();
        Service.tradePostDAO.removeAll();
        Service.tradePostShipDAO.removeAll();
        Service.shipDesignDAO.removeAll();
        Service.tradeFleetDAO.removeAll();
        Service.playerFleetDAO.removeAll();
        Service.fleetDetailDAO.removeAll();
        Service.fleetLoadingDAO.removeAll();
        Service.shipFleetDAO.removeAll();
        Service.planetDefenseDAO.removeAll();
        //  newsDAO.removeAll();
        Service.diplomacyRelationDAO.removeAll();
        Service.styleToUserDAO.removeAll();
        Service.creditDonationDAO.removeAll();
        Service.designModuleDAO.removeAll();
        Service.votingDAO.removeAll();
        Service.voteMetadataDAO.removeAll();
        Service.voteOptionDAO.removeAll();
        Service.votePermissionDAO.removeAll();
        Service.votingDetailDAO.removeAll();

        //Shortly b4 06
        Service.allianceBoardPermissionDAO.removeAll();
        Service.conditionToUserDAO.removeAll();
        Service.campaignToUserDAO.removeAll();
        Service.titleToUserDAO.removeAll();
        Service.activeBonusDAO.removeAll();
        Service.combatPlayerDAO.removeAll();
        Service.combatRoundDAO.removeAll();
        Service.groundCombatDAO.removeAll();
        Service.territoryEntryDAO.removeAll();
        Service.roundEntryDAO.removeAll();
        Service.filterDAO.removeUserFilters();
        Service.damagedShipsDAO.removeAll();
        Service.fleetFormationDAO.removeAll();
        Service.imperialStatisticDAO.removeAll();
        Service.notificationToUserDAO.removeAll();
        Service.planetLogDAO.removeAll();
        Service.tradeRouteDetailDAO.removeAll();
        Service.playerStatisticDAO.removeAll();
        Service.multiLogDAO.removeAll();
        Service.battleResultDAO.removeAll();
        Service.battleLogDAO.removeAll();
        Service.battleLogEntryDAO.removeAll();
        Service.noteDAO.removeAll();
        Service.userSettingsDAO.removeAll();
        Service.galaxyDAO.removeAll();
        Service.scrapResourceDAO.removeAll();
        Service.aiDAO.removeAll();
        Service.viewSystemDAO.removeAll();
        Service.sunTransmitterDAO.removeAll();
        Service.sunTransmitterRouteDAO.removeAll();
        Service.userRelationDAO.removeAll();
    }

    public static GalaxyCreationResult createNewGalaxy(int width, int height, boolean destroyDBEntries, EPlanetDensity pDensity, EGalaxyType gType, int sysDensity, int originX, int originY) {
        if (destroyDBEntries) {
            destroyUniverse();
        }

        AbstractGalaxy galaxy = null;
        if (gType.equals(EGalaxyType.spiral)) {
            galaxy = new Spiralgalaxy(width, height, destroyDBEntries, pDensity, sysDensity, originX, originY);
        } else if (gType.equals(EGalaxyType.ring)) {
            galaxy = new RingGalaxy(width, height, destroyDBEntries, pDensity, sysDensity, originX, originY);
        } else if (gType.equals(EGalaxyType.irregular)) {
            galaxy = new IrregularGalaxy(width, height, destroyDBEntries, pDensity, sysDensity, originX, originY);
        }

        WIDTH = width;
        HEIGHT = height;
        return galaxy.create();

    }

    public static GalaxyCreationResult createNewGalaxy(int width, int height, boolean destroyDBEntries) {
        return createNewGalaxy(width, height, destroyDBEntries, EPlanetDensity.middle, EGalaxyType.spiral, 30, width / 2, height / 2);
    }

    public static synchronized Planet rebuildPlanet(Planet p) {
        int avgTemp = 0;
        int diameter = 6000 + (int) (Math.random() * 14000);

        switch (p.getOrbitLevel()) {
            case 1:
                avgTemp = 590 + (int) (Math.random() * 100);
                break;
            case 2:
                avgTemp = 280 + (int) (Math.random() * 80);
                break;
            case 3:
                avgTemp = 130 + (int) (Math.random() * 60);
                break;
            case 4:
                avgTemp = 60 + (int) (Math.random() * 40);
                break;
            case 5:
                avgTemp = 30 + (int) (Math.random() * 20);
                break;
            case 6:
                avgTemp = 10 + (int) (Math.random() * 10);
                break;
            case 7:
                avgTemp = 0 + (int) (Math.random() * 10);
                break;
            case 8:
                avgTemp = -50 + (int) (Math.random() * 20);
                break;
            case 9:
                avgTemp = -95 + (int) (Math.random() * 30);
                break;
            case 10:
                avgTemp = -140 + (int) (Math.random() * 40);
                break;
            case 11:
                avgTemp = -185 + (int) (Math.random() * 50);
                break;
            case 12:
                avgTemp = -230 + (int) (Math.random() * 60);
                break;
        }

        if (p.getLandType().equalsIgnoreCase("M")) {
            avgTemp = 6 + (int) (Math.random() * 24);
        } else if (p.getLandType().equalsIgnoreCase("C")) {
            avgTemp = -5 + (int) (Math.random() * 10);
        } else if (p.getLandType().equalsIgnoreCase("G")) {
            avgTemp = 25 + (int) (Math.random() * 10);
        }

        p.setDiameter(diameter);
        p.setAvgTemp(avgTemp);
        Planet pNew = Service.planetDAO.update(p);

        resetPlanetRessources(p);

        return pNew;
    }

    private static void resetPlanetRessources(Planet p) {
        at.darkdestiny.core.model.PlanetRessource pr = new at.darkdestiny.core.model.PlanetRessource();
        pr.setPlanetId(p.getId());
        pr.setType(EPlanetRessourceType.PLANET);

        ArrayList<at.darkdestiny.core.model.PlanetRessource> pResList = Service.planetRessourceDAO.find(pr);
        for (at.darkdestiny.core.model.PlanetRessource prTmp : pResList) {
            Service.planetRessourceDAO.remove(prTmp);
        }

        long iron = 0;
        long ynkel = 0;
        long howal = 0;
        long cvEmb = 0;

        if (p.getLandType().equalsIgnoreCase("A")) {
            int randVal = (p.getDiameter() * 10) + (int) (Math.random() * (p.getDiameter() * 10));
            ynkel = randVal;
        } else if (p.getLandType().equalsIgnoreCase("B")) {
            int randVal = (p.getDiameter() * 10) + (int) (Math.random() * (p.getDiameter() * 10));
            ynkel = randVal;
        } else {
            long volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (p.getDiameter() / 2), 3)));

            int orbitLevelDiff = 6 - p.getOrbitLevel();
            if (orbitLevelDiff > 0) {
                iron = (long) (((float) volume / (float) 1000) * (1f + ((float) orbitLevelDiff / (float) 2.5)));
            } else if (orbitLevelDiff < 0) {
                iron = (long) (((float) volume / (float) 1000) / (1f + ((float) Math.abs(orbitLevelDiff) / (float) 2.5)));
            } else {
                iron = (long) ((float) volume / (float) 1000);
            }
        }

        boolean hasHowal = Math.random() < 0.2d;

        if (hasHowal) {
            boolean hasMuchHowal = Math.random() < 0.05d;

            if (hasMuchHowal) {
                howal = (int) (60000 + Math.random() * 250000);
            } else {
                howal = (int) (Math.random() * 30000);
            }
        }

        if (iron != 0) {
            at.darkdestiny.core.model.PlanetRessource prNew = new at.darkdestiny.core.model.PlanetRessource();
            prNew.setPlanetId(p.getId());
            prNew.setType(EPlanetRessourceType.PLANET);
            prNew.setRessId(Ressource.PLANET_IRON);
            prNew.setQty(iron);
            Service.planetRessourceDAO.add(prNew);
        }

        if (ynkel != 0) {
            at.darkdestiny.core.model.PlanetRessource prNew = new at.darkdestiny.core.model.PlanetRessource();
            prNew.setPlanetId(p.getId());
            prNew.setType(EPlanetRessourceType.PLANET);
            prNew.setRessId(Ressource.PLANET_YNKELONIUM);
            prNew.setQty(ynkel);
            Service.planetRessourceDAO.add(prNew);
        }

        if (howal != 0) {
            at.darkdestiny.core.model.PlanetRessource prNew = new at.darkdestiny.core.model.PlanetRessource();
            prNew.setPlanetId(p.getId());
            prNew.setType(EPlanetRessourceType.PLANET);
            prNew.setRessId(Ressource.PLANET_HOWALGONIUM);
            prNew.setQty(howal);
            Service.planetRessourceDAO.add(prNew);
        }
    }

    public static void addSystemToUniverse(int id, int x, int y, int userId, boolean systemAlreadyExists) throws Exception {
        Statement stmt = DbConnect.createStatement();

        if (!systemAlreadyExists) {
            stmt.execute("INSERT INTO system (id, name, posx, posy, visibility) VALUES (" + id + ",'System " + id + "'," + x + "," + y + "," + userId + ")");
        }

        // Check all user user viewtables and refresh system entries
        ArrayList<Integer> updateThisUsers = new ArrayList<Integer>();
        ResultSet rs = stmt.executeQuery("SELECT id FROM user WHERE id<>0 AND systemAssigned=1");
        Statement stmt2 = DbConnect.createStatement();

        while (rs.next()) {
            ResultSet rs2 = stmt2.executeQuery("SELECT systemid FROM viewtable" + rs.getInt(1) + " WHERE systemid=" + id + " LIMIT 1");
            if (rs2.next()) {
                updateThisUsers.add(rs.getInt(1));
                stmt2.execute("DELETE FROM viewtable" + rs.getInt(1) + " WHERE systemid=" + id);
            }
            rs2.close();
        }
        stmt2.close();

        int actPlanetId = 0;
        rs = stmt.executeQuery("SELECT MAX(id) FROM planet");
        if (rs.next()) {
            actPlanetId = rs.getInt(1);
        }

        // Calculate possibilty of planets
        double distanceToCenter = Math.sqrt(Math.pow((HEIGHT / 2d) - (double) y, 2d) + Math.pow((WIDTH / 2d) - (double) x, 2d));
        distanceToCenter = 100d / ((double) WIDTH / 2d) * distanceToCenter;

        double failChance = 0d;
        if (distanceToCenter <= 25d) { // Max fail 30%
            failChance = (30d / 25d * (25d - distanceToCenter)) / 100d;
        } else if (distanceToCenter >= 75d) { // Max fail 15%
            failChance = (15d / 25d * (100d - distanceToCenter)) / 100d;
        }

        int planetCount = 1 + (int) (Math.random() * 12);

        // Reduce planets due to fail chance
        for (int k = 0; k < planetCount; k++) {
            if (Math.random() < failChance) {
                planetCount--;
            }
        }

        int actualPlanetCount = 0;
        boolean[] map = new boolean[13];
        while (actualPlanetCount < planetCount) {
            int orbitLevel = 1 + (int) (Math.random() * 12);
            if (!map[orbitLevel]) {
                actualPlanetCount++;
                map[orbitLevel] = true;
                int diameter = 0;
                if (orbitLevel <= 7) {
                    int zz = 1 + (int) (Math.random() * 20);
                    if (zz == 5) {
                        diameter = 50000 + (int) (Math.random() * 150000);
                    }
                } else {
                    int zz = 1 + (int) (Math.random() * 2);
                    if (zz == 1) {
                        diameter = 50000 + (int) (Math.random() * 250000);
                    }
                }
                if (diameter == 0) {
                    diameter = 6000 + (int) (Math.random() * 14000);
                }
                String landType = "";
                if (diameter >= 50000 && diameter <= 150000) {
                    landType = "B";
                }
                if (diameter > 150000) {
                    landType = "A";
                }
                if (landType.equalsIgnoreCase("")) {
                    if (orbitLevel < 5) {
                        int zz = 1 + (int) (Math.random() * 4);
                        if (zz <= 2) {
                            landType = "J";
                        } else {
                            landType = "E";
                        }
                    }
                    int zz = 1 + (int) (Math.random() * 100);
                    if (orbitLevel == 5) {
                        if (zz < 50) {
                            landType = "G";
                        }
                        if (zz >= 50 && zz < 60) {
                            landType = "M";
                        }
                        if (zz >= 60 && zz < 75) {
                            landType = "C";
                        }
                        if (zz >= 75) {
                            landType = "E";
                        }
                    }

                    if (orbitLevel == 6) {
                        if (zz < 25) {
                            landType = "G";
                        }
                        if (zz >= 25 && zz < 50) {
                            landType = "M";
                        }
                        if (zz >= 50 && zz < 75) {
                            landType = "C";
                        }
                        if (zz >= 75) {
                            landType = "E";
                        }
                    }

                    if (orbitLevel == 7) {
                        if (zz < 15) {
                            landType = "G";
                        }
                        if (zz >= 15 && zz < 25) {
                            landType = "M";
                        }
                        if (zz >= 25 && zz < 75) {
                            landType = "C";
                        }
                        if (zz >= 75) {
                            landType = "E";
                        }
                    }

                    if (orbitLevel > 7) {
                        landType = "L";
                    }
                }

                // Bis hierher ist definiert:
                // OrbitLevel
                // Diameter (Durchmesser)
                // Typ

                int avgTemp = 0;
                switch (orbitLevel) {
                    case 1:
                        avgTemp = 590 + (int) (Math.random() * 100);
                        break;
                    case 2:
                        avgTemp = 280 + (int) (Math.random() * 80);
                        break;
                    case 3:
                        avgTemp = 130 + (int) (Math.random() * 60);
                        break;
                    case 4:
                        avgTemp = 60 + (int) (Math.random() * 40);
                        break;
                    case 5:
                        avgTemp = 30 + (int) (Math.random() * 20);
                        break;
                    case 6:
                        avgTemp = 10 + (int) (Math.random() * 10);
                        break;
                    case 7:
                        avgTemp = 0 + (int) (Math.random() * 10);
                        break;
                    case 8:
                        avgTemp = -50 + (int) (Math.random() * 20);
                        break;
                    case 9:
                        avgTemp = -95 + (int) (Math.random() * 30);
                        break;
                    case 10:
                        avgTemp = -140 + (int) (Math.random() * 40);
                        break;
                    case 11:
                        avgTemp = -185 + (int) (Math.random() * 50);
                        break;
                    case 12:
                        avgTemp = -230 + (int) (Math.random() * 60);
                        break;
                }

                if (landType.equalsIgnoreCase("M")) {
                    avgTemp = 6 + (int) (Math.random() * 24);
                } else if (landType.equalsIgnoreCase("C")) {
                    avgTemp = -5 + (int) (Math.random() * 10);
                } else if (landType.equalsIgnoreCase("G")) {
                    avgTemp = 25 + (int) (Math.random() * 10);
                }

                long iron = 0;
                long ynkel = 0;
                long howal = 0;
                long cvEmb = 0;

                if (landType.equalsIgnoreCase("A")) {
                    int randVal = (diameter * 10) + (int) (Math.random() * (diameter * 10));
                    ynkel = randVal;
                } else if (landType.equalsIgnoreCase("B")) {
                    int randVal = (diameter * 10) + (int) (Math.random() * (diameter * 10));
                    ynkel = randVal;
                } else {
                    long volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (diameter / 2), 3)));

                    int orbitLevelDiff = 6 - orbitLevel;
                    if (orbitLevelDiff > 0) {
                        iron = (long) (((float) volume / (float) 1000) * (1f + ((float) orbitLevelDiff / (float) 2.5)));
                    } else if (orbitLevelDiff < 0) {
                        iron = (long) (((float) volume / (float) 1000) / (1f + ((float) Math.abs(orbitLevelDiff) / (float) 2.5)));
                    } else {
                        iron = (long) ((float) volume / (float) 1000);
                    }
                }

                boolean hasHowal = Math.random() < 0.2d;

                if (hasHowal) {
                    boolean hasMuchHowal = Math.random() < 0.05d;

                    if (hasMuchHowal) {
                        howal = (int) (60000 + Math.random() * 250000);
                    } else {
                        howal = (int) (Math.random() * 30000);
                    }
                }

                PreparedStatement pstmt = DbConnect.prepareStatement(
                        "insert into planet (id,systemId,landType,atmosphereType,avgTemp,diameter,orbitlevel,population,"
                        + "energy, planetId, growthBonus, researchBonus, productionBonus) "
                        + "values (?,?,?,'Z',?,?,?,0,0,0,0,0,0)", false);

                PreparedStatement ress = DbConnect.prepareStatement(
                        "insert into planetressource (planetId,type,ressId,qty) "
                        + "value (?,?,?,?)", false);

                actPlanetId++;

                stmt2 = DbConnect.createStatement();
                stmt2.execute("DELETE FROM planetressource WHERE planetId=" + actPlanetId);
                stmt2.close();

                pstmt.setInt(1, actPlanetId);
                pstmt.setInt(2, id);
                pstmt.setString(3, landType);
                pstmt.setInt(4, avgTemp);
                pstmt.setInt(5, diameter);
                pstmt.setInt(6, orbitLevel);
                // pstmt.setLong(7,iron);
                // pstmt.setLong(8,ynkel);
                // pstmt.setLong(9,howal);
                // pstmt.setLong(10,cvEmb);

                pstmt.execute();

                if (iron != 0) {
                    ress.setInt(1, actPlanetId);
                    ress.setObject(2, EPlanetRessourceType.PLANET);
                    ress.setInt(3, Ressource.PLANET_IRON);
                    ress.setLong(4, iron);
                    // log.debug("EXECUTE: insert into planetressources (planetId,type,ressId,qty) value ("+actPlanetId+","+PlanetTable.PLANET_RESSOURCE+","+GameConstants.RES_PLANET_IRON+","+iron+")");
                    ress.execute();
                }
                if (ynkel != 0) {
                    ress.setInt(1, actPlanetId);
                    ress.setObject(2, EPlanetRessourceType.PLANET);
                    ress.setInt(3, Ressource.PLANET_YNKELONIUM);
                    ress.setLong(4, ynkel);
                    // log.debug("EXECUTE: insert into planetressources (planetId,type,ressId,qty) value ("+actPlanetId+","+PlanetTable.PLANET_RESSOURCE+","+GameConstants.RES_PLANET_YNKELONIUM+","+ynkel+")");
                    ress.execute();
                }
                if (howal != 0) {
                    ress.setInt(1, actPlanetId);
                    ress.setObject(2, EPlanetRessourceType.PLANET);
                    ress.setInt(3, Ressource.PLANET_HOWALGONIUM);
                    ress.setLong(4, howal);
                    // log.debug("EXECUTE: insert into planetressources (planetId,type,ressId,qty) value ("+actPlanetId+","+PlanetTable.PLANET_RESSOURCE+","+GameConstants.RES_PLANET_HOWALGONIUM+","+howal+")");
                    ress.execute();
                }

                for (Integer uid : updateThisUsers) {
                    stmt.execute("INSERT INTO viewtable" + uid + " (systemid, planetid) VALUES (" + id + "," + actPlanetId + ")");
                }

                pstmt.close();
                ress.close();
            }
        }

        // Check if a M class planet was created
        boolean mFound = false;
        rs = stmt.executeQuery("SELECT landType FROM planet WHERE systemId=" + id);
        while (rs.next()) {
            if (rs.getString(1).equalsIgnoreCase("M")) {
                mFound = true;
            }
        }

        if (!mFound) {
            // Create a M planet on orbitLevel 6
            stmt.execute("DELETE FROM planet WHERE systemId=" + id + " AND orbitlevel=6");

            rs = stmt.executeQuery("SELECT MAX(id) FROM planet");
            if (rs.next()) {
                actPlanetId = rs.getInt(1) + 1;
            }

            // Calculate iron and howalgonium
            long volume = (long) ((float) (4 / 3) * Math.PI * (float) (Math.pow((float) (12500 / 2), 3)));

            long iron = (long) ((float) volume / (float) 1000);
            long howal = 0;

            boolean hasHowal = Math.random() < 0.2d;

            if (hasHowal) {
                boolean hasMuchHowal = Math.random() < 0.05d;

                if (hasMuchHowal) {
                    howal = (int) (60000 + Math.random() * 250000);
                } else {
                    howal = (int) (Math.random() * 30000);
                }
            }

            stmt.execute("INSERT INTO planet (id,systemId,landType,atmosphereType,avgTemp,diameter,orbitlevel,population,"
                    + "energy, planetId, growthBonus, researchBonus, productionBonus) "
                    + "values (" + actPlanetId + "," + id + ",'M','Z',15,12500,6,5000000000,0,0,0,0,0)");

            if (iron > 0) {
                stmt.execute(
                        "insert into planetressource (planetId,type,ressId,qty) "
                        + "value (" + actPlanetId + "," + EPlanetRessourceType.PLANET + "," + Ressource.PLANET_IRON + "," + iron + ")");
            }

            if (howal > 0) {
                stmt.execute(
                        "insert into planetressource (planetId,type,ressId,qty) "
                        + "value (" + actPlanetId + "," + EPlanetRessourceType.PLANET + "," + Ressource.PLANET_HOWALGONIUM + "," + howal + ")");
            }
        }

        stmt.close();
    }
}
