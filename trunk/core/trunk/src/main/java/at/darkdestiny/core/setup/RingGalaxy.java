/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.setup;

 import at.darkdestiny.core.enumeration.EPlanetDensity;import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.result.GalaxyCreationResult;
import at.darkdestiny.core.result.SystemGenerationResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.enumeration.EPlanetDensity;
import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.result.GalaxyCreationResult;
import at.darkdestiny.core.result.SystemGenerationResult;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Admin
 */
public class RingGalaxy extends AbstractGalaxy {

    private static final Logger log = LoggerFactory.getLogger(RingGalaxy.class);

    public RingGalaxy(int width, int height, boolean destroyDBEntries, EPlanetDensity pDensity, int sysPerUser, int originX, int originY) {
        super(width, height, destroyDBEntries, pDensity, sysPerUser, originX, originY);
    }

    public GalaxyCreationResult create() {
        log.debug("Create Spiral Galaxy with width/height : " + width + "/" + height);
        log.debug("Create Spiral Galaxy with originX/originY : " + originX + "/" + originY);


        try {
            ArrayList<Planet> planetList = new ArrayList<Planet>();
            ArrayList<at.darkdestiny.core.model.System> systemList = new ArrayList<at.darkdestiny.core.model.System>();
            ArrayList<at.darkdestiny.core.model.PlanetRessource> ressList = new ArrayList<at.darkdestiny.core.model.PlanetRessource>();
            int actSystemId = Service.systemDAO.findLastId();
            int startSystem = actSystemId;

            // int[][] galaxy = new int[WIDTH][HEIGHT];
            HashMap<Integer, HashSet<Integer>> genGalaxy = new HashMap<Integer, HashSet<Integer>>();
            //Zufallsdrehung
            //   int randomArm = (int) Math.round(Math.random() * 2);
            //  randomArm = randomArm + 3;

            int r = (int) (height / 4d);

            int repeatValue = (int)(Math.round(5d * (height/2000d)));


            if(repeatValue < 1){
                repeatValue = 1;
            }
            for (int i = 0; i < 360; i++) {

                //calculate a few random values for that point on the circle
                for (int j = 0; j < repeatValue; j++) {

                    double variance = Math.random() * 1d;

                    double spawnChance = Math.random() * 1d;
                    double ran = Math.random() * 1d;

                    //higher spawn chance if nearer to the ring

               //     java.lang.System.out.println("spawnchance : " + spawnChance + " vairanceValue :" + (1d - variance) + " ranValue : " + ((1d - variance) * ran));
                //    spawnChance += (1d - variance);
                    if (spawnChance > 0.1d && (1d - variance) * ran > 0.2) {

                        int distance = (int) (Math.round(r * variance));

                        double ranValue = Math.random() * 1d;
                        int x = 0;
                        int y = 0;
                       // java.lang.System.out.println("r : " + r + " dist : " + distance + " origin : " + (originX));
                        if (ranValue > 0.5d) {
                            x = (int) ((r + distance) * Math.cos(i));
                            y = (int) ((r + distance) * Math.sin(i));
                        } else {
                            x = (int) ((r - distance) * Math.cos(i));
                            y = (int) ((r - distance) * Math.sin(i));
                        }
                     //   java.lang.System.out.println("Galaxy at : " + x + " - " + y);

                    //    java.lang.System.out.println("Shifting by : " + originX);
                        x += originX;
                        y += originY;

                     //   java.lang.System.out.println("Galaxy at : " + x + " - " + y);
                        if (genGalaxy.containsKey(x)) {
                            if (!genGalaxy.get(x).contains(y)) {
                                genGalaxy.get(x).add(y);
                            }
                        } else {
                            HashSet<Integer> ySet = new HashSet<Integer>();
                            ySet.add(y);
                            genGalaxy.put(x, ySet);
                        }
                    }
                }
            }


            for (int i = (int) Math.round(originX - height / 2d); i < originX + height / 2d; i++) {
                for (int j = (int) Math.round(originY - width / 2d); j < originY + width / 2d; j++) {
                    if (!genGalaxy.containsKey(i)) {
                        continue;
                    }
                    if (!genGalaxy.get(i).contains(j)) {
                        continue;
                    }
                    int posx = i;
                    int posy = j;

                    //Generate a new system
                    // Calculate possibilty of planets
                    double distanceToCenter = Math.sqrt(Math.pow((originX) - (double) posx, 2d) + Math.pow((originY) - (double) posy, 2d));
                    distanceToCenter = 100d / ((double) originX) * distanceToCenter;

                    SystemGenerationResult sgr = generateSystem(posx, posy, distanceToCenter, actSystemId, systemList, planetList, ressList, false);
                    if (sgr.isSystemGenerated()) {
                        actSystemId += sgr.getSystemsGenerated();
                    }
                }
            }
            Galaxy g = new Galaxy();
            g.setStartSystem(startSystem);
            g.setEndSystem(actSystemId);
            g.setOriginX(originX);
            g.setOriginY(originY);
            g.setHeight(height);
            g.setWidth(width);
           // Service.galaxyDAO.add(g);
           // Service.systemDAO.insertAll(systemList);
          //  Service.planetDAO.insertAll(planetList);
          //  Service.planetRessourceDAO.insertAll(ressList);

            return new GalaxyCreationResult(g, ressList, planetList, systemList);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in planet creation", e);
        }
        log.debug("Done Creating Galaxy");
        return null;
    }
}
