/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class AIDebugBuffer {

    private static TreeMap<String, TreeMap<Long, AIDebugEntry>> entries = new TreeMap<String, TreeMap<Long, AIDebugEntry>>();

    public static synchronized void log(AIThread thread, AIDebugLevel level, String message) {
        log(thread, new AIDebugEntry(level, message));
    }

    public static ArrayList<String> getAIs() {
        ArrayList<String> result = Lists.newArrayList();
        for (Map.Entry<String, TreeMap<Long, AIDebugEntry>> entry : entries.entrySet()) {
            result.add(entry.getKey());
        }
        return result;
    }

    public static synchronized void log(AIThread thread, AIDebugEntry entry) {
        long time = System.currentTimeMillis();

        TreeMap<Long, AIDebugEntry> threadEntries = entries.get(thread.getName());

        if (threadEntries == null) {
            threadEntries = new TreeMap<Long, AIDebugEntry>();
        }
        while (threadEntries.containsKey(time)) {
            time++;
        }
        threadEntries.put(time, entry);
        entries.put(thread.getName(), threadEntries);
    }

    public static TreeMap<Long, TreeMap<String, AIDebugEntry>> getEntries(String aiThreadName, String debugLevel) {

        System.out.println("aiThreadName : " + aiThreadName);
        System.out.println("debugLevel : " + debugLevel);
        TreeMap<Long, TreeMap<String, AIDebugEntry>> result = new TreeMap<Long, TreeMap<String, AIDebugEntry>>();
        for (Map.Entry<String, TreeMap<Long, AIDebugEntry>> entry : entries.entrySet()) {
            if (!aiThreadName.equals("") && !aiThreadName.equals(entry.getKey())) {
                continue;
            }
            for (Map.Entry<Long, AIDebugEntry> ai : entry.getValue().entrySet()) {
                if (!debugLevel.equals("") && !debugLevel.equals(ai.getValue().getLevel().toString())) {
                    continue;
                }
                long time = ai.getKey();
                while (result.containsKey(time)) {
                    time++;
                }
                TreeMap<String, AIDebugEntry> value = new TreeMap<String, AIDebugEntry>();
                value.put(entry.getKey(), ai.getValue());
                result.put(time, value);
            }
        }
        return result;
    }

    public static TreeMap<Long, AIDebugEntry> getEntries(AIThread thread) {
        TreeMap<Long, AIDebugEntry> result = Maps.newTreeMap();

        for (Map.Entry<Long, AIDebugEntry> entry : entries.get(thread).entrySet()) {
            long time = entry.getKey();
            while (result.containsKey(time)) {
                time++;
            }
            result.put(time, entry.getValue());
        }

        return result;
    }
}
