 /*
 * TableEntity.java
 *
 * Created on 10. Mai 2007, 13:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.core.view.html;

import java.util.*;

/**
 *
 * @author Horst
 */
public class table extends htmlObject {

    private ArrayList<tr> trs;
    private ArrayList<table> tables;
    private style style1;

    /** Creates a new instance of TableEntity */
    public table() {

        trs = new ArrayList<tr>();
        tables = new ArrayList<table>();
        properties = "";
        style1 = new style();
        data = "";
    }

    public void addTr(tr tr) {

        trs.add(tr);

    }

    public ArrayList<tr> getTrs() {
        return trs;
    }

    public void setTrs(ArrayList<tr> trs) {
        this.trs = trs;
    }

    public void addTable(table table) {

        tables.add(table);

    }

    public ArrayList<table> getTables() {
        return tables;
    }

    public void setTables(ArrayList<table> tables) {
        this.tables = tables;
    }

    public String toString() {

        String returnString = "";
         if (!style1.toString().equals("")) {
             data += style1.toString();
        }
        for (int i = 0; i < trs.size(); i++) {
            data += " " + trs.get(i).toString();
        }
        returnString += "\t\t<table";
        if (!properties.equals("")) {
            returnString += properties + " ";

        }

        returnString += ">\n";

        if (!data.equals("")) {
            returnString += data;
        }


        returnString += " \n\t\t</table>\n ";
     
        return returnString;
    }

    public style getStyle() {
        return style1;
    }

    public void setStyle(style style1) {
        this.style1 = style1;
    }
}
