/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

 import at.darkdestiny.core.enumeration.EAIType;import at.darkdestiny.core.model.AI;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class AIDAO extends ReadWriteTable<AI> implements GenericDAO {


    private static final Logger log = LoggerFactory.getLogger(AIDAO.class);
    public AI findByUserId(int userId) {
        AI ai = new AI();
        ai.setUserId(userId);
        return (AI) get(ai);
    }

    public ArrayList<AI> findByAIType(EAIType aiType) {
        AI ai = new AI();
        ai.setAiType(aiType);
        return find(ai);
    }

    public AI findFreeWorldsAI() {
        AI ai = new AI();
        ai.setAiType(EAIType.FREE_WORLDS_AI);

        ArrayList<AI> ais = find(ai);
        if (ais.isEmpty()) {
            return null;
        } else {
            if (ais.size() > 2) {
                log.warn("Momre than 1 AI with type DELETED_PLAYER_AI found. Taking first one");
            }

            return ais.get(0);
        }
    }
}
