/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Style;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author HorstRabe
 */
public class StyleDAO extends ReadOnlyTable<Style> implements GenericDAO{
    public Style findById(int id) {
        Style s = new Style();
        s.setId(id);
        return (Style) get(s);
    }

}
