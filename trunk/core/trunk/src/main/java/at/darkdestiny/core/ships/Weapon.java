/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ships;

/**
 *
 * @author Stefan
 */
public class Weapon extends BuiltInModule implements Offensive {
    private int energyConsumption;
    private float accuracy;
    private int range;
    private int fireRate;
    private int reloadTime;    
    private int attackStrength;

    public Weapon(ModInitParameter mip, int id) {
        super(mip,id);        
    }
    
    public double getPenetration(Defensive module, ShipDesignDetailed enemy) {
        return 1d;
    }

    public int getEnergyConsumption() {
        return energyConsumption;
    }

    protected void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public float getAccuracy() {
        return accuracy;
    }

    protected void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public int getRange() {
        return range;
    }

    protected void setRange(int range) {
        this.range = range;
    }

    public int getFireRate() {
        return fireRate;
    }

    protected void setFireRate(int fireRate) {
        this.fireRate = fireRate;
    }

    public int getReloadTime() {
        return reloadTime;
    }

    protected void setReloadTime(int reloadTime) {
        this.reloadTime = reloadTime;
    }

    public int getAttackStrength() {
        return attackStrength;
    }

    public void setAttackStrength(int attackStrength) {
        this.attackStrength = attackStrength;
    }
}
