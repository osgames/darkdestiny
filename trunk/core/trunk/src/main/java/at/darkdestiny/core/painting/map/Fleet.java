/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.enumeration.EStarMapObjectType;

/**
 *
 * @author Stefan
 */
public class Fleet extends StarMapObject {
    private final int id;
    private String name;
    private String owner;
    private SMFleet payload;
    
    public Fleet(int id, int x, int y) {
        super(EStarMapObjectType.FLEET,x,y);
        this.id = id;
        this.name = "Flotte #" + name;
    }        

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return the payload
     */
    public SMFleet getPayload() {
        return payload;
    }

    /**
     * @param payload the payload to set
     */
    public void setPayload(SMFleet payload) {
        this.payload = payload;
    }
}
