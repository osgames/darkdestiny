/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

/**
 *
 * @author Stefan
 */
public class JMGalaxy {
    private int id;
    private int endSystem;
    private int height;
    private int width;
    private int originX;
    private int originY;
    private boolean playerStart;
    private int startSystem;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the endSystem
     */
    public int getEndSystem() {
        return endSystem;
    }

    /**
     * @param endSystem the endSystem to set
     */
    public void setEndSystem(int endSystem) {
        this.endSystem = endSystem;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the originX
     */
    public int getOriginX() {
        return originX;
    }

    /**
     * @param originX the originX to set
     */
    public void setOriginX(int originX) {
        this.originX = originX;
    }

    /**
     * @return the originY
     */
    public int getOriginY() {
        return originY;
    }

    /**
     * @param originY the originY to set
     */
    public void setOriginY(int originY) {
        this.originY = originY;
    }

    /**
     * @return the playerStart
     */
    public boolean isPlayerStart() {
        return playerStart;
    }

    /**
     * @param playerStart the playerStart to set
     */
    public void setPlayerStart(boolean playerStart) {
        this.playerStart = playerStart;
    }

    /**
     * @return the startSystem
     */
    public int getStartSystem() {
        return startSystem;
    }

    /**
     * @param startSystem the startSystem to set
     */
    public void setStartSystem(int startSystem) {
        this.startSystem = startSystem;
    }
}
