/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.construction.restriction;

import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.enumeration.ERestrictionReason;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.TradePost;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.result.ConstructionCheckResult;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class RTradePost extends Service implements ConstructionInterface {
    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();

    @Override
    public ConstructionCheckResult onConstructionCheck(PlayerPlanet pp) {
        return new ConstructionCheckResult(true,1);
    }

    @Override
    public void onConstruction(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onFinishing(PlayerPlanet pp) {
        TradePost tp = new TradePost();
        
        // Generate random name
        int randNo = 65536 + (int)Math.floor(Math.random() * (1048575d - 65536d));
        String hexStr = Integer.toHexString(randNo);
        
        tp.setName("Handelsposten " + hexStr);
        tp.setPlanetId(pp.getPlanetId());
        tp.setTransportPerLJ(TradePost.TRADEPOST_CAPACITY);
        tradePostDAO.add(tp);
    }

    @Override
    public void onDeconstruction(PlayerPlanet pp) throws Exception {
      
    }

    @Override
    public void onDestruction(PlayerPlanet pp) throws Exception {
        TradePost tp = tradePostDAO.findByPlanetId(pp.getPlanetId());
        if(tp != null){
            tradePostDAO.remove(tp);
        }

        // Also delete all routes to this planet
        ArrayList<TradeRoute> trList = tradeRouteDAO.findAllByType(ETradeRouteType.TRADE);
        for (TradeRoute tr : trList) {
            if (tr.getStartPlanet().equals(pp.getPlanetId()) || tr.getTargetPlanet().equals(pp.getPlanetId())) {
                ArrayList<TradeRouteDetail> trdList = tradeRouteDetailDAO.getByRouteId(tr.getId());
                for (TradeRouteDetail trd : trdList) {
                    tradeRouteDetailDAO.remove(trd);
                }

                tradeRouteDAO.remove(tr);
            }
        }
    }

    @Override
    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }

    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }
    
    @Override
    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }
}
