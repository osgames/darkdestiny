/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.Buildable;

/**
 *
 * @author Stefan
 */
public class ShipUpgradeOrder extends UpgradeOrder {    
    private final int fleetId;
    
    public ShipUpgradeOrder(Buildable fromUnit, Buildable toUnit, int count, int fleetId) {
        super(fromUnit,toUnit,count);
        this.fleetId = fleetId;
    }

    /**
     * @return the fleetId
     */
    public int getFleetId() {
        return fleetId;
    }
}
