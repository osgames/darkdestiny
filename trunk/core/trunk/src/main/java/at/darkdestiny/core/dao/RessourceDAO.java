/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "ressource")
public class RessourceDAO extends ReadOnlyTable<Ressource> implements GenericDAO {

    private RessourceDAO() {
    }

    public ArrayList<Ressource> findAllDisplayableRessources() {
        ArrayList<Ressource> tmp = findAll();
        ArrayList<Ressource> result = new ArrayList<Ressource>();
        for (Ressource r : tmp) {
            if (r.getDisplayLocation() >= 0) {
                result.add(r);
            }
        }
        return result;
    }

    /**
     *
     * Use findById instead
     *
     * @param id
     * @return
     * @deprecated
     */
    @Deprecated
    public Ressource findRessourceById(Integer id) {
        Ressource r = new Ressource();
        r.setId(id);
        return get(r);
    }

    public Ressource findById(Integer id) {
        Ressource r = new Ressource();
        r.setId(id);
        return get(r);
    }

    public ArrayList<Ressource> findAllStoreableRessources() {

        ArrayList<Ressource> tmp = findAll();
        ArrayList<Ressource> result = new ArrayList<Ressource>();
        for (Ressource r : tmp) {
            //Skip cv embinium
            // if(r.getId() == 6)continue;
            if (!r.getMineable() && r.getTransportable()) {
                result.add(r);
            }
        }
        return result;

    }

    public ArrayList<Ressource> findAllMineableRessources() {

        ArrayList<Ressource> tmp = findAll();
        ArrayList<Ressource> result = new ArrayList<Ressource>();
        for (Ressource r : tmp) {
            if (r.getMineable()) {
                result.add(r);
            }
        }
        return result;
    }

    public ArrayList<Ressource> findAllPayableRessources() {
        Ressource r = new Ressource();
        r.setRessourcecost(true);
        return find(r);
    }
}
