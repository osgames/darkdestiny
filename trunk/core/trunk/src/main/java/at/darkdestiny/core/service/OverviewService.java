/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.planetcalc.*;
import at.darkdestiny.util.DebugBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.construction.BuildingAbortRessourceCost;
import at.darkdestiny.core.construction.BuildingDeconstructionRessourceCost;
import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.development.BonusEntry;
import at.darkdestiny.core.development.conditions.IBonusCondition;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EBonusRange;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.core.model.ActiveBonus;
import at.darkdestiny.core.model.Bonus;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.ConstructionCheckResult;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class OverviewService extends Service {

    private static final Logger log = LoggerFactory.getLogger(OverviewService.class);

    public static Planet findPlanetById(int planetId) {
        return planetDAO.findById(planetId);
    }

    public static PlayerPlanet findPlayerPlanetByPlanetId(int planetId) {
        return playerPlanetDAO.findByPlanetId(planetId);
    }

    public static ArrayList<PlanetRessource> findMineableRessources(int planetId) {
        return planetRessourceDAO.findMineableRessources(planetId);
    }

    public static Ressource findRessourceById(int ressourceId) {
        return ressourceDAO.findRessourceById(ressourceId);
    }

    public static PlanetCalculation PlanetCalculation(int planetId) {
        try {
            return new PlanetCalculation(planetId);

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Retrieval of Extended Planet Data failed for " + planetId + "!", e);
        }

        return null;
    }

    public static ArrayList<ActiveBonus> findActiveBonus(int planetId) {
        return activeBonusDAO.findByPlanetId(planetId);
    }

    public static Bonus findBonusById(int bonusId) {
        return bonusDAO.findById(bonusId);
    }

    public static ArrayList<String> getActiveBonusDescription(int bonusId, int planetId, int userId) {
        Bonus b = Service.bonusDAO.findById(bonusId);
        ArrayList<String> activeBoni = new ArrayList<String>();

        try {
            if (b.getBonusClass() != null) {
                Class bc = Class.forName("at.darkdestiny.core.development.conditions." + b.getBonusClass());
                IBonusCondition ibc = (IBonusCondition) bc.newInstance();
                activeBoni = ibc.activeBoni(bonusId, planetId, userId);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on Construction Restriction: ", e);
        }

        return activeBoni;
    }

    public static ArrayList<BonusEntry> getBonusList(int userId, int planetId) {
        UserData ud = userDataDAO.findByUserId(userId);
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);

        ArrayList<BonusEntry> bonusList = new ArrayList<BonusEntry>();
        for (Bonus b : bonusDAO.findPlanetBonus()) {            
            if (!b.isEnabled()) continue;
            
            BonusEntry be = new BonusEntry();

            be.setId(b.getId());
            be.setName(b.getName());
            be.setDescription(b.getDescription());
            be.setDuration(b.getDuration());            

            // If this bonus is already used on some other planet increase price
            int alreadyActive = 1;
            if (b.hasPriceIncreasePerUsage()) {
                alreadyActive += getActiveBoniForUser(userId, b.getId());
            }
            be.setCost(b.getCost().intValue() * alreadyActive);
                
            boolean skip = false;
            // Skip if a bonus is already active
            for (ActiveBonus ab : activeBonusDAO.findByPlanetId(planetId)) {
                if (b.getId().equals(ab.getBonusId())) {
                    skip = true;
                }
            }

            // Disable if this bonus requires a certain research
            if (b.getReqResearch() > 0) {
                if (!ResearchService.isResearched(playerPlanetDAO.findByPlanetId(planetId).getUserId(), b.getReqResearch())) {
                    be.setEnabled(false);
                    skip = true;
                }
            }                 
            
            // Skip if a Colony already has a special type
            if (b.isPlanetUnique() && (pp.getColonyType() != EColonyType.DEFAULT)) {
                skip = true;
            }

            if (skip) continue;

            // Check special conditions from bonusClass
            try {
                if (b.getBonusClass() != null) {
                    Class bc = Class.forName("at.darkdestiny.core.development.conditions." + b.getBonusClass());
                    IBonusCondition ibc = (IBonusCondition) bc.newInstance();
                    if (!ibc.isPossible(planetId)) {
                        be.setEnabled(false);
                    } else {
                        be.setEnabled(true);
                    }
                } else {
                    be.setEnabled(true);
                }
            } catch (Exception e) {
                be.setEnabled(false);
                DebugBuffer.writeStackTrace("Error on Construction Restriction: ", e);
            }

            // Disable if an improvement is too expensive
            if (b.getCost() > ud.getDevelopementPoints()) {
                be.setEnabled(false);
            }                               
            
            bonusList.add(be);
        }

        // Order Bonus List by Cost and Name
        ArrayList<BonusEntry> sortedList = new ArrayList<BonusEntry>();
        
        TreeMap<Integer,TreeMap<String,BonusEntry>> sorted = Maps.newTreeMap();
        for (BonusEntry be : bonusList) {
            if (sorted.containsKey(be.getCost())) {
                TreeMap<String,BonusEntry> nameSorting = sorted.get(be.getCost());
                nameSorting.put(ML.getMLStr(be.getName(), userId),be);
            } else {
                TreeMap<String,BonusEntry> nameSorting = Maps.newTreeMap();
                nameSorting.put(ML.getMLStr(be.getName(),userId),be);
                sorted.put(be.getCost(),nameSorting);
            }
        }
        
        for (TreeMap<String,BonusEntry> nameSorted : sorted.values()) {
            for (BonusEntry be : nameSorted.values()) {
                sortedList.add(be);
            }
        }
        
        return sortedList;
    }

    @Deprecated
    // Please use getBonusList instead
    public static ArrayList<Bonus> findAvailBonus(int planetId) {
        ArrayList<Bonus> result = new ArrayList<Bonus>();
        for (Bonus b : bonusDAO.findPlanetBonus()) {
            if (!b.isEnabled()) continue;
            
            boolean add = true;
            for (ActiveBonus ab : activeBonusDAO.findByPlanetId(planetId)) {
                if (b.getId().equals(ab.getBonusId())) {
                    add = false;
                    break;
                }
            }
            if (add) {
                if (b.getReqResearch() > 0) {
                    if (!ResearchService.isResearched(playerPlanetDAO.findByPlanetId(planetId).getUserId(), b.getReqResearch())) {
                        continue;
                    }
                }

                PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
                int alreadyActive = getActiveBoniForUser(pp.getUserId(), b.getId());

                b.setCost(b.getCost() * (alreadyActive + 1));
                result.add(b);
            }
        }
        return result;
    }

    public static ExtPlanetCalcResult getCalculatedPlanetValues(int planetId) {
        try {
            PlanetCalculation pc = new PlanetCalculation(planetId);
            return pc.getPlanetCalcData().getExtPlanetCalcData();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Could not load extended planet calculations: ", e);
        }

        return null;
    }

    public static UserData findUserData(int userId) {
        return userDataDAO.findByUserId(userId);
    }

    private static int getActiveBoniForUser(int userId, int bonusId) {
        int activeBoni = 0;

        ArrayList<PlayerPlanet> ppList = playerPlanetDAO.findByUserId(userId);
        for (PlayerPlanet pp : ppList) {
            ArrayList<ActiveBonus> abList = activeBonusDAO.findByBonusId(bonusId);

            for (ActiveBonus ab : abList) {
                if (ab.getRefId().equals(pp.getPlanetId())) {
                    activeBoni++;
                }
            }
        }

        return activeBoni;
    }

    public static ArrayList<BaseResult> buyBonus(int userId, int planetId, Map<String, String[]> params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        if (playerPlanetDAO.findByPlanetId(planetId) == null) {
            result.add(new BaseResult("Planet does not belong to you", true));
            return result;
        }
        if (playerPlanetDAO.findByPlanetId(planetId).getUserId() != userId) {
            result.add(new BaseResult("Planet does not belong to you", true));
            return result;
        }

        if (params.get("bonusId") != null) {
            // We check how many bonuses of this type have already been consumed
            // and increase cost on multiple usage

            int bonusId = Integer.parseInt(params.get("bonusId")[0]);
            if (bonusId > 0) {
                for (BonusEntry be : getBonusList(userId,planetId)) {
                    if (be.getId() == bonusId) {
                        int alreadyActive = getActiveBoniForUser(planetId, be.getId());

                        UserData ud = userDataDAO.findByUserId(userId);
                        if ((be.getCost() * (alreadyActive + 1)) > ud.getDevelopementPoints()) {
                            result.add(new BaseResult(ML.getMLStr("err_notenoughpoints", userId), true));
                            return result;
                        }

                        // Check for Bonus class and run activation code
                        Bonus b = bonusDAO.findById(bonusId);
                        
                        try {
                            if (b.getBonusClass() != null) {
                                Class bc = Class.forName("at.darkdestiny.core.development.conditions." + b.getBonusClass());
                                IBonusCondition ibc = (IBonusCondition) bc.newInstance();
                                ibc.onActivation(bonusId, planetId, userId);
                            }
                        } catch (Exception e) {
                            DebugBuffer.writeStackTrace("Error on Bonus Condition: ", e);
                        }                        
                        
                        ud.setDevelopementPoints(ud.getDevelopementPoints() - (be.getCost() * (alreadyActive + 1)));
                        userDataDAO.update(ud);
                        ActiveBonus ab = new ActiveBonus();
                        ab.setBonusId(bonusId);
                        ab.setDuration(be.getDuration());
                        ab.setRefId(planetId);
                        ab.setBonusRange(EBonusRange.PLANET);
                        activeBonusDAO.add(ab);
                        break;
                    }
                }
            } else {

                result.add(new BaseResult(ML.getMLStr("overview_err_nobonusselected", userId), true));
                return result;
            }
        } else {
            result.add(new BaseResult(ML.getMLStr("overview_err_nobonusselected", userId), true));
            return result;
        }
        return result;
    }

    public static ArrayList<Ressource> findAllRessources() {

        ArrayList<Ressource> result = new ArrayList<Ressource>();
        TreeMap<Integer, Ressource> ressources = new TreeMap<Integer, Ressource>();

        for (Ressource r : ressourceDAO.findAllMineableRessources()) {
            ressources.put(r.getId(), r);
        }
        for (Ressource r : ressourceDAO.findAllStoreableRessources()) {
            ressources.put(r.getId(), r);
        }
        ressources.put(Ressource.ENERGY, ressourceDAO.findRessourceById(Ressource.ENERGY));

        result.addAll(ressources.values());

        return result;

    }

    public static long getPopulation(int planetId) {
        return playerPlanetDAO.findByPlanetId(planetId).getPopulation();
    }

    public static int getMilitaryPower(int planetId) {
        int militaryPower = 0;
        try {
            for (PlayerTroop pt : playerTroopDAO.findByPlanetId(planetId)) {
                GroundTroop gt = groundTroopDAO.findById(pt.getTroopId());
                militaryPower += gt.getHitpoints() * pt.getNumber();
            }
        } catch (Exception e) {
            log.debug("ex : " + e);

        }

        return militaryPower;
    }

    public static BuildingAbortRessourceCost findForAbortion(int timeFinished, int userId) {
        ProductionOrder po = productionOrderDAO.findById(timeFinished);
        int constructionId = actionDAO.findByTimeFinished(timeFinished, EActionType.BUILDING).getConstructionId();

        BuildingAbortRessourceCost barc = new BuildingAbortRessourceCost(constructionId, po);
        return barc;
    }

    public static BuildingDeconstructionRessourceCost findForDeconstruction(int buildingID, int planetId, int userID) {
        BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(buildingID, planetId);

        return bdrc;
    }

    public static BuildingDeconstructionRessourceCost findForDeconstruction(int buildingID, int userID) {
        BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(buildingID);

        return bdrc;
    }

    public static BuildingDeconstructionRessourceCost findForDeconstruction(int buildingID, int planetId, int userID, int count) {
        BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(buildingID, planetId);

        return bdrc;
    }

    @Deprecated
    public static ArrayList<Ressource> findAllStoreableRessources() {
        return ressourceDAO.findAllPayableRessources();
    }
}
