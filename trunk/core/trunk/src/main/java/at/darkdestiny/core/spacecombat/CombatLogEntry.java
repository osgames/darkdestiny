/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

/**
 *
 * @author Stefan
 */
public class CombatLogEntry {
    private int round;
    private int designId;
    private String designName;  
    private long shieldDamageTaken = 0l;
    private long hpDamageTaken = 0l;
    private int totalCount = 0;
    private int destroyed = 0;
    
    public CombatLogEntry(int round, int designId, String name, int totalCount) {
        this.round = round;
        this.designId = designId;
        this.totalCount = totalCount;
        this.designName = name;
    }
    
    public void setShipName(String name) {
        designName = name;
    }
    
    public void addShieldDamage(int shieldDamage) {
        shieldDamageTaken += shieldDamage;
    }
    
    public void addHpDamage(int hpDamage) {
        hpDamageTaken += hpDamage;
    }
    
    public void incDestroyed() {
        destroyed++;
    }

    public int getRound() {
        return round;
    }

    public int getDesignId() {
        return designId;
    }

    public String getDesignName() {
        return designName;
    }

    public long getShieldDamageTaken() {
        return shieldDamageTaken;
    }

    public long getHpDamageTaken() {
        return hpDamageTaken;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public int getDestroyed() {
        return destroyed;
    }
}
