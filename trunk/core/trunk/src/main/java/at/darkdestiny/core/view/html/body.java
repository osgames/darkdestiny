 /*
 * body.java
 *
 * Created on 10. Mai 2007, 13:30
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.darkdestiny.core.view.html;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.*;
/**
 *
 * @author Horst
 */
public class body extends htmlObject{

    private String endData;
    public ArrayList<table> tables;
    /** Creates a new instance of body */
    public body() {
        
        tables = new ArrayList<table>();
        
    }
    
  
    
    public void addTable(table table){
        
        tables.add(table);
        
    }
    
    public ArrayList<table> getTables() {
        return tables;
    }
    
    public void setTables(ArrayList<table> tables) {
        this.tables = tables;
    }
    
    public String toString(){
        String returnString = "";
        DebugBuffer.addLine(DebugLevel.WARNING,"1 " +  data);
        if(data == null){
            data = "";
        }
        data += "";
        DebugBuffer.addLine(DebugLevel.WARNING,"2 " +  data);
        returnString += "\t<body";
        for(int i = 0; i < tables.size(); i++){
            data += " "+tables.get(i).toString();
        }
        DebugBuffer.addLine(DebugLevel.WARNING,"3 " +  data);
        
        if(!properties.equals("")){
            returnString += properties;
        }
        DebugBuffer.addLine(DebugLevel.WARNING,"4 " +  data);
        returnString += " >\n";
        if(!data.equals("")){
            returnString += " "+data;
        }
        DebugBuffer.addLine(DebugLevel.WARNING,"5 " +  data);
        if(endData != null){
            returnString += " "+endData;
        }
        DebugBuffer.addLine(DebugLevel.WARNING,"6 " +  returnString);
        returnString += "\n\t</body>\n";
         return returnString;
    }

    /**
     * @return the endData
     */
    public String getEndData() {
        return endData;
    }

    /**
     * @param endData the endData to set
     */
    public void setEndData(String endData) {
        this.endData = endData;
    }
}
