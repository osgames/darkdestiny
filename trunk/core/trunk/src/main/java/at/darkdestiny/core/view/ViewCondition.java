/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view;

import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.TitleCondition;

/**
 *
 * @author Aion
 */
public class ViewCondition {

    private TitleCondition condition;
    private ConditionToTitle conditionToTitle;

    public ViewCondition(TitleCondition condition, ConditionToTitle conditionToTitle) {
        this.condition = condition;
        this.conditionToTitle = conditionToTitle;

    }

    /**
     * @return the condition
     */
    public TitleCondition getCondition() {
        return condition;
    }

    /**
     * @return the conditionToTitle
     */
    public ConditionToTitle getConditionToTitle() {
        return conditionToTitle;
    }

}
