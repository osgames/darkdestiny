/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.update;

import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class TradeRouteDetailProcessing {
    private TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    
    private final TradeRouteDetail base;
    
    private int maxQty = -1;
    private int maxByTargetSource = Integer.MAX_VALUE;
    
    private int assignedQty = 0;
    private int assignedQtyTmp = 0;    
    private int hiddenAssignedQty = 0;
    
    private boolean inOutRestricted = false;
    private boolean closed = false;
    
    public TradeRouteDetailProcessing(TradeRouteDetail trd) {
        this.base = trd;
    }      

    public int getMaxQty() {
        if (maxQty == -1) {
            maxQty = Integer.MAX_VALUE;
            
            if (getBase().getMaxQty() > 0) {
                maxQty = Math.min(maxQty, (int)((long)getBase().getMaxQty() - getBase().getQtyDelivered()));
            }
            if (getBase().getMaxQtyPerTick() > 0) {
                maxQty = Math.min(maxQty, getBase().getMaxQtyPerTick());
            }
            
            maxQty = Math.min(maxQty,maxByTargetSource);
            // System.out.println("["+this+"]["+base.getRessId()+"] Initialize with " + maxQty);
        }                
        
        return maxQty;
    }

    public TradeRouteDetail getBase() {
        return base;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public int getAssignedQty() {
        return assignedQty;
    }

    /*
    public void setAssignedQty(int assignedQty) {
        this.assignedQty = assignedQty;
    }
    */
    
    public double getRouteDistance() {
        return trDAO.getById(base.getRouteId()).getDistance();
    }

    /**
     * @return the softClosed
     */
    public boolean isInOutRestricted() {
        return inOutRestricted;
    }

    /**
     * @param softClosed the softClosed to set
     */
    public void setInOutRestricted(boolean inOutRestricted) {
        this.inOutRestricted = inOutRestricted;
    }

    /**
     * @return the assignedQtyTmp
     */
    public int getAssignedQtyTmp() {
        return assignedQtyTmp;
    }

    /**
     * @param assignedQtyTmp the assignedQtyTmp to set
     */
    public void setAssignedQtyTmp(int assignedQtyTmp) {
        this.assignedQtyTmp = assignedQtyTmp;
    }
    
    public void writeAssignedQtyTmp() {
        assignedQty += assignedQtyTmp;
        assignedQtyTmp = 0;        
    }

    public void moveAssignedQtyToTotal() {
        hiddenAssignedQty += assignedQty;
        assignedQty = 0;        
    }    
    
    @Override
    public String toString() {
        TradeRoute tr = trDAO.getById(base.getRouteId());
        
        return "{" + tr.getStartPlanet() + "=>" + tr.getTargetPlanet() + "[RESS: " + base.getRessId() + "]}";
    }

    /**
     * @return the hiddenAssignedQty
     */
    public int getTotalAssignedQty() {
        return hiddenAssignedQty;
    }
}
