/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.test;

/**
 *
 * @author Stefan
 */
public abstract class Connector {
    private boolean attacks;
    private boolean helps;

    public Connector(boolean attacks, boolean helps) {
        this.attacks = attacks;
        this.helps = helps;
    }
    
    public boolean isAttacks() {
        return attacks;
    }

    public boolean isHelps() {
        return helps;
    }
}
