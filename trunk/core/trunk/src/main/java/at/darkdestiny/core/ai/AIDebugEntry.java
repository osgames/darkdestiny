/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai;

/**
 *
 * @author Admin
 */

    public class AIDebugEntry{
        private AIDebugLevel level;
        private String message;

        public AIDebugEntry(AIDebugLevel level, String message) {
            this.level = level;
            this.message = message;
        }

    /**
     * @return the level
     */
    public AIDebugLevel getLevel() {
        return level;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
       
    }