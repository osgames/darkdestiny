package at.darkdestiny.core.utilities.img;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Malt den Hintergrund in einer bestimmten Farbe an
 * 
 * @author martin
 */
public class BackgroundPainter implements IModifyImageFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1847581049751611029L;
	private int width;
	private int height;
	private int color;

	/**
	 * 
	 */
	public BackgroundPainter(int color) {
		this.color = color;
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.img.IModifyImageFunction#run(java.awt.Graphics)
	 */
        @Override
	public void run(Graphics g) {
		g.setColor(new Color(color));
		g.fillRect(0, 0, width, height);
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.img.IModifyImageFunction#setSize(int, int)
	 */
        @Override
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

        @Override
	public String createMapEntry() {
		return "";
	}

}
