/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ships;

/**
 *
 * @author Stefan
 */
public class ShipChassis extends ShipModule {

    private int structure;
    private int space;
    private int range;

    public ShipChassis(ModInitParameter mip, int id) {
        super(mip, id);
    }

    public int getStructure() {
        return structure;
    }

    public void setStructure(int structure) {
        this.structure = structure;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    void setRange(int range) {
        this.range = range;
    }

    /**
     * @return the range
     */
    public int getRange() {
        return range;
    }
}
