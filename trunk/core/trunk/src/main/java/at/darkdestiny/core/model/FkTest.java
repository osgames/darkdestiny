/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.OneToMany;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation("fk_testtable")
public class FkTest extends Model<FkTest> {

    @IdFieldAnnotation
    @FieldMappingAnnotation("id")
    private Integer id;
    @OneToMany(baseField = "id", refTable = "FkChildTest", refField = "refId")
    private ArrayList<FkChildTest> fkChildTestList;
    @FieldMappingAnnotation("name")
    private String name;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<FkChildTest> getFkChildTest() {
        return fkChildTestList;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
