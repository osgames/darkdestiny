package at.darkdestiny.core.html;

public class HTMLColorCodes {

	
	public static String getColorCode(double value, double low, double ok) {
		if (value < low) {
			return "FF0000";
		} else if (value < ok) {
			return "FFFF00";
		} else {
			return "33FF00"; 
		}
	}
}
