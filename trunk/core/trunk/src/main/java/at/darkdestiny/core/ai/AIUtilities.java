/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai;

 import at.darkdestiny.core.GameConstants;import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.enumeration.EAIType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.EUserGender;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.model.AI;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlanetLog;
import at.darkdestiny.core.model.PlanetLoyality;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.model.UserSettings;
import at.darkdestiny.core.scanner.StarMapQuadTree;
import at.darkdestiny.core.scanner.SystemDesc;
import at.darkdestiny.core.service.ResearchService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.ViewTableUtilities;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;
import at.darkdestiny.core.viewbuffer.HomeSystemBuffer;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class AIUtilities {
    private static final Logger log = LoggerFactory.getLogger(AIUtilities.class);

    public static User createAI() {
        return createAI(EAIType.NORMAL, true);
    }

    public static User createAI(EAIType aiType, boolean createPlayerPlanet) {
        DebugBuffer.addLine(DebugLevel.DEBUG, "Adding an AI");


        int aiCount = 0;
        for (User u : Service.userDAO.findAll()) {
            if (u.getUserType().equals(EUserType.AI)) {
                aiCount++;
            }
        }
        aiCount++;

        User u = new User();
        u.setAcceptedUserAgreement(true);
        u.setActivationCode("");
        u.setActive(true);
        u.setAdmin(false);
        u.setBetaAdmin(false);
        u.setEmail("support@thedarkdestiny.de");
        u.setGameName("AI " + aiCount);
        u.setGender(EUserGender.MALE);
        u.setUserType(EUserType.AI);
        u.setGuest(false);
        u.setLastUpdate(GameUtilities.getCurrentTick2());

        u.setIp("");
        u.setJoinDate(System.currentTimeMillis());
        u.setLocale("de_DE");
        u.setSystemAssigned(false);
        u.setUserName("AI-User " + aiCount);
        u.setUserConfigured(true);
        u.setPassword("");

        Service.userDAO.add(u);

        UserData ud = new UserData();
        ud.setUserId(u.getUserId());
        ud.setCredits(900000000l);

        Service.userDataDAO.add(ud);

        UserSettings us = new UserSettings();
        us.setUserId(u.getUserId());

        Service.userSettingsDAO.add(us);

        //Create AI entry
        AI ai = new AI();
        ai.setUserId(u.getId());
        ai.setAiType(aiType);

        Service.aiDAO.add(ai);

        if (createPlayerPlanet) {

            boolean planetFound = false;
            int counter = 0;
            int counterMax = 100;


            int planetMaxId = 0;
            int planetMinId = 999999;

            for (Planet p : Service.planetDAO.findAll()) {
                planetMaxId = Math.max(planetMaxId, p.getId());
                planetMinId = Math.min(planetMinId, p.getId());
            }
            Planet p = null;

            PlayerPlanet pp = Service.playerPlanetDAO.findHomePlanetByUserId(u.getId());
            if (pp != null) {
                planetFound = true;
                p = Service.planetDAO.findById(pp.getPlanetId());
            }

            int maxTries = 1000;
            int tries = 0;
            while (!planetFound && tries < maxTries) {
                int random = (int) Math.round(Math.random() * (planetMaxId - planetMinId) + planetMinId);
                for (int i = random; i < planetMaxId; i++) {
                    p = Service.planetDAO.findById(i);
                    if (p != null) {
                        if (p.getLandType().equals(Planet.LANDTYPE_M) && Service.playerPlanetDAO.findByPlanetId(i) == null) {

                            DebugBuffer.addLine(DebugLevel.DEBUG, "Found M-Planet with Id " + i);
                            planetFound = true;
                            break;
                        }
                    }
                }
                if (!planetFound) {
                    for (int i = random; i > planetMinId; i--) {
                        p = Service.planetDAO.findById(i);
                        if (p != null) {
                            if (p.getLandType().equals(Planet.LANDTYPE_M) && Service.playerPlanetDAO.findByPlanetId(i) == null) {

                                DebugBuffer.addLine(DebugLevel.DEBUG, "Found M-Planet with Id " + i);
                                planetFound = true;
                                break;
                            }
                        }
                    }
                }
                if (counter > counterMax) {

                    DebugBuffer.addLine(DebugLevel.DEBUG, "No planet found after " + counterMax + " iterations for AI");
                }
                tries++;
            }

            if (tries >= 100) {
                log.error("Could not find planet for AI after " + maxTries + " tries");
            }
            if (planetFound) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "Colonizing Planet " + p.getId());
                colonize(p.getId(), u.getUserId());
            }
        }

        AIThreadHandler.addThread(u.getUserId());

        DebugBuffer.addLine(DebugLevel.DEBUG, "Done adding AI");

        return u;
    }

    private static void colonize(int planetId, int aiId) {


        boolean needsOrbital = false;
        boolean inhabitable = false;


        TransactionHandler th = TransactionHandler.getTransactionHandler();

        try {
            // Check ob Orbital oder normal
            Planet p = Service.planetDAO.findById(planetId);
            int systemId = p.getSystemId();

            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_A) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_B)) {
                needsOrbital = true;
            } else if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_M) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_G) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_C)) {

                inhabitable = false;
            } else {
                inhabitable = true;
            }

            // Get chassisSize and adjust Factors
            int chassisSize = Chassis.ID_BATTLESHIP;


            boolean fusionResearched = ResearchService.isResearched(aiId, Research.ID_FUSIONPLANT);

            th.startTransaction();

            ArrayList<Integer[]> buildingsG = new ArrayList<Integer[]>();
            ArrayList<Integer[]> buildingsO = new ArrayList<Integer[]>();
            ArrayList<Integer[]> buildingsI = new ArrayList<Integer[]>();
            int sPop = 10000;
            int ressMod = 1;

            switch (chassisSize) {
                case (Chassis.ID_FRIGATE):
                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1});

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    break;
                case (Chassis.ID_DESTROYER):
                    ressMod = 2;
                    sPop = 30000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    buildingsG.add(new Integer[]{Construction.ID_SMALLAGRICULTUREFARM, 1}); // Small Agrar
                    buildingsG.add(new Integer[]{Construction.ID_SMALLRESSOURCESTORAGE, 1}); // Small Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 1}); // Orbital Storage

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    buildingsI.add(new Integer[]{Construction.ID_SMALLRESSOURCESTORAGE, 1}); // Small Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 1}); // Hydroponic farm
                    break;
                case (Chassis.ID_CRUISER):
                    ressMod = 5;
                    sPop = 80000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsG.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 2});
                    } else {
                        buildingsG.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 1});
                    }
                    buildingsG.add(new Integer[]{Construction.ID_BIGAGRICULTUREFARM, 1});
                    buildingsG.add(new Integer[]{Construction.ID_SMALLRESSOURCESTORAGE, 1}); // Small Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 1}); // Orbital Storage
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_ACCOMODATIONUNIT, 1}); // Orbital Hydro
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_HYDROPONDICFARM, 1}); // Orbital Habitat

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    buildingsI.add(new Integer[]{Construction.ID_SMALLRESSOURCESTORAGE, 1}); // Small Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 1}); // Hydroponic farm
                    buildingsI.add(new Integer[]{Construction.ID_ACCOMODATIONUNIT, 1}); // Habitat
                    break;
                case (Chassis.ID_BATTLESHIP):
                    ressMod = 20;
                    sPop = 240000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsG.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 4});
                    } else {
                        buildingsG.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 2});
                    }
                    buildingsG.add(new Integer[]{Construction.ID_AGROCOMPLEX, 1}); // Agrarkomplex
                    buildingsG.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 1}); // Large Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 3}); // Orbital Storage
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_ACCOMODATIONUNIT, 5}); // Orbital Hydro
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_HYDROPONDICFARM, 5}); // Orbital Habitat

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsI.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 2});
                    } else {
                        buildingsI.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 1});
                    }
                    buildingsI.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 1}); // Large Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 5}); // Hydroponic farm
                    buildingsI.add(new Integer[]{Construction.ID_ACCOMODATIONUNIT, 5}); // Habitat
                    break;
                case (Chassis.ID_SUPERBATTLESHIP):
                    ressMod = 60;
                    sPop = 1400000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1}); // Col Center
                    if (!fusionResearched) {
                        buildingsG.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 16});
                    } else {
                        buildingsG.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 8});
                    }
                    buildingsG.add(new Integer[]{Construction.ID_AGROCOMPLEX, 2}); // Agrarkomplex
                    buildingsG.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 3}); // Large Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 9}); // Orbital Storage
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_ACCOMODATIONUNIT, 28}); // Orbital Hydro
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_HYDROPONDICFARM, 28}); // Orbital Habitat

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsI.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 8});
                    } else {
                        buildingsI.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 4});
                    }
                    buildingsI.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 2}); // Large Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 28}); // Hydroponic farm
                    buildingsI.add(new Integer[]{Construction.ID_ACCOMODATIONUNIT, 28}); // Habitat
                    break;
                case (Chassis.ID_TENDER):
                    ressMod = 120;
                    sPop = 4200000;

                    // Normal buildings
                    buildingsG.add(new Integer[]{Construction.ID_COLONYBASE, 1}); // Col Center
                    if (!fusionResearched) {
                        buildingsG.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 16});
                    } else {
                        buildingsG.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 8});
                    }
                    buildingsG.add(new Integer[]{Construction.ID_AGROCOMPLEX, 6}); // Agrarkomplex
                    buildingsG.add(new Integer[]{Construction.ID_COMPLEXIRONMINE, 2}); // Agrarkomplex
                    buildingsG.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 3}); // Large Storage

                    // Orbital buildings
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_COLONY, 1});
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_RESSOURCESTORAGE, 10}); // Orbital Storage
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_ACCOMODATIONUNIT, 50}); // Orbital Hydro
                    buildingsO.add(new Integer[]{Construction.ID_ORBITAL_HYDROPONDICFARM, 50}); // Orbital Habitat

                    // Inhabitable buildings
                    buildingsI.add(new Integer[]{Construction.ID_COLONYBASE, 1});
                    if (!fusionResearched) {
                        buildingsI.add(new Integer[]{Construction.ID_NUCLEARPOWERPLANT, 8});
                    } else {
                        buildingsI.add(new Integer[]{Construction.ID_FUSIONPOWERPLANT, 4});
                    }
                    buildingsI.add(new Integer[]{Construction.ID_BIGRESSOURCESTORAGE, 2}); // Large Storage
                    buildingsI.add(new Integer[]{Construction.ID_HYDROPONICFARM, 50}); // Hydroponic farm
                    buildingsI.add(new Integer[]{Construction.ID_ACCOMODATIONUNIT, 50}); // Habitat
                    buildingsI.add(new Integer[]{Construction.ID_COMPLEXIRONMINE, 1}); // Agrarkomplex
                    break;
            }

            // Erstelle alle benötigten Daten um den Planet dem User hinzuzufügen
            PlayerPlanet pp = new PlayerPlanet();
            pp.setName("Planet #" + planetId);
            pp.setUserId(aiId);
            pp.setPlanetId(planetId);
            pp.setPopulation((long) sPop);
            pp.setMoral(100);
            pp.setTax(20);
            pp.setPriorityIndustry(1);
            pp.setPriorityAgriculture(0);
            pp.setPriorityResearch(2);
            pp.setGrowth(5f);
            pp.setMigration(0);
            pp.setUnrest(0d);
            pp.setInvisibleFlag(false);
            if (HomeSystemBuffer.getForUser(aiId) == null) {
                pp.setHomeSystem(true);
            } else {
                pp.setHomeSystem(false);
            }

            Service.playerPlanetDAO.add(pp);

            // Planet Loyality
            PlanetLoyality pLoyal = new PlanetLoyality();
            pLoyal.setPlanetId(planetId);
            pLoyal.setUserId(aiId);
            pLoyal.setValue(100d);
            Service.planetLoyalityDAO.add(pLoyal);

            // Planet Ressources
            ArrayList<PlanetRessource> prs = Service.planetRessourceDAO.findByPlanetId(planetId);
            for (PlanetRessource pr : prs) {
                if (pr.getType() != EPlanetRessourceType.PLANET) {
                    Service.planetRessourceDAO.remove(pr);
                }
            }
            PlanetRessource iron = new PlanetRessource();
            iron.setPlanetId(planetId);
            iron.setType(EPlanetRessourceType.INSTORAGE);
            iron.setRessId(Ressource.IRON);
            iron.setQty((long) 8000 * ressMod);

            Service.planetRessourceDAO.add(iron);

            PlanetRessource steel = new PlanetRessource();
            steel.setPlanetId(planetId);
            steel.setType(EPlanetRessourceType.INSTORAGE);
            steel.setRessId(Ressource.STEEL);
            steel.setQty((long) 8000 * ressMod);

            Service.planetRessourceDAO.add(steel);

            PlanetRessource food = new PlanetRessource();

            food.setPlanetId(planetId);
            food.setType(EPlanetRessourceType.INSTORAGE);
            food.setRessId(Ressource.FOOD);
            food.setQty(500l);

            Service.planetRessourceDAO.add(food);

            // Erstelle Koloniebasis

            if (needsOrbital) {
                for (Integer[] entry : buildingsO) {
                    DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "ORBITAL ADD BUILDING " + entry[0] + " WITH COUNT " + entry[1]);
                    PlanetConstruction pc = new PlanetConstruction();
                    pc.setPlanetId(planetId);
                    pc.setConstructionId(entry[0]);
                    pc.setNumber(entry[1]);
                    Service.planetConstructionDAO.add(pc);
                }
            } else if (inhabitable) {
                for (Integer[] entry : buildingsI) {
                    DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "INHABITABLE ADD BUILDING " + entry[0] + " WITH COUNT " + entry[1]);
                    PlanetConstruction pc = new PlanetConstruction();
                    pc.setPlanetId(planetId);
                    pc.setConstructionId(entry[0]);
                    pc.setNumber(entry[1]);
                    Service.planetConstructionDAO.add(pc);
                }
            } else {
                for (Integer[] entry : buildingsG) {
                    DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "GAIA ADD BUILDING " + entry[0] + " WITH COUNT " + entry[1]);
                    PlanetConstruction pc = new PlanetConstruction();
                    pc.setPlanetId(planetId);
                    pc.setConstructionId(entry[0]);
                    pc.setNumber(entry[1]);
                    Service.planetConstructionDAO.add(pc);
                }
            }


            PlanetLog pl = new PlanetLog();
            pl.setPlanetId(planetId);
            pl.setUserId(aiId);
            pl.setTime(GameUtilities.getCurrentTick2());
            Service.planetLogDAO.add(pl);

            //Viewtable entry hinzuf�gen
            ViewTableUtilities.addOrRefreshSystem(aiId, p.getSystemId(), GameUtilities.getCurrentTick2());

            //Updaten der Viewtable f�r SPieler die ein Observatorium haben und diesen Planeten sehen
            StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
            at.darkdestiny.core.model.System s = Service.systemDAO.findById(Service.planetDAO.findById(p.getId()).getSystemId());
            SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
            quadtree.addItemToTree(sd);
            ArrayList<Integer> usersRefreshed = new ArrayList<Integer>();
            for (PlayerPlanet ppTmp : (ArrayList<PlayerPlanet>) Service.playerPlanetDAO.findAll()) {
                if (usersRefreshed.contains(ppTmp.getUserId())) {
                    continue;
                }
                at.darkdestiny.core.model.System s1 = Service.systemDAO.findById(Service.planetDAO.findById(ppTmp.getPlanetId()).getSystemId());
                usersRefreshed.add(ppTmp.getUserId());
                if (Service.planetConstructionDAO.isConstructed(ppTmp.getPlanetId(), Construction.ID_OBSERVATORY)) {
                    ArrayList<SystemDesc> tmpSys = quadtree.getItemsAround(s1.getX(), s1.getY(), (int) 100);
                    //Found the colonized system by this observatory
                    if (!tmpSys.isEmpty()) {
                        DebugBuffer.addLine(DebugBuffer.DebugLevel.DEBUG, "Refreshing Viewtabhle after Colonizing for User : " + ppTmp.getUserId() + " systemId : " + systemId);
                        ViewTableUtilities.addOrRefreshSystem(ppTmp.getUserId(), systemId, GameUtilities.getCurrentTick2());
                    }

                }
            }


            HeaderBuffer.reloadUser(aiId);

        } catch (Exception e) {
            th.rollback();

            DebugBuffer.writeStackTrace("Exception in Colonizing - colonize: ", e);
        } finally {
            th.endTransaction();
        }

    }
}
