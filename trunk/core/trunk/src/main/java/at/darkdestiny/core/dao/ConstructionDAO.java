/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class ConstructionDAO extends ReadWriteTable<Construction> implements GenericDAO {

    private ConstructionDAO() {
    }

    public ArrayList<Construction> findByType(EConstructionType type) {
        Construction c = new Construction();
        c.setType(type);
        ArrayList<Construction> result = find(c);

        return result;
    }

    public Construction findById(Integer constructionId) {
        Construction c = new Construction();
        c.setId(constructionId);
        return (Construction)get(c);
    }

}
