/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.filter;

import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.PlanetFilterResult;
import at.darkdestiny.core.result.PlanetFilterResultEntry;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.view.header.PlanetEntity;
import at.darkdestiny.core.view.header.SystemEntity;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class PlanetFilter extends Service {

    PlanetFilterResult pfr;

    public PlanetFilter(HashMap<Integer, SystemEntity> allActSystems, int fromItem, int toItem, int filterId, int userId) {
        // TreeMap<Integer, PlanetFilterResultEntry> result = new TreeMap<Integer, PlanetFilterResultEntry>();
        ArrayList<PlanetFilterResultEntry> result = new ArrayList<PlanetFilterResultEntry>();

        int maxItems = 0;
        int item = 0;

        // Presort the list by Planet Name
        TreeMap<String, ArrayList<PlayerPlanet>> sortedByName = new TreeMap<String, ArrayList<PlayerPlanet>>();

        for (Map.Entry<Integer, SystemEntity> sys : allActSystems.entrySet()) {
            for (Map.Entry<Integer, PlanetEntity> pEntry : sys.getValue().getPlanets().entrySet()) {
                PlayerPlanet pp = playerPlanetDAO.findByPlanetId(pEntry.getKey());
                if (sortedByName.containsKey(pp.getName())) {
                    sortedByName.get(pp.getName()).add(pp);
                } else {
                    ArrayList<PlayerPlanet> ppList = new ArrayList<PlayerPlanet>();
                    ppList.add(pp);
                    sortedByName.put(pp.getName(), ppList);
                }
            }
        }

        for (Map.Entry<String, ArrayList<PlayerPlanet>> namedEntry : sortedByName.entrySet()) {
            for (PlayerPlanet pp : namedEntry.getValue()) {
                // for (Map.Entry<Integer, SystemEntity> sys : allActSystems.entrySet()) {
                // log.debug("Checking System (id: " + sys.getId()+")");
                // for (Map.Entry<Integer, PlanetEntity> pEntry : sys.getValue().getPlanets().entrySet()) {
                // int pId = pEntry.getKey();
                // PlayerPlanet pp = playerPlanetDAO.findByPlanetId(pId);
                int pId = pp.getPlanetId();

                if (pp == null || pp.getInvisibleFlag()) {
                    continue;
                }
                try {
                    if (filterId > 0) {
                        PlanetFilterResultEntry pfre = new PlanetFilterResultEntry(filterId, pId);
                        if (!pfre.isShow()) {
                            continue;
                        } else {
                            if (item < fromItem) {
                                item++;
                                continue;
                            }
                            if (item > toItem) {
                                item++;
                                continue;
                            }
                            item++;
                            // result.put(pId, pfre);
                            result.add(pfre);
                        }
                    } else {
                        PlanetFilterResultEntry pfre = new PlanetFilterResultEntry(0, pId);

                        if (item < fromItem) {
                            item++;
                            continue;
                        }
                        if (item > toItem) {
                            item++;
                            continue;
                        }
                        // result.put(pId, pfre);
                        result.add(pfre);
                        item++;
                    }

                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error in Systemverwaltung bei Planetcalculation der Planeten" + e);
                    DebugBuffer.writeStackTrace("Error", e);
                }
            }
        }

        maxItems = item;
        pfr = new PlanetFilterResult(item, maxItems, result);
    }

    public PlanetFilterResult getResult() {
        return pfr;
    }
}
