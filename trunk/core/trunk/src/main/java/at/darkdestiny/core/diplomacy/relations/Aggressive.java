/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.diplomacy.relations;

import at.darkdestiny.core.enumeration.EOwner;
import at.darkdestiny.core.enumeration.ESharingType;
import at.darkdestiny.core.result.DiplomacyResult;

/**
 *
 * @author Bullet
 */
public class Aggressive implements IRelation {

    public DiplomacyResult getDiplomacyResult() {

        boolean attacks = true;
        boolean helps = false;
        boolean notification = false;
        boolean attackTradeFleets = true;
        ESharingType sharingStarMapInfo = ESharingType.NONE;
        boolean ableToUseShipyard = false;
        EAttackType battleInNeutral = EAttackType.YES;
        EAttackType battleInOwn = EAttackType.YES;
        EOwner owner = EOwner.AGGRESSIVE;
        String color = "#FF0000";

        DiplomacyResult dr = new DiplomacyResult(attacks, helps, notification, attackTradeFleets, color, sharingStarMapInfo, ableToUseShipyard , battleInNeutral, battleInOwn, owner);
        return dr;
    }
}
