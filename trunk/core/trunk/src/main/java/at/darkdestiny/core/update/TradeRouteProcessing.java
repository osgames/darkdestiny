/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.update;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.TradePostDAO;
import at.darkdestiny.core.enumeration.ETradeRouteStatus;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.TradePost;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.core.update.transport.CapacityMultiEdge;
import at.darkdestiny.core.update.transport.DirectionalEdge;
import at.darkdestiny.core.update.transport.PlanetNode;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Stefan
 */
public class TradeRouteProcessing extends AbstractRouteProcessing {

    private static final Logger log = LoggerFactory.getLogger(TradeRouteProcessing.class);

    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static TradePostDAO tpDAO = (TradePostDAO) DAOFactory.get(TradePostDAO.class);

    public TradeRouteProcessing(UpdaterDataSet uds) {
        super(uds);
        initRoutes();
    }

    private void initRoutes() {
        if (debug) {
            log.debug("Start init of Traderoute Handler");
        }

        allDetailEntries.clear();
        clearAllSetRoutes();

        ArrayList<TradeRoute> allRoutes = trDAO.findAllByType(ETradeRouteType.TRADE);
        for (TradeRoute tr : allRoutes) {
            if (debug) {
                log.debug("Found route from " + tr.getStartPlanet() + " to " + tr.getTargetPlanet());
            }

            if (tr.getStatus().equals(ETradeRouteStatus.PENDING)) {
                continue;
            }

            TradeRouteExt tre = new TradeRouteExt(tr);
            TradeRouteProcessingEntry trpe = new TradeRouteProcessingEntry(tre);
            allEntries.put(tr.getId(), trpe);
            allDetailEntries.addAll(trpe.getFromToList());
            allDetailEntries.addAll(trpe.getToFromList());

            int actStart = tr.getStartPlanet();
            int actTarget = tr.getTargetPlanet();

            PlanetNode start = tg.getPlanet(actStart);
            PlanetNode target = tg.getPlanet(actTarget);

            if (start == null) {
                start = new PlanetNode(actStart);
                tg.addPlanet(start);
            }
            if (target == null) {
                target = new PlanetNode(actTarget);
                tg.addPlanet(target);
            }

            // Set global outgoing for planets
            PlanetConstruction pcTradeStart = pcDAO.findBy(start.getPlanetId(), Construction.ID_INTERSTELLARTRADINGPOST);
            PlanetConstruction pcTradeTarget = pcDAO.findBy(target.getPlanetId(), Construction.ID_INTERSTELLARTRADINGPOST);

            // NEW
            if (pcTradeStart != null) {
                int capTp = getTradePostCapacity(tre,pcTradeStart);

                CapacityMultiEdge cme = start.getCapMultiEdge();
                if (cme == null) {
                    cme = new CapacityMultiEdge(capTp);
                    start.setCapacityMultiEdge(cme);
                }
            }

            if (pcTradeTarget != null) {
                int capTp = getTradePostCapacity(tre,pcTradeTarget);

                CapacityMultiEdge cme = target.getCapMultiEdge();
                if (cme == null) {
                    cme = new CapacityMultiEdge(capTp);
                    target.setCapacityMultiEdge(cme);
                }
            }
            // ***

            DirectionalEdge fromTo = new DirectionalEdge(start,target,tre);
            DirectionalEdge toFrom = new DirectionalEdge(target,start,tre);

            if (trpe.isFromTo()) {
                for (TradeRouteDetailProcessing trdp : trpe.getFromToList()) {
                    if (trdp.getBase().getDisabled()) continue;
                    fromTo.addDetail(trdp);

                    CapacityMultiEdge cme = start.getCapMultiEdge();
                    if (cme != null) {
                        cme.addTradeRouteDetailProcessing(trdp);
                    }
                }
            }

            if (trpe.isToFrom()) {
                for (TradeRouteDetailProcessing trdp : trpe.getToFromList()) {
                    if (trdp.getBase().getDisabled()) continue;
                    toFrom.addDetail(trdp);

                    CapacityMultiEdge cme = target.getCapMultiEdge();
                    if (cme != null) {
                        cme.addTradeRouteDetailProcessing(trdp);
                    }
                }
            }

            start.addIncomingTrade(toFrom);
            start.addOutgoingTrade(fromTo);

            target.addIncomingTrade(fromTo);
            target.addOutgoingTrade(toFrom);
        }
    }

    private int getTradePostCapacity(TradeRouteExt tre, PlanetConstruction pc) {
        int capacity = 0;

        TradePost tp = tpDAO.findById(tre.getBase().getTradePostId());
        int level = pc.getLevel();
        if(level <= 0){
            level = 1;
        }

        double capBoost = tp.getCapBoost();
        capacity += ((double)tp.getTransportPerLJ() * (double)level * capBoost);

        return capacity;
    }
}
