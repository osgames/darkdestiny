/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ImperialStatistic;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class ImperialStatisticDAO extends ReadWriteTable<ImperialStatistic> implements GenericDAO {

    public ArrayList<ImperialStatistic> findByUserId(int userId){
        ImperialStatistic is = new ImperialStatistic();
        is.setUserId(userId);
        return find(is);
    }
        public ArrayList<ImperialStatistic> findByAllianceId(int allianceId){
        ImperialStatistic is = new ImperialStatistic();
        is.setAllyId(allianceId);
        return find(is);
    }
}
