/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Stefan
 */
public class JoinMapXMLParser extends DefaultHandler {
    private final JoinMapDataSource jmds;
    private char letter1 = 65;
    private char letter2 = 65;
    
    public JoinMapXMLParser(JoinMapDataSource jmds) {
        super();        
        this.jmds = jmds;
    }
    
    @Override
    public void startElement(String namespaceURI,
                             String localName,
                             String qName, 
                             Attributes atts)
        throws SAXException {

        String key = qName; 

        if (key.equalsIgnoreCase("tile")) {
            int intensity = 0;
            int maxSysCount = 0;
            int maxSysCountPerTile = 0;
            int colSysCount = 0;
            int startSysCount = 0;
            int popScore = 0;
            int sysCount = 0;
            int systems = 0;
            int userScore = 0;
            int xStart = 0;
            int yStart = 0;                                              
            
            try { intensity = Integer.parseInt(atts.getValue("intensity")); } catch (NumberFormatException e) { }
            try { maxSysCount = Integer.parseInt(atts.getValue("maxSysCount")); } catch (NumberFormatException e) { }
            try { maxSysCountPerTile = Integer.parseInt(atts.getValue("maxSysCountPerTile")); } catch (NumberFormatException e) { }
            try { colSysCount = Integer.parseInt(atts.getValue("colSysCount")); } catch (NumberFormatException e) { }
            try { startSysCount = Integer.parseInt(atts.getValue("startSysCount")); } catch (NumberFormatException e) { }
            try { popScore = Integer.parseInt(atts.getValue("popScore")); } catch (NumberFormatException e) { }
            try { sysCount = Integer.parseInt(atts.getValue("sysCount")); } catch (NumberFormatException e) { }
            try { systems = Integer.parseInt(atts.getValue("systems")); } catch (NumberFormatException e) { }
            try { userScore = Integer.parseInt(atts.getValue("userScore")); } catch (NumberFormatException e) { }
            try { xStart = Integer.parseInt(atts.getValue("x")); } catch (NumberFormatException e) { }
            try { yStart = Integer.parseInt(atts.getValue("y")); } catch (NumberFormatException e) { }                        
                        
            JMTile jmTile = new JMTile(xStart,yStart,100);  
            
            jmTile.setIntensity(intensity);
            jmTile.setMaxSysCount(maxSysCount);
            jmTile.setMaxSysCountPerTile(maxSysCountPerTile);
            jmTile.setColSysCount(colSysCount);
            jmTile.setStartSysCount(startSysCount);
            jmTile.setPopScore(popScore);
            jmTile.setSysCount(sysCount);
            jmTile.setSystems(systems);
            jmTile.setUserScore(userScore);
            jmTile.setIdentifier(String.valueOf(letter1) + String.valueOf(letter2));

            letter2++;
            if (letter2 > 90) {
                letter2 = 65;
                letter1++;
            }
            
            jmds.addTile(jmTile);
        } else if (key.equalsIgnoreCase("galaxy")) {
            int id = 0;
            int endSystem = 0;
            int height = 0;
            int width = 0;
            int originX = 0;
            int originY = 0;
            boolean playerStart = false;
            int startSystem = 0;       
            
            JMGalaxy jmGalaxy = new JMGalaxy();            
            
            try { id = Integer.parseInt(atts.getValue("id")); } catch (NumberFormatException e) { }
            try { endSystem = Integer.parseInt(atts.getValue("endSystem")); } catch (NumberFormatException e) { }
            try { height = Integer.parseInt(atts.getValue("height")); } catch (NumberFormatException e) { }
            try { width = Integer.parseInt(atts.getValue("width")); } catch (NumberFormatException e) { }
            try { originX = Integer.parseInt(atts.getValue("originX")); } catch (NumberFormatException e) { }
            try { originY = Integer.parseInt(atts.getValue("originY")); } catch (NumberFormatException e) { }
            try { playerStart = Boolean.parseBoolean(atts.getValue("playerStart")); } catch (NumberFormatException e) { }
            try { startSystem = Integer.parseInt(atts.getValue("startSystem")); } catch (NumberFormatException e) { }  
            
            jmGalaxy.setId(id);
            jmGalaxy.setEndSystem(endSystem);
            jmGalaxy.setHeight(height);
            jmGalaxy.setWidth(width);
            jmGalaxy.setOriginX(originX);
            jmGalaxy.setOriginY(originY);
            jmGalaxy.setPlayerStart(playerStart);
            jmGalaxy.setStartSystem(startSystem);
            
            jmds.addGalaxy(jmGalaxy);
        }
    }    
}
