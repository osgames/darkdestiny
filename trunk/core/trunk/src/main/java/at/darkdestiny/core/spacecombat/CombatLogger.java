/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

 import java.io.BufferedWriter;import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class CombatLogger {

    private static final Logger log = LoggerFactory.getLogger(CombatLogger.class);

    private static final String logFile = "C:\\combat\\combatLog.txt";
    private static CombatLogger instance = null;

    private static HashMap<Integer,CombatLogEntry> entries = new HashMap<Integer,CombatLogEntry>();

    private CombatLogger() {

    }

    public static CombatLogger getInstance() {
        if (instance == null) {
            instance = new CombatLogger();
        }

        return instance;
    }

    public void addLoggedShip(BattleShipNew bs, int round) {
        if (bs == null) {
            log.debug("NULL FOUND!");
            return;
        }
        entries.put(bs.getDesignId(), new CombatLogEntry(round,bs.getDesignId(),bs.getName(),bs.getCount()));
    }

    public void setName(int designId, String name) throws Exception {
        if (!entries.containsKey(designId)) throw new Exception("Ship "+designId+" not logged");

        entries.get(designId).setShipName(name);
    }

    public void addShieldDamage(int designId, int value) throws Exception {
        if (!entries.containsKey(designId)) throw new Exception("Ship "+designId+" not logged");

        entries.get(designId).addShieldDamage(value);
    }

    public void addHpDamage(int designId, int value) throws Exception {
        if (!entries.containsKey(designId)) throw new Exception("Ship "+designId+" not logged");

        entries.get(designId).addHpDamage(value);
    }

    public void incDestroyed(int designId, int value) throws Exception {
        if (!entries.containsKey(designId)) throw new Exception("Ship "+designId+" not logged");

        entries.get(designId).incDestroyed();
    }

    public void printLog() {
        try {
            FileWriter fstream = new FileWriter(logFile, true);
            BufferedWriter out = new BufferedWriter(fstream);

            boolean headWritten = false;

            for (Map.Entry<Integer,CombatLogEntry> me : entries.entrySet()) {
                CombatLogEntry cle = me.getValue();

                if ((cle.getHpDamageTaken() <= 0) || (cle.getShieldDamageTaken() <= 0)) continue;

                if (!headWritten) {
                    out.write("===== ROUND " + cle.getRound() + " =====\n");
                    headWritten = true;
                }

                out.write("Ship " + cle.getDesignName() + "("+cle.getDesignId()+") [Count: "+cle.getTotalCount()+"]:\n");
                out.write("HP Damage taken: " + cle.getHpDamageTaken() + " Shield Damage taken: " + cle.getShieldDamageTaken() + " Destroyed: " + cle.getDestroyed()+"\n");
                out.write("\n");
            }

            out.close();

            entries.clear();
        } catch (Exception e) {
            log.error("CombatLog: " + e.getMessage());
        }
    }
}
