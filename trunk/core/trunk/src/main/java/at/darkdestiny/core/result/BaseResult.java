/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.util.DebugBuffer;

/**
 *
 * @author Stefan
 */
public class BaseResult {
    protected final boolean error;
    protected final String message;
    protected final Exception e;
    protected final boolean internalError;
    
    public BaseResult(Exception e) {
        error = true;
        message = e.getMessage();
        
        this.e = e;
        internalError = true;
    }
    
    public BaseResult(String message, boolean error) {
        this.message = message;
        this.error = error;
        
        this.e = null;
        internalError = false;
    }
    
    public BaseResult(boolean error) {
        this.message = "";
        this.error = error;   
        
        this.e = null;
        internalError = false;
    }    

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getFormattedString(){
        String result = "";
        result += "<B>";

        String color = "#FFFFFF";
        if(error){
            color = "#FF0000";
        }else if(!error){
            color = "#00FF00";
        }
        result += "<FONT color='" + color + "'>";
        result += message;
        result += "</FONT>";
        result += "</B>";
        return result;
    }

    public boolean isInternalError() {
        return internalError;
    }
    
    public String getStackTrace() {
        if (e != null) {
            StringBuilder sb = new StringBuilder(e.getMessage() + " (" + e.getClass().getName() + ")");
            for (StackTraceElement ste : e.getStackTrace()) {
                sb.append("<br>" + ste.toString());
            }
            if (e.getCause() != null) {
                sb.append(DebugBuffer.produceStackTrace(e.getCause()));
            }

            return sb.toString();
        } else {
            return "NA";
        }
    }
}
