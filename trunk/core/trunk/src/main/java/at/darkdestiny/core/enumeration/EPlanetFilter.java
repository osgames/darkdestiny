/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Stefan
 */
public enum EPlanetFilter {
    ALL, CAN_PRODUCE, EMPTY_BUILD_QUEUE, NEAR_FULL_POP, NO_MINABLE_RESSOURCES,
    EMPTY_SHIP_BUILD_QUEUE;
}
