/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.buildable;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.dao.GroundTroopDAO;
import at.darkdestiny.core.dao.RessourceCostDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.ERessourceCostType;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.RessourceCost;
import at.darkdestiny.core.ships.BuildTime;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class GroundTroopExt implements Buildable {
    GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);

    private final GroundTroop troop;
    private final RessourcesEntry re;
    private final BuildTime bt;

    public GroundTroopExt(int id) {
        troop = gtDAO.findById(id);

        ArrayList<RessAmountEntry> raeList = new ArrayList<RessAmountEntry>();
        ArrayList<RessourceCost> rcList = rcDAO.find(ERessourceCostType.GROUNDTROOPS,id);

        for (RessourceCost rc : rcList) {
            RessAmountEntry rae = new RessAmountEntry(rc.getRessId(),rc.getQty());
            raeList.add(rae);
        }

        re = new RessourcesEntry(raeList);

        bt = new BuildTime();
        bt.setModulePoints(troop.getModulPoints());
        bt.setKasPoints(troop.getCrew());
    }

    @Override
    public RessourcesEntry getRessCost() {
        return re;
    }

    @Override
    public Model getBase() {
        return troop;
    }

    @Override
    public BuildTime getBuildTime() {
        return bt;
    }

    @Override
    public Class getModelClass() {
        return GroundTroop.class;
    }
}
