/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.CampaignToUser;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class CampaignToUserDAO extends ReadWriteTable<CampaignToUser> implements GenericDAO  {
    public CampaignToUser findBy(int userId, int campaignId) {
        CampaignToUser c = new CampaignToUser();
        c.setUserId(userId);
        c.setCampaignId(campaignId);
        return (CampaignToUser)get(c);
    }

}
