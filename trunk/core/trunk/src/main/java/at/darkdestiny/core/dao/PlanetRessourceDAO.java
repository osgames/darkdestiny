/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Lists;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlanetRessourceDAO extends ReadWriteTable<PlanetRessource> implements GenericDAO {

    public ArrayList<PlanetRessource> findByPlanetId(Integer planetId) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        return find(pr);
    }

    public PlanetRessource findBy(Integer planetId, Integer ressourceId, EPlanetRessourceType type) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setRessId(ressourceId);
        pr.setType(type);

        return (PlanetRessource) get(pr);
    }

    public PlanetRessource create(PlanetRessource u) {
        PlanetRessource uNew = this.add(u);
        return uNew;
    }

    public PlanetRessource updateRessource(PlanetRessource u) {
        PlanetRessource uNew = this.update(u);
        return uNew;
    }

    public ArrayList<PlanetRessource> findMineableRessources(int planetId) {

        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setType(EPlanetRessourceType.PLANET);

        return find(pr);
    }

    public PlanetRessource findMinStockProduction(int ressourceId, int planetId) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setRessId(ressourceId);
        pr.setType(EPlanetRessourceType.MINIMUM_PRODUCTION);

        ArrayList<PlanetRessource> result = find(pr);

        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<PlanetRessource> findBy(int planetId, EPlanetRessourceType ePlanetRessourceType) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setType(ePlanetRessourceType);

        return find(pr);

    }
    
    public ArrayList<PlanetRessource> findAllByPlanetList(ArrayList<PlayerPlanet> ppList) {
        // DebugBuffer.trace("Start collecting PR Data");  
        ArrayList<PlanetRessource> result = Lists.newArrayList();
        
        for (PlayerPlanet pp : ppList) {
            result.addAll(findByPlanetId(pp.getPlanetId()));
        }
        
        // DebugBuffer.trace("End collecting PR Data");  
        return result;
    }
}
