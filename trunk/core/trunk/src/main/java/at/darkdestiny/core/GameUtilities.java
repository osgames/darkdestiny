/*
 * GameUtilities.java
 *
 * Created on 12. März 2003, 20:47
 */
package at.darkdestiny.core;

import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Rayden
 */
public class GameUtilities {
    public static UserDAO userDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    /**
     * @return the shutDownMsg
     */
    public static String getShutDownMsg() {
        return shutDownMsg;
    }

    /**
     * @param aShutDownMsg the shutDownMsg to set
     */
    public static void setShutDownMsg(String aShutDownMsg) {
        shutDownMsg = aShutDownMsg;
    }

    public static Calendar getDate() {

        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -20);
        return c;
    }

    public static int getTicksPerDay() {

        int msPerDay = 1000 * 60 * 60 * 24;
        int ticksPerDay = (int)((double)msPerDay/(double)Service.gameDataDAO.findAll().get(0).getTickTime());
        return ticksPerDay;
    }

    public static int getTicksPerHour() {

        int msPerDay = 1000 * 60 * 60;
        int ticksPerDay = (int)((double)msPerDay/(double)Service.gameDataDAO.findAll().get(0).getTickTime());
        return ticksPerDay;
    }

    public static int getTicksFromMinutes(int minutes) {

        int msPerDay = 1000 * 60 * minutes;
        int ticksPerDay = (int)((double)msPerDay/(double)Service.gameDataDAO.findAll().get(0).getTickTime());
        return ticksPerDay;
    }

    private int userId;
    private int planetId;
    public static long timeStamp = 0;
    private static long lastRestart = 0;
    private static String shutDownMsg = "";

    public GameUtilities() {
        // Nothing to init!
    }

    public GameUtilities(int userId, int planetId) {
        this.userId = userId;
        this.planetId = planetId;
    }

    public static void setTimeStamp() {
        timeStamp = System.currentTimeMillis();
    }

    public static long compareTimeStamp() {
        return (System.currentTimeMillis() - timeStamp);
    }

    public int getCurrentTick() {
        long timeDiff = System.currentTimeMillis() - GameConfig.getInstance().getStarttime();
        int currentTick = (int) (timeDiff / GameConfig.getInstance().getTicktime());

        return currentTick;
    }

    public static long getMillisNextTick() {
        long millis = 0;

        long timeDiff = System.currentTimeMillis() - GameConfig.getInstance().getStarttime();
        millis = timeDiff % GameConfig.getInstance().getTicktime();

        double currentTickDbl = ((double) timeDiff / (double) GameConfig.getInstance().getTicktime()) - (double) getCurrentTick2();

        millis = (long) (GameConfig.getInstance().getTicktime() * (1d - currentTickDbl));

        return millis;
    }

    public static int getCurrentTick2() {
        long timeDiff = System.currentTimeMillis() - GameConfig.getInstance().getStarttime();
        int currentTick = (int) (timeDiff / GameConfig.getInstance().getTicktime());

        return currentTick;
    }

    public int getSwitchInfo() {
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("select userID from playerplanet where planetID=" + planetId);

            if (rs.next()) {
                if (userId == rs.getInt(1)) {
                    return (GameConstants.OWN_PLANET);
                } else {
                    return (GameConstants.ENEMY_PLANET);
                }
            }
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.addLine(e);
        }

        return (GameConstants.NEUTRAL_PLANET);
    }

    public int getRandomNo(int number) {
        return (int) Math.ceil(Math.random() * number);
    }

    public static int getRandomNo2(int number) {
        return (int) Math.ceil(Math.random() * number);
    }

    public int getRandomNo(int min, int max) {
        return (min + (int) ((Math.random()) * (max - min) + 1));
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public static int getTotalUsers() {
        return userDAO.findAllPlayers().size();
    }

    @Deprecated
    public static int getUsersOnlineSinceMinutes(int minutes) {
        int count = 0;
        int ticks = getTicksFromMinutes(minutes);
        
        for (User u : (ArrayList<User>) userDAO.findAllPlayers()) {
            if (u.getLastUpdate() > (getCurrentTick2() - ticks)) {
                count++;
            }
        }
        return count;
    }
    
    @Deprecated
    public static int getUsersOfflineSinceMinutes(int minutes) {
        int count = 0;
        int ticks = getTicksFromMinutes(minutes);
        for (User u : (ArrayList<User>) userDAO.findAllPlayers()) {
            if (u.getLastUpdate() < (getCurrentTick2() - ticks)) {
                count++;
            }
        }
        return count;
    }

    public int getPlanetId() {
        return planetId;
    }

    public void setPlanetId(int planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the lastRestart
     */
    public static String getLastRestart() {
        java.util.Date msgDate = new java.util.Date(lastRestart);
        String msgDateString = java.text.DateFormat.getDateInstance().format(msgDate) + " " + java.text.DateFormat.getTimeInstance().format(msgDate);
        return msgDateString;
    }

    /**
     * @param lastRestart the lastRestart to set
     */
    public static void setLastRestart(long lastRestart) {
        GameUtilities.lastRestart = lastRestart;
    }
    
    public static int getActivePlayerCountForResearch() {
        // As active player counts, who has a starting location selected
        int count = 0;
        
        for (User u : userDAO.findAll()) {
            if (u.getSystemAssigned()) {
                count++;
            }
        }
        
        return count;
    }    
}
