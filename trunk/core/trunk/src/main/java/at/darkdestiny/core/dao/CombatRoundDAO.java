/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.CombatRound;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class CombatRoundDAO  extends ReadWriteTable<CombatRound> implements GenericDAO  {

    public ArrayList<CombatRound> findByCombatId(Integer combatRoundId) {
        ArrayList<CombatRound> result = new ArrayList<CombatRound>();
        TreeMap<Integer, CombatRound> sorted = new TreeMap<Integer, CombatRound>();

        CombatRound cr = new CombatRound();
        cr.setCombatId(combatRoundId);

        result = find(cr);


        for(CombatRound crTmp : result){
            sorted.put(crTmp.getRound(), crTmp);
        }
        result.clear();
        result.addAll(sorted.values());
        return result;
    }
    public CombatRound findLastRound(Integer combatRoundId) {
        ArrayList<CombatRound> result = new ArrayList<CombatRound>();
        TreeMap<Integer, CombatRound> sorted = new TreeMap<Integer, CombatRound>();

        CombatRound cr = new CombatRound();
        cr.setCombatId(combatRoundId);

        result = find(cr);
        if(result.isEmpty()){
            return null;
        }
        for(CombatRound crTmp : result){
            sorted.put(crTmp.getRound(), crTmp);
        }
        return sorted.get(sorted.size());
    }


}
