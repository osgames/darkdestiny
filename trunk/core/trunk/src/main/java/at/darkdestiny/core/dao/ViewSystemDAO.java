/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EViewSystemLocationType;
import at.darkdestiny.core.model.ViewSystem;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Eobane
 */
//public class ViewTableDAO extends ReadOnlyTable<ViewTable> implements GenericDAO {
public class ViewSystemDAO extends ReadWriteTable<ViewSystem> implements GenericDAO {

    public ViewSystem findBy(int locationId, EViewSystemLocationType locationType, int userId) {
            ViewSystem viewSystem = new ViewSystem();
            viewSystem.setLocationId(locationId);
            viewSystem.setLocationType(locationType);
            viewSystem.setUserId(userId);
            return get(viewSystem);
    }

}
