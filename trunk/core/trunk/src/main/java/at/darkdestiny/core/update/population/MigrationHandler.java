package at.darkdestiny.core.update.population;

 import at.darkdestiny.core.dao.PlanetConstructionDAO;import static java.lang.Math.*;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.service.PlanetService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.update.Updater2;
import at.darkdestiny.framework.dao.DAOFactory;
import static java.lang.Math.*;
import java.util.ArrayList;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.service.PlanetService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.update.Updater2;
import static java.lang.Math.*;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Das Ziel dieser Klasse ist, eine Bev&ouml;lkerungsbewegung zu simulieren,
 * wenn an bestimmten Stellen Bev&ouml;lkerungs&uuml;berschuss besteht, und an
 * anderen bessere Lebensbedingungen erwartet werden
 *
 * Folgende Faktoren werden Herangezogen:
 *
 * - Moral der Bev&ouml;lkerung - Anteil an der Maximalbev&ouml;lkerung
 *
 * Die Abwanderung erfolgt st&auml;rker, wenn ein "Ziviler Raumflughafen" auf
 * diesem Planet und dem Zielplanet besteht.
 *
 * Es erfolgt die Bewegung zwischen eigenen und fremden Planeten, wobei die
 * zwischen eigenen h&ouml;her ist. Die Relation zwischen diesen kann durch die
 * Faktoren MOVEMENT_WITHHIN_IMPERIUM (f&uuml;r Bewegung zwischen Eigenen) und
 * MOVEMENT_BETWEEN_IMPERIEN (f&uuml;r Bewegung &uuml;ber alle Grenzen hinweg)
 * eingestellt werden
 *
 * Der Einfluss der Bev&ouml;lkerung ist sehr gering (max 1 Moralpunkt)
 *
 * @author martin
 */
public class MigrationHandler extends Service {

    private static final Logger log = LoggerFactory.getLogger(MigrationHandler.class);

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    public final static float MIN_SCORE = - 100;
    public final static float MAX_SCORE = 100;
    private boolean doDebug_Migration = false;
    public final static float FACTOR_NO_RAUMHAFEN = 1.3e-5f;
    // Doppelt so gro&szlig; wie ohne Raumhafen ...
    public final static float FACTOR_WITH_RAUMHAFEN = 2f * FACTOR_NO_RAUMHAFEN;
    public final static float MOVEMENT_WITHHIN_IMPERIUM = 1;
    public final static float MOVEMENT_BETWEEN_IMPERIEN = 0.1f;
    public final static float MAX_ZUWANDERUNGSFAKTOR = 2f;
    // Wenn der Sollwert an zuwanderung gr&ouml;&szlig;er ist als
    // der tats&auml;chliche Wert, wird bei diesem Faktor abgeschnitten.
    // Wegen Einreisebeschr&auml;nkungen  ;-)
    // Hat nur den Sinn die Wanderungsbewegung in Grenzen zu halten ...
    // Hierdrin werden die globalen Bewegungen eingestellt
    private ImperiumHolder allPlanets = new ImperiumHolder(MOVEMENT_BETWEEN_IMPERIEN, -1);    // Die Planeten sortiert nach besitzer
    private final TreeMap<Integer, ImperiumHolder> imperiumsPlaneten = new TreeMap<Integer, ImperiumHolder>();

    public void calculatePlanet(Updater2 updater) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Hiermit werden f&uuml;r die einzelnen Imperien die Zu- /
     * Abwanderungsplaneten gespeichert.
     *
     * @author martin
     */
    class ImperiumHolder {

        ArrayList<MyPlanetData> einwanderungsPlaneten = new ArrayList<MyPlanetData>();
        ArrayList<MyPlanetData> auswanderungsPlaneten = new ArrayList<MyPlanetData>();
        float movementFactor;
        int player;

        public ImperiumHolder(float movementFactor, int player) {
            this.movementFactor = movementFactor;
            this.player = player;
        }

        /**
         * Planet richtig einsortieren
         */
        public void addPlanet(MyPlanetData pd) {
            if (pd.score < 0) {
                auswanderungsPlaneten.add(pd);
            } else {
                einwanderungsPlaneten.add(pd);
            }
        }

        /**
         * Den Handler mit den richtigen Parametern ausf&uuml;hren
         */
        public void doMigration(MigrationHandler handler) {
            handler.doMigration(einwanderungsPlaneten, auswanderungsPlaneten, movementFactor);
        }

        /**
         * @return die ID des spielers
         */
        public int getPlayer() {
            return player;
        }
    }

    /**
     * Alles was ich zu einem Planeten wissen muss
     *
     * @author martin
     */
    class MyPlanetData {

        float score;
        long maxpop;
        PlayerPlanet pp;

        public MyPlanetData(float score, PlayerPlanet pp, long maxPop) {
            this.score = score;
            this.maxpop = maxPop;
            this.pp = pp;
        }
    }

    public MigrationHandler() {
        // Empty constructor, on purpose
    }

    /**
     *
     * Setzt: prodConsBuffer = Welche Geb&auml;ude stehen auf welchem Planeten?
     */
    public void preTickInit() {
        // Clear all settings:
        cleanUp();
    }

    /**
     * Einen weiteren Planeten verarbeiten
     *
     * @param data welchen?
     */
    public synchronized void calculatePlanet(PlayerPlanet pp, ExtPlanetCalcResult epcr) {
        float planetScore = getScore(pp, epcr);

        //Planeten, denen es nicht zu gut, aber auch nicht zu schlecht geht,
        //ignorieren. Keine Bev&ouml;lkerungsbewegung
        //Zuwanderung = Abwanderung
        /*
         if (abs(planetScore) < 0.1f * FACTOR_NO_RAUMHAFEN) {
         return;
         }
         */

        MyPlanetData pd = new MyPlanetData(planetScore, pp, epcr.getMaxPopulation());
        pd.maxpop = epcr.getMaxPopulation();
        //In die Globale Migrations-Liste eintragen
        allPlanets.addPlanet(pd);

        //In den zu diesem Benutzer passenden Migrations-Liste eintragen
        int user = pp.getUserId();
        if (imperiumsPlaneten.get(user) == null) {
            imperiumsPlaneten.put(user, new ImperiumHolder(MOVEMENT_WITHHIN_IMPERIUM, user));
        }
        imperiumsPlaneten.get(user).addPlanet(pd);
    }

    /**
     * Gibt an, wie die Wanderbewegung diesen Planeten betrifft
     *
     * Derzeit:
     *
     * (Moral - 100) - (%Maxbev - 50)/100
     *
     * @param data welcher Planet
     * @return Zahlen > 0 geben eine zuwanderung an
     */
    private float getScore(PlayerPlanet pp, ExtPlanetCalcResult epcr) {
        if (pp.getPlanetId() == 15414) {
            doDebug_Migration = true;
        }

        double popMigrationValue = 0d;
        double foodMigrationValue = -20d;
        double moraleMigrationValue = -10d;

        Planet p = pDAO.findById(pp.getPlanetId());

        // Calculate Base Value popMigrationValue
        double planetFill = (100d / epcr.getMaxPopulation() * pp.getPopulation());

        if (doDebug_Migration) {
            log.debug("PlanetFill = " + planetFill);
        }

        if (planetFill < 50d) {
            popMigrationValue = 100d;
        } else if ((planetFill >= 50d) && (planetFill <= 90d)) {
            popMigrationValue = 100d / 40d * (90d - planetFill);
        } else if (planetFill > 90d) {
            popMigrationValue = -1d * (100d / 10d * (10d - (100d - planetFill)));
        }

        if (doDebug_Migration) {
            log.debug("popMigrationValue = " + popMigrationValue);
        }

        // Calculate Food Influence
        double foodMigrationBonus = 0d;

        if (epcr.getFoodcoverageNoStock() > 100d) {
            foodMigrationBonus += epcr.getFoodcoverageNoStock() - 100d;
        } else if (epcr.getFoodcoverage() > 100d) {
            foodMigrationBonus += (epcr.getFoodcoverage() - 100d) / 10d;
        } else {
            foodMigrationBonus -= (100d - epcr.getFoodcoverage()) * 10d;
        }

        if (foodMigrationBonus > 20d) {
            foodMigrationBonus = 20d;
        }

        if (doDebug_Migration) {
            log.debug("foodMigrationBonus = " + foodMigrationBonus);
        }
        foodMigrationValue += foodMigrationBonus;

        // Calculate Morale Influence
        if (epcr.getMorale() > 100d) {
            moraleMigrationValue += epcr.getMorale() - 100d;
        } else {
            moraleMigrationValue -= (100d - epcr.getMorale()) * 2d;
        }

        if (doDebug_Migration) {
            log.debug("moraleMigrationValue = " + moraleMigrationValue);
        }

        if (!(p.getLandType().equalsIgnoreCase("C")
                || p.getLandType().equalsIgnoreCase("M")
                || p.getLandType().equalsIgnoreCase("G"))) {
            if (popMigrationValue < 0d) {
                popMigrationValue = 0d;
            }
            if ((foodMigrationBonus >= 0d) && (foodMigrationValue < 0d)) {
                foodMigrationValue = 0d;
            }
        }

        if (doDebug_Migration) {
            log.debug("Final popMigrationValue = " + popMigrationValue + " Final foodMigrationValue = " + foodMigrationValue);
        }

        double res = popMigrationValue + foodMigrationValue + moraleMigrationValue;

        if (doDebug_Migration) {
            log.debug("res = " + res);
        }

        // Some randomizing so not everything looks so hard :)
        res += 10d - (Math.random() * 20d);

        if (doDebug_Migration) {
            log.debug("res after randomize = " + res);
        }

        if (doDebug_Migration) {
            log.debug("PID=" + pp.getPlanetId() + " PMV=" + popMigrationValue + " FMV=" + foodMigrationValue + " MMV" + moraleMigrationValue);
        }

        if (!(p.getLandType().equalsIgnoreCase("C")
                || p.getLandType().equalsIgnoreCase("M")
                || p.getLandType().equalsIgnoreCase("G"))) {
            if (res > 30) {
                res = 30d;
            }
        } else if (p.getLandType().equalsIgnoreCase("C")
                || p.getLandType().equalsIgnoreCase("G")) {
            if (res > 60) {
                res = 60d;
            }
        }

        if (doDebug_Migration) {
            log.debug("res after landtype = " + res);
        }

        if ((planetFill == 100d) && (res > 0d)) {
            res = 0d;
        }

        if (doDebug_Migration) {
            log.debug("res after planetfill = " + res);
        }

        PlanetConstruction spacePort = pcDAO.findBy(pp.getPlanetId(), 100);
        int civilSpaceportCount = 0;
        if (spacePort != null) {
            civilSpaceportCount = spacePort.getActiveCount();
        }

        if (civilSpaceportCount > 0) {
            res *= FACTOR_WITH_RAUMHAFEN;
        } else {
            res *= FACTOR_NO_RAUMHAFEN;
        }

        if (doDebug_Migration) {
            log.debug("res after spaceport = " + res);
        }

        if (pp.getDismantle()) {
            if (pp.getPopulation() > 0) {
                double refRes = -0.005d;
                double refValue = Math.min(5000d, pp.getPopulation());
                double minRes = (refValue / pp.getPopulation()) * -1d;
                res = Math.min(refRes, minRes);
            } else {
                res = -0.005d;
            }
        }

        if (doDebug_Migration) {
            log.debug("res after dismantle = " + res);
        }

        long maxChange = 150000 + (civilSpaceportCount * 250000);

        if ((res < 0) && (!pp.getDismantle())) {
            //Wenn Abwanderung teste wieviele Arbeiter auf dem Planeten ben�tigt werden
            try {
                // PlanetCalculation pc = new PlanetCalculation(pp);
                // long apRequired = pc.getPlanetCalcData().getExtPlanetCalcData().getUsedPopulation();
                // long minPop = PlanetService.findMinPopPossible(apRequired);
                long freePop = PlanetService.getFreePopulation(pp.getPlanetId());
                if (doDebug_Migration) {
                    log.debug("freePop = " + freePop);
                }

                if (maxChange > freePop) {
                    //   log.debug("Changing value for planet " + pp.getPlanetId());
                    //   log.debug("Pop - Minpop : " + pp.getPopulation() + " - "  +  minPop + " = " + (pp.getPopulation()-minPop));
                }

                long baseMaxChange = maxChange;

                if (doDebug_Migration) {
                    log.debug("maxChange = " + maxChange);
                }
                maxChange = Math.min(maxChange, freePop);
                if ((Math.abs(maxChange) > baseMaxChange) && (maxChange < 0)) {
                    maxChange = -baseMaxChange;
                }

                if (doDebug_Migration) {
                    log.debug("final maxChange = " + maxChange);
                }
                // if (p.getId() == 1688) DebugBuffer.warning("maxChange = " + maxChange + " (freePop = " + freePop + ")");
            } catch (Exception e) {
            }
        }

        // if (p.getId() == 1688) log.debug("res after worker check = " + res + " res * pp.getPopulation = " + (res * pp.getPopulation()) + " maxChange = " + maxChange);

        double currentValue = res * pp.getPopulation();
        if (doDebug_Migration) {
            log.debug("res before popAdjustment = " + res);
        }

        // if (p.getId() == 1688) DebugBuffer.warning("Running into case " + Math.signum(currentValue) + "/" + Math.signum(maxChange));
        if ((Math.signum(currentValue) == -1) && (Math.signum(maxChange) == -1)) {
            if (doDebug_Migration) {
                log.debug("Condition 1");
            }
            if (res * pp.getPopulation() < maxChange) {
                if (doDebug_Migration) {
                    log.debug("Condition 1a");
                }
                res = (double) maxChange / (double) pp.getPopulation() * -1d;
            }
        }

        if ((Math.signum(currentValue) == -1) && (Math.signum(maxChange) == 1)) {
            if (doDebug_Migration) {
                log.debug("Condition 2");
            }
            if (Math.abs(res * pp.getPopulation()) > maxChange) {
                if (doDebug_Migration) {
                    log.debug("Condition 2a");
                }
                res = (double) maxChange / (double) pp.getPopulation() * Math.signum(res);
            }
        }

        if ((Math.signum(currentValue) == 1) && (Math.signum(maxChange) == 1)) {
            if (doDebug_Migration) {
                log.debug("Condition 3");
            }
            if (res * pp.getPopulation() > maxChange) {
                if (doDebug_Migration) {
                    log.debug("Condition 3a");
                }
                res = (double) maxChange / (double) pp.getPopulation();
            }
        }

        // if (data.getUserId() == 30) log.debug("RES for PLANET ("+data.getUserId()+")" + data.getPlanetId() + " = " + res);
        // if (pp.getDismantle()) res = -0.005f;

        if (doDebug_Migration) {
            log.debug("final res = " + res);
        }

        if (pp.getPlanetId() == 15414) {
            doDebug_Migration = false;
        }

        return (float) res;
    }

    /**
     * F&uuml;r alle Reiche und das gesammte Universum die Migration
     * durchf&uuml;hren
     */
    public void globalUpdates() {
        allPlanets.doMigration(this);

        for (ImperiumHolder ih : imperiumsPlaneten.values()) {
            if (doDebug_Migration) {
                log.debug("CALCULATION FOR PLAYER: " + ih.getPlayer());
            }
            ih.doMigration(this);
        }

    }

    /**
     * Hiermit wird die Wanderung durchgef&uuml;hrt. Begrenzend ist die
     * Abwanderung, nicht die M&ouml;glichkeit irgendwohin zu wandern ...
     *
     * (ES GILT N&Auml;HERUNGSWEISE BEV&Ouml;LKERUNGSERHALTUNG AUF DIESEM LEVEL)
     */
    public void doMigration(ArrayList<MyPlanetData> einwanderungsPlaneten, ArrayList<MyPlanetData> auswanderungsPlaneten, float wanderungsFaktor) {
        //Anzahl der Wandernden berechnen (erste Sch&auml;tzung)
        float zugewandert = 0;
        for (MyPlanetData mpd : einwanderungsPlaneten) {
            if (doDebug_Migration) {
                log.debug("einwanderungs faktor = " + mpd.score);
            }
            zugewandert += mpd.score * mpd.pp.getPopulation();
        }

        if (doDebug_Migration) {
            log.debug("Zuwanderung ges: " + zugewandert);        //Wird irgendwohin zugewandert?
        }
        if (Math.abs(zugewandert) < 1) {
            return;        //Abwanderung sch&auml;tzen
        }
        float abgewandert = 0;

        for (MyPlanetData mpd : auswanderungsPlaneten) {
            abgewandert += mpd.score * mpd.pp.getPopulation();
        }
//		log.debug("Abwanderung ges: "+abgewandert);

        //Test: Gibt es wanderbewegungen
        if (Math.abs(abgewandert) < 1) {
            return;        //Zu Debugzwecken. Sollte ca. 0 sein ...
        }
        int popMiss = 0;

        //Endg&uuml;ltige faktoren berechnen
        float zuwanderungsFaktor = abs(abgewandert / zugewandert);
        float abwanderungsFaktor = 1.0f;

        if (zuwanderungsFaktor > MAX_ZUWANDERUNGSFAKTOR) {
            abwanderungsFaktor = MAX_ZUWANDERUNGSFAKTOR / zuwanderungsFaktor;
            zuwanderungsFaktor = MAX_ZUWANDERUNGSFAKTOR;
        }

        //Zuwanderung berechnen.
        //Hier wird halt gerundet
        for (MyPlanetData mpd : einwanderungsPlaneten) {
            PlayerPlanet pp = mpd.pp;
            // PlanetData pd = new PlanetData(mpd.planetId, prodConsBuffer);

            //Wieviel Bev&ouml;lkerung wandert:
            int wanderung = round(abs(mpd.score * pp.getPopulation() * zuwanderungsFaktor * wanderungsFaktor));

            if (doDebug_Migration) {
                log.debug("Zugewandert: " + pp.getPlanetId() + " : " + wanderung);
            }
            popMiss -= wanderung;
            pp.setPopulation(pp.getPopulation() + wanderung);
            pp.setMigration(wanderung);
            if (pp.getPopulation() > mpd.maxpop) {
                pp.setPopulation(mpd.maxpop);
            }
        }

        //Abwanderung durchf&uuml;hren
        for (MyPlanetData mpd : auswanderungsPlaneten) {
            PlayerPlanet pp = mpd.pp;
            // PlanetData pd = new PlanetData(mpd.planetId, prodConsBuffer);

            //Wieviel Bev&ouml;lkerung wandert:
            int wanderung = round(abs(mpd.score * pp.getPopulation() * abwanderungsFaktor * wanderungsFaktor));

            //Die Bev&ouml;lkerung von den Planeten entfernen
            popMiss += wanderung;
            if (doDebug_Migration) {
                log.debug("Abgewandert: " + pp.getPlanetId() + " : " + wanderung);
            }
            pp.setPopulation(pp.getPopulation() - wanderung);
            pp.setMigration(-wanderung);
        }
        if (doDebug_Migration) {
            log.debug("Pop Missing: " + popMiss);
        }
    }

    private void cleanUp() {
        for (ImperiumHolder tmpHolder : imperiumsPlaneten.values()) {
            tmpHolder.auswanderungsPlaneten.clear();
            tmpHolder.einwanderungsPlaneten.clear();
        }
        imperiumsPlaneten.clear();
        allPlanets = new ImperiumHolder(MOVEMENT_BETWEEN_IMPERIEN, -1);
    }

    public void storeData() {
        cleanUp();
    }
}
