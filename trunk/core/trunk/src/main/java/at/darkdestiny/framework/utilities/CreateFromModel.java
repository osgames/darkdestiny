/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.framework.utilities;

 import at.darkdestiny.core.enumeration.ERessourceCostType;import at.darkdestiny.core.model.Filter;
import at.darkdestiny.core.model.FilterToValue;
import at.darkdestiny.core.model.RessourceCost;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DataScope.EScopeType;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.enumeration.ERessourceCostType;
import at.darkdestiny.core.model.Filter;
import at.darkdestiny.core.model.FilterToValue;
import at.darkdestiny.core.model.RessourceCost;
import at.darkdestiny.core.service.Service;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author Stefan
 */
public class CreateFromModel {

    private static final Logger log = LoggerFactory.getLogger(CreateFromModel.class);

    /**
     * Scans all classes accessible from the context class loader which belong
     * to the given package and subpackages.
     *
     * @param packageName The base package
     * @return The classes
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);

        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Recursive method used to find all classes in a given directory and
     * subdirs.
     *
     * @param directory The base directory
     * @param packageName The package name for classes found inside the base
     * directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();

        ArrayList<File> files = Lists.newArrayList();
        if (directory.getAbsolutePath().contains("jar!")) {
            String path = directory.toString();
            String jarFileName = path.substring(0, path.indexOf("!")).replace("file:\\", "");

            try {

                ZipInputStream zip = new ZipInputStream(new FileInputStream(new File(jarFileName)));
                ZipEntry ze = null;

                while ((ze = zip.getNextEntry()) != null) {
                    String entryName = ze.getName();
                    if (!entryName.contains(packageName.replace(".", "/")) && entryName.endsWith(".class")) {
                        continue;
                    }
                    String fileName = entryName.substring(entryName.lastIndexOf("/") + 1);
                    files.add(new File(fileName));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (!directory.exists()) {
            return classes;
        } else {
            files = Lists.newArrayList(directory.listFiles());
        }
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                String className = packageName + '.' + file.getName().substring(0, file.getName().length() - 6);
                classes.add(Class.forName(className));
            }
        }

        return classes;
    }

    public static String createDropSQLForClass(Class c) {

        TableNameAnnotation ta = (TableNameAnnotation) c.getAnnotation(TableNameAnnotation.class);
        if (ta == null) {
            return null;
        }
        String sql = "DROP TABLE `" + ta.value() + "`";
        return sql;
    }

    public static String createSQLForClass(Class c) {
        String sql = "";

        TableNameAnnotation ta = (TableNameAnnotation) c.getAnnotation(TableNameAnnotation.class);
        if (ta == null) {
            return null;
        }

        sql += "CREATE TABLE `" + ta.value() + "` (";

        Field[] fields = c.getDeclaredFields();
        ArrayList<String> primary = new ArrayList<String>();

        boolean firstField = true;
        for (Field f : fields) {
            FieldMappingAnnotation fma = (FieldMappingAnnotation) f.getAnnotation(FieldMappingAnnotation.class);

            if (fma != null) {
                ColumnProperties cpa = (ColumnProperties) f.getAnnotation(ColumnProperties.class);

                if (f.getAnnotation(IdFieldAnnotation.class) != null) {
                    primary.add(fma.value());
                }

                if (!firstField) {
                    sql += ", ";
                } else {
                    firstField = false;
                }
                sql += "`" + fma.value() + "`";

                if (f.getType() == String.class) {
                    StringType sta = (StringType) f.getAnnotation(StringType.class);
                    if (sta != null) {
                        if (sta.value().equalsIgnoreCase("varchar")) {
                            int length = 0;
                            if ((cpa == null) || (cpa.length() == 0)) {
                                length = 50;
                            } else {
                                length = cpa.length();
                            }
                            sql += " varchar(" + length + ")";
                        } else if (sta.value().equalsIgnoreCase("text")) {
                            int length = 0;
                            if ((cpa == null) || (cpa.length() == 0)) {
                                length = 50;
                            } else {
                                length = cpa.length();
                            }
                            sql += sta.value() + " (" + length + ")";
                        } else {
                            sql += " " + sta.value() + " CHARACTER SET utf8";
                        }
                    } else {
                        sql += getTypeDef(f);
                    }
                } else {
                    sql += getTypeDef(f);
                }

                if (cpa != null) {
                    log.debug("Found ColumnProperties Annotation");

                    if (cpa.unsigned()) {
                        sql += " unsigned";
                    }
                    if (!cpa.nullable()) {
                        sql += " NOT NULL";
                    }
                    if (cpa.autoIncrement()) {
                        sql += " AUTO_INCREMENT";
                    }
                }

                DefaultValue dv = (DefaultValue) f.getAnnotation(DefaultValue.class);
                if (dv != null) {
                    if (f.getType() == Boolean.class) {
                        sql += " DEFAULT '" + (Boolean.getBoolean(dv.value()) ? 1 : 0) + "'";
                    } else {
                        sql += " DEFAULT '" + dv.value() + "'";
                    }
                }
            }
        }

        // Add primary
        if (!primary.isEmpty()) {
            boolean firstPrimary = true;
            sql += ", PRIMARY KEY (";
            for (String prim : primary) {
                if (!firstPrimary) {
                    sql += ",";
                } else {
                    firstPrimary = false;
                }

                sql += "`" + prim + "`";
            }
            sql += ") ";
        }

        sql += ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        return sql;
    }

    public static String createInsertStatementsFromDBForDAOClass(Class c) {
        StringBuffer sb = new StringBuffer();

        System.out.println("Class name: " + c.getName());



        ReadOnlyTable rot = null;
        boolean error = false;
        try {
            rot = DAOFactory.get(c);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        Class dataClass = null;
        try {

            dataClass = (Class) ((ParameterizedType) rot.getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        } catch (Exception e) {
            return null;
        }
        if (dataClass == null) {
            return null;
        }

        TableNameAnnotation ta = (TableNameAnnotation) dataClass.getAnnotation(TableNameAnnotation.class);
        if (ta == null) {
            return null;
        }

        DataScope ds = (DataScope) dataClass.getAnnotation(DataScope.class);
        if (ds == null || ds.type().equals(EScopeType.USER_GENERATED)) {
            return null;
        }
        System.out.println("dataClass " + dataClass);
        System.out.println("Datascope: " + ds.type());
        //Default handling of constants
        if (ds.type().equals(EScopeType.CONSTANTS)) {
            for (Model m : (ArrayList<Model>) rot.findAll()) {

                sb.append(generateInsertForModel(m, ta.value()));


            }
            //do sum speshial handling
        } else if (ds.type().equals(EScopeType.MIXED)) {
            sb.append(generateMixedInsertsFor(dataClass, ta.value()));
        }
        return sb.toString();
    }

    private static String generateInsertForModel(Model m, String tableName) {
        StringBuffer insert = new StringBuffer();

        Field[] fields = m.getClass().getDeclaredFields();
        //INSERT INTO `allianceboardpermission` VALUES ('116', 'USER', '8', '1', '0', '0');

        insert.append("INSERT INTO " + tableName + " VALUES (");
        boolean first = true;
        for (Field f : fields) {
            f.setAccessible(true);
            boolean nullValue = false;
            FieldMappingAnnotation fma = (FieldMappingAnnotation) f.getAnnotation(FieldMappingAnnotation.class);
            if (fma != null) {
                try {
                    Object value;
                    if ((f.getType() == boolean.class) || (f.getType() == Boolean.class)) {
                        value = ((Boolean) f.get(m) ? (int) 1 : (int) 0);
                    } else {
                        value = f.get(m);
                    }
                    if (value != null) {
                        value = "'" + value.toString().replace("'", "\\'") + "'";
                    }
                    if (first) {
                        first = false;
                    } else {
                        insert.append(",");

                    }
                    insert.append(value);
                } catch (IllegalArgumentException ex) {
                    java.util.logging.Logger.getLogger(CreateFromModel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    java.util.logging.Logger.getLogger(CreateFromModel.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        insert.append(");");
        insert.append("<BR>");
        return insert.toString();
    }

    private static String generateMixedInsertsFor(Class c, String tableName) {
        StringBuffer sb = new StringBuffer();

        if (c.equals(RessourceCost.class)) {

            ArrayList<RessourceCost> rcs = Service.ressourceCostDAO.findByType(ERessourceCostType.CONSTRUCTION);
            rcs.addAll(Service.ressourceCostDAO.findByType(ERessourceCostType.GROUNDTROOPS));
            rcs.addAll(Service.ressourceCostDAO.findByType(ERessourceCostType.MODULE));
            for (Model m : rcs) {
                sb.append(generateInsertForModel(m, tableName));
            }
        } else if (c.equals(Filter.class)) {


            for (Filter f : (ArrayList<Filter>) Service.filterDAO.findAll()) {
                if (f.getUserId() == 0) {
                    for (FilterToValue ftv : Service.filterToValueDAO.findByFilterId(f.getId())) {
                        sb.append(generateInsertForModel(ftv, ((TableNameAnnotation) ftv.getClass().getAnnotation(TableNameAnnotation.class)).value()));
                    }
                    sb.append(generateInsertForModel(f, tableName));

                }
            }
        }
        return sb.toString();
    }

    private static String getTypeDef(Field f) {
        String typeDef = "";

        Class c = f.getType();

        if ((c == int.class) || (c == Integer.class)) {
            typeDef = " int(11)";
        } else if ((c == double.class) || (c == Double.class)) {
            typeDef = " double";
        } else if ((c == long.class) || (c == Long.class)) {
            typeDef = " bigint(20)";
        } else if ((c == float.class) || (c == Float.class)) {
            typeDef = " float";
        } else if ((c == boolean.class) || (c == Boolean.class)) {
            typeDef = " tinyint(3)";
        } else if (c == char.class) {
            typeDef = " varchar(1)";
        } else if (c == String.class) {
            typeDef = " varchar(50)";
        } else if (c.isEnum()) {
            Class fEnumType = f.getType();
            ArrayList<String> eValues = new ArrayList<String>();  // enum constants

            Enum[] eConstants = (Enum[]) fEnumType.getEnumConstants();
            for (Enum e : eConstants) {
                eValues.add(e.toString());
            }

            typeDef = " enum(";
            boolean firstenum = true;

            for (String name : eValues) {
                if (!firstenum) {
                    typeDef += ",";
                } else {
                    firstenum = false;
                }

                typeDef += "'" + name + "'";
            }

            typeDef += ") COLLATE utf8_general_ci";
        }

        return typeDef;
    }
}
