/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.PriceListEntry;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class PriceListEntryDAO extends ReadWriteTable<PriceListEntry> implements GenericDAO {
    public PriceListEntry getById(int id) {
        PriceListEntry ple = new PriceListEntry();
        ple.setId(id);
        return (PriceListEntry)get(ple);
    }

    public ArrayList<PriceListEntry> findByPriceList(int id) {
        PriceListEntry ple = new PriceListEntry();
        ple.setPriceListId(id);
        return find(ple);
    }

    public PriceListEntry findByPriceListAndRess(int id, int ressId) {
        PriceListEntry ple = new PriceListEntry();
        ple.setPriceListId(id);
        ple.setRessId(ressId);

        ArrayList<PriceListEntry> result = find(ple);
        if (!result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
