/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dominationmap;

 import at.darkdestiny.core.scanner.StarMapQuadTree;import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.scanner.StarMapQuadTree;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class DominationMap {

    private static final Logger log = LoggerFactory.getLogger(DominationMap.class);

    public static void createDominationMap() {
        int width = 2000;
        int height = 2000;
        int distance = 200;

        ArrayList<FactionPoint> testData = new ArrayList<FactionPoint>();

        // Generate Test Data Seed
        for (int i=0;i<100;i++) {
            int x = (int)Math.floor(Math.random() * width);
            int y = (int)Math.floor(Math.random() * width);
            int faction = (int)Math.ceil(Math.random() * 10d);

            testData.add(new FactionPoint(faction,x,y));
        }

        // Fill Quadtree
        StarMapQuadTree<FactionPoint> qt = new StarMapQuadTree<FactionPoint>(width,height,4);
        for (FactionPoint fp : testData) {
            qt.addItemToTree(fp);
        }

        // Find intersecting influence spheres of different users
        for (FactionPoint fp : testData) {
            ArrayList<FactionPoint> fpList = qt.getItemsAround(fp.x, fp.y, distance * 2);

            for (FactionPoint fp2 : fpList) {
                if (fp2.getFaction() != fp.getFaction()) {
                    log.debug("Found influence between user " + fp.getFaction() + " and user " + fp2.getFaction());
                }
            }
        }
    }
}
