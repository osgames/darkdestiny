/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.enumeration.EStarMapObjectType;

/**
 *
 * @author Stefan
 */
public class JMTile extends StarMapObject {
    private int intensity;
    private int maxSysCount;
    private int maxSysCountPerTile;
    private int colSysCount;
    private int startSysCount;
    private int popScore;
    private int sysCount;
    private int systems;
    private int userScore;
    private final int xStart;
    private final int yStart;
    private final int extent;
    private String identifier;

    public JMTile(int x, int y, int extent) {
        super(EStarMapObjectType.JOINTILE,x,y);
        this.xStart = x;
        this.yStart = y;
        this.extent = extent;
    }
    
    /**
     * @return the intensity
     */
    public int getIntensity() {
        return intensity;
    }

    /**
     * @param intensity the intensity to set
     */
    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    /**
     * @return the maxSysCount
     */
    public int getMaxSysCount() {
        return maxSysCount;
    }

    /**
     * @param maxSysCount the maxSysCount to set
     */
    public void setMaxSysCount(int maxSysCount) {
        this.maxSysCount = maxSysCount;
    }

    /**
     * @return the maxSysCountPerTile
     */
    public int getMaxSysCountPerTile() {
        return maxSysCountPerTile;
    }

    /**
     * @param maxSysCountPerTile the maxSysCountPerTile to set
     */
    public void setMaxSysCountPerTile(int maxSysCountPerTile) {
        this.maxSysCountPerTile = maxSysCountPerTile;
    }

    /**
     * @return the popScore
     */
    public int getPopScore() {
        return popScore;
    }

    /**
     * @param popScore the popScore to set
     */
    public void setPopScore(int popScore) {
        this.popScore = popScore;
    }

    /**
     * @return the systems
     */
    public int getSystems() {
        return systems;
    }

    /**
     * @param systems the systems to set
     */
    public void setSystems(int systems) {
        this.systems = systems;
    }

    /**
     * @return the userScore
     */
    public int getUserScore() {
        return userScore;
    }

    /**
     * @param userScore the userScore to set
     */
    public void setUserScore(int userScore) {
        this.userScore = userScore;
    }

    /**
     * @return the xStart
     */
    public int getxStart() {
        return xStart;
    }

    /**
     * @return the yStart
     */
    public int getyStart() {
        return yStart;
    }

    /**
     * @return the sysCount
     */
    public int getSysCount() {
        return sysCount;
    }

    /**
     * @param sysCount the sysCount to set
     */
    public void setSysCount(int sysCount) {
        this.sysCount = sysCount;
    }

    /**
     * @return the colSysCount
     */
    public int getColSysCount() {
        return colSysCount;
    }

    /**
     * @param colSysCount the colSysCount to set
     */
    public void setColSysCount(int colSysCount) {
        this.colSysCount = colSysCount;
    }

    /**
     * @return the startSysCount
     */
    public int getStartSysCount() {
        return startSysCount;
    }

    /**
     * @param startSysCount the startSysCount to set
     */
    public void setStartSysCount(int startSysCount) {
        this.startSysCount = startSysCount;
    }

    /**
     * @return the extent
     */
    public int getExtent() {
        return extent;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
