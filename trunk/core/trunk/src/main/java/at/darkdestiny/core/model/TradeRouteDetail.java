/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.ENoTransportReason;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "traderoutedetail")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TradeRouteDetail extends Model<TradeRouteDetail> implements Comparable {

    @FieldMappingAnnotation(value = "id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation(value = "routeId")
    @IndexAnnotation(indexName = "activeRoute")
    private Integer routeId;
    @FieldMappingAnnotation(value = "direction")
    @DefaultValue("0")
    private Boolean reversed;
    @FieldMappingAnnotation(value = "ressId")
    private Integer ressId;
    @FieldMappingAnnotation(value = "maxQty")
    @DefaultValue("0")
    private Integer maxQty;
    @FieldMappingAnnotation(value = "maxQtyPerTick")
    @DefaultValue("0")
    private Integer maxQtyPerTick;
    @FieldMappingAnnotation(value = "maxDuration")
    @DefaultValue("0")
    private Integer maxDuration;
    @FieldMappingAnnotation(value = "qtyDelivered")
    @DefaultValue("0")
    private Long qtyDelivered;
    @FieldMappingAnnotation(value = "lastTransport")
    @DefaultValue("0")
    private Integer lastTransport;
    @FieldMappingAnnotation(value = "timeStarted")
    @DefaultValue("0")
    private Integer timeStarted;
    @FieldMappingAnnotation(value = "disabled")
    @IndexAnnotation(indexName = "activeRoute")
    @DefaultValue("0")
    private Boolean disabled;
    @FieldMappingAnnotation(value = "capReason")
    @DefaultValue("NONE")
    private ENoTransportReason capReason;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Boolean getReversed() {
        return reversed;
    }

    public void setReversed(Boolean reversed) {
        this.reversed = reversed;
    }

    public Integer getRessId() {
        return ressId;
    }

    public void setRessId(Integer ressId) {
        this.ressId = ressId;
    }

    public Integer getMaxQty() {
        return maxQty;
    }

    public void setMaxQty(Integer maxQty) {
        this.maxQty = maxQty;
    }

    public Integer getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(Integer maxDuration) {
        this.maxDuration = maxDuration;
    }

    public Long getQtyDelivered() {
        return qtyDelivered;
    }

    public void setQtyDelivered(Long qtyDelivered) {
        this.qtyDelivered = qtyDelivered;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Integer getMaxQtyPerTick() {
        return maxQtyPerTick;
    }

    public void setMaxQtyPerTick(Integer maxQtyPerTick) {
        this.maxQtyPerTick = maxQtyPerTick;
    }

    @Override
    public int compareTo(Object o) {
        TradeRouteDetail tr = (TradeRouteDetail) o;
        return this.getRessId().compareTo(tr.getRessId());
    }

    public Integer getTimeStarted() {
        return timeStarted;
    }

    public void setTimeStarted(Integer timeStarted) {
        this.timeStarted = timeStarted;
    }

    public int getRemainingTime(int currTime) {
        if (getMaxDuration() > 0) {
            return (getMaxDuration() - (currTime - getTimeStarted()));
        }

        return 0;
    }

    public Integer getLastTransport() {
        return lastTransport;
    }

    public void setLastTransport(Integer lastTransport) {
        this.lastTransport = lastTransport;
    }

    /**
     * @return the capReason
     */
    public ENoTransportReason getCapReason() {
        return capReason;
    }

    /**
     * @param capReason the capReason to set
     */
    public void setCapReason(ENoTransportReason capReason) {
        this.capReason = capReason;
    }
}
