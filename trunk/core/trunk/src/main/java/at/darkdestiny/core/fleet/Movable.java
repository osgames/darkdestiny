/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.fleet;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;

/**
 *
 * @author Stefan
 */
public interface Movable {
    public AbsoluteCoordinate getAbsoluteCoordinate();
    public RelativeCoordinate getRelativeCoordinate();
    public double getSpeed();
    public boolean isMoving();
    public boolean isScanning();
    public int getScanDuration();
    public boolean canScanSystem();
    public boolean canScanPlanet();
    public boolean canHyperJump();
    public boolean canFlyInterstellar();
    public int getETA();
    public Integer getRange();
    public boolean isAbleToCallBack();
    public AbsoluteCoordinate getTargetAbsoluteCoordinate();
    public RelativeCoordinate getTargetRelativeCoordinate();
    public AbsoluteCoordinate getSourceAbsoluteCoordinate();
    public RelativeCoordinate getSourceRelativeCoordinate();
}
