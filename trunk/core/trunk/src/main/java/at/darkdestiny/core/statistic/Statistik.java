package at.darkdestiny.core.statistic;

 import at.darkdestiny.core.ML;import java.sql.*;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.dao.ResearchDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.enumeration.EFleetViewType;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.fleet.LoadingInformation;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.HangarRelation;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.PlayerStatistic;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.result.FleetListResult;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.ships.ShipStorage;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.dao.ResearchDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.enumeration.EFleetViewType;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.fleet.LoadingInformation;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.HangarRelation;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.PlayerStatistic;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.result.FleetListResult;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.ships.ShipStorage;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Statistik {

    private static final Logger log = LoggerFactory.getLogger(Statistik.class);

    PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    public static final int WRITE_TO_DB_YES = 1;
    public static final int WRITE_TO_DB_NO = 0;
    private int popPoints = 0;
    private int resPoints = 0;
    private int prodPoints = 0;
    private int fleetPoints = 0;
    private int userId = 0;
    private PlayerStatistic ps;
    boolean debug = false;

    public Statistik() {
    }

    public PlayerStatistic calcStatistic(int userId, int writeToDB) {


        this.userId = userId;

        long totalIronProd = 0;
        long totalSteelProd = 0;
        long totalTerkonitProd = 0;
        long totalYnkeloniumProd = 0;
        long totalHowalgoniumProd = 0;
        long totalCVEmbiniumProd = 0;

        HashMap<Integer, Integer> battleShipCount = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> utilityShipCount = new HashMap<Integer, Integer>();

        TreeMap<Integer, Long> groundTroops = new TreeMap<Integer, Long>();

        int totalRessourcePoints = 0;
        int totalPopulationPoints = 0;
        int totalShipPoints = 0;
        int totalResearchPoints = 0;
        int planetCount = 0;

        //GroundTroops
        double gtPoints = 0;

        //Groundtroops Multiplier
        HashMap<Integer, Double> gtValues = new HashMap<Integer, Double>();
        gtValues.put(GroundTroop.ID_MILIZ, 0.000002d);
        gtValues.put(GroundTroop.ID_LIGHTINFANTRY, 0.000004d);
        gtValues.put(GroundTroop.ID_HEAVYINFANTRY, 0.00001d);
        gtValues.put(GroundTroop.ID_ROBOTER, 0.00002d);
        gtValues.put(GroundTroop.ID_LIGHTTANK, 0.0002d);
        gtValues.put(GroundTroop.ID_ROCKETTANK, 0.0004d);
        gtValues.put(GroundTroop.ID_HEAVYTANK, 0.0006d);
        gtValues.put(GroundTroop.ID_ANTIGRAVTANK, 0.0008d);
        gtValues.put(GroundTroop.ID_BOMBER, 0.0006d);
        gtValues.put(GroundTroop.ID_INTERCEPTORPLANE, 0.0001d);

        try {
            PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
            ArrayList<PlayerPlanet> ppList = ppDAO.findByUserId(userId);

            for (PlayerPlanet pp : ppList) {
                PlanetCalculation pc = new PlanetCalculation(pp.getPlanetId());
                ProductionResult pr = pc.getPlanetCalcData().getProductionData();
                // Calculate total Production of every Ressource

                totalSteelProd += pr.getRessBaseProduction(Ressource.STEEL);
                totalTerkonitProd += pr.getRessBaseProduction(Ressource.TERKONIT);
                totalYnkeloniumProd += pr.getRessBaseProduction(Ressource.YNKELONIUM);
                totalHowalgoniumProd += pr.getRessBaseProduction(Ressource.HOWALGONIUM);

                totalPopulationPoints += getPopulationScore(pp.getPopulation());

                //GroundTroops
                for (PlayerTroop pt : ptDAO.findBy(userId, pp.getPlanetId())) {
                    if (groundTroops.get(pt.getTroopId()) == null) {
                        groundTroops.put(pt.getTroopId(), pt.getNumber());
                    } else {
                        groundTroops.put(pt.getTroopId(), groundTroops.get(pt.getTroopId()) + pt.getNumber());
                    }
                }

                planetCount++;

            }
            if (planetCount == 0) {
                return null;
            }
            // Sum up all Ship Designs
            FleetListResult flr = FleetService.getFleetList(userId, 0, 0, EFleetViewType.OWN_ALL);
            FleetListResult flr2 = FleetService.getFleetList(userId, 0, 0, EFleetViewType.OWN_ALL_WITH_ACTION);

            ArrayList<PlayerFleetExt> pfes = flr.getAllContainedFleets();
            pfes.addAll(flr2.getAllContainedFleets());

            ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
            for (PlayerFleetExt pfe : pfes) {
                // Do not take other player fleets in fleet formations
                if (pfe.getUserId() != userId) {
                    continue;
                }

                if (debug) {
                    log.debug("Processing Fleet: " + pfe.getName() + " id : " + pfe.getId());
                }
                //Add loaded Groundtroops
                LoadingInformation li = pfe.getLoadingInformation();
                for (ShipStorage ss : li.getLoadedStorage()) {
                    if (ss.getLoadtype() == ShipStorage.LOADTYPE_GROUNDTROOP) {

                        gtPoints += ss.getCount() * gtValues.get(ss.getId());
                    }
                }

                //Check for Ships in Hangar
                for (HangarRelation hr : Service.hangarRelationDAO.findByFleetId(pfe.getId())) {
                    int designId = hr.getDesignId();
                    ShipDesign sd = Service.shipDesignDAO.findById(designId);
                    if (sd == null) {
                        DebugBuffer.error("Fleet " + pfe.getName() + " ["+pfe.getId()+"] contains ship id " + designId + " in hangar which does not exist!");
                        continue;
                    }
                    addShipDesign(sd, hr.getCount(), battleShipCount, utilityShipCount);
                }


                for (ShipData shipData : pfe.getShipList()) {
                    ShipDesign sd = sdDAO.findById(shipData.getDesignId());
                    if (sd == null) {
                        DebugBuffer.error("Fleet " + pfe.getName() + " ["+pfe.getId()+"] contains ship id " + shipData.getDesignId() + " which does not exist!");
                        continue;
                    }
                    addShipDesign(sd, shipData.getCount(), battleShipCount, utilityShipCount);
                }


            }

            // Calculate Research Points
            int accRP = 0;
            ResearchDAO rDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
            PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);

            ArrayList<PlayerResearch> prList = prDAO.findByUserId(userId);
            for (PlayerResearch pr : prList) {
                Research r = rDAO.findById(pr.getResearchId());
                if (r != null) {
                    accRP += r.getRp() + (r.getRpComputer() * 2);
                }
            }

            totalResearchPoints = (int) (accRP / 600);

            // Build Points for Ressource Production
            totalRessourcePoints += (int) (totalIronProd / 100);
            totalRessourcePoints += (int) (totalSteelProd / 50);
            totalRessourcePoints += (int) (totalTerkonitProd / 10);
            totalRessourcePoints += (int) (totalYnkeloniumProd * 2);
            totalRessourcePoints += (int) (totalHowalgoniumProd * 5);
            totalRessourcePoints += (int) (totalCVEmbiniumProd * 20);

            // Build Points for Population
            // totalPopulationPoints = (int)(totalPopulation / 1000000);

            // Build Points for ships
            double[] battleShipPoints = new double[]{0.01d, 0.1d, 2.5d, 15d, 30d, 150d, 180d, 750d, 800d, 3000d};
            double[] utilityShipPoints = new double[]{0.002d, 0.02d, 0.5d, 3d, 6d, 30d, 36d, 150d, 160d, 600d};


            for (Integer i : battleShipCount.keySet()) {
                if (debug) {
                    log.debug("battle, i: " + i + " count : " + battleShipCount.get(i) + " multiplier : " + battleShipPoints[i - 1]);
                    log.debug("Adding :" + (battleShipCount.get(i) * battleShipPoints[i - 1]));
                }
                totalShipPoints += battleShipCount.get(i) * battleShipPoints[i - 1];
                if (debug) {
                    log.debug("TotalShipPoints Adding BattleShips : " + totalShipPoints);
                }
            }

            for (Integer i : utilityShipCount.keySet()) {

                if (debug) {
                    log.debug("utilitiy, i: " + i + " count : " + utilityShipCount.get(i) + " multiplier : " + utilityShipPoints[i - 1]);
                    log.debug("Adding :" + (utilityShipCount.get(i) * utilityShipPoints[i - 1]));
                }
                totalShipPoints += utilityShipCount.get(i) * utilityShipPoints[i - 1];
                if (debug) {
                    log.debug("TotalShipPoints Adding utilitiy Ships: " + totalShipPoints);
                }
            }


            for (Map.Entry<Integer, Long> entry : groundTroops.entrySet()) {
                gtPoints += entry.getValue() * gtValues.get(entry.getKey());
                if (debug) {
                    log.debug("Gt Points after adding : " + ML.getMLStr(Service.groundTroopDAO.findById(entry.getKey()).getName(), userId) + " = " + gtPoints);
                }
            }
            totalShipPoints += gtPoints;

            ps = new PlayerStatistic();
            ps.setRessourcePoints((long) totalRessourcePoints);
            ps.setPopulationPoints((long) totalPopulationPoints);
            ps.setMilitaryPoints((long) totalShipPoints);
            ps.setResearchPoints((long) totalResearchPoints);
            ps.setUserId(userId);


            // log.debug("TotP=" + totalPoints + " TotRessP="+totalRessourcePoints+" TotPopP="+totalPopulationPoints+" TotShipP="+totalShipPoints+" totResearchP="+totalResearchPoints);
            if (writeToDB == WRITE_TO_DB_YES) {
                writePointsToDatabase();
            } else {
                //Do nothing
            }

            return ps;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Statistics: ", e);
            DebugBuffer.produceStackTrace(e);
        }
        return null;
    }

    private void addShipDesign(ShipDesign sd, int count, HashMap<Integer, Integer> battleShipCount, HashMap<Integer, Integer> utilityShipCount) {
        if (debug) {
            log.debug("Found for Chassis: " + sd.getChassis() + " count : " + count);
        }

        if ((sd.getType() == EShipType.BATTLE) || (sd.getType() == EShipType.SPACEBASE)) {
            if (battleShipCount.containsKey(sd.getChassis())) {
                if (debug) {
                    log.debug("Putting Battleship : " + sd.getChassis() + " : " + battleShipCount.get(sd.getChassis()) + " + " + count);
                }
                battleShipCount.put(sd.getChassis(), battleShipCount.get(sd.getChassis()) + count);

            } else {
                battleShipCount.put(sd.getChassis(), count);
                if (debug) {
                    log.debug("Putting Battleship : " + sd.getChassis() + " : " + count);
                }
            }
        }
        if ((sd.getType() == EShipType.TRANSPORT) || (sd.getType() == EShipType.COLONYSHIP) || (sd.getType() == EShipType.CARRIER)) {
            if (utilityShipCount.containsKey(sd.getChassis())) {
                if (debug) {
                    log.debug("Putting UtilityShip : " + sd.getChassis() + " : " + utilityShipCount.get(sd.getChassis()) + " + " + count);
                }
                utilityShipCount.put(sd.getChassis(), utilityShipCount.get(sd.getChassis()) + count);
            } else {
                utilityShipCount.put(sd.getChassis(), count);
                if (debug) {
                    log.debug("Putting UtilityShip : " + sd.getChassis() + " : " + count);
                }
            }
        }
    }

    private int getPopulationScore(long temp) {

        if (temp <= 0) {
            return 0;
        }

        /* Colonies smaller 100k - 5 Points of Score */
        if (temp < 100000) {
            return 5;
        }

        /* Colonies between 100k and 1 Million - 20 Points Score*/
        if (temp < 1000000) {
            return 20;
        }

        /* Colonies between 1 Million and 100 Millions - 100 Points Score */
        if (temp < 100000000) {
            return 200;
        }

        /* Colonies between 100 Millions and 1 Billion - 700 Points Score */
        if (temp < 1000000000) {
            return 700;
        }

        /*Colonies above 1 Billion get 800 Points Score + 750 Points per additional Billion */
        return 50 + (int) (temp / 1000000000L) * 750;
    }

    public int getPopPoints() {
        return popPoints;
    }

    public int getResPoints() {
        return resPoints;
    }

    public int getProdPoints() {
        return prodPoints;
    }

    public int getFleetPoints() {
        return fleetPoints;
    }

    public void writePointsToDatabase() {
        long date = System.currentTimeMillis();
        try {
            Statement stmt = DbConnect.createStatement();
            stmt.execute("INSERT INTO playerstatistics (userid,date,researchpoints,ressourcepoints,populationpoints,militarypoints) VALUES('" + userId + "'," + date + "," + resPoints + "," + prodPoints + "," + popPoints + "," + fleetPoints + ")");

        } catch (Exception e) {
            DebugBuffer.error("Fehler Statistik: " + e);
        }

    }

    /**
     * @return the ps
     */
    public PlayerStatistic getPs() {
        return ps;
    }
}
