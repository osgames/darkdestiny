/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.utilities.ResearchUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.HashMap;

/**
 *
 * @author Aion
 */
public abstract class AbstractDefenseBuilding extends AbstractDefense implements IDefenseBuilding {
    private PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    
    public AbstractDefenseBuilding(int designId, int count) {
        super(designId, count);
    }
    
    // Load modules into module Array <ModuleId, Count>
    protected void loadModulesDynamically(int constructionId, int userId, HashMap<Integer, Integer> modules) {
        if (constructionId == Construction.ID_PLANETARY_FORTRESS) {
            DebugBuffer.trace("Process building " + Construction.ID_PLANETARY_FORTRESS);
            
            boolean hasLaser = ResearchUtilities.isModuleResearched(900, userId); // Laser
            boolean hasRockets = ResearchUtilities.isModuleResearched(901, userId); // Missiles
            boolean hasIntervall = ResearchUtilities.isModuleResearched(902, userId); // Intervallcannon
            
            boolean hasTerkonit = ResearchUtilities.isModuleResearched(21, userId); // Terkonit
            boolean hasYnkelonium = ResearchUtilities.isModuleResearched(22, userId); // Ynkelonium
            
            boolean hasPS = ResearchUtilities.isModuleResearched(50, userId); // Prallschirm
            boolean hasHU = ResearchUtilities.isModuleResearched(51, userId); // HU-Schirm            
            boolean hasPA = ResearchUtilities.isModuleResearched(52, userId); // Paratron                         
            
            DebugBuffer.trace("User: " + userId + " // TechlevelData: " + hasLaser + "|" + hasRockets + "|" + hasIntervall + "|" + hasTerkonit + "|" + hasYnkelonium + "|" + hasPS + "|" + hasHU + "|" + hasPA);
            
            boolean installedEnergyWeapon = false;
            boolean installedMissileWeapon = false;
            boolean installedArmor = false;            
            
            if (!installedEnergyWeapon && hasIntervall) {
                modules.put(902,5);
                installedEnergyWeapon = true;
            }
            
            if (!installedEnergyWeapon && hasLaser) {
                modules.put(900,5);
                installedEnergyWeapon = true;
            }          
            
            if (!installedMissileWeapon && hasRockets) {
                modules.put(901,5);
                installedEnergyWeapon = true;
            }                   
            
            if (!installedArmor && hasYnkelonium) {
                modules.put(22,1);
                installedArmor = true;
            }
            
            if (!installedArmor && hasTerkonit) {
                modules.put(21,1);
                installedArmor = true;
            }            
            
            if (hasPS) {
                modules.put(50,1);
            }
            
            if (hasHU) {
                modules.put(51,1);
            }
            
            if (hasPA) {
                modules.put(52,1);
            }
        } else {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Could not find appropriate building description");
        }
    }
}
