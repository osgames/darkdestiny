/*
 * TradeRouteInfo.java
 *
 * Created on 24. Dezember 2007, 18:34
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.darkdestiny.core.trade;

/**
 *
 * @author Stefan
 */
public class TradeRouteInfo {
    private int id;
    private int userId;
    private int ressId;
    private int startPlanet;
    private int targetPlanet;
    private int capacity;
    private int targetUserId;
    private boolean externalRoute = false;
    private int offerId;
    private int maxDuration;
    private int maxQty;
    private int startTime;   
    private boolean transmitterPossible = false;
    private int transmitterUsage;
    private double efficiency;
    
    // Variables for extended information if it is a external route
    private long pricePerUnit;
    
    /** Creates a new instance of TradeRouteInfo */
    public TradeRouteInfo() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRessId() {
        return ressId;
    }

    public void setRessId(int ressId) {
        this.ressId = ressId;
    }

    public int getStartPlanet() {
        return startPlanet;
    }

    public void setStartPlanet(int startPlanet) {
        this.startPlanet = startPlanet;
    }

    public int getTargetPlanet() {
        return targetPlanet;
    }

    public void setTargetPlanet(int targetPlanet) {
        this.targetPlanet = targetPlanet;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }    

    public boolean isExternalRoute() {
        return externalRoute;
    }

    public void setExternalRoute(boolean externalRoute) {
        this.externalRoute = externalRoute;
    }

    public int getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(int targetUserId) {
        this.targetUserId = targetUserId;
    }

    public long getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(long pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public int getMaxQty() {
        return maxQty;
    }

    public void setMaxQty(int maxQty) {
        this.maxQty = maxQty;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(double efficiency) {
        this.efficiency = efficiency;
    }

    public int getTransmitterUsage() {
        return transmitterUsage;
    }

    public void setTransmitterUsage(int transmitterUsage) {
        this.transmitterUsage = transmitterUsage;
    }

    public boolean isTransmitterPossible() {
        return transmitterPossible;
    }

    public void setTransmitterPossible(boolean transmitterPossible) {
        this.transmitterPossible = transmitterPossible;
    }
}
