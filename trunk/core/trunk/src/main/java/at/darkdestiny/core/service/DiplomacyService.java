/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.model.*;
import at.darkdestiny.core.voting.*;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.enumeration.EDiplomacyRelationType;
import at.darkdestiny.core.enumeration.EDiplomacyTypeRelType;
import at.darkdestiny.core.enumeration.ELeadership;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EMessageRefType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.enumeration.ESharingType;
import at.darkdestiny.core.enumeration.ESorting;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.notification.EmbassyNotification;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.DiplomacyVoteLinkResult;
import at.darkdestiny.core.result.RelationResult;
import at.darkdestiny.core.utilities.AllianceUtilities;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import at.darkdestiny.core.utilities.ViewTableUtilities;
import at.darkdestiny.core.utilities.ViewTableUtilities.Direction;
import at.darkdestiny.core.view.DiplomacyManagementView;
import at.darkdestiny.core.view.DiplomacyManagementViewEntry;
import at.darkdestiny.framework.exception.TransactionException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class DiplomacyService extends Service {

    private static final Logger log = LoggerFactory.getLogger(DiplomacyService.class);

    private static RelationResult relationResult;

    public static ArrayList<String> createUserRelationVote(int userId, int toUser, int diplomacyType, String message) {
        //Searching for an existing user relation
        DiplomacyType dt = diplomacyTypeDAO.findById(diplomacyType);
        ArrayList<DiplomacyRelation> drs = diplomacyRelationDAO.findBy(userId, toUser, EDiplomacyRelationType.USER_TO_USER);
        DiplomacyRelation dr = null;
        for (DiplomacyRelation drtmp : drs) {
            if (drtmp.getPending()) {
                return null;
            } else if (!drtmp.getPending()) {
                dr = drtmp;
            }
        }
        boolean vote = false;
        int weight = 0;
        String actRel = ML.getMLStr("db_diplomacy_neutral", userId);
        if (dr == null) {
        } else {
            DiplomacyType existingdt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            actRel = ML.getMLStr(existingdt.getName(), userId);
            weight = existingdt.getWeight();
        }

        if (weight < dt.getWeight()) {
            vote = true;
        }
        User u = userDAO.findById(userId);
        ArrayList<String> result = new ArrayList<String>();

        if (dt.isVoteRequired() && vote) {
            try {
                Vote v = new Vote(userId, u.getGameName() + " : " + actRel + " => " + ML.getMLStr(dt.getName(), userId), u.getGameName() + " " + ML.getMLStr("diplomacy_msg_changerequest1", userId) + " <b> " + actRel + " </b> " + ML.getMLStr("diplomacy_msg_changerequest2", userId) + ": <b>" + ML.getMLStr(dt.getName(), userId) + "</b> " + ML.getMLStr("diplomacy_msg_changerequest3", userId) + " <BR>============= <BR>" + message, Voting.TYPE_USERRELATION);
                v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));
                v.setMinVotes(1);
                v.setTotalVotes(1);

                v.addOption(new VoteOption(ML.getMLStr("diplomacy_link_accept", userId), true, VoteOption.OPTION_TYPE_ACCEPT));
                v.addOption(new VoteOption(ML.getMLStr("diplomacy_link_decline", userId), false, VoteOption.OPTION_TYPE_DENY));

                ArrayList<Integer> receivers = new ArrayList<Integer>();

                receivers.add(toUser);

                v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, toUser));

                v.addMetaData(VoteMetadata.NAME_FROMID, u.getUserId(), MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TOID, toUser, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TYPE, diplomacyType, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_MESSAGE, message, MetaDataDataType.STRING);

                v.setReceivers(receivers);

                VotingFactory.persistVote(v);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while creating vote: ", e);
            }

            createUserRelation(userId, toUser, diplomacyType, true);

            result.add("global_done");
        } else {
            createUserRelation(userId, toUser, diplomacyType, false);
            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(userId);
            gm.setTopic(u.getGameName() + " : " + actRel + " => " + ML.getMLStr(dt.getName(), userId));
            gm.setMsg(u.getGameName() + " " + ML.getMLStr("diplomacy_msg_u2udemote1", userId) + " <b> " + actRel + " </b> " + ML.getMLStr("db_diplomacy_u2udemote2", userId) + ": <b>" + ML.getMLStr(dt.getName(), userId) + "</b> " + ML.getMLStr("db_diplomacy_u2udemote3", userId) + " <BR>============= <BR>" + message);
            gm.setMessageType(EMessageType.USER);
            gm.setMessageContentType(EMessageContentType.DIPLOMACY);
            gm.setDestinationUserId(toUser);
            gm.setMasterEntry(true);
            gm.setEscape(false);
            gm.writeMessageToUser();
        }
        return result;

    }

    public static int findCorrespondingVote(DiplomacyRelation dr, int userId) {
        userId = 0;

        if (!dr.getPending()) {
            return -1;
        }
        boolean flip = true;
        for (Voting v : (ArrayList<Voting>) votingDAO.findAllOpenVotes()) {
            if (v.getClosed()) {
                continue;
            }

            // log.debug("Looping Vote " + v.getVoteId());

            Vote vote = VotingFactory.loadVote(v.getVoteId());

            try {
                Integer fromId;
                Integer toId;
                for (int i = 0; i <= 1; i++) {
                    if (i == 0) {
                        fromId = (Integer) vote.getMetaData(VoteMetadata.NAME_FROMID);
                        toId = (Integer) vote.getMetaData(VoteMetadata.NAME_TOID);
                    } else {
                        fromId = (Integer) vote.getMetaData(VoteMetadata.NAME_TOID);
                        toId = (Integer) vote.getMetaData(VoteMetadata.NAME_FROMID);
                    }

                    if (!dr.getFromId().equals(fromId) || !dr.getToId().equals(toId)) {
                        continue;
                    }

                    // java.lang.System.out.println("Found applyable vote " + v.getVoteId() + " VoteType="+v.getType()+" DiplomacyType: " + dr.getType());
                    // java.lang.System.out.println("fromId="+fromId+" toId="+toId);

                    Integer diplomacyTypeId = (Integer) vote.getMetaData(VoteMetadata.NAME_TYPE);

                    // java.lang.System.out.println("DiplomacyTypeId = " + diplomacyTypeId);
                    if (dr.getFromId().equals(fromId) && dr.getToId().equals(toId) && diplomacyTypeId.equals(dr.getDiplomacyTypeId())) {
                        if (v.getType() == Voting.TYPE_USERRELATION && dr.getType().equals(EDiplomacyRelationType.USER_TO_USER)) {
                            if (VoteUtilities.checkVotePermission(userId, v.getVoteId()) || userId == 0) {
                                return v.getVoteId();
                            }
                        } else if (v.getType() == Voting.TYPE_ALLIANCEUSERRELATION && dr.getType().equals(EDiplomacyRelationType.ALLIANCE_TO_USER)) {
                            if (VoteUtilities.checkVotePermission(userId, v.getVoteId()) || userId == 0) {
                                return v.getVoteId();
                            }
                        } else if (v.getType() == Voting.TYPE_USERALLIANCERELATION && dr.getType().equals(EDiplomacyRelationType.USER_TO_ALLIANCE)) {
                            if (VoteUtilities.checkVotePermission(userId, v.getVoteId()) || userId == 0) {
                                return v.getVoteId();
                            }
                        } else if (v.getType() == Voting.TYPE_ALLIANCERELATION && dr.getType().equals(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)) {
                            if (VoteUtilities.checkVotePermission(userId, v.getVoteId()) || userId == 0) {
                                return v.getVoteId();
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        }

        java.lang.System.out.println("GLOBAL FAIL return");
        return -1;
    }

    public static ArrayList<User> findAllUsers() {
        return userDAO.findAllPlayers();
    }

    public static ArrayList<User> findAllUsersSorted() {
        return userDAO.findAllSortedByName();
    }

    public static ArrayList<Integer> findUsersToDisplay(int userId) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        result.add(userId);
        for (UserData ud : (ArrayList<UserData>) userDataDAO.findAll()) {
            if (DiplomacyUtilities.hasAllianceRelation(userId, ud.getUserId())
                    || DiplomacyUtilities.hasRelation(userId, ud.getUserId(), DiplomacyType.TREATY)) {
                result.add(ud.getUserId());
            }

        }
        return result;
    }

    public static ArrayList<Integer> findAlliancesToDisplay(int userId) {
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        if (am == null) {
            return new ArrayList<Integer>();
        }
        ArrayList<Alliance> allys = allianceDAO.findByMasterId(allianceDAO.findById(am.getAllianceId()).getMasterAlliance());
        ArrayList<Integer> result = new ArrayList<Integer>();
        for (Alliance a : allys) {
            result.add(a.getId());
        }

        return result;
    }

    public static BaseResult updateRelations(int userId, Map<String, String[]> params) {
        // log.debug("Updating relations");

        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            if (new StringTokenizer(entry.getKey(), ";").countTokens() == 3) {
                // log.debug("Try to get Dr with user " + userId + " and entry " + entry.getKey());

                DiplomacyRelation dr = StringToRelation(userId, entry.getKey());

                if (dr != null) {
                    // log.debug("Found relation " + dr.getFromId() + " => " + dr.getToId() + " TYPE: " + dr.getType() + " DiplType: " + dr.getDiplomacyTypeId() + " Sharing? " + dr.getSharingMap());

                    if (!dr.getSharingMap()) {
                        // log.debug("Activate Sharing!");
                        dr.setSharingMap(!dr.getSharingMap());
                        diplomacyRelationDAO.update(dr);

                        DiplomacyRelation dr2 = StringToRelation(userId, entry.getKey());
                        // log.debug("Found relation " + dr2.getFromId() + " => " + dr2.getToId() + " TYPE: " + dr2.getType() + " DiplType: " + dr2.getDiplomacyTypeId() + " Sharing? " + dr.getSharingMap());

                        ViewTableUtilities.copyEntries(dr.getFromId(), dr.getToId(), Direction.valueOf(dr.getType().toString()), false);
                    } else {
                        // log.debug("Deactivate Sharing!");
                        dr.setSharingMap(!dr.getSharingMap());
                        diplomacyRelationDAO.update(dr);
                    }
                } else {
                    // log.debug("No DR FOUND");
                }
            }
        }



        return new BaseResult("Done", false);
    }

    public static ArrayList<DiplomacyRelation> findRelsToUpdate(int userId, Map<String, String[]> params) {
        ArrayList<DiplomacyRelation> result = new ArrayList<DiplomacyRelation>();

        for (Map.Entry<String, String[]> entry : params.entrySet()) {

            StringTokenizer token = new StringTokenizer(entry.getKey(), ";");
            if (token.countTokens() == 3) {
                DiplomacyRelation dr = StringToRelation(userId, entry.getKey());
                if (dr != null) {
                    result.add(dr);
                }
            }
        }
        return result;
    }

    private static DiplomacyRelation StringToRelation(int userId, String relString) {
        StringTokenizer token = new StringTokenizer(relString, ";");
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        String type = token.nextToken();
        EDiplomacyRelationType drt = null;
        int fromId = Integer.parseInt(token.nextToken());
        int toId = Integer.parseInt(token.nextToken());
        if (type.equals("uu")) {
            drt = EDiplomacyRelationType.USER_TO_USER;
            if (fromId != userId) {
                return null;
            }
        } else if (type.equals("ua")) {
            drt = EDiplomacyRelationType.USER_TO_ALLIANCE;
            if (fromId != userId) {
                return null;
            }
        } else if (type.equals("au")) {
            drt = EDiplomacyRelationType.ALLIANCE_TO_USER;
            if (am == null || !am.getIsAdmin()) {
                return null;
            }
        } else if (type.equals("aa")) {
            if (am == null || !am.getIsAdmin()) {
                return null;
            }
            drt = EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE;
        }

        DiplomacyRelation dr = diplomacyRelationDAO.findBy(fromId, toId, drt, false);
        return dr;

    }

    public static ArrayList<DiplomacyRelation> findChangeable(int userId, EDiplomacyRelationType drt) {
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        int fromId = userId;
        if (drt.equals(EDiplomacyRelationType.USER_TO_USER)
                || drt.equals(EDiplomacyRelationType.USER_TO_ALLIANCE)) {
        } else if (drt.equals(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)
                || drt.equals(EDiplomacyRelationType.ALLIANCE_TO_USER)) {
            if (drt.equals(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE) && (am == null || !am.getIsAdmin())) {
                return null;
            } else {
                fromId = am.getAllianceId();
            }
        }

        ArrayList<DiplomacyRelation> result = new ArrayList<DiplomacyRelation>();
        for (DiplomacyRelation dr : diplomacyRelationDAO.findByFromId(fromId, drt, false)) {
            if (dr.getDiplomacyTypeId() == DiplomacyType.TREATY && !dr.getPending()) {
                result.add(dr);
            }
        }
        return result;
    }

    public static BaseResult createAllianceRelationRequestVote(int userId, int fromAllianceId, int toId, EDiplomacyRelationType drt, int diplomacyType, String message) {
        BaseResult br = null;

        // Check if current user is allowed to start such a vote
        AllianceMember requester = allianceMemberDAO.findByUserId(userId);
        Alliance aRequester;

        if (requester != null) {
            aRequester = allianceDAO.findById(requester.getAllianceId());

            // In dictorial alliances only admin may request alliance relations
            if ((aRequester.getLeadership() == ELeadership.DICTORIAL) && !requester.getIsAdmin()) {
                return new BaseResult("No permission", true);
            }

            // Check correct alliance Ids
            if (aRequester.getId() != fromAllianceId) {
                return new BaseResult("No permission", true);
            }

            // Check if requester is at least Council
            if (!(requester.getIsAdmin() || requester.getIsCouncil())) {
                return new BaseResult("No permission", true);
            }
        } else {
            return new BaseResult("No permission", true);
        }

        // Security checks are done we can process to generating the vote
        // -> If dictorial create immediate vote in other alliance / player
        // -> Otherwise create vote in own alliance first
        DiplomacyType dt = diplomacyTypeDAO.findById(diplomacyType);
        boolean targetVoteRequired = dt.isVoteRequired();
        boolean sourceVoteRequired = !(aRequester.getLeadership() == ELeadership.DICTORIAL);

        // Check if there are more people who maybe allowed to vote
        boolean otherPeopleMayVote = false;

        if (sourceVoteRequired) {
            ArrayList<AllianceMember> aReqMembers = allianceMemberDAO.findByAllianceId(aRequester.getId());

            for (AllianceMember am : aReqMembers) {
                if (aRequester.getLeadership() == ELeadership.ARISTOCRATIC) {
                    if (am.getUserId() == userId) {
                        continue;
                    }
                    if (am.getIsAdmin() || am.getIsCouncil()) {
                        otherPeopleMayVote = true;
                    }
                } else if (aRequester.getLeadership() == ELeadership.DEMOCRATIC) {
                    if (am.getUserId() == userId) {
                        continue;
                    }
                    if (!am.getIsTrial()) {
                        otherPeopleMayVote = true;
                    }
                }
            }
        }

        sourceVoteRequired = sourceVoteRequired && otherPeopleMayVote;

        // Now we have determined if we need a vote inside our own ally first
        // YES: Create a vote inside our own Ally and store all important data if this vote passes
        if (sourceVoteRequired) {
            // diplomacy_msg_aarequest_send
            try {
                // Get current relation
                // TAKEN FROM OLD VOTING CREATION CODE
                ArrayList<DiplomacyRelation> drs = diplomacyRelationDAO.findBy(fromAllianceId, toId, drt);
                DiplomacyRelation dr = null;
                for (DiplomacyRelation drtmp : drs) {
                    if (drtmp.getPending()) {
                        return null;
                    } else if (!drtmp.getPending()) {
                        dr = drtmp;
                    }
                }

                int weight = 1;
                String actRel = ML.getMLStr("db_diplomacy_neutral", userId);
                if (dr == null) {
                } else {
                    DiplomacyType existingdt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                    actRel = ML.getMLStr(existingdt.getName(), userId);
                    weight = existingdt.getWeight();
                }
                // TAKEN FROM OLD VOTING CREATE CODE ---- END

                String targetName = "";
                if (drt == EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE) {
                    targetName = allianceDAO.findById(toId).getName();
                } else if (drt == EDiplomacyRelationType.ALLIANCE_TO_USER) {
                    targetName = IdToNameService.getUserName(toId);
                }

                HashMap<String, CharSequence> replacementsMsg = new HashMap<String, CharSequence>();
                HashMap<String, CharSequence> replacementsSubject = new HashMap<String, CharSequence>();

                if (drt == EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE) {
                    replacementsMsg.put("%USER%", IdToNameService.getUserName(userId));
                    replacementsMsg.put("%ALL%", targetName);
                    replacementsMsg.put("%ACTREL%", actRel);
                    replacementsMsg.put("%REL%", ML.getMLStr(dt.getName(), userId));
                    replacementsMsg.put("%MSG%", message);

                    replacementsSubject.put("%ALL%", targetName);
                } else {
                    replacementsMsg.put("%USER%", IdToNameService.getUserName(userId));
                    replacementsMsg.put("%TARGETUSER%", targetName);
                    replacementsMsg.put("%ACTREL%", actRel);
                    replacementsMsg.put("%REL%", ML.getMLStr(dt.getName(), userId));
                    replacementsMsg.put("%MSG%", message);

                    replacementsSubject.put("%TARGETUSER%", targetName);
                }

                VotingTextMLSecure votingText;
                Vote v;

                if (drt == EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE) {
                    votingText = new VotingTextMLSecure("diplomacy_msg_aarequest_send", "diplomacy_sbj_aarequest_send", replacementsMsg, replacementsSubject);
                    v = new Vote(userId, votingText, Voting.TYPE_ALLIANCERELATION_REQUEST);
                } else {
                    votingText = new VotingTextMLSecure("diplomacy_msg_aurequest_send", "diplomacy_sbj_aurequest_send", replacementsMsg, replacementsSubject);
                    v = new Vote(userId, votingText, Voting.TYPE_ALLIANCEUSERRELATION_REQUEST);
                }

                v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));

                v.addOption(new VoteOption("diplomacy_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
                v.addOption(new VoteOption("diplomacy_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

                ArrayList<Integer> receivers = new ArrayList<Integer>();

                // If democratic each member has same vote strength
                // If aristocratic admin has 2 .. council 1
                int totalVotes = 0;
                int minVotes = 0;

                for (AllianceMember am : allianceMemberDAO.findByAllianceId(aRequester.getId())) {
                    if (aRequester.getLeadership() == ELeadership.DEMOCRATIC) {
                        if (!am.getIsTrial()) {
                            receivers.add(am.getUserId());
                            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                            totalVotes++;
                        }
                    } else if (aRequester.getLeadership() == ELeadership.ARISTOCRATIC) {
                        if (am.getIsCouncil() || am.getIsAdmin()) {
                            receivers.add(am.getUserId());
                            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));

                            if (am.getIsAdmin()) {
                                totalVotes += 1;
                            }
                            if (am.getIsCouncil()) {
                                totalVotes += 1;
                            }
                        }
                    }
                }

                // On votings like this one we need 2/3 majority
                v.setMinVotes((int) Math.ceil((double) totalVotes * (2d / 3d)));
                v.setTotalVotes(totalVotes);

                v.addMetaData(VoteMetadata.NAME_INITIAL_USER, userId, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_FROMID, fromAllianceId, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TOID, toId, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TYPE, diplomacyType, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_DRT_TYPE, drt, MetaDataDataType.STRING);
                v.addMetaData(VoteMetadata.NAME_MESSAGE, message, MetaDataDataType.STRING);

                v.setReceivers(receivers);

                VotingFactory.persistVote(v);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while creating vote: ", e);
            }

            // We can leave here now as the outcome of this votes decides if we request to the other party
            return new BaseResult(false);
        } else {
            // No source vote necessary => DO IT FAGGOT
            if (drt == EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE) {
                createAllianceRelationVote(userId, aRequester.getId(), toId, diplomacyType, message);
            } else {
                createAllianceUserRelationVote(aRequester.getId(), userId, toId, diplomacyType, message);
            }
        }

        return br;
    }

    /*
     * CREATE VOTE FOR ALLIANCE TO ALLIANCE RELATION
     * COMING SOON: CREATE VOTE FOR ALLIANCE TO USER RELATION
     */
    public static ArrayList<String> createAllianceRelationVote(int fromUserId, int fromAllianceId, int toAllianceId, int diplomacyType, String message) {
        //Searching for an existing user relation
        Alliance fromAlliance = allianceDAO.findById(fromAllianceId);
        Alliance toAlliance = allianceDAO.findById(toAllianceId);
        DiplomacyType dt = diplomacyTypeDAO.findById(diplomacyType);
        ArrayList<DiplomacyRelation> drs = diplomacyRelationDAO.findBy(fromAllianceId, toAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
        DiplomacyRelation dr = null;
        for (DiplomacyRelation drtmp : drs) {
            if (drtmp.getPending()) {
                return null;
            } else if (!drtmp.getPending()) {
                dr = drtmp;
            }
        }
        boolean vote = false;
        int weight = 1;
        String actRel = ML.getMLStr("db_diplomacy_neutral", fromUserId);
        if (dr == null) {
        } else {
            DiplomacyType existingdt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            actRel = ML.getMLStr(existingdt.getName(), fromUserId);
            weight = existingdt.getWeight();
        }

        if (weight < dt.getWeight()) {
            vote = true;
        }
        ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(toAllianceId);
        User u = userDAO.findById(fromUserId);
        ArrayList<String> result = new ArrayList<String>();

        if (dt.isVoteRequired() && vote) {
            try {
                String msg = ML.getMLStr("diplomacy_msg_aarequest", fromUserId);
                msg = msg.replace("%ALL%", fromAlliance.getName());
                msg = msg.replace("%ACTREL%", actRel);
                msg = msg.replace("%REL%", ML.getMLStr(dt.getName(), fromUserId));
                msg = msg.replace("%MSG%", message);
                Vote v = new Vote(fromUserId, fromAlliance.getName() + " : " + actRel + " => " + ML.getMLStr(dt.getName(), fromUserId), msg, Voting.TYPE_ALLIANCERELATION);
                v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));

                // If democratic each member has same vote strength
                // If aristocratic admin has 2 .. council 1
                int totalVotes = 0;
                int minVotes = 0;

                ArrayList<Integer> receivers = new ArrayList<Integer>();

                for (AllianceMember am : allianceMemberDAO.findByAllianceId(toAllianceId)) {
                    if (toAlliance.getLeadership() == ELeadership.DEMOCRATIC) {
                        if (!am.getIsTrial()) {
                            receivers.add(am.getUserId());
                            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                            totalVotes++;
                        }
                    } else if (toAlliance.getLeadership() == ELeadership.ARISTOCRATIC) {
                        if (am.getIsCouncil() || am.getIsAdmin()) {
                            receivers.add(am.getUserId());
                            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));

                            if (am.getIsAdmin()) {
                                totalVotes += 1;
                            }
                            if (am.getIsCouncil()) {
                                totalVotes += 1;
                            }
                        }
                    } else if (toAlliance.getLeadership() == ELeadership.DICTORIAL) {
                        if (am.getIsAdmin()) {
                            receivers.add(am.getUserId());
                            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                            totalVotes += 1;
                        }
                    }
                }

                // On votings like this one we need 2/3 majority
                if (toAlliance.getLeadership() == ELeadership.DICTORIAL) {
                    v.setMinVotes(1);
                    v.setTotalVotes(totalVotes);
                } else {
                    v.setMinVotes((int) Math.ceil((double) totalVotes * (2d / 3d)));
                    v.setTotalVotes(totalVotes);
                }

                v.addOption(new VoteOption("diplomacy_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
                v.addOption(new VoteOption("diplomacy_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

                for (AllianceMember am : ams) {
                    if (am.getIsAdmin()) {
                        receivers.add(am.getUserId());
                        v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                    }
                }

                v.addMetaData(VoteMetadata.NAME_FROMID, fromAllianceId, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TOID, toAllianceId, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TYPE, diplomacyType, MetaDataDataType.INTEGER);

                v.setReceivers(receivers);

                VotingFactory.persistVote(v);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while creating vote: ", e);
            }

            createAllianceRelation(fromAllianceId, toAllianceId, diplomacyType, true);

            result.add("global_done");
        } else {
            createAllianceRelation(fromAllianceId, toAllianceId, diplomacyType, false);

            GenerateMessage gm = new GenerateMessage();
            for (AllianceMember am : ams) {
                gm.setSourceUserId(userDAO.getSystemUser().getUserId());
                gm.setTopic(fromAlliance.getName() + " : " + actRel + " => " + ML.getMLStr(dt.getName(), am.getUserId()));
                String msg = ML.getMLStr("diplomacy_msg_a2a_demote", am.getUserId());
                msg = msg.replace("%ALL%", fromAlliance.getName());
                msg = msg.replace("%ACTREL%", actRel);
                msg = msg.replace("%REL%", ML.getMLStr(dt.getName(), am.getUserId()));
                gm.setMessageType(EMessageType.SYSTEM);
                gm.setMessageContentType(EMessageContentType.DIPLOMACY);
                gm.setMsg(msg);
                gm.setDestinationUserId(am.getUserId());
                gm.setMasterEntry(true);
                gm.setEscape(false);
                gm.writeMessageToUser();
            }
        }
        return result;

    }

    public static ArrayList<String> createAllianceUserRelationVote(int allianceId, int fromUser, int toUser, int diplomacyType, String message) {
        //Searching for an existing user relation

        DiplomacyType dt = diplomacyTypeDAO.findById(diplomacyType);
        ArrayList<DiplomacyRelation> drs = diplomacyRelationDAO.findBy(allianceId, toUser, EDiplomacyRelationType.ALLIANCE_TO_USER);
        DiplomacyRelation dr = null;
        for (DiplomacyRelation drtmp : drs) {
            if (drtmp.getPending()) {
                return null;
            } else if (!drtmp.getPending()) {
                dr = drtmp;
            }
        }
        boolean vote = false;
        int weight = 1;
        String actRel = ML.getMLStr("db_diplomacy_neutral", toUser);
        if (dr == null) {
        } else {
            DiplomacyType existingdt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            actRel = ML.getMLStr(existingdt.getName(), toUser);
            weight = existingdt.getWeight();
        }

        if (weight < dt.getWeight()) {
            vote = true;
        }
        Alliance a = allianceDAO.findById(allianceId);
        User u = userDAO.findById(fromUser);
        ArrayList<String> result = new ArrayList<String>();

        if (dt.isVoteRequired() && vote) {
            try {
                Vote v = new Vote(fromUser, a.getName() + " : " + actRel + " => " + ML.getMLStr(dt.getName(), toUser), u.getGameName() + " " + ML.getMLStr("diplomacy_msg_changerequest1", toUser) + " <b> " + actRel + " </b> " + ML.getMLStr("diplomacy_msg_changerequest2", toUser) + ": <b>" + ML.getMLStr(dt.getName(), toUser) + "</b> " + ML.getMLStr("diplomacy_msg_changerequest3", toUser) + " <BR>============= <BR>" + message, Voting.TYPE_ALLIANCEUSERRELATION);
                v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));
                v.setMinVotes(1);
                v.setTotalVotes(1);

                v.addOption(new VoteOption("diplomacy_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
                v.addOption(new VoteOption("diplomacy_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

                ArrayList<Integer> receivers = new ArrayList<Integer>();

                receivers.add(toUser);

                v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, toUser));

                v.addMetaData(VoteMetadata.NAME_FROMID, allianceId, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TOID, toUser, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TYPE, diplomacyType, MetaDataDataType.INTEGER);

                v.setReceivers(receivers);

                VotingFactory.persistVote(v);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while creating vote: ", e);
            }

            createAllianceUserRelation(allianceId, toUser, diplomacyType, true);

            result.add("global_done");
        } else {
            createAllianceUserRelation(allianceId, toUser, diplomacyType, false);
            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(fromUser);
            gm.setTopic(u.getGameName() + " : " + actRel + " => " + ML.getMLStr(dt.getName(), toUser));
            gm.setMsg(u.getGameName() + " " + ML.getMLStr("diplomacy_msg_u2udemote1", toUser) + " <b> " + actRel + " </b> " + ML.getMLStr("db_diplomacy_u2udemote2", toUser) + ": <b>" + ML.getMLStr(dt.getName(), toUser) + "</b> " + ML.getMLStr("db_diplomacy_u2udemote3", toUser) + " <BR>============= <BR>" + message);
            gm.setMessageType(EMessageType.USER);
            gm.setMessageContentType(EMessageContentType.DIPLOMACY);
            gm.setDestinationUserId(toUser);
            gm.setMasterEntry(true);
            gm.writeMessageToUser();
        }
        return result;

    }

    public static LinkedList<DiplomacyType> getPossibleForUser(int userId, int toUserId) {
        if (toUserId == -1) {
            return new LinkedList<DiplomacyType>();
        }

        LinkedList<DiplomacyType> result = new LinkedList<DiplomacyType>();
        ArrayList<Alliance> alliances = allianceDAO.findAll();
        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        // Alliance alliance = null;
        if (allianceMember != null) {
            Alliance alliance = allianceDAO.findById(allianceMember.getAllianceId());
            alliances.remove(alliance);
        }
        User u = userDAO.findById(toUserId);
        DiplomacyType highestRelation = null;
        DiplomacyType highestAllianceRelation = null;
        int weight = -1000;
        //Checking Userrelations to other users  USER => USER
        if (u == null) {
            return new LinkedList<DiplomacyType>();
        }
        for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(userId, u.getUserId(), EDiplomacyRelationType.USER_TO_USER)) {
            if (dr.getPending()) {
            }
            DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            if (dt.getWeight() > weight) {
                highestRelation = dt;
                if (dr.getPending()) {
                    return new LinkedList<DiplomacyType>();
                }

                weight = dt.getWeight();
            }
        }

        for (DiplomacyType dt : diplomacyTypeDAO.findAllSettable(EDiplomacyTypeRelType.EMPIRE_RELATION)) {
            if (highestRelation != null) {
                if (dt.getId().equals(highestRelation.getId())) {
                    continue;
                }
                if (dt.getWeight() <= highestRelation.getWeight()) {
                    dt.setVoteRequired(false);
                } else {
                    dt.setVoteRequired(true);
                }
            }
            if (highestAllianceRelation != null) {
                if (dt.getWeight() < highestAllianceRelation.getWeight()) {
                    continue;
                }
            }
            result.add(dt);
        }

        return result;
    }

    public static LinkedList<DiplomacyType> getPossibleForUserAlliance(int userId, int aId) {


        if (aId == -1) {
            return new LinkedList<DiplomacyType>();
        }
        LinkedList<DiplomacyType> result = new LinkedList<DiplomacyType>();
        ArrayList<Alliance> alliances = allianceDAO.findAll();
        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        // Alliance alliance = null;
        if (allianceMember != null) {
            Alliance alliance = allianceDAO.findById(allianceMember.getAllianceId());
            alliances.remove(alliance);
        }
        DiplomacyType highestRelation = null;
        int weight = -1000;
        //Checking relations from User to the Alliance of User u  USER => ALLIANCE
        for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(userId, aId, EDiplomacyRelationType.USER_TO_ALLIANCE)) {
            if (dr.getPending()) {
                return new LinkedList<DiplomacyType>();
            }
            DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            if (dt.getWeight() > weight) {
                highestRelation = dt;
                weight = dt.getWeight();
            }
        }

        for (DiplomacyType dt : diplomacyTypeDAO.findAllSettable(EDiplomacyTypeRelType.EMPIRE_RELATION)) {
            if (highestRelation != null) {
                if (dt.getId().equals(highestRelation.getId())) {
                    continue;
                }
                if (dt.getWeight() <= highestRelation.getWeight()) {
                    dt.setVoteRequired(false);
                } else {
                    dt.setVoteRequired(true);

                }
            }
            result.add(dt);
        }

        return result;
    }

    public static LinkedList<DiplomacyType> getPossibleForAllianceUser(int aId, int userId) {


        if (userId == -1) {
            return new LinkedList<DiplomacyType>();
        }
        LinkedList<DiplomacyType> result = new LinkedList<DiplomacyType>();
        ArrayList<Alliance> alliances = allianceDAO.findAll();
        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        // Alliance alliance = null;
        if (allianceMember != null) {
            Alliance alliance = allianceDAO.findById(allianceMember.getAllianceId());
            alliances.remove(alliance);
        }
        DiplomacyType highestRelation = null;
        int weight = -1000;
        for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(aId, userId, EDiplomacyRelationType.ALLIANCE_TO_USER)) {
            if (dr.getPending()) {
                return new LinkedList<DiplomacyType>();
            }
            DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            if (dt.getWeight() > weight) {
                highestRelation = dt;
                weight = dt.getWeight();
            }
        }




        for (DiplomacyType dt : diplomacyTypeDAO.findAllSettable(EDiplomacyTypeRelType.EMPIRE_RELATION)) {
            if (highestRelation != null) {
                if (dt.getId().equals(highestRelation.getId())) {
                    continue;
                }

                if (dt.getWeight() <= highestRelation.getWeight()) {
                    dt.setVoteRequired(false);
                } else {
                    dt.setVoteRequired(true);

                }
            }
            result.add(dt);
        }



        return result;
    }

    public static LinkedList<DiplomacyType> getPossibleForAlliance(int fromAllianceId, int toAllianceId) {


        if (toAllianceId == -1) {
            return new LinkedList<DiplomacyType>();
        }
        LinkedList<DiplomacyType> result = new LinkedList<DiplomacyType>();

        DiplomacyType highestRelation = null;
        int weight = -1000;
        for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(fromAllianceId, toAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)) {
            if (dr.getPending()) {
                return new LinkedList<DiplomacyType>();
            }
            DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            if (dt.getWeight() > weight) {
                highestRelation = dt;
                weight = dt.getWeight();
            }
        }

        for (DiplomacyType dt : diplomacyTypeDAO.findAllSettable(EDiplomacyTypeRelType.EMPIRE_RELATION)) {
            if (highestRelation != null) {
                if (dt.getId().equals(highestRelation.getId())) {
                    continue;
                }
                if (dt.getWeight() <= highestRelation.getWeight()) {
                    dt.setVoteRequired(false);
                } else {
                    dt.setVoteRequired(true);

                }
            }
            result.add(dt);
        }
        return result;
    }

    @Deprecated
    public static HashMap<User, LinkedList<DiplomacyType>> getPossibleForUser(HashMap<User, LinkedList<DiplomacyType>> result, int userId, int toUserId) {


        ArrayList<Alliance> alliances = allianceDAO.findAll();
        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        // Alliance alliance = null;
        if (allianceMember != null) {
            Alliance alliance = allianceDAO.findById(allianceMember.getAllianceId());
            alliances.remove(alliance);
        }
        User u = userDAO.findById(toUserId);
        DiplomacyType highestRelation = null;
        int weight = -1000;
        //Checking Userrelations to other users  USER => USER
        for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(userId, u.getUserId(), EDiplomacyRelationType.USER_TO_USER)) {
            DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            if (dt.getWeight() > weight) {
                highestRelation = dt;
                weight = dt.getWeight();
            }
        }
        //Checking relations from User to the Alliance of User u  USER => ALLIANCE(u)
        AllianceMember am = allianceMemberDAO.findByUserId(u.getUserId());
        if (am != null) {
            for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(u.getUserId(), am.getAllianceId(), EDiplomacyRelationType.USER_TO_ALLIANCE)) {
                DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                if (dt.getWeight() > weight) {
                    highestRelation = dt;
                    weight = dt.getWeight();
                }
            }
        }
        //Checking relations from the Alliance of the User to User u ALLIANCE => USER
        if (allianceMember != null) {
            for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(allianceMember.getAllianceId(), u.getUserId(), EDiplomacyRelationType.ALLIANCE_TO_USER)) {
                DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                if (dt.getWeight() > weight) {
                    highestRelation = dt;
                    weight = dt.getWeight();
                }
            }
        }
        //Checking relations between Alliances  ALLIANCE => ALLIANCE
        if (allianceMember != null && am != null) {
            for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(allianceMember.getAllianceId(), am.getAllianceId(), EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)) {
                DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                if (dt.getWeight() > weight) {
                    highestRelation = dt;
                    weight = dt.getWeight();
                }
            }
        }


        LinkedList<DiplomacyType> dts = result.get(u);
        if (dts == null) {
            dts = new LinkedList<DiplomacyType>();
        }
        for (DiplomacyType dt : diplomacyTypeDAO.findAllSettable(EDiplomacyTypeRelType.EMPIRE_RELATION)) {
            if (highestRelation != null) {
                if (dt.getWeight() <= highestRelation.getWeight()) {
                    continue;
                }
            }
            dts.add(dt);
        }
        result.put(u, dts);



        return result;
    }

    @Deprecated
    public static void preparePossibleRelations(int userId) {
        RelationResult rr = new RelationResult();
        ArrayList<User> users = userDAO.findAll();
        User user = userDAO.findById(userId);
        ArrayList<Alliance> alliances = allianceDAO.findAll();
        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        // Alliance alliance = null;
        if (allianceMember != null) {
            Alliance alliance = allianceDAO.findById(allianceMember.getAllianceId());
            alliances.remove(alliance);
        }

        for (User u : users) {
            if (u.getUserId() == userId) {
                continue;
            }
            rr.setUserRelations(getPossibleForUser(rr.getUserRelations(), userId, u.getUserId()));
        }
        ////// Checking possible USER => ALLIANCE relations
        for (Alliance a : alliances) {
            DiplomacyType highestRelation = null;
            int weight = -1000;
            //Checking relations from User to the Alliance of User u  USER => ALLIANCE(u)
            for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(user.getUserId(), a.getId(), EDiplomacyRelationType.USER_TO_ALLIANCE)) {
                DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                if (dt.getWeight() > weight) {
                    highestRelation = dt;
                    weight = dt.getWeight();
                }
            }

            //Checking relations between Alliances  ALLIANCE => ALLIANCE
            if (allianceMember != null) {
                for (DiplomacyRelation dr : diplomacyRelationDAO.findBy(allianceMember.getAllianceId(), a.getId(), EDiplomacyRelationType.ALLIANCE_TO_USER)) {
                    DiplomacyType dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                    if (dt.getWeight() > weight) {
                        highestRelation = dt;
                        weight = dt.getWeight();
                    }
                }
            }

            LinkedList<DiplomacyType> dts = rr.getUserAllianceRelations().get(a);
            if (dts == null) {
                dts = new LinkedList<DiplomacyType>();
            }
            for (DiplomacyType dt : diplomacyTypeDAO.findAllSettable(EDiplomacyTypeRelType.EMPIRE_RELATION)) {
                if (highestRelation != null) {
                    if (dt.getWeight() <= highestRelation.getWeight()) {
                        continue;
                    }
                }
                dts.add(dt);
            }
            rr.getUserAllianceRelations().put(a, dts);
        }
        if (allianceMember != null) {
            if (allianceMember.getIsAdmin()) {
            }
        }

        DiplomacyService.relationResult = rr;

    }

    public static ArrayList<String> createUserAllianceRelationVote(int userId, int allianceId, int diplomacyType, String message) {
        //Searching for an existing user relation
        DiplomacyType dt = diplomacyTypeDAO.findById(diplomacyType);
        ArrayList<DiplomacyRelation> drs = diplomacyRelationDAO.findBy(userId, allianceId, EDiplomacyRelationType.USER_TO_ALLIANCE);
        DiplomacyRelation dr = null;
        for (DiplomacyRelation drtmp : drs) {
            if (drtmp.getPending()) {
                return null;
            } else if (!drtmp.getPending()) {
                dr = drtmp;
            }
        }
        boolean vote = false;
        int weight = 0;
        String actRel = ML.getMLStr("db_diplomacy_neutral", userId);
        if (dr == null) {
        } else {
            DiplomacyType existingdt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            actRel = ML.getMLStr(existingdt.getName(), userId);
            weight = existingdt.getWeight();
        }

        if (weight < dt.getWeight()) {
            vote = true;
        }
        User u = userDAO.findById(userId);
        ArrayList<String> result = new ArrayList<String>();

        if (dt.isVoteRequired() && vote) {
            try {
                // Load Alliance
                Alliance toAlliance = allianceDAO.findById(allianceId);

                Vote v = new Vote(userId, u.getGameName() + " : " + actRel + " => " + ML.getMLStr(dt.getName(), userId), u.getGameName() + " " + ML.getMLStr("diplomacy_msg_ua_changerequest1", userId) + " <b> " + actRel + " </b> " + ML.getMLStr("diplomacy_msg_ua_changerequest2", userId) + ": <b>" + ML.getMLStr(dt.getName(), userId) + "</b> " + ML.getMLStr("diplomacy_msg_ua_changerequest3", userId) + " <BR>============= <BR>" + message, Voting.TYPE_USERALLIANCERELATION);
                v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));

                // If democratic each member has same vote strength
                // If aristocratic admin has 2 .. council 1
                int totalVotes = 0;
                int minVotes = 0;

                ArrayList<Integer> receivers = new ArrayList<Integer>();

                for (AllianceMember am : allianceMemberDAO.findByAllianceId(allianceId)) {
                    if (toAlliance.getLeadership() == ELeadership.DEMOCRATIC) {
                        if (!am.getIsTrial()) {
                            receivers.add(am.getUserId());
                            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                            totalVotes++;
                        }
                    } else if (toAlliance.getLeadership() == ELeadership.ARISTOCRATIC) {
                        if (am.getIsCouncil() || am.getIsAdmin()) {
                            receivers.add(am.getUserId());
                            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));

                            if (am.getIsAdmin()) {
                                totalVotes += 1;
                            }
                            if (am.getIsCouncil()) {
                                totalVotes += 1;
                            }
                        }
                    } else if (toAlliance.getLeadership() == ELeadership.DICTORIAL) {
                        if (am.getIsAdmin()) {
                            receivers.add(am.getUserId());
                            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                            totalVotes += 1;
                        }
                    }
                }

                // On votings like this one we need 2/3 majority
                if (toAlliance.getLeadership() == ELeadership.DICTORIAL) {
                    v.setMinVotes(1);
                    v.setTotalVotes(totalVotes);
                } else {
                    v.setMinVotes((int) Math.ceil((double) totalVotes * (2d / 3d)));
                    v.setTotalVotes(totalVotes);
                }

                v.addOption(new VoteOption(ML.getMLStr("diplomacy_link_accept", userId), true, VoteOption.OPTION_TYPE_ACCEPT));
                v.addOption(new VoteOption(ML.getMLStr("diplomacy_link_decline", userId), false, VoteOption.OPTION_TYPE_DENY));

                /*
                 ArrayList<Integer> receivers = new ArrayList<Integer>();
                 ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(allianceId);
                 for (AllianceMember am : ams) {
                 if (am.getIsAdmin()) {
                 receivers.add(am.getUserId());
                 v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                 }
                 }
                 */

                v.addMetaData(VoteMetadata.NAME_FROMID, u.getUserId(), MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TOID, allianceId, MetaDataDataType.INTEGER);
                v.addMetaData(VoteMetadata.NAME_TYPE, diplomacyType, MetaDataDataType.INTEGER);

                v.setReceivers(receivers);

                VotingFactory.persistVote(v);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while creating vote: ", e);
                log.debug("Error while creating vote: " + e);
            }

            createUserAllianceRelation(userId, allianceId, diplomacyType, true);

            result.add("global_done");
        } else {
            createUserAllianceRelation(userId, allianceId, diplomacyType, false);
            ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(allianceId);

            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(userId);
            gm.setTopic(u.getGameName() + " : " + actRel + " => " + ML.getMLStr(dt.getName(), userId));
            gm.setMsg(u.getGameName() + " " + ML.getMLStr("diplomacy_msg_u2ademote1", userId) + " <b> " + actRel + " </b> " + ML.getMLStr("diplomacy_msg_u2ademote2", userId) + ": <b>" + ML.getMLStr(dt.getName(), userId) + "</b> " + ML.getMLStr("diplomacy_msg_u2ademote3" + " <BR>============= <BR>" + message, userId));
            gm.setMessageType(EMessageType.USER);
            gm.setMessageContentType(EMessageContentType.DIPLOMACY);
            gm.setMasterEntry(true);

            for (AllianceMember am : ams) {
                if (am.getIsAdmin()) {
                    gm.setDestinationUserId(am.getUserId());
                    gm.writeMessageToUser();
                }
            }

        }
        return result;

    }

    public static String SharingTypeToString(ESharingType type, int userId) {
        return ML.getMLStr("SHARINGTYPE_" + type.toString(), userId);
    }

    public static ArrayList<String> createUserRelation(int fromId, int toId, int diplomacyType, boolean pending) {


        //Forced => Try delete old (higher) relations
        if (!pending) {
            deleteRelation(fromId, toId, EDiplomacyRelationType.USER_TO_USER);
        }


        ArrayList<String> result = new ArrayList<String>();
        // log.debug("Diplo : " + diplomacyType + " pending " + pending);
        if (diplomacyType == DiplomacyType.NEUTRAL && !pending) {
            //do nothing
        } else {
            // log.debug("Creating rel");
            Long date = java.lang.System.currentTimeMillis();
            DiplomacyRelation ur1 = new DiplomacyRelation();
            ur1.setFromId(fromId);
            ur1.setToId(toId);
            ur1.setDiplomacyTypeId(diplomacyType);
            ur1.setType(EDiplomacyRelationType.USER_TO_USER);
            ur1.setInitialiser(true);
            ur1.setAcknowledged(true);
            ur1.setPending(pending);
            ur1.setDate(date);

            DebugBuffer.addLine(DebugLevel.WARNING, "[1] Try to create USER_TO_USER from " + fromId + " to " + toId);
            diplomacyRelationDAO.add(ur1);

            DiplomacyRelation ur2 = new DiplomacyRelation();
            ur2.setDiplomacyTypeId(diplomacyType);
            ur2.setFromId(toId);
            ur2.setToId(fromId);
            ur2.setDate(date);
            ur2.setInitialiser(Boolean.FALSE);
            ur2.setAcknowledged(Boolean.FALSE);
            ur2.setPending(pending);
            ur2.setType(EDiplomacyRelationType.USER_TO_USER);

            DebugBuffer.addLine(DebugLevel.WARNING, "[2] Try to create USER_TO_USER from " + toId + " to " + fromId);
            diplomacyRelationDAO.add(ur2);
        }



        result.add("global_done");
        return result;

    }

    public static ArrayList<String> createUserAllianceRelation(int userId, int toAlliance, int diplomacyType, boolean pending) {
        Long date = java.lang.System.currentTimeMillis();

        //Forced => Try delete old (higher) relations
        if (!pending) {
            deleteRelation(userId, toAlliance, EDiplomacyRelationType.USER_TO_ALLIANCE);
        }
        ArrayList<String> result = new ArrayList<String>();
        if (diplomacyType == DiplomacyType.NEUTRAL && !pending) {
            //do nothing
        } else {
            DiplomacyRelation uar = new DiplomacyRelation();
            uar.setFromId(userId);
            uar.setDiplomacyTypeId(diplomacyType);
            uar.setToId(toAlliance);
            uar.setType(EDiplomacyRelationType.USER_TO_ALLIANCE);
            uar.setInitialiser(Boolean.TRUE);
            uar.setAcknowledged(Boolean.TRUE);
            uar.setPending(pending);
            uar.setDate(date);
            DebugBuffer.addLine(DebugLevel.WARNING, "[3] Try to create USER_TO_ALLIANCE from " + userId + " to " + toAlliance);
            diplomacyRelationDAO.add(uar);

            DiplomacyRelation uar2 = new DiplomacyRelation();
            uar2.setFromId(toAlliance);
            uar2.setDiplomacyTypeId(diplomacyType);
            uar2.setToId(userId);
            uar2.setType(EDiplomacyRelationType.ALLIANCE_TO_USER);
            uar2.setInitialiser(Boolean.FALSE);
            uar2.setAcknowledged(Boolean.FALSE);
            uar2.setPending(pending);
            uar2.setDate(date);
            DebugBuffer.addLine(DebugLevel.WARNING, "[4] Try to create ALLIANCE_TO_USER from " + toAlliance + " to " + userId);
            diplomacyRelationDAO.add(uar2);

        }

        result.add("global_done");
        return result;

    }

    public static long getMaxDonateAmount(int userId) {
        UserData ud = userDataDAO.findByUserId(userId);

        // long maxBasicAmount = (int) Math.floor(ud.getWeeklyIncome() * 0.1d);
        long maxBasicAmount = (long) Math.floor(ud.getWeeklyIncome() * 0.1d);

        // Sum up donates of last week
        ArrayList<CreditDonation> cdList = creditDonationDAO.findLastWeekDonations(userId);
        for (CreditDonation cd : cdList) {
            maxBasicAmount -= cd.getAmount();
        }

        if (maxBasicAmount > 0) {
            return maxBasicAmount;
        } else {
            return 0;
        }
    }

    public static BaseResult createDonation(int userId, Map params) {
        int toUserId = 0;
        long amount = 0;
        BaseResult br;
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String key = (String) me.getKey();
            if (key.equals("toUserId")) {
                toUserId = Integer.parseInt(((String[]) me.getValue())[0]);

            } else if (key.equals("amount")) {
                try {
                    amount = Long.parseLong(((String[]) me.getValue())[0].trim());
                } catch (NumberFormatException nfe) {
                    br = new BaseResult(ML.getMLStr("donatemoney_err_amountinvalid", userId), true);
                    return br;
                }
            }

        }
        br = createNewDonation(userId, toUserId, amount);
        return br;
    }

    public static BaseResult createNewDonation(int fromUserId, int toUserId, long amount) {
        if (amount < 0) {
            return new BaseResult(ML.getMLStr("donatemoney_err_amountinvalid", fromUserId), true);
        }

        if (fromUserId == 0 || toUserId == 0) {
            return new BaseResult(ML.getMLStr("donatemoney_err_userinvalid", fromUserId), true);
        }
        UserData fromUser = userDataDAO.findByUserId(fromUserId);
        UserData toUser = userDataDAO.findByUserId(toUserId);

        if ((fromUser == null) || (toUser == null)) {
            return new BaseResult(ML.getMLStr("donatemoney_err_userinvalid", fromUserId), true);
        }

        if (amount > fromUser.getCredits()) {
            DebugBuffer.addLine(DebugLevel.TRACE, "Spieler : " + fromUserId + " wollte mehr Credits transferieren als er besitzt");
        } else {
            User u1 = userDAO.findById(fromUserId);
            User u2 = userDAO.findById(toUserId);

            if (u1.getTrial() || u2.getTrial()) {
                return new BaseResult(ML.getMLStr("donatemoney_err_userinvalid", fromUserId), true);
            }

            // Check possible amount
            if (amount > getMaxDonateAmount(fromUserId)) {
                return new BaseResult("H�her als maximaler Betrag", true);
            }

            TransactionHandler th = TransactionHandler.getTransactionHandler();
            try {

                th.startTransaction();

                CreditDonation cd = new CreditDonation();
                cd.setFromUserId(fromUserId);
                cd.setToUserId(toUserId);
                cd.setAmount(amount);
                cd.setDate(java.lang.System.currentTimeMillis());

                creditDonationDAO.add(cd);

                toUser = userDataDAO.findByUserId(toUserId);

                fromUser.setCredits(fromUser.getCredits() - amount);
                userDataDAO.update(fromUser);
                toUser.setCredits(toUser.getCredits() + amount);
                userDataDAO.update(toUser);
                th.endTransaction();
            } catch (TransactionException te) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Error in Credit donation");
                th.rollback();
            }


        }
        return new BaseResult(ML.getMLStr("global_done", fromUserId), false);

    }

    public static ArrayList<String> createAllianceRelation(int fromAllianceId, int toAllianceId, int diplomacyType, boolean pending) {
        //Forced => Try delete old (higher) relations
        if (!pending) {
            deleteRelation(fromAllianceId, toAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
        }


        ArrayList<String> result = new ArrayList<String>();
        if (diplomacyType == DiplomacyType.NEUTRAL && !pending) {
            //do nothing
        } else {
            Long date = java.lang.System.currentTimeMillis();
            DiplomacyRelation ur1 = new DiplomacyRelation();
            ur1.setFromId(fromAllianceId);
            ur1.setToId(toAllianceId);
            ur1.setDiplomacyTypeId(diplomacyType);
            ur1.setPending(pending);
            ur1.setType(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            ur1.setInitialiser(Boolean.TRUE);
            ur1.setAcknowledged(Boolean.TRUE);
            ur1.setDate(date);

            DebugBuffer.addLine(DebugLevel.WARNING, "[5] Try to create ALLIANCE_TO_ALLIANCE from " + fromAllianceId + " to " + toAllianceId);
            diplomacyRelationDAO.add(ur1);

            DiplomacyRelation ur2 = new DiplomacyRelation();
            ur2.setFromId(toAllianceId);
            ur2.setToId(fromAllianceId);
            ur2.setDiplomacyTypeId(diplomacyType);
            ur2.setDate(date);
            ur2.setPending(pending);
            ur2.setInitialiser(Boolean.FALSE);
            ur2.setAcknowledged(Boolean.FALSE);
            ur2.setType(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);

            DebugBuffer.addLine(DebugLevel.WARNING, "[6] Try to create ALLIANCE_TO_ALLIANCE from " + toAllianceId + " to " + fromAllianceId);
            diplomacyRelationDAO.add(ur2);

        }
        result.add("global_done");
        return result;

    }

    public static void removeAlliance(int allianceId) {
        // ArrayList<DiplomacyRelation> ars = diplomacyRelationDAO.findUserAllianceRelationByAllianceId(allianceId);
        ArrayList<DiplomacyRelation> ars = diplomacyRelationDAO.findByToId(allianceId, EDiplomacyRelationType.USER_TO_ALLIANCE);
        for (DiplomacyRelation entry : ars) {
            diplomacyRelationDAO.remove(entry);
        }
        ArrayList<DiplomacyRelation> ar1 = diplomacyRelationDAO.findByFromId(allianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
        for (DiplomacyRelation entry : ar1) {
            diplomacyRelationDAO.remove(entry);
        }
        ArrayList<DiplomacyRelation> ar2 = diplomacyRelationDAO.findByToId(allianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
        for (DiplomacyRelation entry : ar2) {
            diplomacyRelationDAO.remove(entry);
        }
        ArrayList<DiplomacyRelation> aur = diplomacyRelationDAO.findByFromId(allianceId, EDiplomacyRelationType.ALLIANCE_TO_USER);
        for (DiplomacyRelation entry : aur) {
            diplomacyRelationDAO.remove(entry);
        }
    }

    public static void removeUser(int userId) {
        ArrayList<DiplomacyRelation> ars = diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_ALLIANCE);

        for (DiplomacyRelation entry : ars) {
            diplomacyRelationDAO.remove(entry);
        }
        ArrayList<DiplomacyRelation> ar1 = diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_USER);
        for (DiplomacyRelation entry : ar1) {
            diplomacyRelationDAO.remove(entry);
        }
        ArrayList<DiplomacyRelation> ar2 = diplomacyRelationDAO.findByToId(userId, EDiplomacyRelationType.USER_TO_USER);
        for (DiplomacyRelation entry : ar2) {
            diplomacyRelationDAO.remove(entry);
        }
        ArrayList<DiplomacyRelation> aur = diplomacyRelationDAO.findByToId(userId, EDiplomacyRelationType.ALLIANCE_TO_USER);
        for (DiplomacyRelation entry : aur) {
            diplomacyRelationDAO.remove(entry);
        }
    }

    public static ArrayList<String> createAllianceUserRelation(int fromAllianceId, int toUser, int diplomacyType, boolean pending) {
        Long date = java.lang.System.currentTimeMillis();
        if (!pending) {
            deleteRelation(fromAllianceId, toUser, EDiplomacyRelationType.ALLIANCE_TO_USER);
        }
        ArrayList<String> result = new ArrayList<String>();
        if (diplomacyType == DiplomacyType.NEUTRAL && !pending) {
            //do nothing
        } else {
            DiplomacyRelation uar = new DiplomacyRelation();
            uar.setFromId(fromAllianceId);
            uar.setDiplomacyTypeId(diplomacyType);
            uar.setToId(toUser);
            uar.setType(EDiplomacyRelationType.ALLIANCE_TO_USER);
            uar.setInitialiser(Boolean.TRUE);
            uar.setAcknowledged(Boolean.TRUE);
            uar.setPending(pending);
            uar.setDate(date);
            DebugBuffer.addLine(DebugLevel.WARNING, "[7] Try to create ALLIANCE_TO_USER from " + fromAllianceId + " to " + toUser);
            diplomacyRelationDAO.add(uar);

            DiplomacyRelation uar2 = new DiplomacyRelation();
            uar2.setFromId(toUser);
            uar2.setDiplomacyTypeId(diplomacyType);
            uar2.setToId(fromAllianceId);
            uar2.setType(EDiplomacyRelationType.USER_TO_ALLIANCE);
            uar2.setInitialiser(Boolean.FALSE);
            uar2.setAcknowledged(Boolean.FALSE);
            uar2.setPending(pending);
            uar2.setDate(date);
            DebugBuffer.addLine(DebugLevel.WARNING, "[8] Try to create USER_TO_ALLIANCE from " + toUser + " to " + fromAllianceId);
            diplomacyRelationDAO.add(uar2);
        }


        result.add("global_done");
        return result;

    }

    public static void deleteRelation(int fromId, int toId, EDiplomacyRelationType type) {

        if (type.equals(EDiplomacyRelationType.USER_TO_USER)) {
            ArrayList<DiplomacyRelation> ur1 = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.USER_TO_USER);
            for (DiplomacyRelation dr : ur1) {
                diplomacyRelationDAO.remove(dr);
            }
            ArrayList<DiplomacyRelation> ur2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.USER_TO_USER);
            for (DiplomacyRelation dr : ur2) {
                diplomacyRelationDAO.remove(dr);
            }
        } else if (type.equals(EDiplomacyRelationType.USER_TO_ALLIANCE)) {
            ArrayList<DiplomacyRelation> ur1 = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.USER_TO_ALLIANCE);
            for (DiplomacyRelation dr : ur1) {
                diplomacyRelationDAO.remove(dr);
            }
            ArrayList<DiplomacyRelation> ur2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.ALLIANCE_TO_USER);
            for (DiplomacyRelation dr : ur2) {
                diplomacyRelationDAO.remove(dr);
            }
        } else if (type.equals(EDiplomacyRelationType.ALLIANCE_TO_USER)) {
            ArrayList<DiplomacyRelation> ur1 = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.ALLIANCE_TO_USER);
            for (DiplomacyRelation dr : ur1) {
                diplomacyRelationDAO.remove(dr);
            }
            ArrayList<DiplomacyRelation> ur2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.USER_TO_ALLIANCE);
            for (DiplomacyRelation dr : ur2) {
                diplomacyRelationDAO.remove(dr);
            }
        } else if (type.equals(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)) {
            ArrayList<DiplomacyRelation> ur1 = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            for (DiplomacyRelation dr : ur1) {
                diplomacyRelationDAO.remove(dr);
            }
            ArrayList<DiplomacyRelation> ur2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            for (DiplomacyRelation dr : ur2) {
                diplomacyRelationDAO.remove(dr);
            }
        }
    }

    public static void deleteRelation(int fromId, int toId, EDiplomacyRelationType type, boolean pending) {

        if (type == EDiplomacyRelationType.USER_TO_USER) {
            DiplomacyRelation ur1 = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.USER_TO_USER, pending);
            diplomacyRelationDAO.remove(ur1);
            DiplomacyRelation ur2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.USER_TO_USER, pending);
            diplomacyRelationDAO.remove(ur2);

        } else if (type == EDiplomacyRelationType.USER_TO_ALLIANCE) {
            DiplomacyRelation ur1 = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.USER_TO_ALLIANCE, pending);
            diplomacyRelationDAO.remove(ur1);
            DiplomacyRelation ur2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.ALLIANCE_TO_USER, pending);

            diplomacyRelationDAO.remove(ur2);

        } else if (type == EDiplomacyRelationType.ALLIANCE_TO_USER) {
            DiplomacyRelation ur1 = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.ALLIANCE_TO_USER, pending);
            diplomacyRelationDAO.remove(ur1);
            DiplomacyRelation ur2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.USER_TO_ALLIANCE, pending);
            diplomacyRelationDAO.remove(ur2);
        } else if (type == EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE) {
            ArrayList<DiplomacyRelation> ur1 = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            for (DiplomacyRelation dr : ur1) {
                diplomacyRelationDAO.remove(dr);
            }
            ArrayList<DiplomacyRelation> ur2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
            for (DiplomacyRelation dr : ur2) {
                diplomacyRelationDAO.remove(dr);
            }
        }
    }

    public static BaseResult createEmbassy(int userId, PlayerFleetExt pfe) {
        if (FleetService.canCreateEmbassy(pfe)) {

            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(pfe.getRelativeCoordinate().getPlanetId());
            return createEmbassy(userId, pp.getUserId());
        } else {
            return new BaseResult("Fehler beim errichten der Botschaft", false);
        }
    }

    public static BaseResult createEmbassy(int userId, int remUserId) {
        BaseResult br = new BaseResult("Botschaft errichtet", false);
        ArrayList<Integer> affectedUsers = new ArrayList<Integer>();
        Embassy e = embassyDAO.findBy(userId, remUserId);
        DebugBuffer.addLine(DebugLevel.WARNING, "Trying to create embassy by " + userId + " at user : " + remUserId);
        if (e != null && e.isSynced()) {
            DebugBuffer.addLine(DebugLevel.WARNING, "Embassy already existing");
            return new BaseResult("Botschaft bereits vorhanden", false);
        } else if (e != null && !e.isSynced()) {
            DebugBuffer.addLine(DebugLevel.WARNING, "Embassy found but not yet synced");
            // Try to Sync
            for (Integer i : DiplomacyUtilities.findSharingUsers(userId)) {
                if (i == userId) {
                    continue;
                }
                DebugBuffer.addLine(DebugLevel.WARNING, "Sharing Viewtables with " + i);
                boolean worked = ViewTableUtilities.copyEntries(userId, i, Direction.USER_TO_USER, true);
                DebugBuffer.addLine(DebugLevel.WARNING, "Entries copied");
                if (worked) {
                    embassyDAO.updateSync(userId, remUserId, true);
                    affectedUsers.add(i);
                }
            }
            EmbassyNotification en = new EmbassyNotification(userId, remUserId, affectedUsers);
        } else if (e == null) {
            DebugBuffer.addLine(DebugLevel.WARNING, "New embassy, trying to sync with users");
            e = new Embassy();
            e.setUserId(userId);
            e.setRemUserId(remUserId);
            e.setSynced(true);
            embassyDAO.add(e);
            e = new Embassy();
            e.setUserId(remUserId);
            e.setRemUserId(userId);
            e.setSynced(true);
            embassyDAO.add(e);
            boolean worked = ViewTableUtilities.copyEntries(userId, remUserId, Direction.USER_TO_USER, true);
            for (Integer i : DiplomacyUtilities.findSharingUsers(userId)) {
                if (i == userId) {
                    continue;
                }
                DebugBuffer.addLine(DebugLevel.WARNING, "Sharing Viewtables with " + i);
                ViewTableUtilities.copyEntries(userId, i, Direction.USER_TO_USER, true);
                DebugBuffer.addLine(DebugLevel.WARNING, "Entries copied");
            }
            if (worked) {
                embassyDAO.updateSync(userId, remUserId, Boolean.TRUE);
                affectedUsers.add(remUserId);

            }
            EmbassyNotification en = new EmbassyNotification(userId, remUserId, affectedUsers);
        }

        return br;
    }

    public static ArrayList<CreditDonation> findAllDonations() {
        TreeMap<Long, CreditDonation> ordered = new TreeMap<Long, CreditDonation>();
        ArrayList<CreditDonation> result = creditDonationDAO.findAll();

        for (CreditDonation cd : result) {
            ordered.put(cd.getDate(), cd);
        }
        result.clear();
        result.addAll(ordered.descendingMap().values());
        return result;
    }

    public static boolean hasEmbassy(int fromId, int toId, EDiplomacyRelationType type) {
        if (type.equals(EDiplomacyRelationType.USER_TO_USER)) {
            return embassyDAO.hasEmbassy(fromId, toId);
        } else if (type.equals(EDiplomacyRelationType.ALLIANCE_TO_USER)) {
            for (AllianceMember am : Service.allianceMemberDAO.findByAllianceId(fromId)) {
                if (embassyDAO.hasEmbassy(am.getUserId(), toId)) {
                    return true;
                }
            }
            return false;
        } else if (type.equals(EDiplomacyRelationType.USER_TO_ALLIANCE)) {
            for (AllianceMember am : Service.allianceMemberDAO.findByAllianceId(toId)) {
                if (embassyDAO.hasEmbassy(am.getUserId(), toId)) {
                    return true;
                }
            }
            return false;
        } else if (type.equals(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)) {
            for (AllianceMember am : Service.allianceMemberDAO.findByAllianceId(fromId)) {
                for (AllianceMember am1 : Service.allianceMemberDAO.findByAllianceId(toId)) {
                    if (embassyDAO.hasEmbassy(am.getUserId(), am1.getUserId())) {
                        return true;
                    }
                }
            }
            return false;
        }
        return false;
    }

    public static Alliance findAllianceById(int allianceId) {
        return allianceDAO.findById(allianceId);
    }

    public static Alliance findAllianceById(int userId, int allianceId) {
        if (findAlliancesToDisplay(userId).contains(allianceId)) {
            return allianceDAO.findById(allianceId);
        } else {
            return null;
        }
    }

    public static String findAllianceNameById(int allianceId) {
        return allianceDAO.findById(allianceId).getName();
    }

    public static String findAllianceTagById(int allianceId) {
        return allianceDAO.findById(allianceId).getTag();
    }

    public static User findUserById(int userId) {
        return userDAO.findById(userId);
    }

    public static User findUserById(int userId, int toShow) {
        if (findUsersToDisplay(userId).contains(toShow)) {
            return userDAO.findById(toShow);
        } else {
            return null;
        }
    }

    public static ArrayList<DiplomacyType> findAllDiplomacyTypeSettable() {
        return diplomacyTypeDAO.findAllSettable(EDiplomacyTypeRelType.EMPIRE_RELATION);
    }

    public static ArrayList<DiplomacyType> findAllDiplomacyType() {
        try {
            return diplomacyTypeDAO.sortDiplomacyTypeArrayList(diplomacyTypeDAO.findAll(EDiplomacyTypeRelType.EMPIRE_RELATION),ESorting.DESCENDING);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in findAllDiplomacyType: ", e);        
        }

        return new ArrayList<DiplomacyType>();
    }

    public static DiplomacyType findDiplomacyTypeById(int diplomacyTypeId) {
        return diplomacyTypeDAO.findById(diplomacyTypeId);
    }

    public static ArrayList<User> findAllUsersDisplayAble(int userId) {
        ArrayList<User> result = new ArrayList<User>();
        for (Integer i : findUsersToDisplay(userId)) {
            result.add(userDAO.findById(i));
        }
        return result;
    }

    public static ArrayList<User> findAllUsersNotTrial() {
        ArrayList<User> result = new ArrayList<User>();
        ArrayList<User> users = userDAO.findAll();
        for (User u : users) {
            if (!u.getTrial() && (u.getUserType() == EUserType.HUMAN)) {
                result.add(u);
            }
        }
        return result;
    }

    public static AllianceMember findAllianceMemberByUserId(Integer userId) {
        return allianceMemberDAO.findByUserId(userId);
    }

    public static ArrayList<Alliance> findAllAlliances() {
        return allianceDAO.findAll();
    }

    public static ArrayList<Alliance> findAllAlliancesSortedByName() {
        return allianceDAO.findAllSortedByName();
    }

    public static void updateUserRelation(Integer fromId, Integer toId, boolean accepted) {

        if (accepted) {
            DiplomacyType dt = null;
            ArrayList<DiplomacyRelation> existing = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.USER_TO_USER);
            for (DiplomacyRelation dr : existing) {
                if (!dr.getPending()) {
                    diplomacyRelationDAO.remove(dr);
                } else if (dr.getPending()) {
                    dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());

                    if (dr.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                        dr.setPending(false);
                        dr.setAcknowledged(true);
                        diplomacyRelationDAO.update(dr);
                    } else {
                        diplomacyRelationDAO.remove(dr);
                    }
                }
            }
            ArrayList<DiplomacyRelation> existing2 = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.USER_TO_USER);
            for (DiplomacyRelation dr : existing2) {
                if (!dr.getPending()) {
                    diplomacyRelationDAO.remove(dr);
                } else if (dr.getPending()) {
                    if (dr.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                        dr.setPending(false);
                        dr.setAcknowledged(true);
                        diplomacyRelationDAO.update(dr);
                    } else {
                        diplomacyRelationDAO.remove(dr);
                    }
                }
            }

            String dtName = "?";
            if (dt != null) {
                dtName = dt.getName();
            }
            
            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            User u = userDAO.findById(toId);
            gm.setTopic(u.getGameName() + " : " + ML.getMLStr(dtName, fromId) + "  " + ML.getMLStr("diplomacy_msg_accepted", fromId));
            String msg = ML.getMLStr("diplomacy_msg_uuaccept", fromId);
            msg = msg.replace("%USR%", u.getGameName());
            msg = msg.replace("%REL%", "<B>" + ML.getMLStr(dtName, fromId) + "</B>");
            gm.setMsg(msg);
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setMessageContentType(EMessageContentType.DIPLOMACY);
            gm.setDestinationUserId(fromId);
            gm.setMasterEntry(true);
            gm.writeMessageToUser();

            //Abgelehnt
        } else {
            DiplomacyType dt;
            DiplomacyRelation existing = diplomacyRelationDAO.findBy(fromId, toId, EDiplomacyRelationType.USER_TO_USER, true);

            dt = diplomacyTypeDAO.findById(existing.getDiplomacyTypeId());
            diplomacyRelationDAO.remove(existing);
            existing = diplomacyRelationDAO.findBy(toId, fromId, EDiplomacyRelationType.USER_TO_USER, true);

            diplomacyRelationDAO.remove(existing);


            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            User u = userDAO.findById(toId);
            gm.setTopic(u.getGameName() + " : " + ML.getMLStr(dt.getName(), fromId) + "  " + ML.getMLStr("diplomacy_msg_decline", fromId));
            String msg = ML.getMLStr("diplomacy_msg_uudecline", fromId);
            msg = msg.replace("%USR%", u.getGameName());
            msg = msg.replace("%REL%", "<B>" + ML.getMLStr(dt.getName(), fromId) + "</B>");
            gm.setMsg(msg);
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setMessageContentType(EMessageContentType.DIPLOMACY);
            gm.setDestinationUserId(fromId);
            gm.setMasterEntry(true);
            gm.writeMessageToUser();
        }
    }

    public static void updateUserAllianceRelation(Integer userId, Integer allianceId, boolean accepted) {

        //Angenommen
        if (accepted) {
            DiplomacyType dt = null;
            ArrayList<DiplomacyRelation> existing = diplomacyRelationDAO.findBy(allianceId, userId, EDiplomacyRelationType.ALLIANCE_TO_USER);

            for (DiplomacyRelation dr : existing) {
                if (!dr.getPending()) {
                    diplomacyRelationDAO.remove(dr);
                } else if (dr.getPending()) {
                    dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                    if (dr.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                        dr.setPending(false);
                        dr.setAcknowledged(true);
                        diplomacyRelationDAO.update(dr);
                    } else {
                        diplomacyRelationDAO.remove(dr);
                    }
                }
            }

            ArrayList<DiplomacyRelation> existing2 = diplomacyRelationDAO.findBy(userId, allianceId, EDiplomacyRelationType.USER_TO_ALLIANCE);
            for (DiplomacyRelation dr : existing2) {
                if (!dr.getPending()) {
                    diplomacyRelationDAO.remove(dr);
                } else if (dr.getPending()) {
                    if (dr.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                        dr.setPending(false);
                        dr.setAcknowledged(true);
                        diplomacyRelationDAO.update(dr);
                    } else {
                        diplomacyRelationDAO.remove(dr);
                    }
                }
            }
            
            String dtName = "?";
            if (dt != null) {
                dtName = dt.getName();
            }            
            
            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            Alliance a = allianceDAO.findById(allianceId);
            // DebugBuffer.error("Error catch Alliance is " + a + " DiplomatyType is " + dt + " userId is " + userId);
            gm.setTopic(a.getName() + " : " + ML.getMLStr(dtName, userId) + "  " + ML.getMLStr("diplomacy_msg_accepted", userId));
            String msg = ML.getMLStr("diplomacy_msg_uaaccept", userId);
            msg = msg.replace("%ALL%", a.getName());
            msg = msg.replace("%REL%", "<B>" + ML.getMLStr(dtName, userId) + "</B>");
            gm.setMsg(msg);
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setMessageContentType(EMessageContentType.DIPLOMACY);
            gm.setDestinationUserId(userId);
            gm.setMasterEntry(true);
            gm.writeMessageToUser();


            //Abgelehnt
        } else {
            DiplomacyType dt = null;

            ArrayList<DiplomacyRelation> existing = diplomacyRelationDAO.findBy(allianceId, userId, EDiplomacyRelationType.ALLIANCE_TO_USER);

            for (DiplomacyRelation dr : existing) {
                if (dr.getPending()) {
                    dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                    diplomacyRelationDAO.remove(dr);
                }
            }
            existing.clear();
            existing = diplomacyRelationDAO.findBy(userId, allianceId, EDiplomacyRelationType.USER_TO_ALLIANCE);

            for (DiplomacyRelation dr : existing) {
                if (dr.getPending()) {
                    dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                    diplomacyRelationDAO.remove(dr);
                }
            }
            
            String dtName = "?";
            if (dt != null) {
                dtName = dt.getName();
            }            
            
            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            Alliance a = allianceDAO.findById(allianceId);
            gm.setTopic(a.getName() + " : " + ML.getMLStr(dtName, userId) + "  " + ML.getMLStr("diplomacy_msg_decline", userId));
            String msg = ML.getMLStr("diplomacy_msg_uadecline", userId);
            msg = msg.replace("%ALL%", a.getName());
            msg = msg.replace("%REL%", "<B>" + ML.getMLStr(dtName, userId) + "</B>");
            gm.setMsg(msg);
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setMessageContentType(EMessageContentType.DIPLOMACY);
            gm.setDestinationUserId(userId);
            gm.setMasterEntry(true);
            gm.writeMessageToUser();
        }
    }

    public static void updateAllianceUserRelation(Integer allianceId, Integer userId, boolean accepted) {

        //Angenommen
        if (accepted) {
            DiplomacyType dt = null;
            ArrayList<DiplomacyRelation> existing = diplomacyRelationDAO.findBy(allianceId, userId, EDiplomacyRelationType.ALLIANCE_TO_USER);




            for (DiplomacyRelation dr : existing) {
                if (!dr.getPending()) {
                    diplomacyRelationDAO.remove(dr);


                } else if (dr.getPending()) {
                    dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                    if (dr.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                        dr.setPending(false);
                        dr.setAcknowledged(true);
                        diplomacyRelationDAO.update(dr);

                    } else {
                        diplomacyRelationDAO.remove(dr);
                    }
                }
            }
            ArrayList<DiplomacyRelation> existing2 = diplomacyRelationDAO.findBy(userId, allianceId, EDiplomacyRelationType.USER_TO_ALLIANCE);


            for (DiplomacyRelation dr : existing2) {
                if (!dr.getPending()) {
                    diplomacyRelationDAO.remove(dr);


                } else if (dr.getPending()) {
                    if (dr.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                        dr.setPending(false);
                        dr.setAcknowledged(true);
                        diplomacyRelationDAO.update(dr);


                    }
                } else {
                    diplomacyRelationDAO.remove(dr);
                }
            }
            GenerateMessage gm = new GenerateMessage();
            User u = userDAO.findById(userId);
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            Alliance a = allianceDAO.findById(allianceId);
            ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(allianceId);

            String dtName = "?";
            if (dt != null) {
                dtName = dt.getName();
            }            

            for (AllianceMember am : ams) {
                gm.setTopic(u.getGameName() + " : " + ML.getMLStr(dtName, am.getUserId()) + "  " + ML.getMLStr("diplomacy_msg_accepted", am.getUserId()));
                String msg = ML.getMLStr("diplomacy_msg_auaccept", userId);
                msg = msg.replace("%USR%", u.getGameName());
                msg = msg.replace("%REL%", "<B>" + ML.getMLStr(dtName, userId) + "</B>");
                gm.setMsg(msg);
                gm.setMessageType(EMessageType.SYSTEM);
                gm.setMessageContentType(EMessageContentType.DIPLOMACY);
                gm.setDestinationUserId(am.getUserId());
                gm.writeMessageToUser();


            }

            //Abgelehnt
        } else {
            DiplomacyType dt;

            DiplomacyRelation existing = diplomacyRelationDAO.findBy(allianceId, userId, EDiplomacyRelationType.ALLIANCE_TO_USER, true);
            dt = diplomacyTypeDAO.findById(existing.getDiplomacyTypeId());

            diplomacyRelationDAO.remove(existing);
            existing = diplomacyRelationDAO.findBy(userId, allianceId, EDiplomacyRelationType.USER_TO_ALLIANCE, true);

            diplomacyRelationDAO.remove(existing);

            GenerateMessage gm = new GenerateMessage();
            User u = userDAO.findById(userId);
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());

            ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(allianceId);


            for (AllianceMember am : ams) {
                gm.setTopic(u.getGameName() + " : " + ML.getMLStr(dt.getName(), am.getUserId()) + "  " + ML.getMLStr("diplomacy_msg_decline", am.getUserId()));
                String msg = ML.getMLStr("diplomacy_msg_audecline", userId);
                msg = msg.replace("%USR%", u.getGameName());
                msg = msg.replace("%REL%", "<B>" + ML.getMLStr(dt.getName(), userId) + "</B>");
                gm.setMsg(msg);
                gm.setMessageType(EMessageType.SYSTEM);
                gm.setMessageContentType(EMessageContentType.DIPLOMACY);
                gm.setDestinationUserId(am.getUserId());
                gm.writeMessageToUser();


            }
        }
    }

    public static void updateAllianceRelation(Integer fromAllianceId, Integer toAllianceId, boolean accepted) {

        Alliance fromAlliance = allianceDAO.findById(fromAllianceId);
        Alliance toAlliance = allianceDAO.findById(toAllianceId);
        //Angenommen


        if (accepted) {
            DiplomacyType dt = null;
            ArrayList<DiplomacyRelation> existing = diplomacyRelationDAO.findBy(fromAllianceId, toAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);

            for (DiplomacyRelation dr : existing) {
                if (!dr.getPending()) {
                    diplomacyRelationDAO.remove(dr);
                } else if (dr.getPending()) {
                    dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                    if (dr.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                        dr.setPending(false);
                        dr.setAcknowledged(true);
                        dt = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
                        diplomacyRelationDAO.update(dr);
                    } else {
                        diplomacyRelationDAO.remove(dr);
                    }
                }
            }

            ArrayList<DiplomacyRelation> existing2 = diplomacyRelationDAO.findBy(toAllianceId, fromAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);

            for (DiplomacyRelation dr : existing2) {
                if (!dr.getPending()) {
                    diplomacyRelationDAO.remove(dr);
                } else if (dr.getPending()) {
                    if (dr.getDiplomacyTypeId() != DiplomacyType.NEUTRAL) {
                        dr.setPending(false);
                        dr.setAcknowledged(true);
                        diplomacyRelationDAO.update(dr);
                    } else {
                        diplomacyRelationDAO.remove(dr);
                    }
                }
            }

            String dtName = "?";
            if (dt != null) {
                dtName = dt.getName();
            }            
            
            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(fromAllianceId);
           
            for (AllianceMember am : ams) {
                gm.setTopic(toAlliance.getName() + " : " + ML.getMLStr(dtName, am.getUserId()) + "  " + ML.getMLStr("diplomacy_msg_accepted", am.getUserId()));
                String msg = ML.getMLStr("diplomacy_msg_aaaccept", am.getUserId());
                msg = msg.replace("%ALL%", toAlliance.getName());
                msg = msg.replace("%REL%", "<B>" + ML.getMLStr(dtName, am.getUserId()) + "</B>");
                gm.setMsg(msg);
                gm.setMessageType(EMessageType.SYSTEM);
                gm.setMessageContentType(EMessageContentType.DIPLOMACY);
                gm.setDestinationUserId(am.getUserId());
                gm.writeMessageToUser();
            }
            //Abgelehnt
        } else {
            DiplomacyType dt;
            DiplomacyRelation existing = diplomacyRelationDAO.findBy(fromAllianceId, toAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE, true);
            dt = diplomacyTypeDAO.findById(existing.getDiplomacyTypeId());
            diplomacyRelationDAO.remove(existing);
            existing = diplomacyRelationDAO.findBy(toAllianceId, fromAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE, true);
            diplomacyRelationDAO.remove(existing);
            GenerateMessage gm = new GenerateMessage();
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(fromAllianceId);

            for (AllianceMember am : ams) {
                gm.setTopic(toAlliance.getName() + " : " + ML.getMLStr(dt.getName(), am.getUserId()) + "  " + ML.getMLStr("diplomacy_msg_decline", am.getUserId()));
                String msg = ML.getMLStr("diplomacy_msg_aadecline", am.getUserId());
                msg = msg.replace("%ALL%", toAlliance.getName());
                msg = msg.replace("%REL%", "<B>" + ML.getMLStr(dt.getName(), am.getUserId()) + "</B>");
                gm.setMsg(msg);
                gm.setMessageType(EMessageType.SYSTEM);
                gm.setMessageContentType(EMessageContentType.DIPLOMACY);
                gm.setDestinationUserId(am.getUserId());
                gm.writeMessageToUser();
            }
        }
    }

    /**
     * @return the relationResult
     */
    public static RelationResult getRelationResult() {
        return relationResult;
    }

    /**
     * @param relationResultA the relationResult to set
     */
    public static void setRelationResult(RelationResult relationResultA) {
        relationResult = relationResultA;
    }

    public static UserData findUserDataByUserId(int userId) {
        return userDataDAO.findByUserId(userId);
    }

    public static String findHigherThan(DiplomacyRelation dr, int userId) {
        DiplomacyType diplomacyType = findDiplomacyTypeById(dr.getDiplomacyTypeId());
        DiplomacyType tmpDT = diplomacyType.clone();
        String result = " - ";

        int fromId = dr.getFromId();
        int toId = dr.getToId();

        DiplomacyRelation dRel = DiplomacyUtilities.getDiplomacyRel(fromId, toId);
        DiplomacyType dRelType = findDiplomacyTypeById(dRel.getDiplomacyTypeId());

        if (!dRelType.getWeight().equals(diplomacyType.getWeight())) {
            tmpDT = dRelType;
        }

        if (!diplomacyType.getId().equals(tmpDT.getId())) {
            result = ML.getMLStr(tmpDT.getName(), userId);
        }

        return result;
    }

    public static ArrayList<DiplomacyRelation> findRelationBy(int fromId, int toId, EDiplomacyRelationType type) {
        return diplomacyRelationDAO.findBy(fromId, toId, type);
    }

    public static ArrayList<DiplomacyRelation> findRelationByToId(int toId, EDiplomacyRelationType type) {
        return diplomacyRelationDAO.findByToId(toId, type);
    }

    public static ArrayList<DiplomacyRelation> findRelationByToId(int toId, EDiplomacyRelationType type, boolean pending) {
        return diplomacyRelationDAO.findByToId(toId, type, pending);
    }

    public static ArrayList<DiplomacyRelation> findRelationByFromId(int fromId, EDiplomacyRelationType type) {
        return diplomacyRelationDAO.findByFromId(fromId, type);
    }

    public static ArrayList<DiplomacyRelation> findRelationByFromId(int fromId, EDiplomacyRelationType type, boolean pending) {
        return diplomacyRelationDAO.findByFromId(fromId, type, pending);
    }

    public static DiplomacyRelation findRelationBy(int fromId, int toId, EDiplomacyRelationType type, boolean pending) {
        return diplomacyRelationDAO.findBy(fromId, toId, type, pending);
    }

    public static ArrayList<Integer> findAllys(int userId) {
        return AllianceUtilities.getAllAlliedUsers(userId);
    }

    public static ArrayList<Integer> getAllAlliedAllianceIds(int allianceId) {
        ArrayList<Integer> alliedAlliances = new ArrayList<Integer>();
        ArrayList<DiplomacyRelation> drList = diplomacyRelationDAO.findByFromId(allianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE, false);

        for (DiplomacyRelation dr : drList) {
            if (dr.getDiplomacyTypeId() != DiplomacyType.ALLY) {
                continue;
            }
            alliedAlliances.add(dr.getToId());
        }

        return alliedAlliances;
    }

    public static DiplomacyManagementView getDiplomacyManagementView(int userId) {
        DiplomacyManagementView dmv = new DiplomacyManagementView();

        // Check if user is in an Alliance
        AllianceMember am = allianceMemberDAO.findByUserId(userId);

        ArrayList<DiplomacyRelation> drListP2P = diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_USER);
        ArrayList<DiplomacyRelation> drListP2A = diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_ALLIANCE);



        ArrayList<DiplomacyRelation> drListA2P = new ArrayList<DiplomacyRelation>();
        ArrayList<DiplomacyRelation> drListA2A = new ArrayList<DiplomacyRelation>();

        if (am != null) {
            int userAllianceId = am.getAllianceId();
            drListA2P = diplomacyRelationDAO.findByFromId(userAllianceId, EDiplomacyRelationType.ALLIANCE_TO_USER);
            drListA2A = diplomacyRelationDAO.findByFromId(userAllianceId, EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE);
        }

        for (DiplomacyRelation dr : drListP2P) {
            // if (!dr.getAcknowledged()) continue;

            DiplomacyManagementViewEntry dmve = new DiplomacyManagementViewEntry(dr);
            dmv.addPlayerToPlayerEntry(dmve);
        }

        for (DiplomacyRelation dr : drListP2A) {
            // if (!dr.getAcknowledged()) continue;

            DiplomacyManagementViewEntry dmve = new DiplomacyManagementViewEntry(dr);
            dmv.addPlayerToAllianceEntry(dmve);
        }

        for (DiplomacyRelation dr : drListA2P) {
            // if (!dr.getAcknowledged()) continue;

            DiplomacyManagementViewEntry dmve = new DiplomacyManagementViewEntry(dr);
            dmv.addAllianceToPlayerEntry(dmve);
        }

        for (DiplomacyRelation dr : drListA2A) {
            // if (!dr.getAcknowledged()) continue;

            DiplomacyManagementViewEntry dmve = new DiplomacyManagementViewEntry(dr);
            dmv.addAllianceToAllianceEntry(dmve);
        }

        DiplomacyUtilities.calcDiplomacyOverrides(dmv);

        return dmv;
    }
    
    public static DiplomacyType getDiplomacyTypeForId(int id) {
        return diplomacyTypeDAO.findById(id);
    }
    
    public static DiplomacyVoteLinkResult getDiplomacyVoteLink(int userId, DiplomacyRelation dr) {        
        DiplomacyVoteLinkResult dvlr;
        
        try {
            int voteId = DiplomacyService.findCorrespondingVote(dr, userId);

            // Find corresponding message
            Message m = new Message();
            m.setRefType(EMessageRefType.VOTE);
            m.setRefId(voteId);
            ArrayList<Message> msgs = messageDAO.find(m);
            if (msgs.isEmpty()) {
                m = null;
            } else {
                m = msgs.get(0);
            }

            if (m == null) {
                dvlr = new DiplomacyVoteLinkResult(false,false,0);
            } else {
                // Check if user is allowed to vote on this:
                // If yes -> make the button clickable and directly redirect to vote 
                // If no -> show the button but not clickable
                boolean canVote = VoteUtilities.checkVotePermission(userId, voteId);
                if (canVote) {
                    dvlr = new DiplomacyVoteLinkResult(true,true,m.getMessageId());
                } else {
                    dvlr = new DiplomacyVoteLinkResult(true,false,m.getMessageId());
                }
            }
        } catch (Exception e) {
            DebugBuffer.error("Error in getDiplomacyVoteLink: ",e);
            dvlr = new DiplomacyVoteLinkResult(false,false,0);
        }
        
        return dvlr;
    }
}
