/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.notification;

/**
 *
 * @author Aion
 */
public interface INotification{


    abstract void generateMessage();
    abstract void persistMessage();

}
