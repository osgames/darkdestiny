/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view.menu.statusbar;

import at.darkdestiny.core.view.html.style;
import at.darkdestiny.core.view.html.table;

/**
 *
 * @author Bullet
 */
public interface IStatusBarEntity {

    @Deprecated
    abstract style addStyle(style rootNode);

    @Deprecated
    abstract table addTr(table table);
}
