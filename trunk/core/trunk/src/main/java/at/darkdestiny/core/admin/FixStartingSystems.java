/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.admin;

import at.darkdestiny.core.dao.*;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.User;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class FixStartingSystems {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    public static void fixStartingSystems() {
        // Gather all Starting Planets
        ArrayList<PlayerPlanet> ppList = new ArrayList<PlayerPlanet>();

        for (PlayerPlanet pp : ppDAO.findAll()) {
            if (pp.isHomeSystem()) ppList.add(pp);
        }

        // Now loop all homesystems and get MaxPop, Iron and Gas Giant Count
        for (PlayerPlanet pp : ppList) {
            Planet p = pDAO.findById(pp.getPlanetId());
            int initialPoints = 10;

            long maxPop = p.getMaxPopulation();
            if (maxPop > 9500000000l) {
                initialPoints -= 14;
            } else if (maxPop > 8500000000l) {
                initialPoints -= 6;
            }

            // Get System for looping other planets
            int systemId = p.getSystemId();
            ArrayList<Planet> pList = pDAO.findBySystemId(systemId);

            int gasGiantCount = 0;
            long ironRess = 0l;
            long addPop = 0l;

            for (Planet pTmp : pList) {
                if (pTmp.getLandType().equalsIgnoreCase("A") || pTmp.getLandType().equalsIgnoreCase("B")) {
                    gasGiantCount++;
                }

                ArrayList<PlanetRessource> prList = prDAO.findBy(pTmp.getId(), EPlanetRessourceType.PLANET);
                for (PlanetRessource pr : prList) {
                    if (pr.getRessId() == Ressource.PLANET_HOWALGONIUM) {
                        ironRess += pr.getQty();
                    }
                }

                if (pTmp.getLandType().equalsIgnoreCase("C") || pTmp.getLandType().equalsIgnoreCase("G")) {
                    addPop += pTmp.getMaxPopulation();
                }
            }

            initialPoints -= (int)Math.floor(addPop / 1000000000l) * 2;

            if (gasGiantCount == 3) initialPoints -= 6;
            if (gasGiantCount == 0) initialPoints += 4;

            // All necessary parameters have been loaded -- DebugPrint
            User u = uDAO.findById(pp.getUserId());
            java.lang.System.out.println("------- Debugging User " + u.getGameName());
            // java.lang.System.out.println("GasGiants: " + gasGiantCount);
            // java.lang.System.out.println("Howal: " + ironRess);

            if (gasGiantCount == 3) {
                long diff = 60000 - ironRess;
                if (Math.abs(diff) > 2000) {
                    // Rebuild howal deposits
                    System.out.println("Rebuilding is necessary!");
                    // Get first howal deposit and set to necessary amount
                    for (Planet pTmp : pList) {
                        ArrayList<PlanetRessource> prList = prDAO.findBy(pTmp.getId(), EPlanetRessourceType.PLANET);
                        for (PlanetRessource pr : prList) {
                            if (pr.getRessId() == Ressource.PLANET_HOWALGONIUM) {
                                pr.setQty(pr.getQty() + diff);
                                prDAO.update(pr);
                                continue;
                            }
                        }
                    }
                }

            } else if (gasGiantCount == 1) {
                long diff = 20000 - ironRess;

                if (Math.abs(diff) > 2000) {
                    // Rebuild howal deposits
                    System.out.println("Rebuilding is necessary");
                    for (Planet pTmp : pList) {
                        ArrayList<PlanetRessource> prList = prDAO.findBy(pTmp.getId(), EPlanetRessourceType.PLANET);
                        for (PlanetRessource pr : prList) {
                            if (pr.getRessId() == Ressource.PLANET_HOWALGONIUM) {
                                pr.setQty(pr.getQty() + diff);
                                prDAO.update(pr);
                                continue;
                            }
                        }
                    }
                }
            }
        }
    }
}
