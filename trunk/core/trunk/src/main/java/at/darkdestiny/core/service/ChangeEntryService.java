/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.model.ChangeEntry;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author Dreloc
 */
public class ChangeEntryService extends Service {

    public static void addChange(Map<String, String[]> params) {
        String text = params.get("change")[0];
        ChangeEntry ce = new ChangeEntry();
        ce.setDate(System.currentTimeMillis());
        ce.setText(text);
        changeEntryDAO.add(ce);

        if (changeEntryDAO.findAll().size() > 0) {
            generateFile();
        }


    }

    public static void deleteChange(Map<String, String[]> params) {
        Integer id = Integer.parseInt(params.get("id")[0]);
        ChangeEntry ce = new ChangeEntry();
        ce.setId(id);
        ce = (ChangeEntry) changeEntryDAO.get(ce);
        changeEntryDAO.remove(ce);
        if (changeEntryDAO.findAll().size() > 0) {
            generateFile();
        }
    }

    private static void generateFile() {

        try {
            File htmlFile = new File("/webs/web495/html/homepage/subSites/ChangeLogShort.htm");
            ChangeEntry ce = changeEntryDAO.findAllSorted().get(0);
            java.util.Date msgDate = new java.util.Date(ce.getDate());
            String msgDateString = java.text.DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.GERMAN).format(msgDate) + " " + java.text.DateFormat.getTimeInstance(DateFormat.SHORT, Locale.GERMAN).format(msgDate);

            FileWriter f1 = new FileWriter(htmlFile);
            String htmlString = "";
            htmlString += "<HTML>";
            htmlString += "<HEAD> \n \t<META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>\n<META http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />\n</HEAD>\n";
            htmlString += "<BODY text='White' style='background-image:url(images/space.jpg); background-attachment:fixed;'>";
            htmlString += "<TABLE style='border-spacing:0px; width:120px; height:80px;table-layout:fixed; word-wrap:break-word; background-repeat: repeat; font-size:13px; color:white;' align='center' cellpadding='0' cellspacing='0'>";
            htmlString += "<TR>";
            htmlString += "<TD  align='center'>";
            htmlString += "<B>" + FormatUtilities.toISO("Letzte �nderung") + "</B>";
            htmlString += "</TD>";
            htmlString += "</TR>";
            htmlString += "<TABLE style='border-style:solid; border-width:thin; border-color:#330099; border-spacing:0px; background-image: url(http://www.thedarkdestiny.at/tableBgd.png); background-repeat: repeat; font-size:13px; color:white;' align='center' cellpadding='0' cellspacing='0'>";
            htmlString += "<TR>";
            htmlString += "<TD  align='center' style='background-color:#3366FF;'>";
            htmlString += "<B>" + FormatUtilities.toISO(msgDateString) + "</B>";
            htmlString += "</TD>";
            htmlString += "</TR>";
            htmlString += "<TR>";
            htmlString += "<TD>";
            htmlString += FormatUtilities.toISO(ce.getText());
            htmlString += "</TD>";
            htmlString += "</TR>";
            htmlString += "<TR>";
            htmlString += "<TD align='center'>";
            htmlString += "<A target='rmid' href='http://www.thedarkdestiny.at/homepage/subSites/ChangeLog.htm'>anzeigen</A>";
            htmlString += "</TD>";
            htmlString += "</TR>";
            htmlString += "</TABLE>";
            htmlString += "</BODY>";
            htmlString += "</HTML>";
            f1.write(htmlString);
            f1.close();

        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, e.getMessage());
            e.printStackTrace();
        }
        try {
            File htmlFile = new File("/webs/web495/html/homepage/subSites/ChangeLog.htm");

            FileWriter f1 = new FileWriter(htmlFile);
            String htmlString = "";
            htmlString += "<HTML>";
            htmlString += "<HEAD> \n \t<META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>\n<META http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />\n</HEAD>\n";
            htmlString += "<BODY text='White' style='background-image:url(images/RightMidBackground.jpg); background-attachment:fixed;'>";
            htmlString += "<CENTER><H2>ChangeLog</H2></CENTER><BR><BR>";
            String warning = ""
                    + "<TABLE width='80%' align='center' style='font-size:13px; color:white;'><TR><TD align='left'>Bitte Beachten Sie, dass nicht alle �nderungen wenn sie hier aufscheinen auch bereits auf dem Server umgesetzt sind</TD></TR></TABLE>";
            htmlString += FormatUtilities.toISO(warning);
            htmlString += "<TABLE width='80%' style='border-style:solid; border-width:thin; border-color:#330099; border-spacing:0px; background-image: url(http://www.thedarkdestiny.at/tableBgd.png); background-repeat: repeat; font-size:13px; color:white;' align='center' cellpadding='0' cellspacing='0'>";
            htmlString += "<TR style='background-color:#3366FF;'>";
            htmlString += "<TD align='center' width='150px'>";
            htmlString += "<B>Datum</B>";
            htmlString += "</TD>";
            htmlString += "<TD align='center'>";
            htmlString += "<B>&Auml;nderung</B>";
            htmlString += "</TD>";
            htmlString += "</TR>";
            for (ChangeEntry ce : changeEntryDAO.findAllSorted()) {
                java.util.Date msgDate = new java.util.Date(ce.getDate());
                String msgDateString = java.text.DateFormat.getDateInstance(DateFormat.LONG, Locale.GERMAN).format(msgDate) + " " + java.text.DateFormat.getTimeInstance(DateFormat.SHORT, Locale.GERMAN).format(msgDate);
                htmlString += "<TR>";
                htmlString += "<TD align='left'>";
                htmlString += FormatUtilities.toISO(msgDateString);
                htmlString += "</TD>";
                htmlString += "<TD align='left'>";
                htmlString += FormatUtilities.toISO(ce.getText());
                htmlString += "</TD>";
                htmlString += "</TR>";
            }
            htmlString += "</TABLE>";
            htmlString += "</BODY>";
            htmlString += "</HTML>";
            f1.write(htmlString);
            f1.close();
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, e.getMessage());
            e.printStackTrace();
        }
    }
}
