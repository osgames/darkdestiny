/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin.module;

import at.darkdestiny.framework.access.DbConnect;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Stefan
 */
public class ModuleAttributeRel {
    private int id;
    private int moduleId;
    private int chassisId;
    private int researchId;
    private int attributeId;
    private String attributeName;
    private AttributeType attributeType;
    private double value;
    private int refId;
    private ReferenceType refType;
    private int appliesTo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public AdminChassis getChassis() {
        AdminChassis ac = new AdminChassis();
        ac.setId(0);
        ac.setName("ALL");
        
        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name FROM chassis WHERE id="+chassisId);
            while (rs.next()) {
                ac = new AdminChassis();
                ac.setId(rs.getInt(1));
                ac.setName(rs.getString(2));                
            }

            stmt.close();
        } catch (Exception e) {
        }        
        
        return ac;
    }

    public void setChassisId(int chassisId) {
        this.chassisId = chassisId;
    }

    public AdminResearch getResearch() {
        AdminResearch ar = new AdminResearch();
        ar.setName("Nothing");
        
        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name FROM research WHERE id="+researchId);
            while (rs.next()) {
                ar = new AdminResearch();
                ar.setId(rs.getInt(1));
                ar.setName(rs.getString(2));
            }

            stmt.close();
        } catch (Exception e) {
        }        
        
        return ar;
    }

    public void setResearchId(int researchId) {
        this.researchId = researchId;
    }

    public int getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(int attributeId) {
        this.attributeId = attributeId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getRefId() {
        return refId;
    }

    public void setRefId(int refId) {
        this.refId = refId;
    }

    public ReferenceType getRefType() {
        return refType;
    }

    public void setRefType(ReferenceType refType) {
        this.refType = refType;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(AttributeType attributeType) {
        this.attributeType = attributeType;
    }

    public int getAppliesTo() {
        return appliesTo;
    }

    public void setAppliesTo(int appliesTo) {
        this.appliesTo = appliesTo;
    }
}
