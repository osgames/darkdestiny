/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class BalancingResult {
    
    
    private boolean actionTaken = false;
    private ArrayList<String> actionsTaken = new ArrayList<String>();

    /**
     * @return the actionTaken
     */
    public boolean isActionTaken() {
        return actionTaken;
    }

    /**
     * @return the actionsTaken
     */
    public ArrayList<String> getActionsTaken() {
        return actionsTaken;
    }

    /**
     * @param actionTaken the actionTaken to set
     */
    public void setActionTaken(boolean actionTaken) {
        this.actionTaken = actionTaken;
    }
    
    public void addActionTaken(String action){
        actionsTaken.add(action);
    }
}
