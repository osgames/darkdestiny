/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.ModuleType;
import at.darkdestiny.core.WeaponModule;
import at.darkdestiny.core.admin.module.AttributeTree;
import at.darkdestiny.core.admin.module.ModuleAttributeResult;
import at.darkdestiny.core.dao.ChassisDAO;
import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.ConstructionModuleDAO;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.dao.ShipTypeAdjustmentDAO;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.ConstructionModule;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.spacecombat.helper.ShieldDefenseDetail;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Aion
 */
public class DefenseBuilding extends AbstractDefenseBuilding implements IDefenseBuilding {

    private static final Logger log = LoggerFactory.getLogger(DefenseBuilding.class);

    private static ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private static ChassisDAO chDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    public static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    public static ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);
    private static ConstructionModuleDAO cmDAO = (ConstructionModuleDAO) DAOFactory.get(ConstructionModuleDAO.class);
    
    private final Construction c;
    private final Chassis ch;
    private final int userId;
    
    public DefenseBuilding(int designId, int count, int userId) {
        super(designId, count);

        c = cDAO.findById(designId);
        ch = chDAO.findById(7);

        this.userId = userId;
        this.name = c.getName();
        this.count = count;
        this.chassisSize = 7;
        this.designId = designId;
        // this.purpose = sd.getType(); ?

        if ((c.getId() == Construction.ID_PLANETARYLASERCANNON) ||
            (c.getId() == Construction.ID_SUNTRANSMITTER_FORTRESS) ||
            (c.getId() == Construction.ID_PLANETARY_FORTRESS)) {
            this.primaryTarget = 5;
        } else if (c.getId() == Construction.ID_PLANETARYMISSLEBASE) {
            this.primaryTarget = 2;
        } else if (c.getId() == Construction.ID_PLANETARY_INTERVALCANNON) {
            this.primaryTarget = 5;
        }

        this.restCount = this.orgCount;

        initializeAllModules();
    }

    private void initializeAllModules() {
        int groupingMultiplier = 1;

        // ShipTypeAdjustment sta = null; // staDAO.findByType(sd.getType());
        AttributeTree at = new AttributeTree(userId,0l);
        
        // Check trough modules
        ArrayList<WeaponModule> weaponArray = new ArrayList<WeaponModule>();
        int weaponEnergyConsumption = 0;
        int shieldEnergyConsumption = 0;
        int internalEnergyConsumption = 0;
        int armorValue = 0;
        int shieldHpPS = 0;
        int shieldHpHU = 0;
        int shieldHpPA = 0;
        int energyPS = 0;
        int energyHU = 0;
        int energyPA = 0;

        setHp(c.getHp());

        HashMap<Integer, Integer> modules = new HashMap<Integer, Integer>();
        ArrayList<ConstructionModule> cmList = null;
        
        if (c.getId() != Construction.ID_PLANETARY_FORTRESS) {
            cmList = cmDAO.findByConstruction(designId);
            for (ConstructionModule cm : cmList) {
                modules.put(cm.getModuleId(), cm.getCount());
            }
        } else {
            // Determine Modules by current techlevel
            DebugBuffer.trace("Determining modules for planetary defense dynamically ...");
            loadModulesDynamically(c.getId(), userId, modules);
            for (Map.Entry<Integer,Integer> entry : modules.entrySet()) {
                DebugBuffer.trace("Loaded " + entry.getValue() + " modules for id " + entry.getKey());
            }            
        }

        //  rs = stmt.executeQuery("SELECT m.name, m.type, m.value, m.energy, m.chassisSize, m.isConstruct, dm.count, m.id FROM designmodule dm, module m WHERE dm.designId=" + designId + " AND m.id=dm.moduleId");

        int totalRange = 0;
        int weaponCount = 0;
        int maxTargetAllocation = 0;
        int fireSpeed = 0;

        for (Map.Entry<Integer, Integer> moduleEntry : modules.entrySet()) {
            Module m = mDAO.findById(moduleEntry.getKey());
            int modCount = moduleEntry.getValue();

            ModuleType type = m.getType();

            DebugBuffer.addLine(DebugLevel.DEBUG, "Check moduel " + m.getName() + " with id " + moduleEntry.getKey() + " and type " + m.getType());
            DebugBuffer.addLine(DebugLevel.DEBUG, "Type of corresponding moduleObjekt is " + type);
            if (type == ModuleType.ARMOR) {
                switch (moduleEntry.getKey()) {
                    case (2):
                        armorType = 1;
                        break;
                    case (21):
                        armorType = 2;
                        break;
                    case (22):
                        armorType = 3;
                        break;
                    case (23):
                        armorType = 4;
                        break;
                }
            } else if (type == ModuleType.WEAPON) {
                WeaponModule wm = new WeaponModule();
                Module mod = mDAO.findById(moduleEntry.getKey());

                ModuleAttributeResult mar = at.getAllAttributes(7, m.getId());

                wm.setWeaponId(m.getId());
                wm.setMultifire((int) mar.getAttributeValue(EAttribute.MULTIFIRE));
                wm.setMultifired(0);

                fireSpeed += mar.getAttributeValue(EAttribute.MULTIFIRE) * modCount;

                maxTargetAllocation += wm.getMultifire() * modCount;

                wm.setRange((int) mar.getAttributeValue(EAttribute.RANGE));

                totalRange += wm.getRange() * modCount;
                weaponCount += modCount;

                DebugBuffer.addLine(DebugLevel.DEBUG, "Multifire = " + wm.getMultifire() + " for weapon " + m.getName());

                wm.setAttackPower((int) ((float) mar.getAttributeValue(EAttribute.ATTACK_STRENGTH) / wm.getMultifire()) * groupingMultiplier);
                wm.setAccuracy((float) mar.getAttributeValue(EAttribute.ACCURACY));
                wm.setCount(modCount * wm.getMultifire());
                wm.setTotalCount(modCount * count * wm.getMultifire());
                wm.setEnergyConsumption(0);
                weaponEnergyConsumption += wm.getEnergyConsumption() * wm.getCount();
                // DebugBuffer.addLine("Add weapons to " + bs.getName());

                weaponArray.add(wm);
            } else if (type == ModuleType.SHIELD) {
                // TODO
                // Shield s = null; // sdd.getShield(moduleEntry.getKey());
                ModuleAttributeResult mar = at.getAllAttributes(6, m.getId());
                
                if (mar != null) {
                    switch (moduleEntry.getKey()) {
                        case (50):
                            shieldEnergyConsumption += (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * modCount * groupingMultiplier);
                            shieldHpPS += (int) ((float) mar.getAttributeValue(EAttribute.DEFENSE_STRENGTH) * (float) modCount) * groupingMultiplier;
                            energyPS += (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * modCount);
                            break;
                        case (51):
                            shieldEnergyConsumption += (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * modCount * groupingMultiplier);
                            shieldHpHU += (int) ((float) mar.getAttributeValue(EAttribute.DEFENSE_STRENGTH) * (float) modCount) * groupingMultiplier;
                            energyHU += (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * modCount);
                            break;
                        case (52):
                            shieldEnergyConsumption += (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * modCount) * groupingMultiplier;
                            shieldHpPA += (int) ((float) mar.getAttributeValue(EAttribute.DEFENSE_STRENGTH) * (float) modCount) * groupingMultiplier;
                            energyPA += (mar.getAttributeValue(EAttribute.ENERGY_CONSUMPTION) * modCount);
                            break;
                    }
                }
            } else if (false) { // 9??
                // FREE
            }
        }

        // setMaxTargetAllocation(maxTargetAllocation);
        weapons = weaponArray;
        // setAverageRange((float) totalRange / weaponCount);
        // setFireSpeed(fireSpeed);
        // setSizeLevel((float) Math.log10(sdd.getBase().getStructure()));
        // bs.setHp(bs.getHp() + (bs.getHp() / 100 * armorValue));

        ShieldDefinition sDef = new ShieldDefinition();

        sDef.setShield_PS(shieldHpPS);
        sDef.setShield_HU(shieldHpHU);
        sDef.setShield_PA(shieldHpPA);
        sDef.setSharedShield_PS(sDef.getShield_PS() * count);
        sDef.setSharedShield_HU(sDef.getShield_HU() * count);
        sDef.setSharedShield_PA(sDef.getShield_PA() * count);
        sDef.setEnergyNeed_PS(energyPS);
        sDef.setEnergyNeed_HU(energyHU);
        sDef.setEnergyNeed_PA(energyPA);

        totalHp = c.getHp() * count;

        sDef.setShieldEnergyPerShip(shieldEnergyConsumption);
        sDef.setShieldEnergy(shieldEnergyConsumption * count);

        // shields = sDef;
        setShields(sDef);
    }

    @Override
    public String getName() {
        return c.getName();
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    protected double getReductionThroughShieldsNew(AbstractCombatUnit abs, AbstractCombatUnit absAtt, WeaponModule wm, int maxHitable, HashMap<Integer, Double> detailDamageAtt, ShieldDefinition shields, ShieldDefenseDetail sdd) {
        double reduction = 0d;
        double damageAttacker = detailDamageAtt.get(wm.getWeaponId());
        double damageAttOrg = damageAttacker;

        HashMap<Double, Double> weightedReductionMap = new HashMap<Double, Double>();

        float psShieldStr = shields.getSharedShield_PS();
        float huShieldStr = shields.getSharedShield_HU();
        float paShieldStr = shields.getSharedShield_PA();

        log.debug("["+abs.getName()+"] PA Value is " + shields.getShield_PA() + " Shared Shield Value is " + shields.getSharedShield_PA());
        log.debug("["+abs.getName()+"] HU Value is " + shields.getShield_HU() + " Shared Shield Value is " + shields.getSharedShield_HU());

        double relShieldStrPA = paShieldStr;
        double relShieldStrHU = huShieldStr;
        double relShieldStrPS = psShieldStr;

        // System.out.println("RelPS: " + relShieldStrPS);
        double currReduction = 0d;

        double absorbPS = 1d * Math.min(1d,((relShieldStrPS * 1.2d) / shields.getShield_PS()) * abs.prallPenetration.get(wm.getWeaponId())); // abs.prallPenetration.get(wm.getWeaponId());
        double absorbHU = 1d * Math.min(1d,((relShieldStrHU * 1.2d) / shields.getShield_HU()) * abs.huPenetration.get(wm.getWeaponId())); // abs.huPenetration.get(wm.getWeaponId());
        double absorbPA = 1d * Math.min(1d,((relShieldStrPA * 1.2d) / shields.getShield_PA()) * abs.paraPenetration.get(wm.getWeaponId())); // abs.paraPenetration.get(wm.getWeaponId());

        double wastedDamage = 0d;

        if (paShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbPA;

            if (absorbedDamage > relShieldStrPA) {
                // abs.sc_LoggingEntry.incPA_DamagePenetrated(absorbedDamage - relShieldStrPA);
                absorbedDamage = relShieldStrPA;
            }

            // abs.sc_LoggingEntry.incPA_DamageAbsorbed(absorbedDamage);
            log.debug("[PA] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrPA + " (Abs. Damage: " + absorbedDamage + " / absorbPA: " + absorbPA + ")");


            relShieldStrPA -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsPA(sdd.getAbsPA() + damageAttacker);
                shields.setSharedShield_PA((float) shields.getSharedShield_PA() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, currReduction + adjust * absorbPA);
                log.debug("CurrReduction: " + currReduction + " Adjust: " + adjust);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                shields.setSharedShield_PA((float) shields.getSharedShield_PA() - effAbsorbedDamage);
                // sdd.setAbsPA(sdd.getAbsPA() + effAbsorbedDamage);
                sdd.setAbsPA(sdd.getAbsPA() + absorbedDamage);
                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenPA(sdd.getPenPA() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // abs.sc_LoggingEntry.incPA_DamagePenetrated(damageAttacker);
            }
        }

        if (huShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbHU;

            if (absorbedDamage > relShieldStrHU) {
                // abs.sc_LoggingEntry.incHU_DamagePenetrated(absorbedDamage - relShieldStrHU);
                absorbedDamage = relShieldStrHU;
            }

            log.debug("[HU] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrHU + " (Abs. Damage: " + absorbedDamage + " / absorbHU: " + absorbHU + ")");


            // abs.sc_LoggingEntry.incHU_DamageAbsorbed(absorbedDamage);
            relShieldStrHU -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsHU(sdd.getAbsHU() + damageAttacker);
                shields.setSharedShield_HU((float) shields.getSharedShield_HU() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, currReduction + adjust * absorbHU);
                // currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbHU);
                log.debug("CurrReduction: " + currReduction + " Adjust: " + adjust);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                shields.setSharedShield_HU((float) shields.getSharedShield_HU() - effAbsorbedDamage);
                // sdd.setAbsHU(sdd.getAbsHU() + effAbsorbedDamage);
                sdd.setAbsHU(sdd.getAbsHU() + absorbedDamage);
                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenHU(sdd.getPenHU() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // sdd.setPenHU(sdd.getPenHU() + damageAttacker);

                // abs.sc_LoggingEntry.incHU_DamagePenetrated(damageAttacker);
            }
        }

        
        if (psShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbPS;

            if (absorbedDamage > relShieldStrPS) {
                // abs.sc_LoggingEntry.incPS_DamagePenetrated(absorbedDamage - relShieldStrPS);
                absorbedDamage = relShieldStrPS;
            }

            // abs.sc_LoggingEntry.incPS_DamageAbsorbed(absorbedDamage);
            relShieldStrPS -= absorbedDamage;
            double adjust = 1d / damageAttOrg * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsPS(sdd.getAbsPS() + damageAttacker);
                shields.setSharedShield_PS((float) shields.getSharedShield_PS() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, adjust * absorbHU);
                // currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbPS);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                shields.setSharedShield_PS((float) shields.getSharedShield_PS() - effAbsorbedDamage);
                // sdd.setAbsPS(sdd.getAbsPS() + effAbsorbedDamage);
                sdd.setAbsPS(sdd.getAbsPS() + absorbedDamage);
                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(effAbsorbedDamage);
                sdd.setPenPS(sdd.getPenPS() + (damageAttacker - absorbedDamage));
                damageAttacker -= effAbsorbedDamage;
                // sdd.setPenPS(sdd.getPenPS() + damageAttacker);
                // abs.sc_LoggingEntry.incPS_DamagePenetrated(damageAttacker);
            }
        }
        
        System.out.println("---------------------------------------- END ---------------------------------------");

        return currReduction;
    }

    public int getArmor() {
        return getArmorType();
    }

    @Override
    public double getDistraction() {
        return 0d;
    }

    @Override
    public int[] getDamageDistribution(int count, double temp_destroyed) {
        temp_destroyed = Math.min(temp_destroyed, 1d);

        int[] distribution = new int[6];
        if (temp_destroyed >= 1d) {
            distribution[5] = count;
        } else {
            int destroyed = (int) Math.floor(count * temp_destroyed);
            int survivor = count - destroyed;

            distribution[0] = survivor;
            distribution[5] = destroyed;
        }

        for (int i = 0; i < 5; i++) {
            this.damageDistribution[i] = distribution[i];
        }

        return distribution;
    }

    @Override
    public void setPlanetaryShields(ShieldDefinition sDef) {
        log.debug("["+this.getName()+"] Set PA shield to " + sDef.getShield_PA());
        log.debug("["+this.getName()+"] Set HU shield to " + sDef.getShield_HU());

        super.setPlanetaryShields(sDef);
    }    
    
    @Override
    public void setShields(ShieldDefinition sDef) {
        log.debug("["+this.getName()+"] Set PA shield to " + sDef.getShield_PA());
        log.debug("["+this.getName()+"] Set HU shield to " + sDef.getShield_HU());

        super.setShields(sDef);
    }

    /**
     * @return the c
     */
    public Construction getConstruction() {
        return c;
    }
}
