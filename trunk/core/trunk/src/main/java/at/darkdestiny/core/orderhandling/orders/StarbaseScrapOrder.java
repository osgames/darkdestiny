/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.Buildable;

/**
 *
 * @author Stefan
 */
public class StarbaseScrapOrder extends ScrapOrder {
    public StarbaseScrapOrder(Buildable unit, int planetId, int userId, int count) {
        super(unit,planetId,userId,count);
    }
}
