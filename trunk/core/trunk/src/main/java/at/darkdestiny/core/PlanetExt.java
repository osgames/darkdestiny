/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.EOwner;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ViewTable;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class PlanetExt {

    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private final int planetId;
    private final Planet planet;
    private final PlayerPlanet playerPlanet;
    private ViewTable vtEntry = null;
    private EOwner ownerAttitude;

    public PlanetExt(int planetId) {
        this.planetId = planetId;
        planet = pDAO.findById(planetId);
        playerPlanet = ppDAO.findByPlanetId(planetId);
    }

    public PlanetExt(Planet p) {
        this.planet = p;
        this.planetId = planet.getId();
        playerPlanet = ppDAO.findByPlanetId(planetId);
    }

    public EOwner getOwnerAttitude(int userId) {
        if (playerPlanet == null ||  getVtEntry() == null || vtEntry.getUserId() == 0) {
            return EOwner.INHABITATED;
        }
        if (playerPlanet.getUserId() == userId) {
            return EOwner.OWN;
        }

        return DiplomacyUtilities.getDiplomacyResult(userId, playerPlanet.getUserId()).getOwner();
    }

    public void setViewTable(ViewTable vt) {
        this.vtEntry = vt;
    }

    public String getSearchPageDisplayStr(int userId) {
        EOwner owner = getOwnerAttitude(userId);
        if (owner == EOwner.OWN) {
            return "<B><FONT color='#00ff00'>" + planet.getLandType() + "</FONT></B>";
        } else if (owner == EOwner.INHABITATED) {
            return "<B>" + planet.getLandType() + "</B>";
        } else {
            String color = DiplomacyUtilities.getColor(owner);
            return "<B><FONT color='" + color + "'>" + planet.getLandType() + "</FONT></B>";
        }
    }

    public Planet getBase() {
        return planet;
    }

    /**
     * @return the vtEntry
     */
    public ViewTable getVtEntry() {
        return vtEntry;
    }

}
