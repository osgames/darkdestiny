/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat.helper;

 import at.darkdestiny.core.spacecombat.AbstractCombatUnit;import at.darkdestiny.core.spacecombat.ICombatUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.spacecombat.AbstractCombatUnit;
import at.darkdestiny.core.spacecombat.ICombatUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Aion
 */
public class TargetListController {

    private static final Logger log = LoggerFactory.getLogger(TargetListController.class);

    private HashMap<ICombatUnit,ArrayList<TargetListEntry>> targetLists = new HashMap<ICombatUnit,ArrayList<TargetListEntry>>();

    public void register(ICombatUnit icu) {
        log.debug("[TLC] Register " + icu.getName());
        targetLists.put(icu, new ArrayList<TargetListEntry>());
    }

    public ArrayList<TargetListEntry> getList(ICombatUnit icu) {
        log.debug("[TLC] Get List for " + icu.getName() + " Result: " + targetLists.get(icu));
        return targetLists.get(icu);
    }

    public void addTarget(ICombatUnit icu, ICombatUnit target, double probability, double targetFireIndicator) {
        log.debug("[TLC] Add " + target.getName() + " for " + icu.getName());

        TargetListEntry tle = new TargetListEntry();
        tle.setTarget(target);
        tle.setBaseAttackProbability(probability);
        tle.setRelativeAttackProbability(probability);
        tle.setTargetFireIndicator(targetFireIndicator);

        targetLists.get(icu).add(tle);
    }

    public void updateTargetLists(ICombatUnit icu) {
        for (Iterator tleIt = targetLists.get(icu).iterator();tleIt.hasNext();) {
            TargetListEntry tle = (TargetListEntry)tleIt.next();
            // log.debug("["+tle+"] Looping a target: " + ((BattleShipNew)tle.getTarget()).getName());

            AbstractCombatUnit acu = (AbstractCombatUnit)tle.getTarget();
            log.debug("[TLC] Set new target value on " + acu.getName() + " for " + icu.getName());

            if (acu.getParentCombatGroupFleet().isRetreated() ||
                acu.getParentCombatGroupFleet().isResigned()) {
                tleIt.remove();
                log.debug("Target was a coward");
                continue;
            }

            if (acu.getTempDestroyed() >= 1d) {
                tleIt.remove();
                log.debug("Target?? Which target?");
                continue;
            } else {
                log.debug("Setting a value: " + (acu.getCount() * (Math.max(0d,1d - acu.getTempDestroyed()))));
                // tle.setRelativeAttackProbability(acu.getCount() * (Math.max(0d,1d - acu.getTempDestroyed())));
                tle.setRelativeAttackProbability(tle.getBaseAttackProbability() * (Math.max(0d,1d - acu.getTempDestroyed())));
            }
        }
    }
}
