/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.GTRelation;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class GTRelationDAO extends ReadOnlyTable<GTRelation> implements GenericDAO {

    public ArrayList<GTRelation> findBySrcId(Integer srcId) {
        GTRelation gtr = new GTRelation();
        gtr.setSrcId(srcId);
        return find(gtr);
    }

    public ArrayList<GTRelation> findBy(Integer srcId, Integer tgtId) {
        GTRelation gtr = new GTRelation();
        gtr.setSrcId(srcId);
        gtr.setTargetId(tgtId);
        return find(gtr);
    }

    public float findTotalAttackprob(Integer srcId) {
        GTRelation gtr = new GTRelation();
        gtr.setSrcId(srcId);
        float total = 0f;
        ArrayList<GTRelation> result = find(gtr);
        for (GTRelation gtrel : result) {
            total += gtrel.getAttackProbab();
        }
        return total;

    }   
    
    //Fake an entry with low damage if no connection between the unit types exist
    public ArrayList<GTRelation> findBySrcIdFake(Integer troopId) {
        ArrayList<GTRelation> result = new ArrayList<GTRelation>();
        ArrayList<GroundTroop> troops = Service.groundTroopDAO.findAll();
        GroundTroop attackingTroop = Service.groundTroopDAO.findById(troopId);
        for (GroundTroop gt : troops) {
            GTRelation gtr = new GTRelation();
            ArrayList<GTRelation> tmp = findBy(troopId, gt.getId());
            //Found no connection
            if (tmp == null || tmp.isEmpty()) {

                        gtr.setSrcId(troopId);
                        gtr.setTargetId(gt.getId());
                        gtr.setAttackPoints((float)gt.getHitpoints() * attackingTroop.getHitpoints() / 1000f);
                        gtr.setAttackProbab((float)gt.getHitpoints() * attackingTroop.getHitpoints() / 1000f);
                        gtr.setHitProbab((float)gt.getHitpoints() * attackingTroop.getHitpoints() / 1000f);
                                 gtr.setFactor(1f);
            } else {
                boolean first = true;
                for (GTRelation gtrTmp : tmp) {
                    if (first) {
                        gtr.setSrcId(troopId);
                        gtr.setTargetId(gtrTmp.getTargetId());
                        gtr.setAttackPoints(gtrTmp.getAttackPoints());
                        gtr.setAttackProbab(gtrTmp.getAttackProbab());
                        gtr.setHitProbab(gtrTmp.getHitProbab());
                        gtr.setFactor(gtrTmp.getFactor());
                        first = false;
                    } else {
                        gtr.setAttackPoints(gtr.getAttackPoints() + gtrTmp.getAttackPoints());
                        gtr.setAttackProbab(gtr.getAttackProbab() + gtrTmp.getAttackProbab());
                        gtr.setHitProbab(gtr.getHitProbab() + gtrTmp.getHitProbab());
                        gtr.setFactor(gtr.getFactor() + gtrTmp.getFactor());
                    }

                }
            }
            result.add(gtr);
        }


        return result;
    }
}
