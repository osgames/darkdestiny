/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "territorymappoint")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TerritoryMapPoint extends Model<TerritoryMapPoint> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    private Integer id;
    @IndexAnnotation(indexName = "territoryId")
    @FieldMappingAnnotation("territoryId")
    @IdFieldAnnotation
    private Integer territoryId;
    @FieldMappingAnnotation("x")
    private Integer x;
    @FieldMappingAnnotation("y")
    private Integer y;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the territoryId
     */
    public Integer getTerritoryId() {
        return territoryId;
    }

    /**
     * @param territoryId the territoryId to set
     */
    public void setTerritoryId(Integer territoryId) {
        this.territoryId = territoryId;
    }

    /**
     * @return the x
     */
    public Integer getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(Integer x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public Integer getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(Integer y) {
        this.y = y;
    }
}
