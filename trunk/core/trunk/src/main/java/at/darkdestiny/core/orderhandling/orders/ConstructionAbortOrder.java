/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.model.ProductionOrder;

/**
 *
 * @author Stefan
 */
public class ConstructionAbortOrder extends AbortOrder {
    private Buildable unit;
    public ConstructionAbortOrder(ProductionOrder po, Buildable unit, int count, int userId, int planetId) {
        super(po, count, userId, planetId);
        this.unit = unit;
    }

    /**
     * @return the unit
     */
    public Buildable getUnit() {
        return unit;
    }
}
