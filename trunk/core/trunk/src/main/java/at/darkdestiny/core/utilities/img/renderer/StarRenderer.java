/**
 * 
 */
package at.darkdestiny.core.utilities.img.renderer;

import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * Zur Anzeige eines ganzen Systems 
 * als Stern mit einem Kreis und passenden Informationen dazu
 * 
 * @author martin
 */
public class StarRenderer implements IModifyImageFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8324669919417845346L;
	private static final int IMG_RAD = 10;
	private final float x;
	private final float y;
	private final int sysStatus;
	private final float visibleSize;
	private int width;
	private int height;
	private final String name;

	/**
	 * 
	 */
	public StarRenderer(String name, float x, float y, float visibleSize, int sysStatus) 
	{
		this.name = name;
		this.x = x;
		this.y = y;
		this.visibleSize = visibleSize;
		this.sysStatus = sysStatus;
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.img.IModifyImageFunction#run(java.awt.Graphics)
	 */
	public void run(Graphics graphics) {
			Image img = Toolkit.getDefaultToolkit().createImage(this.getClass().getResource(filenameForSysStatus(sysStatus)));
			int i = 0;
			while ((img.getWidth(null) <0) && (i < 10))
				try {
					Thread.sleep(10);
					i++;
				} catch (InterruptedException e) {
				}
			graphics.drawImage(img, Math.round(x*width/visibleSize) + width/2- img.getWidth(null)/2, 
					Math.round(y*height/visibleSize) + height/2- img.getHeight(null)/2, null);
	}

	private String filenameForSysStatus(int idx) {
        switch(idx){
        
        case (-2): return "mainstar.gif"; // Hauptsysteme
        case (-1): return "graystar.gif";            
        case  (0): return "star_000.gif";
        case  (1): return "star_001.gif"; //Eigene
        case  (2): return "star_010.gif"; //Alliierte
        case  (3): return "star_011.gif";
        case  (4): return "star_100.gif"; //Feindlich
        case  (5): return "star_101.gif";
        case  (6): return "star_110.gif";
        case  (7): return "star_111.gif"; 
        }
        return "graystar.gif";
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.img.IModifyImageFunction#setSize(int, int)
	 */
	public void setSize(int width, int height) {                         
		this.width = width;
		this.height = height;
	}

	public String createMapEntry() {      
                width = 400;
                height = 400;          
                
                int relativeX = Math.round(x*width/visibleSize) + width/2;
                int relativeY = Math.round(y*height/visibleSize) + height/2;
		// int relativeX = (int)Math.round(((double)x + (double)visibleSize));
		// int relativeY = (int)Math.round(((double)y + (double)visibleSize));
                
		return "<area shape=\"circle\" coords=\"" + relativeX + ","+ relativeY +","+IMG_RAD+"\" onmouseover=\"doTooltip(event,'"+name+"')\" onmouseout=\"hideTip()\">";
	}

}