package at.darkdestiny.core.title;

import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.util.DebugBuffer;

public class TheLucky extends AbstractTitle {

    public boolean check(int userId) {
        for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)) {


            PlanetCalculation pc = null;
            ExtPlanetCalcResult epcr = null;
            if (pp != null) {
                try {
                    pc = new PlanetCalculation(pp.getPlanetId());
                    epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
                } catch (Exception e) {
                    DebugBuffer.error("Error in Titlecondition " + e);

                }
                if (epcr.getMaxPopulation() > 15000000000l) {
                    return true;
                }
            }
        }
        return false;
    }
}
