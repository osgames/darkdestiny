/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.movable;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.dao.FleetDetailDAO;
import at.darkdestiny.core.dao.FleetOrderDAO;
import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.exceptions.InvalidInstantiationException;
import at.darkdestiny.core.fleet.FleetType;
import at.darkdestiny.core.fleet.LoadingInformation;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.FleetDetail;
import at.darkdestiny.core.model.FleetOrder;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class PlayerFleetExt implements IFleet {

    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static FleetOrderDAO foDAO = (FleetOrderDAO) DAOFactory.get(FleetOrderDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);


    private final PlayerFleet base;
    private final FleetDetail moveData;
    private int ETA = 0;
    private boolean ableToCallBack = true;
    private boolean inFleetFormation = false;
    private Double speed = null;
    private Integer range = null;
    private LoadingInformation loadInfo = null;
    private ArrayList<ShipData> shipList = null;
    private FleetOrder fo = null;
    private Boolean canInvade = null;
    private Boolean hasTractorBeam = null;
    private Boolean canDockStations = null;
    private boolean hasDockedStations = false;

    public PlayerFleetExt(PlayerFleet pf) {
        if (pf == null) {
            throw new InvalidInstantiationException("Cannot instantiate PlayerFleetExt with null");
        }

        base = pf;
        moveData = fdDAO.findByFleetId(pf.getId());

        if (base.getFleetFormationId() != 0) {
            inFleetFormation = true;
        }

        if (moveData != null) {
            ETA = moveData.getFlightTime() - (GameUtilities.getCurrentTick2() - moveData.getStartTime());
            if ((moveData.getStartPlanet() == 0) && (moveData.getStartSystem() == 0)) {
                ableToCallBack = false;
            }
        } else {
            ableToCallBack = false;
        }
    }

    public PlayerFleetExt(int id) {
        base = pfDAO.findById(id);
        if (base == null) {
            throw new InvalidInstantiationException("Fleet Id " + id + " does not exist");
        }

        moveData = fdDAO.findByFleetId(id);

        if (base.getFleetFormationId() != null && base.getFleetFormationId() != 0) {
            inFleetFormation = true;
        }

        if (moveData != null) {
            ETA = moveData.getFlightTime() - (GameUtilities.getCurrentTick2() - moveData.getStartTime());
            if ((moveData.getStartPlanet() == 0) && (moveData.getStartSystem() == 0)) {
                ableToCallBack = false;
            }
        } else {
            ableToCallBack = false;
        }
    }

    public LoadingInformation getLoadingInformation() {
        if (loadInfo == null) {
            loadInfo = new LoadingInformation(getBase());
        }

        return loadInfo;
    }
    
    @Override
    public boolean isScanning() {
        return (getScanDuration() > 0);
    }

    @Override
    public int getScanDuration() {
        Integer duration =  FleetService.findRunningScan(base.getUserId(), base.getId());
        if(duration == null){
            return 0;
        }else{
            return duration;
        }
    }

    @Override
    public boolean canScanSystem() {
        return FleetService.canScanSystem(this);
    }

    @Override
    public boolean canScanPlanet() {
        return FleetService.canScanPlanet(this);
    }

    @Override
    public AbsoluteCoordinate getAbsoluteCoordinate() {
        if (isMoving()) {
            int currTime = GameUtilities.getCurrentTick2();
            double distanceDone = (int) Math.floor(100d / moveData.getFlightTime() * (currTime - moveData.getStartTime()));

            int diffX = moveData.getDestX() - moveData.getStartX();
            int diffY = moveData.getDestY() - moveData.getStartY();

            int currX = moveData.getStartX() + (int) Math.floor(diffX / 100d * distanceDone);
            int currY = moveData.getStartY() + (int) Math.floor(diffY / 100d * distanceDone);

            return new AbsoluteCoordinate(currX, currY);
        } else {
            RelativeCoordinate rc = new RelativeCoordinate(getBase().getSystemId(), getBase().getPlanetId());
            return rc.toAbsoluteCoordinate();
        }
    }

    @Override
    public RelativeCoordinate getRelativeCoordinate() {
        if (isMoving()) {
            return null;
        }

        return new RelativeCoordinate(getBase().getSystemId(), getBase().getPlanetId());
    }

    @Override
    public double getSpeed() {
        if (speed == null) {
            double speedTmp = 999d;

            ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(getBase().getId());
            if (sfList.isEmpty()) {
                speedTmp = 0d;
            }
            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());
                if (sd == null) {
                    DebugBuffer.warning("Invalid design (" + sf.getDesignId() + ") found for shipfleet entry " + sf.getId() + " in fleet " + this.base.getName() + " (" + this.base.getId() + ")");
                    continue;
                }

                speedTmp = Math.min(speedTmp, sd.getHyperSpeed());
            }

            speed = speedTmp;
        }

        return speed;
    }
    
    @Override
    public Integer getRange() {
        if (range == null) {

            int rangeTmp = Integer.MAX_VALUE;
            ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(getBase().getId());
            if (sfList.isEmpty()) {
                rangeTmp = 0;
            }
            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());
                if (sd == null) {
                    DebugBuffer.warning("Invalid design (" + sf.getDesignId() + ") found for shipfleet entry " + sf.getId() + " in fleet " + this.base.getName() + " (" + this.base.getId() + ")");
                    continue;
                }

                rangeTmp = Math.min(rangeTmp, sd.getRange());
            }

            range = rangeTmp;
        }

        return range;
    }

    @Override
    public boolean isMoving() {
        return (moveData != null);
    }

    @Override
    public AbsoluteCoordinate getTargetAbsoluteCoordinate() {
        if (!isMoving()) {
            return null;
        }

        return new AbsoluteCoordinate(moveData.getDestX(), moveData.getDestY());
    }

    @Override
    public RelativeCoordinate getTargetRelativeCoordinate() {
        if (!isMoving()) {
            return null;
        }

        return new RelativeCoordinate(moveData.getDestSystem(), moveData.getDestPlanet());
    }

    @Override
    public AbsoluteCoordinate getSourceAbsoluteCoordinate() {
        if (!isMoving()) {
            return null;
        }

        return new AbsoluteCoordinate(moveData.getStartX(), moveData.getStartY());
    }

    @Override
    public RelativeCoordinate getSourceRelativeCoordinate() {
        if (!isMoving()) {
            return null;
        }

        return new RelativeCoordinate(moveData.getStartSystem(), moveData.getStartPlanet());
    }

    public PlayerFleet getBase() {
        return base;
    }

    @Override
    public int getETA() {
        return ETA;
    }

    @Override
    public boolean isAbleToCallBack() {
        return ableToCallBack;
    }

    @Override
    public ArrayList<ShipData> getShipList() {
        if (shipList == null) {
            shipList = new ArrayList<ShipData>();

            ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(base.getId());
            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());
                if (sd == null) {
                    DebugBuffer.warning("Invalid design (" + sf.getDesignId() + ") found for shipfleet entry " + sf.getId() + " in fleet " + this.base.getName() + " (" + this.base.getId() + ")");
                    continue;
                }

                ShipData sda = new ShipData();
                sda.setId(sf.getId());
                sda.setDesignId(sd.getId());
                sda.setDesignName(sd.getName());
                sda.setFleetId(base.getId());
                sda.setFleetName(base.getName());
                sda.setCount(sf.getCount());
                sda.setChassisSize(sd.getChassis());
                sda.setLoaded(false);

                shipList.add(sda);
            }
        }

        return shipList;
    }

    public boolean canColonize() {
        ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(getId());
        for (ShipFleet sf : sfList) {
            ShipDesign sd = sdDAO.findById(sf.getDesignId());
            if (sd == null) {
                DebugBuffer.warning("Invalid design (" + sf.getDesignId() + ") found for shipfleet entry " + sf.getId() + " in fleet " + this.base.getName() + " (" + this.base.getId() + ")");
                continue;
            }

            if (sd.getType() == EShipType.COLONYSHIP) {
                return true;
            }
        }
        return false;
    }

    public boolean canInvade() {
        if (canInvade == null) {
            if (!isMoving()) {
                ArrayList<PlayerFleetExt> pfeList = new ArrayList<PlayerFleetExt>();
                pfeList.add(this);

                canInvade = FleetService.invasionPossible(getBase().getUserId(), this);
            } else {
                canInvade = false;
            }
        }

        return canInvade.booleanValue();
    }

    public FleetOrder getFleetOrder() {
        if (fo == null) {
            fo = foDAO.get(base.getId(), FleetType.FLEET_FORMATION);
        }

        return fo;
    }

    public boolean isInFleetFormation() {
        return inFleetFormation;
    }

    public int getId() {
        return base.getId();
    }

    public String getName() {
        return base.getName();
    }

    public int getUserId() {
        return base.getUserId();
    }

    public boolean canHyperJump() {
        throw new UnsupportedOperationException("Please implement");
    }

    public boolean canFlyInterstellar() {
        for (ShipData sd : getShipList()) {
            if (sd.getChassisSize() == Chassis.ID_FIGHTER
                    || sd.getChassisSize() == Chassis.ID_CORVETTE) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return the hasTractorBeam
     */
    public Boolean getHasTractorBeam() {
        if (hasTractorBeam == null) {
            for (ShipData sData : getShipList()) {
                ShipDesign sd = sdDAO.findById(sData.getDesignId());

                if (sd.getHasTractorBeam()) {
                    hasTractorBeam = true;
                }
            }
        }

        if (hasTractorBeam == null) hasTractorBeam = false;

        return hasTractorBeam;
    }

    /**
     * @return the canDockStations
     */
    public Boolean getCanDockStations() {        
        if (!getHasTractorBeam()) {
            canDockStations = false;
            return false;
        }

        // Calculate available Space
        double availSpace = 0d;

        for (ShipData sData : getShipList()) {
            ShipDesign sd = sdDAO.findById(sData.getDesignId());

            if (sd.getHasTractorBeam()) {
                switch (sd.getChassis()) {
                    case (Chassis.ID_BATTLESHIP):
                        availSpace += 0.25d * sData.getCount();
                        break;
                    case (Chassis.ID_SUPERBATTLESHIP):
                        availSpace += 1d * sData.getCount();
                        break;
                    case (Chassis.ID_TENDER):
                        availSpace += 4d * sData.getCount();
                        // In case of Dockingmodule double transport capacity
                }
            }
        }

        // Get Docked Stations
        PlanetDefense pd = new PlanetDefense();
        pd.setFleetId(getId());
        pd.setType(EDefenseType.SPACESTATION);
        ArrayList<PlanetDefense> pdList = pdDAO.find(pd);

        for (PlanetDefense pdEntry : pdList) {
            ShipDesign sd = sdDAO.findById(pdEntry.getUnitId());
            hasDockedStations = true;

            switch (sd.getChassis()) {
                case (Chassis.ID_SMALLBATTLESTATION):
                    availSpace -= 1d * pdEntry.getCount();
                    break;
                case (Chassis.ID_BIGBATTLESTATION):
                    availSpace -= 2d * pdEntry.getCount();
                    break;
            }
        }
                
        if (availSpace >= 1d) {
            canDockStations = true;
        } else {
            canDockStations = false;
        }

        if (canDockStations) {
            RelativeCoordinate currPosition = getRelativeCoordinate();            
            PlanetDefense pdSearch = new PlanetDefense();
            
            if (currPosition.getPlanetId() != 0) {
                // Check Stations on Planet
                pdSearch.setType(EDefenseType.SPACESTATION);
                pdSearch.setUserId(getUserId());
                pdSearch.setPlanetId(currPosition.getPlanetId());              
                System.out.println(this.getBase().getId() + ": Search for Spacestations with UserId " + getUserId() + " on Planetary Id " + currPosition.getPlanetId());
            } else {
                // Check Stations on System
                pdSearch.setType(EDefenseType.SPACESTATION);
                pdSearch.setUserId(getUserId());
                pdSearch.setSystemId(currPosition.getSystemId());       
                System.out.println(this.getBase().getId() + ": Search for Spacestations with UserId " + getUserId() + " on System Id " + currPosition.getSystemId());
            }
            
            if (pdDAO.find(pdSearch).isEmpty()) {
                canDockStations = false;
            }
        }
        
        return canDockStations;
    }

    /**
     * @return the hasDockedStations
     */
    public boolean getHasDockedStations() {
        if (canDockStations == null) getCanDockStations();

        return hasDockedStations;
    }

    @Override
    public ELocationType getLocationType() {
        return this.base.getLocationType();
    }

    @Override
    public Integer getLocationId() {
        return this.base.getLocationId();
    }
}
