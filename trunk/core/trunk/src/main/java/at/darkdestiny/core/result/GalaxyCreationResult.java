/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.System;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class GalaxyCreationResult {
    
    
    private Galaxy galaxy;
   private final ArrayList<PlanetRessource> ressources;
   private final ArrayList<Planet> planets;
   private final ArrayList<System> systems;

    public GalaxyCreationResult(Galaxy galaxy, ArrayList<PlanetRessource> ressources, ArrayList<Planet> planets, ArrayList<System> systems) {
        this.galaxy = galaxy;
        this.ressources = ressources;
        this.planets = planets;
        this.systems = systems;
    }

    /**
     * @return the galaxy
     */
    public Galaxy getGalaxy() {
        return galaxy;
    }

    /**
     * @return the ressources
     */
    public ArrayList<PlanetRessource> getRessources() {
        return ressources;
    }

    /**
     * @return the planets
     */
    public ArrayList<Planet> getPlanets() {
        return planets;
    }

    /**
     * @return the systems
     */
    public ArrayList<System> getSystems() {
        return systems;
    }

    public void persist() {
        
            galaxy = Service.galaxyDAO.add(galaxy);
            Service.systemDAO.insertAll(systems);
            Service.planetDAO.insertAll(planets);
            Service.planetRessourceDAO.insertAll(ressources);
    }
   
   
}
