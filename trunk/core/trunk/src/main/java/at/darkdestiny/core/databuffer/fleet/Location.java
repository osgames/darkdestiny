/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.databuffer.fleet;

import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.SunTransmitterDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.SunTransmitter;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class Location {
    private static SunTransmitterDAO tDAO = (SunTransmitterDAO) DAOFactory.get(SunTransmitterDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    
    private final ELocationType locationType;
    private final int locationId;

    public Location(ELocationType locType, int locId) {
        if (!verifyLocation(locType,locId)) throw new RuntimeException("Location ("+locType+"/"+locId+") not valid.");
        
        this.locationType = locType;
        this.locationId = locId;  
    }
    
    public Location(RelativeCoordinate rc) {
        if (rc.getPlanetId() != 0) {
            locationType = ELocationType.PLANET;
            locationId = rc.getPlanetId();
        } else {
            locationType = ELocationType.SYSTEM;
            locationId = rc.getSystemId();
        }
    }

    public static boolean verifyLocation(ELocationType locationType, int locationId) throws RuntimeException {
        switch (locationType) {
            case PLANET:
                // Check if planet exists
                if (pDAO.findById(locationId) == null) {
                    return false;
                    // throw new RuntimeException("Invalid Location specified");
                }                
                break; 
            case SYSTEM:
                // Check if system exists
                if (sDAO.findById(locationId) == null) {
                    return false;
                    // throw new RuntimeException("Invalid Location specified");
                }                
                break;
            case TRANSIT:
                // Throw Runtime Error
                throw new RuntimeException("Invalid Location TRANSIT");
            case TRANSMITTER:
                // Check if transmitter exists
                if (tDAO.findById(locationId) == null) {
                    return false;
                    // throw new RuntimeException("Invalid Location specified");
                }
                break;
        }
        
        return true;
    }
    
    public RelativeCoordinate toRelativeCoordinate() {
        RelativeCoordinate rc = null;
        
        switch (locationType) {
            case PLANET:
                Planet p = pDAO.findById(locationId);
                rc = new RelativeCoordinate(p.getSystemId(),p.getId());
                break;
            case SYSTEM:
                at.darkdestiny.core.model.System s = sDAO.findById(locationId);
                rc = new RelativeCoordinate(s.getId(),0);
                break;
            case TRANSMITTER:
                SunTransmitter st = tDAO.findById(locationId);
                rc = new RelativeCoordinate(st.getSystemId(),st.getPlanetId());
                break;
            default:
                throw new RuntimeException("Invalid Conversion");
        }
        
        return rc;
    }
    
    /**
     * @return the locationId
     */
    public int getLocationId() {
        return locationId;
    }

    /**
     * @return the locationType
     */
    public ELocationType getLocationType() {
        return locationType;
    }
}
