/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ChangeEntry;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Dreloc
 */
public class ChangeEntryDAO extends ReadWriteTable<ChangeEntry> implements GenericDAO  {


    public ArrayList<ChangeEntry> findAllSorted(){
        ArrayList<ChangeEntry> result = new ArrayList<ChangeEntry>();
        TreeMap<Integer, ChangeEntry> sorted = new TreeMap<Integer, ChangeEntry>();
        for(ChangeEntry ce : (ArrayList<ChangeEntry>)findAll()){
            sorted.put(ce.getId(), ce);
        }
        result.addAll(sorted.descendingMap().values());
        return result;
    }

}
