package at.darkdestiny.core.title;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.OverviewService;
import at.darkdestiny.framework.dao.DAOFactory;

public class TheGenerous extends AbstractTitle {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    public boolean check(int userId) {
        PlayerPlanet pp = ppDAO.findHomePlanetByUserId(userId);
        if(pp == null) return false;
        int planetId = pp.getPlanetId();
        PlanetCalculation pc = OverviewService.PlanetCalculation(planetId);
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        if(userId == 2){
            //log.debug((pp.getPopulation() - 10) + " > " + )
        }
        if (pp.getPopulation() > (epcr.getMaxPopulation() - 10000000)) {
            return true;
        } else {
            return false;
        }
    }
}
