/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

/**
 *
 * @author Bullet
 */
public class MoraleResult {
    private float maxMorale;
    private float estimatedMorale;
    private int morale;
    private int moraleBonus;

    public MoraleResult(int morale, int moraleBonus, float maxMorale, float estimatedMorale){
        this.morale = morale;
        this.moraleBonus = moraleBonus;
        this.maxMorale = maxMorale;
        this.estimatedMorale = estimatedMorale;
    }

    /**
     * @return the maxMorale
     */
    public float getMaxMorale() {
        return maxMorale;
    }

    /**
     * @param maxMorale the maxMorale to set
     */
    public void setMaxMorale(float maxMorale) {
        this.maxMorale = maxMorale;
    }

    /**
     * @return the estimatedMorale
     */
    public float getEstimatedMorale() {
        return estimatedMorale;
    }

    /**
     * @param estimatedMorale the estimatedMorale to set
     */
    public void setEstimatedMorale(float estimatedMorale) {
        this.estimatedMorale = estimatedMorale;
    }

    /**
     * @return the morale
     */
    public int getMorale() {
        return morale;
    }

    /**
     * @param morale the morale to set
     */
    public void setMorale(int morale) {
        this.morale = morale;
    }

    /**
     * @return the moraleBonus
     */
    public int getMoraleBonus() {
        return moraleBonus;
    }

    /**
     * @param moraleBonus the moraleBonus to set
     */
    public void setMoraleBonus(int moraleBonus) {
        this.moraleBonus = moraleBonus;
    }
}
