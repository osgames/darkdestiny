/*
 *  To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view.menu;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.model.StyleElement;
import at.darkdestiny.core.model.StyleToUser;
import at.darkdestiny.core.service.ProfileService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.view.html.style;
import at.darkdestiny.core.view.html.table;
import at.darkdestiny.core.view.html.td;
import at.darkdestiny.core.view.html.tr;
import at.darkdestiny.core.view.menu.statusbar.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Horst
 */
public class StatusBar extends Service {

    // Breite der Statusbar (bleibt ?berall gleich)
    static final int width = 170;
    // Daten zum Anzeigen
    StatusBarData statusBarData;
    // Table zum retournieren
    private table statusBarTable;
    ArrayList<IStatusBarEntity> data;

    public StatusBar(StatusBarData statusBarData, int userId) {
        this.statusBarData = statusBarData;
        data = new ArrayList<IStatusBarEntity>();
        int style = 1;
        ArrayList<StyleElement> result = styleElementDAO.findByStyleId(style);
        ArrayList<StyleToUser> userStyle = ProfileService.findStyleToUserByUserId(userId);
        HashMap<Integer, StyleElement> ordered = new HashMap<Integer, StyleElement>();
        // If user defined a template
        if (userStyle.size() > 0) {
            style = userStyle.get(0).getStyleId();
            for (StyleToUser stu : userStyle) {
                ordered.put(stu.getRank(), styleElementDAO.findBy(stu.getElementId(), stu.getStyleId()));
            }
            // else use standard
        } else {
            for (StyleElement s : result) {
                if (s.isCore()) {
                    ordered.put(s.getId(), s);
                }
            }
        }

        result = new ArrayList<StyleElement>();
        result.addAll(ordered.values());

        for (StyleElement s : result) {
            try {
                Class clzz = Class.forName(s.getClazz());
                try {
                    Constructor con = clzz.getConstructor(StatusBarData.class, int.class, int.class);
                    try {
                        IStatusBarEntity sbe = (IStatusBarEntity) con.newInstance(statusBarData, userId, styleDAO.findById(style).getWidth());
                        data.add(sbe);

                    } catch (InstantiationException ex) {
                        Logger.getLogger(StatusBar.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(StatusBar.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(StatusBar.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(StatusBar.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(StatusBar.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SecurityException ex) {
                    Logger.getLogger(StatusBar.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(StatusBar.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // Select clzz from db where userId = 12 ordered by rank;


        //Erstellen eines Styles f?r die Div Container
        style style1 = new style();
        //Hinzuf?gen aller verwendeten DivContaienrs
        style1 = addDivContainers(style1);

        style1.setProperties("type='text/css'");
        style1.setData("\t div { border:1px solid #888; } \t\n");


        this.statusBarTable = new table();
        //Nullsetzen des Randes, cellspacing und cellpading
        statusBarTable.setProperties(" id='statusbartable' border = '0'  cellspacing='0' cellpadding='0' ");
        //Hinzuf?gen des CSS-Styles zur Table
        this.statusBarTable.setStyle(style1);

        //Hinzuf?gen des HTML Codes zur Table
        this.statusBarTable = buildStatusBar(statusBarTable);
    }

    private table buildStatusBar(table rightMenuTable) {

        for (IStatusBarEntity sbe : data) {

            rightMenuTable = sbe.addTr(rightMenuTable);

        }
        return rightMenuTable;

    }

    private style addDivContainers(style style1) {
        for (IStatusBarEntity sbe : data) {
            style1 = sbe.addStyle(style1);

        }
        return style1;
    }

    private tr buildFinisher(int height, String imageUrl) {

        String properties = "";

        tr tr1 = new tr();

        td td1 = new td();

        properties += "height='" + height + "' ";
        properties += "width='" + width + "' ";
        properties += "style='background-image: url(" + GameConfig.getInstance().picPath() + imageUrl + ")';  position:relative; text-align:right; background-repeat:repeat-x;";

        td1.setProperties(properties);

        tr1.addTd(td1);

        return tr1;
    }

    public table getStatusBarTable() {
        return statusBarTable;
    }

    public void setStatusBarTable(table statusBarTable) {
        this.statusBarTable = statusBarTable;
    }
}
