/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.WeaponFactor;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class WeaponFactorDAO extends ReadOnlyTable<WeaponFactor> implements GenericDAO {
    public WeaponFactor findByWeaponId(int weaponId) {
        WeaponFactor wf = new WeaponFactor();
        wf.setWeaponId(weaponId);
        return get(wf);
    }
}
