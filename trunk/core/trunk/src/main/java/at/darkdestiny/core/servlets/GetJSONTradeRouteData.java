/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

 import at.darkdestiny.core.ML;import at.darkdestiny.core.TradeShipData;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.TradeRouteResult;
import at.darkdestiny.core.service.TransportService;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.core.trade.TradeUtilities;
import at.darkdestiny.util.DebugBuffer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.TradeShipData;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.TradeRouteResult;
import at.darkdestiny.core.service.TransportService;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.core.trade.TradeUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class GetJSONTradeRouteData extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(GetJSONTradeRouteData.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate"); //HTTP 1.1
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server

        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        StringBuffer json = new StringBuffer();
        json.append("{ ");


        try {
            int userId = Integer.parseInt((String) session.getAttribute("userId"));

            //Load data aff all routes
            if (request.getParameter("updateAll") != null) {

                //Loading data
                TradeRouteResult trr = TradeUtilities.getInternalTradeRoutes(userId);
                json.append("\"label\":\"" + ML.getMLStr("transport_lbl_assignedships", userId) +
                        "\", \"traderoutes\": [ ");
                boolean skip = true;
                for (TradeRouteExt tre : trr.getRoutes()) {

                    if (skip) {
                        skip = false;
                    } else {
                        json.append(",");
                    }
                    ArrayList<TradeShipData> ships = TransportService.getShipsInRoute(userId, tre.getBase().getId());

                    int count = 0;
                    for(TradeShipData tsd : ships){
                        count += tsd.getCount();
                    }

                    json.append("{ \"tradeRouteId\":\"" + tre.getBase().getId()
                            + "\", \"userId\":\"" + userId
                            + "\", \"canAddShips\":\"" + tre.isAddingShipsPossible()
                            + "\", \"canRemoveShips\":\"" + tre.isRemovingShipsPossible()
                            + "\", \"canAddTransmitter\":\"" + tre.isAddingTransmitterPossible()
                            + "\", \"canRemoveTransmitter\":\"" + tre.isRemovingTransmitterPossible()
                            + "\", \"ships\":\"" + count
                            + "\" " + "}" + "");






                }
                json.append("] ");
                //Do specific shit als removing and adding
            } else {

                int tradeRouteId = Integer.parseInt(request.getParameter("tradeRouteId"));
                boolean remove = Boolean.parseBoolean(request.getParameter("remove"));

                boolean action = Boolean.parseBoolean(request.getParameter("action"));

                BaseResult result = null;
                //Remove or add
                if (action) {
                    if (remove) {
                        result = TransportService.removeShipsFromRoute(userId, tradeRouteId, request.getParameterMap());
                    }else{
                        result = TransportService.addShipsToRoute(userId, tradeRouteId, request.getParameterMap());
                    }
                }

                //Loading data

                json.append(" \"tradeRouteId\":\"" + tradeRouteId + "\",");
                json.append(" \"userId\":\"" + userId + "\",");
                if (action) {
                    json.append(" \"action\":\"1\",");
                    if (result.isError()) {
                        json.append(" \"error\":\"1\",");
                        json.append(" \"errmsg\":\"" + result.getMessage() + "\",");
                    } else {
                        json.append(" \"error\":\"0\",");
                    }
                } else {
                    json.append(" \"action\":\"0\",");
                }
                json.append(" \"remove\":\"" + remove + "\"");


                if (remove) {
                    ArrayList<TradeShipData> ships = TransportService.getShipsInRoute(userId, tradeRouteId);

                    if (ships.size() > 0) {
                        json.append(" ,");
                        json.append(" \"ships\": [ ");
                        boolean skip = true;
                        for (TradeShipData ship : ships) {


                            if (skip) {
                                skip = false;
                            } else {
                                json.append(",");
                            }

                            json.append("{ \"id\":\"" + ship.getDesignId()
                                    + "\", \"name\":\"" + ship.getDesignName()
                                    + "\", \"count\":\"" + ship.getCount()
                                    + "\", \"planetId\":\"" + ship.getHome().getPlanetId()
                                    + "\" " + "}" + "");

                        }

                        json.append("] ");
                    } else {
                        json.append(" ,");
                        json.append(" \"ships\": [ ");
                        json.append("] ");
                    }
                } else {

                    ArrayList<ShipData> ships = TransportService.getAllTransportShips(userId, tradeRouteId);
                    if (ships.size() > 0) {
                        json.append(" ,");
                        json.append(" \"ships\": [ ");
                        boolean skip = true;
                        for (ShipData ship : ships) {


                            if (skip) {
                                skip = false;
                            } else {
                                json.append(",");
                            }

                            json.append("{ \"id\":\"" + ship.getDesignId()
                                    + "\", \"fleetName\":\"" + ship.getFleetName()
                                    + "\", \"fleetId\":\"" + ship.getFleetId()
                                    + "\", \"name\":\"" + ship.getDesignName()
                                    + "\", \"count\":\"" + ship.getCount()
                                    + "\" " + "}" + "");

                        }

                        json.append("] ");
                    } else {
                        json.append(" ,");
                        json.append(" \"ships\": [ ");
                        json.append("] ");
                    }
                }


            }

            json.append(" }");
            out.println(json);

            long endTime = System.currentTimeMillis();
        } catch (NumberFormatException nfe) {
            // Seriously i dont care about crap entered by people ;)
            nfe.printStackTrace();
            log.debug("Invalid data entered " + nfe.getMessage());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ManagementData: ", e);
            e.printStackTrace();;
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";


    }// </editor-fold>
}
