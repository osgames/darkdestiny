/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

/**
 *
 * @author Stefan
 */
public class SMHyperscanner {
    private final int x;
    private final int y;
    private final int range;    
    
    public SMHyperscanner(int x, int y, int range) {
        this.x = x;
        this.y = y;
        this.range = range;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @return the range
     */
    public int getRange() {
        return range;
    }
}
