/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling;

/**
 *
 * @author Stefan
 */
class OrderSyncEntry {
    private final int userId;

    protected OrderSyncEntry(int userId) {
        this.userId = userId;
    }
    
    protected int getUserId() {
        return userId;
    }

}
