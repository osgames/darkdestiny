/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core;

import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class FleetFormationCheck {
    private HashSet<Integer> ownFormations = new HashSet<Integer>();
    private HashSet<Integer> alliedFormations = new HashSet<Integer>();
    private HashSet<Integer> displayedFormations = new HashSet<Integer>();

    public boolean userIsContributing(int ffId) {
        return ownFormations.contains(ffId);
    }

    public boolean isAlliedFormation(int ffId) {
        return alliedFormations.contains(ffId);
    }

    public boolean alreadyDisplayed(int ffId) {
        return displayedFormations.contains(ffId);
    }

    public void addContributeTo(int ffId) {
        ownFormations.add(ffId);
    }

    public void addAlliedFormation(int ffId) {
        alliedFormations.add(ffId);
    }

    public void setDisplayed(int ffId) {
        displayedFormations.add(ffId);
    }
}
