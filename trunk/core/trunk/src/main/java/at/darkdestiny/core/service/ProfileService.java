/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.model.NotificationToUser;
import at.darkdestiny.core.model.NotificationType;
import at.darkdestiny.core.model.Style;
import at.darkdestiny.core.model.StyleElement;
import at.darkdestiny.core.model.StyleToUser;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.model.UserSettings;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.ProfileResult;
import at.darkdestiny.core.utilities.MD5;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.w3c.tidy.Tidy;

/**
 *
 * @author Bullet
 */
public class ProfileService extends Service {

    public static UserData findUserDataByUserId(int userId) {
        return userDataDAO.findByUserId(userId);
    }

    public static User findUserByUserId(int userId) {
        return userDAO.findById(userId);
    }

    public static boolean isDeletionExpired(int userId) {
        return getTimeTilDeletion(userId) < 0;
    }

    public static void checkPic(String pic) throws Exception {
        //Alle BilderURLs sind OK
    }

    public static ArrayList<NotificationType> findNotification(){
        return notificationDAO.findAll();
    }

    public static int checkDesc(String desc, StringBuffer sb) throws Exception {
        if (desc == null) return 0;
        
        InputStream is = new ByteArrayInputStream(desc.getBytes());
        StringWriter sw = new StringWriter();
        
        Tidy tidy = new Tidy();
        tidy.setQuiet(true);
        tidy.setPrintBodyOnly(true);
        tidy.parse(is, sw);
        //tidy.parseDOM(fi, fo);
        int tempWarnings = tidy.getParseWarnings();
        int tempErrors = tidy.getParseErrors();
     
        if (sb != null) {
            sb.append(sw.toString());
        }
        
        return tempErrors;
    }
    
    public static void updateNotifications(int userId, Map parMap){
        ArrayList<Integer> logged = new ArrayList<Integer>();
        ArrayList<Integer> away = new ArrayList<Integer>();
        for (Object entry : parMap.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String key = (String) me.getKey();

            if (key.contains("checkEnabled")) {
            int type = Integer.parseInt(key.substring(key.length()-1, key.length()));
                logged.add(type);
            }else if(key.contains("checkAway")){
            int type = Integer.parseInt(key.substring(key.length()-1, key.length()));
                away.add(type);
            }
        }
        for(NotificationToUser ntu : notificationToUserDAO.findByUserId(userId)){
            if(logged.contains(ntu.getNotificationId())){
                ntu.setEnabled(true);
            }else{
                ntu.setEnabled(false);
            }
            if(away.contains(ntu.getNotificationId())){
                ntu.setAwayMode(true);
            }else{
                ntu.setAwayMode(false);
            }
            notificationToUserDAO.update(ntu);
        }
    }

    public static NotificationToUser findNotificationToUserBy(int userId, int type){
        return notificationToUserDAO.getOrCreate(userId, type);
    }

    public static void deleteStyleEntries(int userId) {
        for (StyleToUser stu : styleToUserDAO.findByUserId(userId)) {
            styleToUserDAO.remove(stu);
        }
    }

    public static int findStyleByUserId(int userId) {
        return (styleToUserDAO.findByUserId(userId).get(0).getStyleId());
    }

    public static Style findStyleById(int styleId) {
        return (styleDAO.findById(styleId));
    }

    public static int getTimeTilDeletion(int userId) {
        User u = userDAO.findById(userId);
        
        if (u == null) {
            DebugBuffer.error("User " + userId + " not found!");
            return -999;
        }
        
        GameUtilities gu = new GameUtilities();
        if (u.getDeleteDate() <= 0) {
            return Integer.MAX_VALUE;
        }
        return GameConstants.DELETION_DELAY + u.getDeleteDate() - gu.getCurrentTick();
    }

    public static ArrayList<User> findAllUsers() {
        return userDAO.findAll();
    }

    public static ProfileResult updateProfileData(int userId, Map params) {
        ProfileResult pr = new ProfileResult();

        boolean invalidateSession = false;
        ArrayList<BaseResult> errors = new ArrayList<BaseResult>();
        User user = userDAO.findById(userId);
        UserData userData = userDataDAO.findByUserId(userId);
        UserSettings userSettings = userSettingsDAO.findByUserId(userId);

        String oldpwd = "";
        String newpwd = "";
        String confirmpwd = "";
        String EMail = "";
        String pic = "";
        String desc = "";
        boolean requestDelete = false;

        userSettings.setNewsletter(false);
        userSettings.setProtectaccount(false);

        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String key = (String) me.getKey();
            if (key.contains("oldpwd")) {
                oldpwd = (((String[]) me.getValue())[0]);
            } else if (key.contains("newpwd")) {
                newpwd = (((String[]) me.getValue())[0]);
            } else if (key.contains("confirmpwd")) {
                confirmpwd = (((String[]) me.getValue())[0]);
            } else if (key.contains("email")) {
                EMail = (((String[]) me.getValue())[0]);
            } else if (key.contains("pic")) {
                pic = (((String[]) me.getValue())[0]);
            } else if (key.contains("desc")) {
                desc = (((String[]) me.getValue())[0]);
            } else if (key.contains("delete")) {
                try{
                int delete = Integer.parseInt((((String[]) me.getValue())[0]));
                if ((delete != 0)) {
                    requestDelete = true;

                }
                }catch(Exception e){
                    DebugBuffer.addLine(DebugLevel.ERROR, "NAN in Profile");
                }
            } else if (key.contains("newsletter")) {
                    userSettings.setNewsletter(true);

            }else if (key.contains("protectaccount")) {
                    userSettings.setProtectaccount(true);

            }
        }

        if ((oldpwd != null) && (!oldpwd.equals(""))) {
            if (user.getPassword().equals(MD5.encryptPassword(oldpwd))) {
                if ((newpwd.length() < 5) || (confirmpwd.length() < 5)) {
                    errors.add(new BaseResult(ML.getMLStr("profile_err_newpasstooshort", userId), true));
                } else {
                    if (newpwd.equals(confirmpwd)) {
                        user.setPassword(MD5.encryptPassword(newpwd));
                        invalidateSession = true;
                    } else {
                        errors.add(new BaseResult(ML.getMLStr("profile_err_passwordnotsame", userId), true));
                    }
                }
            } else {
                errors.add(new BaseResult(ML.getMLStr("profile_err_oldpasswordwrong", userId), true));
            }

        }

        EMail = EMail.trim();
        if (!EMail.equalsIgnoreCase(user.getEmail())) {
            if (!EMail.contains("@") || !EMail.contains(".")) {
                errors.add(new BaseResult(ML.getMLStr("profile_err_emailnotvalid", userId), true));
            } else {
                user.setEmail(EMail);
            }
        }
        try {
            checkPic(pic);
        } catch (Exception e) {
            errors.add(new BaseResult(ML.getMLStr("profile_err_imagelinknotvalid", userId), true));
        }
        if (!pic.equalsIgnoreCase(userData.getPic())) {
            userData.setPic(pic);
        }
        try {
            StringBuffer sb = new StringBuffer();
            
            int errorCnt = checkDesc(desc, sb);
            if (errorCnt > 0) {
                errors.add(new BaseResult(ML.getMLStr("profile_err_descriptionnotvalid", userId), true));
            } else {
                desc = sb.toString();
            }
        } catch (Exception e) {
            errors.add(new BaseResult(ML.getMLStr("profile_err_descriptionnotvalid", userId), true));
        }
        if (!desc.equalsIgnoreCase(userData.getDescription())) {
            desc = FormatUtilities.killJavaScript(desc);
            userData.setDescription(desc);
        }

        if (requestDelete) {
            GameUtilities gu = new GameUtilities();
            user.setDeleteDate(gu.getCurrentTick());
            pr.setAutoLogOut(true);

        }else{
            if(user.getDeleteDate() > 0){
                if (GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - user.getDeleteDate()) > GameConstants.DELETION_DELAY / 2) {
                    user.setDeleteDate(0);
                }
            }
        }

        if(errors.isEmpty()){
            errors.add(new BaseResult(ML.getMLStr("global_done", userId), false));
        }
        userDAO.update(user);
        userDataDAO.update(userData);
        userSettingsDAO.update(userSettings);

        pr.setInvalidateSession(invalidateSession);
        pr.setResult(errors);

        return pr;
    }

    public static void revokeDeletionState(int userId) {
        User u = userDAO.findById(userId);

        if (u.getDeleteDate() != 0) {
            // get time left till login is possible again
            int gracePeriod = u.getDeleteDate() + (int)(GameConstants.DELETION_DELAY / 2d);
            int graceLeft = gracePeriod - GameUtilities.getCurrentTick2();

            u.setLocked(graceLeft);
            u.setDeleteDate(0);
        }
    }

    public static void updateUser(int userId, int starmap_height, int starmap_width, boolean starmap_externalBoolean, boolean starmap_staticBoolean, int ttv_height, int ttv_width, boolean ttv_externalBoolean, int sittedById, int leaveDays, boolean conDetails, boolean productionDetails) {
        UserData userData = userDataDAO.findByUserId(userId);
        User user = userDAO.findById(userId);
        UserSettings userSettings = userSettingsDAO.findByUserId(userId);
        userSettings.setStarmapExternalBrowser(starmap_externalBoolean);
        userSettings.setTtvExternalBrowser(ttv_externalBoolean);
        userSettings.setShowConstructionDetails(conDetails);
        userSettings.setSittedById(sittedById);
        if (leaveDays > 0 && userSettings.getLeaveDate() == 0) {
            userSettings.setLeaveDate(System.currentTimeMillis());
        }

        userSettings.setLeaveDays(leaveDays);
        userSettings.setStarmapHeight(starmap_height);
        userSettings.setStarmapWidth(starmap_width);
        userSettings.setStarmapUseInternal(starmap_staticBoolean);
        userSettings.setTtvHeight(ttv_height);
        userSettings.setTtvWidth(ttv_width);
        userSettings.setShowProductionDetails(productionDetails);

        userDAO.update(user);
        userDataDAO.update(userData);
       userSettingsDAO.update(userSettings);
    }

    public static UserData updateUserData(UserData ud) {
        return userDataDAO.update(ud);
    }

    public static User updateUser(User u) {
        return userDAO.update(u);
    }

    public static void removeUser(User u) {
        userDAO.remove(u);
    }

    public static User addUser(User u) {
        return userDAO.add(u);
    }

    public static ArrayList<Style> findAllStyles() {
        return styleDAO.findAll();
    }

    public static ArrayList<StyleElement> findStyleElementsByUserId(int userId) {

        ArrayList<StyleToUser> s2u = styleToUserDAO.findByUserId(userId);
        ArrayList<StyleElement> result = new ArrayList<StyleElement>();
        HashMap<Integer, StyleElement> ordered = new HashMap<Integer, StyleElement>();
        for (StyleToUser se : s2u) {
            ordered.put(se.getRank(), styleElementDAO.findBy(se.getElementId(), se.getStyleId()));
        }
        result.clear();
        result.addAll(ordered.values());
        return result;
    }

    public static void updateOrder(ArrayList<StyleToUser> toOrder) {
        for (StyleToUser stu : toOrder) {
            styleToUserDAO.update(stu);
        }

    }

    public static ArrayList<StyleElement> findStyleElementByStyleId(int styleId) {
        ArrayList<StyleElement> result = styleElementDAO.findByStyleId(styleId);
        HashMap<Integer, StyleElement> ordered = new HashMap<Integer, StyleElement>();
        for (StyleElement se : result) {
            ordered.put(se.getId(), se);
        }
        result.clear();
        result.addAll(ordered.values());
        return result;
    }

    public static ArrayList<StyleToUser> findStyleToUserByUserId(int userId) {
        ArrayList<StyleToUser> result = styleToUserDAO.findByUserId(userId);
        HashMap<Integer, StyleToUser> ordered = new HashMap<Integer, StyleToUser>();
        for (StyleToUser se : result) {
            ordered.put(se.getRank(), se);
        }
        result.clear();
        result.addAll(ordered.values());
        return result;
    }

    public static void addStyleToUser(int userId, ArrayList<StyleElement> toAdd) {

        int order = 1;
        for (StyleElement se : toAdd) {
            StyleToUser stu = new StyleToUser();
            stu.setElementId(se.getId());
            stu.setStyleId(se.getStyleId());
            stu.setRank(order);
            stu.setUserId(userId);

            styleToUserDAO.add(stu);

            order++;
        }
    }
    
    public static String locationCode(int userId) {
        String locationCode = "";
        
        User u = userDAO.findById(userId);
        String tmpStr = u.getGameName() + u.getIp() + u.getEmail();
        locationCode = MD5.encryptPassword(tmpStr).substring(0, 5);
        
        return locationCode;
    }
    
    public static int[] getStarMapSize(int userId) {
        int[] xySize = new int[2];
        
        UserSettings us = Service.userSettingsDAO.findByUserId(userId);
        xySize[0] = us.getStarmapWidth();
        xySize[1] = us.getStarmapHeight();
        
        return xySize;
    }
}
