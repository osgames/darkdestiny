/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.databuffer;


/**
 *
 * @author Stefan
 */
public class MutualRessAmountEntry extends RessAmountEntry {
    public MutualRessAmountEntry(int ressId, long qty) {
        this.ressId = ressId;
        this.qty = qty;
    }   

    public void setQty(long qty) {
        this.qty = qty;
    }
}
