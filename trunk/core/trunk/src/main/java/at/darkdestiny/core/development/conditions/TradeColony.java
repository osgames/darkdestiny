/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.development.conditions;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TradeColony implements IBonusCondition {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);

    @Override
    public boolean isPossible(int planetId) {
        PlanetConstruction pc = pcDAO.findBy(planetId, Construction.ID_INTERSTELLARTRADINGPOST);

        if (pc == null) return false;

        return true;
    }

    @Override
    public boolean isPossible() {
        return false;
    }

    @Override
    public ArrayList<String> activeBoni(int bonusId, int planetId, int userId) {
        ArrayList<String> boniList = new ArrayList<String>();

        boniList.add("trade placeholder");

        return boniList;
    }

    @Override
    public void onActivation(int bonusId, int planetId, int userId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        pp.setColonyType(EColonyType.TRADE_COLONY);
        ppDAO.update(pp);
    }
}
