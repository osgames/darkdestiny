/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "logintracker")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class LoginTracker extends Model<LoginTracker> {

    /**
     * @TODO Eigentlich sind hier alle Felder IDs vertr�gt das Framework
     *
     */
    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("loginIP")
    @IdFieldAnnotation
    private String loginIP;
    @FieldMappingAnnotation("time")
    @IdFieldAnnotation
    private Long time;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the loginIP
     */
    public String getLoginIP() {
        return loginIP;
    }

    /**
     * @param loginIP the loginIP to set
     */
    public void setLoginIP(String loginIP) {
        this.loginIP = loginIP;
    }

    /**
     * @return the time
     */
    public Long getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Long time) {
        this.time = time;
    }
}
