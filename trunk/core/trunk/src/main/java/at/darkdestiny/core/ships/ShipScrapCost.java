/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ships;

import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.dao.ChassisDAO;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class ShipScrapCost extends MutableRessourcesEntry {
    private ChassisDAO chDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);

    private final int count;
    private final BuildTime buildTime;
    private final ShipDesignExt sde;

    public ShipScrapCost(ShipDesignExt sde) {
        this.count = 1;
        this.sde = sde;
        this.buildTime = new BuildTime();

        calcScrapCost();
    }

    public ShipScrapCost(ShipDesignExt sde, int count) {
        this.count = count;
        this.sde = sde;
        this.buildTime = new BuildTime();

        calcScrapCost();
    }

    private void calcScrapCost() {
        RessourcesEntry re = sde.getRessCost();

        this.addRess(re, count);
        this.multiplyRessources(0.8d, 0.1d);

        buildTime.setOrbDockPoints((int)Math.ceil(sde.getBuildTime().getOrbDockPoints() * 0.1d * count));
        buildTime.setDockPoints((int)Math.ceil(sde.getBuildTime().getDockPoints() * 0.1d * count));
        buildTime.setModulePoints((int)Math.ceil(sde.getBuildTime().getModulePoints() * 0.1d * count));

        // Load chassis multiplicator
        // Chassis ch = chDAO.findById(((ShipDesign)sde.getBase()).getChassis());
        // this.multiplyRessources(ch.getMinBuildQty());
        // buildTime.adjustBuildTime(ch.getMinBuildQty());
    }

    public int getCount() {
        return count;
    }

    public BuildTime getBuildTime() {
        return buildTime;
    }
}
