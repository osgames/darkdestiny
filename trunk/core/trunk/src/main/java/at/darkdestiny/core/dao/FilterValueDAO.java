/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.FilterValue;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Aion
 */
public class FilterValueDAO extends ReadWriteTable<FilterValue> implements GenericDAO {

    public FilterValue findById(int valueId){
        FilterValue searchfv = new FilterValue();
        searchfv.setId(valueId);
        return (FilterValue)get(searchfv);
    }

}
