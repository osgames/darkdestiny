/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ProductionOrderProgress;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ProductionOrderProgressDAO extends ReadWriteTable<ProductionOrderProgress> implements GenericDAO {
    public ProductionOrderProgress findById(Integer productionOrderId, Integer resourceId) {
        ProductionOrderProgress pop = new ProductionOrderProgress();
        pop.setProductionOrderId(productionOrderId);
        pop.setResourceId(resourceId);
        return (ProductionOrderProgress)get(pop);

    }

    public ArrayList<ProductionOrderProgress> findByProductionOrderId(Integer productionOrderId) {
        ProductionOrderProgress pop = new ProductionOrderProgress();
        pop.setProductionOrderId(productionOrderId);
        return (ArrayList<ProductionOrderProgress>) find(pop);
    }
}
