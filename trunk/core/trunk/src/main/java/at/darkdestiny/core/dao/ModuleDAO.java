/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Module;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class ModuleDAO extends ReadWriteTable<Module> implements GenericDAO {
    public Module findById(int id) {
        Module m = new Module();
        m.setId(id);
        return (Module)get(m);
    }
}
