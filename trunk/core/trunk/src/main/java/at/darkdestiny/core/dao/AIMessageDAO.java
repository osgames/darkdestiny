/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.AIMessage;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Aion
 */
public class AIMessageDAO extends ReadWriteTable<AIMessage> implements GenericDAO {
    public AIMessage findById(int id){
        AIMessage aim = new AIMessage();
        aim.setId(id);
        return (AIMessage)get(aim);
    }
}
