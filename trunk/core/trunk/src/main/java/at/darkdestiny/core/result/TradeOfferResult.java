/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class TradeOfferResult {
    private ArrayList<BaseResult> results;
    private ArrayList<TradeOfferEntry> offers;

    public TradeOfferResult(ArrayList<BaseResult> results, ArrayList<TradeOfferEntry> offers) {
        this.results = results;
        this.offers = offers;
    }

    /**
     * @return the results
     */
    public ArrayList<BaseResult> getResults() {
        if (results == null) return new ArrayList<BaseResult>();
        return results;
    }

    /**
     * @return the offers
     */
    public ArrayList<TradeOfferEntry> getOffers() {
        if (offers == null) return new ArrayList<TradeOfferEntry>();
        return offers;
    }
    
    
    
    
}
