    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.ELeadership;
import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceBoard;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.AllianceMessage;
import at.darkdestiny.core.model.AllianceMessageRead;
import at.darkdestiny.core.model.AllianceRank;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.VoteMetadata;
import at.darkdestiny.core.model.VoteOption;
import at.darkdestiny.core.model.VotePermission;
import at.darkdestiny.core.model.Voting;
import at.darkdestiny.core.result.AllianceStructureResult;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.utilities.TerritoryUtilities;
import at.darkdestiny.core.utilities.ViewTableUtilities;
import at.darkdestiny.core.voting.*;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 *
 * @author Eobane
 */
public class AllianceService extends Service {
    public final static int OWN_ALLIANCE_STRUCTURE = 0;
    public final static int OWN_ALLIANCE = 1;
    public final static int OWN_COUNCIL = 2;
    public final static int ALL_COUNCIL_AND_ADMINS = 3;
    public final static int ALL_ADMINS = 4;
    public final static int SPECIFIC_ALLIANCE = 5;
    public final static int SPECIFIC_ALLIANCE_ALLADMIN = 6;
    public final static int SPECIFIC_ALLIANCE_ALL_COUNCIL_AND_ADMIN = 7;
    
    public static ArrayList<AllianceBoard> findAllianceBoardByAllianceId(int allianceId) {
        return allianceBoardDAO.findByAllianceId(allianceId);
    }

    public static ArrayList<BaseResult> switchRecruiting(int userId) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        Alliance alliance = null;
        if (am != null) {
            alliance = allianceDAO.findById(am.getAllianceId());
        }

        if ((alliance == null) || (am == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }

        int allianceId = alliance.getId();

        if (am.getIsAdmin() && alliance.getId() == allianceId) {
            if (alliance.getIsRecruiting()) {
                alliance.setIsRecruiting(false);
            } else {
                alliance.setIsRecruiting(true);
            }
        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));
        }
        allianceDAO.update(alliance);
        return result;
    }

    public static Alliance getAllianceFromVote(int voteId) {
        Alliance a = null;
        ArrayList<VoteMetadata> vms = voteMetadataDAO.findByVoteId(voteId);
        if (vms != null) {
            for (VoteMetadata vmd : vms) {
                if (vmd.getDataName().equals("ALLIANCEID")) {
                    int allyId = Integer.parseInt(vmd.getDataValue());
                    a = allianceDAO.findById(allyId);

                }
            }
        }
        return a;
    }

    public static ArrayList<Voting> getExistingVotes(int userId) {
        ArrayList<Voting> vs = votingDAO.findByType(Voting.TYPE_MEMBERJOIN);
        if (vs != null) {
            ArrayList<Voting> result = new ArrayList<Voting>();
            for (Voting v : vs) {
                if (v.getClosed()) {
                    continue;
                }
                ArrayList<VoteMetadata> vms = voteMetadataDAO.findByVoteId(v.getVoteId());
                if (vms != null) {
                    for (VoteMetadata vmd : vms) {
                        if (vmd.getDataName().equals("USERID")) {
                            if (Integer.parseInt(vmd.getDataValue()) == userId) {
                                result.add(v);
                            }
                        }
                    }
                }
            }
            return result;
        } else {
            return null;
        }
    }

    public static ArrayList<BaseResult> sendAllianceMessage(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        String aName = "";
        String message = "";
        int receiver = -1;
        String topic = "";

        for (Object entry : params.entrySet()) {

            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            if (parName.equalsIgnoreCase("aname")) {                
                aName = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("comment")) {
                message = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("topic")) {
                topic = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("receiver")) {
                receiver = Integer.parseInt(((String[]) me.getValue())[0]);
            }
        }    
        
        if (receiver < 0) {
            result.add(new BaseResult("No receipient defined",true));
        }
        
        Alliance a;
        if (receiver >= 5) {
            // Check if aname is valid
            a = Service.allianceDAO.findAllianceByNameOrTag(aName);
            if (a == null) {
                result.add(new BaseResult("No or invalid alliance defined",true));
            }
        } else {
            a = Service.allianceDAO.findById(Service.allianceMemberDAO.findByUserId(userId).getAllianceId());
        }
        
        if (!result.isEmpty()) {
            return result;
        }

        // Collect all users and send Message
        ArrayList<AllianceMember> memberList = new ArrayList<AllianceMember>();
        ArrayList<Integer> userList = new ArrayList<Integer>();
        
        HashMap<Integer,Integer> allAlliances = AllianceService.getAllianceStructure(a);
        
        switch (receiver) {
            case(OWN_ALLIANCE_STRUCTURE):
                for (Integer aId : allAlliances.keySet()) {
                    ArrayList<AllianceMember> members = Service.allianceMemberDAO.findByAllianceId(aId);
                    for (AllianceMember member : members) {
                        userList.add(member.getUserId());
                    }
                }
                break;
            case(OWN_ALLIANCE):
            case(SPECIFIC_ALLIANCE):
                ArrayList<AllianceMember> members = Service.allianceMemberDAO.findByAllianceId(a.getId());
                for (AllianceMember member : members) {
                    userList.add(member.getUserId());
                }                
                break;                
            case(OWN_COUNCIL):
            case(SPECIFIC_ALLIANCE_ALL_COUNCIL_AND_ADMIN):
                members = Service.allianceMemberDAO.findByAllianceId(a.getId());
                for (AllianceMember member : members) {
                    if (member.getIsAdmin() || member.getIsCouncil()) {
                        userList.add(member.getUserId());
                    }
                }                
                break;                 
            case(ALL_COUNCIL_AND_ADMINS):
                for (Integer aId : allAlliances.keySet()) {
                    members = Service.allianceMemberDAO.findByAllianceId(aId);
                    for (AllianceMember member : members) {
                        if (member.getIsAdmin() || member.getIsCouncil()) {
                            userList.add(member.getUserId());
                        }
                    }
                }
                break;            
            case(ALL_ADMINS):
                for (Integer aId : allAlliances.keySet()) {
                    members = Service.allianceMemberDAO.findByAllianceId(aId);
                    for (AllianceMember member : members) {
                        if (member.getIsAdmin()) {
                            userList.add(member.getUserId());
                        }
                    }
                }
                break;        
            case(SPECIFIC_ALLIANCE_ALLADMIN):
                members = Service.allianceMemberDAO.findByAllianceId(a.getId());
                for (AllianceMember member : members) {
                    if (member.getIsAdmin()) {
                        userList.add(member.getUserId());
                    }
                }                
                break;                    
        }
        
        result.add(MessageService.writeAllianceMessageToUser(userId, userList, topic, message, false));
        
        return result;
    }
    
    public static ArrayList<BaseResult> joinAlliance(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        String aName = "";
        String message = "";

        for (Object entry : params.entrySet()) {

            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            if (parName.equalsIgnoreCase("aname")) {
                aName = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("message")) {
                message = ((String[]) me.getValue())[0];
            }
        }
        if (!aName.equals("")) {
            AllianceService.joinAlliance(userId, aName, message);

        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_fillallfields", userId), true));
        }
        return result;
    }

    /**
     *
     * @param userId
     * @param aname Can be name or tag of alliance
     * @param message
     */
    public static void joinAlliance(int userId, String aname, String message) {
        AllianceMember atmp = allianceMemberDAO.findByUserId(userId);
        if (atmp != null) {
            return;
        }
        UserDAO uDAO = userDAO;
        AllianceDAO aDAO = allianceDAO;
        AllianceMemberDAO amDAO = allianceMemberDAO;

        User u = uDAO.findById(userId);
        Alliance a = aDAO.findAllianceByNameOrTag(aname);
        for (Voting v : getExistingVotes(userId)) {
            Alliance ax = getAllianceFromVote(v.getVoteId());
            if (ax.getId().equals(a.getId())) {
                return;
            }
        }
        ArrayList<AllianceMember> memberList = amDAO.findByAllianceId(a.getId());

        try {
            Vote v = new Vote(userId, u.getGameName(), message, Voting.TYPE_MEMBERJOIN);

            ArrayList<Integer> receivers = new ArrayList<Integer>();

            for (AllianceMember am : memberList) {
                if (am.getIsAdmin() || (am.getIsCouncil() && !(a.getLeadership() == ELeadership.DICTORIAL))) {
                    receivers.add(am.getUserId());
                    v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                }
            }
            v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));
            v.setMinVotes((int) Math.ceil((double) receivers.size() / 2d));
            v.setTotalVotes(receivers.size());

            v.addOption(new VoteOption(ML.getMLStr("alliance_link_accept", userId), true, VoteOption.OPTION_TYPE_ACCEPT));
            v.addOption(new VoteOption(ML.getMLStr("alliance_link_decline", userId), false, VoteOption.OPTION_TYPE_DENY));



            v.addMetaData("USERID", u.getUserId(), MetaDataDataType.INTEGER);
            v.addMetaData("ALLIANCEID", a.getId(), MetaDataDataType.INTEGER);

            v.setReceivers(receivers);

            VotingFactory.persistVote(v);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while creating vote: ", e);
        }
    }

    public static void createTreaty(int subAllianceId, int joinAllianceId) {
        Alliance subAlliance = allianceDAO.findById(subAllianceId);
        int masterAlliance = allianceDAO.findById(joinAllianceId).getMasterAlliance();

        //Setting new masterallianceId of suballiances
        ViewTableUtilities.copyEntries(joinAllianceId, subAllianceId, ViewTableUtilities.Direction.ALLIANCE_TO_ALLIANCE, true);
        for (Alliance a : allianceDAO.findByMasterId(subAlliance.getId())) {
            a.setMasterAlliance(masterAlliance);
            allianceDAO.update(a);
        }

        long start = System.currentTimeMillis();
        subAlliance.setMasterAlliance(masterAlliance);
        subAlliance.setIsSubAllianceOf(joinAllianceId);
        allianceDAO.update(subAlliance);

    }

    public static ArrayList<BaseResult> createTreatyRequest(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        int joinAlly = 0;
        String message = "";
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            if (parName.equalsIgnoreCase("aname")) {
                joinAlly = (Integer.parseInt(((String[]) me.getValue())[0]));
            } else if (parName.equalsIgnoreCase("message")) {
                message = ((String[]) me.getValue())[0];
            }
        }

        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        Alliance alliance = null;
        if (am != null) {
            alliance = allianceDAO.findById(am.getAllianceId());
        }

        if ((alliance == null) || (am == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }

        int allianceId = alliance.getId();

        if (joinAlly > 0) {
            AllianceService.createTreatyRequest(userId, allianceId, joinAlly, message);
            result.add(new BaseResult(ML.getMLStr("alliance_msg_senttreatyrequest", userId), false));

        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_fillallfields", userId), true));
        }
        return result;
    }

    public static void createTreatyRequest(int voteStarter, int subAllyId, int joinAllyId, String message) {
        AllianceMemberDAO amDAO = Service.allianceMemberDAO;

        AllianceMember allianceMember = allianceMemberDAO.findByUserId(voteStarter);
        /**
         * @TODO Rightsmanagement
         *
         */
        if (!allianceMember.getIsAdmin() || allianceMember.getAllianceId() != subAllyId) {
            DebugBuffer.addLine(DebugLevel.ERROR, "An alliance member NOT AN ADMIN wants to create a treaty request");
        } else {
            Alliance subAlliance = allianceDAO.findById(subAllyId);
            Alliance joinAlliance = allianceDAO.findById(joinAllyId);
            ArrayList<AllianceMember> memberList = amDAO.findByAllianceId(joinAlliance.getId());

            try {
                Vote v = new Vote(voteStarter, subAlliance.getName(), message, Voting.TYPE_ALLIANCEJOIN);

                ArrayList<Integer> receivers = new ArrayList<Integer>();

                for (AllianceMember am : memberList) {
                    /**
                     * @TODO Rightsmanagement
                     */
                    if (am.getIsAdmin()) {
                        receivers.add(am.getUserId());
                        v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                    }
                }

                v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));
                v.setMinVotes(receivers.size());
                v.setTotalVotes(receivers.size());

                v.addOption(new VoteOption("Akzeptieren", true, VoteOption.OPTION_TYPE_ACCEPT));
                v.addOption(new VoteOption("Ablehnen", false, VoteOption.OPTION_TYPE_DENY));

                v.addMetaData("SUBALLIANCEID", subAlliance.getId(), MetaDataDataType.INTEGER);
                v.addMetaData("JOINALLIANCEID", joinAlliance.getId(), MetaDataDataType.INTEGER);

                v.setReceivers(receivers);

                VotingFactory.persistVote(v);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while creating vote: ", e);
            }
        }
    }

    public static ArrayList<BaseResult> setUserRank(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        Alliance alliance = null;
        if (allianceMember != null) {
            alliance = allianceDAO.findById(allianceMember.getAllianceId());
        }

        if ((alliance == null) || (allianceMember == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }

        int allianceId = alliance.getId();

        boolean specialRank = false;

        int status = 0;
        int targetUserId = 0;
        String rankIdString = "";
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            if (parName.equalsIgnoreCase("Status")) {
                rankIdString = ((String[]) me.getValue())[0];
                if (rankIdString.startsWith("x")) {
                    rankIdString = rankIdString.substring(1);
                    specialRank = true;
                }
                status = Integer.parseInt(rankIdString);
            } else if (parName.equalsIgnoreCase("targetUserId")) {
                targetUserId = Integer.parseInt(((String[]) me.getValue())[0]);
            }
        }

        // Get Change Permissions
        int newStatus = -1;
        HashSet<Integer> possibleSelections = AllianceService.getSelectableRank(userId, targetUserId);
        if (specialRank) {
            AllianceRank arank = allianceRankDAO.findBy(Integer.parseInt(rankIdString), allianceId);
            newStatus = arank.getComparisonRank();
        } else {
            newStatus = status;
        }

        // This change is not authorized ;)
        if (!possibleSelections.contains(newStatus)) {
            result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));
            return result;
        }

        boolean voteCreated = AllianceVoteService.createUserChangeVote(userId, targetUserId, status, specialRank);

        if (!voteCreated) {
            AllianceService.setUserRank(targetUserId, status, specialRank);
            result.add(new BaseResult(ML.getMLStr("alliance_info_userrights_changed", userId), false));
        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_info_userrights_votecreated", userId), false));
        }

        return result;
    }

    public static void setUserRank(int userId, int rankId, boolean specialRank) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();

        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        Alliance a = allianceDAO.findById(am.getAllianceId());
        boolean updateViewTables = true;


        // This user already had full visibility rights so no changes
        updateViewTables = updateViewTables && !(am.getIsAdmin() || am.getIsCouncil() || !am.getIsTrial());
        if (updateViewTables) {
            // if old rank is TRIAL and new Rank is higher transfer tables
            if (specialRank) {
                AllianceRank ar = allianceRankDAO.findBy(rankId, a.getId());
                if (ar.getComparisonRank() < AllianceRank.RANK_MEMBER_INT) {
                    updateViewTables = false;
                }
            }
        }

        try {
            th.startTransaction();

            if (specialRank) {
                AllianceRank ar = allianceRankDAO.findBy(rankId, am.getAllianceId());

                if (ar != null) {
                    am.setSpecialRankId(rankId);

                    switch (ar.getComparisonRank()) {
                        case (AllianceRank.RANK_ADMIN_INT):
                            am.setIsAdmin(true);
                            am.setIsCouncil(true);
                            am.setIsTrial(false);
                            break;
                        case (AllianceRank.RANK_COUNCIL_INT):
                            if (am.getIsAdmin()) {
                                break;
                            }
                            am.setIsAdmin(false);
                            am.setIsCouncil(true);
                            am.setIsTrial(false);
                            break;
                        case (AllianceRank.RANK_MEMBER_INT):
                            if (am.getIsAdmin()) {
                                break;
                            }
                            am.setIsAdmin(false);
                            am.setIsCouncil(false);
                            am.setIsTrial(false);
                            break;
                        case (AllianceRank.RANK_TRIAL_INT):
                            if (am.getIsAdmin()) {
                                break;
                            }
                            am.setIsAdmin(false);
                            am.setIsCouncil(false);
                            am.setIsTrial(true);
                            break;
                    }
                }
            } else {
                am.setSpecialRankId(0);

                switch (rankId) {
                    case (AllianceRank.RANK_ADMIN_INT):
                        am.setIsAdmin(true);
                        am.setIsCouncil(true);
                        am.setIsTrial(false);
                        break;
                    case (AllianceRank.RANK_COUNCIL_INT):
                        // if (am.getIsAdmin()) {
                        //    break;
                        //}
                        am.setIsAdmin(false);
                        am.setIsCouncil(true);
                        am.setIsTrial(false);
                        break;
                    case (AllianceRank.RANK_MEMBER_INT):
                        //if (am.getIsAdmin()) {
                        //    break;
                        //}
                        am.setIsAdmin(false);
                        am.setIsCouncil(false);
                        am.setIsTrial(false);
                        break;
                    case (AllianceRank.RANK_TRIAL_INT):
                        //if (am.getIsAdmin()) {
                        //    break;
                        //}
                        am.setIsAdmin(false);
                        am.setIsCouncil(false);
                        am.setIsTrial(true);
                        break;
                }
            }

            //Update TerritoryEntries needed if demote and promote
            try {
                TerritoryUtilities.updateAllTerritoryPermissions();
            } catch (Exception e) {

                DebugBuffer.writeStackTrace("Error in AllianceService setUserRank: Could not update TerritoryEntries -> " + e.getMessage() + ":<BR>", e);
            }
            allianceMemberDAO.update(am);
        } catch (Exception e) {
            e.printStackTrace();
            th.rollback();
        } finally {
            th.endTransaction();
            if (updateViewTables) {
                ViewTableUtilities.copyEntries(a.getId(), userId, ViewTableUtilities.Direction.ALLIANCE_TO_USER, false);
            }
        }
    }

    public static ArrayList<BaseResult> createRank(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        Alliance alliance = null;
        if (allianceMember != null) {
            alliance = allianceDAO.findById(allianceMember.getAllianceId());
        }

        if ((alliance == null) || (allianceMember == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }

        int allianceId = alliance.getId();

        String rankName = "";


        int compRank = 0;


        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();


            if (parName.equalsIgnoreCase("rankName")) {
                rankName = ((String[]) me.getValue())[0];
                for (AllianceRank ar : allianceRankDAO.findByAllianceId(allianceId)) {
                    if (ar.getRankName().equalsIgnoreCase(rankName.trim())) {
                        BaseResult br = new BaseResult(ML.getMLStr("alliance_err_ranknamealreadyexists", userId), true);
                        result.add(br);
                        return result;
                    }
                }

            } else if (parName.equalsIgnoreCase("compRank")) {
                compRank = Integer.parseInt(((String[]) me.getValue())[0]);


            }
        }
        if (!rankName.equals("") && compRank > 0) {
            if (allianceMember.getIsAdmin()) {
                AllianceService.createRank(rankName, compRank, allianceId);


            } else {
                result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));


            }
        }
        return result;


    }

    public static void createRank(String rankName, int compRank, int allianceId) {
        AllianceRank tmpRank = allianceRankDAO.findByAllianceIdLastRank(allianceId);
        AllianceRank ar = new AllianceRank();
        ar.setAllianceId(allianceId);

        if (tmpRank != null) {
            ar.setRankId(tmpRank.getRankId() + 1);
        } else {
            ar.setRankId(1);
        }

        ar.setComparisonRank(compRank);
        ar.setRankName(rankName);
        allianceRankDAO.add(ar);
    }

    public static ArrayList<BaseResult> deleteRank(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        int rankId = 0;
        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);

        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();

            if (parName.equalsIgnoreCase("rankId")) {
                rankId = Integer.parseInt(((String[]) me.getValue())[0]);
            }
        }

        if (allianceMember.getIsAdmin()) {
            AllianceService.deleteRank(rankId, allianceMember.getAllianceId());
        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));
        }

        return result;
    }

    public static void deleteRank(int rankId, int allianceId) {
        ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(allianceId);

        for (AllianceMember am : ams) {
            if (am.getSpecialRankId() == rankId) {
                am.setSpecialRankId(0);
                allianceMemberDAO.update(am);
            }
        }

        AllianceRank ar = allianceRankDAO.findBy(rankId, allianceId);
        allianceRankDAO.remove(ar);
    }

    public static ArrayList<BaseResult> deleteTreaty(int userId) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        boolean allowed = false;

        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        Alliance alliance = null;
        if (am != null) {
            alliance = allianceDAO.findById(am.getAllianceId());
        }

        if ((alliance == null) || (am == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }

        int allianceId = alliance.getId();

        if (am != null) {
            if (am.getIsAdmin() && am.getAllianceId() == allianceId) {
                allowed = true;
            }
        }

        if (!allowed) {
            result.add(new BaseResult("NO PERMISSION", true));
            return result;
        }

        // Has the current Alliance subAlliances if yes update their masterAllianceId To Current Alliance
        Alliance a = allianceDAO.findById(allianceId);
        a.setMasterAlliance(a.getId());
        a.setIsSubAllianceOf(0);
        allianceDAO.update(a);

        ArrayList<Alliance> aList = allianceDAO.findByIsSubAllianceOf(a.getId());
        for (Alliance aTmp : aList) {
            aTmp.setMasterAlliance(a.getId());
            allianceDAO.update(aTmp);
        }

        result.add(new BaseResult(ML.getMLStr("alliance_msg_treatydeleted", userId), false));
        return result;
    }

    /*
     * private static void deleteTreaty(int userId, int fromAllianceId, int
     * deleteAllianceId) { Alliance fromAlliance =
     * allianceDAO.findById(fromAllianceId); Alliance deleteAlliance =
     * allianceDAO.findById(deleteAllianceId); // If the alliance is really a
     * suballiance of the alliance to delete
     *
     * if (deleteAlliance.getIsSubAllianceOf() == fromAlliance.getId()) {
     * //Reset masteralliance to itself
     * deleteAlliance.setMasterAlliance(deleteAlliance.getId()); //Reset
     * issuballiance to "0" deleteAlliance.setIsSubAllianceOf(0);
     * HashMap<Integer, Integer> suballys = breadthFirstSearch(deleteAlliance);
     * //Reset MasterAlliance of all suballiances
     *
     * for (Map.Entry<Integer, Integer> entry : suballys.entrySet()) { Alliance
     * a = allianceDAO.findById(entry.getKey());
     * a.setMasterAlliance(deleteAlliance.getId()); allianceDAO.update(a); } }
     *
     * allianceDAO.update(deleteAlliance); }
     */
    public static ArrayList<BaseResult> updateAllianceProfile(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        String aName = "";
        String aTag = "";
        String aPic = "";
        String aDesc = "";

        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();

            if (parName.equalsIgnoreCase("aname")) {
                aName = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("atag")) {
                aTag = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("apic")) {
                aPic = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("desc")) {
                aDesc = ((String[]) me.getValue())[0];
                aDesc = FormatUtilities.killJavaScript(aDesc);
            }
        }

        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        Alliance alliance = null;
        if (am != null) {
            alliance = allianceDAO.findById(am.getAllianceId());
        }

        if ((alliance == null) || (am == null)) {
            result.add(new BaseResult("Invalid call", true));
            return result;
        }

        int allianceId = alliance.getId();

        if (!aTag.equals("") && !aName.equals("")) {
            if (am.getIsAdmin() && am.getAllianceId() == allianceId) {
                AllianceService.updateAllianceProfile(allianceId, aName, aTag, aDesc, aPic);
            } else {
                result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));
            }
        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_fillallfields", userId), true));
        }

        return result;
    }

    public static void updateAllianceProfile(int allianceId, String allianceName, String allianceTag, String description, String picture) {
        Alliance alliance = allianceDAO.findById(allianceId);
        alliance.setDescription(description);
        alliance.setTag(allianceTag);
        alliance.setName(allianceName);
        alliance.setPicture(picture);
        allianceDAO.update(alliance);
    }

    public static Alliance findAllianceByNameOrTag(String name) {
        return allianceDAO.findAllianceByNameOrTag(name);
    }

    public static ArrayList<BaseResult> createAlliance(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        String aName = "";
        String aTag = "";
        String aPic = "";
        String aDesc = "";

        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();

            if (parName.equalsIgnoreCase("aname")) {
                aName = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("atag")) {
                aTag = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("apic")) {
                aPic = ((String[]) me.getValue())[0];
            } else if (parName.equalsIgnoreCase("desc")) {
                aDesc = ((String[]) me.getValue())[0];
            }
        }

        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);

        if (!aTag.equals("") && !aName.equals("")) {
            if (AllianceService.findAllianceByNameOrTag(aName) != null || AllianceService.findAllianceByNameOrTag(aTag) != null) {
                result.add(new BaseResult(ML.getMLStr("alliance_err_alliancenameortagexists", userId), true));
            } else {
                if (allianceMember == null) {
                    AllianceService.createAlliance(userId, aName, aTag, aDesc, aPic);
                    allianceMember = AllianceService.findAllianceMemberByUserId(userId);
                } else {
                    result.add(new BaseResult(ML.getMLStr("alliance_err_alreadyinalliance", userId), true));
                }
            }
        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_fillallfields", userId), true));
        }
        return result;
    }

    public static String createAlliance(int userId, String allianceName, String allianceTag, String description, String picture) {

        if (allianceDAO.findAllianceByNameOrTag(allianceName) != null || allianceDAO.findAllianceByNameOrTag(allianceTag) != null) {
            return "alliance_err_tagnameexists";
        }

        Alliance a = new Alliance();
        a.setUserId(userId);
        a.setName(allianceName);
        a.setTag(allianceTag);
        a.setPicture(picture);
        a.setDescription(description);
        a.setIsRecruiting(true);
        a.setIsSubAllianceOf(0);
        a.setMasterAlliance(0);
        a = allianceDAO.add(a);
        a.setMasterAlliance(a.getId());
        a = allianceDAO.update(a);

        AllianceMember am = new AllianceMember();
        am.setAllianceId(a.getId());
        am.setUserId(userId);
        am.setJoinedOn(System.currentTimeMillis());
        am.setLastRead(System.currentTimeMillis());
        am.setIsAdmin(true);
        am.setIsCouncil(false);
        am.setIsTrial(false);
        am.setSpecialRankId(0);

        allianceMemberDAO.add(am);

        return "global_done";
    }

    public static void addUserToAlliance(int allianceId, int userId) {
        AllianceMember existing = allianceMemberDAO.findByUserId(userId);

        if (existing != null) {
            return;
        }

        AllianceMember newMember = new AllianceMember();
        newMember.setAllianceId(allianceId);
        newMember.setIsAdmin(false);
        newMember.setIsCouncil(false);
        newMember.setIsTrial(true);
        newMember.setJoinedOn(java.lang.System.currentTimeMillis());
        newMember.setUserId(userId);

        //Updaten der Viewtables f?r die Allianzmitglieder
        DebugBuffer.addLine(DebugLevel.UNKNOWN, "UserId=" + userId + " AllianceId=" + allianceId);

        //Hierbei wird die Viewtable des Trials allen anderen Allianzmitglieder die > Trial sind
        //geshared

        allianceMemberDAO.add(newMember);
        ViewTableUtilities.copyEntries(userId, allianceId, ViewTableUtilities.Direction.USER_TO_ALLIANCE, false);

        //@TODO Aufr?umen mit anderen Alliance Votes


    }

    public static ArrayList<BaseResult> deleteUserFromAlliance(int userId, Map params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        int targetUserId = 0;
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            if (parName.equalsIgnoreCase("targetUserId")) {
                targetUserId = Integer.parseInt(((String[]) me.getValue())[0]);
            }
        }

        if (allianceMember.getIsAdmin() && targetUserId > 0) {
            if (AllianceService.findAllianceMemberByUserId(targetUserId).getAllianceId().equals(allianceMember.getAllianceId())) {
                AllianceService.leaveAlliance(targetUserId);
            } else {
                result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));
            }
        } else {
            result.add(new BaseResult(ML.getMLStr("alliance_err_notenoughrights", userId), true));
        }
        return result;
    }

    public static void leaveAlliance(int userId) {
        AllianceMember am = AllianceService.findAllianceMemberByUserId(userId);

        if (!am.getIsAdmin()) {
            ArrayList<AllianceMessageRead> abrs = allianceMessageReadDAO.findByAllianceIdUserId(am.getAllianceId(), userId);

            for (AllianceMessageRead abr : abrs) {
                allianceMessageReadDAO.remove(abr);
            }
            ArrayList<AllianceMessage> ams = allianceMessageDAO.findByUserId(userId);

            for (AllianceMessage amsg : ams) {
                amsg.setUserId(AllianceMessage.USER_DELETED);
                allianceMessageDAO.update(amsg);
            }
            //Update TerritoryEntries
            try {
                TerritoryUtilities.updateAllTerritoryPermissions();
            } catch (Exception e) {

                DebugBuffer.writeStackTrace("Error in AllianceService leaveAlliance: Could not update TerritoryEntries -> " + e.getMessage() + ":<BR>", e);
            }
            allianceMemberDAO.remove(am);
        } else {
            removeUserFromAlliance(am);
        }
    }

    public static ArrayList<BaseResult> removeUserFromAlliance(AllianceMember allianceMember) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        final int admin = 0;
        final int council = 1;
        final int member = 2;
        final int trial = 3;

        int allianceId = allianceMember.getAllianceId();
        Alliance a = allianceDAO.findById(allianceId);

        //Check if allianceMember was founder
        boolean founder = false;
        //Update TerritoryEntries
        try {
            TerritoryUtilities.updateAllTerritoryPermissions();
        } catch (Exception e) {

            DebugBuffer.writeStackTrace("Error in AllianceService removeUserFromAlliance: Could not update TerritoryEntries -> " + e.getMessage() + ":<BR>", e);
        }
        allianceMemberDAO.remove(allianceMember);

        ArrayList<AllianceMessageRead> abrs = allianceMessageReadDAO.findByAllianceIdUserId(allianceMember.getAllianceId(), allianceMember.getUserId());

        for (AllianceMessageRead abr : abrs) {
            allianceMessageReadDAO.remove(abr);
        }
        ArrayList<AllianceMember> allianceMembers = allianceMemberDAO.findByAllianceId(allianceId);
        //Suche ob Admin vorhanden wenn nicht promoten;
        if (allianceMembers.size() > 0) {
            ArrayList<AllianceMessage> ams = allianceMessageDAO.findByUserId(allianceMember.getUserId());

            for (AllianceMessage amsg : ams) {
                amsg.setUserId(AllianceMessage.USER_DELETED);
                allianceMessageDAO.update(amsg);
            }

            if (founder) {
                int highestRank = trial;
                HashMap<AllianceMember, Integer> ranks = new HashMap<AllianceMember, Integer>();

                for (AllianceMember am : allianceMembers) {
                    if (am.getIsAdmin() && am.getIsCouncil()) {
                        if (admin < highestRank) {
                            highestRank = admin;
                        }
                        ranks.put(am, admin);
                    } else if (!am.getIsAdmin() && am.getIsCouncil()) {
                        ranks.put(am, council);

                        if (council < highestRank) {
                            highestRank = council;
                        }
                    } else if (!am.getIsAdmin() && !am.getIsCouncil() && !am.getIsTrial()) {
                        ranks.put(am, member);

                        if (member < highestRank) {
                            highestRank = member;
                        }
                    } else if (am.getIsTrial()) {
                        ranks.put(am, trial);
                    }
                }

                ArrayList<AllianceMember> successors = new ArrayList<AllianceMember>();
                //Search for the oldest member to be Founder and admin

                for (Map.Entry<AllianceMember, Integer> entry : ranks.entrySet()) {
                    if (entry.getValue() == highestRank) {
                        successors.add(entry.getKey());
                    }
                }

                AllianceMember oldest = null;

                for (AllianceMember am : successors) {
                    if (oldest == null) {
                        oldest = am;
                    } else {
                        if (am.getJoinedOn() < oldest.getJoinedOn()) {
                            oldest = am;
                        }
                    }
                }

                a.setUserId(oldest.getUserId());
                allianceDAO.update(a);
                //Promote to Admin
                oldest.setIsAdmin(true);
                oldest.setIsCouncil(true);
                oldest.setIsTrial(false);

                allianceMemberDAO.update(oldest);
            } //Allianz hat mehr keine Mitglieder => L�schen
        } else {
            disbandAlliance(allianceId);
        }
        return result;
    }

    public static void deleteAlliance(int userId) {
        AllianceMember allianceMember = allianceMemberDAO.findByUserId(userId);
        Alliance alliance = null;
        if (allianceMember != null) {
            alliance = allianceDAO.findById(allianceMember.getAllianceId());
        }

        if ((alliance == null) || (allianceMember == null)) {
            //result.add(new BaseResult("Invalid call",true));
            //return result;
        }

        int allianceId = alliance.getId();

        if (!allianceMember.getIsAdmin() || allianceMember.getAllianceId() != allianceId) {
            //return;
        } else {
            disbandAlliance(allianceId);
        }
    }

    public static void disbandAlliance(int allianceId) {
        Alliance a = allianceDAO.findById(allianceId);

        //Umstellen der MasterallianceID m?glicher Suballianzen
        //Alliance was a suballiance
        if (a.getIsSubAllianceOf() > 0) {
            int subAllianceId = a.getIsSubAllianceOf();
            a.setMasterAlliance(a.getId());

            HashMap<Integer, Integer> suballys = breadthFirstSearch(a);

            if (suballys.size() > 0) {
                for (Map.Entry<Integer, Integer> entry : suballys.entrySet()) {
                    if (a.getId().equals(entry.getKey())) {
                        continue;
                    } else {
                        Alliance atmp2 = allianceDAO.findById(entry.getKey());
                        //Eine Suballianz von dem neuen Zweig => ?bertragen der neuen MasterId

                        if (atmp2.getIsSubAllianceOf() == allianceId) {
                            atmp2.setIsSubAllianceOf(subAllianceId);
                            allianceDAO.update(atmp2);
                        }
                    }
                }
            }
        } else {
            //Alliance itself is masteralliance
            ArrayList<Alliance> nextLayer = allianceDAO.findByIsSubAllianceOf(a.getId());
            //Suballianzen der aufgel?sten Allianz

            for (Alliance atmp : nextLayer) {
                //Allianzen sind nun Eigenst?ndig
                atmp.setIsSubAllianceOf(0);
                atmp.setMasterAlliance(atmp.getId());

                HashMap<Integer, Integer> suballys2 = breadthFirstSearch(atmp);

                if (suballys2.size() > 0) {
                    for (Map.Entry<Integer, Integer> entry : suballys2.entrySet()) {
                        if (atmp.getId().equals(entry.getKey())) {
                            continue;
                        } else {
                            Alliance atmp2 = allianceDAO.findById(entry.getKey());
                            //Eine Suballianz von dem neuen Zweig => ?bertragen der neuen MasterId

                            if (atmp2.getIsSubAllianceOf().equals(atmp.getId())) {
                                atmp2.setIsSubAllianceOf(atmp.getMasterAlliance());
                            }

                            atmp2.setMasterAlliance(atmp.getMasterAlliance());
                            allianceDAO.update(atmp2);
                        }
                    }
                }

                allianceDAO.update(atmp);
            }
        }

        //L?schen aller Allianzspezifischen Daten
        ArrayList<AllianceMember> ams = allianceMemberDAO.findByAllianceId(allianceId);
        for (AllianceMember am : ams) {
            allianceMemberDAO.remove(am);
        }

        ArrayList<AllianceMessage> amsgs = allianceMessageDAO.findByAllianceId(allianceId);
        for (AllianceMessage amsg : amsgs) {
            allianceMessageDAO.remove(amsg);
        }

        ArrayList<AllianceBoard> abs = allianceBoardDAO.findByAllianceId(allianceId);
        for (AllianceBoard ab : abs) {
            allianceBoardDAO.remove(ab);
        }

        ArrayList<AllianceMessageRead> amrs = allianceMessageReadDAO.findByAllianceId(allianceId);
        for (AllianceMessageRead amr : amrs) {
            allianceMessageReadDAO.remove(amr);
        }

        ArrayList<AllianceRank> ars = allianceRankDAO.findByAllianceId(allianceId);
        for (AllianceRank ar : ars) {
            allianceRankDAO.remove(ar);
        }

        allianceDAO.remove(a);
        //L?schen aller diplomatischen Beziehungen mit dieser Allianz
        DiplomacyService.removeAlliance(allianceId);
    }

    public static HashMap<Integer, Integer> getAllianceStructure(Alliance alliance) {
        Alliance masterAlliance = findAllianceByAllianceId(alliance.getMasterAlliance());
        HashMap<Integer, Integer> allys = new HashMap<Integer, Integer>();
        subSearch(masterAlliance, allys);
        allys.put(masterAlliance.getId(),0);
        return allys;
    }

    public static HashMap<Integer, Integer> subSearch(Alliance alliance, HashMap<Integer, Integer> allys) {
        ArrayList<Alliance> as = findSubAlliancesOf(alliance);

        for (Alliance a : as) {
            subSearch(a, allys);

            // Hat dieser Knoten noch Kinder?
            if (findSubAlliancesOf(a).isEmpty()) {
                //Wenn bereits vorhanden
                if (allys.get(alliance.getId()) != null) {
                    allys.put(alliance.getId(), (allys.get(alliance.getId()) + 1));
                    allys.put(a.getId(), 1);
                } else {
                    allys.put(a.getId(), 1);
                    allys.put(alliance.getId(), 1);
                }
            } else {
                if (allys.get(alliance.getId()) != null) {
                    allys.put(alliance.getId(), (allys.get(alliance.getId()) + allys.get(a.getId())));
                } else {
                    allys.put(alliance.getId(), (allys.get(a.getId())));
                }
            }
        }

        return allys;
    }

    public static HashMap<Integer, Integer> breadthFirstSearch(Alliance alliance) {
        Alliance masterAlliance = allianceDAO.findById(alliance.getMasterAlliance());
        HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();
        Queue<Alliance> aqueue = new LinkedList<Alliance>();

        int depth = 0;
        aqueue.add(masterAlliance);
        result.put(masterAlliance.getId(), depth);
        depth++;

        while (aqueue.size() > 0) {
            Alliance a = aqueue.poll();

            for (Alliance atemp : allianceDAO.findByIsSubAllianceOf(a.getId())) {
                aqueue.add(atemp);
                result.put(atemp.getId(), depth);
            }

            depth++;
        }

        return result;
    }

    public static ArrayList<Alliance> findSubAlliancesOf(Alliance alliance) {
        return allianceDAO.findByIsSubAllianceOf(alliance.getId());
    }

    public static ArrayList<Alliance> findAllAlliancesByMasterId(Integer masterAlliance) {
        return allianceDAO.findByMasterId(masterAlliance);
    }

    public static User findUserById(Integer userId) {
        return userDAO.findById(userId);
    }

    public static AllianceRank findAllianceRankBy(Integer rankId, Integer allianceId) {
        return allianceRankDAO.findBy(rankId, allianceId);
    }

    public static ArrayList<AllianceRank> findAllianceRankByAllianceId(Integer allianceId) {
        return allianceRankDAO.findByAllianceId(allianceId);
    }

    public static AllianceStructureResult getAllianceStructure(int allianceId) {
        try {
            Alliance actAlliance = allianceDAO.findById(allianceId);
            int masterAllianceId = actAlliance.getMasterAlliance();

            ArrayList<Alliance> subAlliances = new ArrayList<Alliance>();
            for (Alliance aTmp : (ArrayList<Alliance>) allianceDAO.findAll()) {
                if ((aTmp.getMasterAlliance() == masterAllianceId) && (aTmp.getId() != masterAllianceId)) {
                    subAlliances.add(aTmp);
                }
            }

            ArrayList<Alliance> equallyAllied = new ArrayList<Alliance>();

            ArrayList<Integer> tmpAllList = DiplomacyService.getAllAlliedAllianceIds(masterAllianceId);
            for (Alliance aTmp : subAlliances) {
                tmpAllList.addAll(DiplomacyService.getAllAlliedAllianceIds(aTmp.getId()));
            }

            // recursivly loop list till no new allied are found
            boolean found = false;
            do {
                found = false;
                ArrayList<Integer> newRelations = new ArrayList<Integer>();

                for (Integer aId : tmpAllList) {
                    newRelations.addAll(DiplomacyService.getAllAlliedAllianceIds(aId));
                }

                for (Integer aId : newRelations) {
                    if (!tmpAllList.contains(aId)) {
                        tmpAllList.add(aId);
                        found = true;
                    }
                }
            } while (found);

            // Get subAlliance structure for other equally allied alliances
            for (Integer aId : tmpAllList) {
                Alliance aTmp = allianceDAO.findById(aId);
                for (Alliance aTmp2 : (ArrayList<Alliance>) allianceDAO.findAll()) {
                    if ((aTmp2.getMasterAlliance().equals(aTmp.getMasterAlliance())) && (!aTmp2.getId().equals(aTmp.getMasterAlliance()))) {
                        subAlliances.add(aTmp2);
                    }
                }
            }

            // Clean up all alliances already contained in subAlliance / masterAllianceId
            if (tmpAllList.remove(new Integer(masterAllianceId)));

            for (Alliance aTmp : subAlliances) {
                tmpAllList.remove(aTmp.getId());
            }

            for (Integer aId : tmpAllList) {
                equallyAllied.add(allianceDAO.findById(aId));
            }

            AllianceStructureResult asr = new AllianceStructureResult(masterAllianceId, subAlliances, equallyAllied);
            return asr;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Exception in getAllianceStructure [PAR=" + allianceId + "]: ", e);
        }

        return new AllianceStructureResult(0, new ArrayList<Alliance>(), new ArrayList<Alliance>());
    }

    public static ArrayList<Alliance> findJoinableAlliances(int allianceId) {
        try {
            Alliance alliance = allianceDAO.findById(allianceId);
            ArrayList<Alliance> allys = allianceDAO.findAll();
            HashMap<Integer, Integer> subs = new HashMap<Integer, Integer>();
            subs = breadthFirstSearch(alliance);
            allys.remove(alliance);

            for (Map.Entry<Integer, Integer> entry : subs.entrySet()) {
                if (entry.getKey() == allianceId) {
                    continue;
                }
                for (Iterator<Alliance> aIt = allys.iterator(); aIt.hasNext();) {
                    Alliance a = aIt.next();
                    if (a.getId().equals(entry.getKey())) {
                        aIt.remove();
                    }
                }
            }

            return allys;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in findJoinableAlliances: ", e);
        }
        return new ArrayList<Alliance>();
    }

    public static ArrayList<Alliance> findAllAlliances() {
        return allianceDAO.findAll();
    }

    public static AllianceMember findAllianceMemberByUserId(Integer userId) {
        return allianceMemberDAO.findByUserId(userId);
    }

    public static ArrayList<AllianceMember> findAllianceMemberByAllianceId(Integer allianceId) {
        return allianceMemberDAO.findByAllianceId(allianceId);
    }

    public static Alliance findAllianceByAllianceId(Integer allianceId) {
        return allianceDAO.findById(allianceId);
    }

    public static ArrayList<AllianceMessage> findAllianceMessageByAllianceId(Integer allianceId) {
        return allianceMessageDAO.findByAllianceId(allianceId);
    }

    public static void updateAllianceMember(AllianceMember allianceMember) {
        allianceMemberDAO.update(allianceMember);
    }

    public static BaseResult changeLeaderShip(int reqUserId, String newType) {
        AllianceMember am = allianceMemberDAO.findByUserId(reqUserId);
        Alliance a = allianceDAO.findById(am.getAllianceId());

        ELeadership oldRank = a.getLeadership();
        ELeadership newRank = ELeadership.valueOf(newType);

        if (oldRank == ELeadership.DICTORIAL) {
            // No voting is required => just process
            a.setLeadership(newRank);
            allianceDAO.update(a);
        } else {
            // All members have to vote with 2/3 majority
            AllianceVoteService.createLeadershipChangeVote(reqUserId, newRank);
        }

        return null;
    }

    public static HashSet<Integer> getSelectableRank(int reqUserId, int targetUserId) {
        HashSet<Integer> possibleSelections = new HashSet<Integer>();

        // If current User is Admin all ranks are possible on every user
        AllianceMember am = allianceMemberDAO.findByUserId(reqUserId);
        AllianceMember amTarget = allianceMemberDAO.findByUserId(targetUserId);

        int reqRank = getRank(am);
        int targetRank = getRank(amTarget);

        // Invalid request return empty map
        if ((am == null) || (amTarget == null) || (!am.getAllianceId().equals(amTarget.getAllianceId()))) {
            return possibleSelections;
        }

        // Trials have no rights at all
        if (reqRank == AllianceRank.RANK_TRIAL_INT) {
            return possibleSelections;
        }

        Alliance a = allianceDAO.findById(am.getAllianceId());

        if (reqUserId == targetUserId) { // Concil and Admin may demote themselves
            if (am.getIsAdmin()) {
                possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                possibleSelections.add(AllianceRank.RANK_COUNCIL_INT);
                possibleSelections.add(AllianceRank.RANK_MEMBER_INT);
                possibleSelections.add(AllianceRank.RANK_TRIAL_INT);
            } else if (am.getIsCouncil()) {
                possibleSelections.add(AllianceRank.RANK_MEMBER_INT);
                possibleSelections.add(AllianceRank.RANK_TRIAL_INT);
            }

            // If there is no admin it may be proposed
            if (!allianceHasActiveAdmin(a.getId())) {
                possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
            }
        } else {
            if (a.getLeadership() == ELeadership.DEMOCRATIC) {
                // All ranks lower or equal than the rank of the requested can be set
                if (targetRank >= AllianceRank.RANK_ADMIN_INT) {
                    possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                }
                if (targetRank >= AllianceRank.RANK_COUNCIL_INT) {
                    possibleSelections.add(AllianceRank.RANK_COUNCIL_INT);
                }
                if (targetRank >= AllianceRank.RANK_MEMBER_INT) {
                    possibleSelections.add(AllianceRank.RANK_MEMBER_INT);
                }
                if (targetRank >= AllianceRank.RANK_TRIAL_INT) {
                    possibleSelections.add(AllianceRank.RANK_TRIAL_INT);
                }

                // If requester is council or admin higher ranks are possible
                if (reqRank >= AllianceRank.RANK_COUNCIL_INT) {
                    possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                    possibleSelections.add(AllianceRank.RANK_COUNCIL_INT);
                    possibleSelections.add(AllianceRank.RANK_MEMBER_INT);
                    possibleSelections.add(AllianceRank.RANK_TRIAL_INT);
                }

                if (!allianceHasActiveAdmin(a.getId()) && !allianceHasActiveCouncil(a.getId())) {
                    possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                }
            } else if (a.getLeadership() == ELeadership.ARISTOCRATIC) {
                // Members have no rights here as long there are still active council and admin members
                if (reqRank <= AllianceRank.RANK_MEMBER_INT) {
                    return possibleSelections;
                }

                if (targetRank >= AllianceRank.RANK_ADMIN_INT) {
                    possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                }
                if (targetRank >= AllianceRank.RANK_COUNCIL_INT) {
                    possibleSelections.add(AllianceRank.RANK_COUNCIL_INT);
                }

                // If requester is council or admin higher ranks are possible
                if (reqRank >= AllianceRank.RANK_COUNCIL_INT) {
                    possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                    possibleSelections.add(AllianceRank.RANK_COUNCIL_INT);
                    possibleSelections.add(AllianceRank.RANK_MEMBER_INT);
                    possibleSelections.add(AllianceRank.RANK_TRIAL_INT);
                }

                if (!allianceHasActiveAdmin(a.getId()) && !allianceHasActiveCouncil(a.getId())) {
                    possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                }
            } else if (a.getLeadership() == ELeadership.DICTORIAL) {
                if (!allianceHasActiveAdmin(a.getId()) && !allianceHasActiveCouncil(a.getId())) {
                    possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                    return possibleSelections;
                }

                // Noone except admin has rights as long as he is active
                if (reqRank <= AllianceRank.RANK_COUNCIL_INT) {
                    if (!allianceHasActiveAdmin(a.getId())) {
                        possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                    }
                    return possibleSelections;
                }

                if (reqRank >= AllianceRank.RANK_ADMIN_INT) {
                    possibleSelections.add(AllianceRank.RANK_ADMIN_INT);
                    possibleSelections.add(AllianceRank.RANK_COUNCIL_INT);
                    possibleSelections.add(AllianceRank.RANK_MEMBER_INT);
                    possibleSelections.add(AllianceRank.RANK_TRIAL_INT);
                }
            }
        }

        return possibleSelections;
    }

    protected static boolean allianceHasActiveAdmin(int allianceId) {
        ArrayList<AllianceMember> amList = allianceMemberDAO.findByAllianceId(allianceId);

        for (AllianceMember am : amList) {
            if (am.getIsAdmin()) {
                if (!isActive(am)) {
                    continue;
                }

                return true;
            }
        }

        return false;
    }

    protected static boolean allianceHasActiveCouncil(int allianceId) {
        ArrayList<AllianceMember> amList = allianceMemberDAO.findByAllianceId(allianceId);

        for (AllianceMember am : amList) {
            if (am.getIsCouncil()) {
                if (!isActive(am)) {
                    continue;
                }

                return true;
            }
        }

        return false;
    }

    protected static boolean isActive(AllianceMember am) {
        User u = userDAO.findById(am.getUserId());
        if (u.getDeleteDate() != 0) {
            return false;
        }
        if (u.getLastUpdate() < (GameUtilities.getCurrentTick2() - (144 * 7 * 2))) {
            return false;
        }

        return true;
    }

    protected static int getRank(AllianceMember am) {
        if (am.getIsAdmin()) {
            return AllianceRank.RANK_ADMIN_INT;
        }
        if (am.getIsCouncil()) {
            return AllianceRank.RANK_COUNCIL_INT;
        }
        if (am.getIsTrial()) {
            return AllianceRank.RANK_TRIAL_INT;
        }
        return AllianceRank.RANK_MEMBER_INT;
    }
}
