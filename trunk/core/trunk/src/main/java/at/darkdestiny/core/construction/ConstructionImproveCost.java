/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.construction;

import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.ships.BuildTime;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class ConstructionImproveCost extends MutableRessourcesEntry {
    private PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);
    private PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);

    private final int count = 1;
    private final int planetId;
    private final BuildTime buildTime;
    private final ConstructionExt ce;
    private final PlanetConstruction pc;

    public ConstructionImproveCost(ConstructionExt ce, int planetId) {
        this.ce = ce;
        this.planetId = planetId;
        this.buildTime = new BuildTime();
        this.pc = pcDAO.findBy(planetId, ((Construction)ce.getBase()).getId());

        if (pc.getLevel() == -1 && !((Construction)ce.getBase()).getLevelable()) throw new RuntimeException("ConstructionImproveCost not available for non-improvable buildings.");

        calcImproveCost();
    }

    private void calcImproveCost() {
        // buliding to improve
        RessourcesEntry re = ce.getRessCost();
        this.addRess(re, count);
        buildTime.setIndPoints((int)Math.ceil(ConstructionService.getIndustryPoints(planetId) * count));

        int nextLevel = pc.getLevel() + 1;

        this.multiplyRessources((Math.pow(1.75d,(double)nextLevel))/(double)nextLevel);
        buildTime.setIndPoints(buildTime.getIndPoints() + (int)Math.floor(buildTime.getIndPoints() * nextLevel * 0.25d));

        if (((Construction)ce.getBase()).getId() == Construction.ID_INTERSTELLARTRADINGPOST) {
            PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
            if (pp.getColonyType() == EColonyType.TRADE_COLONY) {
                this.multiplyRessources(0.25d);
            }
        }
        
        roundValues();
    }
}
