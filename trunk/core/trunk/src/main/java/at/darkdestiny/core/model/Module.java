/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.core.ModuleType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "module")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Module extends Model<Module> implements ITechFunctions {

    public static final Integer MODULE_ENGINE_CHEMICALENGINE = 1;
    public static final Integer MODULE_ENGINE_PLASMAENGINE = 80;
    public static final Integer MODULE_ENGINE_LINEARCONVERTER = 82;
    public static final Integer MODULE_ENGINE_IMPULSENGINE = 84;
    public static final Integer MODULE_ENGINE_METAGRAV = 85;
    public static final Integer MODULE_REACTOR_NUCLEARBATTERY = 9;
    public static final Integer MODULE_WEAPON_LASERCANON = 200;
    public static final Integer MODULE_CHASSIS_FIGHTER = 300;
    public static final Integer MODULE_CHASSIS_CORVETTE = 301;
    public static final Integer MODULE_CHASSIS_DESTROYER = 310;
    public static final Integer MODULE_CHASSIS_CRUISER = 315;
    public static final Integer MODULE_CHASSIS_BATTLESHIP = 320;
    public static final Integer MODULE_CHASSIS_SUPERBATTLESHIP = 340;
    public static final Integer MODULE_CHASSIS_TENDER = 350;
    public static final Integer MODULE_CHASSIS_FRIGATE = 351;

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("type")
    private ModuleType type;
    @FieldMappingAnnotation("isChassis")
    private Boolean isChassis;
    @FieldMappingAnnotation("uniquePerShip")
    private Boolean uniquePerShip;
    @FieldMappingAnnotation("freeForDesign")
    private Boolean freeForDesign;
    @FieldMappingAnnotation("uniquePerType")
    private Boolean uniquePerType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModuleType getType() {
        return type;
    }

    public void setType(ModuleType type) {
        this.type = type;
    }

    public Boolean getIsChassis() {
        return isChassis;
    }

    public void setIsChassis(Boolean isChassis) {
        this.isChassis = isChassis;
    }

    public Boolean getUniquePerShip() {
        return uniquePerShip;
    }

    public void setUniquePerShip(Boolean uniquePerShip) {
        this.uniquePerShip = uniquePerShip;
    }

    public Boolean getFreeForDesign() {
        return freeForDesign;
    }

    public void setFreeForDesign(Boolean freeForDesign) {
        this.freeForDesign = freeForDesign;
    }

    public Boolean getUniquePerType() {
        return uniquePerType;
    }

    public void setUniquePerType(Boolean uniquePerType) {
        this.uniquePerType = uniquePerType;
    }

    public int getTechId() {
        return id;
    }

    public String getTechName() {
        return name;
    }

    public String getTechDescription() {
        return type.toString();
    }
}
