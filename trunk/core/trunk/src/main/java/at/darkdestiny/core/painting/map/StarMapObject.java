/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.enumeration.EStarMapObjectType;

/**
 *
 * @author Stefan
 */
public class StarMapObject {
    private final EStarMapObjectType type;
    private final int x;
    private final int y;
    
    public StarMapObject(EStarMapObjectType type, int x, int y) {
        this.type = type;
        this.x = x;
        this.y = y;
    }

    /**
     * @return the type
     */
    public EStarMapObjectType getType() {
        return type;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }
}
