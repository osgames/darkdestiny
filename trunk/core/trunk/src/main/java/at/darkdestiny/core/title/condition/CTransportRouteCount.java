package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.framework.dao.DAOFactory;

public class CTransportRouteCount extends AbstractCondition {

    private ParameterEntry tradeRouteCount;
    private ParameterEntry comparator;
    private static TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);

    public CTransportRouteCount(ParameterEntry tradeRouteCount, ParameterEntry comparator) {
        this.tradeRouteCount = tradeRouteCount;
        this.comparator = comparator;
    }

    public boolean checkCondition(int userId, int conditionToTitleId) {
        int count = trDAO.findByUserId(userId).size();
        return compare(tradeRouteCount.getParamValue().getValue(), count, tradeRouteCount.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));
    }
}
