/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.fleet.FleetMovement;
import at.darkdestiny.core.requestbuffer.FleetParameterBuffer;
import at.darkdestiny.core.result.FlightResult;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class CheckFlightTarget extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(CheckFlightTarget.class);

    public static final String PAR_ERROR = "error";
    public static final String PAR_DISTANCE = "distance";
    public static final String PAR_FLIGHTTIME = "flightTime";
    public static final String PAR_SUBTRACT = "subtract";
    public static final String PAR_RANGE = "range";
    public static final String ERR_NOT_IN_RANGE = "fleettarget_err_notinrange";
    public static final String ERR_SYSTEM_NOT_FOUND = "fleettarget_err_nosystemfound";
    public static final String ERR_PLANET_NOT_FOUND = "fleettarget_err_noplanetfound";
    public static final String ERR_TOO_MANY_SYSTEMS = "fleettarget_err_foundtoomanysystems";
    public static final String ERR_TOO_MANY_PLANETS = "fleettarget_err_foundtoomanyplanets";
    public static final String ERR_SYSTEM_NOT_VISISBLE = "fleettarget_err_systemnotvisible";
    public static final String ERR_PLANET_NOT_VISISBLE = "fleettarget_err_planetfoundbutsystemnotvisible";
    public static final String ERR_NOT_YOUR_FLEET = "fleettarget_err_notyourfleet";

    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // DebugBuffer.trace("CHECK FLIGHT TARGET");

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuffer json = new StringBuffer();

        try {
            int userId = Integer.parseInt(request.getParameter("userId"));
            String targetType = request.getParameter("targetType");
            String targetIdStr = request.getParameter("targetString");
            boolean isFormation = request.getParameter("fleetId").startsWith("FF") || Boolean.parseBoolean(request.getParameter("isFleetFormation"));
            int fleetId = Integer.parseInt(request.getParameter("fleetId").replace("FF", ""));

            ELocationType locType = ELocationType.valueOf(targetType);
            // DebugBuffer.trace("TARGET LOCATION IS " + locType);

            HashMap<String, String> result = calculateResult(userId, targetType, targetIdStr, fleetId, isFormation);
            boolean first = true;
            json.append("{ ");
            for (Map.Entry<String, String> entry : result.entrySet()) {
                if (first) {
                    json.append(" \"" + entry.getKey() + "\":\"" + entry.getValue() + "\" ");
                    first = false;
                } else {
                    json.append(", \"" + entry.getKey() + "\":\"" + entry.getValue() + "\" ");
                }
            }
            json.append(" }");

            out.println(json);
        } catch (IllegalArgumentException iae) {
            json = new StringBuffer();
            json.append("{ \"error\":\"" + iae.getMessage() + "\" }");

            out.println(json);
            DebugBuffer.writeStackTrace("[IAE] Error in CheckFlightTarget: ", iae);
        } catch (Exception e) {
            json = new StringBuffer();
            json.append("{ \"error\":\"" + e.getMessage() + "\" }");

            out.println(json);
            DebugBuffer.writeStackTrace("Error in CheckFlightTarget: ", e);
        } finally {
            out.close();
        }
    }

    public static HashMap<String, String> calculateResult(int userId, String targetType, String targetIdStr, int fleetId, boolean isFormation) {
        log.debug("Process Request UserId: " + userId + " targetType: " + targetType + " targetIdStr: " + targetIdStr + " fleetId: " + fleetId);

        FleetParameterBuffer fpb = new FleetParameterBuffer(userId);
        if (isFormation) {
            fpb.setParameter(FleetParameterBuffer.FLEET_ID, "FF" + fleetId);
        } else {
            fpb.setParameter(FleetParameterBuffer.FLEET_ID, "" + fleetId);
        }
        fpb.setParameter(FleetParameterBuffer.TARGET_ID, targetIdStr);
        fpb.setParameter(FleetParameterBuffer.LOCATION_TYPE, targetType);
        fpb.setParameter(FleetParameterBuffer.LOCATION_ID, targetIdStr);

        /*
         RelativeCoordinate targetCoord;
        
         String errMessage = null;
         */
        HashMap<String, String> result = Maps.newHashMap();

        try {
            if (targetType.equalsIgnoreCase("SYSTEM")) {
                fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, "true");
                // targetCoord = new RelativeCoordinate(Integer.parseInt(targetIdStr),0);
            } else {
                fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, "false");
                // targetCoord = new RelativeCoordinate(0,Integer.parseInt(targetIdStr));
            }
        } catch (Exception e) {
            // errMessage = e.getMessage();
            result.put(PAR_ERROR, "Invalid Parameter");
            return result;
        }

        FlightResult fr = FleetMovement.checkFlightParameters(userId, fpb);

        // PlayerFleet pf = Service.playerFleetDAO.findById(fleetId);
        // PlayerFleetExt pfExt = new PlayerFleetExt(pf);
        if (fr.isError()) {
            result.put(PAR_ERROR, fr.getMessage());
        } else {
            if (fr.getDistance() == -1f) {
                result.put(PAR_DISTANCE, "?");
            } else {
                result.put(PAR_DISTANCE, String.valueOf(fr.getDistance()));
            }

            result.put(PAR_FLIGHTTIME, String.valueOf(fr.getFlightTime()));

            if (fr.getRange() == -1f) {
                result.put(PAR_RANGE, "?");
            } else {
                result.put(PAR_RANGE, String.valueOf(fr.getRange()));
            }
        }

        return result;

        /*
         HashMap<String, String> result = Maps.newHashMap();
         String errMessage = null;
         int targetId = -1;

         PlayerFleet pf = Service.playerFleetDAO.findById(fleetId);
         PlayerFleetExt pfExt = new PlayerFleetExt(pf);

         if (pf.getUserId() != userId) {
         errMessage = ERR_NOT_YOUR_FLEET;
         }
         Planet targetPlanet = null;
         at.darkdestiny.core.model.System targetSystem = null;

         try {
         targetId = Integer.parseInt(targetIdStr);
         if (targetType.equals("SYSTEM")) {
         targetSystem = Service.systemDAO.findById(targetId);
         if (targetSystem == null) {
         errMessage = ERR_SYSTEM_NOT_FOUND;
         } else {
         if (Service.viewSystemDAO.findBy(targetId, EViewSystemLocationType.SYSTEM, userId) == null) {
         errMessage = ERR_SYSTEM_NOT_VISISBLE;
         }
         }
         } else if (targetType.equals("PLANET")) {
         targetPlanet = Service.planetDAO.findById(targetId);
         if (targetPlanet == null) {
         errMessage = ERR_PLANET_NOT_FOUND;
         } else {
         if (Service.viewSystemDAO.findBy(targetPlanet.getSystemId(), EViewSystemLocationType.SYSTEM, userId) == null) {
         errMessage = ERR_PLANET_NOT_VISISBLE;
         }
         targetSystem = Service.systemDAO.findById(targetPlanet.getSystemId());
         }
         }
         } catch (NumberFormatException nfe) {
         //Might be a name
         if (targetType.equals("PLANET")) {
         ArrayList<PlayerPlanet> pps = Lists.newArrayList(Service.playerPlanetDAO.findByName(targetIdStr));
         if (pps.isEmpty()) {
         errMessage = ERR_PLANET_NOT_FOUND;
         } else if (pps.size() > 1) {
         errMessage = ERR_TOO_MANY_PLANETS;
         } else {
         targetPlanet = Service.planetDAO.findById(pps.get(0).getPlanetId());
         targetSystem = Service.systemDAO.findById(targetPlanet.getSystemId());
         }
         } else if (targetType.equals("SYSTEM")) {
         ArrayList<at.darkdestiny.core.model.System> systems = Lists.newArrayList(Service.systemDAO.findByName(targetIdStr));
         if (systems.isEmpty()) {
         errMessage = ERR_SYSTEM_NOT_FOUND;
         } else if (systems.size() > 1) {
         errMessage = ERR_TOO_MANY_SYSTEMS;
         } else {
         targetSystem = systems.get(0);
         }
         }

         }
         //check visibility
         if (targetSystem != null) {
         if (Service.viewSystemDAO.findBy(targetSystem.getId(), EViewSystemLocationType.SYSTEM, userId) == null
         && Service.viewTableDAO.findByUserAndSystem(userId, targetSystem.getId()) == null) {
         errMessage = ERR_SYSTEM_NOT_VISISBLE;
         }
         }
         if (errMessage != null) {
         result.put(PAR_ERROR, ML.getMLStr(errMessage, userId));
         } else {

         int distance = (int) Math.ceil(Math.sqrt(Math.pow(targetSystem.getX() - pf.getAbsoluteCoordinate().getX(), 2d) + Math.pow(targetSystem.getY() - pf.getAbsoluteCoordinate().getY(), 2d)));
         int flightTime = 0;
         int range = pfExt.getRange();
         if (range < distance) {
         errMessage = ERR_NOT_IN_RANGE;

         result.put(PAR_ERROR, ML.getMLStr(errMessage, userId));

         } else {
         flightTime = (int) ((double) distance / pfExt.getSpeed());
         result.put(PAR_DISTANCE, String.valueOf(distance));
         result.put(PAR_FLIGHTTIME, String.valueOf(flightTime));
         result.put(PAR_SUBTRACT, String.valueOf(distance - range));
         result.put(PAR_RANGE, String.valueOf(range));
         }
         }
         return result;
         */
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
