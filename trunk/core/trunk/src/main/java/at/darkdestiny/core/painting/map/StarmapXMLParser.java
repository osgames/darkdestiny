/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.ships.ShipData;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Stefan
 */
public class StarmapXMLParser extends DefaultHandler {
    private int processingSystem = 0;
    private int processingPlanet = 0;
    private final StarMapDataSource smds;
    private SMSystem lastSystem = null;
    private SMFleet lastFleet = null;
    
    public StarmapXMLParser(StarMapDataSource smds) {
        super();        
        this.smds = smds;
    }
    
    @Override
    public void startElement(String namespaceURI,
                             String localName,
                             String qName, 
                             Attributes atts)
        throws SAXException {

        String key = qName; 

        if (key.equalsIgnoreCase("system")) {
            int x = 0;
            int y = 0;
            int sysStatus = -1;
            
            processingSystem = Integer.parseInt(atts.getValue("id"));
            x = Integer.parseInt(atts.getValue("x"));
            y = Integer.parseInt(atts.getValue("y"));
            sysStatus = Integer.parseInt(atts.getValue("sysstatus"));
            processingPlanet = 0;
            // System.out.println("Processing: " + processingSystem + ":" + processingPlanet);
            
            SMSystem currSys = new SMSystem(processingSystem,x,y,sysStatus);
            currSys.setName(atts.getValue("name"));
            lastSystem = currSys;
            smds.addStarSystem(currSys);
        } else if (key.equalsIgnoreCase("planet")) {
            processingPlanet = Integer.parseInt(atts.getValue("id"));;
            // System.out.println("Processing: " + processingSystem + ":" + processingPlanet);

            boolean homePlanet = false;
            int diplomacyId = 0;
            
            int id = Integer.parseInt(atts.getValue("id"));
            
            SMPlanet currPlanet = new SMPlanet(id);            
            
            String diplomacyIdStr = atts.getValue("pStatus");
            String homeSystemStr = atts.getValue("homesystem");
            String typ = atts.getValue("typ");
            int diameter = Integer.parseInt(atts.getValue("diameter"));
            String name = atts.getValue("name");
            String owner = atts.getValue("owner");
            
            currPlanet.setDiameter(diameter);
            currPlanet.setType(typ);
            
            if (homeSystemStr != null) {            
                homePlanet = Boolean.parseBoolean(homeSystemStr);
                currPlanet.setHomePlanet(homePlanet);
            }
            if (diplomacyIdStr != null) {
                diplomacyId = Integer.parseInt(diplomacyIdStr);
                currPlanet.setDiplomacyId(diplomacyId);
            }
            
            if (name != null) {
                currPlanet.setName(name);
            } else {
                currPlanet.setName("Planet #" + atts.getValue("id"));
            }
            
            if (owner != null) {
                currPlanet.setOwner(owner);
            }
            
            lastSystem.addPlanet(currPlanet);
        } else if (key.equalsIgnoreCase("observatory")) {
            int x = 0;
            int y = 0;            
            x = Integer.parseInt(atts.getValue("x"));
            y = Integer.parseInt(atts.getValue("y"));            
            SMObservatory currObs = new SMObservatory(x,y);
            smds.addObservatory(currObs);
        } else if (key.equalsIgnoreCase("scanner")) {
            int x = 0;
            int y = 0;   
            int range = 0;
            x = Integer.parseInt(atts.getValue("x"));
            y = Integer.parseInt(atts.getValue("y"));            
            range = Integer.parseInt(atts.getValue("range")); 
            SMHyperscanner currScanner = new SMHyperscanner(x,y,range);
            smds.addHyperscanner(currScanner);            
        } else if (key.equalsIgnoreCase("fleet")) {
            int id = 0;
            int x = 0;
            int y = 0;
            int x2 = 0;
            int y2 = 0;        
            int tgtSystemId = 0;
            int tgtPlanetId = 0;            
            int time = 0;
            int status = 0;
            int range = 0;
            
            id = Integer.parseInt(atts.getValue("id"));
            x = Integer.parseInt(atts.getValue("startX"));
            y = Integer.parseInt(atts.getValue("startY")); 
            x2 = Integer.parseInt(atts.getValue("endX"));
            y2 = Integer.parseInt(atts.getValue("endY"));             
            time = Integer.parseInt(atts.getValue("time"));  
            status = Integer.parseInt(atts.getValue("status"));
            tgtPlanetId = Integer.parseInt(atts.getValue("endPlanetId")); 
            tgtSystemId = Integer.parseInt(atts.getValue("endSystemId"));    
            if (atts.getValue("range") != null) {
                range = Integer.parseInt(atts.getValue("range"));
            }

            SMFleet smf = new SMFleet(id,x,y,status);
            smf.setName(atts.getValue("name"));
            smf.setTimeToTarget(time);
            smf.setTargetSystemId(tgtSystemId);
            smf.setTargetPlanetId(tgtPlanetId);
            smf.setRange(range);
            
            if (atts.getValue("owner") != null) {
                smf.setOwner(atts.getValue("owner"));
            }
            
            if (time > 0) {
                System.out.println("Fleet " + atts.getValue("name") + " ["+id+"] is moving");
                smf.setMoving(x2, y2);
            } else {
                System.out.println("Fleet " + atts.getValue("name") + " ["+id+"] is not moving");
            }
            
            lastFleet = smf;
            smds.addFleets(smf);
        } else if (key.equalsIgnoreCase("ChassisDetail")) {
            int chassisId = Integer.parseInt(atts.getValue("chassisid"));
            String chassisName = atts.getValue("chassisname");
            int count = Integer.parseInt(atts.getValue("count"));
            String designName = atts.getValue("designname");
            
            ShipData sd = new ShipData();
            sd.setChassisSize(chassisId);
            sd.setCount(count);
            sd.setDesignName(designName);
            
            lastFleet.addShips(sd);
        }
    }    
}
