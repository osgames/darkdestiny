/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EDiplomacyTypeRelType;
import at.darkdestiny.core.enumeration.ESorting;
import at.darkdestiny.core.model.DiplomacyType;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class DiplomacyTypeDAO extends ReadOnlyTable<DiplomacyType> implements GenericDAO {

    public DiplomacyType findById(int diplomacyTypeId) {
        DiplomacyType dtSearch = new DiplomacyType();
        dtSearch.setId(diplomacyTypeId);
        return (DiplomacyType)get(dtSearch);
    }
    public ArrayList<DiplomacyType> findAllSettable(EDiplomacyTypeRelType dtrt) {
        ArrayList<DiplomacyType> result = new ArrayList<DiplomacyType>();
        DiplomacyType dtSearch = new DiplomacyType();
        dtSearch.setRelationType(dtrt);

        for(DiplomacyType dt : find(dtSearch)){
            if(dt.getUserDefineable()){
                result.add(dt);
            }
        }
        return result;
    }

    public ArrayList<DiplomacyType> findAll(EDiplomacyTypeRelType dtrt) {
        ArrayList<DiplomacyType> result = new ArrayList<DiplomacyType>();
        DiplomacyType dtSearch = new DiplomacyType();
        dtSearch.setRelationType(dtrt);

        // System.out.println("LOOP RESULT");
        for(DiplomacyType dt : find(dtSearch)){
            // System.out.println("FOUND " + dt.getName());
            result.add(dt);
        }
        return result;
    }

    public ArrayList<DiplomacyType> sortDiplomacyTypeArrayList(ArrayList<DiplomacyType> dtList, ESorting sorting) {
        ArrayList<DiplomacyType> outList = new ArrayList<DiplomacyType>();

        TreeMap<Integer,DiplomacyType> sortedMap = new TreeMap<Integer,DiplomacyType>();
        for (DiplomacyType dt : dtList) {
            sortedMap.put(dt.getWeight(), dt);
        }

        if (sorting == ESorting.ASCENDING) {
            outList.addAll(sortedMap.values());
        } else {
            outList.addAll(sortedMap.descendingMap().values());
        }
        return outList;
    }
}
