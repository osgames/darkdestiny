/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.webservice.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "playerstatistic")
public class PlayerStatisticListEntry {

    @XmlElement(name = "gameName")
    private String gameName;
    @XmlElement(name = "date")
    private Long date;
    @XmlElement(name = "points")
    private Long points;
    @XmlElement(name = "researchPoints")
    private Long researchPoints;
    @XmlElement(name = "ressourcePoints")
    private Long ressourcePoints;
    @XmlElement(name = "populationPoints")
    private Long populationPoints;
    @XmlElement(name = "militaryPoints")
    private Long militaryPoints;
    @XmlElement(name = "rank")
    private Integer rank;

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the researchPoints
     */
    public Long getResearchPoints() {
        return researchPoints;
    }

    /**
     * @param researchPoints the researchPoints to set
     */
    public void setResearchPoints(Long researchPoints) {
        this.researchPoints = researchPoints;
    }

    /**
     * @return the ressourcePoints
     */
    public Long getRessourcePoints() {
        return ressourcePoints;
    }

    /**
     * @param ressourcePoints the ressourcePoints to set
     */
    public void setRessourcePoints(Long ressourcePoints) {
        this.ressourcePoints = ressourcePoints;
    }

    /**
     * @return the populationPoints
     */
    public Long getPopulationPoints() {
        return populationPoints;
    }

    /**
     * @param populationPoints the populationPoints to set
     */
    public void setPopulationPoints(Long populationPoints) {
        this.populationPoints = populationPoints;
    }

    /**
     * @return the militaryPoints
     */
    public Long getMilitaryPoints() {
        return militaryPoints;
    }

    /**
     * @param militaryPoints the militaryPoints to set
     */
    public void setMilitaryPoints(Long militaryPoints) {
        this.militaryPoints = militaryPoints;
    }

    /**
     * @return the rank
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    /**
     * @return the gameName
     */
    public String getGameName() {
        return gameName;
    }

    /**
     * @param gameName the gameName to set
     */
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    /**
     * @return the points
     */
    public Long getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(Long points) {
        this.points = points;
    }
}
