/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.SunTransmitter;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class SunTransmitterDAO extends ReadWriteTable<SunTransmitter> implements GenericDAO  {
    public SunTransmitter findById(int id) {
        SunTransmitter st = new SunTransmitter();
        st.setId(id);
        return (SunTransmitter)get(st);
    }

    public SunTransmitter findBySystemId(int sysId) {
        SunTransmitter st = new SunTransmitter();
        st.setSystemId(sysId);
        ArrayList<SunTransmitter> result = find(st);

        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public SunTransmitter findByPlanetId(int planetId) {
        SunTransmitter st = new SunTransmitter();
        st.setPlanetId(planetId);
        ArrayList<SunTransmitter> result = find(st);

        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
