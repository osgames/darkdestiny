package at.darkdestiny.core.construction;

import at.darkdestiny.core.ressources.MutableRessourcesEntry;

public class ModuleEntry extends MutableRessourcesEntry {
    private int moduleId;
    private String name;
    private int count;
    private int space;
    private int energy;
    private int value;
    private int type;
    private int modulePoints;
    
    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public int getModulePoints() {
        return modulePoints;
    }

    public void setModulePoints(int modulePoints) {
        this.modulePoints = modulePoints;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
