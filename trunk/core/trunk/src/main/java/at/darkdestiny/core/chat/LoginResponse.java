/*
 * LoginResponse.java
 *
 * Created on 05. Juli 2008, 15:07
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.darkdestiny.core.chat;

/**
 *
 * @author Stefan
 */
public class LoginResponse {
    private boolean success;
    private String reason;
    
    /** Creates a new instance of LoginResponse */
    public LoginResponse() {
        success = false;
        reason = "Unbekannter Fehler";
    }    

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
