/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.enumeration.ETitleType;
import at.darkdestiny.core.model.Campaign;
import at.darkdestiny.core.model.CampaignToUser;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.Title;
import at.darkdestiny.core.model.TitleCondition;
import at.darkdestiny.core.model.TitleToUser;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.view.ViewCondition;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class TaskService extends Service {

    public static ArrayList<Title> getTitlesSorted(int userId, int campaignId, ETitleType type, int titleStatus, boolean showInvisible) {
        ArrayList<Title> result = titleDAO.findAll();
        TreeMap<Integer, TreeMap<String, ArrayList<Title>>> sorted = new TreeMap<Integer, TreeMap<String, ArrayList<Title>>>();
        for (Title t : result) {
            if (campaignId != 0 && campaignId != t.getCampaignId()) {
                continue;
            }
            boolean userHasTitle = true;
            if (titleToUserDAO.findBy(userId, t.getId()) == null) {
                userHasTitle = false;
            }
            if (userHasTitle && titleStatus == 1) {
                continue;
            }
            if (!userHasTitle && titleStatus == 2) {
                continue;
            }
            if ((!t.getVisible() && !showInvisible) && !userHasTitle) {
                continue;
            }
            if (type != null && t.getType() != type) {
                continue;
            }
            TreeMap<String, ArrayList<Title>> soValues = sorted.get(t.getDifficulty());
            if (soValues == null) {
                soValues = new TreeMap<String, ArrayList<Title>>();
            }
            ArrayList<Title> values = soValues.get(t.getType().toString());
            if (values == null) {
                values = new ArrayList<Title>();
            }
            values.add(t);
            soValues.put(t.getType().toString(), values);
            sorted.put(t.getDifficulty(), soValues);
        }
        result.clear();
        for (Map.Entry<Integer, TreeMap<String, ArrayList<Title>>> entries : sorted.entrySet()) {
            for (Map.Entry<String, ArrayList<Title>> entries2 : entries.getValue().entrySet()) {
                result.addAll(entries2.getValue());
            }
        }
        return result;
    }

    public static void setTitle(int userId, int titleId) {

        UserData ud = userDataDAO.findByUserId(userId);
        if (titleId > 0) {
            if (titleToUserDAO.findBy(userId, titleId) != null) {
                ud.setTitleId(titleId);
                userDataDAO.update(ud);
            }
        } else {
            ud.setTitleId(0);
            userDataDAO.update(ud);

        }
    }

    public static String TypeToColor(ETitleType type) {

        String color = "";
        if (type == ETitleType.RESEARCH) {
            color = "#000022";
        } else if (type == ETitleType.IMPERIAL) {
            color = "#002200";
        } else if (type == ETitleType.MILITARY) {
            color = "#220000";
        } else if (type == ETitleType.VARIOUS) {
            color = "#333333";
        }
        return color;
    }

    public static ArrayList<Title> findTitlesByUserId(int userId) {
        ArrayList<Title> result = new ArrayList<Title>();
        TreeMap<Integer, ArrayList<Title>> sorted = new TreeMap<Integer, ArrayList<Title>>();
        for (TitleToUser ttu : titleToUserDAO.findByUserId(userId)) {
            Title t = titleDAO.findById(ttu.getTitleId());
            ArrayList<Title> values = sorted.get(t.getDifficulty());
            if (values == null) {
                values = new ArrayList<Title>();
            }
            values.add(t);
            sorted.put(t.getDifficulty(), values);
        }
        result.clear();
        for (Map.Entry<Integer, ArrayList<Title>> entries : sorted.entrySet()) {

            result.addAll(entries.getValue());
        }
        return result;
    }

    public static String difficultyToStars(int difficulty) {

        StringBuffer result = new StringBuffer();
        String bStar = "<IMG src='" + GameConfig.getInstance().picPath() + "pic/bstar.png' />";
        String sStar = "<IMG src='" + GameConfig.getInstance().picPath() + "pic/sstar.png' />";
        String gStar = "<IMG src='" + GameConfig.getInstance().picPath() + "pic/gstar.png' />";

        boolean bronze = true;
        boolean silver = true;
        for (int i = 1; i <= difficulty; i++) {
            if (i <= 5) {
                result.append(bStar);
            } else if (i <= 10) {
                if (bronze) {
                    result = new StringBuffer();
                    bronze = false;
                }
                result.append(sStar);
            } else {
                if (silver) {
                    result = new StringBuffer();
                    silver = false;
                }
                result.append(gStar);
            }
        }

        return result.toString();

    }

    public static ArrayList<ViewCondition> getConditionsOrdered(int titleId) {
        ArrayList<ConditionToTitle> ctts = conditionToTitleDAO.findByTitleId(titleId);
        ArrayList<ViewCondition> result = new ArrayList<ViewCondition>();
        TreeMap<Integer, ArrayList<ViewCondition>> sorted = new TreeMap<Integer, ArrayList<ViewCondition>>();
        for (ConditionToTitle ctt : ctts) {
            TitleCondition cond = conditionDAO.findById(ctt.getConditionId());
            ViewCondition vc = new ViewCondition(cond, ctt);
            ArrayList<ViewCondition> values = sorted.get(ctt.getOrder());
            if (values == null) {
                values = new ArrayList<ViewCondition>();
            }
            values.add(vc);
            sorted.put(ctt.getOrder(), values);
        }
        result.clear();
        for (Map.Entry<Integer, ArrayList<ViewCondition>> entry : sorted.entrySet()) {

            result.addAll(entry.getValue());
        }
        return result;
    }

    public static boolean hasTitle(int userId, int titleId) {
        if (titleToUserDAO.findBy(userId, titleId) != null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean hasCondition(int userId, int titleId, int conditionToTitleId) {

        if (titleToUserDAO.findBy(userId, titleId) != null) {
            return true;
        } else {
            if (conditionToUserDAO.findBy(conditionToTitleId, userId) != null) {
                if (conditionToUserDAO.findBy(conditionToTitleId, userId).getValue().equals("")) {
                    return true;
                }
            }
            return false;
        }
    }

    public static int findLastCampaign(int userId) {

        ArrayList<Campaign> camps = campaignDAO.findAllOrdered();
        for (Campaign c : camps) {
            CampaignToUser ctu = campaignToUserDAO.findBy(userId, c.getId());
            if (ctu == null) {
                return c.getId();
            }
        }
        return 0;
    }
}
