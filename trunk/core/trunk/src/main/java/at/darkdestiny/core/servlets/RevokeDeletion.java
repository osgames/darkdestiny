/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.servlets;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.User;
import at.darkdestiny.framework.dao.DAOFactory;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class RevokeDeletion extends HttpServlet {
    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            int userId = Integer.parseInt(request.getParameter("uId"));
            String pw = request.getParameter("pw");

            // System.out.println("Find user for " + userId + " with PW " + pw);

            User u = uDAO.findById(userId);

            // System.out.println("User is " + u);

            if ((u != null) && u.getPassword().equals(pw)) {
                int gracePeriod = u.getDeleteDate() + (int)(GameConstants.DELETION_DELAY / 2d);
                int graceLeft = gracePeriod - GameUtilities.getCurrentTick2();

                u.setLocked(graceLeft);
                u.setDeleteDate(0);
                uDAO.update(u);
            }

            response.sendRedirect(GameConfig.getInstance().getStartURL());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
