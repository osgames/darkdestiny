/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.databuffer;

import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.interfaces.IRessourceCost;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class RessourcesEntry extends Service implements IRessourceCost {
        private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);

        public HashMap<Integer,RessAmountEntry> ress = new HashMap<Integer,RessAmountEntry>();

	public RessourcesEntry() {

	}

	public RessourcesEntry(ArrayList<RessAmountEntry> input) {
            for (RessAmountEntry rae : input) {
                ress.put(rae.getRessId(), rae);
            }
	}

	public RessourcesEntry(HashMap<Integer,RessAmountEntry> input) {
            ress = input;
	}

        @Override
        public ArrayList<RessAmountEntry> getRessArray() {
            ArrayList<RessAmountEntry> tmpArray = new ArrayList<RessAmountEntry>();

            for (RessAmountEntry rc : ress.values()) {
                tmpArray.add(rc);
            }

            return tmpArray;
        }

        public ArrayList<RessAmountEntry> getSortedRessArray() {
            ArrayList<RessAmountEntry> tmpArray = new ArrayList<RessAmountEntry>();
            TreeMap<Integer,RessAmountEntry> sortMap = new TreeMap<Integer,RessAmountEntry>();

            for (RessAmountEntry rc : ress.values()) {
                int key = rDAO.findRessourceById(rc.getRessId()).getDisplayLocation();
                sortMap.put(key,rc);
            }

            tmpArray.addAll(sortMap.values());
            return tmpArray;
        }

        @Override
        public HashMap<Integer,RessAmountEntry> getRess() {
            return ress;
        }

        @Override
        public double getRess(int ressId) {
            if (ress.containsKey(ressId)) {
                return ress.get(ressId).getQty();
            } else {
                return 0;
            }
        }
}
