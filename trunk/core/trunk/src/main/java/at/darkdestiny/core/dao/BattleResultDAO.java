/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.BattleResult;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class BattleResultDAO extends ReadWriteTable<BattleResult> implements GenericDAO  {

    public BattleResult findById(int battleResultId) {

        BattleResult br = new BattleResult();
        br.setId(battleResultId);
       return (BattleResult)get(br);

    }

    public ArrayList<BattleResult> findByUserId(int userId) {

        if(userId == 1){
            return findAll();
        }
        BattleResult br = new BattleResult();
        br.setUserId(userId);
       return find(br);

    }
    public ArrayList<BattleResult> findByUserIdSorted(int userId) {
        ArrayList<BattleResult> result = new ArrayList<BattleResult>();
        TreeMap<Integer, BattleResult> sorted = new TreeMap<Integer, BattleResult>();
        if(userId == 1){
            result =  findAll();
        }else{
        BattleResult br = new BattleResult();
        br.setUserId(userId);
        result = find(br);
        }
        for(BattleResult brTmp : result){
            sorted.put(brTmp.getId(), brTmp);
        }
        result.clear();
        result.addAll(sorted.descendingMap().values());
       return result;

    }

}
