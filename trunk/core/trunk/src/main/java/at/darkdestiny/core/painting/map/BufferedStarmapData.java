/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.JoinMapInfo;
import at.darkdestiny.core.StarMapInfoUtilities;
import at.darkdestiny.core.admin.SessionTracker;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.User;
import static at.darkdestiny.core.service.Service.userDAO;
import at.darkdestiny.core.xml.XMLMemo;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

/**
 *
 * @author Stefan
 */
public class BufferedStarmapData {
    private static UserDAO uDAO = DAOFactory.get(UserDAO.class);
    private static HashMap<String,IMapDataSource> bufferedStarMapData = new HashMap<String,IMapDataSource>();
    private static HashSet<String> areaMapContainerLoaded = new HashSet<String>();
    
    private static void loadJoinMapData(int userId) {
        JoinMapInfo jmi = new JoinMapInfo(new Locale("DE","de"));
        XMLMemo starmap = new XMLMemo("Starmap");
        jmi.addTiles(starmap);        

        // Convert XML to StringBuffer
        StringBuffer xml;
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        starmap.write(pw);

        xml = sw.getBuffer();

        StringBuilder declaration = new StringBuilder(); 
        declaration.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");

        xml.insert(0, declaration);        
        
        IMapDataSource iMDS = new JoinMapDataSource(xml);
        iMDS.parse();  
        
        bufferedStarMapData.put(""+userId, (JoinMapDataSource)iMDS);  
        
        // Build AreaMapContainer
        if (!areaMapContainerLoaded.contains(""+userId)) { 
            AreaMapContainer.clearAreaMap(userId);            
            AreaMapContainer.clearStarMapObjects(userId);        

            JoinMapDataSource jmds = (JoinMapDataSource)iMDS;        
            for (JMTile jTile : jmds.getTiles()) {
                AreaMapContainer.addToObjectMap(userId, jTile.getX(), jTile.getY(), jTile);
            }
        }
        
        areaMapContainerLoaded.add(""+userId);        
        // bufferedStarMapData.put("-1", iMDS); 
    }
    
    private static void loadStarMapUserData(int userId, String userTimeKey) {
        StringBuffer xml = StarMapInfoUtilities.getStarMapInfo(userId);
        IMapDataSource iMDS = new StarMapDataSource(xml);
        ((StarMapDataSource)iMDS).parse();  
        
        bufferedStarMapData.put(userTimeKey, (StarMapDataSource)iMDS);  
        
        if (!areaMapContainerLoaded.contains(userTimeKey)) {                        
            // Build AreaMapContainer
            AreaMapContainer.clearAreaMap(userId);            
            AreaMapContainer.clearStarMapObjects(userId);
            
            StarMapDataSource smds = (StarMapDataSource)iMDS;
            for (SMSystem smSys : smds.getStarSystems()) {
                StarSystem ss = new StarSystem(smSys.getId(),smSys.getX(),smSys.getY());
                ss.setPayload(smSys);
                ss.setName(smSys.getName());
                AreaMapContainer.addToObjectMap(userId, smSys.getX(), smSys.getY(), ss);                
            }
            
            for (SMFleet smFleet : smds.getFleets()) {
                Fleet f = new Fleet(smFleet.getId(),smFleet.getX(),smFleet.getY());
                f.setName(smFleet.getName());
                f.setOwner(smFleet.getOwner());
                f.setPayload(smFleet);
                AreaMapContainer.addToObjectMap(userId, smFleet.getX(), smFleet.getY(), f);                   
            }
            
            areaMapContainerLoaded.add(userTimeKey);
        } else {
            
        }
    }
    
    public static JoinMapDataSource getJoinMapData(int userId) {
        if (!bufferedStarMapData.containsKey(""+userId)) {
            loadJoinMapData(userId);                                    
        }
        
        return (JoinMapDataSource)bufferedStarMapData.get(""+userId);        
    }
    
    public static StarMapDataSource getStarMapUserData(int userId) {
        String userTimeKey = createUserTimeKey(userId);
        
        if (!bufferedStarMapData.containsKey(userTimeKey)) {
            loadStarMapUserData(userId,userTimeKey);                                    
        }
        
        return (StarMapDataSource)bufferedStarMapData.get(userTimeKey);
    }
    
    private static String createUserTimeKey(int userId) {
        String key = userId + "_" + GameUtilities.getCurrentTick2();
        return key;
    }    
    
    public static void cleanOldBuffers() {
        int currentTick = GameUtilities.getCurrentTick2();
        ArrayList<User> users = userDAO.findAll();
        
        for (User u : users) {
            for (int i=currentTick-2;i>(currentTick-10);i--) {
                String key = u.getId() + "_" + currentTick;
                
                IMapDataSource imds = bufferedStarMapData.remove(key);
                boolean removed = areaMapContainerLoaded.remove(key);
                
                if (removed) {
                    DebugBuffer.trace("Cleaned timeUserKey " + key + " from areMapContainerLoaded");
                }
                if (imds != null) {
                    DebugBuffer.trace("Cleaned timeUserKey " + key + " from bufferedStarMapData");
                }
            }
            
            if (!SessionTracker.isOnline(u.getId())) {
                IMapDataSource imds = bufferedStarMapData.remove(""+u.getId());
                boolean removed = areaMapContainerLoaded.remove(""+u.getId());        
                
                if (removed) {
                    DebugBuffer.trace("Cleaned userKey " + u.getId() + " from areMapContainerLoaded (JoinMap)");
                }
                if (imds != null) {
                    DebugBuffer.trace("Cleaned userKey " + u.getId() + " from bufferedStarMapData (JoinMap)");
                }                
            }
        }
    }
}
