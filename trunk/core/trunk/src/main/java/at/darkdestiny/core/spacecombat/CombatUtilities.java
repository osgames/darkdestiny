/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

 import at.darkdestiny.core.test.AttackResolution;import at.darkdestiny.core.test.Connector;
import at.darkdestiny.core.test.EnemyConnector;
import at.darkdestiny.core.test.HelpResolution;
import at.darkdestiny.core.test.PlayerNode;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.test.AttackResolution;
import at.darkdestiny.core.test.Connector;
import at.darkdestiny.core.test.EnemyConnector;
import at.darkdestiny.core.test.HelpResolution;
import at.darkdestiny.core.test.PlayerNode;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Dreloc
 */
public class CombatUtilities {
    private static final Logger log = LoggerFactory.getLogger(CombatUtilities.class);

    public static HashMap<Integer,ArrayList<Integer>> getCombatGroupedPlayers(ArrayList<Integer> players) {
        HashMap<Integer,ArrayList<Integer>> groupedPlayers = new HashMap<Integer,ArrayList<Integer>>();

        ArrayList<PlayerNode> pNodes = new ArrayList<PlayerNode>();
        ArrayList<PlayerNode> assignedNodes = new ArrayList<PlayerNode>();

        for (int player : players) {
            pNodes.add(new PlayerNode(player));
        }

        for (int i=0;i<pNodes.size();i++) {
            for (int j=i;j<pNodes.size();j++) {
                if (pNodes.get(i).getUserId() == pNodes.get(j).getUserId()) continue;
                setRelation(pNodes.get(i),pNodes.get(j),
                        DiplomacyUtilities.getDiplomacyRelation(
                        pNodes.get(i).getUserId(),
                        pNodes.get(j).getUserId()));
            }
        }

        int currGroup = 1;

        // Process relations
        while (assignedNodes.size() < pNodes.size()) {
            for (PlayerNode pn : pNodes) {
                log.debug("Masterprocess " + pn.getUserId());

                if (assignedNodes.contains(pn)) continue;

                if (pn.getGroup() == -1) {
                    assignedNodes.add(pn);
                    pn.setGroup(currGroup);
                    currGroup++;
                }

                ArrayList<PlayerNode> stepProcNodes = new ArrayList<PlayerNode>();
                stepProcNodes.add(pn);

                for (PlayerNode pnInner : pn.getRelationMap().keySet()) {
                    if (stepProcNodes.contains(pnInner)) continue;
                    if (pn.getRelationMap().get(pnInner) instanceof EnemyConnector) continue;
                    processNode(pnInner,pn,stepProcNodes,assignedNodes);
                }
            }
        }

        for (PlayerNode pn : pNodes) {
            if (groupedPlayers.containsKey(pn.getGroup())) {
                groupedPlayers.get(pn.getGroup()).add(pn.getUserId());
            } else {
                ArrayList<Integer> pList = new ArrayList<Integer>();
                pList.add(pn.getUserId());
                groupedPlayers.put(pn.getGroup(), pList);
            }
        }

        return groupedPlayers;
    }

    private static void processNode(PlayerNode currNode, PlayerNode masterNode, ArrayList<PlayerNode> stepProcNodes, ArrayList<PlayerNode> assignedNodes) {
        stepProcNodes.add(currNode);

        if (!contradicts(currNode,masterNode)) {
            /*
            if (hasSameEnemy(currNode,masterNode)) {
                log.debug(currNode.getUserId() + " shares " + masterNode.getUserId());
                currNode.setGroup(masterNode.getGroup());
                assignedNodes.add(currNode);
            }
            */
            log.debug(currNode.getUserId() + " shares " + masterNode.getUserId());
            currNode.setGroup(masterNode.getGroup());
            assignedNodes.add(currNode);
        } else {
            log.debug(currNode.getUserId() + " contradicts " + masterNode.getUserId());
        }

        for (PlayerNode pnInner : currNode.getRelationMap().keySet()) {
            if (stepProcNodes.contains(pnInner)) continue;
            if (currNode.getRelationMap().get(pnInner) instanceof EnemyConnector) continue;
            processNode(pnInner,masterNode,stepProcNodes,assignedNodes);
        }
    }

    private static boolean contradicts(PlayerNode pn1, PlayerNode pn2) {
        for (AttackResolution ar : pn1.getAttacks()) {
            if (ar.getUserId() == pn2.getUserId()) return true;

            for (HelpResolution hr : pn2.getHelps()) {
                if (ar.getUserId() == hr.getUserId()) return true;
            }
        }

        for (HelpResolution hr : pn1.getHelps()) {
            for (AttackResolution ar : pn2.getAttacks()) {
                if (ar.getUserId() == hr.getUserId()) return true;
            }
        }

        return false;
    }

    private static boolean hasSameEnemy(PlayerNode pn1, PlayerNode pn2) {
        for (AttackResolution ar : pn1.getAttacks()) {
            for (AttackResolution ar2 : pn2.getAttacks()) {
                if (ar.getUserId() == ar2.getUserId()) return true;
            }
        }

        return false;
    }

    private static void setRelation(PlayerNode side1, PlayerNode side2, Connector c) {
        side1.addRelation(side2, c);
        side2.addRelation(side1, c);
    }

    public static void buildArmorShieldFaktorMap() {

    }
}
