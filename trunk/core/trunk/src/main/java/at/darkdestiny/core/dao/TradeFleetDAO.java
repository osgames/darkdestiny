/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.TradeFleet;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class TradeFleetDAO extends ReadWriteTable<TradeFleet>  implements GenericDAO{

    public ArrayList<TradeFleet> findByTradePostId(Integer tradePostId) {
        TradeFleet tf = new TradeFleet();
        tf.setTradePostId(tradePostId);
        return find(tf);
    }


}
