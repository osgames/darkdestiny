/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.SystemExt;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class SystemSearchResult extends BaseResult {
    private final ArrayList<SystemExt> systemList;

    public SystemSearchResult(ArrayList<SystemExt> systemList) {
        super("",false);
        this.systemList = systemList;
    }
    
    public SystemSearchResult(String errorMessage) {
        super(errorMessage,true);
        systemList = null;
    }

    public ArrayList<SystemExt> getSystemList() {
        return systemList;
    }
    
    public ArrayList<SystemExt> getListSortedByDistance() {
        TreeMap<Double,SystemExt> sortedMap = new TreeMap<Double,SystemExt>();
        
        for (SystemExt se : systemList) {
            sortedMap.put(se.getDistanceToRef(), se);
        }
        
        ArrayList<SystemExt> sortedList = new ArrayList<SystemExt>();
        sortedList.addAll(sortedMap.values());
        return sortedList;
    }
}
