/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ResearchProgress;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class ResearchProgressDAO extends ReadWriteTable<ResearchProgress> implements GenericDAO {

    public ArrayList<ResearchProgress> findByUserId(Integer userId) {
        ResearchProgress rp = new ResearchProgress();
        rp.setUserId(userId);
        return find(rp);

    }

    public ResearchProgress findById(Integer id) {
        ResearchProgress rp = new ResearchProgress();
        rp.setId(id);
        return (ResearchProgress)get(rp);
    }

    public ResearchProgress findBy(Integer userId, Integer researchId) {
        ResearchProgress rp = new ResearchProgress();
        rp.setUserId(userId);
        rp.setResearchId(researchId);

        ArrayList<ResearchProgress> rpList = find(rp);
        if (rpList.isEmpty()) {
            return null;
        } else {
            return rpList.get(0);
        }
    }
}
