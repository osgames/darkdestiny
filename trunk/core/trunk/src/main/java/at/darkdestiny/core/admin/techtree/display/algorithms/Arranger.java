package at.darkdestiny.core.admin.techtree.display.algorithms;

import at.darkdestiny.core.admin.TechTreeDisplayAdmin;
import at.darkdestiny.core.admin.techtree.TechTree;
import at.darkdestiny.core.admin.techtree.display.DisplayTechTreeHelper;
import at.darkdestiny.core.admin.techtree.display.algorithms.layout.*;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Hier wird die Anordnung aller Technologien
 * als Baum berechnet
 * @author Martin Tsch&ouml;pe
 */
public class Arranger {

	/**
	 * Wie werden die Technologien angeordnet
	 */
	ItemArranger sort;

	/**
	 * Den Sortieralgo setzen
	 */
	public Arranger() {
		super();
		sort = new OrderTopDown();
	}

	/**
	 * Alles Anordnen
	 * @param allItems alle Technologien
	 * @param dasSpiel das Spiel, zu dem alles geh&ouml;rt
	 * @return eine Liste mit den Dingen in den verschiedenen Ebenen, in denen es angeordnet wurde
	 */
	public List<List<DisplayTechTreeHelper>> run(List<DisplayTechTreeHelper> allItems)
	{
		List<List<DisplayTechTreeHelper>> allLevels = new LinkedList<List<DisplayTechTreeHelper>>();
		List<DisplayTechTreeHelper> remaining = new LinkedList<DisplayTechTreeHelper>();
		Set<String> allUsed = new TreeSet<String>();
		remaining.addAll(allItems);

		while (remaining.size() > 0)
		{

//			DebugBuffer.addLine("Rem = "+remaining);
//			DebugBuffer.addLine("Done = "+allUsed);

			List<DisplayTechTreeHelper> tmp = new LinkedList<DisplayTechTreeHelper> ();
			tmp.addAll(remaining);

			for (ListIterator<DisplayTechTreeHelper> i = tmp.listIterator(); i.hasNext();)
			{
				DisplayTechTreeHelper dt = i.next();
				boolean ok = true;
				for (Iterator<TechTree> j = dt.getDependsOn().iterator();j.hasNext();)
				{
					boolean found = false;
					TechTree searched = j.next();
					if (allUsed.contains(searched.getName()))
						found = true;

					if (!found)
						ok = false;
				}
				if (!ok)
					i.remove();
			}
			remaining.removeAll(tmp);
			for (Iterator<DisplayTechTreeHelper> i = tmp.iterator();i.hasNext();)
				allUsed.add(i.next().getTechno().getName());
			allLevels.add(tmp);
			DebugBuffer.addLine(DebugLevel.DEBUG,"Level "+allLevels.size()+" = "+tmp);
		}

		int y = 25;
		int row = 0;
		for (List<DisplayTechTreeHelper> level : allLevels)
		{
			int x = 25;
			int maxY = 0;
			for (DisplayTechTreeHelper di  : level)
			{
				di.moveTo(x, y);
				di.setRow(row);
				maxY = Math.max(maxY,di.getHeight());
				x += di.getWidth() + di.getSpace();
			}
			y += maxY + 20;
			row ++;
	//		DebugBuffer.addLine("LevelH&ouml;he = " + maxY);
		}

		//Jetzt den Abstand berechnen
		sort.preprocessLines(allLevels, allItems, TechTreeDisplayAdmin.getAllLines());

		y = 25;
		row = 0;
		for (List<DisplayTechTreeHelper> level:  allLevels)
		{
			int x = 25;
			int maxY = 0;
			for (DisplayTechTreeHelper di : level)
			{
				di.moveTo(x, y);
				di.setRow(row);
				maxY = Math.max(maxY,di.getHeight());
				x += di.getWidth() + di.getSpace();
			}
			y += maxY + 20 + sort.getRequiredSpace(row);
			row ++;
	//		DebugBuffer.addLine("LevelHöhe = " + maxY);
		}

		long startTime = System.currentTimeMillis();
		sort.processData(allItems, allLevels);
		sort.processLines(allItems, TechTreeDisplayAdmin.getAllLines());
		DebugBuffer.addLine(DebugLevel.DEBUG,"Zum anordnen der Technologien gebrauchte Zeit: "+(System.currentTimeMillis() - startTime)+" ms");


		return allLevels;
	}


}
