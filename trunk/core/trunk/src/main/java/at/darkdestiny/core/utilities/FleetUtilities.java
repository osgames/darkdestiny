/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.utilities;

import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.dao.FleetLoadingDAO;
import at.darkdestiny.core.dao.GroundTroopDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.dao.ViewTableDAO;
import at.darkdestiny.core.exceptions.NameCouldNotBeResolvedException;
import at.darkdestiny.core.exceptions.PlanetNameNotUniqueException;
import at.darkdestiny.core.exceptions.SystemNameNotUniqueException;
import at.darkdestiny.core.fleet.LoadingInformation;
import at.darkdestiny.core.model.FleetLoading;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ViewTable;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.ships.ShipStorage;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class FleetUtilities {
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);

    public static void moveFleetLoadingTotal(int sourceFleet, int targetFleet) {
        PlayerFleetExt srcFleet = new PlayerFleetExt(sourceFleet);
        LoadingInformation li = srcFleet.getLoadingInformation();

        ArrayList<ShipStorage> ssList = li.getLoadedStorage();

        ArrayList<FleetLoading> flList = new ArrayList<FleetLoading>();
        ArrayList<FleetLoading> flUpdList = new ArrayList<FleetLoading>();

        for (ShipStorage ss : ssList) {
            FleetLoading fl = new FleetLoading();
            fl.setFleetId(targetFleet);
            fl.setId(ss.getId());
            fl.setLoadType(ss.getLoadtype());

            FleetLoading flNew = flDAO.getByFleetAndFreight(targetFleet, ss.getLoadtype(), ss.getId());
            if (flNew != null) {
                flNew.setCount(flNew.getCount() + ss.getCount());
                flUpdList.add(flNew);
            } else {
                fl.setCount(ss.getCount());
                flList.add(fl);
            }

            flDAO.deleteByFleetAndFreight(sourceFleet, ss.getLoadtype(), ss.getId());
        }

        flDAO.updateAll(flUpdList);
        flDAO.insertAll(flList);
    }

    public static void moveExcessFleetLoading(int sourceFleet, int targetFleet) {
        PlayerFleetExt srcFleet = new PlayerFleetExt(sourceFleet);
        LoadingInformation li = srcFleet.getLoadingInformation();

        int maxRessStorage = li.getRessSpace();
        int maxTroopStorage = li.getTroopSpace();

        int toMoveRess = Math.max(0, li.getCurrLoadedRess() - maxRessStorage);
        int toMoveTroops = Math.max(0, (li.getCurrLoadedTroops() + li.getCurrLoadedPopulation()) - maxTroopStorage);

        ArrayList<ShipStorage> ssList = li.getLoadedStorage();
        ArrayList<FleetLoading> flList = new ArrayList<FleetLoading>();

        // Move population and troops to new Fleet
        if (toMoveTroops > 0) {
            // Check for ground troops first and move by largest space consumption
            TreeMap<Integer,ShipStorage> sizeMap = new TreeMap<Integer,ShipStorage>();

            for (ShipStorage ss : ssList) {
                if (ss.getLoadtype() == GameConstants.LOADING_TROOPS) {
                    GroundTroop gt = gtDAO.findById(ss.getId());
                    sizeMap.put(gt.getSize(), ss);
                }
            }

            Iterator<Integer> sizeMapIt = sizeMap.descendingMap().keySet().iterator();
            while ((toMoveTroops > 0) && sizeMapIt.hasNext()) {
                int size = sizeMapIt.next();
                ShipStorage actEntry = sizeMap.get(size);

                int capOfActType = size * actEntry.getCount();
                if (capOfActType <= toMoveTroops) {
                    flDAO.deleteByFleetAndFreight(sourceFleet, FleetLoading.LOADTYPE_TROOPS, actEntry.getId());
                    flDAO.changeLoadingAmount(targetFleet, FleetLoading.LOADTYPE_TROOPS,
                            actEntry.getId(), actEntry.getCount());
                    toMoveTroops -= capOfActType;
                } else {
                    int moveCount = (int)Math.ceil(toMoveTroops / size);
                    flDAO.changeLoadingAmount(sourceFleet, FleetLoading.LOADTYPE_TROOPS,
                            actEntry.getId(), -moveCount);
                    flDAO.changeLoadingAmount(targetFleet, FleetLoading.LOADTYPE_TROOPS,
                            actEntry.getId(), moveCount);
                    toMoveTroops -= moveCount;
                }
            }

            // Move Population
            if (toMoveTroops > 0) {
                for (ShipStorage ss : ssList) {
                    if (ss.getLoadtype() == FleetLoading.LOADTYPE_POPULATION) {
                        if (toMoveTroops < ss.getCount()) {
                            flDAO.changeLoadingAmount(sourceFleet, FleetLoading.LOADTYPE_POPULATION,
                                    ss.getId(), -toMoveTroops);
                        } else if (toMoveTroops == ss.getCount()) {
                            flDAO.deleteByFleetAndFreight(sourceFleet, FleetLoading.LOADTYPE_POPULATION, ss.getId());
                        }
                        flDAO.changeLoadingAmount(targetFleet, FleetLoading.LOADTYPE_POPULATION,
                                ss.getId(), toMoveTroops);
                        toMoveTroops = 0;
                    }
                }
            }
        }

        // Move ressources to new Fleet
        if (toMoveRess > 0) {
            Iterator<ShipStorage> ssIt = ssList.iterator();

            while ((toMoveRess > 0) && ssIt.hasNext()) {
                ShipStorage actEntry = ssIt.next();

                if (actEntry.getCount() <= toMoveRess) {
                    flDAO.deleteByFleetAndFreight(sourceFleet, FleetLoading.LOADTYPE_RESS, actEntry.getId());
                    flDAO.changeLoadingAmount(targetFleet, FleetLoading.LOADTYPE_RESS,
                            actEntry.getId(), actEntry.getCount());
                    toMoveRess -= actEntry.getCount();
                } else {
                    flDAO.changeLoadingAmount(sourceFleet, FleetLoading.LOADTYPE_RESS,
                            actEntry.getId(), -actEntry.getCount());
                    flDAO.changeLoadingAmount(targetFleet, FleetLoading.LOADTYPE_RESS,
                            actEntry.getId(), actEntry.getCount());
                    toMoveTroops -= actEntry.getCount();
                }
            }
        }
    }

    public static int resolveLocationByNameOrId(String input, int userId, boolean system)
        throws PlanetNameNotUniqueException, SystemNameNotUniqueException, NameCouldNotBeResolvedException {

        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException nfe) {
            if (system) {
                ArrayList<at.darkdestiny.core.model.System> sysList = sDAO.findByName(input);
                for (Iterator<at.darkdestiny.core.model.System> sIt = sysList.iterator();sIt.hasNext();) {
                    at.darkdestiny.core.model.System s = sIt.next();

                    ArrayList<ViewTable> vtList = vtDAO.findByUserAndSystem(userId, s.getId());
                    if (vtList.isEmpty()) sIt.remove();
                }

                if (sysList.size() > 1) {
                    throw new SystemNameNotUniqueException("System Name nicht eindeutig: " + input);
                } else if (sysList.size() == 1) {
                    return sysList.get(0).getId();
                }
            } else {
                ArrayList<PlayerPlanet> ppList = ppDAO.findByName(input);

                for (Iterator<PlayerPlanet> ppIt = ppList.iterator();ppIt.hasNext();) {
                    Planet p = pDAO.findById(ppIt.next().getPlanetId());
                    ViewTable vt = vtDAO.findBy(userId, p.getSystemId(), p.getId());
                    if (vt == null) ppIt.remove();
                }

                if (ppList.size() > 1) {
                    throw new PlanetNameNotUniqueException("Planeten Name nicht eindeutig: " + input);
                } else if (ppList.size() == 1) {
                    return ppList.get(0).getPlanetId();
                }
            }
        }

        throw new NameCouldNotBeResolvedException("Name konnte nicht gefunden werden: " + input);
    }
}
