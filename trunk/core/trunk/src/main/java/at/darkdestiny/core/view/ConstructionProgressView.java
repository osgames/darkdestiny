/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view;

import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.model.RessourceCost;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ConstructionProgressView {


    private Action action;
    private Construction construction;
    private ArrayList<RessourceCost> ressourceCost;
    private ProductionOrder productionOrder;

    /**
     * @return the action
     */
    public Action getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(Action action) {
        this.action = action;
    }

    /**
     * @return the construction
     */
    public Construction getConstruction() {
        return construction;
    }

    /**
     * @param construction the construction to set
     */
    public void setConstruction(Construction construction) {
        this.construction = construction;
    }

    /**
     * @return the productionOrder
     */
    public ProductionOrder getProductionOrder() {
        return productionOrder;
    }

    /**
     * @param productionOrder the productionOrder to set
     */
    public void setProductionOrder(ProductionOrder productionOrder) {
        this.productionOrder = productionOrder;
    }

    /**
     * @return the ressourceCost
     */
    public ArrayList<RessourceCost> getRessourceCost() {
        return ressourceCost;
    }

    /**
     * @param ressourceCost the ressourceCost to set
     */
    public void setRessourceCost(ArrayList<RessourceCost> ressourceCost) {
        this.ressourceCost = ressourceCost;
    }
}
