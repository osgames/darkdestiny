/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai;

import at.darkdestiny.core.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class AIThreadHandler {

    private static ArrayList<AIThread> threads = new ArrayList<AIThread>();

    protected static void addThread(AIThread thread) {
        getThreads().add(thread);
    }

    /**
     * @return the threads
     */
    public static ArrayList<AIThread> getThreads() {
        return threads;
    }

    public static void addThread(Integer userId) {
        long initValue = (long) (Math.random() * 60000);
        AIThread thread = new AIThread("AI" + userId, Service.userDAO.findById(userId));
        thread.start();
        AIDebugBuffer.log(thread, AIDebugLevel.INFO, "Starting AI thread for userId : " + userId + " initValue " + initValue);
        addThread(thread);
    }
    
    public static void shutdownAI() throws Exception {
        for (AIThread thread : AIThreadHandler.getThreads()) {
            thread.setRun(false);
        }        
        
        boolean threadsRunning = false;
        
        do {
            threadsRunning = false;        
            Thread.sleep(20);
            
            for (AIThread thread : threads) {
                if (thread.isAlive()) threadsRunning = true;
            }                  
        } while (threadsRunning);
        
        threads.clear();
    }
}
