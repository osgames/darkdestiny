/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.HangarRelation;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class HangarRelationDAO extends ReadWriteTable<HangarRelation> implements GenericDAO {
    public ArrayList<HangarRelation> findByFleetAndRelToDesign(int fleetId, int relationToDesign) {
        HangarRelation hr = new HangarRelation();
        hr.setFleetId(fleetId);
        hr.setRelationToDesign(relationToDesign);
        hr.setRelationToSF(1);

        return find(hr);
    }

    public ArrayList<HangarRelation> findByFleetAndRelToDesignInternal(int fleetId, int relationToDesign) {
        HangarRelation hr = new HangarRelation();
        hr.setFleetId(fleetId);
        hr.setRelationToDesign(relationToDesign);
        hr.setRelationToSF(0);

        return find(hr);
    }

    public ArrayList<HangarRelation> findByFleetId(int fleetId) {
        HangarRelation hr = new HangarRelation();
        hr.setFleetId(fleetId);

        return find(hr);
    }

    public ArrayList<HangarRelation> findByDesignId(int designId) {
        HangarRelation hr = new HangarRelation();
        hr.setDesignId(designId);

        return find(hr);
    }
}
