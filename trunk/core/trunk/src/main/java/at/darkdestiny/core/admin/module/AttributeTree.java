/*
 * This class holds extension tree for module attributes
 * and provides fast access to actual values
 */
package at.darkdestiny.core.admin.module;

 import at.darkdestiny.core.dao.AttributeDAO;import at.darkdestiny.core.dao.ModuleAttributeDAO;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.model.Attribute;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.model.ModuleAttribute;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.AttributeDAO;
import at.darkdestiny.core.dao.ModuleAttributeDAO;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.model.Attribute;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.model.ModuleAttribute;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.Ressource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class AttributeTree {

    private static final Logger log = LoggerFactory.getLogger(AttributeTree.class);
    private final int userId;
    private final long time;
    private HashMap<Integer, Long> resTech = new HashMap<Integer,Long>();
    private HashMap<Integer, HashMap<Integer, HashSet<ModuleAttribute>>> values =
            new HashMap<Integer, HashMap<Integer, HashSet<ModuleAttribute>>>();
    private ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    private AttributeDAO aDAO = (AttributeDAO) DAOFactory.get(AttributeDAO.class);
    private ModuleAttributeDAO maDAO = (ModuleAttributeDAO) DAOFactory.get(ModuleAttributeDAO.class);
    private PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);

    public AttributeTree(int userId, long time) {
        this.userId = userId;
        this.time = time;
        initalizeTree();
    }

    private void initalizeTree() {
        ArrayList<Module> modules = mDAO.findAll();
        ArrayList<Attribute> attributes = aDAO.findAll();

        ArrayList<PlayerResearch> pRes = prDAO.findByUserId(userId);
        if (pRes != null) {
            for (PlayerResearch pr : pRes) {
                resTech.put(pr.getResearchId(), pr.getResTime());
            }
        }

        for (Module m : modules) {
            // log.debug("Looping Module " + m.getName());

            ArrayList<ModuleAttribute> mAttribList = maDAO.findByModuleId(m.getId());

            // Read all define values and look for extensions
            // HashMap<Integer, HashMap<Integer, Double>> chassisAttrib = new HashMap<Integer, HashMap<Integer, Double>>();
            HashMap<Integer, HashSet<ModuleAttribute>> chassisAttrib = new HashMap<Integer, HashSet<ModuleAttribute>>();

            for (ModuleAttribute ma : mAttribList) {
                // log.debug("Looping ModuleAttribute (BASIC) " + ma.getChassisId() + " / " + ma.getModuleId() + " / " + ma.getAttributeId() + " >> ATTRIBUTE TYPE: " + ma.getAttributeType());

                if (ma.getAttributeType() == AttributeType.DEFINE) {
                    // Load base entry
                    if (chassisAttrib.containsKey(ma.getChassisId())) {
                        /*
                        HashMap<Integer, Double> attribValue = chassisAttrib.get(ma.getChassisId());
                        attribValue.put(ma.getAttributeId(), ma.getValue());
                        */
                        HashSet<ModuleAttribute> attribValues = chassisAttrib.get(ma.getChassisId());
                        attribValues.add(ma);
                        // log.debug("["+ma.getModuleId()+"] Adding Attribute " + aDAO.findById(ma.getAttributeId()).getName() + " with value " + ma.getValue() + " for chassis " + ma.getChassisId());
                    } else {
                        // HashMap<Integer, Double> attribValue = new HashMap<Integer, Double>();
                        // attribValue.put(ma.getAttributeId(), ma.getValue());
                        // chassisAttrib = new HashMap<Integer, HashSet<ModuleAttribute>>();
                        HashSet<ModuleAttribute> attribValues = new HashSet<ModuleAttribute>();
                        attribValues.add(ma);

                        chassisAttrib.put(ma.getChassisId(), attribValues);
                        // log.debug("["+ma.getModuleId()+"] Adding Attribute " + aDAO.findById(ma.getAttributeId()).getName() + " with value " + ma.getValue() + " for chassis " + ma.getChassisId());
                    }

                    double actValue = ma.getValue();

                    // Look for dependencies
                    ArrayList<ModuleAttribute> extensions = maDAO.findByAppliedToId(ma.getId());
                    if (extensions != null) {
                        for (ModuleAttribute maExt : extensions) {
                            // log.debug("Process Dependency (BASIC) " + ma.getChassisId() + " / " + ma.getModuleId() + " / " + ma.getAttributeId() + " >> ATTRIBUTE TYPE: " + ma.getAttributeType());
                            actValue += processDependency(maExt.getId(), actValue);
                        }

                        // HashMap<Integer, Double> attribValue = chassisAttrib.get(ma.getChassisId());
                        // attribValue.put(ma.getAttributeId(), actValue);
                        HashSet<ModuleAttribute> attribValues = chassisAttrib.get(ma.getChassisId());
                        ma.setValue(actValue);
                    }
                }
            }

            values.put(m.getId(), chassisAttrib);
            // log.debug("Adding " + chassisAttrib.size() + " values for module " + m.getId());
        }
    }

    private double processDependency(int mAttribId, double currValue) {
        // Process current Attribute
        ModuleAttribute currAttrib = maDAO.findById(mAttribId);

        if (currAttrib.getResearchId() != 0) {
            if (resTech.get(currAttrib.getResearchId()) == null) {
                return 0d;
            }
            if (time < resTech.get(currAttrib.getResearchId())) {
                return 0d;
            }
        }

        double modifyValueBy = 0d;

        if (currAttrib.getAttributeType() == AttributeType.INCREASE) {
            modifyValueBy += currAttrib.getValue();
        } else if (currAttrib.getAttributeType() == AttributeType.INCREASE_PERC) {
            modifyValueBy += currValue / 100d * currAttrib.getValue();
        } else if (currAttrib.getAttributeType() == AttributeType.DECREASE) {
            modifyValueBy -= currAttrib.getValue();
        } else if (currAttrib.getAttributeType() == AttributeType.DECREASE_PERC) {
            modifyValueBy -= currValue / 100d * currAttrib.getValue();
        }

        double result = modifyValueBy;

        // Process further expansions
        ArrayList<ModuleAttribute> extensions = maDAO.findByAppliedToId(currAttrib.getId());
        if (extensions != null) {
            for (ModuleAttribute maExt : extensions) {
                log.debug("Process Dependency (RECURSIVE) " + maExt.getChassisId() + " / " + maExt.getModuleId() + " / " + maExt.getAttributeId() + " >> ATTRIBUTE TYPE: " + maExt.getAttributeType());
                result += processDependency(maExt.getId(), modifyValueBy);
            }
        }

        return result;
    }

    public double getAttributeValueFor(int chassisId, int moduleId, EAttribute attribute) {
        ModuleAttributeResult mar = getAllAttributes(chassisId,moduleId);
        return mar.getAttributeValue(attribute);
    }

    public ModuleAttributeResult getAllAttributes(int chassisId, int moduleId) {
        ModuleAttributeResult mar = null;
        ArrayList<ModuleAttributeResEntry> attribList = new ArrayList<ModuleAttributeResEntry>();

        // log.debug("Search for chassisId " + chassisId + " and moduleId " + moduleId);

        if (values.get(moduleId) != null) {
            // log.debug("Contains module Id");
            if (values.get(moduleId).get(chassisId) != null) {
                // log.debug("Contains chassis Id");
                for (ModuleAttribute ma : values.get(moduleId).get(chassisId)) {
                    // log.debug("MA Ref Type = " + ma.getRefType());
                    if (ma.getRefType() != null) {
                        // log.debug("["+ma.getModuleId()+"] Returning referenced attribute " + ma.getAttributeType().name() + " with value " + ma.getValue() + " referenceType " + ma.getRefType().name() + ">>" + ma.getRefId());
                        attribList.add(new ModuleAttributeResEntryReferenced(ma));
                    } else {
                        // log.debug("["+ma.getModuleId()+"] Returning single attribute " + ma.getAttributeType().name() + " with value " + ma.getValue());
                        attribList.add(new ModuleAttributeResEntrySingle(ma));
                    }
                }
            }

            if (values.get(moduleId).get(0) != null) {
                // log.debug("Contains chassis Id");
                for (ModuleAttribute ma : values.get(moduleId).get(0)) {
                    // log.debug("MA Ref Type = " + ma.getRefType());
                    if (ma.getRefType() != null) {
                        // log.debug("["+ma.getModuleId()+"] Returning referenced attribute " + ma.getAttributeType().name() + " with value " + ma.getValue() + " referenceType " + ma.getRefType().name() + ">>" + ma.getRefId());
                        attribList.add(new ModuleAttributeResEntryReferenced(ma));
                    } else {
                        // log.debug("["+ma.getModuleId()+"] Returning single attribute " + ma.getAttributeType().name() + " with value " + ma.getValue());
                        attribList.add(new ModuleAttributeResEntrySingle(ma));
                    }
                }
            }
        }

        return new ModuleAttributeResult(attribList);
    }

    public ArrayList<RessAmountEntry> getRessourceCost(int chassisId, int moduleId) {
        ArrayList<RessAmountEntry> ressCost = new ArrayList<RessAmountEntry>();

        ModuleAttributeResult mar = getAllAttributes(chassisId, moduleId);
        for (Ressource r : rDAO.findAllPayableRessources()) {
            double value = mar.getAttributeValue(at.darkdestiny.core.enumeration.EAttribute.RESSOURCE, r.getId(), ReferenceType.RESSOURCE);
            if (value > 0d) {
                ressCost.add(new RessAmountEntry(r.getId(),value));
            }
        }

        return ressCost;
    }

    public boolean contains(int chassisId, int moduleId) {
        if (values.containsKey(moduleId)) {
            if (values.get(moduleId).containsKey(chassisId)) {
                return true;
            }
        }

        return false;
    }
}
