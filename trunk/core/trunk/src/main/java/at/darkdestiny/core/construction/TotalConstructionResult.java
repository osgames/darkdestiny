/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.construction;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.ECompare;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class TotalConstructionResult {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    
    private final Integer userId;
    private final Integer constructionId;
    private final ECompare compare;
    private final int count;
    
    // Grouped by construction id
    public HashMap<Integer,HashMap<Integer,Integer>> allConsByConsId = Maps.newHashMap();
    // Grouped by planet id
    public HashMap<Integer,HashMap<Integer,Integer>> allConsByPlanetId = Maps.newHashMap();
    // Total count for a construction empire wide
    public HashMap<Integer,Integer> totalCountByConsId = Maps.newHashMap();
    
    private Boolean compareResult = null;
    
    public TotalConstructionResult() {
        this.userId = null;
        this.constructionId = null;
        this.compare = null;
        this.count = 0;
        initialize();
    }
    
    public TotalConstructionResult(int userId) {
        this.userId = userId;
        this.constructionId = null;
        this.compare = null;
        this.count = 0;
        initialize();
    }
    
    public TotalConstructionResult(int constructionId, Integer userId) {
        this.userId = userId;
        this.constructionId = constructionId;
        this.compare = null;
        this.count = 0;        
        initialize();
    }
       
    public TotalConstructionResult(int constructionId, int userId, ECompare compare, int count) {
        this.userId = userId;
        this.constructionId = constructionId;
        this.compare = null;
        this.count = 0;     
        initialize();
    }           
    
    private void initialize() {
        ArrayList<PlayerPlanet> ppList;
        
        if (userId != null) {
            ppList = ppDAO.findByUserId(userId);
        } else {
            ppList = ppDAO.findAll();
        }        
        
        if (compare != null) {
            switch (compare) {
                case EQUAL:
                    compareResult = Boolean.FALSE;
                    break;
                case SMALLER_THAN:
                    compareResult = Boolean.TRUE;
                    break;
                case LARGER_THAN:
                    compareResult = Boolean.FALSE;
                    break;
            }
        }
        
        for (PlayerPlanet pp : ppList) {
            if (constructionId == null) {
                addConstructions(pcDAO.findByPlanetId(pp.getPlanetId()));
            } else if (constructionId != null) {
                PlanetConstruction pc = pcDAO.findBy(pp.getPlanetId(), constructionId);
                if (pc == null) continue;
                
                if (compare != null) {
                    switch (compare) {
                        case EQUAL:
                            throw new RuntimeException("Not exported");
                        case SMALLER_THAN:
                            // Break if we evaluate to false
                            addConstruction(pc);
                            if (getCountForBuilding(constructionId) >= count) {
                                compareResult = Boolean.FALSE;
                                return;
                            }                            
                            break;
                        case LARGER_THAN:
                            // Break if we evaluate to true
                            if (pc.getNumber() > count) {
                                compareResult = Boolean.TRUE;
                                return;
                            } else {
                                addConstruction(pc);
                                if (getCountForBuilding(constructionId) > count) {
                                    compareResult = Boolean.TRUE;
                                    return;
                                }
                            }
                            
                            break;
                    }
                } else {                    
                    addConstruction(pc);
                }
            }
        }
    }
    
    private void addConstruction(PlanetConstruction pc) {
        HashMap<Integer,Integer> byConsId = allConsByConsId.get(pc.getConstructionId());
        
        if (byConsId == null) {
            byConsId = new HashMap<Integer,Integer>();
            byConsId.put(pc.getPlanetId(),pc.getNumber());
            allConsByConsId.put(pc.getConstructionId(),byConsId);
        } else {
            byConsId.put(pc.getPlanetId(),pc.getNumber());
        }
        
        HashMap<Integer,Integer> byPlanetId = allConsByPlanetId.get(pc.getConstructionId());
        
        if (byPlanetId == null) {
            byPlanetId = new HashMap<Integer,Integer>();
            byPlanetId.put(pc.getConstructionId(),pc.getNumber());
            allConsByPlanetId.put(pc.getPlanetId(),byPlanetId);
        } else {
            byPlanetId.put(pc.getConstructionId(), pc.getNumber());
        }
        
        Integer countEntry = totalCountByConsId.get(pc.getConstructionId());
        if (countEntry == null) {
            totalCountByConsId.put(pc.getConstructionId(),pc.getNumber());
        } else {
            totalCountByConsId.put(pc.getConstructionId(), countEntry + pc.getNumber());
        }
    }
    
    private void addConstructions(ArrayList<PlanetConstruction> pcList) {
        for (PlanetConstruction pc : pcList) {
            addConstruction(pc);
        }
    }
    
    public boolean getCompareResult() {
        return compareResult;
    }
    
    public int getCountForBuilding(int constructionId) {
        if ((this.constructionId != null) && (constructionId != this.constructionId)) return 0;
        
        int tmpCount = 0;
        
        Integer countEntry = totalCountByConsId.get(constructionId);
        if (countEntry != null) {
            tmpCount = countEntry;
        }
        
        return tmpCount;
    }
    
    public ArrayList<Integer> getAllPlanetsWithConstruction(int constructionId) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        
        for (Map.Entry<Integer,HashMap<Integer,Integer>> entry : allConsByPlanetId.entrySet()) {
            HashMap<Integer,Integer> constructionEntry = entry.getValue();
            
            if (constructionEntry.containsKey(constructionId)) {
                result.add(entry.getKey());
            }
        }
        
        return result;
    }
}
