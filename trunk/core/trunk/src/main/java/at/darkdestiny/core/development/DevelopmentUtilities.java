/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.development;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.SpecialTradeDAO;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.SpecialTrade;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class DevelopmentUtilities {
    private static PlanetConstructionDAO pcDAO = DAOFactory.get(PlanetConstructionDAO.class);
    private static PlayerPlanetDAO ppDAO = DAOFactory.get(PlayerPlanetDAO.class);
    private static SpecialTradeDAO stDAO = DAOFactory.get(SpecialTradeDAO.class);
    
    public static final float HOME_PLANET_DEVPOINTS = 10f;
    
    public static float getConsSupplyRatio(ProductionResult pr) {
        float suppliedConsumerGoodsPerc;
        
        if (pr.getRessBaseConsumption(7) > 0) {
            suppliedConsumerGoodsPerc = 1f / pr.getRessBaseConsumption(7) * pr.getRessConsumption(7);            
        } else {
            return 1f;
        }       
        
        return suppliedConsumerGoodsPerc;
    }
    
    public static float getDevPointsAdmin(PlayerPlanet pp) {
        float administrationFactor = -.5f;
        boolean isMining = pp.getColonyType() == EColonyType.MINING_COLONY;
        
        PlanetConstruction pc = pcDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETGOVERNMENT);
        if (pc != null) {
            administrationFactor = 2f;
        } else {
            pc = pcDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETADMINISTRATION);
            if (pc != null) {
                administrationFactor = 1f;
            } else {
                pc = pcDAO.findBy(pp.getPlanetId(), Construction.ID_LOCALADMINISTRATION);
                if (pc != null) {
                    administrationFactor = 0f;
                }
            }
        }
        
        if (isMining) administrationFactor = -.5f;        
        
        return administrationFactor;
    }    
    
    @Deprecated
    public static float getDevelopmentPointsForPlanet(int planetId) {
        // Fixed Value for Main Planet (i.e. 5)
        // Mining colonies -0.5
        // Normal colonies -0.5 // Local Administration 0 // Local Government 1 // Planetary Government 2
        // Depending on satisfaction of consumer goods 0.1 up to 1 for megacities
        int megaCityCount = 0;
        float suppliedConsumerGoodsPerc = 0f;
        boolean isMining = false;
        boolean isHome = false;
        float administrationFactor = -.5f;
        
        PlayerPlanet pp = ppDAO.findById(planetId);
        isHome = pp.isHomeSystem();
        isMining = pp.getColonyType() == EColonyType.MINING_COLONY;
        
        PlanetConstruction pc = pcDAO.findBy(planetId, Construction.ID_MEGACITY);
        if (pc != null) {
            megaCityCount = pc.getNumber();
        }
        
        pc = pcDAO.findBy(planetId, Construction.ID_PLANETGOVERNMENT);
        if (pc != null) {
            administrationFactor = 2f;
        } else {
            pc = pcDAO.findBy(planetId, Construction.ID_PLANETADMINISTRATION);
            if (pc != null) {
                administrationFactor = 1f;
            } else {
                pc = pcDAO.findBy(planetId, Construction.ID_LOCALADMINISTRATION);
                if (pc != null) {
                    administrationFactor = 0f;
                }
            }
        }
        
        if (isMining) administrationFactor = -.5f;
        
        SpecialTrade st = stDAO.getByPlanetId(planetId);
        if (st != null) {
            suppliedConsumerGoodsPerc = Math.max(0.1f, 1f / st.getConsumption() * st.getIncomingGoods());
        }
        
        float totalValue = 0f;
        if (isHome) totalValue += HOME_PLANET_DEVPOINTS;
        totalValue += megaCityCount * suppliedConsumerGoodsPerc;
        totalValue += 1f * administrationFactor;
        
        return totalValue;
    }
}
