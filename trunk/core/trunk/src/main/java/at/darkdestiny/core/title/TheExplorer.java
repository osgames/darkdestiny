package at.darkdestiny.core.title;

import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.utilities.TitleUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;


public class TheExplorer extends AbstractTitle {


    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
	public boolean check(int userId) {

        for (ShipDesign sd : (ArrayList<ShipDesign>) sdDAO.findByUserId(userId)) {
            if (sd.getChassis() == Chassis.ID_FRIGATE) {
            TitleUtilities.incrementCondition(ConditionToTitle.EXPLORER_FRIGATEDESIGN, userId);
                break;
            }
        }
		return true;
	}
}
