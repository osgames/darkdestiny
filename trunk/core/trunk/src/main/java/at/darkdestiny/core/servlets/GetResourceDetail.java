/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.servlets;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.planetcalc.PlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.service.TransportService;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class GetResourceDetail extends HttpServlet {
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            int planetId = Integer.parseInt(request.getParameter("planetId"));
            int userId = Integer.parseInt(request.getParameter("userId"));
            int ressId = Integer.parseInt(request.getParameter("ressId"));

            PlanetCalculation pc = new PlanetCalculation(planetId);
            PlanetCalcResult pcr = pc.getPlanetCalcData();
            ProductionResult pr = pcr.getProductionData();

            long production = pr.getRessProduction(ressId);
            long consumption = -pr.getRessConsumption(ressId);
            long tradeIn = 0l;
            long tradeOut = 0l;

            HashMap<Integer, HashMap<Boolean, Long>> tradeBalances = TransportService.findShippingBalanceForPlanetSorted(planetId);
            HashMap<Boolean, Long> balances = tradeBalances.get(ressId);
             if (balances != null) {
                if (balances.get(Boolean.TRUE) != null) {
                    tradeOut = -Math.round(balances.get(Boolean.TRUE));
                }
                if (balances.get(Boolean.FALSE) != null) {
                    tradeIn = Math.round(balances.get(Boolean.FALSE));
                }
            }

            String ressName = ML.getMLStr(rDAO.findRessourceById(ressId).getName(),userId);
            long totalProduction = production + consumption;
            long totalTrade = tradeIn + tradeOut;
            long totalInc = production + tradeIn;
            long totalDec = consumption + tradeOut;
            long total = totalInc + totalDec;

            StringBuilder json = new StringBuilder();
            json.append("{ ");

            json.append("\"ressName\":\""+ressName+"\", ");

            json.append("\"production\": ");
            json.append("{ \"increase\": { \"value\":\""+production+"\", \"color\":\""+getColorForNumber(production)+"\" }, ");
            json.append("\"decrease\": { \"value\":\""+Math.abs(consumption)+"\", \"color\":\""+getColorForNumber(consumption)+"\" }, ");
            json.append("\"total\": { \"value\":\""+totalProduction+"\", \"color\":\""+getColorForNumber(totalProduction)+"\" } }, ");

            json.append("\"trade\": ");
            json.append("{ \"increase\": { \"value\":\""+tradeIn+"\", \"color\":\""+getColorForNumber(tradeIn)+"\" }, ");
            json.append("\"decrease\": { \"value\":\""+Math.abs(tradeOut)+"\", \"color\":\""+getColorForNumber(tradeOut)+"\" }, ");
            json.append("\"total\": { \"value\":\""+totalTrade+"\", \"color\":\""+getColorForNumber(totalTrade)+"\" } }, ");

            json.append("\"total\": ");
            json.append("{ \"increase\": { \"value\":\""+totalInc+"\", \"color\":\""+getColorForNumber(totalInc)+"\" }, ");
            json.append("\"decrease\": { \"value\":\""+Math.abs(totalDec)+"\", \"color\":\""+getColorForNumber(totalDec)+"\" }, ");
            json.append("\"total\": { \"value\":\""+total+"\", \"color\":\""+getColorForNumber(total)+"\" } } }");

            String resultStr = "{ \"content\":\""+ressName+" <BR> <TABLE border=0><TR><TD> </TD><TD><FONT size=1pt>Produktion</FONT></TD><TD><FONT size=1pt> Verbrauch</FONT></TD>" +
                    "<TD> <FONT size=1pt> Gesamt</FONT></TD></TR><TR><TD><FONT size=1pt> Planet</FONT></TD>" +
                    "<TD bgcolor="+getColorForNumber(production)+" align=right><FONT size=1pt  align=right color=#000000><B>"+FormatUtilities.getFormattedNumber(production)+"</B></FONT></TD>" +
                    "<TD bgcolor="+getColorForNumber(consumption)+" align=right><FONT  align=right size=1pt color=#000000><B>"+FormatUtilities.getFormattedNumber(Math.abs(consumption))+"</B></FONT></TD>" +
                    "<TD bgcolor="+getColorForNumber(totalProduction)+" align=right><FONT  align=right size=1pt color=#000000><B>"+FormatUtilities.getFormattedNumber(totalProduction)+"</B></FONT></TD></TR>" +
                    "<TR><TD> <FONT size=1pt> Handel</FONT></TD><TD bgcolor="+getColorForNumber(tradeIn)+" align=right><FONT align=right size=1pt color=#000000><B>"+FormatUtilities.getFormattedNumber(tradeIn)+"</B></FONT></TD>" +
                    "<TD bgcolor="+getColorForNumber(tradeOut)+" align=right><FONT size=1pt  align=right color=#000000><B>"+FormatUtilities.getFormattedNumber(Math.abs(tradeOut))+"</B></FONT></TD>" +
                    "<TD bgcolor="+getColorForNumber(totalTrade)+" align=right><FONT  align=right size=1pt color=#000000><B>"+FormatUtilities.getFormattedNumber(totalTrade)+"</B></FONT></TD></TR>" +
                    "<TR><TD> <FONT size=1pt> Gesamt</FONT></TD> <TD bgcolor="+getColorForNumber(totalInc)+" align=right><FONT align=right size=1pt color=#000000><B>"+FormatUtilities.getFormattedNumber(totalInc)+"</B></FONT></TD>" +
                    "<TD bgcolor="+getColorForNumber(totalDec)+" align=right><FONT  align=right size=1pt color=#000000><B>"+FormatUtilities.getFormattedNumber(Math.abs(totalDec))+"</B></FONT></TD>" +
                    "<TD bgcolor="+getColorForNumber(total)+" align=right><FONT  align=right size=1pt color=#000000><B>"+FormatUtilities.getFormattedNumber(total)+"</B></FONT></TD></TR></TABLE>\" }";

            out.println(json.toString());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in GetResourceDetail: ", e);
        } finally {
            out.close();
        }
    }

    private String getColorForNumber(long number) {
        if (number == 0l) return "#000000";
        if (number > 0l) return "#33FF00";
        return "#FFCC00";
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
