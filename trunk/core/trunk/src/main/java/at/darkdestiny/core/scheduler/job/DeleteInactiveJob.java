/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.scheduler.job;

import at.darkdestiny.core.CheckForUpdate;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserSettings;
import at.darkdestiny.core.service.MainService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.DestructionUtilities;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Admin
 */
public class DeleteInactiveJob implements Job {

    public static final int LIMIT_HOLIDAY = 7;

    public static final int LIMIT_DELETION_1 = 4;
    public static final int LIMIT_DELETION_2 = 14;


    public void execute(JobExecutionContext jec) throws JobExecutionException {
        CheckForUpdate.requestUpdate();

        deleteInactive();
    }

    public void deleteInactive(){
        int currentTick = GameUtilities.getCurrentTick2();
        ArrayList<User> users = MainService.getAllUsers();

        int ticksPerDay = GameUtilities.getTicksPerDay();

        for (User u : users) {                        
            if (!u.getUserType().equals(EUserType.HUMAN)) {
                continue;
            }

            int inactiveTicks = currentTick - u.getLastUpdate();

            // DELETE HARDCORE
            if (!u.getSystemAssigned() || !u.getUserConfigured()) {
                if (inactiveTicks > ticksPerDay) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "User " + u.getGameName() + " [" + u.getId() + "] was deleted (REASON: no system selected or configured) ["+inactiveTicks+"]");
                    DestructionUtilities.destroyPlayer(u.getId());
                }
            } else if (u.getJoinDate().equals((long) u.getLastUpdate())) {
                UserSettings us = Service.userSettingsDAO.findByUserId(u.getId());
                int holidayLimit = (us.getLeaveDays() + LIMIT_HOLIDAY) * ticksPerDay;

                if (inactiveTicks > ticksPerDay * LIMIT_DELETION_1 + holidayLimit) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "User " + u.getGameName() + " [" + u.getId() + "] was deleted (REASON: system selected but no action taken) ["+inactiveTicks+"]");
                    DestructionUtilities.destroyPlayer(u.getId());
                }          
            } else {
                UserSettings us = Service.userSettingsDAO.findByUserId(u.getId());
                int holidayLimit = (us.getLeaveDays() + LIMIT_HOLIDAY) * ticksPerDay;

                if (inactiveTicks > ((ticksPerDay * LIMIT_DELETION_2) + holidayLimit)) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "User " + u.getGameName() + " [" + u.getId() + "] was deleted (REASON: 2 weeks inactive and no holiday modus or expired) ["+inactiveTicks+"]");
                    DestructionUtilities.destroyPlayer(u.getId());
                } else if (u.getDeleteDate() > 0) {
                    int deleteTimer = (GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - u.getDeleteDate()));
                    if (deleteTimer <= 0) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "User " + u.getGameName() + " [" + u.getId() + "] was deleted (REASON: 2 requested deletion) ["+inactiveTicks+"]");
                        DestructionUtilities.destroyPlayer(u.getId());
                    }
                }
            }
        }
    }

}
