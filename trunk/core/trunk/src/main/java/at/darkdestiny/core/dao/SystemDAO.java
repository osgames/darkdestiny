/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class SystemDAO extends ReadWriteTable<at.darkdestiny.core.model.System> implements GenericDAO{

    public void findAllOrdered() {
        this.findAll();
    }

    public at.darkdestiny.core.model.System findById(int id) {
        at.darkdestiny.core.model.System s = new at.darkdestiny.core.model.System();
        s.setId(id);
        return (at.darkdestiny.core.model.System)get(s);
    }

    public ArrayList<at.darkdestiny.core.model.System> findByName(String name) {
        at.darkdestiny.core.model.System s = new at.darkdestiny.core.model.System();
        s.setName(name);
        return find(s);
    }


    public AbsoluteCoordinate getAbsoluteCoordinate(int systemId) {
        at.darkdestiny.core.model.System s = new at.darkdestiny.core.model.System();
        s.setId(systemId);
        ArrayList<at.darkdestiny.core.model.System> sList = find(s);

        if (sList.isEmpty()) {
            return null;
        } else {
            AbsoluteCoordinate ac = new AbsoluteCoordinate(sList.get(0).getX(),sList.get(0).getY());
            return ac;
        }
    }

    public int findLastId() {
        int sysId = 0;
        for(at.darkdestiny.core.model.System s : (ArrayList<at.darkdestiny.core.model.System>)findAll()){
            if(s.getId() > sysId){
                sysId = s.getId();
            }
        }
        sysId = sysId + 1;;
        return sysId;
    }
}
