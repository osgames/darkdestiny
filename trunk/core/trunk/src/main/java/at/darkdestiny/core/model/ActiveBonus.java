/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.core.enumeration.EBonusRange;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "activebonus")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ActiveBonus extends Model<ActiveBonus> {
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    @FieldMappingAnnotation("refId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer refId;
    @FieldMappingAnnotation("bonusId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer bonusId;
    @FieldMappingAnnotation("bonusRange")
    @IdFieldAnnotation
    @DefaultValue("PLANET")
    private EBonusRange bonusRange;
    @FieldMappingAnnotation("duration")
    @ColumnProperties(unsigned = true)
    private Integer duration;

    public ActiveBonus(){
        
    }

    public ActiveBonus(Integer refId, Integer bonusId, EBonusRange bonusRange, Integer duration) {
        this.refId = refId;
        this.bonusId = bonusId;
        this.bonusRange = bonusRange;
        this.duration = duration;
    }

    /**
     * @return the refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    /**
     * @return the bonusId
     */
    public Integer getBonusId() {
        return bonusId;
    }

    /**
     * @param bonusId the bonusId to set
     */
    public void setBonusId(Integer bonusId) {
        this.bonusId = bonusId;
    }

    /**
     * @return the bonusRange
     */
    public EBonusRange getBonusRange() {
        return bonusRange;
    }

    /**
     * @param bonusRange the bonusRange to set
     */
    public void setBonusRange(EBonusRange bonusRange) {
        this.bonusRange = bonusRange;
    }

    /**
     * @return the duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
