/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.AllianceBoard;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class AllianceBoardDAO extends ReadWriteTable<AllianceBoard> implements GenericDAO {

    public AllianceBoard findById(Integer id) {
        AllianceBoard ab = new AllianceBoard();
        ab.setId(id);

        return (AllianceBoard)get(ab);
    }

    public ArrayList<AllianceBoard> findByAllianceId(int allianceId) {
        AllianceBoard ab = new AllianceBoard();
        ab.setAllianceId(allianceId);
        return find(ab);
    }

}
