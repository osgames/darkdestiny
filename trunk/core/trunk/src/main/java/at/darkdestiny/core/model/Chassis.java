/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "chassis")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Chassis extends Model<Chassis> {

    public static final int ID_FIGHTER = 1;
    public static final int ID_CORVETTE = 2;
    public static final int ID_FRIGATE = 3;
    public static final int ID_DESTROYER = 4;
    public static final int ID_CRUISER = 5;
    public static final int ID_BATTLESHIP = 6;
    public static final int ID_SMALLBATTLESTATION = 7;
    public static final int ID_SUPERBATTLESHIP = 8;
    public static final int ID_BIGBATTLESTATION = 9;
    public static final int ID_TENDER = 10;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("minBuildQty")
    private Integer minBuildQty;
    @IndexAnnotation(indexName = "relModule")
    @FieldMappingAnnotation("relModuleId")
    private Integer relModuleId;
    @FieldMappingAnnotation("img_design")
    private String img_design;
    @FieldMappingAnnotation("starbase")
    private Boolean starbase;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRelModuleId() {
        return relModuleId;
    }

    public void setRelModuleId(int relModuleId) {
        this.relModuleId = relModuleId;
    }

    public String getImg_design() {
        return img_design;
    }

    public void setImg_design(String img_design) {
        this.img_design = img_design;
    }

    public Boolean isStarbase() {
        return starbase;
    }

    public void setStarbase(Boolean starbase) {
        this.starbase = starbase;
    }

    public Integer getMinBuildQty() {
        return minBuildQty;
    }

    public void setMinBuildQty(Integer minBuildQty) {
        this.minBuildQty = minBuildQty;
    }
}
