/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.admin;

import at.darkdestiny.core.dao.PlanetLogDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.enumeration.EPlanetLogType;
import at.darkdestiny.core.model.PlanetLog;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * die klass sieht bei mir aber anders aus
 * da stimmt aber was net oder? 
 * @author Stefan
 */
public class TestFramework {
    private static PlanetLogDAO plDAO;
    private static RessourceDAO rDAO;
    private static final Logger log = LoggerFactory.getLogger(TestFramework.class);
    
    public static void testFramework() {
        plDAO = DAOFactory.get(PlanetLogDAO.class);
        log.debug("CHECK PLANETLOG COUNT NOT VIRTUAL: " + plDAO.findAll().size());
        
        DAOFactory.setVirtualMode(true);
                
        plDAO = DAOFactory.get(PlanetLogDAO.class);
        rDAO = DAOFactory.get(RessourceDAO.class);
        
        log.debug("CHECK RESSOURCE COUNT VIRTUAL: " + rDAO.findAll().size());
        
        log.debug("CHECK PLANETLOG COUNT VIRTUAL: " + plDAO.findAll().size());
        
        for (int i=0;i<10;i++) {
            PlanetLog pl = new PlanetLog();
            pl.setPlanetId(1000 + i);
            pl.setTime(9999);
            pl.setType(EPlanetLogType.COLONIZED);
            pl.setUserId(7);
            plDAO.add(pl);
        }
           
        log.debug("CHECK PLANETLOG COUNT VIRTUAL AFTER SINGLE ADD: " + plDAO.findAll().size());
        
        ArrayList<PlanetLog> plBatchInsert = new ArrayList<PlanetLog>();
        for (int i=0;i<10;i++) {
            PlanetLog pl = new PlanetLog();
            pl.setPlanetId(1100 + i);
            pl.setTime(9999);
            pl.setType(EPlanetLogType.COLONIZED);
            pl.setUserId(6);
            plBatchInsert.add(pl);
        }        
        
        plDAO.insertAll(plBatchInsert);                
        
        log.debug("CHECK PLANETLOG COUNT VIRTUAL AFTER BATCH ADD: " + plDAO.findAll().size());
        
        PlanetLog plSearch1 = new PlanetLog();
        plSearch1.setUserId(6);
        ArrayList<PlanetLog> toCheck1 = plDAO.find(plSearch1);
        log.debug("ENTRIES WITH USERID 6: " + toCheck1.size());
        
        plSearch1 = new PlanetLog();
        plSearch1.setUserId(7);
        toCheck1 = plDAO.find(plSearch1);     
        log.debug("ENTRIES WITH USERID 7: " + toCheck1.size());     
        
        PlanetLog pl = plDAO.findAll().get(0);
        plDAO.remove(pl);
        log.debug("CHECK PLANETLOG COUNT VIRTUAL AFTER DELETE SINGLE: " + plDAO.findAll().size());
                
        PlanetLog plSearch = new PlanetLog();
        plSearch.setUserId(6);
        ArrayList<PlanetLog> toDelete = plDAO.find(plSearch);
        plDAO.removeAll(toDelete);
        log.debug("CHECK PLANETLOG COUNT VIRTUAL AFTER DELETE BATCH: " + plDAO.findAll().size());
        
        // Print all data
        for (PlanetLog tmp : plDAO.findAll()) {
            log.debug("PL: " + pl.getId() + "/" + pl.getPlanetId() + "/" + pl.getTime() + "/" + pl.getType() + "/" + pl.getUserId());
        }
        
        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        ArrayList<PlanetLog> toCheck = plDAO.find(plSearch);
        
        log.debug("ENTRIES WITH USERID 7: " + toCheck.size());
        
        int count = 3;
        do {
            count--;
            for (PlanetLog tmp : toCheck) {
                if (tmp.getUserId() == 7) {
                    tmp.setUserId(3);
                    break;
                }              
            }
        } while (count > 0);
        plDAO.updateAll(toCheck);
        
        log.debug("CHANGE 3 ENTRIES FROM USER 7 TO USER 3");
        
        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);        
        log.debug("ENTRIES WITH USERID 7: " + toCheck.size());        
        
        plSearch = new PlanetLog();
        plSearch.setUserId(3);
        toCheck = plDAO.find(plSearch);        
        log.debug("ENTRIES WITH USERID 3: " + toCheck.size());         
        
        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);  
        
        log.debug("CHANGE 1 ENTRY FROM USER 7 TO USER 2");
        
        PlanetLog plTest = toCheck.get(0);
        plTest.setUserId(2);
        plDAO.update(plTest);        
        
        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);        
        log.debug("ENTRIES WITH USERID 7: " + toCheck.size());        
        
        plSearch = new PlanetLog();
        plSearch.setUserId(3);
        toCheck = plDAO.find(plSearch);        
        log.debug("ENTRIES WITH USERID 3: " + toCheck.size());             
        
        plSearch = new PlanetLog();
        plSearch.setUserId(2);
        toCheck = plDAO.find(plSearch);        
        log.debug("ENTRIES WITH USERID 2: " + toCheck.size());                    
        
        plDAO.removeAll();
        
        log.debug("CHECK PLANETLOG COUNT VIRTUAL AFTER DELETE ALL: " + plDAO.findAll().size());
        
        DAOFactory.setVirtualMode(false);
    }
}
