/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.ESpyAttachedToType;
import at.darkdestiny.core.enumeration.ESpyCurrAction;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "spy")
public class Spy extends Model<Spy> {
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true, autoIncrement=true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("xp")
    private Integer xp;
    @FieldMappingAnnotation("attachedToType")
    @DefaultValue("PLANET")
    private ESpyAttachedToType attachedToType;
    @FieldMappingAnnotation("attachedToId")
    private Integer attachedToId;
    @FieldMappingAnnotation("systemId")
    private Integer systemId;
    @FieldMappingAnnotation("planetId")
    private Integer planetId;
    @FieldMappingAnnotation("currAction")
    @DefaultValue("DEFENSE")
    private ESpyCurrAction currAction;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the xp
     */
    public Integer getXp() {
        return xp;
    }

    /**
     * @param xp the xp to set
     */
    public void setXp(Integer xp) {
        this.xp = xp;
    }

    /**
     * @return the attachedToType
     */
    public ESpyAttachedToType getAttachedToType() {
        return attachedToType;
    }

    /**
     * @param attachedToType the attachedToType to set
     */
    public void setAttachedToType(ESpyAttachedToType attachedToType) {
        this.attachedToType = attachedToType;
    }

    /**
     * @return the attachedToId
     */
    public Integer getAttachedToId() {
        return attachedToId;
    }

    /**
     * @param attachedToId the attachedToId to set
     */
    public void setAttachedToId(Integer attachedToId) {
        this.attachedToId = attachedToId;
    }

    /**
     * @return the systemId
     */
    public Integer getSystemId() {
        return systemId;
    }

    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the currAction
     */
    public ESpyCurrAction getCurrAction() {
        return currAction;
    }

    /**
     * @param currAction the currAction to set
     */
    public void setCurrAction(ESpyCurrAction currAction) {
        this.currAction = currAction;
    }

}
