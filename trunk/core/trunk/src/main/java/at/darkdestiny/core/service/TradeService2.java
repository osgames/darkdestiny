/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PriceListDAO;
import at.darkdestiny.core.dao.PriceListEntryDAO;
import at.darkdestiny.core.dao.TradeOfferDAO;
import at.darkdestiny.core.dao.TradePostDAO;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.dao.TradeRouteDetailDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.ETradeOfferType;
import at.darkdestiny.core.enumeration.ETradeRouteStatus;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.enumeration.TradeOfferType;
import at.darkdestiny.core.model.*;
import at.darkdestiny.core.notification.TradeNotification;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.DiplomacyResult;
import at.darkdestiny.core.result.NamedPriceListResult;
import at.darkdestiny.core.result.TradeOfferEntry;
import at.darkdestiny.core.result.TradeOfferResult;
import at.darkdestiny.core.trade.TradeGoodPriceList;
import at.darkdestiny.core.utilities.AllianceUtilities;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import at.darkdestiny.core.utilities.TradeUtilities;
import at.darkdestiny.core.view.TradeRouteView;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class TradeService2 {

    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static TradePostDAO tpDAO = (TradePostDAO) DAOFactory.get(TradePostDAO.class);
    private static TradeOfferDAO toDAO = (TradeOfferDAO) DAOFactory.get(TradeOfferDAO.class);
    private static PriceListDAO plDAO = (PriceListDAO) DAOFactory.get(PriceListDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static PriceListEntryDAO pleDAO = (PriceListEntryDAO) DAOFactory.get(PriceListEntryDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    private static TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);

    public static void renameTradePost(int userId, int id, String newName) {
        TradePost tp = tpDAO.findById(id);
        tp.setName(newName);

        ArrayList<TradePost> tpEntries = tpDAO.find(tp);
        if (tpEntries.size() > 0) return;

        PlayerPlanet pp = ppDAO.findByPlanetId(tp.getPlanetId());
        if (pp != null && pp.getUserId().equals(userId)) {
            tpDAO.update(tp);
        }

    }

    public static void setTradePostOffer(int userId, int id, Map offers) {

        HashMap<Integer, Long> amounts = new HashMap<Integer, Long>();
        HashMap<Integer, ETradeOfferType> types = new HashMap<Integer, ETradeOfferType>();

        for (Object entry : offers.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String key = (String) me.getKey();

            if (key.startsWith("ress_")) {
                StringTokenizer st = new StringTokenizer(key, "_");
                st.nextToken();
                int ressId = Integer.parseInt(st.nextToken());
                String decidOr = st.nextToken();
                if (decidOr.equals("amount")) {
                    long qty = 0;
                    try {
                        qty = Long.parseLong((String) ((String[]) me.getValue())[0]);
                    } catch (NumberFormatException nfe) {
                    }
                    if (qty > 0) {
                        amounts.put(ressId, qty);
                    }
                } else if (decidOr.equals("type")) {
                    try {
                        types.put(ressId, ETradeOfferType.valueOf(((String[]) me.getValue())[0]));
                    } catch (Exception e) {
                    }
                }

            }
        }
        for (Map.Entry<Integer, ETradeOfferType> type : types.entrySet()) {
            int ressId = type.getKey();
            if (type.getValue().equals(ETradeOfferType.MINIMUM)) {
                TradeOffer to = toDAO.findBy(id, type.getKey());
                if (to == null) {
                    to = new TradeOffer();
                    to.setRessourceId(ressId);
                    to.setTradePostId(id);
                    to.setSold(0l);
                    to.setQuantity(0l);
                    to.setType(type.getValue());
                    toDAO.add(to);
                } else {
                    to.setQuantity(0l);
                    to.setType(type.getValue());
                    toDAO.update(to);
                }

            } else if (type.getValue().equals(ETradeOfferType.FIXED)) {
                long amount = -1;
                try {
                    amount = amounts.get(ressId);
                } catch (Exception e) {
                }
                if (amount > 0) {
                    TradeOffer to = toDAO.findBy(id, type.getKey());
                    if (to == null) {
                        to = new TradeOffer();
                        to.setRessourceId(ressId);
                        to.setTradePostId(id);
                        to.setSold(0l);
                        to.setQuantity(amount);
                        to.setType(type.getValue());
                        toDAO.add(to);
                    } else {
                        to.setQuantity(amount);
                        to.setType(type.getValue());
                        toDAO.update(to);
                    }
                } else {
                    TradeOffer to = toDAO.findBy(id, type.getKey());
                    if (to != null) {
                        toDAO.remove(to);
                    }
                }
            } else if (type.getValue().equals(ETradeOfferType.NONE)) {
                TradeOffer to = toDAO.findBy(id, type.getKey());
                if (to != null) {
                    toDAO.remove(to);
                }
            }
        }
    }

    public static TradePost getTradePost(int userId, int tradePostId) {
        TradePost tp = tpDAO.findById(tradePostId);
        PlayerPlanet pp = ppDAO.findByPlanetId(tp.getPlanetId());
        if (pp != null && pp.getUserId() == userId) {
            return tp;
        }
        DebugBuffer.addLine(DebugLevel.ERROR, "Tradepost for a nonexisting Planet Found");
        return null;
    }

    public static TradePost getTradePost(int tradePostId) {
        return tpDAO.findById(tradePostId);

    }

    public static ArrayList<TradePost> getAllOwnTradeposts(int userId, int planetId) {
        ArrayList<TradePost> result = new ArrayList<TradePost>();
        TradePost actTradePost = tpDAO.findByPlanetId(planetId);
        if (actTradePost != null) {
            result.add(actTradePost);
        }
        for (TradePost tp : (ArrayList<TradePost>) tpDAO.findAll()) {
            if (tp.getPlanetId() == planetId) {
                continue;
            }
            PlayerPlanet pp = ppDAO.findByPlanetId(tp.getPlanetId());
            if (pp == null) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Found tradepost but no playerplanet for planetId : " + tp.getPlanetId());
            }
            if (pp.getUserId() == userId) {
                result.add(tp);
            }
        }
        return result;
    }

    public static TradeOffer getOffersByTradePost(int tradePostId, int ressourceId) {
        TradePost tp = tpDAO.findById(tradePostId);
        TradeOffer to = toDAO.findBy(tradePostId, ressourceId);
        if (to != null) {
            if (to.getType().equals(ETradeOfferType.FIXED)) {
                //Do nothing quantity is fixed.
                return to;
            } else if (to.getType().equals(ETradeOfferType.MINIMUM)) {
                PlanetRessource stock = prDAO.findBy(tp.getPlanetId(), to.getRessourceId(), EPlanetRessourceType.INSTORAGE);
                PlanetRessource minimum = prDAO.findBy(tp.getPlanetId(), to.getRessourceId(), EPlanetRessourceType.MINIMUM_TRADE);
                long qty = -1;
                if (minimum != null && stock != null) {
                    qty = stock.getQty() - minimum.getQty();
                } else if (stock != null && minimum == null) {
                    qty = stock.getQty();
                }

                if (qty > 0) {
                    to.setQuantity(qty);
                } else {
                    to.setQuantity(0l);
                }
                //Do nothing quantity is fixed.
                return to;
            }
        }
        to = new TradeOffer();
        to.setQuantity(-1l);
        to.setRessourceId(ressourceId);
        to.setSold(0l);
        to.setType(ETradeOfferType.NONE);
        return to;

    }

    public static TradeOfferEntry getTradeOfferEntry(int userId, int tradePostId, int ressourceId, int targetPlanetId) {
        //<UserId, PriceList> billigste pricelist dieses users
        TradeOfferEntry toe = new TradeOfferEntry();
        HashMap<Integer, PriceListEntry> priceLists = new HashMap<Integer, PriceListEntry>();
        TradePost tp = tpDAO.findById(tradePostId);
        PlayerPlanet pp = ppDAO.findByPlanetId(tp.getPlanetId());
        int offerUserId = pp.getUserId();
        TradeOffer to = toDAO.findBy(tp.getId(), ressourceId);
        PriceListEntry ple = findPriceList(userId, offerUserId, ressourceId);
        if (ple != null && to != null) {
            if (!priceLists.containsKey(offerUserId)) {
                priceLists.put(offerUserId, ple);
            }
            if (to.getType().equals(ETradeOfferType.FIXED)) {
                long onPlanet = 0;
                PlanetRessource stock = prDAO.findBy(tp.getPlanetId(), to.getRessourceId(), EPlanetRessourceType.INSTORAGE);
                if (stock != null) {
                    onPlanet = stock.getQty();
                }
                toe.setAvailOnPlanet(onPlanet);
                toe.setAmount(to.getQuantity());
            } else if (to.getType().equals(ETradeOfferType.MINIMUM)) {
                PlanetRessource stock = prDAO.findBy(tp.getPlanetId(), to.getRessourceId(), EPlanetRessourceType.INSTORAGE);
                PlanetRessource minimum = prDAO.findBy(tp.getPlanetId(), to.getRessourceId(), EPlanetRessourceType.MINIMUM_TRADE);
                long qty = -1;
                if (minimum != null) {
                    qty = stock.getQty() - minimum.getQty();
                } else if (stock != null && minimum == null) {
                    qty = stock.getQty();
                }

                if (qty > 0) {
                    toe.setAmount(qty);
                } else {
                    toe.setAmount(0l);
                }
                long onPlanet = 0;
                if (stock != null) {
                    onPlanet = stock.getQty();
                }
                toe.setAvailOnPlanet(onPlanet);
            }
            toe.setRessourceId(ressourceId);
            toe.setTradePostId(tp.getId());

            //Calculate distance
            RelativeCoordinate rcOffer = new RelativeCoordinate(pDAO.findById(tp.getPlanetId()).getSystemId(), tp.getPlanetId());
            RelativeCoordinate rcTarget = new RelativeCoordinate(pDAO.findById(targetPlanetId).getSystemId(), targetPlanetId);
            toe.setDistance(rcOffer.distanceTo(rcTarget));
            toe.setPrice(priceLists.get(offerUserId).getPrice());


        }


        return toe;
    }

    public static ArrayList<TradeRouteView> getAllIncomingTrades(int userId, int planetId) {
        ArrayList<TradeRouteView> result = new ArrayList<TradeRouteView>();
        TreeMap<Integer, TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>>> sorted = new TreeMap<Integer, TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>>>();
        for (TradeRoute tr : trDAO.findByTargetUserId(userId, ETradeRouteType.TRADE)) {
            for (TradeRouteDetail trd : trdDAO.getByRouteId(tr.getId())) {
                int index = tr.getId();

                TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>> routes = sorted.get(index);
                if (routes == null) {
                    routes = new TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>>();
                }
                TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>> pIds = routes.get(tr.getTargetPlanet());
                if (pIds == null) {
                    pIds = new TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>();
                }
                TreeMap<Integer, TradeRouteDetail> rIds = pIds.get(trd.getReversed());
                if (rIds == null) {
                    rIds = new TreeMap<Integer, TradeRouteDetail>();
                }
                rIds.put(trd.getRessId(), trd);
                pIds.put(trd.getReversed(), rIds);
                routes.put(tr.getTargetPlanet(), pIds);
                sorted.put(index, routes);
            }

        }
        for (Map.Entry<Integer, TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>>> entry : sorted.entrySet()) {

            for (Map.Entry<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>> entry2 : entry.getValue().entrySet()) {

                for (Map.Entry<Boolean, TreeMap<Integer, TradeRouteDetail>> entry3 : entry2.getValue().entrySet()) {
                    for (Map.Entry<Integer, TradeRouteDetail> entry4 : entry3.getValue().entrySet()) {

                        TradeRouteDetail trd = entry4.getValue();
                        TradeRouteView trv = new TradeRouteView(trd, entry2.getKey());
                        result.add(trv);
                    }
                }
            }
        }
        return result;
    }

    public static ArrayList<TradeRouteView> getAllOutgoingTrades(int userId, int planetId) {
        ArrayList<TradeRouteView> result = new ArrayList<TradeRouteView>();
        TreeMap<Integer, TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>>> sorted = new TreeMap<Integer, TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>>>();
        for (TradeRoute tr : trDAO.findByUserId(userId, ETradeRouteType.TRADE)) {
            for (TradeRouteDetail trd : trdDAO.getByRouteId(tr.getId())) {
                int index = 0;
                if (tr.getStartPlanet() == planetId || tr.getTargetPlanet() == planetId) {
                    index = -1;
                } else {
                    index = tr.getId();
                }
                TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>> routes = sorted.get(index);
                if (routes == null) {
                    routes = new TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>>();
                }
                TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>> pIds = routes.get(tr.getTargetPlanet());
                if (pIds == null) {
                    pIds = new TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>();
                }
                TreeMap<Integer, TradeRouteDetail> rIds = pIds.get(trd.getReversed());
                if (rIds == null) {
                    rIds = new TreeMap<Integer, TradeRouteDetail>();
                }
                rIds.put(trd.getRessId(), trd);
                pIds.put(trd.getReversed(), rIds);
                routes.put(tr.getTargetPlanet(), pIds);
                sorted.put(index, routes);
            }

        }
        for (Map.Entry<Integer, TreeMap<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>>> entry : sorted.entrySet()) {

            for (Map.Entry<Integer, TreeMap<Boolean, TreeMap<Integer, TradeRouteDetail>>> entry2 : entry.getValue().entrySet()) {

                for (Map.Entry<Boolean, TreeMap<Integer, TradeRouteDetail>> entry3 : entry2.getValue().entrySet()) {
                    for (Map.Entry<Integer, TradeRouteDetail> entry4 : entry3.getValue().entrySet()) {

                        TradeRouteDetail trd = entry4.getValue();
                        TradeRouteView trv = new TradeRouteView(trd, entry2.getKey());
                        result.add(trv);
                    }
                }
            }
        }
        return result;
    }

    public static TradeOfferResult getTradeOffers(int userId, int targetPlanetId) {
        try {
            ArrayList<TradeOfferEntry> result = new ArrayList<TradeOfferEntry>();
            ArrayList<BaseResult> brs = new ArrayList<BaseResult>();
            for (Ressource r : RessourceService.getAllStorableRessources()) {
                TradeOfferResult tor = getTradeOffers(userId, r.getId(), targetPlanetId);
                result.addAll(tor.getOffers());
                brs.addAll(tor.getResults());
            }

            return new TradeOfferResult(new ArrayList<BaseResult>(), result);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getTradeOffers: ", e);
        }

        return null;
    }

    // PARAMS: TARGETPLACE & SORTING: PRICE, DELIVERY TIME, SPECIAL OFFERS
    public static TradeOfferResult getTradeOffers(int userId, int ressourceId, int targetPlanetId) {
        try {
            ArrayList<TradeOfferEntry> result = new ArrayList<TradeOfferEntry>();
            TreeMap<Integer, TradeOfferEntry> sortedResult = new TreeMap<Integer, TradeOfferEntry>();
            //<UserId, PriceList> billigste pricelist dieses users
            HashMap<Integer, PriceListEntry> priceLists = new HashMap<Integer, PriceListEntry>();
            //DiplomacyResultbuffer
            HashMap<Integer, DiplomacyResult> diplomacy = new HashMap<Integer, DiplomacyResult>();
            for (TradePost tp : (ArrayList<TradePost>) tpDAO.findAll()) {
                PlanetConstruction pc = Service.planetConstructionDAO.findBy(tp.getPlanetId(), Construction.ID_INTERSTELLARTRADINGPOST);

                if (pc == null) {
                    DebugBuffer.warning("Found tradepost for planet " + tp.getPlanetId() + " but no building available!");
                    continue;
                }

                PlayerPlanet pp = ppDAO.findByPlanetId(tp.getPlanetId());
                int offerUserId = pp.getUserId();
                //you cant buy from yourself
                /*if (offerUserId == userId) {
                    continue;
                }*/
                TradeOffer to = toDAO.findBy(tp.getId(), ressourceId);
                PriceListEntry ple = findPriceList(userId, offerUserId, ressourceId);

                if (ple != null && to != null) {
                    if (!priceLists.containsKey(offerUserId)) {
                        priceLists.put(offerUserId, ple);
                    }
                    TradeOfferEntry toe = new TradeOfferEntry();
                    if (to.getType().equals(ETradeOfferType.FIXED)) {
                        if (to.getQuantity() <= 0) {
                            continue;
                        }
                        long onPlanet = 0;
                        PlanetRessource stock = prDAO.findBy(tp.getPlanetId(), to.getRessourceId(), EPlanetRessourceType.INSTORAGE);
                        if (stock != null) {
                            onPlanet = stock.getQty();
                        }
                        toe.setAvailOnPlanet(onPlanet);
                        toe.setAmount(to.getQuantity());
                    } else if (to.getType().equals(ETradeOfferType.MINIMUM)) {
                        PlanetRessource stock = prDAO.findBy(tp.getPlanetId(), to.getRessourceId(), EPlanetRessourceType.INSTORAGE);
                        PlanetRessource minimum = prDAO.findBy(tp.getPlanetId(), to.getRessourceId(), EPlanetRessourceType.MINIMUM_TRADE);
                        long qty = -1;
                        long onPlanet = 0;

                        if (stock != null) {
                            onPlanet = stock.getQty();
                        }

                        if (minimum != null) {
                            qty = onPlanet - minimum.getQty();
                        } else if (stock != null && minimum == null) {
                            qty = onPlanet;
                        }

                        if (qty > 0) {
                            toe.setAmount(qty);
                        } else {
                            continue;
                        }

                        toe.setAvailOnPlanet(onPlanet);
                    }
                    toe.setTradePostLevel(pc.getLevel());
                    toe.setRessourceId(ressourceId);
                    toe.setTradePostId(tp.getId());
                    toe.setOwningUser(offerUserId);
                    DiplomacyResult dr = diplomacy.get(offerUserId);
                    if (dr == null) {
                        toe.setDiplomacyResult(DiplomacyUtilities.getDiplomacyResult(userId, offerUserId));
                    } else {
                        toe.setDiplomacyResult(dr);
                    }
                    //Calculate distance
                    RelativeCoordinate rcOffer = new RelativeCoordinate(pDAO.findById(tp.getPlanetId()).getSystemId(), tp.getPlanetId());
                    RelativeCoordinate rcTarget = new RelativeCoordinate(pDAO.findById(targetPlanetId).getSystemId(), targetPlanetId);
                    toe.setDistance(rcOffer.distanceTo(rcTarget));
                    toe.setPrice(priceLists.get(offerUserId).getPrice());
                    sortedResult.put(to.getId(), toe);
                }

            }
            result.addAll(sortedResult.values());

            return new TradeOfferResult(new ArrayList<BaseResult>(), result);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getTradeOffers: ", e);
        }

        return null;
    }

    private static PriceListEntry findPriceList(int userId, int offerUserId, int ressourceId) {
        PriceListEntry priceListEntry = null;
        //Search for user-specific
        if (priceListEntry == null) {
            PriceList mostSpecific = plDAO.findBy(offerUserId, userId, TradeOfferType.USER);
            if (mostSpecific != null) {
                PriceListEntry ple = pleDAO.findByPriceListAndRess(mostSpecific.getId(), ressourceId);
                if (ple.getPrice() > 0) {
                    priceListEntry = ple;
                }
            }
        }
        //Search for allied-specific
        if (priceListEntry == null) {
            if (DiplomacyUtilities.hasAllianceRelation(offerUserId, userId)) {
                PriceList mostSpecific = plDAO.findByUserIdAndType(offerUserId, TradeOfferType.ALLIED);
                if (mostSpecific != null) {
                    PriceListEntry ple = pleDAO.findByPriceListAndRess(mostSpecific.getId(), ressourceId);
                    if (ple.getPrice() > 0) {
                        priceListEntry = ple;
                    }
                }
            }
        }
        //Search for alliance-specific
        if (priceListEntry == null) {
            if (AllianceUtilities.hasAlliance(userId)) {
                int allianceId = amDAO.findByUserId(userId).getAllianceId();
                PriceList mostSpecific = plDAO.findBy(offerUserId, allianceId, TradeOfferType.ALLY);
                if (mostSpecific != null) {
                    PriceListEntry ple = pleDAO.findByPriceListAndRess(mostSpecific.getId(), ressourceId);
                    if (ple.getPrice() > 0) {
                        priceListEntry = ple;
                    }
                }
            }
        }
        //Search for all
        if (priceListEntry == null) {
            PriceList mostSpecific = plDAO.findByUserIdAndType(offerUserId, TradeOfferType.PUBLIC);
            if (mostSpecific != null) {
                PriceListEntry ple = pleDAO.findByPriceListAndRess(mostSpecific.getId(), ressourceId);
                if (ple == null) {
                    return null;
                }

                if (ple.getPrice() > 0) {
                    priceListEntry = ple;
                }
            }

        }

        return priceListEntry;
    }

    public static NamedPriceListResult getNamedPriceLists(int userId) {
        if (uDAO.findById(userId) == null) {
            return new NamedPriceListResult("Invalid user", true);
        }

        ArrayList<TradeGoodPriceList> tgplList = TradeUtilities.getPriceLists(userId);

        TreeMap<Integer, TradeGoodPriceList> sortedList = new TreeMap<Integer, TradeGoodPriceList>();
        for (TradeGoodPriceList tgpl : tgplList) {
            if (tgpl.getTot() == TradeOfferType.PUBLIC) {
                sortedList.put(0, tgpl);
            } else if (tgpl.getTot() == TradeOfferType.ALLIED) {
                sortedList.put(5, tgpl);
            } else if (tgpl.getTot() == TradeOfferType.ALLY) {
                sortedList.put(10000 + tgpl.getRefId(), tgpl);
            } else if (tgpl.getTot() == TradeOfferType.USER) {
                sortedList.put(50000 + tgpl.getRefId(), tgpl);
            }
        }

        // If there is no public list create it
        if (!sortedList.containsKey(0)) {
            PriceList plNew = TradeUtilities.createTradePriceList(userId, TradeOfferType.PUBLIC, 0);
            TradeGoodPriceList tgpl = new TradeGoodPriceList(userId, TradeOfferType.PUBLIC, 0);
            tgpl.setId(plNew.getId());
            sortedList.put(0, tgpl);
        }

        NamedPriceListResult nplr = new NamedPriceListResult();
        for (Map.Entry<Integer, TradeGoodPriceList> listEntry : sortedList.entrySet()) {
            String name = null;

            if (listEntry.getValue().getTot() == TradeOfferType.PUBLIC) {
                nplr.addNewEntry(listEntry.getValue().getId(), ML.getMLStr("trade_opt_all", userId));
            } else if (listEntry.getValue().getTot() == TradeOfferType.ALLIED) {
                nplr.addNewEntry(listEntry.getValue().getId(), ML.getMLStr("trade_opt_allied", userId));
            } else if (listEntry.getKey() >= 50000) {
                User u = uDAO.findById(listEntry.getValue().getRefId());
                if (u != null) {
                    nplr.addNewEntry(listEntry.getValue().getId(), ML.getMLStr("trade_opt_player", userId) + " " + u.getGameName());
                } else {
                    DebugBuffer.trace("Deleting PriceList " + listEntry.getValue().getId() + " due to non existant user");
                    deletePriceList(listEntry.getValue().getId(), userId);
                }
            } else if (listEntry.getKey() >= 10000) {
                Alliance a = aDAO.findById(listEntry.getValue().getRefId());
                if (a != null) {
                    nplr.addNewEntry(listEntry.getValue().getId(), ML.getMLStr("trade_opt_alliance", userId) + " " + a.getTag());
                } else {
                    DebugBuffer.trace("Deleting PriceList " + listEntry.getValue().getId() + " due to non existant alliance");
                    deletePriceList(listEntry.getValue().getId(), userId);
                }
            }
        }

        return nplr;
    }

    public static int getDefaultPriceListId(int userId) {
        ArrayList<TradeGoodPriceList> tgplList = TradeUtilities.getPriceLists(userId);

        for (TradeGoodPriceList tgpl : tgplList) {
            if (tgpl.getTot() == TradeOfferType.PUBLIC) {
                return tgpl.getId();
            }
        }

        // None public => Create
        PriceList plNew = TradeUtilities.createTradePriceList(userId, TradeOfferType.PUBLIC, 0);
        return plNew.getId();
    }

    public static BaseResult createTradePriceList(int userId, TradeOfferType tot, int refId) {
        if (tot == null) {
            return new BaseResult("No offer type specified", true);
        }
        if (uDAO.findById(userId) == null) {
            return new BaseResult("Invalid userId (" + userId + ")", true);
        }
        if (tot == TradeOfferType.ALLY || tot == TradeOfferType.USER) {
            if (tot == TradeOfferType.ALLY) {
                if (aDAO.findById(refId) == null) {
                    return new BaseResult("Invalid reference alliance id (" + userId + ")", true);
                }
            } else {
                if ((refId == userId) || (uDAO.findById(refId) == null)) {
                    return new BaseResult("Invalid reference user id (" + userId + ")", true);
                }
            }
        }

        if (TradeUtilities.createTradePriceList(userId, tot, refId) == null) {
            return new BaseResult("Internal creation error", true);
        }

        return new BaseResult("", false);
    }

    public static TradeGoodPriceList getTradePrice(int userId, int id) {

        TradeGoodPriceList tgpl = TradeUtilities.getTradePriceList(id);
        if (tgpl.getUserId() != userId) {
            return null;
        }

        return tgpl;
    }

    // PARAMS: TARGETGROUP: ALL, ALLIED, ALLIANCE, PLAYER & ID
    public static TradeGoodPriceList getTradePrice(int userId, TradeOfferType tot, int refId) {

        if (tot == null) {
            return null;
        }
        if (uDAO.findById(userId) == null) {
            return null;
        }

        TradeGoodPriceList tgpl = TradeUtilities.getTradePriceList(userId, tot, refId);

        return tgpl;
    }

    public static void deletePriceList(int listId, int userId) {
        PriceList pl = plDAO.getById(listId);
        if ((pl == null) || (pl.getUserId() != userId)) {
            return;
        }

        TradeUtilities.deletePriceList(listId);
    }

    // PARAMS: TARGETGROUP: ALL, ALLIED, ALLIANCE, PLAYER & ID & PRICEPARAMS
    public static BaseResult setTradePrice(int listId, int userId, Map prices) {
        HashMap<Integer, Integer> valueMap = new HashMap<Integer, Integer>();
        BaseResult br = new BaseResult("OK",false);

        for (Object entry : prices.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String key = (String) me.getKey();

            if (key.startsWith("ress_")) {
                String valueStr = ((String[]) me.getValue())[0];
                int ressId = Integer.parseInt(key.replace("ress_", ""));
                int value = 0;
                try {
                    value = Integer.parseInt(valueStr);
                } catch (NumberFormatException nfe) {
                }

                if (value < 0) {
                    value = 0;
                }
                
                int maxValue = 1000000;
                
                switch(ressId) {
                    case(Ressource.IRON):
                        maxValue = TradeOffer.MAX_IRON_PRICE;
                        break;
                    case(Ressource.STEEL):
                        maxValue = TradeOffer.MAX_STEEL_PRICE;                        
                        break;
                    case(Ressource.TERKONIT):
                        maxValue = TradeOffer.MAX_TERKONIT_PRICE;                        
                        break;
                    case(Ressource.YNKELONIUM):
                        maxValue = TradeOffer.MAX_YNKELONIUM_PRICE;                        
                        break;
                    case(Ressource.HOWALGONIUM):
                        maxValue = TradeOffer.MAX_HOWALGONIUM_PRICE;                        
                        break;
                    case(Ressource.CVEMBINIUM):
                        maxValue = TradeOffer.MAX_CVEMBINIUM_PRICE;                        
                        break;
                    case(Ressource.CONSUMABLES):
                        maxValue = TradeOffer.MAX_CONSUMABLES_PRICE;                        
                        break;                        
                    case(Ressource.FOOD):
                        maxValue = TradeOffer.MAX_FOOD_PRICE;                        
                        break;                          
                }
                
                if (value > maxValue) {                   
                    br = new BaseResult(ML.getMLStr("err_trade_pricetoohigh",userId,value,maxValue), true);
                    value = maxValue;
                }

                valueMap.put(ressId, value);
            }
        }

        TradeUtilities.setPrices(listId, valueMap);

        return br;
    }

    // PARAMS: TRADEOFFERID & QUANTITY
    public static ArrayList<BaseResult> buyGoods(int userId, Map params) {
        ArrayList<BaseResult> results = new ArrayList<BaseResult>();
        int targetPlanetId = -1;
        int tradePostId = -1;
        int ressourceId = -1;
        long amount = -1;

        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String key = (String) me.getKey();
            if (key.equals("targetPlanetId")) {
                targetPlanetId = Integer.parseInt((String) ((String[]) me.getValue())[0]);
                if (ppDAO.findByPlanetId(targetPlanetId) != null && !ppDAO.findByPlanetId(targetPlanetId).getUserId().equals(userId)) {
                    results.add(new BaseResult("TargetPlanet does not belong to you", true));
                    return results;
                }
            }
            if (key.equals("tradePostId")) {
                tradePostId = Integer.parseInt((String) ((String[]) me.getValue())[0]);
            }
            if (key.equals("ressourceId")) {
                ressourceId = Integer.parseInt((String) ((String[]) me.getValue())[0]);
            }
            if (key.equals("amount")) {
                amount = Long.parseLong((String) ((String[]) me.getValue())[0]);
                if (amount < 0) {
                    results.add(new BaseResult("Amount may not be negative", true));
                    return results;
                }
            }
        }
        if (targetPlanetId > 0 && tradePostId > 0 && ressourceId > 0 && amount > 0) {
            TradeOfferEntry toe = getTradeOfferEntry(userId, tradePostId, ressourceId, targetPlanetId);

            //find tradepost
            TradePost tp = tpDAO.findById(tradePostId);

            if(toe.getOwningUser() == userId){
                results.add(new BaseResult("You can't buy from yourself", true));
                return results;
            }

            long availAmount = toe.getAmount();
            if (availAmount < amount) {
                results.add(new BaseResult(ML.getMLStr("err_notenoughresources", userId), true));
                return results;
            }
            double price = toe.getPrice();
            double priceTotal = amount * price;
            UserData ud = Service.userDataDAO.findByUserId(userId);
            long credits = ud.getCredits();
            if (credits < priceTotal) {
                results.add(new BaseResult(ML.getMLStr("err_notenoughgalax", userId), true));
                return results;
            }
            if (tp == null) {
                results.add(new BaseResult("TradePost with id : " + tradePostId + " , not found", true));
                return results;
            }

            PlayerPlanet sourcePlanet = ppDAO.findByPlanetId(tp.getPlanetId());
            if (sourcePlanet == null) {
                results.add(new BaseResult("Source Planet not inhabited", true));
            }
            UserData ud2 = Service.userDataDAO.findByUserId(sourcePlanet.getUserId());

            // Transfer
            ud.setCredits(ud.getCredits() - (long) priceTotal);
            ud2.setCredits(ud2.getCredits() + (long) priceTotal);
            Service.userDataDAO.update(ud);
            Service.userDataDAO.update(ud2);

            TradeOffer to = toDAO.findBy(tradePostId, ressourceId);

            //If the amount set in tradeoffer - amount to be bought is lesser then 0
            if (toe.getAmount() - amount <= 0) {
                //If it is a fixed offer => Remove
                if (to.getType().equals(ETradeOfferType.FIXED)) {
                    toDAO.remove(to);
                    //Else just update the amount sold
                } else {
                    to.setSold(to.getSold() + amount);
                    toDAO.update(to);
                }
                //If not all is sold
            } else {
                //If fixed update the amount in the offer to be sold
                if (to.getType().equals(ETradeOfferType.FIXED)) {
                    to.setQuantity(to.getQuantity() - amount);
                }
                //Update the amount sold
                to.setSold(to.getSold() + amount);
                toDAO.update(to);
            }

            PriceListEntry ple = findPriceList(userId, sourcePlanet.getUserId(), ressourceId);
            if (ple != null) {
                ple.setSold(ple.getSold() + amount);
                pleDAO.update(ple);
            } else {
                DebugBuffer.error("PLE Not found : " + userId + " - " + sourcePlanet.getUserId() + " - " + ressourceId);
            }

            boolean revRouteFound = false;

            TradeRoute tr = trDAO.findBy(tp.getPlanetId(), targetPlanetId, ETradeRouteType.TRADE);
            if (tr == null) {
                tr = trDAO.findBy(targetPlanetId, tp.getPlanetId(), ETradeRouteType.TRADE);
                if (tr != null) {
                    revRouteFound = true;
                }
            }
            if (tr == null) {

                tr = new TradeRoute();

                int distance = 0;
                RelativeCoordinate rc1 = new RelativeCoordinate(0, tp.getPlanetId());
                RelativeCoordinate rc2 = new RelativeCoordinate(0, targetPlanetId);
                distance = (int) Math.ceil(rc1.distanceTo(rc2));

                tr.setDistance((double) distance);
                tr.setStartPlanet(tp.getPlanetId());
                tr.setTargetPlanet(targetPlanetId);
                tr.setTargetUserId(userId);
                tr.setTradePostId(tradePostId);
                tr.setStatus(ETradeRouteStatus.ACTIVE);
                tr.setType(ETradeRouteType.TRADE);
                PlayerPlanet pp = ppDAO.findByPlanetId(tp.getPlanetId());
                if (pp == null) {

                    results.add(new BaseResult("SourcePlanet does not belong to anyone", true));
                    return results;
                }
                tr.setUserId(pp.getUserId());
                tr = trDAO.add(tr);
            }
            TradeRouteDetail trd = trdDAO.findBy(tr.getId(), ressourceId, revRouteFound);
            if (trd == null) {
                trd = trdDAO.findBy(tr.getId(), ressourceId, !revRouteFound);
            }
            if (trd == null) {
                trd = new TradeRouteDetail();
                trd.setDisabled(false);
                trd.setMaxQty((int) amount);
                trd.setLastTransport(0);
                trd.setQtyDelivered(0l);
                trd.setReversed(revRouteFound);
                trd.setRouteId(tr.getId());
                trd.setRessId(ressourceId);
                trdDAO.add(trd);

            } else {
                trd.setMaxQty(trd.getMaxQty() + (int) amount);
                trdDAO.update(trd);
            }
            PlanetRessource pr = prDAO.findBy(tp.getPlanetId(), ressourceId, EPlanetRessourceType.INSTORAGE);
            if (pr != null) {
                pr.setQty(pr.getQty() - amount);
                prDAO.update(pr);
            } else {
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Transport.... wanted to decrement from a non existing stock| planetId:" + tradePostId + " ress : " + ressourceId);
            }

            //Try write Tradelog
            try{

                TradeLog tl = new TradeLog();
                tl.setFromUserId(ud2.getUserId());
                tl.setToUserId(userId);
                tl.setFromPlanetId(sourcePlanet.getId());
                tl.setToPlanetId(targetPlanetId);
                tl.setQuantity(amount);
                tl.setPrice((long)ple.getPrice());
                tl.setTotal(amount * ple.getPrice());
                tl.setRessourceId(ressourceId);
                tl.setDate(java.lang.System.currentTimeMillis());

                Service.tradeLogDAO.add(tl);


            }catch(Exception e){
                DebugBuffer.addLine(DebugLevel.ERROR, "Error generating tradelog entry" + e);
            }


            try {
                TradeNotification tn = new TradeNotification(ppDAO.findByPlanetId(tp.getPlanetId()).getUserId(), ressourceId, tp.getPlanetId(), amount, toe.getPrice());
            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Error generating tradenotification" + e);
            }

        } else {
            results.add(new BaseResult("Invalid parameters", true));
        }
        if (results.isEmpty()) {
            results.add(new BaseResult("done", false));
        }
        return results;
    }
}
