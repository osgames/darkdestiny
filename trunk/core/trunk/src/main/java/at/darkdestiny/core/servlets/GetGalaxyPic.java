/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.setup.GalaxyRenderer;
import at.darkdestiny.core.utilities.img.BackgroundPainter;
import at.darkdestiny.core.utilities.img.BaseRenderer;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.util.DebugBuffer;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class GetGalaxyPic extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("image/png");
        ServletOutputStream out = response.getOutputStream();

        try {
            List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();

            // Check for DATA_FOR_SCAN_PIC
            int galaxyId = 0;
            int mode = 1;

            if (request.getParameter("galaxyId") != null) {
                galaxyId = Integer.parseInt(request.getParameter("galaxyId"));
            }

            Galaxy g = Service.galaxyDAO.findById(galaxyId);
            int picWidth = 1000;
            int picHeight = 1000;

            allItems.add(new GalaxyRenderer(galaxyId, this.getServletContext().getRealPath(GameConfig.getInstance().picPath() + "/pic/") + "/"));

            BaseRenderer renderer = new BaseRenderer(picWidth, picHeight);
// Schreibt den Mime-Type in den Log, Nur Benutzen, wenn
// genau das gew&uuml;nscht wird
// renderer.getImageMimeType();

            renderer.modifyImage(new BackgroundPainter(0x000000));

            for (IModifyImageFunction mif : allItems) {
                renderer.modifyImage(mif);
            }

            renderer.sendToUser(out);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while generating galaxy pic: ", e);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
