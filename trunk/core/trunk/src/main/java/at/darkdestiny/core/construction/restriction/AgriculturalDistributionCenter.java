/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.construction.restriction;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.dao.ActiveBonusDAO;
import at.darkdestiny.core.enumeration.ERestrictionReason;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.ConstructionCheckResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class AgriculturalDistributionCenter extends Service implements ConstructionInterface {
    private static ActiveBonusDAO abDAO = (ActiveBonusDAO) DAOFactory.get(ActiveBonusDAO.class);
    
    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();
    
    @Override
    public ConstructionCheckResult onConstructionCheck(PlayerPlanet pp) {
        // if (pp.getColonyType() != EColonyType.AGRICULTURAL_COLONY) {
        if (abDAO.findByPlanetAndBonusId(pp.getPlanetId(), 7) == null) {
            errMsg.put(ML.getMLStr("msg_need_agricultural_colony", pp.getUserId()),ERestrictionReason.UNSPECIFIED);
            return new ConstructionCheckResult(false,0);
        }

        return new ConstructionCheckResult(true,1);
    }

    @Override
    public void onConstruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public void onFinishing(PlayerPlanet pp) {

    }

    @Override
    public void onDeconstruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public void onDestruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }


    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    @Override
    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }    
}
