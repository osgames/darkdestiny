/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.Threading;

import at.darkdestiny.core.ai.AIThread;
import at.darkdestiny.core.ai.AIThreadHandler;
import at.darkdestiny.core.interfaces.IDDThread;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ThreadController {
    private static ArrayList<IDDThread> activeWorkerThreads = new ArrayList<IDDThread>();
    
    public static ViewtableThread getViewTableThread() {
        ViewtableThread vtt = null;
        
        for (IDDThread thread : activeWorkerThreads) {
            if (thread instanceof ViewtableThread) {
                vtt = (ViewtableThread)thread;
            }
        }
        
        if (vtt == null) {
            vtt = new ViewtableThread();
            vtt.setName("Viewtable Updater");
            vtt.start();
            activeWorkerThreads.add(vtt);            
        }
        
        return vtt;
    }

    public static void stopAllThreads() throws Exception {
        for (IDDThread thread : activeWorkerThreads) {
            thread.stopMe();
        }
        
        boolean threadsRunning = false;
        
        do {
            threadsRunning = false;        
            Thread.sleep(20);
            
            for (IDDThread thread : activeWorkerThreads) {
                if (((Thread)thread).isAlive()) threadsRunning = true;
            }                  
        } while (threadsRunning);
        
        activeWorkerThreads.clear();        
    }
}
