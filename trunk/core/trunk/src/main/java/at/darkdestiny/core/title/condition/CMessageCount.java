package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.MessageDAO;
import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.framework.dao.DAOFactory;


public class CMessageCount extends AbstractCondition {

	private ParameterEntry messageCountValue;
	private ParameterEntry comparator;
    private static MessageDAO mDAO = (MessageDAO) DAOFactory.get(MessageDAO.class);

	public CMessageCount(ParameterEntry messageCountValue, ParameterEntry comparator){
		this.messageCountValue = messageCountValue;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
            int msgCount = mDAO.findBySourceUserId(userId).size();

        return compare(messageCountValue.getParamValue().getValue(), msgCount, messageCountValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

	}
}
