/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Intelligence;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class IntelligenceDAO extends ReadWriteTable<Intelligence> implements GenericDAO {
    public Intelligence findById(Integer id) {
        Intelligence i = new Intelligence();
        i.setId(id);
        return (Intelligence)get(i);
    }
}
