/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Filter;
import at.darkdestiny.core.model.FilterToValue;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class FilterDAO extends ReadWriteTable<Filter> implements GenericDAO {
   public static FilterToValueDAO filterToValueDAO = (FilterToValueDAO) DAOFactory.get(FilterToValueDAO.class);

    public Filter findById(int filterId){
        Filter f = new Filter();
        f.setId(filterId);
        return (Filter)get(f);
    }
    public void removeUserFilters(){
        for(Filter f : (ArrayList<Filter>)findAll()){
            if(f.getUserId() != 0){
                for(FilterToValue ftv : filterToValueDAO.findByFilterId(f.getId())){
                    filterToValueDAO.remove(ftv);
                }
                remove(f);
            }
        }
    }

    public ArrayList<Filter> findByUserId(int userId) {
        Filter f = new Filter();
        f.setUserId(userId);
        return find(f);
    }



}
