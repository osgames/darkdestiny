/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.viewbuffer;

import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class HomeSystemBuffer extends Service{

    public static HashMap<Integer,  at.darkdestiny.core.model.System> homeSystem;

    public static void init(){
        homeSystem = new HashMap<Integer, at.darkdestiny.core.model.System>();

        for(PlayerPlanet pp :  (ArrayList<PlayerPlanet>)playerPlanetDAO.findAll()){
            if(pp.isHomeSystem()){
                Planet p = planetDAO.findById(pp.getPlanetId());
                homeSystem.put(pp.getUserId(), systemDAO.findById(p.getSystemId()));
            }
        }
    }

    public static at.darkdestiny.core.model.System getForUser(int userId){
        
        if(homeSystem == null){
            init();
        }
        return homeSystem.get(userId);
    }

    public static void setForUser(int userId, at.darkdestiny.core.model.System system){
            homeSystem.put(userId, system);
    }
}
