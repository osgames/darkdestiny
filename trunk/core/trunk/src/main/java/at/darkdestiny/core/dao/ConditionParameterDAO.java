/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ConditionParameter;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class ConditionParameterDAO extends ReadWriteTable<ConditionParameter> implements GenericDAO {

    public ArrayList<ConditionParameter> findByConditionId(int conditionId) {
        ConditionParameter cp = new ConditionParameter();
        cp.setConditionId(conditionId);
        return find(cp);
    }
    public ArrayList<ConditionParameter> findByConditionIdSortedByOrder(int conditionId) {

        ConditionParameter cpSearch = new ConditionParameter();
        cpSearch.setConditionId(conditionId);
        ArrayList<ConditionParameter> result = find(cpSearch);
        TreeMap<Integer, ConditionParameter> sorted = new TreeMap<Integer, ConditionParameter>();
        for(ConditionParameter cp : result){
            sorted.put(cp.getOrder(), cp);
        }
        result.clear();
        result.addAll(sorted.values());
        return result;
    }

    public ConditionParameter findById(Integer conditionParameterId) {
        ConditionParameter cp = new ConditionParameter();
        cp.setId(conditionParameterId);
        return (ConditionParameter)get(cp);
    }

}
