/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.development.conditions;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface IBonusCondition {    
    // checkCondition (planetId)
    public boolean isPossible(int planetId);
    // checkCondition (return false if there are no planet independent conditions)
    public boolean isPossible();
    // get current active boni
    public ArrayList<String> activeBoni(int bonusId, int planetId, int userId);
    // tasks on activation
    public void onActivation(int bonusId, int planetId, int userId);
}
