package at.darkdestiny.core.update;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.admin.SessionTracker;
import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.construction.ConstructionScrapCost;
import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.ConstructionRestrictionDAO;
import at.darkdestiny.core.dao.NotificationToUserDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.dao.ProductionOrderDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.ConstructionRestriction;
import at.darkdestiny.core.model.NotificationToUser;
import at.darkdestiny.core.model.NotificationType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.notification.NotificationBuffer;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.ships.ShipUpgradeCost;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.*;

public class ProdOrderBuffer {
    /*     private static int noOfPriorities = 3;

     private static final int LOCAL_ADMINISTRATION = 3;
     private static final int PLANETARY_ADMINISTRATION = 4;
     private static final int PLANETARY_GOVERNMENT = 5; */

    private HashMap<Integer, ProdOrderBufferEntry> globalProductionBuffer = new HashMap<Integer, ProdOrderBufferEntry>();
    private final UpdaterDataSet uds;
    private HashMap<Integer, HashMap<Integer, ArrayList<ProdOrderBufferEntry>>> prioListPP;
    private ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);
    private UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private ConstructionRestrictionDAO crDAO = (ConstructionRestrictionDAO) DAOFactory.get(ConstructionRestrictionDAO.class);
    private NotificationToUserDAO ntuDAO = (NotificationToUserDAO) DAOFactory.get(NotificationToUserDAO.class);
    public static int PRIORITY_COUNT = 11;

    /**
     * Creates a new instance of ProdOrderBuffer
     */
    public ProdOrderBuffer(UpdaterDataSet uds) {
        this.uds = uds;
        initGlobalProductionBuffer();
        buildProductionStructure();
    }

    private void initGlobalProductionBuffer() {
        HashMap<Integer, ProductionOrder> poMap = new HashMap<Integer, ProductionOrder>();
        ArrayList<ProductionOrder> poList = poDAO.findAll();
        for (ProductionOrder po : poList) {
            poMap.put(po.getId(), po);
        }

        HashSet<Action> actions = new HashSet<Action>();
        actions.addAll(uds.getActionEntries(EActionType.BUILDING));
        actions.addAll(uds.getActionEntries(EActionType.DECONSTRUCT));
        actions.addAll(uds.getActionEntries(EActionType.GROUNDTROOPS));
        actions.addAll(uds.getActionEntries(EActionType.SHIP));
        actions.addAll(uds.getActionEntries(EActionType.DEFENSE));
        actions.addAll(uds.getActionEntries(EActionType.SPACESTATION));

        for (Action a : actions) {
            try {
                ProdOrderBufferEntry pobe = new ProdOrderBufferEntry(poMap.get(a.getRefTableId()), a);
                globalProductionBuffer.put(pobe.getId(), pobe);
            } catch (Exception e) {
                DebugBuffer.error("Invalid order: ", e);
            }
        }
    }

    private void buildProductionStructure() {
        prioListPP = new HashMap<Integer, HashMap<Integer, ArrayList<ProdOrderBufferEntry>>>();

        // Build a proritybased HashMap to process Orders properly
        for (ProdOrderBufferEntry pobe : globalProductionBuffer.values()) {

            if (!prioListPP.containsKey(Integer.valueOf(pobe.getPlanetId()))) {
                // This planet wasn't added yet
                HashMap<Integer, ArrayList<ProdOrderBufferEntry>> detail = new HashMap<Integer, ArrayList<ProdOrderBufferEntry>>();
                ArrayList<ProdOrderBufferEntry> priorityArray = new ArrayList<ProdOrderBufferEntry>();
                priorityArray.add(pobe);

                detail.put(pobe.getPriority(), priorityArray);
                prioListPP.put(pobe.getPlanetId(), detail);
            } else {
                // This planet has already some entries
                // Retrieve Detail List and check if current Priority is already maintained
                HashMap<Integer, ArrayList<ProdOrderBufferEntry>> detail = prioListPP.get(pobe.getPlanetId());

                if (!detail.containsKey(pobe.getPriority())) {
                    // There is no entry for this priority
                    ArrayList<ProdOrderBufferEntry> priorityArray = new ArrayList<ProdOrderBufferEntry>();
                    priorityArray.add(pobe);

                    detail.put(pobe.getPriority(), priorityArray);
                } else {
                    // There is an existing entry for this priority
                    ArrayList<ProdOrderBufferEntry> priorityArray = detail.get(pobe.getPriority());
                    priorityArray.add(pobe);
                    detail.put(pobe.getPriority(), priorityArray);
                }
            }
        }
    }

    public synchronized void calcTickProduction(int actPlanet, PlanetCalculation pc) {

        /**
         * *
         * //Notification - START
         */
        boolean loggedChecked = false;
        boolean logged = false;        
        EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>> finishedOrders = new EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>(EProductionOrderType.class);

        //Notification - END

        // DebugBuffer.addLine("Process planet " + actPlanet);

        // Copy priority list and remove finished production Orders from original List
        HashMap<Integer, HashMap<Integer, ArrayList<ProdOrderBufferEntry>>> inProd = new HashMap<Integer, HashMap<Integer, ArrayList<ProdOrderBufferEntry>>>();

        if (prioListPP.get(actPlanet) != null) {
            inProd.put(actPlanet, prioListPP.get(actPlanet));
        }

        if (inProd.isEmpty()) {
            // DebugBuffer.addLine("Planet " + actPlanet + " has no entries");
            return;
        }
        // inProd.putAll(prioListPP);

        Set<Map.Entry<Integer, HashMap<Integer, ArrayList<ProdOrderBufferEntry>>>> planetSet = inProd.entrySet();

        //
        // Hier einbauen das nur f&uuml;r einen Planet der Eintrag genommen wird
        //
        for (Iterator<Map.Entry<Integer, HashMap<Integer, ArrayList<ProdOrderBufferEntry>>>> it = planetSet.iterator(); it.hasNext();) {

            Map.Entry<Integer, HashMap<Integer, ArrayList<ProdOrderBufferEntry>>> planetEntry = it.next();

            Integer planetId = planetEntry.getKey();
            // PlanetData pd = new PlanetData(planetId);
            // DebugBuffer.addLine("Found production entries for planet " + planetId);

            HashMap<Integer, ArrayList<ProdOrderBufferEntry>> ordersByPrio = planetEntry.getValue();

            // Get current Production Values for this planet
            int dockPointsLeft = 0;
            int orbDockPointsLeft = 0;
            int modulPointsLeft = 0;
            int kasPointsLeft = 0;
            int indPointsLeft = 0;
            int bonusLevel = 1;

            // HashMap<Integer,Integer> constructionsMap = pcb.getConsForPlanet(planetId.intValue());
            if (planetId.intValue() == 2) {
                // DebugBuffer.addLine(constructionsMap.size() + " buildings found");
            }

            ProductionResult pr = pc.getPlanetCalcData().getProductionData();

            modulPointsLeft = (int) pr.getRessProduction(Ressource.MODUL_AP);
            dockPointsLeft = (int) pr.getRessProduction(Ressource.PL_DOCK);
            orbDockPointsLeft = (int) pr.getRessProduction(Ressource.ORB_DOCK);
            kasPointsLeft = (int) pr.getRessProduction(Ressource.KAS);
            indPointsLeft = ConstructionService.getIndustryPoints(planetId);

            // Reduce all Production Values till everything finished or no Production Points left
            // Remove finished orders here too
            // int[] prioCon = {0, 0, 0};
            // int[] prioProd = {0, 0, 0};
            int[] prioCon = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int[] prioProd = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

            for (int i = 0; i < PRIORITY_COUNT; i++) {

                if (ordersByPrio.containsKey(i)) {
                    for (ProdOrderBufferEntry pobe : ordersByPrio.get(i)) {
                        if (pobe.getType().equals(EProductionOrderType.C_BUILD)
                                || pobe.getType().equals(EProductionOrderType.C_SCRAP)) {
                            prioCon[i]++;
                        } else {
                            prioProd[i]++;
                        }
                    }
                }
            }


            for (int i = 0; i < PRIORITY_COUNT; i++) {
                boolean finished = true;
                ArrayList<ProdOrderBufferEntry> currOrder = ordersByPrio.get(i);

                if (prioProd[i] == 0 && prioCon[i] == 0) {
                    continue;
                }
                do {
                    int availMod = modulPointsLeft;
                    int availDock = dockPointsLeft;
                    int availOrbDock = orbDockPointsLeft;
                    int availKas = kasPointsLeft;
                    int availInd = indPointsLeft;
                    if (prioProd[i] != 0) {
                        availMod = (int) (modulPointsLeft / prioProd[i]);
                        availDock = (int) (dockPointsLeft / prioProd[i]);
                        availOrbDock = (int) (orbDockPointsLeft / prioProd[i]);
                        availKas = (int) (kasPointsLeft / prioProd[i]);
                    }
                    if (prioCon[i] != 0) {
                        availInd = (int) (indPointsLeft / prioCon[i]);
                    }

                    finished = true;
                    boolean couldAdd = false;

                    for (int j = 0; j < currOrder.size(); j++) {
                        ProdOrderBufferEntry pobe = prioListPP.get(planetId.intValue()).get(i).get(j);
                        int diffMod = pobe.getModuleNeed() - pobe.getModuleProc();
                        int diffDock = pobe.getDockNeed() - pobe.getDockProc();
                        int diffOrb = pobe.getOrbDockNeed() - pobe.getOrbDockProc();
                        int diffKas = pobe.getKasNeed() - pobe.getKasProc();
                        int diffInd = pobe.getIndNeed() - pobe.getIndProc();
                        if ((diffMod > 0) && (availMod > 0)) {
                            couldAdd = true;
                            if (diffMod < availMod) {
                                modulPointsLeft -= diffMod;
                                pobe.setModuleProc(pobe.getModuleNeed());
                            } else {
                                modulPointsLeft -= availMod;
                                pobe.setModuleProc(pobe.getModuleProc() + availMod);
                            }
                        }

                        if ((diffDock > 0) && (availDock > 0)) {
                            couldAdd = true;
                            if (diffDock < availDock) {
                                dockPointsLeft -= diffDock;
                                pobe.setDockProc(pobe.getDockNeed());
                            } else {
                                dockPointsLeft -= availDock;
                                pobe.setDockProc(pobe.getDockProc() + availDock);
                            }
                        }

                        if ((diffOrb > 0) && (availOrbDock > 0)) {
                            couldAdd = true;
                            if (diffOrb < availOrbDock) {
                                orbDockPointsLeft -= diffOrb;
                                pobe.setOrbDockProc(pobe.getOrbDockNeed());
                            } else {
                                orbDockPointsLeft -= availOrbDock;
                                pobe.setOrbDockProc(pobe.getOrbDockProc() + availOrbDock);
                            }
                        }

                        if ((diffKas > 0) && (availKas > 0)) {
                            couldAdd = true;
                            if (diffKas < availKas) {
                                kasPointsLeft -= diffKas;
                                pobe.setKasProc(pobe.getKasNeed());
                            } else {
                                kasPointsLeft -= availKas;
                                pobe.setKasProc(pobe.getKasProc() + availKas);
                            }
                        }

                        if ((diffInd > 0) && (availInd > 0)) {
                            couldAdd = true;
                            if (diffInd < availInd) {
                                indPointsLeft -= diffInd;
                                pobe.setIndProc(pobe.getIndNeed());
                            } else {
                                indPointsLeft -= availInd;
                                pobe.setIndProc(pobe.getIndProc() + availInd);
                            }
                        }

                        if ((pobe.getModuleNeed() <= pobe.getModuleProc()) && (pobe.getDockNeed() <= pobe.getDockProc()) && (pobe.getKasNeed() <= pobe.getKasProc()) && (pobe.getOrbDockNeed() <= pobe.getOrbDockProc()) && (pobe.getIndNeed() <= pobe.getIndProc())) {
                            //Notification - START
                            //If a construction is finished check for this user if he is logged
                            if (!loggedChecked) {
                                logged = SessionTracker.isOnline(pobe.getUserId());                                
                                loggedChecked = true;
                            }
                            ArrayList<ProdOrderBufferEntry> tmpOrders = finishedOrders.get(pobe.getType());
                            if (tmpOrders == null) {
                                tmpOrders = new ArrayList<ProdOrderBufferEntry>();
                            }
                            //Notification - END

                            // Code for removing this item from processing list
                            if (pobe.getType() == EProductionOrderType.PRODUCE) {
                                // DebugBuffer.addLine(DebugLevel.TRACE, "Trying to finish an Production Order");
                                finishProductionOrder(pobe);

                                System.out.println("Create Notification for " + pobe.getUserId());
                                NotificationToUser ntu = ntuDAO.getOrCreate(pobe.getUserId(), NotificationType.ID_SHIP);
                                
                                System.out.println("LOGGED="+logged+" NTU_ENABLED="+ntu.getEnabled()+" NTU_GETAWAYMODE="+ntu.getAwayMode());
                                
                                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                                    System.out.println("Put order type " + pobe.getType() + " into finished orders");
                                    finishedOrders.put(pobe.getType(), tmpOrders);
                                    tmpOrders.add(pobe);
                                }
                            }
                            if (pobe.getType() == EProductionOrderType.UPGRADE) {
                                // DebugBuffer.addLine(DebugLevel.TRACE, "Trying to finish an Upgrade Order");
                                finishUpgradeOrder(pobe);
                                NotificationToUser ntu = ntuDAO.getOrCreate(pobe.getUserId(), NotificationType.ID_SHIP);
                                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                                    finishedOrders.put(pobe.getType(), tmpOrders);
                                    tmpOrders.add(pobe);
                                }
                            }
                            if (pobe.getType() == EProductionOrderType.SCRAP) {
                                // DebugBuffer.addLine(DebugLevel.TRACE, "Trying to finish a Scrap Order");
                                finishScrapOrder(pobe);
                                NotificationToUser ntu = ntuDAO.getOrCreate(pobe.getUserId(), NotificationType.ID_SHIP);
                                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                                    finishedOrders.put(pobe.getType(), tmpOrders);
                                    tmpOrders.add(pobe);
                                }
                            }
                            if (pobe.getType() == EProductionOrderType.REPAIR) {
                                // DebugBuffer.addLine(DebugLevel.TRACE, "Trying to finish a Scrap Order");
                                finishRepairOrder(pobe);
                                NotificationToUser ntu = ntuDAO.getOrCreate(pobe.getUserId(), NotificationType.ID_SHIP);
                                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                                    finishedOrders.put(pobe.getType(), tmpOrders);
                                    tmpOrders.add(pobe);
                                }
                            }
                            if (pobe.getType() == EProductionOrderType.GROUNDTROOPS) {
                                // DebugBuffer.addLine(DebugLevel.TRACE, "Trying to finish a Groundtroop Order");
                                finishGroundTroopOrder(pobe);
                                NotificationToUser ntu = ntuDAO.getOrCreate(pobe.getUserId(), NotificationType.ID_GROUNDTROOP);
                                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                                    finishedOrders.put(pobe.getType(), tmpOrders);
                                    tmpOrders.add(pobe);
                                }

                            }
                            if ((pobe.getType() == EProductionOrderType.C_BUILD) || (pobe.getType() == EProductionOrderType.C_BUILD_STATIONS)) {
                                // DebugBuffer.addLine(DebugLevel.TRACE, "Trying to finish a Construction Order");
                                finishConstructionOrder(pobe);
                                NotificationToUser ntu = ntuDAO.getOrCreate(pobe.getUserId(), NotificationType.ID_CONSTRUCTION);
                                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                                    finishedOrders.put(pobe.getType(), tmpOrders);
                                    tmpOrders.add(pobe);
                                }
                            }
                            if ((pobe.getType() == EProductionOrderType.C_SCRAP)) {
                                // DebugBuffer.addLine(DebugLevel.TRACE, "Trying to finish a Deconstruction Order");
                                finishDeconstructionOrder(pobe);
                                NotificationToUser ntu = ntuDAO.getOrCreate(pobe.getUserId(), NotificationType.ID_CONSTRUCTION);
                                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                                    finishedOrders.put(pobe.getType(), tmpOrders);
                                    tmpOrders.add(pobe);
                                }
                            }
                            if ((pobe.getType() == EProductionOrderType.C_IMPROVE)) {
                                // DebugBuffer.addLine(DebugLevel.TRACE, "Trying to finish a Deconstruction Order");
                                finishConstructionImproveOrder(pobe);
                                NotificationToUser ntu = ntuDAO.getOrCreate(pobe.getUserId(), NotificationType.ID_CONSTRUCTION);
                                if (logged && ntu.getEnabled() || !logged && ntu.getAwayMode()) {
                                    finishedOrders.put(pobe.getType(), tmpOrders);
                                    tmpOrders.add(pobe);
                                }
                            }

                            prioListPP.get(planetId.intValue()).get(i).remove(pobe);
                        }
                    }

                    if (((availDock + availMod + availOrbDock + availKas) > 0) && couldAdd) {
                        finished = false;
                    }

                } while (!finished);
            }

        }
        /**
         * NOTIFICATION
         */
        if (finishedOrders.size() > 0) {
            NotificationBuffer.addPONotification(pc.getPlayerPlanet().getUserId(), actPlanet, finishedOrders);
        }
        // ProductionOrderNotification.createNotification(finishedOrders, actPlanet, logged);
    }

    public void cleanUp() {
        globalProductionBuffer.clear();
        prioListPP.clear();
    }

    private void finishUpgradeOrder(ProdOrderBufferEntry order) {

        if (order.getActionType() == EActionType.SHIP) {
            try {
                ProductionOrder po = order.getProductionOrder();
                Action a = order.getAction();
                ShipDesignExt sdeFrom = null;
                ShipDesignExt sdeTo = null;
                //Getting values
                try {
                    sdeFrom = new ShipDesignExt(a.getShipDesignId());
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error in ShipDesignExt : " + e);
                    sdeFrom = null;
                }
                try {
                    sdeTo = new ShipDesignExt(po.getToId());
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error in ShipDesignExt : " + e);
                    sdeTo = null;
                }
                if (sdeFrom == null || sdeTo == null) {
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "fromDesign oder toDesign in Shipupgrade = null");
                    if (sdeFrom == null) {
                        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "fromDesign == null ersetzte es mit ToDesign");
                        sdeFrom = new ShipDesignExt(po.getToId());
                    }
                    if (sdeTo == null) {
                        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "toDesign == null ersetzte es mit fromDesign");
                        sdeTo = new ShipDesignExt(a.getShipDesignId());
                    }
                }
                ShipUpgradeCost suc = sdeFrom.getShipUpgradeCost(sdeTo, a.getNumber());

                //Adding Ressources
                MutableRessourcesEntry mre = new MutableRessourcesEntry(order.getUserId(), order.getPlanetId());
                for (RessAmountEntry rae : suc.getRessArray()) {
                    if (rae.getQty() > 0) {
                        suc.setRess(rae.getRessId(), 0l);
                    }
                }

                RessourcesEntry re = new RessourcesEntry(suc.getRess());
                mre.subtractRess(re, 1d);

                for (RessAmountEntry rae : mre.getRessArray()) {

                    if (rae.getRessId() == Ressource.CREDITS) {
                        UserData ud = udDAO.findByUserId(order.getUserId());
                        ud.setCredits((long) rae.getQty());
                        udDAO.update(ud);
                    } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                        // TODO: Help me i'm empty!
                    } else {
                        PlanetRessource pr = prDAO.findBy(order.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                        if (pr != null) {
                            pr.setQty((long) rae.getQty());
                            prDAO.update(pr);
                        } else {
                            pr = new PlanetRessource();
                            pr.setPlanetId(order.getPlanetId());
                            pr.setRessId(rae.getRessId());
                            pr.setType(EPlanetRessourceType.INSTORAGE);
                            pr.setQty((long) rae.getQty());
                            prDAO.add(pr);
                        }
                    }
                }

                Planet p = pDAO.findById(order.getPlanetId());

                PlayerFleet pfSearch = new PlayerFleet();
                pfSearch.setName("Upgrade Planet #" + order.getPlanetId());
                pfSearch.setPlanetId(order.getPlanetId());
                pfSearch.setUserId(order.getUserId());
                ArrayList<PlayerFleet> pfList = pfDAO.find(pfSearch);

                PlayerFleet actPF = null;
                if (!pfList.isEmpty()) {
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Fleet exists");
                    actPF = pfList.get(0);
                } else {
                    // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Fleet does not exists");
                    pfSearch.setUserId(order.getUserId());
                    pfSearch.setLocation(ELocationType.PLANET, order.getPlanetId());
                    // pfSearch.setSystemId(p.getSystemId());
                    actPF = pfDAO.add(pfSearch);
                }

                ShipFleet sfSearch = new ShipFleet();
                sfSearch.setFleetId(actPF.getId());
                sfSearch.setDesignId(order.getProductionOrder().getToId());
                ArrayList<ShipFleet> sfList = sfDAO.find(sfSearch);

                if (sfList.size() == 1) {
                    ShipFleet sf = sfList.get(0);
                    sf.setCount(sf.getCount() + order.getCount());
                    sfDAO.update(sf);
                } else {
                    sfSearch.setCount(order.getCount());
                    sfDAO.add(sfSearch);
                }

                uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
                aDAO.remove(order.getAction());
                poDAO.remove(order.getProductionOrder());
                globalProductionBuffer.remove(order.getId());
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while finishing Ship Upgrade Order : ", e);
            }
        } else if (order.getActionType() == EActionType.SPACESTATION) {
            try {
                ProductionOrder po = order.getProductionOrder();
                Action a = order.getAction();
                ShipDesignExt sdeFrom = null;
                ShipDesignExt sdeTo = null;
                //Getting values
                try {
                    sdeFrom = new ShipDesignExt(a.getShipDesignId());
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error in ShipDesignExt : " + e);
                    sdeFrom = null;
                }
                try {
                    sdeTo = new ShipDesignExt(po.getToId());
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error in ShipDesignExt : " + e);
                    sdeTo = null;
                }
                if (sdeFrom == null || sdeTo == null) {
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "fromDesign oder toDesign in Shipupgrade = null");
                    if (sdeFrom == null) {
                        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "fromDesign == null ersetzte es mit ToDesign");
                        sdeFrom = new ShipDesignExt(po.getToId());
                    }
                    if (sdeTo == null) {
                        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "toDesign == null ersetzte es mit fromDesign");
                        sdeTo = new ShipDesignExt(a.getShipDesignId());
                    }
                }
                ShipUpgradeCost suc = sdeFrom.getShipUpgradeCost(sdeTo, a.getNumber());

                //Adding Ressources
                MutableRessourcesEntry mre = new MutableRessourcesEntry(order.getUserId(), order.getPlanetId());
                for (RessAmountEntry rae : suc.getRessArray()) {
                    if (rae.getQty() > 0) {
                        suc.setRess(rae.getRessId(), 0l);
                    }
                }

                RessourcesEntry re = new RessourcesEntry(suc.getRess());
                mre.subtractRess(re, 1d);

                for (RessAmountEntry rae : mre.getRessArray()) {

                    if (rae.getRessId() == Ressource.CREDITS) {
                        UserData ud = udDAO.findByUserId(order.getUserId());
                        ud.setCredits((long) rae.getQty());
                        udDAO.update(ud);
                    } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                        // TODO Help me i'm empty
                    } else {
                        PlanetRessource pr = prDAO.findBy(order.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                        if (pr != null) {
                            pr.setQty((long) rae.getQty());
                            prDAO.update(pr);
                        } else {
                            pr = new PlanetRessource();
                            pr.setPlanetId(order.getPlanetId());
                            pr.setRessId(rae.getRessId());
                            pr.setType(EPlanetRessourceType.INSTORAGE);
                            pr.setQty((long) rae.getQty());
                            prDAO.add(pr);
                        }
                    }
                }


                PlanetDefense pd = pdDAO.findBy(a.getPlanetId(), po.getToId(), a.getUserId(), EDefenseType.SPACESTATION);
                if (pd == null) {
                    pd = new PlanetDefense();
                    pd.setCount(a.getNumber());
                    pd.setType(EDefenseType.SPACESTATION);
                    pd.setPlanetId(a.getPlanetId());
                    pd.setUserId(a.getUserId());
                    pd.setUnitId(po.getToId());
                    pdDAO.add(pd);
                } else {
                    pd.setCount(pd.getCount() + a.getNumber());
                    pdDAO.update(pd);
                }

                uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
                aDAO.remove(order.getAction());
                poDAO.remove(order.getProductionOrder());
                globalProductionBuffer.remove(order.getId());
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while finishing Ship Upgrade Order : ", e);
            }
        }
    }

    private void finishRepairOrder(ProdOrderBufferEntry order) {
        try {
            Planet p = pDAO.findById(order.getPlanetId());

            PlayerFleet pfSearch = new PlayerFleet();
            pfSearch.setName("Repar. Planet #" + order.getPlanetId());
            pfSearch.setPlanetId(order.getPlanetId());
            pfSearch.setUserId(order.getUserId());
            ArrayList<PlayerFleet> pfList = pfDAO.find(pfSearch);

            PlayerFleet actPF = null;
            if (!pfList.isEmpty()) {
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Fleet exists");
                actPF = pfList.get(0);
            } else {
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Fleet does not exists");
                pfSearch.setUserId(order.getUserId());
                pfSearch.setLocation(ELocationType.PLANET, order.getPlanetId());
                // pfSearch.setSystemId(p.getSystemId());
                actPF = pfDAO.add(pfSearch);
            }

            ShipFleet sfSearch = new ShipFleet();
            sfSearch.setFleetId(actPF.getId());
            sfSearch.setDesignId(order.getDesignId());
            ArrayList<ShipFleet> sfList = sfDAO.find(sfSearch);

            if (sfList.size() == 1) {
                ShipFleet sf = sfList.get(0);
                sf.setCount(sf.getCount() + order.getCount());
                sfDAO.update(sf);
            } else {
                sfSearch.setCount(order.getCount());
                sfDAO.add(sfSearch);
            }

            uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
            aDAO.remove(order.getAction());
            poDAO.remove(order.getProductionOrder());
            globalProductionBuffer.remove(order.getId());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while finishing Production=", e);
        }
    }

    private void finishScrapOrder(ProdOrderBufferEntry order) {
        Action a = order.getAction();

        //Getting values
        ShipDesignExt sdeFrom = new ShipDesignExt(a.getShipDesignId());

        //Adding Ressources
        // MutableRessourcesEntry mre = new MutableRessourcesEntry(order.getUserId(), order.getPlanetId());
        RessourcesEntry scrapRE = new RessourcesEntry(sdeFrom.getShipScrapCost(order.getCount()).getRess());

        for (RessAmountEntry rae : scrapRE.getRessArray()) {
            if (rae.getRessId() == Ressource.CREDITS) {
                //do Nothing
            } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                // TODO Help me i'm empty
            } else {
                PlanetRessource pr = uds.getPlanetRessEntry(order.getPlanetId(), EPlanetRessourceType.INSTORAGE, rae.getRessId());
                if (pr != null) {
                    int qty = Math.min((int) (rae.getQty() + pr.getQty()), uds.getMaxStorage(order.getPlanetId(), rae.getRessId()));
                    pr.setQty((long) qty);
                } else {
                    /*
                     PlanetRessource prNew = new PlanetRessource();
                     prNew.setPlanetId(order.getPlanetId());
                     prNew.setType(EPlanetRessourceType.INSTORAGE);
                     prNew.setRessId(rae.getRessId());
                     prNew.setQty((long)rae.getQty());
                     prDAO.add(prNew);
                     */
                    uds.createNewStorageRessEntry(order.getPlanetId(), rae.getRessId(), (int) rae.getQty());
                }
            }
        }

        uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
        aDAO.remove(order.getAction());
        poDAO.remove(order.getProductionOrder());
        globalProductionBuffer.remove(order.getId());
    }

    private void finishDeconstructionOrder(ProdOrderBufferEntry order) {
        try {
            // Building stuff
            if (order.getActionType() == EActionType.DECONSTRUCT) { // Constructions
                Construction c = cDAO.findById(order.getDesignId());

                if (c != null) {
                    //RessourcenR�ckverg�tung
                    ConstructionExt ce = new ConstructionExt(c.getId());
                    ConstructionScrapCost scc = ce.getScrapCost(order.getPlanetId());

                    MutableRessourcesEntry mre = scc;
                    mre.multiplyRessources(order.getCount());

                    for (RessAmountEntry rae : mre.getRessArray()) {
                        if (rae.getRessId() == Ressource.CREDITS) {
                            continue;
                        }
                        if (rae.getRessId() == Ressource.DEV_POINTS) {
                            continue;
                        }

                        PlanetRessource actPr = uds.getPlanetRessEntry(order.getPlanetId(), EPlanetRessourceType.INSTORAGE, rae.getRessId());
                        if (actPr != null) {
                            int qty = Math.min((int) (rae.getQty() + actPr.getQty()), uds.getMaxStorage(order.getPlanetId(), rae.getRessId()));
                            actPr.setQty((long) qty);
                        } else {
                            uds.createNewStorageRessEntry(order.getPlanetId(), rae.getRessId(), (int) rae.getQty());
                        }
                    }
                    ConstructionRestriction cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, order.getDesignId());
                    if (cr != null) {
                        Class rc = Class.forName("at.darkdestiny.core.construction.restriction." + cr.getRestrictionClass());
                        ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                        cir.onDeconstruction(ppDAO.findByPlanetId(order.getPlanetId()));
                    }
                } else {
                    throw new RuntimeException("Failed to get information about the building from the database!");
                }
            }/* else if (order.getActionType() == Action.TYPE_DEFENSE) { // Space stations other stuff
             PlanetDefense pdSearch = new PlanetDefense();
             pdSearch.setPlanetId(order.getPlanetId());
             pdSearch.setUnitId(order.getDesignId());
             pdSearch.setUserId(order.getUserId());
             pdSearch.setType(2);
             ArrayList<PlanetDefense> pdList = pdDAO.find(pdSearch);

             if (pdList.size() == 1) {
             DebugBuffer.addLine(DebugLevel.TRACE, "Building construction " + order.getDesignId() + "("+order.getCount()+") for user " + order.getUserId());
             PlanetDefense pdTmp = pdList.get(0);
             pdTmp.setCount(pdTmp.getCount() + order.getCount());
             pdDAO.update(pdTmp);
             } else {
             pdSearch.setCount(order.getCount());
             pdSearch.setSystemId(order.getSystemId());
             pdDAO.add(pdSearch);
             }
             }*/
            // Building stuff END

            uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
            aDAO.remove(order.getAction());
            poDAO.remove(order.getProductionOrder());
            globalProductionBuffer.remove(order.getId());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while scrapping construction = ", e);
        }

    }

    private void finishConstructionOrder(ProdOrderBufferEntry order) {
        try {
            // Building stuff
            if (order.getActionType() == EActionType.BUILDING) { // Constructions
                Construction c = cDAO.findById(order.getDesignId());

                if (c != null) {
                    if (c.getType() == EConstructionType.PLANETARY_DEFENSE) { // Planetary stationary defense
                        PlanetDefense pdSearch = new PlanetDefense();
                        pdSearch.setPlanetId(order.getPlanetId());
                        pdSearch.setUnitId(order.getDesignId());
                        pdSearch.setUserId(order.getUserId());
                        pdSearch.setType(EDefenseType.TURRET);
                        ArrayList<PlanetDefense> pdList = pdDAO.find(pdSearch);

                        if (pdList.size() == 1) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Building construction " + order.getDesignId() + "(" + order.getCount() + ") for user " + order.getUserId());
                            PlanetDefense pdTmp = pdList.get(0);
                            pdTmp.setCount(pdTmp.getCount() + order.getCount());
                            pdDAO.update(pdTmp);
                        } else {
                            pdSearch.setCount(order.getCount());
                            pdSearch.setSystemId(order.getSystemId());
                            pdDAO.add(pdSearch);
                        }
                    } else { // Normal Buildings
                        PlanetConstruction pcSearch = new PlanetConstruction();
                        pcSearch.setPlanetId(order.getPlanetId());
                        pcSearch.setConstructionId(order.getDesignId());
                        ArrayList<PlanetConstruction> pcList = pcDAO.find(pcSearch);

                        if (pcList.size() == 1) {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Building construction " + order.getDesignId() + "(" + order.getCount() + ") for user " + order.getUserId());
                            PlanetConstruction pcTmp = pcList.get(0);
                            pcTmp.setNumber(pcTmp.getNumber() + order.getCount());
                            pcDAO.update(pcTmp);
                        } else {
                            DebugBuffer.addLine(DebugLevel.TRACE, "Building construction " + order.getDesignId() + "(" + order.getCount() + ") for user " + order.getUserId());
                            pcSearch.setNumber(order.getCount());
                            if (c.getLevelable()) {
                                pcSearch.setLevel(1);
                            }
                            pcDAO.add(pcSearch);
                        }

                        ConstructionRestriction cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, order.getDesignId());
                        if (cr != null) {
                            DebugBuffer.trace("Found ConstructionInterface for " + c.getName());
                            Class rc = Class.forName("at.darkdestiny.core.construction.restriction." + cr.getRestrictionClass());
                            ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                            cir.onFinishing(ppDAO.findByPlanetId(order.getPlanetId()));
                        } else {
                            DebugBuffer.trace("Did not find ConstructionInterface for " + c.getName());
                        }
                        // Trigger for special Buildings which do something special
                        if (c.getSpecialBuildingId() != 0) {
                            // DebugBuffer.addLine("SPECIAL BUILDING FOUND");
                            // SpecialActions sa = new SpecialActions(order.getUserId(), order.getPlanetId(), order.getSystemId());
                            //   sa.triggerBuilding(c.getSpecialBuildingId());
                        } else {
                            // DebugBuffer.addLine("NOTHING SPECIAL TO DO");
                        }
                    }
                } else {
                    throw new RuntimeException("Failed to get information about the building from the database!");
                }
            }
            /*
             * SPACE STATIONS ARE NOW HANDLED
             * OVER PRODUCTION OF SHIPS
             *
             else if (order.getActionType() == EActionType.DEFENSE) { // Space stations other stuff
             PlanetDefense pdSearch = new PlanetDefense();
             pdSearch.setPlanetId(order.getPlanetId());
             pdSearch.setUnitId(order.getDesignId());
             pdSearch.setUserId(order.getUserId());
             pdSearch.setType(EDefenseType.SPACESTATION);
             ArrayList<PlanetDefense> pdList = pdDAO.find(pdSearch);

             if (pdList.size() == 1) {
             DebugBuffer.addLine(DebugLevel.TRACE, "Building construction " + order.getDesignId() + "(" + order.getCount() + ") for user " + order.getUserId());
             PlanetDefense pdTmp = pdList.get(0);
             pdTmp.setCount(pdTmp.getCount() + order.getCount());
             pdDAO.update(pdTmp);
             } else {
             pdSearch.setCount(order.getCount());
             pdSearch.setSystemId(order.getSystemId());
             pdDAO.add(pdSearch);
             }
             }
             */
            // Building stuff END

            uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
            aDAO.remove(order.getAction());
            poDAO.remove(order.getProductionOrder());
            globalProductionBuffer.remove(order.getId());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while building construction = ", e);
        }
    }

    private void finishConstructionImproveOrder(ProdOrderBufferEntry order) {
        try {
            // Building stuff
            if (order.getActionType() == EActionType.BUILDING) { // Constructions
                Construction c = cDAO.findById(order.getDesignId());

                if (c != null) {
                    PlanetConstruction pcSearch = new PlanetConstruction();
                    pcSearch.setPlanetId(order.getPlanetId());
                    pcSearch.setConstructionId(order.getDesignId());
                    ArrayList<PlanetConstruction> pcList = pcDAO.find(pcSearch);

                    if (pcList.size() == 1) {
                        DebugBuffer.addLine(DebugLevel.TRACE, "Building construction " + order.getDesignId() + "(" + order.getCount() + ") for user " + order.getUserId());
                        PlanetConstruction pcTmp = pcList.get(0);
                        pcTmp.setLevel(pcTmp.getLevel() + order.getCount());
                        pcDAO.update(pcTmp);

                        ConstructionRestriction cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, order.getDesignId());
                        if (cr != null) {
                            Class rc = Class.forName("at.darkdestiny.core.construction.restriction." + cr.getRestrictionClass());
                            ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                            cir.onImproved(ppDAO.findByPlanetId(order.getPlanetId()));
                        }
                    } else {
                        // Yeah its a bad situation but an error is not required -- otherwise order is not cleaned up
                        // throw new RuntimeException("Wanted to upgrade not existing building");
                    }
                } else {
                    throw new RuntimeException("Failed to get information about the building from the database!");
                }
            }
            uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
            aDAO.remove(order.getAction());
            poDAO.remove(order.getProductionOrder());
            globalProductionBuffer.remove(order.getId());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while building construction = ", e);
        }
    }

    private void finishGroundTroopOrder(ProdOrderBufferEntry order) {
        try {
            PlayerPlanet pp = ppDAO.findByPlanetId(order.getPlanetId());
            PlayerTroop ptSearch = new PlayerTroop();
            ptSearch.setPlanetId(order.getPlanetId());
            ptSearch.setTroopId(order.getDesignId());
            //  ptSearch.setUserId(order.getUserId());
            ptSearch.setUserId(pp.getUserId());
            ArrayList<PlayerTroop> ptList = ptDAO.find(ptSearch);

            if (ptList.size() == 1) {
                PlayerTroop ptTmp = ptList.get(0);
                ptTmp.setNumber(ptTmp.getNumber() + order.getCount());
                ptDAO.update(ptTmp);
            } else {
                ptSearch.setNumber((long) order.getCount());
                ptDAO.add(ptSearch);
            }

            uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
            aDAO.remove(order.getAction());
            poDAO.remove(order.getProductionOrder());
            globalProductionBuffer.remove(order.getId());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while finishing Production=", e);
        }
    }

    private void finishProductionOrder(ProdOrderBufferEntry order) {
        try {
            Planet p = pDAO.findById(order.getPlanetId());

            if (order.getActionType() == EActionType.SPACESTATION) {
                // SPACE STATIONS
                PlanetDefense pdSearch = new PlanetDefense();
                pdSearch.setPlanetId(order.getPlanetId());
                pdSearch.setUnitId(order.getDesignId());
                pdSearch.setUserId(order.getUserId());
                pdSearch.setType(EDefenseType.SPACESTATION);
                ArrayList<PlanetDefense> pdList = pdDAO.find(pdSearch);

                if (pdList.size() == 1) {
                    DebugBuffer.addLine(DebugLevel.TRACE, "Building construction " + order.getDesignId() + "(" + order.getCount() + ") for user " + order.getUserId());
                    PlanetDefense pdTmp = pdList.get(0);
                    pdTmp.setCount(pdTmp.getCount() + order.getCount());
                    pdDAO.update(pdTmp);
                } else {
                    pdSearch.setCount(order.getCount());
                    pdSearch.setSystemId(order.getSystemId());
                    pdDAO.add(pdSearch);
                }
            } else {
                // NORMAL SHIPS
                PlayerFleet pfSearch = new PlayerFleet();
                pfSearch.setName("Konstr. Planet #" + order.getPlanetId());
                pfSearch.setPlanetId(order.getPlanetId());
                //Has to be the user who constructed it, else it gets to a random fleet
                pfSearch.setUserId(order.getUserId());
                ArrayList<PlayerFleet> pfList = pfDAO.find(pfSearch);

                PlayerFleet actPF = null;
                if (!pfList.isEmpty()) {
                    DebugBuffer.addLine(DebugLevel.UNKNOWN, "Fleet exists");
                    actPF = pfList.get(0);
                } else {
                    DebugBuffer.addLine(DebugLevel.UNKNOWN, "Fleet does not exists");
                    pfSearch.setUserId(order.getUserId());
                    pfSearch.setLocation(ELocationType.PLANET, order.getPlanetId());
                    // pfSearch.setSystemId(p.getSystemId());
                    actPF = pfDAO.add(pfSearch);
                }

                ShipFleet sfSearch = new ShipFleet();
                sfSearch.setFleetId(actPF.getId());
                sfSearch.setDesignId(order.getDesignId());
                ArrayList<ShipFleet> sfList = sfDAO.find(sfSearch);

                if (sfList.size() == 1) {
                    ShipFleet sf = sfList.get(0);
                    sf.setCount(sf.getCount() + order.getCount());
                    sfDAO.update(sf);
                } else {
                    sfSearch.setCount(order.getCount());
                    sfDAO.add(sfSearch);
                }
            }

            uds.removeActionEntry(order.getUserId(), order.getActionType(), order.getAction());
            aDAO.remove(order.getAction());
            poDAO.remove(order.getProductionOrder());
            globalProductionBuffer.remove(order.getId());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while finishing Production=", e);
        }
    }

    protected ArrayList<ProductionOrder> getUpdatedOrders() {
        ArrayList<ProductionOrder> poList = new ArrayList<ProductionOrder>();

        for (ProdOrderBufferEntry pobe : globalProductionBuffer.values()) {
            poList.add(pobe.getProductionOrder());
        }

        return poList;
    }

    protected void destroyProductionOrder(int id) {
        DebugBuffer.trace("Remove production order with id " + id);

        ProdOrderBufferEntry pobe = globalProductionBuffer.get(id);
        if (pobe == null) {
            return;
        }

        poDAO.remove(pobe.getProductionOrder());
        aDAO.remove(pobe.getAction());

        uds.removeActionEntry(pobe.getUserId(), pobe.getActionType(), pobe.getAction());

        globalProductionBuffer.remove(id);
    }
}
