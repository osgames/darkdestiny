/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat.helper;

import at.darkdestiny.core.spacecombat.BattleShipNew;

/**
 *
 * @author Stefan
 */
public class TargetIdentifier {
    private final BattleShipNew target;
    private final int targetValueId;
    
    public TargetIdentifier(BattleShipNew target, int targetValueId) {
        this.target = target;
        this.targetValueId = targetValueId;
    }

    public BattleShipNew getTarget() {
        return target;
    }

    public int getTargetValueId() {
        return targetValueId;
    }
}
