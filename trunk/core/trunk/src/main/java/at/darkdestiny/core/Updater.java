package at.darkdestiny.core;

import at.darkdestiny.core.ai.AIThread;
import at.darkdestiny.core.ai.AIThreadHandler;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.EGameDataStatus;
import at.darkdestiny.core.exceptions.UpdaterException;
import at.darkdestiny.core.model.GameData;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.update.UpdateCoordinator;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Updater extends Service implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(Updater.class);

    private static final int timeOut = 300 * 1000;
    private static UpdateCoordinator uc;
    private static Thread updThread;

    /* Description about cumulativeUpdate
     * ==================================
     * False => calculates every tick completly, inclusive storing all data, then starts the next one
     *          Allows AI to process between ticks
     * True  => Skips storing of data until the last tick is calculated (faster)
     *          Leads to unpredictable behavior of AI
    */
    private static boolean cumulativeUpdate = false;

    boolean isAlive = true;
    boolean isFinished = false;
    Thread calThread = null;
    private PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    public Updater() {
    }

    public boolean runUpdate() throws Exception {
        // Check memory if there is a memory shortage wait till enough memory is free again
        Runtime rt = Runtime.getRuntime();

        boolean waitForMemory = true;
        while (waitForMemory) {
            long freeMemory = rt.freeMemory();

            if (freeMemory < 50000) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Running low on memory (" + FormatUtilities.getFormattedNumber(freeMemory) + " Bytes free)!");
                try {
                    rt.gc();
                    Thread.sleep(5000);

                    freeMemory = rt.freeMemory();

                    if (freeMemory < 5000000) {
                        DebugBuffer.addLine(DebugLevel.ERROR, "Cleaning failed!");
                        DebugBuffer.addLine(DebugLevel.ERROR, "Force DB Connection Reset!");
                        // Critical action should not be performed while update!!!
                        if (isFinished) {
                            DbConnect.forceConnectionReset();
                        }
                    }
                } catch (Exception e) {
                }
            } else {
                waitForMemory = false;
            }
        }

        // Get Update Object and check Update History (if already an update running for this player wait until finished
        int lastBufferTime = 0;

        GameUtilities gu = new GameUtilities();
        int currentTime = gu.getCurrentTick();

        do {
            try {
                boolean startUpdate = false;

                GameData gd = gameDataDAO.findById(1);

                // log.debug(LogLevel.DEBUG, "Last updated Tick = " + gd.getLastUpdatedTick() + " currentTime = " + currentTime);

                if (gd.getLastUpdatedTick() < currentTime && gd.getStatus().equals(EGameDataStatus.RUNNING)) {
                    startUpdate = true;
                }
                lastBufferTime = gd.getLastUpdatedTick().intValue();

                if (startUpdate) {
                    isAlive = true;
                    calThread = Thread.currentThread();
                    calThread.setName("calThread");
                    calThread.setPriority(3);
                    updThread = new Thread(this);
                    // Thread updThread = new Thread(this);
                    updThread.setName("Updater Controller");
                    updThread.start();

                    lastBufferTime = 0;

                    if (gd.getLastUpdatedTick() < currentTime) {
                        lastBufferTime = gd.getLastUpdatedTick().intValue();
                        startUpdate = true;
                    }

                    Runtime runtime = Runtime.getRuntime();
                    int noOfProcessors = runtime.availableProcessors();
                    Time.start("Get all player planet");
                    ArrayList<PlayerPlanet> allPp = ppDAO.findAll();
                    Time.stop();
                    int noOfPlanets = allPp.size();
                    log.debug(noOfProcessors + " cores available");

                    int workerThreads = 1;

                    if (noOfPlanets < noOfProcessors) {
                        workerThreads = noOfPlanets;
                    } else {
                        workerThreads = noOfProcessors;
                    }

                    UpdateCoordinator.getInstance().createWorkerThreads(workerThreads, lastBufferTime);
                    log.debug("Worker threads started");
                    // Updater2 upc = new Updater2(lastBufferTime);

                    // Re-Check if timestamp has changed
                    if (lastBufferTime != 0) {
                        // DebugBuffer.addLine((currentTime - lastBufferTime) + " timeUnits to calcluate (SYNCH="+(!synchError)+")");
                        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "ADDITIONAL TICKS TO CALCULATE!");
                    } else {
                        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "UPDATE WAS ALREADY PROCESSED!");
                        isFinished = true;
                        updThread.interrupt();
                        return false;
                    }

                    DebugBuffer.addLine(DebugLevel.TRACE, "RUNNING UPDATE (LAST TIME=" + lastBufferTime + ", CURRTIME=" + currentTime + ")");

                    int currentTimeTmp = currentTime;
                    if (!cumulativeUpdate) {
                        currentTimeTmp = lastBufferTime + 1;
                    }

                    runUpdates(currentTimeTmp);
                    isFinished = true;
                    updThread.interrupt();

                    lastBufferTime = gd.getLastUpdatedTick().intValue();
                    if (!cumulativeUpdate && (lastBufferTime < currentTime)) {
                        while (updThread.isAlive()) {
                            DebugBuffer.debug("Wait for timeout thread to stop");
                            Thread.sleep(50);
                        }
                        isFinished = false;
                    } else {
                        return true;
                    }
                } else {
                    isFinished = true;
                    return false;
                }
            } catch (UpdaterException ue) {
                isFinished = true;
                updThread.interrupt();
                DebugBuffer.error("Updater Exception: " + ue.getMessage());
                throw ue.getUnderlying();
            } catch (Exception e) {
                isFinished = true;
                updThread.interrupt();
                throw e;
                // DebugBuffer.writeStackTrace("Error in Updater.runUpdate(): ", e);
            }

            if (!cumulativeUpdate) {
                try {
                    // Give AI some time to respond
                    Thread.sleep(500);
                } catch (Exception e) {

                }
            }
        } while (!cumulativeUpdate);

        throw new RuntimeException("Update should not reach here!");
    }

    public boolean runUpdates(int currTime) {
        ArrayList<AIThread> aiThreads = AIThreadHandler.getThreads();
        System.out.println("START UPDATE STOP AI THREADS");
        for (AIThread ait : aiThreads) {
            ait.stopForUpdate();
        }

        log.debug("Run updates");
        uc = UpdateCoordinator.getInstance();
        // uc.setCalculationTime(currTime);
        // upc.setCalctoTick(currTime);

        long startTime = System.currentTimeMillis();

        while (uc.getCalculationTime() < currTime) {
            // while (upc.getCurrentTick() < currTime) {
            uc.setCalculationTime(uc.getCalculationTime() + 1);
            // upc.setCurrentTick(upc.getCurrentTick() + 1);

            long startTime2 = System.currentTimeMillis();

            uc.getMasterThread().preTickInit();
            // upc.preTickInit();
            Time.start("Updating all Planets");
            uc.calculateTick();
            log.debug("Processing finished");
            Time.stop();

            if (uc.isErrorState()) {
                uc.finishUpdate();
                throw new UpdaterException("UPDATER: PLANET UPDATES ENCOUNTERED ERROR!", uc.getErrorCause());
            }

            Time.start("Calculating Global Updates");

            if (!uc.getMasterThread().calculateGlobalUpdates()) {
                Exception e = null;
                if (uc.getMasterThread() != null) {
                    e = uc.getMasterThread().getGlobalUpdateFailCause();
                }

                uc.finishUpdate();
                // isFinished = true;
                // updThread.interrupt();

                if (e == null) {
                    Time.stop();
                    throw new UpdaterException("UPDATER: GLOBAL UPDATES -- MASTER UPDATE THREAD ENCOUNTERED UNKNOWN ERROR", new Exception("UNKNOWN CAUSE -- CHECK BUFFER"));
                }

                Time.stop();
                throw new UpdaterException("UPDATER: GLOBAL UPDATES -- MASTER UPDATE THREAD ENCOUNTERED ERROR: "+e.getMessage()+"!", e);
            }

            Time.stop();

            DebugBuffer.addLine(DebugLevel.TRACE, ">>>>>>>>>>>>>Average Time per Planet: " + ((System.currentTimeMillis() - startTime2) / planetDAO.findAll().size()));
            DebugBuffer.addLine(DebugLevel.TRACE, "UPC=" + uc.getCalculationTime() + " CT=" + currTime);

            isAlive = true;
        }

        Time.start("Writing values to Buffer");
        uc.getMasterThread().writeValuesToBuffer();
        // upc.writeValuesToBuffer();
        Time.stop();

        DebugBuffer.addLine(DebugLevel.TRACE, "Total Time for internal Calculation was " + (System.currentTimeMillis() - startTime) + "ms");

        DebugBuffer.addLine(DebugLevel.TRACE, "SET ALL PLANETS TO " + currTime);
        long startTime2 = System.currentTimeMillis();

        try {
            GameData gd = gameDataDAO.findById(1);
            gd.setLastUpdatedTick(currTime);
            gameDataDAO.update(gd);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("ERROR IN UPDATE=", e);
        }

        DebugBuffer.addLine(DebugLevel.TRACE, "Time for saving all data was " + (System.currentTimeMillis() - startTime2) + "ms");

        long startTime3 = System.currentTimeMillis();

        Time.start("Cleanup");
        uc.getMasterThread().cleanUp();
        Time.stop();

        isAlive = true;

        for (AIThread ait : aiThreads) {
            ait.resetTick();
        }
        System.out.println("END UPDATE STOP AI THREADS");

        uc.finishUpdate();

        DebugBuffer.addLine(DebugLevel.TRACE, "Time cleaning up was " + (System.currentTimeMillis() - startTime3) + "ms");
        DebugBuffer.addLine(DebugLevel.TRACE, "Finished");
        DebugBuffer.addLine(DebugLevel.TRACE, "Total Time for completing whole update was " + (System.currentTimeMillis() - startTime));

        return false;
    }

    @SuppressWarnings("deprecation")
    public void run() {
        DebugBuffer.debug("START TIMEOUT THREAD");

        isFinished = false;

        while (!isFinished) {
            isAlive = false;
            try {
                Thread.sleep(timeOut);

                if (!isAlive) {
                    calThread.stop();
                    DebugBuffer.addLine(DebugLevel.ERROR, "UPDATE FAILED! TOOK LONGER THAN " + FormatUtilities.getFormattedNumber(timeOut) + " ms!!!");
                    if (uc != null) {
                        uc.finishUpdate();
                    }
                    DebugBuffer.debug("TIMEOUT THREAD STOPPED REGULARY");
                    return;
                }
            } catch (InterruptedException e) {
                DebugBuffer.debug("INTERRUPTED TIMEOUT THREAD");
            }
        }

        DebugBuffer.debug("TIMEOUT THREAD STOPPED REGULARY");
    }
}