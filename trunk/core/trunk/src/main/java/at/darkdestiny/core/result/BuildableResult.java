/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class BuildableResult {
    private final boolean buildable;
    private final int possibleCount;    
    private final ArrayList<String> missingRequirements;
    
    private boolean deniedByTech = false;
    private boolean deniedByUnique = false;
    private boolean deniedByLandType = false;
    private boolean deniedBySurface = false;
    private boolean deniedByOutdated = false;
    private boolean deniedByLockedTech = false;
    private boolean possibleByPopActionPoints = true;
    private boolean deniedByMineableRessource = false;
    private boolean deniedByPlanetType = false;
    private boolean notFinished = false;

    public BuildableResult(boolean buildable, int possibleCount, ArrayList<String> missingRequirements) {
        this.buildable = buildable;
        this.possibleCount = possibleCount;
        this.missingRequirements = missingRequirements;
    }
    
    public boolean isBuildable() {
        return buildable;
    }

    public int getPossibleCount() {
        return possibleCount;
    }

    public ArrayList<String> getMissingRequirements() {
        return missingRequirements;
    }

    public boolean isDeniedByTech() {
        return deniedByTech;
    }

    public void setDeniedByTech(boolean deniedByTech) {
        this.deniedByTech = deniedByTech;
    }

    public boolean isNotFinished() {
        return notFinished;
    }

    public void setNotFinished(boolean notFinished) {
        this.notFinished = notFinished;
    }

    public boolean isDeniedByUnique() {
        return deniedByUnique;
    }

    public void setDeniedByUnique(boolean deniedByUnique) {
        this.deniedByUnique = deniedByUnique;
    }

    public boolean isDeniedByLandType() {
        return deniedByLandType;
    }

    public void setDeniedByLandType(boolean deniedByLandType) {
        this.deniedByLandType = deniedByLandType;
    }

    /**
     * @return the possibleByPopActionPoints
     */
    public boolean isPossibleByPopActionPoints() {
        return possibleByPopActionPoints;
    }

    /**
     * @param possibleByPopActionPoints the possibleByPopActionPoints to set
     */
    public void setPossibleByPopActionPoints(boolean possibleByPopActionPoints) {
        this.possibleByPopActionPoints = possibleByPopActionPoints;
    }

    /**
     * @return the deniedBySurface
     */
    public boolean isDeniedBySurface() {
        return deniedBySurface;
    }

    /**
     * @param deniedBySurface the deniedBySurface to set
     */
    public void setDeniedBySurface(boolean deniedBySurface) {
        this.deniedBySurface = deniedBySurface;
    }

    /**
     * @return the deniedByMineableRessource
     */
    public boolean isDeniedByMineableRessource() {
        return deniedByMineableRessource;
    }

    /**
     * @param deniedByMineableRessource the deniedByMineableRessource to set
     */
    public void setDeniedByMineableRessource(boolean deniedByMineableRessource) {
        this.deniedByMineableRessource = deniedByMineableRessource;
    }

    /**
     * @return the deniedByOutdated
     */
    public boolean isDeniedByOutdated() {
        return deniedByOutdated;
    }

    /**
     * @param deniedByOutdated the deniedByOutdated to set
     */
    public void setDeniedByOutdated(boolean deniedByOutdated) {
        this.deniedByOutdated = deniedByOutdated;
    }

    /**
     * @return the deniedByLockedTech
     */
    public boolean isDeniedByLockedTech() {
        return deniedByLockedTech;
    }

    /**
     * @param deniedByLockedTech the deniedByLockedTech to set
     */
    public void setDeniedByLockedTech(boolean deniedByLockedTech) {
        this.deniedByLockedTech = deniedByLockedTech;
    }

    /**
     * @return the deniedByPlanetType
     */
    public boolean isDeniedByPlanetType() {
        return deniedByPlanetType;
    }

    /**
     * @param deniedByPlanetType the deniedByPlanetType to set
     */
    public void setDeniedByPlanetType(boolean deniedByPlanetType) {
        this.deniedByPlanetType = deniedByPlanetType;
    }


}
