/**
 * 
 */
package at.darkdestiny.core.admin.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeMap;

/**
 * @author martin
 *
 */
public class SelectField extends IDatabaseColumn {

	private final int column;
	private TreeMap<Integer, String> allSelections = new TreeMap<Integer, String>();

	/**
	 * @param comment 
	 * @param readonly 
	 * @param name 
	 * @param column 
	 * 
	 */
	public SelectField(int column, String name, boolean readonly, String comment) {
		super(name,comment, readonly);
		this.column = column;
	}

	public void addValue(int integer, String string) {
		allSelections.put(integer, string);
	}

	public Object getFromResultSet(ResultSet rs) throws SQLException {
		return rs.getInt(column);
	}

	public String makeEdit(Object object) {

		if (object == null)
			object = 0;
		int sel = ((Integer)object).intValue();
		
		String res = "<select size=\"1\" width=\"250\" name=\"f_"+name+"\" "+
			(readonly?"readonly ":"")+">\n";
		for (int i : allSelections.keySet())
			res = res + "<option value=\""+i+"\" "+((i == sel)?"selected":"")+" >"+allSelections.get(i)+"</option>\n";
		res = res + "</select>\n";
		return res;
	}

	protected String getValueOnEmpty()
	{
		return "'0'";
	}
}
