/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.construction.ConstructionScrapCost;
import at.darkdestiny.core.model.Construction;

/**
 *
 * @author Stefan
 */
public class ScrapConstructionResult extends BaseResult {
    private final ConstructionScrapCost consScrapCost;
    private final Construction cons;
    private final int count;

    public ScrapConstructionResult(ConstructionScrapCost consScrapCost, Construction cons, int count) {
        super(false);

        this.consScrapCost = consScrapCost;
        this.cons = cons;
        this.count = count;
    }

    public ScrapConstructionResult(String error) {
        super(error,true);

        this.consScrapCost = null;
        this.cons = null;
        this.count = 0;
    }

    /**
     * @return the consScrapCost
     */
    public ConstructionScrapCost getConsScrapCost() {
        return consScrapCost;
    }

    /**
     * @return the cons
     */
    public Construction getCons() {
        return cons;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }
}
