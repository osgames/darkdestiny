/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.orderhandling;

import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.construction.*;
import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.ConstructionRestrictionDAO;
import at.darkdestiny.core.dao.DamagedShipsDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.ProductionOrderDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.ConstructionRestriction;
import at.darkdestiny.core.model.DamagedShips;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.orderhandling.orders.*;
import at.darkdestiny.core.orderhandling.result.OrderResult;
import at.darkdestiny.core.planetcalc.PlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.core.result.AbortableResult;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.BuildableResult;
import at.darkdestiny.core.result.ConstructionUpgradeResult;
import at.darkdestiny.core.result.DestructableResult;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.service.OverviewService;
import at.darkdestiny.core.ships.BuildTime;
import at.darkdestiny.core.ships.ShipRepairCost;
import at.darkdestiny.core.ships.ShipScrapCost;
import at.darkdestiny.core.ships.ShipUpgradeCost;
import at.darkdestiny.core.utilities.ConstructionUtilities;
import at.darkdestiny.core.utilities.DefenseUtilities;
import at.darkdestiny.core.utilities.ShipUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class OrderHandling {

    private static final Logger log = LoggerFactory.getLogger(OrderHandling.class);

    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    private static ConstructionRestrictionDAO crDAO = (ConstructionRestrictionDAO) DAOFactory.get(ConstructionRestrictionDAO.class);

    public static synchronized OrderResult constructOrder(ConstructOrder co) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        try {
            th.startTransaction();
            if (OrderSync.addOrderProcess(new OrderSyncEntry(co.getUserId()))) {
                BuildableResult br = null;
                if (co instanceof ConstructionConstructOrder) {
                    ConstructionConstructOrder cco = (ConstructionConstructOrder) co;
                    br = ConstructionUtilities.canBeBuilt(cco.getUnit().getBase(), cco.getUserId(),
                            cco.getPlanetId(), cco.getCount());

                    if (br.isBuildable()) {
                        processConstructOrder(co);
                        OrderSync.removeOrderProcess(co.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(co.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, br);
                    }
                } else if (co instanceof ShipConstructOrder) {
                    ShipConstructOrder sco = (ShipConstructOrder) co;
                    br = ConstructionUtilities.canBeBuilt(sco.getUnit().getBase(), sco.getUserId(),
                            sco.getPlanetId(), sco.getCount());

                    if (br.isBuildable()) {
                        processConstructOrder(co);
                        OrderSync.removeOrderProcess(co.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(co.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, br);
                    }
                } else if (co instanceof GroundTroopConstructOrder) {
                    GroundTroopConstructOrder gco = (GroundTroopConstructOrder) co;
                    br = ConstructionUtilities.canBeBuilt(gco.getUnit().getBase(), gco.getUserId(),
                            gco.getPlanetId(), gco.getCount());

                    if (br.isBuildable()) {
                        processConstructOrder(co);
                        OrderSync.removeOrderProcess(co.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(co.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, br);
                    }
                } else if (co instanceof ConstructionImproveOrder) {
                    // TODO
                    ConstructionImproveOrder cio = (ConstructionImproveOrder) co;
                    br = ConstructionUtilities.canBeImproved((Construction) cio.getUnit().getBase(), cio.getUserId(), cio.getPlanetId());

                    if (br.isBuildable()) {
                        processImproveOrder(co);
                        OrderSync.removeOrderProcess(co.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(co.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, br);
                    }
                } else {
                    OrderSync.removeOrderProcess(co.getUserId());
                    th.execute();
                    th.endTransaction();
                    return new OrderResult(false, "Invalid Type");
                }
            } else {
                OrderSync.removeOrderProcess(co.getUserId());
                th.execute();
                th.endTransaction();
                return new OrderResult(false, "Invalid Access");
            }

            th.execute();
            th.endTransaction();
            return new OrderResult(true);
        } catch (TransactionException te) {
            DebugBuffer.writeStackTrace("ConstructOrder failed: ", te);

            try {
                th.rollback();
            } catch (Exception e) {
                log.error("Transaction Rollback failed");
            }

            th.endTransaction();
            return new OrderResult(false, "Transaction Error");
        } finally {
            th.endTransaction();
        }
    }

    public static synchronized OrderResult scrapOrder(ScrapOrder so) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        try {
            th.startTransaction();
            if (OrderSync.addOrderProcess(new OrderSyncEntry(so.getUserId()))) {

                DestructableResult dr = null;
                if (so instanceof ConstructionScrapOrder) {
                    dr = ConstructionUtilities.canBeDestructed(so);
                    if (dr.isDestructable()) {
                        processScrapOrder(so);
                        OrderSync.removeOrderProcess(so.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(so.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, dr.getErrors());
                    }
                } else if (so instanceof ShipScrapOrder) {
                    ShipScrapOrder sso = (ShipScrapOrder) so;
                    ArrayList<BaseResult> result = FleetService.canBeScrapped(sso.getUserId(), sso.getFleetId(), sso.getShipDesignId(), sso.getCount());

                    if (result.size() > 0) {
                        ArrayList<String> errors = new ArrayList<String>();
                        for (BaseResult br : result) {
                            errors.add(br.getMessage());
                        }
                        OrderResult or = new OrderResult(false, errors);
                        th.execute();
                        th.endTransaction();
                        return or;
                    }
                    processScrapOrder(so);
                    OrderSync.removeOrderProcess(so.getUserId());
                } else if (so instanceof StarbaseScrapOrder) {
                    StarbaseScrapOrder sso = (StarbaseScrapOrder) so;
                    if (DefenseUtilities.canBeScrapped(sso.getUnit().getBase(), so.getUserId(), so.getPlanetId(), so.getCount())) {
                        processScrapOrder(so);
                        OrderSync.removeOrderProcess(so.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(so.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, "Scrapable check fail");
                    }
                } else {
                    OrderSync.removeOrderProcess(so.getUserId());
                    th.execute();
                    th.endTransaction();
                    return new OrderResult(false, "Invalid Type");
                }
            } else {
                OrderSync.removeOrderProcess(so.getUserId());
                th.execute();
                th.endTransaction();
                return new OrderResult(false, "Invalid Access");
            }
            
            th.execute();
            th.endTransaction();
            return new OrderResult(true);            
        } catch (TransactionException te) {
            DebugBuffer.writeStackTrace("ScrapOrder failed: ", te);

            try {
                th.rollback();
            } catch (Exception e) {
                log.error("Transaction Rollback failed");
            }

            th.endTransaction();
            return new OrderResult(false, "Transaction Error");
        } finally {
            th.endTransaction();
        }
    }

    public static synchronized OrderResult upgradeOrder(UpgradeOrder uo) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        try {
            th.startTransaction();
            if (OrderSync.addOrderProcess(new OrderSyncEntry(uo.getUserId()))) {

                if (uo instanceof ShipUpgradeOrder) {
                    ShipUpgradeOrder suo = (ShipUpgradeOrder) uo;
                    if (ShipUtilities.canBeUpgraded(suo.getFromUnit().getBase(), suo.getToUnit().getBase(),
                            uo.getUserId(), suo.getFleetId(), uo.getCount())) {
                        processUpgradeOrder(uo);
                        OrderSync.removeOrderProcess(uo.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(uo.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, "Upgradeable check fail");
                    }
                } else if (uo instanceof StarbaseUpgradeOrder) {
                    StarbaseUpgradeOrder suo = (StarbaseUpgradeOrder) uo;
                    if (DefenseUtilities.canBeUpgraded(suo.getFromUnit().getBase(), suo.getToUnit().getBase(),
                            uo.getUserId(), suo.getPlanetId(), uo.getCount())) {
                        processUpgradeOrder(uo);
                        OrderSync.removeOrderProcess(uo.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(uo.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, "Upgradeable check fail");
                    }

                } else if (uo instanceof ConstructionUpgradeOrder) {

                    log.debug("5");
                    ConstructionUpgradeOrder cuo = (ConstructionUpgradeOrder) uo;
                    ConstructionUpgradeResult cur = ConstructionUtilities.canBeUpgraded((Construction) cuo.getFromUnit().getBase(), cuo.getUserId(), cuo.getPlanetId(), cuo.getCount(), (Construction) cuo.getToUnit().getBase());

                    log.debug("6");
                    if (cur.isBuildable()) {
                        log.debug("7");
                        processUpgradeOrder(cuo);
                        OrderSync.removeOrderProcess(cuo.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(cuo.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, cur.getMissingRequirements());
                    }
                } else {
                    OrderSync.removeOrderProcess(uo.getUserId());
                    th.execute();
                    th.endTransaction();
                    return new OrderResult(false, "Invalid Type");
                }
            } else {
                OrderSync.removeOrderProcess(uo.getUserId());
                th.execute();
                th.endTransaction();
                return new OrderResult(false, "Invalid Access");
            }
            
            th.execute();
            th.endTransaction();
            return new OrderResult(true);            
        } catch (TransactionException te) {
            DebugBuffer.writeStackTrace("UpgradeOrder failed: ", te);

            try {
                th.rollback();
            } catch (Exception e) {
                log.error("Transaction Rollback failed");
            }

            th.endTransaction();
            return new OrderResult(false, "Transaction Error");
        } finally {
            th.endTransaction();
        }
    }

    public static synchronized OrderResult repairOrder(RepairOrder ro) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        try {
            th.startTransaction();
            if (OrderSync.addOrderProcess(new OrderSyncEntry(ro.getUserId()))) {
                if (ro instanceof ShipRepairOrder) {
                    // @TODO check if possible
                    ShipRepairOrder sro = (ShipRepairOrder) ro;
                    BaseResult check = ShipUtilities.canBeRepaired(ro.getUserId(), sro.getShipFleetId(), sro.getRepairCount());
                    if (!check.isError()) {
                        processRepairOrder(ro);
                        OrderSync.removeOrderProcess(ro.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(ro.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, "Repairable check fail");
                    }
                } else {
                    OrderSync.removeOrderProcess(ro.getUserId());
                    th.execute();
                    th.endTransaction();
                    return new OrderResult(false, "Invalid Type");
                }
            } else {
                OrderSync.removeOrderProcess(ro.getUserId());
                th.execute();
                th.endTransaction();
                return new OrderResult(false, "Invalid Access");
            }
            
            th.execute();
            th.endTransaction();
            return new OrderResult(true);            
        } catch (TransactionException te) {
            DebugBuffer.writeStackTrace("RepairOrder failed: ", te);

            try {
                th.rollback();
            } catch (Exception e) {
                log.error("Transaction Rollback failed");
            }

            th.endTransaction();
            return new OrderResult(false, "Transaction Error");
        } finally {
            th.endTransaction();
        }
    }

    public static synchronized OrderResult abortOrder(AbortOrder ao) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        try {
            th.startTransaction();
            if (OrderSync.addOrderProcess(new OrderSyncEntry(ao.getUserId()))) {
                AbortableResult ar = null;
                if (ao instanceof ConstructionAbortOrder) {
                    ar = ConstructionUtilities.canBeAborted(ao);
                    if (ar.isAbortable()) {
                        processAbortOrder(ao);
                        OrderSync.removeOrderProcess(ao.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(ao.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, ar.getErrors());
                    }
                } else if (ao instanceof ConstructionScrapAbortOrder) {
                    ar = ConstructionUtilities.canBeAborted(ao);
                    if (ar.isAbortable()) {
                        processAbortOrder(ao);
                        OrderSync.removeOrderProcess(ao.getUserId());
                    } else {
                        OrderSync.removeOrderProcess(ao.getUserId());
                        th.execute();
                        th.endTransaction();
                        return new OrderResult(false, ar.getErrors());
                    }
                } else {
                    OrderSync.removeOrderProcess(ao.getUserId());
                    th.execute();
                    th.endTransaction();
                    return new OrderResult(false, "Invalid Type");
                }
            } else {
                OrderSync.removeOrderProcess(ao.getUserId());
                th.execute();
                th.endTransaction();
                return new OrderResult(false, "Invalid Access");
            }
            
            th.execute();
            th.endTransaction();
            return new OrderResult(true);            
        } catch (TransactionException te) {
            DebugBuffer.writeStackTrace("AbortOrder failed: ", te);

            try {
                th.rollback();
            } catch (Exception e) {
                log.error("Transaction Rollback failed");
            }

            th.endTransaction();
            return new OrderResult(false, "Transaction Error");
        } finally {
            th.endTransaction();
        }
    }

    private static synchronized void processRepairOrder(RepairOrder ro) {
        if (ro instanceof ShipRepairOrder) {
            ShipRepairOrder sro = (ShipRepairOrder) ro;
            ShipFleet sf = sfDAO.getById(sro.getShipFleetId());
            PlayerFleet pf = pfDAO.findById(sf.getFleetId());
            ShipDesignExt sde = new ShipDesignExt(sf.getDesignId());

            // Reducing Ressources
            ShipRepairCost src = new ShipRepairCost(sde);
            RessourcesEntry re = src.getTotalCosts(sro.getRepairCount());

            MutableRessourcesEntry mre = new MutableRessourcesEntry(sro.getUserId(), pf.getPlanetId());
            mre.subtractRess(re, 1d);

            for (RessAmountEntry rae : mre.getRessArray()) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    UserData ud = udDAO.findByUserId(sro.getUserId());
                    ud.setCredits((long) rae.getQty());
                    udDAO.update(ud);
                } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                    // TODO: Help me! I'm empty!!!
                } else {
                    PlanetRessource pr = prDAO.findBy(pf.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                    pr.setQty((long) rae.getQty());
                    prDAO.update(pr);
                }
            }

            // Remove ships
            int shipCount = 0;

            int dockPoints = 0;
            int orbDockPoints = 0;
            int modulePoints = 0;

            for (Map.Entry<EDamageLevel, Integer> entry : sro.getRepairCount().entrySet()) {
                EDamageLevel dmgLvl = entry.getKey();
                shipCount += entry.getValue();

                BuildTime bt = src.getBuildTime(dmgLvl);

                modulePoints += (int) (bt.getModulePoints() * entry.getValue());
                dockPoints += (int) (bt.getDockPoints() * entry.getValue());
                orbDockPoints += (int) (bt.getOrbDockPoints() * entry.getValue());

                DamagedShips ds = dsDAO.getByIdAndDamage(sro.getShipFleetId(), entry.getKey());
                if (ds.getCount().equals(entry.getValue())) {
                    dsDAO.remove(ds);
                } else {
                    ds.setCount(ds.getCount() - entry.getValue());
                    dsDAO.update(ds);
                }
            }

            sfDAO.updateOrDelete(pf.getId(), sf.getDesignId(), shipCount);

            if (sfDAO.findByFleetId(pf.getId()).isEmpty()) {
                pfDAO.remove(pf);
            }

            //Creating Production Order
            for (Map.Entry<EDamageLevel, Integer> entry : sro.getRepairCount().entrySet()) {
                ProductionOrder po = new ProductionOrder();
                po.setPlanetId(pf.getPlanetId());
                po.setDockNeed(dockPoints);
                po.setOrbDockNeed(orbDockPoints);
                po.setModuleNeed(modulePoints);
                po.setPriority(ProductionOrder.PRIORITY_MEDIUM);
                po.setType(EProductionOrderType.REPAIR);
                po.setToId(0);
                po = poDAO.add(po);

                Action a = new Action();
                a.setType(EActionType.SHIP);
                a.setUserId(sro.getUserId());
                a.setNumber(entry.getValue());
                a.setShipDesignId(sf.getDesignId());
                a.setPlanetId(pf.getPlanetId());
                a.setSystemId(pf.getSystemId());
                a.setProductionOrderId(po.getId());
                a.setOnHold(false);
                a = aDAO.add(a);
            }
        }
    }

    private static synchronized void processImproveOrder(ConstructOrder co) {
        if (co instanceof ConstructionImproveOrder) {
            ConstructionImproveOrder cio = (ConstructionImproveOrder) co;

            ConstructionExt ce = new ConstructionExt(((Construction) cio.getUnit().getBase()).getId());
            ConstructionImproveCost cic = new ConstructionImproveCost(ce, cio.getPlanetId());

            MutableRessourcesEntry mre = new MutableRessourcesEntry(cio.getUserId(), cio.getPlanetId());
            mre.subtractRess(new RessourcesEntry(cic.getRess()), co.getCount());
            for (RessAmountEntry rae : mre.getRessArray()) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    UserData ud = udDAO.findByUserId(co.getUserId());
                    ud.setCredits((long) rae.getQty());
                    udDAO.update(ud);
                } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                    // TODO: Help me! I'm empty!!!
                } else {
                    System.out.println("Get ressource " + rae.getRessId() + " for planet " + co.getPlanetId() + " INSTORAGE");
                    PlanetRessource pr = prDAO.findBy(co.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                    pr.setQty((long) rae.getQty());
                    prDAO.update(pr);
                }
            }
            Construction c = (Construction) co.getUnit().getBase();

            ProductionOrder po = new ProductionOrder();
            po.setIndNeed(c.getBuildtime() * 1);
            po.setIndProc(0);
            po.setPlanetId(cio.getPlanetId());
            po.setPriority(ProductionOrder.PRIORITY_MEDIUM);
            po.setType(EProductionOrderType.C_IMPROVE);
            po = poDAO.add(po);

            Action a = new Action();
            a.setType(EActionType.BUILDING);
            a.setUserId(cio.getUserId());
            a.setNumber(1);
            a.setConstructionId(c.getId());
            a.setPlanetId(cio.getPlanetId());
            a.setSystemId(0);
            a.setProductionOrderId(po.getId());
            a.setOnHold(false);
            a = aDAO.add(a);
        }
    }

    private static synchronized void processUpgradeOrder(UpgradeOrder uo) {
        if (uo instanceof ShipUpgradeOrder) { //Getting values
            ShipUpgradeOrder suo = (ShipUpgradeOrder) uo;
            int fleetId = suo.getFleetId();
            PlayerFleet pf = pfDAO.findById(fleetId);
            ShipDesignExt sdeFrom = (ShipDesignExt) suo.getFromUnit();
            ShipDesignExt sdeTo = (ShipDesignExt) suo.getToUnit();
            ShipDesign sdFrom = (ShipDesign) suo.getFromUnit().getBase();
            ShipDesign sdTo = (ShipDesign) suo.getToUnit().getBase();
            ShipUpgradeCost suc = sdeFrom.getShipUpgradeCost(sdeTo, suo.getCount());

            //Reducing Ressources
            MutableRessourcesEntry mre = new MutableRessourcesEntry(suo.getUserId(), pf.getPlanetId());
            for (RessAmountEntry rae : suc.getRessArray()) {
                if (rae.getQty() < 0) {
                    suc.setRess(rae.getRessId(), 0l);
                }
            }
            RessourcesEntry re = new RessourcesEntry(suc.getRess());
            mre.subtractRess(re, 1d);

            for (RessAmountEntry rae : mre.getRessArray()) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    UserData ud = udDAO.findByUserId(suo.getUserId());
                    ud.setCredits((long) rae.getQty());
                    udDAO.update(ud);
                } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                    // TODO: Help me! I'm empty!!!
                } else {
                    PlanetRessource pr = prDAO.findBy(pf.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                    boolean add = false;
                    if (pr == null) {
                        add = true;
                        pr = new PlanetRessource();
                        pr.setPlanetId(pf.getPlanetId());
                        pr.setRessId(rae.getRessId());
                        pr.setType(EPlanetRessourceType.INSTORAGE);
                    }
                    pr.setQty((long) rae.getQty());

                    if (add) {
                        prDAO.add(pr);
                    } else {
                        prDAO.update(pr);
                    }
                }
            }
            sfDAO.updateOrDelete(fleetId, sdFrom.getId(), suo.getCount());

            if (sfDAO.findByFleetId(fleetId).isEmpty()) {
                pfDAO.remove(pf);
            }

            //Creating Production Order
            ProductionOrder po = new ProductionOrder();
            po.setPlanetId(pf.getPlanetId());
            po.setDockNeed(suc.getBuildTime().getDockPoints());
            po.setOrbDockNeed(suc.getBuildTime().getOrbDockPoints());
            po.setModuleNeed(suc.getBuildTime().getModulePoints());
            po.setPriority(ProductionOrder.PRIORITY_MEDIUM);
            po.setType(EProductionOrderType.UPGRADE);
            po.setToId(sdTo.getId());
            po = poDAO.add(po);

            Action a = new Action();
            a.setType(EActionType.SHIP);
            a.setUserId(suo.getUserId());
            a.setNumber(suo.getCount());
            a.setShipDesignId(sdFrom.getId());
            a.setPlanetId(pf.getPlanetId());
            a.setSystemId(pf.getSystemId());
            a.setProductionOrderId(po.getId());
            a.setOnHold(false);
            a = aDAO.add(a);
        } else if (uo instanceof StarbaseUpgradeOrder) {
            StarbaseUpgradeOrder suo = (StarbaseUpgradeOrder) uo;
            int planetId = suo.getPlanetId();
            int fromDesignId = ((ShipDesign) suo.getFromUnit().getBase()).getId();
            int toDesignId = ((ShipDesign) suo.getToUnit().getBase()).getId();
            int count = suo.getCount();

            PlanetDefense pd = pdDAO.findBy(planetId, fromDesignId, uo.getUserId(), EDefenseType.SPACESTATION);

            if (pd.getCount() <= count) {
                pdDAO.remove(pd);
            } else {
                pd.setCount(pd.getCount() - count);
                pdDAO.update(pd);
            }

            ShipDesignExt sdeFrom = (ShipDesignExt) suo.getFromUnit();
            ShipDesignExt sdeTo = (ShipDesignExt) suo.getToUnit();
            ShipDesign sdFrom = (ShipDesign) suo.getFromUnit().getBase();
            ShipDesign sdTo = (ShipDesign) suo.getToUnit().getBase();
            ShipUpgradeCost suc = sdeFrom.getShipUpgradeCost(sdeTo, suo.getCount());

            //Reducing Ressources
            MutableRessourcesEntry mre = new MutableRessourcesEntry(suo.getUserId(), planetId);
            for (RessAmountEntry rae : suc.getRessArray()) {
                if (rae.getQty() < 0) {
                    suc.setRess(rae.getRessId(), 0l);
                }
            }
            RessourcesEntry re = new RessourcesEntry(suc.getRess());
            mre.subtractRess(re, 1d);

            for (RessAmountEntry rae : mre.getRessArray()) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    UserData ud = udDAO.findByUserId(suo.getUserId());
                    ud.setCredits((long) rae.getQty());
                    udDAO.update(ud);
                } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                    // TODO: Help me! I'm empty!!!
                } else {
                    PlanetRessource pr = prDAO.findBy(suo.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                    boolean add = false;
                    if (pr == null) {
                        add = true;
                        pr = new PlanetRessource();
                        pr.setPlanetId(suo.getPlanetId());
                        pr.setRessId(rae.getRessId());
                        pr.setType(EPlanetRessourceType.INSTORAGE);
                    }
                    pr.setQty((long) rae.getQty());

                    if (add) {
                        prDAO.add(pr);
                    } else {
                        prDAO.update(pr);
                    }
                }
            }

            //Creating Production Order
            ProductionOrder po = new ProductionOrder();
            po.setPlanetId(suo.getPlanetId());
            po.setDockNeed(suc.getBuildTime().getDockPoints());
            po.setOrbDockNeed(suc.getBuildTime().getOrbDockPoints());
            po.setModuleNeed(suc.getBuildTime().getModulePoints());
            po.setPriority(ProductionOrder.PRIORITY_MEDIUM);
            po.setType(EProductionOrderType.UPGRADE);
            po.setToId(sdTo.getId());
            po = poDAO.add(po);

            Action a = new Action();
            a.setType(EActionType.SPACESTATION);
            a.setUserId(suo.getUserId());
            a.setNumber(suo.getCount());
            a.setShipDesignId(sdFrom.getId());
            a.setPlanetId(suo.getPlanetId());
            a.setProductionOrderId(po.getId());
            a.setOnHold(false);
            a = aDAO.add(a);
            log.debug("Triggered StarbaseUpgradeOrder");
        } else if (uo instanceof ConstructionUpgradeOrder) {

            log.debug("8");
            ConstructionUpgradeOrder cuo = (ConstructionUpgradeOrder) uo;
            ConstructionExt ce = new ConstructionExt(((Construction) cuo.getFromUnit().getBase()).getId());
            ConstructionExt ceNew = new ConstructionExt(((Construction) cuo.getToUnit().getBase()).getId());
            ConstructionUpgradeCost cur = new ConstructionUpgradeCost(ce, ceNew, cuo.getPlanetId(), cuo.getUserId());

            log.debug("9");
            MutableRessourcesEntry mre = new MutableRessourcesEntry(cuo.getUserId(), cuo.getPlanetId());
            mre.subtractRess(new RessourcesEntry(cur.getRess()), cuo.getCount());
            for (RessAmountEntry rae : mre.getRessArray()) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    UserData ud = udDAO.findByUserId(cuo.getUserId());
                    ud.setCredits((long) rae.getQty());
                    udDAO.update(ud);
                } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                    // TODO: Help me! I'm empty!!!
                } else {
                    PlanetRessource pr = prDAO.findBy(cuo.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                    boolean add = false;
                    if (pr == null) {
                        add = true;
                        pr = new PlanetRessource();
                        pr.setPlanetId(cuo.getPlanetId());
                        pr.setRessId(rae.getRessId());
                        pr.setType(EPlanetRessourceType.INSTORAGE);
                    }
                    pr.setQty((long) rae.getQty());

                    if (add) {
                        prDAO.add(pr);
                    } else {
                        prDAO.update(pr);
                    }
                }
            }
            log.debug("10");
            Construction c = (Construction) cuo.getFromUnit().getBase();
            Construction cNew = (Construction) cuo.getToUnit().getBase();

            if (c.getType() == EConstructionType.PLANETARY_DEFENSE) { // Planetary stationary defense
                //TODO
            } else { //Normal Building
                log.debug("11");
                PlanetConstruction pc = pcDAO.findBy(cuo.getPlanetId(), c.getId());
                if (pc.getNumber() == cuo.getCount()) {
                    // INCORPORATE INTERFACE DELETING

                    pcDAO.remove(pc);
                } else if (pc.getNumber() < cuo.getCount()) {
                    DebugBuffer.addLine(DebugLevel.ERROR, "User wollte mehr Geb�ude upgraden als m�glich");
                } else if (pc.getNumber() > cuo.getCount()) {
                    pc.setNumber(pc.getNumber() - cuo.getCount());
                    pcDAO.update(pc);
                }

                ProductionOrder po = new ProductionOrder();
                po.setIndNeed(cur.getBuildTime().getIndPoints() * cuo.getCount());
                po.setIndProc(0);
                po.setPlanetId(cuo.getPlanetId());
                po.setPriority(ProductionOrder.PRIORITY_MEDIUM);
                po.setType(EProductionOrderType.C_UPGRADE);
                po = poDAO.add(po);

                Action a = new Action();
                a.setType(EActionType.BUILDING);
                a.setUserId(cuo.getUserId());
                a.setNumber(cuo.getCount());
                a.setConstructionId(c.getId());
                a.setPlanetId(cuo.getPlanetId());
                a.setSystemId(0);
                a.setProductionOrderId(po.getId());
                a.setOnHold(false);
                a = aDAO.add(a);
            }

            log.debug("TODO: Place construction upgrade order");
        }
    }

    private static synchronized void processAbortOrder(AbortOrder ao) {
        if (ao instanceof ConstructionAbortOrder) {
            ConstructionAbortOrder cao = (ConstructionAbortOrder) ao;
            Construction c = (Construction) cao.getUnit().getBase();

            if (false) { // Planetary stationary defense -- THIS IS ACTUALLY NOT NEEDED!!!!
                //PlanetDefense pd = pdDAO.findBy(ao.getPlanetId(), c.getId(), EDefenseType.TURRET);
                //pd.setCount(pd.getCount() - ao.getCount());
                // pdDAO.update(pd);
            } else {//Normal Building
                Action a = aDAO.findByTimeFinished(cao.getPo().getId(), EActionType.BUILDING);
                ProductionOrder po = cao.getPo();

                // 6 = 10 - 4
                int number = a.getNumber() - cao.getCount();
                if (number < 0) {
                    number = 0;
                }

                //@ TODO Ressource Pay back
                BuildingAbortRessourceCost barc = OverviewService.findForAbortion(po.getId(), cao.getUserId());
                PlanetCalculation pc = null;
                try {
                    pc = new PlanetCalculation(cao.getPlanetId());
                } catch (Exception e) {
                    DebugBuffer.addLine(DebugLevel.ERROR, "Error in PlanetCalculation in Order Handeling : " + e);
                }
                PlanetCalcResult pcr = pc.getPlanetCalcData();
                for (RessAmountEntry rae : barc.getRessCost().getRessArray()) {
                    if (rae.getQty() == 0) {
                        continue;
                    }
                    if (rae.getRessId() == Ressource.CREDITS) {
                        UserData ud = udDAO.findByUserId(cao.getUserId());
                        ud.setCredits(ud.getCredits() + (long) rae.getQty() * cao.getCount());
                        udDAO.update(ud);
                    } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                        UserData ud = udDAO.findByUserId(cao.getUserId());
                        ud.setDevelopementPoints(ud.getDevelopementPoints() + rae.getQty() * cao.getCount());
                        udDAO.update(ud);
                    } else {
                        PlanetRessource press = prDAO.findBy(cao.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);

                        boolean add = false;
                        if (press == null) {
                            add = true;
                            press = new PlanetRessource();
                            press.setPlanetId(cao.getPlanetId());
                            press.setRessId(rae.getRessId());
                            press.setQty(0l);
                            press.setType(EPlanetRessourceType.INSTORAGE);
                        }

                        long qty = press.getQty() + (long) rae.getQty() * cao.getCount();
                        if (qty > pcr.getProductionData().getRessMaxStock(rae.getRessId())) {
                            press.setQty(pcr.getProductionData().getRessMaxStock(rae.getRessId()));
                        } else {
                            press.setQty(qty);
                        }

                        if (add) {
                            prDAO.add(press);
                        } else {
                            prDAO.update(press);
                        }
                    }
                }
                //Delete
                if (number == 0) {
                    aDAO.remove(a);
                    poDAO.remove(po);

                    //reduce
                } else {
                    a.setNumber(number);
                    aDAO.update(a);

                    int newIndNeed = ((Construction) cao.getUnit().getBase()).getBuildtime() * number;
                    int indProc = po.getIndProc();
                    if (indProc > newIndNeed) {
                        po.setIndProc(newIndNeed);
                    }
                    po.setIndNeed(newIndNeed);
                    poDAO.update(po);
                }
            }
        } else if (ao instanceof ConstructionScrapAbortOrder) {
            // Remove the current Deconstruction Order and create a construction order if necessary
            ConstructionScrapAbortOrder csao = (ConstructionScrapAbortOrder) ao;

            ConstructionExt ce = (ConstructionExt) csao.getUnit();
            ConstructionScrapAbortCost csac = ce.getScrapAbortCost(csao.getPo().getId());

            ProductionOrder po = csao.getPo();
            Action a = aDAO.findByTimeFinished(po.getId(), EActionType.DECONSTRUCT);
            double done = po.getIndPerc();

            // TransactionHandler th = TransactionHandler.getTransactionHandler();
            // try {
            //     th.startTransaction();
            if (done <= 10) {
                PlanetConstruction pc = pcDAO.findBy(csao.getPlanetId(), ((Construction) ce.getBase()).getId());
                if (pc == null) {
                    pc = new PlanetConstruction();
                    pc.setPlanetId(csao.getPlanetId());
                    pc.setNumber(csao.getCount());
                    pc.setConstructionId(((Construction) ce.getBase()).getId());
                    pcDAO.add(pc);
                } else {
                    pc.setNumber(pc.getNumber() + csao.getCount());
                    pcDAO.update(pc);
                }

                UserData ud = udDAO.findByUserId(csao.getUserId());
                ud.setCredits(ud.getCredits() + (long) -csac.getRess(Ressource.CREDITS) * csao.getCount());
                udDAO.update(ud);
            } else {
                UserData ud = udDAO.findByUserId(csao.getUserId());
                for (RessAmountEntry rae : csac.getRessArray()) {
                    if (rae.getRessId() == Ressource.CREDITS) {
                        ud.setCredits(ud.getCredits() + (long) -csac.getRess(Ressource.CREDITS) * csao.getCount());
                    } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                        // TODO: Help me! I'm empty!!!
                    } else {
                        PlanetRessource pr = prDAO.findBy(csao.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                        pr.setQty((long) pr.getQty() - (long) (rae.getQty() * csao.getCount()));
                        prDAO.update(pr);
                    }
                }

                // Now we have to create a construction order due to current progress
                double constructionProgress = 100d / 90d * (100d - done);

                Construction c = (Construction) csao.getUnit().getBase();

                ProductionOrder poNew = new ProductionOrder();
                poNew.setIndNeed(c.getBuildtime() * csao.getCount());
                poNew.setIndProc(0);
                poNew.setPlanetId(csao.getPlanetId());
                poNew.setPriority(ProductionOrder.PRIORITY_MEDIUM);
                poNew.setType(EProductionOrderType.C_BUILD);
                poNew = poDAO.add(poNew);

                Action aNew = new Action();
                aNew.setType(EActionType.BUILDING);
                aNew.setUserId(csao.getUserId());
                aNew.setNumber(csao.getCount());
                aNew.setConstructionId(c.getId());
                aNew.setPlanetId(csao.getPlanetId());
                aNew.setSystemId(0);
                aNew.setProductionOrderId(poNew.getId());
                aNew.setOnHold(false);
                aDAO.add(aNew);
            }

            if (a.getNumber().equals(csao.getCount())) {
                poDAO.remove(po);
                aDAO.remove(a);
            } else {
                // Scale Industry demand down on deconstruction order
                po.setIndNeed(po.getIndNeed() / a.getNumber() * (a.getNumber() - csao.getCount()));
                po.setIndProc(po.getIndNeed() / a.getNumber() * (a.getNumber() - csao.getCount()));

                // Reduce Count in action
                a.setNumber(a.getNumber() - csao.getCount());
                aDAO.update(a);
            }

            /*
             th.execute();
             th.endTransaction();
             } catch (TransactionException te) {
             DebugBuffer.writeStackTrace("Abort Scrap Order failed: ", te);

             try {
             th.rollback();
             } catch (Exception e) {
             log.error( "Transaction Rollback failed");
             }
             } finally {
             th.endTransaction();
             }
             */
        }
    }

    private static synchronized void processScrapOrder(ScrapOrder so) {
        UserData ud = udDAO.findByUserId(so.getUserId());

        if (so instanceof ConstructionScrapOrder) {
            // BuildingDeconstructionRessourceCost bdrc = OverviewService.findForDeconstruction(((Construction) so.getUnit().getBase()).getId(), so.getPlanetId(), so.getUserId());
            ConstructionExt ce = new ConstructionExt(((Construction) so.getUnit().getBase()).getId());
            ConstructionScrapCost csc = ce.getScrapCost(so.getPlanetId());

            MutableRessourcesEntry mre = new MutableRessourcesEntry(so.getUserId(), so.getPlanetId());
            mre.subtractRess(csc, so.getCount());

            for (RessAmountEntry rae : mre.getRessArray()) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    ud.setCredits((long) rae.getQty());
                    udDAO.update(ud);
                }
            }
            Construction c = (Construction) so.getUnit().getBase();

            if (c.getType() == EConstructionType.PLANETARY_DEFENSE) { // Planetary stationary defense
                PlanetDefense pd = pdDAO.findBy(so.getPlanetId(), c.getId(), EDefenseType.TURRET);
                if (pd.getCount() == so.getCount()) {
                    pdDAO.remove(pd);
                } else if (pd.getCount() < so.getCount()) {
                    DebugBuffer.addLine(DebugLevel.ERROR, "User wollte mehr Planetenverteidigung abrei�en als m�glich");
                } else if (pd.getCount() > so.getCount()) {
                    pd.setCount(pd.getCount() - so.getCount());
                    pdDAO.update(pd);
                }
            } else { //Normal Building
                PlanetConstruction pc = pcDAO.findBy(so.getPlanetId(), c.getId());
                if (pc.getNumber() == so.getCount()) {
                    pcDAO.remove(pc);
                } else if (pc.getNumber() < so.getCount()) {
                    DebugBuffer.addLine(DebugLevel.ERROR, "User wollte mehr Geb�ude abrei�en als m�glich");
                } else if (pc.getNumber() > so.getCount()) {
                    pc.setNumber(pc.getNumber() - so.getCount());
                    if (pc.getIdle() >= pc.getNumber()) {
                        pc.setIdle(pc.getNumber());
                    }
                    pcDAO.update(pc);
                }
            }

            try {
                ConstructionRestriction cr = null;
                if (c != null) {
                    cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, c.getId());
                }

                if (cr != null) {
                    Class rc = Class.forName("at.darkdestiny.core.construction.restriction." + cr.getRestrictionClass());
                    ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                    cir.onDestruction(ppDAO.findByPlanetId(so.getPlanetId()));
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error on Construction Restriction: ", e);
            }

            try {
                PlanetCalculation pc = new PlanetCalculation(so.getPlanetId());
                ProductionResult pr = pc.getPlanetCalcData().getProductionData();

                for (Ressource r : rDAO.findAllStoreableRessources()) {
                    long maxStock = pr.getRessMaxStock(r.getId());

                    PlanetRessource pRess = prDAO.findBy(so.getPlanetId(), r.getId(), EPlanetRessourceType.INSTORAGE);
                    if ((pRess != null) && (pRess.getQty() > maxStock)) {
                        pRess.setQty(maxStock);
                        prDAO.update(pRess);
                    }
                }
            } catch (Exception e) {
            }

            ProductionOrder po = new ProductionOrder();
            po.setIndNeed((int) Math.round(c.getBuildtime() * so.getCount() * SetupValues.BUILDTIME_MULTI));
            po.setPlanetId(so.getPlanetId());
            po.setPriority(ProductionOrder.PRIORITY_LOW);
            po.setType(EProductionOrderType.C_SCRAP);
            po = poDAO.add(po);

            Action a = new Action();
            a.setType(EActionType.DECONSTRUCT);
            a.setUserId(so.getUserId());
            a.setNumber(so.getCount());
            a.setConstructionId(c.getId());
            a.setPlanetId(so.getPlanetId());
            a.setSystemId(0);
            a.setProductionOrderId(po.getId());
            a.setOnHold(false);
            a = aDAO.add(a);

        } else if (so instanceof ShipScrapOrder) {
            ShipScrapOrder sso = (ShipScrapOrder) so;
            ShipDesign sd = (ShipDesign) so.getUnit().getBase();
            ShipDesignExt sde = new ShipDesignExt(sd.getId());
            sfDAO.updateOrDelete(sso.getFleetId(), sd.getId(), so.getCount());
            PlayerFleet pf = pfDAO.findById(sso.getFleetId());
            if (sfDAO.findByFleetId(sso.getFleetId()).isEmpty()) {
                pfDAO.remove(pf);
            }

            ShipScrapCost ssc = sde.getShipScrapCost(sso.getCount());
            ssc.getRess(Ressource.CREDITS);
            ud.setCredits(ud.getCredits() - (long) ssc.getRess(Ressource.CREDITS));
            udDAO.update(ud);

            ProductionOrder po = new ProductionOrder();
            sde.getBuildTime().setModulePoints(0);
            sde.getBuildTime().setKasPoints(0);
            po.setShipBuildTime(sde.getBuildTime());
            po.setPlanetId(pf.getPlanetId());
            po.setPriority(ProductionOrder.PRIORITY_LOW);
            po.setType(EProductionOrderType.SCRAP);
            po = poDAO.add(po);

            Action a = new Action();
            a.setType(EActionType.SHIP);
            a.setUserId(so.getUserId());
            a.setNumber(so.getCount());
            a.setShipDesignId(sd.getId());
            a.setPlanetId(pf.getPlanetId());
            a.setSystemId(0);
            a.setProductionOrderId(po.getId());
            a.setOnHold(false);
            a = aDAO.add(a);
        } else if (so instanceof StarbaseScrapOrder) {
            // @TODO
            log.debug("Triggered StarbaseScrapOrder");
        }
    }

    private static synchronized void processConstructOrder(ConstructOrder co) {
        MutableRessourcesEntry mre = new MutableRessourcesEntry(co.getUserId(), co.getPlanetId());
        mre.subtractRess(co.getUnit().getRessCost(), co.getCount());
        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getRessId() == Ressource.CREDITS) {
                UserData ud = udDAO.findByUserId(co.getUserId());
                ud.setCredits((long) rae.getQty());
                udDAO.update(ud);
            } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                UserData ud = udDAO.findByUserId(co.getUserId());
                ud.setDevelopementPoints(rae.getQty());
                udDAO.update(ud);
            } else {
                PlanetRessource pr = prDAO.findBy(co.getPlanetId(), rae.getRessId(), EPlanetRessourceType.INSTORAGE);
                pr.setQty((long) rae.getQty());
                prDAO.update(pr);
            }
        }

        if (co instanceof ConstructionConstructOrder) {
            Construction c = (Construction) co.getUnit().getBase();

            ProductionOrder po = new ProductionOrder();
            po.setIndNeed(c.getBuildtime() * co.getCount());
            po.setIndProc(0);
            po.setPlanetId(co.getPlanetId());
            if ((co.getPriority() >= 0) && (co.getPriority() <= 10)) {
                po.setPriority(co.getPriority());
            } else {
                po.setPriority(ProductionOrder.PRIORITY_MEDIUM);
            }
            po.setType(EProductionOrderType.C_BUILD);
            po = poDAO.add(po);

            Action a = new Action();
            a.setType(EActionType.BUILDING);
            a.setUserId(co.getUserId());
            a.setNumber(co.getCount());
            a.setConstructionId(c.getId());
            a.setPlanetId(co.getPlanetId());
            a.setSystemId(0);
            a.setProductionOrderId(po.getId());
            a.setOnHold(false);
            a = aDAO.add(a);
        } else if (co instanceof ShipConstructOrder) {
            ShipDesign sd = (ShipDesign) co.getUnit().getBase();
            ShipDesignExt sde = new ShipDesignExt(sd.getId());

            ProductionOrder po = new ProductionOrder();
            po.setPlanetId(co.getPlanetId());
            po.setDockNeed(sde.getBuildTime().getDockPoints() * co.getCount());
            po.setOrbDockNeed(sde.getBuildTime().getOrbDockPoints() * co.getCount());
            po.setModuleNeed(sde.getBuildTime().getModulePoints() * co.getCount());
            po.setPriority(ProductionOrder.PRIORITY_MEDIUM);
            po.setType(EProductionOrderType.PRODUCE);
            po = poDAO.add(po);

            Action a = new Action();

            if (sd.getType() != EShipType.SPACEBASE) {
                a.setType(EActionType.SHIP);
            } else {
                a.setType(EActionType.SPACESTATION);
            }

            a.setUserId(co.getUserId());
            a.setNumber(co.getCount());
            a.setShipDesignId(sd.getId());
            a.setPlanetId(co.getPlanetId());
            a.setSystemId(0);
            a.setProductionOrderId(po.getId());
            a.setOnHold(false);
            a = aDAO.add(a);
        } else if (co instanceof GroundTroopConstructOrder) {
            GroundTroop gt = (GroundTroop) co.getUnit().getBase();

            ProductionOrder po = new ProductionOrder();

            po.setPlanetId(co.getPlanetId());
            po.setPriority(ProductionOrder.PRIORITY_MEDIUM);
            po.setModuleNeed(gt.getModulPoints() * co.getCount());
            po.setKasNeed(gt.getCrew() * co.getCount());
            po.setType(EProductionOrderType.GROUNDTROOPS);
            po = poDAO.add(po);

            Action a = new Action();
            a.setType(EActionType.GROUNDTROOPS);
            a.setUserId(co.getUserId());
            a.setNumber(co.getCount());
            a.setGroundTroopId(gt.getId());
            a.setPlanetId(co.getPlanetId());
            a.setSystemId(0);
            a.setProductionOrderId(po.getId());
            a.setOnHold(false);
            a = aDAO.add(a);

            //Additionally decrese population
            PlayerPlanet pp = ppDAO.findByPlanetId(co.getPlanetId());
            pp.setPopulation(pp.getPopulation() - gt.getCrew() * co.getCount());
            ppDAO.update(pp);
        }
    }
}
