/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.WeaponModule;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface IBattleShip extends ICombatUnit {
    public void reloadShieldsAndWeapons();
    
    public boolean isDefenseStation();
    public boolean isPlanetaryDefense();
    public boolean hasTargetFound();
    
    public ArrayList<WeaponModule> getWeapons();
    public ShieldDefinition getShields();
    public int getArmor();
    // public DamagedShips getDs();
    
    public void setParentCombatGroupId(int cgId);
    
    public void setSc_LoggingEntry(SC_DataEntry scde);
    
    public void setShipFleetId(int shipFleetId);
    public ShipDesign getShipDesign();
}
