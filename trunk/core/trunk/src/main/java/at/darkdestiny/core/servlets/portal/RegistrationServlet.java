/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets.portal;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.EUserGender;
import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.model.LoginTracker;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.RegisterResult;
import at.darkdestiny.core.service.LoginService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.MD5;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.transaction.TransactionHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Stefan
 */
public class RegistrationServlet extends HttpServlet {

    public enum RegistrationValue {

        PORTAL_VERSION, NAME_USERNAME, NAME_GAMENAME, NAME_REGISTRATIONTOKEN, NAME_PASS1, NAME_PASS2, NAME_LOCALE, NAME_GENDER, NAME_EMAIL, NAME_SOURCE_URL, NAME_USAGE_AGREEMENT, VALUE_USAGE_AGREEMENT
    }
    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        TransactionHandler th = TransactionHandler.getTransactionHandler();

        String portalVersion = null;
        String registrationUrl = "";
        
        try {           
            portalVersion = request.getParameter("PORTALVERSION");
            String userName = request.getParameter(RegistrationValue.NAME_USERNAME.toString());
            String gameName = request.getParameter(RegistrationValue.NAME_GAMENAME.toString());
            String email = request.getParameter(RegistrationValue.NAME_EMAIL.toString());
            String registrationToken = request.getParameter(RegistrationValue.NAME_REGISTRATIONTOKEN.toString());
            String gender = request.getParameter(RegistrationValue.NAME_GENDER.toString());
            String language = request.getParameter(RegistrationValue.NAME_LOCALE.toString());
            Language l = Service.languageDAO.findById(Integer.parseInt(language));

            String usageAgreement = request.getParameter(RegistrationValue.VALUE_USAGE_AGREEMENT.toString());

            for(Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()){
                System.out.println(entry.getKey() + " => " + entry.getValue()[0]);
            }

            if (request.getParameter(RegistrationValue.NAME_SOURCE_URL.toString()) != null) {
                registrationUrl = request.getParameter(RegistrationValue.NAME_SOURCE_URL.toString());
            }

            if(usageAgreement == null){
                String errors = "Error on login!;";
                errors += ML.getMLStr("loginform_err_usageagreement", l.getLocale());
                registrationUrl += "?errors=" + URLEncoder.encode(errors, "UTF-8") + "&rt=" + registrationToken;
                System.out.println("Trying to redirect to : " + registrationUrl);
                
                if (portalVersion != null) {
                    sentNewPortalRegistrationResult(registrationUrl,errors,registrationToken);
                } else {
                    response.sendRedirect(registrationUrl);
                }
                return;
            }


            String password1 = request.getParameter(RegistrationValue.NAME_PASS1.toString());
            String password2 = request.getParameter(RegistrationValue.NAME_PASS2.toString());
            if (!password1.equals(password2)) {
                System.out.println("Error on login, passwords not the same");
                String errors = "Error on login!;";
                errors += ML.getMLStr("loginform_err_passwordsnotsame", l.getLocale());
                registrationUrl += "?errors=" + URLEncoder.encode(errors, "UTF-8") + "&rt=" + registrationToken;
                System.out.println("Trying to redirect to : " + registrationUrl);

                if (portalVersion != null) {
                    sentNewPortalRegistrationResult(registrationUrl,errors,registrationToken);
                } else {                
                    response.sendRedirect(registrationUrl);
                }
                return;
            }

            String orgPassword = password1;

            RegisterResult rr = LoginService.checkUserForCreation(request.getRemoteAddr(), userName, gameName, email, password1, GameUtilities.getCurrentTick2(), request.getLocale(), true);

            User user = new User();

            String portalPassword = RandomStringUtils.randomAlphabetic(20);
            if (rr.isAbleToRegister()) {
                user.setUserName(userName);
                user.setGender(EUserGender.valueOf(gender));
                user.setPassword(MD5.encryptPassword(password1));
                user.setGameName(gameName);
                user.setLastUpdate(GameUtilities.getCurrentTick2());
                user.setIp(request.getRemoteAddr());
                user.setEmail(email.trim());
                user.setSystemAssigned(false);
                user.setActive(true);
                user.setAdmin(false);
                user.setTrial(true);
                user.setBetaAdmin(false);
                user.setLocked(0);
                user.setPortalPassword(portalPassword);
                user.setActivationCode("");
                user.setDeleteDate(0);
                user.setMulti(false);
                user.setRegistrationToken(registrationToken);
                user.setAcceptedUserAgreement(true);
                user.setLocale(l.getLanguage() + "_" + l.getCountry());
                user.setJoinDate(java.lang.System.currentTimeMillis());

                user = LoginService.saveUser(user);

                LoginTracker loginTracker = new LoginTracker();
                loginTracker.setUserId(user.getUserId());
                loginTracker.setTime(java.lang.System.currentTimeMillis());
                loginTracker.setLoginIP(request.getRemoteAddr());

                LoginService.saveLoginTracker(loginTracker);

                request.getSession().invalidate();
                
                if (portalVersion != null) {
                    sentNewPortalRegistrationResult(registrationUrl,GameConfig.getInstance().getHostURL() + "/enter.jsp?user=" + userName + "&password=" + orgPassword,registrationToken);
                } else {                             
                    response.sendRedirect(GameConfig.getInstance().getHostURL() + "/enter.jsp?user=" + userName + "&password=" + orgPassword);
                }
                // response.sendRedirect(registrationUrl);
            } else {
                System.out.println("Error on login");
                String errors = "Error on login!";
                for (BaseResult br : rr.getResults()) {
                    System.out.println(br.getMessage());
                    errors += ";" + br.getMessage();

                }
                registrationUrl += "?errors=" + URLEncoder.encode(errors, "UTF-8") + "&rt=" + registrationToken;
                System.out.println("Trying to redirect to : " + registrationUrl);

                if (portalVersion != null) {
                    sentNewPortalRegistrationResult(registrationUrl,errors,registrationToken);
                } else {                
                    response.sendRedirect(registrationUrl);
                }
            }
        } catch (Exception e) {
            th.rollback();
            System.out.println("error : " + e);
            e.printStackTrace();
        } finally {
            out.close();
            th.endTransaction();

        }
    }
    
    private boolean sentNewPortalRegistrationResult(String server, String error, String registrationToken) {
        try {
            String data = URLEncoder.encode("errors", "UTF-8") + "=" + URLEncoder.encode(error, "UTF-8");
            data += "&" + URLEncoder.encode("rt", "UTF-8") + "=" + URLEncoder.encode(registrationToken, "UTF-8");
       
            URL url = new URL(server);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
              System.out.println(line);
            }
            wr.close();
            rd.close();   
        } catch (UnsupportedEncodingException uee) {
            System.out.println("sendNewPortalRegistrationResult: " + uee.getMessage());
            return false;            
        } catch (MalformedURLException mue) {
            System.out.println("sendNewPortalRegistrationResult: " + mue.getMessage());
            return false;
        } catch (IOException ioe) {
            System.out.println("sendNewPortalRegistrationResult: " + ioe.getMessage());
            return false;
        }
        
        return true;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
