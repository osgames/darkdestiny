/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

 import at.darkdestiny.core.WeaponModule;import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.spacecombat.helper.ShieldDefenseDetail;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import at.darkdestiny.core.spacecombat.helper.TargetListEntry;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.WeaponModule;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.spacecombat.helper.ShieldDefenseDetail;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import at.darkdestiny.core.spacecombat.helper.TargetListEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public abstract class AbstractCombatUnit implements ICombatUnit {
    private static final Logger log = LoggerFactory.getLogger(AbstractCombatUnit.class);

    protected static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);

    private final int id;
    protected final int orgCount;
    private final int shipFleetId;
    private final CombatGroupFleet.UnitTypeEnum type;
    private IBattleShip bs;
    protected int[] damageDistribution = new int[5];
    protected float averageWeaponRange;

    protected int designId;
    protected String name;
    protected int count;
    protected float hp;
    protected int chassisSize;
    protected int primaryTarget;
    protected int armorType = 0;
    protected float totalHp = 0;
    protected int restCount;
    protected int assignedBattleGroup = -1;
    private CombatGroupFleet parentFleet;

    protected float targetFireBonus = 1f;
    protected float targetFireDefenseBonus = 1f;
    protected float damageBonus = 1f;
    protected float damageDefenseBonus = 1f;

    protected SC_DataEntry sc_LoggingEntry = null;

    protected HashMap<Integer, Double> steelAbsorb = new HashMap<Integer, Double>();
    protected HashMap<Integer, Double> terkAbsorb = new HashMap<Integer, Double>();
    protected HashMap<Integer, Double> ynkeAbsorb = new HashMap<Integer, Double>();
    protected HashMap<Integer, Double> paraPenetration = new HashMap<Integer, Double>();
    protected HashMap<Integer, Double> huPenetration = new HashMap<Integer, Double>();
    protected HashMap<Integer, Double> prallPenetration = new HashMap<Integer, Double>();

    protected ArrayList<WeaponModule> weapons = new ArrayList<WeaponModule>();
    private ShieldDefinition shields;
    private ShieldDefinition planetaryShields;

    private double tempDestroyedStart = 0d;
    protected double tempDestroyed = 0d;

    protected double distraction = 0d;

    public AbstractCombatUnit() {
        this.id = 0;
        this.shipFleetId = 0;
        this.orgCount = 0;
        this.type = null;
    }

    public AbstractCombatUnit(int designId, int shipFleetId, int count) {
        this.id = designId;
        this.shipFleetId = shipFleetId;
        this.orgCount = count;
        this.type = null;
    }

    public AbstractCombatUnit(int designId, int count, CombatGroupFleet.UnitTypeEnum ute) {
       this.id = designId;
       this.orgCount = count;
       this.shipFleetId = 0;
       this.type = ute;
    }

    public AbstractCombatUnit(int designId, int shipFleetId, int count, CombatGroupFleet.UnitTypeEnum ute) {
       this.id = designId;
       this.orgCount = count;
       this.shipFleetId = shipFleetId;
       this.type = ute;
    }

    protected abstract double getReductionThroughShieldsNew(AbstractCombatUnit abs, AbstractCombatUnit absAtt, WeaponModule wm, int maxHitable, HashMap<Integer, Double> detailDamageAtt, ShieldDefinition shield, ShieldDefenseDetail sdd);

    protected HashMap<TargetListEntry, HashMap<Integer, Integer>> getWeaponsOnTarget(ArrayList<TargetListEntry> targetLists, double totalValue) {
        // =============================== START =============================
        // Store all damage Shares and apply available weapons onto the target
        // Can be put into a subfunction as soon as tested properly
        // ===================================================================
        HashMap<TargetListEntry, HashMap<Integer, Integer>> fireWeaponOnTargetCount = new HashMap<TargetListEntry, HashMap<Integer, Integer>>();
        TreeMap<Double, TargetListEntry> targetByProbability = new TreeMap<Double, TargetListEntry>();

        for (TargetListEntry tle : targetLists) {
            double currValue = 1d / totalValue * tle.getRelativeAttackProbability();
            double add = 0d;

            if (!targetByProbability.containsKey(currValue) || (currValue == 0d)) {
                targetByProbability.put(currValue, tle);
            } else {
                currValue += 0.00001d;
                while (targetByProbability.containsKey(currValue)) {
                    currValue += 0.00001d;
                }
                targetByProbability.put(currValue, tle);
            }
        }

        // Now we have all targets ordered by probability to be shot
        // Lets get the number of weapons for each module which will shoot this target
        for (WeaponModule wm : getWeapons()) {
            int weaponLeftCount = 0;
            int orgWeaponCount = 0;

            weaponLeftCount = wm.getCount() * this.getCount();
            orgWeaponCount = weaponLeftCount;

            for (Double value : targetByProbability.descendingKeySet()) {
                if (weaponLeftCount == 0) break;

                int weaponsOnCurrentTarget = (int) Math.ceil((double) orgWeaponCount * value);
                weaponsOnCurrentTarget = Math.min(weaponLeftCount, Math.min(weaponsOnCurrentTarget, orgWeaponCount));

                // Assign weapon number for current module on this targetList
                HashMap<Integer, Integer> wModuleCount = null;
                if (fireWeaponOnTargetCount.containsKey(targetByProbability.get(value))) {
                    wModuleCount = fireWeaponOnTargetCount.get(targetByProbability.get(value));
                    wModuleCount.put(wm.getWeaponId(), weaponsOnCurrentTarget);
                } else {
                    wModuleCount = new HashMap<Integer, Integer>();
                    wModuleCount.put(wm.getWeaponId(), weaponsOnCurrentTarget);
                    fireWeaponOnTargetCount.put(targetByProbability.get(value), wModuleCount);
                }

                weaponLeftCount -= weaponsOnCurrentTarget;
            }
        }

        // DEBUG SHOW RESULTING MAP
        for (Map.Entry<TargetListEntry, HashMap<Integer, Integer>> me : fireWeaponOnTargetCount.entrySet()) {
            log.debug(me.getKey().getTarget().getName() + " is targeted by following weapons: ");

            HashMap<Integer, Integer> wMatrix = me.getValue();
            for (Map.Entry<Integer, Integer> me2 : wMatrix.entrySet()) {
                Module m = mDAO.findById(me2.getKey());

                log.debug("Weapon "+m.getName()+" with count of " + me2.getValue());
            }
        }

        return fireWeaponOnTargetCount;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public float getHp() {
        return hp;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public int getDesignId() {
        return designId;
    }

    /**
     * @return the assignedBattleGroup
     */
    public int getParentCombatGroupId() {
        return assignedBattleGroup;
    }

    /**
     * @param assignedBattleGroup the assignedBattleGroup to set
     */
    public void setParentCombatGroupId(int assignedBattleGroup) {
        this.assignedBattleGroup = assignedBattleGroup;
    }

    public void setSc_LoggingEntry(SC_DataEntry sc_LoggingEntry) {
        this.sc_LoggingEntry = sc_LoggingEntry;
    }

    /**
     * @return the sc_LoggingEntry
     */
    public SC_DataEntry getSc_LoggingEntry() {
        return sc_LoggingEntry;
    }

    public ArrayList<WeaponModule> getWeapons() {
        return weapons;
    }

    public ShieldDefinition getPlanetaryShields() {
        return planetaryShields;
    }
    
    public ShieldDefinition getShields() {
        return shields;
    }

    /**
     * @return the tempDestroyed
     */
    public double getTempDestroyed() {
        return tempDestroyed;
    }

    /**
     * @param tempDestroyed the tempDestroyed to set
     */
    public void setTempDestroyed(double tempDestroyed) {
        this.tempDestroyed = tempDestroyed;
    }

    public void setParentCombatGroupFleet(CombatGroupFleet parentFleet) {
        this.setParentFleet(parentFleet);
    }

    /**
     * @return the parentFleet
     */
    public CombatGroupFleet getParentCombatGroupFleet() {
        return parentFleet;
    }

    /**
     * @param parentFleet the parentFleet to set
     */
    public void setParentFleet(CombatGroupFleet parentFleet) {
        this.parentFleet = parentFleet;
    }

    public void setBattleShip(IBattleShip bs) {
        this.bs = bs;
    }

    public int getSurvivors() {
        return damageDistribution[0] +
               damageDistribution[1] +
               damageDistribution[2] +
               damageDistribution[3] +
               damageDistribution[4];
    }

    public int getNoDamage() {
        return damageDistribution[0];
    }

    public int getMinorDamage() {
        return damageDistribution[1];
    }

    public int getLightDamage() {
        return damageDistribution[2];
    }

    public int getMediumDamage() {
        return damageDistribution[3];
    }

    public int getHeavyDamage() {
        return damageDistribution[4];
    }

    public int getOrgCount() {
        return orgCount;
    }

    public int getShipFleetId() {
        return shipFleetId;
    }

    /**
     * @return the type
     */
    public CombatGroupFleet.UnitTypeEnum getType() {
        return type;
    }

    /**
     * @param damageDistribution the damageDistribution to set
     */
    public void setDamageDistribution(int[] damageDistribution) {
        this.damageDistribution = damageDistribution;
    }

    @Override
    public int[] getDamageDistribution(int count, double temp_destroyed) {
        throw new UnsupportedOperationException("May not be called in this context");
    }

    @Override
    public double getDistraction() {
        throw new UnsupportedOperationException("May not be called in this context");
    }

    @Override
    public boolean fire(boolean allowReturnFire) {
        throw new UnsupportedOperationException("May not be called in this context");
    }

    /**
     * @return the tempDestroyedStart
     */
    public double getTempDestroyedStart() {
        return tempDestroyedStart;
    }

    /**
     * @param tempDestroyedStart the tempDestroyedStart to set
     */
    public void setTempDestroyedStart(double tempDestroyedStart) {
        this.tempDestroyedStart = tempDestroyedStart;
    }

    /**
     * @return the targetFireBonus
     */
    public float getTargetFireBonus() {
        return targetFireBonus;
    }

    /**
     * @param targetFireBonus the targetFireBonus to set
     */
    public void setTargetFireBonus(float targetFireBonus) {
        this.targetFireBonus = targetFireBonus;
    }

    /**
     * @return the targetFireDefenseBonus
     */
    public float getTargetFireDefenseBonus() {
        return targetFireDefenseBonus;
    }

    /**
     * @return the damageBonus
     */
    public float getDamageBonus() {
        return damageBonus;
    }

    /**
     * @return the damageDefenseBonus
     */
    public float getDamageDefenseBonus() {
        return damageDefenseBonus;
    }

    /**
     * @return the armorType
     */
    public int getArmorType() {
        return armorType;
    }
    
    public void setShields(ShieldDefinition shield) {
        this.shields = shield;
    }    
    
    public void setPlanetaryShields(ShieldDefinition shield) {
        this.planetaryShields = shield;
    }
}
