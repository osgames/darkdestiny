/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Stefan
 */
public enum EOwner {
    AGGRESSIVE("eowner_AGRESSIVE"),
    FRIENDLY("eowner_FRIENDLY"),
    WAR("eowner_WAR"),
    NEUTRAL("eowner_NEUTRAL"), 
    TRADE("eowner_TRADE"),
    ALLY("eowner_ALLY"),
    TREATY("eowner_TREATY"),
    NAP("eowner_NAP"),
    OWN("eowner_OWN"),
    INHABITATED("eowner_INHABITATED");
    
    private String value;

    EOwner(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }    
    
    @Override
    public String toString() {
        return this.getValue();
    }    
}
