/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view;

import at.darkdestiny.core.dao.SunTransmitterDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.model.SunTransmitter;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TransmitterTargetEntry {
    private SunTransmitterDAO stDAO = DAOFactory.get(SunTransmitterDAO.class);
    private SystemDAO sDAO = DAOFactory.get(SystemDAO.class);
    
    private int transmitterId = 0;
    private String targetName = "";
    private int systemId = 0;
    private boolean targetKnown = false;
    
    public TransmitterTargetEntry(int systemId, boolean alreadyDiscovered) {
        System.out.println("CREATE ENTRY");
        this.systemId = systemId;
        
        SunTransmitter st = new SunTransmitter();
        st.setSystemId(systemId);
        ArrayList<SunTransmitter> stResult = stDAO.find(st);        
        
        if (!stResult.isEmpty()) {
            transmitterId = stResult.get(0).getId();
        }
        
        if (!alreadyDiscovered) {            
            if (stResult.isEmpty()) {
                System.out.println("CASE 1");
                targetName = "#ERROR";
            } else {
                System.out.println("CASE 2");
                targetName = "Transmitter #" + stResult.get(0).getId();
            }
        } else {
            System.out.println("CASE 3");
            targetKnown = true;
            at.darkdestiny.core.model.System sys = sDAO.findById(systemId);
            targetName = sys.getName();
        }
        
        System.out.println("FINISHED");
    }

    /**
     * @return the targetName
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * @return the systemId
     */
    public int getSystemId() {
        return systemId;
    }

    /**
     * @return the targetKnown
     */
    public boolean isTargetKnown() {
        return targetKnown;
    }

    /**
     * @return the transmitterId
     */
    public int getTransmitterId() {
        return transmitterId;
    }
}
