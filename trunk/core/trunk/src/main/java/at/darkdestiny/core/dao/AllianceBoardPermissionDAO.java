/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EAllianceBoardPermissionType;
import at.darkdestiny.core.model.AllianceBoardPermission;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class AllianceBoardPermissionDAO extends ReadWriteTable<AllianceBoardPermission> implements GenericDAO {

   public ArrayList<AllianceBoardPermission> findByAllianceBoardId(int allianceBoardId) {
        AllianceBoardPermission abp = new AllianceBoardPermission();
        abp.setAllianceBoardId(allianceBoardId);
        return find(abp);
    }

    public ArrayList<AllianceBoardPermission> findBy(int boardId, EAllianceBoardPermissionType eAllianceBoardPermissionType) {
        AllianceBoardPermission abp = new AllianceBoardPermission();
        abp.setAllianceBoardId(boardId);
        abp.setType(eAllianceBoardPermissionType);
        return find(abp);
    }

    public Iterable<AllianceBoardPermission> findByUserId(int userId) {
        AllianceBoardPermission abp = new AllianceBoardPermission();
        abp.setRefId(userId);
        abp.setType(EAllianceBoardPermissionType.USER);
        return find(abp);
    }
    public Iterable<AllianceBoardPermission> findByAllianceId(int allianceId) {
        AllianceBoardPermission abp = new AllianceBoardPermission();
        abp.setRefId(allianceId);
        abp.setType(EAllianceBoardPermissionType.ALLIANCE);
        return find(abp);
    }


}
