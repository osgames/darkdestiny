/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.update;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.enumeration.ETradeRouteStatus;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.core.update.transport.DirectionalEdge;
import at.darkdestiny.core.update.transport.PlanetNode;
import at.darkdestiny.core.update.transport.RessCapacityMultiEdge;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Stefan
 */
public class TransportProcessing extends AbstractRouteProcessing {

    private static final Logger log = LoggerFactory.getLogger(TransportProcessing.class);

    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);

    public TransportProcessing(UpdaterDataSet uds) {
        super(uds);
        initRoutes();
    }

    private void initRoutes() {
        // log.debug("Start init of Transport Handler");        

        allDetailEntries.clear();
        clearAllSetRoutes();

        ArrayList<TradeRoute> allRoutes = trDAO.findAllByType(ETradeRouteType.TRANSPORT);
        for (TradeRoute tr : allRoutes) {
            // log.debug("Found route from " + tr.getStartPlanet() + " to " + tr.getTargetPlanet());            

            if (tr.getStatus().equals(ETradeRouteStatus.PENDING)) {
                continue;
            }

            // Create Route Datastructure
            TradeRouteExt tre = new TradeRouteExt(tr);
            TradeRouteProcessingEntry trpe = new TradeRouteProcessingEntry(tre);
            allEntries.put(tr.getId(), trpe);
            allDetailEntries.addAll(trpe.getFromToList());
            allDetailEntries.addAll(trpe.getToFromList());

            int actStart = tr.getStartPlanet();
            int actTarget = tr.getTargetPlanet();

            // Set start and target Planet Node
            PlanetNode start = tg.getPlanet(actStart);
            PlanetNode target = tg.getPlanet(actTarget);

            if (start == null) {
                start = new PlanetNode(actStart);
                tg.addPlanet(start);
            }
            if (target == null) {
                target = new PlanetNode(actTarget);
                tg.addPlanet(target);
            }

            // Set global outgoing for start planet
            PlanetConstruction pcAgrarStart = pcDAO.findBy(start.getPlanetId(), Construction.ID_AGRICULTURAL_DISTRIBUTION);
            PlanetConstruction pcAgrarTarget = pcDAO.findBy(target.getPlanetId(), Construction.ID_AGRICULTURAL_DISTRIBUTION);

            // NEW
            if (pcAgrarStart != null) {
                int capFood = (int)Math.floor(pcAgrarStart.getLevel() * 10000000 * getCapBoost(actStart));

                RessCapacityMultiEdge rcme = start.getRessCapMultiEdge(Ressource.FOOD);
                if (rcme == null) {
                    rcme = new RessCapacityMultiEdge(Ressource.FOOD,capFood);
                    start.setRessCapacityMultiEdge(rcme);
                }
            }

            if (pcAgrarTarget != null) {
                int capFood = (int)Math.floor(pcAgrarTarget.getLevel() * 10000000 * getCapBoost(actTarget));

                RessCapacityMultiEdge rcme = target.getRessCapMultiEdge(Ressource.FOOD);
                if (rcme == null) {
                    rcme = new RessCapacityMultiEdge(Ressource.FOOD,capFood);
                    target.setRessCapacityMultiEdge(rcme);
                }
            }
            // ***

            // Create Edges between planets
            DirectionalEdge outgoingStart = null;
            DirectionalEdge outgoingTarget = null;

            for (DirectionalEdge deTmp : start.getOutgoingTrades()) {
                if ((deTmp.getSource().getPlanetId() == start.getPlanetId()) &&
                    (deTmp.getTarget().getPlanetId() == target.getPlanetId())) {
                    outgoingStart = deTmp;
                }
            }
            for (DirectionalEdge deTmp : target.getOutgoingTrades()) {
                if ((deTmp.getSource().getPlanetId() == target.getPlanetId()) &&
                    (deTmp.getTarget().getPlanetId() == start.getPlanetId())) {
                    outgoingTarget = deTmp;
                }
            }

            if (outgoingStart == null) {
                outgoingStart = new DirectionalEdge(start,target,tre);

            }
            if (outgoingTarget == null) {
                outgoingTarget = new DirectionalEdge(target,start,tre);
            }

            // Add detail routes to Edges
            if (trpe.isFromTo()) {
                for (TradeRouteDetailProcessing trdp : trpe.getFromToList()) {
                    if (trdp.getBase().getDisabled()) continue;
                    // log.debug("Add trdp " + trdp.getBase().getId() + " [RessId: "+trdp.getBase().getRessId()+"] as outgoing to " + start.getPlanetId());
                    outgoingStart.addDetail(trdp);

                    RessCapacityMultiEdge rcme = start.getRessCapMultiEdge(Ressource.FOOD);
                    if ((rcme != null) && (trdp.getBase().getRessId() == Ressource.FOOD)) {
                        rcme.addTradeRouteDetailProcessing(trdp);
                    }
                }
            }

            if (trpe.isToFrom()) {
                for (TradeRouteDetailProcessing trdp : trpe.getToFromList()) {
                    if (trdp.getBase().getDisabled()) continue;
                    // log.debug("Add trdp " + trdp.getBase().getId() + " [RessId: "+trdp.getBase().getRessId()+"] as outgoing to " + target.getPlanetId());
                    outgoingTarget.addDetail(trdp);

                    RessCapacityMultiEdge rcme = target.getRessCapMultiEdge(Ressource.FOOD);
                    if ((rcme != null) && (trdp.getBase().getRessId() == Ressource.FOOD)) {
                        rcme.addTradeRouteDetailProcessing(trdp);
                    }
                    // java.lang.System.out.println("Add trdp " + trdp.getBase().getId() + " [RessId: "+trdp.getBase().getRessId()+"] as outgoing to " + target.getPlanetId());
                }
            }

            // Add directional edges to planets
            // start.addIncomingTrade(toFrom);            
            start.addOutgoingTrade(outgoingStart);

            // target.addIncomingTrade(fromTo);
            target.addOutgoingTrade(outgoingTarget);
        }
    }
    
    private double getCapBoost(int planetId) {
        double capBoost = 1d;
        PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(planetId);
        if (pp != null) {
            PlayerResearch prPlasma = Service.playerResearchDAO.findBy(pp.getUserId(), 43); // Plasmaengine
            PlayerResearch prImpulse = Service.playerResearchDAO.findBy(pp.getUserId(), 80); // Impulseengine
            PlayerResearch prLinear = Service.playerResearchDAO.findBy(pp.getUserId(), 81); // Linear engine
            PlayerResearch prMetagrav = Service.playerResearchDAO.findBy(pp.getUserId(), 109); // Metagrav engine

            if (prPlasma != null) {
                capBoost = 1.5d;
            }
            if (prImpulse != null) {
                capBoost = 2d;
            }
            if (prLinear != null) {
                capBoost = 10d;
            }
            if (prMetagrav != null) {
                capBoost = 20d;
            }
        }
        return capBoost;
    }    
}
