/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ConstructionRestriction;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Eobane
 */
public class ConstructionRestrictionDAO extends ReadOnlyTable<ConstructionRestriction> implements GenericDAO{

    public ConstructionRestriction getConstructionRestrictionBy(int type, Integer constructionId){
        ConstructionRestriction cr = new ConstructionRestriction();
        cr.setType(type);
        cr.setConstructionId(constructionId);
        return (ConstructionRestriction)get(cr);
    }

}
