/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "alliancelastread")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class AllianceLastRead extends Model<AllianceLastRead> {

    @FieldMappingAnnotation("allianceBoardId")
    @IdFieldAnnotation
    private Integer allianceBoardId;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("lastRead")
    @IdFieldAnnotation
    private Long lastRead;

    /**
     * @return the allianceBoardId
     */
    public Integer getAllianceBoardId() {
        return allianceBoardId;
    }

    /**
     * @param allianceBoardId the allianceBoardId to set
     */
    public void setAllianceBoardId(Integer allianceBoardId) {
        this.allianceBoardId = allianceBoardId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the lastRead
     */
    public Long getLastRead() {
        return lastRead;
    }

    /**
     * @param lastRead the lastRead to set
     */
    public void setLastRead(Long lastRead) {
        this.lastRead = lastRead;
    }
}
