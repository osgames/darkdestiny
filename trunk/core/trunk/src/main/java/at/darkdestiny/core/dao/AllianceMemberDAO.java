/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class AllianceMemberDAO extends ReadWriteTable<AllianceMember> implements GenericDAO {

    public ArrayList<AllianceMember> findByAllianceId(Integer allianceId) {
        AllianceMember am = new AllianceMember();
        am.setAllianceId(allianceId);
        return find(am);
    }

    public ArrayList<AllianceMember> findByAllianceIdNotTrial(Integer allianceId) {
        AllianceMember am = new AllianceMember();
        am.setAllianceId(allianceId);
        ArrayList<AllianceMember> ams = find(am);
        ArrayList<AllianceMember> result = new ArrayList<AllianceMember>();

        for (AllianceMember amTmp : ams) {
            if (amTmp.getIsTrial()) {
                continue;
            } else {
                result.add(amTmp);
            }
        }
        return result;
    }

    public AllianceMember findByUserId(int userId) {
        AllianceMember am = new AllianceMember();
        am.setUserId(userId);
        ArrayList<AllianceMember> result = find(am);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    /**
     * Achtung nicht B�ndniss�bergreifende Abfrage
     *
     * @param user1
     * @param user2
     * @return
     */
    public boolean areAllied(int user1, int user2) {
        if (findByUserId(user1) == null || findByUserId(user2) == null) {
            return false;
        }
        
        return findByUserId(user1).getAllianceId().equals(findByUserId(user2).getAllianceId());
    }

    public boolean areAllied(int user1, ArrayList<AllianceMember> allianceMembers) {
        if (findByUserId(user1) == null || allianceMembers == null) {
            return false;
        }
        for (AllianceMember am : allianceMembers) {
            if (am.getAllianceId().equals(findByUserId(user1).getAllianceId())) {
                return true;
            }
        }

        return false;
    }

    public ArrayList<AllianceMember> findAllianceMembersOfAlliances(ArrayList<Alliance> allys) {

        ArrayList<AllianceMember> allyMembers = new ArrayList<AllianceMember>();
        for (Alliance a : allys) {
            allyMembers.addAll(findByAllianceId(a.getId()));
        }
        return allyMembers;
    }

    public ArrayList<AllianceMember> findAllianceMembersOfAlliancesNotTrial(ArrayList<Alliance> allys) {

        ArrayList<AllianceMember> allyMembers = new ArrayList<AllianceMember>();
        for (Alliance a : allys) {
            allyMembers.addAll(findByAllianceIdNotTrial(a.getId()));
        }
        return allyMembers;
    }

    public ArrayList<AllianceMember> findByAllianceIdNotTrial(Integer allianceId, int toId) {
        AllianceMember am = new AllianceMember();
        am.setAllianceId(allianceId);
        ArrayList<AllianceMember> ams = find(am);
        ArrayList<AllianceMember> result = new ArrayList<AllianceMember>();

        for (AllianceMember amTmp : ams) {
            if (amTmp.getIsTrial() || amTmp.getUserId() == toId) {
                continue;
            } else {
                result.add(amTmp);
            }
        }
        return result;
    }
}
