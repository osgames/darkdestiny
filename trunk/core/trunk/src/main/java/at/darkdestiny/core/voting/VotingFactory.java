/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

import at.darkdestiny.core.model.Voting;
import at.darkdestiny.util.DebugBuffer;
import java.lang.reflect.Field;

/**
 *
 * @author Stefan
 */
public class VotingFactory {
    public static Vote createNewVote(int voteStarter, int type) throws VoteException {
        if (!Voting.isValidVoteType(type)) {
            throw new VoteException("Invalid vote type ("+type+")");
        }                        
        
        return new Vote(voteStarter, type);
    }
    
    public static Vote createNewVote(int voteStarter, String voteClazz) throws VoteException {
        // check if voting class exists .. if not throw VoteException                
        return new Vote(voteStarter, 999);
    }
    
    public static int persistVote(Vote v) throws VoteException {        
        int voteId = 0;
        
        try {
            if (v.isPersisted()) throw new VoteException("Vote already persisted");
            voteId = VoteUtilities.createVote(v);
        } catch (VoteException ve) {
            throw ve;
        }
        
        if (voteId != 0) {
            setPersistFlag(v);
        }
        
        return voteId;
    }
    
    public static Vote loadVote(int voteId) {
        Vote v = VoteUtilities.getVote(voteId);
        
        if (v == null) {
            DebugBuffer.error("Vote Id " + voteId + " does not exist");
            return null;
        }
        
        setPersistFlag(v);
        return v;       
    }
    
    private static void setPersistFlag(Vote v) {
        try {
            Field persistField = v.getClass().getDeclaredField("persisted");
            persistField.setAccessible(true);
            persistField.setBoolean(v, true);
        } catch (NoSuchFieldException nsfe) {
            DebugBuffer.writeStackTrace("Error while setting persist Flag: ", nsfe);
        } catch (IllegalAccessException iae) {
            DebugBuffer.writeStackTrace("Error while setting persist Flag: ", iae);
        }
    }
}
