/**
 * 
 */
package at.darkdestiny.core.admin.db;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author martin
 *
 */
public class ImgField extends IDatabaseColumn {

	private final int column;
	private final String basepath;

	/**
	 * @param comment 
	 * @param readonly 
	 * @param name 
	 * @param column 
	 */
	public ImgField(int column, String name, boolean readonly, String comment, String basepath) {
		super(name,comment, readonly);
		this.column = column;
		this.basepath = basepath;
	}

	public Object getFromResultSet(ResultSet rs) throws SQLException {
		return rs.getString(column);
	}

	public String getName() {
		return name;
	}

	public String makeEdit(Object object) {
		String res = "";
		if (object != null)
			res = "</td><td><img src=\""+basepath+object+"\" >";
		return "<input type=\"text\" size=\"80\" name=\"f_"+name+"\" value=\""+
			((object == null)?"":""+object)+"\" "+
			(readonly?"readonly ":"")+"/>"+res;
	}

	protected String getValueOnEmpty()
	{
		return "NULL";
	}

	public String getImgLink(Object object) {
		return basepath+object;
	}
}
