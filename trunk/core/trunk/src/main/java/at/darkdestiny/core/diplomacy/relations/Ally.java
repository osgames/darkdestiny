/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy.relations;

import at.darkdestiny.core.enumeration.EOwner;
import at.darkdestiny.core.enumeration.ESharingType;
import at.darkdestiny.core.result.DiplomacyResult;

/**
 *
 * @author Dreloc
 */
public class Ally implements IRelation{


    public DiplomacyResult getDiplomacyResult() {
        boolean attacks = true;
        boolean helps = true;
        boolean notification = false;
        boolean attackTradeFleets = false;
        ESharingType sharingStarMapInfo = ESharingType.ALL;
        EAttackType battleInNeutral = EAttackType.NO;
        EAttackType battleInOwn = EAttackType.NO;
        boolean ableToUseShipyard = true;
        EOwner owner = EOwner.ALLY;
        String color = "#1700C1";

        DiplomacyResult dr = new DiplomacyResult(attacks, helps, notification, attackTradeFleets, color, sharingStarMapInfo, ableToUseShipyard , battleInNeutral, battleInOwn, owner);
        return dr;
    }
}
