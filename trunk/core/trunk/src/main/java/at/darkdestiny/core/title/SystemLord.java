package at.darkdestiny.core.title;

import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.viewbuffer.HomeSystemBuffer;
import at.darkdestiny.framework.dao.DAOFactory;


public class SystemLord extends AbstractTitle {


    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

	public boolean check(int userId) {
            at.darkdestiny.core.model.System s = HomeSystemBuffer.getForUser(userId);
            if(s == null) return false;
            for(Planet p : pDAO.findBySystemId(s.getId())){
                if(ppDAO.findByPlanetId(p.getId()) == null || ppDAO.findByPlanetId(p.getId()).getUserId() != userId){
                    return false;
                }
            }
		return true;
	}

}
