/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.webservice;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.admin.SessionTracker;
import at.darkdestiny.core.enumeration.EGameDataStatus;
import at.darkdestiny.core.enumeration.EPassword;
import at.darkdestiny.core.enumeration.EUserGender;
import at.darkdestiny.core.model.GameData;
import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.model.PlayerStatistic;
import at.darkdestiny.core.model.Statistic;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.servlets.portal.RegistrationServlet.RegistrationValue;
import at.darkdestiny.core.view.html.form;
import at.darkdestiny.core.view.html.option;
import at.darkdestiny.core.view.html.select;
import at.darkdestiny.core.view.html.table;
import at.darkdestiny.core.view.html.td;
import at.darkdestiny.core.view.html.tr;
import at.darkdestiny.core.webservice.dto.PlayerStatisticList;
import at.darkdestiny.core.webservice.dto.PlayerStatisticListEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 *
 * C:\Programme\java\jdk1.7.0_25\bin>wsimport -d
 * c:\darkdestiny\trunk\portal\src\ma in\java
 * http://127.0.0.1:9090/serverinformation?wsdl -keep -Xnocompile
 *
 * @author Admin
 */


@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class ServerInformation {

    private static final Logger log = LoggerFactory.getLogger(ServerInformation.class);

    /**
     * Web service operation
     */
    @WebMethod(operationName = "isUserRegistered")
    public Boolean isUserRegistered(@WebParam(name = "userName") String userName) {
        log.info("Call isUserRegistered (userName=" + userName + ")");

        User u = Service.userDAO.findByUserName(userName);
        if (u != null) {
            return true;
        } else {
            return false;
        }
    }

    @WebMethod(operationName = "isPortalPasswordValid")
    public boolean isPortalPasswordValid(@WebParam(name = "userName") String userName, @WebParam(name = "webServicePassword") String webServicePassword, @WebParam(name = "portalPassword") String portalPassword) {
        log.info("Call isPortalPasswordValid (userName=" + userName + ", webServicePassword=" + webServicePassword + ", portalPassword=" + portalPassword + ")");
        if (!checkWebServicePassword(webServicePassword)) {
            return false;
        }
        User u = Service.userDAO.findByUserName(userName);
        if (u != null && u.getPortalPassword().equals(portalPassword)) {
            return true;
        } else {
            return false;
        }
    }

    @WebMethod(operationName = "getCurrentTick")
    public int getActualTick() {
        return GameUtilities.getCurrentTick2();
    }

    @WebMethod(operationName = "getPortalPassword")
    public String getPortalPassword(@WebParam(name = "userName") String userName, @WebParam(name = "webServicePassword") String webServicePassword, @WebParam(name = "registrationToken") String registrationToken) {
        log.info("Call getPortalPassword (userName=" + userName + ", webServicePassword=" + webServicePassword + ", registrationToken=" + registrationToken + ")");
        if (!checkWebServicePassword(webServicePassword)) {
            return "";
        }
        User u = Service.userDAO.findByUserName(userName);
        if (u != null && u.getRegistrationToken().equals(registrationToken)) {
            return u.getPortalPassword();
        } else {
            return "";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getRank")
    public Integer getRank(@WebParam(name = "gamename") String gamename) {
        log.info("Call getRank (gamename=" + gamename + ")");
        User u = Service.userDAO.findByGameName(gamename);
        if (u != null) {
            Statistic s = Service.statisticDAO.findByUserId(u.getUserId());
            if (s != null) {
                return s.getRank();
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getPoints")
    public Integer getPoints(@WebParam(name = "gamename") String gamename) {
        log.info("Call getPoints (gamename=" + gamename + ")");
        User u = Service.userDAO.findByGameName(gamename);
        if (u != null) {
            Statistic s = Service.statisticDAO.findByUserId(u.getUserId());
            if (s != null) {
                return s.getPoints();
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    @WebMethod(operationName = "incrementStatus")
    public void incrementStatus(@WebParam(name = "webServicePassword") String webServicePassword) {
        log.info("Call incrementStatus (webServicePassword=" + webServicePassword + ")");
        if (!checkWebServicePassword(webServicePassword)) {
            return;
        }
        GameData gameData = Service.gameDataDAO.findAll().get(0);
        boolean found = false;
        for (EGameDataStatus s : EGameDataStatus.values()) {
            if (found) {
                gameData.setStatus(s);
                Service.gameDataDAO.update(gameData);
                break;
            }
            if (s.equals(gameData.getStatus())) {
                found = true;
            }
        }
    }

    @WebMethod(operationName = "decrementStatus")
    public void decrementStatus(@WebParam(name = "webServicePassword") String webServicePassword) {
        log.info("Call decrementStatus (webServicePassword=" + webServicePassword + ")");
        System.out.println("Received decrement Status action");
        System.out.println("compare : " + Service.passwordDAO.findByType(EPassword.WEBSERVICE).getPassword() + " to " + webServicePassword);
        if (!checkWebServicePassword(webServicePassword)) {
            return;
        }
        EGameDataStatus tmpPrio = null;
        GameData gameData = Service.gameDataDAO.findAll().get(0);
        for (EGameDataStatus s : EGameDataStatus.values()) {
            if (s.equals(gameData.getStatus()) && tmpPrio != null) {
                gameData.setStatus(tmpPrio);
                Service.gameDataDAO.update(gameData);
                break;
            }
            tmpPrio = s;
        }

    }

    @WebMethod(operationName = "getLoginForm")
    public String getLoginForm(@WebParam(name = "webServicePassword") String webServicePassword, @WebParam(name = "locale") String l) {
        log.info("Call getLoginForm (webServicePassword=" + webServicePassword + ", locale=" + l + ")");
        if (!checkWebServicePassword(webServicePassword)) {
            return "Error, wrong webservice password";
        }
        Locale locale = null;
        if (l != null && !l.equals("")) {
            locale = new Locale(l.substring(0, 2), l.substring(3, l.length()));
        }

        StringBuffer stringBuffer = new StringBuffer();

        form f = new form();
        f.setProperties("name='anmelden' method='post' action='" + GameConfig.getInstance().getHostURL() + "/RegistrationServlet'");

        table t = new table();

        tr tr = new tr();
        tr.addTd("<input type='hidden' name='" + RegistrationValue.NAME_USERNAME + "' value='%USERNAME_VALUE%'/>");
        tr.addTd("<input type='hidden' name='" + RegistrationValue.NAME_REGISTRATIONTOKEN + "' value='%REGISTRATIONTOKEN_VALUE%'/>");
        tr.addTd("<input type='hidden' name='" + RegistrationValue.NAME_SOURCE_URL + "' value='%NAME_SOURCE_URL_VALUE%'/>");
        t.addTr(tr);
        tr = new tr();
        tr.addTd(ML.getMLStr("loginform_lbl_gamename", locale));
        tr.addTd("<input name='" + RegistrationValue.NAME_GAMENAME + "'/>");
        t.addTr(tr);

        tr = new tr();
        tr.addTd(ML.getMLStr("loginform_lbl_password", locale));
        tr.addTd("<input type='password' name='" + RegistrationValue.NAME_PASS1 + "'/>");
        t.addTr(tr);

        tr = new tr();
        tr.addTd(ML.getMLStr("loginform_lbl_password_confirmation", locale));
        tr.addTd("<input type='password' name='" + RegistrationValue.NAME_PASS2 + "'/>");
        t.addTr(tr);

        tr = new tr();
        tr.addTd(ML.getMLStr("loginform_lbl_email", locale));
        tr.addTd("<input name='" + RegistrationValue.NAME_EMAIL + "'/>");
        t.addTr(tr);

        tr = new tr();
        tr.addTd(ML.getMLStr("loginform_msg_usageagreement", locale, "<a target='new' href='" + GameConfig.getInstance().getHostURL() + "/UsageAgreement.htm'>", "</a>"));
        tr.addTd("<input type='checkbox' name='" + RegistrationValue.VALUE_USAGE_AGREEMENT + "' value='" + RegistrationValue.VALUE_USAGE_AGREEMENT + "'/>");
        t.addTr(tr);

        tr = new tr();
        tr.addTd(ML.getMLStr("loginform_lbl_language", locale));
        select s = new select();
        s.setProperties(" name='" + RegistrationValue.NAME_LOCALE + "'");
        for (Language language : Service.languageDAO.findAll()) {
            option o = new option();
            o.setData(ML.getMLStr(language.getName(), locale));
            o.setProperties("value=" + language.getId());
            s.addOption(o);
        }
        td td = new td(s.toString());
        tr.addTd(td);
        t.addTr(tr);

        tr = new tr();
        tr.addTd(ML.getMLStr("loginform_lbl_gender", locale));
        s = new select();
        s.setProperties(" name='" + RegistrationValue.NAME_GENDER + "'");
        for (EUserGender gender : EUserGender.values()) {
            option o = new option();
            o.setData(ML.getMLStr("login_opt_" + gender.toString(), locale));
            o.setProperties("value=" + gender.toString());
            s.addOption(o);
        }
        td = new td(s.toString());

        tr.addTd(td);
        t.addTr(tr);

        tr = new tr();
        tr.addTd("<input type='submit' action='submit' value='" + ML.getMLStr("loginform_but_submit", locale) + "'/>", " colspan='2' style='text-align:center'");
        t.addTr(tr);

        f.addDiv(t);
        stringBuffer.append(f.toString());
        return stringBuffer.toString();
    }

    public boolean checkWebServicePassword(String webServicePassword) {
        log.info("Call checkWebServicePassword (webServicePassword=" + webServicePassword + ")");
        if (Service.passwordDAO.findByType(EPassword.WEBSERVICE) == null) {

            return false;
        }
        return ((Service.passwordDAO.findByType(EPassword.WEBSERVICE).getValue().equals(webServicePassword)));
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getStatus")
    public String getStatus() {
        log.info("Call getStatus");
        return Service.gameDataDAO.findAll().get(0).getStatus().toString();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUserOnlineCount")
    public Integer getUserOnlineCount() {
        log.info("Call getUserOnlineCount");
        int i = SessionTracker.getLoggedInUsers();
        log.info("Returning " + i);
        return i;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUserCount")
    public Integer getUserCount() {
        log.info("Call getUserCount");
        return Service.userDAO.findAllPlayers().size();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getStatistics")
    public PlayerStatisticList getStatistics(@WebParam(name = "date") final long date) {
        log.info("Call PlayerStatisticList (date=" + date + ")");
        PlayerStatisticList psl = new PlayerStatisticList();
        System.out.println("Serverside statistics call!");

        Date d = new Date(date);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        System.out.println(sdf.format(d));
        ArrayList<PlayerStatisticListEntry> psle = convertPlayerStatistics(Service.playerStatisticDAO.findByDay(date), date);
        System.out.println("Found : " + psle.size() + " entries");
        psl.setPlayerStatistics(psle);
        return psl;
    }

    private ArrayList<PlayerStatisticListEntry> convertPlayerStatistics(ArrayList<PlayerStatistic> statistics, long date) {
        ArrayList<PlayerStatisticListEntry> result = new ArrayList<PlayerStatisticListEntry>();
        for (PlayerStatistic ps : statistics) {
            PlayerStatisticListEntry plse = new PlayerStatisticListEntry();
            plse.setDate(date);
            String gameName = "Unbekannt";
            System.out.println("UserId : " + ps.getUserId());
            if (Service.userDAO.findById(ps.getUserId()) != null) {
                gameName = Service.userDAO.findById(ps.getUserId()).getGameName();
            }
            plse.setGameName(gameName);
            plse.setRank(ps.getRank());
            plse.setMilitaryPoints(ps.getMilitaryPoints());
            plse.setPopulationPoints(ps.getPopulationPoints());
            plse.setResearchPoints(ps.getResearchPoints());
            plse.setRessourcePoints(ps.getRessourcePoints());
            result.add(plse);
        }
        return result;
    }
}
