/*
 * ShipStorage.java
 *
 * Created on 28. Jänner 2006, 19:18
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package at.darkdestiny.core.ships;

public class ShipStorage {
    private int loadtype;
    private int id;
    private String name;
    private int count;
    
    public static final int LOADTYPE_RESSOURCE = 1;
    public static final int LOADTYPE_GROUNDTROOP = 2;
    public static final int LOADTYPE_POPULATION = 4;
    
    public ShipStorage() {
    }

    public int getLoadtype() {
        return loadtype;
    }

    public void setLoadtype(int loadtype) {
        this.loadtype = loadtype;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
