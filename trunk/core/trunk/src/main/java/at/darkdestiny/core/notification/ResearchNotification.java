/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.notification;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.model.Research;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class ResearchNotification extends Notification {

    private ArrayList<Research> finishedResearches;
    String msg = "";

    public ResearchNotification(ArrayList<Research> finishedResearches, int userId) {
        super(userId);
        this.finishedResearches = finishedResearches;
        generateMessage();
        persistMessage();
    }

    public ResearchNotification(Research finishedResearches, int userId) {
        super(userId);
        ArrayList<Research> rs = new ArrayList<Research>();
        rs.add(finishedResearches);
        this.finishedResearches = rs;
        generateMessage();
    }

    @Override
    public void generateMessage() {
        if (finishedResearches.isEmpty()) {
            return;
        }
        //Add to static buffer



        msg = "";
        msg += ML.getMLStr("notification_res_msg2", userId);
        for (Research r : finishedResearches) {
            msg += "- ";
            if (ML.getAtlanMLStr(r.getDetailText(), userId) != null
                    && !ML.getAtlanMLStr(r.getDetailText(), userId).equals("")
                    && !ML.getAtlanMLStr(r.getDetailText(), userId).trim().equalsIgnoreCase("NULL")
                    && !ML.getAtlanMLStr(r.getDetailText(), userId).trim().contains("e:")) {
                // msg +="<A onClick=\"return popup(this, \\'notes2\\')\" target=\"blank\" href=\"researchText.jsp?researchId=" + r.getId() + "\">";
                msg += "<A onClick=\"return popup(this, 'notes2')\" target=\"blank\" href=\"researchText.jsp?researchId=" + r.getId() + "\">";
                msg += ML.getMLStr(r.getName(), userId);
                msg += "</A>";
                msg += "<BR>";
            } else {
                msg += ML.getMLStr(r.getName(), userId) + "<BR>";
            }
        }
        String topic = "";
        topic += finishedResearches.size() + " ";
        topic += ML.getMLStr("notification_res_msg1", userId);

        gm.setMessageContentType(EMessageContentType.RESEARCH);
        gm.setTopic(topic);
        gm.setMsg(msg);
    }

    public String getMessage() {
        return msg;
    }
}
