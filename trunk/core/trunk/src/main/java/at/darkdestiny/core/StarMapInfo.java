/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.construction.TotalConstructionResult;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResolver;
import at.darkdestiny.core.diplomacy.combat.DiplomacyGroupResult;
import at.darkdestiny.core.diplomacy.relations.IRelation;
import at.darkdestiny.core.enumeration.EFleetViewType;
import at.darkdestiny.core.enumeration.EPlanetLogType;
import at.darkdestiny.core.enumeration.ETerritoryMapType;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.DiplomacyRelation;
import at.darkdestiny.core.model.DiplomacyType;
import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.HangarRelation;
import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetLog;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.SunTransmitter;
import at.darkdestiny.core.model.SunTransmitterRoute;
import at.darkdestiny.core.model.TerritoryMap;
import at.darkdestiny.core.model.TerritoryMapPoint;
import at.darkdestiny.core.model.TerritoryMapShare;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.ViewTable;
import at.darkdestiny.core.movable.FleetFormationExt;
import at.darkdestiny.core.movable.IFleet;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.result.DiplomacyResult;
import at.darkdestiny.core.result.FleetListResult;
import at.darkdestiny.core.scanner.StarMapQuadTree;
import at.darkdestiny.core.scanner.SystemDesc;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.service.IdToNameService;
import at.darkdestiny.core.service.Service;
import static at.darkdestiny.core.service.Service.ressourceDAO;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.core.utilities.AllianceUtilities;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import at.darkdestiny.core.utilities.ViewSystemTableUtilities;
import at.darkdestiny.core.xml.XMLMemo;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author HorstRabe
 */
public class StarMapInfo extends Service {

    private static final Logger log = LoggerFactory.getLogger(StarMapInfo.class);
    public static final int FLEET_ENEMY = 3;
    public static final int FLEET_OWN = 1;
    public static final int FLEET_ALLIED = 2;
    public static final int HyperScannerId = 42;
    public static final int FLEET_MAXIMUM_VISIBLE_RANGE = 300;
    public static final int HYPERSCANNER_RANGE = 200;
    public static final int MILITARYOUTPOST_HYPERSCANNER_RANGE = 300;
    private static boolean debug = false;
    private final int userId;
    private final HashMap<Integer, HashMap<Integer, ViewTable>> vtMap;
    private final ArrayList<Integer> displayedFleets = new ArrayList<Integer>();
    private HashMap<Integer, Galaxy> galaxies = Maps.newHashMap();

    public StarMapInfo() {
        this.userId = 0;
        vtMap = new HashMap<Integer, HashMap<Integer, ViewTable>>();
    }
    
    public StarMapInfo(int userId) {
        this.userId = userId;
        vtMap = viewTableDAO.findByUserCategorized(userId);
    }

    public XMLMemo addSystems(XMLMemo starMap, StarMapInfoData smid) {
        if (smid.userId != null) {
            HashMap<Integer, ArrayList<at.darkdestiny.core.model.System>> observatories = smid.getObservatories();
            HashMap<Integer, PlayerPlanet> pps = new HashMap<Integer, PlayerPlanet>();
            for (PlayerPlanet ppTmp : (ArrayList<PlayerPlanet>) playerPlanetDAO.findAll()) {
                pps.put(ppTmp.getPlanetId(), ppTmp);
            }
            StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
            ArrayList<at.darkdestiny.core.model.System> systems = systemDAO.findAll();

            int i = 0;

            for (at.darkdestiny.core.model.System s : systems) {
                SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
                quadtree.addItemToTree(sd);
                i++;
            }

            //Building all Systems in Observatory reach
            // Also include allied observatories
            ArrayList<Integer> alliedUsers = AllianceUtilities.getAllAlliedUsers(smid.userId);        
            HashSet<Integer> processedSystems = new HashSet<Integer>();

            HashSet<SystemDesc> tmpRes = new HashSet<SystemDesc>();
            HashSet<Integer> sysObs = new HashSet<Integer>();
            for (Map.Entry<Integer, ArrayList<at.darkdestiny.core.model.System>> entry0 : observatories.entrySet()) {            
                for (at.darkdestiny.core.model.System entry : entry0.getValue()) {
                    // DebugBuffer.debug("Processing own observatory at " + entry.getId());
                    processedSystems.add(entry.getId());
                    tmpRes.addAll(quadtree.getItemsAround(entry.getX(), entry.getY(), (int) 100));
                }
            }

            for (int userId : alliedUsers) {
                TotalConstructionResult tcr = new TotalConstructionResult(Construction.ID_OBSERVATORY,userId);      
                ArrayList<Integer> addObsLocations = tcr.getAllPlanetsWithConstruction(Construction.ID_OBSERVATORY);

                for (int obsPlanetId : addObsLocations) {
                    RelativeCoordinate rcObs = new RelativeCoordinate(0,obsPlanetId);
                    if (!processedSystems.contains(rcObs.getSystemId())) {
                        // DebugBuffer.debug("Processing allied observatory at " + rcObs.getSystemId());
                        processedSystems.add(rcObs.getSystemId());
                        tmpRes.addAll(quadtree.getItemsAround(rcObs.toAbsoluteCoordinate().getX(), rcObs.toAbsoluteCoordinate().getY(), (int) 100));
                    }                
                }
            }

            for (SystemDesc sd : tmpRes) {
                sysObs.add(sd.id);
            }

            XMLMemo systemsTag = new XMLMemo("Systems");
            // HashMap<Integer, HashMap<Integer, ViewTable>> vtMap = viewTableDAO.findByUserCategorized(userId);

            HashMap<Integer, ArrayList<Integer>> fleets = new HashMap<Integer, ArrayList<Integer>>();
            for (PlayerFleet pf : (ArrayList<PlayerFleet>) playerFleetDAO.findAll()) {
                ArrayList<Integer> entry1 = fleets.get(pf.getSystemId());
                if (entry1 == null) {
                    entry1 = new ArrayList<Integer>();
                }
                if ((!entry1.contains(pf.getPlanetId()) && (pf.getUserId() == userId || smid.getSharingUsers().contains(pf.getUserId())))) {
                    entry1.add(pf.getPlanetId());
                }
                fleets.put(pf.getSystemId(), entry1);
            }
            //Planetlog
            HashMap<Integer, ArrayList<PlanetLog>> planetLogs = planetLogDAO.findAllSorted();
            //Systems
            HashMap<Integer, ArrayList<Planet>> planetList = planetDAO.findAllSortedBySystem();

            // **************************************************************
            HashSet<Integer> allUsers = new HashSet<Integer>();
            allUsers.add(userId);

            // Gather all visible planets for collecting users for DiplomacyRelations
            for (at.darkdestiny.core.model.System s : systems) {
                if (vtMap.containsKey(s.getId())) {
                    HashMap<Integer, ViewTable> planetEntries = vtMap.get(s.getId());
                    for (Iterator<Integer> pIt = planetEntries.keySet().iterator(); pIt.hasNext();) {
                        Integer planetIdTmp = pIt.next();

                        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetIdTmp);
                        if (pp != null) {
                            allUsers.add(pp.getUserId());

                            if (planetLogs.get(planetIdTmp) != null) {
                                for (PlanetLog pl : planetLogs.get(planetIdTmp)) {
                                    allUsers.add(pl.getUserId());
                                }
                            }
                        }
                    }
                }
            }

            ArrayList<Integer> arrAllUsers = new ArrayList<Integer>();
            arrAllUsers.addAll(allUsers);

            DiplomacyGroupResult dcr = CombatGroupResolver.resolveUsers(arrAllUsers);
            // **************************************************************

            HashSet<Integer> alreadyProcessed = Sets.newHashSet();
            for (at.darkdestiny.core.model.System s : systems) {
                //        if (s.getVisibility() != 0 && s.getVisibility() != userId && !allys.contains(s.getVisibility())) {
                //          continue;
                //   }
                int systemStatus = -1;

                XMLMemo actSystem = new XMLMemo("System");
                actSystem.addAttribute("id", s.getId());

                String sysName = s.getName();
                if (!vtMap.containsKey(s.getId())) {
                    sysName = "System " + s.getId();
                }

                int lastUpdateSystem = 0;
                // If we find a named planet system name can be displayed
                boolean hideSystemName = true;

                // Viewtableentries for map existing
                // if (s.getId() == 2079) log.debug("SCANNING PROBLEM SYSTEM 2079");


                if (vtMap.containsKey(s.getId())) {
                    boolean actual = false;
                    XMLMemo planets = new XMLMemo("Planets");
                    //Search if any player has a Planet in this System
                    HashMap<Integer, ViewTable> planetEntries = vtMap.get(s.getId());

                    for (Map.Entry<Integer, ViewTable> entry : planetEntries.entrySet()) {
                        //PP exists
                        if (pps.containsKey(entry.getKey())) {
                            PlayerPlanet pp = pps.get(entry.getKey());
                            // Whole system is actual
                            if (userId == pp.getUserId() || smid.getSharingUsers().contains(pp.getUserId())) {
                                actual = true;

                            }
                        }
                    }
                    //Search in observatories
                    if (!actual) {
                        if (sysObs.contains(s.getId())) {
                            actual = true;
                        }
                    }
                    //Search in fleets

                    if (fleets.containsKey(s.getId()) && fleets.get(s.getId()).contains(0)) {
                        actual = true;

                    }

                    for (Map.Entry<Integer, ViewTable> entry : planetEntries.entrySet()) {
                        //System without planets
                        if (fleets.containsKey(s.getId()) && fleets.get(s.getId()).contains(entry.getKey())) {
                            actual = true;

                        }

                        //If system without planets
                        if (entry.getKey() == 0) {
                            if (planetList.get(s.getId()) == null) {
                                systemStatus = 0;
                                if (actual) {
                                    lastUpdateSystem = GameUtilities.getCurrentTick2();
                                }
                            }
                            continue;
                        } else {
                            systemStatus = 0;
                        }
                        XMLMemo planet = new XMLMemo("Planet");
                        int pStatus = 0;
                        Planet p = planetDAO.findById(entry.getKey());
                        if (p == null) {
                            continue;
                        }

                        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(entry.getKey());
                        ViewTable vt = entry.getValue();
                        int lastUpdatePlanet = 0;
                        String planetName = "Planet #" + p.getId();
                        int planetOwner = 0;

                        /*
                         if (s.getId() == 2079) log.debug("SCANNING PLANET " + p.getId() + " ACTUAL: " + actual + " LATEST TIMESTAMP: " + vt.getLastTimeVisited() +
                         " CURRENTSTAMP: " + GameUtilities.getCurrentTick2());

                         if (s.getId() == 2079) {
                         if (pp != null) {
                         log.debug("This planet is inhabitated by " + IdToNameService.getUserName(pp.getUserId()));
                         } else {
                         log.debug("This planet is not inhabitated");
                         }
                         }
                         */

                        if (actual) {
                            lastUpdatePlanet = GameUtilities.getCurrentTick2();
                            if (pp != null) {
                                planetName = pp.getName();
                                planetOwner = pp.getUserId();

                                hideSystemName = false;
                                int diplomacyTypeId = 10;
                                if (dcr.getRelationBetween(userId, planetOwner) != null) {
                                    diplomacyTypeId = dcr.getRelationBetween(userId, planetOwner).getDiplomacyTypeId();
                                }
                                if (pp.isHomeSystem() && (diplomacyTypeId <= DiplomacyType.TREATY || pp.getUserId() == userId)) {
                                    planet.addAttribute("homesystem", Boolean.TRUE.toString());
                                } else {
                                    planet.addAttribute("homesystem", Boolean.FALSE.toString());
                                }
                            }
                        } else {
                            lastUpdatePlanet = vt.getLastTimeVisited();
                            if (planetLogs.get(p.getId()) != null) {
                                for (PlanetLog pl : planetLogs.get(p.getId())) {
                                    if (pl.getTime() > vt.getLastTimeVisited()) {
                                        continue;
                                    } else {
                                        if (pl.getType() == EPlanetLogType.ABANDONED) {
                                            pp = null;
                                        }

                                        if (pp != null) {
                                            planetName = pp.getName();
                                            planetOwner = pl.getUserId();

                                            hideSystemName = false;

                                            int diplomacyTypeId = 10;
                                            if (dcr.getRelationBetween(userId, planetOwner) != null) {
                                                diplomacyTypeId = dcr.getRelationBetween(userId, planetOwner).getDiplomacyTypeId();
                                            }
                                            if (pp.isHomeSystem() && (diplomacyTypeId <= DiplomacyType.TREATY || pp.getUserId() == userId)) {
                                                planet.addAttribute("homesystem", Boolean.TRUE.toString());
                                            } else {
                                                planet.addAttribute("homesystem", Boolean.FALSE.toString());
                                            }
                                            break;
                                        }
                                    }
                                }
                            }

                        }

                        if (lastUpdatePlanet > lastUpdateSystem) {
                            lastUpdateSystem = lastUpdatePlanet;
                        }

                        if (planetOwner != 0) {
                            if (planetOwner == userId) {
                                pStatus = 1;
                                lastUpdatePlanet = GameUtilities.getCurrentTick2();
                                lastUpdateSystem = GameUtilities.getCurrentTick2();
                            }
                            if (planetOwner != userId) {
                                if (smid.getSharingUsers().contains(planetOwner)) {
                                    lastUpdatePlanet = GameUtilities.getCurrentTick2();
                                    lastUpdateSystem = GameUtilities.getCurrentTick2();
                                }

                                pStatus = dcr.getRelationBetween(userId, planetOwner).getDiplomacyTypeId() + 10;
                                // pStatus = DiplomacyUtilities.getDiplomacyRel(userId, planetOwner).getDiplomacyTypeId() + 10;
                            }
                        }

                        planet.addAttribute("id", p.getId());
                        planet.addAttribute("typ", p.getLandType());
                        planet.addAttribute("name", StarMapInfo.secString(planetName));
                        planet.addAttribute("diameter", p.getDiameter());

                        if (planetOwner != 0) {
                            // log.debug("Looking for user " + planetOwner);
                            // REMOVED USERBUFFER
                            User u = userDAO.findById(planetOwner);
                            if (u == null) {
                                planet.addAttribute("owner", "UNBEKANNT");
                            } else {
                                planet.addAttribute("owner", u.getGameName());
                            }

                        }
                        if (pStatus != 0) {
                            planet.addAttribute("pStatus", pStatus);
                        }

                        planet.addAttribute("lastupdate", String.valueOf(System.currentTimeMillis() - ((long) (((long) GameUtilities.getCurrentTick2() - (long) lastUpdatePlanet) * (long) GameConfig.getInstance().getTicktime()))));

                        // planet.addAttribute("lastupdate", String.valueOf(GameConfig.getStarttime() + (long) lastUpdatePlanet * GameConfig.getInstance().getTicktime()));
                        planets.addChild(planet);


                    }
                    actSystem.addChild(planets);

                    if (hideSystemName) {
                        sysName = "System " + s.getId();
                    }

                    actSystem.addAttribute("name", StarMapInfo.secString(sysName));
                    actSystem.addAttribute("x", s.getX());
                    actSystem.addAttribute("y", s.getY());
                    actSystem.addAttribute("sysstatus", systemStatus);
                    actSystem.addAttribute("lastupdate", String.valueOf(System.currentTimeMillis() - ((long) (((long) GameUtilities.getCurrentTick2() - (long) lastUpdateSystem) * (long) GameConfig.getInstance().getTicktime()))));
                    systemsTag.addChild(actSystem);
                    alreadyProcessed.add(s.getId());
                } else {


                    if (!s.getGalaxy().getPlayerStart()) {
                    //    continue;
                    }
                    if (hideSystemName) {
                        sysName = "System " + s.getId();
                    }
                    //If not in viewtable and not a startable galaxy skip
                    //Show all systems
                    actSystem.addAttribute("name", StarMapInfo.secString(sysName));
                    actSystem.addAttribute("x", s.getX());
                    actSystem.addAttribute("y", s.getY());
                    actSystem.addAttribute("sysstatus", systemStatus);
                    actSystem.addAttribute("lastupdate", String.valueOf(System.currentTimeMillis() - ((long) (((long) GameUtilities.getCurrentTick2() - (long) lastUpdateSystem) * (long) GameConfig.getInstance().getTicktime()))));

                    systemsTag.addChild(actSystem);


                }
            }

            starMap.addChild(systemsTag);
        } else {
            XMLMemo systemsTag = new XMLMemo("Systems");
            
            ArrayList<at.darkdestiny.core.model.System> systems = systemDAO.findAll();
            for (at.darkdestiny.core.model.System s : systems) {
                XMLMemo actSystem = new XMLMemo("System");
                actSystem.addAttribute("id", s.getId());
                actSystem.addAttribute("name", StarMapInfo.secString("System " + s.getId()));
                actSystem.addAttribute("x", s.getX());
                actSystem.addAttribute("y", s.getY());
                actSystem.addAttribute("sysstatus", "-1");
                actSystem.addAttribute("lastupdate", "0");
                systemsTag.addChild(actSystem);                
            }            
            
            starMap.addChild(systemsTag);
        }
        
        return starMap;
    }

    public XMLMemo addAllFleets(XMLMemo starMap, StarMapInfoData smid) {
        XMLMemo fleets = new XMLMemo("Fleets");

        FleetFormationCheck ffc = new FleetFormationCheck();

        fleets = addOwnFleets(fleets, ffc);
        fleets = addAlliedFleets(fleets, ffc);
        fleets = addScannedFleets(fleets, smid, ffc);

        starMap.addChild(fleets);

        return starMap;
    }

    public void addTerritories(XMLMemo starMap, StarMapInfoData smid) {

        XMLMemo territories = new XMLMemo("Territories");

        for (TerritoryMapShare tms : Service.territoryMapShareDAO.findByUserId(smid.userId)) {
            if (tms.getShow()) {
                TerritoryMap t = Service.territoryMapDAO.findById(tms.getTerritoryId());
                XMLMemo territory = new XMLMemo("Territory");
                territory.addAttribute("id", t.getId());
                territory.addAttribute("name", t.getName());
                territory.addAttribute("description", t.getDescription());
                int status = 0;
                if (t.getType().equals(ETerritoryMapType.USER) && t.getRefId() != userId) {

                    DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRel(smid.userId, t.getRefId());
                    status = dr.getDiplomacyTypeId();
                    territory.addAttribute("status", status + 10);
                } else {
                    territory.addAttribute("status", status);
                }
                for (TerritoryMapPoint tmp : Service.territoryMapPointDAO.findByTerritoryId(t.getId())) {
                    XMLMemo point = new XMLMemo("Point");
                    point.addAttribute("id", tmp.getId());
                    point.addAttribute("x", tmp.getX());
                    point.addAttribute("y", tmp.getY());
                    territory.addChild(point);
                }
                territories.addChild(territory);
            }
        }        

        starMap.addChild(territories);

    }

    public XMLMemo addConstants(XMLMemo starMap) {

        XMLMemo constants = new XMLMemo("Constants");
        XMLMemo chassisList = new XMLMemo("ChassisList");
        XMLMemo viewSystemRanges = new XMLMemo("ViewSystemRanges");
        XMLMemo groundTroops = new XMLMemo("Groundtroops");
        XMLMemo ressources = new XMLMemo("Ressources");
        XMLMemo relations = new XMLMemo("Relations");
        XMLMemo uniConstants = new XMLMemo("UniverseConstants");
        ArrayList<GroundTroop> troops = groundTroopDAO.findAll();

        XMLMemo language = new XMLMemo("Language");        
        
        String locale = userDAO.findById(userId).getLocale();
        Language l = Service.languageDAO.findBy(locale);
        if (!l.getId().equals(l.getMasterLanguageId())) {
            Language lTmp = Service.languageDAO.findById(l.getMasterLanguageId());
            locale = lTmp.getLanguage() + "_" + lTmp.getCountry();
        }
        language.addAttribute("Locale", locale);

        constants.addChild(language);
        for (GroundTroop gt : troops) {
            XMLMemo troop = new XMLMemo("Troop");
            troop.addAttribute("id", gt.getId());
            troop.addAttribute("name", ML.getMLStr(gt.getName(), userId));
            groundTroops.addChild(troop);
        }
        ArrayList<Ressource> tRess = ressourceDAO.findAllStoreableRessources();
        for (Ressource r : tRess) {
            XMLMemo ressource = new XMLMemo("Ressource");

            ressource.addAttribute("id", r.getId());
            ressource.addAttribute("name", secString(ML.getMLStr(r.getName(), userId)));
            ressources.addChild(ressource);

        }

        for (Map.Entry<String, Integer> range : ViewSystemTableUtilities.getRanges().entrySet()) {
            XMLMemo viewSystemRange = new XMLMemo("ViewSystemRange");

            viewSystemRange.addAttribute("type", range.getKey());
            viewSystemRange.addAttribute("range", String.valueOf(range.getValue()));
            viewSystemRanges.addChild(viewSystemRange);

        }
        ArrayList<Chassis> chs = chassisDAO.findAll();
        for (Chassis ch : chs) {
            XMLMemo chassis = new XMLMemo("Chassis");

            chassis.addAttribute("id", ch.getId());
            chassis.addAttribute("name", ML.getMLStr(ch.getName(), userId));
            chassisList.addChild(chassis);

        }
        for (DiplomacyType dt : (ArrayList<DiplomacyType>) diplomacyTypeDAO.findAll()) {
            XMLMemo relation = new XMLMemo("Relation");
            relation.addAttribute("id", dt.getId() + 10);
            try {
                Class clzz = Class.forName(dt.getClazz());
                //Get Constructor
                Constructor con = clzz.getConstructor();

                //Cast Relation
                IRelation sbe = (IRelation) con.newInstance();
                relation.addAttribute("color", sbe.getDiplomacyResult().getColor());
                relations.addChild(relation);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error instatiating DiplomacyType", e);
            }
        }
        uniConstants.addAttribute("height", GameConstants.UNIVERSE_HEIGHT);
        uniConstants.addAttribute("width", GameConstants.UNIVERSE_WIDTH);

        constants.addChild(chassisList);
        constants.addChild(ressources);
        constants.addChild(groundTroops);
        constants.addChild(viewSystemRanges);
        constants.addChild(relations);
        constants.addChild(uniConstants);

        starMap.addChild(constants);
        return starMap;
    }

    public XMLMemo addTradeRoutes(XMLMemo starMap) {
        try {
            ArrayList<TradeRoute> trs = tradeRouteDAO.findByUserId(userId, ETradeRouteType.TRANSPORT);

            XMLMemo tradeRoutes = new XMLMemo("TradeRoutes");
            XMLMemo userTradeRoutes = new XMLMemo("UserTradeRoutes");

            for (TradeRoute tr : trs) {

                TradeRouteExt tre = new TradeRouteExt(tr);
                XMLMemo tradeRoute = new XMLMemo("TradeRoute");
                tradeRoute.addAttribute("id", tr.getId());

                // tradeRoute.addAttribute("ressourceName", ML.getMLStr(ressourceDAO.findRessourceById(tr.getRessourceId()).getName(), userId));
                // tradeRoute.addAttribute("ressourceId", tr.getRessourceId());

                // Security check on start and target data
                PlayerPlanet ppStart = playerPlanetDAO.findByPlanetId(tr.getStartPlanet());
                PlayerPlanet ppTarget = playerPlanetDAO.findByPlanetId(tr.getTargetPlanet());
                if (ppStart == null) {
                    DebugBuffer.warning("Invalid start playerplanet (" + tr.getStartPlanet() + ") found for traderoute " + tre.getBase().getId());
                    continue;
                }
                if (ppStart == null) {
                    DebugBuffer.warning("Invalid target playerplanet (" + tr.getTargetPlanet() + ") found for traderoute " + tre.getBase().getId());
                    continue;
                }

                tradeRoute.addAttribute("startPlanetId", tr.getStartPlanet());
                tradeRoute.addAttribute("startPlanet", ppStart.getName());
                RelativeCoordinate startPlanetCoordinates = new RelativeCoordinate(0, tr.getStartPlanet());
                tradeRoute.addAttribute("startSystem", systemDAO.findById(planetDAO.findById(tr.getStartPlanet()).getSystemId()).getName());
                tradeRoute.addAttribute("startSystemId", planetDAO.findById(tr.getStartPlanet()).getSystemId());
                tradeRoute.addAttribute("startX", startPlanetCoordinates.toAbsoluteCoordinate().getX());
                tradeRoute.addAttribute("startY", startPlanetCoordinates.toAbsoluteCoordinate().getY());
                tradeRoute.addAttribute("endSystemId", planetDAO.findById(tr.getTargetPlanet()).getSystemId());
                tradeRoute.addAttribute("endPlanetId", tr.getTargetPlanet());
                tradeRoute.addAttribute("endPlanet", ppTarget.getName());
                RelativeCoordinate endPlanetCoordinates = new RelativeCoordinate(0, tr.getTargetPlanet());
                tradeRoute.addAttribute("endSystem", systemDAO.findById(planetDAO.findById(tr.getTargetPlanet()).getSystemId()).getName());
                tradeRoute.addAttribute("endX", endPlanetCoordinates.toAbsoluteCoordinate().getX());
                tradeRoute.addAttribute("endY", endPlanetCoordinates.toAbsoluteCoordinate().getY());
                if (tr.getTargetUserId() > 0) {
                    tradeRoute.addAttribute("external", "true");
                } else {
                    tradeRoute.addAttribute("external", "false");
                }
                tradeRoute.addAttribute("status", tr.getStatus().toString());
                int capacity = (int) ((tr.getEfficiency() / 100d) * tr.getCapacity());
                tradeRoute.addAttribute("capacity", capacity);

                for (TradeRouteDetail trd : tre.getRouteDetails()) {
                    XMLMemo tradeRouteDetail = new XMLMemo("TradeRouteDetail");
                    tradeRouteDetail.addAttribute("lastTransport", trd.getLastTransport());
                    tradeRouteDetail.addAttribute("reversed", trd.getReversed().toString());
                    tradeRouteDetail.addAttribute("ressId", trd.getRessId());
                    tradeRoute.addChild(tradeRouteDetail);
                }

                // tradeRoute.addAttribute("status", tr.getStatus());


                userTradeRoutes.addChild(tradeRoute);
            }
            tradeRoutes.addChild(userTradeRoutes);


            starMap.addChild(tradeRoutes);

        } catch (Exception e) {
            e.printStackTrace();
            DebugBuffer.addLine(DebugLevel.ERROR, "Error while Adding TradeRoutes to XML-Info : " + e);
        }
        return starMap;
    }

    public XMLMemo addScannedFleets(XMLMemo fleets, StarMapInfoData smid, FleetFormationCheck ffc) {


        HashMap<Integer, ArrayList<Integer>> systemFleets = new HashMap<Integer, ArrayList<Integer>>();
        for (PlayerFleet pf : (ArrayList<PlayerFleet>) playerFleetDAO.findAll()) {
            if (!smid.getSharingUsers().contains(pf.getUserId())) {
                continue;
            }
            ArrayList<Integer> planetFleets = systemFleets.get(pf.getSystemId());
            if (planetFleets == null) {
                planetFleets = new ArrayList<Integer>();
            }

            if (!planetFleets.contains(pf.getPlanetId())) {
                planetFleets.add(pf.getPlanetId());
                systemFleets.put(pf.getSystemId(), planetFleets);
            }

        }

        XMLMemo enemyFleets = new XMLMemo("enemyFleets");
        for (User u : (ArrayList<User>) userDAO.findAllNonSystem()) {
            //Skipping user and his allies
            if (debug) {
                log.debug("userId : " + u.getUserId());
                log.debug("comparing : " + u.getUserId() + " to : " + userId);
            }
            if (u.getUserId() == userId) {
                continue;
            }


            if (debug) {
                log.debug("Found Enemy Fleet");
            }
            //Gettin Fleets for User
            ArrayList<IFleet> combinedList = new ArrayList<IFleet>();
            FleetListResult flr = FleetService.getFleetList(u.getUserId(), 0, 0, EFleetViewType.OWN_ALL);
            FleetListResult flr2 = FleetService.getFleetList(u.getUserId(), 0, 0, EFleetViewType.OWN_ALL_WITH_ACTION);

            /*
             ArrayList<PlayerFleetExt> list = flr.getSingleFleets();
             list.addAll(flr2.getSingleFleets());
             ArrayList<FleetFormationExt> ffList = flr.getFleetFormations();
             ffList.addAll(flr2.getFleetFormations());
             */
            combinedList.addAll(flr.getAllItems());
            combinedList.addAll(flr2.getAllItems());

            for (IFleet iFle : combinedList) {
                if (iFle instanceof FleetFormationExt) {
                    if (ffc.alreadyDisplayed(iFle.getId())) {
                        continue;
                    }

                    if ((ffc.userIsContributing(iFle.getId()) && (iFle.getUserId() == userId)) || ffc.isAlliedFormation(iFle.getId())) {
                        continue;
                    }

                    ffc.setDisplayed(iFle.getId());
                } else if (iFle instanceof PlayerFleetExt) {
                    if (displayedFleets.contains(iFle.getId())) {
                        continue;
                    }

                    displayedFleets.add(iFle.getId());
                }

                //If not moving skip
                DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRel(userId, iFle.getUserId());
                if (dr.getDiplomacyTypeId() == DiplomacyType.ALLY) {
                    continue;
                }

                /*if (iFle.getSpeed() <= 1d) {
                 continue;
                 }*/

                if (!iFle.isMoving()) {

                    if (debug) {
                        log.debug("chcek for fleet : " + iFle.getName());
                        log.debug("4");
                        log.debug("IF ENEMY");
                    }

                    boolean scannerPhalanxFound = false;
                    if (smid.getPlanetList().get(iFle.getRelativeCoordinate().getSystemId()) != null) {
                        for (Planet p : smid.getPlanetList().get(iFle.getRelativeCoordinate().getSystemId())) {
                            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
                            if (pp != null) {
                                if (pp.getUserId() != userId && !smid.getSharingUsers().contains(pp.getUserId())) {
                                    continue;
                                } else {
                                    if (pp.getUserId() != userId && AllianceUtilities.isTrial(userId)) {
                                        continue;
                                    }
                                    if (planetConstructionDAO.findBy(p.getId(), Construction.ID_SCANNERPHALANX) != null
                                            && (pp.getUserId() == userId || smid.getSharingUsers().contains(pp.getUserId()))) {
                                        scannerPhalanxFound = true;
                                        if (debug) {
                                            log.debug("Found phalanx on planet : " + pp.getName() + " id : " + pp.getPlanetId() + " sysId : " + p.getSystemId());
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    boolean fleetFound = false;

                    if (systemFleets.get(iFle.getRelativeCoordinate().getSystemId()) != null && systemFleets.get(iFle.getRelativeCoordinate().getSystemId()).contains(iFle.getRelativeCoordinate().getPlanetId())) {
                        fleetFound = true;
                    }

                    boolean planetOwnerFound = false;

                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(iFle.getRelativeCoordinate().getPlanetId());
                    if (pp != null) {
                        if (smid.getSharingUsers().contains(pp.getUserId())) {
                            planetOwnerFound = true;
                        }
                    }

                    if (debug) {
                        log.debug("ScannerPhalanxFound : " + scannerPhalanxFound);
                    }
                    if (scannerPhalanxFound || fleetFound || planetOwnerFound) {
                        System.out.println("DISPLAY FLEET " + iFle.getId() + " " + iFle.getName() + " [" + iFle.getUserId() + "] CAUSE OF " + scannerPhalanxFound + "/" + fleetFound + "/" + planetOwnerFound);

                        try {
                            XMLMemo fleet = new XMLMemo("Fleet");
                            fleet.addAttribute("id", iFle.getId());
                            fleet.addAttribute("name", "???");
                            fleet.addAttribute("owner", IdToNameService.getUserName(iFle.getUserId()));
                            fleet.addAttribute("speed", "999");
                            fleet.addAttribute("startX", iFle.getAbsoluteCoordinate().getX());
                            fleet.addAttribute("startY", iFle.getAbsoluteCoordinate().getY());
                            fleet.addAttribute("endX", iFle.getAbsoluteCoordinate().getX());
                            fleet.addAttribute("endY", iFle.getAbsoluteCoordinate().getY());
                            String name = "Planet #" + iFle.getRelativeCoordinate().getPlanetId();

                            if (pp != null) {
                                name = pp.getName();
                            }
                            fleet.addAttribute("endPlanet", name);
                            fleet.addAttribute("time", "0");

                            fleet.addAttribute("endPlanetId", iFle.getRelativeCoordinate().getPlanetId());
                            fleet.addAttribute("endSystemId", iFle.getRelativeCoordinate().getSystemId());

                            if (vtMap.containsKey(iFle.getRelativeCoordinate().getSystemId())) {
                                fleet.addAttribute("endSystem", systemDAO.findById(iFle.getRelativeCoordinate().getSystemId()).getName());
                            } else {
                                fleet.addAttribute("endSystem", "System " + systemDAO.findById(iFle.getRelativeCoordinate().getSystemId()));
                            }

                            if (iFle instanceof FleetFormationExt) {
                                fleet.addAttribute("ff", "1");
                                if (iFle.getUserId() == userId) {
                                    fleet.addAttribute("ffLeader", "1");
                                } else {
                                    fleet.addAttribute("ffLeader", "0");
                                }

                                if (ffc.userIsContributing(iFle.getId())) {
                                    fleet.addAttribute("participating", "1");
                                } else {
                                    fleet.addAttribute("participating", "0");
                                }
                            } else {
                                fleet.addAttribute("ff", "0");
                            }

                            fleet.addAttribute("status", (dr.getDiplomacyTypeId() + 10));
                            XMLMemo ships = new XMLMemo("Ships");
                            XMLMemo chassis = new XMLMemo("ShipChassis");

                            for (ShipData shipData : iFle.getShipList()) {
                                String chassisName = ML.getMLStr(chassisDAO.findById(shipData.getChassisSize()).getName(), userId);

                                //log.debug("in Here: ");


                                XMLMemo chassisDetail = new XMLMemo("ChassisDetail");
                                chassisDetail.addAttribute("chassisname", chassisName);
                                chassisDetail.addAttribute("chassisid", shipData.getChassisSize());
                                //		<ChassisDetail chassisid="6" chassisname="Schlachtschiff" count="1" designname="SS-Trup" />
                                chassisDetail.addAttribute("count", shipData.getCount());
                                //log.debug("Adding to chassis: " + shipData.getDesignName());
                                chassis.addChild(chassisDetail);

                            }
                            ships.addChild(chassis);
                            fleet.addChild(ships);
                            enemyFleets.addChild(fleet);
                        } catch (Exception e) {
                            DebugBuffer.warning("Fleet " + iFle.getId() + " caused major spacetime rupture -- MOVING?" + iFle.isMoving() + " AC: " + iFle.getAbsoluteCoordinate() + " RC: " + iFle.getRelativeCoordinate());
                        }
                    }
                } else {
                    if (debug) {
                        log.debug("ELSE ENEMY");
                    }

                    // This checks have no meaning if current player is allied (treaty) to this player
                    boolean display = false;

                    if ((dr.getDiplomacyTypeId() > DiplomacyType.TREATY) || !dr.getSharingMap() || dr.getPending()) {
                        // If 200 LY arount the current fleet at least 1 hyperscanner exists
                        // Skip sublight fleets - no hyperspace emission
                        if (iFle.getSpeed() < 1d) {
                            continue;
                        }

                        // Skip system internal flights - are not done with hyperspace :)
                        if (iFle.getSourceRelativeCoordinate().getSystemId() == iFle.getTargetRelativeCoordinate().getSystemId()) {
                            continue;
                        }
                    } else {
                        display = true;
                    }

                    if ((smid.getHyperArea().getItemsAround(iFle.getAbsoluteCoordinate().getX(), iFle.getAbsoluteCoordinate().getY(), HYPERSCANNER_RANGE).size() > 0) || display) {
                        XMLMemo fleet = new XMLMemo("Fleet");
                        fleet.addAttribute("id", iFle.getId());
                        fleet.addAttribute("name", StarMapInfo.secString(iFle.getName()));
                        fleet.addAttribute("owner", userDAO.findById(iFle.getUserId()).getGameName());
                        fleet.addAttribute("speed", String.valueOf((Math.round(iFle.getSpeed() * 100d)) / 100d));

                        fleet.addAttribute("startX", iFle.getAbsoluteCoordinate().getX());
                        fleet.addAttribute("startY", iFle.getAbsoluteCoordinate().getY());
                        int endX = iFle.getTargetAbsoluteCoordinate().getX();
                        int endY = iFle.getTargetAbsoluteCoordinate().getY();
                        int startX = iFle.getAbsoluteCoordinate().getX();
                        int startY = iFle.getAbsoluteCoordinate().getY();
                        String endSystem = "";

                        int endSystemId = iFle.getTargetRelativeCoordinate().getSystemId();
                        float distance = iFle.getETA() * (float) iFle.getSpeed();
                        int timeLeft = iFle.getETA();

                        boolean targetVisible = false;

                        if ((distance > FLEET_MAXIMUM_VISIBLE_RANGE) && !display) {

                            timeLeft = (int) (FLEET_MAXIMUM_VISIBLE_RANGE / iFle.getSpeed());
                            int tmpX = 0;
                            int tmpY = 0;

                            float ratio = FLEET_MAXIMUM_VISIBLE_RANGE / distance;

                            tmpX = endX - startX;
                            tmpY = endY - startY;

                            tmpX *= ratio;
                            tmpY *= ratio;

                            if (timeLeft % 2 > 0) {

                                tmpX = (int) Math.floor(tmpX + (distance % 5));
                                tmpY = (int) Math.floor(tmpY - (timeLeft % 5));
                            } else {

                                tmpX = (int) Math.floor(tmpX - (timeLeft % 5));
                                tmpY = (int) Math.floor(tmpY + (distance % 5));
                            }


                            endX = tmpX + startX;
                            endY = startY + tmpY;
                            startX += tmpY;
                            startY += tmpX;
                            endSystem = " ";
                            endSystemId = 0;
                        } else {
                            targetVisible = true;
                            if (vtMap.containsKey(iFle.getTargetRelativeCoordinate().getSystemId())) {
                                endSystem = systemDAO.findById(iFle.getTargetRelativeCoordinate().getSystemId()).getName();
                            } else {
                                endSystem = "System " + systemDAO.findById(iFle.getTargetRelativeCoordinate().getSystemId()).getId();
                            }
                        }

                        fleet.addAttribute("targetVisible", String.valueOf(targetVisible));
                        fleet.addAttribute("endX", endX);
                        fleet.addAttribute("endY", endY);
                        fleet.addAttribute("endSystem", endSystem);
                        fleet.addAttribute("endSystemId", endSystemId);
                        fleet.addAttribute("time", timeLeft);
                        fleet.addAttribute("status", (dr.getDiplomacyTypeId() + 10));

                        fleet.addAttribute("endPlanet", "");
                        fleet.addAttribute("endPlanetId", "0");                        
                        
                        if (iFle instanceof FleetFormationExt) {
                            fleet.addAttribute("ff", "1");
                            if (iFle.getUserId() == userId) {
                                fleet.addAttribute("ffLeader", "1");
                            } else {
                                fleet.addAttribute("ffLeader", "0");
                            }
                        } else {
                            fleet.addAttribute("ff", "0");
                        }

                        XMLMemo ships = new XMLMemo("Ships");
                        XMLMemo chassis = new XMLMemo("ShipChassis");


                        for (ShipData shipData : iFle.getShipList()) {
                            ShipDesign sd = shipDesignDAO.findById(shipData.getDesignId());
                            //log.debug("in Here: ");

                            Chassis chass = chassisDAO.findById(sd.getChassis());

                            String chassisName = ML.getMLStr(chass.getName(), userId);
                            XMLMemo chassisDetail = new XMLMemo("ChassisDetail");
                            chassisDetail.addAttribute("designname", "");
                            chassisDetail.addAttribute("chassisname", chassisName);
                            chassisDetail.addAttribute("chassisid", sd.getChassis());
                            chassisDetail.addAttribute("count", shipData.getCount());
                            //log.debug("Adding to chassis: " + shipData.getDesignName());
                            chassis.addChild(chassisDetail);

                        }
                        ships.addChild(chassis);
                        fleet.addChild(ships);
                        enemyFleets.addChild(fleet);

                    }
                }
            }
        }
        fleets.addChild(enemyFleets);
        return fleets;
    }

    public XMLMemo addOwnFleets(XMLMemo fleets, FleetFormationCheck ffc) {
        FleetListResult flr = FleetService.getFleetList(userId, 0, 0, EFleetViewType.OWN_ALL);
        FleetListResult flr2 = FleetService.getFleetList(userId, 0, 0, EFleetViewType.OWN_ALL_WITH_ACTION);

        ArrayList<IFleet> combinedList = new ArrayList<IFleet>();
        combinedList.addAll(flr.getAllItems());
        combinedList.addAll(flr2.getAllItems());

        XMLMemo ownFleets = new XMLMemo("ownFleets");

        for (IFleet iFle : combinedList) {
            if (iFle instanceof FleetFormationExt) {
                ffc.addContributeTo(iFle.getId());
                if (iFle.getUserId() != userId) {
                    continue;
                }
                ffc.setDisplayed(iFle.getId());
            } else {
                displayedFleets.add(iFle.getId());
            }
            XMLMemo fleet = new XMLMemo("Fleet");
            // log.debug("Adding fleet " + fleets.getString(10) + "("+fleets.getInt(12)+")");
            if (iFle.isMoving()) {
                fleet.addAttribute("id", iFle.getId());
                fleet.addAttribute("name", StarMapInfo.secString(iFle.getName()));
                fleet.addAttribute("owner", IdToNameService.getUserName(iFle.getUserId()));
                fleet.addAttribute("speed", String.valueOf((Math.round(iFle.getSpeed() * 100d)) / 100d));

                fleet.addAttribute("startX", iFle.getAbsoluteCoordinate().getX());
                fleet.addAttribute("startY", iFle.getAbsoluteCoordinate().getY());
                fleet.addAttribute("endX", iFle.getTargetAbsoluteCoordinate().getX());
                fleet.addAttribute("endY", iFle.getTargetAbsoluteCoordinate().getY());

                String planetName = "Planet #" + iFle.getTargetRelativeCoordinate().getPlanetId();

                PlayerPlanet pp = playerPlanetDAO.findByPlanetId(iFle.getTargetRelativeCoordinate().getPlanetId());
                if (pp != null) {
                    planetName = pp.getName();
                }
                fleet.addAttribute("endPlanet", planetName);
                fleet.addAttribute("time", iFle.getETA());

                fleet.addAttribute("endPlanetId", iFle.getTargetRelativeCoordinate().getPlanetId());
                fleet.addAttribute("endSystemId", iFle.getTargetRelativeCoordinate().getSystemId());

                if (vtMap.containsKey(iFle.getTargetRelativeCoordinate().getSystemId())) {
                    fleet.addAttribute("endSystem", systemDAO.findById(iFle.getTargetRelativeCoordinate().getSystemId()).getName());
                } else {
                    fleet.addAttribute("endSystem", "System " + iFle.getTargetRelativeCoordinate().getSystemId());
                }
                fleet.addAttribute("status", FLEET_OWN);

                if (iFle instanceof FleetFormationExt) {
                    fleet.addAttribute("ff", "1");
                    if (iFle.getUserId() == userId) {
                        fleet.addAttribute("ffLeader", "1");
                    } else {
                        fleet.addAttribute("ffLeader", "0");
                    }
                } else {
                    fleet.addAttribute("ff", "0");
                }
            } else {

                fleet.addAttribute("id", iFle.getId());
                fleet.addAttribute("name", StarMapInfo.secString(iFle.getName()));
                fleet.addAttribute("owner", userDAO.findById(iFle.getUserId()).getGameName());
                fleet.addAttribute("speed", String.valueOf((Math.round(iFle.getSpeed() * 100d)) / 100d));
                fleet.addAttribute("startX", iFle.getAbsoluteCoordinate().getX());
                fleet.addAttribute("startY", iFle.getAbsoluteCoordinate().getY());
                fleet.addAttribute("endX", iFle.getAbsoluteCoordinate().getX());
                fleet.addAttribute("endY", iFle.getAbsoluteCoordinate().getY());
                fleet.addAttribute("scanDuration", iFle.getScanDuration());
                fleet.addAttribute("canScanSystem", String.valueOf(iFle.canScanSystem()));
                fleet.addAttribute("canScanPlanet", String.valueOf(iFle.canScanPlanet()));
                PlayerPlanet pp = playerPlanetDAO.findByPlanetId(iFle.getRelativeCoordinate().getPlanetId());
                String name = "Planet #" + iFle.getRelativeCoordinate().getPlanetId();
                if (pp != null) {
                    name = pp.getName();
                }
                fleet.addAttribute("endPlanet", name);
                fleet.addAttribute("time", "0");

                fleet.addAttribute("endPlanetId", iFle.getRelativeCoordinate().getPlanetId());
                fleet.addAttribute("endSystemId", iFle.getRelativeCoordinate().getSystemId());

                fleet.addAttribute("endSystem", systemDAO.findById(iFle.getRelativeCoordinate().getSystemId()).getName());
                fleet.addAttribute("status", FLEET_OWN);

                if (iFle instanceof FleetFormationExt) {
                    fleet.addAttribute("ff", "1");
                    if (iFle.getUserId() == userId) {
                        fleet.addAttribute("ffLeader", "1");
                    } else {
                        fleet.addAttribute("ffLeader", "0");
                    }
                } else {
                    fleet.addAttribute("ff", "0");
                }
            }
            int range = Integer.MAX_VALUE;

            XMLMemo ships = new XMLMemo("Ships");
            XMLMemo loading = new XMLMemo("Loading");

            XMLMemo chassis = new XMLMemo("ShipChassis");

            //log.debug("Trying to get ShipData for Id: " + fleetData.getFleetId());
            //log.debug("Size of Shipdata: " + (LinkedList) ShipUtilities.getShipList(fleetData.getFleetId()));

            //Leere Flotte
            if (iFle.getShipList().isEmpty()) {
                continue;
            }

            boolean instellarFlightPossible = iFle.canFlyInterstellar();
            if (!instellarFlightPossible) range = 0;

            for (ShipData shipData : iFle.getShipList()) {
                String chassisName = "Kein Chassis";
                //log.debug("in Here: ");

                //log.debug("chassis: : " + shipData.getChassisSize());
                ShipDesign shipDesign = shipDesignDAO.findById(shipData.getDesignId());

                range = Math.min(shipDesign.getRange(), range);

                Chassis chass = chassisDAO.findById(shipDesign.getChassis());

                chassisName = ML.getMLStr(chass.getName(), userId);

                XMLMemo chassisDetail = new XMLMemo("ChassisDetail");
                chassisDetail.addAttribute("designname", StarMapInfo.secString(shipData.getDesignName()));
                chassisDetail.addAttribute("chassisname", chassisName);
                chassisDetail.addAttribute("chassisid", chass.getId());

                chassisDetail.addAttribute("count", shipData.getCount());
                //log.debug("Adding to chassis: " + shipData.getDesignName());
                addLoadedShips(iFle, shipDesign, chassisDetail);

                chassis.addChild(chassisDetail);

            }
            if (range != Integer.MAX_VALUE) {
                fleet.addAttribute("range", range);
            }

            //log.debug("Adding Loading");
            ships.addChild(loading);
            //log.debug("Adding chassis");
            ships.addChild(chassis);
            //log.debug("Adding ships");
            fleet.addChild(ships);
            //log.debug("Adding Fleet " + fleetData.getFleetId());
            ownFleets.addChild(fleet);

        }

        //log.debug("Adding OWNFLEET TAG");

        fleets.addChild(ownFleets);

        return fleets;




    }

    public XMLMemo addAlliedFleets(XMLMemo fleets, FleetFormationCheck ffc) {
        ArrayList<Integer> allys = new ArrayList<Integer>();
        AllianceMember am = allianceMemberDAO.findByUserId(userId);
        if (am != null && !am.getIsTrial()) {
            allys = AllianceUtilities.getAllAlliedUsers(userId);
            for (User u : (ArrayList<User>) userDAO.findAllNonSystem()) {
                if (!allys.contains(u.getUserId())) {
                    DiplomacyResult dr = DiplomacyUtilities.getDiplomacyResult(u.getUserId(), userId);

                    if (dr == null) {
                        continue;
                    }
                    if (DiplomacyUtilities.hasAllianceRelation(userId, u.getUserId())) {
                        if (!allys.contains(u.getUserId())) {
                            allys.add(u.getUserId());
                        }
                    }
                }
            }
        }

        if (debug) {
            log.debug("userId : " + userId);
        }
        ArrayList<PlayerFleetExt> lst = new ArrayList<PlayerFleetExt>();
        ArrayList<FleetFormationExt> ffLst = new ArrayList<FleetFormationExt>();
        ArrayList<IFleet> combinedList = new ArrayList<IFleet>();

        for (Integer i : allys) {
            if (i == userId) {
                continue;
            }
            FleetListResult flrTmp = FleetService.getFleetList(i, 0, 0, EFleetViewType.OWN_ALL);
            FleetListResult flrTmp2 = FleetService.getFleetList(i, 0, 0, EFleetViewType.OWN_ALL_WITH_ACTION);
            /*
             lst.addAll(flrTmp.getSingleFleets());
             lst.addAll(flrTmp2.getSingleFleets());
             ffLst.addAll(flrTmp.getFleetFormations());
             ffLst.addAll(flrTmp2.getFleetFormations());
             */
            // combinedList.addAll(lst);
            // combinedList.addAll(ffLst);
            combinedList.addAll(flrTmp.getAllItems());
            combinedList.addAll(flrTmp2.getAllItems());
        }
        if (debug) {
            log.debug("flr : " + lst.size());
        }

        XMLMemo alliedFleets = new XMLMemo("alliedFleets");
        if (combinedList.isEmpty()) {
            fleets.addChild(alliedFleets);
            return fleets;
        }

        for (IFleet iFle : combinedList) {
            try {
                if (iFle instanceof FleetFormationExt) {
                    if (ffc.alreadyDisplayed(iFle.getId())) {
                        continue;
                    }

                    if (ffc.userIsContributing(iFle.getId()) && (iFle.getUserId() == userId)) {
                        continue;
                    }
                    ffc.setDisplayed(iFle.getId());
                } else if (iFle instanceof PlayerFleetExt) {
                    if (displayedFleets.contains(iFle.getId())) {
                        continue;
                    }

                    displayedFleets.add(iFle.getId());
                }

                if (debug) {
                    log.debug("a");
                }
                if (iFle.getUserId() == userId) {
                    continue;
                }
                if (debug) {
                    log.debug("b");
                }
                //Skip intersystem Fleets
                if (iFle.isMoving() && iFle.getSourceRelativeCoordinate().getSystemId() == iFle.getTargetRelativeCoordinate().getSystemId()) {
                    continue;
                }
                if (debug) {
                    log.debug("c");
                }

                XMLMemo fleet = new XMLMemo("Fleet");
                if (debug) {
                    log.debug("d");
                }
                // log.debug("Adding fleet " + fleets.getString(10) + "("+fleets.getInt(12)+")");
                if (iFle.isMoving()) {
                    // Skip sublight fleets - no hyperspace emission

                    if (debug) {
                        log.debug("f");
                    }

                    if (debug) {
                        log.debug("g");
                    }
                    // Skip system internal flights - are not done with hyperspace :)
                    if (iFle.getSourceRelativeCoordinate().getSystemId() == iFle.getTargetRelativeCoordinate().getSystemId()) {

                        if (debug) {
                            log.debug("h");
                        }
                        continue;
                    }

                    if (debug) {
                        log.debug("i");
                    }
                    fleet.addAttribute("id", iFle.getId());
                    fleet.addAttribute("name", StarMapInfo.secString(iFle.getName()));
                    fleet.addAttribute("owner", userDAO.findById(iFle.getUserId()).getGameName());
                    fleet.addAttribute("speed", String.valueOf((Math.round(iFle.getSpeed() * 100d)) / 100d));

                    if (debug) {
                        log.debug("j");
                    }
                    fleet.addAttribute("startX", iFle.getAbsoluteCoordinate().getX());
                    fleet.addAttribute("startY", iFle.getAbsoluteCoordinate().getY());
                    fleet.addAttribute("endX", iFle.getTargetAbsoluteCoordinate().getX());
                    fleet.addAttribute("endY", iFle.getTargetAbsoluteCoordinate().getY());

                    String planetName = "Planet #" + iFle.getTargetRelativeCoordinate().getPlanetId();

                    if (debug) {
                        log.debug("k");
                    }
                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(iFle.getTargetRelativeCoordinate().getPlanetId());
                    if (pp != null) {
                        planetName = pp.getName();
                    }
                    fleet.addAttribute("endPlanet", planetName);
                    fleet.addAttribute("time", iFle.getETA());

                    fleet.addAttribute("endPlanetId", iFle.getTargetRelativeCoordinate().getPlanetId());
                    fleet.addAttribute("endSystemId", iFle.getTargetRelativeCoordinate().getSystemId());
                    DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRel(userId, iFle.getUserId());

                    if (vtMap.containsKey(iFle.getTargetRelativeCoordinate().getSystemId())) {
                        fleet.addAttribute("endSystem", systemDAO.findById(iFle.getTargetRelativeCoordinate().getSystemId()).getName());
                    } else {
                        fleet.addAttribute("endSystem", "System " + iFle.getTargetRelativeCoordinate().getSystemId());
                    }
                    fleet.addAttribute("status", (dr.getDiplomacyTypeId() + 10));

                    if (iFle instanceof FleetFormationExt) {
                        fleet.addAttribute("ff", "1");
                        if (iFle.getUserId() == userId) {
                            fleet.addAttribute("ffLeader", "1");
                        } else {
                            fleet.addAttribute("ffLeader", "0");
                        }

                        if (ffc.userIsContributing(iFle.getId())) {
                            fleet.addAttribute("participating", "1");
                        } else {
                            fleet.addAttribute("participating", "0");
                        }
                    } else {
                        fleet.addAttribute("ff", "0");
                    }

                    if (debug) {
                        log.debug("l");
                    }
                } else {

                    if (debug) {
                        log.debug("m");
                    }
                    fleet.addAttribute("id", iFle.getId());
                    fleet.addAttribute("name", StarMapInfo.secString(iFle.getName()));
                    fleet.addAttribute("owner", userDAO.findById(iFle.getUserId()).getGameName());
                    fleet.addAttribute("speed", String.valueOf(iFle.getSpeed()));
                    fleet.addAttribute("startX", iFle.getAbsoluteCoordinate().getX());
                    fleet.addAttribute("startY", iFle.getAbsoluteCoordinate().getY());
                    fleet.addAttribute("endX", iFle.getAbsoluteCoordinate().getX());
                    fleet.addAttribute("endY", iFle.getAbsoluteCoordinate().getY());
                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(iFle.getRelativeCoordinate().getPlanetId());
                    String name = "Planet #" + iFle.getRelativeCoordinate().getPlanetId();
                    if (pp != null) {
                        name = pp.getName();
                    }
                    fleet.addAttribute("endPlanet", name);
                    fleet.addAttribute("time", "0");

                    fleet.addAttribute("endPlanetId", iFle.getRelativeCoordinate().getPlanetId());
                    fleet.addAttribute("endSystemId", iFle.getRelativeCoordinate().getSystemId());

                    fleet.addAttribute("endSystem", systemDAO.findById(iFle.getRelativeCoordinate().getSystemId()).getName());
                    fleet.addAttribute("status", FLEET_ALLIED);

                    if (iFle instanceof FleetFormationExt) {
                        fleet.addAttribute("ff", "1");
                        if (iFle.getUserId() == userId) {
                            fleet.addAttribute("ffLeader", "1");
                        } else {
                            fleet.addAttribute("ffLeader", "0");
                        }

                        if (ffc.userIsContributing(iFle.getId())) {
                            fleet.addAttribute("participating", "1");
                        } else {
                            fleet.addAttribute("participating", "0");
                        }
                    } else {
                        fleet.addAttribute("ff", "0");
                    }

                    if (debug) {
                        log.debug("n");
                    }
                }

                XMLMemo ships = new XMLMemo("Ships");
                XMLMemo loading = new XMLMemo("Loading");

                XMLMemo chassis = new XMLMemo("ShipChassis");

                //log.debug("Trying to get ShipData for Id: " + fleetData.getFleetId());
                //log.debug("Size of Shipdata: " + (LinkedList) ShipUtilities.getShipList(fleetData.getFleetId()));

                if (iFle.getShipList().isEmpty()) {
                    continue;
                }
                for (ShipData shipData : iFle.getShipList()) {
                    String chassisName = "Kein Chassis";
                    //log.debug("in Here: ");

                    //log.debug("chassis: : " + shipData.getChassisSize());
                    ShipDesign shipDesign = shipDesignDAO.findById(shipData.getDesignId());
                    Chassis chass = chassisDAO.findById(shipDesign.getChassis());

                    chassisName = ML.getMLStr(chass.getName(), userId);

                    XMLMemo chassisDetail = new XMLMemo("ChassisDetail");
                    chassisDetail.addAttribute("designname", StarMapInfo.secString(shipData.getDesignName()));
                    chassisDetail.addAttribute("chassisname", chassisName);
                    chassisDetail.addAttribute("chassisid", chass.getId());

                    chassisDetail.addAttribute("count", shipData.getCount());


                    addLoadedShips(iFle, shipDesign, chassisDetail);
                    //log.debug("Adding to chassis: " + shipData.getDesignName());
                    chassis.addChild(chassisDetail);

                }
                /*
                 FleetData extInf = ShipUtilities.prepareLoadingInformation(fleetData.getFleetId());
                 for (ShipStorage shipStorage : s) {
                 //log.debug("8b");

                 XMLMemo loadingEntry = new XMLMemo("LoadingEntry");
                 loadingEntry.addAttribute("name", shipStorage.getName());
                 loadingEntry.addAttribute("count", shipStorage.getCount());
                 loadingEntry.addAttribute("type", shipStorage.getLoadtype());
                 loadingEntry.addAttribute("id", shipStorage.getId());
                 loading.addChild(loadingEntry);

                 }
                 *
                 */
                //log.debug("Adding Loading");
                ships.addChild(loading);
                //log.debug("Adding chassis");
                ships.addChild(chassis);
                //log.debug("Adding ships");
                fleet.addChild(ships);
                //log.debug("Adding Fleet " + fleetData.getFleetId());
                alliedFleets.addChild(fleet);

            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Inconsistent FleetEntry for Fleet : " + iFle.getId() + " " + e);
            }
        }

        //log.debug("Adding OWNFLEET TAG");

        fleets.addChild(alliedFleets);

        return fleets;
    }

    public XMLMemo addHyperScanner(XMLMemo starMap, StarMapInfoData smid) {

        HashMap<Integer, ArrayList<Hyperscanner>> hyperscanners = smid.getHyperscanners();

        AllianceMember am = allianceMemberDAO.findByUserId(userId);


        XMLMemo hyperScanner = new XMLMemo("Hyperscanner");


        for (Map.Entry<Integer, ArrayList<Hyperscanner>> entry0 : hyperscanners.entrySet()) {
            if (am != null && am.getIsTrial() && entry0.getKey() != userId) {
                continue;
            }
            for (Hyperscanner entry : entry0.getValue()) {
                XMLMemo scanner = new XMLMemo("Scanner");
                scanner.addAttribute("x", entry.getSystem().getX());
                scanner.addAttribute("y", entry.getSystem().getY());
                scanner.addAttribute("range", entry.getScanRange());

                if (entry0.getKey() == userId) {
                    scanner.addAttribute("status", 0);


                } else {
                    scanner.addAttribute("status", 1);


                }
                hyperScanner.addChild(scanner);



            }
        }
        starMap.addChild(hyperScanner);


        return starMap;



    }

    public XMLMemo addObservatory(XMLMemo starMap, StarMapInfoData smid) {

        XMLMemo observatory = new XMLMemo("Observatorys");
        HashMap<Integer, ArrayList<at.darkdestiny.core.model.System>> observatories = smid.getObservatories();


        for (Map.Entry<Integer, ArrayList<at.darkdestiny.core.model.System>> entry0 : observatories.entrySet()) {

            for (at.darkdestiny.core.model.System entry : entry0.getValue()) {
                XMLMemo scanner = new XMLMemo("Observatory");
                scanner.addAttribute("x", entry.getX());
                scanner.addAttribute("y", entry.getY());


                if (entry0.getKey() == userId) {
                    scanner.addAttribute("status", 0);
                } else {
                    scanner.addAttribute("status", 1);
                }
                observatory.addChild(scanner);

            }
        }
        starMap.addChild(observatory);
        return starMap;
    }

    public XMLMemo addSunTransmitter(XMLMemo starMap) {
        XMLMemo suntransmitters = new XMLMemo("SunTransmitters");

        for (SunTransmitter transmitter : (ArrayList<SunTransmitter>) Service.sunTransmitterDAO.findAll()) {
            if (!viewTableDAO.containsSystem(userId, transmitter.getSystemId())) {
                continue;
            }

            boolean transitAllowed = false;
            int transmitterOwner = transmitter.getUserId();

            // Security check -- Check if wrong id and print warning
            PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(transmitter.getPlanetId());
            if (pp != null && !pp.getUserId().equals(transmitter.getUserId())) {
                DebugBuffer.warning("Suntransmitter has wrong assigned user id " + transmitterOwner + " (should be " + pp.getUserId() + ")");
                transmitterOwner = pp.getUserId();
                transmitter.setUserId(pp.getUserId());
                sunTransmitterDAO.update(transmitter);                
            }

            if (transmitterOwner == userId) {
                transitAllowed = true;
            } else {
                // Check if owner is allied
                transitAllowed = DiplomacyUtilities.hasAllianceRelation(userId, transmitterOwner);
            }

            XMLMemo suntransmitter = new XMLMemo("SunTransmitter");
            suntransmitter.addAttribute("id", transmitter.getId());
            suntransmitter.addAttribute("systemId", transmitter.getSystemId());
            suntransmitter.addAttribute("planetId", transmitter.getPlanetId());
            suntransmitter.addAttribute("userId", transmitterOwner);
            suntransmitter.addAttribute("transitAllowed", transitAllowed);
            XMLMemo targetTransmitters = new XMLMemo("TargetTransmitters");
            for(SunTransmitterRoute str : Service.sunTransmitterRouteDAO.findAll()){
                if(str.getEndId().equals(transmitter.getId())){
                    XMLMemo targetTransmitter = new XMLMemo(("TargetTransmitter"));
                    targetTransmitter.addAttribute("id", str.getStartId());
                    targetTransmitters.addChild(targetTransmitter);
                }else if(str.getStartId().equals(transmitter.getId())){
                    XMLMemo targetTransmitter = new XMLMemo(("TargetTransmitter"));
                    targetTransmitter.addAttribute("id", str.getEndId());
                    targetTransmitters.addChild(targetTransmitter);
                }
            }
            suntransmitter.addChild(targetTransmitters);
            suntransmitters.addChild(suntransmitter);
        }

        starMap.addChild(suntransmitters);

        //Suntransmitter-Routes

        XMLMemo suntransmittersroutes = new XMLMemo("SunTransmitterRoutes");

        for (SunTransmitterRoute transmitterRoute : (ArrayList<SunTransmitterRoute>) Service.sunTransmitterRouteDAO.findAll()) {
            int startSystemId = 0;
            int endSystemId = 0;

            try {
                startSystemId = sunTransmitterDAO.findById(transmitterRoute.getStartId()).getSystemId();
                endSystemId = sunTransmitterDAO.findById(transmitterRoute.getEndId()).getSystemId();
            } catch (Exception e) {
                DebugBuffer.error("Invalid cast (Transmitter Id ["+transmitterRoute.getStartId()+" | "+transmitterRoute.getEndId()+"] to System Id)", e);
                continue;
            }

            if (!viewTableDAO.containsSystem(userId, startSystemId) &&
                !viewTableDAO.containsSystem(userId, endSystemId)) {
                continue;
            }

            XMLMemo suntransmitterroute = new XMLMemo("SunTransmitterRoute");
            suntransmitterroute.addAttribute("id", transmitterRoute.getId());
            suntransmitterroute.addAttribute("startId", transmitterRoute.getStartId());
            suntransmitterroute.addAttribute("endId", transmitterRoute.getEndId());
            suntransmittersroutes.addChild(suntransmitterroute);


        }
        starMap.addChild(suntransmittersroutes);
        return starMap;
    }

    public static String secString(String input) {
        char[] inChars = input.toCharArray();
        char[] outChars = new char[inChars.length];

        for (int i = 0; i
                < inChars.length; i++) {
            if ((inChars[i] >= 65) && (inChars[i] <= 90)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 97) && (inChars[i] <= 122)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 35) && (inChars[i] <= 62)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 192) && (inChars[i] <= 255)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] == 45) || (inChars[i] == 46) || (inChars[i] == 95) || (inChars[i] == 32)) {
                outChars[i] = inChars[i];
                continue;
            }

            outChars[i] = 95;
        }

        StringBuffer sb = new StringBuffer();
        sb.append(outChars);

        return sb.toString();
    }

    private void addLoadedShips(IFleet iFle, ShipDesign shipDesign, XMLMemo chassisDetail) {
        ArrayList<HangarRelation> hrs = Service.hangarRelationDAO.findByFleetAndRelToDesign(iFle.getId(), shipDesign.getId());
        hrs.addAll(Service.hangarRelationDAO.findByFleetAndRelToDesignInternal(iFle.getId(), shipDesign.getId()));
        if (!hrs.isEmpty()) {
            XMLMemo loadedChassis = new XMLMemo("LoadedChassis");
            for (HangarRelation hr : hrs) {
                XMLMemo loadedChassisDetail = new XMLMemo("ChassisDetail");
                ShipDesign loadedShipDesign = shipDesignDAO.findById(hr.getDesignId());
                Chassis chass = chassisDAO.findById(loadedShipDesign.getChassis());
                String chassisName = ML.getMLStr(chass.getName(), userId);
                loadedChassisDetail.addAttribute("designname", StarMapInfo.secString(loadedShipDesign.getName()));
                loadedChassisDetail.addAttribute("chassisname", chassisName);
                loadedChassisDetail.addAttribute("chassisid", chass.getId());

                loadedChassisDetail.addAttribute("count", hr.getCount());
                loadedChassis.addChild(loadedChassisDetail);
            }
            chassisDetail.addChild(loadedChassis);
        }
    }
}
