/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.model.ProductionOrder;

/**
 *
 * @author Stefan
 */
public class ShipAbortOrder extends AbortOrder {
    public ShipAbortOrder(ProductionOrder po, int count, int userId, int planetId) {
        super(po,count, userId, planetId);
    }
}
