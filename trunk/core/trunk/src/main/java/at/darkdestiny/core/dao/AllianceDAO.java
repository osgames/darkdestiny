/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class AllianceDAO extends ReadWriteTable<Alliance> implements GenericDAO {

    public Alliance findById(Integer id) {
        Alliance a = new Alliance();
        a.setId(id);
        return (Alliance)get(a);
    }

    public ArrayList<Alliance> findAllSortedByName() {
        ArrayList<Alliance> result = new ArrayList<Alliance>();
        TreeMap<String, Alliance> sorted = new TreeMap<String, Alliance>();
        for(Alliance a : (ArrayList<Alliance>)findAll()){
            sorted.put(a.getName(), a);
        }
        result.addAll(sorted.values());
        return result;
    }

    public Alliance findAllianceByNameOrTag(String name) {
        Alliance a = new Alliance();
        a.setName(name);
        ArrayList<Alliance> result = find(a);
        if (result.size() > 0) {
            return result.get(0);
        }
        a = new Alliance();
        a.setTag(name);
        result = find(a);
        if (result.size() > 0) {
            return result.get(0);
        }else{
            return null;
        }


    }

    public ArrayList<Alliance> findByMasterId(Integer masterAllianceId) {
         Alliance a = new Alliance();
        a.setMasterAlliance(masterAllianceId);
        return find(a);
    }


    public ArrayList<Alliance> findByIsSubAllianceOf(Integer isSubAllianceOf) {
         Alliance a = new Alliance();
        a.setIsSubAllianceOf(isSubAllianceOf);
        return find(a);

    }
}
