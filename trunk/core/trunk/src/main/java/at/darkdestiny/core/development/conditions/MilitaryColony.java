/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.development.conditions;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class MilitaryColony implements IBonusCondition {
    @Override
    public boolean isPossible(int planetId) {
        return true;
    }
    
    @Override
    public boolean isPossible() {
        return false;
    }    
    
    @Override 
    public ArrayList<String> activeBoni(int bonusId, int planetId, int userId) {
        ArrayList<String> boniList = new ArrayList<String>();
        
        boniList.add("military placeholder");
        
        return boniList;
    }    
    
    @Override 
    public void onActivation(int bonusId, int planetId, int userId) {
        
    }    
}
