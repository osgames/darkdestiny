/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import java.util.ArrayList;

public class PropertiesCheckResult {

        private ArrayList<BaseResult> dbResult = new ArrayList<BaseResult>();
        private ArrayList<BaseResult> gameResult = new ArrayList<BaseResult>();

        
        private boolean isDbPropertiesFileOkay = true;
        private boolean isGamePropertiesFileOkay = true;
        
        
        /**
         * @return the propertiesResult
         */
        public ArrayList<BaseResult> getDbResult() {
            return dbResult;
        }


        /**
         * @return the gameResult
         */
        public ArrayList<BaseResult> getGameResult() {
            return gameResult;
        }

        public boolean isAllPropertiesFilesOkay(){
            return isDbPropertiesFileOkay && isGamePropertiesFileOkay;
        }
    /**
     * @return the isDbPropertiesFileOkay
     */
    public boolean isIsDbPropertiesFileOkay() {
        return isDbPropertiesFileOkay;
    }

    /**
     * @param isDbPropertiesFileOkay the isDbPropertiesFileOkay to set
     */
    public void setIsDbPropertiesFileOkay(boolean isDbPropertiesFileOkay) {
        this.isDbPropertiesFileOkay = isDbPropertiesFileOkay;
    }

    /**
     * @return the isGamePropertiesFileOkay
     */
    public boolean isIsGamePropertiesFileOkay() {
        return isGamePropertiesFileOkay;
    }

    /**
     * @param isGamePropertiesFileOkay the isGamePropertiesFileOkay to set
     */
    public void setIsGamePropertiesFileOkay(boolean isGamePropertiesFileOkay) {
        this.isGamePropertiesFileOkay = isGamePropertiesFileOkay;
    }

    }