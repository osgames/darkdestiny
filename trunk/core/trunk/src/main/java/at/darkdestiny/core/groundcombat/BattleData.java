/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.groundcombat;

import at.darkdestiny.core.enumeration.ECombatStatus;
import at.darkdestiny.core.model.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class BattleData {

    private ECombatStatus status;
    private Integer planetOwner;
    private Integer newOwner = 0;
    private Integer planetId;
    private Integer systemId;
    private Integer groundCombatId = 0;
    private HashMap<Integer, Boolean> diplomacy;
    private StringBuffer report = new StringBuffer();
    private PlayerPlanet playerPlanet;
    private ArrayList<CombatPlayer> combatPlayers = new ArrayList<CombatPlayer>();
    private ArrayList<CombatRound> combatRounds = new ArrayList<CombatRound>();
    private ArrayList<TerritoryEntry> territoryEntry = new ArrayList<TerritoryEntry>();
    private ArrayList<RoundEntry> roundEntry = new ArrayList<RoundEntry>();
    private TreeMap<Integer, User> users = new TreeMap<Integer, User>();
    private TreeMap<Integer, TreeMap<Integer, PlayerTroop>> troops = new TreeMap<Integer, TreeMap<Integer, PlayerTroop>>();
    private HashMap<Integer, ArrayList<Integer>> groups = new HashMap<Integer, ArrayList<Integer>>();

    public BattleData() {
    }

    public CombatRound getLastRound() {
        ArrayList<CombatRound> result = getCombatRounds();
        TreeMap<Integer, CombatRound> sorted = new TreeMap<Integer, CombatRound>();

        if (result.isEmpty()) {
            return null;
        }
        for (CombatRound crTmp : result) {
            sorted.put(crTmp.getRound(), crTmp);
        }
        return sorted.get(sorted.size());
    }

    /**
     * @return the status
     */
    public ECombatStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(ECombatStatus status) {
        this.status = status;
    }

    /**
     * @return the planetOwner
     */
    public Integer getPlanetOwner() {
        return planetOwner;
    }

    /**
     * @param planetOwner the planetOwner to set
     */
    public void setPlanetOwner(Integer planetOwner) {
        this.planetOwner = planetOwner;
    }


    /**
     * @return the report
     */
    public StringBuffer getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(StringBuffer report) {
        this.report = report;
    }

    /**
     * @return the combatPlayers
     */
    public ArrayList<CombatPlayer> getCombatPlayers() {
        return combatPlayers;
    }

    /**
     * @param combatPlayers the combatPlayers to set
     */
    public void setCombatPlayers(ArrayList<CombatPlayer> combatPlayers) {
        this.combatPlayers = combatPlayers;
    }

    /**
     * @return the combatRounds
     */
    public ArrayList<CombatRound> getCombatRounds() {
        return combatRounds;
    }

    /**
     * @param combatRounds the combatRounds to set
     */
    public void setCombatRounds(ArrayList<CombatRound> combatRounds) {
        this.combatRounds = combatRounds;
    }

    /**
     * @return the territoryEntry
     */
    public ArrayList<TerritoryEntry> getTerritoryEntry() {
        return territoryEntry;
    }

    /**
     * @param territoryEntry the territoryEntry to set
     */
    public void setTerritoryEntry(ArrayList<TerritoryEntry> territoryEntry) {
        this.territoryEntry = territoryEntry;
    }

    /**
     * @return the roundEntry
     */
    public ArrayList<RoundEntry> getRoundEntry() {
        return roundEntry;
    }

    /**
     * @param roundEntry the roundEntry to set
     */
    public void setRoundEntry(ArrayList<RoundEntry> roundEntry) {
        this.roundEntry = roundEntry;
    }

    /**
     * @return the users
     */
    public TreeMap<Integer, User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(TreeMap<Integer, User> users) {
        this.users = users;
    }

    /**
     * @return the troops
     */
    public TreeMap<Integer, TreeMap<Integer, PlayerTroop>> getTroops() {
        return troops;
    }

    /**
     * @param troops the troops to set
     */
    public void setTroops(TreeMap<Integer, TreeMap<Integer, PlayerTroop>> troops) {
        this.troops = troops;
    }

    /**
     * @return the diplomacy
     */
    public HashMap<Integer, Boolean> getDiplomacy() {
        return diplomacy;
    }

    /**
     * @param diplomacy the diplomacy to set
     */
    public void setDiplomacy(HashMap<Integer, Boolean> diplomacy) {
        this.diplomacy = diplomacy;
    }

    /**
     * @return the newOwner
     */
    public Integer getNewOwner() {
        return newOwner;
    }

    /**
     * @param newOwner the newOwner to set
     */
    public void setNewOwner(Integer newOwner) {
        this.newOwner = newOwner;
    }

    /**
     * @return the groundCombatId
     */
    public Integer getGroundCombatId() {
        return groundCombatId;
    }

    /**
     * @param groundCombatId the groundCombatId to set
     */
    public void setGroundCombatId(Integer groundCombatId) {
        this.groundCombatId = groundCombatId;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the playerPlanet
     */
    public PlayerPlanet getPlayerPlanet() {
        return playerPlanet;
    }

    /**
     * @param playerPlanet the playerPlanet to set
     */
    public void setPlayerPlanet(PlayerPlanet playerPlanet) {
        this.playerPlanet = playerPlanet;
    }

    /**
     * @return the systemId
     */
    public Integer getSystemId() {
        return systemId;
    }

    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the groups
     */
    public HashMap<Integer, ArrayList<Integer>> getGroups() {
        return groups;
    }

    /**
     * @param groups the groups to set
     */
    public void setGroups(HashMap<Integer, ArrayList<Integer>> groups) {
        this.groups = groups;
    }
}
