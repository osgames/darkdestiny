/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat.combatcontroller;

import at.darkdestiny.core.WeaponModule;
import at.darkdestiny.core.admin.module.AttributeTree;
import at.darkdestiny.core.admin.module.ReferenceType;
import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.DamagedShipsDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.diplomacy.combat.CombatGroupEntry;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.enumeration.EShieldStrength;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.DamagedShips;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.spacecombat.AbstractCombatUnit;
import at.darkdestiny.core.spacecombat.AbstractDefenseBuilding;
import at.darkdestiny.core.spacecombat.BattleShipNew;
import at.darkdestiny.core.spacecombat.CombatContext;
import at.darkdestiny.core.spacecombat.CombatGroup;
import at.darkdestiny.core.spacecombat.CombatGroupFleet;
import at.darkdestiny.core.spacecombat.CombatGroupFleet.DesignDescriptor;
import at.darkdestiny.core.spacecombat.CombatGroupFleet.UnitTypeEnum;
import at.darkdestiny.core.spacecombat.CombatHandler;
import at.darkdestiny.core.spacecombat.CombatUnit;
import at.darkdestiny.core.spacecombat.DefenseBuilding;
import at.darkdestiny.core.spacecombat.IBattleShip;
import at.darkdestiny.core.spacecombat.ICombatUnit;
import at.darkdestiny.core.spacecombat.IDefenseBuilding;
import at.darkdestiny.core.spacecombat.SC_DataEntry;
import at.darkdestiny.core.spacecombat.helper.ArmorShieldFactor;
import at.darkdestiny.core.spacecombat.helper.FleetTracker;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import at.darkdestiny.core.spacecombat.helper.TargetList;
import at.darkdestiny.core.spacecombat.helper.TargetListController;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Aion
 */
public class SpaceCombatNew extends AbstractSpaceCombat {

    private static final Logger log = LoggerFactory.getLogger(SpaceCombatNew.class);

    public static final String battleClass = "at.darkdestiny.core.spacecombat.combatcontroller.SpaceCombatNew";

    public static ConstructionDAO cDAO = DAOFactory.get(ConstructionDAO.class);
    public static ShipDesignDAO sdDAO = DAOFactory.get(ShipDesignDAO.class);
    public static DamagedShipsDAO dsDAO = DAOFactory.get(DamagedShipsDAO.class);

    private final int MAX_ROUNDS = 100;
    private ShieldDefinition planetaryShield;
    private final int PLANETARY_HU = 0;
    private final int PLANETARY_PA = 1;
    private boolean planetaryShieldRestored = false;

    private final int test = 1;    

    public SpaceCombatNew(int combatLocation, int combatLocationId) {
        super(combatLocation, combatLocationId);

        try {
            atMap.clear();
            asfMap.clear();
            asfMapPlanetary.clear();

            // Create Planetary Shield ASFMAP
            if (pp != null) {
                AttributeTree atPlanetaryShield = new AttributeTree(pp.getUserId(), 0l);
                asfMapPlanetary.put(0, new ArmorShieldFactor(7, atPlanetaryShield));
            }

            CombatContext.registerObject("TL_MODULE", new TargetListController());
            CombatContext.registerObject("ATMAP", atMap);
            CombatContext.registerObject("ASFMAP", asfMap);
            CombatContext.registerObject("ASFMAP_PLANETARY", asfMapPlanetary);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in SpaceCombatNew Constructor: ", e);
            throw new RuntimeException("SpaceCombatNew: Instantiation error");
        }
    }

    @Override
    public void startFightCalc() {
        if (combatGroups.isEmpty()) {
            return;
        }

        initializeCombatArray();
        fillCombatArray();
        calculateTargetLists(false);

        fight();
    }

    private void fight() {
        for (int rCnt = 0; rCnt < MAX_ROUNDS; rCnt++) {
            log.debug("==============================================");
            log.debug("================ ROUND " + (rCnt + 1) + " ==================");
            log.debug("==============================================");
            planetaryShieldRestored = false;
            
            //  for (int i = 1; i > 0; i--) {
            for (int i = 0; i < noOfCombatGroups; i++) {
                for (int j = 0; j < highestNoOfFleets; j++) {
                    if (combatArray.getUnit(i, j, 0) == null) {
                        continue;
                    }

                    boolean fleetCheckDone = false;
                    for (int k = 0; k < highestNoOfDesigns; k++) {
                        // DebugBuffer.addLine("Scan ShipDesign: " + k);
                        if (combatArray.getUnit(i, j, k) == null) {
                            continue;
                        }

                        ICombatUnit actUnit = combatArray.getUnit(i, j, k);
                        log.debug("[" + i + "," + j + "," + k + "] Processing ship " + actUnit.getName() + " [" + actUnit + "] with count of " + actUnit.getCount());

                        if (rCnt < 2) {
                            boolean currUnitDefenseBuilding = actUnit instanceof DefenseBuilding;
                            boolean currUnitOrbitalDefense = false;

                            if (actUnit instanceof BattleShipNew) {
                                BattleShipNew bsn = (BattleShipNew) actUnit;
                                currUnitOrbitalDefense = bsn.isDefenseStation();
                            }

                            if (rCnt < 1) {
                                if (!currUnitDefenseBuilding) {
                                    continue;
                                }
                            } else {
                                if (!currUnitDefenseBuilding && !currUnitOrbitalDefense) {
                                    continue;
                                }
                            }
                        }

                        if (!fleetCheckDone) {
                            if (skip(combatArray.getUnit(i, j, k))) {
                                break;

                            }
                            fleetCheckDone = true;
                        }

                        ICombatUnit icu = combatArray.getUnit(i, j, k);
                        AbstractCombatUnit acu = (AbstractCombatUnit) icu;

                        if (acu.getTempDestroyed() >= 1d) {
                            continue;
                        }

                        log.debug(acu.getName() + " has paratron? " + (acu.getShields().getShield_PA() != 0));

                        log.debug("Looping " + icu.getName());
                        TargetListController tlc = (TargetListController) CombatContext.getRegisteredObject("TL_MODULE");
                        tlc.updateTargetLists(icu);
                        if (rCnt < 2) {
                            icu.fire(false);
                        } else {
                            icu.fire(true);
                        }
                        tlc.updateTargetLists(icu);
                    }
                }
            }

            if (rCnt >= 2) {
                checkRetreatOrResign();
            }
            if (onlyOneSideLeft()) {
                break;
            }

            restoreShields();
        }

        // Do some stuff to make result page work
        FleetTracker.clearFleetTracker();

        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                if (combatArray.getUnit(i, j, 0) == null) {
                    continue;
                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    // DebugBuffer.addLine("Scan ShipDesign: " + k);
                    if (combatArray.getUnit(i, j, k) == null) {
                        continue;
                    }

                    ICombatUnit icu = combatArray.getUnit(i, j, k);
                    AbstractCombatUnit acu = (AbstractCombatUnit) icu;

                    CombatUnit cu = (CombatUnit) icu;

                    if (icu instanceof BattleShipNew) {
                        cu.setBattleShip((BattleShipNew) icu);
                    }

                    // Move to Battleship Class??
                    // Needs to be in interface to be compatible with old version
                    log.debug("[" + i + "," + j + "," + k + "] Destroyed for " + icu.getName() + " = " + acu.getTempDestroyed());

                    int[] distribution = icu.getDamageDistribution(icu.getCount(), acu.getTempDestroyed());

                    SC_DataEntry sc_DataEntry = null;

                    if (icu instanceof BattleShipNew) {
                        ShipDesign sd = sdDAO.findById(icu.getDesignId());
                        if (sd.getType() == EShipType.SPACEBASE) {
                            sc_DataEntry = sc_DataLogger.getShipLogger(sd.getUserId(), CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE, sd.getId());
                        } else {
                            sc_DataEntry = sc_DataLogger.getShipLogger(icu.getParentCombatGroupFleet().getUserId(), CombatGroupFleet.UnitTypeEnum.SHIP, sd.getId());
                        }
                    } else if (icu instanceof DefenseBuilding) {
                        Construction c = cDAO.findById(icu.getDesignId());
                        log.debug("C is " + c.getName() + " and pp owner is " + pp.getUserId() + " for planet " + pp.getPlanetId());
                        log.debug("DataLogger is " + sc_DataLogger);
                        log.debug("ShipLogger is " + sc_DataLogger.getShipLogger(pp.getUserId(), CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE, c.getId()));
                        sc_DataEntry = sc_DataLogger.getShipLogger(pp.getUserId(), CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE, c.getId());
                    }

                    sc_DataEntry.addDamagedShip(EDamageLevel.NODAMAGE, distribution[0]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.MINORDAMAGE, distribution[1]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.LIGHTDAMAGE, distribution[2]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.MEDIUMDAMAGE, distribution[3]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.HEAVYDAMAGE, distribution[4]);
                    sc_DataEntry.addDamagedShip(EDamageLevel.DESTROYED, distribution[5]);

                    CombatGroupFleet cgf = acu.getParentCombatGroupFleet();
                    Map<DesignDescriptor, CombatUnit> units = cgf.getShipDesign();

                    for (Map.Entry<DesignDescriptor, CombatUnit> entry : units.entrySet()) {
                        CombatUnit cuTmp = entry.getValue();

                        if (((entry.getKey().getType() == UnitTypeEnum.SHIP)
                                || (entry.getKey().getType() == UnitTypeEnum.PLATTFORM_DEFENSE))
                                && (acu instanceof IBattleShip)) {
                            if (entry.getKey().getId() == acu.getDesignId()) {
                                cuTmp.setDamageDistribution(distribution);
                            }
                        } else if ((entry.getKey().getType() == UnitTypeEnum.SURFACE_DEFENSE)
                                && (acu instanceof IDefenseBuilding)) {
                            if (entry.getKey().getId() == acu.getDesignId()) {
                                cuTmp.setDamageDistribution(distribution);
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean onlyOneSideLeft() {
        int CGWithShips = 0;
        // HashSet<Integer> activeCombatGroup = new HashSet<Integer>();

        for (int i = 0; i < noOfCombatGroups; i++) {
            boolean hasShips = false;

            for (int j = 0; j < highestNoOfFleets; j++) {
                if (combatArray.getUnit(i, j, 0) == null) {
                    continue;
                }

                for (int k = 0; k < highestNoOfDesigns; k++) {
                    // DebugBuffer.addLine("Scan ShipDesign: " + k);
                    if (combatArray.getUnit(i, j, k) == null) {
                        continue;
                    }

                    ICombatUnit icu = combatArray.getUnit(i, j, k);
                    if (icu.getParentCombatGroupFleet().isResigned()
                            || icu.getParentCombatGroupFleet().isRetreated()) {
                        break;
                    }

                    AbstractCombatUnit acu = (AbstractCombatUnit) icu;
                    if (acu.getTempDestroyed() < 1d) {
                        hasShips = true;
                        // if (!activeCombatGroup.contains(icu.getParentCombatGroupId())) activeCombatGroup.add(icu.getParentCombatGroupId());
                    }
                }
            }

            if (hasShips) {
                CGWithShips++;
            }
        }

        if (CGWithShips > 1) {
            // Check if remaining groups fight each other
            // If no stop combat and return true
            /*
             for (int cgId1 : activeCombatGroup) {
             for (int cgId2 : activeCombatGroup) {
             if (cgId1 == cgId2) continue;
                    
             CombatGroup cg1 = null;
             CombatGroup cg2 = null;
                    
             for (CombatGroup cg : combatGroups) {
             if (cg.getId() == cgId1) {
             cg1 = cg;
             }
             }
             for (CombatGroup cg : combatGroups) {
             if (cg.getId() == cgId2) {
             cg2 = cg;
             }
             }          
                    
                    
             }
             }        
             */
            return false;
        } else {
            return true;
        }
    }

    private void checkRetreatOrResign() {
        boolean onlyPlanetDefLeft = onlyPlanetaryDefenseLeft();
        HashMap<Integer, Integer> accStrengthPerGroup = new HashMap<Integer, Integer>();

        // Retreat only possible after Round 3 
        for (FleetTracker ft : FleetTracker.getAllFleetTrackers()) {
            log.debug("CROR: " + ft);

            for (CombatGroupFleet tmpCGF : ft.getCGFs()) {
                log.debug("Looping fleet " + tmpCGF + " named " + tmpCGF.getFleetName() + " CombatGroup is " + tmpCGF.getGroupId());
                if (tmpCGF.isPlanetDefense() || tmpCGF.isSystemDefense()) {
                    continue;
                }

                log.debug("Checking fleet " + tmpCGF.getFleetName() + " in FleetTracker: " + ft);
                // CombatGroupFleet tmpCGF = ft.getCGF();

                if (tmpCGF.isRetreated() || tmpCGF.isResigned()) {
                    continue;
                }

                if (ft.getDestroyedPercentage() >= 100) {
                    log.debug("Fleet " + tmpCGF.getFleetName() + " was destroyed no retreat possible");
                    continue;
                }

                if ((ft.getDestroyedPercentage() > tmpCGF.getRetreatFactor()) && (tmpCGF.getRetreatFactor() < 100)) {
                    // Set fleet retreated
                    log.debug("Fleet " + tmpCGF.getFleetName() + " retreats! (" + ft.getDestroyedPercentage() + ">" + tmpCGF.getRetreatFactor() + ")");
                    tmpCGF.setRetreated(true);
                    // checkRetreatOrResign();
                    continue;
                    // return;
                }

                // Summarize total Attack Power of CombatGroups
                if (onlyPlanetDefLeft) {
                    ft.calculateAttackPower();

                    if (accStrengthPerGroup.containsKey(tmpCGF.getGroupId())) {
                        log.debug("Modifying combatGroup " + (tmpCGF.getGroupId()) + " To Strength Map (Initial Fleet: " + tmpCGF.getFleetName() + ")");
                        accStrengthPerGroup.put(tmpCGF.getGroupId(), accStrengthPerGroup.get(tmpCGF.getGroupId()) + ft.getAttackPower());
                    } else {
                        log.debug("Adding combatGroup " + (tmpCGF.getGroupId()) + " To Strength Map (Initial Fleet: " + tmpCGF.getFleetName() + ")");
                        accStrengthPerGroup.put(tmpCGF.getGroupId(), ft.getAttackPower());
                    }
                }
            }
        }

        log.debug("ONLYPLANETDEF LEFT? " + onlyPlanetDefLeft);
        log.debug("PLANETARY DEF GROUP IS " + planetaryDefGroup);
        log.debug("No of CombatGroups is " + noOfCombatGroups);
        if (onlyPlanetDefLeft) {
            for (int i = 1; i <= noOfCombatGroups; i++) {
                log.debug("Looping combatGroup " + i);
                if (i != planetaryDefGroup) {
                    log.debug("FOUND A NON PLANETARY DEF GROUP");

                    if (!accStrengthPerGroup.containsKey(i)) {
                        continue;
                    }

                    log.debug("ACC STRENGTH OF GROUP IS " + accStrengthPerGroup.get(i));
                    log.debug("DEFENDER SHIELD IS " + ((planetaryShield.getShield_HU() + planetaryShield.getShield_PA()) / 2));

                    if (accStrengthPerGroup.get(i) < ((planetaryShield.getShield_HU() + planetaryShield.getShield_PA()) / 2)) {
                        // Set all fleets in this group to resigned!
                        CombatGroup cg = combatGroups.get(i - 1);
                        List<CombatGroupFleet> fleets = cg.getFleetList();
                        for (CombatGroupFleet cgf : fleets) {
                            log.debug("Set " + cgf.getFleetName() + " resigns!");
                            cgf.setResigned(true);
                            log.debug("[SC] CGF IS = " + cgf + " Name = " + cgf.getFleetName() + " RESIGNED = " + cgf.isResigned());
                        }
                    }
                }
            }
        }
    }

    /**
     * Ermittelt ob auf der Verteidigerseite nur noch Planetare Verteidigung
     * existiert bzw. ob nur noch das Schild existiert. Falls kein
     * Schildvorhanden ist, wird immer false retourniert
     *
     * @return Nur noch Verteidigungsanlagen oder Schild (zwingend) vorhanden
     */
    private boolean onlyPlanetaryDefenseLeft() {
        log.debug("Check only Plantary Def Left");

        if (combatLocation == CombatHandler.SYSTEM_COMBAT) {
            return false;

        }
        if (planetaryShield == null) {
            return false;

        }
        if (planetaryShield.getShieldEnergy() == 0) {
            return false;

            // Find planetary surface Defense
        }
        boolean dataFound = false;

        // planetaryDefGroup = -1;
        ArrayList<Integer> groupHasNonSurface = new ArrayList<Integer>();

        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                boolean fleetCheckDone = false;

                if (combatArray.getUnit(i, j, 0) == null) {
                    continue;

                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    if (combatArray.getUnit(i, j, k) == null) {
                        continue;

                    }

                    ICombatUnit icu = combatArray.getUnit(i, j, k);
                    // BattleShip bs = (BattleShip)combatArray[i][j][k];
                    if (icu.getCount() == 0) {
                        continue;

                    }

                    // Check for this ship if its fleet does retreat
                    // if yes we dont need to check all the other ships
                    if (!fleetCheckDone) {
                        if (skip(icu)) {
                            break;

                        }
                        fleetCheckDone = true;
                    }

                    log.debug("Checking fleet " + icu.getParentCombatGroupFleet().getFleetName());
                    log.debug("Check if groupHasNoSurface contains CombatGroupId " + (icu.getParentCombatGroupId() + 1)
                            + " AND planetaryDefGroup is not -1");
                    if (groupHasNonSurface.contains(icu.getParentCombatGroupId() + 1) && planetaryDefGroup != -1) {
                        log.debug("Condition ist TRUE");
                        // if (groupHasNonSurface.contains(bs.getFleetTracker().getCGF().getGroupId() - 1) && planetaryDefGroup != -1) {
                        dataFound = true;
                        break;
                    } else {
                        log.debug("Condition ist FALSE");
                    }

                    log.debug("Check: Is current CombatUnit NOT a DefenseBuilding");
                    if (!(icu instanceof AbstractDefenseBuilding)) {
                        log.debug("NO Defensebuilding");
                        // if (!bs.isPlanetaryDefense()) {
                        // log.debug("Non Planet Def Unit Found for Group "+(bs.getFleetTracker().getRelatedCGF().getGroupId()-1)+"!");
                        groupHasNonSurface.add(icu.getParentCombatGroupId() + 1);
                        // groupHasNonSurface.add(bs.getFleetTracker().getCGF().getGroupId() - 1);
                    } else {
                        log.debug("YES Defensebuilding");
                        // log.debug("Planet Def Group found for Group "+(bs.getFleetTracker().getRelatedCGF().getGroupId()-1)+"!");
                        planetaryDefGroup = icu.getParentCombatGroupId() + 1;
                        // planetaryDefGroup = bs.getFleetTracker().getCGF().getGroupId() - 1;
                    }

                    log.debug("Check if planetaryDefGroup is -1");
                    if (planetaryDefGroup == -1) {
                        log.debug("IT IS");
                        //##CHANGED
                        // if (AllianceUtilities.areAllied(bs.getFleetTracker().getRelatedCGF().getUserId(), pp.getUserId())) {
                        log.debug("Check if current processed fleet (" + icu.getParentCombatGroupFleet().getFleetName() + ") is allied to planetOwner");
                        if ((DiplomacyUtilities.getDiplomacyResult(icu.getParentCombatGroupFleet().getUserId(), pp.getUserId()).isHelps())
                                || (icu.getParentCombatGroupFleet().getUserId() == pp.getUserId())) {
                            log.debug("IT IS");
                            // if (DiplomacyUtilities.getDiplomacyResult(bs.getFleetTracker().getCGF().getUserId(), pp.getUserId()).isHelps()) {
                            // log.debug("Planet Def Group found for Group "+(bs.getFleetTracker().getRelatedCGF().getGroupId()-1)+"!");
                            planetaryDefGroup = icu.getParentCombatGroupId() + 1;
                            // planetaryDefGroup = bs.getFleetTracker().getCGF().getGroupId() - 1;
                        } else {
                            log.debug("IT IS NOT");
                        }
                    } else {
                        log.debug("IT IS NOT");
                    }
                }
                if (dataFound) {
                    break;
                }
            }

            if (dataFound) {
                break;
            }
        }

        if (dataFound) {
            return false;
        }

        if (groupHasNonSurface.contains(planetaryDefGroup)) {
            // log.debug("RESULT: false");
            return false;
        } else {
            // log.debug("RESULT: true");
            return true;
        }
    }

    private void restoreShields() {
        planetaryShieldRestored = false || (planetaryShield == null);

        if (!planetaryShieldRestored) {
            System.out.println("Restore Planetary Shield");
            planetaryShieldRestored = true;
            planetaryShield.setSharedShield_PA((float) (planetaryShield.getShield_PA()));
            planetaryShield.setSharedShield_HU((float) (planetaryShield.getShield_HU()));
            planetaryShield.setSharedShield_PS((float) (planetaryShield.getShield_PS()));
        }

        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                if (combatArray.getUnit(i, j, 0) == null) {
                    continue;
                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    // DebugBuffer.addLine("Scan ShipDesign: " + k);
                    if (combatArray.getUnit(i, j, k) == null) {
                        continue;
                    }

                    CombatUnit cu = (CombatUnit) combatArray.getUnit(i, j, k);
                    (cu).getAlreadyFiredOn().clear();

                    AbstractCombatUnit acu = (AbstractCombatUnit) cu;
                    if (cu instanceof DefenseBuilding) {
                        // Shields of defense buildings are restored to full capacity every round
                        System.out.println("Restore Shields for " + cu.getName());
                        ShieldDefinition sd = acu.getShields();
                        if (sd != null) {
                            sd.setSharedShield_PA((float) (sd.getShield_PA() * cu.getCount() * (1d - cu.getTempDestroyed())));
                            sd.setSharedShield_HU((float) (sd.getShield_HU() * cu.getCount() * (1d - cu.getTempDestroyed())));
                            sd.setSharedShield_PS((float) (sd.getShield_PS() * cu.getCount() * (1d - cu.getTempDestroyed())));
                        }
                    } else if (cu instanceof BattleShipNew) {
                        BattleShipNew bsn = (BattleShipNew) cu;
                        ShieldDefinition sd = acu.getShields();
                        double tmpDestroyedModifier = 1d - cu.getTempDestroyed();

                        double totalShipEnergyProduction = bsn.getEnergyProduction() * cu.getCount() * tmpDestroyedModifier;
                        System.out.println(cu.getName() + " produces " + totalShipEnergyProduction + " energy");

                        double freeEnergyTotal = totalShipEnergyProduction - bsn.getInternalEnergyConsumption() * cu.getCount() * tmpDestroyedModifier;
                        System.out.println(cu.getName() + " has left " + freeEnergyTotal + " energy after internal consumption");

                        double freeShieldEnergy = freeEnergyTotal - bsn.getWeaponEnergyTotal() * tmpDestroyedModifier;

                        if (sd != null) {
                            // Calculate Shield Energy Consumption
                            double shieldEnergyConsumptionTotal = (sd.getEnergyNeed_PS() + sd.getEnergyNeed_HU() + sd.getEnergyNeed_PA()) * cu.getCount() * tmpDestroyedModifier;

                            double totalEnergyCoverage = 1d / shieldEnergyConsumptionTotal * freeShieldEnergy;
                            System.out.println(cu.getName() + " has left " + freeShieldEnergy + " energy after weapons (Total shield Energy cons is " + shieldEnergyConsumptionTotal + ") [Coverage: " + totalEnergyCoverage + "]");

                            if (totalEnergyCoverage >= 1d) {
                                System.out.println("Full Shield Restore for " + cu.getName());
                                sd.setSharedShield_PA((float) (sd.getShield_PA() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                System.out.println("Restore Paratron to " + sd.getSharedShield_PA() + " SingleUnitShield: " + sd.getShield_PA() + " Count: " + cu.getCount() + " Alive: " + (1d - cu.getTempDestroyed()));
                                sd.setSharedShield_HU((float) (sd.getShield_HU() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                sd.setSharedShield_PS((float) (sd.getShield_PS() * cu.getCount() * (1d - cu.getTempDestroyed())));
                            } else {
                                System.out.println("Process Detailed Shield Restore for " + cu.getName());
                                double baseRestore = 1d / shieldEnergyConsumptionTotal * freeShieldEnergy;
                                double totalEnergyRequirement = 0;

                                double paEnergyRequirement = (sd.getEnergyNeed_PA() * cu.getCount() * tmpDestroyedModifier) * (1d - sd.getPa_EnergyLevel((float) (cu.getCount() * (1d - cu.getTempDestroyed()))));
                                double huEnergyRequirement = (sd.getEnergyNeed_HU() * cu.getCount() * tmpDestroyedModifier) * (1d - sd.getHu_EnergyLevel((float) (cu.getCount() * (1d - cu.getTempDestroyed()))));
                                double psEnergyRequirement = (sd.getEnergyNeed_PS() * cu.getCount() * tmpDestroyedModifier) * (1d - sd.getPs_EnergyLevel((float) (cu.getCount() * (1d - cu.getTempDestroyed()))));

                                System.out.println("Energy Requirement PA " + paEnergyRequirement + " HU " + huEnergyRequirement + " PS " + psEnergyRequirement);
                                totalEnergyRequirement = paEnergyRequirement + huEnergyRequirement + psEnergyRequirement;

                                if (totalEnergyRequirement < freeShieldEnergy) {
                                    System.out.println("Full Shield Restore for " + cu.getName());
                                    sd.setSharedShield_PA((float) (sd.getShield_PA() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                    System.out.println("Restore Paratron to " + sd.getSharedShield_PA() + " SingleUnitShield: " + sd.getShield_PA() + " Count: " + cu.getCount() + " Alive: " + (1d - cu.getTempDestroyed()));
                                    sd.setSharedShield_HU((float) (sd.getShield_HU() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                    sd.setSharedShield_PS((float) (sd.getShield_PS() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                    break;
                                } else {
                                    // Restore shields starting from the inner most
                                    if (psEnergyRequirement > 0) {
                                        if (psEnergyRequirement > freeShieldEnergy) {
                                            double restoreCapacity = 1d / freeShieldEnergy * psEnergyRequirement;
                                            double currentState = sd.getPs_EnergyLevel((float) (cu.getCount() * (1d - cu.getTempDestroyed())));
                                            double newState = currentState + ((1d - currentState) / restoreCapacity);

                                            System.out.println("Partial Shield Restore for PS: " + newState);
                                            sd.setSharedShield_PS((float) (sd.getShield_PS() * cu.getCount() * (1d - cu.getTempDestroyed()) * newState));
                                            freeShieldEnergy = 0;
                                            break;
                                        } else {
                                            System.out.println("Full Shield Restore for PS");
                                            freeShieldEnergy -= psEnergyRequirement;
                                            sd.setSharedShield_PS((float) (sd.getShield_PS() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                        }
                                    }
                                    if (huEnergyRequirement > 0) {
                                        if (huEnergyRequirement > freeShieldEnergy) {
                                            double restoreCapacity = 1d / freeShieldEnergy * huEnergyRequirement;
                                            double currentState = sd.getHu_EnergyLevel((float) (cu.getCount() * (1d - cu.getTempDestroyed())));
                                            double newState = currentState + ((1d - currentState) / restoreCapacity);

                                            System.out.println("Partial Shield Restore for HU: " + restoreCapacity);
                                            sd.setSharedShield_HU((float) (sd.getShield_HU() * cu.getCount() * (1d - cu.getTempDestroyed()) * newState));
                                            freeShieldEnergy = 0;
                                            break;
                                        } else {
                                            System.out.println("Full Shield Restore for HU");
                                            freeShieldEnergy -= huEnergyRequirement;
                                            sd.setSharedShield_HU((float) (sd.getShield_HU() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                        }
                                    }
                                    if (paEnergyRequirement > 0) {
                                        if (paEnergyRequirement > freeShieldEnergy) {
                                            double restoreCapacity = 1d / freeShieldEnergy * paEnergyRequirement;
                                            double currentState = sd.getPa_EnergyLevel((float) (cu.getCount() * (1d - cu.getTempDestroyed())));
                                            double newState = currentState + ((1d - currentState) / restoreCapacity);

                                            System.out.println("Partial Shield Restore for PA: " + restoreCapacity);
                                            sd.setSharedShield_PA((float) (sd.getShield_PA() * cu.getCount() * (1d - cu.getTempDestroyed()) * newState));
                                            System.out.println("Restore Paratron to " + sd.getSharedShield_PA() + " SingleUnitShield: " + sd.getShield_PA() + " Count: " + cu.getCount() + " Alive: " + (1d - cu.getTempDestroyed()) + " NewState: " + newState);
                                            freeShieldEnergy = 0;
                                            break;
                                        } else {
                                            System.out.println("Full Shield Restore for PA");
                                            freeShieldEnergy -= paEnergyRequirement;
                                            sd.setSharedShield_PA((float) (sd.getShield_PA() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                            System.out.println("Restore Paratron to " + sd.getSharedShield_PA() + " SingleUnitShield: " + sd.getShield_PA() + " Count: " + cu.getCount() + " Alive: " + (1d - cu.getTempDestroyed()));
                                        }
                                    }
                                    
                                    // sd.setSharedShield_PA((float) (sd.getShield_PA() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                    // sd.setSharedShield_HU((float) (sd.getShield_HU() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                    // sd.setSharedShield_PS((float) (sd.getShield_PS() * cu.getCount() * (1d - cu.getTempDestroyed())));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void initializeCombatArray() {
        noOfCombatGroups = combatGroups.size();

        for (int i = 0; i < noOfCombatGroups; i++) {
            log.debug("I: " + i);

            CombatGroup cg = (CombatGroup) combatGroups.get(i);
            log.debug("CG FleetCount: " + cg.getFleetCount());
            if (cg.getFleetCount() > highestNoOfFleets) {
                highestNoOfFleets = cg.getFleetCount();
            }

            log.debug("CG FleetList: " + cg.getFleetList());
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                int designs = cgf.getShipDesign().size();
                if (designs > highestNoOfDesigns) {
                    highestNoOfDesigns = designs;
                }
            }
        }
    }

    private void fillCombatArray() {
        // TODO MAKE ENUM
        if (combatLocation == CombatHandler.ORBITAL_COMBAT) {
            log.debug("Orbital combat");
            setPlanetaryShield();
        } else {
            log.debug("System combat");
        }

        for (int i = 0; i < combatGroups.size(); i++) {
            CombatGroup cg = (CombatGroup) combatGroups.get(i);
            // DebugBuffer.addLine("CG="+i+" NoOfFleets="+cg.getFleetCount()+"="+((ArrayList)cg.getFleetList()).size());

            for (int j = 0; j < cg.getFleetList().size(); j++) {
                CombatGroupFleet cgf = cg.getFleetList().get(j);
                FleetTracker ft = FleetTracker.getFleetTracker(cgf);
                log.debug("Created Fleettracker " + ft + " for Fleet " + cgf.getFleetName());

                int currDesign = 0;
                // DebugBuffer.addLine("CGF="+j+" NoOfDesigns="+cgf.getShipDesign().size());
                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> me : cgf.getShipDesign().entrySet()) {
                    if (cgf.isPlanetDefense() || cgf.isSystemDefense()) {
                        CombatGroupFleet.DesignDescriptor key = me.getKey();
                        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "type is " + key.getType());

                        int designId = key.getId();
                        int count = me.getValue().getOrgCount();
                        // int count = ((Integer) me.getValue()).intValue();

                        if (key.getType() == CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE) {
                            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Add Defense [" + designId + "] (on surface) to [" + i + "][" + j + "][" + currDesign + "]");
                            IDefenseBuilding idb = (IDefenseBuilding) mu.buildDefenseNew(key.getId(), count, key.getType(), cgf.getUserId());
                            idb.setSc_LoggingEntry(sc_DataLogger.addNewShip(cgf.getUserId(), key.getType(), designId));

                            if (planetaryShield != null) {
                                // ((DefenseBuilding) idb).setShields(planetaryShield);
                                ((DefenseBuilding) idb).setPlanetaryShields(planetaryShield);
                            }

                            // combatArray[i][j][currDesign].setFleetTracker(ft);
                            idb.setParentCombatGroupFleet(cgf);
                            ICombatUnit icu = (ICombatUnit) idb;
                            icu.setParentCombatGroupId(i);
                            combatArray.addUnit(i, j, currDesign, idb);
                            // log.debug("Created planetary defense ("+combatArray[i][j][currDesign].getName()+") with primary: " + combatArray[i][j][currDesign].getPrimaryTarget());

                            currDesign++;
                        } else if (key.getType() == CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE) {
                            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Add Defense [" + designId + "] (orbital) to [" + i + "][" + j + "][" + currDesign + "]");
                            IBattleShip ibs = (IBattleShip) mu.buildDefenseNew(key.getId(), count, key.getType(), cgf.getUserId());
                            ibs.setSc_LoggingEntry(sc_DataLogger.addNewShip(cgf.getUserId(), key.getType(), designId));
                            ibs.setParentCombatGroupFleet(cgf);
                            ICombatUnit icu = (ICombatUnit) ibs;
                            icu.setParentCombatGroupId(i);

                            if (!atMap.containsKey(designId)) {
                                atMap.put(designId, new AttributeTree(cgf.getUserId(), ibs.getShipDesign().getDesignTime()));
                            }
                            if (!asfMap.containsKey(designId)) {
                                asfMap.put(designId, new ArmorShieldFactor(ibs.getShipDesign().getChassis(), atMap.get(designId)));
                            }

                            combatArray.addUnit(i, j, currDesign, ibs);
                            // combatArray[i][j][currDesign].setFleetTracker(ft);
                            currDesign++;
                        }

                        // throw new UnsupportedOperationException("Not supported");
                    } else {
                        int designId = me.getKey().getId();
                        int count = me.getValue().getOrgCount();
                        // int count = me.getValue();

                        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Add Ship to [" + i + "][" + j + "][" + currDesign + "]");
                        IBattleShip ibs = mu.buildBattleShipNew(designId, count, me.getValue().getShipFleetId());
                        ibs.setSc_LoggingEntry(sc_DataLogger.addNewShip(cgf.getUserId(), me.getKey().getType(), designId));
                        ibs.setParentCombatGroupId(i);
                        ibs.setParentCombatGroupFleet(cgf);
                        me.setValue((CombatUnit) ibs);

                        // Check current DamagedShips for this design and apply to TempDestroyed
                        CombatUnit cu = (CombatUnit) ibs;

                        ArrayList<DamagedShips> damagedList = dsDAO.findById(me.getValue().getShipFleetId());
                        double damagedValue = 0d;

                        for (DamagedShips dEntry : damagedList) {
                            if (dEntry.getDamageLevel() == EDamageLevel.MINORDAMAGE) {
                                damagedValue += ((1d / count) * dEntry.getCount()) * 0.2d;
                            } else if (dEntry.getDamageLevel() == EDamageLevel.LIGHTDAMAGE) {
                                damagedValue += ((1d / count) * dEntry.getCount()) * 0.4d;
                            } else if (dEntry.getDamageLevel() == EDamageLevel.MEDIUMDAMAGE) {
                                damagedValue += ((1d / count) * dEntry.getCount()) * 0.6d;
                            } else if (dEntry.getDamageLevel() == EDamageLevel.HEAVYDAMAGE) {
                                damagedValue += ((1d / count) * dEntry.getCount()) * 0.8d;
                            }
                        }

                        log.debug("Applying tempDestroyed of " + damagedValue + " to ship " + cu.getName());

                        cu.setTempDestroyedStart(damagedValue);
                        cu.setTempDestroyed(damagedValue);

                        if (!atMap.containsKey(designId)) {
                            atMap.put(designId, new AttributeTree(cgf.getUserId(), ibs.getShipDesign().getDesignTime()));
                        }
                        if (!asfMap.containsKey(designId)) {
                            asfMap.put(designId, new ArmorShieldFactor(ibs.getShipDesign().getChassis(), atMap.get(designId)));
                        }

                        combatArray.addUnit(i, j, currDesign, ibs);

                        /*
                         ft.setOrgCount(ft.getOrgCount() + combatArray[i][j][currDesign].getCount());
                         ft.setActCount(ft.getOrgCount());

                         ft.setOrgHp((int)(ft.getOrgHp() + (combatArray[i][j][currDesign].getHp() * combatArray[i][j][currDesign].getCount())));
                         ft.setActHp(ft.getOrgHp());
                         */
                        currDesign++;
                    }
                }
            }
        }
    }

    private void calculateTargetLists(boolean update) {
        TargetListController tlc = (TargetListController) CombatContext.getRegisteredObject("TL_MODULE");

        log.debug("No of Groups: " + noOfCombatGroups + " Highest No of Fleets: " + highestNoOfFleets
                + " Highest No of Designs: " + highestNoOfDesigns);

        for (int i = 0; i < noOfCombatGroups; i++) {
            for (int j = 0; j < highestNoOfFleets; j++) {
                log.debug("Load CombatArrayEntry (" + i + "," + j + ",0)");
                if (combatArray.getUnit(i, j, 0) == null) {
                    continue;
                }
                for (int k = 0; k < highestNoOfDesigns; k++) {
                    // DebugBuffer.addLine("Scan ShipDesign: " + k);
                    if (combatArray.getUnit(i, j, k) == null) {
                        continue;
                    }

                    ICombatUnit icuTLOwner = combatArray.getUnit(i, j, k);
                    if (icuTLOwner.getCount() == 0) {
                        DebugBuffer.warning("Found ship [" + icuTLOwner.getName() + "] with count 0");
                        continue;
                    }

                    if (!update) {
                        tlc.register(combatArray.getUnit(i, j, k));
                    }

                    for (int i2 = 0; i2 < noOfCombatGroups; i2++) {
                        if (i2 == i) {
                            continue;
                        }

                        for (int j2 = 0; j2 < highestNoOfFleets; j2++) {
                            if (combatArray.getUnit(i2, j2, 0) == null) {
                                continue;
                            }
                            for (int k2 = 0; k2 < highestNoOfDesigns; k2++) {
                                // DebugBuffer.addLine("Scan ShipDesign: " + k);
                                if (combatArray.getUnit(i2, j2, k2) == null) {
                                    continue;
                                }

                                ICombatUnit icu = combatArray.getUnit(i2, j2, k2);
                                if (icu.getCount() == 0) {
                                    DebugBuffer.warning("Found ship [" + icu.getName() + "] with count 0");
                                    continue;
                                }

                                // Do not add to targetlist if attack not allowed
                                // icuTLOwner - current Ship
                                // icu - enemy
                                // 1. get CombatGroupEntry for current Ship
                                Boolean attacks = null;

                                for (CombatGroupEntry cge : cgr.getCombatGroups()) {
                                    if (cge.getPlayers().contains(icuTLOwner.getParentCombatGroupFleet().getUserId())) {
                                        // 2. get CombatGroupEntry for enemy Ship
                                        for (CombatGroupEntry cgeInner : cgr.getCombatGroups()) {
                                            if (cgeInner.getPlayers().contains(icu.getParentCombatGroupFleet().getUserId())) {
                                                if (!cge.getAttackingGroups().contains(cgeInner.getGroupId())) {
                                                    log.debug("[COMBATDEBUG] >>>>> " + icuTLOwner.getName() + " does not attack " + icu.getName());
                                                    attacks = false;
                                                    break;
                                                } else {
                                                    attacks = true;
                                                    log.debug("[COMBATDEBUG] >>>>> " + icuTLOwner.getName() + " attacks " + icu.getName());
                                                    break;
                                                }
                                            }
                                        }

                                        if (attacks != null) {
                                            break;
                                        }
                                    }
                                }

                                if (!attacks) {
                                    break;
                                }

                                // As the previous calculation is close to crap
                                // feel free to make it better here
                                // What we need
                                // Take own attack power and get estimate shield of enemy
                                // START
                                double targetFireIndicator = 0.1d;
                                int chassisSize = 1;

                                log.debug("Calculate TargetList: " + icuTLOwner.getName() + " >> " + icu.getName());

                                if (icuTLOwner instanceof BattleShipNew) {
                                    ShipDesign sd = sdDAO.findById(icuTLOwner.getDesignId());
                                    chassisSize = sd.getChassis();

                                    BattleShipNew attacker = (BattleShipNew) icuTLOwner;
                                    double attackingStrength = 0;
                                    HashMap<Integer, Double> attackStrengthByWeapon = new HashMap<Integer, Double>();
                                    int enemyHp = 0;

                                    for (WeaponModule wm : attacker.getWeapons()) {
                                        attackingStrength += wm.getAttackPower() * wm.getCount();
                                        attackStrengthByWeapon.put(wm.getWeaponId(), (double) wm.getAttackPower() * (double) wm.getCount());
                                    }

                                    // Summarize enemy shields
                                    if (icu instanceof BattleShipNew) {
                                        BattleShipNew defender = (BattleShipNew) icu;
                                        enemyHp = (int) (defender.getHp() / defender.getGroupingMultiplier());
                                        ShieldDefinition sDef = defender.getShields();

                                        HashMap<Integer, Double> estimatedPenetrationOnDefender = new HashMap<Integer, Double>();
                                        for (Map.Entry<Integer, Double> weaponsAtt : attackStrengthByWeapon.entrySet()) {
                                            int weaponId = weaponsAtt.getKey();
                                            double attackPower = weaponsAtt.getValue();

                                            ArmorShieldFactor asf = asfMap.get(defender.getShipDesign().getId());

                                            if (sDef.getShield_PA() > 0) {
                                                int absorbtionValue = (int) Math.floor((asf.getPTShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, weaponId, ReferenceType.MODULE) * 100d));
                                                if (estimatedPenetrationOnDefender.containsKey(absorbtionValue)) {
                                                    estimatedPenetrationOnDefender.put(absorbtionValue, estimatedPenetrationOnDefender.get(absorbtionValue) + attackPower);
                                                } else {
                                                    estimatedPenetrationOnDefender.put(absorbtionValue, attackPower);
                                                }
                                            } else if (sDef.getShield_HU() > 0) {
                                                int absorbtionValue = (int) Math.floor((asf.getHUShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, weaponId, ReferenceType.MODULE) * 100d));
                                                if (estimatedPenetrationOnDefender.containsKey(absorbtionValue)) {
                                                    estimatedPenetrationOnDefender.put(absorbtionValue, estimatedPenetrationOnDefender.get(absorbtionValue) + attackPower);
                                                } else {
                                                    estimatedPenetrationOnDefender.put(absorbtionValue, attackPower);
                                                }
                                            } else if (sDef.getShield_PS() > 0) {
                                                int absorbtionValue = (int) Math.floor((asf.getPSShield().getAttributeValue(EAttribute.DAMAGE_ABSORBTION, weaponId, ReferenceType.MODULE) * 100d));
                                                if (estimatedPenetrationOnDefender.containsKey(absorbtionValue)) {
                                                    estimatedPenetrationOnDefender.put(absorbtionValue, estimatedPenetrationOnDefender.get(absorbtionValue) + attackPower);
                                                } else {
                                                    estimatedPenetrationOnDefender.put(absorbtionValue, attackPower);
                                                }
                                            }
                                        }

                                        // Multiply enemy hp by 2 if total damage is absorbed
                                        double multiplier = 1d;

                                        for (Map.Entry<Integer, Double> penetrationMap : estimatedPenetrationOnDefender.entrySet()) {
                                            Integer absorbtion = penetrationMap.getKey();
                                            double damage = penetrationMap.getValue();

                                            multiplier += 1d / 100d * (((double) absorbtion / attackingStrength) * damage);
                                        }
                                    }

                                    if (attackingStrength == 0) {
                                        attackingStrength = 1;
                                    }
                                    log.debug(icu.getName() + " attackingStrengh: " + attackingStrength + " enemyHp: " + enemyHp);

                                    targetFireIndicator = Math.max(targetFireIndicator, (double) enemyHp / (double) attackingStrength);
                                }

                                // END
                                int chassisSizeEnemy = 1;
                                EShipType enemyType = null;
                                /*
                                 EShieldStrength enemyShields = EShieldStrength.NOT_SHIELDED;

                                 if (icu instanceof BattleShipNew) {
                                 ShipDesign sd = sdDAO.findById(icu.getDesignId());
                                 enemyType = sd.getType();
                                 chassisSizeEnemy = sd.getChassis();
                                 if (((BattleShipNew)icu).getShields().getSharedShield_PA() > 0) {
                                 enemyShields = EShieldStrength.HEAVY_SHIELDED;
                                 }
                                 if (((BattleShipNew)icu).getShields().getSharedShield_HU() > 0) {
                                 if (enemyShields == EShieldStrength.HEAVY_SHIELDED) {
                                 enemyShields = EShieldStrength.VERY_HEAVY_SHIELDED;
                                 } else {
                                 enemyShields = EShieldStrength.MEDIUM_SHIELDED;
                                 }
                                 }
                                 if (((BattleShipNew)icu).getShields().getSharedShield_PS() > 0) {
                                 if (!((enemyShields == EShieldStrength.VERY_HEAVY_SHIELDED) ||
                                 (enemyShields == EShieldStrength.HEAVY_SHIELDED) ||
                                 (enemyShields == EShieldStrength.MEDIUM_SHIELDED))) {
                                 enemyShields = EShieldStrength.LIGHT_SHIELDED;
                                 }
                                 }
                                 }
                                 */

                                // TODO: Adjust with primary/size of target/other factors
                                double chassisDiff = chassisSizeEnemy - chassisSize;
                                double ignoreFactor = 1d;

                                // double targetFireIndicator = 1d;
                                // Planetary Cannons dont like shooting at fighters
                                if (!(icuTLOwner instanceof DefenseBuilding)) {
                                    /*
                                     DefenseBuilding db = (DefenseBuilding)icuTLOwner;
                                     if (!(db.getConstruction().getId() == Construction.ID_PLANETARYMISSLEBASE)) {
                                     if (db.getConstruction().getId() == Construction.ID_PLANETARYLASERCANNON) {
                                     // Set focus on cruisers
                                     switch (chassisSizeEnemy) {
                                     case(1):
                                     case(2):
                                     ignoreFactor = 0.000001d;
                                     break;
                                     case(3):
                                     ignoreFactor = 0.0001d;
                                     break;
                                     case(4):
                                     ignoreFactor = 0.001d;
                                     break;
                                     case(5):
                                     break;
                                     case(6):
                                     case(7):
                                     ignoreFactor = 0.001d;
                                     break;
                                     case(8):
                                     case(9):
                                     ignoreFactor = 0.001d;
                                     break;
                                     case(10):
                                     ignoreFactor = 0.001d;
                                     break;
                                     }
                                     } else if (db.getConstruction().getId() == Construction.ID_PLANETARY_INTERVALCANNON) {
                                     // Set focus on super battle ships
                                     switch (chassisSizeEnemy) {
                                     case(1):
                                     case(2):
                                     ignoreFactor = 0.000001d;
                                     break;
                                     case(3):
                                     ignoreFactor = 0.00001d;
                                     break;
                                     case(4):
                                     ignoreFactor = 0.0001d;
                                     break;
                                     case(5):
                                     ignoreFactor = 0.001d;
                                     break;
                                     case(6):
                                     case(7):
                                     ignoreFactor = 0.01d;
                                     break;
                                     case(8):
                                     case(9):
                                     case(10):
                                     break;
                                     }
                                     }
                                     }
                                     } else {

                                     */
                                    if (chassisDiff < 0) { // Target is smaller
                                        ignoreFactor = 1d / (double) (Math.abs(chassisDiff) * ((double) icuTLOwner.getCount() / (double) icu.getCount()));
                                        // targetFireIndicator =
                                        log.debug("Tmp Ignore is " + ignoreFactor);
                                        log.debug("chassisDiff is " + chassisDiff + " overwhelm factor is " + (ignoreFactor / (double) Math.abs(1d / (double) chassisDiff)));
                                        log.debug("TargetFireFactor 1 [" + icuTLOwner.getName() + "=>" + icu.getName() + "]=" + targetFireIndicator);
                                    } else if (chassisDiff == 0) {
                                        ignoreFactor = 1d / ((icuTLOwner.getCount() * 2d) / icu.getCount());
                                        // targetFireIndicator =
                                        log.debug("TargetFireFactor 2 [" + icuTLOwner.getName() + "=>" + icu.getName() + "]=" + targetFireIndicator);
                                    } else { // Target is larger
                                        ignoreFactor = 1d / ((icuTLOwner.getCount() * (2d + chassisDiff)) / icu.getCount());
                                        // targetFireIndicator = getTargetFireIndictator(enemyShields, ignoreFactor * (Math.pow(10d, chassisDiff)));
                                        // targetFireIndicator =
                                        log.debug("TargetFireFactor 3 [" + icuTLOwner.getName() + "=>" + icu.getName() + "]=" + targetFireIndicator);
                                    }
                                }

                                if (ignoreFactor > 1d) {
                                    ignoreFactor = 1d;
                                }
                                if (enemyType != null) {
                                    if (enemyType.equals(EShipType.COLONYSHIP)) {
                                        ignoreFactor = Math.min(0.001d, ignoreFactor);
                                    } else if (enemyType.equals(EShipType.TRANSPORT)) {
                                        ignoreFactor = Math.min(0.01d, ignoreFactor);
                                    } else if (enemyType.equals(EShipType.CARRIER)) {
                                        ignoreFactor = Math.min(0.05d, ignoreFactor);
                                    }
                                }

                                log.debug("IgnoreFactor [" + icuTLOwner.getName() + "=>" + icu.getName() + "]=" + ignoreFactor);

                                int value = icu.getCount();

                                if (icuTLOwner instanceof DefenseBuilding) {
                                    DefenseBuilding db = (DefenseBuilding) icuTLOwner;
                                    boolean targetListAssigned = false;

                                    switch (db.getConstruction().getId()) {
                                        case (Construction.ID_PLANETARYMISSLEBASE):
                                            targetListAssigned = true;
                                            tlc.addTarget(combatArray.getUnit(i, j, k), icu, value * chassisSizeEnemy * ignoreFactor, targetFireIndicator * 3d);
                                            break;
                                        case (Construction.ID_PLANETARYLASERCANNON):
                                            targetListAssigned = true;
                                            switch (chassisSizeEnemy) {
                                                case (1):
                                                case (2):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 1, targetFireIndicator * 3d);
                                                    break;
                                                case (3):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 20, targetFireIndicator * 3d);
                                                    break;
                                                case (4):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 40, targetFireIndicator * 3d);
                                                    break;
                                                case (5):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 200, targetFireIndicator * 3d);
                                                    break;
                                                case (6):
                                                case (7):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 80, targetFireIndicator * 3d);
                                                    break;
                                                case (8):
                                                case (9):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 40, targetFireIndicator * 3d);
                                                    break;
                                                case (10):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 20, targetFireIndicator * 3d);
                                                    break;
                                            }
                                            break;
                                        case (Construction.ID_PLANETARY_FORTRESS):
                                        case (Construction.ID_PLANETARY_INTERVALCANNON):
                                        case (Construction.ID_SUNTRANSMITTER_FORTRESS):
                                            targetListAssigned = true;
                                            switch (chassisSizeEnemy) {
                                                case (1):
                                                case (2):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 1, targetFireIndicator * 3d);
                                                    break;
                                                case (3):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 20, targetFireIndicator * 3d);
                                                    break;
                                                case (4):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 40, targetFireIndicator * 3d);
                                                    break;
                                                case (5):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 80, targetFireIndicator * 3d);
                                                    break;
                                                case (6):
                                                case (7):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 100, targetFireIndicator * 3d);
                                                    break;
                                                case (8):
                                                case (9):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 200, targetFireIndicator * 3d);
                                                    break;
                                                case (10):
                                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, 100, targetFireIndicator * 3d);
                                                    break;
                                            }
                                            break;
                                        default:
                                            DebugBuffer.warning("No targetlist definition available for " + db.getConstruction().getName() + "(Will be ignored in battle)");
                                            break;
                                    }
                                } else {
                                    tlc.addTarget(combatArray.getUnit(i, j, k), icu, value * chassisSizeEnemy * ignoreFactor, targetFireIndicator * 3d);
                                }

                                log.debug("Added target " + icu.getName() + " for " + combatArray.getUnit(i, j, k).getName() + " with value " + value);
                            }
                        }
                    }

                }
            }
        }
    }

    private double getTargetFireIndictator(EShieldStrength shieldLevel, double overwhelm) {
        double targetFireIndicator = 1d;

        if (shieldLevel == EShieldStrength.LIGHT_SHIELDED) {
            targetFireIndicator = 2d * overwhelm;
        } else if (shieldLevel == EShieldStrength.MEDIUM_SHIELDED) {
            targetFireIndicator = 4d * overwhelm;
        } else if (shieldLevel == EShieldStrength.HEAVY_SHIELDED) {
            targetFireIndicator = 8d * overwhelm;
        } else if (shieldLevel == EShieldStrength.VERY_HEAVY_SHIELDED) {
            targetFireIndicator = 16d * overwhelm;
        }

        if (targetFireIndicator < 1d) {
            targetFireIndicator = 1d;
        }
        return targetFireIndicator;
    }

    private void setPlanetaryShield() {
        planetaryShield = new ShieldDefinition();

        if (pp == null) {
            return;
        }

        // Load free Energy from Planet
        ProductionResult productionResult = null;
        try {
            PlanetCalculation pc = new PlanetCalculation(combatLocationId);
            productionResult = pc.getPlanetCalcData().getProductionData();
        } catch (Exception e) {
            e.printStackTrace();
            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, " Error while loading free energy" + e);
        }
        long freeEnergy = productionResult.getRessProduction(Ressource.ENERGY) - productionResult.getRessConsumption(Ressource.ENERGY);

        log.debug("Energy Production " + productionResult.getRessProduction(Ressource.ENERGY) + " Energy used " + productionResult.getRessConsumption(Ressource.ENERGY));

        long shieldPower = 0;

        if (freeEnergy < 0) {
            freeEnergy = 0;
        }

        if (iss != null) {
            log.debug("Create simulation shield: " + iss.hasHUShield() + " / " + iss.hasPAShield());
            freeEnergy = 1000000;
        } else {
            log.debug("Dont create simulation shield");
        }

        int shieldType = -1;

        if (iss != null) {
            if (iss.hasPAShield()) {
                shieldType = PLANETARY_PA;
                shieldPower = freeEnergy * 15;
            } else if (iss.hasHUShield()) {
                shieldType = PLANETARY_HU;
                shieldPower = freeEnergy * 10;
            }
        } else {
            if (pcDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETARY_HUESHIELD) != null) {
                shieldType = PLANETARY_HU;
                shieldPower = freeEnergy * 10;
            }
            if (pcDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETARY_PARATRONSHIELD) != null) {
                shieldType = PLANETARY_PA;
                shieldPower = freeEnergy * 15;
            }
        }

        log.debug("Planetary Shield power is " + shieldPower);

        if (freeEnergy > 0) {
            if (shieldType == PLANETARY_HU) {
                planetaryShield.setShield_HU((int) shieldPower);
                planetaryShield.setSharedShield_HU((int) shieldPower);
                planetaryShield.setEnergyNeed_HU((int) freeEnergy);
                planetaryShield.setShieldEnergy((int) freeEnergy);
            } else if (shieldType == PLANETARY_PA) {
                planetaryShield.setShield_PA((int) shieldPower);
                planetaryShield.setSharedShield_PA((int) shieldPower);
                planetaryShield.setEnergyNeed_PA((int) freeEnergy);
                planetaryShield.setShieldEnergy((int) freeEnergy);
            }

            // log.debug("Planetary shield strength: " + planetaryShield.getShield_HU());
            // planetaryShield.setShieldEnergyPerShip(shieldEnergyConsumption);
            // planetaryShield.setShieldEnergy(shieldEnergyConsumption * count);
        }
    }

    @Override
    public TargetList getMyTargetList(BattleShipNew bs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTargetLists() {
        /*
         for (TargetList tl : targetMap.values()) {
         tl.buildTotalTargetValue();
         }
         */
    }

    @Override
    public HashMap<Integer, Integer> getGroupCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean skip(ICombatUnit icu) {
        // TODO Basically each fleet should have a tracker
        // if (bs.getFleetTracker() == null) {
        //    return false;
        // }

        CombatGroupFleet cgf = icu.getParentCombatGroupFleet();

        // log.debug("Check fleet " + cgf.getFleetName() + " for resigned");
        if (cgf.isResigned() || cgf.isRetreated()) {
            // log.debug("Result: true");
            return true;
        }

        // log.debug("Result: false");
        return false;
    }
}
