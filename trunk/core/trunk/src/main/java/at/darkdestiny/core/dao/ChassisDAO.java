/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class ChassisDAO extends ReadWriteTable<Chassis> implements GenericDAO  {
    public Chassis findById(int id) {
        Chassis c = new Chassis();
        c.setId(id);
        return (Chassis)get(c);
    }

    public Chassis findByRelModuleId(int relModuleId) {
        Chassis c = new Chassis();
        c.setRelModuleId(relModuleId);
        ArrayList<Chassis> result = find(c);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<Chassis> findAllOrdered() {
        ArrayList<Chassis> result = new ArrayList<Chassis>();

        TreeMap<Integer, Chassis> sorted = new TreeMap<Integer, Chassis>();
        for(Chassis c : (ArrayList<Chassis>)findAll()){
            sorted.put(c.getId(), c);
        }
        result.addAll(sorted.values());
        return result;
    }
}
