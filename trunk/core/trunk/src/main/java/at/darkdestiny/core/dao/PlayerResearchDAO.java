/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlayerResearchDAO extends ReadWriteTable<PlayerResearch> implements GenericDAO {

    public ArrayList<PlayerResearch> findByUserId(Integer userId) {
        PlayerResearch pr = new PlayerResearch();
        pr.setUserId(userId);
        return find(pr);
    }

    public PlayerResearch findBy(Integer userId, Integer researchId) {
        PlayerResearch pr = new PlayerResearch();
        pr.setUserId(userId);
        pr.setResearchId(researchId);
        return (PlayerResearch)get(pr);
    }

    public ArrayList<PlayerResearch> findByResearchId(Integer researchId) {
        PlayerResearch pr = new PlayerResearch();
        pr.setResearchId(researchId);
        return find(pr);
    }
}
