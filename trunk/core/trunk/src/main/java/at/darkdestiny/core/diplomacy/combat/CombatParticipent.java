/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy.combat;

import at.darkdestiny.core.model.DiplomacyRelation;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface CombatParticipent {
    public DiplomacyRelation getRelationToAlliance(AllianceEntry ae);
    public DiplomacyRelation getRelationToPlayer(PlayerEntry pe);
    public DiplomacyRelation getRelationTo(CombatParticipent cp);
    public ArrayList<Integer> getAllMemberIds();
    public int getId();
}
