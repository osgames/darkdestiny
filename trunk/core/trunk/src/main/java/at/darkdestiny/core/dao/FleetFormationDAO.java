/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.FleetFormation;
import at.darkdestiny.core.service.SystemService;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class FleetFormationDAO extends ReadWriteTable<FleetFormation> implements GenericDAO {
    public FleetFormation findById(int formationId) {
        FleetFormation ffSearch = new FleetFormation();
        ffSearch.setId(formationId);
        return (FleetFormation)get(ffSearch);
    }

    public ArrayList<FleetFormation> findFleetFormations(int userId) {
        ArrayList<FleetFormation> allFF = findAll();
        ArrayList<FleetFormation> result = new ArrayList<FleetFormation>();

        for (FleetFormation ff : allFF) {
            if (SystemService.areAllied(userId, ff.getUserId())) {
                result.add(ff);
            }
        }

        return result;
    }
}
