/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Campaign;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class CampaignDAO extends ReadWriteTable<Campaign> implements GenericDAO  {
    public ArrayList<Campaign> findAllOrdered() {
        ArrayList<Campaign> result = new ArrayList<Campaign>();
        result = findAll();
        TreeMap<Integer, Campaign> sorted = new TreeMap<Integer, Campaign>();
        for(Campaign c : result){
            sorted.put(c.getOrder(), c);
        }
        result.clear();
        result.addAll(sorted.values());

        return result;
    }
}
