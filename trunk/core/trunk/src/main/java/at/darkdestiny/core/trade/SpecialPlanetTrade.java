/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.trade;

import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.construction.TotalConstructionResult;
import at.darkdestiny.core.dao.ActiveBonusDAO;
import at.darkdestiny.core.dao.SpecialTradeDAO;
import at.darkdestiny.core.model.ActiveBonus;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.SpecialTrade;
import at.darkdestiny.core.scanner.AbstractCoordinate;
import at.darkdestiny.core.scanner.StarMapQuadTree;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.tools.graph.DefaultNode;
import at.darkdestiny.core.tools.graph.Edge;
import at.darkdestiny.core.tools.graph.Graph;
import at.darkdestiny.core.tools.graph.Node;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.core.trade.special.TradeGoodsNode;
import at.darkdestiny.core.trade.special.TradeGoodsNode.ETradeNodeType;
import at.darkdestiny.core.trade.special.TransferringEntry;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Stefan
 */
public class SpecialPlanetTrade {

    private TotalConstructionResult tcr;

    private static ActiveBonusDAO activeBonusDAO = (ActiveBonusDAO) DAOFactory.get(ActiveBonusDAO.class);
    private static SpecialTradeDAO specialTradeDAO = DAOFactory.get(SpecialTradeDAO.class);

    private static final int RADIUS = 300;

    public SpecialPlanetTrade() {
        tcr = new TotalConstructionResult(80,null);        
        // Gather all planets with Megacities
        Map<Integer, Integer> megacitiesPerPlanet = tcr.allConsByConsId.get(80);
        List<ActiveBonus> abListIndustrial = activeBonusDAO.findByBonusId(5); // Industrial
        List<ActiveBonus> abListTrade = activeBonusDAO.findByBonusId(10); // Trade

        DebugBuffer.trace("MEGACITY ENTRIES: " + megacitiesPerPlanet.size());
        DebugBuffer.trace("INDUSTRIAL ENTRIES: " + abListIndustrial.size());
        DebugBuffer.trace("TRADE ENTRIES: " + abListTrade.size());
        
        // Assign each industrial planet to nearest trading planet
        // Assign each megacity planet to nearest trading planet
        // Connect trading planets (always 2 nearest)
        
        Graph<TradeGoodsNode> result = buildGraph(megacitiesPerPlanet, abListIndustrial, abListTrade, 300);
       
        // Try to assign all industry production to receivers
        assignConsumersToProducers(result);        
        
        // After all routes have been calculated, loop all Nodes and write out results to database
        specialTradeDAO.removeAll();
        
        for (Node n : result.getNodes()) {
            TradeGoodsNode tgn = (TradeGoodsNode)n;
            
            ArrayList<TransferringEntry> teEntries = tgn.getRoutes();
            int incomingQty = 0;
            int outgoingQty = 0;
            int routedQty = 0;
            
            int incomingRoutes = 0;
            int outgoingRoutes = 0;
            int transportRoutes = 0;
            
            for (TransferringEntry te : teEntries) {
                TradeGoodsNode tgnStart = (TradeGoodsNode)te.getStartNode();
                TradeGoodsNode tgnTarget = (TradeGoodsNode)te.getTargetNode();
                
                if (tgnStart == n) {
                    outgoingQty += te.getQuantity();
                    outgoingRoutes++;
                } else if (tgnTarget == n) {
                    incomingQty += te.getQuantity();
                    incomingRoutes++;
                } else {
                    routedQty += te.getQuantity();
                    transportRoutes++;
                }
            }
            
            if ((incomingQty != 0) || (outgoingQty != 0) || (routedQty != 0)) {
                // Create specialTrade Entry
                SpecialTrade st = specialTradeDAO.getByPlanetId(tgn.getSystemId());
                boolean update = true;
                
                if (st == null) {
                    st = new SpecialTrade();
                    update = false;
                } 
                
                // This is actually the planetId now 
                st.setPlanetId(tgn.getSystemId());
                st.setProduction(0);
                st.setConsumption(0);
                st.setIncomingGoods(incomingQty);
                st.setOutgoingGoods(outgoingQty);
                st.setRoutedCapacity(routedQty);
                st.setIncomingRoutes(incomingRoutes);
                st.setOutgoingRoutes(outgoingRoutes);
                st.setRoutedCapacity(transportRoutes);
                
                if (update) {
                    specialTradeDAO.update(st);
                } else {
                    specialTradeDAO.add(st);
                }
            }
        }
    }

    static Graph buildGraph(Map<Integer, Integer> megacitiesPerPlanet, List<ActiveBonus> abListIndustrial, List<ActiveBonus> abListTrade) {
        return buildGraph(megacitiesPerPlanet, abListIndustrial, abListTrade, RADIUS);
    }

    private static void addNodes(List<ActiveBonus> list, TradeGoodsNode.ETradeNodeType type, Graph<TradeGoodsNode> graph, StarMapQuadTree<TradeGoodsNode> smqt, ArrayList<TradeGoodsNode> allNodes) {

        for (ActiveBonus activeBonus : list) {
            Planet p = Service.planetDAO.findById(activeBonus.getRefId());
            at.darkdestiny.core.model.System s = Service.systemDAO.findById(p.getSystemId());
            TradeGoodsNode gdn = addPlanet(graph, p.getId(), type, s.getId(), s.getX(), s.getY());
            if (type == ETradeNodeType.INDUSTRIAL) {
                gdn.setProductionValue(20);
            }
            if (!allNodes.contains(gdn)) {
                allNodes.add(gdn);
            }
            smqt.addItemToTree(gdn);
        }
    }

    static Graph buildGraph(Map<Integer, Integer> megacitiesPerPlanet, List<ActiveBonus> abListIndustrial, List<ActiveBonus> abListTrade, int radius) {
        Graph<TradeGoodsNode> graph = new Graph<TradeGoodsNode>();
        ArrayList<TradeGoodsNode> allNodes = new ArrayList<TradeGoodsNode>();

        StarMapQuadTree<TradeGoodsNode> smqt = new StarMapQuadTree(GameConstants.UNIVERSE_WIDTH, GameConstants.UNIVERSE_HEIGHT, 5);

        for (Map.Entry<Integer, Integer> entry : megacitiesPerPlanet.entrySet()) {
            Planet p = Service.planetDAO.findById(entry.getKey());
            at.darkdestiny.core.model.System s = Service.systemDAO.findById(p.getSystemId());
            TradeGoodsNode gdn = addPlanet(graph, p.getId(), TradeGoodsNode.ETradeNodeType.MEGACITY, s.getId(), s.getX(), s.getY());
            gdn.setConsumptionValue(10 * entry.getValue());
            
            if (!allNodes.contains(gdn)) {
                allNodes.add(gdn);
            }
            smqt.addItemToTree(gdn);

        }
        addNodes(abListTrade, TradeGoodsNode.ETradeNodeType.TRADE, graph, smqt, allNodes);
        addNodes(abListIndustrial, TradeGoodsNode.ETradeNodeType.INDUSTRIAL, graph, smqt, allNodes);

        Set<AbstractCoordinate> processedNodes = Sets.newHashSet();
        Queue<TradeGoodsNode> toProcess = new LinkedList<TradeGoodsNode>();

        toProcess.add(allNodes.get(0));

        while (!toProcess.isEmpty()) {
            recursiveSearch(graph, smqt, toProcess, processedNodes, radius);

            if (processedNodes.size() < allNodes.size()) {
                for (TradeGoodsNode nTmp : allNodes) {
                    boolean notFound = true;

                    for (AbstractCoordinate nTmp2 : processedNodes) {
                        if (nTmp.getSystemId() == ((TradeGoodsNode) nTmp2).getSystemId()) {
                            notFound = false;
                            break;
                        }
                    }

                    if (notFound) {
                        toProcess.add(nTmp);
                        break;
                    }
                }
            }
        }

        return graph;
    }



    private static TradeGoodsNode addPlanet(Graph<TradeGoodsNode> graph, int planetId, TradeGoodsNode.ETradeNodeType type, int systemId, int x, int y) {
        TradeGoodsNode n = graph.getNode("" + systemId);
        if (n == null) {
            try {
                // n = new TradeGoodsNode(systemId, x, y);
                n = new TradeGoodsNode(planetId, x, y);
                graph.getNodes().add(n);
            } catch (Exception ex) {
                Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (n instanceof TradeGoodsNode) {
            TradeGoodsNode tgn = (TradeGoodsNode) n;
            tgn.addPlanetId(planetId, type);
            return n;
        }

        return null;
    }

    private void assignConsumersToProducers(Graph network) {
        List<Node> allNodes = network.getNodes();
        

        // HashMap<Node,ArrayList<LinkedList<Node>>> deliveryPaths = Maps.newHashMap();
        
        
        // Select a production Node, if found start broad-search for possible consumers
        for (Node n : allNodes) {            
            TradeGoodsNode tgn = (TradeGoodsNode)n;
            int unassignedProduction = tgn.getProductionValue();
                        
            if (tgn.getProductionValue() > 0) {
                int assignedQty = 0;
                
                do {
                    List<Node> processedNodes = new ArrayList<Node>();
                    List<Node> processingList = new ArrayList<Node>();            
                    HashMap<Node,ArrayList<Node>> backTrack = Maps.newHashMap();                    
                    
                    LinkedList<Node> path = new LinkedList<Node>();                
                    processingList.add(tgn);
                    assignedQty = recursiveBroadSearch(processingList,processedNodes,backTrack,path,tgn);
                    unassignedProduction -= assignedQty;
                    
                    java.lang.System.out.println("Just assigned " + assignedQty + " -- remaining " + unassignedProduction);
                    
                    // path.add(0, tgn);
                    // java.lang.System.out.println("Add node " + tgn.toString() + "["+tgn.hashCode()+"] to path");
                    Node lastNode = null;
                    
                    for (Node nPath : path) {
                        if (lastNode != null) {
                            Edge e = network.getEdge(lastNode, nPath);
                            if (e != null) {
                                e.setValue(e.getValue() + assignedQty);
                            }
                        }
                        
                        lastNode = nPath;
                        
                        TradeGoodsNode activePathNode = (TradeGoodsNode)nPath;
                        activePathNode.setOverrideColor(Color.WHITE);
                        TransferringEntry te = new TransferringEntry();
                        te.setPath(path);
                        te.setQuantity(assignedQty);        
                        activePathNode.addRoute(te);                        
                    }
                } while ((unassignedProduction > 0) && (assignedQty > 0));
            }
        }
    }    
    
    // Connect producers and consumers
    private int recursiveBroadSearch(List<Node> toProcess, List<Node> processedNodes, HashMap<Node,ArrayList<Node>> backTrack, LinkedList<Node> path, TradeGoodsNode startNode) {                        
        java.lang.System.out.println("Recursive search: TOPROCESS >> " + toProcess.size() + " PROCESSEDNODES >> " + processedNodes.size() + " BACKTRACKENTRIES >> " + backTrack.size());
        
        List<Node> toProcessNew = Lists.newArrayList();
        
        for (Node n : toProcess) {
            for (Object o : n.getEdges()) {
                Edge e = (Edge)o;
                TradeGoodsNode nextNode;
                
                if (e.getNode1() == n) {
                    nextNode = (TradeGoodsNode)e.getNode2();                      
                    if (processedNodes.contains(nextNode)) continue;                                       
                } else {
                    nextNode = (TradeGoodsNode)e.getNode1();
                    if (processedNodes.contains(nextNode)) continue;
                }
                
                if (nextNode.getConsumptionValue() > 0) {                    
                    int qty = Math.min(nextNode.getFreeConsumption(), startNode.getFreeProduction());                   
                    if (qty > 0) {
                        java.lang.System.out.println("Found consuming node! (Transferring " + qty + " goods)");                                        

                        path.add(n);                    
                        path.add(nextNode);                       

                        java.lang.System.out.println("Add node " + nextNode.toString() + "["+nextNode.hashCode()+"] to path");
                        java.lang.System.out.println("Add node " + n.toString() + "["+n.hashCode()+"] to path");

                        return qty;
                    }
                }
                
                processedNodes.add(nextNode);
                
                if (!nextNode.isTradePlanet()) {
                    continue;
                }                
                
                toProcessNew.add(nextNode);
                
                ArrayList<Node> backTrackNodes = backTrack.get(nextNode);
                if (backTrackNodes == null) {
                    backTrackNodes = new ArrayList<Node>();
                    backTrackNodes.add(n);
                    backTrack.put(nextNode,backTrackNodes);
                } else {
                    backTrackNodes.add(n);
                }
            }
        }
        
        if (toProcessNew.isEmpty()) return -1;
        int result = recursiveBroadSearch(toProcessNew,processedNodes,backTrack,path,startNode);
        
        if (result > 0) {
            Node nbt = backTrack.get(path.get(0)).get(0);
            
            java.lang.System.out.println("Add node " + nbt.toString() + "["+nbt.hashCode()+"] to path");
            path.add(0, nbt);
            return result;
        }
        
        return -1;        
    }
    
    // Build base graph
    private static void recursiveSearch(Graph graph, StarMapQuadTree<TradeGoodsNode> smqt, Queue<TradeGoodsNode> toProcess, Set<AbstractCoordinate> processedNodes, int radius) {
        System.out.println("Enter recursion");

        if (toProcess.isEmpty()) {
            System.out.println("Terminate recursion branch");
            return;
        }

        ArrayList<TradeGoodsNode> toProcessTmp = new ArrayList<TradeGoodsNode>();

        while (!toProcess.isEmpty()) {
            TradeGoodsNode n1 = toProcess.poll();
            processedNodes.add(n1);

            for (TradeGoodsNode tradeGoodsNode : smqt.getItemsAround(n1.x, n1.y, radius)) {
                if (contains(tradeGoodsNode, processedNodes)) {
                    continue;
                }

                if (tradeGoodsNode.getSystemId() == n1.getSystemId()) {
                    continue;
                }

                toProcessTmp.add(tradeGoodsNode);

                Node n1g = new DefaultNode("" + n1.getSystemId(), n1.x, n1.y);
                Node n2g = new DefaultNode("" + tradeGoodsNode.getSystemId(), tradeGoodsNode.x, tradeGoodsNode.y);

                graph.addEdge(n1g, n2g);
            }
        }

        toProcess.addAll(toProcessTmp);
        recursiveSearch(graph, smqt, toProcess, processedNodes, radius);

    }

    private static boolean contains(AbstractCoordinate ac, Set<AbstractCoordinate> nodes) {
        for (AbstractCoordinate node : nodes) {
            if (node.x == ac.x && node.y == ac.y) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param aActiveBonusDAO the activeBonusDAO to set
     */
    public void setActiveBonusDAO(ActiveBonusDAO aActiveBonusDAO) {
        activeBonusDAO = aActiveBonusDAO;
    }
}
