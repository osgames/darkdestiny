/*
 * CombatGroupShipDesign.java
 *
 * Created on 05. November 2005, 21:23
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package at.darkdestiny.core.spacecombat;

import at.darkdestiny.util.DebugBuffer;

public class CombatGroupShipDesign {
    private int designId;
    private int count;
    private boolean isPlanetaryDefense;
    private boolean isOrbitalDefense;

    public int getDesignId() {
        return designId;
    }

    public void setDesignId(int designId) {
        this.designId = designId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    public CombatGroupShipDesign copy() {
        try {
            return (CombatGroupShipDesign)this.clone();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Cloning of CombatGroupShipDesign failed!",e);
        }
        return null;
    }

    public boolean isIsPlanetaryDefense() {
        return isPlanetaryDefense;
    }

    public void setIsPlanetaryDefense(boolean isPlanetaryDefense) {
        this.isPlanetaryDefense = isPlanetaryDefense;
    }

    public boolean isIsOrbitalDefense() {
        return isOrbitalDefense;
    }

    public void setIsOrbitalDefense(boolean isOrbitalDefense) {
        this.isOrbitalDefense = isOrbitalDefense;
    }
}
