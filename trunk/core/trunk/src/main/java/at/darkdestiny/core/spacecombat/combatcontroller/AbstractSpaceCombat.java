/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat.combatcontroller;

import at.darkdestiny.core.admin.module.AttributeTree;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResult;
import at.darkdestiny.core.interfaces.ISimulationSet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.spacecombat.CombatArray;
import at.darkdestiny.core.spacecombat.CombatGroup;
import at.darkdestiny.core.spacecombat.CombatHandler;
import at.darkdestiny.core.spacecombat.ISpaceCombat;
import at.darkdestiny.core.spacecombat.SpaceCombatDataLogger;
import at.darkdestiny.core.spacecombat.helper.ArmorShieldFactor;
import at.darkdestiny.core.utilities.MilitaryUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Stefan
 */
public abstract class AbstractSpaceCombat implements ISpaceCombat {
    protected MilitaryUtilities mu;
    protected final int combatLocation;
    protected final int combatLocationId;
    protected PlayerPlanet pp = null;

    protected CombatArray combatArray = new CombatArray();
    protected List<CombatGroup> combatGroups;
    protected int noOfCombatGroups;
    protected int highestNoOfFleets = 0;
    protected int highestNoOfDesigns = 0;
    protected int planetaryDefGroup = -1;

    protected HashMap<Integer, AttributeTree> atMap = new HashMap<Integer, AttributeTree>();
    protected HashMap<Integer, ArmorShieldFactor> asfMap = new HashMap<Integer, ArmorShieldFactor>();
    protected HashMap<Integer, ArmorShieldFactor> asfMapPlanetary = new HashMap<Integer, ArmorShieldFactor>();

    protected static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    protected static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);

    protected CombatGroupResult cgr;
    protected SpaceCombatDataLogger sc_DataLogger = new SpaceCombatDataLogger();

    protected ISimulationSet iss;

    public AbstractSpaceCombat(int combatLocation, int combatLocationId) {
        try {
            mu = new MilitaryUtilities();
            this.combatLocation = combatLocation;
            this.combatLocationId = combatLocationId;

            if (this.combatLocation == CombatHandler.ORBITAL_COMBAT) {
                pp = ppDAO.findByPlanetId(combatLocationId);
            } else {
                pp = null;
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in AbstractSpaceCombat Constructor: ", e);
            throw new RuntimeException("AbstractSpaceCombat: Instantiation error");
        }
        // DebugBuffer.addLine("Construction finished successfully");
    }

    @Override
    public void setCombatGroups(List<CombatGroup> combatGroups) {
        this.combatGroups = combatGroups;
        noOfCombatGroups = combatGroups.size();
    }

    @Override
    public void setCombatGroupResult(CombatGroupResult cgr) {
        this.cgr = cgr;
    }

    @Override
    public SpaceCombatDataLogger getLoggingData() {
        return sc_DataLogger;
    }

    /**
     * @return the iss
     */
    @Override
    public ISimulationSet getISimulationSet() {
        return iss;
    }

    /**
     * @param iss the iss to set
     */
    @Override
    public void setISimulationSet(ISimulationSet iss) {
        this.iss = iss;
    }
}
