/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "suntransmitterroute")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class SunTransmitterRoute extends Model<SunTransmitterRoute> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("startId")
    private Integer startId;
    @FieldMappingAnnotation("endId")
    private Integer endId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the startId
     */
    public Integer getStartId() {
        return startId;
    }

    /**
     * @param startId the startId to set
     */
    public void setStartId(Integer startId) {
        this.startId = startId;
    }

    /**
     * @return the endId
     */
    public Integer getEndId() {
        return endId;
    }

    /**
     * @param endId the endId to set
     */
    public void setEndId(Integer endId) {
        this.endId = endId;
    }
}
