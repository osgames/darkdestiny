/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

/**
 *
 * @author Stefan
 */
public enum ECloseReason {
    ACCEPTED, REFUSED, VOTE_LIMIT;
}
