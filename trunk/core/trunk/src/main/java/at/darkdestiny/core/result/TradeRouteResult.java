/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.ETradeRouteSorting;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author Stefan
 */
public class TradeRouteResult {

    private PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private final ArrayList<TradeRouteExt> routes = new ArrayList<TradeRouteExt>();

    public void addRoute(TradeRouteExt tre) {
        if (!routes.contains(tre)) {
            routes.add(tre);
        }
    }

    public ArrayList<TradeRouteExt> getRoutes() {
        return routes;
    }

    public ArrayList<TradeRouteDetail> getRouteDetails(TradeRouteExt tre) {
        return tre.getRouteDetails();
    }

    public ArrayList<TradeRouteDetail> getRouteDetailsFromTo(TradeRouteExt tre) {
        ArrayList<TradeRouteDetail> trdList = new ArrayList<TradeRouteDetail>();

        TreeSet<TradeRouteDetail> sortedDetails = new TreeSet<TradeRouteDetail>();
        for (TradeRouteDetail trd : getRouteDetails(tre)) {
            if (trd.getReversed()) {
                continue;
            }
            sortedDetails.add(trd);
        }

        trdList.addAll(sortedDetails);

        return trdList;
    }

    public ArrayList<TradeRouteDetail> getRouteDetailsToFrom(TradeRouteExt tre) {
        ArrayList<TradeRouteDetail> trdList = new ArrayList<TradeRouteDetail>();

        TreeSet<TradeRouteDetail> sortedDetails = new TreeSet<TradeRouteDetail>();
        for (TradeRouteDetail trd : getRouteDetails(tre)) {
            if (!trd.getReversed()) {
                continue;
            }
            sortedDetails.add(trd);
        }

        trdList.addAll(sortedDetails);

        return trdList;
    }

    public TradeRouteResult getSortedResult(ETradeRouteSorting trs) {
        TradeRouteResult trr = new TradeRouteResult();

        TreeMap<String, TreeSet<TradeRouteExt>> sorted = new TreeMap<String, TreeSet<TradeRouteExt>>();

        for (TradeRouteExt tre : routes) {
            if (trs == ETradeRouteSorting.SORT_BY_START) {
                PlayerPlanet pp = ppDAO.findByPlanetId(tre.getBase().getStartPlanet());

                if (pp == null) {
                    DebugBuffer.warning("Invalid planet " + tre.getBase().getStartPlanet() + " in route id " + tre.getBase().getId());
                    continue;
                }

                String planetName = pp.getName();
                if (sorted.containsKey(planetName)) {
                    sorted.get(planetName).add(tre);
                } else {
                    TreeSet<TradeRouteExt> trList = new TreeSet<TradeRouteExt>();
                    trList.add(tre);
                    sorted.put(planetName, trList);
                }
            } else if (trs == ETradeRouteSorting.SORT_BY_TARGET) {
                PlayerPlanet pp = ppDAO.findByPlanetId(tre.getBase().getTargetPlanet());

                if (pp == null) {
                    DebugBuffer.warning("Invalid planet " + tre.getBase().getTargetPlanet() + " in route id " + tre.getBase().getId());
                    continue;
                }

                String planetName = pp.getName();
                if (sorted.containsKey(planetName)) {
                    sorted.get(planetName).add(tre);
                } else {
                    TreeSet<TradeRouteExt> trList = new TreeSet<TradeRouteExt>();
                    trList.add(tre);
                    sorted.put(planetName, trList);
                }
            }
        }

        for (TreeSet<TradeRouteExt> treList : sorted.values()) {
            for (TradeRouteExt tre : treList) {
                trr.addRoute(tre);
            }
        }

        return trr;
    }
}
