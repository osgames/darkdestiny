/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view.menu.statusbar;

import at.darkdestiny.core.view.menu.StatusBarData;

/**
 *
 * @author Bullet
 */
public abstract class StatusBarEntity implements IStatusBarEntity {

    protected StatusBarData statusBarData;
    protected int width;
    protected int userId;
    protected String template;

    public StatusBarEntity() {
    }

    public StatusBarEntity(StatusBarData statusBarData, int userId, int width) {
        this.statusBarData = statusBarData;
        this.userId = userId;
        this.width = width;

    }
}
