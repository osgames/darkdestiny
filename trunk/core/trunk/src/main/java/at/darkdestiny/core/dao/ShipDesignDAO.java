/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ShipDesignDAO extends ReadWriteTable<ShipDesign> implements GenericDAO {

    public ShipDesign findById(Integer id) {
        ShipDesign sd = new ShipDesign();
        sd.setId(id);
        return (ShipDesign)get(sd);
    }

    public ArrayList<ShipDesign> findByUserId(int userId) {
        ShipDesign sd = new ShipDesign();
        sd.setUserId(userId);
        return find(sd);
    }

    public ArrayList<ShipDesign> findByUserIdAndChassis(int userId, int chassisId) {
        ShipDesign sd = new ShipDesign();
        sd.setUserId(userId);
        sd.setChassis(chassisId);
        return find(sd);
    }


    public ArrayList<ShipDesign> findByChassisId(int chassisId) {
        ShipDesign sd = new ShipDesign();
        sd.setChassis(chassisId);
        return find(sd);
    }
}
