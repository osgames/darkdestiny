/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

/**
 *
 * @author Stefan
 */
public class StartLocationReqInfo {
    private boolean reqSuccess = false;
    private String message;
    private String selectedTile;
    private String sectorIdentifier;

    /**
     * @return the reqSuccess
     */
    public boolean isReqSuccess() {
        return reqSuccess;
    }

    /**
     * @param reqSuccess the reqSuccess to set
     */
    public void setReqSuccess(boolean reqSuccess) {
        this.reqSuccess = reqSuccess;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the sectorIdentifier
     */
    public String getSectorIdentifier() {
        return sectorIdentifier;
    }

    /**
     * @param sectorIdentifier the sectorIdentifier to set
     */
    public void setSectorIdentifier(String sectorIdentifier) {
        this.sectorIdentifier = sectorIdentifier;
    }

    /**
     * @return the selectedTile
     */
    public String getSelectedTile() {
        return selectedTile;
    }

    /**
     * @param selectedTile the selectedTile to set
     */
    public void setSelectedTile(String selectedTile) {
        this.selectedTile = selectedTile;
    }
}
