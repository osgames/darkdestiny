/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.VoteOption;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class VoteOptionDAO extends ReadWriteTable<VoteOption> implements GenericDAO {

    public VoteOption findById(int id) {
        VoteOption vo = new VoteOption();
        vo.setId(id);
        return (VoteOption)get(vo);
    }

    public ArrayList<VoteOption> findByVoteId(int voteId) {
        VoteOption vo = new VoteOption();
        vo.setVoteId(voteId);
        ArrayList<VoteOption> result = find(vo);

        int fillerId = 10000;

        TreeMap<Integer, VoteOption> sortedResult = new TreeMap<Integer, VoteOption>();
        for (VoteOption tmpVo : result) {
            if (sortedResult.containsKey(tmpVo.getType())) {
                sortedResult.put(tmpVo.getType() + fillerId, tmpVo);
                fillerId++;
            } else {
                sortedResult.put(tmpVo.getType(), tmpVo);
            }
        }

        result.clear();
        result.addAll(sortedResult.values());
        return result;

    }
}
