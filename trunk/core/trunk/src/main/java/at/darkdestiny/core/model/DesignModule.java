/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation("designmodule")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class DesignModule extends Model<DesignModule> {

    @IdFieldAnnotation
    @FieldMappingAnnotation("designId")
    private Integer designId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("moduleId")
    private Integer moduleId;
    @FieldMappingAnnotation("isConstruction")
    private Boolean isConstruction;
    @FieldMappingAnnotation("count")
    private Integer count;

    public DesignModule() {
    }

    public DesignModule(Integer designId, Integer moduleId, Boolean isConstruction, Integer count) {
        this.designId = designId;
        this.moduleId = moduleId;
        this.isConstruction = isConstruction;
        this.count = count;
    }



    public Integer getDesignId() {
        return designId;
    }

    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Boolean getIsConstruction() {
        return isConstruction;
    }

    public void setIsConstruction(Boolean isConstruction) {
        this.isConstruction = isConstruction;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
