/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EMessageFolder;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.model.Message;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class MessageDAO extends ReadWriteTable<Message> implements GenericDAO {
    public Message findById(int msgId) {
        Message m = new Message();
        m.setMessageId(msgId);
        return (Message)get(m);
    }

    @Deprecated
    public Message findBySourceId(int userId, int msgId) {
        Message m = new Message();
        m.setMessageId(msgId);
        m.setSourceUserId(userId);
        ArrayList<Message> result = find(m);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }

    }

    @Deprecated
    public Message findByTargetId(int userId, int msgId) {
        Message m = new Message();
        m.setMessageId(msgId);
        m.setTargetUserId(userId);
        ArrayList<Message> result = find(m);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }

    }

    public ArrayList<Message> findByTargetUserId(int userId) {

        Message m = new Message();
        m.setTargetUserId(userId);
        return find(m);
    }

    public ArrayList<Message> findBySourceUserId(int userId) {

        Message m = new Message();
        m.setSourceUserId(userId);
        return find(m);
    }
    public ArrayList<Message> findByTargetUserId(int userId, boolean archived) {

        Message m = new Message();
        m.setTargetUserId(userId);
        m.setArchiveByTarget(archived);
        return find(m);
    }

    public ArrayList<Message> findByUserId(int userId, EMessageFolder msgFolder) {
        Message m = new Message();

        if (msgFolder == EMessageFolder.INBOX) {
            m.setArchiveByTarget(false);
            m.setTargetUserId(userId);
        } else if (msgFolder == EMessageFolder.OUTBOX) {
            m.setSourceUserId(userId);
        } else if (msgFolder == EMessageFolder.ARCHIVE) {
            m.setTargetUserId(userId);
            m.setArchiveByTarget(true);
        }

        return find(m);
    }

    public ArrayList<Message> findBySourceUserId(int userId, boolean archived) {
        Message m = new Message();
        m.setSourceUserId(userId);
        m.setArchiveBySource(archived);
        return find(m);
    }

    public ArrayList<Message> findByUserId(int userId, EMessageFolder msgFolder, EMessageType type, boolean viewed) {
        Message m = new Message();
        m.setType(type);
        m.setViewed(viewed);

        if (msgFolder == EMessageFolder.INBOX) {
            m.setArchiveByTarget(false);
            m.setTargetUserId(userId);
        } else if (msgFolder == EMessageFolder.OUTBOX) {
            m.setSourceUserId(userId);
        } else if (msgFolder == EMessageFolder.ARCHIVE) {
            m.setTargetUserId(userId);
            m.setArchiveByTarget(true);
        }

        System.out.println("Search for : " + type + " returning : " + find(m).size());
        return find(m);
    }

    public ArrayList<Message> findByUserId(int userId, EMessageType type) {

        Message m = new Message();
        m.setTargetUserId(userId);
        m.setType(type);
        return find(m);
    }

    public ArrayList<Message> findInboxMessages(int userId) {

        Message m = new Message();
        m.setTargetUserId(userId);
        ArrayList<Message> result = find(m);
        TreeMap<Long, Message> sortedResult = new TreeMap<Long, Message>();
        for (Message tmpM : result) {
            long msgTime = tmpM.getTimeSent();

            while (sortedResult.containsKey(msgTime)) {
                msgTime += 1l;
            }

            sortedResult.put(msgTime, tmpM);
        }
        result.clear();
        result.addAll(sortedResult.descendingMap().values());

        TreeMap<Long, Message> newMes = new TreeMap<Long, Message>();
        TreeMap<Long, Message> oldMes = new TreeMap<Long, Message>();
        for (Message tmpM : result) {
            if (!tmpM.getDelByTarget() && !tmpM.getArchiveByTarget()) {
                if (!tmpM.getViewed()) {
                    newMes.put(tmpM.getTimeSent(), tmpM);
                } else {
                    oldMes.put(tmpM.getTimeSent(), tmpM);
                }
            }
        }
        result = new ArrayList<Message>();
        result.addAll(newMes.descendingMap().values());
        result.addAll(oldMes.descendingMap().values());
        return result;
    }
//   WHERE messages.sourceUserId="+userId+" AND delBySource=0 AND outMaster=1 ORDER BY timeSent DESC");

    public ArrayList<Message> findOutboxMessages(int userId) {
        Message m = new Message();
        m.setSourceUserId(userId);
        m.setDelBySource(false);
        m.setOutMaster(true);
        ArrayList<Message> result = find(m);
        TreeMap<Long, Message> sortedResult = new TreeMap<Long, Message>();
        for (Message tmpM : result) {
            sortedResult.put(tmpM.getTimeSent(), tmpM);
        }
        result.clear();
        result.addAll(sortedResult.descendingMap().values());

        return result;
    }

    public ArrayList<Message> findArchivedMessages(int userId) {

        Message m = new Message();
        m.setTargetUserId(userId);
        m.setDelByTarget(false);
        m.setArchiveByTarget(true);
        ArrayList<Message> result = find(m);
        TreeMap<Long, Message> sortedResult = new TreeMap<Long, Message>();
        for (Message tmpM : result) {
            sortedResult.put(tmpM.getTimeSent(), tmpM);
        }
        result.clear();
        result.addAll(sortedResult.descendingMap().values());

        return result;
    }
}
