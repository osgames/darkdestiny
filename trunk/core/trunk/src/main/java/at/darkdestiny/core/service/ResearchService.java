package at.darkdestiny.core.service;

 import at.darkdestiny.core.GameUtilities;import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.ModuleType;
import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.GroundTroopDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.ProductionDAO;
import at.darkdestiny.core.dao.ResearchDAO;
import at.darkdestiny.core.dao.ResearchProgressDAO;
import at.darkdestiny.core.dao.StatisticDAO;
import at.darkdestiny.core.dao.TechRelationDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EBonusType;
import at.darkdestiny.core.enumeration.ETechListType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.ITechFunctions;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.Production;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.ResearchProgress;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.Statistic;
import at.darkdestiny.core.model.TechRelation;
import at.darkdestiny.core.model.TechType;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.result.BonusResult;
import at.darkdestiny.core.result.ResearchResult;
import at.darkdestiny.core.utilities.BonusUtilities;
import at.darkdestiny.core.utilities.ResearchUtilities;
import at.darkdestiny.core.view.ResearchProgressView;
import at.darkdestiny.core.view.TechListView;
import at.darkdestiny.core.view.TechListViewEntry;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class ResearchService {

    private static final Logger log = LoggerFactory.getLogger(ResearchService.class);

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static ProductionDAO pDAO = (ProductionDAO) DAOFactory.get(ProductionDAO.class);
    private static ResearchDAO rDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    private static StatisticDAO sDAO = (StatisticDAO) DAOFactory.get(StatisticDAO.class);
    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static ResearchProgressDAO rpDAO = (ResearchProgressDAO) DAOFactory.get(ResearchProgressDAO.class);
    private static TechRelationDAO trDAO = (TechRelationDAO) DAOFactory.get(TechRelationDAO.class);
    private static ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    public final static int RESEARCH_VIEW_ALL = 1;
    public final static int RESEARCH_VIEW_RESEARCHED = 2;
    public final static int RESEARCH_VIEW_RESEARCHED_AND_INRESEARCH = 3;
    public final static int RESEARCH_VIEW_INRESEARCH = 4;
    public final static int RESEARCH_VIEW_ON_HOLD = 6;
    public final static int RESEARCH_VIEW_NOT_RESEARCHED = 5;
    public final static int RESEARCH_VIEW_NOT_RESEARCHABLE = 8;
    public final static int RESEARCH_VIEW_TECHTREE = 7;
    private static Properties atlan;
    public static final int PROCESS_RESUME = 1;
    public static final int PROCESS_START = 2;
    public static final int PROCESS_PAUSE = 3;

    public static final int LOCK_TECH = 999;
    
    public static Research findResearchById(int researchId) {

        return rDAO.findById(researchId);
    }

    public static boolean isResearched(int userId, int researchId) {
        return (prDAO.findBy(userId, researchId) != null);
    }

    public static ResearchResult getResearchPointsForPlanet(int planetId) {
        int resPoints = 0;
        int compResPoints = 0;
        boolean planetHasResearch = false;
        for (PlanetConstruction pc : Service.planetConstructionDAO.findByPlanetId(planetId)) {
            ArrayList<Production> pList = pDAO.findProductionForConstructionId(pc.getConstructionId());
            for (Production p : pList) {
                if (p.getRessourceId() == Ressource.RP) {
                    resPoints += p.getQuantity() * pc.getNumber();
                    planetHasResearch = true;
                } else if (p.getRessourceId() == Ressource.RP_COMP) {
                    compResPoints += p.getQuantity() * pc.getNumber();
                    planetHasResearch = true;
                }
            }
        }

        if (planetHasResearch) {
            try {
                PlanetCalculation pc = new PlanetCalculation(planetId);
                ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

                resPoints = (int) (resPoints * epcr.getEffResearch());
                compResPoints = (int) (compResPoints * epcr.getEffResearch());
            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Couldnt initialize PlanetCalculation");
            }
        }

        int bonusRP = 0;
        int bonusCRP = 0;
        BonusResult br = BonusUtilities.findBonus(planetId, EBonusType.RESEARCH);
        if (br != null) {
            bonusRP = (int) ((resPoints * br.getPercBonus()) + br.getNumericBonus());
            bonusCRP = (int) ((compResPoints * br.getPercBonus()) + br.getNumericBonus());
        }

        return new ResearchResult(resPoints, compResPoints, bonusRP, bonusCRP);
    }

    public static String getResearchString(int researchId, int userId) {
        String msg = "<TABLE>";
        msg += "<TR>";
        msg += "<TD><B>";
        msg += ML.getMLStr("research_requiredResearches", userId);
        msg += "</B></TD>";
        ArrayList<Research> ress = getRequiredResearchesForResearch(researchId);
        if (ress.isEmpty()) {
            msg += "<TD>";
            msg += ML.getMLStr("research_none", userId);
            msg += "</TD>";
        } else {
            boolean first = true;
            for (Research res : ress) {

                if (first) {
                    msg += "<TD>";
                    msg += ML.getMLStr(res.getName(), userId);
                    msg += "</TD>";

                    first = false;
                } else {
                    msg += "<TR>";
                    msg += "<TD>";
                    msg += "</TD>";
                    msg += "<TD>";
                    msg += ML.getMLStr(res.getName(), userId);
                    msg += "</TD>";
                }
                msg += "</TR>";
            }
        }
        msg += "<TR>";
        msg += "<TD><B>";
        msg += ML.getMLStr("research_enabledResearches", userId);
        msg += "</B></TD>";
        ress.clear();
        ress = getEnabledResearchesForResearch(researchId);
        if (ress.isEmpty()) {
            msg += "<TD>";
            msg += ML.getMLStr("research_none", userId);
            msg += "</TD>";
        } else {
            boolean first = true;
            for (Research res : ress) {

                if (first) {
                    msg += "<TD>";
                    msg += ML.getMLStr(res.getName(), userId);
                    msg += "</TD>";

                    first = false;
                } else {
                    msg += "<TR>";
                    msg += "<TD>";
                    msg += "</TD>";
                    msg += "<TD>";
                    msg += ML.getMLStr(res.getName(), userId);
                    msg += "</TD>";
                }
                msg += "</TR>";
            }
        }
        msg += "</TABLE>";

        return msg;
    }

    public static void updateResearchText(Map<String, String[]> allPars) {

        int researchId = Integer.parseInt(allPars.get("researchId")[0]);
        String shortText = allPars.get("shortText")[0];
        String detailText = allPars.get("detailText")[0];

        Research r = rDAO.findById(researchId);

        String shortTextKey = r.getName() + "_desc";
        String detailTextKey = r.getName() + "_desc_detail";
        if (!r.getShortText().equals(shortTextKey) || !r.getDetailText().equals(detailTextKey)) {
            if (!r.getShortText().equals(shortTextKey)) {
                r.setShortText(shortTextKey);
            }
            if (!r.getDetailText().equals(detailTextKey)) {
                r.setDetailText(detailTextKey);
            }
            rDAO.update(r);
        }


        try {

            Class type = ResourceBundle.class;
            Field cacheList = type.getDeclaredField("cacheList");
            cacheList.setAccessible(true);
            ((Map) cacheList.get(ResourceBundle.class)).clear();
            if (atlan == null) {
                atlan = new Properties();
                atlan.load(ResearchService.class.getResourceAsStream("/Atlan_de_DE.properties"));
            }
            log.debug("Size :" + atlan.size());
            log.debug("Key Size :" + atlan.keySet().size());
            log.debug("Pfad : " + ResearchService.class.getResource("/Atlan_de_DE.properties").getPath());
            atlan.setProperty(shortTextKey, shortText);
            atlan.setProperty(detailTextKey, detailText);
            FileOutputStream fos = new FileOutputStream(ResearchService.class.getResource("/Atlan_de_DE.properties").getPath());
            atlan.store(fos, "");
            fos.flush();
            fos.close();
            type = ResourceBundle.class;
            cacheList = type.getDeclaredField("cacheList");
            cacheList.setAccessible(true);
            ((Map) cacheList.get(ResourceBundle.class)).clear();
        } catch (Exception e) {
            log.debug("error while loading file : " + e);
        }



    }

    public static HashMap<Integer, Integer> getFinishedTime(int userId) {
        ArrayList<ResearchProgress> rpList = rpDAO.findByUserId(userId);
        HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();

        ResearchResult rr = ResearchService.getResearchPointsGlobal(userId);
        // Alle Forschungen

        int rpPoints = rr.getResearchPoints() + rr.getResearchPointsBonus();
        int crpPoints = rr.getCompResearchPoints() + rr.getCompResearchPointsBonus();
        int skipTicks = 0;

        if ((rpPoints == 0) && (crpPoints == 0)) {
            return result;
        }

        ArrayList<ResearchProgress> removeRP = new ArrayList<ResearchProgress>();
        HashSet<ResearchProgress> needsRP = new HashSet<ResearchProgress>();
        HashSet<ResearchProgress> needsCRP = new HashSet<ResearchProgress>();

        for (ResearchProgress actRp : rpList) {
            // log.debug("Summarizing researches ... looping " + actRp.getResearchId());

            Action a = aDAO.findResearchByTimeFinished(userId, actRp.getId());
            if (a == null) {
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Action for a ResearchProgress is not existing - Start");
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "UserId : " + userId + " ResearchProgress Id : " + actRp.getId());
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "ResearchId : " + actRp.getResearchId());
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Removing ResearchProgress THIS SHOULD NOT OCCUR");
                rpDAO.remove(actRp);
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Action for a ResearchProgress is not existing - End");
                continue;
            }
            if (a.getOnHold()) {
                // log.debug("Skip " + actRp.getResearchId() + " (onHold)");
                removeRP.add(actRp);
                continue;
            }

            if (actRp.getRpLeft() > 0) {
                needsRP.add(actRp);
            }
            if (actRp.getRpLeftComputer() > 0) {
                needsCRP.add(actRp);
            }
        }

        // log.debug(needsRP.size() + " res need RP / " + needsCRP.size() + " res need CRP");

        rpList.removeAll(removeRP);
        removeRP.clear();

        int time = 0;
        int timeLimit = 144 * 7;

        boolean finished = false;
        while (!finished) {
            time++;

            int multiplicator = 1;
            int highestTimeRP = 0;
            int highestTimeCRP = 0;

            if (skipTicks > 0) {
                multiplicator += skipTicks;
            }

            int totalRP = rpPoints * multiplicator;
            int totalRPComp = crpPoints * multiplicator;

            boolean recalculate = true;
            boolean recalculateRp = true;
            boolean recalculateCrp = true;
            int rpPerResearch = 0;
            int rpPerResearchComp = 0;

            removeRP.clear();

            for (ResearchProgress actRp : rpList) {
                // Recalculate if a research needed less than the rpPerResearch
                // log.debug("NEEDRP: " + needsRP.size() + " NEEDCRP: " + needsCRP.size() + " TOTALRP="+totalRP+" TOTALRPCOMP="+totalRPComp);

                if (recalculate) {
                    if (needsRP.size() > 0 && recalculateRp) {
                        rpPerResearch = (int) Math.ceil((double) totalRP / needsRP.size());
                        recalculateRp = false;
                    }
                    if (needsCRP.size() > 0 && recalculateCrp) {
                        rpPerResearchComp = (int) Math.ceil((double) totalRPComp / needsCRP.size());
                        recalculateCrp = false;
                    }
                    recalculate = false;
                }

                // log.debug("RPPERRES: " + rpPerResearch + " CRPPERRES: " + rpPerResearchComp);

                if (rpPerResearch > totalRP) {
                    rpPerResearch = totalRP;
                }
                if (rpPerResearchComp > totalRPComp) {
                    rpPerResearchComp = totalRPComp;
                }

                Research r = rDAO.findById(actRp.getResearchId());

                double bonusRatio = 1d + r.getBonus();

                //If ResearchProgress needs less points than points Per Research
                //DebugBuffer.addLine(DebugLevel.UNKNOWN, "Research " + rp.getResearchId() + " RPLEFT="+rp.getRpLeft() +
                //        " CRPLEFT="+rp.getRpLeftComputer());
                //DebugBuffer.addLine(DebugLevel.UNKNOWN, "RP to subtract="+rpToSubtract +
                //        " CRP to subract="+crpToSubtract);

                boolean rpFinished = false;
                boolean crpFinished = false;

                if (actRp.getRpLeft() <= ((int) (rpPerResearch * bonusRatio)) && actRp.getRpLeft() > 0) {
                    totalRP -= (int) (actRp.getRpLeft() / bonusRatio);
                    actRp.setRpLeft(0);
                    recalculate = true;
                    recalculateRp = true;
                }
                if (actRp.getRpLeftComputer() <= ((int) (rpPerResearchComp * bonusRatio)) && actRp.getRpLeftComputer() > 0) {
                    totalRPComp -= (int) (actRp.getRpLeftComputer() / bonusRatio);
                    actRp.setRpLeftComputer(0);
                    recalculate = true;
                    recalculateCrp = true;
                }

                if (actRp.getRpLeft() > 0 && rpPerResearch > 0) {
                    actRp.setRpLeft(actRp.getRpLeft() - (int) (rpPerResearch * bonusRatio));
                    totalRP -= rpPerResearch;
                } else {
                    if (actRp.getRpLeft() <= 0) {
                        rpFinished = true;
                        needsRP.remove(actRp);
                    }
                }

                if (rpPerResearchComp > 0 && actRp.getRpLeftComputer() > 0) {
                    actRp.setRpLeftComputer(actRp.getRpLeftComputer() - (int) (rpPerResearchComp * bonusRatio));
                    totalRPComp -= rpPerResearchComp;
                } else {
                    if (actRp.getRpLeftComputer() <= 0) {
                        crpFinished = true;
                        needsCRP.remove(actRp);
                    }
                }

                //finished
                if (crpFinished && rpFinished) {
                    // log.debug("Remove " + r.getName());

                    removeRP.add(actRp);
                    result.put(actRp.getResearchId(), time);
                }

                // log.debug("RPLEFT: " + actRp.getRpLeft() + " CRPLEFT: " + actRp.getRpLeftComputer() + " FOR " + r.getName());
            }

            rpList.removeAll(removeRP);
            if (rpList.isEmpty()) {
                finished = true;
            }

            if (time > timeLimit) {
                for (ResearchProgress rpTmp : rpList) {
                    result.put(rpTmp.getResearchId(), null);
                }
                finished = true;
            }
        }

        return result;
    }

    public static ResearchResult getResearchPointsGlobal(int userId) {

        return ResearchUtilities.getResearchPointsGlobal(userId);
    }

    public static ArrayList<ResearchProgressView> getResearchListView(int userId) {
        return getResearchListView(userId, 0, null);

    }

    public static TechListView getTechListView(int userId) {
        TechListView tlv = new TechListView(userId);

        for (TechRelation tr : trDAO.findAll()) {
            TechType resType = Service.techTypeDAO.findById(tr.getTreeType());
            if (resType.getId() == TechRelation.TREETYPE_RESEARCH) {
                continue;
            }
            if (tr.getReqResearchId() == Research.LOCK_TECH) {
                continue;
            }
            if (tr.getReqResearchId() != 0 && prDAO.findBy(userId, tr.getReqResearchId()) != null) {
                continue;
            }
            if (tr.getTreeType() == TechRelation.TREETYPE_BUILD && tr.getReqConstructionId() != 0) {
                continue;
            }
            if (resType.getRefModel() != null && resType.getRefDAO() != null) {
                if (!resType.getRefDAO().equals("") && !resType.getRefModel().equals("")) {
                    String name = "unbekannt";
                    ETechListType tlt = ETechListType.RESEARCH_TYPE_UNKNOWN;
                    try {
                        Class daoClazz = Class.forName(resType.getRefDAO());
                        Class modelClazz = Class.forName(resType.getRefModel());
                        Constructor constructor = modelClazz.getDeclaredConstructor();
                        constructor.setAccessible(true);

                        //    ITechFunctions m = (ITechFunctions)constructor.newInstance();


                        ReadOnlyTable dao = (ReadOnlyTable) DAOFactory.get(daoClazz);
                        ITechFunctions tech = null;

                        for (ITechFunctions m : (ArrayList<ITechFunctions>) dao.findAll()) {
                            if (m.getTechId() == tr.getSourceId()) {
                                tech = m;
                                break;
                            }
                        }
                        tlt = convertTreeType(tr, tech);
                        if (tech != null) {
                            name = ML.getMLStr(tech.getTechName(), userId);
                        }

                    } catch (Exception e) {
                        log.debug("Throw new : " + e);
                    }
                    TechListViewEntry tlve = new TechListViewEntry(tr, name, tlt);
                    tlv.addTechListViewEntry(tlve);
                }
            }
        }
        return tlv;

    }

    public static int convertTechListType(ETechListType techListType) {
        int type = 1;

        if (techListType.equals(ETechListType.RESEARCH_TYPE_CONSTRUCTION)) {
            type = TechRelation.TREETYPE_BUILD;
        } else if (techListType.equals(ETechListType.RESEARCH_TYPE_GROUNDTROOPS)) {
            type = TechRelation.TREETYPE_GROUNDTROOPS;
        } else {
            type = TechRelation.TREETYPE_MODULE;
        }

        return type;
    }

    public static ETechListType convertTreeType(TechRelation tr, ITechFunctions tech) {
        ETechListType tlt = ETechListType.RESEARCH_TYPE_MODULE_VARIOUS;
        if (tr.getTreeType() == TechRelation.TREETYPE_BUILD) {
            tlt = ETechListType.RESEARCH_TYPE_CONSTRUCTION;
        } else if (tr.getTreeType() == TechRelation.TREETYPE_GROUNDTROOPS) {
            tlt = ETechListType.RESEARCH_TYPE_GROUNDTROOPS;
        } else if (tr.getTreeType() == TechRelation.TREETYPE_MODULE) {
            Module module = (Module) tech;
            tlt = ETechListType.RESEARCH_TYPE_MODULE_VARIOUS;
            if (module.getType().equals(ModuleType.ARMOR)) {
                tlt = ETechListType.RESEARCH_TYPE_MODULE_ARMOR;
            } else if (module.getType().equals(ModuleType.SHIELD)) {
                tlt = ETechListType.RESEARCH_TYPE_MODULE_SHIELD;
            } else if (module.getType().equals(ModuleType.REACTOR)) {
                tlt = ETechListType.RESEARCH_TYPE_MODULE_REACTOR;
            } else if (module.getType().equals(ModuleType.WEAPON)) {
                tlt = ETechListType.RESEARCH_TYPE_MODULE_WEAPON;
            } else if (module.getType().equals(ModuleType.CHASSIS)) {
                tlt = ETechListType.RESEARCH_TYPE_CHASSIS;
            } else if (module.getType().equals(ModuleType.ENGINE)) {
                tlt = ETechListType.RESEARCH_TYPE_MODULE_ENGINE;
            } else if (module.getType().equals(ModuleType.COLONISATION) || module.getType().equals(ModuleType.TRANSPORT_POPULATION)
                    || module.getType().equals(ModuleType.TRANSPORT_RESSOURCE) || module.getType().equals(ModuleType.HANGAR)
                    || module.getType().equals(ModuleType.SPECIAL)) {
                tlt = ETechListType.RESEARCH_TYPE_MODULE_SPECIAL;
            }
        }
        return tlt;
    }

    public static ArrayList<ResearchProgressView> getResearchListView(int userId, int techRelationId, ETechListType type) {
        ArrayList<ResearchProgressView> rpvList = new ArrayList<ResearchProgressView>();
        ArrayList<Research> rList = new ArrayList<Research>();
        ArrayList<PlayerResearch> prList = prDAO.findByUserId(userId);
        ArrayList<ResearchProgress> rpList = rpDAO.findByUserId(userId);

        HashMap<Integer, PlayerResearch> alreadyResearched = new HashMap<Integer, PlayerResearch>();
        HashMap<Integer, ResearchProgress> inResearch = new HashMap<Integer, ResearchProgress>();

        for (PlayerResearch pr : prList) {
            alreadyResearched.put(pr.getResearchId(), pr);
        }

        for (ResearchProgress rp : rpList) {
            inResearch.put(rp.getResearchId(), rp);
        }
        if (techRelationId == 0) {
            rList = rDAO.findAll();
            for (Research r : rList) {
                if (alreadyResearched.containsKey(r.getId())) {
                    rpvList.add(new ResearchProgressView(r, ResearchService.RESEARCH_VIEW_RESEARCHED, 0, 0));
                } else if (inResearch.containsKey(r.getId())) {
                    int status = ResearchService.RESEARCH_VIEW_INRESEARCH;
                    Action action = aDAO.findResearchBy(userId, r.getId());
                    if (action != null) {
                        if (action.getOnHold() != null) {
                            if (action.getOnHold()) {
                                status = ResearchService.RESEARCH_VIEW_ON_HOLD;
                            }
                        }
                    }
                    rpvList.add(new ResearchProgressView(r, status,
                            r.getRp() - inResearch.get(r.getId()).getRpLeft(),
                            r.getRpComputer() - inResearch.get(r.getId()).getRpLeftComputer()));
                } else {

                    ArrayList<TechRelation> trList = trDAO.findByResearchId(r.getId());

                    boolean possible = true;

                    for (TechRelation tr : trList) {
                        if (tr.getReqResearchId() != 0) {
                            if (prDAO.findBy(userId, tr.getReqResearchId()) == null) {
                                possible = false;
                            }
                        }
                    }

                    if (possible) {
                        rpvList.add(new ResearchProgressView(r, ResearchService.RESEARCH_VIEW_NOT_RESEARCHED,
                                0,
                                0));
                    }
                }
            }
        } else {
            HashMap<Integer, Research> ress = new HashMap<Integer, Research>();
            Queue<Research> queue = new LinkedList<Research>();
            HashMap<Integer, Research> sRes = new HashMap<Integer, Research>();
           // int typeInt = convertTechListType(type);
            for (TechRelation tr : trDAO.findBy(techRelationId, convertTechListType(type))) {

                int researchId = tr.getReqResearchId();
                if (researchId == Research.LOCK_TECH || researchId == 0) {
                    continue;
                }
                Research res = rDAO.findById(researchId);
                if (tr.getReqResearchId() != 0) {
                    queue.add(res);
                        sRes.put(res.getId(), res);
                }
            }
            while (!queue.isEmpty()) {
                Research r = queue.poll();
                for (TechRelation tr : trDAO.findByResearchId(r.getId())) {
                    if (tr.getTreeType() == 1) {

                        Research tmp = ress.get(tr.getReqResearchId());
                        if (tmp == null) {
                            tmp = rDAO.findById(tr.getReqResearchId());
                            ress.put(tr.getReqResearchId(), tmp);
                        }
                        if (!queue.contains(tmp)) {
                            queue.add(tmp);
                        }
                        sRes.put(tmp.getId(), tmp);

                    }
                }
            }
            for (Map.Entry<Integer, Research> entry : sRes.entrySet()) {
                Research r = entry.getValue();
                int status = ResearchService.RESEARCH_VIEW_NOT_RESEARCHABLE;
                if (!alreadyResearched.containsKey(r.getId())) {
                    if (inResearch.containsKey(r.getId())) {
                        status = ResearchService.RESEARCH_VIEW_INRESEARCH;
                        Action action = aDAO.findResearchBy(userId, r.getId());
                        if (action != null) {
                            if (action.getOnHold() != null) {
                                if (action.getOnHold()) {
                                    status = ResearchService.RESEARCH_VIEW_ON_HOLD;
                                }
                            }
                        }
                        rpvList.add(new ResearchProgressView(r, status,
                                r.getRp() - inResearch.get(r.getId()).getRpLeft(),
                                r.getRpComputer() - inResearch.get(r.getId()).getRpLeftComputer()));
                    } else {
                        ArrayList<TechRelation> trList = trDAO.findByResearchId(r.getId());

                        boolean possible = true;

                        for (TechRelation tr : trList) {
                            if (tr.getReqResearchId() != 0) {
                                if (prDAO.findBy(userId, tr.getReqResearchId()) == null) {
                                    possible = false;
                                }
                            }
                        }

                        if (possible && !alreadyResearched.containsKey(r.getId())) {
                            rpvList.add(new ResearchProgressView(r, ResearchService.RESEARCH_VIEW_NOT_RESEARCHED,
                                    0,
                                    0));
                        } else {
                            rpvList.add(new ResearchProgressView(r, ResearchService.RESEARCH_VIEW_NOT_RESEARCHABLE,
                                    0,
                                    0));
                        }
                    }
                }
            }
        }



        return rpvList;
    }

    public static class res {

        public int totalUsers;
        public int totalUsersResearched;

        public res() {
            totalUsers = 0;
            totalUsersResearched = 0;
        }
    }

    public static void reEvaluateResearchBonus() {
        for (Research r : rDAO.findAll()) {
            // log.debug("Reevaluating " + r.getName());
            calculateResearchBonus(r.getId());
        }
    }
    
    public static double calculateResearchBonus(int researchId) {
        Research r = rDAO.findById(researchId);
        
        double bonus = 0;
        
        // Get number of active Players
        int activePlayers = GameUtilities.getActivePlayerCountForResearch();
        
        // Get number of times this technology was researched
        int researched = ResearchUtilities.noOfFinishedResearches(researchId);
        double baseBonus = Math.min(0.5d, researched / 100d);
        bonus += baseBonus;                
        
        // Calculate Bonus
        double researchRatio = 1d / (double)activePlayers * (double)researched;
        // log.debug("["+r.getName()+"] UsersTotal: " + activePlayers + " Researched: " + researched + " Ratio: "+researchRatio+" BaseBonus: " + bonus);
        
        if (researchRatio > 0.9d) {
            bonus += 0.8d;
        } else if (researchRatio > 0.8d) {
            bonus += 0.4d;
        } else if (researchRatio > 0.7d) {
            bonus += 0.2d;
        } else if (researchRatio > 0.6d) {
            bonus += 0.1d;
        } else if (researchRatio > 0.5d) {
            bonus += 0.05d;
        }

        r.setBonus(bonus);
        rDAO.update(r);
        
        return bonus;
    }
    
    public static void updateResearchBoost() {

        // next number the bonus shall be rounded
        final int roundTo = 5;
        // Number of times the total player account shall be divided, separated and weightned
        final int interval = 10;
        /**
         * Weightning:
         * if the last 10 Percent of players have alreadye
         * researched the research its x-times as important
         * as if the first 10 Percent researched it;
         *
         */
        HashMap<Integer, Double> multipliers = new HashMap<Integer, Double>();
        multipliers.put(1, 1d);
        multipliers.put(2, 1.5d);
        multipliers.put(3, 2d);
        multipliers.put(4, 2.5d);
        multipliers.put(5, 3d);
        multipliers.put(6, 3.5d);
        multipliers.put(7, 4d);
        multipliers.put(8, 4.5d);
        multipliers.put(9, 5d);
        multipliers.put(10, 5.5d);

        //Maximum Points able
        int maxPoints = 0;

        for (Map.Entry<Integer, Double> entry : multipliers.entrySet()) {
            maxPoints += (int) (entry.getValue() * interval);
        }
        // Lower Limit of Points when bonus shall start  limit: 0 => Always Bonus
        final int lowerLimit = 100;
        // Maximum Bonus for a research: maxBonus = 1.d => twice as fast as normal
        final double maxBonus = 0.5;

        ArrayList<Research> researches = rDAO.findAll();
        ArrayList<Statistic> statistics = sDAO.findAll();

        TreeMap<Integer, Statistic> statOrdered = new TreeMap<Integer, Statistic>();
        for (Statistic s : statistics) {
            statOrdered.put(s.getPoints(), s);
        }
        statistics = new ArrayList<Statistic>();
        statistics.addAll(statOrdered.descendingMap().values());


        int totalUsers = uDAO.findAll().size();

        // Debug variable
        int totalPlayersResearched = 0;

        int separator = Math.round(totalUsers / interval);

        for (Research r : researches) {

            double bonus = 0;
            if (r.getBonus() == 0 || r.getBonus() == null) {
                bonus = 0d;
            }
            int researchedByPlayers = 0;
            int multiplier = 1;
            for (int i = 1; i <= statistics.size(); i++) {
                Statistic s = statistics.get(i - 1);

                if (prDAO.findBy(s.getUserId(), r.getId()) != null) {
                    totalPlayersResearched++;
                    researchedByPlayers++;
                }
                if (i % separator == 0 && i > 0) {
                    double multi = multipliers.get(multiplier);
                    float percent = (float) researchedByPlayers / separator;
                    float normalized = (float) (percent * interval);
                    double tmpBonus = (double) (multi * normalized);
                    bonus += tmpBonus;
                    multiplier++;
                    researchedByPlayers = 0;
                }
            }
            //Umrechnen von Punkten in %
            if (bonus > lowerLimit) {
                double percentage = (double) ((bonus - lowerLimit) / (maxPoints - lowerLimit));
                bonus = (double) (percentage * maxBonus);
                bonus = Math.round(bonus * 100.0 / roundTo) / 100.0;
                bonus *= (double) roundTo;
            } else {
                bonus = 0;
            }
            r.setBonus(bonus);
            rDAO.update(r);
            multiplier = 1;

        }
    }

    public static String buildRequiredForResearchString(ArrayList<Research> researches, ArrayList<Construction> constructions, int userId) {
        String result = "";
        if (researches.size() > 0 || constructions.size() > 0) {
            result += ML.getMLStr("research_pop_requires", userId) + ":<BR>";
            for (int i = 0; i < researches.size(); i++) {
                String research = ML.getMLStr(researches.get(i).getName(), userId);
                if (prDAO.findBy(userId, researches.get(i).getId()) != null) {
                    research = "<strike>" + ML.getMLStr(researches.get(i).getName(), userId) + "</strike>";
                }
                if (i == 0) {
                    result += ML.getMLStr("research_pop_research", userId) + ":";
                    result += research;
                } else if (i % 3 == 0) {
                    result += "," + research + "<BR>";
                } else {
                    result += "," + research;
                }
            }

            for (int i = 0; i < constructions.size(); i++) {

                if (i == 0) {
                    if (researches.size() > 0) {
                        result += "<BR>";
                    }
                    result += ML.getMLStr("research_pop_construction", userId) + ":";
                    result += ML.getMLStr(constructions.get(i).getName(), userId);
                } else if (i % 3 == 0) {
                    result += "," + ML.getMLStr(constructions.get(i).getName(), userId) + "<BR>";
                } else {
                    result += "," + ML.getMLStr(constructions.get(i).getName(), userId);
                }
            }

        }

        return result;
    }

    public static String buildEnablesForResearchString(ArrayList<Research> researches, ArrayList<Construction> constructions, ArrayList<GroundTroop> groundtroops, int userId) {

        String result = "";
        if (researches.size() > 0 || constructions.size() > 0 || groundtroops.size() > 0) {
            result += ML.getMLStr("research_pop_enables", userId) + ":<BR>";
            for (int i = 0; i < researches.size(); i++) {
                if (researches.get(i).getId() == Research.LOCK_TECH) {
                    continue;
                }
                if (i == 0) {
                    result += ML.getMLStr("research_pop_research", userId) + ":";
                    result += ML.getMLStr(researches.get(i).getName(), userId);
                } else if (i % 3 == 0) {
                    result += "," + ML.getMLStr(researches.get(i).getName(), userId) + "<BR>";
                } else {
                    result += "," + ML.getMLStr(researches.get(i).getName(), userId);
                }
            }
            for (int i = 0; i < constructions.size(); i++) {

                String construction = ML.getMLStr(constructions.get(i).getName(), userId);
                if (i == 0) {
                    if (researches.size() > 0) {
                        result += "<BR>";
                    }
                    result += ML.getMLStr("research_pop_construction", userId) + ":";
                    result += construction + "";
                } else if (i % 3 == 0) {
                    result += "," + construction + "<BR>";
                } else {
                    result += "," + construction;
                }
            }

            for (int i = 0; i < groundtroops.size(); i++) {
                String Groundtroop = ML.getMLStr(groundtroops.get(i).getName(), userId);
                if (i == 0) {
                    if (researches.size() > 0 || constructions.size() > 0) {
                        result += "<BR>";
                    }
                    result += ML.getMLStr("research_pop_groundtroop", userId) + ":";
                    result += Groundtroop + "";
                } else if (i % 3 == 0) {
                    result += "," + Groundtroop + "<BR>";
                } else {
                    result += "," + Groundtroop;
                }
            }

        }

        return result;
    }

    public static ArrayList<Research> getRequiredResearchesForResearch(int researchId) {
        ArrayList<TechRelation> trs = trDAO.findByResearchId(researchId);
        ArrayList<Research> requiredResearches = new ArrayList<Research>();
        for (TechRelation tr : trs) {
            if (tr.getReqResearchId() > 0) {
                if (tr.getSourceId() == Research.LOCK_TECH) {
                    continue;
                }
                Research r = rDAO.findById(tr.getReqResearchId());
                if (r != null) {
                    requiredResearches.add(r);
                }
            }
        }
        return requiredResearches;
    }

    public static ArrayList<Research> getAllRequiredResearchesForResearch(int researchId) {
        ArrayList<TechRelation> trs = trDAO.findByResearchId(researchId);
        ArrayList<Research> requiredResearches = new ArrayList<Research>();
        for (TechRelation tr : trs) {
            if (tr.getReqResearchId() > 0) {
                if (tr.getSourceId() == Research.LOCK_TECH) {
                    continue;
                }
                Research r = rDAO.findById(tr.getReqResearchId());
                if (r != null) {
                    requiredResearches.add(r);
                    getRequiredResearchesRecursive(requiredResearches,r.getId());
                }
            }
        }
        return requiredResearches;
    }

    private static void getRequiredResearchesRecursive(ArrayList<Research> rList, int researchId) {
        ArrayList<TechRelation> trs = trDAO.findByResearchId(researchId);

        for (TechRelation tr : trs) {
            if (tr.getReqResearchId() > 0) {
                if (tr.getSourceId() == Research.LOCK_TECH) {
                    continue;
                }
                Research r = rDAO.findById(tr.getReqResearchId());
                if (r != null) {
                    rList.add(r);
                    getRequiredResearchesRecursive(rList,r.getId());
                }
            }
        }
    }

    public static ArrayList<Construction> getRequiredConstructionsForResearch(int researchId) {

        ArrayList<TechRelation> trs = trDAO.findByResearchId(researchId);
        ArrayList<Construction> requiredConstruction = new ArrayList<Construction>();
        for (TechRelation tr : trs) {
            if (tr.getReqConstructionId() > 0) {
                requiredConstruction.add(cDAO.findById(tr.getReqConstructionId()));
            }
        }
        return requiredConstruction;
    }

    public static ArrayList<Research> getEnabledResearchesForResearch(int researchId) {
        ArrayList<TechRelation> trs = trDAO.findByRequiredResearchId(researchId);
        ArrayList<Research> enabledResearches = new ArrayList<Research>();
        for (TechRelation tr : trs) {
            if (tr.getTreeType() == TechRelation.TREETYPE_RESEARCH) {
                if (tr.getSourceId() == Research.LOCK_TECH) {
                    continue;
                }
                Research r = rDAO.findById(tr.getSourceId());
                if (r != null) {
                    enabledResearches.add(r);
                }
            }
        }
        return enabledResearches;

    }

    public static ArrayList<Construction> getEnabledConstructionsForResearch(int researchId) {

        ArrayList<TechRelation> trs = trDAO.findByRequiredResearchId(researchId);
        ArrayList<Construction> enabledConstructions = new ArrayList<Construction>();
        for (TechRelation tr : trs) {
            if (tr.getTreeType() == TechRelation.TREETYPE_BUILD) {
                enabledConstructions.add(cDAO.findById(tr.getSourceId()));
            }
        }
        return enabledConstructions;
    }

    public static ArrayList<GroundTroop> getEnabledGroundtroopsForResearch(int researchId) {

        ArrayList<TechRelation> trs = trDAO.findByRequiredResearchId(researchId);
        ArrayList<GroundTroop> enabledGroundtroops = new ArrayList<GroundTroop>();
        for (TechRelation tr : trs) {
            if (tr.getTreeType() == TechRelation.TREETYPE_GROUNDTROOPS) {
                //   log.debug("Enabling Groundtroop: " + tr.getSourceId());
                enabledGroundtroops.add(gtDAO.findById(tr.getSourceId()));
            }
        }
        return enabledGroundtroops;
    }

    public static boolean isPossibleToResearch(int planetId, int researchId, int userId) {        
        /* false = nicht erforschbar
         * true = erforschbar */
        boolean ableToResearch = true;
        // ResultSet rs = stmt.executeQuery("select reqConstructionID, reqResearchID from techrelation where sourceID="+researchId+" and treeType=1");
        // while (rs.next()) {
        
        if (researchId == LOCK_TECH) {
            return false;
        }
        
        for (TechRelation tr : trDAO.findByResearchId(researchId)) {
            // DebugBuffer.addLine("Research " + researchId + " gefunden!");
            // Suche benötigtes Gebäude
            // DebugBuffer.addLine("Suche benötigtes Gebäude " + rs.getInt(1) + "!");
            if (tr.getReqConstructionId() > 0) {
                PlanetConstruction construction = pcDAO.findBy(planetId, tr.getReqConstructionId());
                if (construction == null) {
                    DebugBuffer.addLine(DebugLevel.UNKNOWN, "Research " + researchId + " not possible to research due missing construction [" + tr.getReqConstructionId() + "] (user: " + userId + ")");
                    ableToResearch = false;
                    break;
                }
            } else if (tr.getReqResearchId() > 0) {
                PlayerResearch pr = prDAO.findBy(userId, tr.getReqResearchId());
                if (pr == null) {
                    DebugBuffer.addLine(DebugLevel.UNKNOWN, "Research " + researchId + " not possible to research due missing research [" + tr.getReqResearchId() + "] (user: " + userId + ")");
                    ableToResearch = false;
                    break;
                }
            }
        }
        return ableToResearch;
    }

    public static ArrayList<ResearchProgress> getResearchInProgress(int userId) {
        ArrayList<ResearchProgress> rpList = rpDAO.findByUserId(userId);
        return rpList;
    }

    public static void processAction(int userId, int researchId, int planetId, int procAction) {
        if (isPossibleToResearch(planetId, researchId, userId)) {
            Research research = ResearchService.findResearchById(researchId);
            if (research != null) {
                Action action = aDAO.findResearchBy(userId, researchId);
                //Research already in actionTable
                if (action != null) {
                    if (action.getOnHold() == null) {
                        action.setOnHold(true);
                    } else {
                        if (action.getOnHold() && (procAction == PROCESS_RESUME)) {
                            action.setOnHold(false);
                        } else if (!action.getOnHold() && (procAction == PROCESS_PAUSE)) {
                            action.setOnHold(true);
                        }
                    }
                    aDAO.update(action);
                } else if (procAction == PROCESS_START) {
                    TransactionHandler th = TransactionHandler.getTransactionHandler();

                    try {
                        th.startTransaction();
                        ResearchProgress rp = new ResearchProgress();
                        rp.setUserId(userId);
                        rp.setResearchId(researchId);
                        rp.setRpLeft(research.getRp());
                        rp.setRpLeftComputer(research.getRpComputer());
                        action = new Action();
                        rp = rpDAO.add(rp);
                        action.setUserId(userId);
                        action.setType(EActionType.RESEARCH);
                        action.setResearchId(researchId);
                        action.setOnHold(false);
                        action.setRefTableId(rp.getId());
                        aDAO.add(action);
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            th.rollback();
                        } catch (TransactionException te) {
                            te.printStackTrace();
                        }
                    } finally {
                        th.endTransaction();
                    }
                }
            } else {
                DebugBuffer.error("Research " + researchId + " was not found in research table");
            }
        } else {
            DebugBuffer.error("Research " + researchId + " is not possible to research for user " + userId);
        }
    }

    public static boolean isPossibleByTech(int treeType, int id, int userId, int planetId) {
        ArrayList<TechRelation> neededTech = null;

        if (treeType == TechRelation.TREETYPE_RESEARCH) {
            neededTech = trDAO.findByResearchId(id);
        } else if (treeType == TechRelation.TREETYPE_MODULE) {
            neededTech = trDAO.findByModuleId(id);
        } else if (treeType == TechRelation.TREETYPE_BUILD) {
            neededTech = trDAO.findByConstructionId(id);
        }

        boolean possible = true;

        if (neededTech.size() > 0) {
            for (TechRelation tr : neededTech) {
                if (tr.getReqResearchId() != 0) {
                    if (Service.playerResearchDAO.findBy(userId, tr.getReqResearchId()) == null) {
                        possible = false;
                    }
                }
                if (tr.getReqConstructionId() != 0) {
                    if (Service.planetConstructionDAO.findBy(planetId, tr.getReqConstructionId()) == null) {
                        possible = false;
                    }
                }
            }
        }

        return possible;
    }

    public static boolean showInfo(int userId, int planetId) {
        if (Service.planetConstructionDAO.findBy(planetId, Construction.ID_RESEARCHLABORATORY) == null) {
            PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(planetId);
            if (pp.isHomeSystem()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean techTreeExternal(int userId) {
        return Service.userSettingsDAO.findByUserId(userId).getTtvExternalBrowser();
    }
}
