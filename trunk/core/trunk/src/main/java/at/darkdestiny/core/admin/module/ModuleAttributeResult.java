/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin.module;

import at.darkdestiny.core.dao.AttributeDAO;
import at.darkdestiny.core.model.Attribute;
import at.darkdestiny.core.model.ModuleAttribute;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ModuleAttributeResult {
    private ArrayList<ModuleAttributeResEntry> result = new ArrayList<ModuleAttributeResEntry>();

    private static AttributeDAO aDAO = (AttributeDAO)DAOFactory.get(AttributeDAO.class);

    protected ModuleAttributeResult(ArrayList<ModuleAttributeResEntry> result) {
        this.result = result;
    }

    public ArrayList<ModuleAttribute> getAllSingleEntries() {
        ArrayList<ModuleAttribute> maList = new ArrayList<ModuleAttribute>();

        for (ModuleAttributeResEntry mare : result) {
            if (mare instanceof ModuleAttributeResEntryReferenced) continue;
            maList.add(mare.ma);
        }

        return maList;
    }

    public double getAttributeValue(int attributeId) {
        for (ModuleAttributeResEntry mare : result) {
            if (mare instanceof ModuleAttributeResEntryReferenced) continue;
            if (mare.ma.getAttributeId() == attributeId) return mare.ma.getValue();
        }

        return 0d;
    }

    public double getAttributeValue(int attributeId, int refId, ReferenceType rt) {
        for (ModuleAttributeResEntry mare : result) {
            if (mare instanceof ModuleAttributeResEntrySingle) continue;
            if ((mare.ma.getAttributeId() == attributeId) &&
                (mare.ma.getRefId() == refId) &&
                (mare.ma.getRefType() == rt)) {
                return mare.ma.getValue();
            }
        }

        return 0d;
    }

    public double getAttributeValue(at.darkdestiny.core.enumeration.EAttribute aIn) {
        try {
            for (ModuleAttributeResEntry mare : result) {
                if (mare instanceof ModuleAttributeResEntryReferenced) continue;
                int aId = mare.ma.getAttributeId();
                double value = mare.ma.getValue();

                Attribute a = aDAO.findById(aId);
                if (a.getName().equalsIgnoreCase(aIn.toString())) {
                    return value;
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getAttributeValue: ", e);
        }

        return 0d;
    }

    public double getAttributeValue(at.darkdestiny.core.enumeration.EAttribute aIn, int refId, ReferenceType rt) {
        try {
            for (ModuleAttributeResEntry mare : result) {
                if (mare instanceof ModuleAttributeResEntrySingle) continue;
                int aId = mare.ma.getAttributeId();
                double value = mare.ma.getValue();

                Attribute a = aDAO.findById(aId);
                if ((a.getName().equalsIgnoreCase(aIn.toString())) &&
                    (mare.ma.getRefId() == refId) &&
                    (mare.ma.getRefType() == rt)) {
                    return value;
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getAttributeValue: ", e);
        }

        return 0d;
    }
}
