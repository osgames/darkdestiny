/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ressources;

import at.darkdestiny.core.interfaces.IRessourceCost;

/**
 *
 * @author Stefan
 */
public interface MutableRessourceCost extends IRessourceCost {   
    public void setRess(int ressId, long qty);
}
