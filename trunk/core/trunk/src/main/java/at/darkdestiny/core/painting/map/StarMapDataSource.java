/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.util.DebugBuffer;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

/**
 *
 * @author Stefan
 */
public class StarMapDataSource implements IMapDataSource {
    private final StringBuffer xmlString;
    
    private ArrayList<SMSystem> systems = new ArrayList<SMSystem>();
    private ArrayList<SMFleet> fleets = new ArrayList<SMFleet>();
    private ArrayList<SMObservatory> observatories = new ArrayList<SMObservatory>();
    private ArrayList<SMHyperscanner> hyperscanners = new ArrayList<SMHyperscanner>();
    
    public StarMapDataSource(StringBuffer xmlString) {
        this.xmlString = xmlString;       
    }
    
    @Override
    public void parse() {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(false);        
            SAXParser saxParser = factory.newSAXParser();
            
            XMLReader xmlReader = saxParser.getXMLReader();
            xmlReader.setContentHandler(new StarmapXMLParser(this));
            Reader reader = new StringReader(FormatUtilities.toISO2(xmlString.toString()));
            InputSource is = new InputSource(reader);
            xmlReader.parse(is);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in StarMapDataSource", e);
        }        
    }

    public void addStarSystem(SMSystem sms) {
        systems.add(sms);
    }    
    
    public ArrayList<SMSystem> getStarSystems() {
        return systems;
    }
    
    public void addObservatory(SMObservatory smobs) {
        observatories.add(smobs);
    }    
    
    public ArrayList<SMObservatory> getObservatories() {
        return observatories;
    }    
    
    public void addHyperscanner(SMHyperscanner smscan) {
        hyperscanners.add(smscan);
    }    
    
    public ArrayList<SMHyperscanner> getHyperscanners() {
        return hyperscanners;
    }        

    public void addFleets(SMFleet smf) {
        fleets.add(smf);
    }    
    
    public ArrayList<SMFleet> getFleets() {
        return fleets;
    }

    public void getTerritories() {
        
    }
}
