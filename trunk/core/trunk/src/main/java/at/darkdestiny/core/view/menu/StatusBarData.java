 
package at.darkdestiny.core.view.menu;

import at.darkdestiny.core.model.Note;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;

/**
 *
 * Datenklasse
 * 
 * Beinhaltet alle Informationen zum Aufbau der StatusBar
 * 
 * @author Dreloc
 */
public class StatusBarData extends Service{

    //Credits
    private CreditsEntity credits;
    //MessagePanel (Anzeige ob Nachrichten eingetroffen sind)
    private MessageEntity messages;
    //Bev&ouml;lkerung sowie Nahrungsanzahl und Verbrauch
    private PopulationFoodEntity populationFood;
    //Energieanzeige und Verbreich
    private EnergyEntity energy;
    private Note note;
    private DevelopementEntity developement;
    //Ressourcen: Eisen, Stahl....
    private ArrayList<RessourceEntity> ressources = new ArrayList<RessourceEntity>();
    private ArrayList<RessourceEntity> rawMaterials = new ArrayList<RessourceEntity>();
    
    private int planetId;

    public StatusBarData(){
        credits = new CreditsEntity();
        developement = new DevelopementEntity();
        messages = new MessageEntity();
        populationFood = new PopulationFoodEntity(ressourceDAO.findRessourceById(Ressource.FOOD));
        energy = new EnergyEntity(ressourceDAO.findRessourceById(Ressource.ENERGY));
        note = new Note();
        
    }
            
    public CreditsEntity getCredits() {
        return credits;
    }

    public void setCredits(CreditsEntity credits) {
        this.credits = credits;
    }

    public MessageEntity getMessages() {
        return messages;
    }

    public void setMessages(MessageEntity messages) {
        this.messages = messages;
    }

    public PopulationFoodEntity getPopulationFood() {
        return populationFood;
    }

    public void setPopulationFood(PopulationFoodEntity populationFood) {
        this.populationFood = populationFood;
    }

    public EnergyEntity getEnergy() {
        return energy;
    }

    public void setEnergy(EnergyEntity energy) {
        this.energy = energy;
    }

    public ArrayList<RessourceEntity> getRessources() {
        return ressources;
    }

    public void setRessources(ArrayList<RessourceEntity> ressources) {
        this.ressources = ressources;
    }
    public void addRessource(RessourceEntity ressource){
        this.ressources.add(ressource);
    }

    public int getPlanetId() {
        return planetId;
    }

    public void setPlanetId(int planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the rawMaterials
     */
    public ArrayList<RessourceEntity> getRawMaterials() {
        return rawMaterials;
    }

    /**
     * @param rawMaterials the rawMaterials to set
     */
    public void setRawMaterials(ArrayList<RessourceEntity> rawMaterials) {
        this.rawMaterials = rawMaterials;
    }

    public void addRawMaterial(RessourceEntity ressourceEntity) {
        rawMaterials.add(ressourceEntity);
    }

    /**
     * @return the developement
     */
    public DevelopementEntity getDevelopement() {
        return developement;
    }

    /**
     * @param developement the developement to set
     */
    public void setDevelopement(DevelopementEntity developement) {
        this.developement = developement;
    }

    /**
     * @return the note
     */
    public Note getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(Note note) {
        this.note = note;
    }

}
