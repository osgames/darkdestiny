/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view.header;

import java.util.HashMap;

/**
 *
 * @author Horst
 */
public class SystemEntity {

    private int systemId;
    private String systemName;
    private int firstPlanetId;
    private int previousSystemId;
    private int nextSystemId;
    private HashMap<Integer, PlanetEntity> planets;

    public SystemEntity() {
        systemId = -1;
        systemName = "";
        this.previousSystemId = -1;
        this.nextSystemId = -1;
        this.firstPlanetId = -1;
        planets = new HashMap<Integer, PlanetEntity>();
    }

    public int getSystemId() {
        return systemId;
    }

    public void setSystemId(int systemId) {
        this.systemId = systemId;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public HashMap<Integer, PlanetEntity> getPlanets() {
        return planets;
    }

    public void setPlanets(HashMap<Integer, PlanetEntity> planets) {
        this.planets = planets;
    }

    public int getPreviousSystemId() {
        return previousSystemId;
    }

    public void setPreviousSystemId(int previousSystemId) {
        this.previousSystemId = previousSystemId;
    }

    public int getNextSystemId() {
        return nextSystemId;
    }

    public void setNextSystemId(int nextSystemId) {
        this.nextSystemId = nextSystemId;
    }

    public int getFirstPlanetId() {
        return firstPlanetId;
    }

    public void setFirstPlanetId(int firstPlanetId) {
        this.firstPlanetId = firstPlanetId;
    }
}
