/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.UserRelation;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class UserRelationDAO extends ReadWriteTable<UserRelation> implements GenericDAO {
    public ArrayList<UserRelation> findByUserId(int userId){
        UserRelation ur = new UserRelation();
        ur.setUserId1(userId);
        ArrayList<UserRelation> result = find(ur);

        ur = new UserRelation();
        ur.setUserId2(userId);
        result.addAll(find(ur));

        return result;
    }

    public UserRelation findByUserRelation(int userId1, int userId2){
        UserRelation ur = new UserRelation();
        ur.setUserId1(userId1);
        ur.setUserId2(userId2);
        ArrayList<UserRelation> result = find(ur);
        if (result.size() > 0) {
            return result.get(0);
        }

        ur = new UserRelation();
        ur.setUserId1(userId2);
        ur.setUserId2(userId1);
        result = find(ur);

        if (result.size() > 0) {
            return result.get(0);
        }

        return null;
    }

    public boolean containsUserRelation(int userId1, int userId2) {
        if (findByUserRelation(userId1,userId2) != null) return true;
        return false;
    }
}
