/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.update.transport;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class TransportGraph {
    private HashMap<Integer,PlanetNode> planets = new HashMap<Integer,PlanetNode>();

    public TransportGraph() {

    }

    public void addPlanet(PlanetNode pn) {
        planets.put(pn.getPlanetId(),pn);
    }

    public PlanetNode getPlanet(int planetId) {
        return planets.get(planetId);
    }

    public ArrayList<PlanetNode> getAllPlanets() {
        ArrayList<PlanetNode> pnList = new ArrayList<PlanetNode>();
        pnList.addAll(planets.values());

        return pnList;
    }

    public void cleanUp() {
        for (PlanetNode pn : planets.values()) {
            pn.cleanUp();
        }
    }
}
