/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.enumeration;

/**
 *
 * @author Stefan
 */
public enum EEconomyType {
    INDUSTRY, RESEARCH, AGRICULTURE, RECREATIONAL, MILITARY, NOT_SPECIFIED, NOT_USED;
}
