/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.SizeRelation;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class SizeRelationDAO extends ReadOnlyTable<SizeRelation> implements GenericDAO<SizeRelation> {
    public SizeRelation getByRelModuleId(int id) {
        SizeRelation sr = new SizeRelation();
        sr.setChassisId(id);
        return get(sr);
    }
}
