/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.movable.FleetFormationExt;
import at.darkdestiny.core.movable.IFleet;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.service.FleetService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class FleetScanServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("In Fleet Scan servlet");
        int userId = Integer.parseInt((String) request.getSession().getAttribute("userId"));

        Map<String, String[]> params = request.getParameterMap();
        System.out.println("UserId : " + userId);
        System.out.println("params-size : " + params.size());

        
        PrintWriter out = response.getWriter();

        String fleetIdString = params.get("fleetId")[0];
        boolean isFleetFormation = false;
        if (fleetIdString.startsWith("FF")) {
            isFleetFormation = true;
            fleetIdString = fleetIdString.substring(2);
        }
        int systemId = -1;
        if(request.getParameter("systemId") != null){
            systemId = Integer.parseInt(request.getParameter("systemId"));
        }
        int planetId = -1;
        if(request.getParameter("planetId") != null){
            planetId = Integer.parseInt(request.getParameter("planetId"));
        }
        
        int fleetId = Integer.parseInt(fleetIdString);

        try {
            IFleet fleet = null;
            if (isFleetFormation) {
                fleet = new FleetFormationExt(fleetId);
            } else {
                fleet = new PlayerFleetExt(fleetId);
            }
            if(systemId >= 0 && fleet.canScanSystem()){
                FleetService.scanSystem(fleet);
               System.out.println("Startet systemscan");
            }else if(planetId >= 0 && fleet.canScanPlanet()){
               System.out.println("Startet PlanetScan");
                FleetService.scanPlanet(fleet);
            }          

            out.write("OKAY");
        }catch(Exception e){
            e.printStackTrace();
            out.write(e.toString());
        } finally {
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
