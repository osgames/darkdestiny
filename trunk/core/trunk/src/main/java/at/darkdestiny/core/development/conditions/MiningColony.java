/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.development.conditions;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class MiningColony implements IBonusCondition {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);    
    
    @Override
    public boolean isPossible(int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        
        if (pp.getPopulation() > 10000000) {
            return false;
        }
        
        return true;
    }
    
    @Override
    public boolean isPossible() {
        return false;
    }    
    
    @Override 
    public ArrayList<String> activeBoni(int bonusId, int planetId, int userId) {
        ArrayList<String> boniList = new ArrayList<String>();
        
        boniList.add("Mining Colony placeholder");
        
        return boniList;
    }    
    
    @Override 
    public void onActivation(int bonusId, int planetId, int userId) {
        
    }    
}
