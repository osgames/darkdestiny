/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.enumeration.ETerritoryMapShareType;
import at.darkdestiny.core.enumeration.ETerritoryMapType;
import at.darkdestiny.core.model.GameData;
import at.darkdestiny.core.model.TerritoryMap;
import at.darkdestiny.core.model.TerritoryMapPoint;
import at.darkdestiny.core.model.TerritoryMapShare;
import at.darkdestiny.core.service.Service;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Stefan
 */
public class TerritoryServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        
        boolean gotNessage = false;

        HttpSession session = request.getSession();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(request.getInputStream());
            Document d = builder.parse(is);
            NodeList nl = d.getElementsByTagName("Territory");
            for (int i = 0; i < nl.getLength(); i++) {
                Node n = nl.item(i);
                int id = Integer.parseInt(((Element) n).getAttribute("id"));
                //New Territory
                TerritoryMap tm = new TerritoryMap();
                int userId = Integer.parseInt((String) session.getAttribute("userId"));
                if (id == 0) {

                    //Only allow 3 territories
                    int size = Service.territoryMapDAO.findBy(userId, ETerritoryMapType.USER).size();
                    if (size == 3) {
                        continue;
                    }


                    tm.setDescription("None");
                    tm.setName("None");
                    tm.setType(ETerritoryMapType.USER);
                    tm.setRefId(Integer.parseInt((String) session.getAttribute("userId")));

                    tm = Service.territoryMapDAO.add(tm);

                    TerritoryMapShare tms = new TerritoryMapShare();
                    tms.setShow(true);
                    tms.setTerritoryId(tm.getId());
                    tms.setRefId(Integer.parseInt((String) session.getAttribute("userId")));
                    tms.setType(ETerritoryMapShareType.USER);

                    Service.territoryMapShareDAO.add(tms);
                } else {
                    tm = Service.territoryMapDAO.findById(id);
                    if (tm.getType().equals(ETerritoryMapShareType.USER) && userId != tm.getRefId()) {
                        //Not enough rights
                        return;
                    }

                }
                //Delete and reestablish nodes
                Service.territoryMapPointDAO.removeAll(Service.territoryMapPointDAO.findByTerritoryId(tm.getId()));

                NodeList childs = n.getChildNodes();
                for (int j = 0; j < childs.getLength(); j++) {
                    Node child = childs.item(j);
                    if (child.getNodeName() != null && child.getNodeName().trim().equals("Point")) {

                        TerritoryMapPoint tmp = new TerritoryMapPoint();

                        int childId = Integer.parseInt(((Element) child).getAttribute("id"));
                        int x = (int) Float.parseFloat(((Element) child).getAttribute("x"));
                        int y = (int) Float.parseFloat(((Element) child).getAttribute("y"));

                        if (x < 0) {
                            x = 0;
                        }
                        if (y < 0) {
                            y = 0;
                        }
                        GameData gd = Service.gameDataDAO.findById(1);
                        
                        if (x > gd.getWidth()) {
                            x =  gd.getWidth();
                        } 
                        if (y > gd.getHeight()) {
                            y = gd.getHeight();
                        }
                        tmp.setX(x);
                        tmp.setY(y);
                        tmp.setTerritoryId(tm.getId());
                        tmp.setId(childId);
                        //No Id let it autogenerate
                        Service.territoryMapPointDAO.add(tmp);
                    }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // outStream.println("Read " + message);
        gotNessage = true;

        if (!gotNessage) {
            System.out.println("Got no message");
        }
        /*
        System.out.println("getContentLength: "
        + request.getContentLength());        
        System.out.println("getContentType: "
        + request.getContentType()); */
        processRequest(request, response);

    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
