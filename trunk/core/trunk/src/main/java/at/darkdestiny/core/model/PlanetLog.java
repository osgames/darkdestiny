/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.EPlanetLogType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Dreloc
 */
@TableNameAnnotation(value = "planetlog")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlanetLog extends Model<PlanetLog> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("planetId")
    @IndexAnnotation(indexName = "planet")
    private Integer planetId;
    @FieldMappingAnnotation("userId")
    @IndexAnnotation(indexName = "user")
    private Integer userId;
    @FieldMappingAnnotation("time")
    private Integer time;
    @FieldMappingAnnotation("type")
    @DefaultValue("COLONIZED")
    private EPlanetLogType type;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the time
     */
    public Integer getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Integer time) {
        this.time = time;
    }

    /**
     * @return the type
     */
    public EPlanetLogType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EPlanetLogType type) {
        this.type = type;
    }
}
