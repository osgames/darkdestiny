 /*
 * html.java
 *
 * Created on 10. Mai 2007, 13:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.darkdestiny.core.view.html;
/**
 *
 * @author Horst
 */
public class html extends htmlObject{

    private body body;
  
    /** Creates a new instance of html */
    public html() {
        
        setBody(new body());
         properties = "";
      data ="";
    }

    public body getBody() {
        return body;
    }

    public void setBody(body body) {
        this.body = body;
    }

    public String toString(){
        
        String returnString = "";
        returnString += "<html";
        if(!properties.equals("")){
            returnString += properties;
        }
        returnString += ">\n";
        if(!body.equals("")){
            returnString += body.toString();
        }
        returnString += "\n</html>\n";
        return returnString;
    }


    
}
