/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "planetdefense")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlanetDefense extends Model<PlanetDefense> {

    @FieldMappingAnnotation("planetId")
    @IdFieldAnnotation
    private Integer planetId;
    @FieldMappingAnnotation(value = "systemId")
    @IdFieldAnnotation
    private Integer systemId;
    @FieldMappingAnnotation(value = "fleetId")
    @IdFieldAnnotation
    private Integer fleetId;
    @FieldMappingAnnotation(value = "unitId")
    @IdFieldAnnotation
    private Integer unitId;
    @FieldMappingAnnotation(value = "userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation(value = "type")
    @IdFieldAnnotation
    private EDefenseType type;
    @FieldMappingAnnotation(value = "count")
    private Integer count;

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the systemId
     */
    public Integer getSystemId() {
        return systemId;
    }

    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the fleetId
     */
    public Integer getFleetId() {
        return fleetId;
    }

    /**
     * @param fleetId the fleetId to set
     */
    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    /**
     * @return the unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId the unitId to set
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the type
     */
    public EDefenseType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EDefenseType type) {
        this.type = type;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }
}
