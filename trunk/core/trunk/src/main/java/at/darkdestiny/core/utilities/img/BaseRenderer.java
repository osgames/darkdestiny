package at.darkdestiny.core.utilities.img;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import javax.imageio.ImageWriter;
import javax.imageio.spi.IIORegistry;
import javax.imageio.spi.ImageOutputStreamSpi;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;

/**
 * Create an image in a specified Size,
 * and create the content by a renderer
 *
 * Return the created File to the User
 * using an output-Stream
 *
 * @author martin
 */
public class BaseRenderer {

	private int height;
	private int width;
	private BufferedImage img;
	private ImageOutputStreamSpi outputStream;
	private ImageWriterSpi outputWriter;

	/**
	 *
	 */
        public BaseRenderer(int width, int height) {
            init(width,height,BufferedImage.TYPE_INT_RGB);
        }

	public BaseRenderer(int width, int height, int imageType) {
            init(width,height,imageType);
	}

        private void init(int width, int height, int imageType) {
		this.height = height;
		this.width = width;

		//Bild erstellen:
		//zumindest versuchen
		img = new BufferedImage(width, height , imageType);
		if (img == null)
			throw new NullPointerException("Kann kein Bild im RAM erstellen");

		for (Iterator<ImageOutputStreamSpi> i = IIORegistry.getDefaultInstance().getServiceProviders(ImageOutputStreamSpi.class, false); i.hasNext();)
		{
			ImageOutputStreamSpi o = i.next();
			if (o.getOutputClass().equals(OutputStream.class))
				outputStream = o;
		}
		if (outputStream == null)
			throw new NullPointerException("Es gibt keine Passende Klasse und die Bilder an den Benutzer zu schicken");

		for (Iterator<ImageWriterSpi> i = IIORegistry.getDefaultInstance().getServiceProviders(ImageWriterSpi.class, false); i.hasNext();)
		{
			ImageWriterSpi o = i.next();
			String [] s = o.getFormatNames();
			for (String s1 : s)
				if (s1.equalsIgnoreCase("PNG"))
					outputWriter = o;

		}
        }

	public void modifyImage(IModifyImageFunction func)
	{
		func.setSize(width, height);
		func.run(img.getGraphics());
	}

	public void getImageMimeType()
	{
		System.err.println("=> "+outputWriter.getMIMETypes()[0]);
	}

	public void sendToUser(OutputStream os) throws IOException
	{
		ImageOutputStream output = outputStream.createOutputStreamInstance(os);
		ImageWriter writer = outputWriter.createWriterInstance("png");
		writer.setOutput(output);
		writer.write(img);
		output.flush();
		output.close();
	}

}
