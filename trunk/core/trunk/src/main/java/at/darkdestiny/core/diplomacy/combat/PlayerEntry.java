/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy.combat;

import at.darkdestiny.core.model.DiplomacyRelation;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class PlayerEntry implements CombatParticipent {
    private final int userId;
    
    private HashMap<AllianceEntry,DiplomacyRelation> relationsToAlliance = 
            new HashMap<AllianceEntry,DiplomacyRelation>();
    private HashMap<PlayerEntry,DiplomacyRelation> relationsToPlayer = 
            new HashMap<PlayerEntry,DiplomacyRelation>();    
    
    protected PlayerEntry(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public int getId() {
        return userId;
    }

    @Override
    public ArrayList<Integer> getAllMemberIds() {
        ArrayList<Integer> userIds = new ArrayList<Integer>();
        userIds.add(userId);
        
        return userIds;
    }    
    
    @Override
    public DiplomacyRelation getRelationToAlliance(AllianceEntry ae) {
        return relationsToAlliance.get(ae);
    }

    @Override
    public DiplomacyRelation getRelationToPlayer(PlayerEntry pe) {
        return relationsToPlayer.get(pe);
    }    
    
    @Override
    public DiplomacyRelation getRelationTo(CombatParticipent cp) {
        if (cp instanceof AllianceEntry) {
            return relationsToAlliance.get((AllianceEntry)cp);  
        } else {
            return relationsToPlayer.get((PlayerEntry)cp);    
        }        
    }      
    
    protected void addAllianceRelation(AllianceEntry to, DiplomacyRelation relation) {
        getRelationsToAlliance().put(to, relation);
    }    
    
    protected void addPlayerRelation(PlayerEntry to, DiplomacyRelation relation) {
        getRelationsToPlayer().put(to, relation);
    }

    public HashMap<AllianceEntry, DiplomacyRelation> getRelationsToAlliance() {
        return relationsToAlliance;
    }

    public HashMap<PlayerEntry, DiplomacyRelation> getRelationsToPlayer() {
        return relationsToPlayer;
    }
}
