/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.result.SurfaceResult;
import at.darkdestiny.core.service.ResearchService;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.Locale;

/**
 *
 * @author Stefan
 */
public class SpionageUtilities {
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);

    public static void scanPlanet(PlayerFleetExt pfe) {
        Planet p = pDAO.findById(pfe.getRelativeCoordinate().getPlanetId());
        PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());

        GenerateMessage gm = new GenerateMessage();
        gm.setDestinationUserId(pfe.getUserId());
        gm.setMessageType(EMessageType.SYSTEM);
        gm.setMessageContentType(EMessageContentType.SCAN);
        gm.setSourceUserId(uDAO.getSystemUser().getUserId());
        gm.setTopic(ML.getMLStr("message_msg_scanreport_planetsubject", pfe.getUserId()).replace("%PLANETID%", String.valueOf(p.getId())).replace("%SYSTEMID%", String.valueOf(p.getSystemId())));

        // Build message
        String message = getBasicScanMessage(p, pp, pfe.getUserId());

        gm.setMsg(message);
        gm.writeMessageToUser();
    }

    public static void scanSystem(PlayerFleetExt pfe) {

        GenerateMessage gm = new GenerateMessage();
        String message = "";
        gm.setDestinationUserId(pfe.getUserId());
        gm.setMessageType(EMessageType.SYSTEM);
        gm.setMessageContentType(EMessageContentType.SCAN);
        gm.setSourceUserId(uDAO.getSystemUser().getUserId());
        gm.setTopic(ML.getMLStr("message_msg_scanreport_systemsubject", pfe.getUserId()).replace("%SYSTEMID%", String.valueOf(pfe.getRelativeCoordinate().getSystemId())));

        // Build message
        for (Planet p_list : pDAO.findBySystemId(pfe.getRelativeCoordinate().getSystemId())) {
            PlayerPlanet pp = ppDAO.findByPlanetId(p_list.getId());
            //DiplomacyCheck => Continue if Enemy planet
            if ((pp != null && DiplomacyUtilities.getDiplomacyResult(pfe.getUserId(), pp.getUserId()).isAttacks()) && pfe.getUserId() != pp.getUserId()) {
                continue;
            }

            message = message + getBasicScanMessage(p_list, ppDAO.findByPlanetId(p_list.getId()), pfe.getUserId()) + "<BR><BR>";

        }

        gm.setMsg(message);
        gm.writeMessageToUser();
    }

    private static String getBasicScanMessage(Planet p, PlayerPlanet pp, int scanningUser) {
        StringBuilder message = new StringBuilder();

        message.append(ML.getMLStr("message_msg_scanreport_headline", scanningUser).replace("%PLANETID%", String.valueOf(p.getId())).replace("%SYSTEMID%", String.valueOf(p.getSystemId())));
        message.append(ML.getMLStr("message_msg_scanreport_general", scanningUser));
        if (p.getMaxPopulation() == 0) {
            message.append(ML.getMLStr("message_msg_scanreport_populationhostile", scanningUser));
        } else {
            message.append(ML.getMLStr("message_msg_scanreport_populationmaximum", scanningUser).replace("%MAXPOP%", FormatUtilities.getFormatScaledNumber(p.getMaxPopulation(), GameConstants.VALUE_TYPE_NORMAL)));
        }
        message.append(ML.getMLStr("message_msg_scanreport_colonizationstatus", scanningUser));

        double wrongness = Math.random() / 2.5d;
        if (Math.random() < 0.5d) {
            wrongness = -wrongness;
        }

        long scannedPop = 0l;
        if (pp != null) {
            scannedPop = (pp.getPopulation() + (long) (pp.getPopulation() * wrongness));
        }

        if (scannedPop == 0) {
            message.append(ML.getMLStr("message_msg_scanreport_populationnone", scanningUser));
        } else if (scannedPop < 1000000) {
            message.append(ML.getMLStr("message_msg_scanreport_populationsmall", scanningUser));
        } else {
            if (p.getMaxPopulation() < scannedPop) {
                scannedPop = p.getMaxPopulation();
            }
            message.append(ML.getMLStr("message_msg_scanreport_populationestimated", scanningUser).replace("%POP%", FormatUtilities.getFormattedNumber((int) (scannedPop / 1000000))));
        }

        // Building density report
        SurfaceResult sr = p.getSurface();
        int buildSquares = p.getSurfaceSquares() - sr.getFreeSurface();

        if (buildSquares == 0) {
            message.append(ML.getMLStr("message_msg_scanreport_housingnone", scanningUser));
        } else if (buildSquares < 50) {
            message.append(ML.getMLStr("message_msg_scanreport_housingtiny", scanningUser));
        } else if (buildSquares < 100) {
            message.append(ML.getMLStr("message_msg_scanreport_housingsmall", scanningUser));
        } else if (buildSquares < 400) {
            message.append(ML.getMLStr("message_msg_scanreport_housingmedium", scanningUser));
        } else if (buildSquares < 1000) {
            message.append(ML.getMLStr("message_msg_scanreport_housinglarge", scanningUser));
        } else if (buildSquares < 5000) {
            message.append(ML.getMLStr("message_msg_scanreport_housingstrong", scanningUser));
        } else {
            message.append(ML.getMLStr("message_msg_scanreport_housingenormous", scanningUser));
        }

        message.append(ML.getMLStr("message_msg_scanreport_ressources", scanningUser));
        PlanetRessource iron = prDAO.findBy(p.getId(), Ressource.PLANET_IRON, EPlanetRessourceType.PLANET);
        PlanetRessource ynke = null;
        PlanetRessource howal = null;
        if (ResearchService.isResearched(scanningUser, Research.YNKELONIUM)) {
            ynke = prDAO.findBy(p.getId(), Ressource.PLANET_YNKELONIUM, EPlanetRessourceType.PLANET);
        }
        if (ResearchService.isResearched(scanningUser, Research.HOWALGONIUMSCANNER)) {
            howal = prDAO.findBy(p.getId(), Ressource.PLANET_HOWALGONIUM, EPlanetRessourceType.PLANET);
        }

        if ((iron == null) && (ynke == null) && (howal == null)) {
            message.append(ML.getMLStr("message_msg_scanreport_ressourcesnone", scanningUser));
        } else {
            long scannedIron = 0;
            long scannedYnkelonium = 0;
            long scannedHowalgonium = 0;

            wrongness = Math.random() - 0.5d;
            if (iron != null) {
                scannedIron = (iron.getQty() + (long) (iron.getQty() * wrongness));
            }
            wrongness = Math.random() - 0.5d;
            if (ynke != null) {
                scannedYnkelonium = (ynke.getQty() + (long) (ynke.getQty() * wrongness));
            }
            wrongness = Math.random() - 0.5d;
            if (howal != null) {
                scannedHowalgonium = (howal.getQty() + (long) (howal.getQty() * wrongness));
            }

            Locale l = LanguageUtilities.getMasterLocaleForUser(scanningUser);

            if (scannedIron == 0) {
                // } else if (scannedIron < 1000000) {
                //     message.append(ML.getMLStr("message_msg_scanreport_ressourcesironsmall", scanningUser).replace("%IRON%", FormatUtilities.getFormattedNumber(((int) (scannedIron / 1000) * 1000))));
            } else {
                message.append(ML.getMLStr("message_msg_scanreport_ressourcesiron", scanningUser).replace("%IRON%", FormatUtilities.getFormatScaledNumber(scannedIron, GameConstants.VALUE_TYPE_INTONS, l)));
            }

            if (ResearchService.isResearched(scanningUser, 51)) {
                if (scannedYnkelonium == 0) {

                // } else if (scannedYnkelonium < 1000000) {
                //     message.append(ML.getMLStr("message_msg_scanreport_ressourcesynkesmall", scanningUser).replace("%YNKE%", FormatUtilities.getFormattedNumber(((int) (scannedYnkelonium / 1000) * 1000))));
                } else {
                    message.append(ML.getMLStr("message_msg_scanreport_ressourcesynke", scanningUser).replace("%YNKE%", FormatUtilities.getFormatScaledNumber(scannedYnkelonium, GameConstants.VALUE_TYPE_INTONS, l)));
                }
            } else {
                scannedYnkelonium = 0;
            }

            if (ResearchService.isResearched(scanningUser, 75)) {
                if (scannedHowalgonium == 0) {
                // } else if (scannedHowalgonium < 1000000) {
                //     message.append(ML.getMLStr("message_msg_scanreport_ressourceshowasmall", scanningUser).replace("%HOWA%", FormatUtilities.getFormattedNumber(((int) (scannedHowalgonium / 1000) * 1000))));
                } else {
                    message.append(ML.getMLStr("message_msg_scanreport_ressourceshowa", scanningUser).replace("%HOWA%", FormatUtilities.getFormatScaledNumber(scannedHowalgonium, GameConstants.VALUE_TYPE_INTONS, l)));
                }
            } else {
                scannedHowalgonium = 0;
            }
            if ((scannedIron == 0) && (scannedYnkelonium == 0) && (scannedHowalgonium == 0)) {
                message.append(ML.getMLStr("message_msg_scanreport_ressourcesnoresult", scanningUser));
            }
        }
        return message.toString();
    }
}
