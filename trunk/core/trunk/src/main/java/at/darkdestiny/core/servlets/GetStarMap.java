/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.StarMapInfoUtilities;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.painting.map.AreaMapContainer;
import at.darkdestiny.core.painting.map.ButtonRenderer;
import at.darkdestiny.core.painting.map.FleetRenderer;
import at.darkdestiny.core.painting.map.IMapDataSource;
import at.darkdestiny.core.painting.map.ObservatoryRenderer;
import at.darkdestiny.core.painting.map.StarMapDataSource;
import at.darkdestiny.core.painting.map.StarMapViewCoordinate;
import at.darkdestiny.core.painting.map.StarRenderer;
import at.darkdestiny.core.service.PlanetService;
import at.darkdestiny.core.service.ProfileService;
import at.darkdestiny.core.utilities.img.BackgroundPainter;
import at.darkdestiny.core.utilities.img.BaseRenderer;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.util.DebugBuffer;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class GetStarMap extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("image/png");
        ServletOutputStream out = response.getOutputStream();

        HttpSession session = request.getSession();
        
        try {
            List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();

            int x = 0;
            int y = 0;
            int userId = 0;
            String operation;
            
            if (request.getParameter("x") != null) {
                x = Integer.parseInt(request.getParameter("x"));
            }
            if (request.getParameter("y") != null) {
                y = Integer.parseInt(request.getParameter("y"));
            }
            
            operation = request.getParameter("op");          
            
            userId = Integer.parseInt((String) session.getAttribute("userId"));                        
            
            System.out.println("USERID: " + userId);
            System.out.println("OPERATION: " + operation);
            
            int[] xySize = ProfileService.getStarMapSize(userId);
            if ((x > 0) && (y > 0)) {
                xySize[0] = x;
                xySize[1] = y;
            }           
            
            int picWidth = xySize[0];
            int picHeight = xySize[1];           
            
            StringBuffer xml = StarMapInfoUtilities.getStarMapInfo(userId);
            IMapDataSource iMDS = new StarMapDataSource(xml);
            ((StarMapDataSource)iMDS).parse();
            
            AbsoluteCoordinate topLeft = null;
            AbsoluteCoordinate bottomRight = null;
            
            int planetId = PlanetService.getHomePlanetForUser(userId);
            
            StarMapViewCoordinate smvc = AreaMapContainer.getCurrentView(userId);
            if (smvc == null) {
                AbsoluteCoordinate center = new RelativeCoordinate(0,planetId).toAbsoluteCoordinate();
                topLeft = new AbsoluteCoordinate(center.getX() - (int)Math.floor(picWidth / 2d),center.getY() - (int)Math.floor(picHeight / 2d));
                
                smvc = new StarMapViewCoordinate(topLeft,picWidth,picHeight,1f);
            }
            
            // Do we have a scale or move operation?
            // If yes => scale or move!
            System.out.println("operation is " + operation);
            
            if (operation != null) {
                if (operation.equalsIgnoreCase("up")) {
                    smvc = new StarMapViewCoordinate(new AbsoluteCoordinate(smvc.getTopLeft().getX(), smvc.getTopLeft().getY() - (int)Math.round((picHeight / 4) / smvc.getZoom())),picWidth,picHeight,smvc.getZoom());
                } else if (operation.equalsIgnoreCase("down")) {
                    smvc = new StarMapViewCoordinate(new AbsoluteCoordinate(smvc.getTopLeft().getX(), smvc.getTopLeft().getY() + (int)Math.round((picHeight / 4) / smvc.getZoom())),picWidth,picHeight,smvc.getZoom());
                } else if (operation.equalsIgnoreCase("left")) {
                    smvc = new StarMapViewCoordinate(new AbsoluteCoordinate(smvc.getTopLeft().getX() - (int)Math.round((picWidth / 4) / smvc.getZoom()), smvc.getTopLeft().getY()),picWidth,picHeight,smvc.getZoom());
                } else if (operation.equalsIgnoreCase("right")) {
                    smvc = new StarMapViewCoordinate(new AbsoluteCoordinate(smvc.getTopLeft().getX() + (int)Math.round((picWidth / 4) / smvc.getZoom()), smvc.getTopLeft().getY()),picWidth,picHeight,smvc.getZoom());
                } else if (operation.equalsIgnoreCase("zoomin")) {
                    int newTLX = smvc.getTopLeft().getX() + (int)Math.round((picWidth / smvc.getZoom()) / 4);
                    int newTLY = smvc.getTopLeft().getY() + (int)Math.round((picHeight / smvc.getZoom()) / 4);
                    
                    smvc = new StarMapViewCoordinate(new AbsoluteCoordinate(newTLX, newTLY), picWidth,picHeight,smvc.getZoom() * 2f);
                } else if (operation.equalsIgnoreCase("zoomout")) {
                    int newTLX = smvc.getTopLeft().getX() - (int)Math.round(picWidth / (smvc.getZoom() * 2));
                    int newTLY = smvc.getTopLeft().getY() - (int)Math.round(picHeight / (smvc.getZoom() * 2));                    
                    
                    smvc = new StarMapViewCoordinate(new AbsoluteCoordinate(newTLX, newTLY),picWidth,picHeight,smvc.getZoom() / 2f);
                }
            }
            
            AreaMapContainer.setNewView(userId, smvc);
            AreaMapContainer.clearStarMapObjects(userId);
            
            // AbsoluteCoordinate center = new RelativeCoordinate(0,planetId).toAbsoluteCoordinate();
            // topLeft = new AbsoluteCoordinate(center.getX() - (int)Math.floor(picWidth / 2d),center.getY() - (int)Math.floor(picHeight / 2d));
            bottomRight = new AbsoluteCoordinate(smvc.getTopLeft().getX() + smvc.getxSize(),smvc.getTopLeft().getY() + smvc.getySize());
            
            allItems.add(new ObservatoryRenderer(userId, smvc,iMDS));
            allItems.add(new StarRenderer(userId, smvc,iMDS,this.getServletContext().getRealPath(GameConfig.getInstance().picPath() + "/pic/starmap/") + "/"));
            allItems.add(new FleetRenderer(userId, smvc,iMDS,this.getServletContext().getRealPath(GameConfig.getInstance().picPath() + "/pic/starmap/") + "/"));
            
            ButtonRenderer bRenderer = new ButtonRenderer(userId, this.getServletContext().getRealPath(GameConfig.getInstance().picPath() + "/pic/starmap/") + "/");                        
            allItems.add(bRenderer);

            BaseRenderer renderer = new BaseRenderer(picWidth, picHeight);
// Schreibt den Mime-Type in den Log, Nur Benutzen, wenn
// genau das gew&uuml;nscht wird
// renderer.getImageMimeType();

            renderer.modifyImage(new BackgroundPainter(0x000000));

            for (IModifyImageFunction mif : allItems) {
                renderer.modifyImage(mif);
            }

            renderer.sendToUser(out);
            
            bRenderer.createMapEntry();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while generating galaxy pic: ", e);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
