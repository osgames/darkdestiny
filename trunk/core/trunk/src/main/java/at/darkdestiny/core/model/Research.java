/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "research")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Research extends Model<Ressource> implements ITechFunctions {

    public static final int ID_ORBITALKOLONIE = 66;
    public static final int ID_FUSIONPLANT = 41;
    public static final int LOCK_TECH = 999;
    public static final int KAMPFANZUG = 84;
    public static final int SERUN = 129;
    public static final int HOWALGONIUMSCANNER = 75;
    public static final int YNKELONIUM = 51;
    public static final int ID_LINEARKONVERTER = 81;
    public static final int ID_FIGHTER = 25;
    public static final int ID_COLONIZATION = 34;
    public static final int ID_DESTROYER = 33;
    public static final int ID_BATTLESHIP = 60;
    @FieldMappingAnnotation(value = "id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    Integer id;
    @FieldMappingAnnotation("name")
    String name;
    @FieldMappingAnnotation("rp")
    Integer rp;
    @FieldMappingAnnotation("rpComputer")
    Integer rpComputer;
    @FieldMappingAnnotation("rpSpecial")
    Integer rpSpecial;
    @ColumnProperties(length = 100)
    @StringType("text")
    @FieldMappingAnnotation("shortText")
    String shortText;
    @ColumnProperties(length = 100)
    @StringType("text")
    @FieldMappingAnnotation("detailText")
    String detailText;
    @ColumnProperties(length = 500)
    @StringType("text")
    @FieldMappingAnnotation("smallPic")
    String smallPic;
    @FieldMappingAnnotation("largePic")
    String largePic;
    @FieldMappingAnnotation("bonus")
    private Double bonus;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the rp
     */
    public Integer getRp() {
        return rp;
    }

    /**
     * @param rp the rp to set
     */
    public void setRp(Integer rp) {
        this.rp = rp;
    }

    /**
     * @return the rpComputer
     */
    public Integer getRpComputer() {
        return rpComputer;
    }

    /**
     * @param rpComputer the rpComputer to set
     */
    public void setRpComputer(Integer rpComputer) {
        this.rpComputer = rpComputer;
    }

    /**
     * @return the rpSpecial
     */
    public Integer getRpSpecial() {
        return rpSpecial;
    }

    /**
     * @param rpSpecial the rpSpecial to set
     */
    public void setRpSpecial(Integer rpSpecial) {
        this.rpSpecial = rpSpecial;
    }

    /**
     * @return the shortText
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * @param shortText the shortText to set
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * @return the detailText
     */
    public String getDetailText() {
        return detailText;
    }

    /**
     * @param detailText the detailText to set
     */
    public void setDetailText(String detailText) {
        this.detailText = detailText;
    }

    /**
     * @return the smallPic
     */
    public String getSmallPic() {
        return smallPic;
    }

    /**
     * @param smallPic the smallPic to set
     */
    public void setSmallPic(String smallPic) {
        this.smallPic = smallPic;
    }

    /**
     * @return the largePic
     */
    public String getLargePic() {
        return largePic;
    }

    /**
     * @param largePic the largePic to set
     */
    public void setLargePic(String largePic) {
        this.largePic = largePic;
    }

    /**
     * @return the bonus
     */
    public Double getBonus() {
        return bonus;
    }

    /**
     * @param bonus the bonus to set
     */
    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    public int getTechId() {
        return id;
    }

    public String getTechName() {
        return name;
    }

    public String getTechDescription() {
        return shortText;
    }
}
