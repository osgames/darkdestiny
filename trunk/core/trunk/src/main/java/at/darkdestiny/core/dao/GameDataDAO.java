/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.GameData;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Bullet
 */
public class GameDataDAO extends ReadWriteTable<GameData> implements GenericDAO {

    public GameData findById(Integer id) {
        GameData gd = new GameData();
        gd.setId(id);
        return (GameData)get(gd);
    }
}
