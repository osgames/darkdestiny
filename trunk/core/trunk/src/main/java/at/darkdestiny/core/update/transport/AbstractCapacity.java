/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.update.transport;

import at.darkdestiny.core.update.TradeRouteDetailProcessing;
import com.google.common.collect.Sets;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
abstract class AbstractCapacity implements ICapacity {
    protected final int orgMaxCapacity;    
    protected int maxCapacity;    
    
    public AbstractCapacity(int capacity) {
        this.orgMaxCapacity = capacity;
        maxCapacity = capacity;
    }
    
    private HashSet<TradeRouteDetailProcessing> trdpList = Sets.newHashSet();    
    /**
     * @return the orgMaxCapacity
     */
    public int getOrgMaxCapacity() {
        return orgMaxCapacity;
    }
    
    public void addTradeRouteDetailProcessing(TradeRouteDetailProcessing trdp) {
        getTrdpList().add(trdp);
    }        
    
    public int getFreeCapacity() {
        // boolean available = false;
        int freeCapacity = maxCapacity;
        
        for (TradeRouteDetailProcessing trdp : getTrdpList()) {
            if ((this instanceof RessCapacityMultiEdge) || (this instanceof CapacityMultiEdge)) {                
                freeCapacity -= (int)Math.ceil(trdp.getAssignedQty() * trdp.getRouteDistance() * 2d);           
            } else {
                // System.out.println(trdp + " FC: " + freeCapacity + " AssignedCapacity: " + trdp.getAssignedQty());
                freeCapacity -= trdp.getAssignedQty();
            }
            /*
            if (!trdp.isClosed()) {
                available = true;
            } else {
                continue;
            }
            
            if ((this instanceof RessCapacityMultiEdge) || (this instanceof CapacityMultiEdge)) {
                freeCapacity -= Math.ceil((trdp.getAssignedQtyTmp()) * trdp.getRouteDistance());
            } else {
                freeCapacity -= trdp.getAssignedQtyTmp();
            }
            if (freeCapacity < 0) freeCapacity = 0;
            */
        }
        
        if (this instanceof CapacityMultiEdge) {
            // System.out.println("[TRADE] FC: " + freeCapacity + " AssignedCapacity: " + (maxCapacity - freeCapacity));            
        }
        
        // if (!available) return 0;
        return freeCapacity;
    }    

    /**
     * @return the trdpList
     */
    public HashSet<TradeRouteDetailProcessing> getTrdpList() {
        return trdpList;
    }
    
    public int getOpenRoutes() {
        int count = 0;
        
        for (TradeRouteDetailProcessing trdp : trdpList) {
            if (!trdp.isClosed()) count++;
        }
        
        return count;
    }
}
