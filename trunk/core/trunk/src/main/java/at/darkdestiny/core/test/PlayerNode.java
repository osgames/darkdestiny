/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.test;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class PlayerNode {
    private final HashMap<PlayerNode,Connector> relationMap = new HashMap<PlayerNode,Connector>();
    private final int userId;
    
    private ArrayList<AttackResolution> attacks = new ArrayList<AttackResolution>();
    private ArrayList<HelpResolution> helps = new ArrayList<HelpResolution>();
    private int group = -1;
    
    public PlayerNode(int userId) {
        this.userId = userId;
    }
    
    public void addRelation(PlayerNode pn, Connector relation) {
        getRelationMap().put(pn,relation);

        if (relation instanceof AlliedConnector) {
            getHelps().add(new HelpResolution(pn.getUserId()));
        } else if (relation instanceof EnemyConnector) {
            getAttacks().add(new AttackResolution(pn.getUserId()));
        }
    }

    public int getUserId() {
        return userId;
    }

    public ArrayList<AttackResolution> getAttacks() {
        return attacks;
    }

    public ArrayList<HelpResolution> getHelps() {
        return helps;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public HashMap<PlayerNode, Connector> getRelationMap() {
        return relationMap;
    }
}
