package at.darkdestiny.core.ressources;

import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.Ressource;
import java.util.ArrayList;
import java.util.Map;

/**
 * Eine Variante mit veränderbaren Werten für 
 * die Ressourcen, sowie die Credits.
 * 
 * Mit einem zusätzlichen Constructor,
 * mit dem direkt die Felder mit den Werten 
 * eines Benutzers auf einem Planeten gefüllt
 * werden können
 * 
 * @author martin
 */
public class MutableRessourcesEntry extends RessourcesEntry implements MutableRessourceCost {
	/**
	 * Get Information about the supplied user on the supplied 
	 * planet ;-) 
	 */    
	public MutableRessourcesEntry(int userId, int planetId) {                
                at.darkdestiny.core.model.UserData ud = userDataDAO.findByUserId(userId);                            
                ArrayList<PlanetRessource> prList = planetRessourceDAO.findByPlanetId(planetId);

                for (PlanetRessource pr : prList) {                    
                    if (pr.getType() != EPlanetRessourceType.INSTORAGE) continue;
                    ress.put(pr.getRessId(),new RessAmountEntry(pr.getRessId(), pr.getQty()));                    
                }
                
		ress.put(Ressource.CREDITS,new RessAmountEntry(Ressource.CREDITS, ud.getCredits()));  
                ress.put(Ressource.DEV_POINTS, new RessAmountEntry(Ressource.DEV_POINTS, ud.getDevelopementPoints()));
	}
        
	public MutableRessourcesEntry() {
	}
        
        @Override
        public void setRess(int ressId, long qty) {
            ress.put(ressId,new RessAmountEntry(ressId,qty));
        }
        
        public void subtractRess(RessourcesEntry rse, double multiplier) {
            for (Map.Entry<Integer,RessAmountEntry> me : rse.getRess().entrySet()) {
                
                if (ress.containsKey(me.getKey())) {
                    ress.put(me.getKey(), new RessAmountEntry(me.getKey(),ress.get(me.getKey()).getQty() - (double)(me.getValue().getQty() * multiplier)));
                } else {
                    ress.put(me.getKey(),new RessAmountEntry(me.getKey(),-me.getValue().getQty()));
                }
            }
        }
        
        public void multiplyRessources(double multiplier) {
            for (Map.Entry<Integer,RessAmountEntry> me : ress.entrySet()) {
                ress.put(me.getKey(), new RessAmountEntry(me.getKey(),ress.get(me.getKey()).getQty() * multiplier));
            }            
        }

        public void multiplyRessources(double multiplier, double creditmultiplier) {
            for (Map.Entry<Integer,RessAmountEntry> me : ress.entrySet()) {
                if (me.getKey() == Ressource.CREDITS) {
                    ress.put(me.getKey(), new RessAmountEntry(me.getKey(),ress.get(me.getKey()).getQty() * creditmultiplier));
                } else {
                    ress.put(me.getKey(), new RessAmountEntry(me.getKey(),ress.get(me.getKey()).getQty() * multiplier));
                }
            }            
        }        

        public void addRess(RessourcesEntry rse) {                        
            for (Map.Entry<Integer,RessAmountEntry> me : rse.getRess().entrySet()) {
                if (ress.containsKey(me.getKey())) {
                    ress.put(me.getKey(), new RessAmountEntry(me.getKey(),ress.get(me.getKey()).getQty() + (me.getValue().getQty())));
                } else {
                    ress.put(me.getKey(),new RessAmountEntry(me.getKey(),(me.getValue().getQty())));
                }
            }            
        }

        public void addRess(RessourcesEntry rse, double multiplier) {
            for (Map.Entry<Integer,RessAmountEntry> me : rse.getRess().entrySet()) {
                if (ress.containsKey(me.getKey())) {
                    ress.put(me.getKey(), new RessAmountEntry(me.getKey(),ress.get(me.getKey()).getQty() + (me.getValue().getQty() * multiplier)));
                } else {
                    ress.put(me.getKey(),new RessAmountEntry(me.getKey(),me.getValue().getQty() * multiplier));
                }
            }            
        }

        public void roundValues() {
            for (Map.Entry<Integer,RessAmountEntry> entry : ress.entrySet()) {
                RessAmountEntry rae = entry.getValue();
                RessAmountEntry raeNew = new RessAmountEntry(rae.getRessId(),(long)Math.ceil(rae.getQty()));
                entry.setValue(raeNew);
            }
        }
}
