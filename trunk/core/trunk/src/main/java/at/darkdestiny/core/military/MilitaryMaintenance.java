/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.military;

import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.result.MilitaryMaintenanceCostResult;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class MilitaryMaintenance {        
    private PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    
    // 1 Year is 365 Ticks
    public static final int MAINTENANCE_PER_YEAR_PERC = 1; 
    
    public MilitaryMaintenanceCostResult calculateMilitaryMaintenanceCost() {
        MilitaryMaintenanceCostResult mmcr = new MilitaryMaintenanceCostResult();
        
        // Loop all ships and all planetary defense structures 
        // (Ship + Buildingcost) / 365 / (MAINTENANCE_PER_YEAR_PERC * 100) is total maintenance cost
        
        return mmcr;
    }
}
