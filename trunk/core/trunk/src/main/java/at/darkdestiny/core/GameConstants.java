
/*
 * Constants.java
 *
 * Created on 11. M�rz 2003, 20:47
 */

package at.darkdestiny.core;

/**
 *
 * @author  Rayden
 */
public class GameConstants {
    public static final boolean LOGIN_ONLYADMIN = false;


    public static final int TILE_SIZE = 100;

    public static int UNIVERSE_HEIGHT = 10000;
    public static int UNIVERSE_WIDTH = 10000;
    // GLOBAL VALUES
    public static final int TRIAL_END_TECH = 31;
    public static final int GLOBAL_MONEY_MODIFICATOR = 200000;
    public static final double GLOBAL_MAX_MILIZ_PERC = 0.005d;
    public static final int GLOBAL_MILIZ_TRAIN_UNIT = 2780;

    // ACTION TYPES
    public static final int ACT_TYPE_BUILDING = 1;
    public static final int ACT_TYPE_RESEARCH = 2;
    public static final int ACT_TYPE_MODULE = 3;
    public static final int ACT_TYPE_SHIP = 4;
    public static final int ACT_TYPE_GROUNDTROOPS = 5;
    public static final int ACT_TYPE_FLIGHT = 6;
    public static final int ACT_TYPE_DEFENSE = 7;
    public static final int ACT_TYPE_SPYRELATED = 8;
    public static final int ACT_TYPE_TRADE = 9;
    public static final int ACT_TYPE_DECONSTRUCT = 10;
    public static final int ACT_TYPE_PROBE = 11;

    // PLANET SWITCH
    public static final int SWITCH_HOMESYSTEM = 1;
    public static final int SWITCH_NEXT_HIGHER = 2;
    public static final int SWITCH_NEXT_LOWER = 3;
    public static final int SWITCH_BY_PARAMETER = 4;

    // TYPES OF VALUES
    public static final int VALUE_TYPE_NORMAL = 1;
    public static final int VALUE_TYPE_ENERGY = 2;
    public static final int VALUE_TYPE_INTONS = 3;

    // TYPES OF PLANET
     public static final int OWN_PLANET = 1;
    public static final int ENEMY_PLANET = 2;
    public static final int NEUTRAL_PLANET = 3;

    // TYPES OF SYSTEMVIEWS
    public static final int SYS_OWN = 1;
    public static final int SYS_GENERAL = 2;
    public static final int SYS_BY_ID = 3;
    public static final int SYS_BY_NAME = 4;

    // TYPES OF MODULES
    public static final int MODULE_HYPER_ENGINE = 1;
    public static final int MODULE_ARMOR = 2;
    public static final int MODULE_WEAPON = 3;
    public static final int MODULE_ENGINE = 4;
    public static final int MODULE_CHASSIS = 5;
    public static final int MODULE_ROCKET = 6;
    public static final int MODULE_FREE_2 = 7;
    public static final int MODULE_SHIELD = 8;
    public static final int MODULE_WEAPON_SUPPORT = 9;
    public static final int MODULE_REACTOR = 10;
    public static final int MODULE_SPECIAL = 11;
    public static final int MODULE_HANGAR = 12;

    // LOADING TYPES
    public static final int LOADING_RESS = 1;
    public static final int LOADING_TROOPS = 2;
    public static final int LOADING_SHIPS = 3;
    public static final int LOADING_POPULATION = 4;
    public static final int LOADING_SCRAP = 5;


    // CONFIRM TYPES
    public static final int CONFIRM_FLEET_MOVEMENT = 1;
    public static final int DESTROY_BUILDING = 2;
    public static final int DESTROY_BUILDING_ORDER = 3;
    public static final int DESTROY_SHIP = 4;
    public static final int DESTROY_SHIP_BUILD_ORDER = 5;
    public static final int CONFIRM_SHIP_UPGRADE = 6;
    public static final int CONFIRM_INCONSTRUCTION_CANCEL = 7;
    public static final int CONFIRM_PROBE = 8;
    public static final int CONFIRM_DESTROY_STARBASE = 9;
    public static final int CONFIRM_UPGRADE_STARBASE = 10;
    public static final int CONFIRM_TRADE_TREATY = 11;

    // PLANET UPGRADES
    public static final int UPDATE_LOCAL_ADMINISTRATION = 1;
    public static final int UPDATE_PLANETARY_ADMINISTRATION = 2;
    public static final int UPDATE_PLANETARY_GOVERNMENT = 3;
    public static final int UPDATE_MINING_DESINTEGRATOR = 4;
    public static final int UPDATE_VULCAN_ACTIVITY = 5;
    public static final int UPDATE_HOMEWORLD = 6;
    public static final int UPDATE_SUPER_COMPUTER = 7;

    public final static int STATISTIC_INCOME = 0;
    public final static int STATISTIC_POPULATION = 1;
    public final static int STATISTIC_PLANETS = 2;
    public final static int STATISTIC_SYSTEMS = 3;
        /*
    public final static int INVADE = 99;
     */
    // Links
    public final static String LINK_ADMIN = "subCategory=99&page=admin";
    //Zeit bis der Account gel�scht wird
    //nachdem der Benutzer die L�schung beantragt hat
	public static final int DELETION_DELAY = 1008;

	//Hier wird beschrieben, wie ein Password, in der
	//Datenbank verschl�sselt wird
    public final static String ENCRYPT_PWD_START = "MD5('";
    public final static String ENCRYPT_PWD_END = "')";

    //Invalidierung der Session eines Users:
    public static final int INVALIDATE_USER_AFTER= 3600;


    public static boolean LOGIN_AVAILABLE = true;

    //Hier wird beschrieben, nach welcher Zeit in Ticks ein
    //Benutzer als inaktive betrachtet wird
	public static final int USER_DEAD_2 = 6*24*14; //6 Ticks / Stunde * 24 Stunden/Tag * 14 Tage


        //Management => Happiness

       public static final int MORALE_THRESHOLD = 70;
       public static final double CRIMINALITY_THRESHOLD = 25d;
       public static final double RIOTS_TRESHOLD = 25d;
       public static final double DEVELOPEMENT_POPULATION = 7000000000d;
       public static final double DEVELOPEMENT_POINTS_LIMIT_GLOBAL = 20;

    public final static Object appletSync = new Object();
    public static long NAME_PROTECTION_DURATION = 6 * 24 * 3;
}
