/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.update.transport;

import at.darkdestiny.core.update.TradeRouteDetailProcessing;
import at.darkdestiny.core.update.TradeRouteProcessing;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class PlanetNode implements ICapacity {
    private static final Logger log = LoggerFactory.getLogger(PlanetNode.class);
    
    private final int planetId;

    private ArrayList<DirectionalEdge> outgoingTrades = new ArrayList<DirectionalEdge>();
    private ArrayList<DirectionalEdge> incomingTrades = new ArrayList<DirectionalEdge>();

    private HashMap<Integer,Integer> maxInQty = new HashMap<Integer,Integer>();
    private HashMap<Integer,Integer> maxOutQty = new HashMap<Integer,Integer>();

    private HashMap<Integer,Integer> assignedInQty = new HashMap<Integer,Integer>();
    private HashMap<Integer,Integer> assignedOutQty = new HashMap<Integer,Integer>();    
    
    private CapacityMultiEdge capMultiEdge = null;    
    private HashMap<Integer,RessCapacityMultiEdge> ressCapMultiEdge = Maps.newHashMap();    
    
    public PlanetNode(int planetId) {
        this.planetId = planetId;
    }

    public void cleanUp() {

    }

    /**
     * @return the planetId
     */
    public int getPlanetId() {
        return planetId;
    }

    public void setCapacityMultiEdge(CapacityMultiEdge cme) {
        this.capMultiEdge = cme;
    }
    
    public void setRessCapacityMultiEdge(RessCapacityMultiEdge rcme) {
        ressCapMultiEdge.put(rcme.getRessId(),rcme);
    }

    public DirectionalEdge getDirectionalEdgeOutFor(TradeRouteDetailProcessing trdp) {
        for (DirectionalEdge de : outgoingTrades) {
            if (de.getDetailEntries().contains(trdp)) {
                return de;
            } 
        }

        return null;
    }    
    
    public DirectionalEdge getDirectionalEdgeOutFor(int ressId, TradeRouteDetailProcessing trdp) {
        for (DirectionalEdge de : outgoingTrades) {
            TradeRouteDetailProcessing trdpTmp = de.getEntryForRess(ressId);
            if (trdpTmp != null) {
                if (trdpTmp.equals(trdp)) {
                    return de;
                }
            } else {
                // log.debug("Could not find " + trdp + " for ressource " + ressId + " in outgoing nodes for " + this.getPlanetId());
            }
        }

        return null;
    }

    public void addOutgoingTrade(DirectionalEdge outgoing) {
        if (outgoing.getNoOfEntries() == 0) return;
        if (getOutgoingTrades().contains(outgoing)) return;
        // log.debug("Add outgoing route " + outgoing + " planet " + planetId);                
        getOutgoingTrades().add(outgoing);
    }

    public void addIncomingTrade(DirectionalEdge incoming) {
        if (incoming.getNoOfEntries() == 0) return;
        getIncomingTrades().add(incoming);
    }

    public void removeDirectionalEdgeIn(DirectionalEdge incoming) {
        incomingTrades.remove(incoming);
    }

    public void removeDirectionalEdgeOut(DirectionalEdge outgoing) {
        outgoingTrades.remove(outgoing);
    }

    public void setMaxOutgoingQty(int ressId, int qty) {
        maxOutQty.put(ressId, qty);
    }

    public void setMaxIncomingQty(int ressId, int qty) {
        maxInQty.put(ressId, qty);
    }

    public int getMaxOutgoingQty(int ressId) {
        Integer qty = maxOutQty.get(ressId);
        if (qty == null) return 0;
        return qty;
    }

    public int getMaxIncomingQty(int ressId) {
        Integer qty = maxInQty.get(ressId);
        if (qty == null) return 0;
        return qty;
    }    
    
    public int getAssignedOutgoingQty(int ressId) {
        Integer qty = assignedOutQty.get(ressId);
        if (qty == null) return 0;
        return qty;
    }

    public int getAssignedIncomingQty(int ressId) {
        Integer qty = assignedInQty.get(ressId);
        if (qty == null) return 0;
        return qty;
    }        
    
    public void incAssignedOutgoingQty(int ressId, int qty) {
        if (assignedOutQty.containsKey(ressId)) {
            assignedOutQty.put(ressId, assignedOutQty.get(ressId) + qty);
        } else {
            assignedOutQty.put(ressId, qty);
        }        
    }

    public void incAssignedIncomingQty(int ressId, int qty) {
        if (assignedInQty.containsKey(ressId)) {
            assignedInQty.put(ressId, assignedInQty.get(ressId) + qty);
        } else {
            assignedInQty.put(ressId, qty);
        }        
    }    
    
    public void decAssignedOutgoingQty(int ressId, int qty) {
        if (assignedOutQty.containsKey(ressId)) {
            assignedOutQty.put(ressId, assignedOutQty.get(ressId) - qty);
        } else {
            assignedOutQty.put(ressId, qty);
        }        
    }

    public void decAssignedIncomingQty(int ressId, int qty) {
        if (assignedInQty.containsKey(ressId)) {
            assignedInQty.put(ressId, assignedInQty.get(ressId) - qty);
        } else {
            assignedInQty.put(ressId, qty);
        }        
    }        
    
    public int getFreeIncomingQty(int ressId) {
        return getMaxIncomingQty(ressId) - getAssignedIncomingQty(ressId);
    }
    
    public int getFreeOutgoingQty(int ressId) {
        return getMaxOutgoingQty(ressId) - getAssignedOutgoingQty(ressId);
    }
    
    public ArrayList<TradeRouteDetailProcessing> getOutgoingForRess(int ressId) {
        ArrayList<TradeRouteDetailProcessing> allEntries = new ArrayList<TradeRouteDetailProcessing>();

        for (DirectionalEdge de : getOutgoingTrades()) {
            TradeRouteDetailProcessing trdp = de.getEntryForRess(ressId);
            if (trdp != null) allEntries.add(trdp);                
        }

        return allEntries;
    }

    /**
     * @return the outgoingTrades
     */
    public ArrayList<DirectionalEdge> getOutgoingTrades() {
        return outgoingTrades;
    }

    /**
     * @return the incomingTrades
     */
    public ArrayList<DirectionalEdge> getIncomingTrades() {
        return incomingTrades;
    }

    /**
     * @return the capMultiEdge
     */
    public CapacityMultiEdge getCapMultiEdge() {
        return capMultiEdge;
    }

    /**
     * @return the ressCapMultiEdge
     */
    public RessCapacityMultiEdge getRessCapMultiEdge(int ressId) {
        return ressCapMultiEdge.get(ressId);
    }
}
