/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.update;

;
import at.darkdestiny.core.dao.ConstructionRestrictionDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetLogDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.enumeration.EPlanetLogType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetLog;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.utilities.DestructionUtilities;
import at.darkdestiny.core.utilities.ViewTableUtilities;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;
import at.darkdestiny.framework.dao.DAOFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.dao.ConstructionRestrictionDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetLogDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.enumeration.EPlanetLogType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetLog;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.utilities.DestructionUtilities;
import at.darkdestiny.core.utilities.ViewTableUtilities;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;

/**
 *
 * @author Stefan
 */
public class AbandonColonies {

    private static final Logger log = LoggerFactory.getLogger(AbandonColonies.class);

    private PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

    private final UpdaterDataSet uds;

    protected AbandonColonies(UpdaterDataSet uds) {
        this.uds = uds;
    }

    protected boolean abandonColony(PlayerPlanet pp, int time) {
        // Check if there are PlayerTroops, if yes dont abandon
        // if (ptDAO.findByPlanetId(pp.getPlanetId()).size() > 0) return false;

        log.debug("DELETING COLONY: " + pp.getName() + " WITH ID " + pp.getPlanetId());

        /*
        // Delete all constructions
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("planetId",pp.getPlanetId()));

        // Check for buildings using constructionInterfaces and call destruction
        try {
            for (PlanetConstruction pc : pcDAO.findByPlanetId(pp.getPlanetId())) {
                ConstructionRestriction cr = null;
                cr = crDAO.getConstructionRestrictionBy(ConstructionRestriction.TYPE_BUILDING, pc.getConstructionId());

                if (cr != null) {
                    Class rc = Class.forName("at.darkdestiny.core.construction.restriction." + cr.getRestrictionClass());
                    ConstructionInterface cir = (ConstructionInterface) rc.newInstance();
                    cir.onDestruction(pp);
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on Construction Restriction: ", e);
        }

        pcDAO.removeAll(qks);
        ptDAO.removeAll(qks);

        // Delete ressources
        for (Ressource r : RessourceService.getAllStorableRessources()) {
            uds.removePlanetRessEntry(pp.getPlanetId(), EPlanetRessourceType.INSTORAGE, r.getId());
        }

        ppDAO.remove(pp);
        */

        DestructionUtilities.destroyPlayerPlanet(pp.getPlanetId());
        HeaderBuffer.reloadUser(pp.getUserId());

        // Create a deletion PlanetLog Entry
        PlanetLog pl = new PlanetLog();
        pl.setPlanetId(pp.getPlanetId());
        pl.setUserId(pp.getUserId());
        pl.setTime(time);
        pl.setType(EPlanetLogType.ABANDONED);
        plDAO.add(pl);

        Planet p = pDAO.findById(pp.getPlanetId());

        ViewTableUtilities.addOrRefreshSystem(pp.getUserId(), p.getSystemId(), time);


        return true;
    }
}
