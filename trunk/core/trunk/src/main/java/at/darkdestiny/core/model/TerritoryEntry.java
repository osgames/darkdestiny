/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "territoryentry")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TerritoryEntry extends Model<TerritoryEntry> {

    @FieldMappingAnnotation(value = "combatRoundId")
    @IdFieldAnnotation
    private Integer combatRoundId;
    @FieldMappingAnnotation(value = "userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation(value = "territorySize")
    private Double territorySize;
    @FieldMappingAnnotation(value = "territoryChange")
    private Double change;

    /**
     * @return the combatRoundId
     */
    public Integer getCombatRoundId() {
        return combatRoundId;
    }

    /**
     * @param combatRoundId the combatRoundId to set
     */
    public void setCombatRoundId(Integer combatRoundId) {
        this.combatRoundId = combatRoundId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the territorySize
     */
    public Double getTerritorySize() {
        return territorySize;
    }

    /**
     * @param territorySize the territorySize to set
     */
    public void setTerritorySize(Double territorySize) {
        this.territorySize = territorySize;
    }

    /**
     * @return the change
     */
    public Double getChange() {
        return change;
    }

    /**
     * @param change the change to set
     */
    public void setChange(Double change) {
        this.change = change;
    }
}
