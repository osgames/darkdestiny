/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.development.conditions;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.BonusDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.Bonus;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class AdministrativeColony implements IBonusCondition {
    private static BonusDAO bDAO = (BonusDAO)DAOFactory.get(BonusDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);
    
    @Override
    public boolean isPossible(int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        boolean hasGovernment = pcDAO.isConstructed(planetId, Construction.ID_PLANETGOVERNMENT);
        
        if (pp.getPopulation() < 2000000000l) return false;
        if (!hasGovernment) return false;        
        
        return true;
    }
    
    @Override
    public boolean isPossible() {
        return false;
    }
    
    @Override 
    public ArrayList<String> activeBoni(int bonusId, int planetId, int userId) {
        Bonus b = bDAO.findById(bonusId);
        
        ArrayList<String> boniList = new ArrayList<String>();
                
        boniList.add("["+ML.getMLStr(b.getName(),userId)+"] Erh�hte Loyalit�t auf diesem und umliegenden Planeten");
        
        return boniList;
    }
    
    @Override 
    public void onActivation(int bonusId, int planetId, int userId) {
        
    }    
}
