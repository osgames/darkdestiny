/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.movable;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.dao.FleetDetailDAO;
import at.darkdestiny.core.dao.FleetFormationDAO;
import at.darkdestiny.core.dao.FleetOrderDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.fleet.FleetType;
import at.darkdestiny.core.fleet.LoadingInformation;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.FleetDetail;
import at.darkdestiny.core.model.FleetFormation;
import at.darkdestiny.core.model.FleetOrder;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class FleetFormationExt implements IFleet {

    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static FleetOrderDAO foDAO = (FleetOrderDAO) DAOFactory.get(FleetOrderDAO.class);
    private static FleetFormationDAO ffDAO = (FleetFormationDAO) DAOFactory.get(FleetFormationDAO.class);
    private final FleetFormation base;
    private final FleetDetail moveData;
    private int ETA = 0;
    private boolean ableToCallBack = true;
    private Double speed = null;
    private Integer range = null;
    private LoadingInformation loadInfo = null;
    private ArrayList<ShipData> shipList = null;
    private at.darkdestiny.core.model.FleetOrder fo = null;
    private final ArrayList<PlayerFleetExt> participatingFleets = new ArrayList<PlayerFleetExt>();

    public FleetFormationExt(int id) throws Exception {
        base = ffDAO.findById(id);
        ArrayList<PlayerFleet> pfList = pfDAO.getParticipatingFleets(base);

        for (PlayerFleet pf : pfList) {
            participatingFleets.add(new PlayerFleetExt(pf.getId()));
        }

        // Check consistency of fleet formation
        FleetDetail fdLast = null;
        FleetDetail fdAct = null;
        PlayerFleetExt pfeLast = null;

        for (PlayerFleetExt pfe : participatingFleets) {
            fdAct = fdDAO.findByFleetId(pfe.getBase().getId());
            if (fdLast == null) {
                fdLast = fdAct;
            } else {
                if ((fdAct.getStartX().compareTo(fdLast.getStartX()) != 0)
                        || (fdAct.getStartY().compareTo(fdLast.getStartY()) != 0)
                        || (fdAct.getDestX().compareTo(fdLast.getDestX()) != 0)
                        || (fdAct.getDestY().compareTo(fdLast.getDestY()) != 0)
                        || (fdAct.getStartTime().compareTo(fdLast.getStartTime()) != 0)
                        || (fdAct.getFlightTime().compareTo(fdLast.getFlightTime()) != 0)) {
                    throw new Exception("Inconsistent FleetFormation [FleetDetails] (" + id + ")");
                }
            }

            if (pfeLast == null) {
                pfeLast = pfe;
            } else {
                if ((pfe.getBase().getPlanetId().compareTo(pfeLast.getBase().getPlanetId()) != 0)
                        || (pfe.getBase().getSystemId().compareTo(pfeLast.getBase().getSystemId()) != 0)
                        || (pfe.getBase().getStatus().compareTo(pfeLast.getBase().getStatus()) != 0)) {
                    throw new Exception("Inconsistent FleetFormation [PlayerFleet] (" + id + ") >> "
                            + pfe.getBase().getPlanetId() + "<>" + pfeLast.getBase().getPlanetId() + " "
                            + pfe.getBase().getSystemId() + "<>" + pfeLast.getBase().getSystemId() + " "
                            + pfe.getBase().getStatus() + "<>" + pfeLast.getBase().getStatus());
                }
            }
        }

        moveData = fdLast;

        if (moveData != null) {
            getBase().setStatus(0);
        } else {
            getBase().setStatus(1);
        }

        getBase().setPlanetId(pfeLast.getBase().getPlanetId());
        getBase().setSystemId(pfeLast.getBase().getSystemId());

        if (moveData != null) {
            ETA = moveData.getFlightTime() - (GameUtilities.getCurrentTick2() - moveData.getStartTime());
            if ((moveData.getStartPlanet() == 0) && (moveData.getStartSystem() == 0)) {
                ableToCallBack = false;
            }
        } else {
            ableToCallBack = false;
        }
    }

    @Override
    public Integer getRange() {
        // DebugBuffer.trace("Get FF Range");
        
        if (range == null) {
            int rangeTmp = Integer.MAX_VALUE;
            
            for (PlayerFleetExt pfe : participatingFleets) {
                ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(pfe.getId());
                if (sfList.isEmpty()) {
                    rangeTmp = 0;
                }
                for (ShipFleet sf : sfList) {
                    ShipDesign sd = sdDAO.findById(sf.getDesignId());
                    if (sd == null) {
                        DebugBuffer.warning("Invalid design (" + sf.getDesignId() + ") found for shipfleet entry " + sf.getId() + " in fleet " + this.base.getName() + " (" + this.base.getId() + ")");
                        continue;
                    }

                    // DebugBuffer.trace("Range for Shipdesign " + sd.getName() + " is " + sd.getRange());
                    rangeTmp = Math.min(rangeTmp, sd.getRange());
                }
            }

            range = rangeTmp;
        }

        // DebugBuffer.trace("Return range: " + range);
        return range;
    }
    /*
     public LoadingInformation getLoadingInformation() {
     if (loadInfo == null) {
     loadInfo = new LoadingInformation(getBase());
     }

     return loadInfo;
     }
     */
    
    @Override
    public boolean isScanning() {
        return (getScanDuration() > 0);
    }

    @Override
    public int getScanDuration() {
        Integer duration = FleetService.findRunningScan(base.getUserId(), base.getId());
        if (duration == null) {
            return 0;
        } else {
            return duration;
        }
    }

    @Override
    public boolean canScanSystem() {
        return FleetService.canScanSystem(this);
    }

    @Override
    public boolean canScanPlanet() {
        return FleetService.canScanPlanet(this);
    }

    @Override
    public AbsoluteCoordinate getAbsoluteCoordinate() {
        if (isMoving()) {
            int currTime = GameUtilities.getCurrentTick2();
            double distanceDone = (int) Math.floor(100d / moveData.getFlightTime() * (currTime - moveData.getStartTime()));

            int diffX = moveData.getDestX() - moveData.getStartX();
            int diffY = moveData.getDestY() - moveData.getStartY();

            int currX = moveData.getStartX() + (int) Math.floor(diffX / 100d * distanceDone);
            int currY = moveData.getStartY() + (int) Math.floor(diffY / 100d * distanceDone);

            return new AbsoluteCoordinate(currX, currY);
        } else {
            RelativeCoordinate rc = new RelativeCoordinate(getBase().getSystemId(), getBase().getPlanetId());
            return rc.toAbsoluteCoordinate();
        }
    }

    @Override
    public RelativeCoordinate getRelativeCoordinate() {
        if (isMoving()) {
            return null;
        }

        return new RelativeCoordinate(getBase().getSystemId(), getBase().getPlanetId());
    }

    @Override
    public double getSpeed() {
        if (speed == null) {
            double speedTmp = 999d;

            ArrayList<ShipFleet> sfList = new ArrayList<ShipFleet>();
            for (PlayerFleetExt pf : getParticipatingFleets()) {
                sfList.addAll(sfDAO.findByFleetId(pf.getBase().getId()));
            }
            if (sfList.isEmpty()) {
                speedTmp = 0d;
            }

            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());
                if (sd == null) {
                    DebugBuffer.error("SD was null for Formation " + this.getBase().getId() + " Shipfleet Entry " + sf.getId() + " and Design Entry " + sf.getDesignId());
                } else {
                    speedTmp = Math.min(speedTmp, sd.getHyperSpeed());
                }
            }

            speed = speedTmp;
        }

        return speed;
    }

    @Override
    public boolean isMoving() {
        return (moveData != null);
    }

    @Override
    public AbsoluteCoordinate getTargetAbsoluteCoordinate() {
        if (!isMoving()) {
            return null;
        }

        return new AbsoluteCoordinate(moveData.getDestX(), moveData.getDestY());
    }

    @Override
    public RelativeCoordinate getTargetRelativeCoordinate() {
        if (!isMoving()) {
            return null;
        }

        return new RelativeCoordinate(moveData.getDestSystem(), moveData.getDestPlanet());
    }

    @Override
    public AbsoluteCoordinate getSourceAbsoluteCoordinate() {
        if (!isMoving()) {
            return null;
        }

        return new AbsoluteCoordinate(moveData.getStartX(), moveData.getStartY());
    }

    @Override
    public RelativeCoordinate getSourceRelativeCoordinate() {
        if (!isMoving()) {
            return null;
        }

        return new RelativeCoordinate(moveData.getStartSystem(), moveData.getStartPlanet());
    }

    public FleetFormation getBase() {
        return base;
    }

    @Override
    public int getETA() {
        return ETA;
    }

    @Override
    public boolean isAbleToCallBack() {
        return ableToCallBack;
    }

    @Override
    public ArrayList<ShipData> getShipList() {
        if (shipList == null) {
            shipList = new ArrayList<ShipData>();

            for (PlayerFleetExt pf : getParticipatingFleets()) {
                ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(pf.getId());
                for (ShipFleet sf : sfList) {
                    ShipDesign sd = sdDAO.findById(sf.getDesignId());
                    if (sd == null) {
                        DebugBuffer.error("SD was null for Formation " + this.getBase().getId() + " Shipfleet Entry " + sf.getId() + " and Design Entry " + sf.getDesignId());
                        continue;
                    }

                    ShipData sda = new ShipData();
                    sda.setId(sf.getId());
                    sda.setDesignId(sd.getId());
                    sda.setDesignName(sd.getName());
                    sda.setFleetId(base.getId());
                    sda.setFleetName(base.getName());
                    sda.setCount(sf.getCount());
                    sda.setChassisSize(sd.getChassis());
                    sda.setLoaded(false);

                    shipList.add(sda);
                }
            }
        }

        return shipList;
    }

    public FleetOrder getFleetOrder() {
        if (fo == null) {
            fo = foDAO.get(base.getId(), FleetType.FLEET_FORMATION);
        }

        return fo;
    }

    public ArrayList<PlayerFleetExt> getParticipatingFleets() {
        return participatingFleets;
    }

    @Override
    public int getId() {
        return base.getId();
    }

    @Override
    public String getName() {
        return base.getName();
    }

    @Override
    public int getUserId() {
        return base.getUserId();
    }

    @Override
    public boolean canHyperJump() {
        throw new UnsupportedOperationException("please implement");
    }

    @Override
    public boolean canFlyInterstellar() {
        for (ShipData sd : getShipList()) {
            if (sd.getChassisSize() == Chassis.ID_FIGHTER
                    || sd.getChassisSize() == Chassis.ID_CORVETTE) {
                return false;
            }
        }
        return true;
    }

    @Override
    public ELocationType getLocationType() {
        if (base.getPlanetId() > 0) {
            return ELocationType.PLANET;
        } else if (base.getPlanetId() == 0 && base.getSystemId() > 0) {
            return ELocationType.PLANET;
        } else {
            return ELocationType.TRANSIT;
        }
    }

    @Override
    public Integer getLocationId() {
        if (base.getPlanetId() > 0) {
            return base.getPlanetId();
        } else if (base.getPlanetId() == 0 && base.getSystemId() > 0) {
            return base.getSystemId();
        } else {
            return 0;
        }
    }
}
