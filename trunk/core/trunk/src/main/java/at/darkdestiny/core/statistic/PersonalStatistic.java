package at.darkdestiny.core.statistic;

import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.PlayerStatisticDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.model.PlayerStatistic;
import at.darkdestiny.core.model.User;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedDomainCategoryPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.GroupedStackedBarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * A simple demonstration application showing how to create a stacked bar chart
 * using data from a {@link CategoryDataset}.
 *
 */
public class PersonalStatistic {

    private static final Logger log = LoggerFactory.getLogger(PersonalStatistic.class);

    private static PlayerStatisticDAO psDAO = (PlayerStatisticDAO) DAOFactory.get(PlayerStatisticDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static UserDataDAO userDataDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static TreeMap<Long, PlayerStatistic> playerStats;
    private static ArrayList<PlayerStatistic> dataArray;
    private static DefaultCategoryDataset result;
    private static DefaultCategoryDataset result2;
    private static JFreeChart chart;
    private static final int MAX_BARS = 31;
    private int days;
    private static int userId;
    private static int lowestRank = 0;

    /**
     * @return the chart
     */
    public static JFreeChart getChart() {
        return chart;
    }

    /**
     * Creates a new demo.
     *
     * @param title  the frame title.
     */
    public PersonalStatistic(Integer userId1, int days) {
        // super("Statistic");
        lowestRank = 0;

        try {

            result = new DefaultCategoryDataset();
            result2 = new DefaultCategoryDataset();

            this.days = days;
            userId = userId1;
            playerStats = new TreeMap<Long, PlayerStatistic>();
            for (PlayerStatistic ps : psDAO.findByUserId(userId)) {
                playerStats.put(ps.getDate(), ps);
            }

            int i = 0;
            for (Iterator<Long> it = playerStats.descendingKeySet().iterator();it.hasNext();) {
                i++;

                long value = it.next();

                if (i > days) {
                    Date d = new Date(value);
                    it.remove();
                }
            }

            dataArray = new ArrayList<PlayerStatistic>();
            TreeMap<Long, PlayerStatistic> playerStats2 = new TreeMap<Long, PlayerStatistic>();

            int skipValue = 0;
            boolean skip = false;

            if (days > MAX_BARS) {
                if(days == 9999){
                    days = playerStats.size();
                }

                skipValue = (int)Math.ceil((double)days / (double)MAX_BARS);
                skip = true;
            }

            int value = 0;
            i = 0;
            for (Map.Entry<Long, PlayerStatistic> entry : playerStats.descendingMap().entrySet()) {
                if (skip) {
                    if (i == value) {
                        log.debug(i + " == " + value);
                        playerStats2.put(entry.getKey(), entry.getValue());
                        value += skipValue;
                    }
                } else {
                    playerStats2.put(entry.getKey(), entry.getValue());

                    Date d = new Date(entry.getKey());
                    if (i == days) {
                        break;
                    }
                }

                i++;
            }

            dataArray.addAll(playerStats2.values());
            createDataset();
            chart = createChart();
            if (chart == null) {
                DebugBuffer.trace("Statistic chart could not be created for user " + userId1 + " [Days: " + days + "]");
                return;
            }
            final ChartPanel chartPanel = new ChartPanel(chart);
            chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
            // setContentPane(chartPanel);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Personal Statistic", e);
        }

    }

    /**
     * Creates a sample dataset.
     *
     * @return a sample dataset.
     */
    private void createDataset() {
        createCategoryDataset();
    }

    public static void createCategoryDataset() {
        result = new DefaultCategoryDataset();
        result2 = new DefaultCategoryDataset();
        for (int i = 0; i < dataArray.size(); i++) {
            PlayerStatistic ps = dataArray.get(i);
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(ps.getDate());
            String date = "";
            int day = gc.get(GregorianCalendar.DAY_OF_MONTH);
            if (day < 10) {
                date += "0" + day;
            } else {
                date += day;
            }
            date += "-";
            int month = gc.get(GregorianCalendar.MONTH);
            month++;
            if (month < 10) {
                date += "0" + month;
            } else {
                date += month;
            }

            if (lowestRank < ps.getRank()) {
                lowestRank = ps.getRank();
            }

            //date += "-" + gc.get(GregorianCalendar.YEAR);
            result2.setValue(ps.getRank(), ML.getMLStr("statistic_chart_lbl_rank", userId), date);
            result.setValue((double) ps.getMilitaryPoints(), ML.getMLStr("statistic_chart_lbl_military", userId), date);
            result.setValue((double) ps.getPopulationPoints(), ML.getMLStr("statistic_chart_lbl_population", userId), date);
            result.setValue((double) ps.getResearchPoints(), ML.getMLStr("statistic_chart_lbl_research", userId), date);
            result.setValue((double) ps.getRessourcePoints(), ML.getMLStr("statistic_chart_lbl_ressources", userId), date);
            /* data[0][i] = ps.getMilitaryPoints();
            data[1][i] = ps.getPopulationPoints();
            data[2][i] = ps.getResearchPoints();
            data[3][i] = ps.getRessourcePoints();
            data[4][i] = ps.getRank();*/
        }
    }

    private JFreeChart createChart() {
        GroupedStackedBarRenderer renderer = new GroupedStackedBarRenderer();
        final NumberAxis rangeAxis1 = new NumberAxis(ML.getMLStr("statistic_chart_lbl_points", userId));
        final CategoryPlot subplot1 = new CategoryPlot(result, null, rangeAxis1,
                renderer);


        CategoryItemRenderer renderer2 = new LineAndShapeRenderer();
        final NumberAxis rangeAxis2 = new NumberAxis(ML.getMLStr("statistic_chart_lbl_rank", userId));
        rangeAxis2.setInverted(true);
        rangeAxis2.setAutoRange(false);

        if (lowestRank == 0) return null;
        if (lowestRank == 1) lowestRank = 2;

        // Optimize Range
        double tickIntervall = 1d;

        if (lowestRank > 10) {
            int timesOver10 = (int)Math.ceil(lowestRank / 10d);

            int mod = lowestRank % timesOver10;
            lowestRank += mod;

            tickIntervall = timesOver10;
        }

        rangeAxis2.setTickUnit(new NumberTickUnit(tickIntervall));
        rangeAxis2.setRange(1, lowestRank);

        final CategoryPlot subplot2 = new CategoryPlot(result2, null, rangeAxis2,
                renderer2);

        final CombinedDomainCategoryPlot plot = new CombinedDomainCategoryPlot();
        plot.setGap(15);
        plot.add(subplot2);
        plot.add(subplot1);
        CategoryAxis xAxis = (CategoryAxis) plot.getDomainAxis();
        xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);

        User user = uDAO.findById(userId);
        String date = days + " " + ML.getMLStr("statistic_lbl_days", userId);
        if (days == 31) {
            date = ML.getMLStr("statistic_link_1month", userId);
        } else if (days == 93) {
            date = ML.getMLStr("statistic_link_3month", userId);
        } else if (days == 9999) {
            date = ML.getMLStr("statistic_link_all", userId);
        }
        chart = new JFreeChart(
                ML.getMLStr("statistic_lbl_history", userId) + " - " + user.getGameName() + " - " + date,
                new Font("SansSerif", Font.BOLD, 12),
                plot,
                true);

        return chart;
    }

    public static void RankFix() {
        TreeMap<Long, TreeMap<Long, ArrayList<PlayerStatistic>>> allStats = new TreeMap<Long, TreeMap<Long, ArrayList<PlayerStatistic>>>();

        for (PlayerStatistic ps : (ArrayList<PlayerStatistic>) psDAO.findAll()) {
            TreeMap<Long, ArrayList<PlayerStatistic>> dayStats = allStats.get(ps.getDate());
            if (dayStats == null) {
                dayStats = new TreeMap<Long, ArrayList<PlayerStatistic>>();
            }
            ArrayList<PlayerStatistic> points = dayStats.get(ps.getSum());
            if (points == null) {
                points = new ArrayList<PlayerStatistic>();
            }

            points.add(ps);
            dayStats.put(ps.getSum(), points);
            allStats.put(ps.getDate(), dayStats);
        }

        for (Map.Entry<Long, TreeMap<Long, ArrayList<PlayerStatistic>>> entry : allStats.descendingMap().entrySet()) {
            //ENtries of a specific Day
            TreeMap<Long, ArrayList<PlayerStatistic>> day = entry.getValue();
            int rank = 1;
            for (Map.Entry<Long, ArrayList<PlayerStatistic>> entry2 : day.descendingMap().entrySet()) {
                ArrayList<PlayerStatistic> tmp = entry2.getValue();
                for (PlayerStatistic stats : tmp) {
                    //Statistic for statistic page
                    stats.setRank(rank);

                    //Entries for historyanalyzation
                    psDAO.update(stats);
                }
                rank++;
            }
        }
    }
}
