package at.darkdestiny.core.statistic;

import at.darkdestiny.core.dao.PlayerStatisticDAO;
import at.darkdestiny.core.dao.StatisticDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.exceptions.MissingDataException;
import at.darkdestiny.core.model.PlayerStatistic;
import at.darkdestiny.core.model.Statistic;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * update the statistics for all Users
 *
 * @author martin
 */
public class UserStatisticsUpdater extends Thread implements Runnable {

    private static StatisticDAO statisticDAO = (StatisticDAO) DAOFactory.get(StatisticDAO.class);
    private static UserDataDAO userDataDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static PlayerStatisticDAO playerStatisticDAO = (PlayerStatisticDAO) DAOFactory.get(PlayerStatisticDAO.class);
    private static UserDAO userDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    public UserStatisticsUpdater() {
        this.setPriority(MIN_PRIORITY);
    }

    @Override
    public void run() {
        Map<Integer, Integer> oldRanks = loadOldStatistics();
        statisticDAO.removeAll();
        TreeMap<Long, ArrayList<PlayerStatistic>> allStats = new TreeMap<Long, ArrayList<PlayerStatistic>>();

        for (UserData ud : (ArrayList<UserData>) userDataDAO.findAll()) {
            User u = userDAO.findById(ud.getUserId());            
            
            try {
                if (!u.getUserConfigured()) continue;
                Statistik stat = new Statistik();
                PlayerStatistic ps = stat.calcStatistic(ud.getUserId(), Statistik.WRITE_TO_DB_NO);

                // This case happens on error or if the user has no planets left
                if (ps == null) {
                    continue;
                }

                ArrayList<PlayerStatistic> rank = allStats.get(ps.getSum());
                if (rank == null) {
                    rank = new ArrayList<PlayerStatistic>();
                }
                rank.add(ps);
                allStats.put(ps.getSum(), rank);
            } catch (Exception e) {
                DebugBuffer.addLine(e);
            }
        }

        int rank = 1;
        long date = System.currentTimeMillis();
        GregorianCalendar result = new GregorianCalendar();
        result.setTimeInMillis(date);

        // result.set(GregorianCalendar.HOUR_OF_DAY, 0);
        // result.set(GregorianCalendar.MINUTE, 0);
        result.set(GregorianCalendar.SECOND, 0);
        result.set(GregorianCalendar.MILLISECOND, 0);

        for (Map.Entry<Long, ArrayList<PlayerStatistic>> entry : allStats.descendingMap().entrySet()) {
            ArrayList<PlayerStatistic> tmp = entry.getValue();
            for (PlayerStatistic stats : tmp) {
                //Statistic for statistic page
                Statistic s = new Statistic();
                s.setRank(rank);
                stats.setRank(rank);
                Integer oldRank = oldRanks.get(stats.getUserId());

                if (oldRank == null) {
                    oldRank = 0;
                }
                s.setLastRank(oldRank);
                s.setPoints(Math.round(stats.getSum()));
                s.setUserId(stats.getUserId());
                s.setUserName("Should not be used");
                statisticDAO.add(s);
                //Entries for historyanalyzation
                stats.setDate(result.getTimeInMillis());
                playerStatisticDAO.add(stats);
            }
            rank++;
        }


    }

    private Map<Integer, Integer> loadOldStatistics() {
        Map<Integer, Integer> result = new HashMap<Integer, Integer>();
        for (Statistic s : (ArrayList<Statistic>) statisticDAO.findAll()) {
            result.put(s.getUserId(), s.getRank());
        }

        return result;
    }

    public boolean isInTrial(int userId)
            throws MissingDataException {

        User u = userDAO.findById(userId);
        return u.getTrial();
    }
}
