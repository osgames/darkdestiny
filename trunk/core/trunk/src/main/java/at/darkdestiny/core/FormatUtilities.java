/*
 * Utilities.java
 *
 * Created on 11. M�rz 2003, 20:25
 */
package at.darkdestiny.core;

import at.darkdestiny.util.DebugBuffer;
import java.text.*;
import java.util.ArrayList;
import java.util.Locale;

/**
 *
 * @author Rayden
 */
public final class FormatUtilities {

    private FormatUtilities() {
        // Empty constructor for utility class
    }

    public static String getFormattedNumber(long number) {
        NumberFormat nf = NumberFormat.getInstance();
        return nf.format(number);
    }

    public static String getFormattedNumber(long number, Locale l) {
        NumberFormat nf = NumberFormat.getInstance(l);
        return nf.format(number);
    }

    public static String getFormattedNumber(double number) {
        NumberFormat nf = NumberFormat.getInstance();
        return nf.format(number);
    }

    public static String getFormattedNumber(double number, Locale l) {
        NumberFormat nf = NumberFormat.getInstance(l);
        return nf.format(number);
    }

    public static String getFormattedDecimal(double number, int precision, Locale l) {
        try {
            NumberFormat nf = NumberFormat.getIntegerInstance(l);
            nf.setMaximumFractionDigits(precision);
            nf.setMinimumFractionDigits(precision);
            return nf.format(number);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Format failed: ", e);
        }

        return "?";
    }

    public static String getFormattedDecimal(double number, int precision) {
        try {
            NumberFormat nf = NumberFormat.getIntegerInstance();
            nf.setMaximumFractionDigits(precision);
            nf.setMinimumFractionDigits(precision);
            return nf.format(number);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Format failed: ", e);
        }

        return "?";
    }

    public static String getFormattedDecimalCutEndZero(double number, int precision, Locale l) {
        try {
            NumberFormat nf = NumberFormat.getIntegerInstance(l);
            nf.setMaximumFractionDigits(precision);
            nf.setMinimumFractionDigits(precision);
            String tmpResult = nf.format(number);

            char zero = "0".charAt(0);
            char decimal = ",".charAt(0);

            while (tmpResult.charAt(tmpResult.length() - 1) == zero) {
                tmpResult = tmpResult.substring(0, tmpResult.length() - 1);
            }

            if (tmpResult.charAt(tmpResult.length() - 1) == decimal) {
                tmpResult = tmpResult.substring(0, tmpResult.length() - 1);
            }

            return tmpResult;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Format failed: ", e);
        }

        return "?";
    }

    public static String getFormattedDecimalCutEndZero(double number, int precision) {
        try {
            NumberFormat nf = NumberFormat.getIntegerInstance();
            nf.setMaximumFractionDigits(precision);
            nf.setMinimumFractionDigits(precision);
            String tmpResult = nf.format(number);

            char zero = "0".charAt(0);
            char decimal = ",".charAt(0);

            while (tmpResult.charAt(tmpResult.length() - 1) == zero) {
                tmpResult = tmpResult.substring(0, tmpResult.length() - 1);
            }

            if (tmpResult.charAt(tmpResult.length() - 1) == decimal) {
                tmpResult = tmpResult.substring(0, tmpResult.length() - 1);
            }

            return tmpResult;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Format failed: ", e);
        }

        return "?";
    }

    private static String getFormatScaledNumber(long number, Object... Units) {
        long numbersave = number;
        int exp = 0; //Exponent / 3
        int exp_save = 0;
        while (number > 9999) {
            exp++;
            number /= 1000;
            //Wenn das dargestellt werden soll
            if (Units[exp] != null) {
                numbersave = number;
                exp_save = exp;
            }
        }

        return getFormattedNumber(numbersave) + Units[exp_save];
    }

    private static String getFormatScaledNumber(long number, Locale l, Object... Units) {
        long numbersave = number;
        int exp = 0; //Exponent / 3
        int exp_save = 0;
        while (number > 9999) {
            exp++;
            number /= 1000;
            //Wenn das dargestellt werden soll
            if (Units[exp] != null) {
                numbersave = number;
                exp_save = exp;
            }
        }

        return getFormattedNumber(numbersave, l) + Units[exp_save];
    }

    private static String getFormatScaledNumber(double number, int precision, Object... Units) {
        boolean ignorePrecision = true;

        double numbersave = number;
        int exp = 0; //Exponent / 3
        int exp_save = 0;
        while (number > 9999) {
            exp++;
            if (exp >= 3) {
                ignorePrecision = false;
            }
            number /= 1000;
            //Wenn das dargestellt werden soll
            if (Units[exp] != null) {
                numbersave = number;
                exp_save = exp;
            }
        }

        if (ignorePrecision) {
            return getFormattedDecimal(numbersave, 0) + Units[exp_save];
        } else {
            return getFormattedDecimal(numbersave, precision) + Units[exp_save];
        }
    }

    private static String getFormatScaledNumber(double number, int precision, Locale l, Object... Units) {
        boolean ignorePrecision = true;

        double numbersave = number;
        int exp = 0; //Exponent / 3
        int exp_save = 0;
        while (number > 9999) {
            exp++;
            if (exp >= 3) {
                ignorePrecision = false;
            }
            number /= 1000;
            //Wenn das dargestellt werden soll
            if (Units[exp] != null) {
                numbersave = number;
                exp_save = exp;
            }
        }

        if (ignorePrecision) {
            return getFormattedDecimal(numbersave, 0, l) + Units[exp_save];
        } else {
            return getFormattedDecimal(numbersave, precision, l) + Units[exp_save];
        }
    }

    public static String getFormatScaledNumber(long number, int valueType) {
        if (valueType == GameConstants.VALUE_TYPE_NORMAL) {
            return getFormatScaledNumber(number, "", null, " Mio.", " Mrd.");
        } else if (valueType == GameConstants.VALUE_TYPE_ENERGY) {
            return getFormatScaledNumber(number, " W", null, " MW", " GW");
        } else if (valueType == GameConstants.VALUE_TYPE_INTONS) {
            return getFormatScaledNumber(number, " T.", null, " Mio. T.", " Mrd. T.");
        } else if (valueType == 99) {
            return getFormatScaledNumber(number, " Bytes", " kB", " MB", " GB");
        }

        return ("" + number);
    }

    public static String getFormatScaledNumber(long number, int valueType, Locale l) {
        if (valueType == GameConstants.VALUE_TYPE_NORMAL) {
            return getFormatScaledNumber(number, l, "", null, " " + ML.getMLStr("numberformat_million", l), " " + ML.getMLStr("numberformat_billion", l));
        } else if (valueType == GameConstants.VALUE_TYPE_ENERGY) {
            return getFormatScaledNumber(number, l, " W", null, " MW", " GW");
        } else if (valueType == GameConstants.VALUE_TYPE_INTONS) {
            return getFormatScaledNumber(number, l, " T.", null, " " + ML.getMLStr("numberformat_million", l) + " " + ML.getMLStr("numberformat_tons", l), " " + ML.getMLStr("numberformat_billion", l) + " " + ML.getMLStr("numberformat_tons", l));
        } else if (valueType == 99) {
            return getFormatScaledNumber(number, l, " Bytes", " kB", " MB", " GB");
        }

        return ("" + number);
    }

    public static String getFormatScaledNumber(double number, int precision, int valueType) {
        if (valueType == GameConstants.VALUE_TYPE_NORMAL) {
            return getFormatScaledNumber(number, precision, "", null, " Mio.", " Mrd.");
        } else if (valueType == GameConstants.VALUE_TYPE_ENERGY) {
            return getFormatScaledNumber(number, precision, " W", null, " MW", " GW");
        } else if (valueType == GameConstants.VALUE_TYPE_INTONS) {
            return getFormatScaledNumber(number, precision, " T.", null, " Mio. T.", " Mrd. T.");
        } else if (valueType == 99) {
            return getFormatScaledNumber(number, precision, " Bytes", " kB", " MB", " GB");
        }

        return ("" + number);
    }

    public static String getFormatScaledNumber(double number, int precision, int valueType, Locale l) {
        if (valueType == GameConstants.VALUE_TYPE_NORMAL) {
            return getFormatScaledNumber(number, precision, l, "", null, " " + ML.getMLStr("numberformat_million", l), " " + ML.getMLStr("numberformat_billion", l));
        } else if (valueType == GameConstants.VALUE_TYPE_ENERGY) {
            return getFormatScaledNumber(number, precision, l, " W", null, " MW", " GW");
        } else if (valueType == GameConstants.VALUE_TYPE_INTONS) {
            return getFormatScaledNumber(number, precision, l, " T.", null, " " + ML.getMLStr("numberformat_million", l) + " " + ML.getMLStr("numberformat_tons", l), " " + ML.getMLStr("numberformat_billion", l) + " " + ML.getMLStr("numberformat_tons", l));
        } else if (valueType == 99) {
            return getFormatScaledNumber(number, precision, l, " Bytes", " kB", " MB", " GB");
        }

        return ("" + number);
    }

    public static String getFormatScaledNumberWithShare(long number, long max, int valueType) {
        String res = getFormatScaledNumber(number, valueType);
        return res + " (" + ((max != 0) ? NumberFormat.getInstance().format(0.1 * Math.floor(1000 * ((double) number) / (double) max)) : "0,0") + "%)";
    }

    public static String getColorFor(long currentValue, long available) {
        if (currentValue < available) {
            return "#33FF00";
        } else if (currentValue == available) {
            return "yellow";
        } else {
            return "red";
        }
    }

    public static String getColorFor(double currentValue, double available) {
        if (currentValue < available) {
            return "#33FF00";
        } else if (currentValue == available) {
            return "yellow";
        } else {
            return "red";
        }
    }

    public static String getColorFor(
            double currentValue, double greenMin, double yellowMin) {
        if (currentValue >= greenMin) {
            return "#33FF00";
        } else if (currentValue >= yellowMin) {
            return "yellow";
        } else {
            return "red";
        }
    }

    public static String escapeChars(String in) {
        ArrayList<String> ignoreText = new ArrayList<String>();

        boolean finished = false;

        String res = in.replace("[html}[/html]", "");

        while (!finished) {
            int startIndex = res.indexOf("[html]");
            int endIndex = res.indexOf("[/html]");

            if ((startIndex == -1) || (endIndex == -1)) {
                finished = true;
            } else {
                //  log.debug("SI: " + startIndex + " EI: " + endIndex);

                ignoreText.add(res.substring(startIndex + 6, endIndex));

                //   log.debug("Adding to ignore: " + res.substring(startIndex+6, endIndex));
                //   log.debug("Try to replace >>[html]"+ignoreText.get(ignoreText.size()-1)+"[/html]<< with >>["+ignoreText.size()+"]<<");

                res = res.replace("[html]" + ignoreText.get(ignoreText.size() - 1) + "[/html]", "[" + ignoreText.size() + "]");
            }
        }

        for (int i = 0; i < ignoreText.size(); i++) {
            res = res.replace("[" + (i + 1) + "]", ignoreText.get(i));
        }

        return res;
    }

    public static String toISO(String in) {
        String output = in.replace("�", "&auml;");
        output = output.replace("�", "&ouml;");
        output = output.replace("�", "&uuml;");
        output = output.replace("�", "&Auml;");
        output = output.replace("�", "&Ouml;");
        output = output.replace("�", "&Uuml;");
        output = output.replace("�", "&szlig;");

        return output;
    }

    public static String toISO2(String in) {
        String output = in.replace("&auml;","�");
        output = output.replace("&ouml;","�");
        output = output.replace("&uuml;","�");
        output = output.replace("&Auml;","�");
        output = output.replace("&Ouml;","�");
        output = output.replace("&Uuml;","�");
        output = output.replace("&szlig;","�");

        return output;
    }    
    
    public static String killJavaScript(String in) {
        String output = in.replaceAll("<script", "sgript");

        return output;
    }

    public static String getInfForZero(int value) {
        return (value == 0) ? "&infin;" : getFormattedNumber(value);
    }
}
