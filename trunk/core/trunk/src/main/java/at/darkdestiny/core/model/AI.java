/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.core.enumeration.EAIType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Admin
 */
@TableNameAnnotation(value = "ai")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class AI extends Model<AI> {

    public static final String FREE_WORLDS_AI_gamename = "ai_FREE_WORLDS_AI_gamename";

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, nullable=false)
    private Integer userId;
    @FieldMappingAnnotation("aiType")
    @DefaultValue("NORMAL")
    private EAIType aiType;

    /**
     * @return the aiType
     */
    public EAIType getAiType() {
        return aiType;
    }

    /**
     * @param aiType the aiType to set
     */
    public void setAiType(EAIType aiType) {
        this.aiType = aiType;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }


}
