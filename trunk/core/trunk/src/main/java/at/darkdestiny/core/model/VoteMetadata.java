/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.voting.MetaDataDataType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "votemetadata")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class VoteMetadata extends Model<VoteMetadata> {

    public static final String NAME_INITIAL_USER = "INITIALUSER";
    public static final String NAME_FROMID = "FROMID";
    public static final String NAME_TOID = "TOID";
    public static final String NAME_TYPE = "TYPE";
    public static final String NAME_SRCID = "SRCID";
    public static final String NAME_MESSAGE = "MESSAGE";
    public static final String NAME_DRT_TYPE = "DIPLORELATIONTYPE";
    @FieldMappingAnnotation("voteId")
    @IdFieldAnnotation
    private Integer voteId;
    @FieldMappingAnnotation("dataName")
    @IdFieldAnnotation
    private String dataName;
    @FieldMappingAnnotation("dataValue")
    @StringType("text")
    @ColumnProperties(length = 500)
    private String dataValue;
    @FieldMappingAnnotation("dataType")
    @DefaultValue("INTEGER")
    private MetaDataDataType dataType;

    /**
     * @return the voteId
     */
    public Integer getVoteId() {
        return voteId;
    }

    /**
     * @param voteId the voteId to set
     */
    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    /**
     * @return the dataName
     */
    public String getDataName() {
        return dataName;
    }

    /**
     * @param dataName the dataName to set
     */
    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    /**
     * @return the dataValue
     */
    public String getDataValue() {
        return dataValue;
    }

    /**
     * @param dataValue the dataValue to set
     */
    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

    /**
     * @return the dataType
     */
    public MetaDataDataType getDataType() {
        return dataType;
    }

    /**
     * @param dataType the dataType to set
     */
    public void setDataType(MetaDataDataType dataType) {
        this.dataType = dataType;
    }
}
