/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.service;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Note;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.result.BuildableResult;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.core.utilities.ConstructionUtilities;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class AdministrationService extends Service{

    public static final int NOTE_MAX_LENGTH = 2000;

    public static TreeMap<Integer, ArrayList<Integer>> findSystemsByUserId(int userId){


        ArrayList<PlayerPlanet> pps = playerPlanetDAO.findByUserId(userId);

        TreeMap<Integer, ArrayList<Integer>> result = new TreeMap<Integer, ArrayList<Integer>>();
        for(PlayerPlanet pp : pps){
            Planet p = planetDAO.findById(pp.getPlanetId());
            if(!result.containsKey(p.getSystemId())){
                if(result.get(p.getSystemId()) == null){
                    result.put(p.getSystemId(),new ArrayList<Integer>());
                }else{
                    result.get(p.getSystemId()).add(p.getId());
                }

            }
        }
        return result;
    }
    public static void changeNote(int userId, int planetId, String msg){
        Note n = noteDAO.findBy(userId, planetId);

        if (msg != null) {
            msg = FormatUtilities.killJavaScript(msg);
            msg = GenerateMessage.parseText(msg);

            if (msg.length() > NOTE_MAX_LENGTH) {
                msg = msg.substring(0, NOTE_MAX_LENGTH);
            }
        }

        if(n == null){
            n = new Note();
            n.setUserId(userId);
            n.setPlanetId(planetId);
            n.setMessage(msg);

            noteDAO.add(n);
        }else{
            n.setMessage(msg);
            noteDAO.update(n);
        }
    }

    public static Note findNote(int userId, int planetId){
        return noteDAO.findBy(userId, planetId);
    }

    public static TreeMap<EConstructionType, ArrayList<Construction>> getAllConstruction(int userId){
        TreeMap<EConstructionType, ArrayList<Construction>> result = new TreeMap<EConstructionType, ArrayList<Construction>>();
        TreeMap<EConstructionType, TreeMap<String, Construction>> orderedResult = new TreeMap<EConstructionType, TreeMap<String, Construction>>();
        for(Construction c : (ArrayList<Construction>)constructionDAO.findAll()){
            TreeMap<String, Construction> tmp = orderedResult.get(c.getType());
            if(tmp == null){
                tmp = new TreeMap<String, Construction>();
            }
            tmp.put(ML.getMLStr(c.getName(), userId), c);
            orderedResult.put(c.getType(), tmp);
        }
        for(Map.Entry<EConstructionType, TreeMap<String, Construction>> entry : orderedResult.entrySet()){
            ArrayList<Construction> tmp = result.get(entry.getKey());
            if(tmp == null){
                tmp = new ArrayList<Construction>();
            }
            tmp.addAll(entry.getValue().values());
            result.put(entry.getKey(), tmp);
        }
        return result;
    }

    public static TreeMap<EConstructionType, ProductionBuildResult> getConstructionList(int userId, int planetId) {

        ArrayList<Construction> cList = constructionDAO.findAll();
        TreeMap<EConstructionType, ProductionBuildResult> result = new TreeMap<EConstructionType, ProductionBuildResult>();
        for (Construction c : cList) {
            ProductionBuildResult pbr = result.get(c.getType());
            if(pbr == null){
                pbr = new ProductionBuildResult();
            }
            ConstructionExt ce = new ConstructionExt(c.getId());
            BuildableResult br = ConstructionUtilities.canBeBuilt(c, userId, planetId, 1);
            pbr.addProduction(ce, br);
            result.put(c.getType(), pbr);
        }

        return result;
    }
    public static ArrayList<Ressource> getMineableRessources(){
        return ressourceDAO.findAllMineableRessources();

    }

    public static ArrayList<Ressource> getStoreableRessources(){
        return ressourceDAO.findAllStoreableRessources();

    }

    public static PlayerPlanet findPlayerPlanet(int planetId){
        return playerPlanetDAO.findByPlanetId(planetId);
    }
    public static ArrayList<PlayerPlanet> findPlayerPlanetByUserId(int userId){
        return playerPlanetDAO.findByUserId(userId);
    }
        public static Planet findPlanet(int planetId){
        return planetDAO.findById(planetId);
    }
    public static at.darkdestiny.core.model.System findSystem(int systemId){
        return systemDAO.findById(systemId);
    }

}