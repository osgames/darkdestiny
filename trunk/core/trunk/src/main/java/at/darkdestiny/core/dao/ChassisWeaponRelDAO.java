/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ChassisWeaponRel;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class ChassisWeaponRelDAO extends ReadOnlyTable<ChassisWeaponRel> implements GenericDAO {
    public ChassisWeaponRel getByChassisAndModule(int chassisId, int moduleId) {
        ChassisWeaponRel cwr = new ChassisWeaponRel();
        cwr.setChassisId(chassisId);
        cwr.setModuleId(moduleId);
        return get(cwr);
    }
}
