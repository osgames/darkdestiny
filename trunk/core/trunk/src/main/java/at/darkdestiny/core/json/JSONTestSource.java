/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.json;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.GroundTroopDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.fleet.FleetData;
import at.darkdestiny.core.hangar.ShipTypeEntry;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.DiplomacyType;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.ships.ShipUtilities;
import at.darkdestiny.core.utilities.AllianceUtilities;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import at.darkdestiny.core.utilities.HangarUtilities;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class JSONTestSource {

    private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    // Build a datastructure containing all ship designs and hangarstructure

    public static String getFleetData(int fleetId) {
        String json = "{ ";

        FleetData fd = new FleetData(fleetId);
        fd.loadShipList();
        boolean firstEntry = true;

        try {
            Statement stmt = DbConnect.createStatement();

            for (ShipData sd : fd.getShipList()) {
                if (firstEntry) {
                    json += " \"ships\": [ ";
                }

                if (!firstEntry) {
                    json += ", ";
                }

                String chassisSize = "";
                chassisSize = ShipUtilities.getChassisName(sd.getDesignId());

                json += "{ \"name\": \"" + sd.getDesignName() + "\", \"count\": \"" + sd.getCount() + "\", \"hangarship\": \"false\" }";
                firstEntry = false;

                ArrayList<ShipTypeEntry> hangarLoading = HangarUtilities.getNestedHangarStructure(fd.getFleetId(), sd.getDesignId());
                if (!hangarLoading.isEmpty()) {
                    for (int i = 0; i < hangarLoading.size(); i++) {
                        ShipTypeEntry ste = (ShipTypeEntry) hangarLoading.get(i);
                        if (ste.getNestedLevel() == 0) {
                            continue;
                        }
                        String shipName = "";


                        ResultSet rs = stmt.executeQuery("SELECT name FROM shipdesigns WHERE id=" + ste.getId());
                        if (rs.next()) {
                            shipName = rs.getString(1);
                        }

                        json += ", ";
                        json += "{ \"name\": \"" + shipName + "\", \"count\": \"" + ste.getCount() + "\", \"hangarship\": \"true\" }";
                    }
                }

                firstEntry = false;
            }

            if (!firstEntry) {
                json += " ] ";
            }

            json += " }";
            stmt.close();
        } catch (Exception e) {
        }

        return json;
    }

    public static String getDefenseInformation(int systemId, int planetId, int userId) {
        String json = "{ ";

        // Retrieve all fleets
        try {
            // Load ships on planet
            /*
            ResultSet rs = stmt.executeQuery("SELECT sd.name, ships.shipTotal FROM (SELECT sf.designId, SUM(sf.count) as shipTotal FROM playerfleet pf, shipfleet sf WHERE sf.fleetId=pf.id AND pf.userId="+userId+" AND pf.planetId=" + planetId + " GROUP BY sf.designId) as ships " +
            "LEFT OUTER JOIN shipdesigns sd ON sd.id=ships.designId ORDER BY sd.chassis ASC");
             */
            //  ResultSet rs = stmt.executeQuery("SELECT sd.name, ships.shipTotal, ships.userId FROM (SELECT pf.userId, sf.designId, SUM(sf.count) as shipTotal FROM playerfleet pf, shipfleet sf WHERE sf.fleetId=pf.id AND pf.planetId=" + planetId + " GROUP BY pf.userId, sf.designId) as ships " +
            //         "LEFT OUTER JOIN shipdesigns sd ON sd.id=ships.designId ORDER BY sd.chassis ASC");

            boolean firstEntry = true;
            boolean firstEntryAllied = true;
            boolean firstEntryEnemy = true;

            /*
            String own = " \"own\": [ ";
            String allied = " \"allied\": [ ";
            String enemy = " \"enemy\": [ ";
             */

            String own = "";
            String allied = "";
            String enemy = "";

            boolean scannerPhalanxFound = false;

            ArrayList<Integer> sharingUsers = DiplomacyUtilities.findSharingUsers(userId);
            for (Planet p : pDAO.findBySystemId(systemId)) {
                PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
                if (pp != null) {
                    if (pp.getUserId() != userId && !sharingUsers.contains(pp.getUserId())) {
                        continue;
                    } else {

                        if (pp.getUserId() != userId && AllianceUtilities.isTrial(userId)) {
                            continue;
                        }
                        if (pcDAO.findBy(p.getId(), Construction.ID_SCANNERPHALANX) != null
                                && (pp.getUserId() == userId || sharingUsers.contains(pp.getUserId()))) {

                            scannerPhalanxFound = true;
                            break;
                        }
                    }
                }
            }

            boolean planetFleetFound = false;

            for (PlayerFleet pf : pfDAO.findByPlanetId(planetId)) {
                if (sharingUsers.contains(pf.getUserId())) {
                    planetFleetFound = true;
                    break;
                }
            }
  boolean systemFleetFound = false;

            for (PlayerFleet pf : pfDAO.findBySystemId(systemId)) {
                if (sharingUsers.contains(pf.getUserId())) {
                    systemFleetFound = true;
                    break;
                }
            }

            boolean planetOwnerFound = false;

            PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
            if (pp != null) {
                if (sharingUsers.contains(pp.getUserId())) {
                    planetOwnerFound = true;
                }
            }


            //HashMap Design|Count
            HashMap<Integer, HashMap<String, Integer>> shipDesigns = new HashMap<Integer, HashMap<String, Integer>>();
            final Integer OWN = 1;
            final Integer ALLIED = 2;
            final Integer ENEMY = 3;
            for (PlayerFleet pf : pfDAO.findByPlanetId(planetId)) {
                PlayerFleetExt pfe = new PlayerFleetExt(pf.getId());
                for (ShipData sd : pfe.getShipList()) {
                    if (userId == pf.getUserId()) { // BUILD OWN
                        HashMap<String, Integer> entries = shipDesigns.get(OWN);
                        if (entries == null) {
                            entries = new HashMap<String, Integer>();
                        }
                        Integer count = entries.get(sd.getDesignName());
                        if (count == null) {
                            count = sd.getCount();
                        } else {
                            count += sd.getCount();
                        }
                        entries.put(sd.getDesignName(), count);
                        shipDesigns.put(OWN, entries);
                    } else if (DiplomacyUtilities.hasAllianceRelation(userId, pf.getUserId()) ||
                            (DiplomacyUtilities.hasRelation(userId, pf.getUserId(), DiplomacyType.TREATY))) { // ALLIED
                        HashMap<String, Integer> entries = shipDesigns.get(ALLIED);
                        if (entries == null) {
                            entries = new HashMap<String, Integer>();
                        }
                        Integer count = entries.get(sd.getDesignName());
                        if (count == null) {
                            count = sd.getCount();
                        } else {
                            count += sd.getCount();
                        }
                        entries.put(sd.getDesignName(), count);
                        shipDesigns.put(ALLIED, entries);
                    } else { // ENEMY
                        if (scannerPhalanxFound || planetFleetFound || planetOwnerFound) {
                            HashMap<String, Integer> entries = shipDesigns.get(ENEMY);
                            if (entries == null) {
                                entries = new HashMap<String, Integer>();
                            }
                            Integer count = entries.get(sd.getDesignName());
                            if (count == null) {
                                count = sd.getCount();
                            } else {
                                count += sd.getCount();
                            }
                            entries.put(sd.getDesignName(), count);
                            shipDesigns.put(ENEMY, entries);
                        }
                    }
                }
            }
            for (Map.Entry<Integer, HashMap<String, Integer>> entries : shipDesigns.entrySet()) {
                if (OWN.equals(entries.getKey())) { // BUILD OWN
                    for (Map.Entry<String, Integer> entries2 : entries.getValue().entrySet()) {
                        if (firstEntry) {
                            own += " \"planetShips\": [ ";
                        }

                        if (!firstEntry) {
                            own += ", ";
                        }

                        own += "{ \"name\": \"" + entries2.getKey() + "\", \"count\": \"" + entries2.getValue() + "\" }";
                        firstEntry = false;
                    }
                } else if (ALLIED.equals(entries.getKey())) { // ALLIED
                    for (Map.Entry<String, Integer> entries2 : entries.getValue().entrySet()) {
                        if (firstEntryAllied) {
                            allied += " \"planetShips\": [ ";
                        }

                        if (!firstEntryAllied) {
                            allied += ", ";
                        }

                        allied += "{ \"name\": \"" + entries2.getKey() + "\", \"count\": \"" + entries2.getValue() + "\" }";
                        firstEntryAllied = false;
                    }
                } else { // ENEMY
                    for (Map.Entry<String, Integer> entries2 : entries.getValue().entrySet()) {
                        if (firstEntryEnemy) {
                            enemy += " \"planetShips\": [ ";
                        }

                        if (!firstEntryEnemy) {
                            enemy += ", ";
                        }

                        enemy += "{ \"name\": \"" + entries2.getKey() + "\", \"count\": \"" + entries2.getValue() + "\" }";
                        firstEntryEnemy = false;
                    }
                }
            }

            if (!firstEntry) {
                own += " ]";

            }
            if (!firstEntryAllied) {
                allied += " ]";
            }
            if (!firstEntryEnemy) {
                enemy += " ]";
            }


            // Load ships in system
            /*
            rs = stmt.executeQuery("SELECT sd.name, ships.shipTotal FROM (SELECT sf.designId, SUM(sf.count) as shipTotal FROM playerfleet pf, shipfleet sf WHERE sf.fleetId=pf.id AND pf.systemId=" + systemId + " AND pf.userId="+userId+" AND pf.planetId=0 GROUP BY sf.designId) as ships " +
            "LEFT OUTER JOIN shipdesigns sd ON sd.id=ships.designId ORDER BY sd.chassis ASC");
             */
            //    rs = stmt.executeQuery("SELECT sd.name, ships.shipTotal, ships.userId FROM (SELECT sf.designId, SUM(sf.count) as shipTotal, pf.userId FROM playerfleet pf, shipfleet sf WHERE sf.fleetId=pf.id AND pf.systemId=" + systemId + " AND pf.planetId=0 GROUP BY pf.userId, sf.designId) as ships "

            //            + "LEFT OUTER JOIN shipdesigns sd ON sd.id=ships.designId ORDER BY sd.chassis ASC");

            firstEntry = true;
            firstEntryAllied = true;
            firstEntryEnemy = true;

            shipDesigns = new HashMap<Integer, HashMap<String, Integer>>();

            for (PlayerFleet pf : pfDAO.findBy(0, systemId)) {
                PlayerFleetExt pfe = new PlayerFleetExt(pf.getId());
                for (ShipData sd : pfe.getShipList()) {
                    if (userId == pf.getUserId()) { // BUILD OWN
                        HashMap<String, Integer> entries = shipDesigns.get(OWN);
                        if (entries == null) {
                            entries = new HashMap<String, Integer>();
                        }
                        Integer count = entries.get(sd.getDesignName());
                        if (count == null) {
                            count = sd.getCount();
                        } else {
                            count += sd.getCount();
                        }
                        entries.put(sd.getDesignName(), count);
                        shipDesigns.put(OWN, entries);
                    } else if (DiplomacyUtilities.hasAllianceRelation(userId, pf.getUserId())) { // ALLIED
                        HashMap<String, Integer> entries = shipDesigns.get(ALLIED);
                        if (entries == null) {
                            entries = new HashMap<String, Integer>();
                        }
                        Integer count = entries.get(sd.getDesignName());
                        if (count == null) {
                            count = sd.getCount();
                        } else {
                            count += sd.getCount();
                        }
                        entries.put(sd.getDesignName(), count);
                        shipDesigns.put(ALLIED, entries);
                    } else { // ENEMY
                        if (scannerPhalanxFound || systemFleetFound || planetOwnerFound) {
                        HashMap<String, Integer> entries = shipDesigns.get(ENEMY);
                        if (entries == null) {
                            entries = new HashMap<String, Integer>();
                        }
                        Integer count = entries.get(sd.getDesignName());
                        if (count == null) {
                            count = sd.getCount();
                        } else {
                            count += sd.getCount();
                        }
                        entries.put(sd.getDesignName(), count);
                        shipDesigns.put(ENEMY, entries);
                        }
                    }
                    /*if (userId == pf.getUserId()) { // BUILD OWN
                    if (firstEntry) {
                    if (own.length() > 0) {
                    own += ", ";
                    }
                    own += " \"systemShips\": [ ";
                    }

                    if (!firstEntry) {
                    own += ", ";
                    }

                    own += "{ \"name\": \"" + sd.getDesignName() + "\", \"count\": \"" + sd.getCount() + "\" }";
                    firstEntry = false;
                    } else if (AllianceUtilities.areAllied(userId, pf.getUserId())) { // ALLIED
                    if (firstEntryAllied) {
                    if (allied.length() > 0) {
                    allied += ", ";
                    }
                    allied += " \"systemShips\": [ ";
                    }

                    if (!firstEntryAllied) {
                    allied += ", ";
                    }

                    allied += "{ \"name\": \"" + sd.getDesignName() + "\", \"count\": \"" + sd.getCount() + "\" }";
                    firstEntryAllied = false;
                    } else { // ENEMY
                    if (firstEntryEnemy) {
                    if (enemy.length() > 0) {
                    enemy += ", ";
                    }
                    enemy += " \"systemShips\": [ ";
                    }

                    if (!firstEntryEnemy) {
                    enemy += ", ";
                    }

                    enemy += "{ \"name\": \"" + sd.getDesignName() + "\", \"count\": \"" + sd.getCount() + "\" }";
                    firstEntryEnemy = false;
                    }*/
                }
            }
            for (Map.Entry<Integer, HashMap<String, Integer>> entries : shipDesigns.entrySet()) {
                if (OWN.equals(entries.getKey())) { // BUILD OWN
                    for (Map.Entry<String, Integer> entries2 : entries.getValue().entrySet()) {
                        if (firstEntry) {
                            if (own.length() > 0) {
                                own += ", ";
                            }
                            own += " \"systemShips\": [ ";
                        }

                        if (!firstEntry) {
                            own += ", ";
                        }

                        own += "{ \"name\": \"" + entries2.getKey() + "\", \"count\": \"" + entries2.getValue() + "\" }";
                        firstEntry = false;
                    }
                } else if (ALLIED.equals(entries.getKey())) { // ALLIED
                    for (Map.Entry<String, Integer> entries2 : entries.getValue().entrySet()) {
                        if (firstEntryAllied) {
                            if (allied.length() > 0) {
                                allied += ", ";
                            }
                            allied += " \"systemShips\": [ ";
                        }

                        if (!firstEntryAllied) {
                            allied += ", ";
                        }

                        allied += "{ \"name\": \"" + entries2.getKey() + "\", \"count\": \"" + entries2.getValue() + "\" }";
                        firstEntryAllied = false;
                    }
                } else { // ENEMY
                        for (Map.Entry<String, Integer> entries2 : entries.getValue().entrySet()) {
                            if (firstEntryEnemy) {
                                if (enemy.length() > 0) {
                                    enemy += ", ";
                                }
                                enemy += " \"systemShips\": [ ";
                            }

                            if (!firstEntryEnemy) {
                                enemy += ", ";
                            }

                            enemy += "{ \"name\": \"" + entries2.getKey() + "\", \"count\": \"" + entries2.getValue() + "\" }";
                            firstEntryEnemy = false;

                    }
                }
            }
            if (!firstEntry) {
                own += " ] ";
            }
            if (!firstEntryAllied) {
                allied += " ] ";
            }
            if (!firstEntryEnemy) {
                enemy += " ] ";
            }


            // Load Groundtroops
            /*
            rs = stmt.executeQuery("SELECT gt.name, SUM(pt.number) as troopTotal FROM playertroops pt, groundtroops gt WHERE pt.userId="+userId+" AND pt.troopId=gt.id AND pt.planetId=" + planetId + " GROUP BY gt.id");
             */
            // rs = stmt.executeQuery("SELECT gt.name, SUM(pt.number), pt.userId as troopTotal FROM playertroops pt, groundtroops gt WHERE pt.troopId=gt.id AND pt.planetId=" + planetId + " GROUP BY gt.id");

            firstEntry = true;
            firstEntryAllied = true;
            firstEntryEnemy = true;

            for (PlayerTroop pt : ptDAO.findByPlanetId(planetId)) {
                GroundTroop gt = gtDAO.findById(pt.getTroopId());
                if (userId == pt.getUserId()) { // BUILD OWN
                    if (firstEntry) {
                        if (own.length() > 0) {
                            own += ", ";
                        }

                        own += " \"groundTroops\": [ ";
                    }

                    if (!firstEntry) {
                        own += ", ";
                    }

                    own += "{ \"name\": \"" + ML.getMLStr(gt.getName(), userId) + "\", \"count\": \"" + pt.getNumber() + "\" }";
                    firstEntry = false;
                } else if (DiplomacyUtilities.hasAllianceRelation(userId, pt.getUserId())) { // ALLIED
                    if (firstEntryAllied) {
                        if (allied.length() > 0) {
                            allied += ", ";
                        }
                        allied += " \"groundTroops\": [ ";
                    }

                    if (!firstEntryAllied) {
                        allied += ", ";
                    }

                    allied += "{ \"name\": \"" + ML.getMLStr(gt.getName(), userId) + "\", \"count\": \"" + pt.getNumber() + "\" }";
                    firstEntryAllied = false;
                } else { // ENEMY
                    if (firstEntryEnemy) {
                        if (enemy.length() > 0) {
                            enemy += ", ";
                        }
                        enemy += " \"groundTroops\": [ ";
                    }

                    if (!firstEntryEnemy) {
                        enemy += ", ";
                    }

                    enemy += "{ \"name\": \"" + ML.getMLStr(gt.getName(), userId) + "\", \"count\": \"" + pt.getNumber() + "\" }";
                    firstEntryEnemy = false;
                }
            }

            if (!firstEntry) {
                own += " ] ";
            }
            if (!firstEntryAllied) {
                allied += " ] ";
            }
            if (!firstEntryEnemy) {
                enemy += " ] ";
            }


            // Load Planetary Defense
            // rs = stmt.executeQuery("SELECT c.name, pd.count FROM planetdefense pd LEFT OUTER JOIN construction c ON c.id=pd.unitId WHERE pd.planetId=" + planetId + " AND pd.type=1");

            firstEntry = true;

            for (PlanetDefense pd : pdDAO.findByPlanet(planetId, EDefenseType.TURRET)) {
                Construction c = cDAO.findById(pd.getUnitId());
                if (firstEntry) {
                    if (own.length() > 0) {
                        own += ", ";
                    }

                    own += " \"groundDefense\": [ ";
                }

                if (!firstEntry) {
                    own += ", ";
                }

                own += "{ \"name\": \"" + ML.getMLStr(c.getName(), userId) + "\", \"count\": \"" + pd.getCount() + "\" }";
                firstEntry = false;
            }

            for (PlanetDefense pd : pdDAO.findByPlanet(planetId, EDefenseType.SPACESTATION)) {
                ShipDesign sd = sdDAO.findById(pd.getUnitId());
                if (firstEntry) {
                    if (own.length() > 0) {
                        own += ", ";
                    }

                    own += " \"groundDefense\": [ ";
                }

                if (!firstEntry) {
                    own += ", ";
                }

                own += "{ \"name\": \"" + sd.getName() + "\", \"count\": \"" + pd.getCount() + "\" }";
                firstEntry = false;
            }

            if (!firstEntry) {
                own += " ] ";
            }

            boolean hasPrevious = false;

            if (!(own.length() == 0)) {
                json += " \"own\": { " + own + " }";
                hasPrevious = true;
            }
            if (!(allied.length() == 0)) {
                if (hasPrevious) {
                    json += ", ";
                }
                json += " \"allied\": { " + allied + " }";
                hasPrevious = true;
            }
            if (!(enemy.length() == 0)) {
                if (hasPrevious) {
                    json += ", ";
                }
                json += " \"enemy\": { " + enemy + " }";
            }
        } catch (Exception e) {
            DebugBuffer.error("Error while generating JSON for defenseInformation: ", e);
        }

        json += " }";

        return json;
    }
}
