/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.setup;

 import at.darkdestiny.core.enumeration.EPlanetDensity;import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.result.GalaxyCreationResult;
import at.darkdestiny.core.result.SystemGenerationResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.enumeration.EPlanetDensity;
import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.result.GalaxyCreationResult;
import at.darkdestiny.core.result.SystemGenerationResult;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Admin
 */
public class Spiralgalaxy extends AbstractGalaxy {

    private static final Logger log = LoggerFactory.getLogger(Spiralgalaxy.class);

    public Spiralgalaxy(int width, int height, boolean destroyDBEntries, EPlanetDensity pDensity, int sysPerUser, int originX, int originY) {
        super(width, height, destroyDBEntries, pDensity, sysPerUser, originX, originY);
    }

    public GalaxyCreationResult create() {
        log.debug("Create Spiral Galaxy with width/height : " + width + "/" + height);
        log.debug("Create Spiral Galaxy with originX/originY : " + originX + "/" + originY);


        try {
            ArrayList<Planet> planetList = new ArrayList<Planet>();
            ArrayList<at.darkdestiny.core.model.System> systemList = new ArrayList<at.darkdestiny.core.model.System>();
            ArrayList<at.darkdestiny.core.model.PlanetRessource> ressList = new ArrayList<at.darkdestiny.core.model.PlanetRessource>();
            int actSystemId = Service.systemDAO.findLastId();
            int startSystem = actSystemId;
            actSystemId++;

            // int[][] galaxy = new int[WIDTH][HEIGHT];
            HashMap<Integer, HashSet<Integer>> genGalaxy = new HashMap<Integer, HashSet<Integer>>();
            //Zufallsdrehung

            boolean turnRight = false;
            int ranTurn = (int) Math.round(Math.random() * 1);
            if (ranTurn == 1) {
                turnRight = true;
            }
            log.debug("turnRight : " + turnRight);
            //Anzahl Arme Min - 3 Max - 5
            int randomArm = (int) Math.round(Math.random() * 1);
            randomArm = randomArm + 4;

            double arms[] = new double[randomArm];
            int lastI = -1;

            for (int i = 0; i < 80; i++) {
                //Wertvergabe f�r die Arme
                for (int j = 0; j < randomArm; j++) {
                    //arms[j] = -((width/randomArm * j) % (width/2d - width/10d)) - ran;
                    arms[j] = -((width / randomArm * j) % width);
                }
                double phi = (-40 + (80 / 80 * i)) * Math.PI / 180;
                double diff = Math.abs(40 - i) + 1d;

                do {
                    for (int t = 0; t < arms.length; t++) {
                        double r = arms[t];
                        if ((r > 0) && (r < width / 2d)) {
                            double randLimit = 0.3d * ((width / 3000d) * Math.random());
                            double rand = Math.random();
                            // double compareValue = (randLimit / ((42 - diff) * (1d + ((42-diff)/10d))));

                            double removespiral = 1d;
                            if (r < ((width / 2d) * 0.25d)) {
                                removespiral = 1d + (15d / ((width / 2d) * 0.25d) * (((width / 2d) * 0.25d) - r));
                            }

                            double compareValue = Math.max(800d - Math.pow(diff, 2d), 0d) / (15000d * Math.max(diff / 10d, 0.7d) * removespiral);

                            if (lastI != i) {
                                // System.out.println(rand  + " < " + randLimit + " / " + (42-diff));
                                // System.out.println((randLimit / (42 - diff)));
                            }
                            lastI = i;
                            if (rand < compareValue) {
                                int x = (int) Math.floor(((double) originX) + (r * Math.sin(phi)) - 5d + Math.random() * 10d);
                                int y = (int) Math.floor(((double) originY) + (r * Math.cos(phi)) - 5d + Math.random() * 10d);

                                if (turnRight) {
                                    x = (int) Math.floor(((double) originX) + (r * Math.sin(-phi)) - 5d + Math.random() * 10d);
                                    y = (int) Math.floor(((double) originY) + (r * Math.cos(-phi)) - 5d + Math.random() * 10d);
                                }
                                if (genGalaxy.containsKey(x)) {
                                    genGalaxy.get(x).add(y);
                                } else {
                                    HashSet<Integer> ySet = new HashSet<Integer>();
                                    ySet.add(y);
                                    genGalaxy.put(x, ySet);
                                }

                            }
                        }
                    }

                    for (int x = 0; x < arms.length; x++) {
                        arms[x] = arms[x] + (width) / 360d;
                    }
                    phi += (2d * Math.PI / 360d);
                } while (phi <= (2d * Math.PI) + (2d / 360d * 270d * Math.PI));
            }

            double phi = 0;

            //Inner Cluster of spiral Galaxy
            int min = (int) (originX - (width / 2d) * 4d / 5d);
            int max = (int) (originX + (width / 2d) * 4d / 5d);
            int step = (int) Math.round(width * 50d / width);
            do {
                for (int i = min; i < max; i += step) {
                    double distance = Math.abs(originX - i);
                    double treshHold = 0.5d + ((0.5d * Math.random()));

                    if (0.96d < treshHold) {
                        distance *= (0.5d + (0.7d * Math.random()));
                        // distance = Math.abs(originX - (int) Math.round(i * (1d + (Math.round(Math.random() * 10d) / 1000d))));
                        int x = (int) Math.floor(((double) originX) + distance * Math.sin(phi));
                        int y = (int) Math.floor(((double) originY) + distance * Math.cos(phi));

                        if (genGalaxy.containsKey(x)) {
                            genGalaxy.get(x).add(y);
                        } else {
                            HashSet<Integer> ySet = new HashSet<Integer>();
                            ySet.add(y);
                            genGalaxy.put(x, ySet);
                        }
                    }
                }

                phi += (2d * Math.PI / 360d);
            } while (phi <= (2d * Math.PI));

            int sys = 0;
            for (int i = (int) Math.round(originX - height / 2d); i < originX + height / 2d; i++) {
                for (int j = (int) Math.round(originY - width / 2d); j < originY + width / 2d; j++) {
                    if (!genGalaxy.containsKey(i)) {
                        continue;
                    }
                    if (!genGalaxy.get(i).contains(j)) {
                        continue;
                    }

                    int posx = i;
                    int posy = j;

                    //Generate a new system
                    // Calculate possibilty of planets
                    double distanceToCenter = Math.sqrt(Math.pow((originX) - (double) posx, 2d) + Math.pow((originY) - (double) posy, 2d));
                    distanceToCenter = 100d / ((double) originX) * distanceToCenter;

                    SystemGenerationResult sgr = generateSystem(posx, posy, distanceToCenter, actSystemId, systemList, planetList, ressList, false);
                    if (sgr.isSystemGenerated()) {
                        actSystemId += sgr.getSystemsGenerated();
                        sys++;
                    }
                }
            }
            java.lang.System.out.println(sys + " Systems generated");
            Galaxy g = new Galaxy();
            g.setStartSystem(startSystem);
            g.setEndSystem(actSystemId);
            g.setOriginX(originX);
            g.setOriginY(originY);
            g.setHeight(height);
            g.setWidth(width);
            // g = Service.galaxyDAO.add(g);
            // Service.systemDAO.insertAll(systemList);
            // Service.planetDAO.insertAll(planetList);
            // Service.planetRessourceDAO.insertAll(ressList);
            return new GalaxyCreationResult(g, ressList, planetList, systemList);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in planet creation", e);
        }
        log.debug("Done Creating Galaxy");
        return null;
    }
}
