/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

 import at.darkdestiny.core.model.Galaxy;import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class GalaxyDAO extends ReadWriteTable<Galaxy> implements GenericDAO {

    private static final Logger log = LoggerFactory.getLogger(GalaxyDAO.class);

    public void deleteGalaxy(Integer galaxyId) {
        log.debug("Deleting Galaxy  : " + galaxyId);
        Galaxy g = new Galaxy();
        g.setId(galaxyId);
        g = (Galaxy) get(g);

        ArrayList<PlanetRessource> resToDelete = new ArrayList<PlanetRessource>();
        ArrayList<Planet> planToDelete = new ArrayList<Planet>();
        ArrayList<at.darkdestiny.core.model.System> sysToDelete = new ArrayList<at.darkdestiny.core.model.System>();
        for (at.darkdestiny.core.model.System s : (ArrayList<at.darkdestiny.core.model.System>) Service.systemDAO.findAll()) {
            if (s.getId() < g.getStartSystem() || s.getId() > g.getEndSystem()) {
                continue;
            }
            for (Planet p : Service.planetDAO.findBySystemId(s.getId())) {
                for (PlanetRessource pr : Service.planetRessourceDAO.findByPlanetId(p.getId())) {

                    resToDelete.add(pr);
                }
                planToDelete.add(p);
            }
            sysToDelete.add(s);
        }
        Service.planetRessourceDAO.removeAll(resToDelete);
        Service.planetDAO.removeAll(planToDelete);
        Service.systemDAO.removeAll(sysToDelete);
        remove(g);
        log.debug("Done - Deleting Galaxy  : ");

    }

    public Galaxy findById(int galaxyId) {
        Galaxy g = new Galaxy();
        g.setId(galaxyId);
        return (Galaxy) get(g);
    }
}
