package at.darkdestiny.core.text;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameUtilities;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class FormatTime {

    public static String formatTimeSpan(long ticks) {
        StringBuilder result = new StringBuilder();
        result.append(ticks).append(" Ticks (");
        TimeStruct time = new TimeStruct(ticks);
        boolean reqComma = false;
        if (time.getDays() > 0) {
            result.append(time.getDays()).append(" Tage");
            reqComma = true;
        }
        if (time.getHours() > 0) {
            if (reqComma) {
                result.append(", ");
            }
            result.append(time.getHours()).append(" Stunden");
            reqComma = true;
        }
        if (time.getMinutes() != 0) {
            if (reqComma) {
                result.append(", ");
            }
            result.append(time.getMinutes()).append(" Minuten");
        }
        result.append(")");
        return result.toString();
    }

    public static long convertTicksToMin(long ticks) {
        long secondsPerTick = GameConfig.getInstance().getTicktime() / 1000;
        return ticks * secondsPerTick / 60;
    }

    public static String formatTimeSpanAsTimeStamp(int ticks) {
        TimeStruct time = new TimeStruct(ticks);
        return String.format(
                "%02d:%02d:%02d",
                time.getDays(), time.getHours(), time.getMinutes());
    }

    public static Calendar getTimeOfTick(long tick) {
        GregorianCalendar result = new GregorianCalendar();
        result.setTimeInMillis(
                GameConfig.getInstance().getStarttime() + tick * GameConfig.getInstance().getTicktime());
        return result;
    }

    public static Calendar getEndTimeOfTimeSpan(long ticks) {
        GregorianCalendar result = new GregorianCalendar();
        result.setTimeInMillis(
                GameConfig.getInstance().getStarttime()
                + (ticks + GameUtilities.getCurrentTick2() + 1) * GameConfig.getInstance().getTicktime());
        return result;
    }

    public static String formatEndTimeofTimeSpanAsDate(long ticks) {
        Calendar result = getEndTimeOfTimeSpan(ticks);
        Calendar now = new GregorianCalendar();


        String dayString = "";
        if (ticks == 0) {
            return "Nie";
        }
        if (now.get(Calendar.DAY_OF_YEAR) == result.get(Calendar.DAY_OF_YEAR)
                && now.get(Calendar.YEAR) == result.get(Calendar.YEAR)) {
            dayString = "Heute, ";
        } else {
            now.add(Calendar.DAY_OF_YEAR, 1);
            if (now.get(Calendar.DAY_OF_YEAR) == result.get(Calendar.DAY_OF_YEAR)
                    && now.get(Calendar.YEAR) == result.get(Calendar.YEAR)) {
                dayString = "Morgen, ";
            } else {
                dayString = new SimpleDateFormat("dd.MM.yy, ").format(result.getTime());
            }
        }

        return dayString + new SimpleDateFormat("HH:mm").format(result.getTime());
    }

    public static final class TimeStruct {

        private final int minutes;
        private final int hours;
        private final int days;

        public TimeStruct(long ticks) {
            long minutes_ = convertTicksToMin(ticks);
            minutes = (int) (minutes_ % 60);
            long hours_ = minutes_ / 60;
            hours = (int) (hours_ % 24);
            days = (int) (hours_ / 24);
        }

        public int getMinutes() {
            return minutes;
        }

        public int getHours() {
            return hours;
        }

        public int getDays() {
            return days;
        }
    }
}
