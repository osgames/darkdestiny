/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.ProductionDAO;
import at.darkdestiny.core.dao.ResearchDAO;
import at.darkdestiny.core.dao.ResearchProgressDAO;
import at.darkdestiny.core.dao.TechRelationDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.VoteOptionDAO;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EBonusType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.Production;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.ResearchProgress;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.TechRelation;
import at.darkdestiny.core.model.VoteOption;
import at.darkdestiny.core.model.VotePermission;
import at.darkdestiny.core.model.Voting;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.result.BonusResult;
import at.darkdestiny.core.result.ResearchResult;
import at.darkdestiny.core.service.ResearchService;
import at.darkdestiny.core.view.ResearchProgressView;
import at.darkdestiny.core.voting.MetaDataDataType;
import at.darkdestiny.core.voting.Vote;
import at.darkdestiny.core.voting.VoteException;
import at.darkdestiny.core.voting.VotingFactory;
import at.darkdestiny.core.voting.VotingTextMLSecure;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class ResearchUtilities {

    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static ResearchProgressDAO rpDAO = (ResearchProgressDAO) DAOFactory.get(ResearchProgressDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static ProductionDAO pDAO = (ProductionDAO) DAOFactory.get(ProductionDAO.class);
    private static TechRelationDAO trDAO = (TechRelationDAO) DAOFactory.get(TechRelationDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static ResearchDAO rDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    private static VoteOptionDAO voDAO = (VoteOptionDAO) DAOFactory.get(VoteOptionDAO.class);

    public static boolean isModuleResearched(int moduleId, int userId) {
        ArrayList<TechRelation> trList = trDAO.findByModuleId(moduleId);
        if (trList.isEmpty()) {
            return true;
        }

        for (TechRelation tr : trList) {
            PlayerResearch pr = prDAO.findBy(userId, tr.getReqResearchId());
            if (pr == null) {
                return false;
            }
        }

        return true;
    }

    public static int noOfFinishedResearches(int researchId) {
        return prDAO.findByResearchId(researchId).size();
    }
       
    public static ResearchResult getResearchPointsGlobal(int userId) {
        ArrayList<PlayerPlanet> ppList = ppDAO.findByUserId(userId);

        int totalResPoints = 0;
        int totalCompResPoints = 0;

        int totalRPBonus = 0;
        int totalCRPBonus = 0;

        for (PlayerPlanet pp : ppList) {
            int resPoints = 0;
            int compResPoints = 0;

            ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(pp.getPlanetId());

            boolean planetHasResearch = false;

            // TODO unify rp calculation of epcr
            for (PlanetConstruction pc : pcList) {
                ArrayList<Production> pList = pDAO.findProductionForConstructionId(pc.getConstructionId());
                for (Production p : pList) {
                    if (p.getRessourceId() == Ressource.RP) {
                        resPoints += p.getQuantity() * pc.getActiveCount();
                        planetHasResearch = true;
                    } else if (p.getRessourceId() == Ressource.RP_COMP) {
                        compResPoints += p.getQuantity() * pc.getActiveCount();
                        planetHasResearch = true;
                    }
                }
            }

            if (planetHasResearch) {
                // log.debug("Total research for Planet " + pp.getPlanetId() + " RP: " + resPoints + " CRP: " + compResPoints);

                try {
                    PlanetCalculation pc = new PlanetCalculation(pp.getPlanetId());
                    ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

                    // if (pp.getPlanetId() == 7864) log.debug("RESS EFF IS " + epcr.getEffResearch());
                    // if (pp.getPlanetId() == 7864) log.debug("RESSPOINTS = " + resPoints);
                    // if (pp.getPlanetId() == 7864) log.debug("CRESSPOINTS = " + compResPoints);

                    // log.debug("Research Efficiency on currPlanet is " + epcr.getEffResearch());
                    BonusResult br = BonusUtilities.findBonus(pp.getPlanetId(), EBonusType.RESEARCH);

                    int actRP = (int) (resPoints * epcr.getEffResearch());
                    int actCRP = (int) (compResPoints * epcr.getEffResearch());

                    if (br != null) {
                        totalRPBonus += (actRP * br.getPercBonus()) + br.getNumericBonus();
                        totalCRPBonus += (actCRP * br.getPercBonus()) + br.getNumericBonus();
                    }

                    totalResPoints += (int) (resPoints * epcr.getEffResearch());
                    totalCompResPoints += (int) (compResPoints * epcr.getEffResearch());
                } catch (Exception e) {
                    DebugBuffer.writeStackTrace("Research Points Global Error: ", e);
                }
            }
        }

        return new ResearchResult(totalResPoints, totalCompResPoints, totalRPBonus, totalCRPBonus);
    }

    public static ResearchResult getResearchPointsPlanet(int planetId, PlanetCalculation pc) {

        int totalResPoints = 0;
        int totalCompResPoints = 0;

        int totalRPBonus = 0;
        int totalCRPBonus = 0;


        int resPoints = 0;
        int compResPoints = 0;

        // TODO use ProdCalculation
        ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(planetId);

        boolean planetHasResearch = false;

        for (PlanetConstruction pCon : pcList) {
            ArrayList<Production> pList = pDAO.findProductionForConstructionId(pCon.getConstructionId());
            for (Production p : pList) {
                if (p.getRessourceId() == Ressource.RP) {
                    resPoints += p.getQuantity() * pCon.getActiveCount();
                    planetHasResearch = true;
                } else if (p.getRessourceId() == Ressource.RP_COMP) {
                    compResPoints += p.getQuantity() * pCon.getActiveCount();
                    planetHasResearch = true;
                }
            }
        }

        if (planetHasResearch) {
            // log.debug("Total research for Planet " + pp.getPlanetId() + " RP: " + resPoints + " CRP: " + compResPoints);

            try {
                ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

                // if (pp.getPlanetId() == 7864) log.debug("RESS EFF IS " + epcr.getEffResearch());
                // if (pp.getPlanetId() == 7864) log.debug("RESSPOINTS = " + resPoints);
                // if (pp.getPlanetId() == 7864) log.debug("CRESSPOINTS = " + compResPoints);

                // log.debug("Research Efficiency on currPlanet is " + epcr.getEffResearch());
                BonusResult br = BonusUtilities.findBonus(planetId, EBonusType.RESEARCH);

                int actRP = (int) (resPoints * epcr.getEffResearch());
                int actCRP = (int) (compResPoints * epcr.getEffResearch());

                if (br != null) {
                    totalRPBonus += (actRP * br.getPercBonus()) + br.getNumericBonus();
                    totalCRPBonus += (actCRP * br.getPercBonus()) + br.getNumericBonus();
                }

                totalResPoints += (int) (resPoints * epcr.getEffResearch());
                totalCompResPoints += (int) (compResPoints * epcr.getEffResearch());
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Research Points Planet Error: ",e);
            }

        }

        return new ResearchResult(totalResPoints, totalCompResPoints, totalRPBonus, totalCRPBonus);
    }

    public static boolean isResearchResearched(int resId, int userId) {
        PlayerResearch pr = new PlayerResearch();
        pr.setResearchId(resId);
        pr.setUserId(userId);
        pr = (PlayerResearch) prDAO.get(pr);

        return (pr != null);
    }
    
    public synchronized static void grantVotedResearch(Vote v, int voteResult) {
        int userId = (Integer)v.getMetaData("USERID");
        int researchId = -1;
        
        VoteOption vo = voDAO.findById(v.getCloseOption());
        
        Research rSearch = new Research();
        rSearch.setName(vo.getVoteOption());
        researchId = rDAO.find(rSearch).get(0).getId();
               
        System.out.println("GRANT VOTED RESEARCH FOR USER " + userId + " // VOTERESULT IS " + v.getCloseOption() + " WHICH IS RESEARCH ID " + researchId);                
        
        ResearchProgress rp = rpDAO.findBy(userId, researchId);
        
        if (rp != null) {
            rp.setRpLeft(0);
            rp.setRpLeftComputer(0);
            rpDAO.update(rp);
            
            // Check if action is on Hold
            Action a = aDAO.findResearchBy(userId, researchId);
            if (a.getOnHold()) {
                a.setOnHold(false);
                aDAO.update(a);
            }
        } else {
            rp = new ResearchProgress();
            rp.setResearchId(researchId);
            rp.setUserId(userId);
            rp.setRpLeft(0);
            rp.setRpLeftComputer(0);
            rp = rpDAO.add(rp);                          
            
            Action a = new Action();
            a.setType(EActionType.RESEARCH);
            a.setResearchId(researchId);
            a.setRefTableId(rp.getId());
            a.setOnHold(false);
            a.setUserId(userId);
            aDAO.add(a);
        }
    }
    
    // Generate the message with options for boosting a research
    public synchronized static void generateFreeResearchMessage(int userId) {
        // GenerateMessage gm = new GenerateMessage();
        // gm.setSourceUserId(uDAO.getSystemUser().getUserId());
        // gm.setDestinationUserId(userId);
        // gm.setTopic("Choose free research");
        // gm.setMasterEntry(true);
        
        String message = "Please select research for instant finishing";
        int voteId = 0;
        
        // Set up Vote
        try {
            HashMap<String,CharSequence> msgReplacements = new HashMap<String,CharSequence>();
            HashMap<String,CharSequence> sbjReplacements = new HashMap<String,CharSequence>();

            // sbjReplacements.put("%USER%",IdToNameService.getUserName(reqUserId));

            // msgReplacements.put("%USER%",IdToNameService.getUserName(reqUserId));
            // msgReplacements.put("%NEWLEADERSHIP%",newLeadership.toString());

            VotingTextMLSecure vtML = new VotingTextMLSecure("msg_msg_researchselect","msg_sbj_researchselect",msgReplacements,sbjReplacements);            
            
            Vote v = VotingFactory.createNewVote(userId, Voting.TYPE_VOTE_RESEARCH);
            // Vote v = new Vote(userId, vtML, Voting.TYPE_VOTE_RESEARCH);
            v.setVotingTextML(vtML);
            v.setMsgTranslation(true);
            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, userId));
            v.setExpires(0);            
            
            // Loop all researches possible at current time
            ArrayList<ResearchProgressView> rpvList = ResearchService.getResearchListView(userId);
            for (ResearchProgressView rpv : rpvList) {
                if ((rpv.getStatus() == ResearchService.RESEARCH_VIEW_INRESEARCH) ||
                    (rpv.getStatus() == ResearchService.RESEARCH_VIEW_NOT_RESEARCHED) ||
                    (rpv.getStatus() == ResearchService.RESEARCH_VIEW_ON_HOLD)) {
                    Research r = rpv.getResearchEntry();
                    
                    if (r.getId() == 999) {
                        continue;
                    }
                    
                    v.addOption(new VoteOption(r.getName(), false, VoteOption.OPTION_TYPE_VALUE, true));
                }
            }
            
            ArrayList<Integer> receivers = new ArrayList<Integer>();
            receivers.add(userId);
            
            v.addMetaData("USERID", userId, MetaDataDataType.INTEGER);
            
            v.setReceivers(receivers);
            v.setMinVotes(1);
            v.setTotalVotes(1);                     
            voteId = VotingFactory.persistVote(v);
            // voteId = VoteUtilities.createVote(v);                        
        } catch (VoteException ve) {
            DebugBuffer.writeStackTrace("Creation of vote failed: ", ve);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while creating research vote",e);
        }
                
        // Attach Vote to message
        // gm.setMessageType(EMessageType.SYSTEM);
        // gm.setMsg(message);
        
        // System.out.println("SET vote Id " + voteId);
        // gm.setReference(EMessageRefType.VOTE, voteId);
        // gm.writeMessageToUser();
    }
}
