/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.Threading;

import at.darkdestiny.core.model.ViewSystem;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Lists;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ViewSystemInsertJob extends AbstractCopyJob {

    private final ArrayList<ViewSystem> toInsert;

    public ViewSystemInsertJob(ArrayList<ViewSystem> toInsert) {
        this.toInsert = toInsert;
    }

    @Override
    public void processJob() {
        try {
            ArrayList<ViewSystem> checked = Lists.newArrayList();
            for (ViewSystem vs : toInsert) {
                if (Service.viewSystemDAO.findBy(vs.getLocationId(), vs.getLocationType(), vs.getUserId()) == null) {
                    checked.add(vs);
                }
            }        
        
            Service.viewSystemDAO.insertAll(checked);
        } catch (Exception e) {
            DebugBuffer.error("Error occured while propagating Systems to viewSystem: ", e);            
        }
    }
}
