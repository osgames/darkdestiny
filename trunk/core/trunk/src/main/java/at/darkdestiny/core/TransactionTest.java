/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.transaction.Transaction;
import at.darkdestiny.framework.transaction.TransactionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class TransactionTest {

    private static final Logger log = LoggerFactory.getLogger(TransactionTest.class);

    private static ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);

    public static String runTest() {
        TransactionHandler th = TransactionHandler.getTransactionHandler();

        Construction c = new Construction();
        c.setName("Testgeb�ude");

        th.addTransaction(new Transaction(cDAO, c, Transaction.TRANSACTION_TYPE_INSERT));

        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(16169);
        pr.setQty(1000l);
        pr.setRessId(11);
        pr.setType(EPlanetRessourceType.PLANET);

        th.addTransaction(new Transaction(prDAO, pr, Transaction.TRANSACTION_TYPE_UPDATE));

        try {
            th.execute();
        } catch (TransactionException te) {
            log.debug("ERROR: " + te.getMessage());
            return "Transaction failed!<BR><BR>" + th.getTransactionLog();
        }

        return "Transaction successful!<BR><BR>" + th.getTransactionLog();
    }
}
