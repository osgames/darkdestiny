/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "playerplanet")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PlayerPlanet extends Model<PlayerPlanet> {

    public final static int PRIORITY_LOW = 2;
    public final static int PRIORITY_MEDIUM = 1;
    public final static int PRIORITY_HIGH = 0;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("userId")
    @IndexAnnotation(indexName = "userId")
    private Integer userId;
    @FieldMappingAnnotation("planetId")
    @IndexAnnotation(indexName = "planetId")
    private Integer planetId;
    @FieldMappingAnnotation("homeSystem")
    @DefaultValue("0")
    @IndexAnnotation(indexName = "userId")
    private Boolean homeSystem;
    @FieldMappingAnnotation("colonyType")
    @DefaultValue("DEFAULT")
    private EColonyType colonyType;
    @FieldMappingAnnotation("population")
    @DefaultValue("0")
    private Long population;
    @FieldMappingAnnotation("moral")
    @DefaultValue("0")
    private Integer moral;
    @FieldMappingAnnotation("unrest")
    @DefaultValue("0")
    private Double unrest;
    @FieldMappingAnnotation("growth")
    @DefaultValue("0")
    private Float growth;
    @FieldMappingAnnotation("specialPoints")
    @DefaultValue("0")
    private Float specialPoints;
    @FieldMappingAnnotation("specialGrowth")
    @DefaultValue("0")
    private Float specialGrowth;
    @FieldMappingAnnotation("specialResearch")
    @DefaultValue("0")
    private Float specialResearch;
    @FieldMappingAnnotation("specialProduction")
    @DefaultValue("0")
    private Float specialProduction;
    @FieldMappingAnnotation("tax")
    @DefaultValue("0")
    private Integer tax;
    @FieldMappingAnnotation("growthStatus")
    @DefaultValue("0")
    private Float growthStatus;
    @FieldMappingAnnotation("researchStatus")
    @DefaultValue("0")
    private Float researchStatus;
    @FieldMappingAnnotation("productionStatus")
    @DefaultValue("0")
    private Float productionStatus;
    @FieldMappingAnnotation("priorityIndustry")
    @DefaultValue("1")
    private Integer priorityIndustry;
    @FieldMappingAnnotation("priorityAgriculture")
    @DefaultValue("2")
    private Integer priorityAgriculture;
    @FieldMappingAnnotation("priorityResearch")
    @DefaultValue("0")
    private Integer priorityResearch;
    @FieldMappingAnnotation("invisibleFlag")
    @IndexAnnotation(indexName = "userId")
    @DefaultValue("0")
    private Boolean invisibleFlag;
    @FieldMappingAnnotation("migration")
    @DefaultValue("0")
    private Integer migration;
    @FieldMappingAnnotation("dismantle")
    @DefaultValue("0")
    private Boolean dismantle;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the planetId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the homeSystem
     */
    public Boolean isHomeSystem() {
        return homeSystem;
    }

    /**
     * @param homeSystem the homeSystem to set
     */
    public void setHomeSystem(Boolean homeSystem) {
        this.homeSystem = homeSystem;
    }

    /**
     * @return the population
     */
    public Long getPopulation() {
        return population;
    }

    /**
     * @param population the population to set
     */
    public void setPopulation(Long population) {
        this.population = population;
    }

    /**
     * @return the moral
     */
    public Integer getMoral() {
        return moral;
    }

    /**
     * @param moral the moral to set
     */
    public void setMoral(Integer moral) {
        this.moral = moral;
    }

    /**
     * @return the growth
     */
    public Float getGrowth() {
        return growth;
    }

    /**
     * @param growth the growth to set
     */
    public void setGrowth(Float growth) {
        this.growth = growth;
    }

    /**
     * @return the specialPoints
     */
    public Float getSpecialPoints() {
        return specialPoints;
    }

    /**
     * @param specialPoints the specialPoints to set
     */
    public void setSpecialPoints(Float specialPoints) {
        this.specialPoints = specialPoints;
    }

    /**
     * @return the specialGrowth
     */
    public Float getSpecialGrowth() {
        return specialGrowth;
    }

    /**
     * @param specialGrowth the specialGrowth to set
     */
    public void setSpecialGrowth(Float specialGrowth) {
        this.specialGrowth = specialGrowth;
    }

    /**
     * @return the specialResearch
     */
    public Float getSpecialResearch() {
        return specialResearch;
    }

    /**
     * @param specialResearch the specialResearch to set
     */
    public void setSpecialResearch(Float specialResearch) {
        this.specialResearch = specialResearch;
    }

    /**
     * @return the specialProduction
     */
    public Float getSpecialProduction() {
        return specialProduction;
    }

    /**
     * @param specialProduction the specialProduction to set
     */
    public void setSpecialProduction(Float specialProduction) {
        this.specialProduction = specialProduction;
    }

    /**
     * @return the tax
     */
    public Integer getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(Integer tax) {
        this.tax = tax;
    }

    /**
     * @return the growthStatus
     */
    public Float getGrowthStatus() {
        return growthStatus;
    }

    /**
     * @param growthStatus the growthStatus to set
     */
    public void setGrowthStatus(Float growthStatus) {
        this.growthStatus = growthStatus;
    }

    /**
     * @return the researchStatus
     */
    public Float getResearchStatus() {
        return researchStatus;
    }

    /**
     * @param researchStatus the researchStatus to set
     */
    public void setResearchStatus(Float researchStatus) {
        this.researchStatus = researchStatus;
    }

    /**
     * @return the productionStatus
     */
    public Float getProductionStatus() {
        return productionStatus;
    }

    /**
     * @param productionStatus the productionStatus to set
     */
    public void setProductionStatus(Float productionStatus) {
        this.productionStatus = productionStatus;
    }

    /**
     * @return the priorityIndustry
     */
    public Integer getPriorityIndustry() {
        return priorityIndustry;
    }

    /**
     * @param priorityIndustry the priorityIndustry to set
     */
    public void setPriorityIndustry(Integer priorityIndustry) {
        this.priorityIndustry = priorityIndustry;
    }

    /**
     * @return the priorityAgriculture
     */
    public Integer getPriorityAgriculture() {
        return priorityAgriculture;
    }

    /**
     * @param priorityAgriculture the priorityAgriculture to set
     */
    public void setPriorityAgriculture(Integer priorityAgriculture) {
        this.priorityAgriculture = priorityAgriculture;
    }

    /**
     * @return the priorityResearch
     */
    public Integer getPriorityResearch() {
        return priorityResearch;
    }

    /**
     * @param priorityResearch the priorityResearch to set
     */
    public void setPriorityResearch(Integer priorityResearch) {
        this.priorityResearch = priorityResearch;
    }

    /**
     * @return the invisibleFlag
     */
    public Boolean getInvisibleFlag() {
        return invisibleFlag;
    }

    /**
     * @param invisibleFlag the invisibleFlag to set
     */
    public void setInvisibleFlag(Boolean invisibleFlag) {
        this.invisibleFlag = invisibleFlag;
    }

    /**
     * @return the migration
     */
    public Integer getMigration() {
        return migration;
    }

    /**
     * @param migration the migration to set
     */
    public void setMigration(Integer migration) {
        this.migration = migration;
    }

    // Returns available Actions Poitns for current planet
    public int getPopActionPoints() {
        String habitable = "CMG";

        PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
        Planet p = pDAO.findById(planetId);

        if (!habitable.contains((p.getLandType()))) {
            return (int) (population / 100);
        } else {
            return getPopActionPoints(population);
        }
    }

    public int getPopActionPointsSimulated(long inPop) {
        String habitable = "CMG";

        PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
        Planet p = pDAO.findById(planetId);

        if (!habitable.contains((p.getLandType()))) {
            return (int) (inPop / 100);
        } else {
            return getPopActionPoints(inPop);
        }
    }

    private static int getPopActionPointsInhabitable(long inPop) {
        return (int) (inPop / 100);
    }

    // returns Actions Points for a specific number of population
    private static int getPopActionPoints(long inPop) {
        int actionPoints = 0;
        long tmpPop = inPop;

        if (tmpPop < 500000) {
            actionPoints += (int) (inPop / 100);
            tmpPop = 0;
        } else {
            actionPoints += (int) (500000 / 100);
            tmpPop -= 500000;
        }

        if (tmpPop < 2000000) {
            actionPoints += (int) (inPop / 400);
            tmpPop = 0;
        } else {
            actionPoints += (int) (2000000 / 400);
            tmpPop -= 2000000;
        }

        if (tmpPop < 10000000) {
            actionPoints += (int) (inPop / 1200);
            tmpPop = 0;
        } else {
            actionPoints += (int) (10000000 / 1200);
            tmpPop -= 10000000;
        }

        if (tmpPop < 100000000) {
            actionPoints += (int) (inPop / 3600);
            tmpPop = 0;
        } else {
            actionPoints += (int) (100000000 / 3600);
            tmpPop -= 100000000;
        }

        if (tmpPop < 500000000) {
            actionPoints += (int) (inPop / 9000);
            tmpPop = 0;
        } else {
            actionPoints += (int) (500000000 / 9000);
            tmpPop -= 500000000;
        }

        if (tmpPop > 0) {
            actionPoints += (int) (inPop / 16000);
        }

        return actionPoints;
    }

    /**
     * @return the dismantle
     */
    public Boolean getDismantle() {
        return dismantle;
    }

    /**
     * @param dismantle the dismantle to set
     */
    public void setDismantle(Boolean dismantle) {
        this.dismantle = dismantle;
    }

    /**
     * @return the loyality
     */
    public Double getUnrest() {
        return unrest;
    }

    /**
     * @param loyality the loyality to set
     */
    public void setUnrest(Double unrest) {
        this.unrest = unrest;
    }

    /**
     * @return the colonyType
     */
    public EColonyType getColonyType() {
        String habitable = "CMG";

        PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
        Planet p = pDAO.findById(planetId);

        if (!habitable.contains((p.getLandType()))) {
            return EColonyType.MINING_COLONY;
        }

        return colonyType;
    }

    /**
     * @param colonyType the colonyType to set
     */
    public void setColonyType(EColonyType colonyType) {
        this.colonyType = colonyType;
    }
}
