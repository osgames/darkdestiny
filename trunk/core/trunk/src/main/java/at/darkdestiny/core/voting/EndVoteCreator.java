/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.StatisticDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.Statistic;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.VoteOption;
import at.darkdestiny.core.model.VotePermission;
import at.darkdestiny.core.model.Voting;
import at.darkdestiny.core.service.IdToNameService;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class EndVoteCreator {
    private static StatisticDAO sDAO = (StatisticDAO) DAOFactory.get(StatisticDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);

    public final static int VOTABLE_ITEMS = 5;

    public static void createEndVote() {
        // Get possible vote candidates
        // Read top 10 of statistic
        ArrayList<Statistic> statList = sDAO.findAll();

        TreeMap<Integer,Integer> sorted = new TreeMap<Integer,Integer>();
        for (Statistic s : statList) {
            sorted.put(s.getPoints(), s.getUserId());
        }

        int currElements = 0;

        ArrayList<Integer> players = new ArrayList<Integer>();
        ArrayList<Integer> alliances = new ArrayList<Integer>();
        int checkingCount = 0;

        for (Iterator<Integer> it = sorted.descendingKeySet().iterator();it.hasNext();) {
            checkingCount++;

            Integer points = it.next();
            int userId = sorted.get(points);
            int allianceId = 0;

            // Check if user is in alliance
            AllianceMember am = amDAO.findByUserId(userId);
            if (am != null) {
                // Get master Alliance Id
                Alliance a = aDAO.findById(am.getAllianceId());
                allianceId = a.getMasterAlliance();
            }

            if (allianceId != 0) {
                if (!alliances.contains(allianceId)) {
                    currElements++;
                    alliances.add(allianceId);
                } else {
                    continue;
                }
            } else {
                players.add(userId);
                currElements++;
            }

            if (currElements == VOTABLE_ITEMS) break;
        }

        System.out.println("Votable alliances: " + alliances.size());
        for (Integer aId : alliances) {
            Alliance a = aDAO.findById(aId);
            System.out.println("Alliance " + a.getName());
        }
        System.out.println("Votable players: " + players.size());
        for (Integer uId : players) {
            System.out.println("Player " + IdToNameService.getUserName(uId));
        }

        ArrayList<User> uList = uDAO.findAll();
        for (Iterator<User> uIt = uList.iterator();uIt.hasNext();) {
            User u = uIt.next();
            if ((u.getActive() == false) || (u.getTrial())) uIt.remove();
        }

        try {
            // AllianceMember amReq = amDAO.findByUserId(reqUserId);
            // Alliance a = aDAO.findById(amReq.getAllianceId());

            Vote v = null;
            if (true) {
                HashMap<String,CharSequence> msgReplacements = new HashMap<String,CharSequence>();
                HashMap<String,CharSequence> sbjReplacements = new HashMap<String,CharSequence>();

                /*
                sbjReplacements.put("%USER%",IdToNameService.getUserName(reqUserId));

                msgReplacements.put("%USER%",IdToNameService.getUserName(reqUserId));
                msgReplacements.put("%NEWLEADERSHIP%",newLeadership.toString());
                */

                VotingTextMLSecure vtML = new VotingTextMLSecure("galaxy_victory_vote","galaxy_victory_vote_sbj",msgReplacements,sbjReplacements);
                v = new Vote(0, vtML, Voting.TYPE_VOTEROUNDEND);
            }

            ArrayList<Integer> receivers = new ArrayList<Integer>();
            // ArrayList<AllianceMember> memberList = amDAO.findByAllianceId(a.getId());

            for (User u : (ArrayList<User>)uDAO.findAll()) {
                if (u.getTrial() || !u.getActive() || !u.getSystemAssigned()) continue;

                receivers.add(u.getUserId());

                Statistic s = sDAO.findByUserId(u.getUserId());
                int votePower = (int)Math.ceil(s.getPoints() / 1000d);

                v.addMetaData("VOTEPOWER" + u.getUserId(), votePower, MetaDataDataType.INTEGER);
                v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, u.getUserId()));
            }

            v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));
            v.setMinVotes(0);
            v.setTotalVotes(receivers.size());

            for (Integer aId : alliances) {
                Alliance a2 = aDAO.findById(aId);
                v.addOption(new VoteOption("[%$vote_option_alliance$%] " + a2.getName(),false, VoteOption.OPTION_TYPE_VALUE));
            }
            System.out.println("Votable players: " + players.size());
            for (Integer uId : players) {
                v.addOption(new VoteOption("[%$vote_option_player$%] " + IdToNameService.getUserName(uId),false, VoteOption.OPTION_TYPE_VALUE));
            }

            v.addOption(new VoteOption("vote_option_abstain",false, VoteOption.OPTION_TYPE_VALUE, true));

            // v.addOption(new VoteOption("alliance_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
            // v.addOption(new VoteOption("alliance_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

            // v.addMetaData("NEWSTATE", newLeadership, MetaDataDataType.STRING);
            // v.addMetaData("ALLIANCEID", a.getId(), MetaDataDataType.INTEGER);

            v.setReceivers(receivers);

            VotingFactory.persistVote(v);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on leadership state change vote: ", e);
        }
    }
}
