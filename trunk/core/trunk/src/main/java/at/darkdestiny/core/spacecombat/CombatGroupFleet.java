/*
 * CombatGroupFleet.java
 *
 * Created on 22. Oktober 2005, 16:00
 */

package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.util.DebugBuffer;
import java.util.*;

public class CombatGroupFleet {
    private int groupId;
    private int fleetId;
    private int fleetFormationId;
    private String fleetName;
    private int userId;
    private int destPlanet;

    private int retreatFactor;
    private ELocationType retreatToType = null;
    private int retreatTo = 0;

    private int shipCount;
    private int actShipCount;
    private Map<DesignDescriptor, CombatUnit> shipDesign;
    private boolean planetDefense;
    private boolean systemDefense;
    private boolean defenseMode;

    private boolean retreated = false;
    private boolean resigned = false;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getFleetId() {
        return fleetId;
    }

    public String getFleetIdStr() {
        return Integer.toString(fleetId);
    }

    public void setFleetId(int fleetId) {
        this.fleetId = fleetId;
    }

    public int getUserId() {
        return userId;
    }

    public String getUserIdStr() {
        return Integer.toString(userId);
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getDestPlanet() {
        return destPlanet;
    }

    public void setDestPlanet(int destPlanet) {
        this.destPlanet = destPlanet;
    }

    public int getRetreatFactor() {
        return retreatFactor;
    }

    public void setRetreatFactor(int retreatFactor) {
        this.retreatFactor = retreatFactor;
    }

    public int getShipCount() {
        return shipCount;
    }

    public void setShipCount(int shipCount) {
        this.shipCount = shipCount;
    }

    public int getActShipCount() {
        return actShipCount;
    }

    public void setActShipCount(int actShipCount) {
        this.actShipCount = actShipCount;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public Map<DesignDescriptor, CombatUnit> getShipDesign() {
        return shipDesign;
    }

    public void setShipDesign(Map<DesignDescriptor, CombatUnit> shipDesign) {
        this.shipDesign = shipDesign;
    }

    public boolean isPlanetDefense() {
        return planetDefense;
    }

    public void setPlanetDefense(boolean planetDefense) {
        this.planetDefense = planetDefense;
    }

    public boolean isSystemDefense() {
        return systemDefense;
    }

    public void setSystemDefense(boolean systemDefense) {
        this.systemDefense = systemDefense;
    }

    public CombatGroupFleet copy() {
        try {
            return (CombatGroupFleet)this.clone();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Cloning of CombatGroupFleet failed!",e);
        }
        return null;
    }

    public boolean isDefenseMode() {
        return defenseMode;
    }

    public void setDefenseMode(boolean defenseMode) {
        this.defenseMode = defenseMode;
    }

    public boolean isRetreated() {
        return retreated;
    }

    public void setRetreated(boolean retreated) {
        this.retreated = retreated;
    }

    public boolean isResigned() {
        return resigned;
    }

    public void setResigned(boolean resigned) {
        this.resigned = resigned;
    }

    public ELocationType getRetreatToType() {
        return retreatToType;
    }

    public void setRetreatToType(ELocationType retreatToType) {
        this.retreatToType = retreatToType;
    }

    public int getRetreatTo() {
        return retreatTo;
    }

    public void setRetreatTo(int retreatTo) {
        this.retreatTo = retreatTo;
    }

    /**
     * @return the fleetFormationId
     */
    public int getFleetFormationId() {
        return fleetFormationId;
    }

    /**
     * @param fleetFormationId the fleetFormationId to set
     */
    public void setFleetFormationId(int fleetFormationId) {
        this.fleetFormationId = fleetFormationId;
    }

    public final static class DesignDescriptor {
    	private final int id;
    	private final UnitTypeEnum type;

		public DesignDescriptor(int id, UnitTypeEnum type) {
			super();
			this.id = id;
			this.type = type;
		}

		public DesignDescriptor(int designId, int typeId) {
			this(designId, UnitTypeEnum.getTypeForId(typeId));
		}

		public int getId() {
			return id;
		}

		public UnitTypeEnum getType() {
			return type;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final DesignDescriptor other = (DesignDescriptor) obj;
			if (id != other.id)
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			return true;
		}

		public DesignDescriptor clone() {
			return new DesignDescriptor(id, type);
		}

    }

    public static enum UnitTypeEnum {
    	SURFACE_DEFENSE,
    	PLATTFORM_DEFENSE,
    	SHIP;

		public static UnitTypeEnum getTypeForId(int typeId) {
			switch (typeId) {
			case 1: return SURFACE_DEFENSE;
			case 2: return PLATTFORM_DEFENSE;
			default: return SHIP;
			}
		}
    }
}
