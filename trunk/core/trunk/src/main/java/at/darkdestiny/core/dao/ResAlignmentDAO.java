/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ResAlignment;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Bullet
 */
public class ResAlignmentDAO extends ReadWriteTable<ResAlignment> implements GenericDAO {

    public ResAlignment findById(int id) {
        ResAlignment ra = new ResAlignment();
        ra.setId(id);
        return (ResAlignment)get(ra);
    }
}
