/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.dao.DiplomacyTypeDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.EDiplomacyRelationType;
import at.darkdestiny.core.enumeration.EDoVoteRefType;
import at.darkdestiny.core.model.DiplomacyRelation;
import at.darkdestiny.core.model.DiplomacyType;
import at.darkdestiny.core.model.VoteMetadata;
import at.darkdestiny.core.model.VoteOption;
import at.darkdestiny.core.model.Voting;
import at.darkdestiny.core.model.VotingDetail;
import at.darkdestiny.core.service.DiplomacyService;
import at.darkdestiny.core.utilities.LanguageUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class DefaultVoteWrapper extends GenericVoteWrapper {
    protected static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    protected static DiplomacyTypeDAO dtDAO = (DiplomacyTypeDAO) DAOFactory.get(DiplomacyTypeDAO.class);
    protected static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    
    @Override
    public String generateVoteBody(Vote v, int id, int userId) throws VoteException {
        return generateVoteBody(v, id, userId, null, 0);
    }

    @Override
    public String generateVoteBody(Vote v, int id, int userId, EDoVoteRefType refType, int refId) throws VoteException {
        StringBuilder voteBody = new StringBuilder();

        /*
        if (v.isMsgTranslation()) {
        voteBody.append(v.getVotingTextML().getMessage(userId));
        } else {
        voteBody.append(v.getVoteMessage());
        }
         */

        if (v.isClosed()) {
            return "";

            // Also return if user has already voted

        }
        VotingDetail vd = vdDAO.findByVoteIdAndUser(id, userId);
        if (vd != null) {
            return "";


        }

        voteBody.append("<BR>");

        String backLink = "";
        if (refType != null) {
            if (refType == EDoVoteRefType.MESSAGE) {
                backLink = "&msgId="+refId;
            }
        }

        voteBody.append("<form method=\"post\" action=\"main.jsp?page=doVote&voteId=");
        voteBody.append(id);
        voteBody.append(backLink);
        voteBody.append("\">");
        voteBody.append("<p>");

        ArrayList<VoteOption> vos = voDAO.findByVoteId(id);

        //dbg log.debug("id " + id);
        //dbg log.debug("Vos : " + vos);
        //dbg log.debug("VosSize : " + vos.size());
        for (VoteOption vo : vos) {
            //dbg log.debug("Vo : " + vo);
            //dbg log.debug("Vopres : " + vo.getPreSelect());
            String voteOptionText = vo.getVoteOption();
            if (vo.isConstant()) {
                voteOptionText = ML.getMLStr(voteOptionText, userId);
            } else {
                Locale l = LanguageUtilities.getMasterLocaleForUser(userId);
                voteOptionText = VoteUtilities.translateParametrizedString(vo.getVoteOption(),l);
            }

            if (vo.getPreSelect()) {
                voteBody.append("<input checked=\"checked\" type=\"radio\" name=\"voteoption\" value=\"");
                voteBody.append(vo.getId().toString());
                voteBody.append("\"> ");
                voteBody.append(voteOptionText);
                voteBody.append("<br>");
            } else {
                voteBody.append("<input type=\"radio\" name=\"voteoption\" value=\"");
                voteBody.append(vo.getId().toString());
                voteBody.append("\"> ");
                voteBody.append(voteOptionText);
                voteBody.append("<br>");
            }
        }

        voteBody.append("</p>");
        voteBody.append("<input type=\"submit\" value=\"");
        voteBody.append(ML.getMLStr("vote_but_vote", userId));
        voteBody.append("\" />");
        voteBody.append("</form>");


        return voteBody.toString();
    }    
    
    @Override
    public VoteResult currentState(int userId, int voteId) throws VoteException {
        // log.debug("DEBUG 1");
        VoteResult vr;

        // Check if vote exists
        Vote v = VoteUtilities.getVote(voteId);
        if (v == null) {
            // log.debug("DEBUG 2");
            vr = new VoteResult(VoteResult.VOTE_NOT_EXISTS, null, null);
            return vr;
        } else {
            // log.debug("DEBUG 3 " + v.getCloseOption());
            // Get all votes and options
            HashMap<Integer, Integer> userVotes = new HashMap<Integer, Integer>();
            HashMap<Integer, VoteOption> options = new HashMap<Integer, VoteOption>();

            try {
                ArrayList<VoteOption> voList = voDAO.findByVoteId(voteId);

                for (VoteOption vo : voList) {
                    ArrayList<VotingDetail> vdList = vdDAO.findByVoteIdAndOption(voteId, vo.getId());

                    for (VotingDetail vd : vdList) {
                        if (vd.getUserId() != 0) {
                            userVotes.put(vd.getUserId(), vd.getOptionVoted());
                        }
                    }

                    if (!options.containsKey(vo.getId())) {
                        options.put(vo.getId(), vo);
                    }
                }

            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in vote: ", e);
            }

            if (v.isClosed()) {
                // log.debug("CHECK 1 CLOSED");
                //dbg log.debug("va");
                if (v.getCloseOption() != 0) {
                    VoteOption vo = voDAO.findById(v.getCloseOption());
                    // log.debug("CHECK 2 " + vo.getType());

                    if (vo.getType() == VoteOption.OPTION_TYPE_ACCEPT) {
                        // log.debug("ACCEEEEEEEEEEEEEEPTED");
                        vr = new VoteResult(VoteResult.VOTE_CLOSED_ACCEPT, v.getCloseOption(), userVotes, options);
                    } else if (vo.getType() == VoteOption.OPTION_TYPE_DENY) {
                        vr = new VoteResult(VoteResult.VOTE_CLOSED_DENIED, v.getCloseOption(), userVotes, options);
                    } else {
                        vr = new VoteResult(VoteResult.VOTE_CLOSED_OPTION, v.getCloseOption(), userVotes, options);
                    }

                    return vr;
                } else {
                    vr = new VoteResult(VoteResult.VOTE_CLOSED_DENIED, userVotes, options);
                    return vr;
                }
            }

            vr = new VoteResult(VoteResult.VOTE_STATE, userVotes, options);
        }

        return vr;
    }    
    
    @Override
    public String getVoteStateHTML(VoteResult vr, int voteId, Locale locale) {
        String message = "<BR/><BR/>";

        Vote v = VoteUtilities.getVote(voteId);
        HashMap<Integer, VoteOption> vo = vr.getOptions();

        //dbg log.debug("VO Size = " + vo.size());

        boolean closed = false;

        int optionYes = -1;
        int optionNo = -1;


        HashMap<Integer, ArrayList<String>> voted = new HashMap<Integer, ArrayList<String>>();


        /**
        for (Map.Entry<Integer, VoteOption> me : vo.entrySet()) {
        if (me.getValue().getType() == VoteOption.OPTION_TYPE_ACCEPT) {
        //dbg log.debug("Setting optionYes1from : " + optionYes + " to " + me.getKey());
        optionYes = me.getKey();
        }
        if (me.getValue().getType() == VoteOption.OPTION_TYPE_DENY) {
        //dbg log.debug("Setting optionNo from : " + optionNo + " to " + me.getKey());
        optionNo = me.getKey();
        }
        }*/
        for (Map.Entry<Integer, Integer> me : vr.getUserVotes().entrySet()) {
            String userName;
            userName = uDAO.findById(me.getKey()).getGameName();

            //dbg log.debug("Found voted option " + me.getValue() + " for user " + userName);
            VoteOption voteOption = voDAO.findById(me.getValue());
            //dbg log.debug("Vote option " + voteOption);
            if (voteOption != null) {
                if (voteOption.getType() == VoteOption.OPTION_TYPE_ACCEPT) {
                    //dbg log.debug("Incrementing Option Yes");
                    optionYes++;
                } else if (voteOption.getType() == VoteOption.OPTION_TYPE_DENY) {
                    //dbg log.debug("Incrementing Option No");
                    optionNo++;
                }
            }
            if (!voted.containsKey(me.getValue())) {
                ArrayList<String> users = new ArrayList<String>();
                users.add(userName);
                voted.put(me.getValue(), users);
            } else {
                voted.get(me.getValue()).add(userName);
            }
        }

        System.out.println("VOTE CODE: " + vr.getVoteCode());
        
        if ((vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_DENIED) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_OPTION)) {
            closed = true;

            message += "<B><FONT color=\"yellow\">";
            message += "Diese Abstimmung wurde ";
            if ((vr.getVoteCode() == VoteResult.VOTED_ACCEPT) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT)) {
                message += "angenommen!";
            } else {
                message += "abgelehnt!";
            }
            message += "</FONT></B><BR><BR>";
        } else {
            if (vr.getVoteCode() == VoteResult.VOTE_ALREADY_VOTED) {
                /*
                message += "Diese Abstimmung wurde ";
                if (optionYes > optionNo) {
                message += "angenommen!";
                } else {
                message += "abgelehnt!";
                }
                message += "<BR><BR>";
                 */
            } else {
                message += "Diese Abstimmung l&auml;uft noch!<BR><BR>";
            }
        }

        if (v.getVoteMessage().length() > 0) {
            message += GenerateMessage.parseText(v.getVoteMessage(),true) + "<BR><BR>";
        }

        if (closed) {
            message += "Abstimmungsergebnis:<BR>";
        } else {
            message += "Vorl&auml;ufiges Abstimmungsergebnis:<BR>";
        }

        boolean firstUser = true;
        boolean userFound = false;

        message += "Ben&ouml;tigte Stimmen: " + v.getMinVotes() + "<BR>";

        for (Map.Entry<Integer, VoteOption> me : vo.entrySet()) {
            //dbg log.debug("Checking option " + me.getValue().getVoteOption() + " with id " + me.getKey());

            int count = 0;
            String users = "";

            if (voted.containsKey(me.getKey())) {
                //dbg log.debug("Voted contains this option (" + me.getKey() + ")");

                ArrayList<String> userVotes = voted.get(me.getKey());
                for (String userName : userVotes) {
                    count++;
                    if (users.equalsIgnoreCase("")) {
                        users += userName;
                    } else {
                        users += ", " + userName;
                    }
                }
            } else {
                count = 0;
            }

            if (me.getValue().isConstant()) {
                message += ML.getMLStr(me.getValue().getVoteOption(), locale);
            } else {
                // Check for parametrized constants and translate
                String baseContent = me.getValue().getVoteOption();
                String replacedContent = VoteUtilities.translateParametrizedString(baseContent, locale);

                // message += me.getValue().getVoteOption();
                message += replacedContent;
            }

            message += " (" + count + "): " + users;
            message += "<BR>";
        }

        return message;
    }    
    
    @Override
    public String getHtmlCode(Vote vote, int voteId, int userId) {
        String html = "";

        if (vote.isClosed()) {
            return "";


        }
        if (vote.getType() == Voting.TYPE_ALLIANCERELATION
                || vote.getType() == Voting.TYPE_USERRELATION
                || vote.getType() == Voting.TYPE_ALLIANCEUSERRELATION
                || vote.getType() == Voting.TYPE_USERALLIANCERELATION
                || vote.getType() == Voting.TYPE_ALLIANCERELATION_REQUEST) {

            Integer fromId = (Integer) vote.getMetaData(VoteMetadata.NAME_FROMID);
            Integer toId = (Integer) vote.getMetaData(VoteMetadata.NAME_TOID);
            Integer diplomacyTypeId = (Integer) vote.getMetaData(VoteMetadata.NAME_TYPE);
            DiplomacyType dt = dtDAO.findById(diplomacyTypeId);

            EDiplomacyRelationType drType = null;

            String name1 = "";
            // String name2 = "";
            String msg = "";
            String voteText;
            DiplomacyRelation dr = DiplomacyService.findRelationBy(fromId, toId, EDiplomacyRelationType.USER_TO_USER, false);
            DiplomacyType actRel = dtDAO.findById(DiplomacyType.NEUTRAL);
            if (dr != null) {
                actRel = dtDAO.findById(dr.getDiplomacyTypeId());
            }
            try {
                msg += (String) vote.getMetaData(VoteMetadata.NAME_MESSAGE);
            } catch (Exception e) {
            }
            if (vote.getType() == Voting.TYPE_ALLIANCERELATION) {
                name1 = aDAO.findById(fromId).getName();
                // name2 = aDAO.findById(fromId).getName();
                drType = EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE;
            } else if (vote.getType() == Voting.TYPE_USERRELATION) {
                name1 = uDAO.findById(fromId).getGameName();
                // name2 = uDAO.findById(fromId).getGameName();
                drType = EDiplomacyRelationType.USER_TO_USER;
            } else if (vote.getType() == Voting.TYPE_ALLIANCEUSERRELATION) {
                name1 = aDAO.findById(fromId).getName();
                // name2 = uDAO.findById(fromId).getGameName();
                drType = EDiplomacyRelationType.ALLIANCE_TO_USER;
            } else if (vote.getType() == Voting.TYPE_USERALLIANCERELATION) {
                name1 = uDAO.findById(fromId).getGameName();
                // name2 = aDAO.findById(fromId).getName();
                drType = EDiplomacyRelationType.USER_TO_ALLIANCE;
            }
            voteText = name1 + " " + ML.getMLStr("diplomacy_msg_changerequest1", userId) + " <b> " + ML.getMLStr(actRel.getName(), userId) + " </b> " + ML.getMLStr("diplomacy_msg_changerequest2", userId) + ": <b>" + ML.getMLStr(dt.getName(), userId) + "</b> " + ML.getMLStr("diplomacy_msg_changerequest3", userId) + " <BR>";

            html += "<center>" + name1 + " : " + ML.getMLStr(actRel.getName(), userId) + " => " + ML.getMLStr(dt.getName(), userId) + "</center>";

            html += "<br><br>";
            html += voteText + "<br><br>";
            html += msg;
            try {
                html += generateVoteBody(vote, voteId, userId);
            } catch (VoteException e) {
                DebugBuffer.writeStackTrace("Error creating vote HTML code: ", e);
            }
        }

        return html;
    }    
}
