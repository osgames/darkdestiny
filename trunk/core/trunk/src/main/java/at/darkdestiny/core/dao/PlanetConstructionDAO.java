/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlanetConstructionDAO extends ReadWriteTable<PlanetConstruction> implements GenericDAO {
    public ArrayList<PlanetConstruction> findByPlanetId(Integer planetId) {
        PlanetConstruction pc = new PlanetConstruction();
        pc.setPlanetId(planetId);
        return find(pc);
    }

    public PlanetConstruction findBy(Integer planetId, Integer constructionId) {
        PlanetConstruction pc = new PlanetConstruction();
        pc.setPlanetId(planetId);
        pc.setConstructionId(constructionId);
        return (PlanetConstruction)get(pc);
    }

    public PlanetConstruction create(PlanetConstruction u) {
        PlanetConstruction uNew = this.add(u);
        return uNew;
    }

    public boolean isConstructed(int planetId, Integer constructionId) {
        PlanetConstruction pc = findBy(planetId, constructionId);
        if (pc != null) {
            return true;
        } else {
            return false;
        }
    }
}
