/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view;

import at.darkdestiny.core.model.TradeRouteDetail;

/**
 *
 * @author Orlov
 */
public class TradeRouteView {
    private TradeRouteDetail trd;
    private int targetPlanet;

    public TradeRouteView(TradeRouteDetail trd, int targetPlanet) {
        this.trd = trd;
        this.targetPlanet = targetPlanet;
    }

    /**
     * @return the trd
     */
    public TradeRouteDetail getTrd() {
        return trd;
    }

    /**
     * @return the targetPlanet
     */
    public int getTargetPlanet() {
        return targetPlanet;
    }

}
