/*
 * ConfirmBuffer.java
 *
 * Created on 04. Oktober 2006, 16:48
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.darkdestiny.core.databuffer;

import java.util.*;

public class ConfirmBuffer {

    private static HashMap<Integer,ConfirmEntry> confirmQueue = new HashMap<Integer,ConfirmEntry>();

    public ConfirmBuffer() {

    }

    public static int addNewConfirmEntry(Map<String, Object> pMap, int userId, int type) {

        long time = System.currentTimeMillis();
        String part1 = String.valueOf(time);
        String part2 = String.valueOf(userId);
        String identStr = part1.substring(part1.length()-3) + part2;

        int identNo = Integer.parseInt(identStr);

        ConfirmEntry ce = new ConfirmEntry();
        ce.setEntryTime(identNo);
        ce.setUserId(userId);
        ce.setParMap(new TreeMap<String, Object>(pMap));
        ce.setType(type);
        confirmQueue.put(identNo,ce);

        return identNo;
    }

    public static boolean deleteConfirmEntry(int queueIdent) {
        if (confirmQueue.containsKey(queueIdent)) {
            confirmQueue.remove(queueIdent);
            return true;
        }

        return false;
    }

    public static ConfirmEntry getParameters(int queueIdent) {
        if (confirmQueue.containsKey(queueIdent)) {
            return confirmQueue.get(queueIdent);
        }

        return null;
    }
}
