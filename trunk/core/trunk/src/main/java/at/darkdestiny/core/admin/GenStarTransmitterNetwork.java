/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.admin;

import at.darkdestiny.core.dao.*;
import at.darkdestiny.core.databuffer.fleet.*;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.enumeration.EPlanetLogType;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlanetLog;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.model.SunTransmitter;
import at.darkdestiny.core.model.SunTransmitterRoute;
import at.darkdestiny.core.model.System;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.tools.graph.DefaultNode;
import at.darkdestiny.core.tools.graph.Edge;
import at.darkdestiny.core.tools.graph.Graph;
import at.darkdestiny.framework.QueryKey;
import at.darkdestiny.framework.QueryKeySet;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class GenStarTransmitterNetwork {

    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static GalaxyDAO gDAO = (GalaxyDAO) DAOFactory.get(GalaxyDAO.class);
    private static SunTransmitterDAO stDAO = (SunTransmitterDAO) DAOFactory.get(SunTransmitterDAO.class);
    private static SunTransmitterRouteDAO strDAO = (SunTransmitterRouteDAO) DAOFactory.get(SunTransmitterRouteDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);

    public void resetTransmitterNetwork() {
        for (SunTransmitter st : stDAO.findAll()) {
            int planetId = st.getPlanetId();

            QueryKeySet qks = new QueryKeySet();
            QueryKey qk = new QueryKey("planetId", planetId);
            qks.addKey(qk);
            
            pcDAO.removeAll(qks);

            PlayerPlanet pp = ppDAO.findById(planetId);
            ppDAO.remove(pp);

            pdDAO.removeAll(qks);
            ptDAO.removeAll(qks);
        }

        stDAO.removeAll();
        strDAO.removeAll();
    }

    public void generate() {
        // Select a random star from a galaxy and place a transmitter there
        // depending on size of the galaxy there can only be a certain amount of transmitters
        resetTransmitterNetwork();

        // stDAO.removeAll();
        // strDAO.removeAll();

        ArrayList<Integer> unprocessedSystems = new ArrayList<Integer>();
        HashMap<Integer, ArrayList<System>> transmitters =
                new HashMap<Integer, ArrayList<System>>();
        HashMap<Integer, Integer> transmitterCountNeed =
                new HashMap<Integer, Integer>();

        for (System s : (ArrayList<System>) sDAO.findAll()) {
            unprocessedSystems.add(s.getId());
        }

        for (Galaxy g : (ArrayList<Galaxy>) gDAO.findAll()) {
            int transmitterCount = (int) Math.ceil((g.getEndSystem() - g.getStartSystem()) / 1000d);
            transmitterCountNeed.put(g.getId(), transmitterCount);
        }

        // Get a random system
        // Determine Galaxy Id .. check if max Count is reached
        // If yes continue -- otherwise check if there already is a transmitter and if yes define a minimum distance
        boolean satisfied = false;

        TransactionHandler th = TransactionHandler.getTransactionHandler();
        th.startTransaction();

        try {
            while (!satisfied && (unprocessedSystems.size() > 0)) {
                int currSys = unprocessedSystems.get((int) Math.floor(unprocessedSystems.size() * Math.random()));

                System actSystem = sDAO.findById(currSys);
                Integer galaxyId = actSystem.getGalaxyId();

                Galaxy g = gDAO.findById(galaxyId);


                if (transmitters.get(g.getId()) == null) {
                    // We are planting the first transmitter to this galaxy - ITS OK
                    ArrayList<System> sList = new ArrayList<System>();
                    sList.add(actSystem);
                    transmitters.put(g.getId(), sList);

                    java.lang.System.out.println("[BASIC] Created Suntransmitter at " + actSystem.getX() + ":" + actSystem.getY());

                    // buildTransmitterPlanet(actSystem.getId());
                } else {
                    // Check if this galaxy already has maximum transmitter Count
                    int allowed = transmitterCountNeed.get(g.getId());
                    int created = transmitters.get(g.getId()).size();

                    if (created < allowed) {
                        // We can do it .. ITS OKAY
                        double minDistance = ((double) g.getWidth() + (double) g.getHeight()) / 4d;

                        AbsoluteCoordinate acCurr = new AbsoluteCoordinate(actSystem.getX(), actSystem.getY());
                        System newToAdd = null;

                        boolean distanceOK = true;

                        for (System compSys : transmitters.get(g.getId())) {
                            AbsoluteCoordinate acComp = new AbsoluteCoordinate(compSys.getX(), compSys.getY());

                            if (acCurr.distanceTo(acComp) >= minDistance) {
                                // ArrayList<System> sList = transmitters.get(g.getId());
                                // sList.add(actSystem);
                                java.lang.System.out.println("DISTANCE IS " + acCurr.distanceTo(acComp) + " MINDISTANCE=" + minDistance);
                                newToAdd = actSystem;
                            } else {
                                distanceOK = false;
                            }
                        }

                        if (distanceOK) {
                            if (newToAdd != null) {
                                ArrayList<System> sList = transmitters.get(g.getId());
                                sList.add(actSystem);
                                java.lang.System.out.println("[ADDITIONAL] Created Suntransmitter at " + actSystem.getX() + ":" + actSystem.getY());
                            }
                        }
                    } else {
                        java.lang.System.out.println("Max count reached [" + created + ">=" + allowed + "]");
                    }
                }

                unprocessedSystems.remove((Integer) currSys);
            }

            // Create Database Entries
        /* HashMap<Integer,ArrayList<System>> transmitters =
             new HashMap<Integer,ArrayList<System>>();  */
            Graph<DefaultNode> g = new Graph<DefaultNode>();

            for (Map.Entry<Integer, ArrayList<System>> entry : transmitters.entrySet()) {
                int i = 0;

                for (System s : entry.getValue()) {
                    i++;

                    int planetId = buildTransmitterPlanet(s.getId());

                    // This will be found after above function was executed
                    User aiUser = new User();
                    aiUser.setGameName("Lemurer");
                    aiUser.setUserName("Lemurer");
                    ArrayList<User> aiUserResult = uDAO.find(aiUser);

                    SunTransmitter st = new SunTransmitter();
                    st.setSystemId(s.getId());
                    st.setPlanetId(planetId);
                    st.setUserId(aiUserResult.get(0).getId());
                    st = stDAO.add(st);

                    SunTransmitterEntry ste = new SunTransmitterEntry(st, entry.getKey());
                    DefaultNode n = new DefaultNode("Trans " + entry.getKey() + "-" + i, ste);

                    g.addNode(n);
                }
            }

            // All nodes have been created
            // Connect intra Galaxy
            ArrayList<Integer> unConnectedGalaxies = new ArrayList<Integer>();
            ArrayList<Integer> connectedGalaxies = new ArrayList<Integer>();

            for (Galaxy gal : gDAO.findAll()) {
                unConnectedGalaxies.add(gal.getId());

                // Get all Nodes for a galaxy
                int galId = gal.getId();

                ArrayList<DefaultNode> unConnected = new ArrayList<DefaultNode>();
                ArrayList<DefaultNode> connected = new ArrayList<DefaultNode>();

                for (DefaultNode n : g.getNodes()) {
                    SunTransmitterEntry ste = (SunTransmitterEntry) n.getLoad();
                    if (ste.galaxyId == galId) {
                        unConnected.add(n);
                    }
                }

                switch (unConnected.size()) {
                    case (1): // Nothing to do
                        continue;
                    case (2): // Trivial
                        g.addEdge(unConnected.get(0), unConnected.get(1));
                        continue;
                    case (3): // Build randomized network
                        while (!unConnected.isEmpty()) {
                            if (connected.isEmpty()) {
                                // Connect 2 random unconnected nodes
                                DefaultNode n1 = unConnected.get((int) Math.floor(Math.random() * unConnected.size()));
                                DefaultNode n2 = unConnected.get((int) Math.floor(Math.random() * unConnected.size()));
                                while (n1.equals(n2)) {
                                    n2 = unConnected.get((int) Math.floor(Math.random() * unConnected.size()));
                                }

                                unConnected.remove(n1);
                                unConnected.remove(n2);
                                connected.add(n1);
                                connected.add(n2);

                                g.addEdge(n1, n2);
                            } else if (!unConnected.isEmpty()) {
                                // Connect a free node to a random assigned node
                                DefaultNode n1 = unConnected.get((int) Math.floor(Math.random() * unConnected.size()));
                                DefaultNode n2 = connected.get((int) Math.floor(Math.random() * connected.size()));

                                g.addEdge(n1, n2);
                                unConnected.remove(n1);
                                connected.add(n1);
                            }
                        }
                }
            }

            // We have connected all galaxies inside
            // Now we have to connect the galaxies to each other
            while (!unConnectedGalaxies.isEmpty()) {
                if (connectedGalaxies.isEmpty()) {
                    // Connect 2 random nodes from 2 random galaxies
                    int gal1 = unConnectedGalaxies.get((int) Math.floor(Math.random() * unConnectedGalaxies.size()));
                    int gal2 = unConnectedGalaxies.get((int) Math.floor(Math.random() * unConnectedGalaxies.size()));

                    while (gal1 == gal2) {
                        gal2 = unConnectedGalaxies.get((int) Math.floor(Math.random() * unConnectedGalaxies.size()));
                    }

                    ArrayList<DefaultNode> nodesGal1 = new ArrayList<DefaultNode>();
                    ArrayList<DefaultNode> nodesGal2 = new ArrayList<DefaultNode>();

                    for (DefaultNode n : g.getNodes()) {
                        SunTransmitterEntry ste = (SunTransmitterEntry) n.getLoad();
                        if (ste.galaxyId == gal1) {
                            nodesGal1.add(n);
                        } else if (ste.galaxyId == gal2) {
                            nodesGal2.add(n);
                        }
                    }

                    DefaultNode n1 = nodesGal1.get((int) Math.floor(nodesGal1.size() * Math.random()));
                    DefaultNode n2 = nodesGal2.get((int) Math.floor(nodesGal2.size() * Math.random()));

                    g.addEdge(n1, n2);

                    unConnectedGalaxies.remove((Integer) gal1);
                    unConnectedGalaxies.remove((Integer) gal2);
                    connectedGalaxies.add(gal1);
                    connectedGalaxies.add(gal2);
                } else if (!unConnectedGalaxies.isEmpty()) {
                    // Connect a free node to a random assigned node
                    int gal1 = unConnectedGalaxies.get((int) Math.floor(Math.random() * unConnectedGalaxies.size()));
                    int gal2 = connectedGalaxies.get((int) Math.floor(Math.random() * connectedGalaxies.size()));

                    ArrayList<DefaultNode> nodesGal1 = new ArrayList<DefaultNode>();
                    ArrayList<DefaultNode> nodesGal2 = new ArrayList<DefaultNode>();

                    for (DefaultNode n : g.getNodes()) {
                        SunTransmitterEntry ste = (SunTransmitterEntry) n.getLoad();
                        if (ste.galaxyId == gal1) {
                            nodesGal1.add(n);
                        } else if (ste.galaxyId == gal2) {
                            nodesGal2.add(n);
                        }
                    }

                    DefaultNode n1 = nodesGal1.get((int) Math.floor(nodesGal1.size() * Math.random()));
                    DefaultNode n2 = nodesGal2.get((int) Math.floor(nodesGal2.size() * Math.random()));

                    g.addEdge(n1, n2);

                    unConnectedGalaxies.remove((Integer) gal1);
                    connectedGalaxies.add(gal1);
                }
            }

            // Add all determined routes
            for (Edge e : g.getEdges()) {
                SunTransmitterRoute str = new SunTransmitterRoute();
                str.setStartId(((SunTransmitterEntry) e.getNode1().getLoad()).stObj.getId());
                str.setEndId(((SunTransmitterEntry) e.getNode2().getLoad()).stObj.getId());
                strDAO.add(str);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while creating Suntransmitter system: ", e);

            try {
                th.rollback();
                DebugBuffer.addLine(DebugBuffer.DebugLevel.ERROR, th.getTransactionLog());
            } catch (TransactionException te) {
                DebugBuffer.writeStackTrace("Error during Transaction Handling: ", te);
            }
        } finally {
            th.endTransaction();
        }
    }

    private int buildTransmitterPlanet(int systemId) {
        ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
        if (pList.isEmpty()) {
            // Create planet
        }

        Planet randomPlanet = pList.get((int) Math.floor(Math.random() * pList.size()));

        // Get AI user
        User aiUser = new User();
        aiUser.setGameName("Lemurer");
        aiUser.setUserName("Lemurer");
        ArrayList<User> aiUserResult = uDAO.find(aiUser);

        if (aiUserResult.isEmpty()) {
            // Create AI User
            User newAiUser = new User();
            newAiUser.setGameName("Lemurer");
            newAiUser.setUserName("Lemurer");
            // TODO - Your kidding me
            newAiUser.setPassword("xyz");
            newAiUser.setIp("0.0.0.0");
            newAiUser.setEmail("");
            newAiUser.setUserType(EUserType.AI);
            aiUser = uDAO.add(newAiUser);
        } else {
            aiUser = aiUserResult.get(0);
        }

        // Create PlayerPlanet
        PlayerPlanet pp = new PlayerPlanet();
        pp.setName("Sonnentransmitter #" + randomPlanet.getId());
        pp.setPlanetId(randomPlanet.getId());
        pp.setUserId(aiUser.getId());
        ppDAO.add(pp);

        // Create planetLog entry
        PlanetLog pl = new PlanetLog();
        pl.setPlanetId(randomPlanet.getId());
        pl.setUserId(aiUser.getId());
        pl.setType(EPlanetLogType.COLONIZED);
        pl.setTime(0);
        plDAO.add(pl);
        
        // Add Constructions
        /*
        PlanetConstruction pc = new PlanetConstruction();
        pc.setConstructionId(Construction.ID_SUNTRANSMITTER_FORTRESS);
        pc.setNumber(50);
        pc.setPlanetId(randomPlanet.getId());
        pcDAO.add(pc);
        */

        PlanetConstruction pc = new PlanetConstruction();
        pc.setConstructionId(Construction.ID_SUNTRANSMITTER_CONTROLLER);
        pc.setNumber(1);
        pc.setPlanetId(randomPlanet.getId());
        pcDAO.add(pc);

        // Add planet defense
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(randomPlanet.getId());
        pd.setSystemId(systemId);
        pd.setUnitId(Construction.ID_SUNTRANSMITTER_FORTRESS);
        pd.setType(EDefenseType.TURRET);
        pd.setUserId(aiUser.getId());
        pd.setCount(50);
        pdDAO.add(pd);

        // Add ground troops
        PlayerTroop pt = new PlayerTroop();
        pt.setUserId(aiUser.getId());
        pt.setPlanetId(randomPlanet.getId());
        pt.setTroopId(GroundTroop.ID_ROBOTER);
        pt.setNumber(1000000l);
        ptDAO.add(pt);

        return randomPlanet.getId();
    }

    private class SunTransmitterEntry {

        private final SunTransmitter stObj;
        private final int galaxyId;

        public SunTransmitterEntry(SunTransmitter stObj, int galaxyId) {
            this.stObj = stObj;
            this.galaxyId = galaxyId;
        }
    }
}
