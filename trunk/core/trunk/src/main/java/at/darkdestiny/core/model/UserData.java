/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "userdata")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class UserData extends Model<UserData> {

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("credits")
    private Long credits;
    @FieldMappingAnnotation("pic")
    @ColumnProperties(length = 500)
    @DefaultValue("")
    private String pic;
    @FieldMappingAnnotation("description")
    @ColumnProperties(length = 500)
    @DefaultValue("")
    private String description;
    @FieldMappingAnnotation("rating")
    private Integer rating;
    @FieldMappingAnnotation("titleId")
    @DefaultValue("0")
    private Integer titleId;
    @FieldMappingAnnotation("developementPoints")
    @DefaultValue("0")
    private Double developementPoints;
    @FieldMappingAnnotation("temporary")
    @DefaultValue("0")
    private Integer temporary;
    @FieldMappingAnnotation("weeklyIncome")
    @DefaultValue("0")
    private Long weeklyIncome;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the credits
     */
    public Long getCredits() {
        return credits;
    }

    /**
     * @param credits the credits to set
     */
    public void setCredits(Long credits) {
        this.credits = credits;
    }

    /**
     * @return the rating
     */
    public Integer getRating() {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    public void setRating(Integer rating) {
        this.rating = rating;
    }

    /**
     * @return the titleId
     */
    public Integer getTitleId() {
        return titleId;
    }

    /**
     * @param titleId the titleId to set
     */
    public void setTitleId(Integer titleId) {
        this.titleId = titleId;
    }

    /**
     * @return the developementPoints
     */
    public Double getDevelopementPoints() {
        return developementPoints;
    }

    /**
     * @param developementPoints the developementPoints to set
     */
    public void setDevelopementPoints(Double developementPoints) {
        this.developementPoints = developementPoints;
    }

    /**
     * @return the temporary
     */
    public Integer getTemporary() {
        return temporary;
    }

    /**
     * @param temporary the temporary to set
     */
    public void setTemporary(Integer temporary) {
        this.temporary = temporary;
    }

    /**
     * @return the pic
     */
    public String getPic() {
        return pic;
    }

    /**
     * @param pic the pic to set
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the weeklyIncome
     */
    public Long getWeeklyIncome() {
        return weeklyIncome;
    }

    /**
     * @param weeklyIncome the weeklyIncome to set
     */
    public void setWeeklyIncome(Long weeklyIncome) {
        this.weeklyIncome = weeklyIncome;
    }
}
