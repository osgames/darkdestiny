/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.service;

import at.darkdestiny.core.model.MultiLog;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Dreloc
 */
public class MultiService extends Service{


    public static TreeMap<String, TreeMap<Integer, TreeMap<Integer, MultiLog>>> findAllSorted(){

        TreeMap<String, TreeMap<Integer, TreeMap<Integer, MultiLog>>> result
                = new TreeMap<String, TreeMap<Integer, TreeMap<Integer, MultiLog>>>();
        for(MultiLog ml : (ArrayList<MultiLog>)multiLogDAO.findAll()){
            TreeMap<Integer, TreeMap<Integer, MultiLog>> ips = result.get(ml.getIp());
            if(ips == null){
                ips = new TreeMap<Integer, TreeMap<Integer, MultiLog>>();
            }
            TreeMap<Integer, MultiLog> ticks = ips.get(ml.getTick());
            if(ticks == null){
                ticks = new TreeMap<Integer, MultiLog>();
            }

            ticks.put(ml.getUserId(), ml);
            ips.put(ml.getTick(), ticks);
            result.put(ml.getIp(), ips);
        }

        return result;
    }

    public static void deleteByIp(Map<String, String[]> allPars){

            String ip = allPars.get("ip")[0];
           multiLogDAO.deleteByIp(ip);
    }
    public static void deleteByTick(Map<String, String[]> allPars){

            String ip = allPars.get("ip")[0];
            int tick = Integer.parseInt(allPars.get("tick")[0]);
           multiLogDAO.deleteByIpAndTick(ip, tick);
    }

    public static void deleteByEntry(Map<String, String[]> allPars){

            String ip = allPars.get("ip")[0];
            int tick = Integer.parseInt(allPars.get("tick")[0]);
            int userId = Integer.parseInt(allPars.get("userId")[0]);
           multiLogDAO.deleteBy(ip, tick, userId);
    }

}
