/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.WeaponModule;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public interface IDefenseBuilding extends ICombatUnit {
    public ArrayList<WeaponModule> getWeapons();
    public ShieldDefinition getShields();

    public void setSc_LoggingEntry(SC_DataEntry scde);
}
