/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.scanner.StarMapQuadTree;
import at.darkdestiny.core.scanner.SystemDesc;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Aion
 */
public class StarMapInfoData extends Service {
    private ArrayList<Integer> sharingUsers;
    private HashMap<Integer, ArrayList<Planet>> planetList;
    private HashMap<Integer, ArrayList<Hyperscanner>> hyperscanners;
    private HashMap<Integer, ArrayList<at.darkdestiny.core.model.System>> observatories;
    private StarMapQuadTree hyperArea;
    Integer userId = null;

    public StarMapInfoData() {
        
    }
    
    public StarMapInfoData(Integer userId) {
        this.userId = userId;
        init();
    }

    private void init() {
        sharingUsers = DiplomacyUtilities.findSharingUsers(userId);
        planetList = planetDAO.findAllSortedBySystem();
        hyperscanners = findHyperscanners(userId, getSharingUsers());
        hyperArea = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
        observatories = findObservatories(userId);

        //Adding All scanners to the quadtree
        for (Map.Entry<Integer, ArrayList<Hyperscanner>> entry0 : hyperscanners.entrySet()) {
            for (Hyperscanner entry : entry0.getValue()) {
                at.darkdestiny.core.model.System s = entry.getSystem();
                SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
                hyperArea.addItemToTree(sd);
            }
        }
    }

    private static HashMap<Integer, ArrayList<Hyperscanner>> findHyperscanners(int userId, ArrayList<Integer> sharingUsers) {
        HashMap<Integer, ArrayList<Hyperscanner>> hyperscanners = Maps.newHashMap();
        ArrayList<PlayerPlanet> pps = playerPlanetDAO.findAll();

        for (PlayerPlanet pp : pps) {
            if (pp.getUserId() != userId
                    && !sharingUsers.contains(pp.getUserId())) {
                continue;
            }
            
            if (planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_MILITARY_OUTPOST) != null) {
                ArrayList<Hyperscanner> entries = hyperscanners.get(pp.getUserId());

                if (entries == null) {
                    entries = new ArrayList<Hyperscanner>();
                }
                entries.add(new Hyperscanner(systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId()),StarMapInfo.MILITARYOUTPOST_HYPERSCANNER_RANGE));
                hyperscanners.put(pp.getUserId(), entries);                
            } else if (planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_HYPERSPACESCANNER) != null) {
                ArrayList<Hyperscanner> entries = hyperscanners.get(pp.getUserId());

                if (entries == null) {
                    entries = new ArrayList<Hyperscanner>();
                }
                entries.add(new Hyperscanner(systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId()),StarMapInfo.HYPERSCANNER_RANGE));
                hyperscanners.put(pp.getUserId(), entries);
            } 

        }
        return hyperscanners;


    }

    private static HashMap<Integer, ArrayList<at.darkdestiny.core.model.System>> findObservatories(int userId) {
        HashMap<Integer, ArrayList<at.darkdestiny.core.model.System>> observatories = new HashMap<Integer, ArrayList<at.darkdestiny.core.model.System>>();
        ArrayList<PlayerPlanet> pps = playerPlanetDAO.findAll();

        for (PlayerPlanet pp : pps) {
            if (pp.getUserId() != userId) {
                continue;
            }

            if (planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_OBSERVATORY) != null) {
                ArrayList<at.darkdestiny.core.model.System> entries = observatories.get(pp.getUserId());
                if (entries == null) {
                    entries = new ArrayList<at.darkdestiny.core.model.System>();
                }
                if (!entries.contains(systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId()))) {
                    entries.add(systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId()));
                }
                observatories.put(pp.getUserId(), entries);
            }
        }
        return observatories;
    }

    /**
     * @return the sharingUsers
     */
    public ArrayList<Integer> getSharingUsers() {
        return sharingUsers;
    }

    /**
     * @return the planetList
     */
    public HashMap<Integer, ArrayList<Planet>> getPlanetList() {
        return planetList;
    }

    /**
     * @return the hyperscanners
     */
    public HashMap<Integer, ArrayList<Hyperscanner>> getHyperscanners() {
        return hyperscanners;
    }

    /**
     * @return the hyperArea
     */
    public StarMapQuadTree getHyperArea() {
        return hyperArea;
    }

    /**
     * @return the observatories
     */
    public HashMap<Integer, ArrayList<at.darkdestiny.core.model.System>> getObservatories() {
        return observatories;
    }
}
