/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.SpecialTrade;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class SpecialTradeDAO extends ReadWriteTable<SpecialTrade> implements GenericDAO<SpecialTrade> {
    public SpecialTrade getByPlanetId(int id) {
        SpecialTrade st = new SpecialTrade();
        st.setPlanetId(id);
        return get(st);
    }    
}
