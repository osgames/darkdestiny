/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.Threading;

import at.darkdestiny.core.utilities.ViewTableUtilities;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ViewTableCopyJob extends AbstractCopyJob {
    private final int systemId;
    private final int time;
    private final ArrayList<Integer> toUsers;
    
    public ViewTableCopyJob(ArrayList<Integer> toUsers, int systemId, int time) {
        this.toUsers = toUsers;
        this.systemId = systemId;
        this.time = time;
    }

    public void processJob() {
        for (Integer userId : toUsers) {
            ViewTableUtilities.addOrRefreshSystemForUser(userId, systemId, time);
            try {
                Thread.sleep(10);
            } catch (Exception e) {
                DebugBuffer.error("ViewTableCopyJob failed [SystemId: " + systemId + " time: " + time + "]");
            }
        }        
    }
    
    public ArrayList<Integer> getToUsers() {
        return toUsers;
    }

    public int getSystemId() {
        return systemId;
    }

    public int getTime() {
        return time;
    }
}
