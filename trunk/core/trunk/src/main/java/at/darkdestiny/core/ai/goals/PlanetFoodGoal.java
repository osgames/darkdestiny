/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai.goals;

import at.darkdestiny.core.ai.AIDebugBuffer;
import at.darkdestiny.core.ai.AIDebugLevel;
import at.darkdestiny.core.ai.AIMessageHandler;
import at.darkdestiny.core.ai.AIMessagePayload;
import at.darkdestiny.core.ai.AIThread;
import at.darkdestiny.core.ai.ThreadSharedObjects;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Production;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.service.ConstructionService;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class PlanetFoodGoal extends AIGoal {

    private int goalId = 2;

    @Override
    public boolean processGoal(int userId) {
        PlanetCalculation pc = (PlanetCalculation) ThreadSharedObjects.getThreadSharedObject("PlanetCalculation");
        ProductionResult prFuture = pc.getProductionDataFuture();
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

        long population = pc.getPlayerPlanet().getPopulation();
        if (population <= 0) return false;
        
        long foodProductionFuture = prFuture.getRessBaseProduction(Ressource.FOOD);
        long foodConsumption = pc.getPlanetCalcData().getProductionData().getRessBaseConsumption(Ressource.FOOD);

        long futureBalance = foodProductionFuture - foodConsumption;

        float foodCoverageStock = 100;
        float planetFill = 100;
        
        if (pc.getPlanetCalcData().getProductionData().getRessConsumption(Ressource.FOOD) > 0) { 
            foodCoverageStock = pc.getPlanetCalcData().getProductionData().getRessStock(Ressource.FOOD)
                / pc.getPlanetCalcData().getProductionData().getRessConsumption(Ressource.FOOD);
        }
        if (epcr.getMaxPopulation() > 0) {
            planetFill = 100f / epcr.getMaxPopulation() * population;
        }

        int popIncreasePerTick = (int) (population / 360d * epcr.getGrowth());
        int estimatedFoodConsInc = popIncreasePerTick / 10000;

        long futureBalanceEstimated = 0;
        if (estimatedFoodConsInc > 0) {
            futureBalanceEstimated = futureBalance / estimatedFoodConsInc;
        }
        float rateValue = futureBalanceEstimated + foodCoverageStock;

        /*
         boolean small = (population < 1000000) && ;
         boolean medium = (population > 1000000) && ;
         boolean large = (population > 10000000l) && ;
         */

        // Get building / foodcons ratio get appropriate Building
        int level = -1;
        int increaseLimit = 100;
        double ratio = 0;

        int[] selectedAgriculture = {Construction.ID_SMALLAGRICULTUREFARM, Construction.ID_BIGAGRICULTUREFARM, Construction.ID_AGROCOMPLEX};

        while ((ratio < increaseLimit) && (level < 2)) {
            level++;
            ArrayList<Production> prodList = ConstructionService.findProductionForConstructionId(pc.getPlanetId(), selectedAgriculture[level]);

            long foodProduction = 0;
            for (Production pe : prodList) {
                if (pe.getRessourceId() == Ressource.FOOD) {
                    foodProduction = pe.getQuantity();
                }
            }

            ratio = (double) (foodProduction / (long) estimatedFoodConsInc);            
        }

        if (planetFill < 100d) {
            if (rateValue < 100) {
                if (level == 2) {
                    AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "[" + goalId + "/" + pc.getPlanetId() + "] foodCoverage < 100 => Build Complex");
                    sendDirectConstructionOrder(pc.getPlanetId(), Construction.ID_AGROCOMPLEX, AIGoal.MESSAGE_PRIORITY_HIGH);
                } else if (level == 1) {
                    AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "[" + goalId + "/" + pc.getPlanetId() + "] foodCoverage < 100 => Build Large");
                    sendDirectConstructionOrder(pc.getPlanetId(), Construction.ID_BIGAGRICULTUREFARM, AIGoal.MESSAGE_PRIORITY_HIGH);
                } else {
                    AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "[" + goalId + "/" + pc.getPlanetId() + "] foodCoverage < 100 => Build Small");
                    sendDirectConstructionOrder(pc.getPlanetId(), Construction.ID_SMALLAGRICULTUREFARM, AIGoal.MESSAGE_PRIORITY_HIGH);
                }
            }
        }

        return false;
    }

    @Override
    public void evaluateGoal(int userId) {
    }

    @Override
    public int getGoalId() {
        return goalId;
    }

    private void sendDirectConstructionOrder(int planetId, Integer construction, Integer priority) {
        AIMessagePayload amp = new AIMessagePayload();
        amp.addParameter("CONSTRUCTIONID", construction.toString());
        amp.addParameter("PRIORITY", priority.toString());

        AIMessageHandler aimh = new AIMessageHandler();
        aimh.sendMessage(goalId, 5, planetId, amp);
    }
}
