/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "constructionrestriction")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class ConstructionRestriction extends Model<ConstructionRestriction> {

    public static final Integer TYPE_BUILDING = 0;
    @FieldMappingAnnotation("type")
    @IdFieldAnnotation
    private Integer type;
    @FieldMappingAnnotation("consId")
    @IdFieldAnnotation
    private Integer constructionId;
    @FieldMappingAnnotation("restrictionClass")
    private String restrictionClass;

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the consId
     */
    public Integer getConstructionId() {
        return constructionId;
    }

    /**
     * @param consId the consId to set
     */
    public void setConstructionId(Integer constructionId) {
        this.constructionId = constructionId;
    }

    /**
     * @return the restrictionClass
     */
    public String getRestrictionClass() {
        return restrictionClass;
    }

    /**
     * @param restrictionClass the restrictionClass to set
     */
    public void setRestrictionClass(String restrictionClass) {
        this.restrictionClass = restrictionClass;
    }
}
