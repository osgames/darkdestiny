/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.update;

import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class UpdateCoordinator {

    private static final Logger log = LoggerFactory.getLogger(UpdateCoordinator.class);

    private static ArrayList<Updater2> updateThreads = new ArrayList<Updater2>();
    private static ArrayList<Thread> threadList = new ArrayList<Thread>();
    private static Updater2 masterThread;
    private static UpdateCoordinator instance = null;

    private int noOfThreads = 0;
    private int currTime = 0;

    private final Object mutex = new Object();
    private final Object mutex2 = new Object();

    private boolean errorState = false;
    private Exception errorCause = null;

    public static UpdateCoordinator getInstance() {
        if (instance == null) {
            instance = new UpdateCoordinator();
        }

        return instance;
    }

    public void createWorkerThreads(int number, int startTime) throws Exception {
        if (updateThreads.size() > 0) throw new Exception("UpdateCoordinator still in use!");
        if (number < 1) number = 1;

        errorState = false;

        currTime = startTime;
        noOfThreads = number;
        number--;

        masterThread = new Updater2(startTime);
        masterThread.initUpdater();
        updateThreads.add(masterThread);

        while (number > 0) {
            updateThreads.add(new Updater2(startTime));
            number--;
        }

        assignPlanets();

        int j = 1;
        for (Updater2 upd : updateThreads) {
            Thread updThread = new Thread(upd);
            updThread.setName("Update worker thread " + j);
            j++;
            threadList.add(updThread);
            updThread.start();
        }

        int readyThreads = 0;
        while (readyThreads < noOfThreads) {
            readyThreads = 0;

            for (Updater2 upd : updateThreads) {
                if (upd.ready) readyThreads++;
            }

            try {
                Thread.sleep(100);
            } catch (Exception e) {
                DebugBuffer.error("UC Error: ",e);
            }
        }
    }

    public Updater2 getMasterThread() {
        return masterThread;
    }

    public ArrayList<Updater2> getAllThreads() {
        ArrayList<Updater2> allThreads = new ArrayList<Updater2>();
        allThreads.addAll(updateThreads);

        return allThreads;
    }

    private void assignPlanets() {
        for (int i=0;i<masterThread.getPlanetList().size();i++) {
            int assignedThread = i % noOfThreads;
            // log.debug("Assign planet " + i + " to thread " + assignedThread);
            updateThreads.get(assignedThread).addPlanetJob(i);
        }
    }

    public void setCalculationTime(int calcTime) {
        currTime = calcTime;

        for (Updater2 upd : updateThreads) {
            upd.setCurrentTick(calcTime);
        }
    }

    public void finishUpdate() {
        for (Updater2 upd : updateThreads) {
            upd.stop = true;
        }

        synchronized (mutex) {
            mutex.notifyAll();
        }

        updateThreads.clear();
        threadList.clear();
        masterThread = null;
        noOfThreads = 0;
        currTime = 0;
    }

    public void calculateTick() {
        masterThread.preTickInit();

        for (Updater2 upd : updateThreads) {
            upd.processing = true;
        }

        synchronized (mutex) {
            mutex.notifyAll();
        }

        int finishedThreads = 0;
        int threadsInError = 0;
        while (finishedThreads < noOfThreads) {
            finishedThreads = 0;

            for (Updater2 upd : updateThreads) {
                if (!upd.processing) {
                    finishedThreads++;
                    if (upd.error) {
                        threadsInError++;
                        errorCause = upd.errorCause;
                    }
                }
            }

            try {
                Thread.sleep(100);
            } catch (Exception e) {
                DebugBuffer.error("UC Error: ",e);
            }
        }

        if (threadsInError > 0) {
            errorState = true;
            log.error("Processing stopped for Tick " + currTime + " due to Update Error");
        } else {
            masterThread.postTick();
            log.debug("Processing finished for Tick " + currTime);
        }
    }

    /**
     * @return the currTime
     */
    public int getCalculationTime() {
        return currTime;
    }

    protected void waitForProcessingStart() {
        try {
            synchronized (mutex) {
                mutex.wait();
            }
        } catch (Exception e) {
            DebugBuffer.error("UC Error: ",e);
        }
    }

    public void waitForProcessingEnd() {
        try {
            synchronized (mutex2) {
                log.debug("Updater waiting on MUTEX2");
                mutex2.wait();
            }
        } catch (Exception e) {
            DebugBuffer.error("UC Error: ",e);
        }
    }

    /**
     * @return the errorState
     */
    public boolean isErrorState() {
        return errorState;
    }

    /**
     * @return the errorCause
     */
    public Exception getErrorCause() {
        return errorCause;
    }
}
