/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Stefan
 */
public enum EPlanetLogType {
    COLONIZED, INVADED, ABANDONED, INVADED_HELP, TRANSFER;
}
