/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.development.conditions;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.ActiveBonusDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.core.model.ActiveBonus;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.Locale;

/**
 *
 * @author Stefan
 */
public class IndustrialColony implements IBonusCondition {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);
    private static ActiveBonusDAO abDAO = (ActiveBonusDAO)DAOFactory.get(ActiveBonusDAO.class);

    @Override
    public boolean isPossible(int planetId) {
        // Population larger than 500 Mio.
        // 75% work in agriculture
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        boolean hasGovernment = pcDAO.isConstructed(planetId, Construction.ID_PLANETGOVERNMENT);

        if (!hasGovernment) return false;
        if (abDAO.findByPlanetAndBonusId(pp.getPlanetId(), 7) != null) return false;
        
        try {
            /*
            PlanetCalculation pc = new PlanetCalculation(planetId);
            ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

            if (epcr.getPlanetEconomyWorkerPerc().get(EEconomyType.INDUSTRY) == null) return false;
            if (epcr.getPlanetEconomyWorkerPerc().get(EEconomyType.INDUSTRY) < 50d) {
                return false;
            }
            */
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in checking bonus condition: ", e);
            return false;
        }

        return true;
    }

    @Override
    public boolean isPossible() {
        return false;
    }

    @Override
    public ArrayList<String> activeBoni(int bonusId, int planetId, int userId) {
        ArrayList<String> boniList = new ArrayList<String>();

        boniList.add(ML.getMLStr("db_overview_industrial", userId));

        return boniList;
    }
    
    @Override 
    public void onActivation(int bonusId, int planetId, int userId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        pp.setColonyType(EColonyType.INDUSTRIAL_COLONY);
        ppDAO.update(pp);        
    }    
}
