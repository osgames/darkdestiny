 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view.menu;

/**
 *
 * Datenklasse f&uuml;r die Klasse StatusBarData.java
 * Daten zum Erstellen der StatusBar
 * 
 * @author Dreloc
 */
public class CreditsEntity {
    
    private long globalCredits = 0;
    private long planetCredits = 0;
    
    
    public long getGlobalCredits() {
        return globalCredits;
    }

    public void setGlobalCredits(long globalCredits) {
        this.globalCredits = globalCredits;
    }

    public long getPlanetCredits() {
        return planetCredits;
    }

    public void setPlanetCredits(long planetCredits) {
        this.planetCredits = planetCredits;
    }
    
}
