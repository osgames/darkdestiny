/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ressources;

 import at.darkdestiny.core.enumeration.EConstructionProdType;import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.enumeration.EConstructionProdType;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class RessProdNodeSplitted extends RessProdNode {

    private static final Logger log = LoggerFactory.getLogger(RessProdNodeSplitted.class);

    private HashMap<EConstructionProdType,Long> consumptionSource = new HashMap<EConstructionProdType,Long>();

    public RessProdNodeSplitted(int r, int type) {
        super(r,type);
    }

    public long getBaseConsumption(EConstructionProdType prodType) {
        if (consumptionSource.containsKey(prodType)) {
            return consumptionSource.get(prodType);
        } else {
            return 0;
        }
    }

    public void addBaseConsumption(long baseConsumption, EConstructionProdType prodType) {
        if (consumptionSource.containsKey(prodType)) {
            consumptionSource.put(prodType, consumptionSource.get(prodType) + baseConsumption);
        } else {
            consumptionSource.put(prodType, baseConsumption);
        }
    }

    public void printNode() {
        log.debug("DEBUG NODE");
        for (Map.Entry<EConstructionProdType,Long> mapEntry : consumptionSource.entrySet()) {
            log.debug("Consumption for type " + mapEntry.getKey() + " is " + mapEntry.getValue());
        }
    }
}
