/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.notification;

 import at.darkdestiny.core.ML;import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.GroundTroopDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.update.ProdOrderBufferEntry;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.GroundTroopDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.update.ProdOrderBufferEntry;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class ProductionOrderNotification extends Notification {

    private static final Logger log = LoggerFactory.getLogger(ProductionOrderNotification.class);

    public static PlayerPlanetDAO playerPlanetDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    public static ConstructionDAO constructionDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    public static ShipDesignDAO shipDesignDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    public static GroundTroopDAO groundTroopDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    private TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> planetEntries;

    public ProductionOrderNotification(TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> planetEntries, int userId) {
        super(userId);
        this.planetEntries = planetEntries;
        generateMessage();
        persistMessage();
    }

    /*
    public void persistMessage() {

        Message m = new Message();

        m.setArchiveBySource(this.getArchiveBySource());
        m.setArchiveByTarget(this.getArchiveByTarget());
        m.setDelBySource(this.getDelBySource());
        m.setDelByTarget(this.getDelByTarget());
        m.setType(this.getType());
        m.setOutMaster(this.getOutMaster());
        m.setSourceUserId(this.getSourceUserId());
        m.setTargetUserId(this.getTargetUserId());
        m.setTimeSent(this.getTimeSent());
        m.setViewed(this.getViewed());
        m.setTopic(this.getTopic());
        m.setText(this.getText());

        messageDAO.add(m);
    }
    */

    public void generateMessage() {

        String topic = ML.getMLStr("notification_msg_topic", userId);
        String msg = "";
        for (Map.Entry<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> entry : planetEntries.entrySet()) {
            int pId = entry.getKey();
            EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>> not = entry.getValue();
            msg += ML.getMLStr("notification_msg_finishedonplanet1", userId) + " ";
            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(pId);
            if (pp == null) {
                log.error( "PP was null where it should not have been [generateMessage/ProductionOrderNotification] on planet Id " + pId);
                continue;
            }

            msg += "<A href=\"selectview.jsp?showPlanet=" + pId + "\">";
            msg += pp.getName() + " ";
            msg += "</A>";
            msg += ML.getMLStr("notification_msg_finishedonplanet2", userId) + "<BR>";
            msg += addMessages(not, userId);
            msg = msg.substring(0, msg.length() - 4);
            msg += "_______________________" + "<BR>";
        }
        
        gm.setMessageContentType(EMessageContentType.CONSTRUCTION);
        gm.setTopic(topic);
        gm.setMsg(msg);
    }

    public static String addMessages(EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>> finishedOrders, int userId) {

        String msg = "";

        ArrayList<ProdOrderBufferEntry> tmp = new ArrayList<ProdOrderBufferEntry>();
        // Constructions
        tmp = finishedOrders.get(EProductionOrderType.C_BUILD);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_con_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                Construction c = constructionDAO.findById(pboe.getAction().getConstructionId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += ML.getMLStr(c.getName(), userId);
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Scrap Construction
        tmp = finishedOrders.get(EProductionOrderType.C_SCRAP);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_scrap_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                Construction c = constructionDAO.findById(pboe.getAction().getConstructionId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += ML.getMLStr(c.getName(), userId);
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Produce Groundtroops
        tmp = finishedOrders.get(EProductionOrderType.GROUNDTROOPS);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_grnd_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                GroundTroop gt = groundTroopDAO.findById(pboe.getAction().getRefEntityId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += ML.getMLStr(gt.getName(), userId);
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Produce Order
        tmp = finishedOrders.get(EProductionOrderType.PRODUCE);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_prod_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                ShipDesign sd = shipDesignDAO.findById(pboe.getAction().getRefEntityId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += sd.getName();
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Repair Order
        tmp = finishedOrders.get(EProductionOrderType.REPAIR);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_repair_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                ShipDesign sd = shipDesignDAO.findById(pboe.getAction().getShipDesignId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += sd.getName();
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // SCRAP Order
        tmp = finishedOrders.get(EProductionOrderType.SCRAP);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_prodscrap_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                ShipDesign sd = shipDesignDAO.findById(pboe.getAction().getShipDesignId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += sd.getName();
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // IMPROVE Order
        tmp = finishedOrders.get(EProductionOrderType.C_IMPROVE);

        if (tmp != null) {
            msg += ML.getMLStr("notification_prod_prodcimprove_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
                Construction c = constructionDAO.findById(pboe.getAction().getConstructionId());
                msg += "- ";
                msg += pboe.getAction().getNumber() + " ";
                msg += ML.getMLStr(c.getName(), userId);
                msg += "<BR>";
            }
            msg += "<BR>";
        }
        // Upgrade Order
        tmp = finishedOrders.get(EProductionOrderType.UPGRADE);

        if (tmp != null) {
            log.debug("z1");
            msg += ML.getMLStr("notification_prod_upg_msg1", userId);
            for (ProdOrderBufferEntry pboe : tmp) {
            log.debug("z2");
                if (pboe.getActionType() == EActionType.BUILDING) {
            log.debug("z3");
                    Construction c = constructionDAO.findById(pboe.getAction().getConstructionId());
                    msg += "- ";
                    msg += pboe.getAction().getNumber() + " ";
                    msg += ML.getMLStr(c.getName(), userId);
                    msg += "<BR>";
                } else if (pboe.getActionType() == EActionType.SHIP) {
            log.debug("z4");
                    ShipDesign sd = shipDesignDAO.findById(pboe.getProductionOrder().getToId());
                    msg += "- ";
                    msg += pboe.getAction().getNumber() + " ";
                    msg += sd.getName();
                    msg += "<BR>";
                } else if (pboe.getActionType() == EActionType.SPACESTATION) {
            log.debug("z5");
                    ShipDesign sd = shipDesignDAO.findById(pboe.getProductionOrder().getToId());
                    msg += "- ";
                    msg += pboe.getAction().getNumber() + " ";
                    msg += sd.getName();
                    msg += "<BR>";
                }
            }
            msg += "<BR>";
        }

        return msg;
    }
}
