/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.model.FleetFormation;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.ships.ShipUtilities;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlayerFleetDAO extends ReadWriteTable<PlayerFleet> implements GenericDAO {

    public PlayerFleet findById(int id) {
        PlayerFleet pf = new PlayerFleet();
        pf.setId(id);
        return (PlayerFleet)get(pf);
    }

    public Iterable<PlayerFleet> findByUserId(int userId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setUserId(userId);
        return find(pf);
    }
    public ArrayList<PlayerFleet> findByPlanetId(int planetId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setPlanetId(planetId);
        return find(pf);
    }
    public ArrayList<PlayerFleet> findByUserIdPlanetId(int userId, int planetId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setUserId(userId);
        pf.setPlanetId(planetId);
        return find(pf);
    }
    public ArrayList<PlayerFleet> findBySystemId(int systemId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setSystemId(systemId);
        return find(pf);
    }

    @Deprecated
    public ArrayList<PlayerFleet> findBy(int planetId, int systemId) {
        PlayerFleet pf = new PlayerFleet();

        pf.setPlanetId(planetId);
        pf.setSystemId(systemId);

        return find(pf);
    }

    // TODO dont use deprecated function
    public ArrayList<PlayerFleet> findBy(RelativeCoordinate rc) {
        return findBy(rc.getPlanetId(),rc.getSystemId());
    }

    @Deprecated
    public ArrayList<PlayerFleet> findBy(int planetId, int systemId, int userId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setPlanetId(planetId);
        pf.setSystemId(systemId);
        pf.setUserId(userId);
        return find(pf);
    }
    public ArrayList<PlayerFleet> findByFleetFormation(int formationId) {
        PlayerFleet pf = new PlayerFleet();
        pf.setFleetFormationId(formationId);
        return find(pf);
    }


    public PlayerFleet createNewFleet(String name, Integer userId, Integer systemId, Integer planetId) {
        int fleetId = ShipUtilities.getFreeFleetId();
        String fleetName = name.replace("%ID%", String.valueOf(fleetId));

        PlayerFleet pf = new PlayerFleet();
        pf.setId(fleetId);
        pf.setName(fleetName);
        pf.setUserId(userId);
        if (planetId != 0) {
            pf.setLocation(ELocationType.PLANET,planetId);
        } else {
            pf.setLocation(ELocationType.SYSTEM,systemId);
        }
        pf.setPlanetId(planetId);
        pf.setSystemId(systemId);
        pf.setStatus(0);

        return add(pf);
    }

    public ArrayList<PlayerFleet> getParticipatingFleets(FleetFormation ff) {
        PlayerFleet pfSearch = new PlayerFleet();
        pfSearch.setFleetFormationId(ff.getId());
        return find(pfSearch);
    }
}
