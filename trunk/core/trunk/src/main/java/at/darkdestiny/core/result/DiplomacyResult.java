/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.diplomacy.relations.EAttackType;
import at.darkdestiny.core.enumeration.EOwner;
import at.darkdestiny.core.enumeration.ESharingType;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class DiplomacyResult {
    private ArrayList<BaseResult> result;
    private boolean attacks;
    private boolean helps;
    private boolean notification;
    private boolean attackTradeFleets;
    private ESharingType sharingStarmapInfo;
    private boolean ableToUseShipyard;
    private EAttackType battleInNeutral;
    private EAttackType battleInOwn;
    private EOwner owner;
    private String color;

    public DiplomacyResult(boolean attacks, boolean helps, boolean notification, boolean attackTradeFleets, String color, ESharingType sharingStarmapInfo,
                           boolean ableToUseShipyard, EAttackType battleInNeutral, EAttackType battleInOwn, EOwner owner){
        this.attacks = attacks;
        this.helps = helps;
        this.ableToUseShipyard = ableToUseShipyard;
        this.notification = notification;
        this.attackTradeFleets = attackTradeFleets;
        this.sharingStarmapInfo = sharingStarmapInfo;
        this.battleInNeutral = battleInNeutral;
        this.battleInOwn = battleInOwn;
        this.owner = owner;
        this.color = color;

    }

    /**
     * @return the attacks
     */
    public boolean isAttacks() {
        return attacks;
    }

    /**
     * @return the helps
     */
    public boolean isHelps() {
        return helps;
    }

    /**
     * @return the notification
     */
    public boolean isNotification() {
        return notification;
    }

    /**
     * @return the attackTradeFleets
     */
    public boolean isAttackTradeFleets() {
        return attackTradeFleets;
    }

    public boolean isSharingMap(){
        if(sharingStarmapInfo.equals(ESharingType.ALL)){
            return true;
        }else if(sharingStarmapInfo.equals(ESharingType.NONE)){
            return false;
        }else{
            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Mapsharing for a relation is not specified... that's bad mkay?");
            return false;
        }
    }

    /**
     * @return the battleInNeutral
     */
    public EAttackType getBattleInNeutral() {
        return battleInNeutral;
    }

    /**
     * @return the battleInOwn
     */
    public EAttackType getBattleInOwn() {
        return battleInOwn;
    }

    /**
     * @return the ableToUseShipyard
     */
    public boolean isAbleToUseShipyard() {
        return ableToUseShipyard;
    }

    /**
     * @return the sharingStarmapInfo
     */
    public ESharingType getSharingStarmapInfo() {
        return sharingStarmapInfo;
    }

    /**
     * @param sharingStarmapInfo the sharingStarmapInfo to set
     */
    public void setSharingStarmapInfo(ESharingType sharingStarmapInfo) {
        this.sharingStarmapInfo = sharingStarmapInfo;
    }

    /**
     * @return the owner
     */
    public EOwner getOwner() {
        return owner;
    }

    public String getColor() {
        return color;
    }

}
