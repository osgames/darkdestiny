/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.webservice.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "playerStatisticList")
public class PlayerStatisticList {

    @XmlElements({
        @XmlElement(name = "playerStatistics", type = at.darkdestiny.core.webservice.dto.PlayerStatisticListEntry.class)})
    private List<PlayerStatisticListEntry> playerStatistics;

    /**
     * @return the playerStatistics
     */
    public List<PlayerStatisticListEntry> getPlayerStatistics() {
        return playerStatistics;
    }

    /**
     * @param playerStatistics the playerStatistics to set
     */
    public void setPlayerStatistics(List<PlayerStatisticListEntry> playerStatistics) {
        this.playerStatistics = playerStatistics;
    }
}
