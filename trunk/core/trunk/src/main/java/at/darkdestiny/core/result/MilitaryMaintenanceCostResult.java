/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import com.google.common.collect.Maps;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class MilitaryMaintenanceCostResult {
    private HashMap<Integer,Integer> costPerUser = Maps.newHashMap();
    
    public void addUserCost(int user, int cost) {
        costPerUser.put(user,cost);                
    }
    
    public int getCostForUser(int user) {
        Integer cost = costPerUser.get(user);
        if (cost == null) {
            return 0;
        } else {
            return cost.intValue();
        }
    }
}
