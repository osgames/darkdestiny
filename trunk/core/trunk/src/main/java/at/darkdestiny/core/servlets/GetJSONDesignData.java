/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

 import at.darkdestiny.core.ML;import at.darkdestiny.core.ships.*;
import at.darkdestiny.core.ModuleType;
import at.darkdestiny.core.admin.module.AttributeTree;
import at.darkdestiny.core.admin.module.ModuleAttributeResult;
import at.darkdestiny.core.dao.AttributeDAO;
import at.darkdestiny.core.dao.ChassisDAO;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.dao.RessourceCostDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipTypeAdjustmentDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.model.ModuleAttribute;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipTypeAdjustment;
import at.darkdestiny.core.ships.*;
import at.darkdestiny.core.utilities.ResearchUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.ModuleType;
import at.darkdestiny.core.admin.module.AttributeTree;
import at.darkdestiny.core.admin.module.ModuleAttributeResult;
import at.darkdestiny.core.dao.AttributeDAO;
import at.darkdestiny.core.dao.ChassisDAO;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.dao.RessourceCostDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipTypeAdjustmentDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.model.ModuleAttribute;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipTypeAdjustment;
import at.darkdestiny.core.ships.*;
import at.darkdestiny.core.utilities.ResearchUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class GetJSONDesignData extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(GetJSONDesignData.class);

    private ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    private RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    private AttributeDAO aDAO = (AttributeDAO) DAOFactory.get(AttributeDAO.class);
    private ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate"); //HTTP 1.1
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server

        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        boolean firstChassis = true;
        StringBuffer json = new StringBuffer();
        json.append("{ ");

        try {
            int userId = 0;

            try {
                userId = Integer.parseInt((String)session.getAttribute("userId"));
            } catch (NumberFormatException nfe) {
                return;
            }

            int designId = Integer.parseInt(request.getParameter("designId"));

            ShipDesign sdTmp = sdDAO.findById(designId);
            if (sdTmp.getUserId() != userId) return;

            long designTime = System.currentTimeMillis();

            // Chassis
            json.append(" \"chassis\": [ ");

            ArrayList<Chassis> chassis = cDAO.findAll();

            DebugBuffer.addLine(DebugLevel.UNKNOWN, "Loading Chassis");
            long startTime = System.currentTimeMillis();

            AttributeTree at = new AttributeTree(userId, startTime);

            for (Chassis c : chassis) {
                if (!ResearchUtilities.isModuleResearched(c.getRelModuleId(), userId)) {
                    continue;
                }
                ShipDesignDetailed sdd = new ShipDesignDetailed(designId, c.getRelModuleId(), at);

                if (!firstChassis) {
                    json.append(", ");
                }
                firstChassis = false;

                json.append("{ \"id\":\"" + c.getId() + "\", \"name\":\"" + ML.getMLStr(c.getName(), userId) + "\", \"moduleId\":\"" + c.getRelModuleId() + "\", \"picLink\":\"" + c.getImg_design() + "\", ");
                // ArrayList<RessAmountEntry> cost = rcDAO.findRessAmountEntries(RessourceCost.COST_MODULE, c.getRelModuleId());

                json.append("\"Ressources\": [ ");

                // - Chassis cost - base values
                ArrayList<RessAmountEntry> cost = at.getRessourceCost(c.getId(), c.getRelModuleId());
                boolean first = true;

                for (RessAmountEntry rae : cost) {
                    Ressource r = rDAO.findRessourceById(rae.getRessId());
                    if (!first) {
                        json.append(", ");
                    }
                    json.append("{ ");
                    json.append("\"id\":\"" + r.getId() + "\", \"name\":\"" + ML.getMLStr(r.getName(), userId) + "\", \"qty\":\"" + rae.getQty() + "\"");
                    json.append(" }");
                    first = false;
                }

                json.append(" ], \"basicValues\": { "); // Add basic chassis values

                if (sdd.getBase().getId() == c.getRelModuleId()) {
                    json.append("\"space\":\"" + sdd.getBase().getSpace() + "\", ");
                    json.append("\"structure\":\"" + sdd.getBase().getStructure() + "\", ");
                    json.append("\"range\":\"" + sdd.getBase().getRange() + "\"");
                }

                json.append(" }, \"modules\": { ");

                String engineContent = " ";
                String computerContent = " ";
                String reactorContent = " ";
                String armorContent = " ";
                String weaponContent = " ";
                String shieldContent = " ";
                String otherContent = " ";

                boolean engineContentExists = false;
                boolean computerContentExists = false;
                boolean reactorContentExists = false;
                boolean armorContentExists = false;
                boolean weaponContentExists = false;
                boolean shieldContentExists = false;
                boolean otherContentExists = false;

                DebugBuffer.addLine(DebugLevel.UNKNOWN, "Loaded chassis " + ML.getMLStr(c.getName(), userId) + " after " + (System.currentTimeMillis() - startTime) + "ms");
                int maxModuleSize = sdd.getBase().getSpace();

                ArrayList<ShipModule> modules = ShipBuilder.getModulesForChassis(c, userId, designTime);

                ShipDesign sd = sdDAO.findById(sdd.getId());

                for (ShipModule sm : modules) {
                    log.debug("Building Module " + sm.getName() + " with Type " + sm.getType().name());
                    String moduleName = ML.getMLStr(sm.getName(), userId);
                    String attribEntries = createAttributeEntries(sm, maxModuleSize, sd.getType());

                    if (sm.getType() == ModuleType.ENGINE) {
                        if (attribEntries != null) {
                            if (engineContentExists) {
                                engineContent += ", ";
                            }
                            engineContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            // log.debug("Engine content = " + engineContent);
                            engineContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.REACTOR) {
                        if (attribEntries != null) {
                            if (reactorContentExists) {
                                reactorContent += ", ";
                            }
                            reactorContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            // log.debug("Reactor content = " + reactorContent);
                            reactorContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.WEAPON) {
                        if (attribEntries != null) {
                            if (weaponContentExists) {
                                weaponContent += ", ";
                            }
                            weaponContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            // log.debug("Weapon content = " + weaponContent);
                            weaponContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.SHIELD) {
                        if (attribEntries != null) {
                            if (shieldContentExists) {
                                shieldContent += ", ";
                            }
                            shieldContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            shieldContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.HANGAR) {
                        log.debug("HANGAR -> " + attribEntries);

                        if (attribEntries != null) {
                            if (otherContentExists) {
                                otherContent += ", ";
                            }
                            otherContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            otherContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.TRANSPORT_POPULATION) {
                        if (attribEntries != null) {
                            if (otherContentExists) {
                                otherContent += ", ";
                            }
                            otherContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            otherContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.TRANSPORT_RESSOURCE) {
                        if (attribEntries != null) {
                            if (otherContentExists) {
                                otherContent += ", ";
                            }
                            otherContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            otherContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.ARMOR) {
                        if (attribEntries != null) {
                            if (armorContentExists) {
                                armorContent += ", ";
                            }
                            armorContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            armorContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.COLONISATION) {
                        if (attribEntries != null) {
                            if (otherContentExists) {
                                otherContent += ", ";
                            }
                            otherContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            otherContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.SPECIAL) {
                        if (attribEntries != null) {
                            if (otherContentExists) {
                                otherContent += ", ";
                            }
                            otherContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            otherContentExists = true;
                        }
                    } else if (sm.getType() == ModuleType.COMPUTER) {
                        if (attribEntries != null) {
                            if (computerContentExists) {
                                computerContent += ", ";
                            }
                            computerContent += "{ \"id\":\"" + sm.getId() + "\", \"name\":\"" + moduleName + "\", \"Ressources\": [ " + createRessEntryModule(at, c.getId(), sm.getId()) + " ], "
                                    + "\"Attributes\": [ " + attribEntries + " ] "
                                    + " }";
                            computerContentExists = true;
                        }
                    }
                }

                DebugBuffer.addLine(DebugLevel.UNKNOWN, "Loaded chassis " + c.getName() + " modules after " + (System.currentTimeMillis() - startTime) + "ms");

                json.append(" \"engine\": [");

                if (engineContentExists) {
                    json.append(engineContent);
                } else {
                }

                json.append("], ");

                json.append(" \"computers\": [");

                if (computerContentExists) {
                    json.append(computerContent);
                } else {
                }

                json.append("], ");

                json.append(" \"reactors\": [");

                if (reactorContentExists) {
                    json.append(reactorContent);
                } else {
                }

                json.append("], ");

                json.append(" \"armors\": [");

                if (armorContentExists) {
                    json.append(armorContent);
                } else {
                }

                json.append("], ");

                json.append(" \"weapons\": [");

                if (weaponContentExists) {
                    json.append(weaponContent);
                } else {
                }

                json.append("], ");

                json.append(" \"shields\": [");

                if (shieldContentExists) {
                    json.append(shieldContent);
                } else {
                }

                json.append("], ");

                json.append(" \"other\": [");

                if (otherContentExists) {
                    json.append(otherContent);
                } else {
                }

                json.append("]");
                json.append(" }"); // modules endblock

                json.append(" } "); // chassis endblock
            }

            json.append("],");

            // Get data of current ship
            int chassisId = sdDAO.findById(designId).getChassis();
            ShipDesignDetailed sdd = new ShipDesignDetailed(designId, at);

            json.append(" \"storedDesign\": { \"chassisId\":\"" + chassisId + "\", \"modules\": [ ");

            HashMap<Integer, Integer> modules = sdd.getModuleCount();
            boolean firstEntry = true;

            for (Map.Entry<Integer, Integer> entries : modules.entrySet()) {
                if (!firstEntry) {
                    json.append(", ");
                }
                firstEntry = false;
                json.append(" { \"id\":\"" + entries.getKey() + "\", \"count\":\"" + entries.getValue() + "\" }");

            }

            json.append(" ] } ");
            json.append(" , ");
            appendShipTypes(json);
            json.append(" }");
            out.println(json);

            long endTime = System.currentTimeMillis();
            DebugBuffer.addLine(DebugLevel.UNKNOWN, "Processing Time for all Chassis: " + (endTime - startTime) + "ms");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ShipDesignJSON: ", e);
        } finally {
            out.close();
        }
    }

    private void appendShipTypes(StringBuffer json) {
        json.append(" \"shiptypes\": [ ");
        boolean skip = true;
        for (EShipType type : EShipType.values()) {
            ShipTypeAdjustment sta = staDAO.findByType(type);
            if (skip) {
                skip = false;
            } else {
                json.append(",");
            }

            json.append("{ \"id\":\"" + sta.getId()
                    + "\", \"typeId\":\"" + sta.getEnumId().ordinal()
                    + "\", \"name\":\"" + type.toString()
                    + "\", \"ressFactor\":\"" + sta.getRessFactor()
                    + "\", \"structureFactor\":\"" + sta.getStructureFactor()
                    + "\", \"shieldFactor\":\"" + sta.getShieldFactor()
                    + "\", \"attackFactor\":\"" + sta.getAttackFactor()
                    + "\", \"troopFactor\":\"" + sta.getTroopFactor()
                    + "\", \"costFactor\":\"" + sta.getCostFactor()
                    + "\", \"hangarFactor\":\"" + sta.getHangarFactor()
                    + "\" " + "}" + "");

        }
        json.append("] ");
    }

    private String createAttributeEntries(ShipModule sm, int maxAllowedSize, EShipType st) {
        // log.debug(">>>>>>> Try to get attributes for " + sm.getName());

        ModuleAttributeResult mar = sm.getBase().at.getAllAttributes(sm.getBase().chassisId, sm.getId());
        // ModuleAttributeResult marGeneral = sm.getBase().at.getAllAttributes(0, sm.getId());

        // ArrayList<AttributeTreeEntry> attributes = sm.getBase().at.getAllAttributes(sm.getBase().chassisId, sm.getId());
        // ArrayList<AttributeTreeEntry> attributesGeneral = sm.getBase().at.getAllAttributes(0, sm.getId());

        if (mar.getAttributeValue(EAttribute.SPACE_CONSUMPTION) > maxAllowedSize) {
            return null;
        }
        ArrayList<ModuleAttribute> maList = mar.getAllSingleEntries();
        // maList.addAll(marGeneral.getAllSingleEntries());

        StringBuffer attribEntry = new StringBuffer();
        boolean firstAttribute = true;
        boolean hasContent = false;

        for (ModuleAttribute ma : maList) {
            if (aDAO.findById(ma.getAttributeId()).getName().equalsIgnoreCase("ENABLED")) {
                continue;
            }
            if (!firstAttribute) {
                attribEntry.append(", ");
            }

            firstAttribute = false;
            hasContent = true;

            if (aDAO.findById(ma.getAttributeId()).getName().equalsIgnoreCase(at.darkdestiny.core.enumeration.EAttribute.ATTACK_STRENGTH.toString())) {
                ShipTypeAdjustment sta = staDAO.findByType(st);
                attribEntry.append("{ \"id\":\"" + ma.getAttributeId() + "\", \"name\":\"" + aDAO.findById(ma.getAttributeId()).getName() + "\", \"value\":\"" + (int) (ma.getValue() * sta.getAttackFactor()) + "\" }");
            } else if (aDAO.findById(ma.getAttributeId()).getName().equalsIgnoreCase(at.darkdestiny.core.enumeration.EAttribute.DEFENSE_STRENGTH.toString())) {
                ShipTypeAdjustment sta = staDAO.findByType(st);
                attribEntry.append("{ \"id\":\"" + ma.getAttributeId() + "\", \"name\":\"" + aDAO.findById(ma.getAttributeId()).getName() + "\", \"value\":\"" + (int) (ma.getValue() * sta.getShieldFactor()) + "\" }");
            } else {
                attribEntry.append("{ \"id\":\"" + ma.getAttributeId() + "\", \"name\":\"" + aDAO.findById(ma.getAttributeId()).getName() + "\", \"value\":\"" + ma.getValue() + "\" }");
            }


        }

        // Append special Attributes from module table
        // like unique
        Module m = mDAO.findById(sm.getId());
        if (m.getUniquePerShip()) {
            if (hasContent) {
                attribEntry.append(", ");
            }
            hasContent = true;
            attribEntry.append("{ \"id\":\"0\", \"name\":\"UNIQUE_PER_SHIP\", \"value\":\"true\" }");
        }
        if (m.getUniquePerType()) {
            if (hasContent) {
                attribEntry.append(", ");
            }
            hasContent = true;
            attribEntry.append("{ \"id\":\"0\", \"name\":\"UNIQUE_PER_TYPE\", \"value\":\"true\" }");
        }

        return attribEntry.toString();
    }

    private String createRessEntryModule(AttributeTree at, int chassisId, int moduleId) {
        StringBuffer ressEntry = new StringBuffer();

        // ArrayList<RessAmountEntry> cost = rcDAO.findRessAmountEntries(RessourceCost.COST_MODULE, moduleId);
        ArrayList<RessAmountEntry> cost = at.getRessourceCost(chassisId, moduleId);
        boolean firstRessEntry = true;

        for (RessAmountEntry rae : cost) {
            Ressource r = rDAO.findRessourceById(rae.getRessId());
            if (!firstRessEntry) {
                ressEntry.append(", ");
            }
            ressEntry.append("{ ");
            ressEntry.append("\"id\":\"" + r.getId() + "\", \"name\":\"" + r.getName() + "\", \"qty\":\"" + rae.getQty() + "\"");
            ressEntry.append(" }");
            firstRessEntry = false;
        }

        return ressEntry.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
