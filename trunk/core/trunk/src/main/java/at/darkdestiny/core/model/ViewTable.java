/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "viewtable")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ViewTable extends Model<ViewTable> {

    @FieldMappingAnnotation("systemId")
    @IdFieldAnnotation
    private Integer systemId;
    @FieldMappingAnnotation("planetId")
    @IdFieldAnnotation
    private Integer planetId;
    @IndexAnnotation(indexName = "userId")
    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("lastTimeVisited")
    @DefaultValue("0")
    private Integer lastTimeVisited;
    @FieldMappingAnnotation("mostRecent")
    @DefaultValue("0")
    private Boolean mostRecent;

    public Integer getSystemId() {
        return systemId;
    }

    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    public Integer getPlanetId() {
        return planetId;
    }

    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLastTimeVisited() {
        return lastTimeVisited;
    }

    public void setLastTimeVisited(Integer lastTimeVisited) {
        this.lastTimeVisited = lastTimeVisited;
    }

    public Boolean isMostRecent() {
        return mostRecent;
    }

    public void setMostRecent(Boolean mostRecent) {
        this.mostRecent = mostRecent;
    }
}
