/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.service.ConstructionService;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class ProductionBuildResult {

    private final HashMap<Buildable, BuildableResult> availUnits = new HashMap<Buildable, BuildableResult>();
    private final TreeMap<Boolean, TreeMap<Object, HashMap<Buildable, BuildableResult>>> availConstructionsSorted = new TreeMap<Boolean, TreeMap<Object, HashMap<Buildable, BuildableResult>>>();

    public ProductionBuildResult() {
    }

    public void addProduction(Buildable b, BuildableResult br) {
        getAvailUnits().put(b, br);
    }

    public void addConstruction(Buildable b, BuildableResult br) {
        TreeMap<Object, HashMap<Buildable, BuildableResult>> result = getAvailConstructionsSorted().get(br.isDeniedByTech() || br.isDeniedByUnique() || br.isDeniedByLandType() || br.isDeniedBySurface() || br.isDeniedByPlanetType());
        if (result == null) {
            result = new TreeMap<Object, HashMap<Buildable, BuildableResult>>();
        }

        if (b.getBase() instanceof GroundTroop) {
            HashMap<Buildable, BuildableResult> entries = result.get(((GroundTroop) b.getBase()).getName());
            if (entries == null) {
                entries = new HashMap<Buildable, BuildableResult>();
            }
            entries.put(b, br);
            result.put(((GroundTroop) b.getBase()).getName(), entries);
        } else if (b.getBase() instanceof ShipDesign) {
            HashMap<Buildable, BuildableResult> entries = result.get(((ShipDesign) b.getBase()).getName());
            if (entries == null) {
                entries = new HashMap<Buildable, BuildableResult>();
            }
            entries.put(b, br);
            result.put(((ShipDesign) b.getBase()).getName(), entries);
        } else if (b.getBase() instanceof Construction) {
            HashMap<Buildable, BuildableResult> entries = result.get(((Construction) b.getBase()).getOrderNo());
            if (entries == null) {
                entries = new HashMap<Buildable, BuildableResult>();
            }
            entries.put(b, br);
            result.put(((Construction) b.getBase()).getOrderNo(), entries);
        }

        // Hierbei handelt es sich um Constructionen die Ausgegraut dargestellt werden sollen
        getAvailConstructionsSorted().put(br.isDeniedByTech() || br.isDeniedByUnique() || br.isDeniedByLandType() || br.isDeniedBySurface() || br.isDeniedByPlanetType(), result);

    }

    public ArrayList<Buildable> getUnits() {
        TreeMap<Object, ArrayList<Buildable>> sorted = sortEntries();

        ArrayList<Buildable> units = new ArrayList<Buildable>();

        for (ArrayList<Buildable> bList : sorted.values()) {
            units.addAll(bList);
        }

        return units;
    }

    // Boolean states if Construction is Buildable by Tech or not
    public TreeMap<Boolean, HashMap<Buildable, BuildableResult>> getConstructionsSorted(int userId, int planetId, EConstructionType type) {
        TreeMap<Boolean, HashMap<Buildable, BuildableResult>> result = new TreeMap<Boolean, HashMap<Buildable, BuildableResult>>();
        TreeMap<Object, ArrayList<Buildable>> sorted = sortEntries();

        ProductionBuildResult pbr = ConstructionService.getConstructionList(userId, planetId, type);

        ArrayList<Buildable> units = new ArrayList<Buildable>();

        for (ArrayList<Buildable> bList : sorted.values()) {
            units.addAll(bList);
        }
        for (Buildable b : units) {
            BuildableResult br = pbr.getBuildInfo(b);
            java.lang.System.out.println("Processing Building " + ((Construction) b.getBase()).getName() + " DBT: " + br.isDeniedByTech() + " DBPT: " + br.isDeniedByPlanetType());

            HashMap<Buildable, BuildableResult> cons = result.get(br.isDeniedByTech() || br.isDeniedByPlanetType());
            if (cons == null) {
                cons = new HashMap<Buildable, BuildableResult>();
            }
            cons.put(b, br);
            result.put(br.isDeniedByTech() || br.isDeniedByPlanetType(), cons);
        }
        return result;
    }

    public ArrayList<Buildable> getUnitsWithPossibleTech() {
        ArrayList<Buildable> units = new ArrayList<Buildable>();

        TreeMap<Object, ArrayList<Buildable>> sorted = sortEntries();

        for (ArrayList<Buildable> bList : sorted.values()) {
            for (Buildable b : bList) {
                if (getAvailUnits().get(b).isDeniedByTech() || getAvailUnits().get(b).isDeniedByPlanetType()) {
                    continue;
                }
                if (getAvailUnits().get(b).isNotFinished()) {
                    continue;
                }

                units.add(b);
            }
        }

        return units;
    }

    public BuildableResult getBuildInfo(Buildable b) {
        return getAvailUnits().get(b);
    }

    private TreeMap<Object, ArrayList<Buildable>> sortEntries() {
        TreeMap<Object, ArrayList<Buildable>> sorted = new TreeMap<Object, ArrayList<Buildable>>();
        for (Buildable b : getAvailUnits().keySet()) {
            if (b.getBase() instanceof GroundTroop) {
                int orderKey = ((GroundTroop) b.getBase()).getId();
                if (!sorted.containsKey(orderKey)) {
                    ArrayList<Buildable> elements = new ArrayList<Buildable>();
                    elements.add(b);
                    sorted.put(orderKey, elements);
                } else {
                    ArrayList<Buildable> elements = sorted.get(orderKey);
                    elements.add(b);
                }
            } else if (b.getBase() instanceof ShipDesign) {
                String orderKey = ((ShipDesign) b.getBase()).getName();
                if (!sorted.containsKey(orderKey)) {
                    ArrayList<Buildable> elements = new ArrayList<Buildable>();
                    elements.add(b);
                    sorted.put(orderKey, elements);
                } else {
                    ArrayList<Buildable> elements = sorted.get(orderKey);
                    elements.add(b);
                }
            } else if (b.getBase() instanceof Construction) {
                int orderKey = ((Construction) b.getBase()).getOrderNo();
                if (!sorted.containsKey(orderKey)) {
                    ArrayList<Buildable> elements = new ArrayList<Buildable>();
                    elements.add(b);
                    sorted.put(orderKey, elements);
                } else {
                    ArrayList<Buildable> elements = sorted.get(orderKey);
                    elements.add(b);
                }
            }
        }

        return sorted;
    }

    private TreeMap<Object, Buildable> sortEntries(HashMap<Buildable, BuildableResult> entries) {
        TreeMap<Object, Buildable> sorted = new TreeMap<Object, Buildable>();
        for (Buildable b : entries.keySet()) {
            if (b.getBase() instanceof GroundTroop) {
                sorted.put(((GroundTroop) b.getBase()).getName(), b);
            } else if (b.getBase() instanceof ShipDesign) {
                sorted.put(((ShipDesign) b.getBase()).getName(), b);
            } else if (b.getBase() instanceof Construction) {
                sorted.put(((Construction) b.getBase()).getOrderNo(), b);
            }
        }

        return sorted;
    }

    public Map<Buildable, BuildableResult> getUnitsSorted(boolean buildable) {

        Map<Buildable, BuildableResult> result = Maps.newLinkedHashMap();
        if (getAvailConstructionsSorted().get(buildable) == null) {
            return result;
        }
        for (Map.Entry<Object, HashMap<Buildable, BuildableResult>> entry : getAvailConstructionsSorted().get(buildable).entrySet()) {
            for (Map.Entry<Buildable, BuildableResult> entry2 : entry.getValue().entrySet()) {
                result.put(entry2.getKey(), entry2.getValue());
            }
        }
        return result;
    }

    /**
     * @return the availConstructionsSorted
     */
    public TreeMap<Boolean, TreeMap<Object, HashMap<Buildable, BuildableResult>>> getAvailConstructionsSorted() {
        return availConstructionsSorted;
    }

    /**
     * @return the availUnits
     */
    public HashMap<Buildable, BuildableResult> getAvailUnits() {
        return availUnits;
    }
    /**
     * @return the availConstructionsSorted
     */
}
