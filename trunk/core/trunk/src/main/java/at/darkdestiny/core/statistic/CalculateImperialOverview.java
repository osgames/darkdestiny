/*
 * CalculateImperialOverview.java
 *
 * Created on 21. Februar 2008, 13:21
 *
 */
package at.darkdestiny.core.statistic;

import at.darkdestiny.core.*;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.ImperialStatisticDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.dao.TradeRouteShipDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.enumeration.EFleetViewType;
import at.darkdestiny.core.enumeration.EImperialStatisticType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.fleet.LoadingInformation;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.HangarRelation;
import at.darkdestiny.core.model.ImperialStatistic;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.result.FleetListResult;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.service.ManagementService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.service.StatisticService;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.ships.ShipStorage;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.*;

/**
 *
 * @author craft4
 */
public class CalculateImperialOverview extends Thread implements Runnable {

    private int currentuser;
    private int currentally;
    ArrayList<ImperialStatistic> toPersist;
    private HashMap<Integer, Long> userShipCount;
    //private HashMap<Integer,Integer> allyShipCount;
    private HashMap<Integer, Double> userRess;
    private HashMap<Integer, Double> userProduction;
    private HashMap<Integer, Long> userTroops;
    private HashMap<Integer, Long> userDefense;
    private HashMap<Integer, HashMap<Integer, Integer>> planetDefense;
    private HashMap<Integer, HashMap<Integer, Integer>> spaceStations;
    private long user_population;
    private int user_income;
    private int user_planets;
    private int user_systems;
    private static UserDataDAO userDataDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static ImperialStatisticDAO imperialStatisticDAO = (ImperialStatisticDAO) DAOFactory.get(ImperialStatisticDAO.class);
    private static PlanetDefenseDAO planetDefenseDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static AllianceMemberDAO allianceMemberDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static PlanetDAO planetDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetRessourceDAO planetRessourceDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static PlayerPlanetDAO playerPlanetDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static RessourceDAO ressourceDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static ShipDesignDAO shipDesignDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlayerTroopDAO playerTroopDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private static TradeRouteShipDAO tradeRouteShipDAO = (TradeRouteShipDAO) DAOFactory.get(TradeRouteShipDAO.class);
    private static TradeRouteDAO tradeRouteDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);

    /** Creates a new instance of CalculateImperialOverview */
    public CalculateImperialOverview() {
        setPriority(MIN_PRIORITY);

        //Delete all old statistics
        imperialStatisticDAO.removeAll();

        toPersist = new ArrayList<ImperialStatistic>();
        userShipCount = new HashMap<Integer, Long>();
        userRess = new HashMap<Integer, Double>();
        userProduction = new HashMap<Integer, Double>();
        userTroops = new HashMap<Integer, Long>();
        userDefense = new HashMap<Integer, Long>();
        user_income = 0;
        user_planets = 0;
        user_systems = 0;
        user_population = 0;


        planetDefense = new HashMap<Integer, HashMap<Integer, Integer>>();
        spaceStations = new HashMap<Integer, HashMap<Integer, Integer>>();

        for (PlanetDefense pd : (ArrayList<PlanetDefense>) planetDefenseDAO.findAll()) {
            if (pd.getType() == EDefenseType.TURRET) {
                if (!planetDefense.containsKey(pd.getUserId())) {
                    planetDefense.put(pd.getUserId(), new HashMap<Integer, Integer>());
                }

                if (!planetDefense.get(pd.getUserId()).containsKey(pd.getUnitId())) {
                    // Create new construction entry
                    planetDefense.get(pd.getUserId()).put(pd.getUnitId(), pd.getCount());
                } else {
                    HashMap<Integer, Integer> defenseCount = planetDefense.get(pd.getUserId());
                    defenseCount.put(pd.getUnitId(), defenseCount.get(pd.getUnitId()) + pd.getCount());                    
                }
            } else if (pd.getType() == EDefenseType.SPACESTATION) {
                ShipDesign sd = shipDesignDAO.findById(pd.getUnitId());
                if (sd == null) {
                    DebugBuffer.warning("Could not find ship design for unit id " + pd.getUnitId() + " and user id " + pd.getUserId());
                    continue;
                }
                if (!spaceStations.containsKey(sd.getUserId())) {
                    spaceStations.put(sd.getUserId(), new HashMap<Integer, Integer>());
                }

                if (!spaceStations.get(sd.getUserId()).containsKey(sd.getChassis())) {
                    // Create new space station entry
                    spaceStations.get(sd.getUserId()).put(sd.getChassis(), pd.getCount());
                } else {
                    HashMap<Integer, Integer> stationCount = spaceStations.get(sd.getUserId());
                    stationCount.put(sd.getChassis(), stationCount.get(sd.getChassis()) + pd.getCount());
                }
            }
        }
    }

    private void reInit() {
        userRess.clear();
        userProduction.clear();
        userTroops.clear();
        userDefense.clear();
        userShipCount.clear();

        user_income = 0;
        user_planets = 0;
        user_systems = 0;
        user_population = 0;
    }

    @Override
    public void run() {
        // clear the statistic table from last time
        imperialStatisticDAO.removeAll();
        //   stmt_users.execute("DELETE FROM imperialstatistic");

        ArrayList<UserData> uds = userDataDAO.findAll();
        //   result_user = stmt_users.executeQuery("SELECT u.id, a.allianceId from user u LEFT OUTER JOIN alliancemembers a ON u.id = a.userId");

        DebugBuffer.addLine(DebugLevel.TRACE, "yep, i am doing the statistics, sucker");

        // cycle through all players
        for (UserData ud : uds) {
            User u = Service.userDAO.findById(ud.getUserId());
            if (u.getUserType() != EUserType.HUMAN) continue;
            
            //while (result_user.next()) {
            currentuser = ud.getUserId();
            DebugBuffer.addLine(DebugLevel.TRACE, "working on user: " + Integer.toString(currentuser));
            AllianceMember am = allianceMemberDAO.findByUserId(currentuser);
            if (am == null) {
                currentally = 0;
            } else {
                currentally = am.getAllianceId();
            }
            ArrayList<PlayerPlanet> pps = playerPlanetDAO.findByUserId(currentuser);
            //  result_planets = stmt.executeQuery("SELECT planetID from playerplanet WHERE userID = \""+currentuser+"\" ");
            ArrayList<Integer> systems = new ArrayList<Integer>();
            addToDoubleMap(userRess, Ressource.DEV_POINTS, (long)Math.floor(ud.getDevelopementPoints()));
            
            for (PlayerPlanet pp : pps) {
                Planet p = planetDAO.findById(pp.getPlanetId());
                if (!systems.contains(p.getSystemId())) {
                    systems.add(p.getSystemId());
                }
                
                PlanetCalculation planetCalc = ManagementService.getCalculatedPlanetValues(pp.getPlanetId());
                PlanetCalcResult pcr = planetCalc.getPlanetCalcData();
                ExtPlanetCalcResult epcr = pcr.getExtPlanetCalcData();
                                
                //while (result_planets.next()) {
                // DebugBuffer.addLine("currentplanet: " + Integer.toString(result_planets.getInt(1)));
                for (Ressource r : StatisticService.findAllRessources()) {
                    PlanetRessource pr = planetRessourceDAO.findBy(pp.getPlanetId(), r.getId(), EPlanetRessourceType.INSTORAGE);
                    if (pr != null) {
                        addToDoubleMap(userRess, r.getId(), (long) pr.getQty());
                    } else {
                        if (r.getId() == Ressource.DEV_POINTS) {
                            System.out.println("["+u.getGameName()+"] Adding " + (long)pcr.getExtPlanetCalcData().getDevelopementPointsGlobal() + " for planet " + pp.getName());
                            // Use further down for production
                            addToDoubleMap(userProduction, r.getId(), pcr.getExtPlanetCalcData().getDevelopementPointsGlobal());                
                        }
                    }
                    
                    Long value = pcr.getProductionData().getRessBaseProduction(r.getId());
                    // addToDoubleMap(userProduction,r.getId(),value);
                }
                
                user_population += pp.getPopulation();

                // get the taxes of the current planet
                user_income += epcr.getTaxIncome();

                // check for planetare abwehr
                // DebugBuffer.addLine("Planetare Abwehr");
                // Raketenbasis
                /* OBSOLETE !!! USE PLANETDEFENSE MAP !!! 
                if (getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARYMISSLEBASE) > 0) {
                    addToMap(userDefense, Construction.ID_PLANETARYMISSLEBASE, (long)getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARYMISSLEBASE));
                }

                // Lasergesch&uuml;tz
                if (getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARYLASERCANNON) > 0) {
                    addToMap(userDefense, Construction.ID_PLANETARYLASERCANNON, (long)getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARYLASERCANNON));
                }

                // Intervallgesch&uuml;tz
                if (getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARY_INTERVALCANNON) > 0) {
                    addToMap(userDefense, Construction.ID_PLANETARY_INTERVALCANNON, (long)getCountForBuilding(pp.getPlanetId(), Construction.ID_PLANETARY_INTERVALCANNON));
                }
                */
            }
            
            
            HashMap<Integer,Integer> defenseBuildings = planetDefense.get(u.getId());
            if (defenseBuildings != null) {
                for (Map.Entry<Integer,Integer> defenseEntry : defenseBuildings.entrySet()) {
                    addToMap(userDefense, defenseEntry.getKey(), (long)defenseEntry.getValue());
                }
            }


            user_planets = pps.size();

            // Get all Systems of the Player
            user_systems = systems.size();

            // get the ships of the current player in fleets

            FleetListResult flr = FleetService.getFleetList(currentuser, 0, 0, EFleetViewType.OWN_ALL);
            ArrayList<PlayerFleetExt> fleets = flr.getSingleFleets();
            fleets.addAll(FleetService.getFleetList(currentuser, 0, 0, EFleetViewType.OWN_ALL_WITH_ACTION).getSingleFleets());
            for (PlayerFleetExt pfe : flr.getSingleFleets()) {

                LoadingInformation li = pfe.getLoadingInformation();
                for (ShipStorage ss : li.getLoadedStorage()) {
                    if (ss.getLoadtype() == ShipStorage.LOADTYPE_GROUNDTROOP) {

                        addToMap(userTroops, ss.getId(), (long)ss.getCount());
                    }
                }

                //Check for Ships in Hangar
                for (HangarRelation hr : Service.hangarRelationDAO.findByFleetId(pfe.getId())) {
                    int designId = hr.getDesignId();
                    ShipDesign sd = shipDesignDAO.findById(designId);
                    if (sd == null) {
                        DebugBuffer.error("Fleet " + pfe.getName() + " ["+pfe.getId()+"] contains ship id " + designId + " in hangar which does not exist!");
                        continue;
                    }
                    addToMap(userShipCount, sd.getChassis(), (long)hr.getCount());
                }

                for (ShipData shipData : pfe.getShipList()) {
                    //    while (result_temp.next()) {
                    // Skip space stations
                    ShipDesign sd = shipDesignDAO.findById(shipData.getDesignId());
                    if (sd == null) {
                        DebugBuffer.error("Fleet " + pfe.getName() + " ["+pfe.getId()+"] contains shipdesing which does not exist!");
                        continue;
                    }
                    addToMap(userShipCount, sd.getChassis(), (long)shipData.getCount());
                }
            }
            for (TradeRoute tr : tradeRouteDAO.findByUserId(currentuser)) {
                TradeRouteExt tre = new TradeRouteExt(tr);
                for (Map.Entry<ShipDesign, Integer> entry : tre.getAssignedShips().entrySet()) {

                    addToMap(userShipCount, entry.getKey().getChassis(), (long)entry.getValue());
                }
            }

            if (spaceStations.containsKey(currentuser)) {
                HashMap<Integer, Integer> stationCount = spaceStations.get(currentuser);

                for (Map.Entry<Integer, Integer> stationEntry : stationCount.entrySet()) {
                    addToMap(userShipCount, stationEntry.getKey(), (long)stationEntry.getValue());
                }
            }
            /*
            // ships in handelflotten finden
            // DebugBuffer.addLine("Handelsflotten");
            result_temp = stmt.executeQuery("SELECT s.count,d.chassis FROM tradefleets s LEFT JOIN shipdesigns d ON s.designId = d.id WHERE d.userId = \"" + currentuser + "\"");

            while (result_temp.next()) {
            addToMap(userShipCount, result_temp.getInt(2), result_temp.getInt(1));
            }


            // ships in handelsrouten finden
            //DebugBuffer.addLine("Handelsrouten");
            result_temp = stmt.executeQuery("SELECT s.number,d.chassis FROM traderouteships s LEFT JOIN shipdesigns d ON s.designId = d.id WHERE d.userId = \"" + currentuser + "\"");

            while (result_temp.next()) {
            addToMap(userShipCount, result_temp.getInt(2), result_temp.getInt(1));
            }
             */
            // bodentruppen checken
            //DebugBuffer.addLine("Bodentruppen");
            ArrayList<PlayerTroop> pts = playerTroopDAO.findByUserId(currentuser);
            //   result_temp = stmt.executeQuery("SELECT number, troopId FROM playertroops WHERE userId = \"" + currentuser + "\"");
            for (PlayerTroop pt : pts) {
                //     while (result_temp.next()) {
                addToMap(userTroops, pt.getTroopId(), (long) pt.getNumber());
            }

            // kampfstationen einbauen
            // to be done

            // Daten des Users in Datebank schreiben

            // Add Ships
            for (Map.Entry<Integer, Long> me : userShipCount.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.SHIPS);
                is.setSourceId(me.getKey());
                is.setValue((long) me.getValue());
                toPersist.add(is);
            }

            // Add Defence Systems
            for (Map.Entry<Integer, Long> me : userDefense.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.DEFENCE);
                is.setSourceId(me.getKey());
                is.setValue((long) me.getValue());
                toPersist.add(is);
            }

            // Add Ressources
            for (Map.Entry<Integer, Double> me : userRess.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.RESSOURCE);
                is.setSourceId(me.getKey());
                is.setValue((long)Math.floor(me.getValue()));
                toPersist.add(is);
            }

            // Add Production
            for (Map.Entry<Integer, Double> me : userProduction.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.RESSPROD);
                is.setSourceId(me.getKey());
                is.setValue((long)Math.floor(me.getValue()));
                toPersist.add(is);
            }            
            
            // Add Troops
            for (Map.Entry<Integer, Long> me : userTroops.entrySet()) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.TROOPS);
                is.setSourceId(me.getKey());
                is.setValue((long) me.getValue());
                toPersist.add(is);
            }

            // add remaining stuff
            for (int i = 0; i < 4; i++) {
                ImperialStatistic is = new ImperialStatistic();
                is.setUserId(currentuser);
                is.setAllyId(currentally);
                is.setTypeId(EImperialStatisticType.STATISTIC);
                is.setSourceId(i);
                toPersist.add(is);
                switch (i) {
                    case GameConstants.STATISTIC_POPULATION:
                        is.setValue(user_population);
                        // pstmt1.setDouble(5, user_population);
                        break;
                    case GameConstants.STATISTIC_INCOME:
                        is.setValue((long) user_income);
                        //pstmt1.setDouble(5, user_income);
                        break;
                    case GameConstants.STATISTIC_PLANETS:

                        is.setValue((long) user_planets);
                        // pstmt1.setDouble(5, user_planets);
                        break;
                    case GameConstants.STATISTIC_SYSTEMS:
                        is.setValue((long) user_systems);
                        //  pstmt1.setDouble(5, user_systems);
                        break;
                    default:
                        break;
                }

            }

            reInit();
        }
        for (ImperialStatistic is : toPersist) {
            // imperialStatisticDAO.add(is);
            //     log.debug("User Id = " + is.getUserId() + " sourceId : " + is.getSourceId() + " value " + is.getValue());
        }
        imperialStatisticDAO.insertAll(toPersist);
    }

    private void addToDoubleMap(HashMap<Integer, Double> hm, Integer key, Double value) {
        if (hm.containsKey(key)) {
            hm.put(key, hm.get(key) + value);
        } else {
            hm.put(key, value);
        }
    }
    
    private void addToDoubleMap(HashMap<Integer, Double> hm, Integer key, Long value) {
        if (hm.containsKey(key)) {
            hm.put(key, hm.get(key) + value);
        } else {
            hm.put(key, (double)value);
        }
    }    
    
    private void addToMap(HashMap<Integer, Long> hm, Integer key, Long value) {
        if (hm.containsKey(key)) {
            hm.put(key, hm.get(key) + value);
        } else {
            hm.put(key, value);
        }
    }        
}
