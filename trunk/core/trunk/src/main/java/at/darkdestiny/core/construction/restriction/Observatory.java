/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.construction.restriction;

import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.enumeration.ERestrictionReason;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.ConstructionCheckResult;
import at.darkdestiny.core.scanner.StarMapQuadTree;
import at.darkdestiny.core.scanner.SystemDesc;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.ViewTableUtilities;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class Observatory extends Service implements ConstructionInterface {

    public static final int OBSERVATORY_RANGE = 100;
    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();

    public ConstructionCheckResult onConstructionCheck(PlayerPlanet pp) {
        return new ConstructionCheckResult(true,1);
    }

    public void onConstruction(PlayerPlanet pp) throws Exception {
    }

    public void onFinishing(PlayerPlanet pp) {
        refreshViewTable(pp);
    }

    public void onDeconstruction(PlayerPlanet pp) throws Exception {
        onDestruction(pp);
    }

    public void onDestruction(PlayerPlanet pp) throws Exception {
        refreshViewTable(pp);
    }

    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }

    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    @Override
    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }

    private void refreshViewTable(PlayerPlanet pp) {
        StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
        ArrayList<at.darkdestiny.core.model.System> systems = systemDAO.findAll();

        for (at.darkdestiny.core.model.System s : systems) {
            SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
            quadtree.addItemToTree(sd);
        }
        at.darkdestiny.core.model.System s = systemDAO.findById(planetDAO.findById(pp.getPlanetId()).getSystemId());
        ArrayList<SystemDesc> tmpSys = quadtree.getItemsAround(s.getX(), s.getY(), (int) OBSERVATORY_RANGE);
        //Creating Viewtable Entries
        int currTick = GameUtilities.getCurrentTick2();
        for (SystemDesc sd : tmpSys) {
            ViewTableUtilities.addOrRefreshSystem(pp.getUserId(), sd.id, currTick);
        }
    }
}
