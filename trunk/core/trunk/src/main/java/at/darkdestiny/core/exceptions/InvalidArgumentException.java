/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.exceptions;

/**
 *
 * @author Stefan
 */
public class InvalidArgumentException
        extends RuntimeException {

    private static final long serialVersionUID = -8603572275339751496L;

    public InvalidArgumentException() {
    }

    public InvalidArgumentException(String message) {
        super(message);
    }

    public InvalidArgumentException(Throwable cause) {
        super(cause);
    }

    public InvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }
}
