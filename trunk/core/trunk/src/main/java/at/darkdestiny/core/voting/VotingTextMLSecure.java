/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

 import at.darkdestiny.core.ML;import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class VotingTextMLSecure {

    private static final Logger log = LoggerFactory.getLogger(VotingTextMLSecure.class);

    private final String textConstant;
    private final String textConstantSubject;
    private final HashMap<String,CharSequence> replacements;
    private final HashMap<String,CharSequence> replacementsSubject;

    public VotingTextMLSecure(String textConstant, String textConstantSubject, HashMap<String,CharSequence> replacements, HashMap<String,CharSequence> replacementsSubject) {
        this.textConstant = textConstant;
        this.textConstantSubject = textConstantSubject;
        if (replacements == null) {
            this.replacements = new HashMap<String,CharSequence>();
        } else {
            this.replacements = replacements;
        }
        if (replacementsSubject == null) {
            this.replacementsSubject = new HashMap<String,CharSequence>();
        } else {
            this.replacementsSubject = replacementsSubject;
        }
    }

    public VotingTextMLSecure(String textConstant, String textConstantSubject) {
        this.textConstant = textConstant;
        this.textConstantSubject = textConstantSubject;
        this.replacements = new HashMap<String,CharSequence>();
        this.replacementsSubject = new HashMap<String,CharSequence>();
    }

    protected String getMessage(int userId) {
        String msg = ML.getMLStr(getTextConstant(),userId);
        log.debug("Request Text: " + msg + " [replacements size is " + replacements.size() + "]");
        msg = replaceConstants(msg,replacements,userId);
        return msg;
    }        

    protected String getSubject(int userId) {
        String subject = ML.getMLStr(getTextConstantSubject(),userId);
        log.debug("Request Subject: " + subject + " [replacements size is " + replacementsSubject.size() + "]");
        subject = replaceConstants(subject,replacementsSubject,userId);
        return subject;
    }

    private String replaceConstants(String text, HashMap<String,CharSequence> constantMapping, int userId) {
        for (Map.Entry<String,CharSequence> replacement : constantMapping.entrySet()) {
            log.debug("Replace Constant " + replacement.getKey() + " to " + replacement.getValue());
            if (replacement.getValue() instanceof MLString) {
                text = text.replace(replacement.getKey(), ((MLString)replacement.getValue()).toMLString(userId));
            } else {
                text = text.replace(replacement.getKey(), replacement.getValue());
            }
        }

        return text;
    }

    /**
     * @return the textConstant
     */
    protected String getTextConstant() {
        return textConstant;
    }

    /**
     * @return the textConstantSubject
     */
    protected String getTextConstantSubject() {
        return textConstantSubject;
    }
}
