/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

/**
 *
 * @author Stefan
 */
public class VoteException extends Exception {
    public VoteException() {
        super();
    }
    
    public VoteException(String message) {
        super(message);
    }    
}
