/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.Threading.ThreadController;
import at.darkdestiny.core.Threading.ViewSystemInsertJob;
import at.darkdestiny.core.Threading.ViewtableThread;
import at.darkdestiny.core.construction.restriction.Observatory;
import at.darkdestiny.core.enumeration.EViewSystemLocationType;
import at.darkdestiny.core.model.ViewSystem;
import at.darkdestiny.core.scanner.StarMapQuadTree;
import at.darkdestiny.core.scanner.SystemDesc;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class ViewSystemTableUtilities {

    public static final int RANGE_SYSTEM = Observatory.OBSERVATORY_RANGE * 4;
    public static final int RANGE_OBSERVATORY = Observatory.OBSERVATORY_RANGE * 8;
    public static final int RANGE_FLEET_INFLIGHT = Observatory.OBSERVATORY_RANGE * 1;
    public static final int RANGE_FLEET_STATIONARY = Observatory.OBSERVATORY_RANGE * 3;

    /**
     * @return the ranges
     */
    public static HashMap<String, Integer> getRanges() {
        return ranges;
    }

    public enum Type {

        SYSTEM, OBSERVATORY, FLEET_INFLIGHT, FLEET_STATIONARY
    }
    private static final HashMap<String, Integer> ranges = Maps.newHashMap();
    private static final HashMap<Integer, HashMap<EViewSystemLocationType, ArrayList<Integer>>> transactionValues = Maps.newHashMap();
    private static ArrayList<ViewSystem> toAdd = Lists.newArrayList();
    private static StarMapQuadTree quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);

    static {
        getRanges().put(Type.SYSTEM.toString(), RANGE_SYSTEM);
        getRanges().put(Type.OBSERVATORY.toString(), RANGE_OBSERVATORY);
        getRanges().put(Type.FLEET_INFLIGHT.toString(), RANGE_FLEET_INFLIGHT);
        getRanges().put(Type.FLEET_STATIONARY.toString(), RANGE_FLEET_STATIONARY);

        //We assume that no systems are added while the game is running
        quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
        ArrayList<at.darkdestiny.core.model.System> systems = Service.systemDAO.findAll();

        for (at.darkdestiny.core.model.System s : systems) {
            SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
            quadtree.addItemToTree(sd);
        }

    }

    public static void reInitQuadTree() {
        //We assume that no systems are added while the game is running
        quadtree = new StarMapQuadTree(GameConstants.UNIVERSE_HEIGHT, GameConstants.UNIVERSE_WIDTH, 5);
        ArrayList<at.darkdestiny.core.model.System> systems = Service.systemDAO.findAll();

        for (at.darkdestiny.core.model.System s : systems) {
            SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), 0, 0);
            quadtree.addItemToTree(sd);
        }
    }

    public static void startTransaction() {
        toAdd.clear();
        transactionValues.clear();

    }

    public static void addLocation(int locationId, EViewSystemLocationType locationType, ViewSystemTableUtilities.Type type, int userId) {

        int x = 0;
        int y = 0;
        if (locationType.equals(EViewSystemLocationType.SYSTEM)) {
            at.darkdestiny.core.model.System system = Service.systemDAO.findById(locationId);
            x = system.getX();
            y = system.getY();
        }
        addLocation(x, y, locationType, type, userId);
    }

    public static void addLocation(int x, int y, EViewSystemLocationType locationType, Type type, Integer userId) {

        ArrayList<SystemDesc> tmpSys = quadtree.getItemsAround(x, y, getRanges().get(type.toString()));
        int checkedData = 0;
        int queriedData = 0;
        int foundData = 0;       

        for (SystemDesc systemDesc : tmpSys) {
            checkedData++;
            
            if (containsIndex(systemDesc.id, locationType, type, userId, transactionValues)) {
                continue;
            }
            ViewSystem viewSystemEntry = new ViewSystem();
            // viewSystemEntry.setDate(System.currentTimeMillis());
            viewSystemEntry.setLocationId(systemDesc.id);
            viewSystemEntry.setLocationType(locationType);
            viewSystemEntry.setUserId(userId);
            
            queriedData++;
            // DebugBuffer.trace("QUERY: " + viewSystemEntry.getLocationType() + " - " + viewSystemEntry.getLocationId() + " - " + viewSystemEntry.getDate() + " - " + viewSystemEntry.getUserId());
            ViewSystem vsResult = Service.viewSystemDAO.get(viewSystemEntry);
            if (vsResult != null) {
                // DebugBuffer.trace("FOUND: " + vsResult.getLocationType() + " - " + vsResult.getLocationId() + " - " + vsResult.getDate() + " - " + vsResult.getUserId());
                continue;
            }
            
            foundData++;
            addIndex(systemDesc.id, locationType, userId, transactionValues);
            toAdd.add(viewSystemEntry);

        }

        // DebugBuffer.trace("CheckedData: " + checkedData + " QueriedData: " + queriedData + " FoundData: " + foundData);
    }

    public static void addIndex(int locationId, EViewSystemLocationType locationType, Integer userId, HashMap<Integer, HashMap<EViewSystemLocationType, ArrayList<Integer>>> map) {
        if (map == null) {
            map = Maps.newHashMap();
        }
        HashMap<EViewSystemLocationType, ArrayList<Integer>> users = map.get(userId);
        if (users == null) {
            users = Maps.newHashMap();
        }
        if (users.get(locationType) != null) {
            return;
        } else {
            ArrayList<Integer> points = users.get(locationType);
            if (points == null) {
                points = Lists.newArrayList();
            }
            if (points.contains(locationId)) {
                return;
            } else {
                points.add(locationId);
            }
        }

    }

    public static boolean containsIndex(int locationId, EViewSystemLocationType locationType, Type type, Integer userId, HashMap<Integer, HashMap<EViewSystemLocationType, ArrayList<Integer>>> map) {
        if (map == null) {
            return false;
        }
        HashMap<EViewSystemLocationType, ArrayList<Integer>> users = map.get(userId);
        if (users == null) {
            return false;
        }
        if (users.get(locationType) != null) {
            return false;
        } else {

            return (users.get(locationType).contains(locationId));

        }
    }

    public static void endTransaction() {        
        ViewtableThread thread = ThreadController.getViewTableThread();
        thread.addJob(new ViewSystemInsertJob(toAdd));
        thread.start();
    }
}
