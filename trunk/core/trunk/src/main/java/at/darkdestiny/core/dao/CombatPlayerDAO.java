/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.CombatPlayer;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class CombatPlayerDAO  extends ReadWriteTable<CombatPlayer> implements GenericDAO  {

    public ArrayList<CombatPlayer> findByCombatId(Integer combatId) {
        CombatPlayer cp = new CombatPlayer();
        cp.setCombatId(combatId);
        return find(cp);
    }

    public CombatPlayer findBy(Integer combatId, Integer userId) {
        CombatPlayer cp = new CombatPlayer();
        cp.setCombatId(combatId);
        cp.setUserId(userId);
        return (CombatPlayer)get(cp);
    }

}
