/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.model.ProductionOrder;

/**
 *
 * @author Stefan
 */
public class AbortOrder {
    private final ProductionOrder po;
    private final int count;
    private final int userId;
    private final int planetId;

    public AbortOrder( ProductionOrder po, int count, int userId, int planetId) {
        this.po = po;
        this.count = count;
        this.userId = userId;
        this.planetId = planetId;
    }


    /**
     * @return the po
     */
    public ProductionOrder getPo() {
        return po;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @return the planetId
     */
    public int getPlanetId() {
        return planetId;
    }

}
