/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view;

import at.darkdestiny.core.enumeration.ETechListType;
import at.darkdestiny.core.model.TechRelation;

/**
 *
 * @author Admin
 */
public class TechListViewEntry {
    private TechRelation techRelation;
    private String name;
    private ETechListType techType;

    public TechListViewEntry(TechRelation techRelation, String name, ETechListType techType) {
        this.techRelation = techRelation;
        this.name = name;
        this.techType = techType;
    }

    
    
    /**
     * @return the techRelation
     */
    public TechRelation getTechRelation() {
        return techRelation;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the techType
     */
    public ETechListType getTechType() {
        return techType;
    }
}
