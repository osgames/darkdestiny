/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.service;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.User;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author HorstRabe
 */
public class IdToNameService {
    private final int userId;

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    public IdToNameService(int userId) {
        this.userId = userId;
    }

    public static String getPlanetName(int planetId) {
        if(ppDAO.findByPlanetId(planetId) != null){
        return ppDAO.findByPlanetId(planetId).getName();
        }else{
            return "Planet #" + planetId;
        }
    }

    public String getRessourceName(int ressId) {
        return ML.getMLStr(rDAO.findRessourceById(ressId).getName(),userId);
    }

    public String getModuleName(int moduleId) {
        return ML.getMLStr(mDAO.findById(moduleId).getName(),userId);
    }

    public static String getUserName(int userId) {
        User u = uDAO.findById(userId);

        if (u == null) {
            return "DELETED";
        }

        return u.getGameName();
    }
}
