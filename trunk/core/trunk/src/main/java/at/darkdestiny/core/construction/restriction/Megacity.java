/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.construction.restriction;

import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.construction.InConstructionData;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.enumeration.ERestrictionReason;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.ConstructionCheckResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.ConstructionUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Stefan
 */
public class Megacity implements ConstructionInterface {
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);   
    
    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();
    public ConstructionCheckResult onConstructionCheck(PlayerPlanet pp) {
        PlanetConstruction pc = pcDAO.findBy(pp.getPlanetId(), Construction.ID_MEGACITY);
        int count = 0;
        
        if (pc != null) count = pc.getNumber();
        
        // Also consider megacities already in construction
        List<InConstructionData> icdList = ConstructionUtilities.getInConstructionData(pp.getPlanetId());
        for (InConstructionData icd : icdList) {
            if (icd.getConstructionData().getConstruct().getId() == Construction.ID_MEGACITY) {
                count += icd.getConstructionData().getCount();
            }
        }
        
        int maxByMaxPop = (int)Math.floor(Service.planetDAO.findById(pp.getPlanetId()).getMaxPopulation() / 1000000000l);
        int maxByCurrPop = (int)Math.floor(pp.getPopulation() / 1000000000l);
        
        int totalMax = Math.min(maxByMaxPop, maxByCurrPop) - count;
        
        if ((maxByMaxPop - count) < 1) {
            errMsg.put("Building count limit for planet reached", ERestrictionReason.UNSPECIFIED);            
        } else if ((maxByCurrPop - count) < 1) {
            errMsg.put("Too low population for this building", ERestrictionReason.NOTENOUGHPOPULATION);
        }
        
        if (!errMsg.isEmpty()) return new ConstructionCheckResult(false,0);
        
        return new ConstructionCheckResult(true,totalMax);
    }

    @Override
    public void onConstruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public void onFinishing(PlayerPlanet pp) {

    }

    @Override
    public void onDeconstruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public void onDestruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }


    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    @Override
    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }
}
