/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation("specialtrade")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class SpecialTrade extends Model<SpecialTrade> { 
    @FieldMappingAnnotation("planetId")
    @IdFieldAnnotation()
    private Integer planetId;
    @FieldMappingAnnotation("production")
    private Integer production;
    @FieldMappingAnnotation("consumption")
    private Integer consumption;
    @FieldMappingAnnotation("incomingRoutes")
    private Integer incomingRoutes;
    @FieldMappingAnnotation("outgoingRoutes")
    private Integer outgoingRoutes;
    @FieldMappingAnnotation("routedCapacity")
    private Integer routedCapacity;    
    @FieldMappingAnnotation("incomingGoods")
    private Integer incomingGoods;    
    @FieldMappingAnnotation("outgoingGoods")
    private Integer outgoingGoods;        

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the production
     */
    public Integer getProduction() {
        return production;
    }

    /**
     * @param production the production to set
     */
    public void setProduction(Integer production) {
        this.production = production;
    }

    /**
     * @return the consumption
     */
    public Integer getConsumption() {
        return consumption;
    }

    /**
     * @param consumption the consumption to set
     */
    public void setConsumption(Integer consumption) {
        this.consumption = consumption;
    }

    /**
     * @return the incomingRoutes
     */
    public Integer getIncomingRoutes() {
        return incomingRoutes;
    }

    /**
     * @param incomingRoutes the incomingRoutes to set
     */
    public void setIncomingRoutes(Integer incomingRoutes) {
        this.incomingRoutes = incomingRoutes;
    }

    /**
     * @return the outgoingRoutes
     */
    public Integer getOutgoingRoutes() {
        return outgoingRoutes;
    }

    /**
     * @param outgoingRoutes the outgoingRoutes to set
     */
    public void setOutgoingRoutes(Integer outgoingRoutes) {
        this.outgoingRoutes = outgoingRoutes;
    }

    /**
     * @return the routedCapacity
     */
    public Integer getRoutedCapacity() {
        return routedCapacity;
    }

    /**
     * @param routedCapacity the routedCapacity to set
     */
    public void setRoutedCapacity(Integer routedCapacity) {
        this.routedCapacity = routedCapacity;
    }

    /**
     * @return the incomingGoods
     */
    public Integer getIncomingGoods() {
        return incomingGoods;
    }

    /**
     * @param incomingGoods the incomingGoods to set
     */
    public void setIncomingGoods(Integer incomingGoods) {
        this.incomingGoods = incomingGoods;
    }

    /**
     * @return the outgoingGoods
     */
    public Integer getOutgoingGoods() {
        return outgoingGoods;
    }

    /**
     * @param outgoingGoods the outgoingGoods to set
     */
    public void setOutgoingGoods(Integer outgoingGoods) {
        this.outgoingGoods = outgoingGoods;
    }
}
