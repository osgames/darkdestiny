/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "alliancemessage")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class AllianceMessage extends Model<AllianceMessage> {

    public static final int USER_DELETED = 0;
    @FieldMappingAnnotation("allianceId")
    @IdFieldAnnotation
    private Integer allianceId;
    @IndexAnnotation(indexName = "userId")
    @FieldMappingAnnotation("userId")
    private Integer userId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("boardId")
    private Integer boardId;
    @FieldMappingAnnotation("topic")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String topic;
    @FieldMappingAnnotation("message")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String message;
    @FieldMappingAnnotation("postTime")
    @IdFieldAnnotation
    private Long postTime;

    /**
     * @return the allianceId
     */
    public Integer getAllianceId() {
        return allianceId;
    }

    /**
     * @param allianceId the allianceId to set
     */
    public void setAllianceId(Integer allianceId) {
        this.allianceId = allianceId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the postTime
     */
    public Long getPostTime() {
        return postTime;
    }

    /**
     * @param postTime the postTime to set
     */
    public void setPostTime(Long postTime) {
        this.postTime = postTime;
    }

    /**
     * @return the boardId
     */
    public Integer getBoardId() {
        return boardId;
    }

    /**
     * @param boardId the boardId to set
     */
    public void setBoardId(Integer boardId) {
        this.boardId = boardId;
    }
}
