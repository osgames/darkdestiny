package at.darkdestiny.core.admin.techtree;

import at.darkdestiny.core.admin.db.DBDescription;
import at.darkdestiny.core.admin.db.IDatabaseColumn;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.util.DebugBuffer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Hiermit werden Abhängigkeiten von Modulen beschrieben
 *
 * @author martin
 */
public class ModulTechTreeEntry extends TechTree {

	/**
	 * Der OFFSET für die IDs, um Verwechslungen auszuschließen
	 */
	public static final int OFFSET = 0x20000000;

	/**
	 * @param ID
	 */
	public ModulTechTreeEntry(int ID) {
		super(ID);

		// DebugBuffer.addLine("Modul mit ID"+ID);

		try {
			Statement statement = DbConnect.getConnection().createStatement();
			ResultSet rs = statement
					.executeQuery("SELECT * FROM module WHERE id=" + ID);
			if (rs.next())
				this.init(rs);
		} catch (SQLException e) {
			DebugBuffer.writeStackTrace("Error in ModulTechTreeEntry: ",e);
		}

	}

	public ModulTechTreeEntry(ResultSet rs) throws SQLException {
		super(rs.getInt(1));
		init(rs);
	}

	@Override
	public int getID() {
		return ID + OFFSET;
	}

	@Override
	public String getName() {
		if (get("name") != null)
			return ""+get("name");
		return "Modul " + ID;
	}

	@Override
	public String getTyp() {
		return "Modul";
	}

	public static int makeID(int int1) {
		return int1 + OFFSET;
	}

	@Override
	public int getBaseID() {
		return ID;
	}

	@Override
	public List<IDatabaseColumn> getAllDBColumns() {
		return DBDescription.alleFelder_module;
	}

	@Override
	public String getTitle() {
		return "das Modul";
	}

	@Override
	public String getTable() {
		return "module";
	}

}
