package at.darkdestiny.core.admin.techtree.actions;

import at.darkdestiny.core.admin.IMyPageProducer;
import at.darkdestiny.core.admin.TechTreeAdmin;
import at.darkdestiny.core.admin.techtree.ConstructionTechTreeEntry;
import at.darkdestiny.core.admin.techtree.GroundTroopTechTreeEntry;
import at.darkdestiny.core.admin.techtree.ModulTechTreeEntry;
import at.darkdestiny.core.admin.techtree.TechTree;
import at.darkdestiny.core.admin.techtree.TechnologieTechTreeEntry;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Hiermit wird eine Abh&auml;ngigkeit entfernt
 *
 * @author martin
 */
public class DeleteTechnoProducer implements IMyPageProducer {

	private int id;
	private int tid;
	private boolean sure;

	/**
	 *
	 */
	public DeleteTechnoProducer(int id, int tid, boolean sure) {
		this.id = id;
		this.tid = tid;
		this.sure = sure;
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.admin.IMyPageProducer#producePage()
	 */
	public String producePage() throws SQLException {
		if (TechTreeAdmin.techTree == null)
			TechTreeAdmin.loadTechTree();

		StringBuffer res = new StringBuffer();

		if (sure)
		{
			TechTree tt_source = TechTreeAdmin.getTreeItemByID(id);
			TechTree tt_target = TechTreeAdmin.getTreeItemByID(tid);

			Statement state = DbConnect.getConnection().createStatement();

			int type = 1;
			if (tt_source instanceof ConstructionTechTreeEntry)
				type = 0;
			if (tt_source instanceof TechnologieTechTreeEntry)
				type = 1;
			if (tt_source instanceof ModulTechTreeEntry)
				type = 2;
			if (tt_source instanceof GroundTroopTechTreeEntry)
				type = 3;


			int target_ConsID = 0;
			int target_researchID = 0;
			if (tt_target instanceof ConstructionTechTreeEntry)
				target_ConsID = tt_target.getBaseID();
			if (tt_target instanceof TechnologieTechTreeEntry)
				target_researchID = tt_target.getBaseID();

			DebugBuffer.addLine(DebugLevel.DEBUG,"DELETE FROM `techrelation` WHERE (sourceID="+tt_source.getBaseID()+") AND (reqConstructionID = "+target_ConsID+") && ("+
					"reqResearchID = "+target_researchID+") && (treeType = "+type+")");
			state.execute("DELETE FROM `techrelation` WHERE (sourceID="+tt_source.getBaseID()+") AND (reqConstructionID = "+target_ConsID+") && ("+
					"reqResearchID = "+target_researchID+") && (treeType = "+type+")");
			res.append("Verkn&uuml;pfung entfernt<br>");
			res.append(TechTreeAdmin.makeLink(tt_source)+"<br>");
			res.append(TechTreeAdmin.makeLink(tt_target));
			TechTreeAdmin.forceReload();
			return res.toString();
		}
		TechTree tt_source = TechTreeAdmin.getTreeItemByID(id);
		TechTree tt_target = TechTreeAdmin.getTreeItemByID(tid);
		res.append("<h2>Abh&auml;ngigkeit entfernen</h2>");
		res.append("Bist du dir sicher, dass du die Abh&auml;ngigkeit zwischen");
		res.append(TechTreeAdmin.makeLink(tt_source)+" und ");
		res.append(TechTreeAdmin.makeLink(tt_target));
		res.append(" entfernen m&ouml;chtest?<br>");
		res.append(TechTreeAdmin.makeLink("Ja, sicher", TechTreeAdmin.ACTION_DELETE_DEPENDENCY, "id="+id+"&tid="+tid+"&sure=1"));
		res.append("<br>");
		res.append(TechTreeAdmin.makeLink(tt_source)+"<br>");
		res.append(TechTreeAdmin.makeLink(tt_target));

		return res.toString();
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.admin.IMyPageProducer#setup(java.lang.Object)
	 */
	public void setup(Object o) {
        // Empty on purpose
	}

}
