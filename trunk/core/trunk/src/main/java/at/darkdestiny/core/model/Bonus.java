/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.core.admin.module.AttributeType;
import at.darkdestiny.core.enumeration.EBonusRange;
import at.darkdestiny.core.enumeration.EBonusType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "bonus")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class Bonus extends Model<Bonus> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("description")
    private String description;
    @FieldMappingAnnotation("value")
    private Double value;
    @FieldMappingAnnotation("valueType")
    private AttributeType valueType;
    @FieldMappingAnnotation("bonusType")
    private EBonusType bonusType;
    @FieldMappingAnnotation("priceIncreasePerUsage")
    private Boolean priceIncreasePerUsage;
    @FieldMappingAnnotation("planetUnique")
    private Boolean planetUnique;
    @FieldMappingAnnotation("duration")
    private Integer duration;
    @FieldMappingAnnotation("bonusRange")
    private EBonusRange bonusRange;
    @FieldMappingAnnotation("cost")
    private Long cost;
    @FieldMappingAnnotation("reqResearch")
    private Integer reqResearch;
    @FieldMappingAnnotation("bonusClass")
    private String bonusClass;
    @FieldMappingAnnotation("enabled")
    private Boolean enabled;


    public static final int BONUS_PRODUCTION_1 = 2;
    public static final int BONUS_CONSTRUCTION_1 = 3;
    public static final int BONUS_RESEARCH_1 = 4;
    public static final int DEVELOPMENT_INDUSTRIAL = 5;
    public static final int DEVELOPMENT_MINING = 6;
    public static final int DEVELOPMENT_AGRICULTURAL = 7;
    public static final int DEVELOPMENT_POPULATION = 8;
    public static final int DEVELOPMENT_RESEARCH = 9;
    public static final int DEVELOPMENT_TRADE = 10;
    public static final int DEVELOPMENT_ADMINISTRATIVE = 12;
    public static final int BONUS_MINING_PERC_1 = 13;
    /*
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the value
     */
    public Double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * @return the valueType
     */
    public AttributeType getValueType() {
        return valueType;
    }

    /**
     * @param valueType the valueType to set
     */
    public void setValueType(AttributeType valueType) {
        this.valueType = valueType;
    }

    /**
     * @return the bonusType
     */
    public EBonusType getBonusType() {
        return bonusType;
    }

    /**
     * @param bonusType the bonusType to set
     */
    public void setBonusType(EBonusType bonusType) {
        this.bonusType = bonusType;
    }

    /**
     * @return the duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * @return the cost
     */
    public Long getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(Long cost) {
        this.cost = cost;
    }

    /**
     * @return the reqResearch
     */
    public Integer getReqResearch() {
        return reqResearch;
    }

    /**
     * @param reqResearch the reqResearch to set
     */
    public void setReqResearch(Integer reqResearch) {
        this.reqResearch = reqResearch;
    }

    /**
     * @return the bonusRange
     */
    public EBonusRange getBonusRange() {
        return bonusRange;
    }

    /**
     * @param bonusRange the bonusRange to set
     */
    public void setBonusRange(EBonusRange bonusRange) {
        this.bonusRange = bonusRange;
    }

    /**
     * @return the bonusClass
     */
    public String getBonusClass() {
        return bonusClass;
    }

    /**
     * @param bonusClass the bonusClass to set
     */
    public void setBonusClass(String bonusClass) {
        this.bonusClass = bonusClass;
    }

    /**
     * @return the priceIncreasePerUsage
     */
    public Boolean hasPriceIncreasePerUsage() {
        return priceIncreasePerUsage;
    }

    /**
     * @param priceIncreasePerUsage the priceIncreasePerUsage to set
     */
    public void estPriceIncreasePerUsage(Boolean priceIncreasePerUsage) {
        this.priceIncreasePerUsage = priceIncreasePerUsage;
    }

    /**
     * @return the planetUnique
     */
    public Boolean isPlanetUnique() {
        return planetUnique;
    }

    /**
     * @param planetUnique the planetUnique to set
     */
    public void setPlanetUnique(Boolean planetUnique) {
        this.planetUnique = planetUnique;
    }

    /**
     * @return the enabled
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
