/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view.header;

import java.util.HashMap;

/**
 *
 * @author Horst
 */
public class CategoryEntity {

    private int categoryId;
    private String categoryName;
    private int previousCategoryId;
    private int nextCategoryId;
    private int firstSystemId;
    private HashMap<Integer, SystemEntity> systems;

    public CategoryEntity(){
        this.categoryId = -1;
        this.categoryName = "";
        this.previousCategoryId = -1;
        this.nextCategoryId = -1;
        this.firstSystemId = -1;
        this.systems = new HashMap<Integer, SystemEntity>();
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }



    public HashMap<Integer, SystemEntity> getSystems() {
        return systems;
    }

    public void setSystems(HashMap<Integer, SystemEntity> systems) {
        this.systems = systems;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getPreviousCategoryId() {
        return previousCategoryId;
    }

    public void setPreviousCategoryId(int previousCategoryId) {
        this.previousCategoryId = previousCategoryId;
    }

    public int getNextCategoryId() {
        return nextCategoryId;
    }

    public void setNextCategoryId(int nextCategoryId) {
        this.nextCategoryId = nextCategoryId;
    }

    public int getFirstSystemId() {
        return firstSystemId;
    }

    public void setFirstSystemId(int firstSystemId) {
        this.firstSystemId = firstSystemId;
    }

    
    
    
}
