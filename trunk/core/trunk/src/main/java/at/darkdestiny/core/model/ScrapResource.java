/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.DataChangeAware;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "scrapresource")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ScrapResource extends Model<ScrapResource> implements DataChangeAware {

    /*
     public static final Integer TYPE_RESSOURCE_PLANET = 0;
     public static final Integer TYPE_RESSOURCE_INSTORAGE = 1;
     public static final Integer TYPE_RESSOURCE_MINIMUM = 2;
     */
    @FieldMappingAnnotation("planetId")
    @IndexAnnotation(indexName="planetId")
    private Integer planetId;
    @FieldMappingAnnotation("systemId")
    @IndexAnnotation(indexName="systemId")
    private Integer systemId;
    @FieldMappingAnnotation("locationId")
    @IdFieldAnnotation
    private Integer locationId;
    @FieldMappingAnnotation("locationType")
    @IdFieldAnnotation
    private ELocationType locationType;
    @FieldMappingAnnotation("resourceId")
    @IdFieldAnnotation
    private Integer resourceId;
    @FieldMappingAnnotation("quantity")
    private Long quantity;

    private boolean modified = false;

    public ScrapResource(){

    }

    @Override
    public boolean isModified() {
        return modified;
    }
public void setLocation(ELocationType locationType, int locationId) {
        this.locationType = locationType;
        this.locationId = locationId;

        if (locationType == ELocationType.PLANET) {
            RelativeCoordinate rc = new RelativeCoordinate(0, locationId);
            systemId = rc.getSystemId();
            planetId = locationId;
        } else if (locationType == ELocationType.SYSTEM) {
            systemId = locationId;
        }else {
            systemId = 0;
            planetId = 0;
        }
    }
    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the systemId
     */
    public Integer getSystemId() {
        return systemId;
    }

    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the locationId
     */
    public Integer getLocationId() {
        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the locationType
     */
    public ELocationType getLocationType() {
        return locationType;
    }

    /**
     * @param locationType the locationType to set
     */
    public void setLocationType(ELocationType locationType) {
        this.locationType = locationType;
    }

    /**
     * @return the resourceId
     */
    public Integer getResourceId() {
        return resourceId;
    }

    /**
     * @param resourceId the resourceId to set
     */
    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * @return the quantity
     */
    public Long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(boolean modified) {
        this.modified = modified;
    }

}
