package at.darkdestiny.core.admin;

import at.darkdestiny.framework.access.DbConnect;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Die Repräsentation eines Spielers als Admin
 * 
 * @author martin
 */
public class AdminUser {

    private int userID;
    private int adminRights;
    private int betaAdminRights;

    /**
     * 
     */
    public AdminUser(int UserID) throws Exception {
        this.userID = UserID;


        Statement stmt = DbConnect.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT Admin, BetaAdmin FROM user WHERE id=" + userID);
        if (!rs.next()) {
            throw new Exception("Der Benutzer exitiert leider nicht");
        }

        adminRights = rs.getInt(1);

        if (adminRights == 0) {
            throw new Exception("Du bist leider kein Admin.");
        }

        betaAdminRights = rs.getInt(2);
    }

    public static boolean isAdmin(int UserID) {
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT Admin, BetaAdmin FROM user WHERE id=" + UserID);
            if (!rs.next()) {
                throw new Exception("Der Benutzer exitiert leider nicht");
            }

            int adminRights = rs.getInt(1);

            if (adminRights == 0) {
                throw new Exception("Du bist leider kein Admin.");
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isAdmin() {
        return adminRights != 0;
    }

    public boolean isBetaAdmin() {
        return betaAdminRights != 0;
    }

    public boolean isTechTreeAdmin() {
        return isAdmin();
    }
}
