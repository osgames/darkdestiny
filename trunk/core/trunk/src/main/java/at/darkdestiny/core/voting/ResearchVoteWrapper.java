/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.ResearchDAO;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.VoteOption;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ResearchVoteWrapper extends DefaultVoteWrapper {
    private static ResearchDAO rDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    
    @Override
    public String getVoteStateHTML(VoteResult vr, int voteId, Locale locale) {
        System.out.println("Vote Code: " + vr.getVoteCode());
        
        String message = "<BR/>";

        Vote v = VoteUtilities.getVote(voteId);
        HashMap<Integer, VoteOption> vo = vr.getOptions();

        //dbg log.debug("VO Size = " + vo.size());

        boolean closed = false;

        int optionYes = -1;
        int optionNo = -1;


        HashMap<Integer, ArrayList<String>> voted = new HashMap<Integer, ArrayList<String>>();


        /**
        for (Map.Entry<Integer, VoteOption> me : vo.entrySet()) {
        if (me.getValue().getType() == VoteOption.OPTION_TYPE_ACCEPT) {
        //dbg log.debug("Setting optionYes1from : " + optionYes + " to " + me.getKey());
        optionYes = me.getKey();
        }
        if (me.getValue().getType() == VoteOption.OPTION_TYPE_DENY) {
        //dbg log.debug("Setting optionNo from : " + optionNo + " to " + me.getKey());
        optionNo = me.getKey();
        }
        }*/
        for (Map.Entry<Integer, Integer> me : vr.getUserVotes().entrySet()) {
            String userName;
            userName = uDAO.findById(me.getKey()).getGameName();

            //dbg log.debug("Found voted option " + me.getValue() + " for user " + userName);
            VoteOption voteOption = voDAO.findById(me.getValue());
            //dbg log.debug("Vote option " + voteOption);
            if (voteOption != null) {
                if (voteOption.getType() == VoteOption.OPTION_TYPE_ACCEPT) {
                    //dbg log.debug("Incrementing Option Yes");
                    optionYes++;
                } else if (voteOption.getType() == VoteOption.OPTION_TYPE_DENY) {
                    //dbg log.debug("Incrementing Option No");
                    optionNo++;
                }
            }
            if (!voted.containsKey(me.getValue())) {
                ArrayList<String> users = new ArrayList<String>();
                users.add(userName);
                voted.put(me.getValue(), users);
            } else {
                voted.get(me.getValue()).add(userName);
            }
        }

        if ((vr.getVoteCode() == VoteResult.VOTED_ACCEPT) || (vr.getVoteCode() == VoteResult.VOTED_DENIED) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_DENIED) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_OPTION)) {
            closed = true;

            int userId = (Integer)v.getMetaData("USERID");
            int researchId = -1;

            VoteOption closeOption = voDAO.findById(v.getCloseOption());

            Research rSearch = new Research();
            rSearch.setName(closeOption.getVoteOption());
            researchId = rDAO.find(rSearch).get(0).getId();            
            
            message += "<B><FONT color=\"yellow\">";
            message += "Die Forschung "+ML.getMLStr(closeOption.getVoteOption(), userId)+" wurde fertiggestellt!";
            message += "</FONT></B><BR><BR>";
        } else {
            if (vr.getVoteCode() == VoteResult.VOTE_ALREADY_VOTED) {
                /*
                message += "Diese Abstimmung wurde ";
                if (optionYes > optionNo) {
                message += "angenommen!";
                } else {
                message += "abgelehnt!";
                }
                message += "<BR><BR>";
                 */
            }
        }

        /*
        GenerateMessage.setIgnoreSmiley(true);
        if (v.getVoteMessage().length() > 0) {
            message += GenerateMessage.parseText(v.getVoteMessage()) + "<BR><BR>";
        }
        
        if (closed) {
            message += "Abstimmungsergebnis:<BR>";
        } else {
            message += "Vorl&auml;ufiges Abstimmungsergebnis:<BR>";
        }

        boolean firstUser = true;
        boolean userFound = false;

        message += "Ben&ouml;tigte Stimmen: " + v.getMinVotes() + "<BR>";

        for (Map.Entry<Integer, VoteOption> me : vo.entrySet()) {
            //dbg log.debug("Checking option " + me.getValue().getVoteOption() + " with id " + me.getKey());

            int count = 0;
            String users = "";

            if (voted.containsKey(me.getKey())) {
                //dbg log.debug("Voted contains this option (" + me.getKey() + ")");

                ArrayList<String> userVotes = voted.get(me.getKey());
                for (String userName : userVotes) {
                    count++;
                    if (users.equalsIgnoreCase("")) {
                        users += userName;
                    } else {
                        users += ", " + userName;
                    }
                }
            } else {
                count = 0;
            }

            if (me.getValue().isConstant()) {
                message += ML.getMLStr(me.getValue().getVoteOption(), locale);
            } else {
                // Check for parametrized constants and translate
                String baseContent = me.getValue().getVoteOption();
                String replacedContent = VoteUtilities.translateParametrizedString(baseContent, locale);

                // message += me.getValue().getVoteOption();
                message += replacedContent;
            }

            message += " (" + count + "): " + users;
            message += "<BR>";
        }
        */
        return message;
    }        
}
