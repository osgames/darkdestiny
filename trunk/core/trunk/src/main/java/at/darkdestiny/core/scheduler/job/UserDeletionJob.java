/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.scheduler.job;

 import at.darkdestiny.core.CheckForUpdate;import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.DestructionUtilities;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
;
import java.util.ArrayList;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Admin
 */
public class UserDeletionJob implements Job {

    public void execute(JobExecutionContext jec) throws JobExecutionException {
        CheckForUpdate.requestUpdate();

        deleteInactive();

    }

    public void deleteInactive(){
        try {

            CheckForUpdate.requestUpdate();
            boolean foundUserToDelete = false;
            ArrayList<User> toDelete = new ArrayList<User>();
            for (User u : (ArrayList<User>) Service.userDAO.findAll()) {
                if (u.getUserId() == 0 || u.getUserType().equals(EUserType.AI)) {
                    continue;
                }
                if (u.getDeleteDate() != null && u.getDeleteDate() > 0) {
                    foundUserToDelete = true;
                    toDelete.add(u);
                }
            }
            if (foundUserToDelete) {
                for (User u : toDelete) {
                    DebugBuffer.addLine(DebugLevel.WARNING, "User " + u.getGameName() + " id: " + u.getId() + " deletionDate : " + u.getDeleteDate() + " acutalDate : " + GameUtilities.getCurrentTick2() + " (actualDate - DeletaionDate) > deletionDelay?: " + (GameUtilities.getCurrentTick2() - u.getDeleteDate()) + " > " + GameConstants.DELETION_DELAY);
                    if (GameUtilities.getCurrentTick2() - u.getDeleteDate() > GameConstants.DELETION_DELAY) {
                        DebugBuffer.addLine(DebugLevel.WARNING, "Deleting User");
                        DestructionUtilities.destroyPlayer(u.getId());
                    } else {
                        DebugBuffer.addLine(DebugLevel.WARNING, (GameConstants.DELETION_DELAY - (GameUtilities.getCurrentTick2() - u.getDeleteDate())) + " Ticks till deletion");
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error in deleteInactive.jsp : " + e);
            DebugBuffer.writeStackTrace("DeleteInactive error", e);
        }
    }
}
