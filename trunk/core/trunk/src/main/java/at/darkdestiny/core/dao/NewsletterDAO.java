/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Newsletter;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class NewsletterDAO extends ReadWriteTable<Newsletter> implements GenericDAO {
    public Newsletter getById(Integer newsletterId) {
        Newsletter n = new Newsletter();
        n.setId(newsletterId);
        return (Newsletter)get(n);
    }

    public ArrayList<Newsletter> findByAllowedEmail() {
        Newsletter n = new Newsletter();
        n.setAllowEmail(true);
        return (ArrayList<Newsletter>)find(n);
    }
}
