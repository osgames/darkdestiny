/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Research;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Eobane
 */
public class ResearchDAO extends ReadWriteTable<Research> implements GenericDAO{
    public Research findById(Integer id) {
        Research r = new Research();
        r.setId(id);
        return (Research) get(r);
    }
}
