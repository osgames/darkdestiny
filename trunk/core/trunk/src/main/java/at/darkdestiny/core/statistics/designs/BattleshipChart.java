/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.statistics.designs;

import at.darkdestiny.core.model.StatisticEntry;
import at.darkdestiny.core.statistics.Chart;
import at.darkdestiny.core.statistics.IChart;

/**
 *
 * @author Bullet
 */
public class BattleshipChart implements IChart {

    Chart chart;

    public BattleshipChart(StatisticEntry se, int userId) {

        ShipDesignChart sdc = new ShipDesignChart(se, 6);
        this.chart = sdc.getChart(userId);
    }

    public Chart getChart() {
        return chart;
    }
}
