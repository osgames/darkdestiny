/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.update.transport;

import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.core.update.TradeRouteDetailProcessing;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class DirectionalEdge {
    private final PlanetNode source;
    private final PlanetNode target;

    private final int orgMaxCapacity;
    private int maxCapacity;
    private int additionalCapacity;
    private final double distance;

    // ********** NEW CAPACITY SYSTEM **********
    private CapacityOneEdge capSingleEdge = null;
    private HashMap<Integer,RessCapacityOneEdge> ressCapSingleEdge = Maps.newHashMap();
    // *****************************************

    private HashMap<Integer,Integer> orgMaxCapacityByRes = Maps.newHashMap();
    private HashMap<Integer,Integer> maxCapacityByRes = Maps.newHashMap();
    private HashMap<Integer,Integer> additionalCapacityByRes = Maps.newHashMap();

    private final HashMap<Integer,TradeRouteDetailProcessing> detailEntries =
            new HashMap<Integer,TradeRouteDetailProcessing>();
    private HashMap<Integer,Integer> assignedQuantities =
            new HashMap<Integer,Integer>();

    public DirectionalEdge(PlanetNode source, PlanetNode target, TradeRouteExt tre) {
        this.source = source;
        this.target = target;
        this.maxCapacity = 0;
        this.orgMaxCapacity = 0;
        // this.maxCapacity = tre.getCapacity();
        // this.orgMaxCapacity = tre.getCapacity();

        if (tre.getCapSingleEdge() != null) {
            capSingleEdge = tre.getCapSingleEdge().getOriginalCopy();
        }
        
        /*
        if (capSingleEdge != null) {
            // System.out.println("DE " + source.getPlanetId() + " => " + target.getPlanetId() + ": MaxCap => " + capSingleEdge.getOrgMaxCapacity());
            // System.out.println("DE " + source.getPlanetId() + " => " + target.getPlanetId() + ": FreeCap => " + capSingleEdge.getFreeCapacity());
        }
        */
        RelativeCoordinate rcSource = new RelativeCoordinate(0,source.getPlanetId());
        RelativeCoordinate rcTarget = new RelativeCoordinate(0,target.getPlanetId());

        distance = rcSource.distanceTo(rcTarget);
    }

    public void addDetail(TradeRouteDetailProcessing trdp) {
        detailEntries.put(trdp.getBase().getRessId(), trdp);

        if (getCapSingleEdge() != null) {
            getCapSingleEdge().addTradeRouteDetailProcessing(trdp);
        }

        RessCapacityOneEdge cpoe = ressCapSingleEdge.get(trdp.getBase().getRessId());
        if (cpoe != null) {
            cpoe.addTradeRouteDetailProcessing(trdp);
        }
    }
    
    public int getOpenRoutes() {
        int open = 0;
        
        for (TradeRouteDetailProcessing trdp : detailEntries.values()) {
            if (!trdp.isClosed()) open++;
        }
        
        return open;
    }

    public void setAssignedQty(int ressId, int qty) {
        assignedQuantities.put(ressId,qty);
    }

    public void increaseAssignedQty(int ressId, int qty) {
        assignedQuantities.put(ressId,assignedQuantities.get(ressId) + qty);
    }

    public void decreaseAssignedQty(int ressId, int qty) {
        assignedQuantities.put(ressId,assignedQuantities.get(ressId) - qty);
    }

    public TradeRouteDetailProcessing getEntryForRess(int ressId) {
        return detailEntries.get(ressId);
    }

    public ArrayList<TradeRouteDetailProcessing> getDetailEntries() {
        ArrayList<TradeRouteDetailProcessing> trdpList = new ArrayList<TradeRouteDetailProcessing>();
        trdpList.addAll(detailEntries.values());

        return trdpList;
    }

    public int getNoOfEntries() {
        int number = 0;

        for (TradeRouteDetailProcessing trdp : detailEntries.values()) {
            if (!trdp.isClosed()) number++;
        }

        return number;
    }

    /**
     * @return the source
     */
    public PlanetNode getSource() {
        return source;
    }

    /**
     * @return the target
     */
    public PlanetNode getTarget() {
        return target;
    }

    /**
     * @return the distance
     */
    public double getDistance() {
        return distance;
    }

    /**
     * @return the capSingleEdge
     */
    public CapacityOneEdge getCapSingleEdge() {
        return capSingleEdge;
    }

    @Override
    public String toString() {
        if (this.getCapSingleEdge() != null) {
            return "{" + source.getPlanetId() + "=>" + target.getPlanetId() + "[FREE: " + this.getCapSingleEdge().getFreeCapacity() +
                    " ASSIGNED: " + (this.getCapSingleEdge().getOrgMaxCapacity() - this.getCapSingleEdge().getFreeCapacity()) + "]}";
        } else {
            return "{" + source.getPlanetId() + "=>" + target.getPlanetId() + "[FREE: <Invalid>" +
                    " ASSIGNED: <Invalid>]}";
        }
    }
}
