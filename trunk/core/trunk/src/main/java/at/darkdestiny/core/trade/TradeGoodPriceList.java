/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.trade;

import at.darkdestiny.core.dao.PriceListEntryDAO;
import at.darkdestiny.core.enumeration.TradeOfferType;
import at.darkdestiny.core.model.PriceListEntry;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class TradeGoodPriceList {
    private int id;
    private final int userId;
    private final TradeOfferType tot;
    private final int refId;
    private HashMap<Integer,PriceListEntry> entries = new HashMap<Integer,PriceListEntry>();

    private boolean initialized = false;

    private static PriceListEntryDAO pleDAO = (PriceListEntryDAO)DAOFactory.get(PriceListEntryDAO.class);

    public TradeGoodPriceList(int userId) {
        this.userId = userId;
        tot = TradeOfferType.PUBLIC;
        refId = 0;
    }

    public TradeGoodPriceList(int userId, TradeOfferType tot, int refId) {
        this.userId = userId;
        this.tot = tot;
        this.refId = refId;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @return the tot
     */
    public TradeOfferType getTot() {
        return tot;
    }

    /**
     * @return the refId
     */
    public int getRefId() {
        return refId;
    }

    private boolean initalize() {
        if (initialized) return true;
        if (id == 0) return false;

        ArrayList<PriceListEntry> pleList = pleDAO.findByPriceList(id);
        for (PriceListEntry ple : pleList) {
            entries.put(ple.getRessId(),ple);
        }

        initialized = true;
        return true;
    }

    public ArrayList<PriceListEntry> getAllPrices() {
        ArrayList<PriceListEntry> plList = new ArrayList<PriceListEntry>();
        if (!initalize()) return plList;

        for (Map.Entry<Integer,PriceListEntry> plEntry : entries.entrySet()) {
            plList.add(plEntry.getValue());
        }

        return plList;
    }

    public PriceListEntry getPrice(int ressId) {
        initalize();
        return entries.get(ressId);
    }

    public void setPrice(int ressId, int price) {
        PriceListEntry ple = new PriceListEntry();
        ple.setRessId(ressId);
        ple.setPrice(price);
        entries.put(ressId, ple);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
}
