/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TransmitterTargetsView {
    private ArrayList<TransmitterTargetEntry> transmitterDestinations = new ArrayList<TransmitterTargetEntry>();
    
    public void addTransmitterDestination(TransmitterTargetEntry tte) {
        System.out.println("ADD TARGET");
        transmitterDestinations.add(tte);
        System.out.println("TARGET COUNT = " + transmitterDestinations.size());
    }    
    
    public ArrayList<TransmitterTargetEntry> getConnectedTransmitters() {
        System.out.println("TARGET COUNT ON RETURN = " + transmitterDestinations.size());
        return transmitterDestinations;
    }
}
