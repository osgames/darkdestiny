/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.ships.BuildTime;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "productionorderprogress")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ProductionOrderProgress extends Model<ProductionOrderProgress> {
    @FieldMappingAnnotation("productionOrderId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer productionOrderId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("resourceId")
    @ColumnProperties(unsigned = true)
    private Integer resourceId;
    @FieldMappingAnnotation("needed")
    @DefaultValue("0")
    @ColumnProperties(unsigned = true)
    private Integer needed;
    @FieldMappingAnnotation("processed")
    @DefaultValue("0")
    @ColumnProperties(unsigned = true)
    private Integer processed;

    /**
     * @return the productionOrderId
     */
    public Integer getProductionOrderId() {
        return productionOrderId;
    }

    /**
     * @param productionOrderId the productionOrderId to set
     */
    public void setProductionOrderId(Integer productionOrderId) {
        this.productionOrderId = productionOrderId;
    }

    /**
     * @return the resourceId
     */
    public Integer getResourceId() {
        return resourceId;
    }

    /**
     * @param resourceId the resourceId to set
     */
    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * @return the needed
     */
    public Integer getNeeded() {
        return needed;
    }

    /**
     * @param needed the needed to set
     */
    public void setNeeded(Integer needed) {
        this.needed = needed;
    }

    /**
     * @return the processed
     */
    public Integer getProcessed() {
        return processed;
    }

    /**
     * @param processed the processed to set
     */
    public void setProcessed(Integer processed) {
        this.processed = processed;
    }

}
