/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import java.awt.Graphics;

/**
 *
 * @author Stefan
 */
public class ScannerRenderer implements IModifyImageFunction { 
    private int width;
    private int height;
    private final int userId;
    private final Integer picWidth;
    private final Integer picHeight;    
    private final StarMapDataSource dataSource;
    private final AbsoluteCoordinate topLeft;
    private final AbsoluteCoordinate bottomRight;
    
    public ScannerRenderer(int userId, int picWidth, int picHeight, AbsoluteCoordinate topLeft, AbsoluteCoordinate bottomRight, IMapDataSource imds) {
        this.userId = userId;
        this.picWidth = picWidth;
        this.picHeight = picHeight;
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
        this.dataSource = (StarMapDataSource)imds;
    }
    
    @Override
    public void run(Graphics graphics) {
    
    }       
    
    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }    
    
    @Override
    public String createMapEntry() {
        return "";
    }    
}
