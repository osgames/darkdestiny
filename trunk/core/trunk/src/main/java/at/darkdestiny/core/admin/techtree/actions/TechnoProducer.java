package at.darkdestiny.core.admin.techtree.actions;

import at.darkdestiny.core.admin.IMyPageProducer;
import at.darkdestiny.core.admin.TechTreeAdmin;
import at.darkdestiny.core.admin.db.IDatabaseColumn;
import at.darkdestiny.core.admin.techtree.ConstructionTechTreeEntry;
import at.darkdestiny.core.admin.techtree.GroundTroopTechTreeEntry;
import at.darkdestiny.core.admin.techtree.ModulTechTreeEntry;
import at.darkdestiny.core.admin.techtree.TechTree;
import at.darkdestiny.framework.access.DbConnect;
import static java.lang.Math.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;

/**
 * Hier wird eine Technologie dargestellt
 *
 * @author martin
 */
public class TechnoProducer implements IMyPageProducer {

	private int id;
	private HttpServletRequest request;

	/**
	 *
	 */
	public TechnoProducer(int id, HttpServletRequest request) {
		this.id = id;
		this.request = request;
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.admin.IMyPageProducer#producePage()
	 */
	public String producePage() throws SQLException {

		if (id < 0)
		{
			Statement state = DbConnect.getConnection().createStatement();
			if (id == -1)
			{
				//Niedrigste freie ID suchen
				ResultSet rs = state.executeQuery("SELECT id FROM research ORDER BY ID ASC");
				id = 1;
				boolean found = false;
				while (rs.next() && !found)
				{
					int tmp = rs.getInt(1);
					if (tmp > id)
						found = true;
					else id = tmp+1;
				}
				state.execute("INSERT INTO research VALUES ("+id+",'neue Technologie',0,'','','','')", Statement.RETURN_GENERATED_KEYS);
			}
			else if (id == -2)
			{
				state.execute("INSERT INTO module SET name='neues Modul'", Statement.RETURN_GENERATED_KEYS);
				ResultSet rs =state.getGeneratedKeys();
				if (rs.next())
					id = ModulTechTreeEntry.makeID(rs.getInt(1));
			}
			else if (id == -3)
			{
				state.execute("INSERT INTO construction SET name='neues Gebaeude'", Statement.RETURN_GENERATED_KEYS);
				ResultSet rs =state.getGeneratedKeys();
				if (rs.next())
					id = ConstructionTechTreeEntry.makeID(rs.getInt(1));
			}
			else if (id == -4)
			{
				//Niedrigste freie ID suchen
				ResultSet rs = state.executeQuery("SELECT id FROM groundtroops ORDER BY ID ASC");
				id = 1;
				boolean found = false;
				while (rs.next() && !found)
				{
					int tmp = rs.getInt(1);
					if (tmp > id)
						found = true;
					else id = tmp+1;
				}
				state.execute("INSERT INTO groundtroops SET id="+id+", Name='GroundTroop 0'");
				id = GroundTroopTechTreeEntry.makeID(id);
			}
			TechTreeAdmin.forceReload();
		}

		if (TechTreeAdmin.techTree == null)
			TechTreeAdmin.loadTechTree();
		TechTree tt = TechTreeAdmin.allTechnologies.get(id);
		updateTechno(tt);

		if (TechTreeAdmin.techTree == null)
			TechTreeAdmin.loadTechTree();
		tt = TechTreeAdmin.allTechnologies.get(id);

		StringBuffer res = new StringBuffer();
		if (tt == null)
			return "Leider konnte die Technologie \""+id+"\" nicht gefunden werden";

		showTechno(tt,res);

		return res.toString();
	}

	private void showDependencies(TechTree tt, StringBuffer res)
	{
		res.append("<p>Die Abh&auml;ngigkeiten:<br>\n");
		res.append("<table border=\"1\">\n");
		res.append("<tr><th>H&auml;ngt ab von</th><th></th><th>Davon h&auml;ngen ab</th><th></th></tr>\n");
		for (int i = 0; i < max(tt.getDependencies().size(), tt.getDependsOn().size()); i++)
		{
			res.append("<tr><td>");
			if (i < tt.getDependsOn().size())
			{
				TechTree t = tt.getDependsOn().get(i);
				res.append(""+TechTreeAdmin.makeLink(t));
				res.append("</td><td>");
				res.append(TechTreeAdmin.makeLink("Delete Dependency",TechTreeAdmin.ACTION_DELETE_DEPENDENCY, "tid="+t.getID()+"&id="+tt.getID()));
			}
			else res.append("</td><td>");
			res.append("</td><td>");

			if (i < tt.getDependencies().size())
			{
				TechTree t = tt.getDependencies().get(i);
				res.append(""+TechTreeAdmin.makeLink(t));
				res.append("</td><td>");
				res.append(TechTreeAdmin.makeLink("Delete Dependency",TechTreeAdmin.ACTION_DELETE_DEPENDENCY, "id="+t.getID()+"&tid="+tt.getID()));
			}
			else res.append("</td><td>");
			res.append("</td></tr>");
		}

		res.append("<tr><td></td><td>"+
				TechTreeAdmin.makeLink("Add Dependency",TechTreeAdmin.ACTION_ADD_DEPENDENCY, "id="+tt.getID())
				+"</td><td></td><td>"+
				TechTreeAdmin.makeLink("Add Dependency",TechTreeAdmin.ACTION_ADD_DEPENDENCY, "tid="+tt.getID())
				+"</td></tr></table>");
	}

	private void showTechno(TechTree tt, StringBuffer res) {
		{
			res.append("<h2>"+tt.getTitle()+" \""+tt.getName()+"\"</h2><br>\n");
			showDependencies(tt, res);
			res.append("<p><h3>Infos &uuml;ber "+tt.getTitle()+"</h3><br>\n");
			res.append("<form action=\""+TechTreeAdmin.BASEPATH+"\" method=\"POST\">\n");
			res.append("<input type=\"hidden\" name=\"action\" value=\""+TechTreeAdmin.ACTION_ONE_ITEM+"\" />\n");
			res.append("<input type=\"hidden\" name=\"id\" value=\""+id+"\" />\n");
			res.append("<table border=\"1\">");

			for (IDatabaseColumn col : tt.getAllDBColumns())
			{
				res.append("<tr><td>"+col.getName()+"</td><td>"+col.getComment()+"</td><td>"+col.makeEdit(tt.get(col.getName()))+"</td></tr>\n");
			}

			res.append("<tr><td><input type=\"submit\" /></td><td><input type=\"reset\" /></td></tr>\n");

			res.append("</table></from>");

			res.append("<br><br>");
			res.append(TechTreeAdmin.makeLink(tt.getName()+" l&ouml;schen", TechTreeAdmin.ACTION_DELETE_TECH, "id="+tt.getID()));
		}
	}


	private void updateTechno(TechTree tt) throws SQLException {
		if (request.getParameter("f_name") != null)
		{
			Statement state = DbConnect.getConnection().createStatement();
			for (IDatabaseColumn col : tt.getAllDBColumns())
				if (!"id".equalsIgnoreCase(col.getName()))
				{
					col.update(state, tt.getTable(), request.getParameter("f_"+col.getName()), tt.getBaseID());
				}

			TechTreeAdmin.forceReload();
		}
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.admin.IMyPageProducer#setup(java.lang.Object)
	 */
	public void setup(Object o) {
        // Empty on purpose
	}

}
