/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

/**
 *
 * @author Aion
 */
public class AttackPossibleResult {

    private int userId;
    private String color;
    private String name;

    public AttackPossibleResult(int userId, String color, String name) {
        this.userId = userId;
        this.color = color;
        this.name = name;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }




}
