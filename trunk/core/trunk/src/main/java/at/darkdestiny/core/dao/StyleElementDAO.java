/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.StyleElement;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class StyleElementDAO extends ReadOnlyTable<StyleElement> implements GenericDAO {

    public StyleElement findBy(int id, int styleId) {
        StyleElement se = new StyleElement();
        se.setId(id);
        se.setStyleId(styleId);
        return (StyleElement) get(se);
    }

    public ArrayList<StyleElement> findByStyleId(int styleId) {
        StyleElement se = new StyleElement();
        se.setStyleId(styleId);
        return find(se);

    }
}
