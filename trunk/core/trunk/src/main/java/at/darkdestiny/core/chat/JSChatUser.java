/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.chat;

/**
 *
 * @author Stefan
 */
public class JSChatUser {
    private final String name;
    private boolean inactive = false;
    
    public JSChatUser(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the inactive
     */
    public boolean isInactive() {
        return inactive;
    }

    /**
     * @param inactive the inactive to set
     */
    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }
}
