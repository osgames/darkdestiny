/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.update.transport;

/**
 *
 * @author Stefan
 */
public class CapacityOneEdge extends AbstractCapacity {
    public CapacityOneEdge(int capacity) {
        super(capacity);
    }
    
    public CapacityOneEdge getOriginalCopy() {
        CapacityOneEdge coe = new CapacityOneEdge(this.getOrgMaxCapacity());
        return coe;
    }
}
