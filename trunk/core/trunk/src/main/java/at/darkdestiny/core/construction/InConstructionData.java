/*
 * InConstructionData.java
 *
 * Created on 05. Mai 2007, 21:52
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.darkdestiny.core.construction;

import at.darkdestiny.core.dao.ProductionDAO;
import at.darkdestiny.core.model.Production;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class InConstructionData {
    private ConstructCount cData;
    private ArrayList<Production> pData;

    private ProductionDAO pDAO = (ProductionDAO)DAOFactory.get(ProductionDAO.class);

    /** Creates a new instance of InConstructionData */
   public InConstructionData(ConstructCount cData) {
        this.cData = cData;

        pData = pDAO.findProductionForConstructionId(cData.getConstruct().getId());
    }

    /**
     * Liefert die Geb&auml;udedaten dieses Geb&auml;udes
     *
     * @return Eine RessourceCost Klasse mit Daten und Anzahl von Geb&auml;ude dieses Typs in Bau
     */
    public ConstructCount getConstructionData() {
        return cData;
    }

    /**
     * Liefert Produktions-/Verbrauchseintr&auml;ge f&uuml;r alle Ressourcen dieses Geb&auml;udes
     *
     * @return Eine ArrayList mit ProductionEntry Klassen falls ein Eintrag gefunden wurde, ansonsten NULL
     */
    public ArrayList<Production> getProductionEntries() {
        return pData;
    }

    /**
     * Retourniert den Produktionseintrag f&uuml;r eine bestimmte Ressource
     *
     * @param ressId die Ressource &uuml;ber die Informationen ben&ouml;tigt werden
     * @return Eine ProductionEntry Klasse falls ein Eintrag gefunden wurde, ansonsten NULL
     */
    public Production getProductionEntryByRessId(int ressId) {
        Production p = null;

        for (Production tmp : pData) {
            if (tmp.getRessourceId() == ressId) {
                p = tmp;
                break;
            }
        }

        return p;
    }
}
