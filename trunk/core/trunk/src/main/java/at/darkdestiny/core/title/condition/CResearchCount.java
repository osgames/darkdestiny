package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.framework.dao.DAOFactory;

public class CResearchCount extends AbstractCondition {

    private ParameterEntry researchCountValue;
    private ParameterEntry comparator;
    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);

    public CResearchCount(ParameterEntry researchCountValue, ParameterEntry comparator) {
        this.researchCountValue = researchCountValue;
        this.comparator = comparator;
    }

    public boolean checkCondition(int userId, int conditionToTitleId) {
        int researchCount = prDAO.findByUserId(userId).size();
        return compare(researchCountValue.getParamValue().getValue(), researchCount, researchCountValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

    }
}
