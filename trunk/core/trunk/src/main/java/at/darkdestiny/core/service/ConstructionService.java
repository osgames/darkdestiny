/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.dao.*;
import at.darkdestiny.core.orderhandling.orders.*;
import at.darkdestiny.core.result.*;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.construction.ConstructionScrapAbortCost;
import at.darkdestiny.core.construction.ConstructionScrapCost;
import at.darkdestiny.core.construction.InConstructionData;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EBonusType;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.enumeration.ERessourceCostType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Production;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.RessourceCost;
import at.darkdestiny.core.orderhandling.OrderHandling;
import at.darkdestiny.core.orderhandling.result.OrderResult;
import at.darkdestiny.core.utilities.BonusUtilities;
import at.darkdestiny.core.utilities.ConstructionUtilities;
import at.darkdestiny.core.view.ConstructionProgressView;
import at.darkdestiny.core.view.IndustryPointsView;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class ConstructionService {

    private static final Logger log = LoggerFactory.getLogger(ConstructionService.class);

    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);

    @Deprecated
    public static LinkedList<InConstructionData> getInConstructionData(int planetId) {
        LinkedList<InConstructionData> inCons = new LinkedList<InConstructionData>();

        // TODO Implement

        return inCons;
    }

    public static boolean isShowConstructionDetails(int userId) {
        return Service.userSettingsDAO.findByUserId(userId).getShowConstructionDetails();
    }

    public static boolean isShowProductionDetails(int userId) {
        return Service.userSettingsDAO.findByUserId(userId).getShowProductionDetails();
    }

    public static synchronized ArrayList<String> buildBuilding(int userId, Map params) {
        int planetId = 0;
        int constructionId = 0;
        int number = 0;
        ArrayList<String> errorList = new ArrayList<String>();
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String key = (String) me.getKey();
            if (key.equals("planetId")) {
                planetId = Integer.parseInt(((String[]) me.getValue())[0]);

            } else if (key.equals("cId")) {
                constructionId = Integer.parseInt(((String[]) me.getValue())[0]);

            } else if (key.equals("number")) {
                number = Integer.parseInt(((String[]) me.getValue())[0]);
            }
        }
        if (number <= 0) {
            errorList.add(ML.getMLStr("construction_err_nonegativenumbers", userId));
            return errorList;
        }

        if (planetId > 0 && constructionId > 0 && number > 0) {
            errorList.addAll(buildBuilding(userId, planetId, constructionId, number));
        } else {
            errorList.add(ML.getMLStr("administration_err_errorwhilebuilding", userId));
        }
        return errorList;
    }

    public static synchronized ArrayList<String> upgradeBuilding(int userId, int planetId, int constructionId, int newConstructionId, int number) {
        ArrayList<String> errorList = new ArrayList<String>();

        log.debug("1");
        Construction c = Service.constructionDAO.findById(constructionId);
        Construction cNew = Service.constructionDAO.findById(newConstructionId);

        log.debug("2");
        ConstructionUpgradeResult cur = ConstructionUtilities.canBeUpgraded(c, userId, planetId, number, cNew);
        if (!cur.isBuildable()) {
            errorList.add(ML.getMLStr("construction_msg_constructionnotpossible", userId));
            return errorList;
        }
        if (number <= 0) {
            errorList.add(ML.getMLStr("construction_err_nonegativenumbers", userId));
            return errorList;
        }

        log.debug("3");
        ConstructionExt cEntry = new ConstructionExt(constructionId);
        ConstructionExt cEntryNew = new ConstructionExt(newConstructionId);
        ConstructionUpgradeOrder cuo = new ConstructionUpgradeOrder(cEntry, cEntryNew, number, planetId, userId);

        log.debug("4");
        OrderResult oRes = OrderHandling.upgradeOrder(cuo);

        log.debug("5x");
        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }


        return errorList;
    }

    public static synchronized ArrayList<String> improveBuilding(int userId, int planetId, int constructionId) {
        ArrayList<String> errorList = new ArrayList<String>();

        Construction c = Service.constructionDAO.findById(constructionId);

        BuildableResult br = ConstructionUtilities.canBeImproved(c, userId, planetId);
        if (!br.isBuildable()) {
            errorList.add(ML.getMLStr("construction_msg_constructionnotpossible", userId));
            return errorList;
        }
        ConstructionExt cEntry = new ConstructionExt(constructionId);
        ConstructionImproveOrder cio = new ConstructionImproveOrder(cEntry, planetId, userId);

        OrderResult oRes = OrderHandling.constructOrder(cio);

        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }


        return errorList;
    }

    public static synchronized ArrayList<String> buildBuilding(int userId, int planetId, int constructionId, int number) {
        return buildBuilding(userId, planetId, constructionId, number, -1);
    }

    public static synchronized ArrayList<String> buildBuilding(int userId, int planetId, int constructionId, int number, int priority) {
        ArrayList<String> errorList = new ArrayList<String>();
        Construction c = Service.constructionDAO.findById(constructionId);
        BuildableResult br = ConstructionUtilities.canBeBuilt(c, userId, planetId, number);
        if (!br.isBuildable()) {
            errorList.add(ML.getMLStr("construction_msg_constructionnotpossible", userId));
            errorList.addAll(br.getMissingRequirements());
            return errorList;
        }

        if (number <= 0) {
            return errorList;
        }

        ConstructionExt cEntry = new ConstructionExt(constructionId);
        ConstructionConstructOrder cco = new ConstructionConstructOrder(cEntry, planetId, userId, number, priority);

        OrderResult oRes = OrderHandling.constructOrder(cco);

        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        return errorList;
    }

    public static BaseResult idleBuilding(int userId, int planetId, int constructionId, int count) {
        PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(planetId);
        if (pp.getUserId() != userId) {
            return new BaseResult("Dieser Planet geh�rt nicht ihnen", true);
        }
        PlanetConstruction pc = Service.planetConstructionDAO.findBy(planetId, constructionId);
        if (pc == null) {
            return new BaseResult("Geb�ude nicht vorhanden", true);
        }
        if (pc.getNumber() < count) {
            return new BaseResult("Soviele Geb�ude sind nicht vorhanden", true);
        }
        if (count < 0) {
            return new BaseResult("Scherzkeks, versuchs doch mit ner positiven Zahl", true);
        }

        Construction c = Service.constructionDAO.findById(constructionId);
        if (!c.isIdleable()) {
            return new BaseResult("Dieses Geb�ude kann nicht gestoppt werden, wir werden alle sterben!1!!!11!!!", true);
        }

        pc.setIdle(count);
        Service.planetConstructionDAO.update(pc);
        return new BaseResult("", false);
    }

    @Deprecated
    public static ArrayList<ConstructionProgressView> findRunningConstructions(int planetId, int userId) {
        ArrayList<Action> actions = Service.actionDAO.findRunningConstructions(planetId, userId);
        TreeMap<Integer, ArrayList<ConstructionProgressView>> constructions = new TreeMap<Integer, ArrayList<ConstructionProgressView>>();

        for (Action a : actions) {

            ConstructionProgressView cpv = new ConstructionProgressView();
            cpv.setAction(a);
            ProductionOrder po = Service.productionOrderDAO.findById(a.getRefTableId());
            cpv.setProductionOrder(po);

            cpv.setConstruction(Service.constructionDAO.findById(a.getConstructionId()));
            ArrayList<ConstructionProgressView> tmp = null;
            if (constructions.get(po.getPriority()) == null) {
                tmp = new ArrayList<ConstructionProgressView>();
            } else {
                tmp = constructions.get(po.getPriority());
            }
            tmp.add(cpv);
            constructions.put(po.getPriority(), tmp);

        }
        ArrayList<ConstructionProgressView> result = new ArrayList<ConstructionProgressView>();
        for (Map.Entry<Integer, ArrayList<ConstructionProgressView>> entry : constructions.entrySet()) {
            result.addAll(entry.getValue());
        }
        return result;

    }

    public static InConstructionResult findRunnigConstructions(int planetId) {
        ArrayList<Action> actionList = new ArrayList<Action>();
        actionList.addAll(Service.actionDAO.findByPlanet(planetId));

        InConstructionResult icr = new InConstructionResult();
        TreeMap<Integer, ArrayList<InConstructionEntry>> tmpResult = new TreeMap<Integer, ArrayList<InConstructionEntry>>();
        for (Action a : actionList) {
            if ((a.getType() != EActionType.BUILDING)
                    && (a.getType() != EActionType.DECONSTRUCT)) {
                continue;
            }

            ProductionOrder po = Service.productionOrderDAO.findByAction(a);


            Model m = null;
            if (a.getType() == EActionType.BUILDING
                    || a.getType() == EActionType.DECONSTRUCT) {
                m = Service.constructionDAO.findById(a.getConstructionId());
            }
            ArrayList<InConstructionEntry> entries = tmpResult.get(po.getPriority());
            if (entries == null) {
                entries = new ArrayList<InConstructionEntry>();
            }
            entries.add(new InConstructionEntry(po, a, m));
            tmpResult.put(po.getPriority(), entries);
        }
        for (Map.Entry<Integer, ArrayList<InConstructionEntry>> entry : tmpResult.entrySet()) {
            icr.addEntries(entry.getValue());
        }
        return icr;

    }

    public static IndustryPointsView getIndustryPointsView(int planetId) {
        int indPoints = getIndustryPoints(planetId);

        int[] extractedBonus = BonusUtilities.calculateValue(planetId, indPoints, EBonusType.CONSTRUCTION);

        return new IndustryPointsView(extractedBonus[0], extractedBonus[1]);
    }

    public static int getIndustryPoints(int planetId) {
        // 6*log(col(A))^2*0.9+(col(A))/(4*10^7)
        PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(planetId);

        if (pp.getPopulation() <= 0) {
            return 0;
        }

        long population = pp.getPopulation();
        double valueA = (Math.pow(Math.log10(population), 2d) * 0.9d);

        if (Service.planetConstructionDAO.findBy(planetId, Construction.ID_PLANETGOVERNMENT) != null) {
            valueA *= 6;
        } else if (Service.planetConstructionDAO.findBy(planetId, Construction.ID_PLANETADMINISTRATION) != null) {
            valueA *= 4;
        } else if (Service.planetConstructionDAO.findBy(planetId, Construction.ID_LOCALADMINISTRATION) != null) {
            valueA *= 2;
        }

        double valueB = (population / (4 * Math.pow(10d, 7d)));
        double totalBase = valueA + valueB;

        double adjust = (200 - totalBase);
        if (adjust < -15) {
            adjust = -15;
        }

        totalBase += 35d / 200d * ((adjust) + 15d);

        BonusResult br = BonusUtilities.findBonus(planetId, EBonusType.CONSTRUCTION);
        if (br != null) {
            totalBase += (totalBase * br.getPercBonus()) + br.getNumericBonus();
        }

        return (int) Math.ceil(totalBase);
    }

    public static void setNewPriority(int orderNo, Integer priority) {

        ProductionOrder po = Service.productionOrderDAO.findById(orderNo);
        if (po == null) return;
        
        po.setPriority(priority);
        Service.productionOrderDAO.update(po);
    }

    public static ArrayList<Construction> findConstructionByType(EConstructionType ect) {
        return Service.constructionDAO.findByType(ect);
    }    
    
    public static Construction findConstructionById(int constructionId) {
        return Service.constructionDAO.findById(constructionId);
    }

    public static ProductionBuildResult getConstructionList(int userId, int planetId, EConstructionType type) {
        ProductionBuildResult pbr = new ProductionBuildResult();

        ArrayList<Construction> cList = Service.constructionDAO.findByType(type);
        for (Construction c : cList) {
            ConstructionExt ce = new ConstructionExt(c.getId());
            BuildableResult br = ConstructionUtilities.canBeBuilt(c, userId, planetId, 1);
            pbr.addProduction(ce, br);
        }

        return pbr;
    }

    public static Ressource findRessourceById(int ressourceId) {
        return Service.ressourceDAO.findRessourceById(ressourceId);

    }

    public static ArrayList<Production> findProductionForConstructionId(int planetId, int constructionId) {
        TreeMap<Integer, TreeMap<Integer, ArrayList<Production>>> tmp = new TreeMap<Integer, TreeMap<Integer, ArrayList<Production>>>();
        ArrayList<Production> result = Service.productionDAO.findProductionForConstructionId(constructionId);



        for (Production p : result) {
            TreeMap<Integer, ArrayList<Production>> incDec = tmp.get(p.getDecinc());
            if (incDec == null) {
                incDec = new TreeMap<Integer, ArrayList<Production>>();
            }
            Ressource r = Service.ressourceDAO.findRessourceById(p.getRessourceId());

            if (constructionId == Construction.ID_SOLARPLANT) {
                if (p.getRessourceId() == Ressource.ENERGY) {
                    Planet planet = Service.planetDAO.findById(planetId);

                    int orbitLevel = planet.getOrbitLevel();
                    float factor = 1f;

                    if (orbitLevel < 6) {
                        factor += (6f - orbitLevel) * 0.1f;
                    } else if (orbitLevel > 6) {
                        factor -= (orbitLevel - 6f) * 0.1f;
                    }

                    p.setQuantity((long) Math.floor((float) p.getQuantity() * factor));
                }
            }

            ArrayList<Production> prods = incDec.get(r.getId());
            if (prods == null) {
                prods = new ArrayList<Production>();
            }
            prods.add(p);
            incDec.put(r.getId(), prods);
            tmp.put(p.getDecinc(), incDec);

        }
        result.clear();
        for (Map.Entry<Integer, TreeMap<Integer, ArrayList<Production>>> entry : tmp.entrySet()) {
            for (Map.Entry<Integer, ArrayList<Production>> entry2 : entry.getValue().entrySet()) {
                result.addAll(entry2.getValue());
            }
        }

        return result;
    }

    public static ProductionBuildResult getConstructionListSorted(int userId, int planetId, List<EConstructionType> types) {
        // log.debug("a");
        ProductionBuildResult pbr = new ProductionBuildResult();

        // log.debug("b");
        ArrayList<Construction> cList = new ArrayList<Construction>();
        for (EConstructionType type : types) {
            cList.addAll(Service.constructionDAO.findByType(type));
        }
        for (Construction c : cList) {
            ConstructionExt ce = new ConstructionExt(c.getId());
            BuildableResult br = ConstructionUtilities.canBeBuilt(c, userId, planetId, 1);
            if (br.isDeniedByLandType()) {
                continue;
            } else if (br.isDeniedByUnique()) {
                continue;
            } else if (br.isDeniedByMineableRessource()) {
                continue;
            } else if (br.isDeniedByOutdated()) {
                continue;
            } else if (br.isDeniedByLockedTech()) {
                continue;
            }
            // log.debug("1");
            pbr.addConstruction(ce, br);
        }

        return pbr;
    }

    public static ArrayList<RessourceCost> findRessourceCostForConstructionId(int constructionId) {
        ArrayList<RessourceCost> result = Service.ressourceCostDAO.find(ERessourceCostType.CONSTRUCTION, constructionId);
        TreeMap<Integer, ArrayList<RessourceCost>> tmp = new TreeMap<Integer, ArrayList<RessourceCost>>();
        for (RessourceCost rc : result) {

            Ressource r = Service.ressourceDAO.findRessourceById(rc.getRessId());
            ArrayList<RessourceCost> ress = tmp.get(r.getDisplayLocation());
            if (ress == null) {
                ress = new ArrayList<RessourceCost>();
            }
            ress.add(rc);
            tmp.put(r.getDisplayLocation(), ress);

        }
        result.clear();
        for (Map.Entry<Integer, ArrayList<RessourceCost>> entry : tmp.entrySet()) {
            result.addAll(entry.getValue());
        }

        return result;
    }

    public static ArrayList<String> abortDeconstruction(int orderId, int userId, int count) {
        ArrayList<String> errorList = new ArrayList<String>();

        ProductionOrder po = Service.productionOrderDAO.findById(orderId);
        
        if (po == null) {
            errorList.add(ML.getMLStr("construction_err_ordernotexisting", userId));
            return errorList;
        }        
        
        Action a = Service.actionDAO.findByTimeFinished(orderId, EActionType.DECONSTRUCT);
        int constructionId = a.getConstructionId();
        int planetId = a.getPlanetId();

        if (a.getNumber() < count) {
            errorList.add(ML.getMLStr("construction_err_notenoughconstructionsexisting", userId));
            return errorList;
        }
        
        if (count <= 0) {
            errorList.add(ML.getMLStr("construction_err_nonegativenumbers", userId));
            return errorList;
        }

        ConstructionExt cEntry = new ConstructionExt(constructionId);
        ConstructionScrapAbortOrder csao = new ConstructionScrapAbortOrder(po, cEntry, count, userId, planetId);

        OrderResult oRes = OrderHandling.abortOrder(csao);

        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        return errorList;
    }

    public static ArrayList<String> abortConstruction(int orderId, int userId, int count) {
        ArrayList<String> errorList = new ArrayList<String>();
        ProductionOrder po = Service.productionOrderDAO.findById(orderId);
        
        if (po == null) {
            errorList.add(ML.getMLStr("construction_err_ordernotexisting", userId));
            return errorList;
        }        

        Action a = Service.actionDAO.findByTimeFinished(orderId, EActionType.BUILDING);
        int constructionId = a.getConstructionId();
        int planetId = a.getPlanetId();
 
        if (a.getNumber() < count) {
            errorList.add(ML.getMLStr("construction_err_notenoughconstructionsexisting", userId));
            return errorList;
        }
        
        if (count <= 0) {
            errorList.add(ML.getMLStr("construction_err_nonegativenumbers", userId));
            return errorList;
        }

        ConstructionExt cEntry = new ConstructionExt(constructionId);
        ConstructionAbortOrder cao = new ConstructionAbortOrder(po, cEntry, count, userId, planetId);

        OrderResult oRes = OrderHandling.abortOrder(cao);

        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        return errorList;
    }

    public static ArrayList<String> deconstructConstruction(int planetId, int constructionId, int userId, int count) {

        ArrayList<String> errorList = new ArrayList<String>();
        Construction c = Service.constructionDAO.findById(constructionId);
        PlanetConstruction pc = Service.planetConstructionDAO.findBy(planetId, constructionId);
        PlanetDefense pd = Service.planetDefenseDAO.findBy(planetId, constructionId, EDefenseType.TURRET);
        if (!c.getType().equals(EConstructionType.PLANETARY_DEFENSE) && pc == null) {
            errorList.add(ML.getMLStr("construction_err_constructionnotexisting", userId));
            return errorList;

        } else if (c.getType().equals(EConstructionType.PLANETARY_DEFENSE) && pd == null) {
            errorList.add(ML.getMLStr("construction_err_constructionnotexisting", userId));
            return errorList;
        }
        if (!c.getType().equals(EConstructionType.PLANETARY_DEFENSE) && pc.getNumber() < count) {
            errorList.add(ML.getMLStr("construction_err_notenoughconstructionsexisting", userId));
            return errorList;
        }
        if (c.getType().equals(EConstructionType.PLANETARY_DEFENSE) && pd.getCount() < count) {
            errorList.add(ML.getMLStr("construction_err_notenoughconstructionsexisting", userId));
            return errorList;
        }
        if (count <= 0) {
            errorList.add(ML.getMLStr("construction_err_nonegativenumbers", userId));
            return errorList;
        }

        if (!c.isDestructible()) {
            errorList.add(ML.getMLStr("construction_err_notdestructible", userId));
        }
        ConstructionExt cEntry = new ConstructionExt(constructionId);
        ConstructionScrapOrder cso = new ConstructionScrapOrder(cEntry, planetId, userId, count);

        OrderResult oRes = OrderHandling.scrapOrder(cso);

        if (!oRes.isSuccess()) {
            errorList.addAll(oRes.getErrors());
        }

        return errorList;
    }

    public static TreeMap<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>> findPlanetConstructions(int planetId) {
        TreeMap<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>> tmpResult = new TreeMap<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>>();
        ArrayList<PlanetConstruction> constructions = Service.planetConstructionDAO.findByPlanetId(planetId);
        for (PlanetConstruction pc : constructions) {
            Construction c = Service.constructionDAO.findById(pc.getConstructionId());
            TreeMap<Integer, ArrayList<PlanetConstruction>> orderNo = tmpResult.get(c.getType());
            if (orderNo == null) {
                orderNo = new TreeMap<Integer, ArrayList<PlanetConstruction>>();
            }
            ArrayList<PlanetConstruction> tmp = orderNo.get(c.getOrderNo());
            if (tmp == null) {
                tmp = new ArrayList<PlanetConstruction>();
            }

            tmp.add(pc);
            orderNo.put(c.getOrderNo(), tmp);

            tmpResult.put(c.getType(), orderNo);
        }
        return tmpResult;
    }

    public static ScrapConstructionResult getScrapResult(int buildingId, int planetId) {
        ScrapConstructionResult scr = null;

        ConstructionExt ce = new ConstructionExt(buildingId);
        ConstructionScrapCost csc = ce.getScrapCost(planetId);

        int count = 0;
        if (((Construction) ce.getBase()).getType().equals(EConstructionType.PLANETARY_DEFENSE)) {
            PlanetDefense pd = Service.planetDefenseDAO.findBy(planetId, buildingId, EDefenseType.TURRET);
            count = pd.getCount();
        } else {
            PlanetConstruction pc = pcDAO.findBy(planetId, buildingId);
            count = pc.getNumber();
        }

        scr = new ScrapConstructionResult(csc, (Construction) ce.getBase(), count);

        return scr;
    }

    public static boolean orderIdIsValid(int orderId) {
        ProductionOrder po = Service.productionOrderDAO.findById(orderId);
        return (po != null);        
    }
    
    public static AbortConstructionResult getAbortRefund(int orderId) {
        AbortConstructionResult acr = null;

        Action a = aDAO.findByTimeFinished(orderId, EActionType.BUILDING);
        ConstructionExt ce = new ConstructionExt(a.getConstructionId());

        acr = new AbortConstructionResult((Construction) ce.getBase(), a, ce.getAbortRefund(orderId));

        return acr;
    }

    public static AbortDeconstructionResult getDeconstructionAbortCost(int orderId) {
        AbortDeconstructionResult adr = null;

        Action a = aDAO.findByTimeFinished(orderId, EActionType.DECONSTRUCT);
        ConstructionExt ce = new ConstructionExt(a.getConstructionId());
        ConstructionScrapAbortCost csac = new ConstructionScrapAbortCost(ce, orderId);

        adr = new AbortDeconstructionResult((Construction) ce.getBase(), a, csac);

        return adr;
    }

    public static synchronized void compressOrders(int userId, int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        if ((pp == null) || (pp.getUserId() != userId)) {
            return;
        }

        InConstructionResult icr = findRunnigConstructions(planetId);
        TreeMap<Integer, TreeMap<Integer, InConstructionEntry>> sorted = new TreeMap<Integer, TreeMap<Integer, InConstructionEntry>>();

        for (InConstructionEntry ice : icr.getEntries()) {
            if (!sorted.containsKey(ice.getOrder().getPriority())) {
                sorted.put(ice.getOrder().getPriority(), new TreeMap<Integer, InConstructionEntry>());
            }

            sorted.get(ice.getOrder().getPriority()).put(ice.getOrder().getId(), ice);
        }

        int priority = 0;
        ArrayList<ProductionOrder> poList = new ArrayList<ProductionOrder>();

        for (TreeMap<Integer, InConstructionEntry> pIdSorted : sorted.values()) {
            for (InConstructionEntry ice : pIdSorted.values()) {
                ice.getOrder().setPriority(priority);
                if (priority < 10) {
                    priority++;
                }
                poList.add(ice.getOrder());
            }
        }

        poDAO.updateAll(poList);
    }
}
