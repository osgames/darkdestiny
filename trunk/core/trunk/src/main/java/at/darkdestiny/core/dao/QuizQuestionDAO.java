/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.QuizQuestion;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class QuizQuestionDAO extends ReadWriteTable<QuizQuestion> implements GenericDAO  {


    public ArrayList<QuizQuestion> findByQuizEntryId(Integer id) {

        QuizQuestion qq = new QuizQuestion();
        qq.setQuizEntryId(id);

        return (ArrayList<QuizQuestion>)find(qq);
    }
}
