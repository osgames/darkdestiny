/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

/**
 *
 * @author Aion
 */
public class SetupProperties {

    public static final String DATABASE_DRIVERCLASS = "driverClass";
    public static final String DATABASE_URL = "url";
    public static final String DATABASE_PASS = "pass";
    public static final String DATABASE_USER = "user";
    public static final String DATABASE_NAME = "name";
    public static final String DATABASE_MYSQLIP = "mysqlIp";
    public static final String GAME_GAMEHOSTURL = "gamehostURL";
    public static final String GAME_WEBHOSTURL = "webserviceURL";
    public static final String GAME_SRCPATH_WEB = "srcPathWeb";
    public static final String GAME_SRCPATH_CLASSES = "srcPathClasses";
    public static final String GAME_SRCPATH_RESOURCES = "srcPathResources";
    public static final String GAME_LOGGING = "logging";
    public static final String GAME_CLASSFOLDER = "classFolder";
    public static final String GAME_TICKTIME = "tickTime";
    public static final String GAME_STARTURL = "startURL";
    public static final String GAME_UNIVERSE_HEIGHT = "height";
    public static final String GAME_UNIVERSE_WIDTH = "width";
    public static final String GAME_UPDATETICKONSTARTUP = "updateTickOnStartup";
    public static final String GAME_OPTIONS_ASSIGN_DELETED_EMPIRE_TO_AI = "asssignDeletedEmpireToAI";
    public static final String GAME_OPTIONS_DELETE_INACTIVE = "setDeleteInactive";
}
