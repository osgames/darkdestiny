/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin.module;

import at.darkdestiny.core.enumeration.EAttribute;

/**
 *
 * @author Stefan
 */
public class AttributeTreeEntry {
    private final int id;
    private final EAttribute attribute;
    private final double value;
    
    public AttributeTreeEntry(int id, EAttribute attribute, double value) {
        this.id = id;
        this.attribute = attribute;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public EAttribute getAttribute() {
        return attribute;
    }

    public double getValue() {
        return value;
    }
}
