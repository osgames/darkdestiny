/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai;

import at.darkdestiny.core.ai.message.AIMessage;
import at.darkdestiny.core.ai.message.ConstructMessage;
import at.darkdestiny.core.dao.AIMessageDAO;
import at.darkdestiny.core.dao.AIMessageParameterDAO;
import at.darkdestiny.core.enumeration.EAIMessageType;
import at.darkdestiny.core.model.AIMessageParameter;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class AIMessageHandler {

    private AIMessageDAO aimDAO = DAOFactory.get(AIMessageDAO.class);
    private AIMessageParameterDAO aimpDAO = DAOFactory.get(AIMessageParameterDAO.class);

    public void deleteMessage(int messageId) {
        at.darkdestiny.core.model.AIMessage msg = aimDAO.findById(messageId);

        AIMessageParameter aimp = new AIMessageParameter();
        aimp.setMessageId(messageId);
        ArrayList<AIMessageParameter> msgPars = aimpDAO.find(aimp);
        aimpDAO.removeAll(msgPars);
        aimDAO.remove(msg);
    }

    public ArrayList<AIMessage> getReceivedMessages(int goalId) {
        return null;
    }

    public ArrayList<AIMessage> getReceivedMessages(int goalId, int planetId) {
        ArrayList<AIMessage> messages = new ArrayList<AIMessage>();

        at.darkdestiny.core.model.AIMessage aim = new at.darkdestiny.core.model.AIMessage();
        aim.setReceiverId(goalId);
        aim.setPlanetId(planetId);

        ArrayList<at.darkdestiny.core.model.AIMessage> msgList = aimDAO.find(aim);
        for (at.darkdestiny.core.model.AIMessage msg : msgList) {
            AIMessage aiMsg;

            AIMessageParameter aimp = new AIMessageParameter();
            aimp.setMessageId(msg.getId());

            ArrayList<AIMessageParameter> aimpList = aimpDAO.find(aimp);
            AIMessagePayload payload = new AIMessagePayload();

            for (AIMessageParameter aimpTmp : aimpList) {
                payload.addParameter(aimpTmp.getParName(), aimpTmp.getParValue());
            }

            if (msg.getType() == EAIMessageType.CONSTRUCT) {
                aiMsg = new ConstructMessage(msg.getId(), msg.getSenderId(), msg.getReceiverId(), msg.getPlanetId(), payload);
            } else {
                throw new RuntimeException("BAM");
            }

            messages.add(aiMsg);
        }

        return messages;
    }

    public ArrayList<AIMessage> getSentMessages(int goalId) {
        return null;
    }

    public ArrayList<AIMessage> getSentMessages(int goalId, int planetId) {
        return null;
    }

    private boolean alreadySent(int goalId, int receiverGoalId, int planetId, AIMessagePayload amp) {
        at.darkdestiny.core.model.AIMessage aim = new at.darkdestiny.core.model.AIMessage();
        aim.setReceiverId(goalId);
        aim.setReceiverId(receiverGoalId);
        aim.setPlanetId(planetId);

        ArrayList<at.darkdestiny.core.model.AIMessage> aimList = aimDAO.find(aim);

        for (at.darkdestiny.core.model.AIMessage aiMsgTmp : aimList) {
            System.out.println("Check Message with ID " + aiMsgTmp.getId());

            AIMessageParameter aimp = new AIMessageParameter();
            aimp.setMessageId(aiMsgTmp.getId());

            ArrayList<AIMessageParameter> aimpList = aimpDAO.find(aimp);

            boolean found = true;

            HashMap<String, String> params = amp.getAll();
            for (Iterator<AIMessageParameter> aimpIt = aimpList.iterator(); aimpIt.hasNext();) {
                AIMessageParameter aimpTmp = aimpIt.next();

                if (!params.containsKey(aimpTmp.getParName())) {
                    found = false;
                }
                if (!aimpTmp.getParValue().equalsIgnoreCase(params.get(aimpTmp.getParName()))) {
                    found = false;
                }
            }

            if (found) {
                System.out.println("Message exists");
                return true;
            }
        }

        System.out.println("Message does not exist");
        return false;
    }

    public void sendMessage(int senderGoalId, int receiverGoalId, int planetId, AIMessagePayload amp) {
        if (alreadySent(senderGoalId, receiverGoalId, planetId, amp)) {
            AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "[" + senderGoalId + "/" + planetId + "] Ignore Message as already sent");
            return;
        }

        AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "Sending CONSTRUCT message from goal " + senderGoalId + " to goal " + receiverGoalId);

        at.darkdestiny.core.model.AIMessage aim = new at.darkdestiny.core.model.AIMessage();
        aim.setSenderId(senderGoalId);
        aim.setReceiverId(receiverGoalId);
        aim.setPlanetId(planetId);
        aim.setTime(System.currentTimeMillis());
        aim.setType(EAIMessageType.CONSTRUCT);

        aim = aimDAO.add(aim);

        for (Map.Entry<String, String> entry : amp.getAll().entrySet()) {
            AIMessageParameter aimp = new AIMessageParameter();
            aimp.setMessageId(aim.getId());
            aimp.setParName(entry.getKey());
            aimp.setParValue(entry.getValue());
            aimpDAO.add(aimp);
        }
    }

    public void removeMessage(int msgId) {
    }
}
