/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "constructionmodule")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ConstructionModule extends Model<ConstructionModule> {

    @FieldMappingAnnotation("constructionId")
    @IdFieldAnnotation
    private Integer constructionId;
    @FieldMappingAnnotation("moduleId")
    @IdFieldAnnotation
    private Integer moduleId;
    @FieldMappingAnnotation("count")
    private Integer count;

    public Integer getConstructionId() {
        return constructionId;
    }

    public void setConstructionId(Integer constructionId) {
        this.constructionId = constructionId;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
