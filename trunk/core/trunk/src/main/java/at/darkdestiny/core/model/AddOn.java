/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "addon")
public class AddOn extends Model<AddOn> {

    @FieldMappingAnnotation("name")
    @IdFieldAnnotation
    private String name;
    @FieldMappingAnnotation("version")
    @IdFieldAnnotation
    private String version;
    @FieldMappingAnnotation("packageName")
    private String packageName;
    @FieldMappingAnnotation("interfaceName")
    private String interfaceName;
    @FieldMappingAnnotation("installed")
    private Boolean installed;
    @FieldMappingAnnotation("active")
    private Boolean active;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the installed
     */
    public Boolean getInstalled() {
        return installed;
    }

    /**
     * @param installed the installed to set
     */
    public void setInstalled(Boolean installed) {
        this.installed = installed;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the interfaceName
     */
    public String getInterfaceName() {
        return interfaceName;
    }

    /**
     * @param interfaceName the interfaceName to set
     */
    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }
}
