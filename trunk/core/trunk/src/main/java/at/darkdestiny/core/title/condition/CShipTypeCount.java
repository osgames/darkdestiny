package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.service.Service;

public class CShipTypeCount extends AbstractCondition {

    private ParameterEntry chassisId;
    private ParameterEntry count;
    private ParameterEntry comparator;

    public CShipTypeCount(ParameterEntry chassisId, ParameterEntry count, ParameterEntry comparator) {
        this.chassisId = chassisId;
        this.count = count;
        this.comparator = comparator;
    }

    public boolean checkCondition(int userId, int conditionToTitleId) {
        int actCount = 0;
        for (PlayerFleet pf : Service.playerFleetDAO.findByUserId(userId)) {
            for (ShipFleet sf : Service.shipFleetDAO.findByFleetId(pf.getId())) {
                if (sf.getDesignId().equals(Integer.parseInt(chassisId.getParamValue().getValue()))) {
                    actCount += sf.getCount();
                }
            }
        }
        return compare(count.getParamValue().getValue(), actCount, count.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));


    }
}
