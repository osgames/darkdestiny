/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.Buildable;

/**
 *
 * @author Stefan
 */
public class RepairOrder {
    protected final Buildable unit;
    protected final int planetId;
    protected final int userId;
    
    protected RepairOrder(Buildable unit, int planetId, int userId) {
        this.unit = unit;
        this.planetId = planetId;
        this.userId = userId;
    }
    
    public Buildable getUnit() {
        return unit;
    }

    public int getPlanetId() {
        return planetId;
    }

    public int getUserId() {
        return userId;
    }
}
