/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ConstructionUpgrade;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ConstructionUpgradeDAO extends ReadOnlyTable<ConstructionUpgrade> implements GenericDAO {
    public ConstructionUpgrade findBySourceAndTarget(int sourceConstructionId, int targetConstructionId) {
        ConstructionUpgrade cu = new ConstructionUpgrade();
        cu.setBaseConstruction(sourceConstructionId);
        cu.setTargetConstruction(targetConstructionId);

        ArrayList<ConstructionUpgrade> result = find(cu);
        if (result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }
    public ArrayList<ConstructionUpgrade> findBySourceId(int sourceConstructionId) {
        ConstructionUpgrade cu = new ConstructionUpgrade();
        cu.setBaseConstruction(sourceConstructionId);

        return find(cu);
    }
}
