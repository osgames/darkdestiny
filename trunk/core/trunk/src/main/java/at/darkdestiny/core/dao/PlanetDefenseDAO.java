/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class PlanetDefenseDAO extends ReadWriteTable<PlanetDefense> implements GenericDAO {
    public PlanetDefense get(int systemId, int planetId, int userId, EDefenseType edt, int unitId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setSystemId(systemId);
        pd.setPlanetId(planetId);
        pd.setUserId(userId);
        pd.setUnitId(unitId);
        pd.setType(edt);
        pd.setFleetId(0);
        return (PlanetDefense)get(pd);
    }

    public PlanetDefense findByFleetAndDesign(int fleetId, int designId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setFleetId(fleetId);
        pd.setUnitId(designId);

        ArrayList<PlanetDefense> pdResult = find(pd);
        if (pdResult.isEmpty()) {
            return null;
        } else {
            return pdResult.get(0);
        }
    }

    public ArrayList<PlanetDefense> findAttachedToFleet(int fleetId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setFleetId(fleetId);
        return find(pd);
    }

    public ArrayList<PlanetDefense> findByUserAndType(int userId, EDefenseType edt) {
        PlanetDefense pd = new PlanetDefense();
        pd.setUserId(userId);
        pd.setType(edt);
        return find(pd);
    }

    public ArrayList<PlanetDefense> findByUserAndShipDesign(int userId, int designId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setUserId(userId);
        pd.setType(EDefenseType.SPACESTATION);
        pd.setUnitId(designId);
        return find(pd);
    }

    public ArrayList<PlanetDefense> findByPlanetId(int planetId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(planetId);
        return find(pd);
    }

    public ArrayList<PlanetDefense> findBySystemId(int systemId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(0);
        pd.setSystemId(systemId);
        return find(pd);
    }

    public ArrayList<PlanetDefense> findByPlanetAndUserId(int planetId, int userId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setUserId(userId);
        pd.setPlanetId(planetId);
        return find(pd);
    }

    public ArrayList<PlanetDefense> findBySystemAndUserId(int systemId, int userId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setUserId(userId);
        pd.setPlanetId(0);
        pd.setSystemId(systemId);
        return find(pd);
    }

    public PlanetDefense findStationBySystemPlanetAndDesignId(int systemId, int planetId, int designId) {
        PlanetDefense pd = new PlanetDefense();
        pd.setType(EDefenseType.SPACESTATION);
        pd.setUnitId(designId);
        
        if (planetId != 0) {
            pd.setPlanetId(planetId);
        } else if (systemId != 0) {
            pd.setSystemId(systemId);
        }

        ArrayList<PlanetDefense> result = find(pd);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public PlanetDefense findBy(int planetId, int unitId, EDefenseType type) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(planetId);
        pd.setUnitId(unitId);
        pd.setType(type);
        ArrayList<PlanetDefense> result = find(pd);
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }
    public PlanetDefense findBy(int planetId, int unitId, int userId, EDefenseType type) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(planetId);
        pd.setUnitId(unitId);
        pd.setType(type);
        pd.setUserId(userId);
        ArrayList<PlanetDefense> result = find(pd);
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public ArrayList<PlanetDefense> findByPlanet(int planetId, EDefenseType type) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(planetId);
        pd.setType(type);
        return find(pd);
    }
    public ArrayList<PlanetDefense> findBySystem(int systemId, EDefenseType type) {
        PlanetDefense pd = new PlanetDefense();
        pd.setPlanetId(0);
        pd.setType(type);
        pd.setSystemId(systemId);
        return find(pd);
    }
}
