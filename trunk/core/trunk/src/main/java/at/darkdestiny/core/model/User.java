/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

 import at.darkdestiny.core.GameConfig;import at.darkdestiny.core.ML;
import at.darkdestiny.core.ai.ThreadSharedObjects;
import at.darkdestiny.core.enumeration.EUserGender;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.util.Locale;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "user")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class User extends Model<User> {

    private static final Logger log = LoggerFactory.getLogger(User.class);

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = false, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("userName")
    @IndexAnnotation(indexName = "userName")
    @ColumnProperties(length = 15)
    @StringType("text")
    private String userName;
    @FieldMappingAnnotation("userType")
    @IndexAnnotation(indexName = "userType")
    @DefaultValue("HUMAN")
    private EUserType userType;
    @FieldMappingAnnotation("password")
    @IndexAnnotation(indexName = "userName")
    @ColumnProperties(length = 25)
    @StringType("text")
    private String password;
    @FieldMappingAnnotation("gameName")
    @IndexAnnotation(indexName = "gameName")
    @ColumnProperties(length = 15)
    @StringType("text")
    private String gameName;
    @FieldMappingAnnotation("locale")
    private String locale;
    @FieldMappingAnnotation("joinDate")
    private Long joinDate;
    @FieldMappingAnnotation("lastUpdate")
    @DefaultValue("0")
    private Integer lastUpdate;
    @FieldMappingAnnotation("activationCode")
    private String activationCode;
    @FieldMappingAnnotation("active")
    private Boolean active;
    @FieldMappingAnnotation("admin")
    private Boolean admin;
    @FieldMappingAnnotation("betaAdmin")
    private Boolean betaAdmin;
    @FieldMappingAnnotation("guest")
    @DefaultValue("0")
    private Boolean guest;
    @FieldMappingAnnotation("locked")
    private Integer locked;
    @ColumnProperties(length = 150)
    @StringType("text")
    @FieldMappingAnnotation("ip")
    private String ip;
    @FieldMappingAnnotation("deleteDate")
    private Integer deleteDate;
    @FieldMappingAnnotation("trial")
    private Boolean trial;
    @DefaultValue("0")
    @FieldMappingAnnotation("systemAssigned")
    private Boolean systemAssigned;
    @DefaultValue("0")
    @FieldMappingAnnotation("userConfigured")
    private Boolean userConfigured;
    @FieldMappingAnnotation("multi")
    private Boolean multi;
    @FieldMappingAnnotation("email")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String email;
    @FieldMappingAnnotation("gender")
    private EUserGender gender;
    @FieldMappingAnnotation("trusted")
    @DefaultValue("0")
    private Boolean trusted;
    @FieldMappingAnnotation("acceptedUserAgreement")
    @DefaultValue("0")
    private Boolean acceptedUserAgreement;
    @FieldMappingAnnotation("registrationToken")
    @DefaultValue("")
    private String registrationToken;
    @FieldMappingAnnotation("portalPassword")
    @DefaultValue("")
    private String portalPassword;
    @DefaultValue("")
    private String passwordSalt;

    /**
     * Use getId()
     *
     * @return
     * @deprecated
     */
    @Deprecated
    public Integer getUserId() {
        return id;
    }

    public Integer getId() {
        return id;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer id) {
        this.id = id;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the gameName
     */
    public String getGameName() {
        if (this.userType.AI.equals(EUserType.AI)) {
        }
        return gameName;

    }

    /**
     * @return the gameName
     */
    public String getGameName(int userId) {
        if (this.userType.AI.equals(EUserType.AI)) {
            try {
                Locale l = (Locale) ThreadSharedObjects.getThreadSharedObject(ThreadSharedObjects.KEY_LOCALE);
                return ML.getMLStr(gameName, l);
            } catch (Exception e) {
                log.warn("Could not translate gamename for user : " + userId);
            }
        }
        return gameName;

    }

    /**
     * @param gameName the gameName to set
     */
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    /**
     * @return the joinDate
     */
    public Long getJoinDate() {
        return joinDate;
    }

    /**
     * @param joinDate the joinDate to set
     */
    public void setJoinDate(Long joinDate) {
        this.joinDate = joinDate;
    }

    /**
     * @return the lastUpdate
     */
    public Integer getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param lastUpdate the lastUpdate to set
     */
    public void setLastUpdate(Integer lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * @return the activationCode
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * @param activationCode the activationCode to set
     */
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the admin
     */
    public Boolean getAdmin() {
        return admin;
    }

    /**
     * @param admin the admin to set
     */
    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    /**
     * @return the betaAdmin
     */
    public Boolean getBetaAdmin() {
        return betaAdmin;
    }

    /**
     * @param betaAdmin the betaAdmin to set
     */
    public void setBetaAdmin(Boolean betaAdmin) {
        this.betaAdmin = betaAdmin;
    }

    /**
     * @return the locked
     */
    public Boolean isGuest() {
        return guest;
    }

    /**
     * @param locked the locked to set
     */
    public void setGuest(Boolean guest) {
        this.guest = guest;
    }

    /**
     * @return the locked
     */
    public Integer getLocked() {
        return locked;
    }

    /**
     * @param locked the locked to set
     */
    public void setLocked(Integer locked) {
        this.locked = locked;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the deleteDate
     */
    public Integer getDeleteDate() {
        return deleteDate;
    }

    /**
     * @param deleteDate the deleteDate to set
     */
    public void setDeleteDate(Integer deleteDate) {
        this.deleteDate = deleteDate;
    }

    /**
     * @return the trial
     */
    public Boolean getTrial() {
        return trial;
    }

    /**
     * @param trial the trial to set
     */
    public void setTrial(Boolean trial) {
        this.trial = trial;
    }

    /**
     * @return the systemAssigned
     */
    public Boolean getSystemAssigned() {
        return systemAssigned;
    }

    /**
     * @param systemAssigned the systemAssigned to set
     */
    public void setSystemAssigned(Boolean systemAssigned) {
        this.systemAssigned = systemAssigned;
    }

    /**
     * @return the multi
     */
    public Boolean getMulti() {
        return multi;
    }

    /**
     * @param multi the multi to set
     */
    public void setMulti(Boolean multi) {
        this.multi = multi;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the locale
     */
    public String getLocale() {
        return locale;
    }

    /**
     * @return the locale
     */
    public String getLocaleImage() {
        if (locale.startsWith("de")) {
            return GameConfig.getInstance().picPath() + "pic/icons/german.png";
        } else {
            return GameConfig.getInstance().picPath() + "pic/icons/english.png";
        }
    }

    /**
     * @param locale the locale to set
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * @return the gender
     */
    public EUserGender getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(EUserGender gender) {
        this.gender = gender;
    }

    /**
     * @return the trusted
     */
    public Boolean getTrusted() {
        return trusted;
    }

    /**
     * @param trusted the trusted to set
     */
    public void setTrusted(Boolean trusted) {
        this.trusted = trusted;
    }

    /**
     * @return the userConfigured
     */
    public Boolean getUserConfigured() {
        return userConfigured;
    }

    /**
     * @param userConfigured the userConfigured to set
     */
    public void setUserConfigured(Boolean userConfigured) {
        this.userConfigured = userConfigured;
    }

    /**
     * @return the acceptedUserAgreement
     */
    public Boolean getAcceptedUserAgreement() {
        return acceptedUserAgreement;
    }

    /**
     * @param acceptedUserAgreement the acceptedUserAgreement to set
     */
    public void setAcceptedUserAgreement(Boolean acceptedUserAgreement) {
        this.acceptedUserAgreement = acceptedUserAgreement;
    }

    /**
     * @return the userType
     */
    public EUserType getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(EUserType userType) {
        this.userType = userType;
    }

    public boolean isSystemUser() {
        return userType == EUserType.SYSTEM;
    }

    /**
     * @return the registrationToken
     */
    public String getRegistrationToken() {
        return registrationToken;
    }

    /**
     * @param registrationToken the registrationToken to set
     */
    public void setRegistrationToken(String registrationToken) {
        this.registrationToken = registrationToken;
    }

    /**
     * @return the portalPassword
     */
    public String getPortalPassword() {
        return portalPassword;
    }

    /**
     * @param portalPassword the portalPassword to set
     */
    public void setPortalPassword(String portalPassword) {
        this.portalPassword = portalPassword;
    }

    /**
     * @return the passwordSalt
     */
    public String getPasswordSalt() {
        return passwordSalt;
    }

    /**
     * @param passwordSalt the passwordSalt to set
     */
    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }
}
