/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat.combatcontroller;

import at.darkdestiny.core.spacecombat.ISpaceCombat;
import at.darkdestiny.util.DebugBuffer;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author Stefan
 */
public class CombatControllerFactory {
    public static ISpaceCombat getCombatController(String className, int combatLocation, int combatLocationId) {
        try {
            Class controllerClass = Class.forName(className);
            Constructor c = controllerClass.getConstructor(int.class,int.class);
            Object newInstance = c.newInstance(combatLocation,combatLocationId);

            return (ISpaceCombat)newInstance;
        } catch (ClassNotFoundException cnfe) {
            DebugBuffer.error("CombatControllerFactory: Class " + className + " was not found! ("+cnfe.getMessage()+")");
            return null;
        } catch (InstantiationException ie) {
            DebugBuffer.error("CombatControllerFactory: Class " + className + " could not be instantiated! ("+ie.getMessage()+")");
            return null;
        } catch (IllegalAccessException iae) {
            DebugBuffer.error("CombatControllerFactory: Class " + className + " could not be accessed! ("+iae.getMessage()+")");
            return null;
        } catch (NoSuchMethodException nsme) {
            DebugBuffer.error("CombatControllerFactory: Contructor(int,int) was not found! ("+nsme.getMessage()+")");
            return null;
        } catch (InvocationTargetException ite) {
            DebugBuffer.error("CombatControllerFactory: Contructor(int,int) cound not be invocated with values ("+combatLocation+","+combatLocationId+")! ("+ite.getMessage()+")");
            return null;
        }
    }
}
