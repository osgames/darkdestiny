/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.requestbuffer;

/**
 *
 * @author Stefan
 */
public class ConstructionImproveBuffer extends ParameterBuffer {
    public static final String CONSTRUCTION_ID = "consId";
    public static final String PLANET_ID = "planetId";

    public ConstructionImproveBuffer(int userId) {
        super(userId);
    }

    public int getConstructionId() {
        return (Integer)getParameter(CONSTRUCTION_ID);
    }

    public int getPlanetId() {
        return (Integer)getParameter(PLANET_ID);
    }
}
