/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class UserShareEntry {
    private final int id;
    private final EntryType type;
    private ArrayList<Integer> users;

    public UserShareEntry(int id) {
        this.id = id;
        this.type = EntryType.SINGLE_ENTRY;
        
        ArrayList<Integer> users = new ArrayList<Integer>();
        users.add(id);
        
        this.users = users;
    }    
    
    public UserShareEntry(int id, ArrayList<Integer> users) {
        this.id = id;
        this.type = EntryType.ALLIANCE_ENTRY;
        this.users = users;
    }
    
    public int getId() {
        return id;
    }

    public EntryType getType() {
        return type;
    }

    public ArrayList<Integer> getUsers() {
        return users;
    }
    public enum EntryType {
        SINGLE_ENTRY, ALLIANCE_ENTRY;
    }
        
}
