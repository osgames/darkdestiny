/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.notification;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Aion
 */
public class OutOfRessourceNotification extends Notification {

    int ressourceId;
    int planetId;

    public static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    public static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    public OutOfRessourceNotification(int ressourceId, int planetId, int userId) {

        super(userId);
        this.ressourceId = ressourceId;
        this.planetId = planetId;
            generateMessage();
            persistMessage();
    }

    @Override
    public void generateMessage() {

        String msg = ML.getMLStr("notification_outofressource_msg", userId);
        msg = msg.replace("%RESNAME%", ML.getMLStr(rDAO.findRessourceById(ressourceId).getName(), userId));
        msg = msg.replace("%PLANETID%", String.valueOf(planetId));
        msg = msg.replace("%PLANETNAME%", ppDAO.findByPlanetId(planetId).getName());

        gm.setMsg(msg);
        gm.setMessageContentType(EMessageContentType.INFO);

        String topic = ML.getMLStr("notification_outofressource_topic", userId);
        topic = topic.replace("%RESNAME%", ML.getMLStr(rDAO.findRessourceById(ressourceId).getName(), userId));
        gm.setTopic(topic);
    }
}
