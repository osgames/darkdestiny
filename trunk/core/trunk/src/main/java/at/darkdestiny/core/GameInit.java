/*
 * GameInit.java
 *
 * Created on 29. April 2007, 19:57
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.dao.GameDataDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.ResearchDAO;
import at.darkdestiny.core.databuffer.*;
import at.darkdestiny.core.service.ResearchService;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class GameInit {

    private static final Logger log = LoggerFactory.getLogger(GameInit.class);

    private static ResearchDAO researchDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    private static GameDataDAO gameDataDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
    private static PlayerResearchDAO playerResearchDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private static boolean init = false;

    /**
     * Creates a new instance of GameInit
     */
    private GameInit() {
        // This constructor of the utility class should not be called!
    }

    public static synchronized void initGame() {
        if (init) {
            return;
        }

        GameUtilities.setLastRestart(System.currentTimeMillis());
        try {

            log.debug("Resetting and updating Researchbonus");
            ResearchService.reEvaluateResearchBonus();
            /*
            ArrayList<Research> researchList = researchDAO.findAll();

            for (Research r : (ArrayList<Research>) researchDAO.findAll()) {
                int size = playerResearchDAO.findByResearchId(r.getId()).size();
                double bonus = (double) size * 0.01d;
                if (bonus > 5) {
                    bonus = 5d;
                }
                // TODO Remove in live version
                // bonus = 500d;
                // r.setBonus(bonus);
            }

            researchDAO.updateAll(researchList);
            */            

            try {
                GameConstants.UNIVERSE_HEIGHT = gameDataDAO.findById(1).getHeight();
                GameConstants.UNIVERSE_WIDTH = gameDataDAO.findById(1).getWidth();

            } catch (Exception e) {
                log.warn("Error while setting Height,Width from GameData to GameConstants");
            }

            try {
                ML.generateJS(null, null);
            } catch (Exception e) {
                log.warn("Error creating ML-Javscript file");
            }

            // StartingArea.initStaticData();

            // log.debug("Initializing RessourceCostBuffer");
            //     RessourceCostBuffer.initialize();
            //  log.debug("Initializing TechRelationBuffer");
            //     TechRelationBuffer.initialize();
            // log.debug("Initializing ConstructionTable");
            //    ConstructionTable.initialize();
            // log.debug("Initializing ResearchTable");
            //    ResearchTable.initialize();
            log.debug("Initializing HeaderBuffer");
            HeaderBuffer.initialize();
            log.debug("Initializing RangeSizePenalty");
            RangeSizePenalty.initialize();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Initialization failed: ", e);
        }

        init = true;

        /*
         try {
         // ServerListener sl = new ServerListener();
         // sl.start();
         } catch (Exception e) {

         }
         */
    }
}
