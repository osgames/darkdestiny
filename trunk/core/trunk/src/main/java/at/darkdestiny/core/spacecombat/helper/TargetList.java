/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.spacecombat.BattleShipNew;
import at.darkdestiny.core.spacecombat.CombatContext;
import at.darkdestiny.core.spacecombat.ISpaceCombat;

/**
 *
 * @author Stefan
 */
public class TargetList {

    private static final Logger log = LoggerFactory.getLogger(TargetList.class);

    private BattleShipNew[] targetShip;
    private int[] targetValue;
    private float[] targetValuePerCount;
    private int totalTargetValue;

    public BattleShipNew[] getTargetShip() {
        return targetShip;
    }

    public void setTargetShip(BattleShipNew[] targetShip) {
        this.targetShip = targetShip;
    }

    public int[] getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(int[] targetValue) {
        this.targetValue = targetValue;
    }

    public int getTotalTargetValue() {
        return totalTargetValue;
    }

    public void setTotalTargetValue(int totalTargetValue) {
        this.totalTargetValue = totalTargetValue;
    }

    public float[] getTargetValuePerCount() {
        return targetValuePerCount;
    }

    public void setTargetValuePerCount(float[] targetValuePerCount) {
        this.targetValuePerCount = targetValuePerCount;
    }

    public void buildTotalTargetValue() {
        totalTargetValue = 0;

        log.debug("Build TotalTargetValue");

        for (int i = 0; i < targetShip.length; i++) {
            log.debug("Looping ship " + targetShip[i].getName());

            if (((BattleShipNew) targetShip[i]) == null) {
                continue;
            }
            BattleShipNew tmpShp = (BattleShipNew) targetShip[i];

            ISpaceCombat isc = CombatContext.getCombatController();
            if (isc.skip(tmpShp)) {
                continue;
            }

            log.debug("Calculating target value for this ship TVpC: " + targetValuePerCount[i] + " * Count: " + tmpShp.getCount());;
            targetValue[i] = (int) (targetValuePerCount[i] * (float) tmpShp.getCount());
            if ((tmpShp.getCount() > 0) && (targetValue[i] == 0)) {
                 log.debug("Assign new targetValue: 1 (Something must have gone missing)");
                targetValue[i] = 1;
            }
            totalTargetValue += targetValue[i];
        }

        log.debug("Total Targetvalue = " + totalTargetValue);
    }
}
