/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.enumeration.EDamagedShipRefType;
import at.darkdestiny.core.model.DamagedShips;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class DamagedShipsDAO extends ReadWriteTable<DamagedShips> implements GenericDAO {
    public ArrayList<DamagedShips> findById(Integer shipFleetId) {
        DamagedShips ds = new DamagedShips();
        ds.setRefTable(EDamagedShipRefType.SHIPFLEET);
        ds.setShipFleetId(shipFleetId);
        return find(ds);
    }

    public ArrayList<DamagedShips> findByHangarId(Integer hangarRelId) {
        DamagedShips ds = new DamagedShips();
        ds.setRefTable(EDamagedShipRefType.HANGARRELATION);
        ds.setShipFleetId(hangarRelId);
        return find(ds);
    }

    public DamagedShips getByIdAndDamage(Integer shipFleetId, EDamageLevel damageLevel) {
        DamagedShips ds = new DamagedShips();
        ds.setShipFleetId(shipFleetId);
        ds.setRefTable(EDamagedShipRefType.SHIPFLEET);
        ds.setDamageLevel(damageLevel);

        ArrayList<DamagedShips> dsList = find(ds);
        if (dsList.isEmpty()) {
            return null;
        } else {
            return dsList.get(0);
        }
    }

    public DamagedShips getByIdAndDamageHangar(Integer hangarRelId, EDamageLevel damageLevel) {
        DamagedShips ds = new DamagedShips();
        ds.setShipFleetId(hangarRelId);
        ds.setRefTable(EDamagedShipRefType.HANGARRELATION);
        ds.setDamageLevel(damageLevel);

        ArrayList<DamagedShips> dsList = find(ds);
        if (dsList.isEmpty()) {
            return null;
        } else {
            return dsList.get(0);
        }
    }
}
