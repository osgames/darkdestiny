/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.interfaces;

import java.awt.image.BufferedImage;

/**
 *
 * @author Stefan
 */
public interface ITileGenerator {
    public BufferedImage createBufferedImage(int userId, int zoom, int x, int y);
}
