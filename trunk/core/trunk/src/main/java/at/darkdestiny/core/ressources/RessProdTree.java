/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ressources;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class RessProdTree {
    private HashMap<Integer,RessProdNode> nodes = new HashMap<Integer,RessProdNode>();
    private ArrayList<RessProdNode> rootNodes = new ArrayList<RessProdNode>();
    
    public RessProdNode getNode(int ressId) {
        return nodes.get(ressId);
    }
    
    public void setNode(RessProdNode rpn) {
        nodes.put(rpn.ressource, rpn);
    }
    
    public void addRootNode(RessProdNode rpn) {
        rootNodes.add(rpn);        
    }
    
    public ArrayList<RessProdNode> getRootNodes() {
        return rootNodes;
    }
    
    public HashMap<Integer,RessProdNode> getAllNodes() {
        return nodes;
    }
}
