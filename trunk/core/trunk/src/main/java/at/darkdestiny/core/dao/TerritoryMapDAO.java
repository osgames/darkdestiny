/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.ETerritoryMapType;
import at.darkdestiny.core.model.TerritoryMap;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TerritoryMapDAO extends ReadWriteTable<TerritoryMap> implements GenericDAO {

    public ArrayList<TerritoryMap> findBy(int refId, ETerritoryMapType type) {
        TerritoryMap tmSearch = new TerritoryMap();
        tmSearch.setRefId(refId);
        tmSearch.setType(type);
        return (ArrayList<TerritoryMap>) find(tmSearch);
    }

    public TerritoryMap findById(int territoryId) {
        TerritoryMap tmSearch = new TerritoryMap();
        tmSearch.setId(territoryId);
        return (TerritoryMap)get(tmSearch);
    }

    public ArrayList<TerritoryMap> findByUserId(int userId) {
        TerritoryMap tmSearch = new TerritoryMap();
        tmSearch.setRefId(userId);
        return find(tmSearch);
    }
}
