package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;


public class CTotalPopulationCount extends AbstractCondition {

	private ParameterEntry populationCountValue;
	private ParameterEntry comparator;
private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

	public CTotalPopulationCount(ParameterEntry populationCountValue, ParameterEntry comparator){
		this.populationCountValue = populationCountValue;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
            long populationCount = 0;
            for(PlayerPlanet pp : ppDAO.findByUserId(userId)){
                populationCount += pp.getPopulation();
            }
        return compare(populationCountValue.getParamValue().getValue(), populationCount, populationCountValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

	}
}
