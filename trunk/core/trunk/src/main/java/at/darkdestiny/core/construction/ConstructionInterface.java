/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.construction;

import at.darkdestiny.core.enumeration.ERestrictionReason;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.ConstructionCheckResult;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public interface ConstructionInterface {        
    public ConstructionCheckResult onConstructionCheck(PlayerPlanet pp) throws Exception;
    public void onConstruction(PlayerPlanet pp) throws Exception;
    public void onFinishing(PlayerPlanet pp) throws Exception;
    public void onImproving(PlayerPlanet pp) throws Exception;
    public void onImproved(PlayerPlanet pp) throws Exception;
    public void onDeconstruction(PlayerPlanet pp) throws Exception;
    public void onDestruction(PlayerPlanet pp) throws Exception;
    
    public boolean isDeactivated(PlayerPlanet pp) throws Exception;
    
    public HashMap<String,ERestrictionReason> getReasons();
}
