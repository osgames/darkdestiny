/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.painting.map.AreaMapContainer;
import at.darkdestiny.core.painting.map.Fleet;
import at.darkdestiny.core.painting.map.SMPlanet;
import at.darkdestiny.core.painting.map.StarMapObject;
import at.darkdestiny.core.painting.map.StarSystem;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class GetStarMapData extends HttpServlet {
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain");        
        ServletOutputStream out = response.getOutputStream();

        HttpSession session = request.getSession();

        try {
            int userId = 0;

            userId = Integer.parseInt((String) session.getAttribute("userId"));
            String id = request.getParameter("id");
            int x = 0;
            int y = 0;
            int zoom = 11;
            
            if (id == null) {
                try {
                    x = Integer.parseInt(request.getParameter("x"));
                    y = Integer.parseInt(request.getParameter("y"));                        
                } catch (NumberFormatException nfe) {
                    DebugBuffer.warning("GetStarMapData: Id was null and no x or y provided");
                    out.close();
                    return;
                }
                
                try {                        
                    zoom = Integer.parseInt(request.getParameter("z")); 
                } catch (NumberFormatException nfe) {
                    
                }
            }

            System.out.println("USERID: " + userId + " X: " + x + " Y: " + y);

            StringBuilder resp; 
            
            if (id != null) {
                System.out.println("Request Detail window ["+userId+"/"+id+"]");
                resp = getDetail(userId,id);
            } else {
                System.out.println("Request Complete window ["+userId+"/"+x+":"+y+"]");
                resp = getCompleteWindow(userId, x, y, zoom);
            }
            
            out.println(resp.toString());
            System.out.println("RESPONSE: " + resp.toString());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while generating area map: ", e);
        } finally {
            out.close();
        }
    }

    private StringBuilder getDetail(int userId, String id) {
        StringBuilder sb = new StringBuilder();
        StarMapObject smo = AreaMapContainer.getObjectById(userId, id);
        
        // System.out.println("SMO is " + smo + " for id " + id);
        
        if (smo instanceof StarSystem) {
            StarSystem ss = (StarSystem)smo;
            
            sb.append("<FONT onclick=\"selectTarget(false,"+ss.getPayload().getId()+")\" size=+1><B><U>System "+ss.getPayload().getName()+"</U></B></FONT><BR><BR>");
            sb.append("<TABLE>");
                                                
            // Sort planets by id (which is also orbitlevel conform)
            TreeMap<Integer,SMPlanet> sortedPlanets = Maps.newTreeMap();
            for (SMPlanet smp : ss.getPayload().getPlanets()) {  
                sortedPlanets.put(smp.getId(), smp);
            }
            
            for (SMPlanet smp : sortedPlanets.values()) {                                            
                // Planet Image
                String planetClassTR = "smdetailplanetdefault";
                if (smp.getDiplomacyId() == 1) {
                    planetClassTR = "smdetailplanetdefaultown";
                } else if ((smp.getDiplomacyId() == 10) || (smp.getDiplomacyId() == 11)) {
                    planetClassTR = "smdetailplanetallied";
                } else if (smp.getDiplomacyId() == 12) {
                    planetClassTR = "smdetailplanetnap";
                } else if ((smp.getDiplomacyId() == 13) || (smp.getDiplomacyId() == 14) || (smp.getDiplomacyId() == 15)) {
                    planetClassTR = "smdetailplanetneutral";
                } else if ((smp.getDiplomacyId() == 16) || (smp.getDiplomacyId() == 17)) {
                    planetClassTR = "smdetailplanetenemy";
                }            
                
                sb.append("<TR class=\"" + planetClassTR + "\" onclick=\"selectTarget(true,"+smp.getId()+")\"><TD valign=\"middle\" align=\"center\">");
                Planet p = pDAO.findById(smp.getId());   
                sb.append("<IMG src=\"pic/"+p.getPlanetPic()+"\" />");
                sb.append("</TD>");
                
                // Planet Data Table
                sb.append("<TD valign=\"middle\"><TABLE>");                                
                sb.append("<TR><TD style=\"font-size:14px;\">" + smp.getName() + "</TD></TR>");  // &Oslash;  
                if (smp.getOwner() != null) {
                    sb.append("<TR><TD style=\"font-size:12px;\">Besitzer: "+smp.getOwner()+"</TD></TR>");
                }                
                sb.append("<TR><TD style=\"font-size:12px;\">Typ: " + smp.getType() + " / &Oslash;: " + FormatUtilities.getFormattedNumber(smp.getDiameter()) + " km</TD></TR>");
                sb.append("</TABLE>");
                sb.append("</TD></TR>");
            }

            sb.append("</TABLE>");
        } else {
            Fleet f = (Fleet)smo;
            
            sb.append("<FONT class=\"smdetailfleetheader\"><B><U>Flotte "+f.getPayload().getName()+"</U></B></FONT><BR><BR>");
            if (f.getPayload().getTimeToTarget() > 0) {
                sb.append("<P class=\"smdetailfleetcoord\">Ziel: "+f.getPayload().getTargetSystemId()+":"+f.getPayload().getTargetPlanetId()+" [ETA: "+f.getPayload().getTimeToTarget()+"]</P>");
            } else {
                if (((f.getPayload().getStatus() == 0) || (f.getPayload().getStatus() == 1)) && !f.getPayload().isMoving()) {
                    sb.append("<P><IMG class=\"smdetailfleetaction\" onclick=\"startSendFleet('"+f.getPayload().getId()+"')\" src=\"pic/starmapJS/pic/starmap/movefleet.gif\" /></P>");
                }
                sb.append("<P class=\"smdetailfleetcoord\">Standort: "+f.getPayload().getTargetSystemId()+":"+f.getPayload().getTargetPlanetId()+"</P>");
            }
            sb.append("<P class=\"smdetailfleetowner\">Besitzer: "+f.getOwner()+"</P>");
            sb.append("<TABLE>");
            
            for (ShipData sd : f.getPayload().getShips()) {
                sb.append("<TR><TD>");
                sb.append("<TABLE><TR>");
                sb.append("<TD valign=\"middle\" align=\"center\"><IMG class=\"smdetailfleetchassisimg\" src=\"pic/starmap/"+getChassisImage(sd.getChassisSize())+"\"/></TD>");
                sb.append("<TD valign=\"middle\" class=\"smdetailfleetdesigninfo\">"+FormatUtilities.getFormattedNumber(sd.getCount())+"x "+sd.getDesignName()+"</TD>");
                // sb.append("<TR><TD style=\"font-size:14px;\">" + sd.getDesignName() + "</TD></TR>");  // &Oslash;             
                // sb.append("<TR><TD style=\"font-size:12px;\">Chassis: " + sd.getChassisSize() + " / Anzahl: " + FormatUtilities.getFormattedNumber(sd.getCount()) + " km</TD></TR>");
                sb.append("</TR></TABLE>");
                sb.append("</TD></TR>");
            }
            
            sb.append("</TABLE>");
        }
        
        return sb;
    }
    
    private StringBuilder getCompleteWindow(int userId, int x, int y, int zoom) {
        StringBuilder sb = new StringBuilder();

        ArrayList<StarMapObject> objects = AreaMapContainer.getObjectsAround(userId, x, y, 15 - zoom);
        System.out.println("Count of objects loaded around " + x + ":" + y + " => " + objects.size());
        
        // DRAW HTML  onclick=\"selectTarget(true,"+smp.getId()+")\"
        // Men� DIV
        sb.append("<TABLE><TR><TD width=\"200\" valign=\"top\">");
        sb.append("<DIV id=\"smdetaillist\" class=\"smmainlist\">");
        sb.append("<TABLE width=\"100%\" style=\"font-size:12px;\">");
        for (StarMapObject smo : objects) {
            if (smo instanceof Fleet) {
                Fleet f = (Fleet) smo;
                sb.append("<TR valign=\"middle\"><TD>");
                sb.append("<TABLE><TR>");
                sb.append("<TD class=\"smainiconcell\"><IMG class=\"smmainlisticon\" src=\"pic/starmapJS/pic/ship_small.png\" /></TD>");
                sb.append("<TD class=\"smmainfleet\" onclick=\"showDetails('"+AreaMapContainer.FLEET_OBJECT_PREFIX+"_"+f.getId()+"')\">" + f.getName() + "</TD>");
                sb.append("<TD class=\"smmainownercell\">");
                
                if ((f.getPayload().getStatus() == 0) || (f.getPayload().getStatus() == 1)) {
                    sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/green.png\" />");
                }

                if ((f.getPayload().getStatus() == 10) || (f.getPayload().getStatus() == 11) || (f.getPayload().getStatus() == 2)) {
                    sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/blue.png\" />");
                }

                if (f.getPayload().getStatus() == 12) {
                    sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/lightblue.png\" />");
                }

                if ((f.getPayload().getStatus() == 13) || (f.getPayload().getStatus() == 14) || (f.getPayload().getStatus() == 15)) {
                    sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/yellow.png\" />");
                }

                if ((f.getPayload().getStatus() == 16) || (f.getPayload().getStatus() == 17)) {
                    sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/red.png\" />");
                }                
                
                sb.append("</TD>");
                sb.append("</TR>");
                sb.append("</TD></TR></TABLE>");
                sb.append("</TD></TR>");
            } else if (smo instanceof StarSystem) {
                StarSystem ss = (StarSystem) smo;
                sb.append(drawSystemToList(ss));
            }
            // System.out.println("FOUND OBJECT " + smo.getType() + " at " + x + "/" + y);                
        }
        sb.append("</TABLE>");
        sb.append("</DIV>");
        sb.append("</TD>");
        // Info DIV
        sb.append("<TD width=\"300\" valign=\"top\">");
        sb.append("<DIV id=\"smdetailinfo\" class=\"smdetailinfo\">");
        // Display information about the first element in the list
        sb.append("</DIV>");
        sb.append("</TD>");
        sb.append("</TR></TABLE>");

        return sb;
    }

    private StringBuilder drawSystemToList(StarSystem system) {
        StringBuilder sb = new StringBuilder();

        sb.append("<TR valign=\"middle\"><TD>");
        sb.append("<TABLE><TR>");
        sb.append("<TD class=\"smainiconcell\"><IMG class=\"smmainlisticon\" src=\"pic/starmapJS/pic/star_small.png\" /></TD>");
        sb.append("<TD class=\"smmainsystem\" onclick=\"selectTarget(false,"+system.getPayload().getId()+"); showDetails('"+AreaMapContainer.STAR_OBJECT_PREFIX+"_"+system.getPayload().getId()+"')\">");                       
        sb.append(system.getName());
        sb.append("</TD>");

        sb.append("<TD class=\"smmainownercell\">");
        if (system.getPayload().isOwn()) {
            sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/green.png\" />");
        }
        sb.append("</TD>");
        sb.append("<TD class=\"smmainownercell\">");
        if (system.getPayload().isAllied()) {
            sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/blue.png\" />");
        }
        sb.append("</TD>");
        sb.append("<TD class=\"smmainownercell\">");
        if (system.getPayload().isNAP()) {
            sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/lightblue.png\" />");
        }
        sb.append("</TD>");
        sb.append("<TD class=\"smmainownercell\">");
        if (system.getPayload().isNeutral()) {
            sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/yellow.png\" />");
        }
        sb.append("</TD>");
        sb.append("<TD class=\"smmainownercell\">");
        if (system.getPayload().isEnemy()) {
            sb.append("<IMG class=\"smmainownerpic\" src=\"pic/starmapJS/pic/red.png\" />");
        }
        sb.append("</TD>");

        sb.append("</TR></TABLE>");
        sb.append("</TD></TR>");

        return sb;
    }

    private void drawPlanetInfo(SMPlanet planet) {

    }
    
    private String getChassisImage(int chassisSize) {
        switch(chassisSize) {
            case(1):
                return "star_fighter.png";            
            case(2):
                return "star_corvette.png";              
            case(3):
                return "star_frigate.png";             
            case(4):
                return "star_destroyer.png";
            case(5):
                return "star_cruiser.png";
            case(6):
                return "star_battleship.png";
            case(8):
                return "star_superbattleship.png";
            case(10):
                return "star_tender.png";
            default:
                return "";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
