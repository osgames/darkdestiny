/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class BattleLogRestoreResult {

    private Integer batteLogId;
    private ArrayList<BattleLogRestoreEntry> entries = new ArrayList<BattleLogRestoreEntry>();

    /**
     * @return the batteLogId
     */
    public Integer getBatteLogId() {
        return batteLogId;
    }

    public void addEntry(BattleLogRestoreEntry blre) {
        entries.add(blre);
    }

    /**
     * @param batteLogId the batteLogId to set
     */
    public void setBatteLogId(Integer batteLogId) {
        this.batteLogId = batteLogId;
    }

    /**
     * @return the entries
     */
    public ArrayList<BattleLogRestoreEntry> getEntries() {
        return entries;
    }

    
}