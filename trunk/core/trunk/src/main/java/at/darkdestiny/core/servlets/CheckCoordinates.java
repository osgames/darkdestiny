/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

 import at.darkdestiny.core.ML;import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.DiplomacyResult;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.DiplomacyResult;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class CheckCoordinates extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(CheckCoordinates.class);

    private PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuffer json = new StringBuffer();
        String errMessage = null;

        json.append("{ ");

        try {
            int userId = Integer.parseInt(request.getParameter("userId"));
            String sourceIdStr = request.getParameter("sourceId");
            String targetIdStr = request.getParameter("targetId");
            int sourceId = -1;
            int targetId = -1;

            try {
                sourceId = Integer.parseInt(sourceIdStr);
            } catch (NumberFormatException nfe) {
                PlayerPlanet pp = new PlayerPlanet();
                pp.setName(sourceIdStr);
                pp.setUserId(userId);
                ArrayList<PlayerPlanet> ppList = ppDAO.find(pp);

                log.debug("SOURCE = " + ppList.size());

                if (ppList.size() == 1) {
                    pp = ppList.get(0);
                    sourceId = pp.getPlanetId();
                } else if (ppList.isEmpty()) {
                    errMessage = ML.getMLStr("transport_err_couldnotfindstartplanet", userId);
                    errMessage = errMessage.replace("%PLANET%", sourceIdStr);
                } else if (ppList.size() > 1) {
                    errMessage = ML.getMLStr("transport_err_startplanetnotexplicit", userId);
                    errMessage = errMessage.replace("%PLANET%", sourceIdStr);
                }
            }

            try {
                targetId = Integer.parseInt(targetIdStr);
            } catch (NumberFormatException nfe) {
                PlayerPlanet pp = new PlayerPlanet();
                pp.setName(targetIdStr);
                pp.setUserId(userId);
                ArrayList<PlayerPlanet> ppList = ppDAO.find(pp);

                log.debug("TARGET = " + ppList.size());

                if (ppList.size() == 1) {
                    pp = ppList.get(0);
                    targetId = pp.getPlanetId();
                } else if (ppList.isEmpty()) {
                    errMessage = ML.getMLStr("transport_err_couldnotfindtargetplanet", userId);
                    errMessage = errMessage.replace("%PLANET%", sourceIdStr);
                } else if (ppList.size() > 1) {
                    errMessage = ML.getMLStr("transport_err_targetplanetnotexplicit", userId);
                    errMessage = errMessage.replace("%PLANET%", sourceIdStr);
                }
            }
            if (targetId > 0 && sourceId > 0 && targetId == sourceId) {
                errMessage = "Start und Zielplanet d�rfen nicht ident sein";
            }

            PlayerPlanet srcPlanet = ppDAO.findByPlanetId(sourceId);
            PlayerPlanet targetPlanet = ppDAO.findByPlanetId(targetId);

            if (srcPlanet == null) {
                errMessage = ML.getMLStr("transport_err_startplanetnotfound", userId);
            } else {
                if (srcPlanet.getUserId() != userId) {
                    errMessage = ML.getMLStr("transport_err_startplanetnotvalid", userId);
                }
            }

            if (targetPlanet == null) {
                errMessage = ML.getMLStr("transport_err_targetplanetnotfound", userId);
            } else {
                if (targetPlanet.getUserId() != userId) {
                    DiplomacyResult dr = DiplomacyUtilities.getDiplomacyResult(targetPlanet.getUserId(), userId);
                    if (dr.isAttackTradeFleets() == true) {
                        errMessage = ML.getMLStr("transport_err_planetnotyoursorally", userId);

                    }
                }
            }

            if (errMessage != null) {
                json.append("\"error\":\"" + errMessage + "\" }");
            } else {
                json.append("\"error\":\"\", ");
                json.append("\"source\": { ");
                json.append("\"id\":\"" + srcPlanet.getPlanetId() + "\", ");
                json.append("\"name\":\"" + srcPlanet.getName() + "\" }, ");
                json.append("\"target\": { ");
                json.append("\"id\":\"" + targetPlanet.getPlanetId() + "\", ");
                json.append("\"name\":\"" + targetPlanet.getName() + "\" } }");
            }

            out.println(json);
        } catch (Exception e) {
            json = new StringBuffer();
            json.append("{ \"error\":\"" + e.getMessage() + "\" }");

            out.println(json);
            DebugBuffer.writeStackTrace("Error in ShipDesignJSON: ", e);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
