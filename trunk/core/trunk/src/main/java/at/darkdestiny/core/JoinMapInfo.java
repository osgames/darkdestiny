/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Statistic;
import at.darkdestiny.core.scanner.StarMapQuadTree;
import at.darkdestiny.core.scanner.SystemDesc;
import at.darkdestiny.core.service.LoginService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.xml.XMLMemo;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 *
 * @author HorstRabe
 */
public class JoinMapInfo extends Service {

    public static final int FLEET_ENEMY = 3;
    public static final int FLEET_OWN = 1;
    public static final int FLEET_ALLIED = 2;
    public static final int HyperScannerId = 42;
    public static final int FLEET_MAXIMUM_VISIBLE_RANGE = 300;
    public static final int HYPERSCANNER_RANGE = 200;
    private Locale l;

    public JoinMapInfo(Locale l) {
        this.l = l;
    }

    public XMLMemo addTiles(XMLMemo starMap) {

         XMLMemo constants = new XMLMemo("Constants");

        XMLMemo language = new XMLMemo("Language");
        language.addAttribute("Locale", l.toString());

        constants.addChild(language);
        XMLMemo uniConstants = new XMLMemo("UniverseConstants");
        uniConstants.addAttribute("height", GameConstants.UNIVERSE_HEIGHT);
        uniConstants.addAttribute("width", GameConstants.UNIVERSE_WIDTH);
        constants.addChild(uniConstants);

        starMap.addChild(constants);




        XMLMemo galaxys = new XMLMemo("Galaxys");
        for (Galaxy g : (ArrayList<Galaxy>) Service.galaxyDAO.findAll()) {
            XMLMemo galaxy = new XMLMemo("Galaxy");
            galaxy.addAttribute("id", g.getId());
            galaxy.addAttribute("height", g.getHeight());
            galaxy.addAttribute("width", g.getWidth());
            galaxy.addAttribute("originX", g.getOriginX());
            galaxy.addAttribute("originY", g.getOriginY());
            galaxy.addAttribute("playerStart", g.getPlayerStart().toString());
            galaxy.addAttribute("startSystem", g.getStartSystem());
            galaxy.addAttribute("endSystem", g.getEndSystem());
            galaxys.addChild(galaxy);
        }
        starMap.addChild(galaxys);

        XMLMemo tiles = new XMLMemo("Tiles");
        for (Galaxy g : (ArrayList<Galaxy>) Service.galaxyDAO.findAll()) {
            if (!g.getPlayerStart()) {
                continue;
            }
            addGalaxyTiles(tiles, g);
        }

        starMap.addChild(tiles);

        return starMap;
    }

    public XMLMemo addGalaxyTiles(XMLMemo tiles, Galaxy g) {

        System.out.println("GALAXY : " + g.getId());
        System.out.println("##############################################################");
        System.out.println("##############################################################");
        System.out.println("##############################################################");
        System.out.println("##############################################################");
        System.out.println("##############################################################");
        System.out.println("##############################################################");
        HashMap<Integer, HashMap<Integer, Integer>> systemCount = Maps.newHashMap();
        HashMap<Integer, HashMap<Integer, Integer>> colonizedSystemCount = Maps.newHashMap();
        HashMap<Integer, HashMap<Integer, Integer>> colonizeableSystemCount = Maps.newHashMap();
        int maxSysCount = 0;
        int maxSysCountPerTile = 0;

        int tileSize = GameConstants.TILE_SIZE;
        int factor = 6;
        long then = System.currentTimeMillis();

        int xMin = (int) Math.round(g.getOriginX() - g.getWidth() / 2d);
        int yMin = (int) Math.round(g.getOriginY() - g.getHeight() / 2d);
        int xMax = (int) Math.round(g.getOriginX() + g.getWidth() / 2d);
        int yMax = (int) Math.round(g.getOriginY() + g.getHeight() / 2d);

        DebugBuffer.addLine(DebugLevel.DEBUG, "Quadtree initialize with factor : " + factor + " x : " + xMin + " y : " + yMin + " height : " + xMax + " width: " + yMax);
        StarMapQuadTree quadtree = new StarMapQuadTree(xMin, yMin, xMax, yMax, factor);

        DebugBuffer.addLine(DebugLevel.DEBUG, "Quadtree initialized! MS " + (System.currentTimeMillis() - then));

        HashMap<Integer, HashMap<Integer, String>> landMapping = new HashMap<Integer, HashMap<Integer, String>>();



        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Loading Land mapping!");

        for (Planet p : LoginService.findAllPlanets()) {
            if (p.getSystemId() > g.getEndSystem() || p.getSystemId() < g.getStartSystem()) {
                continue;
            }
            if (landMapping.containsKey(p.getSystemId())) {
                landMapping.get(p.getSystemId()).put(p.getId(), p.getLandType());
            } else {
                HashMap<Integer, String> planetMapping = new HashMap<Integer, String>();
                planetMapping.put(p.getId(), p.getLandType());
                landMapping.put(p.getSystemId(), planetMapping);
            }
        }

        DebugBuffer.addLine(DebugLevel.UNKNOWN, landMapping.size() + " mappings created");

        HashMap<Integer, Integer> pointsMap = new HashMap<Integer, Integer>();

        for (Statistic s : LoginService.findAllStatistics()) {
            pointsMap.put(s.getUserId(), s.getPoints());
        }

        try {
            for (at.darkdestiny.core.model.System s : LoginService.findAllSystemsOrdered()) {
                if (s.getId() > g.getEndSystem() || s.getId() < g.getStartSystem()) {
                    continue;
                }
                if(!g.getPlayerStart()){
                    continue;
                }
                int x = s.getX();
                int y = s.getY();

                int xNorm = x - xMin;
                int yNorm = y - yMin;

                int xRounded = (int) (Math.floor((double) xNorm / tileSize) * tileSize);
                int yRounded = (int) (Math.floor((double) yNorm / tileSize) * tileSize);

                xRounded += xMin;
                yRounded += yMin;
                HashMap<Integer, Integer> yCoord = systemCount.get(xRounded);
                if (yCoord == null) {
                    yCoord = Maps.newHashMap();
                }
                Integer val = yCoord.get(yRounded);

                if (val == null) {
                    val = 1;
                } else {
                    val++;
                }
                maxSysCount += 1;
                maxSysCountPerTile = Math.max(maxSysCountPerTile, val);
                yCoord.put(yRounded, val);
                systemCount.put(xRounded, yCoord);
                if (!landMapping.containsKey(s.getId())) {
                    continue;
                }

                int points = 0;
                long population = 0;
                boolean occupied = false;
                boolean hasMPlanet = false;

                HashMap<Integer, String> planetMapping = landMapping.get(s.getId());

                for (Integer planetId : planetMapping.keySet()) {
                    PlayerPlanet pp = LoginService.findPlayerPlanetByPlanetId(planetId);
                    Planet p = Service.planetDAO.findById(planetId);
                    if(p.getLandType().equals(Planet.LANDTYPE_M)){
                        hasMPlanet = true;
                    }
                    if (pp != null) {
                        population += pp.getPopulation();
                        int userId = pp.getUserId();

                        if (pointsMap.containsKey(userId)) {
                            if (points < pointsMap.get(userId)) {
                                points = pointsMap.get(userId);
                            }
                        }

                        occupied = true;
                    }
                }

                if (occupied) {
                    yCoord = colonizedSystemCount.get(xRounded);
                    if (yCoord == null) {
                        yCoord = Maps.newHashMap();
                    }
                    val = yCoord.get(yRounded);
                    if (val == null) {
                        val = 1;
                    } else {
                        val++;
                    }
                    yCoord.put(yRounded, val);
                    colonizedSystemCount.put(xRounded, yCoord);
                    SystemDesc sd = new SystemDesc(s.getId(), s.getX(), s.getY(), population, points);
                    // if (rs.getInt(1) == 2405) DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding system 2405 with population: " + (int)Math.log10(population) + " points: " + points + " at ("+rs.getInt(3)+","+rs.getInt(4)+")");
                    quadtree.addItemToTree(sd);
                }else{
                    if(hasMPlanet){
                        yCoord = colonizeableSystemCount.get(xRounded);
                    if (yCoord == null) {
                        yCoord = Maps.newHashMap();
                    }
                    val = yCoord.get(yRounded);
                    if (val == null) {
                        val = 1;
                    } else {
                        val++;
                    }
                    yCoord.put(yRounded, val);
                    colonizeableSystemCount.put(xRounded, yCoord);
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while adding systems to QuadTree: ", e);
        }



        for (int i = xMin; i < xMax; i = i + tileSize) {
            for (int j = yMin; j < yMax; j = j + tileSize) {

                XMLMemo tile = new XMLMemo("Tile");
                tile.addAttribute("x", i);
                tile.addAttribute("y", j);

                int x = i + tileSize / 2;
                int y = j + tileSize / 2;

                ArrayList<SystemDesc> systems = quadtree.getItemsAround(x, y, tileSize * 3);

                tile.addAttribute("systems", systems.size());
                long population = 0;
                int popScore = 1;
                int userScore = 0;
                int intensity = 128;

                for (SystemDesc sd : systems) {
                    double distanceToCore = Math.sqrt(Math.pow(x - sd.x, 2d) + Math.pow(y - sd.y, 2d));

                    population += (int) ((double) sd.popscore / Math.pow(10d, Math.max(1d, Math.ceil(distanceToCore / (double) tileSize))));


                    if (userScore < sd.statscore) {
                        userScore = sd.statscore;
                    }
                }

                //   log.debug("POPULATION LOG IS " + Math.log10(population));
                intensity = (int) Math.max(0d, Math.min(255d, Math.ceil(255d / 121d * Math.pow(Math.log10(population), 2d))));
                //   log.debug("Intensity = " + intensity);

                if (systemCount.get(i) != null && systemCount.get(i).get(j) != null) {
                    tile.addAttribute("sysCount", systemCount.get(i).get(j));
                }
                if (colonizedSystemCount.get(i) != null && colonizedSystemCount.get(i).get(j) != null) {
                    tile.addAttribute("colSysCount", colonizedSystemCount.get(i).get(j));
                }
                if (colonizeableSystemCount.get(i) != null && colonizeableSystemCount.get(i).get(j) != null) {
                    tile.addAttribute("startSysCount", colonizeableSystemCount.get(i).get(j));
                }
                tile.addAttribute("maxSysCountPerTile", maxSysCountPerTile);
                tile.addAttribute("maxSysCount", maxSysCount);
                tile.addAttribute("popScore", popScore);
                tile.addAttribute("userScore", userScore);
                tile.addAttribute("intensity", intensity);
                tiles.addChild(tile);
            }


        }




        return tiles;
    }

    public static String secString(String input) {
        char[] inChars = input.toCharArray();
        char[] outChars = new char[inChars.length];

        for (int i = 0; i
                < inChars.length; i++) {
            if ((inChars[i] >= 65) && (inChars[i] <= 90)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 97) && (inChars[i] <= 122)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 35) && (inChars[i] <= 62)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] >= 192) && (inChars[i] <= 255)) {
                outChars[i] = inChars[i];
                continue;
            }

            if ((inChars[i] == 45) || (inChars[i] == 46) || (inChars[i] == 95) || (inChars[i] == 32)) {
                outChars[i] = inChars[i];
                continue;
            }

            outChars[i] = 95;
        }

        StringBuffer sb = new StringBuffer();
        sb.append(outChars);

        return sb.toString();
    }
}
