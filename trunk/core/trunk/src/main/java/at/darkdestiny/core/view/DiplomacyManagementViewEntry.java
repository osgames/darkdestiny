/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view;

import at.darkdestiny.core.model.DiplomacyRelation;

/**
 *
 * @author Stefan
 */
public class DiplomacyManagementViewEntry {
    private final DiplomacyRelation base;
    private boolean overridden = false;
    private DiplomacyManagementViewEntry overriddenBy = null;
    private boolean embassy = false;

    public DiplomacyManagementViewEntry(DiplomacyRelation base) {
        this.base = base;
    }

    /**
     * @return the base
     */
    public DiplomacyRelation getBase() {
        return base;
    }

    /**
     * @return the overridden
     */
    public boolean isOverridden() {
        return overridden;
    }

    /**
     * @param overridden the overridden to set
     */
    public void setOverridden(boolean overridden) {
        this.overridden = overridden;
    }

    /**
     * @return the overriddenBy
     */
    public DiplomacyManagementViewEntry getOverriddenBy() {
        return overriddenBy;
    }

    /**
     * @param overriddenBy the overriddenBy to set
     */
    public void setOverriddenBy(DiplomacyManagementViewEntry overriddenBy) {
        this.overriddenBy = overriddenBy;
    }

    /**
     * @return the embassyExists
     */
    public boolean hasEmbassy() {
        return embassy;
    }

    /**
     * @param embassyExists the embassyExists to set
     */
    public void setEmbassy(boolean embassy) {
        this.embassy = embassy;
    }
}
