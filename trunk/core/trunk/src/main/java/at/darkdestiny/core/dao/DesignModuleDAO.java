/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

 import at.darkdestiny.core.model.DesignModule;import at.darkdestiny.framework.QueryKey;
import at.darkdestiny.framework.QueryKeySet;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.model.DesignModule;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class DesignModuleDAO extends ReadWriteTable<DesignModule> implements GenericDAO {

    private static final Logger log = LoggerFactory.getLogger(DesignModuleDAO.class);

    public ArrayList<DesignModule> findByDesignId(Integer designId) {
        DesignModule dm = new DesignModule();
        dm.setDesignId(designId);
        return find(dm);
    }

    public void deleteDesign(int designId) {
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("designId",designId));

        removeAll(qks);
    }

    public void addModulesForDesign(int designId, HashMap<Integer,Integer> modules) {
        ArrayList<DesignModule> toInsert = new ArrayList<DesignModule>();

        for (Map.Entry<Integer,Integer> mEntry : modules.entrySet()) {
            if (mEntry.getValue() <= 0) continue;

            DesignModule dm = new DesignModule();
            dm.setDesignId(designId);
            dm.setModuleId(mEntry.getKey());
            dm.setCount(mEntry.getValue());
            dm.setIsConstruction(false);

            log.debug("----------------- BEFORE ----------------");
            log.debug("-- DID: "+designId+" MID: "+mEntry.getKey()+" C: "+mEntry.getValue()+" ISC: false --");
            toInsert.add(dm);
            log.debug("----------------- AFTER ----------------");
        }

        this.insertAll(toInsert);
    }
}
