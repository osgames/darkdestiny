/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.interfaces;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.spacecombat.CombatReportGenerator;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Dreloc
 */
public interface ISimulationSet{
    public void cleanUp();
    public ArrayList<PlayerFleet> getFleets();
    public CombatReportGenerator.CombatType getCombatType();
    public RelativeCoordinate getRelLocation();
    public AbsoluteCoordinate getAbsLocation();
    public CombatReportGenerator getCombatReportGenerator();
    public HashMap<Integer, ArrayList<Integer>> getGroups();
    public String getBattleClass();
    public int getPlanetOwner();
    public boolean hasHUShield();
    public boolean hasPAShield();
    public HashMap<Integer, Integer> getGroundDefense();
}
