/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.model.ScrapResource;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class ScrapResourceDAO extends ReadWriteTable<ScrapResource> implements GenericDAO{

    public ArrayList<ScrapResource> findBy(Integer locationId, ELocationType locationType){
        ScrapResource scrapResource = new ScrapResource();
        scrapResource.setLocationId(locationId);
        scrapResource.setLocationType(locationType);
        return find(scrapResource);
    }

    public ScrapResource findBy(Integer locationId, ELocationType locationType, Integer resourceId){
        ScrapResource scrapResource = new ScrapResource();
        scrapResource.setLocationId(locationId);
        scrapResource.setLocationType(locationType);
        scrapResource.setResourceId(resourceId);
        return get(scrapResource);
    }
}
