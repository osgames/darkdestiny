package at.darkdestiny.core.title;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;


public class TheTyrant extends AbstractTitle {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
	public boolean check(int userId) {
            for(PlayerPlanet pp : ppDAO.findByUserId(userId)){
                if(pp.getTax() >= 60){
                    return true;
                }
            }
		return false;
	}
}
