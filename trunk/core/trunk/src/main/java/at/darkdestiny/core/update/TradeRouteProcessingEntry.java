/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.update;

import at.darkdestiny.core.dao.TradeRouteDetailDAO;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TradeRouteProcessingEntry {

    private TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    private final TradeRouteExt route;
    private final ArrayList<TradeRouteDetailProcessing> routeDetailsList =
            new ArrayList<TradeRouteDetailProcessing>();
    private final ArrayList<TradeRouteDetailProcessing> fromToList =
            new ArrayList<TradeRouteDetailProcessing>();
    private final ArrayList<TradeRouteDetailProcessing> toFromList =
            new ArrayList<TradeRouteDetailProcessing>();
    private boolean fromTo = false;
    private boolean toFrom = false;
    private int freeRouteCap;
    private boolean procClosed;
    private boolean deleted;

    public TradeRouteProcessingEntry(TradeRouteExt tre) {
        route = tre;

        // Load maxQty possible for every Detail
        ArrayList<TradeRouteDetail> routeDetails = trdDAO.getActiveByRouteId(tre.getBase().getId());
        int partRoutes = routeDetails.size();

        // log.debug(route.getBase().getType() + " - partROutes : " + partRoutes);
        // log.debug(route.getBase().getType() + " - tre.getCapacity() = " + tre.getCapacity());
        // int maxCapPerRoute = 0;
        // if (partRoutes > 0) {
        //     maxCapPerRoute = (int) Math.floor(tre.getCapacity() / partRoutes);
            // log.debug(route.getBase().getType() + " - Capacity per route = " + maxCapPerRoute);
        // }

        for (TradeRouteDetail trd : routeDetails) {
            TradeRouteDetailProcessing trdp = new TradeRouteDetailProcessing(trd);
            routeDetailsList.add(trdp);
            if (trd.getReversed()) {
                toFrom = true;
                toFromList.add(trdp);
            } else {
                fromTo = true;
                fromToList.add(trdp);
            }
        }

        // freeRouteCap = tre.getCapacity();
        // log.debug(route.getBase().getType() + " - freeRouteCap = " + maxCapPerRoute);
    }

    public TradeRouteExt getRoute() {
        return route;
    }

    public ArrayList<TradeRouteDetailProcessing> getRouteDetails() {
        return routeDetailsList;
    }

    public ArrayList<TradeRouteDetailProcessing> getFromToList() {
        return fromToList;
    }

    public ArrayList<TradeRouteDetailProcessing> getToFromList() {
        return toFromList;
    }

    public boolean isFromTo() {
        return fromTo;
    }

    public boolean isToFrom() {
        return toFrom;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
