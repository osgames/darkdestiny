/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.DeletedUser;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Aion
 */
public class DeletedUserDAO extends ReadWriteTable<DeletedUser> implements GenericDAO {
    public DeletedUser findByEmail(String email) {
        DeletedUser du = new DeletedUser();
        du.setEmail(email);
        return (DeletedUser)get(du);
    }
}
