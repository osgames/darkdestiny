/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

 import at.darkdestiny.core.FormatUtilities;import at.darkdestiny.core.ML;
import at.darkdestiny.core.battlelog.BattleLogSimulationSet;
import at.darkdestiny.core.battlelog.TempConfig;
import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.model.BattleLogEntry;
import at.darkdestiny.core.model.BattleResult;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.ships.ShipDesignDetailed;
import at.darkdestiny.core.spacecombat.CombatGroupFleet;
import at.darkdestiny.core.spacecombat.CombatHandler_NEW;
import at.darkdestiny.core.spacecombat.CombatSimulationSet;
import at.darkdestiny.core.spacecombat.SC_DataEntry;
import at.darkdestiny.core.spacecombat.SpaceCombatDataLogger;
import at.darkdestiny.util.DebugBuffer;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.battlelog.BattleLogSimulationSet;
import at.darkdestiny.core.battlelog.TempConfig;
import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.model.BattleLogEntry;
import at.darkdestiny.core.model.BattleResult;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.ships.ShipDesignDetailed;
import at.darkdestiny.core.spacecombat.CombatGroupFleet;
import at.darkdestiny.core.spacecombat.CombatHandler_NEW;
import at.darkdestiny.core.spacecombat.CombatSimulationSet;
import at.darkdestiny.core.spacecombat.SC_DataEntry;
import at.darkdestiny.core.spacecombat.SpaceCombatDataLogger;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class BattleLogService extends Service {
    private static final Logger log = LoggerFactory.getLogger(BattleLogService.class);

    private static final Object mutex = new Object();
    private static TreeMap<Integer, TempConfig> tempConfig = new TreeMap<Integer, TempConfig>();
    private static int tmpConfigId = 0;

    public static SpaceCombatDataLogger getResult(int battleId) {
        synchronized (mutex) {
            //Build fake battle
            HashMap<Integer, Integer> attacker = new HashMap<Integer, Integer>();
            HashMap<Integer, Integer> defender = new HashMap<Integer, Integer>();

            int attackerId = 0;
            boolean first = true;
            int defenderId = 0;
            //Load shipdesigns

            for (BattleLogEntry ble : battleLogEntryDAO.findById(battleId)) {
                ShipDesign sd = shipDesignDAO.findById(ble.getDesignId());
                if (first) {
                    attackerId = sd.getUserId();
                    first = false;
                }
                if (attackerId == sd.getUserId()) {
                    attacker.put(ble.getDesignId(), ble.getCount());
                } else {
                    defenderId = sd.getUserId();
                    defender.put(ble.getDesignId(), ble.getCount());
                }
            }

            CombatSimulationSet css = new CombatSimulationSet(attackerId, defenderId, attacker, defender);

            CombatHandler_NEW ch = new CombatHandler_NEW(css);
            ch.executeBattle();
            return ch.getLoggingData();
        }
    }

    public static void saveResult(String xml, String name, int userId) {


        BattleResult br = new BattleResult();
        br.setName(name + " - v" + gameDataDAO.findById(1).getVersion());
        br.setData(xml);
        br.setVersion(gameDataDAO.findById(1).getVersion());
        br.setUserId(userId);
        battleResultDAO.add(br);


    }

    public static SpaceCombatDataLogger generateFight(Map<String, String[]> params) {
        ArrayList<String> players = new ArrayList<String>();
        HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants = new HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>>();
        HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> designs = new HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>>();
        HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> shipCount = new HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>>();

        HashMap<Integer, Integer> groundDefense = new HashMap<Integer, Integer>();
        boolean PAShield = false;
        boolean HUShield = false;
        int planetOwner = 0;
        HashMap<Integer, ArrayList<Integer>> groups = new HashMap<Integer, ArrayList<Integer>>();

        String battleClass = null;

        for (Map.Entry<String, String[]> entry : params.entrySet()) {

            if (entry.getKey().startsWith("group")) {

                StringTokenizer st = new StringTokenizer(entry.getKey(), "_");
                String token = "";
                while (st.hasMoreElements()) {
                    //Group tag
                    token = st.nextToken();
                    //userId
                    token = st.nextToken();
                    int userId = Integer.parseInt(token);
                    //groupId
                    int groupId = Integer.parseInt(entry.getValue()[0]);
                    ArrayList<Integer> users = groups.get(groupId);
                    if (users == null) {
                        users = new ArrayList<Integer>();
                    }
                    users.add(userId);
                    groups.put(groupId, users);
                }
            }
            if (entry.getKey().equals("battleClass")) {
                battleClass = entry.getValue()[0];
            }
            if (entry.getKey().startsWith("planetOwner")) {
                try {
                    planetOwner = Integer.parseInt(entry.getValue()[0]);
                } catch (Exception e) {
                }
            }
            if (entry.getKey().startsWith("shield")) {
                try {
                    int cId = Integer.parseInt(entry.getValue()[0]);
                    if (cId == Construction.ID_PLANETARY_PARATRONSHIELD) {
                        PAShield = true;
                    } else if (cId == Construction.ID_PLANETARY_HUESHIELD) {
                        HUShield = true;
                    }
                } catch (Exception e) {
                }
            }
            if (entry.getKey().startsWith("defense")) {

                StringTokenizer st = new StringTokenizer(entry.getKey(), "_");
                String token = "";

                while (st.hasMoreElements()) {
                    token = st.nextToken();
                    if (!token.equals("defense")) {
                        Integer cId = Integer.parseInt(token);
                        Integer value = 0;
                        try {
                            value = Integer.parseInt(entry.getValue()[0]);
                        } catch (Exception e) {
                        }
                        if (value > 0) {
                            groundDefense.put(cId, value);
                        }
                    }
                }
            }


            //Fleetparsing following No other parsing beyond
            if (!entry.getKey().startsWith("fleet")) {
                continue;
            }
            StringTokenizer st = new StringTokenizer(entry.getKey(), "_");
            String token = "";
            if (st.hasMoreElements()) {
                int count = 0;
                int value = 0;
                int userId = 0;
                int fleet = 0;
                int fleetEntry = 0;
                String type = "";
                while (st.hasMoreElements()) {
                    token = st.nextToken();
                    switch (count) {

                        case (0):

                            break;
                        case (1):
                            userId = Integer.parseInt(token);
                            String userName = userDAO.findById(userId).getGameName();
                            if (!players.contains(userName)) {
                                players.add(userName);
                            }
                            break;
                        case (2):
                            fleet = Integer.parseInt(token);
                            break;
                        case (3):
                            fleetEntry = Integer.parseInt(token);
                            break;
                        case (4):
                            type = token;
                            break;
                    }
                    count++;
                }
                value = Integer.parseInt(entry.getValue()[0]);
                if (value > 0) {
                    if (type.equals("design")) {

                        HashMap<Integer, HashMap<Integer, Integer>> desEntry = designs.get(userId);
                        if (desEntry == null) {
                            desEntry = new HashMap<Integer, HashMap<Integer, Integer>>();
                        }
                        HashMap<Integer, Integer> desEntry2 = desEntry.get(fleet);
                        if (desEntry2 == null) {
                            desEntry2 = new HashMap<Integer, Integer>();
                        }
                        if (desEntry2.get(fleetEntry) == null) {
                            desEntry2.put(fleetEntry, value);
                        } else {
                            desEntry2.put(fleetEntry, (value + desEntry2.get(fleetEntry)));
                        }
                        desEntry.put(fleet, desEntry2);
                        designs.put(userId, desEntry);

                    }
                    if (type.equals("count")) {

                        HashMap<Integer, HashMap<Integer, Integer>> countEntry = shipCount.get(userId);
                        if (countEntry == null) {
                            countEntry = new HashMap<Integer, HashMap<Integer, Integer>>();
                        }
                        HashMap<Integer, Integer> countEntry2 = countEntry.get(fleet);
                        if (countEntry2 == null) {
                            countEntry2 = new HashMap<Integer, Integer>();
                        }
                        if (countEntry2.get(fleetEntry) == null) {
                            countEntry2.put(fleetEntry, value);
                        } else {
                            countEntry2.put(fleetEntry, (value + countEntry2.get(fleetEntry)));
                        }
                        countEntry.put(fleet, countEntry2);
                        shipCount.put(userId, countEntry);

                    }
                }
            }

        }
        //Building final map
        for (Map.Entry<Integer, HashMap<Integer, HashMap<Integer, Integer>>> entry : shipCount.entrySet()) {

            for (Map.Entry<Integer, HashMap<Integer, Integer>> entry1 : entry.getValue().entrySet()) {

                for (Map.Entry<Integer, Integer> entry2 : entry1.getValue().entrySet()) {
                    int count = entry2.getValue();
                    int design = designs.get(entry.getKey()).get(entry1.getKey()).get(entry2.getKey());
                    HashMap<Integer, HashMap<Integer, Integer>> parts = participants.get(entry.getKey());
                    if (parts == null) {
                        parts = new HashMap<Integer, HashMap<Integer, Integer>>();
                    }
                    HashMap<Integer, Integer> des = parts.get(entry1.getKey());
                    if (des == null) {
                        des = new HashMap<Integer, Integer>();
                    }
                    if (des.get(design) == null) {
                        des.put(design, count);
                    } else {
                        des.put(design, (count + des.get(design)));
                    }
                    parts.put(entry1.getKey(), des);
                    participants.put(entry.getKey(), parts);
                }
            }
        }
        printData(participants);
        BattleLogSimulationSet blss = new BattleLogSimulationSet(participants, groups, battleClass, planetOwner, HUShield, PAShield, groundDefense, null, null);


        int counter = tempConfig.size() + 1;
        tmpConfigId = counter;
        TempConfig tc = new TempConfig(players, System.currentTimeMillis(), blss);
        tempConfig.put(counter, tc);
        return startCombat(blss);

    }

    public static SpaceCombatDataLogger startCombat(BattleLogSimulationSet blss) {
        synchronized (mutex) {
            CombatHandler_NEW ch = new CombatHandler_NEW(blss);

            try {
                ch.executeBattle();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Exception occured while simulating combat: ", e);
            } finally {
                blss.cleanUp();
            }

            return ch.getLoggingData();
        }
    }

    public static SpaceCombatDataLogger startConfig(int configId) {
        return startCombat(tempConfig.get(configId).getBlss());

    }

    public static void deleteConfig(int configId) {
        tempConfig.remove(configId);

    }

    public static void printData(HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants) {
        for (Map.Entry<Integer, HashMap<Integer, HashMap<Integer, Integer>>> users : participants.entrySet()) {
            log.debug("##### User : " + users.getKey() + " #####");
            for (Map.Entry<Integer, HashMap<Integer, Integer>> fleet : users.getValue().entrySet()) {
                log.debug("##### Fleet : " + fleet.getKey() + " #####");

                for (Map.Entry<Integer, Integer> design : fleet.getValue().entrySet()) {
                    log.debug("##### Design : " + design.getKey() + " => " + design.getValue() + " #####");

                }
            }
        }
    }

    public static int getTotal(HashMap<EDamageLevel, Integer> ships) {
        int count = 0;
        for (Map.Entry<EDamageLevel, Integer> entry : ships.entrySet()) {
            count += entry.getValue();
        }
        return count;
    }

    public static int getLost(HashMap<EDamageLevel, Integer> ships) {
        int count = ships.get(EDamageLevel.DESTROYED);
        return count;
    }

    public static TreeMap<Integer, TempConfig> getTempConfigs() {
        log.debug("Size : " + tempConfig.size());
        return tempConfig;
    }

    public static TreeMap<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>> reSort(HashMap<Integer, HashMap<CombatGroupFleet.UnitTypeEnum, HashMap<Integer, SC_DataEntry>>> input) {
        TreeMap<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>> data = new TreeMap<Integer, TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>>();
        for (Map.Entry<Integer, HashMap<CombatGroupFleet.UnitTypeEnum, HashMap<Integer, SC_DataEntry>>> entry : input.entrySet()) {
            for (Map.Entry<CombatGroupFleet.UnitTypeEnum, HashMap<Integer, SC_DataEntry>> types : entry.getValue().entrySet()) {
                for (Map.Entry<Integer, SC_DataEntry> designs : types.getValue().entrySet()) {

                    TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>> type = data.get(entry.getKey());
                    if (type == null) {
                        type = new TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>>();
                    }
                    TreeMap<Integer, TreeMap<Integer, SC_DataEntry>> users = type.get(types.getKey());
                    if (users == null) {
                        users = new TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>();
                    }
                    int key = 0;
                    if (types.getKey().equals(CombatGroupFleet.UnitTypeEnum.SHIP) || types.getKey().equals((CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE))) {
                        ShipDesign sd = shipDesignDAO.findById(designs.getValue().designId);

                        key = sd.getChassis();
                    } else {
                        key = designs.getValue().designId;
                    }
                    TreeMap<Integer, SC_DataEntry> chassis = users.get(key);
                    if (chassis == null) {
                        chassis = new TreeMap<Integer, SC_DataEntry>();
                    }
                    chassis.put(designs.getValue().designId, designs.getValue());
                    users.put(key, chassis);
                    type.put(types.getKey(), users);
                    data.put(entry.getKey(), type);
                }


            }
        }


        return data;
    }

    public static ArrayList<RessAmountEntry> getRessources(TreeMap<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>> data) {
        ArrayList<RessAmountEntry> result = new ArrayList<RessAmountEntry>();
        TreeMap<Integer, RessAmountEntry> sorted = new TreeMap<Integer, RessAmountEntry>();
        for (Map.Entry<CombatGroupFleet.UnitTypeEnum, TreeMap<Integer, TreeMap<Integer, SC_DataEntry>>> types : data.entrySet()) {
            for (Map.Entry<Integer, TreeMap<Integer, SC_DataEntry>> chassis : types.getValue().entrySet()) {
                for (Map.Entry<Integer, SC_DataEntry> designs : chassis.getValue().entrySet()) {
                    SC_DataEntry de = designs.getValue();
                    RessourcesEntry re = null;
                    if(types.getKey().equals(CombatGroupFleet.UnitTypeEnum.SHIP) || types.getKey().equals(CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE)){

                    ShipDesignExt sde = new ShipDesignExt(de.designId);
                    re = sde.getRessCost();
                    }else if(types.getKey().equals(CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE)){
                        ConstructionExt ce = new ConstructionExt(de.designId);
                        re = ce.getRessCost();
                    }

                    for (RessAmountEntry raeTmp : re.getSortedRessArray()) {
                        RessAmountEntry rae = sorted.get(raeTmp.getRessId());
                        if (rae == null) {
                            rae = new RessAmountEntry(raeTmp.getRessId(), raeTmp.getQty() * getTotal(de.getDamageDistribution()));
                        } else {
                            double qty = rae.getQty();
                            qty += raeTmp.getQty() * getTotal(de.getDamageDistribution());
                            rae = new RessAmountEntry(raeTmp.getRessId(), qty);
                        }
                        sorted.put(raeTmp.getRessId(), rae);
                    }
                }
            }
        }
        result.addAll(sorted.values());
        return result;

    }

    public static String toColor(EDamageLevel damage) {
        String color = "#FFFFFF";
        if (damage.equals(EDamageLevel.DESTROYED)) {
            color = "#FF0000";
        } else if (damage.equals(EDamageLevel.HEAVYDAMAGE)) {
            color = "#AA4400";
        } else if (damage.equals(EDamageLevel.MEDIUMDAMAGE)) {
            color = "#AA7700";
        } else if (damage.equals(EDamageLevel.LIGHTDAMAGE)) {
            color = "#AA9900";
        } else if (damage.equals(EDamageLevel.MINORDAMAGE)) {
            color = "#AAAA00";
        } else if (damage.equals(EDamageLevel.NODAMAGE)) {
            color = "#AAAA44";
        }
        return color;
    }

    public static SpaceCombatDataLogger getBattleResult(int battleResultId) {
        BattleResult br = battleResultDAO.findById(battleResultId);
        XStream xstream = new XStream(new DomDriver());
        SpaceCombatDataLogger scdl = (SpaceCombatDataLogger) xstream.fromXML(br.getData());
        return scdl;
    }

    public static int getConfigId() {
        return tmpConfigId;
    }

    public static SpaceCombatDataLogger substractResult(SpaceCombatDataLogger logger, SpaceCombatDataLogger toSubtract) {
        HashMap<Integer, HashMap<CombatGroupFleet.UnitTypeEnum, HashMap<Integer, SC_DataEntry>>> input = logger.getAllLoggedShips();
        for (Map.Entry<Integer, HashMap<CombatGroupFleet.UnitTypeEnum, HashMap<Integer, SC_DataEntry>>> entry : input.entrySet()) {
            for (Map.Entry<CombatGroupFleet.UnitTypeEnum, HashMap<Integer, SC_DataEntry>> designs : entry.getValue().entrySet()) {

                for (Map.Entry<Integer, SC_DataEntry> design : designs.getValue().entrySet()) {
                    design.setValue(design.getValue().subtract(toSubtract.getShipLogger(entry.getKey(), designs.getKey(), design.getKey())));
                }
            }
        }
        return logger;
    }

    public static String buildPopUp(int designId, Locale locale, int count) {
        String popUp = "";
        ShipDesignDetailed sdd = new ShipDesignDetailed(designId);
        popUp += "<TABLE cellpadding=0 cellspacing=0>";
        for (Map.Entry<Integer, Integer> module : sdd.getModuleCount().entrySet()) {
            popUp += "<TR>";
            popUp += "<TD colspan=2><FONT size=1pt  align=right color=#FFFFFF>"
                    + ML.getMLStr(moduleDAO.findById(module.getKey()).getName(), locale) + ": " + module.getValue()
                    + "</FONT></TD>";
            popUp += "</TR>";
        }
        ShipDesignExt sde = new ShipDesignExt(designId);
        for (RessAmountEntry rae : sde.getRessCost().getSortedRessArray()) {
            popUp += "<TR>";
            popUp += "<TD><FONT size=1pt  align=right color=#FFFFFF>"
                    + "<IMG width=15px height=15px src=" + ressourceDAO.findRessourceById(rae.getRessId()).getImageLocation() + " />"
                    // + ML.getMLStr(ressourceDAO.findRessourceById(rae.getRessId()).getName(), locale)
                    + "</FONT></TD>";
            popUp += "<TD align=right><FONT size=1pt  align=right color=#FFFFFF>"
                    + FormatUtilities.getFormattedDecimal(rae.getQty(), 0)
                    + "</FONT></TD>";
            popUp += "<TD align=right><FONT size=1pt  align=right color=#FFFFFF>"
                    + FormatUtilities.getFormattedDecimal(rae.getQty() * count, 0)
                    + "</FONT></TD>";
            popUp += "</TR>";
        }
        popUp += "</TABLE>";

        return popUp;

    }

    public static String groupToColor(int id) {
        String color = "white";
        switch (id) {
            case (1):
                color = "red";
                break;
            case (2):
                color = "blue";
                break;
            case (3):
                color = "green";
                break;
            case (4):
                color = "yellow";
                break;
        }
        return color;
    }
}
