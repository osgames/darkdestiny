/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Note;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Aion
 */
public class NoteDAO extends ReadWriteTable<Note> implements GenericDAO {


    public Note findBy(int userId, int planetId){
        Note n = new Note();
        n.setPlanetId(planetId);
        n.setUserId(userId);

        return (Note)get(n);
    }

}
