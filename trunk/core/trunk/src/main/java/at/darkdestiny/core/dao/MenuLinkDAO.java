/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.MenuLink;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Eobane
 */
public class MenuLinkDAO extends ReadWriteTable<MenuLink> implements GenericDAO {

    public MenuLink findById(int id) {
        MenuLink menuLink = new MenuLink();
        menuLink.setId(id);
        return get(menuLink);
    }
}
