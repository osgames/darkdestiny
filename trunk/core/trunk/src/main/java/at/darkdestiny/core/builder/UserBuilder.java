/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.builder;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.model.User;
import com.google.common.base.Strings;

/**
 *
 * @author Admin
 */
public class UserBuilder {


    private User u;

    public UserBuilder(){
        this.u = new User();
    }

    public UserBuilder setGameName(String gameName){
        this.u.setGameName(gameName);
        return this;
    }
    public UserBuilder setUserName(String userName){
        this.u.setUserName(userName);
        return this;
    }
    public UserBuilder setIp(String ip){
        this.u.setIp(ip);
        return this;
    }
    public UserBuilder setPassword(String password){
        this.u.setPassword(password);
        return this;
    }
    public UserBuilder setEmail(String email){
        this.u.setEmail(email);
        return this;
    }

    public User build(){

        if(u.getIp().equals(Strings.emptyToNull(u.getIp()))){
            u.setIp("127.0.0.1");
        }
        if(u.getEmail().equals(Strings.emptyToNull(u.getEmail()))){
            u.setEmail("test@darkdestiny.at");
        }
        if(u.getPassword().equals(Strings.emptyToNull(u.getPassword()))){
            u.setPassword("test");
        }
        if(u.getJoinDate() == null){
            u.setJoinDate((long)GameUtilities.getCurrentTick2());
        }


        return u;
    }
}
