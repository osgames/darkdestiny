/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.TechType;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Bullet
 */
public class TechTypeDAO extends ReadOnlyTable<TechType> implements GenericDAO {

    public TechType findById(Integer id) {
        TechType tt = new TechType();
        tt.setId(id);
        return (TechType) get(tt);
    }
}
