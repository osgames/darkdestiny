package at.darkdestiny.core.title;

import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Production;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;

public class TheManiac extends AbstractTitle {

    public boolean check(int userId) {

        
        int cpCount = 0;
        for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)) {
            cpCount += getPointsForConstructionId(pp.getPlanetId(), Construction.ID_RESEARCHLABORATORY);
            cpCount += getPointsForConstructionId(pp.getPlanetId(), Construction.ID_PLANETARY_POSITRONCOMPUTER);
        }
        if (cpCount >= 500) {
            return true;
        } else {
            return false;
        }
    }

    private long getPointsForConstructionId(int planetId, int constructionId) {

        long cpProduction = 0;
        ArrayList<Production> ps = Service.productionDAO.findProductionForConstructionId(constructionId);
        for (Production p : ps) {
            if (p.getRessourceId() == Ressource.RP_COMP) {
                cpProduction = (p.getQuantity());
                break;
            }
        }


        PlanetConstruction labs = Service.planetConstructionDAO.findBy(planetId, constructionId);
        if (labs != null) {
            return labs.getNumber() * cpProduction;
        } else {
            return 0L;
        }

    }
}
