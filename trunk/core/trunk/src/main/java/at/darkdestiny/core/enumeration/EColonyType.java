/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.enumeration;

/**
 *
 * @author Stefan
 */
public enum EColonyType {
    DEFAULT, MINING_COLONY, AGRICULTURAL_COLONY, INDUSTRIAL_COLONY, MILITARY_OUTPOST, POPULATION_COLONY, TRADE_COLONY, NPC_TRANSMITTER, ADMINISTRATIVE_COLONY;
}
