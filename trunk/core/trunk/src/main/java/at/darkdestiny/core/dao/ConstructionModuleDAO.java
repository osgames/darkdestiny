/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ConstructionModule;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ConstructionModuleDAO extends ReadOnlyTable<ConstructionModule> implements GenericDAO {
    private ConstructionModuleDAO() {
    }

    public ArrayList<ConstructionModule> findByConstruction(int constructionId) {
        ConstructionModule cm = new ConstructionModule();
        cm.setConstructionId(constructionId);

        return find(cm);
    }
}
