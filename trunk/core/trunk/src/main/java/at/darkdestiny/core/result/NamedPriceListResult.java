/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class NamedPriceListResult extends BaseResult {
    private ArrayList<Integer> idList = new ArrayList<Integer>();
    private HashMap<Integer,String> idNameMap = new HashMap<Integer,String>();

    public NamedPriceListResult(String message, boolean error) {
        super(message,error);
    }

    public NamedPriceListResult() {
        super(false);
    }

    public void addNewEntry(int id, String name) {
        idList.add(id);
        idNameMap.put(id,name);
    }

    public ArrayList<Integer> getSortedList() {
        return idList;
    }

    public String getNameFor(int id) {
        return idNameMap.get(id);
    }
}
