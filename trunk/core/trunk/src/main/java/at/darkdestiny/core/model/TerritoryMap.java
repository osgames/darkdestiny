/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.ETerritoryMapType;
import at.darkdestiny.core.enumeration.ETerritoryType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "territorymap")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TerritoryMap extends Model<TerritoryMap> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @IndexAnnotation(indexName = "refId")
    @FieldMappingAnnotation("refId")
    private Integer refId;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("description")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String description;
    @FieldMappingAnnotation("type")
    private ETerritoryMapType type;
    @FieldMappingAnnotation("territoryType")
    private ETerritoryType territoryType;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the type
     */
    public ETerritoryMapType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ETerritoryMapType type) {
        this.type = type;
    }

    /**
     * @return the territoryType
     */
    public ETerritoryType getTerritoryType() {
        return territoryType;
    }

    /**
     * @param territoryType the territoryType to set
     */
    public void setTerritoryType(ETerritoryType territoryType) {
        this.territoryType = territoryType;
    }
}
