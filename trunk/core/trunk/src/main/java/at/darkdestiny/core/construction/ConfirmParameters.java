package at.darkdestiny.core.construction;

public interface ConfirmParameters {
    public final static String PLANETID = "planetId";
    public final static String BUILDINGID = "buildingId";
    public final static String ORDERID = "orderId";
    public final static String COUNT = "count";
    public static final String SYSTEMID = "systemId";
    public static final String FROMSYSTEMID = "fromSystemId";
    public static final String ISSYSTEMID = "isSystemId";
    public static final String USERID = "userId";    
    public final static String FLEETID = "fleetId";
    public final static String DESIGNID = "designId";
    public final static String NEWDESIGNID = "newDesignId";   
    public final static String CREDITS = "credits";
    public final static String IRON = "iron";
    public final static String STEEL = "steel";
    public final static String TERKONIT = "terkonit";
    public final static String YNKELONIUM = "ynkelonium";
    public final static String HOWALGONIUM = "howalgonium";
    public final static String CVEMBINIUM = "cvembinium";
    public final static String MODULEPOINTS = "modulPoints";
    public final static String WORKSPACE = "workSpace";
    public final static String COST = "cost";
    public final static String NAME = "name";
}
