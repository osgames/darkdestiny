/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "userrelation")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class UserRelation extends Model<UserRelation> { 
    @FieldMappingAnnotation("userId1")
    @IdFieldAnnotation    
    private Integer userId1;
    @FieldMappingAnnotation("userId2")
    @IdFieldAnnotation    
    private Integer userId2;
    @FieldMappingAnnotation("relationValue")
    private Float relationValue;

    /**
     * @return the userId1
     */
    public Integer getUserId1() {
        return userId1;
    }

    /**
     * @param userId1 the userId1 to set
     */
    public void setUserId1(Integer userId1) {
        this.userId1 = userId1;
    }

    /**
     * @return the userId2
     */
    public Integer getUserId2() {
        return userId2;
    }

    /**
     * @param userId2 the userId2 to set
     */
    public void setUserId2(Integer userId2) {
        this.userId2 = userId2;
    }

    /**
     * @return the relationValue
     */
    public Float getRelationValue() {
        return relationValue;
    }

    /**
     * @param relationValue the relationValue to set
     */
    public void setRelationValue(Float relationValue) {
        this.relationValue = relationValue;
    }
}
