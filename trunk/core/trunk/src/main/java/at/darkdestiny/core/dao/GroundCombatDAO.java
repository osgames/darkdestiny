/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.ECombatStatus;
import at.darkdestiny.core.model.GroundCombat;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class GroundCombatDAO extends ReadWriteTable<GroundCombat> implements GenericDAO {

    public ArrayList<GroundCombat> findByStatus(ECombatStatus combatStatus) {
        GroundCombat gc = new GroundCombat();
        gc.setCombatStatus(combatStatus);
        return (find(gc));
    }

    public GroundCombat findById(Integer id) {
        GroundCombat gc = new GroundCombat();
        gc.setId(id);
        return (GroundCombat)get(gc);
    }
    public GroundCombat findBy(int planetId, ECombatStatus combatStatus) {
        GroundCombat gc = new GroundCombat();
        gc.setPlanetId(planetId);
        gc.setCombatStatus(combatStatus);

        ArrayList<GroundCombat> gcs = find(gc);
        if (gcs.isEmpty()) {
            return null;
        } else if (gcs.size() == 1) {
            return gcs.get(0);
        } else if (gcs.size() > 1) {
            DebugBuffer.addLine(DebugLevel.ERROR, "There seem to be more active battles than 1 on planet : " + planetId);

            return gcs.get(0);
        }
        return null;
    }

    public ArrayList<GroundCombat> findRunningCombats() {
        ArrayList<GroundCombat> result = new ArrayList<GroundCombat>();
        for(GroundCombat gc : (ArrayList<GroundCombat>)findAll()){
            if(!gc.getCombatStatus().equals(ECombatStatus.FINISHED)){
                result.add(gc);
            }
        }
        return result;
    }
    public GroundCombat findRunningCombat(int planetId) {
        GroundCombat gc = new GroundCombat();
        gc.setPlanetId(planetId);

        for(GroundCombat gcTmp : (ArrayList<GroundCombat>)find(gc)){
            if(!gcTmp.getCombatStatus().equals(ECombatStatus.FINISHED)){
                return gcTmp;
            }
        }
        return null;
    }
    public ArrayList<GroundCombat> findAllSorted() {

        ArrayList<GroundCombat> result = new ArrayList<GroundCombat>();
        TreeMap<Long, ArrayList<GroundCombat>> sorted = new TreeMap<Long, ArrayList<GroundCombat>>();


        for(GroundCombat gc : (ArrayList<GroundCombat>)findAll()){
            ArrayList<GroundCombat> gcs = sorted.get(gc.getStartTime());
            if(gcs == null){
                gcs = new ArrayList<GroundCombat>();
            }
            gcs.add(gc);
            sorted.put(gc.getStartTime(), gcs);
        }
       for(ArrayList<GroundCombat> entry : sorted.descendingMap().values()){
           result.addAll(entry);
       }
        return result;
    }
}
