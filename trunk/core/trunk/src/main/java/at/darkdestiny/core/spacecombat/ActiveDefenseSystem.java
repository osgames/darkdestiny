/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
public class ActiveDefenseSystem extends Model<ActiveDefenseSystem> implements ICombatParticipent {
    private final Integer id;
    private final Integer userId;
    private final String name;
    
    public ActiveDefenseSystem(Integer id, Integer userId, String name) {
        this.id = id;
        this.userId = userId;
        this.name = name;
    }
    
    public Integer getId() {
        return id;
    }
    
    public Integer getUserId() {
        return userId;
    }
    
    public String getName() {
        return name;
    }
}
