/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Maps;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;

/**
 *
 * @author Stefan
 */
public class FleetRenderer implements IModifyImageFunction {

    private int width;
    private int height;
    private final int userId;
    private final StarMapDataSource dataSource;
    private final AbsoluteCoordinate topLeft;
    private final AbsoluteCoordinate bottomRight;
    private final StarMapViewCoordinate smvc;
    private final String imageLocation;
    
    private static Image fleetOutline = null;
    private static Image fleetOutlineMirror = null;
    private static Image fleetBase = null;
    private static Image fleetBaseMirror = null;    
    
    private static HashMap<Integer,BufferedImage> fleetColouredByStatus = Maps.newHashMap();
    private static HashMap<Integer,BufferedImage> fleetOutlineColoured = Maps.newHashMap();
    private static HashMap<Integer,BufferedImage> fleetColouredByStatus_Mirror = Maps.newHashMap();
    private static HashMap<Integer,BufferedImage> fleetOutlineColoured_Mirror = Maps.newHashMap();    

    private static HashMap<Integer,Color> statusColorCodes = Maps.newHashMap();
    
    public FleetRenderer(int userId, StarMapViewCoordinate smvc, IMapDataSource imds, String imageLocation) {
        this.userId = userId;
        this.smvc = smvc;
        this.topLeft = smvc.getTopLeft();
        this.bottomRight = smvc.getBottomRight();
        this.dataSource = (StarMapDataSource) imds;
        this.imageLocation = imageLocation;                   
        
        if (statusColorCodes.isEmpty()) {
            statusColorCodes.put(1,Color.GREEN);
            statusColorCodes.put(2,new Color(23,0,193));
            statusColorCodes.put(3,Color.RED);
            statusColorCodes.put(10,new Color(23,0,193));
            statusColorCodes.put(11,new Color(120,188,255));
            statusColorCodes.put(12,new Color(120,188,255));
            statusColorCodes.put(13,new Color(255,140,0));
            statusColorCodes.put(14,new Color(251,255,0));
            statusColorCodes.put(15,new Color(251,255,0));
            statusColorCodes.put(16,Color.RED);
            statusColorCodes.put(17,Color.RED);
        }
    }

    @Override
    public void run(Graphics graphics) {
        Graphics2D g2d = (Graphics2D)graphics;
        
        try {            
            if (fleetOutline == null) fleetOutline = ImageIO.read(new File(imageLocation + "Fleet_Outline.png")); 
            if (fleetOutlineMirror == null) fleetOutlineMirror = ImageIO.read(new File(imageLocation + "Fleet_Outline_Mirror.png"));
            if (fleetBase == null) fleetBase = ImageIO.read(new File(imageLocation + "Fleet.png"));
            if (fleetBaseMirror == null) fleetBaseMirror = ImageIO.read(new File(imageLocation + "Fleet_Mirror.png"));                                   
        } catch (Exception e) {
            DebugBuffer.error("Failed to load image");
        }
        
        createColoredFleetImages();
        
        for (SMFleet currFleet : dataSource.getFleets()) {
            if ((currFleet.getX() > topLeft.getX()) && 
                (currFleet.getX() < bottomRight.getX()) && 
                (currFleet.getY() > topLeft.getY()) &&
                (currFleet.getY() < bottomRight.getY())) {
                System.out.println("Process Fleet " + currFleet.getId());
                                                                          
                if (!currFleet.isMoving()) {
                    System.out.println("RENDER FLEET TAB " + currFleet.getId());
                    switch (currFleet.getStatus()) {
                        case(1):
                            renderFleetTab(g2d,ESMFleetType.OWN,false,false,currFleet.getX(),currFleet.getY(),currFleet);
                            break;
                        case(2):
                        case(10):
                        case(11):
                            renderFleetTab(g2d,ESMFleetType.ALLIED,false,false,currFleet.getX(),currFleet.getY(),currFleet);
                            break;       
                        default:
                            renderFleetTab(g2d,ESMFleetType.OTHER,false,false,currFleet.getX(),currFleet.getY(),currFleet);
                            break;                                
                    }
                } else {
                    renderFleet(g2d,ESMFleetType.OWN,false,false,currFleet);
                }                
            }
        }
    }

    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public String createMapEntry() {
        return "";
    }

    private void createColoredFleetImages() {        
        // fleetColouredByStatus
        // fleetOutlineColoured
        if (!(fleetColouredByStatus.isEmpty() && fleetOutlineColoured.isEmpty())) return;
        
        for (Map.Entry<Integer,Color> colorEntry : statusColorCodes.entrySet()) {
            fleetColouredByStatus.put(colorEntry.getKey(), DrawingUtilities.getColorizedImage(0, fleetBase, colorEntry.getValue(), DrawingUtilities.FLEET_PIC));
            fleetOutlineColoured.put(colorEntry.getKey(), DrawingUtilities.getColorizedImage(0, fleetOutline, colorEntry.getValue(), DrawingUtilities.FLEET_OUTLINE_PIC));
            fleetColouredByStatus_Mirror.put(colorEntry.getKey(), DrawingUtilities.getColorizedImage(0, fleetBaseMirror, colorEntry.getValue(), DrawingUtilities.FLEET_PIC));
            fleetOutlineColoured_Mirror.put(colorEntry.getKey(), DrawingUtilities.getColorizedImage(0, fleetOutlineMirror, colorEntry.getValue(), DrawingUtilities.FLEET_OUTLINE_PIC));            
        } 
    }
    
    private void renderFleet(Graphics2D g2d, ESMFleetType fleetType, boolean formation, boolean formationOwner, SMFleet fleet) {
        BufferedImage fleetImage = null;

        if (fleet.getTargetX() < fleet.getX()) {           
            fleetImage = fleetColouredByStatus.get(fleet.getStatus());
        } else {
            fleetImage = fleetColouredByStatus_Mirror.get(fleet.getStatus());
        }
        
        int imgHeight = fleetImage.getHeight(null);
        int imgWidth = fleetImage.getWidth(null);       
        
        // scale
        imgHeight = (int)Math.ceil(AreaMapContainer.getZoomImageAdjust(smvc.getZoom()) * imgHeight);
        imgWidth = (int)Math.ceil(AreaMapContainer.getZoomImageAdjust(smvc.getZoom()) * imgWidth);
        
        int drawX = (int)Math.round((fleet.getX() - topLeft.getX()) * smvc.getZoom());
        int drawY = (int)Math.round((fleet.getY() - topLeft.getY()) * smvc.getZoom());      
        
        Fleet f = new Fleet(fleet.getId(),drawX,drawY);
        f.setName(fleet.getName());
        f.setOwner(fleet.getOwner());
        f.setPayload(fleet);
        AreaMapContainer.addToObjectMap(userId, drawX, drawY, f);             
        
        int drawX2 = (int)Math.round((fleet.getTargetX() - topLeft.getX()) * smvc.getZoom());
        int drawY2 = (int)Math.round((fleet.getTargetY() - topLeft.getY()) * smvc.getZoom());   
        
        g2d.setColor(statusColorCodes.get(fleet.getStatus()));
        g2d.drawLine(drawX, drawY, drawX2, drawY2);
        g2d.drawImage(fleetImage, drawX - (int)Math.floor(imgWidth / 2d), drawY - (int)Math.floor(imgHeight / 2d), imgWidth, imgHeight, null);
    }
    
    private void renderFleetTab(Graphics2D g2d, ESMFleetType fleetType, boolean formation, boolean formationOwner, int xCoord, int yCoord, SMFleet fleet) {
        float zoom = 1f;

        int imgHeight = fleetBase.getHeight(null);
        int imgWidth = fleetBase.getWidth(null);
        
        double radius = 30;
        int circleRadius = 20;
        
        // scale
        imgHeight = (int)Math.ceil(AreaMapContainer.getZoomImageAdjust(smvc.getZoom()) * imgHeight);
        imgWidth = (int)Math.ceil(AreaMapContainer.getZoomImageAdjust(smvc.getZoom()) * imgWidth);
        radius = (int)Math.ceil(AreaMapContainer.getZoomImageAdjust(smvc.getZoom()) * radius);
        circleRadius = (int)Math.ceil(AreaMapContainer.getZoomImageAdjust(smvc.getZoom()) * circleRadius);        
        
        if (fleetType == ESMFleetType.OWN) {
            Polygon poly = new Polygon();

            int[] point1 = new int[2];
            int[] point2 = new int[2];
            int[] point3 = new int[2];
            int[] circleCenter = new int[2];
            
            int drawX = (int)Math.round((xCoord - topLeft.getX()) * smvc.getZoom());
            int drawY = (int)Math.round((yCoord - topLeft.getY()) * smvc.getZoom());            

            Fleet f = new Fleet(fleet.getId(),drawX,drawY);
            f.setName(fleet.getName());
            f.setOwner(fleet.getOwner());
            f.setPayload(fleet);
            AreaMapContainer.addToObjectMap(userId, drawX, drawY, f);             
            
            int x = drawX;
            int y = drawY;

            System.out.println("Draw at " + x + "/" + y);
            
            point1[0] = x;
            point1[1] = y;
            poly.addPoint(point1[0], point1[1]);

            point2[0] = (int)Math.floor(x + Math.sin(Math.toRadians(330)) * radius);
            point2[1] = (int)Math.floor(y - Math.cos(Math.toRadians(330)) * radius);
            poly.addPoint(point2[0], point2[1]);
            
            point3[0] = (int)Math.floor(x + Math.sin(Math.toRadians(30)) * radius);
            point3[1] = (int)Math.floor(y - Math.cos(Math.toRadians(30)) * radius);       
            poly.addPoint(point3[0], point3[1]);
            
            g2d.setStroke(new BasicStroke(2));
            g2d.setColor(Color.GREEN);
            g2d.fillPolygon(poly);
            g2d.setColor(Color.BLACK);          
            g2d.drawPolygon(poly);
            
            circleCenter[0] = (int)Math.floor(x + Math.sin(Math.toRadians(0)) * radius);
            circleCenter[1] = (int)Math.floor(y - Math.cos(Math.toRadians(0)) * radius);
            
            g2d.setColor(Color.GREEN);
            g2d.fillOval(circleCenter[0] - circleRadius, circleCenter[1] - circleRadius, circleRadius * 2, circleRadius * 2);
            g2d.setColor(Color.BLACK);  
            g2d.drawOval(circleCenter[0] - circleRadius, circleCenter[1] - circleRadius, circleRadius * 2, circleRadius * 2);
            g2d.setStroke(new BasicStroke(1));
            
            // Draw fleet Image                                    
            g2d.drawImage(fleetBase, circleCenter[0] - (int)Math.floor(imgWidth / 2d), circleCenter[1] - (int)Math.floor(imgHeight / 2d), imgWidth, imgHeight, null);            
        } else if (fleetType == ESMFleetType.ALLIED) {
            Polygon poly = new Polygon();

            int[] point1 = new int[2];
            int[] point2 = new int[2];
            int[] point3 = new int[2];
            int[] circleCenter = new int[2];
            
            int drawX = (int)Math.round((xCoord - topLeft.getX()) * smvc.getZoom());
            int drawY = (int)Math.round((yCoord - topLeft.getY()) * smvc.getZoom());            
            Fleet f = new Fleet(fleet.getId(),drawX,drawY);
            f.setName(fleet.getName());
            f.setOwner(fleet.getOwner());
            f.setPayload(fleet);
            AreaMapContainer.addToObjectMap(userId, drawX, drawY, f);              
            
            int x = drawX;
            int y = drawY;

            System.out.println("Draw at " + x + "/" + y);
            
            point1[0] = x;
            point1[1] = y;
            poly.addPoint(point1[0], point1[1]);

            point2[0] = (int)Math.floor(x + Math.sin(Math.toRadians(90)) * radius);
            point2[1] = (int)Math.floor(y - Math.cos(Math.toRadians(90)) * radius);
            poly.addPoint(point2[0], point2[1]);
            
            point3[0] = (int)Math.floor(x + Math.sin(Math.toRadians(150)) * radius);
            point3[1] = (int)Math.floor(y - Math.cos(Math.toRadians(150)) * radius);       
            poly.addPoint(point3[0], point3[1]);
            
            g2d.setStroke(new BasicStroke(2));
            g2d.setColor(Color.BLUE);
            g2d.fillPolygon(poly);
            g2d.setColor(Color.BLACK);          
            g2d.drawPolygon(poly);
            
            circleCenter[0] = (int)Math.floor(x + Math.sin(Math.toRadians(120)) * radius);
            circleCenter[1] = (int)Math.floor(y - Math.cos(Math.toRadians(120)) * radius);
            
            g2d.setColor(Color.BLUE);
            g2d.fillOval(circleCenter[0] - circleRadius, circleCenter[1] - circleRadius, circleRadius * 2, circleRadius * 2);
            g2d.setColor(Color.BLACK);  
            g2d.drawOval(circleCenter[0] - circleRadius, circleCenter[1] - circleRadius, circleRadius * 2, circleRadius * 2);
            g2d.setStroke(new BasicStroke(1));
            
            // Draw fleet Image                                    
            g2d.drawImage(fleetBase, circleCenter[0] - (int)Math.floor(imgWidth / 2d), circleCenter[1] - (int)Math.floor(imgHeight / 2d), imgWidth, imgHeight, null);  
        } else if (fleetType == ESMFleetType.OTHER) {
            Polygon poly = new Polygon();

            int[] point1 = new int[2];
            int[] point2 = new int[2];
            int[] point3 = new int[2];
            int[] circleCenter = new int[2];
            
            int drawX = (int)Math.round((xCoord - topLeft.getX()) * smvc.getZoom());
            int drawY = (int)Math.round((yCoord - topLeft.getY()) * smvc.getZoom());            
            Fleet f = new Fleet(fleet.getId(),drawX,drawY);
            f.setName(fleet.getName());
            f.setOwner(fleet.getOwner());
            f.setPayload(fleet);
            AreaMapContainer.addToObjectMap(userId, drawX, drawY, f);  
            
            int x = drawX;
            int y = drawY;

            System.out.println("Draw at " + x + "/" + y);
            
            point1[0] = x;
            point1[1] = y;
            poly.addPoint(point1[0], point1[1]);

            point2[0] = (int)Math.floor(x + Math.sin(Math.toRadians(210)) * radius);
            point2[1] = (int)Math.floor(y - Math.cos(Math.toRadians(210)) * radius);
            poly.addPoint(point2[0], point2[1]);
            
            point3[0] = (int)Math.floor(x + Math.sin(Math.toRadians(270)) * radius);
            point3[1] = (int)Math.floor(y - Math.cos(Math.toRadians(270)) * radius);       
            poly.addPoint(point3[0], point3[1]);
            
            g2d.setStroke(new BasicStroke(2));
            g2d.setColor(statusColorCodes.get(fleet.getStatus()));
            g2d.fillPolygon(poly);
            g2d.setColor(Color.BLACK);          
            g2d.drawPolygon(poly);
            
            circleCenter[0] = (int)Math.floor(x + Math.sin(Math.toRadians(240)) * radius);
            circleCenter[1] = (int)Math.floor(y - Math.cos(Math.toRadians(240)) * radius);
            
            g2d.setColor(statusColorCodes.get(fleet.getStatus()));
            g2d.fillOval(circleCenter[0] - circleRadius, circleCenter[1] - circleRadius, circleRadius * 2, circleRadius * 2);
            g2d.setColor(Color.BLACK);  
            g2d.drawOval(circleCenter[0] - circleRadius, circleCenter[1] - circleRadius, circleRadius * 2, circleRadius * 2);
            g2d.setStroke(new BasicStroke(1));
            
            // Draw fleet Image                        
            g2d.drawImage(fleetBase, circleCenter[0] - (int)Math.floor(imgWidth / 2d), circleCenter[1] - (int)Math.floor(imgHeight / 2d), imgWidth, imgHeight, null);  
        }
    }

    /*
    private void renderCircleAndFleet(Graphics2D g2d, ESMFleetType fleetType, boolean formation, boolean formationOwner, int xCoord, int yCoord) {
        String fleetImage;

        // TODO RENDERING
        int xCoord2 = 0;
        int yCoord2 = 0;

        float zoom = 1f;
        
        float offsetX = 0f;
        float offsetY = 0f;
        
        // if target X < source X = mirror
        boolean mirror = false;
        
        String imgOutline = "Fleet_Outline";
        if (mirror) {
            imgOutline += "_Mirr";
        }

        imgOutline += ".png";
        Image imageOutline = DisplayManagement.getGraphic(imgOutline);
        BufferedImage bufferedImageSource = DisplayManagement.toBufferedImage(imageOutline, imgOutline);
        BufferedImage copyOfImage
                = new BufferedImage(bufferedImageSource.getWidth(), bufferedImageSource.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D cloneBuffer = (Graphics2D) copyOfImage.getGraphics();
        cloneBuffer.drawImage(bufferedImageSource, 0, 0, painter.getPanel());

        if (this.timeRemaining <= 0) {
            Point p = new Point(id, "fleet : " + id, this.getX1(), this.getY1());
            if (!formation) {
                if (((PainterData) painter.getPainterData()).fleetsDrawnContains(p, status)) {
                    return;
                } else {
                    ((PainterData) painter.getPainterData()).addFleetDrawn(p, status);
                }
            }
        }
        if (status == ESMFleetType.OWN || status == 10) {
            g2d.setColor(Color.BLUE);
            fleetImage = AppletConstants.FLEET_IMAGE_ALLIED;

            if (mirror) {
                fleetImage += "_Mirr";
                imgOutline += "_Mirr";
            }

            createColorizeOp(AppletConstants.FLEET_COLOR_ALLIED).filter(copyOfImage, copyOfImage);
            fleetImage += ".gif";
            // !Stationary
            Image img = DisplayManagement.getGraphic(fleetImage);
            int wx = getZFactorAdjust(MAXIMG_WIDTH * zoom);
            int wy = getZFactorAdjust(MAXIMG_HEIGHT * zoom);

            int width = wx;
            int height = wy;

            this.offsetX = getZFactorAdjust(16f * zoom);
            this.offsetY = getZFactorAdjust(14f * zoom, 14);

            if (this.timeRemaining != 0) {

                g2d.drawLine((int) (xCoord),
                        (int) (yCoord),
                        (int) (xCoord2),
                        (int) (yCoord2));

                if (formation) {
                    g2d.drawImage(copyOfImage, (xCoord - width / 2), (yCoord - height / 2), width, height, null);
                }

                g2d.drawImage(img, (xCoord - width / 2), (yCoord - height / 2), width, height, null);

            } else {

                // Inner Ring
                g2d.setColor(Color.BLUE);
                g2d.fillOval((xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height);

                if (formation) {
                    addFormation(getEndSystemId(), g2d.getColor());
                }

                // Draw diagonal Balken if fleet formation under other player control but you participate
                if (formation && !formationOwner) {
                    int middleX = ((xCoord - width / 2) + getOffsetX() - 2) + (width + 4) / 2;
                    int middleY = ((yCoord - height / 2) + getOffsetY() - 2) + (height + 4) / 2;

                    g2d.setColor(AppletConstants.FLEET_COLOR_OWN);

                    int[] xBar = new int[4];
                    int[] yBar = new int[4];

                    xBar[0] = middleX + (int) (-0.5d * ((width + 4d) / 2d));
                    xBar[1] = middleX + (int) (-(Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));
                    xBar[2] = middleX + (int) (0.5d * ((width + 4d) / 2d));
                    xBar[3] = middleX + (int) ((Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));

                    yBar[0] = middleY + (int) (-(Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[1] = middleY + (int) (-0.5d * ((height + 4d) / 2d));
                    yBar[2] = middleY + (int) ((Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[3] = middleY + (int) (0.5d * ((height + 4d) / 2d));

                    g2d.fillPolygon(xBar, yBar, 4);
                }

                // Outer Ring
                g2d.setColor(Color.BLACK);
                if (formation || drawFormationStyle(getEndSystemId(), Color.BLUE)) {
                    g2d.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
                }

                Ellipse2D e = new Ellipse2D.Float((xCoord - width / 2) + getOffsetX() - 2, (yCoord - height / 2) + this.getOffsetY() - 2, width + 4, height + 4);
                Ellipse2D e2 = new Ellipse2D.Float((xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height);

                Area shapeOuterRing = new Area(e);
                Area Middle = new Area(e2);

                shapeOuterRing.subtract(Middle);
                g2d.fill(shapeOuterRing);

                g2d.drawImage(img, (xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height, null);

            }

        } else if (status == ESMFleetType.OWN) {
            g2d.setColor(Color.GREEN);

            fleetImage = AppletConstants.FLEET_IMAGE_OWN;
            if (mirror) {
                fleetImage += "_Mirr";
            }
            fleetImage += ".gif";
            createColorizeOp(AppletConstants.FLEET_COLOR_OWN).filter(copyOfImage, copyOfImage);
            // !Stationary
            Image img = DisplayManagement.getGraphic(fleetImage);
            int wx = getZFactorAdjust(MAXIMG_WIDTH * zoom);
            int wy = getZFactorAdjust(MAXIMG_HEIGHT * zoom);

            int width = wx;
            int height = wy;

            this.offsetX = getZFactorAdjust(0f * zoom);
            this.offsetY = -getZFactorAdjust(30f * zoom);

            if (this.timeRemaining != 0) {

                g2d.drawLine((int) (xCoord),
                        (int) (yCoord),
                        (int) (xCoord2),
                        (int) (yCoord2));
                if (formation) {
                    g2d.drawImage(copyOfImage, (xCoord - width / 2), (yCoord - height / 2), width, height, null);
                }

                g2d.drawImage(img, (xCoord - width / 2), (yCoord - height / 2), width, height, null);
            } else {

                g2d.setColor(Color.BLACK);
                if (formation) {
                    g2d.setColor(Color.GREEN);
                }

                // Inner Ring
                g2d.setColor(Color.GREEN);
                g2d.fillOval((xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height);

                if (formation) {
                    addFormation(getEndSystemId(), g2d.getColor());
                }

                // Draw diagonal Balken if fleet formation under other player control but you participate
                if (formation && !formationOwner) {
                    int middleX = ((xCoord - width / 2) + getOffsetX() - 2) + (width + 4) / 2;
                    int middleY = ((yCoord - height / 2) + getOffsetY() - 2) + (height + 4) / 2;

                    g2d.setColor(Color.GREEN);

                    int[] xBar = new int[4];
                    int[] yBar = new int[4];

                    xBar[0] = middleX + (int) (-0.5d * ((width + 4d) / 2d));
                    xBar[1] = middleX + (int) (-(Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));
                    xBar[2] = middleX + (int) (0.5d * ((width + 4d) / 2d));
                    xBar[3] = middleX + (int) ((Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));

                    yBar[0] = middleY + (int) (-(Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[1] = middleY + (int) (-0.5d * ((height + 4d) / 2d));
                    yBar[2] = middleY + (int) ((Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[3] = middleY + (int) (0.5d * ((height + 4d) / 2d));

                    g2d.fillPolygon(xBar, yBar, 4);
                }

                // Outer Ring
                g2d.setColor(Color.BLACK);
                if (formation || drawFormationStyle(getEndSystemId(), Color.GREEN)) {
                    g2d.setColor(Color.YELLOW);
                }

                Ellipse2D e = new Ellipse2D.Float((xCoord - width / 2) + getOffsetX() - 2, (yCoord - height / 2) + this.getOffsetY() - 2, width + 4, height + 4);
                Ellipse2D e2 = new Ellipse2D.Float((xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height);

                Area shapeOuterRing = new Area(e);
                Area Middle = new Area(e2);

                shapeOuterRing.subtract(Middle);
                g2d.fill(shapeOuterRing);

                g2d.drawImage(img, (xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height, null);

            }
        } else {

            g2d.setColor(Color.RED);
            fleetImage = AppletConstants.FLEET_IMAGE_ENEMY;

            if (mirror) {
                fleetImage += "_Mirr";
            }
            fleetImage += ".gif";
            // !Stationary
            Image img = DisplayManagement.getGraphic(fleetImage);
            int wx = getZFactorAdjust(MAXIMG_WIDTH * zoom);
            int wy = getZFactorAdjust(MAXIMG_HEIGHT * zoom);

            int width = wx;
            int height = wy;

            this.offsetX = -getZFactorAdjust(28f * zoom, 28);
            this.offsetY = getZFactorAdjust(19f * zoom, 19);

            if (this.timeRemaining != 0) {

                if (status == 3) {
                    g2d.setColor(Color.PINK);
                    createColorizeOp(Color.PINK).filter(copyOfImage, copyOfImage);

                } else {
                    Color c = Color.decode(((AppletData) appletData).getRelations().get(status).getColor());
                    createColorizeOp(c).filter(copyOfImage, copyOfImage);

                    g2d.setColor(c);
                }

                g2d.drawLine((int) (xCoord),
                        (int) (yCoord),
                        (int) (xCoord2),
                        (int) (yCoord2));
                if (formation) {
                    g2d.drawImage(copyOfImage, (xCoord - width / 2), (yCoord - height / 2), width, height, null);

                }
                g2d.drawImage(img, (xCoord - width / 2), (yCoord - height / 2), width, height, null);
            } else {
                Color c = Color.decode(((AppletData) appletData).getRelations().get(status).getColor());
                Color relColor;

                if (status == 3) {
                    g2d.setColor(Color.PINK);
                } else {
                    g2d.setColor(c);
                }

                relColor = g2d.getColor();

                if (formation) {
                    addFormation(getEndSystemId(), g2d.getColor());
                }

                g2d.setColor(Color.BLACK);
                if (formation) {
                    g2d.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
                }

                // Inner Ring
                g2d.setColor(c);
                g2d.fillOval((xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height);

                // Draw diagonal Balken if fleet formation under other player control but you participate
                if (formation && participating) {
                    int middleX = ((xCoord - width / 2) + getOffsetX() - 2) + (width + 4) / 2;
                    int middleY = ((yCoord - height / 2) + getOffsetY() - 2) + (height + 4) / 2;

                    g2d.setColor(AppletConstants.FLEET_COLOR_OWN);

                    int[] xBar = new int[4];
                    int[] yBar = new int[4];

                    xBar[0] = middleX + (int) (-0.5d * ((width + 4d) / 2d));
                    xBar[1] = middleX + (int) (-(Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));
                    xBar[2] = middleX + (int) (0.5d * ((width + 4d) / 2d));
                    xBar[3] = middleX + (int) ((Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));

                    yBar[0] = middleY + (int) (-(Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[1] = middleY + (int) (-0.5d * ((height + 4d) / 2d));
                    yBar[2] = middleY + (int) ((Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[3] = middleY + (int) (0.5d * ((height + 4d) / 2d));

                    g2d.fillPolygon(xBar, yBar, 4);
                }

                // Outer Ring
                g2d.setColor(Color.BLACK);

                if (formation || drawFormationStyle(getEndSystemId(), relColor)) {
                    g2d.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
                }

                Ellipse2D e = new Ellipse2D.Float((xCoord - width / 2) + getOffsetX() - 2, (yCoord - height / 2) + this.getOffsetY() - 2, width + 4, height + 4);
                Ellipse2D e2 = new Ellipse2D.Float((xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height);

                Area shapeOuterRing = new Area(e);
                Area Middle = new Area(e2);

                shapeOuterRing.subtract(Middle);
                g2d.fill(shapeOuterRing);

                g2d.drawImage(img, (xCoord - width / 2) + getOffsetX(), (yCoord - height / 2) + getOffsetY(), width, height, null);

            }
        }
    }
    */
    
    private static double[] getPoint(double angle, double radius) {
        double rads = angle * Math.PI / 180.00;
        double Px = Math.cos(rads) * radius;
        double Py = Math.sin(rads) * radius;
        double[] point = new double[2];
        point[0] = Px;
        point[1] = Py;
        return point;
    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
            // TODO RENDERING
        } else if (inValue > 10) {
            return 10;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }
}
