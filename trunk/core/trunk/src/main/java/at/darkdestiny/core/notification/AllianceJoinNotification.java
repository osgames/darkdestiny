/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.notification;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Bullet
 */
public class AllianceJoinNotification extends Notification {

    public static AllianceDAO allianceDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    boolean accepted;
    int allianceId;

    public AllianceJoinNotification(int userId, int allianceId, boolean accepted) {
        super(userId);
        this.accepted = accepted;
        this.allianceId = allianceId;
        generateMessage();
        persistMessage();
    }

    public void generateMessage() {
        String topic = "";
        String msg = "";
        Alliance a = allianceDAO.findById(allianceId);
        topic += a.getName() + " | ";
        topic += ML.getMLStr("notification_msg_joinrequest", userId) + " ";
        if (accepted) {
            topic += ML.getMLStr("notification_msg_accepted", userId) + " ";
            msg += ML.getMLStr("notification_msg_alliancejoinaccepted", userId);            
        } else {
            topic += ML.getMLStr("notification_msg_declined", userId) + " ";
            msg += ML.getMLStr("notification_msg_alliancejoindeclined", userId);
        }
        msg = msg.replace("%1", a.getName());
        gm.setMessageContentType(EMessageContentType.DIPLOMACY);
        gm.setTopic(topic);
        gm.setMsg(msg);

    }

}
