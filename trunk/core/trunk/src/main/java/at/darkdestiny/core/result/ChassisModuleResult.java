/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.ships.Armor;
import at.darkdestiny.core.ships.Engine;
import at.darkdestiny.core.ships.Reactor;
import at.darkdestiny.core.ships.Shield;
import at.darkdestiny.core.ships.ShipChassis;
import at.darkdestiny.core.ships.Weapon;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ChassisModuleResult {
    private ArrayList<ShipChassis> chassis;
    private ArrayList<Engine> engines;
    private ArrayList<Armor> armors;
    // private ArrayList<ComputerSystem> computerSystems;    
    private ArrayList<Weapon> weapons;
    private ArrayList<Shield> shields;
    private ArrayList<Reactor> reactors;
}
