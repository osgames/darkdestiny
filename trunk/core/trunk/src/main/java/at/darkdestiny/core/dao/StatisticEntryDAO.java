/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.StatisticEntry;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class StatisticEntryDAO  extends ReadWriteTable<StatisticEntry>  implements GenericDAO{

    public StatisticEntry findById(int id){
        StatisticEntry se = new StatisticEntry();
        se.setId(id);
        return (StatisticEntry)get(se);
    }

    public ArrayList<StatisticEntry>findByCategoryId(int categoryId){
        StatisticEntry se = new StatisticEntry();
        se.setCategoryId(categoryId);
        return find(se);
    }
}
