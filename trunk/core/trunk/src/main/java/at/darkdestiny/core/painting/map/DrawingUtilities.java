/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.painting.map;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.LookupOp;
import java.awt.image.LookupTable;
import java.awt.image.PixelGrabber;
import java.awt.image.ShortLookupTable;
import javax.swing.ImageIcon;

/**
 *
 * @author Stefan
 */
public class DrawingUtilities {
    public final static int FLEET_PIC = 1;
    public final static int FLEET_OUTLINE_PIC = 2;
    
    // private static HashMap<String, BufferedImage> imageBuffer = new HashMap<String, BufferedImage>();

    /*
    public static boolean checkIfContained(String imageName) {
        return imageBuffer.containsKey(imageName);
    }
    */
    
    public static BufferedImage toBufferedImage(Image image) {
        // image = new ImageIcon(image).getImage();
        return toBufferedImage(image, image.getWidth(null), image.getHeight(null));
    }

    public static BufferedImage toBufferedImage(Image image, int width, int height) {
        /*
        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        }
        */
        
        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(image, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;        
        
        /*
        // This code ensures that all the pixels in the image are loaded
        image = new ImageIcon(image).getImage();

        // Determine if the image has transparent pixels
        boolean hasAlpha = hasAlpha(image);

        // Create a buffered image with a format that's compatible with the screen
        BufferedImage bimage = null;

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

        try {

            // Determine the type of transparency of the new buffered image
            int transparency = Transparency.OPAQUE;

            if (hasAlpha == true) {
                transparency = Transparency.BITMASK;
            }

            // Create the buffered image
            GraphicsDevice gs = ge.getDefaultScreenDevice();

            GraphicsConfiguration gc = gs.getDefaultConfiguration();

            bimage = gc.createCompatibleImage(width, height, transparency);

        } catch (HeadlessException e) {
        } //No screen

        if (bimage == null) {

            // Create a buffered image using the default color model
            int type = BufferedImage.TYPE_INT_RGB;

            if (hasAlpha == true) {
                type = BufferedImage.TYPE_INT_ARGB;
            }

            bimage = new BufferedImage(width, height, type);

        }

        // Copy image to buffered image
        Graphics g = bimage.createGraphics();

        // Paint the image onto the buffered image
        g.drawImage(image, 0, 0, null);

        g.dispose();

        // imageBuffer.remove(imageName);
        // imageBuffer.put(imageName, bimage);
        return bimage;
        */
    }

    public static boolean hasAlpha(Image image) {
        // If buffered image, the color model is readily available
        if (image instanceof BufferedImage) {
            return ((BufferedImage) image).getColorModel().hasAlpha();
        }

        // Use a pixel grabber to retrieve the image's color model;
        // grabbing a single pixel is usually sufficient
        PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);

        try {
            pg.grabPixels();
        } catch (InterruptedException e) {
        }

        // Get the image's color model
        return pg.getColorModel().hasAlpha();
    }
    
    public static LookupOp createColorizeOpOutline(Color newColor) {
        short[] alpha = new short[256];
        short[] red = new short[256];
        short[] green = new short[256];
        short[] blue = new short[256];

        for (short i = 0; i < 256; i++) {
            alpha[i] = i;
            red[i] = i;
            green[i] = i;
            blue[i] = i;
        }

        red[201] = (short) newColor.getRed();
        green[201] = (short) newColor.getGreen();
        blue[201] = (short) newColor.getBlue();     

        short[][] data = new short[][]{
            red, green, blue, alpha
        };
        LookupTable lookupTable = new ShortLookupTable(0, data);
        return new LookupOp(lookupTable, null);
    }    
    
    public static LookupOp createColorizeOpBase(Color newColor) {
        short[] alpha = new short[256];
        short[] red = new short[256];
        short[] green = new short[256];
        short[] blue = new short[256];

        for (short i = 0; i < 256; i++) {                      
            alpha[i] = i;
            red[i] = i;
            green[i] = i;
            blue[i] = i;
        }

        red[200] = (short) newColor.getRed();
        green[200] = (short) newColor.getGreen();
        blue[200] = (short) newColor.getBlue();        

        short[][] data = new short[][]{
            red, green, blue, alpha
        };
        LookupTable lookupTable = new ShortLookupTable(0, data);
        return new LookupOp(lookupTable, null);
    }        
    
    public static BufferedImage getColorizedImage(int status, Image img, Color color, int type) {
        BufferedImage src = toBufferedImage(img);
        // System.out.println(src.getColorModel());
        BufferedImage target = new BufferedImage(src.getWidth(),src.getHeight(),BufferedImage.TYPE_INT_ARGB);
        
        if (type == FLEET_PIC) {
            createColorizeOpBase(color).filter(src,target);
        } else if (type == FLEET_OUTLINE_PIC) {
            createColorizeOpOutline(color).filter(src,target);
        } else {
            System.out.println("WRONG OPTION: " + type);
        }
        
        System.out.println("SRC: " + src + " TARGET: " + target);
        
        return target;
    }
}
