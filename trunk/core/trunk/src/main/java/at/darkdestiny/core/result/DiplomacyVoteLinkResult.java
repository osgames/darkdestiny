/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.construction.ConstructionAbortRefund;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;

/**
 *
 * @author Stefan
 */
public class DiplomacyVoteLinkResult extends BaseResult {
    private boolean displayVoteIcon = false;
    private boolean allowedToVote = false;
    private int messageId = 0;
    
    public DiplomacyVoteLinkResult(boolean displayVoteIcon, boolean allowedToVote, int messageId) {
        super(false);

        this.displayVoteIcon = displayVoteIcon;
        this.allowedToVote = allowedToVote;
        this.messageId = messageId;
    }

    public DiplomacyVoteLinkResult(String error) {
        super(error,true);
    }    

    /**
     * @return the displayVoteIcon
     */
    public boolean isDisplayVoteIcon() {
        return displayVoteIcon;
    }

    /**
     * @return the allowedToVote
     */
    public boolean isAllowedToVote() {
        return allowedToVote;
    }

    /**
     * @return the messageId
     */
    public int getMessageId() {
        return messageId;
    }
}
