package at.darkdestiny.core.xml;

public class XMLMalFormatedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5397096001743027958L;

	public XMLMalFormatedException(XMLTokenizer tok, String message) {
		super("Error at "+tok.getPosition()+": "+message);
	}
}
