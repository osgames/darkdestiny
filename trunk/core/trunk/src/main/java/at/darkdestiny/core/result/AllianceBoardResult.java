/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceBoard;
import at.darkdestiny.core.model.AllianceBoardPermission;
import at.darkdestiny.core.model.AllianceMessage;
import at.darkdestiny.core.model.User;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class AllianceBoardResult {
    private TreeMap<Integer, AllianceBoard> boards = new TreeMap<Integer, AllianceBoard>();
    private TreeMap<Integer, Integer> unreadBoardMsgCount = new TreeMap<Integer, Integer>();
    private ArrayList<AllianceMessage> messages = new ArrayList<AllianceMessage>();
    private ArrayList<AllianceBoardPermission> permissions = new ArrayList<AllianceBoardPermission>();
    private ArrayList<AllianceBoardPermission> boardPermissions = new ArrayList<AllianceBoardPermission>();
    private ArrayList<User> allowedUsers = new ArrayList<User>();
    private ArrayList<Alliance> allowedAlliances = new ArrayList<Alliance>();
    private AllianceBoardPermission userPermission;
    
    public void addPermissions(ArrayList<AllianceBoardPermission> perms, int userId){
        
        getPermissions().addAll(perms);
    }
    public void setUnreadMessageCount(int boardId, int count){
        getUnreadBoardMsgCount().put(boardId, count);
    }
    public void addBoard(int boardId, AllianceBoard board){
        boards.put(boardId, board);
    }

    /**
     * @return the boards
     */
    public TreeMap<Integer, AllianceBoard> getBoards() {
        return boards;
    }

    /**
     * @return the unreadBoardMsgCount
     */
    public TreeMap<Integer, Integer> getUnreadBoardMsgCount() {
        return unreadBoardMsgCount;
    }

    /**
     * @return the messages
     */
    public ArrayList<AllianceMessage> getMessages() {
        return messages;
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(ArrayList<AllianceMessage> messages) {
        this.messages = messages;
    }

    /**
     * @return the permissions
     */
    public ArrayList<AllianceBoardPermission> getPermissions() {
        return permissions;
    }

    /**
     * @return the allowedUsers
     */
    public ArrayList<User> getAllowedUsers() {
        return allowedUsers;
    }

    /**
     * @param allowedUsers the allowedUsers to set
     */
    public void setAllowedUsers(ArrayList<User> allowedUsers) {
        this.allowedUsers = allowedUsers;
    }

    /**
     * @return the allowedAlliances
     */
    public ArrayList<Alliance> getAllowedAlliances() {
        return allowedAlliances;
    }

    /**
     * @param allowedAlliances the allowedAlliances to set
     */
    public void setAllowedAlliances(ArrayList<Alliance> allowedAlliances) {
        this.allowedAlliances = allowedAlliances;
    }

    /**
     * @return the userPermission
     */
    public AllianceBoardPermission getUserPermission() {
        return userPermission;
    }

    /**
     * @param userPermission the userPermission to set
     */
    public void setUserPermission(AllianceBoardPermission userPermission) {
        this.userPermission = userPermission;
    }

    /**
     * @return the boardPermissions
     */
    public ArrayList<AllianceBoardPermission> getBoardPermissions() {
        return boardPermissions;
    }

    /**
     * @param boardPermissions the boardPermissions to set
     */
    public void setBoardPermissions(ArrayList<AllianceBoardPermission> boardPermissions) {
        this.boardPermissions = boardPermissions;
    }


}
