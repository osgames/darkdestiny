/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

 import at.darkdestiny.core.dao.PlanetDAO;import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.framework.dao.DAOFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;

/**
 *
 * @author Stefan
 */
public class PlanetService {

    private static final Logger log = LoggerFactory.getLogger(PlanetService.class);

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO)DAOFactory.get(PlanetDAO.class);

    public static int interpolationStepsTotal = 0;

    public static synchronized PlayerPlanet getPlanetName(int planetId) {
        return ppDAO.findByPlanetId(planetId);
    }

    public static long getStockRessourceOnPlanet(int planetId, int ressId) {
        PlanetRessource pr = prDAO.findBy(planetId, ressId, EPlanetRessourceType.INSTORAGE);

        if (pr == null) {
            return 0l;
        }
        return pr.getQty();
    }

    public static Planet getPlanet(int planetId) {
        return pDAO.findById(planetId);
    }

    public static PlayerPlanet getPlayerPlanet(int planetId) {
        return ppDAO.findByPlanetId(planetId);
    }

    public static long getFreePopulation(int planetId) {
        long result = 0;

        // log.debug("Get Planet " + planetId);

        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        Planet p = pDAO.findById(pp.getPlanetId());

        String habitable = "CMG";
        boolean habitablePlanet = habitable.contains((p.getLandType()));

        int totalAP = pp.getPopActionPoints();
        long actPopulation = pp.getPopulation();

        int usedAP = 0;
        PlanetCalculation pc = OverviewService.PlanetCalculation(planetId);
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        usedAP = epcr.getUsedPopulation() + epcr.getInConsUsedPopulation();

        if (usedAP == 0) return pp.getPopulation();

        // Start interpolating population till a optimal population number is found, where free AP = 0
        long interpolationStep = 0;
        int exp = 0;
        double pop = pp.getPopulation();

        while (pop >= 10) {
            pop /= 10d;
            exp++;
        }

        interpolationStep = (long)Math.pow(10,exp);
        int currentFreeAP = totalAP - usedAP;

        double initialAmplifier = 2.5d;
        double currInterpolationAmplifier = initialAmplifier;

        // if (pp.getPlanetId() == 1060) DebugBuffer.warning("ACTPOP IS " + actPopulation);

        if (planetId == 15414) log.debug("Curr Free AP: " + currentFreeAP);

        if (currentFreeAP < 0) { // We start from a hypothectical population number and see how low we can go
            actPopulation = (long)2 * (long)Math.pow(10d, 10d);

            while (interpolationStep > 1) {
                interpolationStepsTotal++;
                long currStep = (long)(interpolationStep * currInterpolationAmplifier);

                actPopulation -= currStep;

                int tmpFreeAP = 0;
                // if (habitablePlanet) {
                    // tmpFreeAP = pp.getPopActionPoints() - usedAP;
                tmpFreeAP = pp.getPopActionPointsSimulated(actPopulation);
                    if (planetId == 15414) log.debug("[AP < 0] Temp Free AP: " + tmpFreeAP + " actPopulation: " + actPopulation);
                // } else {
                //     tmpFreeAP = PlayerPlanet.getPopActionPointsInhabitable(actPopulation) - usedAP;
                // }

                if ((tmpFreeAP < 0) || (actPopulation < 0)) {
                    actPopulation += currStep;

                    interpolationStep /= 10d;
                    currInterpolationAmplifier = initialAmplifier;
                } else {
                    currInterpolationAmplifier += 0.1d;
                }

                // if (pp.getPlanetId() == 1060) DebugBuffer.warning("[A] ACT POP for " + pp.getName() + " is " + actPopulation + " (FreeAP: "+tmpFreeAP+" IP-Step: " + interpolationStep + ")");
            }
        } else {
            while (interpolationStep > 1) {
                interpolationStepsTotal++;
                long currStep = (long)(interpolationStep * currInterpolationAmplifier);

                actPopulation -= currStep;
                int tmpFreeAP = 0;
                // tmpFreeAP = pp.getPopActionPoints() - usedAP;
                tmpFreeAP = pp.getPopActionPointsSimulated(actPopulation) - usedAP;

                if (planetId == 15414) log.debug("[AP > 0] Temp Free AP: " + tmpFreeAP + " actPopulation: " + actPopulation);

                /*
                if (habitablePlanet) {

                } else {
                    tmpFreeAP = PlayerPlanet.getPopActionPointsInhabitable(actPopulation) - usedAP;
                }
                */
                if ((tmpFreeAP < 0) || (actPopulation < 0)) {
                    actPopulation += currStep;

                    interpolationStep /= 10d;
                    currInterpolationAmplifier = initialAmplifier;
                } else {
                    currInterpolationAmplifier += 0.1d;
                }

                // if (pp.getPlanetId() == 1060) DebugBuffer.warning("[B] ACT POP for " + pp.getName() + " is " + actPopulation + " (FreeAP: "+tmpFreeAP+" IP-Step: " + interpolationStep + ")");
            }
        }

        if (currentFreeAP < 0) {
            result = pp.getPopulation() * -1l;
            if (planetId == 15414) log.debug("[A] MAX TO REMOVE: " + result);
        } else {
            result = pp.getPopulation() - actPopulation;
            if (planetId == 15414) log.debug("[B] MAX TO REMOVE: " + result);
        }

        return result;
    }

    public static int getHomePlanetForUser(int userId) {
        PlayerPlanet pp = ppDAO.findHomePlanetByUserId(userId);
        return pp.getPlanetId();
    }
}
