/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.construction.restriction;

import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.enumeration.ERestrictionReason;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.ConstructionCheckResult;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class RPlanetaryHUEShield implements ConstructionInterface  {

    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();
    public ConstructionCheckResult onConstructionCheck(PlayerPlanet pp) throws Exception {

        PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);
        if(pcDAO.isConstructed(pp.getPlanetId(), Construction.ID_PLANETARY_PARATRONSHIELD)){
            errMsg.put("Planetarer Paratronschirm bereits vorhanden", ERestrictionReason.OUTDATED);
            return new ConstructionCheckResult(false,0);
        }
        return new ConstructionCheckResult(true,1);
    }

    public void onConstruction(PlayerPlanet pp) throws Exception {
    }

    public void onFinishing(PlayerPlanet pp) throws Exception {
    }

    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    public void onDeconstruction(PlayerPlanet pp) throws Exception {
    }

    public void onDestruction(PlayerPlanet pp) throws Exception {
    }

    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }

    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);
        if(pcDAO.isConstructed(pp.getPlanetId(), Construction.ID_PLANETARY_PARATRONSHIELD)){
            errMsg.put("Planetarer Paratronschirm bereits vorhanden", ERestrictionReason.DEACTIVATED);
            return true;
        }
        return false;
    }
}
