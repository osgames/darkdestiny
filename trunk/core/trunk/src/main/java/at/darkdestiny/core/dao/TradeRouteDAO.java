/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TradeRouteDAO extends ReadWriteTable<TradeRoute> implements GenericDAO {

    public TradeRoute getById(int id) {
        TradeRoute tr = new TradeRoute();
        tr.setId(id);
        return (TradeRoute) get(tr);
    }
    public ArrayList<TradeRoute> findByStartPlanet(Integer startPlanet) {
        TradeRoute tr = new TradeRoute();
        tr.setStartPlanet(startPlanet);
        return find(tr);
    }

    public ArrayList<TradeRoute> findByStartPlanet(Integer startPlanet, ETradeRouteType type) {
        TradeRoute tr = new TradeRoute();
        tr.setStartPlanet(startPlanet);
        tr.setType(type);
        return find(tr);
    }

    public ArrayList<TradeRoute> findAllByType(ETradeRouteType type) {
        TradeRoute tr = new TradeRoute();
        tr.setType(type);
        return find(tr);
    }

    public boolean exists(Integer startPlanet, Integer endPlanet) {
        TradeRoute tr = new TradeRoute();
        tr.setStartPlanet(startPlanet);
        tr.setTargetPlanet(endPlanet);
        ArrayList<TradeRoute> result = find(tr);
        if (result.size() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public boolean exists(Integer startPlanet, Integer endPlanet, ETradeRouteType type) {
        TradeRoute tr = new TradeRoute();
        tr.setStartPlanet(startPlanet);
        tr.setTargetPlanet(endPlanet);
        tr.setType(type);
        ArrayList<TradeRoute> result = find(tr);
        if (result.size() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<TradeRoute> findByEndPlanet(Integer endPlanet) {
        TradeRoute tr = new TradeRoute();
        tr.setTargetPlanet(endPlanet);
        return find(tr);
    }


    public ArrayList<TradeRoute> findByEndPlanet(Integer endPlanet, ETradeRouteType type) {
        TradeRoute tr = new TradeRoute();
        tr.setTargetPlanet(endPlanet);
        tr.setType(type);
        return find(tr);
    }

    public ArrayList<TradeRoute> findByUserIdInternal(Integer userId, ETradeRouteType type) {
        TradeRoute tr = new TradeRoute();
        tr.setUserId(userId);
        tr.setTargetUserId(0);
        tr.setType(type);
        return find(tr);
    }
    public ArrayList<TradeRoute> findByUserIdInternal(Integer userId, ETradeRouteType type, int planetId) {
        ArrayList<TradeRoute> result = new ArrayList<TradeRoute>();
        for (TradeRoute tr : (ArrayList<TradeRoute>) findAll()) {
            if (((tr.getUserId().equals(userId)) && (!tr.getTargetUserId().equals(userId))
                    && tr.getTargetUserId() == 0)
                    || (tr.getTargetUserId().equals(userId))) {
                if (tr.getType().equals(type) && tr.getTargetPlanet() == planetId || tr.getStartPlanet() == planetId) {
                    result.add(tr);
                }
            }

        }
        return result;
    }

    public ArrayList<TradeRoute> findByUserId(int userId, ETradeRouteType type) {
        TradeRoute tr = new TradeRoute();
        tr.setUserId(userId);
        tr.setType(type);
        return find(tr);
    }

    public ArrayList<TradeRoute> findByTargetUserId(int targetUserId, ETradeRouteType type) {
        TradeRoute tr = new TradeRoute();
        tr.setTargetUserId(targetUserId);
        tr.setType(type);
        return find(tr);
    }

    public ArrayList<TradeRoute> findByUserId(int userId) {
        TradeRoute tr = new TradeRoute();
        tr.setUserId(userId);
        return find(tr);
    }

    public TradeRoute findBy(int startPlanet, int targetPlanet) {
        TradeRoute tr = new TradeRoute();
        tr.setStartPlanet(startPlanet);
        tr.setTargetPlanet(targetPlanet);
        ArrayList<TradeRoute> result = find(tr);
        if (result.size() == 1) {
            return result.get(0);
        } else if (result.size() > 2) {
            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error in TradeRouteDAO, 2 entries found");
        } else {
            return null;
        }
        return null;
    }
    public TradeRoute findBy(int startPlanet, int targetPlanet, ETradeRouteType type) {
        TradeRoute tr = new TradeRoute();
        tr.setStartPlanet(startPlanet);
        tr.setTargetPlanet(targetPlanet);
        tr.setType(type);
        ArrayList<TradeRoute> result = find(tr);
        if (result.size() == 1) {
            return result.get(0);
        } else if (result.size() > 2) {
            DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error in TradeRouteDAO, 2 entries found");
        } else {
            return null;
        }
        return null;
    }

    public ArrayList<TradeRoute> findByUserIdExternal(int userId, ETradeRouteType type, int planetId) {
        ArrayList<TradeRoute> result = new ArrayList<TradeRoute>();
        for (TradeRoute tr : (ArrayList<TradeRoute>) findAll()) {
            if (((tr.getUserId() == userId) && (tr.getTargetUserId() != userId)
                    && tr.getTargetUserId() > 0)
                    || (tr.getTargetUserId() == userId)) {
                if (tr.getType().equals(type) && (tr.getTargetPlanet() == planetId || tr.getStartPlanet() == planetId)) {
                    result.add(tr);
                }
            }

        }
        return result;
    }

    public ArrayList<TradeRoute> findByUserIdExternal(int userId, ETradeRouteType type) {
        ArrayList<TradeRoute> result = new ArrayList<TradeRoute>();
        for (TradeRoute tr : (ArrayList<TradeRoute>) findAll()) {
            if (((tr.getUserId() == userId) && (tr.getTargetUserId() != userId)
                    && tr.getTargetUserId() > 0)
                    || (tr.getTargetUserId() == userId)) {
                if (tr.getType().equals(type)) {
                    result.add(tr);
                }
            }

        }
        return result;
    }
}
