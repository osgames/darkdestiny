/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import at.darkdestiny.core.enumeration.ETerritoryMapShareType;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class TerritorySaveResult {

        private HashMap<ETerritoryMapShareType, ArrayList<Integer>> permissions;
        private HashMap<ETerritoryMapShareType, ArrayList<Integer>> newPermissions;

        public TerritorySaveResult() {
            permissions = new HashMap<ETerritoryMapShareType, ArrayList<Integer>>();
            newPermissions = new HashMap<ETerritoryMapShareType, ArrayList<Integer>>();
        }

        public void addPermission(ETerritoryMapShareType type, Integer id) {

            ArrayList<Integer> perm = getPermissions().get(type);

            if(perm == null){                
                perm = new ArrayList<Integer>();
            }
            if (perm == null || !perm.contains(id)) {
                perm.add(id);
                getPermissions().put(type, perm);
            }
        }

        public void addNewPermission(ETerritoryMapShareType type, Integer id) {

            ArrayList<Integer> perm = getNewPermissions().get(type);

            if(perm == null){                
                perm = new ArrayList<Integer>();
            }
            if (!perm.contains(id)) {
                perm.add(id);
                getNewPermissions().put(type, perm);
            }
        }

        /**
         * @return the permissions
         */
        public HashMap<ETerritoryMapShareType, ArrayList<Integer>> getPermissions() {
            return permissions;
        }

        /**
         * @return the newPermissions
         */
        public HashMap<ETerritoryMapShareType, ArrayList<Integer>> getNewPermissions() {
            return newPermissions;
        }
    }

