/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.FilterToValue;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class FilterToValueDAO extends ReadWriteTable<FilterToValue> implements GenericDAO {

    public ArrayList<FilterToValue> findByFilterId(int filterId){
        FilterToValue searchftv = new FilterToValue();
        searchftv.setFilterId(filterId);
        return find(searchftv);
    }
    public FilterToValue findBy(int filterId, int valueId){
        FilterToValue searchftv = new FilterToValue();
        searchftv.setFilterId(filterId);
        searchftv.setValueId(valueId);
        return (FilterToValue)get(searchftv);
    }

}
