/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.model.Filter;
import at.darkdestiny.core.model.FilterToValue;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.FilterResult;
import at.darkdestiny.core.utilities.FilterUtilities;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Aion
 */
public class FilterService extends Service {

    public static FilterResult addValue(int userId, Map<String, String[]> allPars) {

        FilterResult result = new FilterResult();

        try {
            int filterId = Integer.parseInt(allPars.get("filterId")[0]);

            Filter f = filterDAO.findById(filterId);
            if (f.getUserId() != userId) {
                result.setResult(new BaseResult("Dieser Filter wurde nicht von ihnen erstellt", true));
                return result;
            }
            int valueId = Integer.parseInt(allPars.get("filterValueId")[0]);
            String comparator = allPars.get("comparator")[0];
            String value = allPars.get("value")[0];

            FilterToValue ftv = new FilterToValue();
            ftv.setFilterId(filterId);
            ftv.setValueId(valueId);
            ftv.setValue(value);
            ftv.setComparator(FilterUtilities.getComparator(comparator));

            if (filterToValueDAO.get(ftv) != null) {
                result.setResult(new BaseResult("This value already exists", true));
                return result;
            }

            filterToValueDAO.add(ftv);

        } catch (Exception e) {
            DebugBuffer.error("Error while Adding filter : " + e);
            result.setResult(new BaseResult("Parameter inkorrekt", true));
            return result;
        }
        result.setResult(new BaseResult("Wert hinzugef�gt", false));
        return result;
    }

    public static FilterResult addFilter(int userId, Map<String, String[]> allPars) {

        FilterResult result = new FilterResult();

        Filter f = new Filter();
        f.setUserId(userId);
        try {
            String description = allPars.get("description")[0];
            String name = allPars.get("name")[0];
            f.setDescription(description);
            f.setName(name);
        } catch (Exception e) {
            DebugBuffer.error("Error while Adding filter : " + e);
            result.setResult(new BaseResult("Parameter inkorrekt", true));
            return result;
        }
        f = filterDAO.add(f);
        result.setId(f.getId());
        result.setResult(new BaseResult("Filter hinzugef�gt", false));
        return result;
    }

    public static BaseResult deleteFilter(int userId, Map<String, String[]> allPars) {

        try {

            int filterId = Integer.parseInt(allPars.get("filterId")[0]);
            Filter f = Service.filterDAO.findById(filterId);
            if (f.getUserId() != userId) {
                return new BaseResult("Dieser Filter geh�rt nicht ihnen", false);
            }
            for (FilterToValue ftv : (ArrayList<FilterToValue>) Service.filterToValueDAO.findByFilterId(filterId)) {
                Service.filterToValueDAO.remove(ftv);
            }
            Service.filterDAO.remove(f);
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error while delete Filter : " + e);
            return new BaseResult("Ung�ltige Parameter", false);
        }

        return new BaseResult("Filter gel�scht", false);
    }

    public static BaseResult deleteValue(int userId, Map<String, String[]> allPars) {
        try {

            int filterId = Integer.parseInt(allPars.get("filterId")[0]);
            int valueId = Integer.parseInt(allPars.get("valueId")[0]);
            Filter f = Service.filterDAO.findById(filterId);
            if (f.getUserId() != userId) {
                return new BaseResult("Dieser Filter geh�rt nicht ihnen", false);
            }
            FilterToValue ftv = Service.filterToValueDAO.findBy(filterId, valueId);
            Service.filterToValueDAO.remove(ftv);
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error while delete Filter Value : " + e);
            return new BaseResult("Ung�ltige Parameter", false);
        }
        return new BaseResult("Wert gel�scht", false);

    }

    public static ArrayList<Filter> findAllFilters() {
        return filterDAO.findAll();
    }
}
