/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ConditionToUser;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ConditionToUserDAO extends ReadWriteTable<ConditionToUser> implements GenericDAO {
    public ConditionToUser findBy(int conditionToTitleId, int userId){
        ConditionToUser ctu = new ConditionToUser();
        ctu.setConditionToTitleId(conditionToTitleId);
        ctu.setUserId(userId);
        return (ConditionToUser)get(ctu);
    }

    public ArrayList<ConditionToUser> findBy(int userId) {
        ConditionToUser ctu = new ConditionToUser();
        ctu.setUserId(userId);

        return find(ctu);
    }
}
