/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view;

import com.google.common.collect.Lists;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class DiplomacyManagementView {
    private ArrayList<DiplomacyManagementViewEntry> playerToPlayer = new ArrayList<DiplomacyManagementViewEntry>();
    private ArrayList<DiplomacyManagementViewEntry> playerToAlliance = new ArrayList<DiplomacyManagementViewEntry>();
    private ArrayList<DiplomacyManagementViewEntry> allianceToPlayer = new ArrayList<DiplomacyManagementViewEntry>();
    private ArrayList<DiplomacyManagementViewEntry> allianceToAlliance = new ArrayList<DiplomacyManagementViewEntry>();

    public void addPlayerToPlayerEntry(DiplomacyManagementViewEntry dmve) {
        getPlayerToPlayer().add(dmve);
    }

    public void addPlayerToAllianceEntry(DiplomacyManagementViewEntry dmve) {
        System.out.println("Add DMVE P2A: " + dmve.getBase().getFromId() + " TO " + dmve.getBase().getToId() + " TYPE: " + dmve.getBase().getType());
        
        getPlayerToAlliance().add(dmve);
    }

    public void addAllianceToPlayerEntry(DiplomacyManagementViewEntry dmve) {
        getAllianceToPlayer().add(dmve);
    }

    public void addAllianceToAllianceEntry(DiplomacyManagementViewEntry dmve) {
        getAllianceToAlliance().add(dmve);
    }

    /**
     * @return the playerToPlayer
     */
    public ArrayList<DiplomacyManagementViewEntry> getPlayerToPlayer() {
        return playerToPlayer;
    }

    /**
     * @return the playerToAlliance
     */
    public ArrayList<DiplomacyManagementViewEntry> getPlayerToAlliance() {
        return playerToAlliance;
    }

    /**
     * @return the allianceToPlayer
     */
    public ArrayList<DiplomacyManagementViewEntry> getAllianceToPlayer() {
        return allianceToPlayer;
    }

    /**
     * @return the allianceToAlliance
     */
    public ArrayList<DiplomacyManagementViewEntry> getAllianceToAlliance() {
        return allianceToAlliance;
    }
    
    public ArrayList<DiplomacyManagementViewEntry> getAll() {
        ArrayList<DiplomacyManagementViewEntry> allEntries = Lists.newArrayList();
        
        allEntries.addAll(playerToPlayer);
        allEntries.addAll(playerToAlliance);
        allEntries.addAll(allianceToPlayer);
        allEntries.addAll(allianceToAlliance);
        
        return allEntries;
    }
}
