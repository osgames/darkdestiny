/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.construction;

import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.ships.BuildTime;

/**
 *
 * @author Stefan
 */
public class ConstructionScrapCost extends MutableRessourcesEntry {
    private final int count;
    private final int planetId;
    private final BuildTime buildTime;
    private final ConstructionExt ce;

    protected ConstructionScrapCost(ConstructionExt ce) {
        this.count = 1;
        this.ce = ce;
        this.planetId = 0;
        this.buildTime = null;

        calcScrapCostWithoutBuildTime();
    }    
    
    public ConstructionScrapCost(ConstructionExt ce, int planetId) {
        this.count = 1;
        this.ce = ce;
        this.planetId = planetId;
        this.buildTime = new BuildTime();

        calcScrapCost();
    }

    public ConstructionScrapCost(ConstructionExt ce, int planetId, int count) {
        this.count = count;
        this.ce = ce;
        this.planetId = planetId;
        this.buildTime = new BuildTime();

        calcScrapCost();
    }

    private void calcScrapCost() {
        RessourcesEntry re = ce.getRessCost();

        this.addRess(re, count);
        this.multiplyRessources(0.7d, 0.1d);

        buildTime.setIndPoints((int)Math.ceil(ConstructionService.getIndustryPoints(planetId) * 0.1d * count));
    }
    
    private void calcScrapCostWithoutBuildTime() {
        RessourcesEntry re = ce.getRessCost();

        this.addRess(re, count);
        this.multiplyRessources(0.7d, 0.1d);        
    }

    public int getCount() {
        return count;
    }

    public BuildTime getBuildTime() {
        return buildTime;
    }
}
