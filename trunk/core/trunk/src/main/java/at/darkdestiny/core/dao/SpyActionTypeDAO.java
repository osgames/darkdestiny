/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.SpyActionType;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class SpyActionTypeDAO extends ReadWriteTable<SpyActionType> implements GenericDAO {
    public SpyActionType findById(int id) {
        SpyActionType spyActionType = new SpyActionType();
        spyActionType.setId(id);
        return (SpyActionType)get(spyActionType);
    }
}
