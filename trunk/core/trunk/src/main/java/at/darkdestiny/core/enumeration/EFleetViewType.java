/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Stefan
 */
public enum EFleetViewType {
    OWN_SYSTEM, OWN_NOT_IN_SYSTEM, OWN_ALL_WITH_ACTION, 
    ALL_SYSTEM, ALL_PLANET, ALL_SYSTEM_OTHER_USERS, ALL_SYSTEM_ALLIED, ALL_SYSTEM_ENEMY,
    OWN_ALL, OWN_ALL_PLANET, DEFENSE_SYSTEM, ALL_DEFENSE, OWN_ALL_TRANSPORT_SYSTEM, 
    OWN_ALL_TRANSPORT_NOT_IN_SYSTEM, ENEMY_NOT_IN_SYSTEM, ALLIED_NOT_IN_SYSTEM;
}
