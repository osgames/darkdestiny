/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

/**
 *
 * @author Stefan
 */
public class Hyperscanner {
    private final at.darkdestiny.core.model.System system;
    private final int scanRange;
    
    public Hyperscanner(at.darkdestiny.core.model.System system, int scanRange) {
        this.system = system;
        this.scanRange = scanRange;
    }

    /**
     * @return the system
     */
    public at.darkdestiny.core.model.System getSystem() {
        return system;
    }

    /**
     * @return the scanRange
     */
    public int getScanRange() {
        return scanRange;
    }
}
