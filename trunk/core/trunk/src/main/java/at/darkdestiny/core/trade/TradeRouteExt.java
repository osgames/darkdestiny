/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.trade;

import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.TradeShipData;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.TradeOfferDAO;
import at.darkdestiny.core.dao.TradePostDAO;
import at.darkdestiny.core.dao.TradeRouteDetailDAO;
import at.darkdestiny.core.dao.TradeRouteShipDAO;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.TradePost;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.model.TradeRouteShip;
import at.darkdestiny.core.update.transport.CapacityOneEdge;
import at.darkdestiny.core.update.transport.RessCapacityOneEdge;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class TradeRouteExt implements Comparable {

    private static final Logger log = LoggerFactory.getLogger(TradeRouteExt.class);


    private final TradeRoute base;
    private static TradeOfferDAO toDAO = (TradeOfferDAO) DAOFactory.get(TradeOfferDAO.class);
    private static TradePostDAO tpDAO = (TradePostDAO) DAOFactory.get(TradePostDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    private static TradeRouteShipDAO trsDAO = (TradeRouteShipDAO) DAOFactory.get(TradeRouteShipDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);

    private ArrayList<TradeRouteDetail> routeDetails = null;
    private HashMap<ShipDesign, Integer> assignedShips = null;
    private int assignedTransmitterCap = 0;
    private boolean addingTransmitterPossible = false;
    private boolean removingTransmitterPossible = false;
    private boolean addingShipsPossible = false;
    private boolean removingShipsPossible = false;

    // ********** NEW CAPACITY SYSTEM **********
    private CapacityOneEdge capSingleEdge = null;
    private HashMap<Integer,RessCapacityOneEdge> ressCapSingleEdge = Maps.newHashMap();
    // *****************************************

    private boolean capInit = false;
    private HashMap<Integer,Integer> additionalCapacityByRes = Maps.newHashMap();

    private final double routeLength;
    private final RelativeCoordinate rc1;
    private final RelativeCoordinate rc2;
    boolean debug = false;

    public TradeRouteExt(TradeRoute tr) {
        this.base = tr;

        rc1 = new RelativeCoordinate(0, tr.getStartPlanet());
        rc2 = new RelativeCoordinate(0, tr.getTargetPlanet());

        routeLength = Math.max(1d, rc1.distanceTo(rc2));
        calcCapacity();
    }

    public int getAdditionalCapacityByRes(int resId) {
        if (!capInit) {
            calcCapacity();
        }

        if (additionalCapacityByRes.containsKey(resId)) {
            return additionalCapacityByRes.get(resId);
        }

        return 0;
    }

    private void calcCapacity() {
        if (!capInit) {
            capInit = true;
            int capacity = 0;

            if (base.getType().equals(ETradeRouteType.TRANSPORT)) {
                for (ShipDesign sd : getAssignedShips().keySet()) {
                    if (debug) {
                        log.debug("Check capacity for ship " + sd);
                        log.debug("Count " + assignedShips.get(sd));
                    }
                    double capTmp = 0d;

                    if (rc1.getSystemId() == rc2.getSystemId()) {
                        capTmp = (sd.getRessSpace() * assignedShips.get(sd)) / 2d;
                    } else {
                        capTmp = (sd.getRessSpace() * assignedShips.get(sd)) / 2d / routeLength * sd.getHyperSpeed();
                    }

                    capacity += capTmp;
                }

                capacity += getAssignedTransmitterCap();                

                // NEW
                // System.out.println("Create Single Cap Entry with capacity: " + capacity);
                setCapSingleEdge(new CapacityOneEdge(capacity));
                // ***

                /*
                PlanetConstruction pcAgrar = pcDAO.findBy(base.getStartPlanet(), Construction.ID_AGRICULTURAL_DISTRIBUTION);
                if (pcAgrar != null) {
                    additionalCapacityByRes.put(Ressource.FOOD, (int)Math.floor((pcAgrar.getLevel() * 1000000) / 2d / routeLength));
                }
                */
            } else if (base.getType().equals((ETradeRouteType.TRADE))) {
                TradePost tp = tpDAO.findById(base.getTradePostId());
                if(tp == null){
                   // return 0;
                }
                PlanetConstruction pc = pcDAO.findBy(tp.getPlanetId(), Construction.ID_INTERSTELLARTRADINGPOST);
                if(pc == null){
                    DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Try to trade from tradepost which has no planetconstruction :(");
                    // return 0;
                }

                int level = pc.getLevel();
                if(level <= 0){
                    level = 1;
                }


                double capBoost = tp.getCapBoost();

                capacity += ((double)tp.getTransportPerLJ() * (double)level * capBoost)  / 2d / routeLength;


                /*
                 * Disabled for Trade until Solution is found
                 * Only excess from normal transport should be available
                PlanetConstruction pcAgrar = pcDAO.findBy(base.getStartPlanet(), Construction.ID_AGRICULTURAL_DISTRIBUTION);
                if (pcAgrar != null) {
                    // NEW
                    int capFood = (int)Math.floor((pcAgrar.getLevel() * 1000000) / 2d / routeLength);
                    ressCapMultiEdge.put(Ressource.FOOD, new RessCapacityMultiEdge(Ressource.FOOD,capFood));
                    // ***

                    additionalCapacityByRes.put(Ressource.FOOD, (int)Math.floor((pc.getLevel() * 1000000) / 2d / routeLength));
                }
                */
            }
        }
    }

    public ArrayList<TradeRouteDetail> getRouteDetails() {
        if (routeDetails == null) {
            routeDetails = trdDAO.getByRouteId(base.getId());
        }

        return routeDetails;
    }

    public void setRouteDetails(ArrayList<TradeRouteDetail> routeDetails) {
        this.routeDetails = routeDetails;
    }

    public HashMap<ShipDesign, Integer> getAssignedShips() {
        if (assignedShips == null) {
            assignedShips = new HashMap<ShipDesign, Integer>();

            ArrayList<TradeShipData> resList = TradeUtilities.getShipsInRoute(base.getId());
            for (TradeShipData sData : resList) {
                if (debug) {
                    log.debug("SD = " + sdDAO.findById(sData.getDesignId()));
                }

                ShipDesign sd = sdDAO.findById(sData.getDesignId());
                if (sd == null) {
                    continue;
                }
                assignedShips.put(sd, sData.getCount());
            }
        }
        if (debug) {
            log.debug("assignedShips Size is " + assignedShips.size());
        }
        return assignedShips;
    }

    public void setAssignedShips(HashMap<ShipDesign, Integer> assignedShips) {
        this.assignedShips = assignedShips;
    }

    public int getAssignedTransmitterCap() {
        TradeRouteShip trs = trsDAO.getTransmitterEntry(base.getId());

        if (trs == null) {
            assignedTransmitterCap = 0;
        } else {
            assignedTransmitterCap = trs.getNumber();
        }

        return assignedTransmitterCap;
    }

    public void setAssignedTransmitterCap(int assignedTransmitterCap) {
        this.assignedTransmitterCap = assignedTransmitterCap;
    }

    public boolean isAddingTransmitterPossible() {
        return addingTransmitterPossible;
    }

    public void setAddingTransmitterPossible(boolean addingTransmitterPossible) {
        this.addingTransmitterPossible = addingTransmitterPossible;
    }

    public boolean isRemovingTransmitterPossible() {
        return removingTransmitterPossible;
    }

    public void setRemovingTransmitterPossible(boolean removingTransmitterPossible) {
        this.removingTransmitterPossible = removingTransmitterPossible;
    }

    public boolean isAddingShipsPossible() {
        return addingShipsPossible;
    }

    public void setAddingShipsPossible(boolean addingShipsPossible) {
        this.addingShipsPossible = addingShipsPossible;
    }

    public boolean isRemovingShipsPossible() {
        return removingShipsPossible;
    }

    public void setRemovingShipsPossible(boolean removingShipsPossible) {
        this.removingShipsPossible = removingShipsPossible;
    }

    public TradeRoute getBase() {
        return base;
    }

    @Override
    public int compareTo(Object o) {
        TradeRouteExt tre = (TradeRouteExt) o;
        return this.getBase().getId().compareTo(tre.getBase().getId());
    }

    public double getRouteLength() {
        return routeLength;
    }

    public int getAssignedShipCount(){
         int shipCount = 0;
                                for(Map.Entry<ShipDesign, Integer> entry : getAssignedShips().entrySet()){
                                    shipCount += entry.getValue();
                                }
                                return shipCount;
    }

    /**
     * @return the capSingleEdge
     */
    public CapacityOneEdge getCapSingleEdge() {
        return capSingleEdge;
    }

    /**
     * @param capSingleEdge the capSingleEdge to set
     */
    public void setCapSingleEdge(CapacityOneEdge capSingleEdge) {
        this.capSingleEdge = capSingleEdge;
    }

    /**
     * @return the ressCapSingleEdge
     */
    public HashMap<Integer,RessCapacityOneEdge> getRessCapSingleEdge() {
        return ressCapSingleEdge;
    }
}
