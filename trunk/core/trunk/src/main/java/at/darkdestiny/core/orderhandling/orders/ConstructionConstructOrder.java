/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.Buildable;

/**
 *
 * @author Stefan
 */
public class ConstructionConstructOrder extends ConstructOrder {
    public ConstructionConstructOrder(Buildable unit, int planetId, int userId, int count) {
        super(unit,planetId,userId,count);
    } 
    public ConstructionConstructOrder(Buildable unit, int planetId, int userId, int count, int priority) {
        super(unit,planetId,userId,count,priority);
    }
}
