package at.darkdestiny.core.admin.db;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author martin
 *
 */
public class BooleanField extends IDatabaseColumn {

	private final int column;

	/**
	 * @param comment 
	 * @param readonly 
	 * @param name 
	 * @param column 
	 * 
	 */
	public BooleanField(int column, String name, boolean readonly, String comment) {
		super(name, comment, readonly);
		this.column = column;
	}

	public Object getFromResultSet(ResultSet rs) 
	        throws SQLException {
		return new Boolean(rs.getInt(column) != 0);
	}

	public String makeEdit(Object object) {
		if (object == null)
			object = false;
		
		return "<input type=\"checkbox\" name=\"f_"+name+"\" "+
			(((Boolean)object).booleanValue()?"checked ":"")+
			(readonly?"readonly ":"")+"/>";
	}

	protected String getValueOnEmpty()
	{
		return "'0'";
	}
}
