/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.construction.ConstructionScrapAbortCost;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;

/**
 *
 * @author Stefan
 */
public class AbortDeconstructionResult extends BaseResult {
    private final ConstructionScrapAbortCost consScrapAbortCost;
    private final Action actionEntry;
    private final Construction cons;

    public AbortDeconstructionResult(Construction cons, Action actionEntry, ConstructionScrapAbortCost consScrapAbortCost) {
        super(false);

        this.consScrapAbortCost = consScrapAbortCost;
        this.actionEntry = actionEntry;
        this.cons = cons;
    }

    public AbortDeconstructionResult(String error) {
        super(error,true);

        this.consScrapAbortCost = null;
        this.actionEntry = null;
        this.cons = null;
    }

    /**
     * @return the consAbortRefund
     */
    public ConstructionScrapAbortCost getConsScrapAbortCost() {
        return consScrapAbortCost;
    }

    /**
     * @return the actionEntry
     */
    public Action getActionEntry() {
        return actionEntry;
    }

    /**
     * @return the cons
     */
    public Construction getCons() {
        return cons;
    }
}
