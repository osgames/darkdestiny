/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.interfaces;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;

/**
 *
 * @author Stefan
 */
public interface ILocation {
    public AbsoluteCoordinate getAbsoluteCoordinate();
}
