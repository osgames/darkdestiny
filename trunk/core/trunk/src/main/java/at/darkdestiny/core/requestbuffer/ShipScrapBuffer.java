/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.requestbuffer;

 import org.slf4j.Logger;import org.slf4j.LoggerFactory;
;

/**
 *
 * @author Stefan
 */
public class ShipScrapBuffer extends ParameterBuffer {

    private static final Logger log = LoggerFactory.getLogger(ShipScrapBuffer.class);

    public static final String FLEET_ID = "fleetId";
    public static final String DESIGN_ID = "sourceId";
    public static final String COUNT = "count";

    public ShipScrapBuffer(int userId) {
        super(userId);
    }

    public int getFleetId() {
        log.debug("Par : " + getParameter(FLEET_ID));
        return (Integer)(getParameter(FLEET_ID));
    }

    public int getDesignId() {
        return (Integer)getParameter(DESIGN_ID);
    }

    public int getCount() {
        return Integer.parseInt((String)getParameter(COUNT));
    }
}
