/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin;

import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class SessionEntry {
    private final HttpSession session;
    private long lastActivity = -1;
    
    public SessionEntry(HttpSession session, long lastActivity) {
        this.session = session;
        this.lastActivity = lastActivity;
    }

    /**
     * @return the session
     */
    public HttpSession getSession() {
        return session;
    }

    /**
     * @return the lastActivity
     */
    public long getLastActivity() {
        return lastActivity;
    }

    /**
     * @param lastActivity the lastActivity to set
     */
    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }
}
