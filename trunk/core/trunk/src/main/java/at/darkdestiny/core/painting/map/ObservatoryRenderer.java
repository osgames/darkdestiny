/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.util.DebugBuffer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import javax.imageio.ImageIO;

/**
 *
 * @author Stefan
 */
public class ObservatoryRenderer implements IModifyImageFunction { 
    private int width;
    private int height;
    private final int userId;
    private final StarMapDataSource dataSource;
    private final AbsoluteCoordinate topLeft;
    private final AbsoluteCoordinate bottomRight;
    private final StarMapViewCoordinate smvc;
    
    private static Image greyStar = null;
    private static Image yellowStar = null;
    private static Image ownStar = null;
    private static Image homeStar = null;
    
    public ObservatoryRenderer(int userId, StarMapViewCoordinate smvc, IMapDataSource imds) {
        this.userId = userId;
        this.smvc = smvc;
        this.topLeft = smvc.getTopLeft();
        this.bottomRight = smvc.getBottomRight();
        this.dataSource = (StarMapDataSource)imds;
    }
    
    @Override
    public void run(Graphics graphics) {
        System.out.println("RENDER OBSERVATORY");
        
        Graphics2D g2d = (Graphics2D)graphics;                
        
        for (SMObservatory currObs : dataSource.getObservatories()) {
            System.out.println("CHECK: " + currObs.getX() + ":" + currObs.getY() + " vs. + [" + (topLeft.getX() - SMObservatory.getRANGE()) + "-" + (bottomRight.getX() + SMObservatory.getRANGE()) + "]:[" + (topLeft.getY() - SMObservatory.getRANGE()) + "-" + (bottomRight.getY() + SMObservatory.getRANGE()) + "]");
            g2d.setColor(Color.WHITE);
            
            if (((currObs.getX() + SMObservatory.getRANGE()) > topLeft.getX()) && 
                ((currObs.getX() - SMObservatory.getRANGE()) < bottomRight.getX()) && 
                ((currObs.getY() + SMObservatory.getRANGE()) > topLeft.getY()) &&
                ((currObs.getY() - SMObservatory.getRANGE()) < bottomRight.getY())) {
                                                
                System.out.println("RENDER OBSERVATORY CIRCLE");
                int drawX = (int)Math.round((currObs.getX() - topLeft.getX()) * smvc.getZoom());
                int drawY = (int)Math.round((currObs.getY() - topLeft.getY()) * smvc.getZoom());                
                int drawRange = Math.round(SMObservatory.getRANGE() * smvc.getZoom());
                
                g2d.fillOval(drawX - drawRange, drawY - drawRange, drawRange * 2, drawRange * 2);
                
                // g2d.fillOval(currSys.getX() - topLeft.getX(), currSys.getY() - topLeft.getY(), 2, 2);
            }
        }
    }    
    
    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }
    
    @Override
    public String createMapEntry() {
        return "";
    }            
}
