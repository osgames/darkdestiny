package at.darkdestiny.core.title;

import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.service.Service;


public class TheCautious extends AbstractTitle {

	public boolean check(int userId) {
        for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(userId)) {
            PlanetConstruction pc = Service.planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETARY_HUESHIELD);
            PlanetConstruction pc1 = Service.planetConstructionDAO.findBy(pp.getPlanetId(), Construction.ID_PLANETARY_PARATRONSHIELD);
            if(pc != null || pc1 != null){
                return true;
            }
        }
        return false;
	}
}
