/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.statistics.designs;

import at.darkdestiny.core.model.StatisticEntry;
import at.darkdestiny.core.statistics.Chart;
import at.darkdestiny.core.statistics.IChart;

/**
 *
 * @author Bullet
 */
public class CruiserChart implements IChart {

    Chart chart;

    public CruiserChart(StatisticEntry se, int userId) {

        ShipDesignChart sdc = new ShipDesignChart(se, 5);
        this.chart = sdc.getChart(userId);
    }

    public Chart getChart() {
        return chart;
    }
}
