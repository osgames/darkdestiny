/*
 * TestConnection.java
 *
 * Created on 17. M&auml;rz 2006, 18:42
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package at.darkdestiny.core.datatransfer;

import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author Stefan
 */
public class TestConnection {
    private static DatagramSocket socket;
    private static DatagramPacket packet;
//    private static HashMap incomingBuffer;
    private static boolean initialized;
    
    /** Creates a new instance of TestConnection */      
    public static void start() {
        if (initialized) return;
        
        try { 
            socket = new DatagramSocket(4135);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Problem: ", e);
        }        
        
        listenForEvent();
    }
    
    private static void listenForEvent() {        
        try { 
            while (true) {            
                // Auf Anfrage warten
                packet = new DatagramPacket( new byte[1024], 1024 );
                socket.receive( packet );
                
                // Empf&auml;nger auslesen
                InetAddress address = packet.getAddress();
                int port = packet.getPort();
                int len = packet.getLength();
                byte data[] = packet.getData();
                DebugBuffer.addLine(DebugLevel.TRACE, "Anfrage von " + address +
                                    " vom Port " + port +
                                    " L&auml;nge " + len +
                                    "\n" + new String( data, 0, len ) );
                Thread.sleep(100);     
                DebugBuffer.addLine(DebugLevel.TRACE,"Waiting for Event");
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while waiting for Event",e);
        }
    }
}
