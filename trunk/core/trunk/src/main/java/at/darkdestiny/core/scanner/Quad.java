/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.scanner;

 import java.util.ArrayList;import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class Quad<T extends AbstractCoordinate> {

    private static final Logger log = LoggerFactory.getLogger(Quad.class);

    public final int x;
    public final int y;
    public final int width;
    public final int height;

    public ArrayList<Quad> subQuads = new ArrayList<Quad>();
    public ArrayList<T> systems = new ArrayList<T>();

    public Quad(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void split(int resStep) {
        if (resStep >= 1) {
            resStep--;

            // log.debug("Splitting Base Quad ("+x+","+y+")->("+width+","+height+")");

            int middleBorderX = (int)Math.floor(width / 2d);
            int middleBorderY = (int)Math.floor(height / 2d);

            // Calculate Quad top-left
            Quad tl = new Quad(x,y,middleBorderX,middleBorderY);
            // log.debug("Create Quad (TL) ("+tl.x+","+tl.y+")->("+tl.width+","+tl.height+")");
            tl.split(resStep);

            // Calculate Quad top-right
            Quad tr = new Quad(middleBorderX+1+x,y,(width - (middleBorderX + 1)),(height - middleBorderY));
            // log.debug("Create Quad (TR) ("+tr.x+","+tr.y+")->("+tr.width+","+tr.height+")");
            tr.split(resStep);

            // Calculate Quad bottom-left
            Quad bl = new Quad(x,middleBorderY+1+y,(width - middleBorderX),(height - (middleBorderY + 1)));
            // log.debug("Create Quad (BL) ("+bl.x+","+bl.y+")->("+bl.width+","+bl.height+")");
            bl.split(resStep);

            // Calculate Quad bottom-right
            Quad br = new Quad(middleBorderX+1+x,middleBorderY+1+y,(width - (middleBorderX + 1)),(height - (middleBorderY + 1)));
            // log.debug("Create Quad (BR) ("+br.x+","+br.y+")->("+br.width+","+br.height+")");
            br.split(resStep);

            subQuads.add(tl);
            subQuads.add(tr);
            subQuads.add(bl);
            subQuads.add(br);
        }
    }

    public void getQuads(ArrayList<Quad> selQuads, QuadTreeCoordinateSet qtcs) {
        for (Quad q : subQuads) {
            // Check if Quad intersects with selection
            if (!((q.x > qtcs.trX) ||
                  ((q.x + q.width) < qtcs.tlX) ||
                  (q.y > qtcs.blY) ||
                  ((q.y + q.height) < qtcs.tlY))) {
                if (q.subQuads.isEmpty()) {
                    selQuads.add(q);
                } else {
                    q.getQuads(selQuads,qtcs);
                }
            }
        }
    }

    public Quad getQuadAt(int x, int y) {
        for (Quad q : subQuads) {
            // log.debug("Scanning Quad ("+q.x+","+q.y+")->("+q.width+","+q.height+")");

            boolean goRight = false;
            boolean goLeft = false;
            boolean goBottom = false;
            boolean goTop = false;

            // Falls x größer go right Quad
            if (x > (q.x + q.width)) goRight = true;

            // Falls x kleiner go left Quad
            if (x < q.x) goLeft = true;

            // Falls y größer go bottom Quad
            if (y > (q.y + q.height)) goBottom = true;

            // Falls y kleiner go top Quad
            if (y < q.y) goTop = true;

            // log.debug("GR: " + goRight + " GL: " + goLeft + " GB: " + goBottom + " GT: " + goTop);

            if (!(goRight || goLeft || goBottom || goTop)) {
                if (q.subQuads.isEmpty()) {
                    return q;
                } else {
                    return q.getQuadAt(x, y);
                }
            }
        }

        return null;
    }

    public boolean addItem(T sd) {
        for (Quad q : subQuads) {
            // log.debug("Scanning Quad ("+q.x+","+q.y+")->("+q.width+","+q.height+")");

            boolean goRight = false;
            boolean goLeft = false;
            boolean goBottom = false;
            boolean goTop = false;

            // Falls x größer go right Quad
            if (sd.x > (q.x + q.width)) goRight = true;

            // Falls x kleiner go left Quad
            if (sd.x < q.x) goLeft = true;

            // Falls y größer go bottom Quad
            if (sd.y > (q.y + q.height)) goBottom = true;

            // Falls y kleiner go top Quad
            if (sd.y < q.y) goTop = true;

            // log.debug("GR: " + goRight + " GL: " + goLeft + " GB: " + goBottom + " GT: " + goTop);

            if (!(goRight || goLeft || goBottom || goTop)) {
                if (q.subQuads.isEmpty()) {
                    q.systems.add(sd);
                    return true;
                } else {
                    return q.addItem(sd);
                }
            }
        }

        return false;
    }

    public int getQuadElements() {
        int elementCount = systems.size();

        for (Quad q : subQuads) {
            elementCount += q.getQuadElements();
        }

        log.debug(elementCount + " Elements in Quad (X: " + x + " Y: " + y + " WIDTH: " + width + " HEIGHT: " + height);
        return elementCount;
    }
}
