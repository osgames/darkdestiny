/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.admin;

import at.darkdestiny.core.CheckForUpdate;
import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.dao.GameDataDAO;
import at.darkdestiny.core.model.GameData;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class AdminService {

    private static GameDataDAO gdDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);

    public static long skipTicks(int userId, int ticks) {
        GameData gd = (GameData) gdDAO.findAll().get(0);

        int tickTime = gd.getTickTime();
        int lastUpdatedTick = gd.getLastUpdatedTick();

        long time = System.currentTimeMillis() - tickTime * ((long) lastUpdatedTick + ticks);
        gd.setStartTime(time);
        gdDAO.update(gd);

        GameConfig.getInstance().rereadValues();
        CheckForUpdate.requestUpdate();

        return lastUpdatedTick;
    }
}
