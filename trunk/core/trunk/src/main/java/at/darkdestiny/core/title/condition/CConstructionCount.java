package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.ConditionToTitleDAO;
import at.darkdestiny.core.dao.ConditionToUserDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;

public class CConstructionCount extends AbstractCondition {

    private ParameterEntry quantityValue;
    private ParameterEntry planetValue;
    private ParameterEntry constructionValue;
    private ParameterEntry comparatorValue;
    private ParameterEntry isHomePlanet;
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

         private static ConditionToTitleDAO ctDAO = (ConditionToTitleDAO) DAOFactory.get(ConditionToTitleDAO.class);
    private static ConditionToUserDAO cuDAO = (ConditionToUserDAO) DAOFactory.get(ConditionToUserDAO.class);

    public CConstructionCount(ParameterEntry constructionValue, ParameterEntry planetValue, ParameterEntry isHomePlanet, ParameterEntry quantityValue, ParameterEntry comparatorValue) {
        this.quantityValue = quantityValue;
        this.planetValue = planetValue;
        this.constructionValue = constructionValue;
        this.comparatorValue = comparatorValue;
        this.isHomePlanet = isHomePlanet;
    }

    public boolean checkCondition(int userId, int conditionToTitleId) {
        int pId = Integer.parseInt(planetValue.getParamValue().getValue());

        if (Integer.parseInt(isHomePlanet.getParamValue().getValue()) == 1) {
            if(ppDAO.findHomePlanetByUserId(userId) == null){
                return false;
            }
            pId = ppDAO.findHomePlanetByUserId(userId).getPlanetId();
        }
        int count = 0;
        if (pId == 0) {
            for (PlayerPlanet pp : ppDAO.findByUserId(userId)) {
                PlanetConstruction pc = pcDAO.findBy(pp.getPlanetId(), Integer.parseInt(constructionValue.getParamValue().getValue()));
                if (pc == null) {
                    continue;
                }
                count += pc.getNumber();
            }
        } else {
            PlanetConstruction pc = pcDAO.findBy(pId, Integer.parseInt(constructionValue.getParamValue().getValue()));
            if (pc == null) {
                return false;
            }
            count = pc.getNumber();

        }
        return compare(quantityValue.getParamValue().getValue(), count, quantityValue.getParam().getType(), ETitleComparator.valueOf(comparatorValue.getParamValue().getValue()));
    }
}
