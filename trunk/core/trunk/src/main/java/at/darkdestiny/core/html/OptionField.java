package at.darkdestiny.core.html;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Create a Drop down box (i.e. a html &lt;select&gt; field)
 * from a map of values. A selected item can also be set.
 * @author martin
 */
public class OptionField {
    private final Writer output;

    private Object selected = null;

    private String clazz;
    private final String fieldname;

    private final List<Entry> entries = new ArrayList<Entry>();

    public OptionField(Writer output_, String fieldname_) {
        this.output = output_;
        fieldname = fieldname_;
    }

    public void setSelected(Object o) {
        selected = o;
    }

    public void addEntry(Object key, String value) {
        // TODO: ensure value contains no characters, which require escaping!
        entries.add(new Entry(key, value));
    }
    public void setClass(String s){
        clazz = s;
    }

    public void output()
            throws IOException {
        // Write the HTML code to the output.
        if(clazz != null){
        output.write(String.format(
                "<SELECT " + "class=\"" + clazz + "\" " + " id=\"%s\" name=\"%s\">\n", fieldname, fieldname
                ));
        }else{            
        output.write(String.format(
                "<SELECT id=\"%s\" name=\"%s\">\n", fieldname, fieldname
                ));
        }
        for (Entry e : entries) {
            output.write(String.format(
                    "<OPTION %s value=\"%s\">%s</OPTION>\n",
                    e.isSelected(selected) ? "SELECTED" : "",
                    (e.key != null) ? e.key.toString() : "", e.value
                    ));
        }
        output.write("</SELECT>\n");
    }

    private final static class Entry {
        private final Object key;

        private final String value;

        public Entry(Object key_, String value_) {
            key = key_;
            value = value_;
        }

        public boolean isSelected(Object selectedKey) {
            if (selectedKey == null) {
                return key == selectedKey;
            }
            return selectedKey.equals(key);
        }
    }
}
