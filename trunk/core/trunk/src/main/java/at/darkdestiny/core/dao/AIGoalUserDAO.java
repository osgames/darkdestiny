/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.AIGoalUser;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class AIGoalUserDAO extends ReadWriteTable<AIGoalUser> implements GenericDAO {

    public ArrayList<AIGoalUser> findByUserId(int id) {
        AIGoalUser aiGoalUser = new AIGoalUser();
        aiGoalUser.setUserId(id);
        return find(aiGoalUser);
    }

}
