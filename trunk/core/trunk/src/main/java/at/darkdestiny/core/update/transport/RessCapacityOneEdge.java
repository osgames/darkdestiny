/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.update.transport;

/**
 *
 * @author Stefan
 */
public class RessCapacityOneEdge extends AbstractCapacity {
    private final int ressId;   
    
    public RessCapacityOneEdge(int ressId, int capacity) {
        super(capacity);
        this.ressId = ressId;
    }    
}
