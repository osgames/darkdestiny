package at.darkdestiny.core.spacecombat;

import java.util.*;
import at.darkdestiny.util.DebugBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.spacecombat.CombatGroupFleet.DesignDescriptor;

public class CombatGroup {

    private static final Logger log = LoggerFactory.getLogger(CombatGroup.class);

    private int id;
    private int fleetCount;
    private int shipCount;
    private int masterAlliance;
    private HashSet<Integer> doAttack = new HashSet<Integer>();

    private List<CombatGroupFleet> fleetList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFleetCount() {
        return fleetCount;
    }

    public void setFleetCount(int fleetCount) {
        this.fleetCount = fleetCount;
    }

    public int getShipCount() {
        return shipCount;
    }

    public void setShipCount(int shipCount) {
        this.shipCount = shipCount;
    }

    public List<CombatGroupFleet> getFleetList() {
        return fleetList;
    }

    public void setFleetList(List<CombatGroupFleet> fleetList) {
        this.fleetList = fleetList;
    }

    public void addFleet(CombatGroupFleet cgf) {
        if (this.fleetList == null) {
            fleetList = new ArrayList<CombatGroupFleet>();
        }
        this.fleetList.add(cgf);
    }

    public int getMasterAlliance() {
        return masterAlliance;
    }

    public void setMasterAlliance(int masterAlliance) {
        this.masterAlliance = masterAlliance;
    }

    public CombatGroup copy() {
        try {
            return (CombatGroup)this.clone();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Cloning of CombatGroup failed!",e);
        }
        return null;
    }

    public boolean isLoser() {
        boolean loser = true;

        for (CombatGroupFleet cgf : fleetList) {
            for (Map.Entry<DesignDescriptor,CombatUnit> ship : cgf.getShipDesign().entrySet()) {
                if (ship.getValue().getSurvivors() > 0) {
                    log.debug("Found survivor " + ship.getKey().getId() + " with count " + ship.getValue().getSurvivors());
                    loser = false;
                }
            }
        }

        return loser;
    }

    public void addToAttack(int userId) {
        doAttack.add(userId);
    }

    public boolean attackAllowed(int userId) {
        return !doAttack.contains(userId);
    }
}
