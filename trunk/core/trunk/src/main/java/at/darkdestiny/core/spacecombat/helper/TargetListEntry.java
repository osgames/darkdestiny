/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat.helper;

 import at.darkdestiny.core.spacecombat.ICombatUnit;import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.spacecombat.ICombatUnit;

/**
 *
 * @author Aion
 */
public class TargetListEntry {

    private static final Logger log = LoggerFactory.getLogger(TargetListEntry.class);


    private ICombatUnit target;
    private double baseAttackProbability;
    private double relativeAttackProbability;
    private double targetFireIndicator;

    /**
     * @return the target
     */
    public ICombatUnit getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(ICombatUnit target) {
        this.target = target;
    }

    /**
     * @return the relativeAttackProbability
     */
    public double getRelativeAttackProbability() {
        return relativeAttackProbability;
    }

    /**
     * @param relativeAttackProbability the relativeAttackProbability to set
     */
    public void setRelativeAttackProbability(double relativeAttackProbability) {
        log.debug(this + " SET RAP: " + relativeAttackProbability);
        this.relativeAttackProbability = relativeAttackProbability;
    }

    /**
     * @return the targetFireIndicator
     */
    public double getTargetFireIndicator() {
        return targetFireIndicator;
    }

    /**
     * @param targetFireIndicator the targetFireIndicator to set
     */
    public void setTargetFireIndicator(double targetFireIndicator) {
        this.targetFireIndicator = targetFireIndicator;
    }

    /**
     * @return the baseAttackProbability
     */
    public double getBaseAttackProbability() {
        return baseAttackProbability;
    }

    /**
     * @param baseAttackProbability the baseAttackProbability to set
     */
    public void setBaseAttackProbability(double baseAttackProbability) {
        this.baseAttackProbability = baseAttackProbability;
    }
}
