/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.fleet.FleetType;
import at.darkdestiny.core.model.FleetOrder;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class FleetOrderDAO extends ReadWriteTable<FleetOrder> implements GenericDAO {
    public FleetOrder get(Integer fleetId, FleetType fleetType) {
        FleetOrder fd = new FleetOrder();
        fd.setFleetId(fleetId);
        fd.setFleetType(fleetType);

        return (FleetOrder)get(fd);
    }
}
