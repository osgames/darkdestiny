/**
 * 
 */
package at.darkdestiny.core.utilities.img.renderer;

import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * Zur Anzeige eines ganzen Systems 
 * als Stern mit einem Kreis und passenden Informationen dazu
 * 
 * @author martin
 */
public class FleetRenderer implements IModifyImageFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5902612864276394112L;
	private final static int IMG_SIZE = 32;
	private final float x;
	private final float y;
	private final float tx;
	private final float ty;
	private final float visibleSize;
	private int width;
	private int height;
	private boolean isOwn;
	private boolean isAllied;
	private final String name;

	/**
	 * 
	 */
	public FleetRenderer(String name, float x, float y, float tx, float ty, float visibleSize, boolean isOwn, boolean isAllied) 
	{
		this.name = name;
		this.tx = tx;
		this.ty = ty;
		this.x = x;
		this.y = y;
		this.visibleSize = visibleSize;
		this.isOwn = isOwn;
		this.isAllied = isAllied;
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.img.IModifyImageFunction#run(java.awt.Graphics)
	 */
	public void run(Graphics graphics) {
			Image img = Toolkit.getDefaultToolkit().createImage(this.getClass().getResource(filenameForStatus(isOwn, isAllied)));
			int i = 0;
			while ((img.getWidth(null) <0) && (i < 10))
				try {
					Thread.sleep(10);
					i++;
				} catch (InterruptedException e) {
				}
				
			graphics.setColor(new Color(0x808080));
			graphics.drawLine(Math.round(x*width/visibleSize) + width/2, Math.round(y*height/visibleSize) + height/2, 
					Math.round(tx*width/visibleSize) + width/2, Math.round(ty*height/visibleSize) + height/2);
			graphics.drawImage(img, Math.round(x*width/visibleSize) + width/2- img.getWidth(null)/2, 
					Math.round(y*height/visibleSize) + height/2- img.getHeight(null)/2, null);
	}

	private String filenameForStatus(boolean isOwn, boolean isAllied) {
		if (isOwn)    return "own.gif";
		if (isAllied) return "all.gif"; 
		return "opp.gif";
	}

	/* (non-Javadoc)
	 * @see at.darkdestiny.core.img.IModifyImageFunction#setSize(int, int)
	 */
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public String createMapEntry() {
            	//int relativeX = (int)Math.round(((double)(x-16) + (double)visibleSize));
		//int relativeY = (int)Math.round(((double)(y-16) + (double)visibleSize));
                
                width = 400;
                height = 400;
            
                int relativeX = Math.round(x*width/visibleSize) + width/2- 32/2;
                int relativeY = Math.round(y*height/visibleSize) + height/2- 17/2;                
                
		// int relativeX = Math.round(x*width/visibleSize) + width/2-IMG_SIZE/2;
		// int relativeY = Math.round(y*height/visibleSize) + height/2-IMG_SIZE/2;
		return "<area shape=\"rect\" coords=\"" + relativeX + ","+ relativeY +","+(relativeX+IMG_SIZE)+","+(relativeY+IMG_SIZE)+"\" onmouseover=\"doTooltip(event,'Flotte "+name+"')\" onmouseout=\"hideTip()\">";
	}

}