/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class MainService extends Service{


    public static void changeLanguage(int userId, String language) {
        User u = userDAO.findById(userId);
        u.setLocale(language);
        ML.removeLocale(userId);
        userDAO.update(u);
    }

    public static ArrayList<User> getAllPlayers() {
        return userDAO.findAllSorted(false);
    }
    
    public static ArrayList<User> getAllUsers() {
        return userDAO.findAllSorted(true);
    }
    
    public static UserData findUserDataByUserId(int userId) {
        return userDataDAO.findByUserId(userId);
    }

    public static User findUserByUserId(int userId) {
        return userDAO.findById(userId);
    }
    
    public static ArrayList<Language> findAllLanguages() {
        return languageDAO.findAll();
    }
    
    public static Planet findPlanetById(int planetId) {
        return planetDAO.findById(planetId);
    }

    public static PlayerPlanet findPlayerPlanetByPlanetId(int planetId){
        return playerPlanetDAO.findByPlanetId(planetId);
    }
}
