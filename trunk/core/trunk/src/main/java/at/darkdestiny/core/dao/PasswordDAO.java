/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EPassword;
import at.darkdestiny.core.model.Password;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Eobane
 */
public class PasswordDAO extends ReadWriteTable<Password> implements GenericDAO {
    public Password findByType(EPassword password) {
        Password p = new Password();
        p.setPassword(password);

        return (Password)get(p);
    }
}
