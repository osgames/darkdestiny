/*
 * MilitaryUtilities.java
 *
 * Created on 03. Dezember 2007, 23:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.core.ModuleType;
import at.darkdestiny.core.dao.ConstructionModuleDAO;
import at.darkdestiny.core.dao.ShipTypeAdjustmentDAO;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.diplomacy.combat.CombatGroupEntry;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResolver;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResult;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.fleet.FleetData;
import at.darkdestiny.core.fleet.LoadingInformation;
import at.darkdestiny.core.military.DefensePlanetEntry;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.DiplomacyType;
import at.darkdestiny.core.model.FleetLoading;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.ships.ShipDesignDetailed;
import at.darkdestiny.core.ships.ShipStorage;
import at.darkdestiny.core.ships.Weapon;
import at.darkdestiny.core.spacecombat.BattleShipNew;
import at.darkdestiny.core.spacecombat.CombatGroupFleet.UnitTypeEnum;
import at.darkdestiny.core.spacecombat.DefenseBuilding;
import at.darkdestiny.core.spacecombat.ICombatUnit;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class MilitaryUtilities extends Service {

    private static final Logger log = LoggerFactory.getLogger(MilitaryUtilities.class);

 private static ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);
 private static ConstructionModuleDAO cmDAO = (ConstructionModuleDAO) DAOFactory.get(ConstructionModuleDAO.class);


    public ArrayList<DefensePlanetEntry> getDefensePlanetEntries(int userId) {
        ArrayList<DefensePlanetEntry> defList = new ArrayList<DefensePlanetEntry>();

        // Get all own planets
        try {
            // Statement stmt = DbConnect.createStatement();
            // PreparedStatement pstmt1 = DbConnect.prepareStatement("SELECT userId FROM playerfleet WHERE planetId=?");
            //  PreparedStatement pstmt2 = DbConnect.prepareStatement("SELECT COUNT(planetId) FROM playertroops WHERE planetId=?");
            //   PreparedStatement pstmt3 = DbConnect.prepareStatement("SELECT DISTINCT(type) FROM planetdefense WHERE planetId=? GROUP BY type");
            //  PreparedStatement pstmt4 = DbConnect.prepareStatement("SELECT userId FROM playerfleet WHERE planetId=0 AND systemId=?");
            // PreparedStatement pstmt5 = DbConnect.prepareStatement("SELECT DISTINCT(type) FROM planetdefense WHERE planetId=0 AND systemId=? GROUP BY type");

            ArrayList<PlayerPlanet> pps = playerPlanetDAO.findByUserIdSorted(userId);
            //ResultSet rs = stmt.executeQuery("SELECT pp.planetID, pp.name FROM playerplanet pp WHERE userID="+userId);

            HashMap<Integer,Boolean> hasScannerArray = new HashMap<Integer,Boolean>();

            for (PlayerPlanet pp : pps) {
                int systemId = planetDAO.findById(pp.getPlanetId()).getSystemId();

                DefensePlanetEntry dpe = new DefensePlanetEntry();

                dpe.setId(pp.getPlanetId());
                dpe.setSystemId(systemId);
                dpe.setName(pp.getName());

                //   pstmt1.setInt(1, rs.getInt(1));
                //   pstmt2.setInt(1,rs.getInt(1));
                //   pstmt3.setInt(1,rs.getInt(1));
                //pstmt4.setInt(1, systemId);
                //  pstmt5.setInt(1, systemId);

                // Check for Space Ships

                for (PlayerFleet pf : playerFleetDAO.findByPlanetId(pp.getPlanetId())) {
                    if (pf.getUserId() == userId) {
                        dpe.setHasFleetDefense(true);
                        continue;
                    }

                    if (!dpe.isHasFleetDefense() &&
                            (DiplomacyUtilities.hasAllianceRelation(pf.getUserId(), userId) ||
                            DiplomacyUtilities.hasRelation(pf.getUserId(), userId, DiplomacyType.TREATY))) {
                        dpe.setHasFleetDefenseAllied(true);
                        continue;
                    } else if (!dpe.isHasEnemyFleet() && !(
                            DiplomacyUtilities.hasAllianceRelation(pf.getUserId(), userId) ||
                            DiplomacyUtilities.hasRelation(pf.getUserId(), userId, DiplomacyType.TREATY)
                            )) {
                        dpe.setHasEnemyFleet(true);
                        continue;
                    }
                }


                // Check for Ground Troops
                if (playerTroopDAO.findByPlanetId(pp.getPlanetId()).size() > 0) {
                    dpe.setHasMilitary(true);
                }

                // Check for space stations or planetary defense
                if (planetDefenseDAO.findByPlanet(pp.getPlanetId(), EDefenseType.TURRET).size() > 0) {
                    dpe.setHasPlanetDefense(true);
                }
                if (planetDefenseDAO.findByPlanet(pp.getPlanetId(), EDefenseType.SPACESTATION).size() > 0) {
                    dpe.setHasOrbitalDefense(true);
                }


                // Check for system fleets
                for (PlayerFleet pf : playerFleetDAO.findBy(0, systemId)) {
                    if (pf.getUserId() == userId) {
                        dpe.setHasSystemFleet(true);
                        continue;
                    }

                    if (!dpe.isHasFleetDefenseAllied() && (AllianceUtilities.areAllied(pf.getUserId(), userId))) {
                        dpe.setHasSystemFleetAllied(true);
                        continue;
                    } else if (!dpe.isHasEnemySystemFleet() && (!AllianceUtilities.areAllied(pf.getUserId(), userId))) {
                        dpe.setHasEnemySystemFleet(true);
                        continue;
                    }
                }


                // Check for system space installations
                if (planetDefenseDAO.findBySystem(systemId, EDefenseType.SPACESTATION).size() > 0) {
                    dpe.setHasSystemDefense(true);
                }

                // Check for scanner Array
                if (!hasScannerArray.containsKey(systemId)) {
                    hasScannerArray.put(systemId, systemHasScannerArray(systemId));
                }

                dpe.setHasScannerArray(hasScannerArray.get(systemId));
                defList.add(dpe);
            }

            // Check List, if enemy fleet flag is set and there are no own fleets in system and no scanner Array remove Flag
            /*
            for (DefensePlanetEntry dpeCheck : defList) {
                if (dpeCheck.isHasEnemySystemFleet() || dpeCheck.isHasEnemyFleet()) {                    
                    boolean hasFleet = dpeCheck.isHasFleetDefense() || dpeCheck.isHasSystemFleet() || dpeCheck.isHasFleetDefenseAllied() || dpeCheck.isHasSystemFleetAllied();
                    if (hasFleet) continue;
                    
                    if (!dpeCheck.isHasScannerArray()) {
                        dpeCheck.setHasEnemyFleet(false);
                        dpeCheck.setHasEnemySystemFleet(false);
                    }
                }
            }
            */
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("ERROR: ", e);
        }

        return defList;
    }

    private boolean systemHasScannerArray(int systemId) {
        ArrayList<Planet> planetList = planetDAO.findBySystemId(systemId);
        for (Planet p : planetList) {
            if (planetConstructionDAO.isConstructed(p.getId(), Construction.ID_SCANNERPHALANX)) return true;
        }

        return false;
    }

    public static ArrayList<PlayerFleet> getInvadeableFleets(int userId, int planetId) {
        ArrayList<PlayerFleet> result = new ArrayList<PlayerFleet>();

        ArrayList<PlayerFleet> fleets = playerFleetDAO.findByUserIdPlanetId(userId, planetId);
        for (PlayerFleet pf : fleets) {
            log.debug("Check fleet " + pf.getId() + " - " + pf.getName() + "[LOC: "+pf.getSystemId()+":"+pf.getPlanetId()+"]");

            PlayerFleetExt currFleet = new PlayerFleetExt(pf.getId());
            LoadingInformation li = currFleet.getLoadingInformation();
            if (li.getCurrLoadedTroops() > 0) {
                result.add(pf);
            }
        }


        return result;
    }

    public static void invade(int fleetId) {
        PlayerFleet pf = playerFleetDAO.findById(fleetId);
        PlayerFleetExt pfExt = new PlayerFleetExt(pf.getId());
        LoadingInformation li = pfExt.getLoadingInformation();
        int planetId = pf.getPlanetId();
        int userId = pf.getUserId();

        for (ShipStorage ss : li.getLoadedStorage()) {
            if (ss.getLoadtype() == FleetLoading.LOADTYPE_TROOPS) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "Loop");
                int troopId = ss.getId();
                int count = ss.getCount();

                if (count == 0) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Continue");
                    continue;
                }
                if (playerPlanetDAO.findByPlanetId(planetId) != null) {

                    GroundCombatUtilities.checkForCombat(userId, planetId);
                }
                // Erstma
                PlayerTroop pt = playerTroopDAO.findBy(userId, planetId, troopId);
                FleetLoading fl = fleetLoadingDAO.getByFleetAndFreight(fleetId, FleetLoading.LOADTYPE_TROOPS, troopId);

                if (fl.getCount() == count) {
                    fleetLoadingDAO.remove(fl);
                } else {
                    fl.setCount(fl.getCount() - count);
                    fleetLoadingDAO.update(fl);
                }

                DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading1");
                if (pt != null) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading2");

                    pt.setNumber(pt.getNumber() + count);
                    playerTroopDAO.update(pt);
                } else {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Fleetunloading3");
                    pt = new PlayerTroop();
                    pt.setPlanetId(planetId);
                    pt.setTroopId(troopId);
                    pt.setNumber((long) count);
                    DebugBuffer.addLine(DebugLevel.DEBUG, "userId : " + pf.getUserId());
                    pt.setUserId(pf.getUserId());
                    playerTroopDAO.add(pt);
                }
            }

        }
    }

    public static boolean loadingTroopsPossible(int userId, int planetId) {
        ArrayList<PlayerTroop> ptList = playerTroopDAO.findBy(userId, planetId);
        if (ptList.isEmpty()) return false;

        for (PlayerTroop pt : ptList) {
            if (pt.getTroopId() != 0) return true;
        }

        return false;
    }

    public static boolean unloadTroopPossible(int userId, int planetId) {
        if (!DiplomacyUtilities.getDiplomacyResult(userId, playerPlanetDAO.findByPlanetId(planetId).getUserId()).isHelps()) {
            return false;
        }

        return true;
    }

    @Deprecated
    /**
     * Please use invasionPossible in FleetService if possible
     * as it is about 3 times faster
     */
    public static boolean invasionPossible (int userId, int planetId, List<FleetData> fleetList) {
        DebugBuffer.addLine(DebugLevel.DEBUG,"Called by user " + userId + " for planet " + planetId);
        if (fleetList != null) { DebugBuffer.addLine(DebugLevel.DEBUG,"FleetCount = " + fleetList.size()); }

        log.debug("Find fleets for ("+userId+" / "+planetId+")");
        ArrayList<PlayerFleet> fleets = getInvadeableFleets(userId, planetId);
        if (fleets.size() <= 0) {
            log.debug("No fleets available for invasion ("+userId+" / "+planetId+")");
            return false;
        }

        ArrayList<PlayerFleet> allFleets = playerFleetDAO.findByPlanetId(planetId);

        /*
        boolean enemyInOrbit = false;
        for (PlayerFleet pf : allFleets) {
            if (!AllianceUtilities.areAllied(pf.getUserId(), userId) &&
                 !DiplomacyUtilities.getDiplomacyResult(userId, pf.getUserId()).isAttacks()) {
                enemyInOrbit = true;
            }
        }
        */

        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);

        if (pp == null) {
            log.debug("REJECT INVASION DUE TO null");
            return false;
        } else if (userId == pp.getUserId()) {
            log.debug("REJECT INVASION DUE TO SAME USERID");
            return false;
        /*
        } else if (enemyInOrbit) {
            log.debug("REJECT INVASION DUE TO ORBITAL FLEETS");
            return false;
        */
        } else if (getAttackerStrength(planetId, userId) < (getPlanetaryShieldStrength(planetId) / 2d)) {
            log.debug("REJECT INVASION DUE TO SHIELD");
            return false;
        } else {
            ArrayList<Integer> users = new ArrayList<Integer>();
            users.add(pp.getUserId());
            users.add(userId);

            // log.debug("Adding users >> " + pp.getUserId() + " and " + userId);

            CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(users, new RelativeCoordinate(0,planetId));
            /*
            for (CombatGroupEntry cg : cgr.getCombatGroups()) {
                log.debug("CG ID: " + cg.getGroupId() + ":");
                for (Integer p : cg.getPlayers()) {
                    log.debug("Player >> " + p);
                }
            }
            */
            if (!cgr.hasConflictingParties()) {
                log.debug("No conflict");
                return false;
            }
        }
        return true;
    }

    public static int getPlanetaryShieldStrength(int planetId) {
        ShieldDefinition planetaryShield = new ShieldDefinition();
        int PLANETARY_HU = 0;
        int PLANETARY_PA = 1;
        // Check if planet has Colony - if no skip following stuff
        if (playerPlanetDAO.findByPlanetId(planetId) == null) {
            return 0;
        }

        // Load free Energy from Planet
        try {
            PlanetCalculation pc = new PlanetCalculation(planetId);

            ProductionResult prodRes = pc.getPlanetCalcData().getProductionData();
            long freeEnergy = prodRes.getRessProduction(at.darkdestiny.core.model.Ressource.ENERGY) - prodRes.getRessConsumption(at.darkdestiny.core.model.Ressource.ENERGY);
            long shieldPower = 0;

            if (freeEnergy < 0) {
                freeEnergy = 0;
            }


            int shieldType = -1;

            if (planetConstructionDAO.isConstructed(planetId, Construction.ID_PLANETARY_HUESHIELD)) {
                shieldType = PLANETARY_HU;
                shieldPower = freeEnergy * 10;
            }
            if (planetConstructionDAO.isConstructed(planetId, Construction.ID_PLANETARY_PARATRONSHIELD)) {
                shieldType = PLANETARY_PA;
                shieldPower = freeEnergy * 20;
            }

            if (freeEnergy > 0) {
                if (shieldType == PLANETARY_HU) {
                    planetaryShield.setShield_HU((int) shieldPower);
                    planetaryShield.setSharedShield_HU((int) shieldPower);
                    planetaryShield.setEnergyNeed_HU((int) freeEnergy);
                    planetaryShield.setShieldEnergy((int) freeEnergy);
                } else if (shieldType == PLANETARY_PA) {
                    planetaryShield.setShield_PA((int) shieldPower);
                    planetaryShield.setSharedShield_PA((int) shieldPower);
                    planetaryShield.setEnergyNeed_PA((int) freeEnergy);
                    planetaryShield.setShieldEnergy((int) freeEnergy);
                }

                // log.debug("Planetary shield strength: " + planetaryShield.getShield_HU());
                // planetaryShield.setShieldEnergyPerShip(shieldEnergyConsumption);
                // planetaryShield.setShieldEnergy(shieldEnergyConsumption * count);
            }
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error in Calculation of Planetaryshield : " + e);
        }
        DebugBuffer.debug("Getting Shield for Invasioncheck vs Shield" + Math.max(planetaryShield.getShield_HU(), planetaryShield.getShield_PA()));

        return Math.max(planetaryShield.getShield_HU(), planetaryShield.getShield_PA());
    }

    public static long getAttackerStrength(int planetId, int userId) {
        long attackStr = 0l;
        for (PlayerFleet pf : playerFleetDAO.findByUserIdPlanetId(userId, planetId)) {
            PlayerFleetExt pfe = new PlayerFleetExt(pf.getId());
            long fleetStrength = 0l;

            for (ShipData sd : pfe.getShipList()) {
                long shipStrength = 0l;
                ShipDesignDetailed sdd = new ShipDesignDetailed(sd.getDesignId());
                for (Map.Entry<Integer, Integer> entries : sdd.getModuleCount().entrySet()) {
                    if (moduleDAO.findById(entries.getKey()).getType().equals(ModuleType.WEAPON)) {
                        Weapon w = sdd.getWeapon(entries.getKey());
                        //Add each weapon to shipStrength
                        shipStrength += w.getAttackStrength() * entries.getValue();
                    }
                }
                //Add Shipdesign damage for the number of ships in this fleet
                fleetStrength += sd.getCount() * shipStrength;
            }

            //Add each Fleet
            DebugBuffer.debug("Add strength of "+fleetStrength+" by fleet " + pf.getName());
            attackStr += fleetStrength;
        }

        // Find other fleets of users which are allied to this user
        // First loop all other fleets on this planet and gather users
        ArrayList<Integer> users = new ArrayList<Integer>();

        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        users.add(pp.getUserId());

        for (PlayerFleet pf : playerFleetDAO.findByPlanetId(planetId)) {
            DebugBuffer.debug("Found fleet "+pf.getName()+" by user " + pf.getUserId());
            if (!users.contains(pf.getUserId())) users.add(pf.getUserId());
        }

        CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(users, new RelativeCoordinate(0,planetId));
        DebugBuffer.debug("CONFLICT??? " + cgr.hasConflictingParties());

        for (CombatGroupEntry cge : cgr.getCombatGroups()) {
            for (Integer tmpId : cge.getPlayers()) {
                DebugBuffer.debug("CG "+cge.getGroupId()+" contains user " + tmpId);
            }

            if (cge.getPlayers().contains(userId)) {
                for (PlayerFleet pf : playerFleetDAO.findByPlanetId(planetId)) {
                    if (pf.getUserId().equals(userId)) continue;

                    if (cge.getPlayers().contains(pf.getUserId())) {
                        PlayerFleetExt pfe = new PlayerFleetExt(pf.getId());
                        long fleetStrength = 0l;

                        for (ShipData sd : pfe.getShipList()) {
                            long shipStrength = 0l;
                            ShipDesignDetailed sdd = new ShipDesignDetailed(sd.getDesignId());
                            for (Map.Entry<Integer, Integer> entries : sdd.getModuleCount().entrySet()) {
                                if (moduleDAO.findById(entries.getKey()).getType().equals(ModuleType.WEAPON)) {
                                    Weapon w = sdd.getWeapon(entries.getKey());
                                    //Add each weapon to shipStrength
                                    shipStrength += w.getAttackStrength() * entries.getValue();
                                }
                            }
                            //Add Shipdesign damage for the number of ships in this fleet
                            fleetStrength += sd.getCount() * shipStrength;
                        }

                        //Add each Fleet
                        DebugBuffer.debug("Add strength of "+fleetStrength+" by fleet " + pf.getName());
                        attackStr += fleetStrength;
                    }
                }
            } else {
                continue;
            }
        }

        DebugBuffer.debug("Getting attackstrength for Invasioncheck vs Shield" + attackStr);
        return attackStr;
    }

    public BattleShipNew buildBattleShipNew(int designId, int count, int shipFleetId) {
        BattleShipNew bsn = null;

        try {
            bsn = new BattleShipNew(designId, shipFleetId, count);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Unexpected error in new function: ", e);
        }

        return bsn;
    }

    public ICombatUnit buildDefenseNew(int designId, int count, UnitTypeEnum ute, int userId) {
        ICombatUnit icu = null;

        try {
            if (ute == UnitTypeEnum.PLATTFORM_DEFENSE) {
                icu = new BattleShipNew(designId, count);
            } else if (ute == UnitTypeEnum.SURFACE_DEFENSE) {
                icu = new DefenseBuilding(designId, count, userId);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Unexpected error in new function: ", e);
        }

        return icu;
    }
}
