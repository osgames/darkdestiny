/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.dao.*;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Bullet
 */
public class Service {
    public static AllianceDAO allianceDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    public static AIDAO aiDAO = (AIDAO) DAOFactory.get(AIDAO.class);
    public static AIGoalDAO aiGoalDAO = (AIGoalDAO) DAOFactory.get(AIGoalDAO.class);
    public static AIGoalUserDAO aiGoalUserDAO = (AIGoalUserDAO) DAOFactory.get(AIGoalUserDAO.class);
    public static AllianceMessageDAO allianceMessageDAO = (AllianceMessageDAO) DAOFactory.get(AllianceMessageDAO.class);
    public static AllianceMessageReadDAO allianceMessageReadDAO = (AllianceMessageReadDAO) DAOFactory.get(AllianceMessageReadDAO.class);
    public static AllianceMemberDAO allianceMemberDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    public static AllianceRankDAO allianceRankDAO = (AllianceRankDAO) DAOFactory.get(AllianceRankDAO.class);
    public static UserDAO userDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    public static UserSettingsDAO userSettingsDAO = (UserSettingsDAO) DAOFactory.get(UserSettingsDAO.class);
    public static ViewTableDAO viewTableDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    public static AllianceBoardPermissionDAO allianceBoardPermissionDAO = (AllianceBoardPermissionDAO) DAOFactory.get(AllianceBoardPermissionDAO.class);
    public static AllianceBoardDAO allianceBoardDAO = (AllianceBoardDAO) DAOFactory.get(AllianceBoardDAO.class);
    public static ConstructionDAO constructionDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    public static HangarRelationDAO hangarRelationDAO = (HangarRelationDAO) DAOFactory.get(HangarRelationDAO.class);
    public static RessourceDAO ressourceDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    public static BattleLogDAO battleLogDAO = (BattleLogDAO) DAOFactory.get(BattleLogDAO.class);
    public static BattleLogEntryDAO battleLogEntryDAO = (BattleLogEntryDAO) DAOFactory.get(BattleLogEntryDAO.class);
    public static DeletedUserDAO deletedUserDAO = (DeletedUserDAO) DAOFactory.get(DeletedUserDAO.class);
    public static EmbassyDAO embassyDAO = (EmbassyDAO) DAOFactory.get(EmbassyDAO.class);
    public static ProductionDAO productionDAO = (ProductionDAO) DAOFactory.get(ProductionDAO.class);
    public static ProductionOrderDAO productionOrderDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);
    public static RessourceCostDAO ressourceCostDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);
    public static TradeLogDAO tradeLogDAO = (TradeLogDAO) DAOFactory.get(TradeLogDAO.class);
    public static PlanetRessourceDAO planetRessourceDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    public static TechRelationDAO techRelationDAO = (TechRelationDAO) DAOFactory.get(TechRelationDAO.class);
    public static ConstructionRestrictionDAO constructionRestrictionDAO = (ConstructionRestrictionDAO) DAOFactory.get(ConstructionRestrictionDAO.class);
    public static PlayerResearchDAO playerResearchDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    public static ResearchDAO researchDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);
    public static PlanetDAO planetDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    public static NoteDAO noteDAO = (NoteDAO) DAOFactory.get(NoteDAO.class);
    public static AddOnDAO addOnDAO = (AddOnDAO) DAOFactory.get(AddOnDAO.class);
    public static ChangeEntryDAO changeEntryDAO = (ChangeEntryDAO) DAOFactory.get(ChangeEntryDAO.class);
    public static PlanetConstructionDAO planetConstructionDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    public static UserDataDAO userDataDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    public static PlayerPlanetDAO playerPlanetDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    public static ActionDAO actionDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    public static GalaxyDAO galaxyDAO = (GalaxyDAO) DAOFactory.get(GalaxyDAO.class);
    public static SystemDAO systemDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    public static PasswordDAO passwordDAO = (PasswordDAO) DAOFactory.get(PasswordDAO.class);
    public static PlayerCategoryDAO playerCategoryDAO = (PlayerCategoryDAO) DAOFactory.get(PlayerCategoryDAO.class);
    public static PlanetCategoryDAO planetCategoryDAO = (PlanetCategoryDAO) DAOFactory.get(PlanetCategoryDAO.class);
    public static PlanetLoyalityDAO planetLoyalityDAO = (PlanetLoyalityDAO) DAOFactory.get(PlanetLoyalityDAO.class);
    public static PriceListDAO priceListDAO = (PriceListDAO) DAOFactory.get(PriceListDAO.class);
    public static PriceListEntryDAO priceListEntryDAO = (PriceListEntryDAO) DAOFactory.get(PriceListEntryDAO.class);
    public static StatisticDAO statisticDAO = (StatisticDAO) DAOFactory.get(StatisticDAO.class);
    public static PlayerStatisticDAO playerStatisticDAO = (PlayerStatisticDAO) DAOFactory.get(PlayerStatisticDAO.class);
    public static ResearchProgressDAO researchProgressDAO = (ResearchProgressDAO) DAOFactory.get(ResearchProgressDAO.class);
    public static GroundTroopDAO groundTroopDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);
    public static GTRelationDAO gtRelationDAO = (GTRelationDAO) DAOFactory.get(GTRelationDAO.class);
    public static TradeOfferDAO tradeOfferDAO = (TradeOfferDAO) DAOFactory.get(TradeOfferDAO.class);
    public static TradeRouteDAO tradeRouteDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    public static SunTransmitterDAO sunTransmitterDAO = (SunTransmitterDAO) DAOFactory.get(SunTransmitterDAO.class);
    public static SunTransmitterRouteDAO sunTransmitterRouteDAO = (SunTransmitterRouteDAO) DAOFactory.get(SunTransmitterRouteDAO.class);
    public static TradeRouteDetailDAO tradeRouteDetailDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    public static TradeRouteShipDAO tradeRouteShipDAO = (TradeRouteShipDAO) DAOFactory.get(TradeRouteShipDAO.class);
    public static PlayerTroopDAO playerTroopDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    public static LoginTrackerDAO loginTrackerDAO = (LoginTrackerDAO) DAOFactory.get(LoginTrackerDAO.class);
    public static GameDataDAO gameDataDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
    public static MessageDAO messageDAO = (MessageDAO) DAOFactory.get(MessageDAO.class);
    public static BattleResultDAO battleResultDAO = (BattleResultDAO) DAOFactory.get(BattleResultDAO.class);
    public static TradePostDAO tradePostDAO = (TradePostDAO) DAOFactory.get(TradePostDAO.class);
    public static TradePostShipDAO tradePostShipDAO = (TradePostShipDAO) DAOFactory.get(TradePostShipDAO.class);
    public static TerritoryMapDAO territoryMapDAO = (TerritoryMapDAO) DAOFactory.get(TerritoryMapDAO.class);
    public static TerritoryMapShareDAO territoryMapShareDAO = (TerritoryMapShareDAO) DAOFactory.get(TerritoryMapShareDAO.class);
    public static TerritoryMapPointDAO territoryMapPointDAO = (TerritoryMapPointDAO) DAOFactory.get(TerritoryMapPointDAO.class);
    public static ShipDesignDAO shipDesignDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    public static TradeFleetDAO tradeFleetDAO = (TradeFleetDAO) DAOFactory.get(TradeFleetDAO.class);
    public static PlayerFleetDAO playerFleetDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    public static FleetDetailDAO fleetDetailDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    public static FleetLoadingDAO fleetLoadingDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    public static FleetOrderDAO fleetOrderDAO = (FleetOrderDAO) DAOFactory.get(FleetOrderDAO.class);
    public static FleetFormationDAO fleetFormationDAO = (FleetFormationDAO) DAOFactory.get(FleetFormationDAO.class);
    public static ShipFleetDAO shipFleetDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    public static PlanetDefenseDAO planetDefenseDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    public static NewsDAO newsDAO = (NewsDAO) DAOFactory.get(NewsDAO.class);
    public static DiplomacyRelationDAO diplomacyRelationDAO = (DiplomacyRelationDAO) DAOFactory.get(DiplomacyRelationDAO.class);
    public static DiplomacyTypeDAO diplomacyTypeDAO = (DiplomacyTypeDAO) DAOFactory.get(DiplomacyTypeDAO.class);
    public static MenuImageDAO menuImageDAO = (MenuImageDAO) DAOFactory.get(MenuImageDAO.class);
    public static ScrapResourceDAO scrapResourceDAO = (ScrapResourceDAO) DAOFactory.get(ScrapResourceDAO.class);
    public static MenuLinkDAO menuLinkDAO = (MenuLinkDAO) DAOFactory.get(MenuLinkDAO.class);
    public static StyleDAO styleDAO = (StyleDAO) DAOFactory.get(StyleDAO.class);
    public static StyleElementDAO styleElementDAO = (StyleElementDAO) DAOFactory.get(StyleElementDAO.class);
    public static StyleToUserDAO styleToUserDAO = (StyleToUserDAO) DAOFactory.get(StyleToUserDAO.class);
    public static CreditDonationDAO creditDonationDAO = (CreditDonationDAO) DAOFactory.get(CreditDonationDAO.class);
    public static QuizDAO quizDAO = (QuizDAO) DAOFactory.get(QuizDAO.class);
    public static QuizEntryDAO quizEntryDAO = (QuizEntryDAO) DAOFactory.get(QuizEntryDAO.class);
    public static QuizAnswerDAO quizAnswerDAO = (QuizAnswerDAO) DAOFactory.get(QuizAnswerDAO.class);
    public static QuizQuestionDAO quizQuestionDAO = (QuizQuestionDAO) DAOFactory.get(QuizQuestionDAO.class);
    public static AttributeDAO attributeDAO = (AttributeDAO) DAOFactory.get(AttributeDAO.class);
    public static ModuleAttributeDAO moduleAttributeDAO = (ModuleAttributeDAO) DAOFactory.get(ModuleAttributeDAO.class);
    public static ModuleDAO moduleDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    public static DesignModuleDAO designModuleDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);
    public static ResAlignmentDAO resAlignmentDAO = (ResAlignmentDAO) DAOFactory.get(ResAlignmentDAO.class);
    public static TechTypeDAO techTypeDAO = (TechTypeDAO) DAOFactory.get(TechTypeDAO.class);
    public static ImprovementDAO improvementDAO = (ImprovementDAO) DAOFactory.get(ImprovementDAO.class);
    public static VotingDAO votingDAO = (VotingDAO) DAOFactory.get(VotingDAO.class);
    public static VoteMetadataDAO voteMetadataDAO = (VoteMetadataDAO) DAOFactory.get(VoteMetadataDAO.class);
    public static LanguageDAO languageDAO = (LanguageDAO) DAOFactory.get(LanguageDAO.class);
    public static VoteOptionDAO voteOptionDAO = (VoteOptionDAO) DAOFactory.get(VoteOptionDAO.class);
    public static VotePermissionDAO votePermissionDAO = (VotePermissionDAO) DAOFactory.get(VotePermissionDAO.class);
    public static VotingDetailDAO votingDetailDAO = (VotingDetailDAO) DAOFactory.get(VotingDetailDAO.class);
    public static NotificationDAO notificationDAO = (NotificationDAO) DAOFactory.get(NotificationDAO.class);
    public static NotificationToUserDAO notificationToUserDAO = (NotificationToUserDAO) DAOFactory.get(NotificationToUserDAO.class);
    public static ImperialStatisticDAO imperialStatisticDAO = (ImperialStatisticDAO) DAOFactory.get(ImperialStatisticDAO.class);
    public static ChassisDAO chassisDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    public static StatisticEntryDAO statisticEntryDAO = (StatisticEntryDAO) DAOFactory.get(StatisticEntryDAO.class);
    public static StatisticCategoryDAO statisticCategoryDAO = (StatisticCategoryDAO) DAOFactory.get(StatisticCategoryDAO.class);
    public static FilterDAO filterDAO = (FilterDAO) DAOFactory.get(FilterDAO.class);
    public static FilterValueDAO filterValueDAO = (FilterValueDAO) DAOFactory.get(FilterValueDAO.class);
    public static FilterToValueDAO filterToValueDAO = (FilterToValueDAO) DAOFactory.get(FilterToValueDAO.class);
    public static TitleDAO titleDAO = (TitleDAO) DAOFactory.get(TitleDAO.class);
    public static TitleToUserDAO titleToUserDAO = (TitleToUserDAO) DAOFactory.get(TitleToUserDAO.class);
    public static UserRelationDAO userRelationDAO = (UserRelationDAO) DAOFactory.get(UserRelationDAO.class);
    public static ConditionDAO conditionDAO = (ConditionDAO) DAOFactory.get(ConditionDAO.class);
    public static ConditionParameterDAO conditionParameterDAO = (ConditionParameterDAO) DAOFactory.get(ConditionParameterDAO.class);
    public static ConditionToTitleDAO conditionToTitleDAO = (ConditionToTitleDAO) DAOFactory.get(ConditionToTitleDAO.class);
    public static ConditionToUserDAO conditionToUserDAO = (ConditionToUserDAO) DAOFactory.get(ConditionToUserDAO.class);
    public static ParameterToTitleDAO parameterToTitleDAO = (ParameterToTitleDAO) DAOFactory.get(ParameterToTitleDAO.class);
    public static CampaignDAO campaignDAO = (CampaignDAO) DAOFactory.get(CampaignDAO.class);
    public static CampaignToUserDAO campaignToUserDAO = (CampaignToUserDAO) DAOFactory.get(CampaignToUserDAO.class);
    public static GroundCombatDAO groundCombatDAO = (GroundCombatDAO) DAOFactory.get(GroundCombatDAO.class);
    public static CombatPlayerDAO combatPlayerDAO = (CombatPlayerDAO) DAOFactory.get(CombatPlayerDAO.class);
    public static CombatRoundDAO combatRoundDAO = (CombatRoundDAO) DAOFactory.get(CombatRoundDAO.class);
    public static TerritoryEntryDAO territoryEntryDAO = (TerritoryEntryDAO) DAOFactory.get(TerritoryEntryDAO.class);
    public static RoundEntryDAO roundEntryDAO = (RoundEntryDAO) DAOFactory.get(RoundEntryDAO.class);
    public static BonusDAO bonusDAO = (BonusDAO) DAOFactory.get(BonusDAO.class);
    public static ActiveBonusDAO activeBonusDAO = (ActiveBonusDAO) DAOFactory.get(ActiveBonusDAO.class);
    public static PlanetLogDAO planetLogDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    public static DamagedShipsDAO damagedShipsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    public static MultiLogDAO multiLogDAO = (MultiLogDAO) DAOFactory.get(MultiLogDAO.class);
    public static ViewSystemDAO viewSystemDAO = (ViewSystemDAO) DAOFactory.get(ViewSystemDAO.class);
    
    public Service(){

    }

    public static void initialize() {

    }
}
