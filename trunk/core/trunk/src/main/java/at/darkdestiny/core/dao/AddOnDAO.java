/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.AddOn;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class AddOnDAO extends ReadWriteTable<AddOn> implements GenericDAO {


    public ArrayList<AddOn> findAllActive(){
        ArrayList<AddOn> result = new ArrayList<AddOn>();
        for(AddOn addOn : (ArrayList<AddOn>)findAll()){
            if(addOn.getActive()){
                result.add(addOn);
            }
        }
        return result;
    }
}
