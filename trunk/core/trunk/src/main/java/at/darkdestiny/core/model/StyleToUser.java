/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author HorstRabe
 */
@TableNameAnnotation(value = "styletouser")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class StyleToUser extends Model<StyleToUser> {

    @FieldMappingAnnotation("styleId")
    @IdFieldAnnotation
    Integer styleId;
    @FieldMappingAnnotation("elementId")
    @IdFieldAnnotation
    Integer elementId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("userId")
    Integer userId;
    @FieldMappingAnnotation("rank")
    Integer rank;

    public Integer getStyleId() {
        return styleId;
    }

    public void setStyleId(Integer styleId) {
        this.styleId = styleId;
    }

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }
}
