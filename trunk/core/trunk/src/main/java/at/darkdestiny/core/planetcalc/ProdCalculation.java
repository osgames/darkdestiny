/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.planetcalc;

import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.ActiveBonusDAO;
import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.ProductionDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.enumeration.EBonusType;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.core.enumeration.EEconomyType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Production;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.ressources.RessProdNode;
import at.darkdestiny.core.ressources.RessProdNodeSplitted;
import at.darkdestiny.core.ressources.RessProdTree;
import at.darkdestiny.core.result.BonusResult;
import at.darkdestiny.core.update.UpdaterDataSet;
import at.darkdestiny.core.utilities.BonusUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Stack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class ProdCalculation {

    private static final Logger log = LoggerFactory.getLogger(ProdCalculation.class);

    private RessProdTree rpt;
    private PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private ProductionDAO pDAO = (ProductionDAO) DAOFactory.get(ProductionDAO.class);
    private PlanetRessourceDAO prDAO = (PlanetRessourceDAO) DAOFactory.get(PlanetRessourceDAO.class);
    private RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    private ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private PlanetDAO plDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private PlayerResearchDAO pResDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
    private ActiveBonusDAO abDAO = (ActiveBonusDAO) DAOFactory.get(ActiveBonusDAO.class);

    private final PlayerPlanet pp;
    private final Planet planet;

    private boolean hasDesintegratorBonus = false;
    private boolean isAgriculturalPlanet = false;
    private boolean hasMiningBonus = false;
    private boolean industrialPlanet = false;

    private double moraleAdjust = 1d;

    private boolean calcFuture = false;

    public static final int VALUETYPE_PROD = 1;
    public static final int VALUETYPE_CONS = 2;
    public static final int VALUETYPE_STOCK = 3;
    public static final int VALUETYPE_MAXSTOCK = 4;
    public static final int VALUETYPE_MINSTOCK = 5;

    private boolean debug = false;
    private HashMap<Integer, ArrayList<String>> calcLog = new HashMap<Integer, ArrayList<String>>();

    private final UpdaterDataSet uds;
    private final boolean updateProcessing;

    private Map<EEconomyType, Integer> planetEconomySurface = new EnumMap<EEconomyType, Integer>(EEconomyType.class);
    private Map<EEconomyType, Integer> planetEconomyWorker = new EnumMap<EEconomyType, Integer>(EEconomyType.class);

    private HashSet<RessProdNode> unprocessedRootNodes = new HashSet<RessProdNode>();
    private Stack<RessProdNode> processingStack = new Stack<RessProdNode>();
    private HashSet<RessProdNode> consideredLocked = new HashSet<RessProdNode>();

    public ProdCalculation(PlayerPlanet pp, boolean calcFuture) {
        // if (pp.getPlanetId() == 3803) debug = true;

        this.pp = pp;
        planet = plDAO.findById(pp.getPlanetId());
        calculateBoni(pp);

        this.calcFuture = calcFuture;
        this.updateProcessing = false;
        uds = null;

        initMoraleFactor();
        buildTree();

        try {           
           initNew();
           // } else {
           //     init();
           // }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error during initialization of RessourceTree: ", e);
            e.printStackTrace();
        }

        // debug = false;
    }

    public ProdCalculation(PlayerPlanet pp) {
        // if (pp.getPlanetId() == 3803) debug = true;

        this.pp = pp;
        this.updateProcessing = false;
        uds = null;

        planet = plDAO.findById(pp.getPlanetId());
        calculateBoni(pp);

        initMoraleFactor();
        buildTree();

        try {
            initNew();
            //} else {
            //    init();
            //}
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error during initialization of RessourceTree: ", e);
            e.printStackTrace();
        }

        // debug = false;
    }

    public ProdCalculation(PlayerPlanet pp, UpdaterDataSet uds) {
        // if (pp.getPlanetId() == 59046) debug = true;

        this.pp = pp;
        this.uds = uds;
        this.updateProcessing = true;

        planet = plDAO.findById(pp.getPlanetId());
        calculateBoni(pp);

        initMoraleFactor();
        buildTree();

        try {            
            initNew();
            //} else {
            //    init();
            //}
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error during initialization of RessourceTree: ", e);
            e.printStackTrace();
        }

        debug = false;
    }

    private void initMoraleFactor() {
        moraleAdjust = 1d;

        if (pp.getMoral() < 70) {
            moraleAdjust = 1d / 70d * (double) pp.getMoral();
        }
    }

    private void buildTree() {
        rpt = new RessProdTree();
        RessProdNode rpnIronOre = new RessProdNode(Ressource.PLANET_IRON, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnYnkeloniumOre = new RessProdNode(Ressource.PLANET_YNKELONIUM, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnHowalgoniumOre = new RessProdNode(Ressource.PLANET_HOWALGONIUM, RessProdNode.NODE_TYPE_INDUSTRY);

        RessProdNode rpnIron = new RessProdNode(Ressource.IRON, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnSteel = new RessProdNode(Ressource.STEEL, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnTerkonit = new RessProdNode(Ressource.TERKONIT, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnYnkelonium = new RessProdNode(Ressource.YNKELONIUM, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnHowalgonium = new RessProdNode(Ressource.HOWALGONIUM, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnCVEmbinium = new RessProdNode(Ressource.CVEMBINIUM, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnEnergy = new RessProdNodeSplitted(Ressource.ENERGY, RessProdNode.NODE_TYPE_ENERGY);
        RessProdNode rpnFood = new RessProdNode(Ressource.FOOD, RessProdNode.NODE_TYPE_AGRICULTURE);
        RessProdNode rpnConsumables = new RessProdNode(Ressource.CONSUMABLES, RessProdNode.NODE_TYPE_INDUSTRY);
        RessProdNode rpnDevPoints = new RessProdNode(Ressource.DEV_POINTS, RessProdNode.NODE_TYPE_CONSTRUCTION);

        RessProdNode rpnResearchPoints = new RessProdNode(Ressource.RP, RessProdNode.NODE_TYPE_RESEARCH);
        RessProdNode rpnCompResearchPoints = new RessProdNode(Ressource.RP_COMP, RessProdNode.NODE_TYPE_RESEARCH);

        RessProdNode rpnKasProd = new RessProdNode(Ressource.KAS, RessProdNode.NODE_TYPE_CONSTRUCTION);
        RessProdNode rpnModulProd = new RessProdNode(Ressource.MODUL_AP, RessProdNode.NODE_TYPE_CONSTRUCTION);
        RessProdNode rpnDockProd = new RessProdNode(Ressource.PL_DOCK, RessProdNode.NODE_TYPE_CONSTRUCTION);
        RessProdNode rpnOrbDockProd = new RessProdNode(Ressource.ORB_DOCK, RessProdNode.NODE_TYPE_CONSTRUCTION);

        rpnFood.setBaseConsumption((long) (pp.getPopulation() / 1000));
        rpnFood.setActConsumption(rpnFood.getBaseConsumption());
        addLogEntry(Ressource.FOOD, "Initialize BaseConsumption: " + rpnFood.getBaseConsumption());
        addLogEntry(Ressource.FOOD, "Initialize ActConsumption: " + rpnFood.getActConsumption());

        rpnIron.setPrevious(rpnIronOre);
        rpnYnkelonium.setPrevious(rpnYnkeloniumOre);
        rpnHowalgonium.setPrevious(rpnHowalgoniumOre);

        rpnSteel.setPrevious(rpnIron);
        rpnTerkonit.setPrevious(rpnSteel);
        rpnCVEmbinium.setPrevious(rpnHowalgonium);

        rpnConsumables.setPrevious(rpnIron);
        rpnConsumables.setPrevious(rpnSteel);

        rpt.setNode(rpnIronOre);
        rpt.setNode(rpnYnkeloniumOre);
        rpt.setNode(rpnHowalgoniumOre);
        rpt.setNode(rpnEnergy);

        rpt.addRootNode(rpnIronOre);
        rpt.addRootNode(rpnYnkeloniumOre);
        rpt.addRootNode(rpnHowalgoniumOre);

        rpt.setNode(rpnIron);
        rpt.setNode(rpnSteel);
        rpt.setNode(rpnTerkonit);
        rpt.setNode(rpnYnkelonium);
        rpt.setNode(rpnHowalgonium);
        rpt.setNode(rpnCVEmbinium);
        rpt.setNode(rpnConsumables);

        rpt.addRootNode(rpnFood);
        rpt.setNode(rpnFood);

        rpt.addRootNode(rpnResearchPoints);
        rpt.addRootNode(rpnCompResearchPoints);
        rpt.setNode(rpnResearchPoints);
        rpt.setNode(rpnCompResearchPoints);

        rpt.addRootNode(rpnKasProd);
        rpt.addRootNode(rpnModulProd);
        rpt.addRootNode(rpnDockProd);
        rpt.addRootNode(rpnOrbDockProd);
        rpt.setNode(rpnKasProd);
        rpt.setNode(rpnModulProd);
        rpt.setNode(rpnDockProd);
        rpt.setNode(rpnOrbDockProd);

        rpt.addRootNode(rpnDevPoints);
        rpt.setNode(rpnDevPoints);

        unprocessedRootNodes.addAll(rpt.getRootNodes());
    }

    private void initNew() throws Exception {
        // if (pp.getPlanetId() == 59046) debug = true;
        if (debug) System.out.println("PROCESSING INITNEW! --- PLANET: " + this.pp.getPlanetId() + " " + this.pp.getName());

        if (debug) {
            log.debug("--------------------------------------------------");
        }
        ArrayList<at.darkdestiny.core.model.PlanetRessource> prList = null;

        // If we are processing the update we will use the UpdaterDataSet from the Update Thread
        // as this is faster and does not need database access
        if (updateProcessing) {
            prList = uds.getRessEntriesForPlanet(pp.getPlanetId());
        } else {
            prList = prDAO.findByPlanetId(pp.getPlanetId());
        }

        // Set storage and minimum production globally on all Nodes
        for (at.darkdestiny.core.model.PlanetRessource pr : prList) {
            if ((pr.getType() == EPlanetRessourceType.INSTORAGE) || (pr.getType() == EPlanetRessourceType.PLANET)) {
                rpt.getNode(pr.getRessId()).setActStorage(pr.getQty());
                addLogEntry(pr.getRessId(), "Set ActStorage to " + pr.getQty());
            } else if (pr.getType() == EPlanetRessourceType.MINIMUM_PRODUCTION) {
                rpt.getNode(pr.getRessId()).setMinStorage(pr.getQty());
                addLogEntry(pr.getRessId(), "Set MinStorage to " + pr.getQty());
            }
        }

        // Add in construction buildings if we need to display future production
        if (calcFuture) {
            ArrayList<Action> aList = aDAO.findRunningConstructions(pp.getPlanetId());
            for (Action a : aList) {
                Construction c = cDAO.findById(a.getConstructionId());
                addConstructionToTree(c, a.getNumber(), a.getNumber());
            }
        }

        // TODO: Investigate detailed purpose of this processing (Seems like splitting up Buildings into Economy category groups for later display in diagram)
        // Otherwise all active buildings on planet are added here 
        ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(pp.getPlanetId());
        for (PlanetConstruction pc : pcList) {
            Construction c = cDAO.findById(pc.getConstructionId());

            if (getPlanetEconomySurface().containsKey(c.getEconType())) {
                // System.out.println("[Add] Found building " + c.getName() + " with economy type " + c.getEconType() + " with value " + (pc.getNumber() * c.getSurface()));
                getPlanetEconomySurface().put(c.getEconType(), getPlanetEconomySurface().get(c.getEconType()) + pc.getNumber() * c.getSurface());
                getPlanetEconomyWorker().put(c.getEconType(), (int) (getPlanetEconomyWorker().get(c.getEconType()) + pc.getNumber() * c.getWorkers()));
            } else {
                // System.out.println("[Insert] Found building " + c.getName() + " with economy type " + c.getEconType() + " with value " + (pc.getNumber() * c.getSurface()));
                getPlanetEconomySurface().put(c.getEconType(), pc.getNumber() * c.getSurface());
                getPlanetEconomyWorker().put(c.getEconType(), (int) (pc.getNumber() * c.getWorkers()));
            }

            addConstructionToTree(c, pc.getActiveCount(), pc.getNumber());
        }

        // Start traversing the tree - begin with a random unprocessed root node
        while (unprocessedRootNodes.size() > 0) {
            RessProdNode actRoot = unprocessedRootNodes.iterator().next();
            unprocessedRootNodes.remove(actRoot);

            if (debug) {
                // log.debug("Retrieved Root Node for resource: " + actRoot.ressource);
            }
            traverseForward(actRoot, null);
            if (debug) System.out.println("FORWARD LOOP -- ACTROOT: " + actRoot.ressource);
        }

        traverseBackward();
    }

    private void traverseForward(RessProdNode currNode, RessProdNode lastNode) {
        if (debug) System.out.println("FORWARD LOOP -- PROCESSING: " + currNode.ressource);
        // If lastNode is null we are in a root Node
        at.darkdestiny.core.model.Ressource r = rDAO.findRessourceById(currNode.ressource);

        if (debug) {
            if (lastNode == null) {
                log.debug("Process Root Ressource Node " + currNode.ressource);
            } else {
                log.debug("Process Ressource Node " + currNode.ressource + " -- previous was " + lastNode.ressource);
            }
        }

        // Check if this node has other nodes which are not yet processed in Stack
        for (RessProdNode rpn : currNode.getPreviousNodes()) {
            if (!processingStack.contains(rpn) && !rpn.getLocked()) {
                if (debug) {
                    log.debug(currNode.ressource + " denies processing cause it depends on yet unprocessed node");
                }
                return;
            }
        }

        // Handle mineable ressources as they get ressource values from Planet        
        if (r.getMineable()) {
            at.darkdestiny.core.model.PlanetRessource pr = null;
            if (updateProcessing) {
                pr = uds.getPlanetRessEntry(pp.getPlanetId(), EPlanetRessourceType.PLANET, r.getId());
            } else {
                pr = prDAO.findBy(pp.getPlanetId(), r.getId(), EPlanetRessourceType.PLANET);
            }

            // Set consumption to zero as there are no further ore deposits
            // Decrease Consumption and Production of following node
            if (pr == null) {
                currNode.setActConsumption(0);
                addLogEntry(currNode.ressource, "Set ActConsumption to 0 (due to no ore available)");

                for (RessProdNode nextNode : currNode.getNextNodes()) {
                    if (nextNode.getLocked()) continue;
                    
                    if (debug) {
                        log.debug("Set Production for " + nextNode.ressource + " [" + pp.getPlanetId() + "] to 0");
                    }
                    nextNode.setActProduction(0);
                    nextNode.setMaxProductionByPrevious(0);
                    addLogEntry(nextNode.ressource, "Set ActProduction to 0 (due to no ore on previous)");
                    addLogEntry(nextNode.ressource, "Set MaxProductionByPrevious to 0 (due to no ore on previous)");
                    // rpnTmp.getNext().setBaseProduction(VALUETYPE_PROD);

                    // if (nextNode.getNextNodes().size() > 0) {
                    //     traverseForward(nextNode,currNode);                        
                    // }
                }

                // if (debug) {
                //     log.debug(">>> REK >>> RETURN DUE TO EMPTY ORE STORAGE");
                // }
                // return;
            } else if (pr.getQty() < currNode.getActConsumption()) {

                // Set consumption to new limit due to low ore deposits
                // Decrease Consumption and Production of following node
                // if (pr.getQty() < currNode.getActConsumption()) {
                double maxEff = 1d / currNode.getActConsumption() * pr.getQty();
                currNode.setActConsumption(Math.max(1, pr.getQty()));

                addLogEntry(currNode.ressource, "Set ActConsumption to " + pr.getQty() + " (due to low ore)");

                // give each of the next Nodes an equal share of the production                
                for (RessProdNode nextNode : currNode.getNextNodes()) {
                    if (nextNode.getLocked()) continue;
                    
                    long actProduction = (long) Math.max(1l, (long) Math.floor(nextNode.getActProduction() * maxEff));
                    nextNode.setActProduction(actProduction);
                    addLogEntry(nextNode.ressource, "Set ActProduction to " + nextNode.getActProduction() + " (due to low ore on previous)");
                    nextNode.setMaxProductionByPrevious(actProduction);
                    addLogEntry(nextNode.ressource, "Set MaxProductionByPrevious to " + nextNode.getMaxProductionByPrevious() + " (due to low ore on previous)");
                }
            }

            /*
             for (RessProdNode nextNode : currNode.getNextNodes()) {  
             if (debug) {
             log.debug(">>> REK >>> TRAVERSE NEXT NODE (BRANCH 0)");
             }                    
             traverseForward(nextNode,currNode);            
             }
             */
        } else { // For all nodes which are not mineable
            if (!((currNode.type == RessProdNode.NODE_TYPE_RESEARCH)
                    || (currNode.type == RessProdNode.NODE_TYPE_CONSTRUCTION))) { // THIS APPLIES TO EVERYTHING WHICH IS NOT RESEARCH OR CONSTRUCTION/PRODUCTION
                // Limit production due to full stock
                if (debug) {
                    log.debug("[" + currNode.ressource + "] ACTSTORAGE=" + currNode.getActStorage() + " ACTPROD=" + currNode.getActProduction() + " ACTCONS=" + currNode.getActConsumption() + " MAXSTORAGE=" + currNode.getMaxStorage());
                }

                // Check if storage is full
                if ((currNode.getActStorage() + currNode.getActProduction() - currNode.getActConsumption()) > currNode.getMaxStorage()) {
                    if (debug) {
                        log.debug("Limit production due to stock for ress " + currNode.ressource);
                    }
                    // double maxEff = 100d / rpnTmp.getActProduction() * (rpnTmp.getMaxStorage() - rpnTmp.getActStorage());
                    currNode.setActProduction(currNode.getMaxStorage() - currNode.getActStorage() + currNode.getActConsumption());
                    addLogEntry(currNode.ressource, "Reduce ActProduction to " + currNode.getActProduction() + " cause it exceeds maximum storage");
                }

                // Minstorage calculation
                if (currNode.getMinStorage() > 0) {
                    for (RessProdNode nextNode : currNode.getNextNodes()) {
                        if (debug) {
                            log.debug("MINSTOCK >> ");
                            log.debug("STORAGE: " + currNode.getActStorage() + " PROD: " + currNode.getActProduction() + " CONS: " + currNode.getActConsumption());
                            log.debug("COMPARE: " + (currNode.getActStorage() + currNode.getActProduction() - currNode.getActConsumption()) + " <=> " + currNode.getMinStorage());
                        }
                        if (currNode.getActStorage() + currNode.getActProduction() - currNode.getActConsumption() < currNode.getMinStorage()) {
                            double maxCons = Math.max(0, currNode.getActStorage() + currNode.getActProduction() - currNode.getMinStorage());
                            if (debug) {
                                log.debug("MAXCONS: " + maxCons);
                            }
                            double maxEff = 1d / currNode.getActConsumption() * maxCons;

                            if (maxEff < 1d) {
                                if (debug) {
                                    log.debug("Set Act Cons of " + r.getName() + " to " + maxCons);
                                    log.debug("Max Eff of next node = " + maxEff);
                                    log.debug("Set Production of next node to " + (long) Math.floor(nextNode.getActProduction() * maxEff)); // TODO
                                }

                                currNode.setActConsumption((long) maxCons);
                                nextNode.setActProduction((long) Math.floor(nextNode.getActProduction() * maxEff));
                                nextNode.setMaxProductionByPrevious((long) Math.floor(nextNode.getActProduction() * maxEff));
                                addLogEntry(currNode.ressource, "Set ActConsumption to " + maxCons + " due to minStorage");
                                addLogEntry(nextNode.ressource, "Set ActProduction to " + nextNode.getActProduction() + " due to minStorage limitation on previous node");
                                addLogEntry(nextNode.ressource, "Set MaxProductionByPrevious to " + nextNode.getMaxProductionByPrevious() + " due to minStorage limitation on previous node");
                            }
                        }

                        /*
                         if (debug) {
                         log.debug(">>> REK >>> TRAVERSE NEXT NODE (BRANCH 1)");
                         }                                            
                         traverseForward(nextNode,currNode);
                         */
                    }
                } else {
                    /*
                     if (debug) {
                     log.debug(">>> REK >>> TRAVERSE NEXT NODE (BRANCH 1.1)");
                     }                           
                    
                     for (RessProdNode nextNode : currNode.getNextNodes()) {                    
                     traverseForward(nextNode,currNode);                                    
                     }
                     */
                }

                /*
                 if ((currNode.getMinStorage() > 0) && (currNode.getNext() != null)) { // TODO
                 if (debug) {
                 log.debug("MINSTOCK >> ");
                 log.debug("STORAGE: " + currNode.getActStorage() + " PROD: " + currNode.getActProduction() + " CONS: " + currNode.getActConsumption());
                 log.debug("COMPARE: " + (currNode.getActStorage() + currNode.getActProduction() - currNode.getActConsumption()) + " <=> " + currNode.getMinStorage());
                 }
                 if (currNode.getActStorage() + currNode.getActProduction() - currNode.getActConsumption() < currNode.getMinStorage()) {
                 double maxCons = Math.max(0, currNode.getActStorage() + currNode.getActProduction() - currNode.getMinStorage());
                 if (debug) {
                 log.debug("MAXCONS: " + maxCons);
                 }
                 double maxEff = 1d / currNode.getActConsumption() * maxCons;

                 if (maxEff < 1d) {
                 if (debug) {
                 log.debug("Set Act Cons of " + r.getName() + " to " + maxCons);
                 log.debug("Max Eff of next node = " + maxEff);
                 log.debug("Set Production of next node to " + (long) Math.floor(currNode.getNext().getActProduction() * maxEff)); // TODO
                 }

                 currNode.setActConsumption((long) maxCons);
                 currNode.getNext().setActProduction((long) Math.floor(currNode.getNext().getActProduction() * maxEff)); // TODO
                 currNode.getNext().setMaxProductionByPrevious((long) Math.floor(currNode.getNext().getActProduction() * maxEff)); // TODO
                 addLogEntry(currNode.ressource, "Set ActConsumption to " + maxCons + " due to minStorage");
                 addLogEntry(currNode.getNext().ressource, "Set ActProduction to " + currNode.getNext().getActProduction() + " due to minStorage limitation on previous node"); // TODO
                 addLogEntry(currNode.getNext().ressource, "Set MaxProductionByPrevious to " + currNode.getNext().getMaxProductionByPrevious() + " due to minStorage limitation on previous node"); // TODO
                 }
                 }
                 }
                 */
                // Check if current Node has a higher consumption than it actually can produce
                if (currNode.type != RessProdNode.NODE_TYPE_AGRICULTURE) { // THIS APPLIES TO EVERYTHING WHICH IS NOT FOOD
                    if (currNode.getActConsumption() > (currNode.getActProduction() + currNode.getActStorage())) {
                        if (debug) {
                            log.debug("Check if " + r.getName() + " Consumption (" + currNode.getActConsumption() + ") is larger than PROD + STORAGE (" + (currNode.getActProduction() + currNode.getActStorage()) + ")");
                        }
                        double maxEff = 1d / currNode.getActConsumption() * (currNode.getActProduction() + currNode.getActStorage());
                        for (RessProdNode nextNode : currNode.getNextNodes()) {
                            nextNode.setActProduction((long) Math.floor(nextNode.getActProduction() * maxEff));
                            addLogEntry(nextNode.ressource, "Set ActProduction to " + nextNode.getActProduction() + " as production and storage of previous node is lower than consumption");

                            /*
                             if (debug) {
                             log.debug(">>> REK >>> TRAVERSE NEXT NODE (BRANCH 2)");
                             }                                            
                             traverseForward(currNode,nextNode);
                             */
                        }
                        currNode.setActConsumption((currNode.getActProduction() + currNode.getActStorage()));
                        addLogEntry(currNode.ressource, "Set ActConsumption to " + currNode.getActConsumption() + " cause Consumption higher than production + storage");
                    }
                } else { // FOOD 
                    double maxConsByProdAndStock = currNode.getActProduction() + currNode.getActStorage();

                    currNode.setActConsumption(currNode.getBaseConsumption());
                    addLogEntry(currNode.ressource, "Set ActConsumption to " + currNode.getActConsumption() + " (BaseConsumption) as we have enough food to meet demands");
                    currNode.setActConsumption((long) Math.min(maxConsByProdAndStock, currNode.getActConsumption()));
                    addLogEntry(currNode.ressource, "Limit ActConsumption to " + currNode.getActConsumption() + " which is either BaseConsumption or available Qty in Stock + production");
                }
            }
        }

        if (debug) addLogEntry(currNode.ressource,"Added this node to processingStack");
        processingStack.push(currNode);

        ArrayList<RessProdNode> processedDecendants = new ArrayList<RessProdNode>();

        do {
            for (RessProdNode nextNode : currNode.getNextNodes()) {
                if (processingStack.contains(nextNode) || nextNode.getLocked()) {
                    if (!processedDecendants.contains(nextNode)) {
                        processedDecendants.add(nextNode);
                    }
                    continue;
                }

                if (debug) {
                    log.debug(">>> REK >>> TRAVERSE NEXT NODE (NEXTNODE: " + nextNode.ressource + ")");
                }
                
                traverseForward(nextNode, currNode);

                if (processingStack.contains(nextNode)) {
                    if (debug) {
                        log.debug(">>> REK >>> ADD PROCESSED DESCENDANT (NODE: " + nextNode.ressource + ")");
                    }
                    processedDecendants.add(nextNode);
                }
            }

            if (debug) {
                log.debug(">>> REK >>> [" + currNode.ressource + "] PROCESSED DESCENDANTS " + processedDecendants.size() + " VS NEXTNODESSIZE" + currNode.getNextNodes().size());
            }
        } while (processedDecendants.size() < currNode.getNextNodes().size());

        if (debug) {
            log.debug(">>> REK >>> END REK DEFAULT");
        }
    }

    private void traverseBackward() {
        // Use the processing stack to traverse backwards
        boolean reprocess = false;
        RessProdNode reprocessNode = null;

        do {
            reprocess = false;
            reprocessNode = null;
            
            while (!processingStack.isEmpty()) {
                if (debug) System.out.println("BACKWARD LOOP -- STACKSIZE: " + processingStack.size());
                RessProdNode currNode = processingStack.pop();
                if (debug) addLogEntry(currNode.ressource," Removed this node from processingStack");

                if (debug) {
                    log.debug("Backward Check " + currNode.ressource);
                }

                int previousNodesCnt = currNode.getPreviousNodes().size();
                if (previousNodesCnt == 0) {
                    continue;
                }

            // If one of the next nodes is locked but has not been considered yet, reset current node, 
                // leave while loop and process again (forward processing)
                boolean breakWhile = false;

                for (RessProdNode next : currNode.getNextNodes()) {
                    if (next.getLocked() && !consideredLocked.contains(next)) {
                        consideredLocked.add(next);
                        breakWhile = true;
                    }
                }

                if (breakWhile) {
                    resetTreeBranch(currNode);
                    reprocess = true;
                    reprocessNode = currNode;
                    break;
                }

            // RessProdNode rpnPrev = currNode.getPrevious(); // WE HAVE FUCKING MULTIPLE PREVIOUS DAMMIT
                // if (rpnPrev == null) {
                //                 continue;
                // }
                // Scale the consumption of this node in regards to consumption of next nodes
                // During backtraversing we have to check how much the following nodes consume and adjust the production of this node 
                // due to the consumption and storage available
                long actConsumption = currNode.getFixedConsumption();
                for (RessProdNode nextNode : currNode.getNextNodes()) {
                    if (currNode.getActConsumptionByNode(nextNode) == null) {
                        if (debug) {
                            log.debug("No consumption node found for ressource " + nextNode.ressource);
                        }
                        actConsumption = 0l;
                        break;
                    }

                    actConsumption += currNode.getActConsumptionByNode(nextNode);
                    if (debug) {
                        log.debug("[" + currNode.ressource + "] Adding " + currNode.getActConsumptionByNode(nextNode) + " consumption by node " + nextNode.ressource);
                    }
                }

                currNode.setActConsumption(actConsumption);

                if (!rDAO.findById(currNode.ressource).getMineable()) {
                    long freeCapacity = currNode.getMaxStorage() - (currNode.getActStorage() - currNode.getActConsumption());
                    // long toProduce = Math.min(freeCapacity, currNode.getMaxProductionByPrevious());
                    long toProduce = Math.min(freeCapacity, currNode.getActProduction());
                    
                    if (debug) {
                        addLogEntry(currNode.ressource,"Free Capacity is " + freeCapacity + " -- Act Production: " + currNode.getActProduction());
                    }                    

                // This node has a lower production capacity than basically could be provided by previous node
                    // TODO: Therefore lock this node and mark previous node for recalculation in case there was a 
                    //       resource shortage in previous node (so maybe resources can be redirected)
                    if (freeCapacity <= currNode.getActProduction()) {
                        currNode.setLocked();
                        if (debug) {
                            addLogEntry(currNode.ressource,"Lock resource node");
                        }
                    }

                    if (debug) {
                        log.debug("Set new production for " + currNode.ressource + " -- FREECAP: " + freeCapacity + " TOPROD: " + toProduce);
                    }

                    currNode.setActProduction(toProduce);
                    addLogEntry(currNode.ressource, "Set ActProduction to " + currNode.getActProduction() + ", which is maxQty in respect to storage and production of previous node");
                }

                // Scale consumption of previous node if this node has decreased production
                for (RessProdNode rpnPrev : currNode.getPreviousNodes()) {
                    if (currNode.getActProduction() > 0) {
                        if (debug) {
                            log.debug("Previous node has production > 0");
                        }

                        if (currNode.getActProduction() < currNode.getBaseProduction()) {
                            double maxEff = 1d / currNode.getBaseProduction() * currNode.getActProduction();
                            if (debug) {
                                log.debug("Previous prduction is scaled down to " + maxEff + " (use on " + rpnPrev.getBaseConsumption() + ")");
                            }

                            if (currNode.type != RessProdNode.NODE_TYPE_AGRICULTURE) {
                                if (rpnPrev.getBaseConsumptionByNode(currNode) != null) {
                                    rpnPrev.setActConsumptionByNode(currNode, (int) Math.ceil(rpnPrev.getBaseConsumptionByNode(currNode) * maxEff));
                                    
                                    if (rpnPrev.getPreviousNodes().isEmpty()) {
                                        rpnPrev.setActConsumption((int) Math.ceil(rpnPrev.getBaseConsumptionByNode(currNode) * maxEff));
                                    }                                    
                                    // If this node is locked set the current Consumption locked in previous node as well 
                                    // So we know we can distribute quanities on other nodes
                                    if (currNode.getLocked()) {
                                        rpnPrev.setLockedConsumptionByNode(currNode, (int) Math.ceil(rpnPrev.getBaseConsumptionByNode(currNode) * maxEff));
                                    }
                                    
                                    // rpnPrev.setActConsumption((int) Math.ceil(rpnPrev.getBaseConsumption() * maxEff));
                                    addLogEntry(rpnPrev.ressource, "Set ActConsumption to " + (int) Math.ceil(rpnPrev.getBaseConsumptionByNode(currNode) * maxEff) + " for " + currNode.ressource + " in node " + rpnPrev.ressource + " (BaseConsumption * maxEff)");
                                } else {
                                    if (debug) {
                                        log.debug("No base consumption node found for resource " + currNode.ressource);
                                    }
                                }
                            }

                            /*
                             if (!rDAO.findRessourceById(rpnPrev.ressource).getMineable()) {
                             long freeCapacity = rpnPrev.getMaxStorage() - (rpnPrev.getActStorage() - rpnPrev.getActConsumption());
                             long toProduce = Math.min(freeCapacity, rpnPrev.getMaxProductionByPrevious());
                             if (debug) {
                             log.debug("Set new production for " + rpnPrev.ressource + " -- FREECAP: " + freeCapacity + " TOPROD: " + toProduce);
                             }
                            
                             rpnPrev.setActProduction(toProduce);
                             addLogEntry(rpnPrev.ressource, "Set ActProduction to " + rpnPrev.getActProduction() + ", which is maxQty in respect to storage and production of previous node");
                             }
                             */
                        } else {
                            if (rpnPrev.getBaseConsumptionByNode(currNode) != null) {
                                rpnPrev.setActConsumptionByNode(currNode, rpnPrev.getBaseConsumptionByNode(currNode));
                                addLogEntry(rpnPrev.ressource, "Set ActConsumption to " + rpnPrev.getBaseConsumptionByNode(currNode) + " for " + currNode.ressource + " in node " + rpnPrev.ressource + " as next node has no production");
                            } else {
                                if (debug) {
                                    log.debug("No base consumption node found for resource " + currNode.ressource);
                                }
                            }
                        }
                    } else {
                        if (currNode.type != RessProdNode.NODE_TYPE_AGRICULTURE) {
                            rpnPrev.setActConsumptionByNode(currNode, 0);
                            // TODO: check if this process on setActConsumption for ore is correct 
                            //       Should be covered by ActConsumptionByNode
                            //       Also needs to be considered when next node is only partially active
                            if (rpnPrev.getPreviousNodes().isEmpty()) {
                                rpnPrev.setActConsumption(0l);
                            }
                            // If this node is locked set the current Consumption locked in previous node as well 
                            // So we know we can distribute quanities on other nodes
                            if (currNode.getLocked()) {
                                rpnPrev.setLockedConsumptionByNode(currNode, 0);
                            }                            
                            // rpnPrev.setActConsumption(0);
                            addLogEntry(rpnPrev.ressource, "Set ActConsumption to 0 for " + currNode.ressource + " in node " + rpnPrev.ressource + " as next node has no production");
                        }
                    }
                }

                for (RessProdNode rpnPrev : currNode.getPreviousNodes()) {
                    if (!rDAO.findById(rpnPrev.ressource).getMineable()) {
                        if ((rpnPrev.getActStorage() + rpnPrev.getActProduction() - rpnPrev.getActConsumption()) > rpnPrev.getMaxStorage()) {
                            if (debug) {
                                log.debug("Limit production due to stock for ress " + rpnPrev.ressource);
                            }
                            rpnPrev.setActProduction((long) Math.min(rpnPrev.getMaxStorage() - rpnPrev.getActStorage() + rpnPrev.getActConsumption(), rpnPrev.getMaxProductionByPrevious()));
                            addLogEntry(rpnPrev.ressource, "Set ActProduction to " + rpnPrev.getActConsumption() + " which in respect to storage of current node and production of previous");
                        }
                    }
                }
            }
            
            if (reprocess) {
                // Debug Processing Stack
                for (RessProdNode rpn : processingStack) {
                    if (debug) addLogEntry(rpn.ressource," I'm inside processing Stack");
                }

                traverseForward(reprocessNode,null);
            }
            
        } while (reprocess);
    }

    private void resetTreeBranch(RessProdNode startingNode) {
        if (debug) {
            log.debug("Remove node " + startingNode.ressource + " from processing Stack");
        }
        processingStack.remove(startingNode);

        // prepare current Node
        // Set consumption to Locked Qty + Maximum Qty by following node
        Long calculatedConsumption = startingNode.getFixedConsumption();
        if (debug) addLogEntry(startingNode.ressource," Add fixed consumption of " + calculatedConsumption + " by general");
        
        for (RessProdNode next : startingNode.getNextNodes()) {   
            if (startingNode.getLockedConsumptionByNode(next) != null) {
                calculatedConsumption += startingNode.getLockedConsumptionByNode(next);
                if (debug) addLogEntry(startingNode.ressource," Add locked consumption of " + startingNode.getLockedConsumptionByNode(next) + " by node " + next.ressource);
            } else {
                Long baseCons = startingNode.getBaseConsumptionByNode(next);
                
                if (baseCons != null) {
                    calculatedConsumption += baseCons;                
                    if (debug) addLogEntry(startingNode.ressource," Add base consumption of " + startingNode.getBaseConsumptionByNode(next) + " by node " + next.ressource);
                }
            }
        }
                
        startingNode.setActConsumption(calculatedConsumption);
        if (debug) addLogEntry(startingNode.ressource," Set new act consumption of " + calculatedConsumption);
        
        for (RessProdNode next : startingNode.getNextNodes()) {                        
            if (next.getLocked()) {
                continue;
            }

            // Prepare nextNode
            next.setActProduction(next.getBaseProduction());
            
            resetTreeBranch(next);
        }
    }

    private void init() throws Exception {
        if (debug) {
            log.debug("--------------------------------------------------");
        }
        ArrayList<at.darkdestiny.core.model.PlanetRessource> prList = null;

        if (updateProcessing) {
            prList = uds.getRessEntriesForPlanet(pp.getPlanetId());
        } else {
            prList = prDAO.findByPlanetId(pp.getPlanetId());
        }

        for (at.darkdestiny.core.model.PlanetRessource pr : prList) {
            if ((pr.getType() == EPlanetRessourceType.INSTORAGE) || (pr.getType() == EPlanetRessourceType.PLANET)) {
                rpt.getNode(pr.getRessId()).setActStorage(pr.getQty());
                addLogEntry(pr.getRessId(), "Set ActStorage to " + pr.getQty());
            } else if (pr.getType() == EPlanetRessourceType.MINIMUM_PRODUCTION) {
                rpt.getNode(pr.getRessId()).setMinStorage(pr.getQty());
                addLogEntry(pr.getRessId(), "Set MinStorage to " + pr.getQty());
            }
        }

        if (calcFuture) {
            ArrayList<Action> aList = aDAO.findRunningConstructions(pp.getPlanetId());
            for (Action a : aList) {
                Construction c = cDAO.findById(a.getConstructionId());
                addConstructionToTree(c, a.getNumber(), a.getNumber());
            }
        }

        ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(pp.getPlanetId());
        for (PlanetConstruction pc : pcList) {
            Construction c = cDAO.findById(pc.getConstructionId());

            if (getPlanetEconomySurface().containsKey(c.getEconType())) {
                getPlanetEconomySurface().put(c.getEconType(), getPlanetEconomySurface().get(c.getEconType()) + pc.getNumber() * c.getSurface());
                getPlanetEconomyWorker().put(c.getEconType(), (int) (getPlanetEconomyWorker().get(c.getEconType()) + pc.getNumber() * c.getWorkers()));
            } else {
                getPlanetEconomySurface().put(c.getEconType(), pc.getNumber() * c.getSurface());
                getPlanetEconomyWorker().put(c.getEconType(), (int) (pc.getNumber() * c.getWorkers()));
            }

            addConstructionToTree(c, pc.getActiveCount(), pc.getNumber());
        }

        // When tree is filled go down from root and set values to maxmimum
        for (RessProdNode rpn : rpt.getRootNodes()) {
            RessProdNode rpnTmp = rpn;
            boolean firstLoop = true;

            do {
                if (firstLoop) {
                    firstLoop = false;
                } else {
                    rpnTmp = rpnTmp.getNext();
                }

                at.darkdestiny.core.model.Ressource r = rDAO.findRessourceById(rpnTmp.ressource);

                if (debug) {
                    log.debug("Process Ressource Node " + rpnTmp.ressource);                // Handle mineable ressources as they get ressource values from Planet
                }
                if (r.getMineable()) {
                    at.darkdestiny.core.model.PlanetRessource pr = null;
                    if (updateProcessing) {
                        pr = uds.getPlanetRessEntry(pp.getPlanetId(), EPlanetRessourceType.PLANET, r.getId());
                    } else {
                        pr = prDAO.findBy(pp.getPlanetId(), r.getId(), EPlanetRessourceType.PLANET);
                    }

                    // Set consumption to zero as there are no further ore deposits
                    // Decrease Consumption and Production of following node
                    if (pr == null) {
                        rpnTmp.setActConsumption(0);
                        addLogEntry(rpnTmp.ressource, "Set ActConsumption to 0 (due to no ore available)");
                        if (rpnTmp.getNext() != null) {
                            if (debug) {
                                log.debug("Set Production for " + rpnTmp.getNext().ressource + " [" + pp.getPlanetId() + "] to 0");
                            }
                            rpnTmp.getNext().setActProduction(0);
                            rpnTmp.getNext().setMaxProductionByPrevious(0);
                            addLogEntry(rpnTmp.getNext().ressource, "Set ActProduction to 0 (due to no ore on previous)");
                            addLogEntry(rpnTmp.getNext().ressource, "Set MaxProductionByPrevious to 0 (due to no ore on previous)");
                            // rpnTmp.getNext().setBaseProduction(VALUETYPE_PROD);
                        }

                        continue;
                    }

                    // Set consumption to new limit due to low ore deposits
                    // Decrease Consumption and Production of following node
                    if (pr.getQty() < rpnTmp.getActConsumption()) {
                        double maxEff = 1d / rpnTmp.getActConsumption() * pr.getQty();
                        rpnTmp.setActConsumption(Math.max(1, pr.getQty()));

                        addLogEntry(rpnTmp.ressource, "Set ActConsumption to " + pr.getQty() + " (due to low ore)");

                        if (rpnTmp.getNext() != null) {
                            long actProduction = (long) Math.max(1l, (long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                            rpnTmp.getNext().setActProduction(actProduction);
                            addLogEntry(rpnTmp.getNext().ressource, "Set ActProduction to " + rpnTmp.getNext().getActProduction() + " (due to low ore on previous)");
                            rpnTmp.getNext().setMaxProductionByPrevious(actProduction);
                            addLogEntry(rpnTmp.getNext().ressource, "Set MaxProductionByPrevious to " + rpnTmp.getNext().getMaxProductionByPrevious() + " (due to low ore on previous)");
                        }
                    }
                } else {
                    if (!((rpnTmp.type == RessProdNode.NODE_TYPE_RESEARCH)
                            || (rpnTmp.type == RessProdNode.NODE_TYPE_CONSTRUCTION))) {
                        // Limit production due to full stock
                        if (debug) {
                            log.debug("[" + rpnTmp.ressource + "] ACTSTORAGE=" + rpnTmp.getActStorage() + " ACTPROD=" + rpnTmp.getActProduction() + " ACTCONS=" + rpnTmp.getActConsumption() + " MAXSTORAGE=" + rpnTmp.getMaxStorage());
                        }

                        if ((rpnTmp.getActStorage() + rpnTmp.getActProduction() - rpnTmp.getActConsumption()) > rpnTmp.getMaxStorage()) {
                            if (debug) {
                                log.debug("Limit production due to stock for ress " + rpnTmp.ressource);
                            }
                            // double maxEff = 100d / rpnTmp.getActProduction() * (rpnTmp.getMaxStorage() - rpnTmp.getActStorage());
                            rpnTmp.setActProduction(rpnTmp.getMaxStorage() - rpnTmp.getActStorage() + rpnTmp.getActConsumption());
                            addLogEntry(rpnTmp.ressource, "Reduce ActProduction to " + rpnTmp.getActProduction() + " cause it exceeds maximum storage");
                        }

                        if ((rpnTmp.getMinStorage() > 0) && (rpnTmp.getNext() != null)) {
                            if (debug) {
                                log.debug("MINSTOCK >> ");
                                log.debug("STORAGE: " + rpnTmp.getActStorage() + " PROD: " + rpnTmp.getActProduction() + " CONS: " + rpnTmp.getActConsumption());
                                log.debug("COMPARE: " + (rpnTmp.getActStorage() + rpnTmp.getActProduction() - rpnTmp.getActConsumption()) + " <=> " + rpnTmp.getMinStorage());
                            }
                            if (rpnTmp.getActStorage() + rpnTmp.getActProduction() - rpnTmp.getActConsumption() < rpnTmp.getMinStorage()) {
                                double maxCons = Math.max(0, rpnTmp.getActStorage() + rpnTmp.getActProduction() - rpnTmp.getMinStorage());
                                if (debug) {
                                    log.debug("MAXCONS: " + maxCons);
                                }
                                double maxEff = 1d / rpnTmp.getActConsumption() * maxCons;

                                if (maxEff < 1d) {
                                    if (debug) {
                                        log.debug("Set Act Cons of " + r.getName() + " to " + maxCons);
                                        log.debug("Max Eff of next node = " + maxEff);
                                        log.debug("Set Production of next node to " + (long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                                    }

                                    rpnTmp.setActConsumption((long) maxCons);
                                    rpnTmp.getNext().setActProduction((long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                                    rpnTmp.getNext().setMaxProductionByPrevious((long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                                    addLogEntry(rpnTmp.ressource, "Set ActConsumption to " + maxCons + " due to minStorage");
                                    addLogEntry(rpnTmp.getNext().ressource, "Set ActProduction to " + rpnTmp.getNext().getActProduction() + " due to minStorage limitation on previous node");
                                    addLogEntry(rpnTmp.getNext().ressource, "Set MaxProductionByPrevious to " + rpnTmp.getNext().getMaxProductionByPrevious() + " due to minStorage limitation on previous node");
                                }
                            }
                        }

                        if (rpnTmp.type != RessProdNode.NODE_TYPE_AGRICULTURE) {
                            if (rpnTmp.getActConsumption() > (rpnTmp.getActProduction() + rpnTmp.getActStorage())) {
                                if (debug) {
                                    log.debug("Check if " + r.getName() + " Consumption (" + rpnTmp.getActConsumption() + ") is larger than PROD + STORAGE (" + (rpnTmp.getActProduction() + rpnTmp.getActStorage()) + ")");
                                }
                                double maxEff = 1d / rpnTmp.getActConsumption() * (rpnTmp.getActProduction() + rpnTmp.getActStorage());
                                if (rpnTmp.getNext() != null) {
                                    rpnTmp.getNext().setActProduction((long) Math.floor(rpnTmp.getNext().getActProduction() * maxEff));
                                    addLogEntry(rpnTmp.getNext().ressource, "Set ActProduction to " + rpnTmp.getNext().getActProduction() + " as production and storage of previous node is lower than consumption");
                                }
                                rpnTmp.setActConsumption((rpnTmp.getActProduction() + rpnTmp.getActStorage()));
                                addLogEntry(rpnTmp.ressource, "Set ActConsumption to " + rpnTmp.getActConsumption() + " cause Consumption higher than production + storage");
                            }
                        } else {
                            double maxConsByProdAndStock = rpnTmp.getActProduction() + rpnTmp.getActStorage();

                            rpnTmp.setActConsumption(rpnTmp.getBaseConsumption());
                            addLogEntry(rpnTmp.ressource, "Set ActConsumption to " + rpnTmp.getActConsumption() + " (BaseConsumption) as we have enough food to meet demands");
                            rpnTmp.setActConsumption((long) Math.min(maxConsByProdAndStock, rpnTmp.getActConsumption()));
                            addLogEntry(rpnTmp.ressource, "Limit ActConsumption to " + rpnTmp.getActConsumption() + " which is either BaseConsumption or available Qty in Stock + production");
                        }
                    }
                }
            } while (rpnTmp.getNext() != null);

            // If the last node is reached we check the production of this node and update backwards
            firstLoop = true;
            do {
                if (firstLoop) {
                    firstLoop = false;
                } else {
                    rpnTmp = rpnTmp.getPrevious();
                }

                if (debug) {
                    log.debug("Backward Check " + rpnTmp.ressource);
                }

                RessProdNode rpnPrev = rpnTmp.getPrevious();
                if (rpnPrev == null) {
                    break;
                }

                // Scale consumption of previous node if this node has decreased production
                if (rpnTmp.getActProduction() > 0) {
                    if (debug) {
                        log.debug("Previous node has production > 0");
                    }

                    if (rpnTmp.getActProduction() < rpnTmp.getBaseProduction()) {
                        double maxEff = 1d / rpnTmp.getBaseProduction() * rpnTmp.getActProduction();
                        if (debug) {
                            log.debug("Previous prduction is scaled down to " + maxEff + " (use on " + rpnPrev.getBaseConsumption() + ")");
                        }

                        if (rpnTmp.type != RessProdNode.NODE_TYPE_AGRICULTURE) {
                            rpnPrev.setActConsumption((int) Math.ceil(rpnPrev.getBaseConsumption() * maxEff));
                            addLogEntry(rpnPrev.ressource, "Set ActConsumption to " + rpnPrev.getActConsumption() + " (BaseConsumption * maxEff)");
                        }

                        if (!rDAO.findRessourceById(rpnPrev.ressource).getMineable()) {
                            long freeCapacity = rpnPrev.getMaxStorage() - (rpnPrev.getActStorage() - rpnPrev.getActConsumption());
                            long toProduce = Math.min(freeCapacity, rpnPrev.getMaxProductionByPrevious());
                            if (debug) {
                                log.debug("Set new production for " + rpnPrev.ressource + " -- FREECAP: " + freeCapacity + " TOPROD: " + toProduce);
                            }
                            rpnPrev.setActProduction(toProduce);
                            addLogEntry(rpnPrev.ressource, "Set ActProduction to " + rpnPrev.getActProduction() + ", which is maxQty in respect to storage and production of previous node");
                        }
                    }
                } else {
                    if (rpnTmp.type != RessProdNode.NODE_TYPE_AGRICULTURE) {
                        rpnPrev.setActConsumption(0);
                        addLogEntry(rpnPrev.ressource, "Set ActConsumption to 0 as next node has no production");
                    }
                }

                if (!rDAO.findRessourceById(rpnPrev.ressource).getMineable()) {
                    if ((rpnPrev.getActStorage() + rpnPrev.getActProduction() - rpnPrev.getActConsumption()) > rpnPrev.getMaxStorage()) {
                        if (debug) {
                            log.debug("Limit production due to stock for ress " + rpnPrev.ressource);
                        }
                        rpnPrev.setActProduction((long) Math.min(rpnPrev.getMaxStorage() - rpnPrev.getActStorage() + rpnPrev.getActConsumption(), rpnPrev.getMaxProductionByPrevious()));
                        addLogEntry(rpnPrev.ressource, "Set ActProduction to " + rpnPrev.getActConsumption() + " which in respect to storage of current node and production of previous");
                    }
                }
            } while (rpnTmp.getPrevious() != null);
        }
    }

    private void addConstructionToTree(Construction c, int count, int totalCount) {
        ArrayList<Production> pList = pDAO.findProductionForConstructionId(c.getId());

        boolean megacity = false;
        /*
         if (c.getId() == Construction.ID_MEGACITY) {
         DebugBuffer.trace("Process Megacity");
         megacity = true;
         }
         */

        RessProdNode consumer = null;

        // TODO: Find the consuming node -- very inefficient should be changed in future
        for (Production p : pList) {
            if (p.getDecinc() == 0) { // 0 is production, we need to get the corresponding node for this resource
                if (debug) {
                    log.debug("Found production of resource " + p.getRessourceId());
                }
                RessProdNode rpn = rpt.getNode(p.getRessourceId());
                if (rpn.type != RessProdNode.NODE_TYPE_INDUSTRY) {
                    continue;
                } else {
                    consumer = rpn;
                    if (debug) {
                        log.debug("Consumer node with resource " + consumer.ressource + " found!");
                    }
                    break;
                }
            }
        }

        for (Production p : pList) {
            // prDAO.findBy(pp.getPlanetId(), p.getRessourceId(), EPlanetRessourceType.MINIMUM);                        
            if (p.getDecinc() == Production.PROD_STORAGE) {
                // if (megacity) DebugBuffer.trace("Process STORAGE");
                RessProdNode rpn = rpt.getNode(p.getRessourceId());

                if (rpn != null) {
                    long qty = p.getQuantity();

                    if (isAgriculturalPlanet
                            && (p.getRessourceId() == Ressource.FOOD)) {
                        qty = (long) (qty * 1.5d);
                    }

                    rpn.setMaxStorage(rpn.getMaxStorage() + (qty * totalCount));
                    addLogEntry(rpn.ressource, "(Storage) SetMaxStorage to " + rpn.getMaxStorage());
                }
            } else if (p.getDecinc() == Production.PROD_INCREASE) {
                // if (megacity) DebugBuffer.trace("Process INCREASE");
                RessProdNode rpn = rpt.getNode(p.getRessourceId());
                // if (megacity) DebugBuffer.trace("RPN is " + rpn);    

                if (rpn != null) {
                    long qty = p.getQuantity();

                    if (hasDesintegratorBonus
                            && ((p.getRessourceId() == Ressource.IRON)
                            || (p.getRessourceId() == Ressource.YNKELONIUM))) {
                        qty = (long) (qty * 1.2d);
                    }

                    if (hasMiningBonus
                            && ((p.getRessourceId() == Ressource.IRON)
                            || (p.getRessourceId() == Ressource.YNKELONIUM)
                            || (p.getRessourceId() == Ressource.HOWALGONIUM))) {
                        qty = (long) (qty * 1.25d);
                    }

                    if (isAgriculturalPlanet
                            && (p.getRessourceId() == Ressource.FOOD)) {
                        qty = (long) (qty * 1.3d);
                    }

                    if (industrialPlanet) {
                        if ((p.getRessourceId() == Ressource.MODUL_AP)
                                || (p.getRessourceId() == Ressource.ORB_DOCK)
                                || (p.getRessourceId() == Ressource.PL_DOCK)
                                || (p.getRessourceId() == Ressource.STEEL)
                                || (p.getRessourceId() == Ressource.TERKONIT)
                                || (p.getRessourceId() == Ressource.CVEMBINIUM)) {
                            qty = (long) (qty * 1.3d);
                        }
                    }

                    if ((planet.getLandType().equalsIgnoreCase(Planet.LANDTYPE_J))
                            && (p.getRessourceId() == Ressource.IRON)) {
                        qty = (long) (qty * 1.25d);
                    }

                    // Solar planet
                    if (c.getId() == Construction.ID_SOLARPLANT) {
                        if (p.getRessourceId() == Ressource.ENERGY) {
                            int orbitLevel = planet.getOrbitLevel();
                            float factor = 1f;

                            if (orbitLevel < 6) {
                                factor += (6f - orbitLevel) * 0.1f;
                            } else if (orbitLevel > 6) {
                                factor -= (orbitLevel - 6f) * 0.1f;
                            }

                            qty = (long) Math.floor((float) qty * factor);
                        }
                    }

                    // ResearchBonus
                    if ((p.getRessourceId() == Ressource.RP) || (p.getRessourceId() == Ressource.RP_COMP)) {
                        BonusResult br = BonusUtilities.findBonus(planet.getId(), EBonusType.RESEARCH);
                        if (br != null) {
                            long inc = (long) (qty * br.getPercBonus());
                            inc += br.getNumericBonus();

                            qty += inc;
                        }
                    } else if ((p.getRessourceId() == Ressource.MODUL_AP)
                            || (p.getRessourceId() == Ressource.ORB_DOCK)
                            || (p.getRessourceId() == Ressource.PL_DOCK)) {
                        BonusResult br = BonusUtilities.findBonus(planet.getId(), EBonusType.PRODUCTION);
                        if (br != null) {
                            long inc = (long) (qty * br.getPercBonus());
                            inc += br.getNumericBonus();

                            qty += inc;
                        }
                    }

                    rpn.setBaseProduction((long) (rpn.getBaseProduction() + qty * count * moraleAdjust));
                    rpn.setActProduction(rpn.getBaseProduction());
                    rpn.setMaxProductionByPrevious(rpn.getBaseProduction());
                    addLogEntry(rpn.ressource, "(AddConstruction/INCREASE) Set BaseProduction to " + rpn.getBaseProduction());
                    addLogEntry(rpn.ressource, "(AddConstruction/INCREASE) Set ActProduction to " + rpn.getActProduction());
                    addLogEntry(rpn.ressource, "(AddConstruction/INCREASE) Set MaxProductionByPrevious to " + rpn.getMaxProductionByPrevious());
                }
            } else if (p.getDecinc() == Production.PROD_DECREASE) {
                RessProdNode rpn = rpt.getNode(p.getRessourceId());

                if (rpn != null) {
                    // This only matches for Energy Node
                    if (rpn instanceof RessProdNodeSplitted) {
                        RessProdNodeSplitted rpns = (RessProdNodeSplitted) rpn;
                        // log.debug("Add energy consumption of " + (p.getQuantity() * count) + " for ProdType " + c.getProdType() + "["+c.getName()+"]");
                        rpns.addBaseConsumption(p.getQuantity() * count, c.getProdType());
                        addLogEntry(rpns.ressource, "(Energy) Add baseConsumption of " + (p.getQuantity() * count) + " for ProdType " + c.getProdType());
                    }

                    long qty = p.getQuantity();

                    if (hasDesintegratorBonus
                            && ((p.getRessourceId() == Ressource.PLANET_IRON)
                            || (p.getRessourceId() == Ressource.PLANET_YNKELONIUM))) {
                        qty = (long) (qty * 1.2d);
                    }

                    if (hasMiningBonus
                            && ((p.getRessourceId() == Ressource.PLANET_IRON)
                            || (p.getRessourceId() == Ressource.PLANET_YNKELONIUM)
                            || (p.getRessourceId() == Ressource.PLANET_HOWALGONIUM))) {
                        qty = (long) (qty * 1.75d);
                    }

                    if (industrialPlanet) {
                        if ((p.getRessourceId() == Ressource.IRON)
                                || (p.getRessourceId() == Ressource.STEEL)
                                || (p.getRessourceId() == Ressource.TERKONIT)
                                || (p.getRessourceId() == Ressource.YNKELONIUM)) {
                            qty = (long) (qty * 1.3d);
                        }
                    }

                    if ((planet.getLandType().equalsIgnoreCase(Planet.LANDTYPE_J))
                            && (p.getRessourceId() == Ressource.PLANET_IRON)) {
                        qty = (long) (qty * 1.25d);
                    }

                    // Do not decrease Energy consumption if morale is low
                    if (!(rpn instanceof RessProdNodeSplitted)) {
                        rpn.setBaseConsumption((long) (rpn.getBaseConsumption() + qty * count * moraleAdjust));
                        if (consumer != null) {
                            long previousConsumption = 0l;
                            if (rpn.getBaseConsumptionByNode(consumer) != null) {
                                previousConsumption = rpn.getBaseConsumptionByNode(consumer);
                            }
                            if (debug) {
                                log.debug("[1.1] Set base consumption of node " + consumer.ressource + " in node " + rpn.ressource + " (Qty: " + (long) (previousConsumption + qty * count) + ")");
                            }

                            rpn.setBaseConsumptionByNode(consumer, (long) (previousConsumption + qty * count));
                        } else {                          
                            rpn.setFixedConsumption(rpn.getFixedConsumption() + (long) (qty * count));                            
                        }
                    } else {
                        rpn.setBaseConsumption((long) (rpn.getBaseConsumption() + qty * count));
                        if (consumer != null) {
                            long previousConsumption = 0l;
                            if (rpn.getBaseConsumptionByNode(consumer) != null) {
                                previousConsumption = rpn.getBaseConsumptionByNode(consumer);
                            }
                            if (debug) {
                                log.debug("[2.1]Set base consumption of node " + consumer.ressource + " in node " + rpn.ressource + " (Qty: " + (long) (previousConsumption + qty * count) + ")");
                            }

                            rpn.setBaseConsumptionByNode(consumer, (long) (previousConsumption + qty * count));
                        } else {                 
                            rpn.setFixedConsumption(rpn.getFixedConsumption() + (long) (qty * count));
                        }
                    }
                    rpn.setActConsumption(rpn.getBaseConsumption());
                    addLogEntry(rpn.ressource, "(AddConstruction/DECREASE) Set BaseConsumption to " + rpn.getBaseConsumption());
                    addLogEntry(rpn.ressource, "(AddConstruction/DECREASE) Set ActConsumption to " + rpn.getActConsumption());
                }
            }
        }
    }

    private void calculateBoni(PlayerPlanet pp) {
        hasDesintegratorBonus = (pResDAO.findBy(pp.getUserId(), 106) != null);
        isAgriculturalPlanet = (pp.getColonyType() == EColonyType.AGRICULTURAL_COLONY);
        hasMiningBonus = (abDAO.findByPlanetAndBonusId(pp.getPlanetId(), 13) != null);
        industrialPlanet = (pp.getColonyType() == EColonyType.INDUSTRIAL_COLONY);
    }

    protected long getValue(int ressId, int valueType) {
        RessProdNode rpn = rpt.getNode(ressId);

        if (rpn == null) {
            return 0;
        }
        switch (valueType) {
            case (VALUETYPE_PROD):
                return rpn.getActProduction();
            case (VALUETYPE_CONS):
                return rpn.getActConsumption();
            case (VALUETYPE_STOCK):
                return rpn.getActStorage();
            case (VALUETYPE_MAXSTOCK):
                return rpn.getMaxStorage();
            case (VALUETYPE_MINSTOCK):
                return rpn.getMinStorage();
            default:
                return 0;
        }
    }

    protected RessProdNode getNode(int ressId) {
        return rpt.getNode(ressId);
    }

    protected RessProdTree getProdTree() {
        return rpt;
    }

    protected ProductionResult getProduction() {
        ProductionResult pr = new ProductionResult(rpt);
        pr.setCalcLog(calcLog);
        return pr;
    }

    private void addLogEntry(int ressId, String message) {
        if (!debug) {
            return;
        }

        Ressource r = rDAO.findRessourceById(ressId);
        message = "[" + r.getName() + "] " + message;

        log.debug(message);

        if (calcLog.containsKey(ressId)) {
            calcLog.get(ressId).add(message);
        } else {
            ArrayList<String> messageList = new ArrayList<String>();
            messageList.add(message);
            calcLog.put(ressId, messageList);
        }
    }

    private void printLogEntry(int ressId) {
        ArrayList<String> logEntries = calcLog.get(ressId);

        if (logEntries != null) {
            for (String entry : logEntries) {
                System.out.println("RESSLOG: " + entry);
            }
        }
    }

    /**
     * @return the planetEconomySurface
     */
    protected Map<EEconomyType, Integer> getPlanetEconomySurface() {
        return planetEconomySurface;
    }

    /**
     * @return the planetEconomyWorker
     */
    protected Map<EEconomyType, Integer> getPlanetEconomyWorker() {
        return planetEconomyWorker;
    }
}
