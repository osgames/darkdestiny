/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.Buildable;

/**
 *
 * @author Stefan
 */
public abstract class UpgradeOrder {
    private final Buildable fromUnit;
    private final Buildable toUnit;
    private final int count;
    protected int userId;

    protected UpgradeOrder(Buildable fromUnit, Buildable toUnit, int count) {
        this.fromUnit = fromUnit;
        this.toUnit = toUnit;
        this.count = count;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the fromUnit
     */
    public Buildable getFromUnit() {
        return fromUnit;
    }

    /**
     * @return the toUnit
     */
    public Buildable getToUnit() {
        return toUnit;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }
}
