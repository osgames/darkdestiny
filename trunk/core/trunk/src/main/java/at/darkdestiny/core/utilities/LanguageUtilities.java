/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.utilities;

import at.darkdestiny.core.dao.LanguageDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.model.User;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.Locale;

/**
 *
 * @author Stefan
 */
public class LanguageUtilities {
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static LanguageDAO lDAO = (LanguageDAO) DAOFactory.get(LanguageDAO.class);

    public static Locale getMasterLocaleForUser(int userId) {
        User u = uDAO.findById(userId);

        Language actLang = lDAO.findBy(u.getLocale());
        Language baseLang = lDAO.getMasterLanguageOf(actLang.getId());

        Locale l = new Locale(baseLang.getLanguage(),baseLang.getCountry().toUpperCase());
        return l;
    }
}
