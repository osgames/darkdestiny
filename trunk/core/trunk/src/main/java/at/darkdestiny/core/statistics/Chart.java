/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.statistics;

import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.AbstractDataset;

/**
 *
 * @author Aion
 */
public class Chart{
    private AbstractDataset dataset;
    private String title;
    private String xTitle;
    private String yTitle;
    private PlotOrientation orientation;
    private boolean legend;
    private boolean tooltips;
    private boolean urls;
    private String name;
    private int width;
    private int height;

    public Chart(AbstractDataset dataset, String title, String xTitle, String yTitle, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls, String name, int width, int height) {
        this.dataset = dataset;
        this.title = title;
        this.xTitle = xTitle;
        this.yTitle = yTitle;
        this.orientation = orientation;
        this.legend = legend;
        this.tooltips = tooltips;
        this.urls = urls;
        this.name = name;
        this.width = width;
        this.height = height;
    }


    /**
     * @return the dataset
     */
    public AbstractDataset getDataset() {
        return dataset;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the legend
     */
    public boolean isLegend() {
        return legend;
    }

    /**
     * @return the tooltips
     */
    public boolean isTooltips() {
        return tooltips;
    }

    /**
     * @return the urls
     */
    public boolean isUrls() {
        return urls;
    }

    /**
     * @return the xTitle
     */
    public String getxTitle() {
        return xTitle;
    }

    /**
     * @return the yTitle
     */
    public String getyTitle() {
        return yTitle;
    }

    /**
     * @return the orientation
     */
    public PlotOrientation getOrientation() {
        return orientation;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

}
