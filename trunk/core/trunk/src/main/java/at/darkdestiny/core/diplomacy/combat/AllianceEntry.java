/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy.combat;

import at.darkdestiny.core.model.DiplomacyRelation;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class AllianceEntry implements CombatParticipent {
    private final int allianceId;
    private AllianceEntry masterEntry = null;
    
    private ArrayList<AllianceEntry> subAlliances = new ArrayList<AllianceEntry>();
    private ArrayList<AllianceMemberEntry> members = new ArrayList<AllianceMemberEntry>();
    
    private HashMap<AllianceEntry,DiplomacyRelation> relationsToAlliance = 
            new HashMap<AllianceEntry,DiplomacyRelation>();
    private HashMap<PlayerEntry,DiplomacyRelation> relationsToPlayer = 
            new HashMap<PlayerEntry,DiplomacyRelation>();    
    
    protected AllianceEntry(int allianceId) {
        this.allianceId = allianceId;
    }        
    
    @Override
    public ArrayList<Integer> getAllMemberIds() {
        ArrayList<Integer> userIds = new ArrayList<Integer>();

        for (AllianceMemberEntry ame : getMembers()) {
            userIds.add(ame.getUserId());
        }

        /*
        for (AllianceEntry ae : subAlliances) {
            userIds.addAll(ae.getAllMemberIds());            
        }
        */
        
        return userIds;
    }

    @Override
    public int getId() {
        return allianceId;
    }

    @Override
    public DiplomacyRelation getRelationToAlliance(AllianceEntry ae) {
        return relationsToAlliance.get(ae);
    }

    @Override
    public DiplomacyRelation getRelationToPlayer(PlayerEntry pe) {
        return relationsToPlayer.get(pe);
    }
    
    @Override
    public DiplomacyRelation getRelationTo(CombatParticipent cp) {
        if (cp instanceof AllianceEntry) {
            if (this.getAllianceId() == ((AllianceEntry)cp).getAllianceId()) {
                DiplomacyRelation dr = new DiplomacyRelation();
                dr.setFromId(allianceId);
                dr.setToId(((AllianceEntry)cp).getAllianceId());
                dr.setDiplomacyTypeId(0);
                return dr;
            }

            return relationsToAlliance.get((AllianceEntry)cp);  
        } else {
            return relationsToPlayer.get((PlayerEntry)cp);    
        }        
    }    
    
    private void addSubAlliance(AllianceEntry ae) {
        subAlliances.add(ae);
    }
    
    protected void setMasterAlliance(AllianceEntry ae) {
        masterEntry = ae;
        ae.addSubAlliance(this);        
    }
    
    protected void addAllianceMember(AllianceMemberEntry ame) {
        getMembers().add(ame);
    }

    protected void addAllianceRelation(AllianceEntry to, DiplomacyRelation relation) {
        getRelationsToAlliance().put(to, relation);
    }    
    
    protected void addPlayerRelation(PlayerEntry to, DiplomacyRelation relation) {
        getRelationsToPlayer().put(to, relation);
    }    
    
    public int getAllianceId() {
        return allianceId;
    }

    public AllianceEntry getMasterEntry() {
        return masterEntry;
    }

    public HashMap<AllianceEntry, DiplomacyRelation> getRelationsToAlliance() {
        return relationsToAlliance;
    }

    public HashMap<PlayerEntry, DiplomacyRelation> getRelationsToPlayer() {
        return relationsToPlayer;
    }

    public ArrayList<AllianceMemberEntry> getMembers() {
        return members;
    }
}
