/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.tools.graph;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.scanner.AbstractCoordinate;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class DefaultNode extends AbstractCoordinate implements Node {
    private final String name;
    private final Object load;
    private ArrayList<Edge> edges = new ArrayList<Edge>();

    public DefaultNode(String name) {
        this(name, 0, 0);
    }
    public DefaultNode(String name, Object load) {
        this(name, 0, 0, load);
    }

    public DefaultNode(String name, int x, int y) {
        this(name, x, y, null);
    }

    public DefaultNode(String name, int x, int y, Object load) {
        super(x, y);
        this.name = name;
        this.load = load;
    }

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @return the o
     */
    @Override
    public Object getLoad() {
        return load;
    }

    /**
     * @return the edges
     */
    @Override
    public ArrayList<Edge> getEdges() {
        return edges;
    }

    @Override
    public void addEdge(Edge e) {
        edges.add(e);
    }

    @Override
    public AbsoluteCoordinate getAbsoluteCoordinate() {
        return new AbsoluteCoordinate(this.x,this.y);
    }
    @Override
    public String toColor(){
        return "gray";
    }
}
