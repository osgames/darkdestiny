package at.darkdestiny.core.title;

import at.darkdestiny.core.enumeration.EDiplomacyRelationType;
import at.darkdestiny.core.model.DiplomacyRelation;
import at.darkdestiny.core.model.DiplomacyType;
import at.darkdestiny.core.service.Service;


public class ThePolitician extends AbstractTitle {

	public boolean check(int userId) {
            int count = 0;
            for(DiplomacyRelation dr : Service.diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_USER, false)){
                if(dr.getDiplomacyTypeId() == DiplomacyType.NAP || dr.getDiplomacyTypeId() == DiplomacyType.TREATY){
                    count++;
                }
            }
            for(DiplomacyRelation dr : Service.diplomacyRelationDAO.findByFromId(userId, EDiplomacyRelationType.USER_TO_ALLIANCE, false)){
                if(dr.getDiplomacyTypeId() == DiplomacyType.NAP || dr.getDiplomacyTypeId() == DiplomacyType.TREATY){
                    count++;
                }
            }
            if(count >= 3){
                return true;
            }else{
                return false;
            }
	}
}
