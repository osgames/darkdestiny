/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin;

 import at.darkdestiny.core.dao.PlanetDAO;import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class RestorePopulation {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);

    private static final Logger log = LoggerFactory.getLogger(RestorePopulation.class);

    public static synchronized void restorePop() {
       ArrayList<PlayerPlanet> ppList = ppDAO.findAll();
       for (PlayerPlanet pp : ppList) {
           Planet p = pDAO.findById(pp.getPlanetId());
           if (!(p.getLandType().equalsIgnoreCase("M") ||
                p.getLandType().equalsIgnoreCase("C") ||
                p.getLandType().equalsIgnoreCase("G"))) {
                try {
                    PlanetCalculation pc = new PlanetCalculation(p.getId());
                    ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
                    long maxPop = epcr.getMaxPopulation();

                    log.debug("#" + p.getId() + " ["+pp.getName()+"]> Fixing planet type " + p.getLandType() + " -> MaxPop Calculated: " + maxPop);

                    pp.setPopulation(maxPop);
                    pp.setMoral(100);
                    ppDAO.update(pp);
                } catch (Exception e) {

                }
           }
       }
    }
}
