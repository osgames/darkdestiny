/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.buildable;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.construction.ConstructionAbortRefund;
import at.darkdestiny.core.construction.ConstructionScrapAbortCost;
import at.darkdestiny.core.construction.ConstructionScrapCost;
import at.darkdestiny.core.dao.ConstructionDAO;
import at.darkdestiny.core.dao.RessourceCostDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.ERessourceCostType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.RessourceCost;
import at.darkdestiny.core.ships.BuildTime;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ConstructionExt implements Buildable {
    ConstructionDAO cDAO = (ConstructionDAO) DAOFactory.get(ConstructionDAO.class);
    RessourceCostDAO rcDAO = (RessourceCostDAO) DAOFactory.get(RessourceCostDAO.class);

    private final Construction cons;
    private final RessourcesEntry re;

    public ConstructionExt(int id) {
        cons = cDAO.findById(id);

        ArrayList<RessAmountEntry> raeList = new ArrayList<RessAmountEntry>();
        ArrayList<RessourceCost> rcList = rcDAO.find(ERessourceCostType.CONSTRUCTION,id);

        for (RessourceCost rc : rcList) {
            RessAmountEntry rae = new RessAmountEntry(rc.getRessId(),rc.getQty());
            raeList.add(rae);
        }

        re = new RessourcesEntry(raeList);
    }

    @Override
    public RessourcesEntry getRessCost() {
        return re;
    }

    @Override
    public Model getBase() {
        return cons;
    }

    @Override
    public Class getModelClass(){
        return Construction.class;
    }

    //@TODO
    @Override
    public BuildTime getBuildTime() {
        return null;
    }

    public ConstructionAbortRefund getAbortRefund(int orderId) {
        return new ConstructionAbortRefund(this,orderId);
    }

    public ConstructionScrapAbortCost getScrapAbortCost(int orderId) {
        return new ConstructionScrapAbortCost(this,orderId);
    }

    public ConstructionScrapCost getScrapCost(int planetId) {
        return new ConstructionScrapCost(this,planetId);
    }

    /*
    public ConstructionScrapAbortCost getScrapAbortCost(int orderId) {

    }
    */
}
