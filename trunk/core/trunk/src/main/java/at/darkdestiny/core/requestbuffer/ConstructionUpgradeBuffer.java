/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.requestbuffer;

/**
 *
 * @author Stefan
 */
public class ConstructionUpgradeBuffer extends ParameterBuffer {
    public static final String CONSTRUCTION_ID = "consId";
    public static final String CONSTRUCTION_ID_TARGET = "consIdTarget";
    public static final String COUNT = "count";
    public static final String PLANET_ID = "planetId";

    public ConstructionUpgradeBuffer(int userId) {
        super(userId);
    }

    public int getConstructionId() {
        return (Integer)getParameter(CONSTRUCTION_ID);
    }

    public int getCount() {
        return (Integer)getParameter(COUNT);
    }

    public int getPlanetId() {
        return (Integer)getParameter(PLANET_ID);
    }
    public int getConstructionTargetId() {
        return (Integer)getParameter(CONSTRUCTION_ID_TARGET);
    }
}
