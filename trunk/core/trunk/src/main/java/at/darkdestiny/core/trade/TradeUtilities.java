/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.trade;

 import at.darkdestiny.core.GameUtilities;import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.TradeShipData;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.dao.TradeRouteDetailDAO;
import at.darkdestiny.core.dao.TradeRouteShipDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EAddLocation;
import at.darkdestiny.core.enumeration.EFleetViewType;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.enumeration.ETradeRouteStatus;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.exceptions.PlanetNameNotUniqueException;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.model.TradeRouteShip;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.FleetListResult;
import at.darkdestiny.core.result.TradeRouteResult;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.TradeShipData;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.dao.TradeRouteDetailDAO;
import at.darkdestiny.core.dao.TradeRouteShipDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EAddLocation;
import at.darkdestiny.core.enumeration.EFleetViewType;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.enumeration.ETradeRouteStatus;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.exceptions.PlanetNameNotUniqueException;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.model.TradeRouteShip;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.FleetListResult;
import at.darkdestiny.core.result.TradeRouteResult;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.ships.ShipData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class TradeUtilities {

    private static final Logger log = LoggerFactory.getLogger(TradeUtilities.class);

    private static TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    private static TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static TradeRouteShipDAO trsDAO = (TradeRouteShipDAO) DAOFactory.get(TradeRouteShipDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    public static final int TRANSMITTER_CAP = 10000;
    public static final int SUNTRANSMITTER_CAP = 50000;
    public static final int CONTAINERTERMINAL_CAP = 10000;
    public static final int TRANSMITTER_MAX_DISTANCE = 500;

    public static void createExternalTradeRoute(int userId, int startPlanetId, int targetPlanetId,
            ArrayList<TradeRouteDetail> routes) throws Exception {


        TradeRoute tr = new TradeRoute();

        tr.setStartPlanet(startPlanetId);
        tr.setTargetPlanet(targetPlanetId);
        tr.setUserId(userId);
        tr.setTargetUserId(ppDAO.findByPlanetId(targetPlanetId).getUserId());
        tr.setType(ETradeRouteType.TRANSPORT);

        // Calculate Distance
        RelativeCoordinate rc1 = new RelativeCoordinate(0, startPlanetId);
        RelativeCoordinate rc2 = new RelativeCoordinate(0, targetPlanetId);
        int distance = (int) Math.ceil(rc1.distanceTo(rc2));

        tr.setDistance((double) distance);
        tr.setStatus(ETradeRouteStatus.PENDING);

        GenerateMessage gm = new GenerateMessage();
        gm.setSourceUserId(userId);
        gm.setDestinationUserId(ppDAO.findByPlanetId(targetPlanetId).getUserId());
        String topic = ML.getMLStr("transport_msg_newofferreceived", ppDAO.findByPlanetId(targetPlanetId).getUserId());
        topic = topic.replace("%USER%", uDAO.findById(userId).getGameName());
        gm.setTopic(topic);
        String msg = ML.getMLStr("transport_msg_newofferreceived_msg", ppDAO.findByPlanetId(targetPlanetId).getUserId());
        msg = msg.replace("%USER%", uDAO.findById(userId).getGameName());

        gm.setMsg(msg);
        gm.setMessageContentType(EMessageContentType.DIPLOMACY);
        gm.setMessageType(EMessageType.USER);
        gm.setEscape(false);
        gm.writeMessageToUser();
        tr = trDAO.add(tr);

        int timeStarted = GameUtilities.getCurrentTick2();

        for (TradeRouteDetail trd : routes) {
            trd.setRouteId(tr.getId());
            trd.setTimeStarted(timeStarted);
        }
        trdDAO.insertAll(routes);
    }

    public static BaseResult createInternalTradeRoute(int userId, int startPlanetId, int targetPlanetId,
            ArrayList<TradeRouteDetail> routes) throws Exception {
        TradeRoute tr = new TradeRoute();
        boolean routeExists = false;
        int timeStarted = GameUtilities.getCurrentTick2();

        //A Route already exists
        if (trDAO.exists(startPlanetId, targetPlanetId, ETradeRouteType.TRANSPORT)) {
            routeExists = true;
            tr = trDAO.findBy(startPlanetId, targetPlanetId, ETradeRouteType.TRANSPORT);
        } else {

            tr.setStartPlanet(startPlanetId);
            tr.setTargetPlanet(targetPlanetId);
            tr.setUserId(userId);

            // Calculate Distance
            int distance = 0;

            RelativeCoordinate rc1 = new RelativeCoordinate(0, startPlanetId);
            RelativeCoordinate rc2 = new RelativeCoordinate(0, targetPlanetId);
            distance = (int) Math.ceil(rc1.distanceTo(rc2));

            tr.setDistance((double) distance);
            tr.setStatus(ETradeRouteStatus.ACTIVE);
            tr.setType(ETradeRouteType.TRANSPORT);

            tr = trDAO.add(tr);
        }

        boolean existResourcesFound = false;
        ArrayList<Integer> existResources = new ArrayList<Integer>();

        ArrayList<TradeRouteDetail> toAdd = new ArrayList<TradeRouteDetail>();

        //Check which resources already exist in this route
        for (TradeRouteDetail trd : routes) {
            TradeRouteDetail trdSearch = new TradeRouteDetail();
            trdSearch.setRessId(trd.getRessId());
            trdSearch.setRouteId(tr.getId());
            //Resource already exists
            if (trdDAO.find(trdSearch).size() > 0) {
                if (!existResourcesFound) {
                    existResourcesFound = true;
                }
                existResources.add(trd.getRessId());
            } else {
                trd.setTimeStarted(timeStarted);
                trd.setRouteId(tr.getId());
                toAdd.add(trd);
            }

        }
        trdDAO.insertAll(toAdd);

        String msg = "";
        if (existResourcesFound) {
            if (existResources.size() == 1) {
                msg += ML.getMLStr("transport_err_resourceexisted", userId);
                msg = msg.replace("%RES%", ML.getMLStr(rDAO.findRessourceById(existResources.get(0)).getName(), userId));
                msg += " ";
            } else {
                msg += ML.getMLStr("transport_err_resourcesexisted1", userId);
                boolean first = true;
                for (Integer i : existResources) {
                    if (first) {
                        msg += ML.getMLStr(rDAO.findRessourceById(existResources.get(0)).getName(), userId) + " ";
                    } else {
                        msg += "," + ML.getMLStr(rDAO.findRessourceById(existResources.get(0)).getName(), userId) + " ";
                    }
                }
                msg += "<BR>";
                msg += ML.getMLStr("transport_err_resourcesexisted2", userId);
            }
            return new BaseResult(msg, false);
        } else {
            if (routeExists) {
                return new BaseResult(ML.getMLStr("transport_msg_routemodified", userId), false);
            } else {
                return new BaseResult(ML.getMLStr("transport_msg_routecreated", userId), false);
            }
        }

    }

    public static ArrayList<TradeShipData> getShipsInRoute(int routeId) {
        ArrayList<TradeRouteShip> trsList = trsDAO.findByTradeRouteId(routeId);
        ArrayList<TradeShipData> sDataList = new ArrayList<TradeShipData>();

        TradeRoute tr = trDAO.getById(routeId);

        for (TradeRouteShip trs : trsList) {
            if (trs.getShipType() == TradeRouteShip.SHIPTYPE_NORMAL) {
                ShipDesign sd = sdDAO.findById(trs.getDesignId());

                RelativeCoordinate home = null;

                if (trs.getAddLocation() == EAddLocation.START) {
                    home = new RelativeCoordinate(0, tr.getStartPlanet());
                } else {
                    home = new RelativeCoordinate(0, tr.getTargetPlanet());
                }

                TradeShipData sData = new TradeShipData(home);

                if (sd == null) {
                    sData.setDesignId(0);
                    sData.setDesignName("NA");
                } else {
                    sData.setDesignId(sd.getId());
                    sData.setDesignName(sd.getName());
                }

                sData.setCount(trs.getNumber());
                sDataList.add(sData);
            }
        }

        return sDataList;
    }

    public static ArrayList<ShipData> getTradeShips(int userId, int routeId) {
        TradeRoute tr = trDAO.getById(routeId);

        boolean startOwnedByUser = ppDAO.findByPlanetId(tr.getStartPlanet()).getUserId() == userId;
        boolean targetOwnedByUser = ppDAO.findByPlanetId(tr.getTargetPlanet()).getUserId() == userId;

        ArrayList<PlayerFleetExt> pfExtList = new ArrayList<PlayerFleetExt>();

        if (startOwnedByUser) {
            FleetListResult flr1 = FleetService.getFleetList(userId, 0, tr.getStartPlanet(), EFleetViewType.ALL_PLANET);
            pfExtList.addAll(flr1.getAllContainedFleets());
        }

        if (targetOwnedByUser) {
            FleetListResult flr2 = FleetService.getFleetList(userId, 0, tr.getTargetPlanet(), EFleetViewType.ALL_PLANET);
            pfExtList.addAll(flr2.getAllContainedFleets());
        }

        ArrayList<ShipData> sdResult = new ArrayList<ShipData>();

        for (PlayerFleetExt pfExt : pfExtList) {
            ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(pfExt.getId());
            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());

                if (sd.getRessSpace() > 0) {
                    ShipData sData = new ShipData();
                    sData.setCount(sf.getCount());
                    sData.setDesignId(sd.getId());
                    sData.setDesignName(sd.getName());
                    sData.setFleetId(pfExt.getId());
                    sData.setFleetName(pfExt.getName());

                    sdResult.add(sData);
                }
            }
        }

        return sdResult;
    }

    public static TradeRouteResult getInternalTradeRoutes(int userId) {
        return getInternalTradeRoutes(userId, 0);
    }

    public static TradeRouteResult getInternalTradeRoutes(int userId, int planetId) {
        TradeRouteResult trr = new TradeRouteResult();

        log.debug("Internal");
        ArrayList<TradeRoute> trList = new ArrayList<TradeRoute>();
        if (planetId > 0) {
            trList = trDAO.findByUserIdInternal(userId, ETradeRouteType.TRANSPORT, planetId);
        } else {
            trList = trDAO.findByUserIdInternal(userId, ETradeRouteType.TRANSPORT);
        }
        HashMap<Integer, Integer> planetTransCap = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> usedPlanetTransCap = new HashMap<Integer, Integer>();
        HashMap<Integer, Boolean> shipsAvailable = new HashMap<Integer, Boolean>();

        for (TradeRoute tr : trList) {
            TradeRouteExt tre = new TradeRouteExt(tr);

            // Check for Transmitter
            Integer ptcStart = planetTransCap.get(tr.getStartPlanet());
            Integer ptcTarget = planetTransCap.get(tr.getTargetPlanet());

            if (ptcStart == null) {
                PlanetConstruction pc = pcDAO.findBy(tr.getStartPlanet(), Construction.ID_TRANSMITTER);
                if (pc == null) {
                    planetTransCap.put(tr.getStartPlanet(), 0);
                } else {
                    planetTransCap.put(tr.getStartPlanet(), pc.getNumber() * 10000);
                }
            }
            if (ptcTarget == null) {
                PlanetConstruction pc = pcDAO.findBy(tr.getTargetPlanet(), Construction.ID_TRANSMITTER);
                if (pc == null) {
                    planetTransCap.put(tr.getTargetPlanet(), 0);
                } else {
                    planetTransCap.put(tr.getTargetPlanet(), pc.getNumber() * 10000);
                }
            }

            // Check if additional ships are possible
            if (!shipsAvailable.containsKey(tr.getTargetPlanet())) {
                FleetListResult flr = FleetService.getFleetList(tr.getUserId(), 0, tr.getTargetPlanet(), EFleetViewType.ALL_PLANET);
                ArrayList<PlayerFleetExt> allFleets = flr.getAllContainedFleets();
                for (PlayerFleetExt pfe : allFleets) {
                    if (pfe.getLoadingInformation().getRessSpace() > 0) {
                        shipsAvailable.put(tr.getTargetPlanet(), true);
                    }
                }

                if (!shipsAvailable.containsKey(tr.getTargetPlanet())) {
                    shipsAvailable.put(tr.getTargetPlanet(), false);
                }
            }

            if (!shipsAvailable.containsKey(tr.getStartPlanet())) {
                FleetListResult flr = FleetService.getFleetList(tr.getUserId(), 0, tr.getStartPlanet(), EFleetViewType.ALL_PLANET);
                ArrayList<PlayerFleetExt> allFleets = flr.getAllContainedFleets();
                for (PlayerFleetExt pfe : allFleets) {
                    if (pfe.getLoadingInformation().getRessSpace() > 0) {
                        shipsAvailable.put(tr.getStartPlanet(), true);
                    }
                }

                if (!shipsAvailable.containsKey(tr.getStartPlanet())) {
                    shipsAvailable.put(tr.getStartPlanet(), false);
                }
            }

            // Load Route Details
            // ArrayList<TradeRouteDetail> trdList = trdDAO.getByRouteId(tr.getId());
            // tre.setRouteDetails(trdList);

            // Check for assigned Ships
            ArrayList<TradeRouteShip> trsList = trsDAO.findByTradeRouteId(tr.getId());
            for (TradeRouteShip trs : trsList) {
                if (trs.getShipType() == trs.SHIPTYPE_NORMAL) {
                    tre.setRemovingShipsPossible(true);
                } else if (trs.getShipType() == trs.SHIPTYPE_CONSTRUCTION) {
                    tre.setRemovingTransmitterPossible(true);

                    if (usedPlanetTransCap.containsKey(tr.getStartPlanet())) {
                        usedPlanetTransCap.put(tr.getStartPlanet(),
                                usedPlanetTransCap.get(tr.getStartPlanet()) + trs.getNumber() * 10000);
                    } else {
                        usedPlanetTransCap.put(tr.getStartPlanet(), trs.getNumber() * 10000);
                    }

                    if (usedPlanetTransCap.containsKey(tr.getTargetPlanet())) {
                        usedPlanetTransCap.put(tr.getTargetPlanet(),
                                usedPlanetTransCap.get(tr.getTargetPlanet()) + trs.getNumber() * 10000);
                    } else {
                        usedPlanetTransCap.put(tr.getTargetPlanet(), trs.getNumber() * 10000);
                    }
                }
            }

            if (shipsAvailable.get(tr.getStartPlanet()) || shipsAvailable.get(tr.getTargetPlanet())) {
                tre.setAddingShipsPossible(true);
            }

            int capSource = planetTransCap.get(tr.getStartPlanet());
            int capTarget = planetTransCap.get(tr.getTargetPlanet());

            if ((capSource > 0) && (capTarget > 0)) {
                RelativeCoordinate rc1 = new RelativeCoordinate(0, tr.getStartPlanet());
                RelativeCoordinate rc2 = new RelativeCoordinate(0, tr.getTargetPlanet());

                AbsoluteCoordinate ac1 = rc1.toAbsoluteCoordinate();
                AbsoluteCoordinate ac2 = rc2.toAbsoluteCoordinate();

                double distance = ac1.distanceTo(ac2);
                if (distance <= TRANSMITTER_MAX_DISTANCE) {
                    TradeRouteTransmitterInfo trti = getRouteTransmitterInfo(tr.getId());

                    if (Math.min(trti.getTotalCapStart() - trti.getUsedCapStart(), trti.getTotalCapTarget() - trti.getUsedCapTarget()) > 0) {
                        tre.setAddingTransmitterPossible(true);
                    }
                }
            }



            trr.addRoute(tre);
        }

        return trr;
    }

    public static TradeRouteResult getExternalTradeRoutes(int userId) {
        return getExternalTradeRoutes(userId, 0);
    }

    public static TradeRouteResult getExternalTradeRoutes(int userId, int planetId) {
        TradeRouteResult trr = new TradeRouteResult();
        ArrayList<TradeRoute> trList = new ArrayList<TradeRoute>();
        if (planetId > 0) {
            trList = trDAO.findByUserIdExternal(userId, ETradeRouteType.TRANSPORT, planetId);
        } else {
            trList = trDAO.findByUserIdExternal(userId, ETradeRouteType.TRANSPORT);
        }

        HashMap<Integer, Integer> planetTransCap = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> usedPlanetTransCap = new HashMap<Integer, Integer>();
        HashMap<Integer, Boolean> shipsAvailable = new HashMap<Integer, Boolean>();

        for (TradeRoute tr : trList) {
            TradeRouteExt tre = new TradeRouteExt(tr);

            boolean startOwnedByUser = ppDAO.findByPlanetId(tr.getStartPlanet()).getUserId() == userId;
            boolean targetOwnedByUser = ppDAO.findByPlanetId(tr.getTargetPlanet()).getUserId() == userId;

            // Check for Transmitter
            Integer ptcStart = planetTransCap.get(tr.getStartPlanet());
            Integer ptcTarget = planetTransCap.get(tr.getTargetPlanet());

            if (ptcStart == null) {
                PlanetConstruction pc = pcDAO.findBy(tr.getStartPlanet(), Construction.ID_TRANSMITTER);
                if (pc == null) {
                    planetTransCap.put(tr.getStartPlanet(), 0);
                } else {
                    planetTransCap.put(tr.getStartPlanet(), pc.getNumber() * 10000);
                }
            }
            if (ptcTarget == null) {
                PlanetConstruction pc = pcDAO.findBy(tr.getTargetPlanet(), Construction.ID_TRANSMITTER);
                if (pc == null) {
                    planetTransCap.put(tr.getTargetPlanet(), 0);
                } else {
                    planetTransCap.put(tr.getTargetPlanet(), pc.getNumber() * 10000);
                }
            }

            // Check if additional ships are possible
            if (!shipsAvailable.containsKey(tr.getTargetPlanet())) {
                FleetListResult flr = FleetService.getFleetList(tr.getUserId(), 0, tr.getTargetPlanet(), EFleetViewType.ALL_PLANET);
                ArrayList<PlayerFleetExt> allFleets = flr.getAllContainedFleets();
                for (PlayerFleetExt pfe : allFleets) {
                    if (pfe.getLoadingInformation().getRessSpace() > 0) {
                        shipsAvailable.put(tr.getTargetPlanet(), true);
                    }
                }

                if (!shipsAvailable.containsKey(tr.getTargetPlanet())) {
                    shipsAvailable.put(tr.getTargetPlanet(), false);
                }
            }

            if (!shipsAvailable.containsKey(tr.getStartPlanet())) {
                FleetListResult flr = FleetService.getFleetList(tr.getUserId(), 0, tr.getStartPlanet(), EFleetViewType.ALL_PLANET);
                ArrayList<PlayerFleetExt> allFleets = flr.getAllContainedFleets();
                for (PlayerFleetExt pfe : allFleets) {
                    if (pfe.getLoadingInformation().getRessSpace() > 0) {
                        shipsAvailable.put(tr.getStartPlanet(), true);
                    }
                }

                if (!shipsAvailable.containsKey(tr.getStartPlanet())) {
                    shipsAvailable.put(tr.getStartPlanet(), false);
                }
            }

            // Load Route Details
            // ArrayList<TradeRouteDetail> trdList = trdDAO.getByRouteId(tr.getId());
            // tre.setRouteDetails(trdList);

            // Check for assigned Ships
            ArrayList<TradeRouteShip> trsList = trsDAO.findByTradeRouteId(tr.getId());
            for (TradeRouteShip trs : trsList) {
                if (trs.getShipType() == trs.SHIPTYPE_NORMAL) {
                    if (((trs.getAddLocation() == EAddLocation.START) && startOwnedByUser)
                            || ((trs.getAddLocation() == EAddLocation.TARGET) && targetOwnedByUser)) {
                        tre.setRemovingShipsPossible(true);
                    }
                } else if (trs.getShipType() == trs.SHIPTYPE_CONSTRUCTION) {
                    tre.setRemovingTransmitterPossible(true);

                    if (usedPlanetTransCap.containsKey(tr.getStartPlanet())) {
                        usedPlanetTransCap.put(tr.getStartPlanet(),
                                usedPlanetTransCap.get(tr.getStartPlanet()) + trs.getNumber() * 10000);
                    } else {
                        usedPlanetTransCap.put(tr.getStartPlanet(), trs.getNumber() * 10000);
                    }

                    if (usedPlanetTransCap.containsKey(tr.getTargetPlanet())) {
                        usedPlanetTransCap.put(tr.getTargetPlanet(),
                                usedPlanetTransCap.get(tr.getTargetPlanet()) + trs.getNumber() * 10000);
                    } else {
                        usedPlanetTransCap.put(tr.getTargetPlanet(), trs.getNumber() * 10000);
                    }
                }
            }

            if (shipsAvailable.get(tr.getStartPlanet()) || shipsAvailable.get(tr.getTargetPlanet())) {
                tre.setAddingShipsPossible(true);
            }

            int capSource = planetTransCap.get(tr.getStartPlanet());
            int capTarget = planetTransCap.get(tr.getTargetPlanet());

            if ((capSource > 0) && (capTarget > 0)) {
                int usedSource = 0;
                int usedTarget = 0;

                if (usedPlanetTransCap.containsKey(tr.getStartPlanet())) {
                    usedSource =
                            usedPlanetTransCap.get(tr.getStartPlanet());
                }
                if (usedPlanetTransCap.containsKey(tr.getTargetPlanet())) {
                    usedTarget =
                            usedPlanetTransCap.get(tr.getTargetPlanet());
                }

                if (Math.min(capSource - usedSource, capTarget - usedTarget) > 0) {
                    tre.setAddingTransmitterPossible(true);
                }
            }



            trr.addRoute(tre);
        }

        return trr;
    }

    public static void removeShipsFromRoute(int routeId, ArrayList<ShipsToRouteEntry> streList) {
        try {
            //If no ships to remove return
            if (streList == null || streList.isEmpty()) {
                return;
            }

            TradeRoute tr = trDAO.getById(routeId);
            PlayerFleet targetFleet = null;

            ArrayList<ShipFleet> sfList = new ArrayList<ShipFleet>();
            ArrayList<ShipFleet> sfListNew = new ArrayList<ShipFleet>();

            TransactionHandler th = TransactionHandler.getTransactionHandler();
            th.startTransaction();

            for (ShipsToRouteEntry stre : streList) {
                ArrayList<TradeRouteShip> trsList = trsDAO.findByRouteAndDesign(routeId, stre.getDesignId());

                for (TradeRouteShip trs : trsList) {
                    if (trs.getAddLocation() != stre.getAddLoc()) {
                        continue;
                    }

                    // Determine PlayerFleet
                    PlayerFleet pf = new PlayerFleet();
                    pf.setName(ML.getMLStr("transport_lbl_tradefleet", tr.getUserId()) + " #" + routeId);

                    // Take User Id from owner of start or targetPlanet
                    int userId = 0;

                    if (stre.getAddLoc() == EAddLocation.START) {
                        userId = ppDAO.findByPlanetId(tr.getStartPlanet()).getUserId();
                    }
                    if (stre.getAddLoc() == EAddLocation.TARGET) {
                        userId = ppDAO.findByPlanetId(tr.getTargetPlanet()).getUserId();
                    }

                    // Security Check
                    ShipDesign sd = sdDAO.findById(stre.getDesignId());
                    if (sd.getUserId() != userId) {
                        DebugBuffer.warning("Tried to write ship of user " + sd.getUserId() + " into a fleet of user " + userId + " >> Forcing change of userId to " + sd.getUserId());
                        userId = sd.getUserId();
                    }

                    pf.setUserId(userId);

                    RelativeCoordinate rc = null;
                    if (trs.getAddLocation() == EAddLocation.START) {
                        rc = new RelativeCoordinate(0, tr.getStartPlanet());
                    }
                    if (trs.getAddLocation() == EAddLocation.TARGET) {
                        rc = new RelativeCoordinate(0, tr.getTargetPlanet());
                    }

                    pf.setSystemId(rc.getSystemId());
                    pf.setPlanetId(rc.getPlanetId());
                    ArrayList<PlayerFleet> pfList = pfDAO.find(pf);

                    if (pfList.isEmpty()) {
                        pf.setLocation(ELocationType.PLANET, rc.getPlanetId());
                        targetFleet = pfDAO.add(pf);
                    } else {
                        targetFleet = pfList.get(0);
                    }

                    ArrayList<ShipFleet> sfListCurr = sfDAO.findByFleetId(targetFleet.getId());
                    sfList.addAll(sfListCurr);

                    // Do Remove
                    int restCount = trs.getNumber() - stre.getCount();

                    if (restCount == 0) {
                        trsDAO.remove(trs);
                    } else {
                        trs.setNumber(restCount);
                        trsDAO.update(trs);
                    }

                    boolean addedExistant = false;
                    for (ShipFleet sf : sfListCurr) {
                        if (sf.getDesignId() == stre.getDesignId()) {
                            sf.setCount(sf.getCount() + stre.getCount());
                            addedExistant = true;
                            break;
                        }
                    }

                    if (!addedExistant) {
                        ShipFleet sf = new ShipFleet();
                        sf.setFleetId(targetFleet.getId());
                        sf.setDesignId(stre.getDesignId());
                        sf.setCount(stre.getCount());
                        sfListNew.add(sf);
                    }
                }
            }

            sfDAO.updateAll(sfList);
            sfDAO.insertAll(sfListNew);

            th.endTransaction();
        } catch (TransactionException te) {
            DebugBuffer.writeStackTrace("Error while assigning ships to route: ", te);
        }
    }

    public static BaseResult addShipsToRoute(int routeId, ArrayList<ShipsToRouteEntry> streList) {
        TransactionHandler th = TransactionHandler.getTransactionHandler();

        try {
            th.startTransaction();

            ArrayList<TradeRouteShip> trsList = trsDAO.findByTradeRouteId(routeId);
            // ArrayList<TradeRouteShip> trsListNew = new ArrayList<TradeRouteShip>();
            for (ShipsToRouteEntry stre : streList) {
                ShipFleet sf = sfDAO.getByFleetDesign(stre.getFleetId(), stre.getDesignId());
                int fleetId = sf.getFleetId();
                int restCount = sf.getCount() - stre.getCount();

                TradeRoute tr = trDAO.getById(routeId);
                PlayerFleet pf = pfDAO.findById(stre.getFleetId());

                EAddLocation addLoc = EAddLocation.START;

                // log.debug("START = " + tr.getStartPlanet());
                // log.debug("TARGET = " + tr.getTargetPlanet());
                // log.debug("PLAYERFLEET LOC = " + pf.getPlanetId());
                if (pf.getPlanetId().equals(tr.getStartPlanet())) {
                    addLoc = EAddLocation.START;
                } else if (pf.getPlanetId().equals(tr.getTargetPlanet())) {
                    addLoc = EAddLocation.TARGET;
                }

                // log.debug("ADDLOC="+addLoc);

                if (restCount == 0) {
                    DebugBuffer.addLine(DebugLevel.DEBUG, "Adding last ship of a ShipFleet to a TradeRoute");
                    sfDAO.remove(sf);
                    if (sfDAO.findByFleetId(sf.getFleetId()).isEmpty()) {
                        DebugBuffer.addLine(DebugLevel.DEBUG, "(Last Ship of Fleet Added to TradeRoute) => Deleting Fleet" + fleetId);

                        PlayerFleet pfTmp = pfDAO.findById(fleetId);
                        pfDAO.remove(pfTmp);
                    }
                } else {
                    sf.setCount(restCount);
                    sfDAO.update(sf);
                }

                boolean addedExistant = false;
                for (TradeRouteShip trs : trsList) {
                    if (trs.getShipType() == trs.SHIPTYPE_NORMAL) {
                        if ((trs.getDesignId() == stre.getDesignId())
                                && (trs.getAddLocation() == addLoc)) {
                            trs.setNumber(trs.getNumber() + stre.getCount());
                            addedExistant = true;
                            trsDAO.update(trs);
                            break;
                        }
                    }
                }

                if (!addedExistant) {
                    TradeRouteShip trsNew = new TradeRouteShip();
                    trsNew.setDesignId(stre.getDesignId());
                    trsNew.setNumber(stre.getCount());
                    trsNew.setAddLocation(addLoc);
                    trsNew.setShipType(TradeRouteShip.SHIPTYPE_NORMAL);
                    trsNew.setTradeRouteId(routeId);
                    trsDAO.add(trsNew);
                    trsList.add(trsNew);
                    // trsListNew.add(trsNew);
                }
            }

            // trsDAO.updateAll(trsList);
            // trsDAO.insertAll(trsListNew);

            th.endTransaction();
        } catch (TransactionException te) {
            th.rollback();
            DebugBuffer.writeStackTrace("Error while assigning ships to route: ", te);
            return new BaseResult(te.getMessage(), true);
        }

        return new BaseResult(false);
    }

    public static void switchRouteDetailState(int detailId) {
        TradeRouteDetail trd = trdDAO.getById(detailId);
        trd.setDisabled(!trd.getDisabled());
        trdDAO.update(trd);
    }

    public static void deleteRoute(TradeRoute tr) {
        TradeRouteExt tre = new TradeRouteExt(tr);
        ArrayList<TradeRouteDetail> details = tre.getRouteDetails();
        ArrayList<TradeShipData> ships = getShipsInRoute(tr.getId());
        ArrayList<ShipsToRouteEntry> streList = new ArrayList<ShipsToRouteEntry>();

        for (TradeShipData tsd : ships) {
            EAddLocation addLoc = null;
            if (tr.getStartPlanet() == tsd.getHome().getPlanetId()) {
                addLoc = EAddLocation.START;
            }
            if (tr.getTargetPlanet() == tsd.getHome().getPlanetId()) {
                addLoc = EAddLocation.TARGET;
            }

            ShipsToRouteEntry stre = new ShipsToRouteEntry(0, addLoc, tsd.getDesignId(), tsd.getCount());
            streList.add(stre);
        }

        removeShipsFromRoute(tr.getId(), streList);

        for (TradeRouteDetail trd : details) {
            trdDAO.remove(trd);
        }

        trDAO.remove(tr);
    }

    public static void deleteRouteDetail(TradeRoute tr, TradeRouteDetail trd) {
        try {
            TradeRouteExt tre = new TradeRouteExt(tr);
            int actRouteDetails = tre.getRouteDetails().size();
            log.debug("actRouteDetails = " + actRouteDetails);

            boolean deleteRoute = false;
            if (actRouteDetails == 1) {
                deleteRoute = true;
            }

            log.debug("remove trd " + trd.getId() + " delete route = " + deleteRoute);
            trdDAO.remove(trd);
            if (deleteRoute) {
                deleteRoute(tr);
            }
        } catch (Exception e) {
            DebugBuffer.warning("TOXIC Traderoute found ... destroy immediatly (ID: " + tr.getId() + " / please check for toxic waste remains)!");
            trdDAO.remove(trd);
            trDAO.remove(tr);
        }
    }

    public static TradeRouteTransmitterInfo getRouteTransmitterInfo(int routeNo) {
        TradeRouteTransmitterInfo trti = null;
        TradeRouteExt tre = new TradeRouteExt(trDAO.getById(routeNo));

        try {
            int actUsedCapSrc = 0;
            int actUsedCapTarget = 0;
            int totalCapSrc = 0;
            int totalCapTarget = 0;
            int srcPlanet = 0;
            int targetPlanet = 0;
            int currRouteUsage = 0;

            srcPlanet = tre.getBase().getStartPlanet();
            targetPlanet = tre.getBase().getTargetPlanet();

            ArrayList<TradeRoute> allRoutes = trDAO.findByStartPlanet(srcPlanet, ETradeRouteType.TRANSPORT);
            allRoutes.addAll(trDAO.findByEndPlanet(srcPlanet, ETradeRouteType.TRANSPORT));
            allRoutes.addAll(trDAO.findByStartPlanet(targetPlanet, ETradeRouteType.TRANSPORT));
            allRoutes.addAll(trDAO.findByEndPlanet(targetPlanet, ETradeRouteType.TRANSPORT));

            HashSet<Integer> processedRoutes = new HashSet<Integer>();

            for (TradeRoute tr : allRoutes) {
                TradeRouteExt treTmp = new TradeRouteExt(tr);

                if (processedRoutes.contains(tr.getId())) {
                    continue;
                } else {
                    processedRoutes.add(tr.getId());
                }

                if (((tr.getStartPlanet() == srcPlanet) && (tr.getTargetPlanet() == targetPlanet))
                        || ((tr.getStartPlanet() == targetPlanet) && (tr.getTargetPlanet() == srcPlanet))) {
                    currRouteUsage += treTmp.getAssignedTransmitterCap();

                    actUsedCapSrc += treTmp.getAssignedTransmitterCap();
                    actUsedCapTarget += treTmp.getAssignedTransmitterCap();
                } else {
                    if ((tr.getStartPlanet() == srcPlanet) || (tr.getTargetPlanet() == srcPlanet)) {
                        actUsedCapSrc += treTmp.getAssignedTransmitterCap();
                    }

                    if ((tr.getStartPlanet() == targetPlanet) || (tr.getTargetPlanet() == targetPlanet)) {
                        actUsedCapTarget += treTmp.getAssignedTransmitterCap();
                    }
                }


            }

            totalCapSrc = pcDAO.findBy(srcPlanet, 103).getNumber() * TRANSMITTER_CAP;
            totalCapTarget = pcDAO.findBy(targetPlanet, 103).getNumber() * TRANSMITTER_CAP;

            // Additional Cap by Sun Transmitters
            
            trti = new TradeRouteTransmitterInfo();
            trti.setPlanetIdStart(srcPlanet);
            trti.setPlanetIdTarget(targetPlanet);
            trti.setTotalCapStart(totalCapSrc);
            trti.setTotalCapTarget(totalCapTarget);
            trti.setUsedCapStart(actUsedCapSrc);
            trti.setUsedCapTarget(actUsedCapTarget);
            trti.setUsageThisRoute(currRouteUsage);

        } catch (Exception e) {
            DebugBuffer.error("Error in getRouteTransmitterInfo: ", e);
        }

        return trti;
    }

    public static PlayerPlanet resolvePlanetNameId(String input, int userId, boolean internal)
            throws PlanetNameNotUniqueException {
        PlayerPlanet pp = null;

        try {
            int value = Integer.parseInt(input);
            pp = ppDAO.findByPlanetId(value);
            if (pp == null) {
            }
        } catch (NumberFormatException nfe) {
            ArrayList<PlayerPlanet> ppList = null;

            if (internal) {
                ppList = ppDAO.findByNameAndUserId(input, userId);
            } else {
                ppList = ppDAO.findByName(input);
            }

            if (ppList.size() > 1) {
                throw new PlanetNameNotUniqueException("PlanetName: " + input);
            } else if (ppList.size() == 1) {
                pp = ppList.get(0);
            }
        }

        return pp;
    }
}
