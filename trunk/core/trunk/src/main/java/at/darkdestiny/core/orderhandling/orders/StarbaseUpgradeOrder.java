/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.Buildable;

/**
 *
 * @author Stefan
 */
public class StarbaseUpgradeOrder extends UpgradeOrder {
    private final int planetId;

    public StarbaseUpgradeOrder(Buildable fromUnit, Buildable toUnit, int count, int planetId) {
        super(fromUnit,toUnit,count);
        this.planetId = planetId;
    }    
    
    public int getPlanetId() {
        return planetId;
    }
}
