/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.TerritoryMapPoint;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TerritoryMapPointDAO extends ReadWriteTable<TerritoryMapPoint> implements GenericDAO {

    public ArrayList<TerritoryMapPoint> findByTerritoryId(Integer id) {
        TerritoryMapPoint tmp = new TerritoryMapPoint();
        tmp.setTerritoryId(id);
        return find(tmp);
    }
}
