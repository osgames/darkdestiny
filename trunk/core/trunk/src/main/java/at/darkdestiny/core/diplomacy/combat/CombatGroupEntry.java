/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy.combat;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class CombatGroupEntry {
    private final int groupId;
    private final ArrayList<Integer> players = new ArrayList<Integer>();
    private final ArrayList<Integer> attackingGroups = new ArrayList<Integer>();
    
    protected CombatGroupEntry(int groupId) {
        this.groupId = groupId;
    }
    
    protected void addPlayer(int userId) {
        getPlayers().add(userId);        
    }

    protected void addPlayers(ArrayList<Integer> userIds) {
        getPlayers().addAll(userIds);        
    }    
    
    protected void addAttackingGroup(int groupId) {
        getAttackingGroups().add(groupId);
    }

    public int getGroupId() {
        return groupId;
    }

    public ArrayList<Integer> getPlayers() {
        return players;
    }

    public ArrayList<Integer> getAttackingGroups() {
        return attackingGroups;
    }
}
