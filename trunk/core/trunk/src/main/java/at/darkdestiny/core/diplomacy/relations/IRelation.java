/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy.relations;

import at.darkdestiny.core.result.DiplomacyResult;

/**
 *
 * @author Bullet
 */
public interface IRelation {

    public DiplomacyResult getDiplomacyResult();

}
