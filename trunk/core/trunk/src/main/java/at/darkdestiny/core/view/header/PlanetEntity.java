/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view.header;

/**
 *
 * @author Horst
 */
public class PlanetEntity {
    private int planetId;
    private String planetName;
    
    private int previousPlanetId;
    private int nextPlanetId;
    
    public PlanetEntity(){
        planetId = -1;
        planetName = "";
       this.previousPlanetId = -1;
       this.nextPlanetId = -1;
    }

    public int getPlanetId() {
        return planetId;
    }

    public void setPlanetId(int planetId) {
        this.planetId = planetId;
    }

    public String getPlanetName() {
        return planetName;
    }

    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }

    public

    int getPreviousPlanetId() {
        return previousPlanetId;
    }

    public void setPreviousPlanetId(int previousPlanetId) {
        this.previousPlanetId = previousPlanetId;
    }

    public int getNextPlanetId() {
        return nextPlanetId;
    }

    public void setNextPlanetId(int nextPlanetId) {
        this.nextPlanetId = nextPlanetId;
    }
}
