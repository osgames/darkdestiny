package at.darkdestiny.core.ships;

 import at.darkdestiny.core.dao.DamagedShipsDAO;import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.model.DamagedShips;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.DamagedShipsDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.model.DamagedShips;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import java.util.ArrayList;

public class ShipData {

    private static final Logger log = LoggerFactory.getLogger(ShipData.class);

    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private int id;
    private int fleetId;
    private int chassisSize;
    private String fleetName;
    private int designId;
    private String designName;
    private int count;
    private boolean loaded;

    public ShipData() {
    }

    public ShipData(int shipFleetId) {
        ShipFleet sf = sfDAO.getById(shipFleetId);

        if (sf == null) {
            throw new RuntimeException("Invalid parameter on call");
        }
        ShipDesign sd = sdDAO.findById(sf.getDesignId());
        PlayerFleet pf = pfDAO.findById(sf.getFleetId());

        this.id = sf.getId();
        this.fleetId = sf.getFleetId();
        this.designId = sf.getDesignId();
        this.count = sf.getCount();

        this.chassisSize = sd.getChassis();
        this.designName = sd.getName();
        this.fleetName = pf.getName();

        this.loaded = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFleetId() {
        return fleetId;
    }

    public void setFleetId(int fleetId) {
        this.fleetId = fleetId;
    }

    public int getDesignId() {
        return designId;
    }

    public void setDesignId(int designId) {
        this.designId = designId;
    }

    public java.lang.String getDesignName() {
        return designName;
    }

    public void setDesignName(java.lang.String designName) {
        this.designName = designName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public int getChassisSize() {
        return chassisSize;
    }

    public void setChassisSize(int chassisSize) {
        this.chassisSize = chassisSize;
    }

    public ArrayList<ShipStatusEntry> getShipDataStatus() {
        ArrayList<ShipStatusEntry> sdsList = new ArrayList<ShipStatusEntry>();

        ArrayList<DamagedShips> damageLevels = dsDAO.findById(this.getId());

        if (damageLevels.isEmpty()) {
            sdsList.add(new ShipStatusEntry(EDamageLevel.NODAMAGE, count));
            return sdsList;
        }

        int countLeft = count;

        ShipStatusEntry noDamage = new ShipStatusEntry(EDamageLevel.NODAMAGE, 0);
        ShipStatusEntry minorDamage = new ShipStatusEntry(EDamageLevel.MINORDAMAGE, 0);
        ShipStatusEntry lightDamage = new ShipStatusEntry(EDamageLevel.LIGHTDAMAGE, 0);
        ShipStatusEntry mediumDamage = new ShipStatusEntry(EDamageLevel.MEDIUMDAMAGE, 0);
        ShipStatusEntry heavyDamage = new ShipStatusEntry(EDamageLevel.HEAVYDAMAGE, 0);

        for (DamagedShips ds : damageLevels) {
            if (ds.getDamageLevel() == EDamageLevel.NODAMAGE) {
                noDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            } else if (ds.getDamageLevel() == EDamageLevel.MINORDAMAGE) {
                // DebugBuffer.addLine(DebugLevel.DEBUG,"REDUCE COUNT LEFT BY " + ds.getCount() + " ["+ds.getDamageLevel()+"]");
                minorDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            } else if (ds.getDamageLevel() == EDamageLevel.LIGHTDAMAGE) {
                // DebugBuffer.addLine(DebugLevel.DEBUG,"REDUCE COUNT LEFT BY " + ds.getCount() + " ["+ds.getDamageLevel()+"]");
                lightDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            } else if (ds.getDamageLevel() == EDamageLevel.MEDIUMDAMAGE) {
                // DebugBuffer.addLine(DebugLevel.DEBUG,"REDUCE COUNT LEFT BY " + ds.getCount() + " ["+ds.getDamageLevel()+"]");
                mediumDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            } else if (ds.getDamageLevel() == EDamageLevel.HEAVYDAMAGE) {
                // DebugBuffer.addLine(DebugLevel.DEBUG,"REDUCE COUNT LEFT BY " + ds.getCount() + " ["+ds.getDamageLevel()+"]");
                heavyDamage.increaseCount(ds.getCount());
                countLeft -= ds.getCount();
            }
        }

        // DebugBuffer.addLine(DebugLevel.DEBUG,"COUNT LEFT (END)= " + countLeft);
        noDamage.increaseCount(countLeft);

        sdsList.add(noDamage);
        sdsList.add(minorDamage);
        sdsList.add(lightDamage);
        sdsList.add(mediumDamage);
        sdsList.add(heavyDamage);

        return sdsList;
    }

    public static String getBarContent(ArrayList<ShipStatusEntry> sseList) {
        int height = 10;
        int lengthNoDamage = 0;
        int lengthMinorDamage = 0;
        int lengthLightDamage = 0;
        int lengthMediumDamage = 0;
        int lengthHeavyDamage = 0;

        int sum = 0;

        for (ShipStatusEntry sse : sseList) {
            sum += sse.getCount();
        }

        StringBuffer sb = new StringBuffer();
        int highestValue = 0;
        EDamageLevel highest = null;

        for (ShipStatusEntry sse : sseList) {
            if (sse.getStatus() == EDamageLevel.NODAMAGE) {
                log.debug("NO DAMAGE = " + sse.getCount());
                lengthNoDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthNoDamage > highestValue) {
                    highestValue = lengthNoDamage;
                    highest = EDamageLevel.NODAMAGE;
                }
            } else if (sse.getStatus() == EDamageLevel.MINORDAMAGE) {
                lengthMinorDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthMinorDamage > highestValue) {
                    highestValue = lengthMinorDamage;
                    highest = EDamageLevel.MINORDAMAGE;
                }
            } else if (sse.getStatus() == EDamageLevel.LIGHTDAMAGE) {
                lengthLightDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthLightDamage > highestValue) {
                    highestValue = lengthLightDamage;
                    highest = EDamageLevel.LIGHTDAMAGE;
                }
            } else if (sse.getStatus() == EDamageLevel.MEDIUMDAMAGE) {
                lengthMediumDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthMediumDamage > highestValue) {
                    highestValue = lengthMediumDamage;
                    highest = EDamageLevel.MEDIUMDAMAGE;
                }
            } else if (sse.getStatus() == EDamageLevel.HEAVYDAMAGE) {
                lengthHeavyDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthHeavyDamage > highestValue) {
                    highestValue = lengthHeavyDamage;
                    highest = EDamageLevel.HEAVYDAMAGE;
                }
            }
        }

        int endSum = lengthNoDamage + lengthMinorDamage + lengthLightDamage
                + lengthMediumDamage + lengthHeavyDamage;
        int adjustBy = 100 - endSum;

        if (lengthNoDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.NODAMAGE)) {
                lengthNoDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthNoDamage + "px;\" src=\"./pic/menu/energyProductionLater.png\"></IMG></TD>");
        }
        if (lengthMinorDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.MINORDAMAGE)) {
                lengthMinorDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthMinorDamage + "px;\" src=\"./pic/menu/energyProductionNow.png\"></IMG></TD>");
        }
        if (lengthLightDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.LIGHTDAMAGE)) {
                lengthLightDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthLightDamage + "px;\" src=\"./pic/menu/energyEqual.png\"></IMG></TD>");
        }
        if (lengthMediumDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.MEDIUMDAMAGE)) {
                lengthMediumDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthMediumDamage + "px;\" src=\"./pic/menu/foodUsage.png\"></IMG></TD>");
        }
        if (lengthHeavyDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.HEAVYDAMAGE)) {
                lengthHeavyDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthHeavyDamage + "px;\" src=\"./pic/menu/foodRed.png\"></IMG></TD>");
        }

        return sb.toString();
    }

    public String getBarContent() {
        ArrayList<ShipStatusEntry> sseList = getShipDataStatus();

        int height = 10;
        int lengthNoDamage = 0;
        int lengthMinorDamage = 0;
        int lengthLightDamage = 0;
        int lengthMediumDamage = 0;
        int lengthHeavyDamage = 0;

        int countNoDamage = 0;
        int countMinorDamage = 0;
        int countLightDamage = 0;
        int countMediumDamage = 0;
        int countHeavyDamage = 0;

        int sum = 0;

        for (ShipStatusEntry sse : sseList) {
            sum += sse.getCount();
        }

        StringBuffer sb = new StringBuffer();
        int highestValue = 0;
        EDamageLevel highest = null;

        for (ShipStatusEntry sse : sseList) {
            if (sse.getStatus() == EDamageLevel.NODAMAGE) {
                log.debug("NO DAMAGE = " + sse.getCount());
                countNoDamage = sse.getCount();
                lengthNoDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthNoDamage > highestValue) {
                    highestValue = lengthNoDamage;
                    highest = EDamageLevel.NODAMAGE;
                }
            } else if (sse.getStatus() == EDamageLevel.MINORDAMAGE) {
                countMinorDamage = sse.getCount();
                lengthMinorDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthMinorDamage > highestValue) {
                    highestValue = lengthMinorDamage;
                    highest = EDamageLevel.MINORDAMAGE;
                }
            } else if (sse.getStatus() == EDamageLevel.LIGHTDAMAGE) {
                countLightDamage = sse.getCount();
                lengthLightDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthLightDamage > highestValue) {
                    highestValue = lengthLightDamage;
                    highest = EDamageLevel.LIGHTDAMAGE;
                }
            } else if (sse.getStatus() == EDamageLevel.MEDIUMDAMAGE) {
                countMediumDamage = sse.getCount();
                lengthMediumDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthMediumDamage > highestValue) {
                    highestValue = lengthMediumDamage;
                    highest = EDamageLevel.MEDIUMDAMAGE;
                }
            } else if (sse.getStatus() == EDamageLevel.HEAVYDAMAGE) {
                countHeavyDamage = sse.getCount();
                lengthHeavyDamage = (int) Math.ceil(100d / sum * sse.getCount());
                if (lengthHeavyDamage > highestValue) {
                    highestValue = lengthHeavyDamage;
                    highest = EDamageLevel.HEAVYDAMAGE;
                }
            }
        }

        int endSum = lengthNoDamage + lengthMinorDamage + lengthLightDamage
                + lengthMediumDamage + lengthHeavyDamage;
        int adjustBy = 100 - endSum;

        if (lengthNoDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.NODAMAGE)) {
                lengthNoDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthNoDamage + "px;\" "
                    + "src=\"./pic/menu/energyProductionLater.png\" onmouseover=\"doTooltip(event,'Nicht besch�digt (" + countNoDamage + ")')\" onmouseout=\"hideTip()\"></IMG></TD>");
        }
        if (lengthMinorDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.MINORDAMAGE)) {
                lengthMinorDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthMinorDamage + "px;\" "
                    + "src=\"./pic/menu/energyProductionNow.png\" onmouseover=\"doTooltip(event,'Geringf�gig besch�digt (" + countMinorDamage + ")')\" onmouseout=\"hideTip()\"></IMG></TD>");
        }
        if (lengthLightDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.LIGHTDAMAGE)) {
                lengthLightDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthLightDamage + "px;\" "
                    + "src=\"./pic/menu/energyEqual.png\" onmouseover=\"doTooltip(event,'Leicht besch�digt (" + countLightDamage + ")')\" onmouseout=\"hideTip()\"></IMG></TD>");
        }
        if (lengthMediumDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.MEDIUMDAMAGE)) {
                lengthMediumDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthMediumDamage + "px;\" "
                    + "src=\"./pic/menu/foodUsage.png\" onmouseover=\"doTooltip(event,'Besch�digt (" + countMediumDamage + ")')\" onmouseout=\"hideTip()\"></IMG></TD>");
        }
        if (lengthHeavyDamage > 0) {
            if ((adjustBy != 0) && (highest == EDamageLevel.HEAVYDAMAGE)) {
                lengthHeavyDamage += adjustBy;
            }
            sb.append("<TD><IMG style=\"height:12px; width:" + lengthHeavyDamage + "px;\" "
                    + "src=\"./pic/menu/foodRed.png\" onmouseover=\"doTooltip(event,'Schwer besch�digt (" + countHeavyDamage + ")')\" onmouseout=\"hideTip()\"></IMG></TD>");
        }

        return sb.toString();
    }
}
