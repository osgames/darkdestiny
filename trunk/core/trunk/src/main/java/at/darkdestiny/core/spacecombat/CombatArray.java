/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class CombatArray {
    private final HashMap<Integer,HashMap<Integer,HashMap<Integer,ICombatUnit>>> combatArray = 
            new HashMap<Integer,HashMap<Integer,HashMap<Integer,ICombatUnit>>>();
    
    public void addUnit(int groupIdx, int fleetIdx, int unitIdx, ICombatUnit icu) {
        HashMap<Integer,HashMap<Integer,ICombatUnit>> fleets = combatArray.get(groupIdx);
        if(fleets == null){
            fleets = new HashMap<Integer, HashMap<Integer, ICombatUnit>>();
        }
        HashMap<Integer, ICombatUnit> unit = fleets.get(fleetIdx);
        if(unit == null){
            unit = new HashMap<Integer, ICombatUnit>();
        }
       
        unit.put(unitIdx, icu);
        fleets.put(fleetIdx, unit);
        combatArray.put(groupIdx, fleets);
        
    }
    
    public ICombatUnit getUnit(int groupIdx, int fleetIdx, int unitIdx) {
        if (combatArray.get(groupIdx) == null) return null;
        if (combatArray.get(groupIdx).get(fleetIdx) == null) return null;

        return combatArray.get(groupIdx).get(fleetIdx).get(unitIdx);
    }
}
