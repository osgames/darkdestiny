/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.EViewSystemLocationType;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "viewsystem")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ViewSystem extends Model<ViewSystem> {

    @FieldMappingAnnotation("locationId")
    @IndexAnnotation(indexName="location")
    @IdFieldAnnotation
    private Integer locationId;
    @FieldMappingAnnotation("locationType")
    @IndexAnnotation(indexName="location")
    @IdFieldAnnotation
    private EViewSystemLocationType locationType;
    @FieldMappingAnnotation("userId")
    @IndexAnnotation(indexName="userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("date")
    private Long date;

    /**
     * @return the locationId
     */
    public Integer getLocationId() {
        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the locationType
     */
    public EViewSystemLocationType getLocationType() {
        return locationType;
    }

    /**
     * @param locationType the locationType to set
     */
    public void setLocationType(EViewSystemLocationType locationType) {
        this.locationType = locationType;
    }


}
