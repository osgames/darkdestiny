/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.statistics.EStatisticRefType;
import at.darkdestiny.core.statistics.EStatisticType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "statisticentry")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class StatisticEntry extends Model<StatisticEntry> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @StringType("text")
    @ColumnProperties(length = 450)
    @FieldMappingAnnotation("ref")
    private String ref;
    @FieldMappingAnnotation("refType")
    private EStatisticRefType refType;
    @FieldMappingAnnotation("type")
    private EStatisticType type;
    @FieldMappingAnnotation("categoryId")
    private int categoryId;
    @FieldMappingAnnotation("xTitle")
    private String xTitle;
    @FieldMappingAnnotation("yTitle")
    private String yTitle;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * @param ref the ref to set
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

    /**
     * @return the refType
     */
    public EStatisticRefType getRefType() {
        return refType;
    }

    /**
     * @param refType the refType to set
     */
    public void setRefType(EStatisticRefType refType) {
        this.refType = refType;
    }

    /**
     * @return the type
     */
    public EStatisticType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EStatisticType type) {
        this.type = type;
    }

    /**
     * @return the categoryId
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return the xTitle
     */
    public String getxTitle() {
        return xTitle;
    }

    /**
     * @param xTitle the xTitle to set
     */
    public void setxTitle(String xTitle) {
        this.xTitle = xTitle;
    }

    /**
     * @return the yTitle
     */
    public String getyTitle() {
        return yTitle;
    }

    /**
     * @param yTitle the yTitle to set
     */
    public void setyTitle(String yTitle) {
        this.yTitle = yTitle;
    }
}
