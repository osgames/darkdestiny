/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.planetcalc;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.update.UpdaterDataSet;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;

/**
 *
 * @author Stefan
 */
public class PlanetCalculation {
    private PlayerPlanetDAO ppDao = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);

    private PlayerPlanet pp;
    private ExtPlanetCalculation extPlanetCalc;
    private ProdCalculation prodCalc;
    private final PlanetCalcResult planetCalcResult = new PlanetCalcResult();
    private ProductionResult prodResultFuture = null;
    private final UpdaterDataSet uds;

    public PlanetCalculation(int planetId) throws Exception {
        PlayerPlanet pp = ppDao.findByPlanetId(planetId);
        if (pp == null) {
            DebugBuffer.warning("Trying to display PlanetCalc for non existant planetId: " + planetId);
            throw new Exception();
        }

        uds = null;
        init(pp);
    }

    public PlanetCalculation(PlayerPlanet pp) throws Exception {
        if (pp == null) throw new Exception();

        uds = null;
        init(pp);
    }

    public PlanetCalculation(PlayerPlanet pp, UpdaterDataSet pcDataSet) throws Exception {
        if (pp == null) throw new Exception();

        uds = pcDataSet;
        init(pp);
    }

    private void init(PlayerPlanet pp) throws Exception {
        this.pp = pp;

        extPlanetCalc = new ExtPlanetCalculation(pp);
        if (uds != null) {
            prodCalc = new ProdCalculation(pp,uds);
        } else {
            prodCalc = new ProdCalculation(pp);
        }

        extPlanetCalc.calcEconomyPercentage(prodCalc);
        extPlanetCalc.adjustBaseProdCalculation(prodCalc);
        extPlanetCalc.calculateRessDependentValues(prodCalc.getProduction());
    }

    public PlanetCalcResult getPlanetCalcData() {
        planetCalcResult.setExtPlanetCalcData(extPlanetCalc.getData());
        planetCalcResult.setProductionData(prodCalc.getProduction());

        return planetCalcResult;
    }

    public ProductionResult getProductionDataFuture() {
        if (prodResultFuture == null) {
            ProdCalculation pc = new ProdCalculation(pp,true);
            prodResultFuture = pc.getProduction();
        }

        return prodResultFuture;
    }

    public int getPlanetId() {
        if (pp != null) {
            return pp.getPlanetId();
        }

        return -1;
    }

    public PlayerPlanet getPlayerPlanet() {
        return pp.clone();
    }
}
