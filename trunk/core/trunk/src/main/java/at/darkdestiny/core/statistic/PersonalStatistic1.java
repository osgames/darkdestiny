package at.darkdestiny.core.statistic;

 import at.darkdestiny.core.dao.PlayerStatisticDAO;import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.model.PlayerStatistic;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.awt.Font;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TreeMap;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedDomainCategoryPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.GroupedStackedBarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.PlayerStatisticDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.model.PlayerStatistic;
import at.darkdestiny.core.model.UserData;
import java.awt.Font;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TreeMap;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedDomainCategoryPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.GroupedStackedBarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

/**
 * A simple demonstration application showing how to create a stacked bar chart
 * using data from a {@link CategoryDataset}.
 *
 */
public class PersonalStatistic1 extends ApplicationFrame {

    private static final Logger log = LoggerFactory.getLogger(PersonalStatistic1.class);

    private static PlayerStatisticDAO psDAO = (PlayerStatisticDAO) DAOFactory.get(PlayerStatisticDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static HashMap<Integer, TreeMap<Long, PlayerStatistic>> playerStats;
    private static ArrayList<PlayerStatistic> dataArray;
    static DefaultCategoryDataset result = new DefaultCategoryDataset();
    static DefaultCategoryDataset result2 = new DefaultCategoryDataset();
    private static JFreeChart chart;

    /**
     * @return the chart
     */
    public static JFreeChart getChart() {
        return chart;
    }

    /**
     * Creates a new demo.
     *
     * @param title  the frame title.
     */
    public PersonalStatistic1(int userId) {

        super("Statistic");
        if (playerStats == null) {
            createBuffer();
        }
        userId = 1;
        chart = createChart();
        loadPlayerStatistic(userId);
        createDataset();
        final ChartPanel chartPanel = new ChartPanel(getChart());
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);

    }

    /**
     * Creates a sample dataset.
     *
     * @return a sample dataset.
     */
    private void createDataset() {
        createCategoryDataset();
    }

    public static void createBuffer() {

        playerStats = new HashMap<Integer, TreeMap<Long, PlayerStatistic>>();
        for (UserData ud : (ArrayList<UserData>) udDAO.findAll()) {
            TreeMap<Long, PlayerStatistic> userStat = playerStats.get(ud.getUserId());
            if (userStat == null) {
                userStat = new TreeMap<Long, PlayerStatistic>();
            }
            for (PlayerStatistic ps : (ArrayList<PlayerStatistic>) psDAO.findAll()) {
                userStat.put(ps.getDate(), ps);
            }
            playerStats.put(ud.getUserId(), userStat);
        }

    }

    public static void loadPlayerStatistic(int userId) {
        TreeMap<Long, PlayerStatistic> userStat = playerStats.get(userId);
        dataArray = new ArrayList<PlayerStatistic>();
        dataArray.addAll(userStat.descendingMap().values());
    }

    public static void createCategoryDataset() {
        result = new DefaultCategoryDataset();
        result2 = new DefaultCategoryDataset();

        for (int i = 0; i < dataArray.size(); i++) {
            PlayerStatistic ps = dataArray.get(i);
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(ps.getDate());
            String date = "";
            int day = gc.get(GregorianCalendar.DAY_OF_MONTH);
            if(day < 10){
                date += "0" + day;
            }else{
                    date += day;
            }
            date += "-";
            int month = gc.get(GregorianCalendar.MONTH);
            if(month < 10){
                date += "0" + month;
            }else{
                    date += month;
            }
            date += "-" + gc.get(GregorianCalendar.YEAR);
            result2.setValue(ps.getRank(), "Rank", date);
/*
            result.setValue(100, "MilitaryPoints", date);
            result.setValue(120, "PopulationPoints", date);
            result.setValue(150, "ResearchPoints", date);
            result.setValue(170, "RessourcePoints", date);
           */
            result.setValue((double)ps.getMilitaryPoints(), "MilitaryPoints", date);
            result.setValue((double)ps.getPopulationPoints(),"PopulationPoints" , date);
            result.setValue((double)ps.getResearchPoints(), "ResearchPoints", date);
            result.setValue((double)ps.getRessourcePoints(), "RessourcePoints", date);
        }
    }

    private JFreeChart createChart() {


        GroupedStackedBarRenderer renderer = new GroupedStackedBarRenderer();
        final NumberAxis rangeAxis1 = new NumberAxis("Points");
        final CategoryPlot subplot1 = new CategoryPlot(result, null, rangeAxis1,
                renderer);


        CategoryItemRenderer renderer2 = new LineAndShapeRenderer();
        final NumberAxis rangeAxis2 = new NumberAxis("Rank");
        final CategoryPlot subplot2 = new CategoryPlot(result2, null, rangeAxis2,
                renderer2);
        final CombinedDomainCategoryPlot plot = new CombinedDomainCategoryPlot();
        plot.setGap(15);
        plot.add(subplot2);
        plot.add(subplot1);

        final JFreeChart chart = new JFreeChart(
                "Personal Statistics",
                new Font("SansSerif", Font.BOLD, 12),
                plot,
                true);
        return chart;
    }

    /**
     * Creates a sample chart.
     *
     * @param dataset  the dataset for the chart.
     *
     * @return a sample chart.
     */
    // ****************************************************************************
    // * JFREECHART DEVELOPER GUIDE                                               *
    // * The JFreeChart Developer Guide, written by David Gilbert, is available   *
    // * to purchase from Object Refinery Limited:                                *
    // *                                                                          *
    // * http://www.object-refinery.com/jfreechart/guide.html                     *
    // *                                                                          *
    // * Sales are used to provide funding for the JFreeChart project - please    *
    // * support us so that we can continue developing free software.             *
    // ****************************************************************************
    /**
     * Starting point for the demonstration application.
     *
     * @param args  ignored.
     */

    public void createHtmlFile(JFreeChart chart){
        DebugBuffer.addLine(DebugLevel.TRACE,"Creating HTML File for Statistics");

           File image = new File(StatisticConstants.CHART_DIRECTORY,"PStat"+".jpg");
        try {
            ChartUtilities.saveChartAsJPEG(image, chart, 800, 600);
        } catch (Exception e) {
        log.debug("E : " + e);
            if (StatisticConstants.localOutput) log.debug("Fehler im Bildgenerieren "+e);
            DebugBuffer.writeStackTrace("Fehler in SingleStatistik.java -  CreateHTMLFILE",e);
        }

        try{
            if(StatisticConstants.localOutput)
                log.debug("PATH: "+StatisticConstants.CHART_DIRECTORY);
            File test  = new File(StatisticConstants.CHART_DIRECTORY,"Pstat2"+".html");
            String htmlPath = (test.getAbsolutePath());

            String tmpImageFilePath = htmlPath;

            tmpImageFilePath = "/"+tmpImageFilePath.substring(18,tmpImageFilePath.length());

            DebugBuffer.addLine(DebugLevel.TRACE,"BildPfad = "+tmpImageFilePath);

            FileWriter f1 = new FileWriter(test);

            f1.write("<html>\n\n");
            f1.write("\t <body bgcolor='BLACK'>\n\n");
            f1.write("\t\t <table align='CENTER'>\n");
            f1.write("\t\t\t <tr>\n");
            f1.write("\t\t\t\t <td align='CENTER'>");
            f1.write("<img src='"+tmpImageFilePath+"'></img>");
            f1.write("</td>\n");
            f1.write("\t\t\t </tr>\n");
            f1.write("\t\t </table>\n");

            f1.write("\t </body>\n\n");
            f1.write("</html>");

            f1.close();

            if(StatisticConstants.localOutput)
                log.debug("SingleHtmlFIle generated");

        } catch (Exception e) {
        log.debug("E : " + e);
            DebugBuffer.writeStackTrace("Fehler in SingleStatistik.java",e);

            DebugBuffer.addLine(DebugLevel.TRACE,"Fehler in SingleStatistic - CreateHtmlFile "+ e);
            if(StatisticConstants.localOutput)
                log.debug("Fehler8: + "+e);
        }
    }
    public static void main(final String[] args) {
        String test = "dies ist 1 ein fucking testString";
       test = test.replace("1", "2");
        log.debug(test);

        final PersonalStatistic1 demo = new PersonalStatistic1(1);
        demo.pack();
        demo.setVisible(true);
    }
}
