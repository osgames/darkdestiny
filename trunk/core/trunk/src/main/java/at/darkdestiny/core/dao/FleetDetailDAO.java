/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.FleetDetail;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class FleetDetailDAO extends ReadWriteTable<FleetDetail> implements GenericDAO {
    public FleetDetail findById(Integer id) {
        FleetDetail fd = new FleetDetail();
        fd.setId(id);
        return (FleetDetail)get(fd);
    }

    public FleetDetail findByFleetId(Integer fleetId) {
        FleetDetail fdSearch = new FleetDetail();
        fdSearch.setFleetId(fleetId);
        ArrayList<FleetDetail> result = find(fdSearch);

        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
