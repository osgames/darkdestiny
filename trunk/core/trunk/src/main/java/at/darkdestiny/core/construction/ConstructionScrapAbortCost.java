/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.construction;

import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.dao.ProductionOrderDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class ConstructionScrapAbortCost extends MutableRessourcesEntry {
    private static ProductionOrderDAO poDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);

    private final int count;
    private final int orderId;
    private final ConstructionExt ce;

    private final ConstructionScrapCost csc;
    private final RessourcesEntry buildCost;

    public ConstructionScrapAbortCost(ConstructionExt ce, int orderId) {
        this.count = 1;
        this.ce = ce;
        this.orderId = orderId;

        csc = new ConstructionScrapCost(ce);
        buildCost = ce.getRessCost();

        calcScrapAbortCost();
    }

    private void calcScrapAbortCost() {
        ProductionOrder po = poDAO.findById(orderId);

        int totalNeed = po.getIndNeed();
        int done = po.getIndProc();

        //If no tick reconstruct Building
        if (done == 0) { // full financial refund
            for (RessAmountEntry rae : csc.getRessArray()) {
                if (rae.getRessId() == Ressource.CREDITS) {
                    this.setRess(rae.getRessId(), (long)-rae.getQty());
                }
            }
        } else {
            double progress = po.getIndPerc();

            // Calculate financial refund
            if (progress <= 10) { // Only minor money loss - no resource loss
                for (RessAmountEntry rae : csc.getRessArray()) {
                    if (rae.getRessId() == Ressource.CREDITS) {
                        this.setRess(rae.getRessId(), (long)-(rae.getQty() * (1d - 0.2d / 10d * progress)));
                    }
                }
            } else { // No resource refund construction has to be paid up to full cost - same for resources
                for (RessAmountEntry rae : buildCost.getRessArray()) {
                    if (rae.getRessId() == Ressource.CREDITS) {
                        long cost = (long)((rae.getQty() / 90d * (progress - 10d)) - csc.getRess(rae.getRessId()) * 0.8d);
                        this.setRess(rae.getRessId(), cost);
                    } else if (rae.getRessId() == Ressource.DEV_POINTS) {
                        continue;
                    } else {
                        long cost = (long)((rae.getQty() / 90d * (progress - 10d)) - (csc.getRess(rae.getRessId()) / 90d * (progress - 10d)));
                        this.setRess(rae.getRessId(), cost);
                    }
                }
            }
        }
    }

    public int getCount() {
        return count;
    }
}
