/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.planetcalc;

import at.darkdestiny.core.enumeration.EEconomyType;
import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 * 
 * Conains calculated Values from Extended Planet Calculation
 */
public class ExtPlanetCalcResult {
    // Morale Values

    private int morale;
    private int estimatedMorale;
    private int moraleBonus;
    private int moralBonusTotal;
    private int moraleBonusTax;
    private int moralBonusSurface;
    private int moralFood;
    private int changeValueMorale;
    private float maxMorale;
    // Efficiency Values
    private float effEnergy;
    private float effAgriculture;
    private float effIndustry;
    private float effResearch;
    private float effCommon;
    // Population Related Values
    private long maxPopulation;
    private float growth;
    private float changevalueGrowth;
    private int baseTaxIncome;
    private int taxIncome;
    private int taxCrimDiscount;
    private int taxRiotDiscount;
    private float maxGrowth;
    private float maxGrowthBonus;
    private float minGrowth;
    private int usedPopulation;
    private int neededPopulation;
    private int inConsUsedPopulation;
    private int inConsUsedSpace;
    // Food related
    private float foodcoverage;
    private float foodcoverageNoStock;
    //Developement related
    private double criminalityValue;
    private BigDecimal loyalityValue;
    private BigDecimal loyalityChange;
    private HashMap<Integer, BigDecimal> enemyLoyalityChange;
    private double riotValue;
    private double riotValueChange;
    private double devCriminality;
    private double devMorale;
    private double devRiots;
    private double developementPoints;
    private double developementPointsGlobal;

    // Variables for planet specialization    
    private Map<EEconomyType,Float> planetEconomySurfacePerc = new EnumMap<EEconomyType,Float>(EEconomyType.class);
    private Map<EEconomyType,Float> planetEconomyWorkerPerc = new EnumMap<EEconomyType,Float>(EEconomyType.class);
    
    public float getEconomySurfacePerc(EEconomyType eet) {
        if (getPlanetEconomySurfacePerc().get(eet) == null) return 0;
        return getPlanetEconomySurfacePerc().get(eet);
    }

    public float getEconomyWorkerPerc(EEconomyType eet) {
        if (getPlanetEconomyWorkerPerc().get(eet) == null) return 0;
        return getPlanetEconomyWorkerPerc().get(eet);
    }    
    
    public int getMorale() {
        return morale;
    }

    public void setMorale(int morale) {
        this.morale = morale;
    }

    public int getEstimatedMorale() {
        return estimatedMorale;
    }

    public void setEstimatedMorale(int estimatedMorale) {
        this.estimatedMorale = estimatedMorale;
    }

    public int getMoraleBonus() {
        return moraleBonus;
    }

    public void setMoraleBonus(int moraleBonus) {
        this.moraleBonus = moraleBonus;
    }

    public int getMoralBonusTotal() {
        return moralBonusTotal;
    }

    public void setMoralBonusTotal(int moralBonusTotal) {
        this.moralBonusTotal = moralBonusTotal;
    }

    public int getMoralBonusSurface() {
        return moralBonusSurface;
    }

    public void setMoralBonusSurface(int moralBonusSurface) {
        this.moralBonusSurface = moralBonusSurface;
    }

    public int getChangeValueMorale() {
        return changeValueMorale;
    }

    public void setChangeValueMorale(int changeValueMorale) {
        this.changeValueMorale = changeValueMorale;
    }

    public float getMaxMorale() {
        return maxMorale;
    }

    public void setMaxMorale(float maxMorale) {
        this.maxMorale = maxMorale;
    }

    public float getEffEnergy() {
        return effEnergy;
    }

    public void setEffEnergy(float effEnergy) {
        this.effEnergy = effEnergy;
    }

    public float getEffAgriculture() {
        return effAgriculture;
    }

    public void setEffAgriculture(float effAgriculture) {
        this.effAgriculture = effAgriculture;
    }

    public float getEffIndustry() {
        return effIndustry;
    }

    public void setEffIndustry(float effIndustry) {
        this.effIndustry = effIndustry;
    }

    public float getEffResearch() {
        return effResearch;
    }

    public void setEffResearch(float effResearch) {
        this.effResearch = effResearch;
    }

    public float getEffCommon() {
        return effCommon;
    }

    public void setEffCommon(float effCommon) {
        this.effCommon = effCommon;
    }

    public long getMaxPopulation() {
        return maxPopulation;
    }

    public void setMaxPopulation(long maxPopulation) {
        this.maxPopulation = maxPopulation;
    }

    public float getGrowth() {
        return growth;
    }

    public void setGrowth(float growth) {
        this.growth = growth;
    }

    public float getChangevalueGrowth() {
        return changevalueGrowth;
    }

    public void setChangevalueGrowth(float changevalueGrowth) {
        this.changevalueGrowth = changevalueGrowth;
    }

    public int getTaxIncome() {
        return taxIncome;
    }

    public void setTaxIncome(int taxIncome) {
        this.taxIncome = taxIncome;
    }

    public float getMaxGrowth() {
        return maxGrowth;
    }

    public void setMaxGrowth(float maxGrowth) {
        this.maxGrowth = maxGrowth;
    }

    public float getMinGrowth() {
        return minGrowth;
    }

    public void setMinGrowth(float minGrowth) {
        this.minGrowth = minGrowth;
    }

    public int getUsedPopulation() {
        return usedPopulation;
    }

    public void setUsedPopulation(int usedPopulation) {
        this.usedPopulation = usedPopulation;
    }

    public int getNeededPopulation() {
        return neededPopulation;
    }

    public void setNeededPopulation(int neededPopulation) {
        this.neededPopulation = neededPopulation;
    }

    public float getFoodcoverage() {
        return foodcoverage;
    }

    public void setFoodcoverage(float foodcoverage) {
        this.foodcoverage = foodcoverage;
    }

    public float getFoodcoverageNoStock() {
        return foodcoverageNoStock;
    }

    public void setFoodcoverageNoStock(float foodcoverageNoStock) {
        this.foodcoverageNoStock = foodcoverageNoStock;
    }

    public int getInConsUsedPopulation() {
        return inConsUsedPopulation;
    }

    public void setInConsUsedPopulation(int inConsUsedPopulation) {
        this.inConsUsedPopulation = inConsUsedPopulation;
    }

    public int getInConsUsedSpace() {
        return inConsUsedSpace;
    }

    public void setInConsUsedSpace(int inConsUsedSpace) {
        this.inConsUsedSpace = inConsUsedSpace;
    }

    public String getMoraleChangeStr() {
        String change = " ";

        // DebugBuffer.addLine("morale="+morale+"  estimated="+estimatedMorale);        
        int diff = (int) (morale - estimatedMorale);
        // DebugBuffer.addLine("DIFF = " + diff);

        if (diff > 0) {
            change += "<FONT style=\"font-size:14px\" color=\"red\"><B>-";
            if (diff > 15) {
                change += "-";
            }
            change += "</B></FONT> ";
        } else if (diff < 0) {
            change += "<FONT color=\"green\"><B>+";
            if (diff < -15) {
                change += "+";
            }
            change += "</B></FONT> ";
        }

        return change;
    }

    public String getMoraleChangeStrWhite() {
        String change = " ";

        // DebugBuffer.addLine("morale="+morale+"  estimated="+estimatedMorale);
        int diff = (int) (morale - estimatedMorale);
        // DebugBuffer.addLine("DIFF = " + diff);

        if (diff > 0) {
            change += "-";
            if (diff > 15) {
                change += "-";
            }
        } else if (diff < 0) {
            change += "+";
            if (diff < -15) {
                change += "+";
            }
        }

        return change;
    }

    public void setCriminalityValue(double criminalityValue) {
        this.criminalityValue = criminalityValue;
    }

    /**
     * @return the criminalityValue
     */
    public double getCriminalityValue() {
        return criminalityValue;
    }

    /**
     * @return the riotValue
     */
    public double getRiotValue() {
        return riotValue;
    }

    /**
     * @param riotValue the riotValue to set
     */
    public void setRiotValue(double riotValue) {
        this.riotValue = riotValue;
    }

    /**
     * @return the riotValuChange
     */
    public double getRiotValueChange() {
        return riotValueChange;
    }

    /**
     * @param riotValuChange the riotValuChange to set
     */
    public void setRiotValueChange(double riotValueChange) {
        this.riotValueChange = riotValueChange;
    }

    /**
     * @return the devCriminality
     */
    public double getDevCriminality() {
        return devCriminality;
    }

    /**
     * @param devCriminality the devCriminality to set
     */
    public void setDevCriminality(double devCriminality) {
        this.devCriminality = devCriminality;
    }

    /**
     * @return the devMorale
     */
    public double getDevMorale() {
        return devMorale;
    }

    /**
     * @param devMorale the devMorale to set
     */
    public void setDevMorale(double devMorale) {
        this.devMorale = devMorale;
    }

    /**
     * @return the devRiots
     */
    public double getDevRiots() {
        return devRiots;
    }

    /**
     * @param devRiots the devRiots to set
     */
    public void setDevRiots(double devRiots) {
        this.devRiots = devRiots;
    }

    /**
     * @return the developementPoints
     */
    public double getDevelopementPoints() {
        return developementPoints;
    }

    /**
     * @param developementPoints the developementPoints to set
     */
    public void setDevelopementPoints(double developementPoints) {
        this.developementPoints = developementPoints;
    }

    /**
     * @return the developementPointsGlobal
     */
    public double getDevelopementPointsGlobal() {
        return developementPointsGlobal;
    }

    /**
     * @param developementPointsGlobal the developementPointsGlobal to set
     */
    public void setDevelopementPointsGlobal(double developementPointsGlobal) {
        this.developementPointsGlobal = developementPointsGlobal;
    }

    public float getMaxGrowthBonus() {
        return maxGrowthBonus;
    }

    public void setMaxGrowthBonus(float maxGrowthBonus) {
        this.maxGrowthBonus = maxGrowthBonus;
    }

    /**
     * @return the baseTaxIncome
     */
    public int getBaseTaxIncome() {
        return baseTaxIncome;
    }

    /**
     * @param baseTaxIncome the baseTaxIncome to set
     */
    public void setBaseTaxIncome(int baseTaxIncome) {
        this.baseTaxIncome = baseTaxIncome;
    }

    /**
     * @return the taxCrimDiscount
     */
    public int getTaxCrimDiscount() {
        return taxCrimDiscount;
    }

    /**
     * @param taxCrimDiscount the taxCrimDiscount to set
     */
    public void setTaxCrimDiscount(int taxCrimDiscount) {
        this.taxCrimDiscount = taxCrimDiscount;
    }

    /**
     * @return the taxRiotDiscount
     */
    public int getTaxRiotDiscount() {
        return taxRiotDiscount;
    }

    /**
     * @param taxRiotDiscount the taxRiotDiscount to set
     */
    public void setTaxRiotDiscount(int taxRiotDiscount) {
        this.taxRiotDiscount = taxRiotDiscount;
    }

    /**
     * @return the moraleBonusTax
     */
    public int getMoraleBonusTax() {
        return moraleBonusTax;
    }

    /**
     * @param moraleBonusTax the moraleBonusTax to set
     */
    public void setMoraleBonusTax(int moraleBonusTax) {
        this.moraleBonusTax = moraleBonusTax;
    }

    /**
     * @return the loyalityValue
     */
    public BigDecimal getLoyalityValue() {
        return loyalityValue;
    }

    /**
     * @param loyalityValue the loyalityValue to set
     */
    public void setLoyalityValue(BigDecimal loyalityValue) {
        this.loyalityValue = loyalityValue;
    }

    /**
     * @return the loyalityChange
     */
    public BigDecimal getLoyalityChange() {
        return loyalityChange;
    }

    /**
     * @param loyalityChange the loyalityChange to set
     */
    public void setLoyalityChange(BigDecimal loyalityChange) {
        this.loyalityChange = loyalityChange;
    }

    /**
     * @return the enemyLoyalityChange
     */
    public HashMap<Integer, BigDecimal> getEnemyLoyalityChange() {
        return enemyLoyalityChange;
    }

    /**
     * @param enemyLoyalityChange the enemyLoyalityChange to set
     */
    public void setEnemyLoyalityChange(HashMap<Integer, BigDecimal> enemyLoyalityChange) {
        this.enemyLoyalityChange = enemyLoyalityChange;
    }

    /**
     * @return the moralFood
     */
    public int getMoralFood() {
        return moralFood;
    }

    /**
     * @param moralFood the moralFood to set
     */
    public void setMoralFood(int moralFood) {
        this.moralFood = moralFood;
    }

    /**
     * @return the planetEconomySurfacePerc
     */
    public Map<EEconomyType,Float> getPlanetEconomySurfacePerc() {
        return planetEconomySurfacePerc;
    }

    /**
     * @param planetEconomySurfacePerc the planetEconomySurfacePerc to set
     */
    public void setPlanetEconomySurfacePerc(EEconomyType type,Float value) {
        this.planetEconomySurfacePerc.put(type, value);
    }

    /**
     * @return the planetEconomyWorkerPerc
     */
    public Map<EEconomyType,Float> getPlanetEconomyWorkerPerc() {
        return planetEconomyWorkerPerc;
    }

    /**
     * @param planetEconomyWorkerPerc the planetEconomyWorkerPerc to set
     */
    public void setPlanetEconomyWorkerPerc(EEconomyType type,Float value) {
        this.planetEconomyWorkerPerc.put(type, value);
    }
}
