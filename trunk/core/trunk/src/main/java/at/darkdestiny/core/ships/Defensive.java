/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ships;

/**
 *
 * @author Stefan
 */
public interface Defensive {
    public double getAbsorbtion(Offensive module, ShipDesignDetailed enemy);
}
