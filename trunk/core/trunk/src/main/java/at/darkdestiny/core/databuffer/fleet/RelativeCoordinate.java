package at.darkdestiny.core.databuffer.fleet;

import at.darkdestiny.core.dao.GalaxyDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.framework.dao.DAOFactory;

public class RelativeCoordinate implements ICoordinate {
    private static GalaxyDAO gDAO = (GalaxyDAO)DAOFactory.get(GalaxyDAO.class);
    private static SystemDAO sDAO = (SystemDAO)DAOFactory.get(SystemDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO)DAOFactory.get(PlanetDAO.class);

    private int systemId;
    private int planetId;

    public RelativeCoordinate(int systemId,int planetId) {
        this.systemId = systemId;
        this.planetId = planetId;

        if (systemId == 0) {
            determineSystemId();
        }
    }

    public int getSystemId() {
        return systemId;
    }

    public int getPlanetId() {
        return planetId;
    }

    public int getGalaxyId() {
        return sDAO.findById(getSystemId()).getGalaxyId();
    }

    public double distanceTo(RelativeCoordinate rc) {
        AbsoluteCoordinate ac1 = this.toAbsoluteCoordinate();
        AbsoluteCoordinate ac2 = rc.toAbsoluteCoordinate();

        if ((ac1 == null) || (ac2 == null)) return 0;

        double a = ac1.getX() - ac2.getX();
        double b = ac1.getY() - ac2.getY();

        return Math.sqrt(a * a + b * b);
    }

    public AbsoluteCoordinate toAbsoluteCoordinate() {
        if ((systemId == 0) && (planetId == 0)) {
            // DebugBuffer.addLine("Retrieving of Absolute Coordinate failed - Both ZERO");
            return null;
        }
        if ((systemId == 0) && (planetId != 0)) {
            determineSystemId();

            // log.debug("Trying to retrieve by planet ("+systemId+")");

            return sDAO.getAbsoluteCoordinate(systemId);
        }

        // log.debug("Trying to retrieve by System ("+systemId+")");
        return sDAO.getAbsoluteCoordinate(systemId);
    }

    private void determineSystemId() {
        Planet p = pDAO.findById(planetId);
        if(p == null){
            systemId = 0;
            return;
        }
        systemId = p.getSystemId();
    }
}
