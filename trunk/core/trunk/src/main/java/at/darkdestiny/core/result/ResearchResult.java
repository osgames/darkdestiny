/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

/**
 *
 * @author Stefan
 */
public class ResearchResult {
    private final int researchPoints;
    private final int compResearchPoints;
    private final int researchPointsBonus;
    private final int compResearchPointsBonus;
    
    public ResearchResult(int researchPoints, int compResearchPoints,
            int researchPointsBonus, int compResearchPointsBonus) {
        this.researchPoints = researchPoints;
        this.compResearchPoints = compResearchPoints;
        this.researchPointsBonus = researchPointsBonus;
        this.compResearchPointsBonus = compResearchPointsBonus;
    }

    public int getResearchPoints() {
        return researchPoints;
    }

    public int getCompResearchPoints() {
        return compResearchPoints;
    }
    
    public int getResearchPointsBonus() {
        return researchPointsBonus;        
    }
    
    public int getCompResearchPointsBonus() {
        return compResearchPointsBonus;
    }
}
