/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.admin.module;

 import at.darkdestiny.framework.access.DbConnect;import at.darkdestiny.util.DebugBuffer;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class ModuleConfiguration {

    private static final Logger log = LoggerFactory.getLogger(ModuleConfiguration.class);
    public static ArrayList<AdminModule> getAllModules() {
        ArrayList<AdminModule> modules = new ArrayList<AdminModule>();
        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name FROM module");
            while (rs.next()) {
                AdminModule am = new AdminModule();
                am.setId(rs.getInt(1));
                am.setName(rs.getString(2));
                modules.add(am);
            }

            stmt.close();
        } catch (Exception e) {
        }

        return modules;
    }
    public static TreeMap<String, ArrayList<AdminModule>> getAllModulesSortedByType(int userId) {
        TreeMap<String, ArrayList<AdminModule>> sorted = new TreeMap<String,  ArrayList<AdminModule>>();
        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name, type FROM module");
            while (rs.next()) {
                AdminModule am = new AdminModule();
                am.setId(rs.getInt(1));
                am.setName(rs.getString(2));
                ArrayList<AdminModule> ams = sorted.get(rs.getString(3));
                if(ams == null){
                    ams = new ArrayList<AdminModule>();
                }
                ams.add(am);
                sorted.put(rs.getString(3), ams);
            }

            stmt.close();
        } catch (Exception e) {
        }

        return sorted;
    }

    public static ArrayList<AdminAttribute> getAllAttributes() {
        ArrayList<AdminAttribute> attributes = new ArrayList<AdminAttribute>();

        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name, reference FROM attribute");
            while (rs.next()) {
                AdminAttribute aa = new AdminAttribute();
                aa.setId(rs.getInt(1));
                aa.setName(rs.getString(2));
                aa.setRef(Reference.valueOf(rs.getString(3)));
                attributes.add(aa);
            }

            stmt.close();
        } catch (Exception e) {
        }

        return attributes;
    }

    public static AdminAttribute getAttribute(int id) {
        AdminAttribute aa = null;

        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name, reference FROM attribute WHERE id=" + id);
            if (rs.next()) {
                aa = new AdminAttribute();
                aa.setId(rs.getInt(1));
                aa.setName(rs.getString(2));
                aa.setRef(Reference.valueOf(rs.getString(3)));
            }

            stmt.close();
        } catch (Exception e) {
        }

        return aa;
    }

    public static void deleteAttribute(int id) {
        try {
            Statement stmt = DbConnect.createStatement();
            stmt.execute("DELETE FROM attribute WHERE id=" + id);
            stmt.close();
        } catch (Exception e) {
        }
    }

    public static void modifyAttribute(Map params, boolean addMode) throws InvalidDataException {
        // Debug
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            log.debug("KEY=" + me.getKey() + " VALUE=" + ((String[]) me.getValue())[0]);
        }

        // Read all necessary parameters
        int aId = 0;
        String name = null;
        Reference ref = null;

        try {
            if (!addMode) {
                if (params.containsKey("id")) {
                    aId = Integer.parseInt(((String[]) params.get("id"))[0]);
                } else {
                    throw new InvalidDataException("Attribute Id is not specified!");
                }
            }

            if (params.containsKey("name") && !((String[]) params.get("name"))[0].equals("")) {
                name = ((String[]) params.get("name"))[0];
            } else {
                throw new InvalidDataException("Name is not specified!");
            }

            if (params.containsKey("reference")) {
                ref = Reference.valueOf(((String[]) params.get("reference"))[0]);
            } else {
                throw new InvalidDataException("Attribute Type is not specified!");
            }
        } catch (NumberFormatException nfe) {
            throw new InvalidDataException("An attribute had an wrong value type!");
        }

        try {
            Statement stmt = DbConnect.createStatement();

            if (addMode) {
                stmt.execute("INSERT INTO attribute (name, reference) VALUES ('" + name + "','" + ref + "')");
            } else {
                stmt.executeUpdate("UPDATE attribute SET name='" + name + "', reference='" + ref + "' WHERE id=" + aId);
            }

            stmt.close();
        } catch (Exception e) {
            throw new InvalidDataException("SQL ERROR: " + e.getMessage());
        }
    }

    public static AdminModule getModule(int id) {
        AdminModule am = null;

        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name FROM module WHERE id=" + id);
            if (rs.next()) {
                am = new AdminModule();
                am.setId(rs.getInt(1));
                am.setName(rs.getString(2));
            }

            stmt.close();
        } catch (Exception e) {
        }

        return am;
    }

    public static ModuleAttributeRel getModuleAttribute(int id) {
        ModuleAttributeRel mar = null;

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ma.id, ma.chassisId, a.id, ma.researchId, a.name, ma.attributeType, ma.value, ma.refId, ma.refType, ma.moduleId, ma.appliesTo " +
                    "FROM moduleattributes ma, attribute a WHERE ma.Id=" + id + " AND a.id=ma.attributeId");
            if (rs.next()) {
                mar = new ModuleAttributeRel();

                mar.setId(rs.getInt(1));
                mar.setModuleId(rs.getInt(10));
                mar.setChassisId(rs.getInt(2));
                mar.setAttributeId(rs.getInt(3));
                mar.setResearchId(rs.getInt(4));
                mar.setAttributeName(rs.getString(5));
                mar.setAttributeType(AttributeType.valueOf(rs.getString(6)));
                mar.setValue(rs.getDouble(7));
                mar.setRefId(rs.getInt(8));

                if (rs.getString(9) != null) {
                    mar.setRefType(ReferenceType.valueOf(rs.getString(9)));
                }

                mar.setAppliesTo(rs.getInt(11));
            }
        } catch (Exception e) {
        }

        return mar;
    }

    public static ArrayList<AdminChassis> getAllChassis() {
        ArrayList<AdminChassis> chassis = new ArrayList<AdminChassis>();

        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name FROM chassis");
            while (rs.next()) {
                AdminChassis ac = new AdminChassis();
                ac.setId(rs.getInt(1));
                ac.setName(rs.getString(2));
                chassis.add(ac);
            }

            stmt.close();
        } catch (Exception e) {
        }

        return chassis;
    }

    public static ArrayList<AdminResearch> getAllResearch() {
        ArrayList<AdminResearch> researches = new ArrayList<AdminResearch>();

        try {
            Statement stmt = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id, name FROM research");
            while (rs.next()) {
                AdminResearch ar = new AdminResearch();
                ar.setId(rs.getInt(1));
                ar.setName(rs.getString(2));
                researches.add(ar);
            }

            stmt.close();
        } catch (Exception e) {
        }

        return researches;
    }

    // Delete this relation and all dependend relations
    public static void deleteModuleAttributeRelation(int id) {
        try {
            // Find dependent entries and recursive call for deleting them
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT appliesTo FROM moduleattributes WHERE id=" + id);

            while (rs.next()) {
                deleteModuleAttributeRelation(rs.getInt(1));
            }

            stmt.close();

            Statement stmt2 = DbConnect.createStatement();
            stmt2.execute("DELETE FROM moduleattributes WHERE id=" + id);
            stmt2.close();
        } catch (Exception e) {
        }
    }

    public static void changeModuleAttributeRelation(Map params) throws InvalidDataException {
        // Debug
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            log.debug("KEY=" + me.getKey() + " VALUE=" + ((String[]) me.getValue())[0]);
        }

        ModuleAttributeRel virtualEntry = new ModuleAttributeRel();

        // Read all necessary parameters
        int moduleattributeId = 0;
        int researchId = 0;
        int chassisId = 0;
        int attributeId = 0;
        AttributeType attributeType = null;
        double value = 0d;
        int refId = 0;
        ReferenceType refType = null;
        int appliesTo = 0;

        try {
            if (params.containsKey("id")) {
                moduleattributeId = Integer.parseInt(((String[]) params.get("id"))[0]);
            } else {
                throw new InvalidDataException("ModuleAttributeId Id is not specified!");
            }

            if (params.containsKey("chassis")) {
                chassisId = Integer.parseInt(((String[]) params.get("chassis"))[0]);
            } else {
                throw new InvalidDataException("Chassis Id is not specified!");
            }

            if (params.containsKey("research")) {
                researchId = Integer.parseInt(((String[]) params.get("research"))[0]);
            } else {
                throw new InvalidDataException("Research Id is not specified!");
            }

            if (params.containsKey("attribute")) {
                attributeId = Integer.parseInt(((String[]) params.get("attribute"))[0]);
            } else {
                throw new InvalidDataException("Attribute Id is not specified!");
            }

            if (params.containsKey("attributeType")) {
                attributeType = AttributeType.valueOf(((String[]) params.get("attributeType"))[0]);
            } else {
                throw new InvalidDataException("Attribute Type is not specified!");
            }

            if (params.containsKey("value")) {
                value = Double.parseDouble(((String[]) params.get("value"))[0]);
            } else {
                throw new InvalidDataException("Value is not specified!");
            }

            if (params.containsKey("appliesTo")) {
                appliesTo = Integer.parseInt(((String[]) params.get("appliesTo"))[0]);
            } else {
                throw new InvalidDataException("AppliesTo is not specified!");
            }

            if (params.containsKey("refId")) {
                refId = Integer.parseInt(((String[]) params.get("refId"))[0]);

                if (refId != 0) { // A ref type has to be set as well!
                    if (params.containsKey("refType")) {
                        String refTypeValue = ((String[]) params.get("refType"))[0];

                        if (refTypeValue.equalsIgnoreCase("-")) {
                            throw new InvalidDataException("Reference Type is not specified!");
                        }
                        refType = ReferenceType.valueOf(refTypeValue);
                    } else {
                        throw new InvalidDataException("Reference Type is not specified!");
                    }
                }
            }
        } catch (NumberFormatException nfe) {
            throw new InvalidDataException("An attribute had an wrong value type!");
        }

        virtualEntry.setChassisId(chassisId);
        virtualEntry.setAttributeId(attributeId);
        virtualEntry.setAttributeType(attributeType);
        virtualEntry.setResearchId(researchId);
        virtualEntry.setRefId(refId);
        virtualEntry.setRefType(refType);
        virtualEntry.setValue(value);
        virtualEntry.setAppliesTo(appliesTo);

        checkModelAttributeConstraint(virtualEntry);

        try {
            String refTypeStr = "";
            if (refType != null) {
                refTypeStr = ", refType='" + refType + "'";
            }
            Statement stmt = DbConnect.createStatement();
            stmt.executeUpdate("UPDATE moduleattributes SET chassisId=" + chassisId + ", researchId=" + researchId + ", " +
                    "attributeId=" + attributeId + ", attributeType='" + attributeType + "', value=" + value + ", " +
                    "refId=" + refId + "" + refTypeStr + ", appliesTo=" + appliesTo + " " +
                    "WHERE id=" + moduleattributeId);
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while saving into database:<BR>", e);
            throw new InvalidDataException("Database store failed!");
        }
    }

    public static void addModuleAttributeRelation(Map params) throws InvalidDataException {
        // Debug
        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            log.debug("KEY=" + me.getKey() + " VALUE=" + ((String[]) me.getValue())[0]);
        }

        ModuleAttributeRel virtualEntry = new ModuleAttributeRel();

        // Read all necessary parameters
        int moduleId = 0;
        int researchId = 0;
        int chassisId = 0;
        int attributeId = 0;
        double value = 0d;
        int refId = 0;
        ReferenceType refType = null;
        AttributeType attributeType = null;
        int appliesTo = 0;

        try {
            if (params.containsKey("id")) {
                moduleId = Integer.parseInt(((String[]) params.get("id"))[0]);
            } else {
                throw new InvalidDataException("Module Id is not specified!");
            }

            if (params.containsKey("chassis")) {
                chassisId = Integer.parseInt(((String[]) params.get("chassis"))[0]);
            } else {
                throw new InvalidDataException("Chassis Id is not specified!");
            }

            if (params.containsKey("research")) {
                researchId = Integer.parseInt(((String[]) params.get("research"))[0]);
            } else {
                throw new InvalidDataException("Research Id is not specified!");
            }

            if (params.containsKey("attribute")) {
                attributeId = Integer.parseInt(((String[]) params.get("attribute"))[0]);
            } else {
                throw new InvalidDataException("Attribute Id is not specified!");
            }

            if (params.containsKey("attributeType")) {
                attributeType = AttributeType.valueOf(((String[]) params.get("attributeType"))[0]);
            } else {
                throw new InvalidDataException("Attribute Type is not specified!");
            }

            if (params.containsKey("value")) {
                value = Double.parseDouble(((String[]) params.get("value"))[0]);
            } else {
                throw new InvalidDataException("Value is not specified!");
            }

            if (params.containsKey("appliesTo")) {
                appliesTo = Integer.parseInt(((String[]) params.get("appliesTo"))[0]);
            } else {
                throw new InvalidDataException("AppliesTo is not specified!");
            }

            if (params.containsKey("refId")) {
                refId = Integer.parseInt(((String[]) params.get("refId"))[0]);

                if (refId != 0) { // A ref type has to be set as well!
                    if (params.containsKey("refType")) {
                        String refTypeValue = ((String[]) params.get("refType"))[0];

                        if (refTypeValue.equalsIgnoreCase("-")) {
                            throw new InvalidDataException("Reference Type is not specified!");
                        }
                        refType = ReferenceType.valueOf(refTypeValue);
                    } else {
                        throw new InvalidDataException("Reference Type is not specified!");
                    }
                }
            }
        } catch (NumberFormatException nfe) {
            throw new InvalidDataException("An attribute had an wrong value type!");
        }

        virtualEntry.setChassisId(chassisId);
        virtualEntry.setAttributeId(attributeId);
        virtualEntry.setAttributeType(attributeType);
        virtualEntry.setResearchId(researchId);
        virtualEntry.setRefId(refId);
        virtualEntry.setRefType(refType);
        virtualEntry.setValue(value);
        virtualEntry.setAppliesTo(appliesTo);

        checkModelAttributeConstraint(virtualEntry);

        try {
            String refTypeStr = "";
            String refTypeStrFld = "";
            if (refType != null) {
                refTypeStr = ",'" + refType + "'";
                refTypeStrFld = ", refType";
            }

            Statement stmt = DbConnect.createStatement();
            stmt.execute("INSERT INTO moduleattributes (moduleId, chassisId, researchId, attributeId, attributeType, value, refId" + refTypeStrFld + ", appliesTo) " +
                    "VALUES (" + moduleId + "," + chassisId + "," + researchId + "," + attributeId + ",'" + attributeType + "'," + value + "," + refId + "" + refTypeStr + "," + appliesTo + ")");
            stmt.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while saving into database:<BR>", e);
            throw new InvalidDataException("DATABASE ERROR: " + e);
        }
    }

    private static void checkModelAttributeConstraint(ModuleAttributeRel mar) throws InvalidDataException {
        // Check if attribute allows reference
        Reference ref = null;

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT reference FROM attribute WHERE id=" + mar.getAttributeId());
            if (rs.next()) {
                ref = Reference.valueOf(rs.getString(1));
            } else {
                throw new InvalidDataException("Specified Attribute does not exists");
            }

            stmt.close();
        } catch (Exception e) {
            throw new InvalidDataException("DATABASE ERROR: " + e.getMessage());
        }

        if (mar.getRefId() != 0) {
            if (ref == Reference.NOT_ALLOWED) {
                throw new InvalidDataException("Reference not allowed");
            }
        } else {
            if (ref == Reference.REQUIRED) {
                throw new InvalidDataException("Required Reference not specified");
            }
        }

        // Check for attributeId on INCREASE, DECREASE Entries
        if (mar.getAttributeType() == AttributeType.DEFINE) {
            if (mar.getAppliesTo() != 0) {
                throw new InvalidDataException("No appliesTo Entry allowed!");
            }
        } else {
            if (mar.getAppliesTo() == 0) {
                throw new InvalidDataException("AppliesTo must be specified for Attribute Type " + mar.getAttributeType());
            }

            try {
                Statement stmt = DbConnect.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT id FROM moduleattributes WHERE id="+mar.getAppliesTo());
                if (!rs.next()) {
                    throw new InvalidDataException("Referenced ModuleAttribute Entry does not exist!");
                }

                stmt.close();
            } catch (Exception e) {
                throw new InvalidDataException("DATABASE ERROR: " + e.getMessage());
            }
        }
    }
}
