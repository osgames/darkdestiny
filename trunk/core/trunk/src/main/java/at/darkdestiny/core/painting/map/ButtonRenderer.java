/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.util.DebugBuffer;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import javax.imageio.ImageIO;

/**
 *
 * @author Stefan
 */
public class ButtonRenderer implements IModifyImageFunction {  
    private int width;
    private int height;
    private int userId;
    
    private static Image up = null;
    private static Image down = null;
    private static Image left = null;
    private static Image right = null;    
    private static Image zoomIn = null; 
    private static Image zoomOut = null; 
    
    private int xCenter = 0;
    
    public ButtonRenderer(int userId, String imageLocation) {
        this.userId = userId;
        
        try {
            if (up == null) up = ImageIO.read(new File(imageLocation + "arrup.png"));            
            if (down == null) down = ImageIO.read(new File(imageLocation + "arrdown.png"));   
            if (left == null) left = ImageIO.read(new File(imageLocation + "arrleft.png"));   
            if (right == null) right = ImageIO.read(new File(imageLocation + "arrright.png"));               
            if (zoomIn == null) zoomIn = ImageIO.read(new File(imageLocation + "zoomIn.png"));   
            if (zoomOut == null) zoomOut = ImageIO.read(new File(imageLocation + "zoomOut.png"));              
        } catch (Exception e) {
            DebugBuffer.error("Did not find picture: ",e);
        }        
        
        System.out.println("Image path: " + imageLocation);
    }    
    
    @Override
    public void run(Graphics graphics) {
        Graphics2D g2d = (Graphics2D)graphics;
        
        xCenter = (int)Math.floor(width / 2d);        
        
        g2d.drawImage(up, xCenter - (int)Math.floor(up.getWidth(null) / 2d), height - up.getHeight(null) * 2, null);    
        g2d.drawImage(down, xCenter - (int)Math.floor(up.getWidth(null) / 2d), height - up.getHeight(null), null); 
        g2d.drawImage(left, xCenter - (int)Math.floor(up.getWidth(null) / 2d) - up.getWidth(null), height - up.getHeight(null), null); 
        g2d.drawImage(right, xCenter - (int)Math.floor(up.getWidth(null) / 2d) + up.getWidth(null), height - up.getHeight(null), null); 
        g2d.drawImage(zoomIn, xCenter - (int)Math.floor(up.getWidth(null) / 2d) + up.getWidth(null), height - up.getHeight(null) * 2, null); 
        g2d.drawImage(zoomOut, xCenter - (int)Math.floor(up.getWidth(null) / 2d) - up.getWidth(null), height - up.getHeight(null) * 2, null);         
    }       
    
    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }    
    
    @Override
    public String createMapEntry() {
        StringBuilder buttons = new StringBuilder();
        
        int tlx = (xCenter - (int)Math.floor(up.getWidth(null) / 2d));
        int tly = (height - up.getHeight(null) * 2);
        int brx = tlx + up.getWidth(null);
        int bry = tly + up.getHeight(null);
        buttons.append("<area shape=\"rect\" coords=\"" + 
                tlx + "," + 
                tly + 
                "," + brx + 
                "," + bry + "\"\n" +
        /*
"               onClick=\"window.location.href = 'main.jsp?page=new/staticstarmap&op=up'\" alt=\"Up\" title=\"Up\">");
        */                
                "onClick=\"reloadStarMap('up')\" alt=\"Up\" title=\"Up\">");
        tlx = xCenter - (int)Math.floor(up.getWidth(null) / 2d);
        tly = height - up.getHeight(null);
        brx = tlx + up.getWidth(null);
        bry = tly + up.getHeight(null);
        buttons.append("<area shape=\"rect\" coords=\"" + 
                tlx + "," + 
                tly + 
                "," + brx + 
                "," + bry + "\"\n" +
        /*
"               onClick=\"window.location.href = 'main.jsp?page=new/staticstarmap&op=down'\" alt=\"Down\" title=\"Down\">");     
        */
                "onClick=\"reloadStarMap('down')\" alt=\"Down\" title=\"Down\">");
        
        tlx = xCenter - (int)Math.floor(up.getWidth(null) / 2d) - up.getWidth(null);
        tly = (height - up.getHeight(null));
        brx = tlx + up.getWidth(null);
        bry = tly + up.getHeight(null);
        buttons.append("<area shape=\"rect\" coords=\"" + 
                tlx + "," + 
                tly + 
                "," + brx + 
                "," + bry + "\"\n" +
        /*
"               onClick=\"window.location.href = 'main.jsp?page=new/staticstarmap&op=left'\" alt=\"Left\" title=\"Left\">");    
        */
                "onClick=\"reloadStarMap('left')\" alt=\"Left\" title=\"Left\">");
        
        tlx = xCenter - (int)Math.floor(up.getWidth(null) / 2d) + up.getWidth(null);
        tly = (height - up.getHeight(null));
        brx = tlx + up.getWidth(null);
        bry = tly + up.getHeight(null);
        buttons.append("<area shape=\"rect\" coords=\"" + 
                tlx + "," + 
                tly + 
                "," + brx + 
                "," + bry + "\"\n" +
        /*
"               onClick=\"window.location.href = 'main.jsp?page=new/staticstarmap&op=right'\" alt=\"Right\" title=\"Right\">");        
        */
                "onClick=\"reloadStarMap('right')\" alt=\"Right\" title=\"Right\">");
        
        tlx = (xCenter - up.getWidth(null) - (int)Math.floor(up.getWidth(null) / 2d));
        tly = (height - up.getHeight(null) * 2);
        brx = tlx + up.getWidth(null);
        bry = tly + up.getHeight(null);
        buttons.append("<area shape=\"rect\" coords=\"" + 
                tlx + "," + 
                tly + 
                "," + brx + 
                "," + bry + "\"\n" +
        /*
"               onClick=\"window.location.href = 'main.jsp?page=new/staticstarmap&op=zoomout'\" alt=\"Zoom Out\" title=\"Zoom Out\">");   
        */
                "onClick=\"reloadStarMap('zoomout')\" alt=\"Zoom Out\" title=\"Zoom Out\">");
        
        tlx = (xCenter + up.getWidth(null) - (int)Math.floor(up.getWidth(null) / 2d));
        tly = (height - up.getHeight(null) * 2);
        brx = tlx + up.getWidth(null);
        bry = tly + up.getHeight(null);
        buttons.append("<area shape=\"rect\" coords=\"" + 
                tlx + "," + 
                tly + 
                "," + brx + 
                "," + bry + "\"\n" +
        /*
"               onClick=\"window.location.href = 'main.jsp?page=new/staticstarmap&op=zoomin'\" alt=\"Zoom In\" title=\"Zoom In\">");        
        */
                "onClick=\"reloadStarMap('zoomin')\" alt=\"Zoom In\" title=\"Zoom In\">");
        
        AreaMapContainer.addToAreaMap(userId, buttons.toString());
        return buttons.toString();
    }        
}
