/*
 * FleetMovement.java
 *
 * Created on 25. Juli 2004, 12:12
 */
package at.darkdestiny.core.fleet;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.FleetDetailDAO;
import at.darkdestiny.core.dao.FleetFormationDAO;
import at.darkdestiny.core.dao.FleetOrderDAO;
import at.darkdestiny.core.dao.GalaxyDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.ViewTableDAO;
import at.darkdestiny.core.databuffer.fleet.Location;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.enumeration.EViewSystemLocationType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.FleetDetail;
import at.darkdestiny.core.model.FleetFormation;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.ViewTable;
import at.darkdestiny.core.movable.FleetFormationExt;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.requestbuffer.FleetParameterBuffer;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.FlightResult;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.service.TransmitterService;
import at.darkdestiny.core.ships.ShipUtilitiesNonStatic;
import at.darkdestiny.core.update.FlightProcessingEntry;
import at.darkdestiny.core.utilities.TitleUtilities;
import at.darkdestiny.core.utilities.ViewTableUtilities;
import at.darkdestiny.core.view.TransmitterTargetEntry;
import at.darkdestiny.core.viewbuffer.HomeSystemBuffer;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.exception.TransactionException;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 *
 * @author Stefan
 */
public class FleetMovement {

    private static final Logger log = LoggerFactory.getLogger(FleetMovement.class);

    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static GalaxyDAO gDAO = (GalaxyDAO) DAOFactory.get(GalaxyDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static FleetFormationDAO ffDAO = (FleetFormationDAO) DAOFactory.get(FleetFormationDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static FleetOrderDAO foDAO = (FleetOrderDAO) DAOFactory.get(FleetOrderDAO.class);

    boolean error;
    String errorMess;
    private Map<String, Object> parMap;
    private FleetOrder fo = null;
    public final static int MOVE_FROM_SYSVIEW = 2;
    public final static int MOVE_FROM_FLEETVIEW = 1;
    // Constructor for generating a fleet movement
    //TODO This uses parameters from the JSP-File!
    /*
     public FleetMovement(Map<String, Object> pMap, int userId, int source) {
     this.parMap = pMap;
     this.userId = userId;
     }

     // Constructor for finishing a fleet movement
     public FleetMovement(int fleetId) {
     this.fleetId = fleetId;
     }
     */

    public static final String ERR_NOT_IN_RANGE = "fleettarget_err_notinrange";
    public static final String ERR_SYSTEM_NOT_FOUND = "fleettarget_err_nosystemfound";
    public static final String ERR_PLANET_NOT_FOUND = "fleettarget_err_noplanetfound";
    public static final String ERR_TOO_MANY_SYSTEMS = "fleettarget_err_foundtoomanysystems";
    public static final String ERR_TOO_MANY_PLANETS = "fleettarget_err_foundtoomanyplanets";
    public static final String ERR_SYSTEM_NOT_VISIBLE = "fleettarget_err_systemnotvisible";
    public static final String ERR_PLANET_NOT_VISIBLE = "fleettarget_err_planetfoundbutsystemnotvisible";
    public static final String ERR_NOT_YOUR_FLEET = "fleettarget_err_notyourfleet";

    private static boolean systemIsVisible(int userId, int systemId) {
        if (Service.viewSystemDAO.findBy(systemId, EViewSystemLocationType.SYSTEM, userId) == null
            && Service.viewTableDAO.findByUserAndSystem(userId, systemId).isEmpty()) {
            return false;
        }

        return true;
    }

    public static BaseResult checkFleetOrders(int userId, FleetParameterBuffer fpb) {
        int fleetId;
        int retreatFactor;
        boolean retreatLocIsSystem;
        int retreatTo;

        try {
            fleetId = fpb.getFleetId();
            retreatFactor = fpb.getRetreatValue();
            retreatLocIsSystem = fpb.isRetreatLocSystem();
            retreatTo = fpb.getRetreatTo();
        } catch (Exception e) {
            // DebugBuffer.writeStackTrace("Paramter Error: ", e);
            String msg = ML.getMLStr("fleetmovement_err_invalidparameter", userId) + "(0)";
            msg = msg.replace("%ERR%", e.getMessage());
            return new BaseResult(msg, true);
        }

        try {
            // CHECK FLEET ORDERS
            System.out.println("Checking retreat orders ["+retreatFactor+"]["+retreatLocIsSystem+"]["+retreatTo+"]");

            ShipUtilitiesNonStatic su = new ShipUtilitiesNonStatic(userId, 0);
            at.darkdestiny.core.model.FleetOrder foTmp = new at.darkdestiny.core.model.FleetOrder();
            foTmp.setRetreatFactor(retreatFactor);
            foTmp.setRetreatToType(fpb.isRetreatLocSystem() ? ELocationType.SYSTEM : ELocationType.PLANET);
            foTmp.setRetreatTo(retreatTo);

            su.checkFleetOrders(fleetId, fpb.isFleetFormation(), foTmp);

            RelativeCoordinate rcRetreat;
            if (fpb.isRetreatLocSystem()) {
                rcRetreat = new RelativeCoordinate(retreatTo,0);
            } else {
                rcRetreat = new RelativeCoordinate(0,retreatTo);
            }

            System.out.println("Checking Coordinate: " + rcRetreat.getSystemId() + "/" + rcRetreat.getPlanetId());
            if (rcRetreat.getSystemId() != 0) {
                if (!systemIsVisible(userId,rcRetreat.getSystemId())) {
                    System.out.println("System is not visible");
                    // return new BaseResult(ML.getMLStr(ERR_SYSTEM_NOT_VISIBLE, userId), true);
                } else {
                    System.out.println("System is visible");
                }
            }

            if (su.outMsg != null) {
                return new BaseResult(su.outMsg, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new BaseResult(ML.getMLStr("fleetmovement_err_invalidvalues", userId) + "(1)", true);
        }

        return new BaseResult(ML.getMLStr("fleetmovement_msg_allok", userId) + "(10)", false);
    }

    public static FlightResult checkFlightParameters(int userId, FleetParameterBuffer fpb) {
        int fleetId;
        boolean targetIsSystem = false;
        int targetId = 0;
        int destsystem;
        int destplanet;
        int checkType;
        int actionType;
        int preFlightTime;
        double preDistance;

        // Try to retrieve Coordinates by Location, if not possible fall back to targetIsSystem 
        try {
            fleetId = fpb.getFleetId();
        } catch (Exception e) {
            DebugBuffer.error("Parsing of fleet id failed");
            String msg = ML.getMLStr("fleetmovement_err_invalidparameter", userId) + "(0)";
            msg = msg.replace("%ERR%", e.getMessage());
            return new FlightResult(msg, true);            
        }
        
        boolean locFailed = false;
        boolean targetIsSystemFailed = false;
                        
        try {
            Location loc = fpb.getLocation();
            
            if (loc != null) {
                RelativeCoordinate rc = loc.toRelativeCoordinate();

                if (loc.getLocationType() == ELocationType.PLANET) {
                    targetIsSystem = false;
                    targetId = rc.getPlanetId();
                } else {
                    targetIsSystem = true;
                    targetId = rc.getSystemId();
                }
            } else {
                locFailed = true;
            }
        } catch (Exception e) {
            locFailed = true;
            DebugBuffer.writeStackTrace("Error while determining target by Location: ", e);
        }        
        
        if (locFailed) {
            try {                            
                targetIsSystem = fpb.targetIsSystem();
                targetId = fpb.getTarget();
            } catch (Exception e) {
                // DebugBuffer.writeStackTrace("Paramter Error: ", e);
                String msg = ML.getMLStr("fleetmovement_err_invalidparameter", userId) + "(0)";
                msg = msg.replace("%ERR%", e.getMessage());
                return new FlightResult(msg, true);
            }
        }

        at.darkdestiny.core.model.System targetSystem = null;
        Planet targetPlanet = null;

        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Target is system = " + targetIsSystem + " targetId = " + targetId);

        if (targetIsSystem) {
            targetSystem = sDAO.findById(targetId);
        } else {
            targetPlanet = pDAO.findById(targetId);
            if (targetPlanet != null) {
                targetSystem = sDAO.findById(targetPlanet.getSystemId());
            }
        }

        BaseResult orderCheck = checkFleetOrders(userId, fpb);
        if (orderCheck.isError()) {
            return new FlightResult(orderCheck.getMessage(),true);
        }

        /*
         *
         * @TODO Abfrage ob der User Im Trial Modus ist�// Allianz
         */
        if ((targetSystem == null)) {
            return new FlightResult(ML.getMLStr("fleetmovement_err_novalidsystem", userId) + "(2)", true);
        }

        for (Planet p : pDAO.findBySystemId(targetSystem.getId())) {
            PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());

            ArrayList<ViewTable> vtEntries = vtDAO.findByUserAndSystem(userId, targetSystem.getId());
            if (!vtEntries.isEmpty()) {
                if (pp != null) {
                    if (uDAO.findById(pp.getUserId()).getTrial() && pp.getUserId() != userId) {
                        return new FlightResult(ML.getMLStr("fleetmovement_err_novalidtargettrialmode", userId) + "(3)", true);
                    }
                }
            }
        }

        if (!targetIsSystem && (targetPlanet == null)) {
            return new FlightResult(ML.getMLStr("fleetmovement_err_novalidplanet", userId) + "(4)", true);
        }

        Movable fleet = null;

        if (fpb.isFleetFormation()) {
            try {
                log.debug("Initialize fleetformation " + fpb.getFleetId());
                fleet = new FleetFormationExt(fpb.getFleetId());
            } catch (Exception e) {
                return new FlightResult(e.getMessage(), true);
            }
        } else {
            log.debug("Initialize fleet " + fpb.getFleetId());
            fleet = new PlayerFleetExt(fpb.getFleetId());
        }

        if (fleet == null) {
            return new FlightResult(ML.getMLStr("fleetmovement_err_fleetnotexisting", userId) + "(5)", true);
        }

        PlayerFleetExt pfe = null;
        FleetFormationExt ffe = null;

        boolean invalidAccess = false;

        if (fpb.isFleetFormation()) {
            ffe = (FleetFormationExt) fleet;
            if (ffe.getBase().getUserId() != userId) {
                invalidAccess = true;
            }
        } else {
            pfe = (PlayerFleetExt) fleet;
            if (pfe.getBase().getUserId() != userId) {
                invalidAccess = true;
            }
        }

        if (invalidAccess) {
            return new FlightResult(ML.getMLStr("fleetmovement_err_invalidfleet", userId) + "(6)", true);
        }
        if (fleet.isMoving()) {
            return new FlightResult(ML.getMLStr("fleetmovement_err_invalidfleet", userId) + "(7)", true);
        }
        
        RelativeCoordinate targetCoord;

        if (targetIsSystem) {
            fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, "true");
            targetCoord = new RelativeCoordinate(targetId,0);
        } else {
            fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, "false");
            targetCoord = new RelativeCoordinate(0,targetId);
        }
        boolean flightIsTransmitter = TransmitterService.flightIsTransmitter(userId, fleet.getRelativeCoordinate().getSystemId(), targetCoord.getSystemId());

        // TODO This check can be partly moved down
        log.debug("Fleetspeed : " + fleet.getSpeed());
        if ((fleet.getRelativeCoordinate().getSystemId() != targetSystem.getId())
                && !fleet.canFlyInterstellar() && !flightIsTransmitter) {

            log.debug("Part 1");
            return new FlightResult(ML.getMLStr("fleetmovement_err_interstellarflightnotpossible" + "(8)", userId), true);
        } else {
            /*
            if (fleet.getRelativeCoordinate().getSystemId() == targetSystem.getId()) {
                log.debug("Part 2");
                fpb.setParameter("timeToTarget", 1);
                fpb.setParameter("distance", 0);
            } else {
                log.debug("Part 3");
                AbsoluteCoordinate ac1 = fleet.getAbsoluteCoordinate();
                AbsoluteCoordinate ac2 = new AbsoluteCoordinate(targetSystem.getX(), targetSystem.getY());

                double distance = ac1.distanceTo(ac2);
                log.debug("Part 3.1");
                fpb.setParameter("timeToTarget", (int) Math.ceil(distance / fleet.getSpeed()));
                fpb.setParameter("distance", distance);
                log.debug("Part 3.2 (Distance is " + distance + ")");
            }
            */
        }

        // Check if target and start are in valid galaxy
        int galaxyStart = fleet.getRelativeCoordinate().getGalaxyId();
        int galaxyTarget = new RelativeCoordinate(targetSystem.getId(), 0).getGalaxyId();

        if (galaxyStart != galaxyTarget) {
            if (!gDAO.findById(galaxyStart).isIntergalacticFlight() || !gDAO.findById(galaxyTarget).isIntergalacticFlight()) {
                return new FlightResult(ML.getMLStr("fleetmovement_err_galaxylocked", userId) + "(9)", true);
            }
        }

        // --------------------------
        // Distance check f�r Servlet
        // --------------------------
        String errMessage;
        HashMap<String, String> result = Maps.newHashMap();
        // PlayerFleet pf = Service.playerFleetDAO.findById(fleetId);
        // PlayerFleetExt pfExt = new PlayerFleetExt(pf);

        if (targetSystem != null) {
            if (!systemIsVisible(userId,targetSystem.getId())) {
                // return new FlightResult(ML.getMLStr(ERR_SYSTEM_NOT_VISIBLE, userId), true);
            }
        }

        int distance = (int) Math.ceil(Math.sqrt(Math.pow(targetCoord.toAbsoluteCoordinate().getX() - fleet.getAbsoluteCoordinate().getX(), 2d) + Math.pow(targetCoord.toAbsoluteCoordinate().getY() - fleet.getAbsoluteCoordinate().getY(), 2d)));
        int flightTime = 0;
        int range = fleet.getRange();

        FlightResult okResult = new FlightResult(ML.getMLStr("fleetmovement_msg_allok", userId), false);

        if ((range < distance) && !flightIsTransmitter) {
            errMessage = ERR_NOT_IN_RANGE;

            return new FlightResult(ML.getMLStr(errMessage, userId),true);
        } else {
            flightTime = Math.max(1,(int) ((double) distance / fleet.getSpeed()));

            if (flightIsTransmitter) {
                ArrayList<TransmitterTargetEntry> targetEntries = TransmitterService.getTransmitterTargets(FleetService.getFleet(fleetId).getRelativeCoordinate().getSystemId(), userId).getConnectedTransmitters();
                boolean targetKnown = false;

                fpb.setParameter("distance", -1);
                okResult.setDistance(-1f);
                for (TransmitterTargetEntry tte : targetEntries) {
                    if ((tte.getSystemId() == targetCoord.getSystemId()) && tte.isTargetKnown()) {
                        fpb.setParameter("distance", distance);
                        okResult.setDistance(distance);
                        break;
                    }
                }

                fpb.setParameter("timeToTarget", 1);
                okResult.setFlightTime(1);
                okResult.setRange(-1);
            } else {
                fpb.setParameter("timeToTarget", flightTime);
                fpb.setParameter("distance", distance);
                okResult.setDistance(distance);
                okResult.setFlightTime(flightTime);
                okResult.setRange(range);
            }
            // result.put(PAR_SUBTRACT, String.valueOf(distance - range));
        }
        // --------------------------
        // Distance check f�r Servlet
        // --------------------------

        fpb.setParameter("targetSystem", targetSystem);
        fpb.setParameter("targetPlanet", targetPlanet);
        log.debug("Fleet : " + fleet);
        fpb.setParameter("fleet", fleet);

        User u = uDAO.findById(userId);
        if (u.getTrial()) {
            fpb.setParameter(FleetParameterBuffer.TRIAL_WARNING, "true");
        }
        if (FleetService.findRunningScan(userId, fleetId) != null) {
            fpb.setParameter(FleetParameterBuffer.SCAN_WARNING, "true");
        }
        return okResult;
    }

    public static BaseResult generateFlight(int userId, FleetParameterBuffer fpb) throws Exception {
        PlayerFleetExt pfe = null;
        FleetFormationExt ffe = null;
        Movable fleet = null;

        ArrayList<PlayerFleetExt> fleetsToMove = new ArrayList<PlayerFleetExt>();
        float speed = 0;

        if (fpb.isFleetFormation()) {
            ffe = (FleetFormationExt) fpb.getParameter("fleet");
            fleet = ffe;
            speed = (float) ffe.getSpeed();
            fleetsToMove.addAll(ffe.getParticipatingFleets());
        } else {
            pfe = (PlayerFleetExt) fpb.getParameter("fleet");
            log.debug("pfe : " + pfe);
            fleet = pfe;
            speed = (float) pfe.getSpeed();
            fleetsToMove.add(pfe);
        }

        at.darkdestiny.core.model.System targetSystem = (at.darkdestiny.core.model.System) fpb.getParameter("targetSystem");

        /*
        if(targetSystem != null){
            if(Service.viewSystemDAO.findBy(targetSystem.getId(), EViewSystemLocationType.SYSTEM, userId) == null
                    && Service.viewTableDAO.findByUserAndSystem(userId, targetSystem.getId()) == null){
                return new BaseResult("System not visible", true);
            }
        }
        */


        Planet targetPlanet = (Planet) fpb.getParameter("targetPlanet");
        int timeToTarget = Integer.parseInt(fpb.getParameter("timeToTarget").toString());

        /*
        log.debug("SYSTEM COORDINATE: " + fleet.getRelativeCoordinate().getSystemId() + " TARGETSYSTEM COORINATE: " + targetSystem.getId());
        if (fleet.getRelativeCoordinate().getSystemId() == targetSystem.getId()) {
            timeToTarget = 1;
        } else {
            AbsoluteCoordinate ac1 = fleet.getAbsoluteCoordinate();
            AbsoluteCoordinate ac2 = new AbsoluteCoordinate(targetSystem.getX(), targetSystem.getY());

            if ((ac1 == null) || (ac2 == null)) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Something was null for Fleet " + pfe.getName() + " (" + pfe.getId() + ") (AC1=" + ac1 + " AC2=" + ac2 + ")");
            }

            double distance = ac1.distanceTo(ac2);
            if(fleet.getRange() < distance){
                return new BaseResult("System not in Range", true);
            }
            timeToTarget = (int) Math.ceil(distance / fleet.getSpeed());
        }
        */

        TransactionHandler th = TransactionHandler.getTransactionHandler();
        th.startTransaction();

        String error = null;

        if (fpb.isScanWarning()) {
            try {
                ArrayList<Action> result = new ArrayList<Action>();

                Action a = new Action();
                a.setType(EActionType.SCAN_PLANET);
                a.setUserId(fpb.getUserId());
                a.setFleetId(fpb.getFleetId());
                result.addAll(aDAO.find(a));
                a.setType(EActionType.SCAN_SYSTEM);
                result.addAll(aDAO.find(a));

                if (result.size() != 1) {
                    DebugBuffer.error("No unique scanning entry found for aborting of scanning action for fleet " + fpb.getFleetId());
                } else {
                    aDAO.remove(result.get(0));
                }
            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.WARNING, "Konnte Scanaction nicht abbrechen f�r Flotte : " + fpb.getFleetId());
            }
        }

        // Get retreat information
        int retreatPercentage = fpb.getRetreatValue();
        boolean retreatLocIsSystem = fpb.isRetreatLocSystem();
        int retreatLoc = fpb.getRetreatTo();

        boolean retreatSet = false;

        try {
            for (PlayerFleetExt actFleet : fleetsToMove) {
                PlayerFleet pf = actFleet.getBase();

                at.darkdestiny.core.model.FleetDetail fd = new at.darkdestiny.core.model.FleetDetail();
                fd.setFleetId(pf.getId());
                fd.setStartSystem(pf.getSystemId());
                fd.setStartPlanet(pf.getPlanetId());
                fd.setStartX(actFleet.getAbsoluteCoordinate().getX());
                fd.setStartY(actFleet.getAbsoluteCoordinate().getY());
                fd.setDestSystem(targetSystem.getId());
                if (targetPlanet != null) {
                    fd.setDestPlanet(targetPlanet.getId());
                } else {
                    fd.setDestPlanet(0);
                }
                fd.setDestX(targetSystem.getX());
                fd.setDestY(targetSystem.getY());
                fd.setFlightTime(timeToTarget);
                fd.setSpeed(speed);
                fd.setCommandType(0);
                fd.setCommandTypeId(0);
                if (fpb.getParameter("retreating") != null) {
                    fd.setStartTime(GameUtilities.getCurrentTick2() - 1);
                } else {
                    fd.setStartTime(GameUtilities.getCurrentTick2());
                }
                fd = fdDAO.add(fd);

                // Process retreat factor
                if (!retreatSet) {
                    if (retreatPercentage < 100) {
                        at.darkdestiny.core.model.FleetOrder fo = null;

                        if (fpb.isFleetFormation()) {
                            fo = foDAO.get(fpb.getFleetId(), FleetType.FLEET_FORMATION);
                        } else {
                            fo = foDAO.get(fpb.getFleetId(), FleetType.FLEET);
                        }

                        boolean update = true;
                        if (fo == null) {
                            update = false;

                            fo = new at.darkdestiny.core.model.FleetOrder();

                            fo.setFleetId(fpb.getFleetId());

                            if (fpb.isFleetFormation()) {
                                fo.setFleetType(FleetType.FLEET_FORMATION);
                            } else {
                                fo.setFleetType(FleetType.FLEET);
                            }
                        }

                        fo.setRetreatFactor(retreatPercentage);
                        fo.setRetreatToType(retreatLocIsSystem ? ELocationType.SYSTEM : ELocationType.PLANET);
                        fo.setRetreatTo(retreatLoc);

                        if (update) {
                            foDAO.update(fo);
                        } else {
                            foDAO.add(fo);
                        }
                    }

                    retreatSet = true;
                }

                DebugBuffer.addLine(DebugLevel.UNKNOWN, "FleetDetail: " + fd);

                Action a = new Action();
                a.setType(EActionType.FLIGHT);
                a.setRefTableId(fd.getId());
                a.setNumber(0);
                a.setUserId(userId);
                a.setFleetId(fd.getFleetId());
                aDAO.add(a);

                ViewTableUtilities.addOrRefreshSystem(pf.getUserId(), pf.getSystemId(), GameUtilities.getCurrentTick2());

                pf.setLocation(ELocationType.TRANSIT);
                // pf.setStatus(1);
                // pf.setPlanetId(0);
                // pf.setSystemId(0);
                pfDAO.update(pf);

                /*
                 * TASKS/TITLE - START
                 */
                if (!HomeSystemBuffer.getForUser(userId).getId().equals(targetSystem.getId())) {
                    TitleUtilities.incrementCondition(ConditionToTitle.EXPLORER_LEAVEYOURSYSTEM, userId);
                }
                /*
                 * TASKS/TITLE - END
                 */
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("FleetMovement Transaction failed", e);
            error += e.getMessage();
            try {
                th.rollback();
            } catch (TransactionException te2) {
                DebugBuffer.writeStackTrace("Rollback failed", te2);
                error += " >> " + te2.getMessage();
            }
        } finally {
            th.endTransaction();
            if (error != null) {
                return new BaseResult("ERROR: " + error, true);
            }
        }

        /*
         * TASKS/TITLE - START
         */
        if (!HomeSystemBuffer.getForUser(userId).getId().equals(targetSystem.getId())) {
            TitleUtilities.incrementCondition(ConditionToTitle.EXPLORER_LEAVEYOURSYSTEM, userId);
        }
        /*
         * TASKS/TITLE - END
         */
        return new BaseResult(ML.getMLStr("fleetmovement_msg_allok", userId), false);
    }

    public static void retreatFleet(FlightProcessingEntry fpe, int userId) {
        try {
            // Get basic data
            FleetDetail fd = fpe.getFleetDetail();
            int fleetId = fd.getFleetId();
            at.darkdestiny.core.model.FleetOrder fo = foDAO.get(fleetId, FleetType.FLEET);

            // Write fleet to target system
            PlayerFleet pf = pfDAO.findById(fleetId);
            pf.setLocation(ELocationType.SYSTEM, fd.getDestSystem());
            // pf.setSystemId(fd.getDestSystem());
            // pf.setPlanetId(0);
            // pf.setStatus(0);
            pfDAO.update(pf);

            // Remove moving and order data
            fdDAO.remove(fd);
            aDAO.remove(fpe.getAction());

            if (fo != null) {
                foDAO.remove(fo);

                PlayerFleetExt pfe = new PlayerFleetExt(pf);

                // Create a FleetParameterBuffer object
                FleetParameterBuffer fpb = new FleetParameterBuffer(userId);
                fpb.setParameter(FleetParameterBuffer.FLEET_ID, "" + fleetId);
                fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, fo.getRetreatToType() == ELocationType.SYSTEM ? "true" : "false");
                fpb.setParameter(FleetParameterBuffer.TARGET_ID, "" + fo.getRetreatTo());
                fpb.setParameter(FleetParameterBuffer.RETREAT_FACTOR, "" + 100);
                fpb.setParameter("fleet", pfe);

                RelativeCoordinate rc = null;
                at.darkdestiny.core.model.System targetSys = null;
                Planet targetPlanet = null;

                if (fo.getRetreatToType() == ELocationType.SYSTEM) {
                    log.debug("GET RETREAT COORdINATES by System");
                    rc = new RelativeCoordinate(fo.getRetreatTo(), 0);
                    targetSys = sDAO.findById(rc.getSystemId());
                } else {
                    log.debug("GET RETREAT COORdINATES by Planet");
                    rc = new RelativeCoordinate(0, fo.getRetreatTo());
                    targetSys = sDAO.findById(rc.getSystemId());
                    targetPlanet = pDAO.findById(rc.getPlanetId());
                }

                log.debug("RETREAT TARGETSYSTEM = " + targetSys + " TARGETPLANET = " + targetPlanet);

                fpb.setParameter("targetSystem", targetSys);
                fpb.setParameter("targetPlanet", targetPlanet);
                fpb.setParameter("retreating", true);

                // Generate new Flight Entry
                generateFlight(userId, fpb);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while retreat of Fleet: ", e);
        }
    }

    public static void retreatFleet2(FlightProcessingEntry fpe, int userId) {
        try {
            // Get basic data
            FleetDetail fd = fpe.getFleetDetail();
            int fleetId = fd.getFleetId();
            at.darkdestiny.core.model.FleetOrder fo = foDAO.get(fleetId, FleetType.FLEET);

            // Write fleet to target system
            PlayerFleet pf = pfDAO.findById(fleetId);
            pf.setLocation(ELocationType.SYSTEM, fd.getDestSystem());
            // pf.setSystemId(fd.getDestSystem());
            // pf.setPlanetId(0);
            // pf.setStatus(0);
            pfDAO.update(pf);

            // Remove moving and order data
            fdDAO.remove(fd);
            aDAO.remove(fpe.getAction());

            if (fo != null) {
                foDAO.remove(fo);

                PlayerFleetExt pfe = new PlayerFleetExt(pf);

                // Create a FleetParameterBuffer object
                FleetParameterBuffer fpb = new FleetParameterBuffer(userId);
                fpb.setParameter(FleetParameterBuffer.FLEET_ID, "" + fleetId);
                fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, fo.getRetreatToType() == ELocationType.SYSTEM ? "true" : "false");
                fpb.setParameter(FleetParameterBuffer.TARGET_ID, "" + fo.getRetreatTo());
                fpb.setParameter(FleetParameterBuffer.RETREAT_FACTOR, "" + 100);
                fpb.setParameter("fleet", pfe);

                RelativeCoordinate rc = null;
                at.darkdestiny.core.model.System targetSys = null;
                Planet targetPlanet = null;

                if (fo.getRetreatToType() == ELocationType.SYSTEM) {
                    log.debug("GET RETREAT COORdINATES by System");
                    rc = new RelativeCoordinate(fo.getRetreatTo(), 0);
                    targetSys = sDAO.findById(rc.getSystemId());
                } else {
                    log.debug("GET RETREAT COORdINATES by Planet");
                    rc = new RelativeCoordinate(0, fo.getRetreatTo());
                    targetSys = sDAO.findById(rc.getSystemId());
                    targetPlanet = pDAO.findById(rc.getPlanetId());
                }

                log.debug("RETREAT TARGETSYSTEM = " + targetSys + " TARGETPLANET = " + targetPlanet);

                fpb.setParameter("targetSystem", targetSys);
                fpb.setParameter("targetPlanet", targetPlanet);
                fpb.setParameter("retreating", true);

                // Generate new Flight Entry
                generateFlight(userId, fpb);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while retreat of Fleet: ", e);
        }
    }

    public static void retreatFleetFormation(int formationId, ArrayList<FlightProcessingEntry> fpeList) {
        try {
            // Get basic data
            at.darkdestiny.core.model.FleetOrder fo = foDAO.get(formationId, FleetType.FLEET_FORMATION);
            foDAO.remove(fo);

            for (FlightProcessingEntry fpe : fpeList) {
                FleetDetail fd = fpe.getFleetDetail();
                int fleetId = fd.getFleetId();

                // Write fleet to target system
                PlayerFleet pf = pfDAO.findById(fleetId);
                pf.setLocation(ELocationType.SYSTEM, fd.getDestSystem());
                // pf.setSystemId(fd.getDestSystem());
                // pf.setPlanetId(0);
                // pf.setStatus(0);
                pfDAO.update(pf);

                // Remove moving and order data
                fdDAO.remove(fd);
                aDAO.remove(fpe.getAction());
            }

            FleetFormation ff = ffDAO.findById(formationId);
            FleetFormationExt ffe = new FleetFormationExt(formationId);
            // PlayerFleetExt pfe = new PlayerFleetExt(pf);

            // Create a FleetParameterBuffer object
            FleetParameterBuffer fpb = new FleetParameterBuffer(ff.getUserId());
            fpb.setParameter(FleetParameterBuffer.FLEET_ID, "FF" + ff.getId());
            fpb.setParameter(FleetParameterBuffer.TO_SYSTEM, fo.getRetreatToType() == ELocationType.SYSTEM ? "true" : "false");
            fpb.setParameter(FleetParameterBuffer.TARGET_ID, "" + fo.getRetreatTo());
            fpb.setParameter(FleetParameterBuffer.RETREAT_FACTOR, "" + 100);
            fpb.setParameter("fleet", ffe);

            RelativeCoordinate rc = null;
            at.darkdestiny.core.model.System targetSys = null;
            Planet targetPlanet = null;

            if (fo.getRetreatToType() == ELocationType.SYSTEM) {
                log.debug("GET RETREAT COORdINATES by System");
                rc = new RelativeCoordinate(fo.getRetreatTo(), 0);
                targetSys = sDAO.findById(rc.getSystemId());
            } else {
                log.debug("GET RETREAT COORdINATES by Planet");
                rc = new RelativeCoordinate(0, fo.getRetreatTo());
                targetSys = sDAO.findById(rc.getSystemId());
                targetPlanet = pDAO.findById(rc.getPlanetId());
            }

            log.debug("RETREAT TARGETSYSTEM = " + targetSys + " TARGETPLANET = " + targetPlanet);

            fpb.setParameter("targetSystem", targetSys);
            fpb.setParameter("targetPlanet", targetPlanet);
            fpb.setParameter("retreating", true);

            // Generate new Flight Entry
            generateFlight(ff.getUserId(), fpb);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while retreat of Fleet: ", e);
        }
    }

    public static BaseResult callBackFleet(int fleetId, int userId) {
        try {
            // Check if fleet is callBackAble

            FleetDetail fd = fdDAO.findByFleetId(fleetId);
            if (fd == null) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_fleetnotflying", userId), true);
            }
            //ResultSet rs = stmt.executeQuery("SELECT startsystem, startplanet, startTime, flightTime, speed, startX, startY, destX, destY FROM fleetdetails WHERE fleetId=" + fleetId);
            if (fd.getStartPlanet() == 0 && fd.getStartSystem() == 0) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_fleetalreadycalledback", userId), true);
            }
            if (pfDAO.findById(fleetId).getUserId() != userId) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_fleetisnotyours", userId), true);
            }

            // Fleet was started this tick immediate return possible
            if (fd.getStartTime() == GameUtilities.getCurrentTick2()) {
                PlayerFleet pf = pfDAO.findById(fleetId);
                if (fd.getStartPlanet() != 0) {
                    pf.setLocation(ELocationType.PLANET, fd.getStartPlanet());
                } else {
                    pf.setLocation(ELocationType.SYSTEM, fd.getStartSystem());
                }
                // pf.setPlanetId(fd.getStartPlanet());
                // pf.setSystemId(fd.getStartSystem());
                // pf.setStatus(0);
                pfDAO.update(pf);
                fdDAO.remove(fd);
                Action a = new Action();
                a.setType(EActionType.FLIGHT);
                a.setFleetId(pf.getId());
                a.setRefTableId(fd.getId());
                ArrayList<Action> aList = aDAO.find(a);
                aDAO.remove(aList.get(0));
            } else {
                // Calc distance which fleet has already done
                int diffX = fd.getDestX() - fd.getStartX();
                int diffY = fd.getDestY() - fd.getStartY();

                double totalDistance = Math.sqrt(Math.pow(diffX, 2d) + Math.pow(diffY, 2d));
                double travelledDistance = Math.min(100d, 100d / fd.getFlightTime() * (GameUtilities.getCurrentTick2() - fd.getStartTime()));

                DebugBuffer.addLine(DebugLevel.TRACE, "Returning fleet " + fd.getFleetId());
                DebugBuffer.addLine(DebugLevel.TRACE, "Travelled distance (%): " + travelledDistance + " Total Distance: " + totalDistance);

                // Get current fleet position
                int currX = (int) (fd.getStartX() + ((diffX / 100d) * travelledDistance));
                int currY = (int) (fd.getStartY() + ((diffY / 100d) * travelledDistance));

                DebugBuffer.addLine(DebugLevel.TRACE, "OldStartLoc: X=" + fd.getStartX() + " Y=" + fd.getStartY());
                DebugBuffer.addLine(DebugLevel.TRACE, "CurrentLoc: X=" + currX + " Y=" + currY);
                DebugBuffer.addLine(DebugLevel.TRACE, "OldTargetLoc: X=" + fd.getDestX() + " Y=" + fd.getDestY());

                // Get distance and calculate flight time
                double newDistance = Math.sqrt(Math.pow(fd.getStartX() - currX, 2d) + Math.pow(fd.getStartY() - currY, 2d));

                float speed = 0.1f;
                if (fd.getSpeed() > 0) {
                    speed = fd.getSpeed();
                }

                DebugBuffer.addLine(DebugLevel.TRACE, "Fly back distance: " + newDistance + " Fleetspeed: " + speed);

                int newTravelTime = (int) (newDistance / speed);

                DebugBuffer.addLine(DebugLevel.TRACE, "Travel time: " + newTravelTime);

                // Set target = Source
                // delete Relative Coordinates for source
                // Calculate distance and traveltime due to speed
                // Update actions entry
                fd.setDestSystem(fd.getStartSystem());
                fd.setDestPlanet(fd.getStartPlanet());
                fd.setDestX(fd.getStartX());
                fd.setDestY(fd.getStartY());
                fd.setStartSystem(0);
                fd.setStartPlanet(0);
                fd.setStartX(currX);
                fd.setStartY(currY);
                fd.setStartTime(GameUtilities.getCurrentTick2());
                fd.setFlightTime(newTravelTime);
                //     stmt.executeUpdate("UPDATE fleetdetails SET destsystem=startsystem, destplanet=startplanet, destX=startX, destY=startY WHERE fleetId=" + fleetId);
                //    stmt.executeUpdate("UPDATE fleetdetails SET startsystem=0, startplanet=0, startX=" + currX + ", startY=" + currY + ", startTime=" + GameUtilities.getCurrentTick2() + ", flightTime=" + newTravelTime + " WHERE fleetId=" + fleetId);
                fdDAO.update(fd);
                //  stmt.executeUpdate("UPDATE actions SET timeFinished=" + (GameUtilities.getCurrentTick2() + newTravelTime) + " WHERE number=" + fleetId + " AND actionType=6");
                //  stmt.close();
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while callBack of Fleet: ", e);
            return new BaseResult("Error: " + e, true);
        }

        return new BaseResult(ML.getMLStr("fleetmovement_msg_allok", userId), false);
    }

    public static BaseResult callBackFleetFormation(int fleetFormationId, int userId) {
        try {
            //Search FleetFormation
            FleetFormation ff = ffDAO.findById(fleetFormationId);
            FleetFormationExt ffe = new FleetFormationExt(fleetFormationId);
            ArrayList<PlayerFleet> pfs = pfDAO.findByFleetFormation(fleetFormationId);

            if (ff.getUserId() != userId) {
                return new BaseResult(ML.getMLStr("fleetmovement_err_notleaderoffleetformation", userId), true);
            }

            boolean calculateValues = true;
            //New Values
            int currX = 0;
            int currY = 0;
            int newTravelTime = 0;

            //Search Fleets
            for (PlayerFleet pf : pfs) {
                FleetDetail fd = fdDAO.findByFleetId(pf.getId());

                if (fd == null) {
                    return new BaseResult(ML.getMLStr("fleetmovement_err_fleetformationnotflying", userId), true);
                }
                if (fd.getStartPlanet() == 0 && fd.getStartSystem() == 0) {
                    return new BaseResult(ML.getMLStr("fleetmovement_err_fleetformationalreadycalledback", userId), true);
                }
                //No tick processed yet
                if (fd.getStartTime() == GameUtilities.getCurrentTick2()) {
                    if (fd.getStartPlanet() != 0) {
                        pf.setLocation(ELocationType.PLANET, fd.getStartPlanet());
                    } else {
                        pf.setLocation(ELocationType.SYSTEM, fd.getStartSystem());
                    }
                    // pf.setPlanetId(fd.getStartPlanet());
                    // pf.setSystemId(fd.getStartSystem());
                    // pf.setStatus(0);
                    pfDAO.update(pf);
                    fdDAO.remove(fd);

                    Action a = new Action();
                    a.setType(EActionType.FLIGHT);
                    a.setFleetId(pf.getId());
                    a.setRefTableId(fd.getId());
                    ArrayList<Action> aList = aDAO.find(a);
                    aDAO.remove(aList.get(0));
                } else {
                    //Calculate just once
                    if (calculateValues) {
                        int diffX = fd.getDestX() - fd.getStartX();
                        int diffY = fd.getDestY() - fd.getStartY();

                        double totalDistance = Math.sqrt(Math.pow(diffX, 2d) + Math.pow(diffY, 2d));
                        double travelledDistance = Math.min(100d, 100d / fd.getFlightTime() * (GameUtilities.getCurrentTick2() - fd.getStartTime()));

                        DebugBuffer.addLine(DebugLevel.TRACE, "Returning fleet " + fd.getFleetId());
                        DebugBuffer.addLine(DebugLevel.TRACE, "Travelled distance (%): " + travelledDistance + " Total Distance: " + totalDistance);

                        currX = (int) (fd.getStartX() + (diffX / 100d * travelledDistance));
                        currY = (int) (fd.getStartY() + (diffY / 100d * travelledDistance));

                        DebugBuffer.addLine(DebugLevel.TRACE, "OldStartLoc: X=" + fd.getStartX() + " Y=" + fd.getStartY());
                        DebugBuffer.addLine(DebugLevel.TRACE, "CurrentLoc: X=" + currX + " Y=" + currY);
                        DebugBuffer.addLine(DebugLevel.TRACE, "OldTargetLoc: X=" + fd.getDestX() + " Y=" + fd.getDestY());

                        double newDistance = Math.sqrt(Math.pow(fd.getStartX() - currX, 2d) + Math.pow(fd.getStartY() - currY, 2d));

                        float speed = 0.1f;
                        if (ffe.getSpeed() > 0) {
                            speed = (float) ffe.getSpeed();
                        }

                        DebugBuffer.addLine(DebugLevel.TRACE, "Fly back distance: " + newDistance + " Fleetspeed: " + speed);

                        newTravelTime = (int) (newDistance / speed);

                        DebugBuffer.addLine(DebugLevel.TRACE, "Travel time: " + newTravelTime);

                        calculateValues = false;
                    }


                    //Update to Database
                    fd.setDestSystem(fd.getStartSystem());
                    fd.setDestPlanet(fd.getStartPlanet());
                    fd.setDestX(fd.getStartX());
                    fd.setDestY(fd.getStartY());
                    fd.setStartSystem(0);
                    fd.setStartPlanet(0);
                    fd.setStartX(currX);
                    fd.setStartY(currY);
                    fd.setStartTime(GameUtilities.getCurrentTick2());
                    fd.setFlightTime(newTravelTime);
                    fdDAO.update(fd);
                }

            }
        } catch (Exception e) {
            log.debug("Error5:  " + e);
            DebugBuffer.writeStackTrace("Error while callBack of Fleet: ", e);
        }
        return new BaseResult(ML.getMLStr("fleetmovement_msg_allok", userId), false);
    }
}
