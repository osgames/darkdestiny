/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Admin
 */
@TableNameAnnotation(value = "aimessagepar")
public class AIMessageParameter extends Model<AIMessageParameter> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("messageId")
    private Integer messageId;
    @FieldMappingAnnotation("parName")
    private String parName;
    @FieldMappingAnnotation("parValue")
    private String parValue;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the messageId
     */
    public Integer getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the parName
     */
    public String getParName() {
        return parName;
    }

    /**
     * @param parName the parName to set
     */
    public void setParName(String parName) {
        this.parName = parName;
    }

    /**
     * @return the parValue
     */
    public String getParValue() {
        return parValue;
    }

    /**
     * @param parValue the parValue to set
     */
    public void setParValue(String parValue) {
        this.parValue = parValue;
    }
}
