/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.admin.module.AttributeType;
import at.darkdestiny.core.admin.module.ReferenceType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author HorstRabe
 */
@TableNameAnnotation(value = "moduleattributes")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class ModuleAttribute extends Model<ModuleAttribute> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("moduleId")
    @IndexAnnotation(indexName = "moduleId")
    private Integer moduleId;
    @FieldMappingAnnotation("chassisId")
    @IndexAnnotation(indexName = "chassisId")
    private Integer chassisId;
    @FieldMappingAnnotation("researchId")
    private Integer researchId;
    @FieldMappingAnnotation("attributeId")
    private Integer attributeId;
    @FieldMappingAnnotation("attributeType")
    private AttributeType attributeType;
    @FieldMappingAnnotation("value")
    private Double value;
    @FieldMappingAnnotation("refId")
    private Integer refId;
    @FieldMappingAnnotation("refType")
    private ReferenceType refType;
    @IndexAnnotation(indexName = "appliesTo")
    @FieldMappingAnnotation("appliesTo")
    private Integer appliesTo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getChassisId() {
        return chassisId;
    }

    public void setChassisId(Integer chassisId) {
        this.chassisId = chassisId;
    }

    public Integer getResearchId() {
        return researchId;
    }

    public void setResearchId(Integer researchId) {
        this.researchId = researchId;
    }

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(AttributeType attributeType) {
        this.attributeType = attributeType;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public ReferenceType getRefType() {
        return refType;
    }

    public void setRefType(ReferenceType refType) {
        this.refType = refType;
    }

    public Integer getAppliesTo() {
        return appliesTo;
    }

    public void setAppliesTo(Integer appliesTo) {
        this.appliesTo = appliesTo;
    }
}
