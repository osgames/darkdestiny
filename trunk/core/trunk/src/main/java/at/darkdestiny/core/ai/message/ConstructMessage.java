/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai.message;

import at.darkdestiny.core.ai.AIMessagePayload;

/**
 *
 * @author Admin
 */
public class ConstructMessage extends AIMessage {
    public ConstructMessage(int messageId, int senderId, int receiverId, int planetId, AIMessagePayload payload) {
        super(messageId,senderId,receiverId,planetId,payload);
    }            
    
    public int getConstructionId() {
        return Integer.parseInt(this.getPayload().getParameter("CONSTRUCTIONID"));
    }
    
    public int getPriority() {
        return Integer.parseInt(this.getPayload().getParameter("PRIORITY"));
    }    
}
