/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class AreaMapContainer {
    public static final String STAR_OBJECT_PREFIX = "0";
    public static final String FLEET_OBJECT_PREFIX = "1";
    
    private static HashMap<Integer,String> areaMaps = new HashMap<Integer,String>();
    private static HashMap<Integer,StarMapViewCoordinate> views = new HashMap<Integer,StarMapViewCoordinate>();
    private static HashMap<Integer,HashMap<Integer,HashMap<Integer,ArrayList<StarMapObject>>>> objects = Maps.newHashMap();
    
    private static HashMap<Integer,HashMap<String,StarMapObject>> objectList = Maps.newHashMap();    
    
    public static void clearAreaMap(int userId) {
        DebugBuffer.trace("Clear AreaMap for userId " + userId);
        areaMaps.remove(userId);
    }
    
    public static void clearStarMapObjects(int userId) {
        DebugBuffer.trace("Clear StarMapObjects for userId " + userId);
        objects.remove(userId);
    }

    public static void clearViews(int userId) {
        DebugBuffer.trace("Clear Views for userId " + userId);
        views.remove(userId);
    }    
    
    public static synchronized void addToAreaMap(int userId, String content) {
        if (!areaMaps.containsKey(userId)) {
            areaMaps.put(userId,content);
        } else {
            areaMaps.put(userId, areaMaps.get(userId) + content);
        }
    }
    
    public static synchronized StarMapObject getObjectById(int userId, String id) {
        return objectList.get(userId).get(id);
    }
    
    public static synchronized void addToObjectMap(int userId, int x, int y, StarMapObject smo) {
        // System.out.println("Add object at " + x + ":" + y);
        
        if (smo instanceof StarSystem) {
            StarSystem ss = (StarSystem)smo;

            System.out.println("Adding System "+ss.getId()+" at coordinates " + x + ":" + y + " ["+ss.getX()+":"+ss.getY()+"]");            
        }
        
        HashMap<String,StarMapObject> objectsByUser = objectList.get(userId);
        if (objectsByUser == null) {
            objectsByUser = new HashMap<String,StarMapObject>();
            // System.out.println("Created object list for user " + userId);
            objectList.put(userId,objectsByUser);
        }
        
        if (smo instanceof StarSystem) {
            // System.out.println("Add id " + (STAR_OBJECT_PREFIX + "_" + ((StarSystem)smo).getId()));
            objectsByUser.put(STAR_OBJECT_PREFIX + "_" + ((StarSystem)smo).getPayload().getId(), smo);
        } else if (smo instanceof Fleet) {
            objectsByUser.put(FLEET_OBJECT_PREFIX + "_" + ((Fleet)smo).getId(), smo);
        }
        
        HashMap<Integer,HashMap<Integer,ArrayList<StarMapObject>>> userEntry = objects.get(userId);
        if (userEntry != null) {
            HashMap<Integer,ArrayList<StarMapObject>> xEntry = userEntry.get(x);
            if (xEntry != null) {
                ArrayList<StarMapObject> objectsSM = xEntry.get(y);
                if (objectsSM == null) {
                    // System.out.println("Create StarMap Object List");
                    objectsSM = new ArrayList<StarMapObject>();
                    objectsSM.add(smo);
                    // System.out.println("Create Y Coordinate: " + y);
                    xEntry.put(y,objectsSM);
                } else {
                    // System.out.println("Add to StarMap Object List");
                    objectsSM.add(smo);
                }
            } else {
                // System.out.println("Create StarMap Object List");
                ArrayList<StarMapObject> objectsSM = new ArrayList<StarMapObject>();
                objectsSM.add(smo);
                xEntry = Maps.newHashMap();
                // System.out.println("Create Y Coordinate: " + y);
                xEntry.put(y, objectsSM);
                // System.out.println("Create X Coordinate: " + x);
                userEntry.put(x,xEntry);
            }
        } else {
            ArrayList<StarMapObject> objectsSM = new ArrayList<StarMapObject>();
            // System.out.println("Create StarMap Object List");
            objectsSM.add(smo);
            HashMap<Integer,ArrayList<StarMapObject>> xEntry = Maps.newHashMap();
            // System.out.println("Create Y Coordinate: " + y);
            xEntry.put(y, objectsSM);
            userEntry = new HashMap<Integer,HashMap<Integer,ArrayList<StarMapObject>>>();
            // System.out.println("Create X Coordinate: " + x);
            userEntry.put(x,xEntry);            
            // System.out.println("Create User Entry: " + userId);
            objects.put(userId,userEntry);
        }
    }
    
    public static synchronized ArrayList<StarMapObject> getObjectsAround(int userId, int x, int y, int radius) {
        ArrayList<StarMapObject> result = Lists.newArrayList();
        
        HashMap<Integer,HashMap<Integer,ArrayList<StarMapObject>>> userEntry = objects.get(userId);
        if (userEntry == null) return result;        
        // System.out.println("Found user Id: " + userId);
        
        for (int i=x-radius;i<=x+radius;i++) {
            HashMap<Integer,ArrayList<StarMapObject>> xEntry = userEntry.get(i);
            if (xEntry == null) continue;
            // System.out.println("Found x entry: " + i);
            
            for (int j=y-radius;j<=y+radius;j++) {                
                ArrayList<StarMapObject> objectsSM = xEntry.get(j);
                if (objectsSM == null) continue;
                // System.out.println("Found y entry: " + j);
                
                // Check distance and add object if distance smaller than radius
                for (StarMapObject smo : objectsSM) {
                    double distance = Math.sqrt(Math.pow(smo.getX() - x, 2d) + Math.pow(smo.getY() - y, 2d));
                    // System.out.println("Found object with distance: " + distance);
                    
                    if (distance <= radius) {
                        result.add(smo);
                    }
                }
            }
        }
        
        return result;
    }
    
    public static String getAreaMap(int userId) {
        return areaMaps.get(userId);
    }
    
    public static HashMap<String,StarMapObject> getObjectList(int userId) {
        return objectList.get(userId);
    }
    
    public static StarMapViewCoordinate getCurrentView(int userId) {
        return views.get(userId);
    }
    
    public static synchronized void setNewView(int userId, StarMapViewCoordinate smvc) {
        views.put(userId, smvc);
    }
    
    public static float getZoomImageAdjust(float zoomLevel) {
        if (zoomLevel <= 0.0625) {
            return zoomLevel * 0.8f;
        } else if (zoomLevel <= 0.125) {
            return zoomLevel * 1.5f;
        } else if (zoomLevel <= 0.25) {
            return zoomLevel * 0.8f;
        } else if (zoomLevel <= 0.5) {
            return zoomLevel * 0.8f;
        } else if (zoomLevel <= 1) {
            return zoomLevel * 0.8f;
        }
        
        return 1f;
    }
}
