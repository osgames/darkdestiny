/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "spyactiontype")
public class SpyActionType extends Model<SpyActionType> {
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true, autoIncrement=true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("duration")
    private Integer duration;
    @FieldMappingAnnotation("description")
    private String description;
    @FieldMappingAnnotation("xpNeed")
    private Integer xpNeed;
    @FieldMappingAnnotation("descriptionClass")
    private String descriptionClass;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the xpNeed
     */
    public Integer getXpNeed() {
        return xpNeed;
    }

    /**
     * @param xpNeed the xpNeed to set
     */
    public void setXpNeed(Integer xpNeed) {
        this.xpNeed = xpNeed;
    }

    /**
     * @return the descriptionClass
     */
    public String getDescriptionClass() {
        return descriptionClass;
    }

    /**
     * @param descriptionClass the descriptionClass to set
     */
    public void setDescriptionClass(String descriptionClass) {
        this.descriptionClass = descriptionClass;
    }
}
