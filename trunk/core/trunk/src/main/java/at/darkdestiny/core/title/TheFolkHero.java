package at.darkdestiny.core.title;

import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.utilities.TitleUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;


public class TheFolkHero extends AbstractTitle {


    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
	public boolean check(int userId) {
        for (ShipDesign sd : (ArrayList<ShipDesign>) sdDAO.findByUserId(userId)) {
            if (sd.getChassis() == Chassis.ID_DESTROYER && sd.getType() == EShipType.COLONYSHIP) {
                TitleUtilities.incrementCondition(ConditionToTitle.COLONISATOR_COLONYSHIP, userId);
                break;
            }
        }
		return true;
	}
}
