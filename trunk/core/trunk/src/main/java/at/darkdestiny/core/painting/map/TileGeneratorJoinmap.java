/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.interfaces.ITileGenerator;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;

/**
 *
 * @author Stefan
 */
public class TileGeneratorJoinmap implements ITileGenerator {
    private final static Object mutex = new Object();
    
    // Initial Test Values for Fake Map
    private static final int absoluteWidth = 10000;
    private static final int absoluteHeight = 10000;

    // Testing function for creating Buffered Images
    @Override
    public BufferedImage createBufferedImage(int userId, int zoom, int x, int y) {                
        int zoom2 = zoom + 2;
        double range = (double)absoluteWidth / (double)Math.pow(2d,zoom + 1);
        double scaling = (double)512 / range;       
        int edgeLength = (int)Math.floor((double)(100d * scaling));
        
        final Font f = new Font("SansSerif", Font.BOLD, (int)Math.ceil(edgeLength * 0.5));

        BufferedImage bi = new BufferedImage(512,512,TYPE_INT_ARGB);  

        synchronized(mutex) {
            /* Not necessary anymore as stars are drawn different now
            String path = "";


            try {
                path = this.getClass().getClassLoader().getResource("").getPath().replace("WEB-INF/classes/", "images/");
                System.out.println("PATH: " + path);
            } catch (Exception e) {
                System.out.println("ERROR GETTING PATH: " + e);
            }
            */                    

            // Draw black background
            Graphics2D g2d = (Graphics2D)bi.getGraphics();
            g2d.setColor(Color.BLACK);
            g2d.fillRect(0, 0, 512, 512);                

            // Draw raster
            // g2d.setColor(Color.WHITE);
            // g2d.drawRect(0, 0, 511, 512);

            // Calculate coordinates to display
            // int totalXRange = 512 * (int)Math.pow(2d, zoom + 1);
            // int totalYRange = 512 * (int)Math.pow(2d, zoom + 1);
            int noOfTiles = (int)Math.pow(2d, zoom + 1);


            // System.out.println("RANGE: " + range);

            /*
            int realXMin = (int)Math.floor(x * range);
            int realXMax = (int)Math.floor((x+1) * range);
            int realYMin = (int)Math.floor(y * range);
            int realYMax = (int)Math.floor((y+1) * range);
            */
            // System.out.println("Draw String " + "[" + realXMin + "/" + realYMin + "->" + realXMax + "/" + realYMax + "]");       

            // Calculate real Coordinate 
            double xMinMapping = ((double)absoluteWidth / (double)noOfTiles) * (double)(x) + ((zoom == 0) ? 1 : 0);                
            // double xMaxMapping = ((double)absoluteWidth / (double)noOfTiles) * (double)(x + 1);
            double yMinMapping = ((double)absoluteHeight / (double)noOfTiles) * (double)(y) + ((zoom == 0) ? 1 : 0);
            // double yMaxMapping = ((double)absoluteHeight / (double)noOfTiles) * (double)(y + 1);

            /*
            g2d.setColor(Color.WHITE);
            g2d.drawString("[" + xMinMapping + " : " + yMinMapping + " -> " + xMaxMapping + " : " + yMaxMapping + "]", 150, 200);   
            */
            /*
            realXMin -= 1000;
            realXMax += 1000;
            realYMin -= 1000;
            realYMax += 1000;


            */

            // Draw observatories and hyperspacescanner on tiles
            JoinMapDataSource jmds = BufferedStarmapData.getJoinMapData(userId);

            g2d.setColor(Color.WHITE);
            Color tileColor;            
            
            for (JMTile jmt : jmds.getTiles()) {   
                g2d.setFont(f);                
                
                if (jmt.getSystems() != 0 && jmt.getPopScore() > 0) {
                    int totalDensity = jmt.getIntensity();

                    int sRGBValue = 0;
                    sRGBValue += ((int) (totalDensity & 0xFF)) << 24; // alpha
                    sRGBValue += ((int) (255 & 0xFF)) << 16; // rot
                    sRGBValue += ((int) (255 & 0xFF)) << 8; // gr�n
                    sRGBValue += ((int) (0)); // blau

                    tileColor = new Color(sRGBValue, true);
                    g2d.setColor(tileColor);                                        
                    
                    g2d.fillRect((int)Math.floor((double)(jmt.getxStart() - xMinMapping) * scaling), (int)Math.floor((double)(jmt.getyStart() - yMinMapping) * scaling), edgeLength, edgeLength);
                    g2d.setColor(Color.BLACK);
                    if (jmt.getStartSysCount() > 0) {
                        drawCenteredString(""+jmt.getIdentifier(), edgeLength, edgeLength, 
                            (int)Math.floor((double)(jmt.getxStart() - xMinMapping) * scaling), (int)Math.floor((double)(jmt.getyStart() - yMinMapping) * scaling), g2d);
                    }
                } else {
                    g2d.setColor(Color.BLACK);
                    g2d.fillRect((int)Math.floor((double)(jmt.getxStart() - xMinMapping) * scaling), (int)Math.floor((double)(jmt.getyStart() - yMinMapping) * scaling), edgeLength, edgeLength);
                                        
                    g2d.setColor(Color.WHITE);
                    if (jmt.getStartSysCount() > 0) {
                        drawCenteredString(""+jmt.getIdentifier(), edgeLength, edgeLength, 
                            (int)Math.floor((double)(jmt.getxStart() - xMinMapping) * scaling), (int)Math.floor((double)(jmt.getyStart() - yMinMapping) * scaling), g2d);
                    }
                }                
            }
            
            for (JMGalaxy jmg : jmds.getGalaxies()) {
                if (!jmg.isPlayerStart()) {                    
                    // Draw red rectangle
                    g2d.setColor(Color.RED);
                    
                    int originX = jmg.getOriginX() - (int)Math.floor(jmg.getWidth() / 2d);
                    int originY = jmg.getOriginY() - (int)Math.floor(jmg.getHeight() / 2d);
                    
                    g2d.drawRect((int)Math.floor((double)(originX - xMinMapping) * scaling),
                            (int)Math.floor((double)(originY - yMinMapping) * scaling), 
                            (int)Math.floor((double)(jmg.getWidth() * scaling)), 
                            (int)Math.floor((double)(jmg.getHeight() * scaling)));
                    
                    // Line TopLeft to BottomRight
                    g2d.drawLine((int)Math.floor((double)(originX - xMinMapping) * scaling),
                            (int)Math.floor((double)(originY - yMinMapping) * scaling),
                            (int)Math.floor((double)(originX + jmg.getWidth() - xMinMapping) * scaling),
                            (int)Math.floor((double)(originY + jmg.getHeight() - yMinMapping) * scaling));
                    // Line BottomRight to TopLeft
                    g2d.drawLine((int)Math.floor((double)(originX - xMinMapping) * scaling),
                            (int)Math.floor((double)(originY + jmg.getHeight() - yMinMapping) * scaling),
                            (int)Math.floor((double)(originX + jmg.getWidth() - xMinMapping) * scaling),
                            (int)Math.floor((double)(originY - yMinMapping) * scaling));
                }
            }
        }

        return bi;
    }        
    

    public void drawCenteredString(String s, int w, int h, int xStart, int yStart, Graphics g) {
      FontMetrics fm = g.getFontMetrics();
      int x = (w - fm.stringWidth(s)) / 2;
      int y = (fm.getAscent() + (h - (fm.getAscent() + fm.getDescent())) / 2);
      g.drawString(s, xStart + x, yStart + y);
    }    
}
