/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.diplomacy.relations;

import at.darkdestiny.core.enumeration.EOwner;
import at.darkdestiny.core.enumeration.ESharingType;
import at.darkdestiny.core.result.DiplomacyResult;

/**
 *
 * @author Bullet
 */
public class Neutral implements IRelation{

    public DiplomacyResult getDiplomacyResult() {
        boolean attacks = true;
        boolean helps = false;
        boolean notification = false;
        boolean attackTradeFleets = true;
        ESharingType sharingStarMapInfo = ESharingType.NONE;
        boolean ableToUseShipyard = false;
        EAttackType battleInNeutral = EAttackType.OPTIONAL;
        EAttackType battleInOwn = EAttackType.YES;

        EOwner owner = EOwner.NEUTRAL;
        String color = "#FBFF00";
       DiplomacyResult dr = new DiplomacyResult(attacks, helps, notification, attackTradeFleets, color, sharingStarMapInfo, ableToUseShipyard , battleInNeutral, battleInOwn, owner);
         return dr;
    }
}
