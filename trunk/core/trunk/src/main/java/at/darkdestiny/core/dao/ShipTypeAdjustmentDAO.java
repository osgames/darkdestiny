/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.model.ShipTypeAdjustment;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Dreloc
 */
public class ShipTypeAdjustmentDAO extends ReadOnlyTable<ShipTypeAdjustment> implements GenericDAO<ShipTypeAdjustment> {
    public ShipTypeAdjustment findById(int id) {
        ShipTypeAdjustment staSearch = new ShipTypeAdjustment();
        staSearch.setId(id);
        return get(staSearch);
    }

    public ShipTypeAdjustment findByType(EShipType est) {
        ShipTypeAdjustment staSearch = new ShipTypeAdjustment();
        staSearch.setEnumId(est);
        ArrayList<ShipTypeAdjustment> staList = find(staSearch);

        if (staList.isEmpty()) {
            return null;
        } else {
            return staList.get(0);
        }
    }
}
