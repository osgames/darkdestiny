/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.tools.graph;

import at.darkdestiny.core.trade.special.TradeGoodsNode;
import com.google.common.collect.Lists;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Stefan
 */
public class Graph<T extends Node> {

    private List<T> nodes = Lists.newArrayList();
    private List<Edge> edges = Lists.newArrayList();

    public void addNode(T n) {
        if (!containsNode(n.getName())) {
            getNodes().add(n);
        }
    }

    BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g = image.createGraphics();
    FontMetrics fm = g.getFontMetrics();

    public void addEdge(T n1, T n2) {
        // System.out.println("Add edge to graph : " + n1.getAbsoluteCoordinate().getX() + "|" + n1.getAbsoluteCoordinate().getY() + " => " + n2.getAbsoluteCoordinate().getX() + "|" + n2.getAbsoluteCoordinate().getY());
        Node nStart = getNode(n1.getName());
        Node nEnd = getNode(n2.getName());

        if (nStart == null) {
            nStart = n1;
            getNodes().add(n1);
        }
        if (nEnd == null) {
            nEnd = n2;
            getNodes().add(n2);
        }

        Edge newEdge = new Edge(nStart, nEnd);
        nStart.addEdge(newEdge);
        nEnd.addEdge(newEdge);

        /*
         if (!containsNode(n1.getName())) {
         getNodes().add(n1);
         }
         if (!containsNode(n2.getName())) {
         getNodes().add(n2);
         }
         */
        getEdges().add(newEdge);
    }

    public void addEdge(Edge e) {
        e.getNode1().addEdge(e);
        e.getNode2().addEdge(e);
        getEdges().add(e);
    }

    public Edge getEdge(Node n1, Node n2) {
        for (Edge e : this.edges) {
            if (e.getNode1().equals(n1) && e.getNode2().equals(n2)) {
                return e;
            }
            if (e.getNode1().equals(n2) && e.getNode2().equals(n1)) {
                return e;
            }
        }
        
        return null;
    }
    
    public boolean containsNode(String name) {
        for (Node n : nodes) {
            if (n.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public T getNode(String name) {
        for (T n : nodes) {
            if (n.getName().equals(name)) {
                return n;
            }
        }

        return null;
    }

    /**
     * @param <T>
     * @return the nodes
     */    
    public List<T> getNodes() {
        return nodes;
    }

    /**
     * @return the edges
     */
    public List<Edge> getEdges() {
        return edges;
    }

    public boolean reachable(Node nStart, Node nEnd) {
        return false;
    }

    public ArrayList<Graph> getPartGraphs() {
        return new ArrayList<Graph>();
    }

    public void visualize() {
        GFrame frame = new GFrame();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.pack();

        frame.setVisible(true);
    }

    public class GFrame extends JFrame {

        public GFrame() {
            this.getContentPane().add(new GPanel());
        }
    }

    public class GPanel extends JPanel {

        public GPanel() {
            this.setBackground(Color.red);
            mxGraph graph = new mxGraph();
            graph.getModel().beginUpdate();

            Map<Node, Object> addedNodes = new HashMap<Node, Object>();

            for (Edge e : Graph.this.edges) {
                Object v1 = addedNodes.get(e.getNode1());
                if (v1 == null) {
                    v1 = addNode(graph, e.getNode1());
                    addedNodes.put(e.getNode1(), v1);
                }
                Object v2 = addedNodes.get(e.getNode2());
                if (v2 == null) {
                    v2 = addNode(graph, e.getNode2());
                    addedNodes.put(e.getNode2(), v2);
                }

                // graph.insertEdge(graph.getDefaultParent(), null, (int) e.getNode1().getAbsoluteCoordinate().distanceTo(e.getNode2().getAbsoluteCoordinate()), v1, v2);
                if (e.getValue() > 0) {
                    graph.insertEdge(graph.getDefaultParent(), null, (int) e.getValue(), v1, v2);
                }
            }

            for (Node n : Graph.this.nodes) {
                if (!addedNodes.containsKey(n)) {

                    graph.insertVertex(graph.getDefaultParent(), null, n, n.getAbsoluteCoordinate().getX(), n.getAbsoluteCoordinate().getY(), fm.stringWidth(n.toString()), 30, "fillColor=" + n.toColor());
                }
            }
            graph.getModel().endUpdate();
            mxGraphComponent graphComponent = new mxGraphComponent(graph);
            this.add(graphComponent);
        }
    }

    private Object addNode(mxGraph graph, Node n) {

        Object defaultParent = graph.getDefaultParent();
        // System.out.println("Adding node at : " + n.x + " | " + n.y);
        return graph.insertVertex(defaultParent, null, n, n.getAbsoluteCoordinate().getX(), n.getAbsoluteCoordinate().getY(), fm.stringWidth(n.toString()), 30, "fillColor=" + n.toColor());
    }

}
