/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

/**
 *
 * @author Stefan
 */
public class FlightResult extends BaseResult {
    private float distance;
    private float range;
    private int flightTime;
    
    public FlightResult(Exception e) {
        super(e);
    }
    
    public FlightResult(String message, boolean error) {
        super(message,error);
    }
    
    public FlightResult(boolean error) {
        super(error);
    }  
    
    /**
     * @return the distance
     */
    public float getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(float distance) {
        this.distance = distance;
    }

    /**
     * @return the range
     */
    public float getRange() {
        return range;
    }

    /**
     * @param range the range to set
     */
    public void setRange(float range) {
        this.range = range;
    }

    /**
     * @return the flightTime
     */
    public int getFlightTime() {
        return flightTime;
    }

    /**
     * @param flightTime the flightTime to set
     */
    public void setFlightTime(int flightTime) {
        this.flightTime = flightTime;
    }
}
