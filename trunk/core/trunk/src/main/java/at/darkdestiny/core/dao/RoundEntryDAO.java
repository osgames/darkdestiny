/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.RoundEntry;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class RoundEntryDAO  extends ReadWriteTable<RoundEntry> implements GenericDAO {

    public ArrayList<RoundEntry> findByCombatRoundId(Integer id) {
        RoundEntry re = new RoundEntry();
        re.setCombatRoundId(id);
        return find(re);
    }

}
