/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.AllianceMessage;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class AllianceMessageDAO extends ReadWriteTable<AllianceMessage> implements GenericDAO {

    public ArrayList<AllianceMessage> findByAllianceId(Integer allianceId) {

         AllianceMessage am = new AllianceMessage();
        am.setAllianceId(allianceId);
        return find(am);
    }

    public AllianceMessage findByPostTime(long postTime, int boardId, int allianceId) {

         AllianceMessage am = new AllianceMessage();
        am.setAllianceId(allianceId);
        am.setPostTime(postTime);
        am.setBoardId(boardId);

        ArrayList<AllianceMessage> result = find(am);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<AllianceMessage> findByAllyIdBoardId(Integer allianceId, int boardId) {

         AllianceMessage am = new AllianceMessage();
        am.setAllianceId(allianceId);
        am.setBoardId(boardId);
        return find(am);
    }
    public ArrayList<AllianceMessage> findByAllyIdBoardIdOrdered(Integer allianceId, int boardId) {

         AllianceMessage am = new AllianceMessage();
        am.setAllianceId(allianceId);
        am.setBoardId(boardId);
        return find(am);
    }

        public ArrayList<AllianceMessage> findByUserId(int userId) {
         AllianceMessage am = new AllianceMessage();
        am.setUserId(userId);
        return find(am);
    }
        /*
    public ArrayList<AllianceMessage> findBy(Integer allianceId, Integer masterAllianceId) {
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("allianceId", allianceId));
        return find("primary", qks);
    }*/
}
