package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.enumeration.ETitleComparator;


public class CDBComparator extends AbstractCondition {

	private ParameterEntry value;
	private ParameterEntry comparator;

	public CDBComparator(ParameterEntry value, ParameterEntry comparator){
		this.value = value;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
          	return checkVsDatabase(userId, conditionToTitleId, value.getParamValue().getValue(), value.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));
	}
}
