/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.chat;

import at.darkdestiny.core.admin.SessionTracker;
import at.darkdestiny.core.dao.ChatMessageDAO;
import at.darkdestiny.core.dao.ChatMessageReadDAO;
import at.darkdestiny.core.model.ChatMessage;
import at.darkdestiny.core.model.ChatMessageRead;
import at.darkdestiny.framework.dao.DAOFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.TreeMap;

/**
 *
 * @author Stefan
 */
public class JSChatUtilities {

    private static ChatMessageDAO cmDAO = (ChatMessageDAO) DAOFactory.get(ChatMessageDAO.class);
    private static ChatMessageReadDAO cmrDAO = (ChatMessageReadDAO) DAOFactory.get(ChatMessageReadDAO.class);

    public static ArrayList<String> getUsers() {
        ArrayList<String> output = Lists.newArrayList();
        ArrayList<JSChatUser> users = SessionTracker.getChatUsers();
        
        for (JSChatUser jscu : users) {
            if (jscu.isInactive()) {
                output.add("<FONT color=\"gray\">"+jscu.getName()+"</FONT>");
            } else {
                output.add(jscu.getName());
            }
        }
        
        return output;
    }
    
    public static ArrayList<String> getLast30Messages() {
        ArrayList<String> output = Lists.newArrayList();
        TreeMap<Long, ChatMessage> messages = Maps.newTreeMap();
        for (ChatMessage cmTmp : cmDAO.findAll()) {
            messages.put(cmTmp.getDate(), cmTmp);
        }

        int msgCnt = 30;
        
        LinkedList<ChatMessage> sorted = Lists.newLinkedList();
        
        for (ChatMessage cmTmp : messages.descendingMap().values()) {
            msgCnt--;
            if (msgCnt == 0) {
                break;
            }
            
            sorted.addFirst(cmTmp);
        }                
        
        for (ChatMessage cmTmp : sorted) {
            Date date = new Date(cmTmp.getDate());
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            String time = "[" + df.format(date) + "]";             
            String line = "<B>" + cmTmp.getUserName() + ":</B> " + cmTmp.getText();            
            
            output.add(time + " " + line);
        }
        
        return output;
    }
    
    public static void setUnreadTimeStamp(int userId) {
        ChatMessageRead cmr = new ChatMessageRead();
        cmr.setId(userId);
        
        cmr = cmrDAO.get(cmr);
        if (cmr == null) {
            cmr = new ChatMessageRead();
            cmr.setId(userId);
            cmr.setLastRead(System.currentTimeMillis());
            cmrDAO.add(cmr);
        } else {
            cmr.setLastRead(System.currentTimeMillis());
            cmrDAO.update(cmr);
        }
    }
    
    public static int getUnreadMessageCount(int userId) {
        ChatMessageRead cmr = new ChatMessageRead();
        cmr.setId(userId);
        
        cmr = cmrDAO.get(cmr);
                                
        TreeMap<Long, ChatMessage> messages = Maps.newTreeMap();
        for (ChatMessage cmTmp : cmDAO.findAll()) {
            messages.put(cmTmp.getDate(), cmTmp);
        }

        int msgCnt = 30;
        int unreadCnt = 0;
        
        LinkedList<ChatMessage> sorted = Lists.newLinkedList();
        
        for (ChatMessage cmTmp : messages.descendingMap().values()) {
            msgCnt--;
            if (msgCnt == 0) {
                break;
            }
            
            sorted.addFirst(cmTmp);
        }                
        
        for (ChatMessage cmTmp : sorted) {
            if (cmr == null) {
                unreadCnt++;
            } else if (cmr.getLastRead() < cmTmp.getDate()) {
                unreadCnt++;
            }
        }        
        
        return unreadCnt;
    }
}
