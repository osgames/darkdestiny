/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.AttributeDAO;
import at.darkdestiny.core.dao.ChassisDAO;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.dao.RessourceCostDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipTypeAdjustmentDAO;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Note;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.result.BuildableResult;
import at.darkdestiny.core.result.InConstructionEntry;
import at.darkdestiny.core.result.InConstructionResult;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.core.result.ResearchResult;
import at.darkdestiny.core.service.AdministrationService;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.service.ManagementService;
import at.darkdestiny.core.service.PlanetAdministrationService;
import at.darkdestiny.core.service.ResearchService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.ShippingUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class GetJSONManagementData extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(GetJSONManagementData.class);
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate"); //HTTP 1.1
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server

        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        StringBuffer json = new StringBuffer();
        json.append("{ ");

        try {
            int userId = Integer.parseInt((String) session.getAttribute("userId"));
            int planetId = Integer.parseInt(request.getParameter("planetId"));

            PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(planetId);
            if (pp.getUserId() != userId) {
                return;
            }

            //Construct something
            int constructionId = 0;
            if (request.getParameter("constructionId") != null) {
                constructionId = Integer.parseInt(request.getParameter("constructionId"));
            }
            int priority = -1;
            if (request.getParameter("priority") != null) {
                priority = Integer.parseInt(request.getParameter("priority"));
            }
            int amount = 0;
            if (request.getParameter("amount") != null) {
                amount = Integer.parseInt(request.getParameter("amount"));
            }
            if (constructionId > 0 && amount > 0 && ((priority >= 0) && (priority <= 10))) {
                ConstructionService.buildBuilding(userId, planetId, constructionId, amount, priority);
            }

            if (request.getParameter("compress") != null) {
                ConstructionService.compressOrders(userId, planetId);
            }
            //setTaxes

            int taxes = 0;
            if (request.getParameter("taxes") != null) {
                taxes = Integer.parseInt(request.getParameter("taxes"));
                ManagementService.changeTaxes(taxes, userId, planetId);
            }


            json.append(" \"planetId\":\"" + planetId + "\",");
            json.append(" \"userId\":\"" + userId + "\"");
            appendRessources(json, planetId, userId);
            appendRawMaterials(json, planetId, userId);
            appendManagementData(json, planetId, userId);

            appendRunningConstructions(json, planetId, userId);
            appendConstructions(json, planetId, userId, priority, constructionId);

            json.append(" }");
            out.println(json);

            long endTime = System.currentTimeMillis();
        } catch (NumberFormatException nfe) {
            // Seriously i dont care about crap entered by people ;)
            log.debug("Invalid data entered " + nfe.getMessage());
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ManagementData: ", e);
            e.printStackTrace();;
        } finally {
            out.close();
        }
    }

    private void appendRessources(StringBuffer json, int planetId, int userId) {
        ArrayList<PlanetRessource> prs = Service.planetRessourceDAO.findBy(planetId, EPlanetRessourceType.INSTORAGE);
        if (prs.size() > 0) {
            json.append(" ,");
            json.append(" \"ressources\": [ ");
            boolean skip = true;
            for (Ressource r : Service.ressourceDAO.findAllStoreableRessources()) {
                PlanetRessource pr = Service.planetRessourceDAO.findBy(planetId, r.getId(), EPlanetRessourceType.INSTORAGE);
                if (pr == null || pr.getRessId() == Ressource.FOOD || pr.getQty() == 0l) {
                    continue;
                }
                if (skip) {
                    skip = false;
                } else {
                    json.append(",");
                }

                json.append("{ \"id\":\"" + r.getId()
                        + "\", \"value\":\"" + String.valueOf(FormatUtilities.getFormattedNumber(pr.getQty(), ML.getLocale(userId)))
                        + "\", \"image\":\"" + r.getImageLocation()
                        + "\", \"name\":\"" + ML.getMLStr(r.getName(), userId)
                        + "\" " + "}" + "");

            }
            json.append("]");
        }

    }

    private void appendRunningConstructions(StringBuffer json, int planetId, int userId) {
        InConstructionResult icr = ConstructionService.findRunnigConstructions(planetId);
        TreeMap<Integer, ArrayList<InConstructionEntry>> sortedIcds = new TreeMap<Integer, ArrayList<InConstructionEntry>>();
        ArrayList<InConstructionEntry> constructions = icr.getEntries();
        if (constructions.size() > 0) {
            json.append(" ,");
            json.append(" \"runningconstructionslabel\": \" " + ML.getMLStr("administration_lbl_buildingsinconstruction", userId) + "\"");
            json.append(" ,");
            json.append(" \"runningconstructions\": [ ");
            boolean skip = true;
            for (InConstructionEntry ice : constructions) {
                ArrayList<InConstructionEntry> icdTmp = sortedIcds.get(ice.getOrder().getPriority());
                if (icdTmp == null) {
                    icdTmp = new ArrayList<InConstructionEntry>();
                }
                icdTmp.add(ice);
                sortedIcds.put(ice.getOrder().getPriority(), icdTmp);
            }

            for (Map.Entry<Integer, ArrayList<InConstructionEntry>> entry : sortedIcds.entrySet()) {
                ArrayList<InConstructionEntry> icdTmp = entry.getValue();
                for (InConstructionEntry ice : icdTmp) {
                    Construction c = (Construction) ice.getUnit();
                    String completionString = ((int) ice.getOrder().getIndPerc()) + "%";
                    String type = "";
                    if (ice.getOrder().getType().equals(EProductionOrderType.C_SCRAP)) {
                        type = "(A)";
                    }
                    String constructionEntry = ice.getAction().getNumber() + "x "
                            + ML.getMLStr(c.getName(), userId) + type + " (" + completionString + ") [" + ice.getOrder().getPriority() + "]";

                    if (skip) {
                        skip = false;
                    } else {
                        json.append(",");
                    }

                    // json.append("{ \"entry\":\"" + constructionEntry
                    //         + "\" " + "}" + "");
                    json.append("{ \"entry\":\"" + constructionEntry + "\"");
                    json.append(", \"priority\":\"" + ice.getOrder().getPriority() + "\"}");
                }
            }
            json.append("]");
        }
    }

    private void appendWarnings(StringBuffer json, int planetId, int userId, PlayerPlanet pp, ProductionResult pr, ExtPlanetCalcResult epcr, HashMap<Integer, HashMap<Boolean, Long>> tradeBalance) {
        ArrayList<Warning> wrngs = new ArrayList<Warning>();


        // Prepare food values
        long energyBalance = pr.getRessProduction(Ressource.ENERGY) - pr.getRessConsumption(Ressource.ENERGY);

        long foodProd = pr.getRessBaseProduction(at.darkdestiny.core.model.Ressource.FOOD);
        long foodCons = pr.getRessConsumption(at.darkdestiny.core.model.Ressource.FOOD);
        long foodStorage = pr.getRessStock(at.darkdestiny.core.model.Ressource.FOOD);
        long foodStorageTotal = pr.getRessMaxStock(at.darkdestiny.core.model.Ressource.FOOD);
        long foodIncoming = 0l;
        if (tradeBalance != null && tradeBalance.get(Ressource.FOOD) != null
                && tradeBalance.get(Ressource.FOOD).get(Boolean.FALSE) != null) {
            foodIncoming = tradeBalance.get(Ressource.FOOD).get(Boolean.FALSE);
        }
        long foodOutgoing = 0l;
        if (tradeBalance != null && tradeBalance.get(Ressource.FOOD) != null
                && tradeBalance.get(Ressource.FOOD).get(Boolean.TRUE) != null) {
            foodOutgoing = tradeBalance.get(Ressource.FOOD).get(Boolean.TRUE);
        }
        foodProd += foodIncoming;
        foodCons += foodOutgoing;
        boolean showFoodWarning = false;

        int coverage = 1;

        if ((foodCons - foodProd) > 0) {
            coverage = (int) Math.floor(foodStorage / (foodCons - foodProd));
        }

        if (((foodProd < foodCons)
                || ((foodProd < foodCons) && (pp.getPopulation() < epcr.getMaxPopulation())))
                && (coverage < 200)) {
            showFoodWarning = true;
        }

        boolean showWarning = false;
        boolean seriousWarning = false;
        boolean info = false;
        boolean showCivilHint = false;

        if (epcr.getEffAgriculture() < 1
                || epcr.getEffIndustry() < 1
                || epcr.getEffResearch() < 1
                || pp.getMoral() < 70
                || (energyBalance < 0)) {
            showWarning = true;
            seriousWarning = true;
        }

        if (showFoodWarning) {
            showWarning = true;
            if (!(((foodProd == foodCons) || (foodProd - foodCons != 0) && (coverage > 50)))) {
                seriousWarning = true;
            }
        }

        showCivilHint = PlanetAdministrationService.showMigrationWarning(userId, pp.getPlanetId());
        if (showCivilHint) {
            info = true;
        }

        String color = "#00FF00";
        if (seriousWarning) {
            color = "#FF0000";
        } else if (showWarning) {
            color = "#FFFF00";
        }

        if (showWarning || info) {

            if (epcr.getEffAgriculture() < 1 || epcr.getEffIndustry() < 1 || epcr.getEffResearch() < 1) {
                wrngs.add(new Warning(ML.getMLStr("warn_low_productivity", userId), "pic/icons/efficienty.jpg"));
                //  <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("warn_low_productivity", userId)%>')" onmouseout="hideTip()" src="<%= basePicPath%>pic/icons/efficienty.jpg"/>
            }
            if (pp.getMoral() < 70) {
                wrngs.add(new Warning(ML.getMLStr("warn_low_morale", userId), convertMoraleValueToImage(epcr.getMorale())));
                //  <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("warn_low_morale", userId)%>')" onmouseout="hideTip()" src="<%= imageIcon%>"/>
            }
            if (energyBalance < 0) {
                wrngs.add(new Warning(ML.getMLStr("warn_low_energy", userId), "pic/icons/energy.jpg"));
                //  <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("warn_low_energy", userId)%>')" onmouseout="hideTip()" src="<%= basePicPath%>pic/icons/energy.jpg"/>
            }
            if (showFoodWarning) {
                wrngs.add(new Warning(ML.getMLStr("warn_low_food", userId), "pic/icons/food.jpg"));
                //  <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("warn_low_food", userId)%>')" onmouseout="hideTip()" src="<%= basePicPath%>pic/icons/food.jpg"/>
            }
            if (showCivilHint) {
                wrngs.add(new Warning(ML.getMLStr("info_max_migration", userId), "pic/icons/zivmigration.png"));
                //   <IMG vspace="1" style="width:17px; height:17px" onmouseover="doTooltip(event,'<%= ML.getMLStr("info_max_migration", userId)%>')" onmouseout="hideTip()" src="<%= basePicPath%>pic/icons/zivmigration.png"/>
            }
        }
        if (wrngs.size() > 0) {
            json.append(" ,");
            json.append(" \"warningcolor\": \"" + color + "\"");
            json.append(" ,");
            json.append(" \"warnings\": [ ");
            boolean skip = true;
            for (Warning wrng : wrngs) {


                if (skip) {
                    skip = false;
                } else {
                    json.append(",");
                }

                json.append("{ \"msg\":\"" + wrng.msg
                        + "\", \"image\":\"" + wrng.img
                        + "\" " + "}" + "");

            }
            json.append("]");
        }
    }

    private void appendConstructions(StringBuffer json, int planetId, int userId, int lastPriority, int lastConstruction) {

        TreeMap<EConstructionType, ProductionBuildResult> pbrs = AdministrationService.getConstructionList(userId, planetId);

        if (pbrs.size() > 0) {
            json.append(" ,");
            json.append(" \"constructionbuttonvalue\": \"" + ML.getMLStr("administration_but_buildnow", userId) + "\"");
            json.append(" ,");
            json.append(" \"lastpriority\": \"" + lastPriority + "\"");
            json.append(" ,");
            json.append(" \"lastconstruction\": \"" + lastConstruction + "\"");
            json.append(" ,");
            json.append(" \"constructions\": [ ");
            boolean skip = true;
            for (Map.Entry<EConstructionType, ProductionBuildResult> entry : pbrs.entrySet()) {


                boolean buildableFound = false;
                ProductionBuildResult pbr = entry.getValue();
                ArrayList<Buildable> tmp = pbr.getUnits();
                for (Buildable b : tmp) {

                    Construction c = (Construction) b.getBase();
                    BuildableResult br = pbr.getBuildInfo(b);
                    //BuildableResult br = ConstructionUtilities.canBeBuilt(, userId, planetId, count)
                    if (br.isBuildable()) {
                        if (!buildableFound) {
                            buildableFound = true;
                        }
                        if (skip) {
                            skip = false;
                        } else {
                            json.append(",");
                        }
                        String constructionEntry = ML.getMLStr(c.getName(), userId);
                        constructionEntry += " (" + br.getPossibleCount() + ")";
                        json.append("{ \"entry\":\"" + constructionEntry
                                + "\", \"id\":\"" + c.getId()
                                + "\" " + "}" + "");
                    }


                    //   json.append("{ \"entry\":\"" + constructionEntry
                    //       + "\" " + "}" + "");

                }
                if (buildableFound) {
                    json.append(",");
                    json.append("{ \"entry\":\"" + "<------"
                            + "\", \"id\":\"" + "0"
                            + "\" " + "}" + "");
                }
            }

            json.append("]");
        }
    }

    private void appendRawMaterials(StringBuffer json, int planetId, int userId) {

        ArrayList<PlanetRessource> prs = Service.planetRessourceDAO.findBy(planetId, EPlanetRessourceType.PLANET);
        if (prs.size() > 0) {
            json.append(" ,");
            json.append(" \"rawmaterials\": [ ");
            boolean skip = true;
            for (PlanetRessource pr : prs) {

                Ressource r = Service.ressourceDAO.findRessourceById(pr.getRessId());
                if ((r.getId() == Ressource.PLANET_YNKELONIUM) && !ResearchService.isResearched(userId, Research.YNKELONIUM)) {
                    continue;
                }
                if ((r.getId() == Ressource.PLANET_HOWALGONIUM) && !ResearchService.isResearched(userId, Research.HOWALGONIUMSCANNER)) {
                    continue;
                }
                if (skip) {
                    skip = false;
                } else {
                    json.append(",");
                }

                json.append("{ \"id\":\"" + r.getId()
                        + "\", \"value\":\"" + String.valueOf(FormatUtilities.getFormattedNumber(pr.getQty(), ML.getLocale(userId)))
                        + "\", \"image\":\"" + r.getImageLocation()
                        + "\", \"name\":\"" + ML.getMLStr(r.getName(), userId)
                        + "\" " + "}" + "");

            }

            json.append("] ");
        } else {
            json.append(" ,");
            json.append(" \"rawmaterials\": [ ");
            json.append("] ");
        }
    }

    private void appendManagementData(StringBuffer json, int planetId, int userId) {
        PlayerPlanet pp;
        PlanetCalcResult pcr;
        ProductionResult pr;
        PlanetCalculation pc;
        ExtPlanetCalcResult epcr;
        ResearchResult rr;
        Planet p;

        HashMap<Integer, HashMap<Boolean, Long>> tradeValues = ShippingUtilities.findShippingBalanceForPlanetSorted(planetId);
        try {
            json.append(" ,");
            pp = AdministrationService.findPlayerPlanet(planetId);
            p = Service.planetDAO.findById(planetId);

            rr = ResearchService.getResearchPointsForPlanet(planetId);
            pc = new PlanetCalculation(planetId);
            pcr = pc.getPlanetCalcData();
            pr = pcr.getProductionData();
            epcr = pcr.getExtPlanetCalcData();


            long foodIncoming = 0l;
            if (tradeValues != null && tradeValues.get(Ressource.FOOD) != null
                    && tradeValues.get(Ressource.FOOD).get(Boolean.FALSE) != null) {
                foodIncoming = tradeValues.get(Ressource.FOOD).get(Boolean.FALSE);
            }
            long foodOutgoing = 0l;
            if (tradeValues != null && tradeValues.get(Ressource.FOOD) != null
                    && tradeValues.get(Ressource.FOOD).get(Boolean.TRUE) != null) {
                foodOutgoing = tradeValues.get(Ressource.FOOD).get(Boolean.TRUE);
            }

            long foodBalance = pr.getRessBaseProduction(Ressource.FOOD) - pr.getRessConsumption(Ressource.FOOD);
            long tradeBalance = foodIncoming - foodOutgoing;
            String foodBalanceColor = "WHITE";
            if (foodBalance < 0) {
                foodBalanceColor = "RED";
            } else if (foodBalance == 0) {
                foodBalanceColor = "WHITE";
            } else {
                foodBalanceColor = "#33CC33";
            }
            String tradeBalanceColor = "WHITE";
            if (tradeBalance < 0) {
                tradeBalanceColor = "RED";
            } else if (tradeBalance == 0) {
                tradeBalanceColor = "WHITE";
            } else {
                tradeBalanceColor = "#33CC33";
            }

            long energyBalance = pr.getRessProduction(Ressource.ENERGY) - pr.getRessConsumption(Ressource.ENERGY);

            String energyColor = "WHITE";
            String energySign = "+";

            if (energyBalance < 0) {
                energySign = "-";
                energyColor = "RED";
            } else if (energyBalance == 0) {
                energySign = "+/-";
            } else {
                energyColor = "#33CC33";
                energySign = "+";
            }

            String moraleColor = "WHITE";
            if (epcr.getMorale() < 70) {
                moraleColor = "RED";
            }

            Note n = AdministrationService.findNote(userId, p.getId());
            int maxLength = 30;
            int length = maxLength;
            String noteText = null;
            if (n != null) {
                length = Math.min(n.getMessage().length(), maxLength);

                noteText = n.getMessage().substring(0, length).replace("<BR>", " ");
                if (n.getMessage().length() > maxLength) {
                    noteText += "....";
                }
            }

            json.append(" \"populationvalue\":\"" + FormatUtilities.getFormatScaledNumber(pp.getPopulation(), GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId))
                    + "\", \"populationfreepopulation\":\"" + FormatUtilities.getFormattedDecimal(100d / epcr.getMaxPopulation() * pp.getPopulation(), 1)
                    + "\",  \"populationname\":\"" + ML.getMLStr("administration_pop_currentpopulation", userId)
                    + "\", \"foodvalue\":\"" + FormatUtilities.getFormatScaledNumber(foodBalance, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId))
                    + "\", \"foodtrade\":\"" + FormatUtilities.getFormatScaledNumber(tradeBalance, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId))
                    + "\", \"foodcolor\":\"" + foodBalanceColor
                    + "\", \"foodtradecolor\":\"" + tradeBalanceColor
                    + "\",  \"foodname\":\"" + ML.getMLStr("administration_lbl_food", userId)
                    + "\", \"energyvalue\":\"" + energySign + FormatUtilities.getFormatScaledNumber(energyBalance, GameConstants.VALUE_TYPE_NORMAL, ML.getLocale(userId))
                    + "\", \"energycolor\":\"" + energyColor
                    + "\",  \"energyname\":\"" + ML.getMLStr("administration_lbl_energy", userId)
                    + "\",  \"incomevalue\":\"" + FormatUtilities.getFormattedNumber(epcr.getTaxIncome(), ML.getLocale(userId))
                    + "\",  \"taxvalue\":\"" + pp.getTax()
                    + "\",  \"moralevalue\":\"" + epcr.getMorale() + epcr.getMoraleChangeStrWhite()
                    + "\",  \"moraleimage\":\"" + convertMoraleValueToImage(epcr.getMorale())
                    + "\",  \"moralecolor\":\"" + moraleColor);


            if (noteText != null && !noteText.equals("")) {

                json.append("\",  \"note\":\"" + noteText);
            }
            json.append("\" ");


            appendWarnings(json, planetId, userId, pp, pr, epcr, tradeValues);
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error in GetJSONManagementData: " + e);
            e.printStackTrace();;
        }
    }

    private String convertMoraleValueToImage(int moraleValue) {
        String imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_equal.png";
        if (moraleValue >= 100) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_verygood.png";
        } else if (moraleValue >= 75 && moraleValue < 100) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_good.png";
        } else if (moraleValue >= 50 && moraleValue < 75) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_equal.png";
        } else {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_bad.png";
        }
        return imageIcon;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";


    }// </editor-fold>
}

class Warning {

    String img;
    String msg;

    public Warning(String msg, String img) {
        this.img = img;
        this.msg = msg;
    }
}
