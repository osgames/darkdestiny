/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.setup;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.util.DebugBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.enumeration.EPlanetDensity;
import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.result.GalaxyCreationResult;
import at.darkdestiny.core.result.SystemGenerationResult;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class IrregularGalaxy extends AbstractGalaxy {

    private static final Logger log = LoggerFactory.getLogger(IrregularGalaxy.class);

    public IrregularGalaxy(int width, int height, boolean destroyDBEntries, EPlanetDensity pDensity, int sysDensity, int originX, int originY) {
        super(width, height, destroyDBEntries, pDensity, sysDensity, originX, originY);
    }

    public GalaxyCreationResult create() {
        log.debug("Create Irregular Galaxy with width/height : " + width + "/" + height);
        log.debug("Create Irregular Galaxy with originX/originY : " + originX + "/" + originY);


        try {
            ArrayList<Planet> planetList = new ArrayList<Planet>();
            ArrayList<at.darkdestiny.core.model.System> systemList = new ArrayList<at.darkdestiny.core.model.System>();
            ArrayList<at.darkdestiny.core.model.PlanetRessource> ressList = new ArrayList<at.darkdestiny.core.model.PlanetRessource>();
            int actSystemId = Service.systemDAO.findLastId();
            int startSystem = actSystemId;

            // int[][] galaxy = new int[WIDTH][HEIGHT];
            HashMap<Integer, HashSet<Integer>> genGalaxy = new HashMap<Integer, HashSet<Integer>>();
            //Zufallsdrehung
            //   int randomArm = (int) Math.round(Math.random() * 2);
            //  randomArm = randomArm + 3;

            // Create gravity centers
            int availArea = width * height;
            int gravCentersCnt = 4 + (int)Math.ceil(Math.random() + 4);
            
            int topLeftX = originX - (width / 2);
            int topLeftY = originY - (height / 2);

            ArrayList<AbsoluteCoordinate> gravCenters = new ArrayList<AbsoluteCoordinate>();
            for (int i=0;i<gravCentersCnt;i++) {
                AbsoluteCoordinate ac = new AbsoluteCoordinate((int)Math.ceil(Math.random() * width) + topLeftX,(int)Math.ceil(Math.random() * height) + topLeftY);
                gravCenters.add(ac);
            }
            
            // Please testsystem at gravity centers
            for (AbsoluteCoordinate ac : gravCenters) {
                int x = ac.getX();
                int y = ac.getY();
                
                SystemGenerationResult sgr = generateSystem(x, y, actSystemId, systemList, planetList, ressList);
                if (sgr.isSystemGenerated()) {
                    actSystemId += sgr.getSystemsGenerated();
                }                
            }
            
            // Draw stars around each gravitational center
            for (AbsoluteCoordinate ac : gravCenters) {
                int spread = 2 + (int)Math.ceil(Math.random() * 3d);
                for (int i = 0; i < 360; i++) {                
                    int distance = (int)Math.ceil(Math.random() * (width / spread));
                    
                    if (Math.random() > 0.95d) {
                        int x = (int) (ac.getX() + (distance * Math.cos(i)));
                        int y = (int) (ac.getY() + (distance * Math.sin(i)));                        
                                                
                        if (genGalaxy.containsKey(x)) {
                            if (!genGalaxy.get(x).contains(y)) {
                                // System.out.println("Generate System at " + x + "/" + y);
                                genGalaxy.get(x).add(y);
                            }
                        } else {
                            HashSet<Integer> ySet = new HashSet<Integer>();
                            ySet.add(y);
                            genGalaxy.put(x, ySet);
                        }                        
                    }
                }
            }
            
            for (Map.Entry<Integer,HashSet<Integer>> entryXallY : genGalaxy.entrySet()) {
                HashSet<Integer> coordSetY = entryXallY.getValue();
                int x = entryXallY.getKey();
                
                for (Integer y : coordSetY) {
                    SystemGenerationResult sgr = generateSystem(x, y, actSystemId, systemList, planetList, ressList);
                    if (sgr.isSystemGenerated()) {
                        actSystemId += sgr.getSystemsGenerated();
                    }                         
                }
            }            
                    
            Galaxy g = new Galaxy();
            g.setStartSystem(startSystem);
            g.setEndSystem(actSystemId);
            g.setOriginX(originX);
            g.setOriginY(originY);
            g.setHeight(height);
            g.setWidth(width);

            return new GalaxyCreationResult(g, ressList, planetList, systemList);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in planet creation", e);
        }
        log.debug("Done Creating Galaxy");
        return null;
    }
}
