/*
 * GenerateMessage.java
 *
 * Created on 11. Juni 2004, 14:05
 */
package at.darkdestiny.core;

import at.darkdestiny.core.dao.MessageDAO;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EMessageRefType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.Message;
import at.darkdestiny.core.utilities.TitleUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;

/**
 *
 * @author  Stefan
 */
public class GenerateMessage {

    private static MessageDAO mDAO = (MessageDAO) DAOFactory.get(MessageDAO.class);
    /**
     * @param aEscape the escape to set
     */
    private int sourceUserId;
    private int destinationUserId;
    private String topic;
    private String msg;
    private EMessageType type;
    private EMessageContentType contentType = EMessageContentType.DEFAULT;
    private boolean masterEntry;
    private boolean ignoreSmiley;
    private boolean escape = true;
    private EMessageRefType refType = EMessageRefType.NOTHING;
    private Integer refId = 0;

    /*
    public final static int SYS = 1;
    public final static int P2P = 2;
    public final static int ALL = 3;
     */

    public GenerateMessage() {
        ignoreSmiley = false;
        masterEntry = true;
    }

    public GenerateMessage(boolean ignoreSmiley) {
        this.ignoreSmiley = ignoreSmiley;
        masterEntry = true;
    }    
    
    public void writeMessageToUser() {
        try {
            Message m = new Message();

            if (topic.equalsIgnoreCase("")) {
                topic = ML.getMLStr("message_lbl_nosubject", destinationUserId);
            }

            if (escape) {
                topic = FormatUtilities.escapeChars(topic);
                msg = FormatUtilities.escapeChars(msg);
            }

            if (sourceUserId != 0) {
                msg = parseText(msg,ignoreSmiley);
            }

            m.setSourceUserId(sourceUserId);
            m.setTargetUserId(destinationUserId);

            topic = FormatUtilities.killJavaScript(topic);
            msg = FormatUtilities.killJavaScript(msg);

            m.setTopic(topic);
            m.setText(msg);
            m.setTimeSent(System.currentTimeMillis());
            m.setType(type);
            m.setMsgType(contentType);
            m.setOutMaster(masterEntry);
            m.setRefType(refType);
            m.setRefId(refId);
            mDAO.add(m);                                        /*
             *
             * TITLE
             *
             */

            try {
                if (sourceUserId != 0) {
                    TitleUtilities.incrementCondition(ConditionToTitle.COMMUNICATIVE, sourceUserId);
                    TitleUtilities.incrementCondition(ConditionToTitle.CHATTERBOX, sourceUserId);
                }
            } catch (Exception e) {
                DebugBuffer.error("Error in GroundCombatutilities while increment TitleCondition : " + e);
            }

            masterEntry = false;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in create Message=", e);
        }
    }

    public static String parseText(String textToParse) {
        return parseText(textToParse,false);
    }
    
    public static String parseText(String textToParse, boolean ignoreSmiley) {
        StringBuffer sb = new StringBuffer(textToParse);

        for (int i = 0; i < sb.length(); i++) {
            if ((int) sb.charAt(i) == 13) {
                sb.replace(i, i + 2, "<BR>");
            }

            if (!ignoreSmiley) {
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 58) && ((int) sb.charAt(i + 1) == 41)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_smile.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 58) && ((int) sb.charAt(i + 1) == 68)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_mrgreen.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 58) && ((int) sb.charAt(i + 1) == 80)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_razz.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 58) && ((int) sb.charAt(i + 1) == 112)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_razz.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 56) && ((int) sb.charAt(i + 1) == 58)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_cool.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 56) && ((int) sb.charAt(i + 1) == 41)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_cool.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 59) && ((int) sb.charAt(i + 1) == 41)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_wink.gif\"/>");
                    }
                }
            }
        }



        return sb.toString();
    }

    public void setSourceUserId(int sourceUserId) {
        this.sourceUserId = sourceUserId;
    }

    public void setDestinationUserId(int destinationUserId) {
        this.destinationUserId = destinationUserId;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setMessageType(EMessageType type) {
        this.type = type;
    }

    public void setMessageContentType(EMessageContentType contentType) {
        this.contentType = contentType;
    }    
    
    public void setMasterEntry(boolean masterEntry) {
        this.masterEntry = masterEntry;
    }

    public void setEscape(boolean escape) {
        this.escape = escape;
    }

    public void setReference(EMessageRefType msgRefType, Integer msgRefId) {
        this.refType = msgRefType;
        this.refId = msgRefId;
    }
}
