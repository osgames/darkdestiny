/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

 import java.util.HashMap;import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class CombatContext {

    private static final Logger log = LoggerFactory.getLogger(CombatContext.class);

    private final ISpaceCombat combatController;
    private static HashMap<String,CombatContext> instance = new HashMap<String,CombatContext>();
    private static HashMap<String,HashMap<String,Object>> registeredObjects = new HashMap<String,HashMap<String,Object>>();

    private CombatContext(ISpaceCombat isc) {
        combatController = isc;
    }

    protected static void createCombatContext(ISpaceCombat isc) {
        CombatContext actContext = new CombatContext(isc);
        instance.put(Thread.currentThread().getName(), actContext);
    }

    protected static void destroyCombatContext() {
        instance.remove(Thread.currentThread().getName());

        HashMap<String,Object> objects = registeredObjects.get(Thread.currentThread().getName());
        if (objects != null) {
            objects.clear();
        }

        registeredObjects.clear();
    }

    /**
     * @return the combatController
     */
    public static ISpaceCombat getCombatController() {
        return instance.get(Thread.currentThread().getName()).combatController;
    }

    /**
     * @return the registeredObjects
     */
    public static <T> T getRegisteredObject(String name) {
        log.debug("Try to retrieve " + name + " for thread " + Thread.currentThread().getName());

        return (T)registeredObjects.get(Thread.currentThread().getName()).get(name);
    }

    /**
     * @param aRegisteredObjects the registeredObjects to set
     */
    public static void registerObject(String name, Object o) {
        HashMap<String,Object> objects = registeredObjects.get(Thread.currentThread().getName());
        if (objects == null) {
            objects = new HashMap<String,Object>();
            registeredObjects.put(Thread.currentThread().getName(), objects);
        }

        log.debug("Try to register " + name + " for thread " + Thread.currentThread().getName() + ": " + o);
        objects.put(name,o);
    }
}
