package at.darkdestiny.core.title;

import at.darkdestiny.core.model.CreditDonation;
import at.darkdestiny.core.service.Service;


public class TheBenificiary extends AbstractTitle {

	public boolean check(int userId) {
            for(CreditDonation cd : Service.creditDonationDAO.findByToUserId(userId)){
                if(cd.getAmount() > 500000000l){
                    return true;
                }
            }
            return false;
	}
}
