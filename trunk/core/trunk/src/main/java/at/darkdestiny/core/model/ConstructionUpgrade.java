/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "constructionupgrade")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class ConstructionUpgrade extends Model<ConstructionUpgrade> {

    @FieldMappingAnnotation("id")
    @ColumnProperties(unsigned = true, autoIncrement = true)
    @IdFieldAnnotation
    private Integer id;
    @FieldMappingAnnotation("baseConstruction")
    @ColumnProperties(unsigned = true, nullable = false)
    private Integer baseConstruction;
    @FieldMappingAnnotation("targetConstruction")
    @ColumnProperties(unsigned = true, nullable = false)
    private Integer targetConstruction;
    @FieldMappingAnnotation("baseConsConsumption")
    @ColumnProperties(unsigned = true, nullable = false)
    private Integer baseConsConsumption;
    @FieldMappingAnnotation("targetConsCreation")
    @ColumnProperties(unsigned = true, nullable = false)
    private Integer targetConsCreation;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the baseConstruction
     */
    public Integer getBaseConstruction() {
        return baseConstruction;
    }

    /**
     * @param baseConstruction the baseConstruction to set
     */
    public void setBaseConstruction(Integer baseConstruction) {
        this.baseConstruction = baseConstruction;
    }

    /**
     * @return the targetConstruction
     */
    public Integer getTargetConstruction() {
        return targetConstruction;
    }

    /**
     * @param targetConstruction the targetConstruction to set
     */
    public void setTargetConstruction(Integer targetConstruction) {
        this.targetConstruction = targetConstruction;
    }

    /**
     * @return the baseConsConsumption
     */
    public Integer getBaseConsConsumption() {
        return baseConsConsumption;
    }

    /**
     * @param baseConsConsumption the baseConsConsumption to set
     */
    public void setBaseConsConsumption(Integer baseConsConsumption) {
        this.baseConsConsumption = baseConsConsumption;
    }

    /**
     * @return the targetConsCreation
     */
    public Integer getTargetConsCreation() {
        return targetConsCreation;
    }

    /**
     * @param targetConsCreation the targetConsCreation to set
     */
    public void setTargetConsCreation(Integer targetConsCreation) {
        this.targetConsCreation = targetConsCreation;
    }
}
