/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.util.DebugBuffer;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

/**
 *
 * @author Stefan
 */
public class JoinMapDataSource implements IMapDataSource {
    private final StringBuffer xmlString;
    
    private ArrayList<JMTile> tiles = new ArrayList<JMTile>();
    private ArrayList<JMGalaxy> galaxies = new ArrayList<JMGalaxy>();
    
    public JoinMapDataSource(StringBuffer xmlString) {
        this.xmlString = xmlString;       
        
        System.out.println("JoinMap: " + xmlString);
    }
    
    @Override
    public void parse() {                
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(false);        
            SAXParser saxParser = factory.newSAXParser();
            
            XMLReader xmlReader = saxParser.getXMLReader();
            xmlReader.setContentHandler(new JoinMapXMLParser(this));
            Reader reader = new StringReader(FormatUtilities.toISO2(xmlString.toString()));
            InputSource is = new InputSource(reader);
            xmlReader.parse(is);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in StarMapDataSource", e);
        }        
    }

    public void addTile(JMTile jmt) {
        tiles.add(jmt);
    }    
    
    public ArrayList<JMTile> getTiles() {
        return tiles;
    }    
    
    public void addGalaxy(JMGalaxy jmg) {
        galaxies.add(jmg);
    }    
    
    public ArrayList<JMGalaxy> getGalaxies() {
        return galaxies;
    }        
}
