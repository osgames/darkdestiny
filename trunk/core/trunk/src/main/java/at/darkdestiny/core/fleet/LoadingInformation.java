/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.fleet;

import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.dao.FleetLoadingDAO;
import at.darkdestiny.core.dao.GroundTroopDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.hangar.ShipTypeEntry;
import at.darkdestiny.core.model.FleetLoading;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.ships.ShipStorage;
import at.darkdestiny.core.utilities.HangarUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class LoadingInformation {
    private final PlayerFleet fleet;

    private int troopSpace;
    private int ressSpace;
    private ArrayList<ShipStorage> loadedStorage;
    private int currLoadedRess;
    private int currLoadedTroops;
    private int currLoadedPopulation;
    private int smallHangarSpace;
    private int mediumHangarSpace;
    private int largeHangarSpace;
    private ArrayList<ShipTypeEntry> loadedShips;

    private static ShipFleetDAO sfDAO = (ShipFleetDAO)DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO)DAOFactory.get(ShipDesignDAO.class);
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO)DAOFactory.get(FleetLoadingDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO)DAOFactory.get(RessourceDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO)DAOFactory.get(GroundTroopDAO.class);

    public LoadingInformation(PlayerFleet fleet) {
        this.fleet = fleet;
        init();
    }

    private void init() {
        // Loop through all Ship Designs and sum up
        try {
            ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(fleet.getId());
            for (ShipFleet sf : sfList) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());
                if (sd == null) {
                    DebugBuffer.warning("Could not find ship design for shipfleet id " + sf.getDesignId() + " with fleet id " + sf.getFleetId() + " [Entry id: "+sf.getId()+"]");
                    continue;
                }
                
                troopSpace += sd.getTroopSpace() * sf.getCount();
                ressSpace += sd.getRessSpace() * sf.getCount();
                smallHangarSpace += sd.getSmallHangar() * sf.getCount();
                mediumHangarSpace += sd.getMediumHangar() * sf.getCount();
                largeHangarSpace += sd.getLargeHangar() * sf.getCount();
            }

            // Calculate Loading
            ArrayList<FleetLoading> flList = flDAO.findByFleetId(fleet.getId());

            ArrayList<ShipStorage> loadingDetails = new ArrayList<ShipStorage>();
            for (FleetLoading fl : flList) {
                ShipStorage ss = new ShipStorage();
                ss.setLoadtype(fl.getLoadType());
                ss.setId(fl.getId());
                ss.setCount(fl.getCount());

                String name = "#" + ss.getLoadtype() + "-" + ss.getId();

                switch (ss.getLoadtype()) {
                    case (GameConstants.LOADING_RESS):
                        currLoadedRess += ss.getCount();
                        Ressource r = rDAO.findRessourceById(ss.getId());

                        if (r != null) {
                            name = r.getName();
                        }
                        break;
                    case (GameConstants.LOADING_TROOPS):
                        GroundTroop gt = gtDAO.findById(ss.getId());

                        if (gt != null) {
                            currLoadedTroops += ss.getCount() * gt.getSize();
                            name = gt.getName();
                        }
                        break;
                    case (GameConstants.LOADING_POPULATION):
                        currLoadedPopulation += ss.getCount();
                        name = "Arbeiter";
                        break;
                }

                ss.setName(name);
                loadingDetails.add(ss);
            }

            // Read HangarLoading
            HangarUtilities hu = new HangarUtilities();
            hu.initializeFleet(fleet.getId());
            loadedShips = hu.getNestedHangarStructure();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while preparing Loading Information ", e);
        }
    }

    public int getTroopSpace() {
        return troopSpace;
    }

    public int getRessSpace() {
        return ressSpace;
    }

    public ArrayList<ShipStorage> getLoadedStorage() {
        if (loadedStorage == null) {
            loadedStorage = new ArrayList<ShipStorage>();

            ArrayList<FleetLoading> flList = flDAO.findByFleetId(fleet.getId());
            for (FleetLoading fl : flList) {
                ShipStorage ss = new ShipStorage();
                ss.setId(fl.getId());
                ss.setLoadtype(fl.getLoadType());
                ss.setCount(fl.getCount());

                if (fl.getLoadType() == FleetLoading.LOADTYPE_RESS) {
                    Ressource r = rDAO.findById(ss.getId());
                    if (r == null) {
                        DebugBuffer.error("Unknown RESS Loading with id " + ss.getId() + " in fleet " + fleet.getName() + "("+fleet.getId()+")");
                        continue;
                    } 
                    ss.setName(r.getName());
                } else if (fl.getLoadType() == FleetLoading.LOADTYPE_TROOPS) {
                    GroundTroop gt = gtDAO.findById(ss.getId());
                    if (gt == null) {
                        DebugBuffer.error("Unknown TROOP Loading with id " + ss.getId() + " in fleet " + fleet.getName() + "("+fleet.getId()+")");
                        continue;
                    }                     
                    ss.setName(gt.getName());
                } else if (fl.getLoadType() == FleetLoading.LOADTYPE_POPULATION) {
                    ss.setName("Arbeiter");
                }

                loadedStorage.add(ss);
            }
        }

        return loadedStorage;
    }

    public int getCurrLoadedRess() {
        return currLoadedRess;
    }

    public int getCurrLoadedTroops() {
        return currLoadedTroops;
    }

    public int getCurrLoadedPopulation() {
        return currLoadedPopulation;
    }

    public int getSmallHangarSpace() {
        return smallHangarSpace;
    }

    public int getCurrLoadedSmallHangar() {
        return 0;
    }

    public int getMediumHangarSpace() {
        return mediumHangarSpace;
    }

    public int getCurrLoadedMediumHangar() {
        return 0;
    }

    public int getLargeHangarSpace() {
        return largeHangarSpace;
    }

    public int getCurrLoadedLargeHangar() {
        return 0;
    }

    public ArrayList<ShipTypeEntry> getLoadedShips() {
        return loadedShips;
    }

    public boolean hasRessourceLoading() {
        return currLoadedRess > 0;
    }

    public boolean hasTroopLoading() {
        return currLoadedTroops > 0;
    }

    public boolean hasHangarLoading() {
        boolean hasLoading = false;

        for (ShipTypeEntry ste : loadedShips) {
            if ((ste.getSmallHangarLoaded().size() > 0) ||
                    (ste.getMediumHangarLoaded().size() > 0) ||
                    (ste.getLargeHangarLoaded().size() > 0)) {
                hasLoading = true;
            }
        }

        return hasLoading;
    }
}
