/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view.header;

import java.util.HashMap;

/**
 *
 * @author Horst
 */
public class HeaderData {

    private HashMap<Integer, CategoryEntity> categorys;
    public static final Integer NO_SELECTION = null;
    public static final Integer CATEGORY_ALL = 0;

    public HeaderData() {
        categorys = new HashMap<Integer, CategoryEntity>();
    }

    public HashMap<Integer, CategoryEntity> getCategorys() {
        return categorys;
    }

    public void setCategorys(HashMap<Integer, CategoryEntity> categorys) {
        this.categorys = categorys;
    }
}
