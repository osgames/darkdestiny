/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.TradeOffer;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TradeOfferDAO extends ReadWriteTable<TradeOffer> implements GenericDAO {

    public ArrayList<TradeOffer> findByTradePostId(int tradePostId) {
        TradeOffer to = new TradeOffer();
        to.setTradePostId(tradePostId);
        return find(to);
    }
    public TradeOffer getById(int id) {
        TradeOffer to = new TradeOffer();
        to.setId(id);
        return (TradeOffer)get(to);
    }

    public TradeOffer findBy(int tradePostId, int ressourceId) {
        TradeOffer to = new TradeOffer();
        to.setTradePostId(tradePostId);
        to.setRessourceId(ressourceId);
        ArrayList<TradeOffer> result = find(to);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
