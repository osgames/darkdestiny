 /*
 * td.java
 *
 * Created on 10. Mai 2007, 13:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.core.view.html;

import java.util.ArrayList;

/**
 *
 * @author Horst
 */
public class form extends htmlObject {

    private ArrayList<htmlObject> content;

    /**
     * Creates a new instance of td
     */
    public form() {
        properties = "";
        data = "";
        content = new ArrayList<htmlObject>();
    }

    public form(String data) {
        properties = "";
        this.data = data;
        content = new ArrayList<htmlObject>();
    }

    public form(String data, String properties) {
        this.properties = properties;
        this.data = data;
        content = new ArrayList<htmlObject>();
    }

    public String toString() {
        String returnString = "";
        returnString += "\t\t\t\t<form ";
        if (!properties.equals("")) {
            returnString += properties + " ";
        }
        returnString += ">";

        for (int i = 0; i < content.size(); i++) {

            data += "\n" + content.get(i).toString();

        }
        if (!data.equals("")) {
            returnString += data;
        }


        returnString += "</form>\n";

        return returnString;
    }

    public void addDiv(htmlObject htmlObject1) {
        content.add(htmlObject1);
    }
}
