package at.darkdestiny.core.update;

import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.ProductionOrder;

public class ProdOrderBufferEntry {
    private final ProductionOrder po;
    private final Action a;
    /*
    private int id;
    private int userId;
    private int planetId;
    private int designId;
    private int count;
    private int indNeed;
    private int indProc;
    private int moduleNeed;
    private int moduleProc;
    private int dockNeed;
    private int dockProc;
    private int orbDockNeed;
    private int orbDockProc;
    private int kasNeed;
    private int kasProc;
    private int priority;
    private int type;
    private int toId;
    */

    public ProdOrderBufferEntry(ProductionOrder po, Action a) throws Exception {
        if ((po == null) || (a == null)) {
            String actionDetails = "";
            
            if (a != null) {
                actionDetails += "TYPE="+a.getType().toString()+" REFTABLE="+a.getRefTableId()+" REFENTITY="+a.getRefEntityId() + " USER="+a.getUserId();
            }
            throw new Exception("Invalid order (PO="+po+"/A="+a+" ["+actionDetails+"]");
        }

        this.po = po;
        this.a = a;
    }

    public int getId() {
        return po.getId();
    }

    public int getPlanetId() {
        return po.getPlanetId();
    }

    public int getSystemId() {
        return a.getSystemId();
    }    
    
    public int getDesignId() {
        return a.getRefEntityId();
    }

    public int getCount() {
        return a.getNumber();
    }

    public int getModuleNeed() {
        return po.getModuleNeed();
    }

    public void setModuleNeed(int moduleNeed) {
        po.setModuleNeed(moduleNeed);
    }

    public int getModuleProc() {
        return po.getModuleProc();
    }

    public void setModuleProc(int moduleProc) {
        po.setModuleProc(moduleProc);
    }

    public double getModulePerc() {
        if (po.getModuleNeed() == 0) return 0d;
        return Math.floor(100d / po.getModuleNeed() * po.getModuleProc());
    }
    
    public int getDockNeed() {
        return po.getDockNeed();
    }

    public void setDockNeed(int dockNeed) {
        po.setDockNeed(dockNeed);
    }

    public int getDockProc() {
        return po.getDockProc();
    }

    public void setDockProc(int dockProc) {
        po.setDockProc(dockProc);
    }
    
    public double getDockPerc() {
        if (po.getDockNeed() == 0) return 0d;
        return Math.floor(100d / po.getDockNeed() * po.getDockProc());
    }

    public int getOrbDockNeed() {
        return po.getOrbDockNeed();
    }

    public void setOrbDockNeed(int orbDockNeed) {
        po.setOrbDockNeed(orbDockNeed);
    }

    public int getOrbDockProc() {
        return po.getOrbDockProc();
    }

    public void setOrbDockProc(int orbDockProc) {
        po.setOrbDockProc(orbDockProc);
    }

    public double getOrbDockPerc() {
        if (po.getOrbDockNeed() == 0) return 0d;
        return Math.floor(100d / po.getOrbDockNeed() * po.getOrbDockProc());
    }    
    
    public Integer getPriority() {
        return po.getPriority();
    }

    public EProductionOrderType getType() {
        return po.getType();
    }

    public EActionType getActionType() {
        return a.getType();
    }    
    
    public int getToId() {
        return po.getToId();
    }

    public int getKasNeed() {
        return po.getKasNeed();
    }

    public void setKasNeed(int kasNeed) {
        po.setKasNeed(kasNeed);
    }

    public int getKasProc() {
        return po.getKasProc();
    }

    public void setKasProc(int kasProc) {
        po.setKasProc(kasProc);
    }
    
    public double getKasPerc() {
        if (po.getKasNeed() == 0) return 0d;
        return Math.floor(100d / po.getKasNeed() * po.getKasProc());
    }        

    public int getIndNeed() {
        return po.getIndNeed();
    }

    public void setIndNeed(int indNeed) {
        po.setIndNeed(indNeed);
    }

    public int getIndProc() {
        return po.getIndProc();
    }

    public void setIndProc(int indProc) {
        po.setIndProc(indProc);
    }
    
    public double getIndPerc() {
        if (po.getIndNeed() == 0) return 0d;
        return Math.floor(100d / po.getIndNeed() * po.getIndProc());
    }

    public int getUserId() {
        return a.getUserId();
    }

    public ProductionOrder getProductionOrder() {
        return po;
    }

    public Action getAction() {
        return a;
    }
}
