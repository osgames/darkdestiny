/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.TitleToUser;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class TitleToUserDAO extends ReadWriteTable<TitleToUser> implements GenericDAO {

    public ArrayList<TitleToUser> findByUserId(int userId) {
        TitleToUser titleToUser = new TitleToUser();
        titleToUser.setUserId(userId);
        return find(titleToUser);
    }
    public TitleToUser findBy(int userId, int titleId) {
        TitleToUser titleToUser = new TitleToUser();
        titleToUser.setUserId(userId);
        titleToUser.setTitleId(titleId);
        return (TitleToUser)get(titleToUser);
    }


}
