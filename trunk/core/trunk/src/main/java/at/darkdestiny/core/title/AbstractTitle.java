/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.title;

 import at.darkdestiny.core.dao.ConditionDAO;import at.darkdestiny.core.dao.ConditionParameterDAO;
import at.darkdestiny.core.dao.ConditionToTitleDAO;
import at.darkdestiny.core.dao.ConditionToUserDAO;
import at.darkdestiny.core.dao.ParameterToTitleDAO;
import at.darkdestiny.core.model.ConditionParameter;
import at.darkdestiny.core.model.TitleCondition;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.ConditionToUser;
import at.darkdestiny.core.model.ParameterToTitle;
import at.darkdestiny.core.model.TitleCondition;
import at.darkdestiny.core.title.condition.ITitleCondition;
import at.darkdestiny.core.title.condition.ParameterEntry;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.ConditionDAO;
import at.darkdestiny.core.dao.ConditionParameterDAO;
import at.darkdestiny.core.dao.ConditionToTitleDAO;
import at.darkdestiny.core.dao.ConditionToUserDAO;
import at.darkdestiny.core.dao.ParameterToTitleDAO;
import at.darkdestiny.core.model.TitleCondition;
import at.darkdestiny.core.model.ConditionParameter;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.ConditionToUser;
import at.darkdestiny.core.model.ParameterToTitle;
import at.darkdestiny.core.title.condition.ITitleCondition;
import at.darkdestiny.core.title.condition.ParameterEntry;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public abstract class AbstractTitle implements ITitle {

    private static final Logger log = LoggerFactory.getLogger(AbstractTitle.class);


    private static ConditionDAO cDAO = (ConditionDAO) DAOFactory.get(ConditionDAO.class);
    private static ConditionParameterDAO cpDAO = (ConditionParameterDAO) DAOFactory.get(ConditionParameterDAO.class);
    private static ParameterToTitleDAO ptDAO = (ParameterToTitleDAO) DAOFactory.get(ParameterToTitleDAO.class);
    private static ConditionToTitleDAO ctDAO = (ConditionToTitleDAO) DAOFactory.get(ConditionToTitleDAO.class);
    private static ConditionToUserDAO cuDAO = (ConditionToUserDAO) DAOFactory.get(ConditionToUserDAO.class);

    public boolean checkConditions(int userId, int titleId) {

        boolean allTrue = true;

        for (ConditionToTitle ctt : ctDAO.findByTitleId(titleId)) {

            //Already has fullfilled this condition
            ConditionToUser ctu = cuDAO.findBy(ctt.getId(), userId);

            if (ctu != null) {
                if (ctu.getValue().equals("")) {
                    continue;
                }
            }

            TitleCondition cond = cDAO.findById(ctt.getConditionId());
            try {
                Class clzz = null;
                clzz = Class.forName(cond.getClazz());
                //Get Constructor
                Constructor con = null;

                //Cast Relation
                ITitleCondition titleCondition = null;
                ArrayList<ConditionParameter> params = new ArrayList<ConditionParameter>();
                params = cpDAO.findByConditionId(cond.getId());
                TreeMap<Integer, ParameterEntry> sortedParams = new TreeMap<Integer, ParameterEntry>();

                for (ConditionParameter cp : params) {
                    ParameterToTitle ptt = ptDAO.findBy(ctt.getId(), cp.getId());
                    if (ptt == null) {
                        DebugBuffer.addLine(DebugLevel.ERROR, "F�r Vorbedingung: " + cond.getDescription() + " wurde f�r Titel " + titleId + " kein Wert zugewiesen");
                        return false;
                    }
                    ParameterEntry pe = new ParameterEntry(ptt, cp);
                    sortedParams.put(cp.getOrder(), pe);

                }
                Class[] clzzs = new Class[sortedParams.size()];
                int j = 0;
                for (Map.Entry<Integer, ParameterEntry> entry : sortedParams.entrySet()) {
                    clzzs[j] = entry.getValue().getClass();
                    j++;
                }

                con = clzz.getConstructor(clzzs);
                //Cast Relation
                titleCondition = (ITitleCondition) con.newInstance(sortedParams.values().toArray());
                boolean check = titleCondition.checkCondition(userId, ctt.getId());
                if (check) {
                    ConditionToUser ctu2 = cuDAO.findBy(ctt.getId(), userId);
                    if (ctu2 == null) {

                        ctu2 = new ConditionToUser();
                        ctu2.setConditionToTitleId(ctt.getId());
                        ctu2.setUserId(userId);
                        ctu2.setValue("");
                        cuDAO.add(ctu2);
                    } else {

                        ctu2.setValue("");
                        cuDAO.update(ctu2);
                    }
                } else {
                    allTrue = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.debug("Error in TitleCondition : " + e);
            }
        }
        return allTrue;
    }
}
