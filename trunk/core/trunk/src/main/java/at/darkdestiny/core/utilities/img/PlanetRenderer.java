/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities.img;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import javax.imageio.ImageIO;

/**
 *
 * @author Stefan
 */
public class PlanetRenderer implements IModifyImageFunction {

    private static PlanetDAO planetDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlanetConstructionDAO planetConstructionDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private Planet p;
    private int planetId;
    private static final long serialVersionUID = -8324669939427845346L;
    private int width;
    private int height;
    private final String path;
    private final int mode;
    public final static int LARGE_PIC = 1;
    public final static int SMALL_PIC = 2;

    public PlanetRenderer(int planetId, String path, int mode) {
        if (planetId != 0) {
            p = planetDAO.findById(planetId);
            this.planetId = p.getId();
        } else {
            this.planetId = 0;
        }
        this.path = path;
        this.mode = mode;
    }

    /* (non-Javadoc)
     * @see at.darkdestiny.core.img.IModifyImageFunction#run(java.awt.Graphics)
     */
    public void run(Graphics graphics) {
        String picLink = null;

        if (p == null) {
            picLink = "empty.gif";
        } else {
            if (mode == LARGE_PIC) {
                picLink = p.getLargePic();
            } else if (mode == SMALL_PIC) {
                picLink = p.getPlanetPic();
            }
        }
        Image img = null; //Toolkit.getDefaultToolkit().createImage(path + GetSystemView.getLargePic(pd.getPlanetId()));
        try {

            img = ImageIO.read(new File(path + picLink));

            graphics.drawImage(img, 0, 0, null);
            Image img2 = null; // Toolkit.getDefaultToolkit().createImage(path + "shieldG.png");
            if (planetId == 0) {
                return;
            }

            if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_A) || p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_B)) {
                return;
            }



            PlanetConstruction hue = planetConstructionDAO.findBy(p.getId(), Construction.ID_PLANETARY_HUESHIELD);
            PlanetConstruction paratron = planetConstructionDAO.findBy(p.getId(), Construction.ID_PLANETARY_PARATRONSHIELD);
            if (hue == null && paratron == null) {
                return;
            }
            if (hue != null && paratron == null) {
                if (mode == LARGE_PIC) {
                    if (p.getDiameter() < 10500) {
                        img2 = ImageIO.read(new File(path + "shield_B_S.png"));
                    } else if (p.getDiameter() > 16000) {
                        img2 = ImageIO.read(new File(path + "shield_B_L.png"));
                    } else {
                        img2 = ImageIO.read(new File(path + "shield_B_M.png"));
                    }
                } else if (mode == SMALL_PIC) {
                    if (p.getDiameter() < 10500) {
                        img2 = ImageIO.read(new File(path + "shield_S_S.png"));
                    } else if (p.getDiameter() > 16000) {
                        img2 = ImageIO.read(new File(path + "shield_S_L.png"));
                    } else {
                        img2 = ImageIO.read(new File(path + "shield_S_M.png"));
                    }
                }
            }
            if (paratron != null) {

                if (mode == LARGE_PIC) {
                    if (p.getDiameter() < 10500) {
                        img2 = ImageIO.read(new File(path + "shield_B_S_PAR.png"));
                    } else if (p.getDiameter() > 16000) {
                        img2 = ImageIO.read(new File(path + "shield_B_L_PAR.png"));
                    } else {
                        img2 = ImageIO.read(new File(path + "shield_B_M_PAR.png"));
                    }
                } else if (mode == SMALL_PIC) {
                    if (p.getDiameter() < 10500) {
                        img2 = ImageIO.read(new File(path + "shield_S_S_PAR.png"));
                    } else if (p.getDiameter() > 16000) {
                        img2 = ImageIO.read(new File(path + "shield_S_L_PAR.png"));
                    } else {
                        img2 = ImageIO.read(new File(path + "shield_S_M_PAR.png"));
                    }
                }
            }

            graphics.drawImage(img2, 0, 0, null);
        } catch (Exception e) {
            DebugBuffer.warning("Picture not found " + e + "piclink " + path + picLink);
        }
    }

    /* (non-Javadoc)
     * @see at.darkdestiny.core.img.IModifyImageFunction#setSize(int, int)
     */
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public String createMapEntry() {
        return "";
    }
}
