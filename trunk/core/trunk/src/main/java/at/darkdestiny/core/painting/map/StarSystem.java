/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.enumeration.EStarMapObjectType;

/**
 *
 * @author Stefan
 */
public class StarSystem extends StarMapObject {
    private int id;
    private String name;
    private SMSystem payload;
    
    public StarSystem(int id, int x, int y) {
        super(EStarMapObjectType.SYSTEM,x,y);
        this.id = id;
        this.name = "System #" + id;
    }    

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the payload
     */
    public SMSystem getPayload() {
        return payload;
    }

    /**
     * @param payload the payload to set
     */
    public void setPayload(SMSystem payload) {
        this.payload = payload;
    }
}
