/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.diplomacy;

import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.EDiplomacyRelationType;
import at.darkdestiny.core.model.DiplomacyRelation;
import at.darkdestiny.core.utilities.AllianceUtilities;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Dreloc
 */
public class DiplomacyRelationExt {

    private DiplomacyRelation diplomacyRelation;
    private String fromName;
    private String toName;
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);

    public DiplomacyRelationExt(DiplomacyRelation diplomacyRelation) {
        this.diplomacyRelation = diplomacyRelation;

        if (diplomacyRelation.getType().equals(EDiplomacyRelationType.USER_TO_USER)) {
            fromName = findUser(diplomacyRelation.getFromId());
            toName = findUser(diplomacyRelation.getToId());
        }else if (diplomacyRelation.getType().equals(EDiplomacyRelationType.USER_TO_ALLIANCE)) {
            fromName = findUser(diplomacyRelation.getFromId());
            toName = findAlliance(diplomacyRelation.getToId());
        }else if (diplomacyRelation.getType().equals(EDiplomacyRelationType.ALLIANCE_TO_USER)) {
            fromName = findAlliance(diplomacyRelation.getFromId());
            toName = findUser(diplomacyRelation.getToId());
        }else if (diplomacyRelation.getType().equals(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)) {
            fromName = findAlliance(diplomacyRelation.getFromId());
            toName = findAlliance(diplomacyRelation.getToId());
        }

    }

    private String findUser(int userId) {
        String name = uDAO.findById(userId).getGameName();
        if (AllianceUtilities.hasAlliance(userId)) {
            name += " " + "[" + aDAO.findById(amDAO.findByUserId(userId).getAllianceId()).getTag() + "]";
        }
        return name;
    }

    private String findAlliance(int allianceId) {
        String name = aDAO.findById(allianceId).getName();
        return name;
    }

    public String getAsToken(){
        String token = "";
        String type = "";
        if(diplomacyRelation.getType().equals(EDiplomacyRelationType.USER_TO_USER)){
            type = "uu";
        }else if(diplomacyRelation.getType().equals(EDiplomacyRelationType.ALLIANCE_TO_USER)){
            type = "au";
        }else if(diplomacyRelation.getType().equals(EDiplomacyRelationType.USER_TO_ALLIANCE)){
            type = "ua";
        }else if(diplomacyRelation.getType().equals(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)){
            type = "aa";
        }
        token += type + ";";
        token += diplomacyRelation.getFromId() + ";";
        token += diplomacyRelation.getToId();
        return token;
    }

    /**
     * @return the fromName
     */
    public String getFromName() {
        return fromName;
    }

    /**
     * @return the toName
     */
    public String getToName() {
        return toName;
    }

    /**
     * @return the diplomacyRelation
     */
    public DiplomacyRelation getDiplomacyRelation() {
        return diplomacyRelation;
    }
}
