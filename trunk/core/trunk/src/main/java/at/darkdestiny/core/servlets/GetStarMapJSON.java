/*
 * Copyright (coffee) 2016 Jean-R�my Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.StarMapInfoUtilities;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.painting.map.AreaMapContainer;
import at.darkdestiny.core.painting.map.Fleet;
import at.darkdestiny.core.painting.map.JMTile;
import at.darkdestiny.core.painting.map.SMFleet;
import at.darkdestiny.core.painting.map.SMHyperscanner;
import at.darkdestiny.core.painting.map.SMObservatory;
import at.darkdestiny.core.painting.map.SMSystem;
import at.darkdestiny.core.painting.map.StarMapDataSource;
import at.darkdestiny.core.painting.map.StarMapObject;
import at.darkdestiny.core.requestbuffer.FleetParameterBuffer;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.SendFleetResponse;
import at.darkdestiny.core.result.StartLocationReqInfo;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.servlets.jaxgzip.Compress;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Jean-R�my Buchs <jrb0001@692b8c32.de>
 */
@Path("/StarMapJSON")
public class GetStarMapJSON {

 @GET
 @Path("/systems")
 @Compress
 @Produces(MediaType.APPLICATION_JSON)
 public Collection<SMSystem> getSystems(@Context HttpServletRequest req) {
  return getDatasource(req).getStarSystems();
 }

 @GET
 @Path("/fleets")
 @Compress
 @Produces(MediaType.APPLICATION_JSON)
 public Collection<SMFleet> getFleets(@Context HttpServletRequest req) {
  return getDatasource(req).getFleets();
 }

 @GET
 @Path("/scanners")
 @Produces(MediaType.APPLICATION_JSON)
 public Collection<SMHyperscanner> getHyperscanners(@Context HttpServletRequest req) {
  return getDatasource(req).getHyperscanners();
 }

 @GET
 @Path("/observatories")
 @Produces(MediaType.APPLICATION_JSON)
 public Collection<SMObservatory> getObservatories(@Context HttpServletRequest req) {
  return getDatasource(req).getObservatories();
 }

 @GET
 @Path("/joinselection")
 @Produces(MediaType.APPLICATION_JSON)
 public StartLocationReqInfo getStartLocationInfo(@Context HttpServletRequest req) {
    return getStartLocationReqInfo(req);
 } 

 @GET
 @Path("/sendfleet")
 @Produces(MediaType.APPLICATION_JSON)
 public SendFleetResponse getSendFleetResponse(@Context HttpServletRequest req) {
    return getSendFleetReqResponse(req);
 }  

 @GET
 @Path("/exit")
 @Produces(MediaType.APPLICATION_JSON)
 public Boolean exitMap(@Context HttpServletRequest req) {
    try {
        int userId = Integer.parseInt((String) req.getSession().getAttribute("userId"));
        
        AreaMapContainer.clearAreaMap(userId);
        AreaMapContainer.clearStarMapObjects(userId);
        AreaMapContainer.clearViews(userId);
        
        DebugBuffer.trace("Areamap cleaning done for user " + userId);
        return true;
    } catch (Exception e) {
        DebugBuffer.addLine(e);
        return false;
    }
 }   
 
 private SendFleetResponse getSendFleetReqResponse(HttpServletRequest req) {
     SendFleetResponse sfr = new SendFleetResponse();
     
    int userId = Integer.parseInt((String) req.getSession().getAttribute("userId"));
    FleetParameterBuffer fpb = new FleetParameterBuffer(userId);

    if (req.getParameter("toSystem") != null) {
        fpb.setParameter("toSystem", req.getParameter("toSystem"));
        fpb.setParameter("targetId", req.getParameter("targetId"));
        fpb.setParameter(FleetParameterBuffer.LOCATION_ID, req.getParameter("targetId"));
    }

    if (req.getParameter("targetType") != null) {
        fpb.setParameter(FleetParameterBuffer.LOCATION_TYPE, req.getParameter("targetType"));                
    }

    if (req.getParameter("fleetId") != null) {
        fpb.setParameter("fleetId", req.getParameter("fleetId"));
    }

    synchronized (GameConstants.appletSync) {
        BaseResult br = FleetService.checkFlightRequest(userId, fpb);

        if (!br.isError()) {
            br = FleetService.processFlightRequest(userId, fpb);
        }

        sfr.setSuccess(!br.isError());
        if (br.isError()) {
            sfr.setErrorMessage(br.getMessage());
        } else {
            // On success update AreaMapContainer
            HashMap<String,StarMapObject> allObjects = AreaMapContainer.getObjectList(userId);
            
            String identifier = AreaMapContainer.FLEET_OBJECT_PREFIX + "_" + req.getParameter("fleetId");
            System.out.println("Looking for identifier " + identifier + " -- Object list size: " + allObjects.size());
            StarMapObject smo = allObjects.get(identifier);
            
            if (smo != null) {                
                Fleet f = (Fleet)smo;
                SMFleet smf = f.getPayload();
                System.out.println("Found fleet " + smf.getName() + " ["+smf.getId()+"]");
                
                RelativeCoordinate rc;
                int targetX = 0;
                int targetY = 0;
                
                try {
                    if (fpb.targetIsSystem()) {
                        System.out.println("Target is System looking for id " + fpb.getTarget());                        
                        smf.setTargetSystemId(fpb.getTarget());
                        rc = new RelativeCoordinate(fpb.getTarget(),0);
                    } else {
                        System.out.println("Target is Planet looking for id " + fpb.getTarget());                        
                        rc = new RelativeCoordinate(0,fpb.getTarget());
                        smf.setTargetPlanetId(rc.getPlanetId());
                        smf.setTargetSystemId(rc.getSystemId());
                    }
                    
                    targetX = rc.toAbsoluteCoordinate().getX();                    
                    targetY = rc.toAbsoluteCoordinate().getY();                                        
                } catch (Exception e) {
                    DebugBuffer.addLine(e);
                    System.out.println("Error occured: " + e.getMessage());
                }

                smf.setMoving(targetX, targetY);                       
                
                PlayerFleetExt pfe = new PlayerFleetExt(smf.getId());
                smf.setTimeToTarget(pfe.getETA());
            }
        }
    }     
     
     return sfr;
 }
 
 private StartLocationReqInfo getStartLocationReqInfo(HttpServletRequest req) {
     StartLocationReqInfo slri = new StartLocationReqInfo();

     try {
        Integer userId = null;   

        if (req.getSession().getAttribute("userId") != null) {
            userId = Integer.parseInt((String) req.getSession().getAttribute("userId"));
        }         
         
        int x = Integer.parseInt(req.getParameter("x"));
        int y = Integer.parseInt(req.getParameter("y"));
        
        // Check in AreaMapContainer for the right tile
        // If there isnt a tile found return null
        slri.setSelectedTile(null);
        
        ArrayList<StarMapObject> tiles = AreaMapContainer.getObjectsAround(userId, x, y, 110);
        boolean validTile = false;
        
        for (StarMapObject tile : tiles) {
            JMTile actTile = (JMTile)tile;
            
            if (((x >= actTile.getxStart()) && (x < (actTile.getxStart() + 100))) && 
                ((y >= actTile.getyStart()) && (y < (actTile.getyStart() + 100)))) {                
                if (actTile.getStartSysCount() > 0) {
                    validTile = true;
                }
                
                slri.setMessage("Wollen sie in Sektor " + actTile.getIdentifier() + " starten?");
                slri.setSelectedTile(actTile.getIdentifier());
                slri.setSectorIdentifier(actTile.getIdentifier());                    
                break;                
            }
        }
        
        if (validTile) {        
            slri.setReqSuccess(true);                
        } else {
            slri.setReqSuccess(false);
        }
     } catch (Exception e) {
         slri.setReqSuccess(false);
         slri.setSelectedTile(null);
         slri.setMessage("FEHLER: " + e.getMessage());
         slri.setSectorIdentifier(null);
     }
     
     return slri;
 }
 
 private StarMapDataSource getDatasource(HttpServletRequest req) {  
    Integer userId = null;   

    if (req.getSession().getAttribute("userId") != null) {
        userId = Integer.parseInt((String) req.getSession().getAttribute("userId"));
    }
   
    StringBuffer xml = StarMapInfoUtilities.getStarMapInfo(userId);
    StarMapDataSource datasource = new StarMapDataSource(xml);
    datasource.parse();

    return datasource;
   }
}