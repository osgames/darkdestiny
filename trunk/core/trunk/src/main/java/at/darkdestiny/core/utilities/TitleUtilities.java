/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

 import at.darkdestiny.core.GameUtilities;import at.darkdestiny.core.model.Campaign;
import at.darkdestiny.core.model.CampaignToUser;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.ConditionToUser;
import at.darkdestiny.core.model.Title;
import at.darkdestiny.core.model.TitleToUser;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.title.ITitle;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.model.Campaign;
import at.darkdestiny.core.model.CampaignToUser;
import at.darkdestiny.core.model.ConditionToTitle;
import at.darkdestiny.core.model.ConditionToUser;
import at.darkdestiny.core.model.Title;
import at.darkdestiny.core.model.TitleToUser;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.title.ITitle;
import java.lang.reflect.Constructor;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class TitleUtilities extends Service {

    private static final Logger log = LoggerFactory.getLogger(TitleUtilities.class);

    public static synchronized void updateTitles() {
        long now = System.currentTimeMillis();
        DebugBuffer.addLine(DebugLevel.DEBUG, "Starting updating all Titles!");
        for (Campaign c : (ArrayList<Campaign>) campaignDAO.findAll()) {
            for (User u : (ArrayList<User>) userDAO.findAllPlayers()) {
                if (!u.getUserConfigured()) {
                    continue;
                }

                if ((GameUtilities.getCurrentTick2() - u.getLastUpdate()) <= 2) {
                    //do nothing
                } else {
                    continue;
                }
                boolean hasCampaign = true;
                //User already has ALL Titles within this campaign with all conditions
                if (campaignToUserDAO.findBy(u.getId(), c.getId()) != null) {
                    continue;
                }
                for (Title t : (ArrayList<Title>) titleDAO.findByCampaignId(c.getId())) {
                    int titleId = t.getId();
                    //User has already this Title with ALL subconditions
                    if (titleToUserDAO.findBy(u.getId(), titleId) != null) {
                        continue;
                    }
                    try {
                        Class clzz = Class.forName(t.getClazz());
                        //Get Constructor
                        Constructor con = null;

                        //Cast Relation
                        ITitle titleClazz = null;
                        con = clzz.getConstructor();

                        //Cast Relation
                        titleClazz = (ITitle) con.newInstance();
                        //Check title condition
                        boolean check = titleClazz.check(u.getId());
                        //Check each subcondition
                        boolean conditions = titleClazz.checkConditions(u.getId(), titleId);
                        if (!conditions) {
                            if (t.getVisible()) {
                                hasCampaign = false;
                            }
                            continue;
                        }
                        if (!check) {
                            if (t.getVisible()) {
                                hasCampaign = false;
                            }
                            continue;
                        } else {
                            //Removing conditions not needed any more
                            for (ConditionToTitle ctt : conditionToTitleDAO.findByTitleId(titleId)) {
                                ConditionToUser ctu = conditionToUserDAO.findBy(ctt.getId(), u.getId());
                                conditionToUserDAO.remove(ctu);
                            }
                            TitleToUser tto = new TitleToUser();
                            tto.setUserId(u.getId());
                            tto.setTitleId(titleId);

                            titleToUserDAO.add(tto);
                        }


                    } catch (Exception e) {
                        log.debug("Error in Titleutilities : " + e);
                        e.printStackTrace();
                    }
                }
                if (hasCampaign) {
                    CampaignToUser ctu = new CampaignToUser();
                    ctu.setUserId(u.getId());
                    ctu.setCampaignId(c.getId());
                    campaignToUserDAO.add(ctu);
                }
            }
        }
        DebugBuffer.addLine(DebugLevel.DEBUG, "Done updating all Titles in : " + (System.currentTimeMillis() - now) + "ms");


    }

    public static synchronized void incrementCondition(int conditionToTitleId, int userId, int number) {
        ConditionToTitle ctt = conditionToTitleDAO.findById(conditionToTitleId);
        if (titleToUserDAO.findBy(userId, ctt.getTitleId()) != null) {
            return;
        }
        ConditionToUser ctu = conditionToUserDAO.findBy(conditionToTitleId, userId);
        if (ctu == null) {
            ctu = new ConditionToUser();
            ctu.setConditionToTitleId(conditionToTitleId);
            ctu.setUserId(userId);
            ctu.setValue(String.valueOf(number));
            conditionToUserDAO.add(ctu);
        } else {
            if (ctu.getValue().equals("")) {
            } else {
                ctu.setValue(String.valueOf(Integer.parseInt(ctu.getValue()) + number));
            }
            conditionToUserDAO.update(ctu);
        }

    }

    public static void incrementCondition(int conditionToTitleId, int userId) {
        incrementCondition(conditionToTitleId, userId, 1);

    }
}
