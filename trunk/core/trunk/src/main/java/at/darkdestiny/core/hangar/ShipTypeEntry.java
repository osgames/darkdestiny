/*
 * ShipTypeEntry.java
 *
 * Created on 13. November 2006, 19:00
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.core.hangar;

import at.darkdestiny.core.admin.module.ModuleAttributeResult;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import java.util.*;

/**
 *
 * @author Stefan
 */
public class ShipTypeEntry {

    private int id;
    private int count;
    private int sizeType;
    private int spaceCons;
    private float hyperSpeed;
    private List<Hangar> smallHangar;
    private List<Hangar> mediumHangar;
    private List<Hangar> largeHangar;
    private int nestedLevel;
    private int hangarEntryId;
    //shiptypes order by shipsize!
    private static int[] shipTypesSmall = {ChassisSize.CORVETTE_ID, ChassisSize.FIGHTER_ID};
    private static int[] shipTypesMedium = {ChassisSize.DESTROYER_ID, ChassisSize.FRIGATE_ID};
    private static int[] shipTypesLarge = {ChassisSize.BATTLESHIP_ID, ChassisSize.CRUISER_ID};

    public static ShipTypeEntry initializeShipTypeEntry(ModuleAttributeResult marChassis, Chassis c, ShipDesign sd, ShipFleet sf) {
        // Statement stmt2 = DbConnect.createStatement();
        // ResultSet rs2 = stmt2.executeQuery("SELECT baseStructure FROM module WHERE id="+rs.getInt(3));
        ShipTypeEntry ste = new ShipTypeEntry();
        ste.setSpaceCons((int) marChassis.getAttributeValue(EAttribute.HITPOINTS) * c.getMinBuildQty());
        ste.setId(sd.getId());
        ste.setCount(sf.getCount());
        ste.setSizeType(sd.getChassis());
        ste.setHyperSpeed(sd.getHyperSpeed());
        ste.initializeHangars(sd.getSmallHangar(), sd.getMediumHangar(), sd.getLargeHangar());
        return ste;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSizeType() {
        return sizeType;
    }

    public void setSizeType(int sizeType) {
        this.sizeType = sizeType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Hangar> getSmallHangar() {
        return smallHangar;
    }

    public List<Hangar> getMediumHangar() {
        return mediumHangar;
    }

    public List<Hangar> getLargeHangar() {
        return largeHangar;
    }

    public float getHyperSpeed() {
        return hyperSpeed;
    }

    public void setHyperSpeed(float hyperSpeed) {
        this.hyperSpeed = hyperSpeed;
    }

    public List<ShipTypeEntry> getLargeHangarLoaded() {
        return getHangarLoad(largeHangar);
    }

    public List<ShipTypeEntry> getMediumHangarLoaded() {
        return getHangarLoad(mediumHangar);
    }

    public List<ShipTypeEntry> getSmallHangarLoaded() {
        return getHangarLoad(smallHangar);
    }

    private List<ShipTypeEntry> getHangarLoad(List<Hangar> hangarList) {
        //List<ShipTypeEntry> loadedShips = new ArrayList<ShipTypeEntry>();
        Map<Integer, ShipTypeEntry> loadedShips = new HashMap<Integer, ShipTypeEntry>();
        for (Hangar hangar : hangarList) {
            final List<ShipTypeEntry> loaded = hangar.getLoaded();
            for (ShipTypeEntry ste : loaded) {
                final int currentDesignId = ste.getId();
                if (loadedShips.containsKey(currentDesignId)) {
                    ShipTypeEntry existingShipTypeEntry = loadedShips.get(currentDesignId);
                    ShipTypeEntry mergedSTE = existingShipTypeEntry.mergeToNew(ste);
                    loadedShips.put(currentDesignId, mergedSTE);
                } else {
                    loadedShips.put(currentDesignId, ste);
                }
            }
        }
        return new ArrayList(loadedShips.values());
    }

    public int getSpaceCons() {
        return spaceCons;
    }

    public void setSpaceCons(int spaceCons) {
        this.spaceCons = spaceCons;
    }

    public int getNestedLevel() {
        return nestedLevel;
    }

    public void setNestedLevel(int nestedLevel) {
        this.nestedLevel = nestedLevel;
    }

    public int getHangarEntryId() {
        return hangarEntryId;
    }

    public void setHangarEntryId(int hangarEntryId) {
        this.hangarEntryId = hangarEntryId;
    }

    public void initializeHangars(Integer smallHangar, Integer mediumHangar, Integer largeHangar) {
        this.smallHangar = initializeHangar(this.getCount(), smallHangar);
        this.mediumHangar = initializeHangar(this.getCount(), mediumHangar);
        this.largeHangar = initializeHangar(this.getCount(), largeHangar);
    }

    private int getHangarSize(List<Hangar> hangar) {
        if (hangar.size() > 0) {
            return hangar.get(0).getSize();
        } else {
            return 0;
        }
    }

    private int getLargeHangarSize() {
        return getHangarSize(this.getLargeHangar());
    }

    private int getMediumHangarSize() {
        return getHangarSize(this.getMediumHangar());
    }

    private int getSmallHangarSize() {
        return getHangarSize(this.getSmallHangar());
    }

    public boolean shipTypeDoesNotContainHangar() {
        return (getSmallHangarSize() == 0) && (getMediumHangarSize() == 0) && (getLargeHangarSize() == 0);
    }

    public void fillHangar(TreeMap<Float, TreeMap<Integer, ArrayList<ShipTypeEntry>>> availShips) {
        if (shipTypeDoesNotContainHangar()) {
            return;
        }

        if (getLargeHangarSize() != 0) {
            fillSpecificHangarRecursive(getLargeHangar(), availShips, shipTypesLarge);
        }

        if (getMediumHangarSize() != 0) {
            fillSpecificHangarRecursive(getMediumHangar(), availShips, shipTypesMedium);
        }

        if (getSmallHangarSize() != 0) {
            fillSpecificHangarRecursive(getSmallHangar(), availShips, shipTypesSmall);
        }
    }

    private ArrayList<Hangar> initializeHangar(int count, int size) {
        ArrayList<Hangar> hangarList = new ArrayList<Hangar>();
        if (size > 0) {
            for (int i = 0; i < count; i++) {
                hangarList.add(new Hangar(size));
            }
        }
        return hangarList;
    }

    public ShipTypeEntry copyAndReduceSTE(int splitCount) {
        ShipTypeEntry newSTE = copySTE(splitCount);
        this.reduceSTE(splitCount);
        return newSTE;
    }

    private ShipTypeEntry copySTE(int splitCount) {
        ShipTypeEntry ste2 = new ShipTypeEntry();

        ste2.setSpaceCons(this.getSpaceCons());
        ste2.setId(this.getId());
        ste2.setHyperSpeed(this.getHyperSpeed());
        ste2.setSizeType(this.getSizeType());
        ste2.setCount(splitCount);
        ste2.initializeHangars(getSmallHangarSize(), getMediumHangarSize(), getLargeHangarSize());

        return ste2;
    }

    public void reduceSTE(int splitCount) {
        this.setCount(this.getCount() - splitCount);
        smallHangar = adjustHangarToSize(smallHangar);
        mediumHangar = adjustHangarToSize(mediumHangar);
        largeHangar = adjustHangarToSize(largeHangar);
    }

    private ShipTypeEntry mergeToNew(ShipTypeEntry otherSTE) {
        ShipTypeEntry newEntry = new ShipTypeEntry();
        newEntry.setSpaceCons(getSpaceCons());
        newEntry.setId(this.getId());
        newEntry.setSizeType(this.getSizeType());
        newEntry.setHyperSpeed(this.getHyperSpeed());
        newEntry.setCount(getCount() + otherSTE.getCount());
       
        newEntry.setSmallHangar(combineHangars(this.getSmallHangar(), otherSTE.getSmallHangar()));
        newEntry.setMediumHangar(combineHangars(this.getMediumHangar(), otherSTE.getMediumHangar()));
        newEntry.setLargeHangar(combineHangars(this.getLargeHangar(), otherSTE.getLargeHangar()));
        
        return newEntry;
    }
    
    private static List<Hangar> combineHangars(List<Hangar> hangars1, List<Hangar> hangars2 ) {
         List<Hangar> newSmallHangar = new ArrayList<Hangar>();
        newSmallHangar.addAll(hangars1);
        newSmallHangar.addAll(hangars2);
        return newSmallHangar;
    }

    private void setSmallHangar(List<Hangar> hangar) {
        this.smallHangar = hangar;
    }

    private void setMediumHangar(List<Hangar> hangar) {
        this.mediumHangar = hangar;
    }

    private void setLargeHangar(List<Hangar> hangar) {
        this.largeHangar = hangar;
    }

    private List<Hangar> adjustHangarToSize(List<Hangar> hangar) {
        ArrayList<Hangar> newHangar;
        if (hangar.size() > 0) {
            newHangar = new ArrayList<Hangar>(hangar.subList(0, getCount()));
        } else {
            newHangar = new ArrayList<Hangar>();
        }
        return newHangar;
    }

    public int getHangarSizeCumulated() {
        return getHangarSizeCumulated(smallHangar) + getHangarSizeCumulated(mediumHangar) + getHangarSizeCumulated(largeHangar);
    }

    private int getHangarSizeCumulated(List<Hangar> hangars) {
        int availableSize = 0;
        for (Hangar hangar : hangars) {
            availableSize += hangar.getAvailableSize();
        }
        return availableSize;
    }

    private void fillSpecificHangarRecursive(List<Hangar> hangars, TreeMap<Float, TreeMap<Integer, ArrayList<ShipTypeEntry>>> availShips, int[] shipTypesLarge) {
        for (Hangar hangar : hangars) {
            hangar.fillSpecificHangarRecursive(availShips, shipTypesLarge, this.getHyperSpeed());
        }
    }


}
