/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.construction.ConstructionImproveCost;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.service.Service;

/**
 *
 * @author Orlov
 */
public class ConstructionImproveResult extends BaseResult{

    private ConstructionImproveCost constructionImproveCost;
    private ConstructionExt consExt;
    private int level;

    public ConstructionImproveResult(int constructionId, int planetId){
        super(false);
        consExt = new ConstructionExt(constructionId);
        PlanetConstruction pc = Service.planetConstructionDAO.findBy(planetId, constructionId);
        if(pc == null){
            throw new IllegalStateException("Wanna Improve non existing Building");
        }else{
            level = pc.getLevel();
        }
        constructionImproveCost = new ConstructionImproveCost(consExt,planetId);
    }
    public int getLevel(){
        return level;
    }

    /**
     * @return the constructionImproveCost
     */
    public ConstructionImproveCost getConstructionImproveCost() {
        return constructionImproveCost;
    }

    /**
     * @return the consExt
     */
    public ConstructionExt getConsExt() {
        return consExt;
    }
}
