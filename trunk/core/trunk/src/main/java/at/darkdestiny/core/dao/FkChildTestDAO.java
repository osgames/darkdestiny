/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.FkChildTest;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Aion
 */
public class FkChildTestDAO extends ReadWriteTable<FkChildTest> implements GenericDAO {
    public FkChildTest findById(int filterId){
        FkChildTest f = new FkChildTest();
        f.setId(filterId);
        return (FkChildTest)get(f);
    }
}
