package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.PlayerResearchDAO;
import at.darkdestiny.core.dao.ResearchDAO;
import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.framework.dao.DAOFactory;


public class CResearchPointCount extends AbstractCondition {

	private ParameterEntry researchPointValue;
	private ParameterEntry comparator;
 private static PlayerResearchDAO prDAO = (PlayerResearchDAO) DAOFactory.get(PlayerResearchDAO.class);
private static ResearchDAO rDAO = (ResearchDAO) DAOFactory.get(ResearchDAO.class);


	public CResearchPointCount(ParameterEntry researchPointValue, ParameterEntry comparator){
		this.researchPointValue = researchPointValue;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
               int researchPoints = 0;
               for(PlayerResearch pr : prDAO.findByUserId(userId)){
                   Research r = rDAO.findById(pr.getResearchId());
                   if(r == null){
                       continue;
                   }
                   researchPoints += r.getRp();
               }

               return compare(researchPointValue.getParamValue().getValue(), researchPoints, researchPointValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

	}
}
