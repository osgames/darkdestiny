/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.setup;

import at.darkdestiny.core.dao.GameDataDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.EGameDataStatus;
import at.darkdestiny.core.model.GameData;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.dao.DAOFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ConfigurationResult {

    private final boolean dbConnectionOk;
    private final boolean serverRunning;
    private final EGameDataStatus serverState;
    private final boolean systemUserOk;
    private final boolean adminUserOk;
    private final boolean galaxyExisting;
    private final boolean tableExisting;
    private boolean constantsExisting = true;
    ;
    private final GameDataDAO gdDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
    private final UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private ArrayList<BaseResult> constantsResult = new ArrayList<BaseResult>();

    public ConfigurationResult(boolean dbConnection) {
        this.dbConnectionOk = dbConnection;

        boolean err = false;
        try {
            Connection c = DbConnect.getConnection();
            Statement stmt = c.createStatement();
            stmt.execute("Select * FROM user");
            stmt.close();
        } catch (SQLException e) {
            System.out.println("in Here");
            e.printStackTrace();
            err = true;
        }

        if (err) {
            tableExisting = false;
        } else {
            tableExisting = true;
        }


        if (dbConnectionOk && tableExisting) {

            //Check if some Constants are existing
            if (Service.languageDAO.findAll().isEmpty()) {
                constantsExisting = false;
                constantsResult.add(new BaseResult("No Language Entries found", true));
            }

            //Check if some Constants are existing
            if (Service.researchDAO.findAll().isEmpty()) {
                constantsExisting = false;
                constantsResult.add(new BaseResult("No Research Entries found", true));
            }

            //Check if some Constants are existing
            if (Service.constructionDAO.findAll().isEmpty()) {
                constantsExisting = false;
                constantsResult.add(new BaseResult("No Construction Entries found", true));
            }

            ArrayList<GameData> gdList = gdDAO.findAll();
            GameData gd = null;
            if (gdList.size() > 0) {
                gd = gdList.get(0);
            }

            if (gd != null) {
                if (gd.getStatus().equals(EGameDataStatus.RUNNING)) {
                    serverRunning = true;
                } else {
                    serverRunning = false;
                }

                serverState = gd.getStatus();
            } else {
                serverRunning = false;
                serverState = EGameDataStatus.STOPPED;
            }

            User sysUser = uDAO.getSystemUser();
            if (sysUser == null) {
                systemUserOk = false;
            } else {
                systemUserOk = true;
            }

            boolean foundAdmin = false;
            ArrayList<User> uList = uDAO.findAll();
            for (User u : uList) {
                if (u.getAdmin()) {
                    foundAdmin = true;
                }
            }
            if (!Service.galaxyDAO.findAll().isEmpty()) {
                galaxyExisting = true;
            } else {
                galaxyExisting = false;
            }

            adminUserOk = foundAdmin;
        } else {
            serverRunning = false;
            serverState = EGameDataStatus.STOPPED;
            systemUserOk = false;
            adminUserOk = false;
            galaxyExisting = false;
        }
    }

    /**
     * @return the dbConnectionOk
     */
    public boolean isDbConnectionOk() {
        return dbConnectionOk;
    }

    /**
     * @return the serverRunning
     */
    public boolean isServerRunning() {
        return serverRunning;
    }

    /**
     * @return the serverState
     */
    public EGameDataStatus getServerState() {
        return serverState;
    }

    /**
     * @return the systemUserOk
     */
    public boolean isSystemUserOk() {
        return systemUserOk;
    }

    /**
     * @return the adminUserOk
     */
    public boolean isAdminUserOk() {
        return adminUserOk;
    }

    /**
     * @return the galaxyExisting
     */
    public boolean isGalaxyExisting() {
        return galaxyExisting;
    }

    /**
     * @return the tableExisting
     */
    public boolean isTableExisting() {
        return tableExisting;
    }

    /**
     * @return the constantsExisting
     */
    public boolean isConstantsExisting() {
        return constantsExisting;
    }

    /**
     * @return the constantsResult
     */
    public ArrayList<BaseResult> getConstantsResult() {
        return constantsResult;
    }
}
