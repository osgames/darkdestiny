/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai;

import at.darkdestiny.core.ai.goals.AIGoal;
import at.darkdestiny.core.ai.goals.IGoal;
import at.darkdestiny.core.model.AIGoalUser;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class AIThread extends Thread {

    private final User ai;
    private boolean run = true;
    private ArrayList<AIGoal> goals = new ArrayList<AIGoal>();
    private boolean tickCalculated = false;
    private boolean updateRunning = false;
    private boolean idle = false;

    public AIThread(String name, User ai) {
        this.setName(name);
        AIDebugBuffer.log(this, AIDebugLevel.INFO, "Initialize AI (userId: " + ai.getUserId() + ")");

        for (AIGoalUser goalUser : Service.aiGoalUserDAO.findByUserId(ai.getId())) {
            at.darkdestiny.core.model.AIGoal goal = Service.aiGoalDAO.findById(goalUser.getGoalId());
            //TODO invocate class and pass to ai goals
            //goals.add(new PlanetDevelopmentGoal());
            //goals.add(new ResearchGoal());
        }

        this.ai = ai;
    }

    @Override
    public void run() {
        while (run) {
            try {
                // AIDebugBuffer.add(AIDebugLevel.INFO, "Ticking AI (userId: " + ai.getUserId() + ")");
                while (tickCalculated || updateRunning) {
                    idle = true;
                    Thread.sleep(50);
                    if (!run) {
                        break;
                    }
                }
                idle = false;
                if (!run) {
                    break;
                }

                DebugBuffer.addLine(DebugLevel.TRACE, ai.getGameName() + " processing ...");

                for (IGoal ig : goals) {
                    System.out.println("[" + Thread.currentThread().getName() + "] Processing Goal " + ig.getClass());
                    ig.processGoal(ai.getUserId());
                }
                /*
                 AIDebugBuffer.add(AIDebugLevel.INFO, "Got " + Service.playerPlanetDAO.findByUserId(ai.getUserId()).size() + " planets");
                 for (PlayerPlanet pp : Service.playerPlanetDAO.findByUserId(ai.getUserId())) {
                 planetActions(pp);
                 }
                 */

                tickCalculated = true;

            } catch (InterruptedException ex) {
                Logger.getLogger(AIThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Do some Funky Stuff;
    }

    public void planetActions(PlayerPlanet pp) {

        AIDebugBuffer.log(this, AIDebugLevel.INFO, "Checking mah planet : [" + pp.getPlanetId() + "] " + pp.getName());
        try {
            PlanetCalculation pc = new PlanetCalculation(pp.getPlanetId());
            ProductionResult pcrf = pc.getProductionDataFuture();
            ProductionResult pcr = pc.getPlanetCalcData().getProductionData();

            AIDebugBuffer.log(this, AIDebugLevel.INFO, "Checking mah food production ");
            AIDebugBuffer.log(this, AIDebugLevel.INFO, "producing " + pcr.getRessProduction(Ressource.FOOD) + " consuming " + pcr.getRessConsumption(Ressource.FOOD) + " producing in future " + pcrf.getRessProduction(Ressource.FOOD));

            if (pcrf.getRessProduction(Ressource.FOOD) <= pcr.getRessConsumption(Ressource.FOOD)) {
                AIDebugBuffer.log(this, AIDebugLevel.INFO, "Consumption > Production in future => Act immediatly. Trying to build small agriculture");
                ArrayList<String> result = ConstructionService.buildBuilding(pp.getUserId(), pp.getPlanetId(), Construction.ID_SMALLAGRICULTUREFARM, 1);
                for (String s : result) {
                    AIDebugBuffer.log(this, AIDebugLevel.DEBUG, s);
                }
            } else if ((pcrf.getRessProduction(Ressource.FOOD) - pcr.getRessConsumption(Ressource.FOOD)) / (pcrf.getRessProduction(Ressource.FOOD)) < 0.05d) {
                AIDebugBuffer.log(this, AIDebugLevel.INFO, ((pcrf.getRessProduction(Ressource.FOOD) - pcr.getRessConsumption(Ressource.FOOD)) / (pcrf.getRessProduction(Ressource.FOOD))) + "% more production than consumption. maybe i should act. Trying to build small agriculture");
                ArrayList<String> result = ConstructionService.buildBuilding(pp.getUserId(), pp.getPlanetId(), Construction.ID_SMALLAGRICULTUREFARM, 1);
                for (String s : result) {
                    AIDebugBuffer.log(this, AIDebugLevel.DEBUG, s);
                }
            } else if ((pcrf.getRessProduction(Ressource.FOOD) - pcr.getRessConsumption(Ressource.FOOD)) / (pcrf.getRessProduction(Ressource.FOOD)) > 0.05d) {
                AIDebugBuffer.log(this, AIDebugLevel.INFO, ((pcrf.getRessProduction(Ressource.FOOD) - pcr.getRessConsumption(Ressource.FOOD)) / (pcrf.getRessProduction(Ressource.FOOD))) + "% more production than consumption totally okay");
            }

        } catch (Exception ex) {
            Logger.getLogger(AIThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param run the run to set
     */
    public void setRun(boolean run) {
        this.run = run;

    }

    /**
     * @return the ai
     */
    public User getAi() {
        return ai;
    }

    public void resetTick() {
        tickCalculated = false;
        updateRunning = false;
    }

    public void stopForUpdate() {
        updateRunning = true;
        while (!idle) {
            try {
                Thread.sleep(20);
            } catch (Exception e) {
            }
        }
    }
}
