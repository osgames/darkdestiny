package at.darkdestiny.core;

import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 * @author Martin
 */
public class CheckForUpdate implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(CheckForUpdate.class);

    private Thread currentUpdateThread = null;
    private static CheckForUpdate instance = null;
    private boolean isUpdateRequested = false;
    private static boolean isUpdateDead = false;
    private static HashMap<Long, Exception> exception = new HashMap<Long, Exception>();
    private static HashMap<Long, Throwable> throwables = new HashMap<Long, Throwable>();
    private static int restartTry = 0;

    private CheckForUpdate() {
    }

    protected synchronized void waitForNextRequest() throws InterruptedException {
        while (!isUpdateRequested) {
            wait();
        }
    }

    public static Thread getCheckForUpdateThread() {
        if (instance == null) return null;
        return instance.currentUpdateThread;
    }

    public void run() {
        try {
            while (true) {
                try {
                    waitForNextRequest();
                } catch (InterruptedException ie) {
                    return;
                }

                Updater update = new Updater();
                if (!update.runUpdate()) {
                    isUpdateRequested = false;
                    wakeWaiting();
                }
            }
        } catch (Throwable t) {
            isUpdateRequested = false;
            wakeWaiting();

            StringBuffer sb = new StringBuffer("Update-Thread encountered Exception: " + t.getMessage() + " (" + t.getClass().getName() + ")");
            for (StackTraceElement ste : t.getStackTrace()) {
                sb.append("<br>" + ste.toString());
            }
            if (t.getCause() != null) {
                sb.append(DebugBuffer.produceStackTrace(t.getCause()));
            }

            log.debug("ALAAAARM: " + sb.toString());
            DebugBuffer.writeStackTrace("Update-Thread encountered Exception ", t);
            if (t instanceof Exception) {
                exception.put(System.currentTimeMillis(), (Exception) t);
            } else {
                throwables.put(System.currentTimeMillis(), t);
            }

        } finally { //Hier kommt das Update nur mit einem Error hin ...
            isUpdateDead = true;
            isUpdateRequested = false;
            currentUpdateThread = null;
        }
    }

    protected synchronized void wakeWaiting() {
        notifyAll();
    }

    // Adds a new user into request buffer queue
    public static void requestUpdate() {
        if (instance == null) {
            instance = new CheckForUpdate();
        }

        if (isUpdateDead) {
            if (restartTry < 3) {
                restartTry++;
                DebugBuffer.addLine(DebugLevel.ERROR, "Updater is DEAD - Automatic restart (try " + restartTry + ")!");
                CheckForUpdate.restartUpdateThread();
            } else {
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Updater is DEAD - Automatic restart failed!");
            }
        } else {
            instance.doRequestUpdate();
        }
    }

    public static void restartUpdateThreadManual() {
        if (!isUpdateDead) {
            return;
        }
        restartTry = 0;
        restartUpdateThread();
    }

    public static synchronized void restartUpdateThread() {
        if (!isUpdateDead) {
            return;
        }
        isUpdateDead = false;

        instance.currentUpdateThread = null;
        instance.isUpdateRequested = false;
        instance.doRequestUpdate();
    }

    protected synchronized void doRequestUpdate() {
        if (isUpdateDead) {
            return;
        }

        if (!isUpdateRequested) // DebugBuffer.addLine("ADDING UPDATE REQUEST");
        {
            if ((currentUpdateThread == null) || (!currentUpdateThread.isAlive())) {
                // log.debug("CREATING NEW UPDATE THREAD");
                currentUpdateThread = new Thread(this);
                currentUpdateThread.setName("Updater (2)");
                currentUpdateThread.start();
            }
        }

        isUpdateRequested = true;
        notifyAll();
        while (isUpdateRequested && (!isUpdateDead)) {
            try {
                wait(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public static HashMap<Long, Exception> getException() {
        return exception;
    }

    public static HashMap<Long, Throwable> getThrowables() {
        return throwables;
    }

    public static boolean isUpdateDead() {
        return isUpdateDead;
    }

    public static int getRestartTries() {
        return restartTry;
    }
}
