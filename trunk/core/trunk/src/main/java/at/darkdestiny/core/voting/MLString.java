/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

import at.darkdestiny.core.ML;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class MLString implements CharSequence {
    private final String mlstring;
    private final HashMap<String,String> mlComponent = new HashMap<String,String>();

    public MLString(String mlstring) {
        this.mlstring = mlstring;
    }

    public void addMLComponent(String identifier, String constant) {
        mlComponent.put(identifier,constant);
    }

    @Override
    public int length() {
        return mlstring.length();
    }

    @Override
    public char charAt(int index) {
        return mlstring.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return mlstring.subSequence(start, end);
    }

    @Override
    public String toString() {
        return mlstring;
    }

    public String toMLString(int userId) {
        String mlStringOut = mlstring;

        for (Map.Entry<String,String> component : mlComponent.entrySet()) {
            mlStringOut = mlStringOut.replaceAll(component.getKey(), ML.getMLStr(component.getValue(),userId));
        }

        return mlStringOut;
    }
}
