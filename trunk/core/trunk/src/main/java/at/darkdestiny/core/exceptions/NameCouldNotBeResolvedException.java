/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.exceptions;

/**
 *
 * @author Stefan
 */
public class NameCouldNotBeResolvedException extends Exception {
    private static final long serialVersionUID = -8601278311339751496L;

    public NameCouldNotBeResolvedException() {
    }

    public NameCouldNotBeResolvedException(String message) {
        super(message);
    }

    public NameCouldNotBeResolvedException(Throwable cause) {
        super(cause);
    }

    public NameCouldNotBeResolvedException(String message, Throwable cause) {
        super(message, cause);
    }
}
