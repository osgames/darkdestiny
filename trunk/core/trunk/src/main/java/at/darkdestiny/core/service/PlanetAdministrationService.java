/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.service;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public class PlanetAdministrationService {
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    public static boolean showMigrationWarning(int userId, int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        if (pp.getUserId() != userId) return false;

        PlanetConstruction pc = pcDAO.findBy(planetId, Construction.ID_CIVILSPACEPORT);
        int maxMigration = 150000;
        if (pc != null) {
            maxMigration += (pc.getNumber() * 250000);
        }

        if (pp.getMigration() < 0) {
            if (Math.abs(pp.getMigration()) > ((double)maxMigration * 0.95d)) {
                return true;
            }
        }

        return false;
    }
}
