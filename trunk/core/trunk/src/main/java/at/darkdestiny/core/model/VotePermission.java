/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "votepermission")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class VotePermission extends Model<VotePermission> {

    public static final int PERM_SINGLE = 1;
    public static final int PERM_ALLIANCE = 2;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("voteId")
    private Integer voteId;
    @FieldMappingAnnotation("type")
    private Integer type;
    @FieldMappingAnnotation("typeId")
    private Integer typeId;
    @FieldMappingAnnotation("minRank")
    private Integer minRank;

    public VotePermission() {
    }

    public VotePermission(int type, int typeId) throws Exception {
        this.type = type;
        this.typeId = typeId;
        this.minRank = 0;

        if ((type < 1) || (type > 2)) {
            throw new Exception("Invalid permission type");
        }
    }

    public VotePermission(int type, int typeId, int minRank) throws Exception {
        this.type = type;
        this.typeId = typeId;
        this.minRank = minRank;

        if ((type < 1) || (type > 2)) {
            throw new Exception("Invalid permission type");
        }
        if ((minRank < 0) || (minRank > 4)) {
            throw new Exception("Ranks outside 0-4 are not allowed!");
        }
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the voteId
     */
    public Integer getVoteId() {
        return voteId;
    }

    /**
     * @param voteId the voteId to set
     */
    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the typeId
     */
    public Integer getTypeId() {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    /**
     * @return the minRank
     */
    public Integer getMinRank() {
        return minRank;
    }

    /**
     * @param minRank the minRank to set
     */
    public void setMinRank(Integer minRank) {
        this.minRank = minRank;
    }
}
