/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.StyleToUser;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class StyleToUserDAO extends ReadWriteTable<StyleToUser> implements GenericDAO {

    public ArrayList<StyleToUser> findByUserId(int userId) {
        StyleToUser stu = new StyleToUser();
        stu.setUserId(userId);
        return find(stu);
    }

    public StyleToUser findBy(int userId, int styleId) {
        StyleToUser stu = new StyleToUser();
        stu.setUserId(userId);
        stu.setStyleId(styleId);
        return (StyleToUser) get(stu);
    }
}
