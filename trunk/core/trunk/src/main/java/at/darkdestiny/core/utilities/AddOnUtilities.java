/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

 import at.darkdestiny.core.addon.AddOnProperties;import at.darkdestiny.core.addon.IAddOn;
import at.darkdestiny.core.dao.AddOnDAO;
import at.darkdestiny.core.enumeration.EAddOnProperty;
import at.darkdestiny.core.model.AddOn;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.io.File;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.addon.AddOnProperties;
import at.darkdestiny.core.addon.IAddOn;
import at.darkdestiny.core.dao.AddOnDAO;
import at.darkdestiny.core.enumeration.EAddOnProperty;
import at.darkdestiny.core.model.AddOn;
import java.io.File;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class AddOnUtilities {

    private static final Logger log = LoggerFactory.getLogger(AddOnUtilities.class);

    public static AddOnDAO addOnDAO = (AddOnDAO) DAOFactory.get(AddOnDAO.class);
    public static HashMap<String, IAddOn> addOns = new HashMap<String, IAddOn>();

    public static void loadAddOns() {

        DebugBuffer.addLine(DebugLevel.TRACE, "Loading AddOns");
        for (AddOn addOn : addOnDAO.findAllActive()) {
            try {
                URL u = AddOnUtilities.class.getResource("/db.properties");

                String loc;
                loc = u.toString().replace("classes/db.properties", "addon").substring(6);
                loc = loc.replace("root/", "");

                File file = new File(loc + "/" + addOn.getPackageName());
                log.debug("File : " + file.getAbsolutePath());
                URL jarfile = new URL("jar", "", "file:" + file.getAbsolutePath() + "!/");
                URLClassLoader cl = URLClassLoader.newInstance(new URL[]{jarfile}, AddOnUtilities.class.getClassLoader());


                Class loadedClass = cl.loadClass("at.darkdestiny.core.addon." + addOn.getInterfaceName().trim());



                Constructor c = loadedClass.getConstructor();

                IAddOn loadedAddOn = (IAddOn) c.newInstance();
                loadedAddOn.print();
                AddOnProperties aop = loadedAddOn.getProperties();

                loadedAddOn.install();

                register(addOn.getName(), loadedAddOn);

            } catch (Exception e) {
                e.printStackTrace();
                DebugBuffer.addLine(DebugLevel.ERROR, "Could not load addon : name =" + addOn.getName());
            }
        }
    }

    public static void register(String addOnName, IAddOn addOnClass) {
        addOns.put(addOnName, addOnClass);

    }

    public static void loadPages(AddOnProperties properties){

        log.debug("Webpagefolder : " + properties.getProperty(EAddOnProperty.WEBPAGE_FOLDER));
        log.debug("Webpagefolder_ADMIN : " + properties.getProperty(EAddOnProperty.WEBPAGE_FOLDER_ADMIN));
        log.debug("Webpagefolder_ADDON : " + properties.getProperty(EAddOnProperty.WEBPAGE_FOLDER_ADDON));
    }
/*
    private static AddOnProperties defineAddOnProperties() {
        AddOnProperties aop = new AddOnProperties();

        URL u = AddOnUtilities.class.getResource("/db.properties");

        String loc;
        loc = u.toString().replace("/WEB-INF/classes/db.properties", "").substring(6);

        File file = new File(loc);
        aop.setProperty(EAddOnProperty.WEBPAGE_FOLDER, file.getAbsolutePath());

        loc = u.toString().replace("/WEB-INF/classes/db.properties", "/addon").substring(6);
        file = new File(loc);
        aop.setProperty(EAddOnProperty.WEBPAGE_FOLDER_ADDON, file.getAbsolutePath());
        loc = u.toString().replace("/WEB-INF/classes/db.properties", "/admin").substring(6);
        file = new File(loc);
        aop.setProperty(EAddOnProperty.WEBPAGE_FOLDER_ADMIN, file.getAbsolutePath());



        return aop;
    }*/
}
