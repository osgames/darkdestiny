/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.AIGoal;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Aion
 */
public class AIGoalDAO extends ReadWriteTable<AIGoal> implements GenericDAO {

    public AIGoal findById(int id) {
        AIGoal aiGoal = new AIGoal();
        aiGoal.setId(id);
        return (AIGoal) get(aiGoal);
    }

}
