/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.EmbassyDAO;
import at.darkdestiny.core.enumeration.EDiplomacyRelationType;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation("diplomacyrelation")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class DiplomacyRelation extends Model<DiplomacyRelation> {

    private static EmbassyDAO embassyDAO = (EmbassyDAO) DAOFactory.get(EmbassyDAO.class);
    private static AllianceDAO allianceDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static AllianceMemberDAO allianceMemberDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    @FieldMappingAnnotation("fromId")
    @IdFieldAnnotation
    private Integer fromId;
    @FieldMappingAnnotation("toId")
    @IdFieldAnnotation
    private Integer toId;
    @FieldMappingAnnotation("diplomacyTypeId")
    @IdFieldAnnotation
    private Integer diplomacyTypeId;
    @FieldMappingAnnotation("type")
    @IdFieldAnnotation
    private EDiplomacyRelationType type;
    @FieldMappingAnnotation("date")
    private Long date;
    @FieldMappingAnnotation("initialiser")
    private Boolean initialiser;
    @FieldMappingAnnotation("acknowledged")
    private Boolean acknowledged;
    @FieldMappingAnnotation("pending")
    private Boolean pending;
    @FieldMappingAnnotation("sharingMap")
    @DefaultValue("0")
    private Boolean sharingMap;

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the fromId
     */
    public Integer getFromId() {
        return fromId;
    }

    /**
     * @param fromId the fromId to set
     */
    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    /**
     * @return the toId
     */
    public Integer getToId() {
        return toId;
    }

    /**
     * @param toId the toId to set
     */
    public void setToId(Integer toId) {
        this.toId = toId;
    }

    /**
     * @return the initialiser
     */
    public Boolean getInitialiser() {
        return initialiser;
    }

    /**
     * @param initialiser the initialiser to set
     */
    public void setInitialiser(Boolean initialiser) {
        this.initialiser = initialiser;
    }

    /**
     * @return the acknowledged
     */
    public Boolean getAcknowledged() {
        return acknowledged;
    }

    /**
     * @param acknowledged the acknowledged to set
     */
    public void setAcknowledged(Boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    /**
     * @return the pending
     */
    public Boolean getPending() {
        return pending;
    }

    /**
     * @param pending the pending to set
     */
    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    /**
     * @return the diplomacyTypeId
     */
    public Integer getDiplomacyTypeId() {
        return diplomacyTypeId;
    }

    /**
     * @param diplomacyTypeId the diplomacyTypeId to set
     */
    public void setDiplomacyTypeId(Integer diplomacyTypeId) {
        this.diplomacyTypeId = diplomacyTypeId;
    }

    /**
     * @return the type
     */
    public EDiplomacyRelationType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EDiplomacyRelationType type) {
        this.type = type;
    }

    /**
     * @return the sharingMap
     */
    public Boolean getSharingMap() {
        //       log.debug(fromId +" - " + toId + " - " + type.toString() + " - " + sharingMap);
        if (diplomacyTypeId == DiplomacyType.ALLY) {
            return true;
        }
        if (diplomacyTypeId == DiplomacyType.TREATY) {
            Embassy e = null;
            //Checken auf Embassy
            if (type.equals(EDiplomacyRelationType.USER_TO_USER)) {
                e = embassyDAO.findBy(fromId, toId);
            } else if (type.equals(EDiplomacyRelationType.USER_TO_ALLIANCE)) {
                ArrayList<Alliance> allys = allianceDAO.findByMasterId(toId);
                for (AllianceMember am : allianceMemberDAO.findAllianceMembersOfAlliancesNotTrial(allys)) {
                    int toUserId = am.getUserId();
                    e = embassyDAO.findBy(fromId, toUserId);
                    if (e != null) {
                        break;
                    }
                }
            } else if (type.equals(EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)) {
                ArrayList<Alliance> allys1 = allianceDAO.findByMasterId(fromId);
                ArrayList<Alliance> allys2 = allianceDAO.findByMasterId(toId);
                for (AllianceMember am1 : allianceMemberDAO.findAllianceMembersOfAlliancesNotTrial(allys1)) {
                    for (AllianceMember am2 : allianceMemberDAO.findAllianceMembersOfAlliancesNotTrial(allys2)) {
                        e = embassyDAO.findBy(am1.getUserId(), am2.getUserId());
                        if (e != null) {
                            break;
                        }
                    }
                    if (e != null) {
                        break;
                    }
                }
            } else if (type.equals(EDiplomacyRelationType.ALLIANCE_TO_USER)) {
                ArrayList<Alliance> allys = allianceDAO.findByMasterId(fromId);
                for (AllianceMember am : allianceMemberDAO.findAllianceMembersOfAlliancesNotTrial(allys)) {
                    int fromUserId = am.getUserId();
                    e = embassyDAO.findBy(fromUserId, toId);
                    if (e != null) {
                        break;
                    }
                }
            }
            if (e == null) {
                return false;
            }
            return sharingMap;
        } else {
            return false;
        }
    }

    /**
     * @return the sharingMap
     */
    public Boolean getSharingMapClean() {
        //Checken auf Embassy
        return sharingMap;
    }

    /**
     * @param sharingMap the sharingMap to set
     */
    public void setSharingMap(Boolean sharingMap) {
        this.sharingMap = sharingMap;
    }
}
