/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.update;

import at.darkdestiny.core.dao.*;
import at.darkdestiny.core.enumeration.ENoTransportReason;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.*;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.service.RessourceService;
import at.darkdestiny.core.service.TradeService;
import at.darkdestiny.core.update.transport.CapacityMultiEdge;
import at.darkdestiny.core.update.transport.CapacityOneEdge;
import at.darkdestiny.core.update.transport.DirectionalEdge;
import at.darkdestiny.core.update.transport.PlanetNode;
import at.darkdestiny.core.update.transport.RessCapacityMultiEdge;
import at.darkdestiny.core.update.transport.TransportGraph;
import at.darkdestiny.core.utilities.TitleUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Stefan
 */
public abstract class AbstractRouteProcessing {
    private static final Logger log = LoggerFactory.getLogger(AbstractRouteProcessing.class);
    protected static TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);
    protected static TradeRouteDetailDAO trdDAO = (TradeRouteDetailDAO) DAOFactory.get(TradeRouteDetailDAO.class);
    protected static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    protected static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    protected final UpdaterDataSet uds;
    protected HashMap<Integer, TradeRouteProcessingEntry> allEntries = new HashMap<Integer, TradeRouteProcessingEntry>();
    protected ArrayList<TradeRouteDetailProcessing> allDetailEntries = new ArrayList<TradeRouteDetailProcessing>();
    protected ArrayList<TradeRouteDetail> allRouteDetails = new ArrayList<TradeRouteDetail>();
    protected boolean debug = false;
    protected TransportGraph tg = new TransportGraph();

    protected ArrayList<TradeRouteDetail> toUpdateLocalTransport = Lists.newArrayList();
    protected ArrayList<TradeRouteDetail> toUpdateLocalTrade = Lists.newArrayList();
    
    protected AbstractRouteProcessing(UpdaterDataSet uds) {
        this.uds = uds;
    }

    private void log(String message) {
        if (debug) log.debug("[TRANSPORT] " + message);
    }

    public void processRoutes(int actTick) {
        // Clean temporary working values in Tranport Graph
        tg.cleanUp();

        // allDetailEntries.clear();
        // planetMaxIn.clear();
        // planetMaxOut.clear();
        ArrayList<TradeRouteDetailProcessing> markedForDeletion = new ArrayList<TradeRouteDetailProcessing>();

        // Process all planets and calculate maxIn and maxOut values
        ArrayList<PlanetNode> allPlanets = tg.getAllPlanets();

        for (TradeRouteDetailProcessing trdp : allDetailEntries) {
            trdp.getBase().setCapReason(ENoTransportReason.NONE);
        }

        if (debug) {
            log("Start processing of transport routes - Found " + allPlanets.size() + " planet entries");
        }

        for (PlanetNode pn : allPlanets) {
            for (Ressource r : rDAO.findAllStoreableRessources()) {
                // -----------------------------
                // Check Source Planet for Stock
                // -----------------------------
                PlanetRessource pr = uds.getPlanetStorageEntry(pn.getPlanetId(), r.getId());
                long inStock = 0;
                if (pr != null) {
                    inStock = pr.getQty();
                }

                if (this instanceof TradeRouteProcessing) {
                    pr = uds.getPlanetRessEntry(pn.getPlanetId(),
                        EPlanetRessourceType.MINIMUM_TRADE, r.getId().intValue());
                } else {
                    pr = uds.getPlanetRessEntry(pn.getPlanetId(),
                        EPlanetRessourceType.MINIMUM_TRANSPORT, r.getId().intValue());
                }

                long minStock = 0;
                if (pr != null) {
                    minStock = pr.getQty();
                }

                long maxStock = uds.getMaxStorage(pn.getPlanetId(),
                        r.getId());

                if (debug) {
                    log("[" + pn.getPlanetId() + "/" + r.getId() + "] MAXOUT: " + inStock + " - " + minStock);
                }
                if (debug) {
                    log("[" + pn.getPlanetId() + "/" + r.getId() + "] MAXIN: " + maxStock + " - " + inStock);
                }

                long maxIn = Math.max(0, maxStock - inStock);
                long maxOut = Math.max(0, inStock - minStock);

                pn.setMaxOutgoingQty(r.getId(), (int) maxOut);
                pn.setMaxIncomingQty(r.getId(), (int) maxIn);
            }
        }

        // Loop each planet and transport as much ressources as possible
        for (PlanetNode pn : allPlanets) {            
            if (!(this instanceof TradeRouteProcessing)) {
                // Process Outgoing MultiRessEntries for current Planet
                processMultiEdgeSingleRess(pn);
                moveAllAssignedToTotal();
                // ===========================================================================
                // If we have special routes only transporting one resource over one route
                // process this data here
                // ===========================================================================
                // processOneEdgeSingleRess();

                // ===========================================================================
                // Oldschool processing of normal routes served by ships and transmitters
                // ===========================================================================
                processOneEdgeMultipleRess(pn);
                moveAllAssignedToTotal();
            } else {
                // ===========================================================================
                // Now calculate resource independent MultiEntries (currently only trade)
                //               C U R R E N T L Y    T R A D E    O N L Y
                // ===========================================================================
                processMultiEdgeMultiRess(pn);                        
                moveAllAssignedToTotal();
            }
        }

        // Quantities have been assigned - now store data to database
        ArrayList<TradeRouteDetail> toUpdate = new ArrayList<TradeRouteDetail>();

        for (TradeRouteDetailProcessing trdp : allDetailEntries) {
            TradeRouteDetail trd = trdp.getBase();

            TradeRoute tr = trDAO.getById(trdp.getBase().getRouteId());
            TradeRouteDetail trdTmp = trdDAO.getById(trdp.getBase().getId());

            if (tr == null) {
                DebugBuffer.error("Skipped processing of traderoute entry which does not exist anymore [TraderouteId: " + trdp.getBase().getRouteId() +
                        "] referenced by Traderoute detail: " + trdp.getBase().getRessId() + " - MD: " + trdp.getBase().getMaxDuration() + " - MQTY: " + trdp.getBase().getMaxQty());
                continue;
            }
            if (trdTmp == null) {
                DebugBuffer.trace("TradeRoute Info: UserId="+tr.getUserId()+" StartPlanet="+tr.getStartPlanet()+" TargetPlanet="+tr.getTargetPlanet()+" TradePostId="+tr.getTradePostId()+" Status="+tr.getStatus()+ " Type="+tr.getType());
                DebugBuffer.trace("TradeRouteDetail Info: Not available");
                DebugBuffer.error("Skipped processing of traderoutedetail entry which does not exist anymore [TraderouteDetailId: " + trdp.getBase().getRouteId() +
                        "] referenced by Traderoute detail: " + trdp.getBase().getRessId() + " - MD: " + trdp.getBase().getMaxDuration() + " - MQTY: " + trdp.getBase().getMaxQty() + " - CLOSED: " + trdp.isClosed());
                continue;
            }                
            
            if (trdp.getTotalAssignedQty() > 0) {
                trdp.getBase().setLastTransport(trdp.getTotalAssignedQty());
                // trdDAO.update(trdp.getBase());            
                
                // int sourcePlanet = tr.getStartPlanet();
                int targetPlanet = tr.getTargetPlanet();
                PlanetRessource source;
                PlanetRessource target;

                // log("<TRADE> Source: " + sourcePlanet + " Target: " + targetPlanet + " / Reversed: " + trdp.getBase().getReversed());

                if (trdp.getBase().getReversed()) {
                    // sourcePlanet = tr.getTargetPlanet();
                    targetPlanet = tr.getStartPlanet();
                    target = uds.getPlanetStorageEntry(tr.getStartPlanet(), trdp.getBase().getRessId());
                    source = uds.getPlanetStorageEntry(tr.getTargetPlanet(), trdp.getBase().getRessId());
                } else {
                    // sourcePlanet = tr.getStartPlanet();
                    targetPlanet = tr.getTargetPlanet();
                    source = uds.getPlanetStorageEntry(tr.getStartPlanet(), trdp.getBase().getRessId());
                    target = uds.getPlanetStorageEntry(tr.getTargetPlanet(), trdp.getBase().getRessId());
                }

                // // java.lang.System.out.println("Process route " + tr.getId() + " from " + sourcePlanet + " to " + targetPlanet + " RESSID: " + trdp.getBase().getRessId());

                try {
                    TitleUtilities.incrementCondition(ConditionToTitle.MERCHANT_RESOURCES, ppDAO.findByPlanetId(targetPlanet).getUserId(), trdp.getTotalAssignedQty());
                } catch (Exception e) {
                    // DebugBuffer.addLine(DebugBuffer.DebugLevel.DEBUG, "Error1 in traderouteprocessingNew with titles : " + e);
                }

                if (target == null) {
                    uds.createNewStorageRessEntry(targetPlanet, trdp.getBase().getRessId(), trdp.getTotalAssignedQty());
                } else {
                    target.setQty(target.getQty() + trdp.getTotalAssignedQty());
                }

                // log("<TRADE> Reduce storage for " + trdp + " by " + trdp.getAssignedQty());
                if (this instanceof TransportProcessing) {
                    source.setQty(source.getQty() - trdp.getTotalAssignedQty());
                }

                // Increase Qty already delivered
                trd.setQtyDelivered(trd.getQtyDelivered() + trdp.getTotalAssignedQty());

                // Do Qty check
                if (trdp.getBase().getMaxQty() > 0) {
                    if ((trd.getMaxQty() - trd.getQtyDelivered()) <= 0) {
                        markedForDeletion.add(trdp);
                        if (debug) {
                            log("Mark route for deletion due to maxQty");
                        }
                        // trpe.setDeleted(true);
                    } else {
                        toUpdate.add(trd);
                    }
                } else {
                    toUpdate.add(trd);
                }
            } else {
                if (trd.getLastTransport() != 0) {
                    trd.setLastTransport(0);
                }

                toUpdate.add(trd);
            }

            if (trdp.getBase().getMaxDuration() > 0) {
                if (trdp.getBase().getRemainingTime(actTick) <= 0) {
                    markedForDeletion.add(trdp);
                    // trpe.setDeleted(true);
                    if (debug) {
                        log("Mark route for deletion due to maxDur");
                    }
                }
            }
        }

        DebugBuffer.trace("Updating #1: " + toUpdate.size() + " entries");
        
        if (this instanceof TransportProcessing) {
            toUpdateLocalTransport = toUpdate;
            Thread storeThread1 = new Thread(new Runnable() {
                 public void run() {        
                    trdDAO.updateAll(toUpdateLocalTransport);
                 }
            });  
            storeThread1.run();            
        } else if (this instanceof TradeRouteProcessing) {
            toUpdateLocalTrade = toUpdate;
            Thread storeThread1 = new Thread(new Runnable() {
                 public void run() {        
                    trdDAO.updateAll(toUpdateLocalTrade);
                 }
            });  
            storeThread1.run();                  
        }        
        
        for (TradeRouteDetailProcessing trdp : markedForDeletion) {
            TradeRoute tr = trDAO.getById(trdp.getBase().getRouteId());
            TradeService.deleteRouteDetail(tr.getUserId(), trdp.getBase().getId());
        }
    }

    // Capacity is set on a planet and a specific ressource and can be splitted up on all routes transporting that resource (AGRICULTURAL DISTRIBUTION CENTER)
    private void processMultiEdgeSingleRess(PlanetNode pn) {
        log("============ PROCESS RESSCAPMULTI ===============");        
        ArrayList<DirectionalEdge> outgoingEdges = pn.getOutgoingTrades();
        
        HashMap<Integer,Integer> openRoutesPerResource = Maps.newHashMap();
        int openOutgoingRoutes = 0;
        
        for (DirectionalEdge de : outgoingEdges) {
            for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                // log.debug("Read " + trdp);
                
                if (!trdp.isClosed()) {
                    int ressId = trdp.getBase().getRessId();
                    
                    // We do not need to consider routes where no capacity is available
                    if (pn.getRessCapMultiEdge(ressId) == null) continue;
                    
                    if (openRoutesPerResource.containsKey(ressId)) {
                        openRoutesPerResource.put(ressId,openRoutesPerResource.get(ressId) + 1);
                    } else {
                        openRoutesPerResource.put(ressId, 1);
                    }
                    
                    openOutgoingRoutes++;
                }
            }
        }
        
        int openCapacity = 0;
        int openRoutes = 0;
        int loopRun = 0;
        
        boolean localDebug = false;
        /*
        if (pn.getPlanetId() == 17647) {
            localDebug = true;
        }
        */
        
        do {                      
            loopRun++;
            if (localDebug) log.debug("Loop run");
            // DebugBuffer.debug("[RESS CAP] Processing Planet #" + pn.getPlanetId() + " LoopRun: " + loopRun);
            ArrayList<TradeRouteDetailProcessing> markedForClose = Lists.newArrayList();                                                      
            
            for (Ressource r : RessourceService.getAllTransportableRessources()) {
                RessCapacityMultiEdge rcme = pn.getRessCapMultiEdge(r.getId());
                
                if (rcme == null) {
                    continue;
                }
                
            
                // DebugBuffer.trace("[PRE] OpenCapacity: " + rcme.getFreeCapacity() + " OpenRoutes: " + openRoutes);
                openCapacity = 0;                                             
                
                if (localDebug) log.debug("Process Ressource " + r.getName());
                if (openRoutesPerResource.get(r.getId()) == null) {
                    continue;
                }      
                    
                int restrictionSharedRess = openRoutesPerResource.get(r.getId()); 
                
                for (DirectionalEdge de : outgoingEdges) {
                    // int restrictionByRouteCount = openOutgoingRoutes;    
                    if (localDebug) log.debug("Process " + de);
                                    
                    // log.debug("Process resource " + r.getId());
              
                    
                    PlanetNode pnTarget = de.getTarget();
                    
                    int outgoingMax = pn.getFreeOutgoingQty(r.getId());
                    int incomingMax = pnTarget.getFreeIncomingQty(r.getId());
                    
                    /*
                    if (restrictionByRouteCount == 0) {
                        if (localDebug) log.debug("Skip route count = 0");
                        continue;
                    }
                    */
                    if (restrictionSharedRess == 0) {
                        if (localDebug) log.debug("Skip sharedRess = 0");
                        continue;
                    }
                    
                    // DebugBuffer.trace("de.getCapSingleEdge().getFreeCapacity(): " + rcme.getFreeCapacity() + " / restrictionByRouteCount: " + restrictionSharedRess);
                    int maxCapPerDE = (int)Math.ceil(rcme.getFreeCapacity() / restrictionSharedRess);
                    // DebugBuffer.trace("outgoingMax: " + outgoingMax + " / restrictionSharedRess: " + restrictionSharedRess);
                    int maxQtyPerSR = (int)Math.ceil(Math.max(0,outgoingMax) / restrictionSharedRess);
                    // DebugBuffer.trace("MaxCapDE: " + maxCapPerDE + " / MaxQtySR: " + maxQtyPerSR);
                    
                    for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                        if (trdp.isClosed()) continue;
                        
                        if (!trdp.getBase().getRessId().equals(r.getId())) {                           
                            continue;
                        }
                        
                        boolean close = false;
                        // int stillFree = trdp.getMaxQty() - trdp.getAssignedQty();
                        
                        int qtyToAssign = Math.min((int)Math.ceil(maxCapPerDE / de.getDistance() / 2d), maxQtyPerSR);
                        
                        if (qtyToAssign > trdp.getMaxQty()) {
                            close = true;                            
                        }
                        
                        qtyToAssign = Math.min(qtyToAssign,trdp.getMaxQty());
                        
                        if (qtyToAssign <= 0) {
                            close = true;           
                            trdp.getBase().setCapReason(ENoTransportReason.SOURCE_LOW);
                            // DebugBuffer.debug(trdp + " close due to low source");
                        }

                        if (qtyToAssign >= incomingMax) {
                            close = true;
                            trdp.getBase().setCapReason(ENoTransportReason.TARGET_FULL);
                            // DebugBuffer.debug(trdp + " close due to full target");
                        }                                                     
                        
                        qtyToAssign = Math.min(qtyToAssign,incomingMax); // Reason To Close
                        
                        if (close) {
                            trdp.setClosed(true);
                            openRoutesPerResource.put(r.getId(),openRoutesPerResource.get(r.getId()) - 1);
                            openOutgoingRoutes--;
                        }
                         
                        // Assign Qty 
                        trdp.setAssignedQtyTmp(qtyToAssign);
                        pn.incAssignedOutgoingQty(r.getId(), qtyToAssign);
                        pnTarget.incAssignedIncomingQty(r.getId(), qtyToAssign);
                        markedForClose.add(trdp);
                        // -- See TODO at bottom
                        // trdp.writeAssignedQtyTmp(); 
                        if (localDebug) log.debug("Assigned Qty = " + trdp.getAssignedQtyTmp());
                        // DebugBuffer.trace("["+trdp+"] Assigned Qty = " + trdp.getAssignedQtyTmp());
                    }                                        
                }                               
            }                            
            
            openRoutes = openOutgoingRoutes;
            
            for (TradeRouteDetailProcessing trdp : markedForClose) {                
                trdp.writeAssignedQtyTmp();
            }
            markedForClose.clear();
            
            for (Ressource r : RessourceService.getAllTransportableRessources()) {
                RessCapacityMultiEdge rcme = pn.getRessCapMultiEdge(r.getId());
                
                if (rcme == null) {
                    continue;
                }            
                
                openCapacity = rcme.getFreeCapacity();
            }
            
            // log.debug("OpenCapacity: " + openCapacity + " OpenRoutes: " + openRoutes);
            // DebugBuffer.trace("[POST] OpenCapacity: " + openCapacity + " OpenRoutes: " + openRoutes);
            // TODO: Write Quantities here!!!! Otherwise calculation is messed up 
        } while ((openCapacity > 0) && (openRoutes > 0) && (loopRun < 100));        
    }

    // Capacity is set on a planet and can be used on all outgoing routes (TRADE)
    private void processMultiEdgeMultiRess(PlanetNode pn) {
        log("============ PROCESS CAPMULTI ===============");
        CapacityMultiEdge cme = pn.getCapMultiEdge();

        // A planet without trading post but buying from a trading post must be skipped here
        if (cme == null) {
            // DebugBuffer.warning("CapMultiEdge is null for planet " + pn.getPlanetId());
            return;
        }
        
        ArrayList<DirectionalEdge> outgoingEdges = pn.getOutgoingTrades();
        
        HashMap<Integer,Integer> openRoutesPerResource = Maps.newHashMap();
        int openOutgoingRoutes = 0;
        
        for (DirectionalEdge de : outgoingEdges) {
            for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                // log.debug("Read " + trdp);
                
                if (!trdp.isClosed()) {
                    int ressId = trdp.getBase().getRessId();
                    
                    if (openRoutesPerResource.containsKey(ressId)) {
                        openRoutesPerResource.put(ressId,openRoutesPerResource.get(ressId) + 1);
                    } else {
                        openRoutesPerResource.put(ressId, 1);
                    }
                    
                    openOutgoingRoutes++;
                }
            }
        }
        
        int openCapacity = 0;
        int openRoutes = 0;
        int loopRun = 0;
        
        boolean localDebug = false;
        /*
        if (pn.getPlanetId() == 17647) {
            localDebug = true;
        }
        */
        
        do {                      
            loopRun++;
            if (localDebug) DebugBuffer.debug("Processing Planet #" + pn.getPlanetId() + " LoopRun: " + loopRun);
            ArrayList<TradeRouteDetailProcessing> markedForClose = Lists.newArrayList();
            
            if (localDebug) log.debug("Loop run");
            if (localDebug) DebugBuffer.trace("[PRE] OpenCapacity: " + cme.getFreeCapacity() + " OpenRoutes: " + openRoutes);
            openCapacity = 0;                                                                       
            
            for (Ressource r : RessourceService.getAllTransportableRessources()) {
                if (localDebug) log.debug("Process Ressource " + r.getName());
                if (openRoutesPerResource.get(r.getId()) == null) {
                    continue;
                }      
                    
                int restrictionSharedRess = openRoutesPerResource.get(r.getId()); 
                
                for (DirectionalEdge de : outgoingEdges) {
                    int restrictionByRouteCount = openOutgoingRoutes;    
                    if (localDebug) log.debug("Process " + de);
                                    
                    // log.debug("Process resource " + r.getId());
              
                    
                    PlanetNode pnTarget = de.getTarget();
                    
                    // int outgoingMax = pn.getFreeOutgoingQty(r.getId());
                    int outgoingMax = Integer.MAX_VALUE;
                    int incomingMax = pnTarget.getFreeIncomingQty(r.getId());
                    
                    if (restrictionByRouteCount == 0) {
                        if (localDebug) log.debug("Skip route count = 0");
                        continue;
                    }
                    if (restrictionSharedRess == 0) {
                        if (localDebug) log.debug("Skip sharedRess = 0");
                        continue;
                    }
                    
                    if (localDebug) DebugBuffer.trace("de.getCapSingleEdge().getFreeCapacity(): " + cme.getFreeCapacity() + " / restrictionByRouteCount: " + restrictionByRouteCount);
                    int maxCapPerDE = (int)Math.ceil(cme.getFreeCapacity() / restrictionByRouteCount);
                    if (localDebug) DebugBuffer.trace("outgoingMax: " + outgoingMax + " / restrictionSharedRess: " + restrictionSharedRess);
                    int maxQtyPerSR = (int)Math.ceil(Math.max(0,outgoingMax) / restrictionSharedRess);
                    if (localDebug) DebugBuffer.trace("MaxCapDE: " + maxCapPerDE + " / MaxQtySR: " + maxQtyPerSR);
                    
                    for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                        if (trdp.isClosed()) continue;
                        
                        if (!trdp.getBase().getRessId().equals(r.getId())) {                           
                            continue;
                        }
                        
                        boolean close = false;
                        // int stillFree = trdp.getMaxQty() - trdp.getAssignedQty();
                        
                        int qtyToAssign = Math.min((int)Math.ceil(maxCapPerDE / de.getDistance() / 2d), maxQtyPerSR);
                        
                        if (qtyToAssign > trdp.getMaxQty()) {
                            close = true;
                            if (localDebug) DebugBuffer.debug(trdp + " close due to max qty");
                        }                        
                        qtyToAssign = Math.min(qtyToAssign,trdp.getMaxQty());
                        
                        if (qtyToAssign <= 0) {
                            close = true;           
                            trdp.getBase().setCapReason(ENoTransportReason.SOURCE_LOW);
                            if (localDebug) DebugBuffer.debug(trdp + " close due to low source");
                        }

                        if (qtyToAssign >= incomingMax) {
                            close = true;
                            trdp.getBase().setCapReason(ENoTransportReason.TARGET_FULL);
                            if (localDebug) DebugBuffer.debug(trdp + " close due to full target");
                        }                                                     
                        
                        qtyToAssign = Math.min(qtyToAssign,incomingMax); // Reason To Close
                        
                        if (close) {
                            trdp.setClosed(true);
                        }
                         
                        // Assign Qty 
                        trdp.setAssignedQtyTmp(qtyToAssign);
                        pn.incAssignedOutgoingQty(r.getId(), qtyToAssign);
                        pnTarget.incAssignedIncomingQty(r.getId(), qtyToAssign);
                        markedForClose.add(trdp);
                        // -- See TODO at bottom
                        // trdp.writeAssignedQtyTmp(); 
                        if (localDebug) log.debug("Assigned Qty = " + trdp.getAssignedQtyTmp());
                        if (localDebug) DebugBuffer.trace("["+trdp+"] Assigned Qty = " + trdp.getAssignedQtyTmp());
                    }                                        
                }                               
            }                            
            
            openRoutes = openOutgoingRoutes;
            
            for (TradeRouteDetailProcessing trdp : markedForClose) {       
                if (trdp.isClosed()) {
                    openRoutesPerResource.put(trdp.getBase().getRessId(),openRoutesPerResource.get(trdp.getBase().getRessId()) - 1);
                    openOutgoingRoutes--;                
                }
                trdp.writeAssignedQtyTmp();
            }
            markedForClose.clear();
            
            openCapacity = cme.getFreeCapacity();
            // log.debug("OpenCapacity: " + openCapacity + " OpenRoutes: " + openRoutes);
            if (localDebug) DebugBuffer.trace("[POST] OpenCapacity: " + openCapacity + " OpenRoutes: " + openRoutes);
            // TODO: Write Quantities here!!!! Otherwise calculation is messed up 
        } while ((openCapacity > 0) && (openRoutes > 0) && (loopRun < 100));
    }

    // Capacity is set on a specific route and a specific route (NOT USED CURRENTLY)
    private void processOneEdgeSingleRess() {

    }

    // Capacity is set on a specific route and is distrubuted through that route (NORMAL PROCESSING)
    private void processOneEdgeMultipleRess(PlanetNode pn) {
        log("============ PROCESS NORMAL ROUTES (Processing Planet "+pn.getPlanetId()+") ===============");        
        ArrayList<DirectionalEdge> outgoingEdges = pn.getOutgoingTrades();
        
        HashMap<Integer,Integer> openRoutesPerResource = Maps.newHashMap();
        HashMap<DirectionalEdge,Integer> openRoutesPerEdge = Maps.newHashMap();
        
        for (DirectionalEdge de : outgoingEdges) {            
            boolean foundEntries = false;
            
            for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                // log.debug("Read " + trdp);
                
                if (!trdp.isClosed()) {
                    foundEntries = true;
                    int ressId = trdp.getBase().getRessId();
                    
                    if (openRoutesPerResource.containsKey(ressId)) {
                        openRoutesPerResource.put(ressId,openRoutesPerResource.get(ressId) + 1);
                    } else {
                        openRoutesPerResource.put(ressId, 1);
                    }

                    if (openRoutesPerEdge.containsKey(de)) {
                        openRoutesPerEdge.put(de, openRoutesPerEdge.get(de) + 1);
                    } else {
                        openRoutesPerEdge.put(de, 1);
                    }
                }
            }
            
            if (!foundEntries) {
                openRoutesPerEdge.put(de, 0);
            }
        }
        
        int openCapacity = 0;
        int openRoutes = 0;
        int loopRun = 0;
        
        boolean localDebug = false;
        
        if ((pn.getPlanetId() == 99999) || (pn.getPlanetId() == 99999)) {
            localDebug = true;
        }        
        
        do {                   
            loopRun++;
            // DebugBuffer.debug("[NORMAL] Processing Planet #" + pn.getPlanetId() + " LoopRun: " + loopRun);
            ArrayList<TradeRouteDetailProcessing> markedForClose = Lists.newArrayList();
            
            if (localDebug) log.debug("Loop run");
            openCapacity = 0;                                                                       
            
            for (Ressource r : RessourceService.getAllTransportableRessources()) {
                if (localDebug) {
                    log.debug("==============================================================================");
                    log.debug("Process Ressource " + r.getName());
                }
                if (openRoutesPerResource.get(r.getId()) == null) {
                    continue;
                }      
                    
                int restrictionSharedRess = openRoutesPerResource.get(r.getId()); 
                int outgoingMax = pn.getFreeOutgoingQty(r.getId());

                for (DirectionalEdge de : outgoingEdges) {     
                    if (openRoutesPerEdge == null) {
                        DebugBuffer.warning(de + "OpenRoutesPerEdge is null");
                        continue;
                    } 

                    if (openRoutesPerEdge.get(de) == null) {
                        DebugBuffer.warning("OpenRoutesPerEdge does not contain entry for " + de);
                        continue;
                    }                     
                    
                    int restrictionByRouteCount = openRoutesPerEdge.get(de);    
                    if (localDebug) log.debug("Process " + de);
                                    
                    // log.debug("Process resource " + r.getId());
              
                    
                    PlanetNode pnTarget = de.getTarget();
                                       
                    int incomingMax = pnTarget.getFreeIncomingQty(r.getId());
                    if (localDebug) log.debug("OutgoingMax: " + outgoingMax + " - IncomingMax: " + incomingMax);
                    
                    if (restrictionByRouteCount == 0) {
                        if (localDebug) log.debug("Skip route count = 0");
                        continue;
                    }
                    if (restrictionSharedRess == 0) {
                        if (localDebug) log.debug("Skip sharedRess = 0");
                        continue;
                    }
                    
                    if (localDebug) log.debug("de.getCapSingleEdge().getFreeCapacity(): " + de.getCapSingleEdge().getFreeCapacity() + " / restrictionByRouteCount: " + restrictionByRouteCount);
                    int maxPerDE = (int)Math.ceil(de.getCapSingleEdge().getFreeCapacity() / restrictionByRouteCount);
                    if (localDebug) log.debug("outgoingMax: " + outgoingMax + " / restrictionSharedRess: " + restrictionSharedRess);
                    int maxPerSR = (int)Math.ceil(Math.max(0,outgoingMax) / restrictionSharedRess);
                    if (localDebug) log.debug("MaxDE: " + maxPerDE + " / MaxSR: " + maxPerSR);
                    
                    if ((maxPerSR == 0) && (outgoingMax > 0)) {
                        maxPerSR = 1;
                    }
                    
                    for (TradeRouteDetailProcessing trdp : de.getDetailEntries()) {
                        if (trdp.isClosed()) continue;
                        
                        if (!trdp.getBase().getRessId().equals(r.getId())) {                           
                            continue;
                        }
                        
                        if (localDebug) log.debug("Process Detail Entry >> " + trdp);
                        
                        boolean close = false;
                        int stillFree = trdp.getMaxQty() - trdp.getAssignedQty();
                        
                        int qtyToAssign = Math.min(maxPerDE, maxPerSR);
                        if (qtyToAssign <= 0) {
                            close = true;
                            if (localDebug) log.debug(trdp + " close due to low source");
                        }
                        
                        if (qtyToAssign >= trdp.getMaxQty()) {
                            close = true;
                            if (localDebug) log.debug(trdp + " close due to maximum amount");
                        }
                        qtyToAssign = Math.min(qtyToAssign,trdp.getMaxQty()); // Reason To Close
                        
                        if (qtyToAssign >= stillFree) {
                            close = true;
                            if (localDebug) log.debug(trdp + " close due to maximum amount (by already assigned)");
                        }                        
                        qtyToAssign = Math.min(qtyToAssign,stillFree); // Reason To Close   
                        
                        if (qtyToAssign >= incomingMax) {
                            close = true;
                            if (localDebug) log.debug(trdp + " close due to full target");
                        }                                                     
                        qtyToAssign = Math.min(qtyToAssign,incomingMax); // Reason To Close
                        
                        if (close) {
                            trdp.setClosed(true);
                            openRoutesPerResource.put(r.getId(),openRoutesPerResource.get(r.getId()) - 1);
                            openRoutesPerEdge.put(de,openRoutesPerEdge.get(de) - 1);
                        }
                         
                        // Assign Qty 
                        trdp.setAssignedQtyTmp(qtyToAssign);
                        pn.incAssignedOutgoingQty(r.getId(), qtyToAssign);
                        pnTarget.incAssignedIncomingQty(r.getId(), qtyToAssign);                        
                        // trdp.writeAssignedQtyTmp();
                        markedForClose.add(trdp);
                        // -- See TODO at bottom
                        // trdp.writeAssignedQtyTmp();                         
                        if (localDebug) log.debug("Assigned Qty = " + trdp.getAssignedQtyTmp());
                    }                                        
                }                               
            }                            
            
            openRoutes = 0;
            for (Integer i : openRoutesPerResource.values()) {
                openRoutes += i;                                
            }
            for (Integer i : openRoutesPerEdge.values()) {
                openRoutes += i;            
            }
            
            for (TradeRouteDetailProcessing trdp : markedForClose) {
               trdp.writeAssignedQtyTmp();
            }
            markedForClose.clear();
            
            for (DirectionalEdge de : outgoingEdges) {
                openCapacity += de.getCapSingleEdge().getFreeCapacity();
            }
            
            if (localDebug) log.debug("OpenCapacity: " + openCapacity + " OpenRoutes: " + openRoutes);
            // TODO: Write Quantities here!!!! Otherwise calculation is messed up 
            // DebugBuffer.trace("OpenCapacity: " + openCapacity + " openRoutes: " + openRoutes);
        } while ((openCapacity > 0) && (openRoutes > 0) && (loopRun < 100));
    }

    protected void destroyRoute(TradeRoute tr) {
        if (allEntries.containsKey(tr.getId())) {
            // DebugBuffer.trace("Remove Traderoute Entry " + tr.getId() + " from RouteProcessing");

            for (Iterator<TradeRouteDetailProcessing> trdpIt = allDetailEntries.iterator(); trdpIt.hasNext();) {
                TradeRouteDetailProcessing trdp = trdpIt.next();
                if (trdp.getBase().getRouteId().equals(tr.getId())) {
                    trdpIt.remove();
                }
            }

            TradeRouteProcessingEntry trpe = allEntries.remove(tr.getId());
        }
                
        BaseResult br = TradeService.deleteRoute(tr.getUserId(), tr.getId());
        if (br.isError()) {
            // log.info("Error while destroyingRoute: " + br.getMessage());
        }
    }

    protected void clearAllSetRoutes() {
        ArrayList<TradeRouteDetail> toUpdate = new ArrayList<TradeRouteDetail>();

        for (TradeRouteDetail trd : ((ArrayList<TradeRouteDetail>) trdDAO.findAll())) {
            if (trd.getLastTransport() != 0) {
                trd.setLastTransport(0);
                toUpdate.add(trd);
            }
        }

        DebugBuffer.trace("Updating #2: " + toUpdate.size() + " entries");
        trdDAO.updateAll(toUpdate);
    }

    private DirectionalEdge getDirectionalEdge(PlanetNode outgoing, TradeRouteDetailProcessing trdp) {
        return outgoing.getDirectionalEdgeOutFor(trdp);
    }

    // returns if all capacity could be assigned
    private boolean assignCapacityToDirectionalEdge(int quantity, HashSet<TradeRouteProcessingEntry> ignore) {
        return false;
    }

    // returns free capacity
    private int recalculateSharedRoutes(ArrayList<TradeRouteProcessingEntry> sharedEntries) {
        return 0;
    }
    
    private void moveAllAssignedToTotal() {
        for (TradeRouteDetailProcessing trdp : allDetailEntries) {
            trdp.moveAssignedQtyToTotal();
        }
    }
}
