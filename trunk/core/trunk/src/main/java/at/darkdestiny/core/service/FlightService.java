/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.service;

import at.darkdestiny.core.dao.ViewTableDAO;
import at.darkdestiny.core.model.ViewTable;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class FlightService {
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);

    public static boolean hasViewTableEntryFor(int userId, int systemId, int planetId) {
        ArrayList<ViewTable> vtEntries = vtDAO.findByUserAndSystem(userId, systemId);

        return (vtEntries.size() > 0);
    }
}
