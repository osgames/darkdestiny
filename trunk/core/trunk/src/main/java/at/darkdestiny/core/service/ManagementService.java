/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

 import at.darkdestiny.core.GameConstants;import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetCategory;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.System;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.core.utilities.DestructionUtilities;
import at.darkdestiny.core.utilities.PopulationUtilities;
import at.darkdestiny.core.utilities.ResearchUtilities;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetCategory;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.System;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.planetcalc.ProductionResult;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.trade.TradeRouteExt;
import at.darkdestiny.core.utilities.DestructionUtilities;
import at.darkdestiny.core.utilities.PopulationUtilities;
import at.darkdestiny.core.utilities.ResearchUtilities;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author Eobane
 */
public class ManagementService extends Service {

    private static final Logger log = LoggerFactory.getLogger(ManagementService.class);

    private static TradeRouteDAO trDAO = (TradeRouteDAO) DAOFactory.get(TradeRouteDAO.class);

    public static PlayerPlanet getPlayerPlanetById(int planetId) {
        return playerPlanetDAO.findByPlanetId(planetId);
    }

    public static Planet getPlanetById(int planetId) {
        return planetDAO.findById(planetId);
    }

    public static ArrayList<Ressource> getAllStorableRessources(int planetId) {
        ArrayList<Ressource> storeable = ressourceDAO.findAllStoreableRessources();
        // @TODO Remove ressources where store on planet == 0

        return storeable;
    }

    public static long findMinStock(int planetId, int ressourceId, EPlanetRessourceType type) {
        PlanetRessource pr = planetRessourceDAO.findBy(planetId, ressourceId, type);
        if (pr == null) {
            return 0;
        }
        return pr.getQty();
    }

    public static String getPlanetName(int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        if (pp != null) {
            return pp.getName();
        }
        return "Planet #" + planetId;
    }

    public static String getSystemNamefromPlanetId(int planetId) {
        System s = systemDAO.findById(planetDAO.findById(planetId).getSystemId());
        if (s != null) {
            return s.getName();
        }

        return "System #" + s.getId();
    }

    public static System getSystemForPlanet(int planetId) {
        System s = systemDAO.findById(planetDAO.findById(planetId).getSystemId());

        return s;
    }

    public static PlanetCalculation getCalculatedPlanetValues(int planetId) {
        try {
            return new PlanetCalculation(planetId);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Could not load extended planet calculations: ", e);
        }

        return null;
    }

    @Deprecated
    public static String getMoraleChange(int planetId) {
        return PopulationUtilities.getChange(planetId);
    }

    @Deprecated
    public static int getMorale(int planetId) {
        return playerPlanetDAO.findByPlanetId(planetId).getMoral();
    }

    @Deprecated
    public static float getMaxGrowth(int planetId) {
        return PopulationUtilities.getGrowth(planetId).getMaxGrowth();
    }

    @Deprecated
    public static int getTaxIncome(int planetId) {
        return PopulationUtilities.getTaxIncome(planetId);
    }

    @Deprecated
    public static int getMoraleBonus(int planetId) {
        return PopulationUtilities.getMorale(planetId).getMoraleBonus();
    }

    @Deprecated
    public static float getMaxMorale(int planetId) {
        return PopulationUtilities.getMorale(planetId).getMaxMorale();
    }

    public static BaseResult dismantleColony(int userId, int planetId) {
        try {
            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
            if (pp.getUserId() == userId) {
                pp.setDismantle(!pp.getDismantle());
                playerPlanetDAO.update(pp);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Fehler in dismantleColony: ", e);
            return new BaseResult(e.getMessage(), true);
        }

        return new BaseResult("OK", false);
    }

    @Deprecated
    public static void setVisible(int userId, int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        if (pp.getUserId() == userId) {
            pp.setInvisibleFlag(!pp.getInvisibleFlag());
            playerPlanetDAO.update(pp);
        }
    }

    public static BaseResult changePlanetProperties(Map<String, String[]> parMap, int userId, int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        if (pp.getUserId() != userId) {
            return new BaseResult("No Access", true);
        }

        int categoryId = 0;

        for (final Map.Entry<String, String[]> entry : parMap.entrySet()) {
            String parName = entry.getKey();
            java.lang.System.out.println("parname = " + parName);
            if (parName.startsWith("category")) {
                java.lang.System.out.println("blubb " + parMap.get("category")[0]);
                categoryId = Integer.parseInt(parMap.get("category")[0]);
            }
        }

        if (categoryId == 0) {
            PlanetCategory pc = planetCategoryDAO.findByPlanetId(planetId);
            if (pc != null) {
                planetCategoryDAO.remove(pc);
            }
        } else {
            PlanetCategory pc = planetCategoryDAO.findByPlanetId(planetId);
            if (pc == null) {
                pc = new PlanetCategory();
                pc.setCategoryId(categoryId);
                pc.setPlanetId(planetId);
                planetCategoryDAO.add(pc);
            } else {
                pc.setCategoryId(categoryId);
                planetCategoryDAO.update(pc);
            }
        }
        //Neuladen der Headerdaten da sich die Kategorie geändert hat
        HeaderBuffer.reloadUser(userId);

        try {
            boolean visibleState = false;
            if (parMap.get("visibleState") == null) {
                visibleState = false;
            } else {
                if (((String) parMap.get("visibleState")[0]).equalsIgnoreCase("on")) {
                    visibleState = true;
                }
            }

            pp.setInvisibleFlag(visibleState);
            playerPlanetDAO.update(pp);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Setting of invisble flag failed", e);
            return new BaseResult("Error", true);
        }

        return new BaseResult("Erfolgreich", false);
    }

    public static BaseResult renamePlanSys(Map<String, String[]> parMap, int userId, int planetId, int systemId) {
        BaseResult result = new BaseResult("Done", false);
        String planetName = "";
        String systemName = "";

        for (final Map.Entry<String, String[]> entry : parMap.entrySet()) {
            String parName = entry.getKey();

            if (parName.startsWith("planetname")) {
                planetName = parMap.get("planetname")[0];
            }
            if (parName.startsWith("systemname")) {
                systemName = parMap.get("systemname")[0];
            }
        }

        planetName = planetName.trim();
        systemName = systemName.trim();

        // Planetenname ändern
        if ((planetName.length() > 1) && (planetName.length() <= 20)) {
            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);

            for (PlayerPlanet ppTmp : playerPlanetDAO.findByUserId(userId)) {
                if (ppTmp.getName().equals(planetName) && ppTmp.getPlanetId() != planetId) {
                    log.debug("Returning this");
                    return new BaseResult("Sie besitzen bereits einen Planeten mit diesem Namen", true);
                }

            }

            pp.setName(planetName);
            playerPlanetDAO.update(pp);

        }

        // Systemname ändern
        if (sysNameChangeAllowed(userId, systemId)) {
            System s = systemDAO.findById(systemId);
            if (systemName.trim().toLowerCase().startsWith("system ") && !systemName.trim().equals("System " + s.getId())) {
                return new BaseResult("Namen die mit 'System' starten sind um Verwirrungen zu verhindern gesch�tzt", true);
            }
            s.setName(systemName);
            systemDAO.update(s);
        }

        //Neuladen der Headerdaten da sich der Sstemname geändert hat
        HeaderBuffer.reloadUser(userId);
        return result;
    }

    public static boolean sysNameChangeAllowed(int userId, int systemId) {

        int userWithHighest = userId;
        long highestValue = 0;

        ArrayList<Planet> planets = planetDAO.findBySystemId(systemId);

        for (Planet p : planets) {
            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
            if (pp != null) {
                //If it is homesystem always renameable for owner
                if (pp.isHomeSystem() && pp.getUserId() == userId) {
                    return true;
                } else if (pp.getPopulation() > highestValue) {
                    userWithHighest = pp.getUserId();
                }
            }
        }

        // Check if highest population in system
        if (userWithHighest == userId) {
            return true;
        }

        return false;
    }

    @Deprecated
    public static void changeCategoryId(int userId, int planetId, int categoryId) {
        if (categoryId == 0) {
            PlanetCategory pc = planetCategoryDAO.findByPlanetId(planetId);
            planetCategoryDAO.remove(pc);

        } else {

            PlanetCategory pc = planetCategoryDAO.findByPlanetId(planetId);
            if (pc == null) {
                pc = new PlanetCategory();
                pc.setCategoryId(categoryId);
                pc.setPlanetId(planetId);
                planetCategoryDAO.add(pc);
            } else {
                pc.setCategoryId(categoryId);
                planetCategoryDAO.update(pc);
            }
        }
        //Neuladen der Headerdaten da sich die Kategorie geändert hat
        HeaderBuffer.reloadUser(userId);
    }

    public static void changeInvisibleFlag(int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        if (pp.getInvisibleFlag()) {
            pp.setInvisibleFlag(false);
        } else {
            pp.setInvisibleFlag(true);
        }
    }

    @Deprecated
    public static int getTaxes(int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        return pp.getTax();
    }

    public static void changeTaxes(Map<String, String[]> parMap, int userId, int planetId) {
        for (final Map.Entry<String, String[]> entry : parMap.entrySet()) {
            String parName = entry.getKey();

            if (parName.equalsIgnoreCase("tax")) {

                int value = Integer.parseInt(entry.getValue()[0]);
                changeTaxes(value, userId, planetId);

            }
        }
    }

    public static void changeTaxes(int value, int userId, int planetId) {
        if ((value >= 0) && (value <= 100)) {

            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
            if (pp.getUserId() != userId) {
                return;
            }
            pp.setTax(value);
            playerPlanetDAO.update(pp);
        }
    }

    public static BaseResult updatePriorities(Map<String, String[]> parMap, int planetId) {
        try {
            PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);

            for (final Map.Entry<String, String[]> entry : parMap.entrySet()) {
                String parName = entry.getKey();

                if (parName.equalsIgnoreCase("priorityA")) {
                    int value = Integer.parseInt(entry.getValue()[0]);
                    if ((value >= 0) && (value <= 2)) {
                        pp.setPriorityAgriculture(value);
                    }

                } else if (parName.equalsIgnoreCase("priorityI")) {
                    int value = Integer.parseInt(entry.getValue()[0]);
                    if ((value >= 0) && (value <= 2)) {
                        pp.setPriorityIndustry(value);
                    }

                } else if (parName.equalsIgnoreCase("priorityF")) {
                    int value = Integer.parseInt(entry.getValue()[0]);
                    if ((value >= 0) && (value <= 2)) {
                        pp.setPriorityResearch(value);
                    }
                }
            }
            playerPlanetDAO.update(pp);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in update Priorities", e);
            return new BaseResult(e.getMessage(), true);
        }

        return new BaseResult("OK", false);
    }

    public static synchronized BaseResult changeLimits(Map<String, String[]> parMap, int userId, int planetId) {
        ArrayList<String> errorList = new ArrayList<String>();

        try {
            for (Ressource r : getAllStorableRessources(planetId)) {
                for (final Map.Entry<String, String[]> entry : parMap.entrySet()) {
                    String parName = entry.getKey();
                    if (parName.equalsIgnoreCase("r_prod_" + String.valueOf(r.getId()))) {
                        PlanetRessource pr = planetRessourceDAO.findBy(planetId, r.getId(), EPlanetRessourceType.MINIMUM_PRODUCTION);

                        boolean newEntry = false;
                        if (pr == null) {
                            pr = new PlanetRessource();
                            newEntry = true;
                        }
                        int value = 0;
                        try {
                            value = Integer.parseInt(entry.getValue()[0]);
                        } catch (Exception e) {
                            errorList.add(ML.getMLStr("management_err_nan", userId));
                            return new BaseResult(ML.getMLStr("management_err_nan", userId), true);
                        }
                        if ((value < 0) || (value > 99999999)) {
                            continue;
                        }
                        // stmt.executeUpdate("UPDATE playerplanet SET minIronStorage="+value+" where planetID="+pData.getPlanetId());
                        pr.setQty((long) value);
                        if (newEntry) {
                            pr.setPlanetId(planetId);
                            pr.setRessId(r.getId());
                            pr.setType(EPlanetRessourceType.MINIMUM_PRODUCTION);
                            planetRessourceDAO.add(pr);
                        } else {
                            planetRessourceDAO.update(pr);
                        }
                    } else if (parName.equalsIgnoreCase("r_trans_" + String.valueOf(r.getId()))) {
                        PlanetRessource pr = planetRessourceDAO.findBy(planetId, r.getId(), EPlanetRessourceType.MINIMUM_TRANSPORT);

                        boolean newEntry = false;
                        if (pr == null) {
                            pr = new PlanetRessource();
                            newEntry = true;
                        }
                        int value = 0;
                        try {
                            value = Integer.parseInt(entry.getValue()[0]);
                        } catch (Exception e) {
                            errorList.add(ML.getMLStr("management_err_nan", userId));
                            return new BaseResult(ML.getMLStr("management_err_nan", userId), true);
                        }
                        if ((value < 0) || (value > 99999999)) {
                            continue;
                        }
                        // stmt.executeUpdate("UPDATE playerplanet SET minIronStorage="+value+" where planetID="+pData.getPlanetId());
                        pr.setQty((long) value);
                        if (newEntry) {
                            pr.setPlanetId(planetId);
                            pr.setRessId(r.getId());
                            pr.setType(EPlanetRessourceType.MINIMUM_TRANSPORT);
                            planetRessourceDAO.add(pr);
                        } else {
                            planetRessourceDAO.update(pr);
                        }
                    } else if (parName.equalsIgnoreCase("r_trade_" + String.valueOf(r.getId()))) {
                        PlanetRessource pr = planetRessourceDAO.findBy(planetId, r.getId(), EPlanetRessourceType.MINIMUM_TRADE);

                        boolean newEntry = false;
                        if (pr == null) {
                            pr = new PlanetRessource();
                            newEntry = true;
                        }
                        int value = 0;
                        try {
                            value = Integer.parseInt(entry.getValue()[0]);
                        } catch (Exception e) {
                            errorList.add(ML.getMLStr("management_err_nan", userId));
                            return new BaseResult(ML.getMLStr("management_err_nan", userId), true);
                        }
                        if ((value < 0) || (value > 99999999)) {
                            continue;
                        }
                        // stmt.executeUpdate("UPDATE playerplanet SET minIronStorage="+value+" where planetID="+pData.getPlanetId());
                        pr.setQty((long) value);
                        if (newEntry) {
                            pr.setPlanetId(planetId);
                            pr.setRessId(r.getId());
                            pr.setType(EPlanetRessourceType.MINIMUM_TRADE);
                            planetRessourceDAO.add(pr);
                        } else {
                            planetRessourceDAO.update(pr);
                        }
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in changeLimits: ", e);
            return new BaseResult(e.getMessage(), true);
        }

        return new BaseResult("OK", false);
    }

    public static ArrayList<Integer> getUpgrades(int userId, int planetId) {
        ArrayList<Integer> foundUpgrades = new ArrayList<Integer>();

        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        Planet p = planetDAO.findById(planetId);

        // HOMEWORLD
        if (pp.isHomeSystem()) {
            foundUpgrades.add(GameConstants.UPDATE_HOMEWORLD);
        }

        // LOCAL ADMINISTRATION
        if (planetConstructionDAO.findBy(planetId, Construction.ID_LOCALADMINISTRATION) != null) {
            foundUpgrades.add(GameConstants.UPDATE_LOCAL_ADMINISTRATION);
        }

        // PLANETARY ADMINISTRATION
        if (planetConstructionDAO.findBy(planetId, Construction.ID_PLANETADMINISTRATION) != null) {
            foundUpgrades.add(GameConstants.UPDATE_PLANETARY_ADMINISTRATION);
        }

        // PLANETARY GOVERNMENT
        if (planetConstructionDAO.findBy(planetId, Construction.ID_PLANETGOVERNMENT) != null) {
            foundUpgrades.add(GameConstants.UPDATE_PLANETARY_GOVERNMENT);
        }

        if (foundUpgrades.contains(GameConstants.UPDATE_PLANETARY_GOVERNMENT)) {
            foundUpgrades.remove(new Integer(GameConstants.UPDATE_PLANETARY_ADMINISTRATION));
            foundUpgrades.remove(new Integer(GameConstants.UPDATE_LOCAL_ADMINISTRATION));
        } else if (foundUpgrades.contains(GameConstants.UPDATE_PLANETARY_ADMINISTRATION)) {
            foundUpgrades.remove(new Integer(GameConstants.UPDATE_LOCAL_ADMINISTRATION));
        }

        // DESINTEGRATOR
        if (ResearchUtilities.isResearchResearched(106, userId)) {
            foundUpgrades.add(GameConstants.UPDATE_MINING_DESINTEGRATOR);
        }

        if (p.getLandType().equalsIgnoreCase(Planet.LANDTYPE_J)) {
            foundUpgrades.add(GameConstants.UPDATE_VULCAN_ACTIVITY);
        }

        return foundUpgrades;
    }

    public static ArrayList<Ressource> getMinStockRessources(int planetId, ProductionResult prodRes) {
        ArrayList<Ressource> ressList = new ArrayList<Ressource>();

        HashSet<Integer> coveredRess = new HashSet<Integer>();

        ArrayList<TradeRoute> trList = trDAO.findByEndPlanet(planetId, ETradeRouteType.TRANSPORT);
        trList.addAll(trDAO.findByEndPlanet(planetId, ETradeRouteType.TRANSPORT));

        for (TradeRoute tr : trList) {
            TradeRouteExt tre = new TradeRouteExt(tr);

            for (TradeRouteDetail trd : tre.getRouteDetails()) {
                coveredRess.add(trd.getRessId());
            }
        }

        for (Ressource r : ManagementService.getAllStorableRessources(planetId)) {
            if ((prodRes.getRessStock(r.getId()) > 0)
                    || (prodRes.getRessConsumption(r.getId()) > 0)
                    || (prodRes.getRessProduction(r.getId()) > 0)
                    || coveredRess.contains(r.getId())) {
                ressList.add(r);
            }
        }

        return ressList;
    }

    public static boolean abadonable(int userId, int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        if (pp.getUserId() != userId) {
            return false;
        }
        if (pp.isHomeSystem()) {
            return false;
        }
        if (pp.getPopulation() <= 100L && !pp.isHomeSystem()) {
            return true;
        } else {
            return false;
        }
    }

    public static void abadonColony(int userId, int planetId) {
        if (abadonable(userId, planetId)) {
            DestructionUtilities.destroyPlayerPlanet(planetId);
        }
    }
    
    public static void getSpecialTradeInformation(int planetId) {
        
    }
}
