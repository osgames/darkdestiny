/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.construction.restriction;

import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.enumeration.ERestrictionReason;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.ConstructionCheckResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.ResearchUtilities;
import at.darkdestiny.util.DebugBuffer;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class ScientificAcademy extends Service implements ConstructionInterface {
    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();
    
    @Override
    public ConstructionCheckResult onConstructionCheck(PlayerPlanet pp) {
        return new ConstructionCheckResult(true,1);
    }

    @Override
    public void onConstruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public void onFinishing(PlayerPlanet pp) {
        DebugBuffer.trace("Calling onFinishing");
        
        // Generate a message offering all available researches which can be boosted
        ResearchUtilities.generateFreeResearchMessage(pp.getUserId());
    }

    @Override
    public void onDeconstruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public void onDestruction(PlayerPlanet pp) throws Exception {

    }

    @Override
    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }


    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    @Override
    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }        
}
