/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.HashMap;
import java.util.Locale;

/**
 *
 * @author Eobane
 */
public class ML_NEW extends Service {

    private static HashMap<Integer, Locale> locales;
    private static final String languageFolder = "";
    public static final String DEFAULT_PROPERTIES_FILE = "language/lang";

    public static Language getLanguageByUserId(int userId) {
        return Service.languageDAO.findBy(Service.userDAO.findById(userId).getLocale());
    }

    //Main method to get a ml value
    public static String getMLStr(String propertiesFile, String key, Locale l, Object... params) {


        if (key == null || key.equals("")) {
            return "";
        }
        String data = key;
        String propertiesFileName = DEFAULT_PROPERTIES_FILE;
        if (propertiesFile != null) {
            propertiesFileName = propertiesFile;
        }
        //System.out.println("Constant {" + key + "] Locale [" + l.getCountry() + " " + l.getLanguage() + "] par length" + params.length);
        try {
            if (data.equals(key)) {
                data = java.util.ResourceBundle.getBundle(propertiesFileName, l).getString(key);
            }
        } catch (Exception e) {
            System.err.println("Error beim holen eines Multilanguagekeys: aus dem propertiesfile : " + propertiesFileName + " | " + e + " key : " + key);


            return "e:" + data;
        }

        int counter = 0;
        for (Object param : params) {
            counter++;
            data = data.replace("%" + counter, param.toString().replace("[", "").replace("]", ""));
        }

        return data;
        //return data;
    }

    public static String getMLStr(String propertiesFile, String key, int userId) {

        Locale l = locales.get(userId);
        return getMLStr(propertiesFile, key, l);
    }

    public static String getMLStr(String key, Locale locale, Object... params) {

        return getMLStr(null, key, locale, params);

    }

    public static String getMLStr(String key, Locale l) {
        return getMLStr("lang", l, key);
    }

    public static String getMLStr(String propertiesFile, Locale l, String key, String params) {
        return getMLStr(propertiesFile, l, key, params);
    }

    public static void removeLocale(int userId) {
        try {
            locales.remove(userId);
        } catch (Exception e) {
        }
    }

    public static Locale getLocale(int userId) {
        if (locales == null) {

            locales = new HashMap<Integer, Locale>();

        }
        Locale l = locales.get(userId);
        if (l == null) {
            loadLocaleForUser(userId);
        }
        return locales.get(userId);

    }

    private static void loadLocaleForUser(int userId) {
        //Holen des Locales vom User
        String userLocale = Service.userDAO.findById(userId).getLocale();
        //Suchen der dazugeh?rigen Sprache
        Language tempLang = Service.languageDAO.findBy(userLocale);
        //Wenn es eine MasterId gibt wird diese Sprache verwendet
        if (!tempLang.getId().equals(tempLang.getMasterLanguageId())) {
            tempLang = Service.languageDAO.findById(tempLang.getMasterLanguageId());
            userLocale = tempLang.getLanguage() + "_" + tempLang.getCountry();
        }

        String language = userLocale.substring(0, userLocale.indexOf("_"));
        String country = userLocale.substring(userLocale.indexOf("_") + 1, userLocale.length());

        Locale l = new Locale(language, country);

        locales.put(userId, l);
    }

    public static String getLocaleString(int userId) {
        Locale l = locales.get(userId);

        return l.getLanguage() + "_" + l.getCountry();
    }

    public static String getMLStr(String key, int userId) {
        Object[] params = new Object[1];
        params[0] = "";
        return getMLStr(key, userId, params);
    }

    public static String getMLStr(String key, int userId, Object... params) {
        String data = key;
        boolean debug = false;
        Locale l = null;
        try {
            if (locales == null) {
                locales = new HashMap<Integer, Locale>();
            }
            l = locales.get(userId);
            if (debug) {
                DebugBuffer.addLine(DebugLevel.WARNING, "MULTILANGUAGE - " + "l : " + l);
            }
            if (l == null) {
                loadLocaleForUser(userId);
                l = locales.get(userId);
            }

            return getMLStr(data, key, l, params);
        } catch (Exception e) {
            System.err.println("Error beim holen eines Multilanguagekeys: " + e + " key : " + key + " for User : " + userId + " Locale : " + l);
            if (l != null) {
                System.err.println("locale-coutner" + l.getCountry() + " locale-language : " + l.getLanguage());
            }
            // e.printStackTrace();
            return "e:" + data;
        }
    }

    public static Language getLanguageByLocale(Locale locale) {
        Language l = new Language();
        //find by detailed
        l.setLanguage(locale.getLanguage());
        l.setCountry(locale.getCountry());
        l = Service.languageDAO.get(l);
        if (l == null) {
            l = new Language();
            l.setLanguage(locale.getLanguage());
            Language result = null;
            for (Language lang : Service.languageDAO.find(l)) {
                if (result == null) {
                    result = lang;
                } else {
                    if (lang.getMasterLanguageId().equals(lang.getId())) {
                        return lang;
                    }
                }
            }
            if (result != null) {
                return result;
            }
            //Unknown language take english
            l = new Language();
            l.setLanguage("en");
            l.setCountry("US");
            return Service.languageDAO.get(l);
        }
        return l;
    }
}
