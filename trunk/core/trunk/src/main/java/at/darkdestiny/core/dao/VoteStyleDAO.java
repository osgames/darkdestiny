/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.VoteStyle;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Bullet
 */
public class VoteStyleDAO extends ReadWriteTable<VoteStyle> implements GenericDAO {
    public VoteStyle getByVoteId(int voteId) {
        VoteStyle vs = new VoteStyle();
        vs.setVoteId(voteId);
        return get(vs);
    }
}
