/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.model.ProductionOrder;

/**
 *
 * @author Stefan
 */
public class GroundTroopAbortOrder extends AbortOrder {
    public GroundTroopAbortOrder(ProductionOrder po, int count, int userId, int planetId) {
        super(po,count,userId, planetId);
    }
}
