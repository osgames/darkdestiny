 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view.menu;

/**
 *
 * Datenklasse f?r die Klasse StatusBarData.java
 * Daten zum Erstellen der StatusBar
 * 
 * @author Dreloc
 */
public class ProductionUsageEntity {

    private String nameNow = "";
    private String nameLater = "";
    private long productionNow = 0;
    private long usageNow = 0;
    private long productionLater = 0;
    private long usageLater = 0;
 
    public ProductionUsageEntity(){
        
    }
    
    public String getNameNow() {
        return nameNow;
    }

    public void setNameNow(String nameNow) {
        this.nameNow = nameNow;
    }

    public String getNameLater() {
        return nameLater;
    }

    public void setNameLater(String nameLater) {
        this.nameLater = nameLater;
    }

    public long getProductionNow() {
        return productionNow;
    }

    public void setProductionNow(long productionNow) {
        this.productionNow = productionNow;
    }

    public long getUsageNow() {
        return usageNow;
    }

    public void setUsageNow(long usageNow) {
        this.usageNow = usageNow;
    }

    public long getProductionLater() {
        return productionLater;
    }

    public void setProductionLater(long productionLater) {
        this.productionLater = productionLater;
    }

    public long getUsageLater() {
        return usageLater;
    }

    public void setUsageLater(long usageLater) {
        this.usageLater = usageLater;
    }
    


  
    
    
}
