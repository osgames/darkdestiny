/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.update;

import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.model.*;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Stefan
 */
public class UpdaterDataSet {
    private final Map<Integer,EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>>> planetRess =
        Collections.synchronizedMap(new HashMap<Integer,EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>>>());
    private final Map<Integer,HashMap<Integer,Integer>> maxStorage =
        Collections.synchronizedMap(new HashMap<Integer,HashMap<Integer,Integer>>());    
    private final Map<Integer,UserData> userData = Collections.synchronizedMap(new HashMap<Integer,UserData>());    
    private final Map<Integer,EnumMap<EActionType,HashSet<Action>>> actionData = Collections.synchronizedMap(new HashMap<Integer,EnumMap<EActionType,HashSet<Action>>>());    
    private final Map<Integer,HashMap<Integer,PlanetLoyality>> planetLoyalityData = Collections.synchronizedMap(new HashMap<Integer,HashMap<Integer,PlanetLoyality>>());    
    /*
    private final Map<Integer,EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>>> planetRess =
        new ConcurrentHashMap<Integer,EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>>>();    
    private final Map<Integer,HashMap<Integer,Integer>> maxStorage =
        new ConcurrentHashMap<Integer,HashMap<Integer,Integer>>();    
    private final Map<Integer,UserData> userData = new ConcurrentHashMap<Integer,UserData>();
    private final Map<Integer,EnumMap<EActionType,HashSet<Action>>> actionData = new ConcurrentHashMap<Integer,EnumMap<EActionType,HashSet<Action>>>();
    private final Map<Integer,HashMap<Integer,PlanetLoyality>> planetLoyalityData = new ConcurrentHashMap<Integer,HashMap<Integer,PlanetLoyality>>();
    */
    private PlanetRessourceDAO prDAO = (PlanetRessourceDAO)DAOFactory.get(PlanetRessourceDAO.class);

    public UpdaterDataSet(ArrayList<PlayerPlanet> ppList, ArrayList<PlanetRessource> prList, ArrayList<UserData> udList,
            ArrayList<Action> aList) {
        HashSet<Integer> ppIdList = new HashSet<Integer>();

        for (PlayerPlanet pp : ppList) {
            ppIdList.add(pp.getPlanetId());
        }

        for (PlanetRessource pr : prList) {
            if (!ppIdList.contains(pr.getPlanetId())) continue;

            if (planetRess.containsKey(pr.getPlanetId())) {
                EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> typeMap = planetRess.get(pr.getPlanetId());
                if (typeMap.containsKey(pr.getType())) {
                    HashMap<Integer,PlanetRessource> ressMap = typeMap.get(pr.getType());
                    ressMap.put(pr.getRessId(), pr);
                } else {
                    HashMap<Integer,PlanetRessource> ressMap = new HashMap<Integer,PlanetRessource>();
                    typeMap.put(pr.getType(), ressMap);
                    ressMap.put(pr.getRessId(), pr);
                }
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding entry to ressMap (Ressource: " + pr.getRessId() + " Type: " + pr.getType() + " PlanetId: " + pr.getPlanetId());
            } else {
                EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> typeMap =
                    new EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>>(EPlanetRessourceType.class);
                HashMap<Integer,PlanetRessource> ressMap = new HashMap<Integer,PlanetRessource>();
                planetRess.put(pr.getPlanetId(), typeMap);
                typeMap.put(pr.getType(), ressMap);
                ressMap.put(pr.getRessId(), pr);
                // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding entry to ressMap (Ressource: " + pr.getRessId() + " Type: " + pr.getType() + " PlanetId: " + pr.getPlanetId());
            }
        }

        for (UserData ud : udList) {
            userData.put(ud.getUserId(), ud);
        }

        for (Action a : aList) {
            if (actionData.containsKey(a.getUserId())) {
                EnumMap<EActionType,HashSet<Action>> typeData = actionData.get(a.getUserId());
                if (typeData.containsKey(a.getType())) {
                    typeData.get(a.getType()).add(a);
                } else {
                    HashSet<Action> aTmp = new HashSet<Action>();
                    aTmp.add(a);
                    typeData.put(a.getType(), aTmp);
                }
            } else {
                EnumMap<EActionType,HashSet<Action>> typeData = new EnumMap<EActionType,HashSet<Action>>(EActionType.class);
                HashSet<Action> aTmp = new HashSet<Action>();
                aTmp.add(a);
                typeData.put(a.getType(), aTmp);
                actionData.put(a.getUserId(), typeData);
            }
        }
    }

    public UserData getUserDataEntry(int userId) {
        return userData.get(userId);
    }

    public HashSet<Action> getActionEntries(int userId, EActionType type) {
        if (actionData.containsKey(userId)) {
            EnumMap<EActionType,HashSet<Action>> typeData = actionData.get(userId);
            if (typeData.containsKey(type)) {
                return typeData.get(type);
            }
        }

        return new HashSet<Action>();
    }

    public HashSet<Action> getActionEntries(EActionType type) {
        HashSet<Action> results = new HashSet<Action>();

        for (Map.Entry<Integer,EnumMap<EActionType,HashSet<Action>>> uEntry : actionData.entrySet()) {
            EnumMap<EActionType,HashSet<Action>> typeData = uEntry.getValue();
            if (typeData.containsKey(type)) {
                results.addAll(typeData.get(type));
            }
        }

        return results;
    }

    public void removeActionEntry(int userId, EActionType type, Action a) {
        actionData.get(userId).get(type).remove(a);
    }

    public void addActionEntry(Action a) {
        if (actionData.containsKey(a.getUserId())) {
            EnumMap<EActionType,HashSet<Action>> typeData = actionData.get(a.getUserId());
            if (typeData.containsKey(a.getType())) {
                typeData.get(a.getType()).add(a);
            } else {
                HashSet<Action> aTmp = new HashSet<Action>();
                aTmp.add(a);
                typeData.put(a.getType(), aTmp);
            }
        } else {
            EnumMap<EActionType,HashSet<Action>> typeData = new EnumMap<EActionType,HashSet<Action>>(EActionType.class);
            HashSet<Action> aTmp = new HashSet<Action>();
            aTmp.add(a);
            typeData.put(a.getType(), aTmp);
            actionData.put(a.getUserId(), typeData);
        }
    }

    public PlanetRessource getPlanetRessEntry(int planetId, EPlanetRessourceType typeId, int ressId) {
        if (planetRess.containsKey(planetId)) {
            EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> typeMap = planetRess.get(planetId);
            if (typeMap.containsKey(typeId)) {
                HashMap<Integer,PlanetRessource> ressMap = typeMap.get(typeId);
                return ressMap.get(ressId);
            }
        }

        return null;
    }

    public ArrayList<PlanetRessource> getRessEntriesForPlanet(int planetId) {
        ArrayList<PlanetRessource> ressList = new ArrayList<PlanetRessource>();

        if (planetRess.containsKey(planetId)) {
            EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> typeMap = planetRess.get(planetId);
            for (HashMap<Integer,PlanetRessource> ressMap : typeMap.values()) {
                ressList.addAll(ressMap.values());
            }
        }

        return ressList;
    }

    public void removePlanetRessEntry(int planetId, EPlanetRessourceType typeId, int ressId) {
        if (planetRess.containsKey(planetId)) {
            EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> typeMap = planetRess.get(planetId);
            if (typeMap.containsKey(typeId)) {
                HashMap<Integer,PlanetRessource> ressMap = typeMap.get(typeId);
                if (ressMap.get(ressId) != null) {
                    prDAO.remove(ressMap.get(ressId));
                    ressMap.remove(ressId);
                }
            }
        }

    }

    public void setMaxStorage(int planetId, int ressId, int qty) {
        synchronized(maxStorage) {
            if (maxStorage.containsKey(planetId)) {
                maxStorage.get(planetId).put(ressId, qty);
            } else {
                HashMap<Integer,Integer> ressMap = new HashMap<Integer,Integer>();
                ressMap.put(ressId,qty);
                maxStorage.put(planetId, ressMap);
            }
        }
    }

    public int getMaxStorage(int planetId, int ressId) {
        if (maxStorage.containsKey(planetId)) {
            if (maxStorage.get(planetId).containsKey(ressId)) {
            // log.debug("returning");
                return maxStorage.get(planetId).get(ressId);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public PlanetRessource getPlanetStorageEntry(int planetId, int ressId) {
        if (planetRess.containsKey(planetId)) {
            EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> typeMap = planetRess.get(planetId);
            if (typeMap.containsKey(EPlanetRessourceType.INSTORAGE)) {
                HashMap<Integer,PlanetRessource> ressMap = typeMap.get(EPlanetRessourceType.INSTORAGE);
                return ressMap.get(ressId);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public PlanetRessource createNewStorageRessEntry(int planetId, int ressId, long qty) {
        int maxValue = getMaxStorage(planetId,ressId);
        if (qty > maxValue) qty = maxValue;

        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setRessId(ressId);
        pr.setType(EPlanetRessourceType.INSTORAGE);
        pr.setQty(qty);
        pr = prDAO.add(pr);

        if (planetRess.containsKey(pr.getPlanetId())) {
            EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> typeMap = planetRess.get(pr.getPlanetId());
            if (typeMap.containsKey(pr.getType())) {
                HashMap<Integer,PlanetRessource> ressMap = typeMap.get(pr.getType());
                ressMap.put(pr.getRessId(), pr);
            } else {
                HashMap<Integer,PlanetRessource> ressMap = new HashMap<Integer,PlanetRessource>();
                typeMap.put(pr.getType(), ressMap);
                ressMap.put(pr.getRessId(), pr);
            }
            // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding entry to ressMap (Ressource: " + pr.getRessId() + " Type: " + pr.getType() + " PlanetId: " + pr.getPlanetId());
        } else {
            EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> typeMap =
                new EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>>(EPlanetRessourceType.class);
            HashMap<Integer,PlanetRessource> ressMap = new HashMap<Integer,PlanetRessource>();
            planetRess.put(pr.getPlanetId(), typeMap);
            typeMap.put(pr.getType(), ressMap);
            ressMap.put(pr.getRessId(), pr);
            // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Adding entry to ressMap (Ressource: " + pr.getRessId() + " Type: " + pr.getType() + " PlanetId: " + pr.getPlanetId());
        }

        return pr;
    }

    public ArrayList<PlanetRessource> getUpdatedPlanetRess() {
        ArrayList<PlanetRessource> res = new ArrayList<PlanetRessource>();
        // HashMap<Integer,HashMap<Integer,HashMap<Integer,PlanetRessource>>>
        for (EnumMap<EPlanetRessourceType,HashMap<Integer,PlanetRessource>> entry : planetRess.values()) {
            for (HashMap<Integer,PlanetRessource> entry2 : entry.values()) {
                res.addAll(entry2.values());
            }
        }

        // DebugBuffer.addLine(DebugLevel.UNKNOWN, "Res Count: " + res.size());
        return res;
    }

    public ArrayList<UserData> getUpdatedUserData() {
        ArrayList<UserData> res = new ArrayList<UserData>();
        res.addAll(userData.values());
        return res;
    }

    public PlanetLoyality getLoyalityData(int planetId, int userId) {
        if (planetLoyalityData.get(planetId) == null) {
            return null;
        } else {
            return planetLoyalityData.get(planetId).get(userId);
        }
    }

    public ArrayList<PlanetLoyality> getUpdatedLoyalityData() {
        ArrayList<PlanetLoyality> data = new ArrayList<PlanetLoyality>();

        for (Map.Entry<Integer,HashMap<Integer,PlanetLoyality>> allLoyEntriesPlanet : planetLoyalityData.entrySet()) {
            data.addAll(allLoyEntriesPlanet.getValue().values());
        }

        return data;
    }

    public void addLoyalityEntry(PlanetLoyality pl) {
        HashMap<Integer,PlanetLoyality> planetEntries = planetLoyalityData.get(pl.getPlanetId());

        if (planetEntries == null) {
            planetEntries = new HashMap<Integer,PlanetLoyality>();
            planetEntries.put(pl.getUserId(), pl);
            planetLoyalityData.put(pl.getPlanetId(), planetEntries);
        } else {
            planetEntries.put(pl.getUserId(), pl);
        }
    }

    public void removeLoyalityEntry(PlanetLoyality pl) {
        HashMap<Integer,PlanetLoyality> planetEntries = planetLoyalityData.get(pl.getPlanetId());

        if (planetEntries != null) {
            planetEntries.remove(pl.getUserId());
        }
    }
}
