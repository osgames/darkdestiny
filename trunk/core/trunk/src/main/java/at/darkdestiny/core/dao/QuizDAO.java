/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Quiz;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class QuizDAO extends ReadWriteTable<Quiz> implements GenericDAO  {

    public Quiz findById(Integer id) {
        Quiz qSearch = new Quiz();
        qSearch.setId(id);
        return (Quiz)get(qSearch);
    }


}
