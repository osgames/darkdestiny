/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.core.ships.ShipScrapCost;
import at.darkdestiny.core.ships.ShipUpgradeCost;
import at.darkdestiny.core.view.ShipModifyEntry;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;

/**
 *
 * @author Stefan
 */
public class DefenseUtilities {

    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);

    public static boolean canBeUpgraded(Model from, Model to, int userId, int planetId, int count) {
        ShipDesign SDFrom = (ShipDesign) from;
        ShipDesign SDTo = (ShipDesign) to;

        if (SDFrom.getUserId() != userId) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 1 (" + userId + "<>" + SDFrom.getUserId() + ")");
            return false;
        }

        if (!SDFrom.getUserId().equals(SDTo.getUserId())) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 2");
            return false;
        }

        ShipModifyEntry sme = ShipUtilities.getShipModifyEntry(SDFrom, planetId);
        if (sme.getCount() < count) {
            return false;
        }

        ShipDesignExt SDEFrom = new ShipDesignExt(SDFrom.getId());
        ShipDesignExt SDETo = new ShipDesignExt(SDTo.getId());

        ShipUpgradeCost suc = SDEFrom.getShipUpgradeCost(SDETo, count);

        MutableRessourcesEntry mre = new MutableRessourcesEntry(userId, planetId);
        RessourcesEntry re = new RessourcesEntry(suc.getRess());
        mre.subtractRess(re, count);

        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getQty() < 0) {
                DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 4 With ress " + rae.getRessId() + " >> " + rae.getQty());
                return false;
            }
        }

        return true;
    }

    public static boolean canBeScrapped(Model ship, int userId, int planetId, int count) {
        ShipDesign sd = (ShipDesign) ship;

        if (sd.getUserId() != userId) {
            return false;
        }

        ShipModifyEntry sme = ShipUtilities.getShipModifyEntry(sd, planetId);
        if (sme.getCount() < count) {
            return false;
        }

        UserData ud = udDAO.findByUserId(userId);
        ShipDesignExt sde = new ShipDesignExt(sd.getId());
        ShipScrapCost ssc = sde.getShipScrapCost(count);

        if (ud.getCredits() < ssc.getRess(Ressource.CREDITS)) {
            return false;
        }

        return true;
    }
}
