/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.exceptions;

/**
 *
 * @author Stefan
 */
public class UpdaterException extends RuntimeException {
    private final Exception underlying;

    public UpdaterException(String name, Exception underlying) {
        super(name);
        this.underlying = underlying;
    }

    /**
     * @return the underlying
     */
    public Exception getUnderlying() {
        return underlying;
    }
}
