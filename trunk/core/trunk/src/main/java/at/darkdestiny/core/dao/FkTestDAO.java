/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.FkTest;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Aion
 */
public class FkTestDAO extends ReadWriteTable<FkTest> implements GenericDAO {
    public FkTest findById(int id){
        FkTest f = new FkTest();
        f.setId(id);
        return (FkTest)get(f);
    }
}
