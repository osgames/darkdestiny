/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.utilities;

import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.result.GrowthResult;
import at.darkdestiny.core.result.MoraleResult;
import at.darkdestiny.core.service.Service;

/**
 *
 * @author Bullet
 */
public class UpdateUtilities extends Service{


    public static void calc1TickPopulation(int planetId) {


        // Check for food shortage
        // DebugBuffer.addLine("Start calculations food="+(rc.getFood() + rc.getProdCons(GameConstants.RES_FOOD)[GameConstants.RES_INCREASE])+" morale="+morale+" growth="+growth);

        // DebugBuffer.addLine("Calculated Food coverage = " + foodcoverage);

        // DebugBuffer.addLine("Calculated Food coverage = " + foodcoverage + " MoraleChange=" + changevalueMorale);

        // DebugBuffer.addLine("ChangeValueMorale="+changevalueMorale);
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        int morale = pp.getMoral();
        MoraleResult mr = PopulationUtilities.getMorale(planetId);
        if (mr.getEstimatedMorale() > morale) {
            morale++;
        } else if (mr.getEstimatedMorale() < morale) {
            morale -= (int) Math.ceil((morale - mr.getEstimatedMorale()) / 10d);
        }

        // DebugBuffer.addLine("maxMorale="+maxMorale);
        if (morale > (int) mr.getMaxMorale()) {
            morale = (int) mr.getMaxMorale();
        }
        if (morale < 0) {
            morale = 0;
        }

        pp.setMoral(morale);

        // DebugBuffer.addLine("ChangeValueGrowth="+changevalueGrowth);
        float oldgrowth = pp.getGrowth();
        GrowthResult gr = PopulationUtilities.getGrowth(planetId);
        float growth = pp.getGrowth();

        growth += gr.getChangevalueGrowth();
        // DebugBuffer.addLine("gcv="+changevalueGrowth);
        if (growth > gr.getMaxGrowth()) {
            growth = gr.getMaxGrowth();
        }
        if (growth < gr.getMinGrowth()) {
            growth = oldgrowth + 1;
            if (growth > gr.getMinGrowth()) {
                growth = gr.getMinGrowth();
            }
        }
        pp.setGrowth(growth);
        // DebugBuffer.addLine("PopulationChange="+(int)(((float)population / (float)100) * growth / 360));

        long population = pp.getPopulation();
        population += (int) (((float) population / (float) 100) * growth / 360);
        if (population < 0) {
            population = 0;
        }
        long maxPopulation = PopulationUtilities.getMaxPopulation(planetId);
        if (population > maxPopulation) {
            population = maxPopulation;
        }

        pp.setPopulation(population);

        pp = playerPlanetDAO.update(pp);
        UserData ud = userDataDAO.findByUserId(pp.getUserId());
        ud.setCredits(ud.getCredits() + PopulationUtilities.getTaxIncome(planetId));

        userDataDAO.update(ud);

    }
}
