/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.webservice;

/**
 *
 * @author Admin
 */
public class WebData {
    
    private static int chatUserCount = 0;

    /**
     * @return the chatUserCount
     */
    public static int getChatUserCount() {
        return chatUserCount;
    }

    /**
     * @param chatUserCount the chatUserCount to set
     */
    public static void setChatUserCount(int chatUserCount_) {
        chatUserCount = chatUserCount_;
    }
}
