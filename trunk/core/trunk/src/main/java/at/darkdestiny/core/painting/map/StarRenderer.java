/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.enumeration.EStarMapObjectType;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.util.DebugBuffer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import javax.imageio.ImageIO;

/**
 *
 * @author Stefan
 */
public class StarRenderer implements IModifyImageFunction { 
    private int width;
    private int height;
    private final int userId;
    private final StarMapDataSource dataSource;
    private final AbsoluteCoordinate topLeft;
    private final AbsoluteCoordinate bottomRight;
    private final StarMapViewCoordinate smvc;
    private final String imageLocation;
    
    private static Image greyStar = null;
    private static Image yellowStar = null;
    private static Image ownStar = null;
    private static Image homeStar = null;
    
    public StarRenderer(int userId, StarMapViewCoordinate smvc, IMapDataSource imds, String imageLocation) {
        this.userId = userId;
        this.topLeft = smvc.getTopLeft();
        this.smvc = smvc;
        this.bottomRight = smvc.getBottomRight();
        this.dataSource = (StarMapDataSource)imds;
        this.imageLocation = imageLocation;                
        
        System.out.println("Image path: " + imageLocation);
    }
    
    @Override
    public void run(Graphics graphics) {
        System.out.println("RENDER STAR SYSTEMS");
        
        try {
            if (greyStar == null) greyStar = ImageIO.read(new File(imageLocation + "graystar.gif"));            
            if (yellowStar == null) yellowStar = ImageIO.read(new File(imageLocation + "star_000.gif"));   
            if (ownStar == null) ownStar = ImageIO.read(new File(imageLocation + "star_001.gif"));   
            if (homeStar == null) homeStar = ImageIO.read(new File(imageLocation + "mainstar.gif"));               
        } catch (Exception e) {
            DebugBuffer.error("Did not find picture: ",e);
        }
        
        Graphics2D g2d = (Graphics2D)graphics;                
        int picSize = yellowStar.getWidth(null);
        picSize = (int)Math.ceil(AreaMapContainer.getZoomImageAdjust(smvc.getZoom()) * picSize);
        
        for (SMSystem currSys : dataSource.getStarSystems()) {
            if ((currSys.getX() > topLeft.getX()) && 
                (currSys.getX() < bottomRight.getX()) && 
                (currSys.getY() > topLeft.getY()) &&
                (currSys.getY() < bottomRight.getY())) {
                
                int circleSize = picSize + (currSys.getColorLayers() - 1) * 6;
                circleSize = (int)Math.round(AreaMapContainer.getZoomImageAdjust(smvc.getZoom()) * circleSize);
                // System.out.println("DRAW: " + currSys.getId() + " WITH COLOR LAYERS: " + currSys.getColorLayers());
                
                int drawX = (int)Math.round((currSys.getX() - topLeft.getX()) * smvc.getZoom()) - 1;
                int drawY = (int)Math.round((currSys.getY() - topLeft.getY()) * smvc.getZoom()) - 1;
                
                StarSystem ss = new StarSystem(currSys.getId(),drawX,drawY);
                ss.setPayload(currSys);
                ss.setName(currSys.getName());
                AreaMapContainer.addToObjectMap(userId, drawX, drawY, ss);
                
                if (currSys.getSysStatus() == 0) {                    
                    if (currSys.isEnemy()) {
                        g2d.setColor(Color.RED);
                        g2d.fillOval(drawX - (int)Math.floor(circleSize / 2d), drawY - (int)Math.floor(circleSize / 2d), circleSize, circleSize);                        
                        circleSize -= 6;
                    }
                    if (currSys.isNeutral()) {
                        g2d.setColor(Color.YELLOW);
                        g2d.fillOval(drawX - (int)Math.floor(circleSize / 2d), drawY - (int)Math.floor(circleSize / 2d), circleSize, circleSize);                        
                        circleSize -= 6;                        
                    }
                    if (currSys.isNAP()) {
                        g2d.setColor(new Color(153,204,255));
                        g2d.fillOval(drawX - (int)Math.floor(circleSize / 2d), drawY - (int)Math.floor(circleSize / 2d), circleSize, circleSize);                        
                        circleSize -= 6;
                    }
                    if (currSys.isAllied()) {
                        g2d.setColor(Color.BLUE);
                        g2d.fillOval(drawX - (int)Math.floor(circleSize / 2d), drawY - (int)Math.floor(circleSize / 2d), circleSize, circleSize);                        
                        circleSize -= 6;
                    }                    
                    if (currSys.isOwn()) {
                        g2d.setColor(Color.GREEN.brighter());
                        g2d.fillOval(drawX - (int)Math.floor(circleSize / 2d), drawY - (int)Math.floor(circleSize / 2d), circleSize, circleSize);                        
                        circleSize -= 6;
                    }
                }
            }            
        }
        
        for (SMSystem currSys : dataSource.getStarSystems()) {
            // System.out.println("CHECK: " + currSys.getX() + ":" + currSys.getY() + " vs. + [" + topLeft.getX() + "-" + bottomRight.getX() + "]:[" + topLeft.getY() + "-" + bottomRight.getY() + "]");
            g2d.setColor(Color.WHITE);
            
            int drawX = (int)Math.round((currSys.getX() - topLeft.getX()) * smvc.getZoom());
            int drawY = (int)Math.round((currSys.getY() - topLeft.getY()) * smvc.getZoom());            
            
            if ((currSys.getX() > topLeft.getX()) && 
                (currSys.getX() < bottomRight.getX()) && 
                (currSys.getY() > topLeft.getY()) &&
                (currSys.getY() < bottomRight.getY())) {
                // System.out.println("Render System " + currSys.getId() + "["+currSys.getSysStatus()+"]");
                
                if (currSys.getSysStatus() == -1) {
                    // System.out.println("Render gray ["+greyStar+"]");                    
                    g2d.drawImage(greyStar, drawX - (int)Math.floor(picSize / 2d), drawY - (int)Math.floor(picSize / 2d), picSize, picSize, null);                    
                } else if (currSys.getSysStatus() == 0) {
                    if (currSys.isHomeSystem()) {
                        g2d.drawImage(homeStar, drawX - (int)Math.floor(picSize / 2d), drawY - (int)Math.floor(picSize / 2d), picSize, picSize, null);    
                    } else {
                    // System.out.println("Render yellow ["+yellowStar+"]");
                        g2d.drawImage(yellowStar, drawX - (int)Math.floor(picSize / 2d), drawY - (int)Math.floor(picSize / 2d), picSize, picSize, null);    
                    }
                }                
                
                // g2d.fillOval(currSys.getX() - topLeft.getX(), currSys.getY() - topLeft.getY(), 2, 2);
            }
        }
    }    
    
    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }
    
    @Override
    public String createMapEntry() {
        return "";
    }            
}
