/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

/**
 *
 * @author Stefan
 */
public class SMPlanet {
    private boolean homePlanet = false;
    private int diplomacyId = 0;
    private String name;
    private String owner;
    private int diameter;
    private String type;
    private final int id;
    
    public SMPlanet(int id) {
        this.id = id;
    }

    /**
     * @return the homePlanet
     */
    public boolean isHomePlanet() {
        return homePlanet;
    }

    /**
     * @param homePlanet the homePlanet to set
     */
    public void setHomePlanet(boolean homePlanet) {
        this.homePlanet = homePlanet;
    }

    /**
     * @return the diplomacyId
     */
    public int getDiplomacyId() {
        return diplomacyId;
    }

    /**
     * @param diplomacyId the diplomacyId to set
     */
    public void setDiplomacyId(int diplomacyId) {
        this.diplomacyId = diplomacyId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return the diameter
     */
    public int getDiameter() {
        return diameter;
    }

    /**
     * @param diameter the diameter to set
     */
    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
    
    
}
