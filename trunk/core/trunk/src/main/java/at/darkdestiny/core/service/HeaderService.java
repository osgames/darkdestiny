/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

 import at.darkdestiny.core.ML;import at.darkdestiny.core.dao.PlanetCategoryDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerCategoryDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetCategory;
import at.darkdestiny.core.model.PlayerCategory;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.System;
import at.darkdestiny.core.view.header.CategoryEntity;
import at.darkdestiny.core.view.header.HeaderData;
import at.darkdestiny.core.view.header.PlanetEntity;
import at.darkdestiny.core.view.header.SystemEntity;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.dao.PlanetCategoryDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerCategoryDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetCategory;
import at.darkdestiny.core.model.PlayerCategory;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.System;
import at.darkdestiny.core.view.header.CategoryEntity;
import at.darkdestiny.core.view.header.HeaderData;
import at.darkdestiny.core.view.header.PlanetEntity;
import at.darkdestiny.core.view.header.SystemEntity;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class HeaderService {

    private static final Logger log = LoggerFactory.getLogger(HeaderService.class);

    private static SystemDAO systemDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO planetDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static PlayerPlanetDAO playerPlanetDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlayerCategoryDAO playerCategoryDAO = (PlayerCategoryDAO) DAOFactory.get(PlayerCategoryDAO.class);
    private static PlanetCategoryDAO planetCategoryDAO = (PlanetCategoryDAO) DAOFactory.get(PlanetCategoryDAO.class);
    private HeaderData headerData;

    public HeaderService() {
        headerData = new HeaderData();
    }

    public void load(int userId) {
        // Erstellen des k&uuml;nstlichen <ALLE> Systems das alle Systeme und Planeten enth&auml;lt
        //dbg log.debug("User: "+userId);
        try {
            ArrayList<PlayerPlanet> pps = playerPlanetDAO.findByUserId(userId);
            TreeMap<String, System> systems = new TreeMap<String, System>();
            for (PlayerPlanet pp : pps) {
                if (pp.getUserId() == userId) {
                    Planet p = planetDAO.findById(pp.getPlanetId());
                    System s = systemDAO.findById(p.getSystemId());
                    if (systems.containsKey(s.getName() + s.getId())) {
                        continue;
                    }
                    systems.put(s.getName() + s.getId(), systemDAO.findById(p.getSystemId()));
                }
            }


            CategoryEntity all = new CategoryEntity();
            all.setCategoryId(HeaderData.CATEGORY_ALL);
            all.setCategoryName(ML.getMLStr("header_sel_all", userId));

            int lastCategoryId = 0;
            int lastSystemId = 0;
            int lastPlanetId = 0;
            int firstSystemId = 0;
            int firstPlanetId = 0;

            boolean firstSystem = true;
            for (Map.Entry<String, System> entry : systems.entrySet()) {

                SystemEntity system = new SystemEntity();
                System s = entry.getValue();
                system.setSystemId(s.getId());
                system.setSystemName(s.getName());
                if (firstSystem) {
                    firstSystem = false;
                    firstSystemId = s.getId();
                    all.setFirstSystemId(firstSystemId);
                } else {
                    system.setPreviousSystemId(lastSystemId);
                    all.getSystems().get(lastSystemId).setNextSystemId(s.getId());



                }
                ArrayList<Planet> planets = planetDAO.findBySystemId(s.getId());
                TreeMap<String, PlayerPlanet> ppsSystem = new TreeMap<String, PlayerPlanet>();
                for (Planet p : planets) {
                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
                    if (pp != null) {
                        if (pp.getUserId() == userId) {
                            ppsSystem.put(pp.getName() + p.getId(), pp);
                        }
                    }


                }

                boolean firstPlanet = true;
                for (Map.Entry<String, PlayerPlanet> entry2 : ppsSystem.entrySet()) {
                    PlayerPlanet pp = entry2.getValue();
                    PlanetEntity planet = new PlanetEntity();
                    planet.setPlanetId(pp.getPlanetId());
                    planet.setPlanetName(pp.getName());

                    if (firstPlanet) {
                        firstPlanetId = pp.getPlanetId();
                        system.setFirstPlanetId(firstPlanetId);

                        firstPlanet = false;
                    } else {
                        planet.setPreviousPlanetId(lastPlanetId);
                        system.getPlanets().get(lastPlanetId).setNextPlanetId(pp.getPlanetId());
                    }
                    lastPlanetId = pp.getPlanetId();

                    system.getPlanets().put(pp.getPlanetId(), planet);

                }
                system.getPlanets().get(firstPlanetId).setPreviousPlanetId(lastPlanetId);
                system.getPlanets().get(lastPlanetId).setNextPlanetId(firstPlanetId);

                all.getSystems().put(s.getId(), system);
                lastSystemId = s.getId();


            }
            try {
                if (all.getSystems().size() > 0) {
                    all.getSystems().get(firstSystemId).setPreviousSystemId(lastSystemId);
                    all.getSystems().get(lastSystemId).setNextSystemId(firstSystemId);
                }
            } catch (Exception ex) {
                //Wenn er zwar in playerplanet einen user gefunden hat aber f&uuml;r diesen planeten kein system bestimmen konnte
                DebugBuffer.addLine(DebugLevel.ERROR, "Error in Header : Planet in playerplanet but not assigned to a system: " + "");
                return;
            }

            headerData.getCategorys().put(HeaderData.CATEGORY_ALL, all);
            // Erstellen der Category spezifischen Ansichten
            ArrayList<PlayerCategory> playerCategorys = playerCategoryDAO.findByUserId(userId);
            //dbg log.debug("2");
            lastCategoryId = 0;
            lastSystemId = 0;
            lastPlanetId = 0;
            firstSystemId = 0;
            firstPlanetId = 0;

            for (PlayerCategory pc : playerCategorys) {
                if (Service.planetCategoryDAO.findByCategoryId(pc.getId()) == null) {
                    continue;
                }
                if (Service.planetCategoryDAO.findByCategoryId(pc.getId()).isEmpty()) {
                    continue;
                }
                //dbg log.debug("Done3");
                CategoryEntity category = new CategoryEntity();
                category.setCategoryId(pc.getId());
                category.setCategoryName(pc.getName());
                category.setPreviousCategoryId(lastCategoryId);
                if (!headerData.getCategorys().containsKey(lastCategoryId)) {
                    DebugBuffer.addLine(DebugLevel.WARNING, "!headerData.getCategorys().containsKey(lastCategoryId) : " + lastCategoryId);
                }
                if (headerData.getCategorys().get(lastCategoryId) == null) {
                    DebugBuffer.addLine(DebugLevel.WARNING, "headerData.getCategorys().get(lastCategoryId) == null" + lastCategoryId);
                }
                headerData.getCategorys().get(lastCategoryId).setNextCategoryId(pc.getId());
                lastCategoryId = pc.getId();
                ArrayList<PlanetCategory> planetCategorys = planetCategoryDAO.findByCategoryId(pc.getId());
                ArrayList<System> sysTmp = new ArrayList<System>();
                for (PlanetCategory plc : planetCategorys) {
                    Planet p = planetDAO.findById(plc.getPlanetId());
                    System s = systemDAO.findById(p.getSystemId());
                    PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
                    if (pp == null || pp.getUserId() != userId) {
                        planetCategoryDAO.remove(plc);
                    } else if (!sysTmp.contains(s)) {
                        sysTmp.add(s);
                    }
                }
                firstSystem = true;
                for (System s : sysTmp) {

                    SystemEntity system = new SystemEntity();

                    system.setSystemId(s.getId());
                    system.setSystemName(s.getName());
                    if (firstSystem) {
                        firstSystem = false;
                        firstSystemId = s.getId();
                        category.setFirstSystemId(firstSystemId);
                    } else {
                        system.setPreviousSystemId(lastSystemId);
                        category.getSystems().get(lastSystemId).setNextSystemId(s.getId());
                    }
                    lastSystemId = s.getId();

                    ArrayList<PlanetCategory> pcs = new ArrayList<PlanetCategory>();
                    for (Planet p : planetDAO.findBySystemId(s.getId())) {
                        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(p.getId());
                        if (pp == null) {
                            continue;
                        }
                        if (pp.getUserId() == userId) {
                            PlanetCategory pcTmp = planetCategoryDAO.findBy(p.getId(), pc.getId());
                            if (pcTmp != null) {
                                pcs.add(pcTmp);
                            }
                        }

                    }
                    //     rs3 = stmt3.executeQuery("SELECT pc.planetid, pc.planetname, pc.categoryId FROM planetcategory pc, planet p WHERE pc.planetid = p.id AND p.systemId =" + s.getId() + "  ORDER by p.systemId asc;");

                    //dbg log.debug("Done5");

                    boolean firstPlanet = true;

                    for (PlanetCategory pcTmp : pcs) {
                        if (category.getCategoryId() != pcTmp.getCategoryId()) {
                            continue;
                        }

                        PlanetEntity planet = new PlanetEntity();
                        planet.setPlanetId(pcTmp.getPlanetId());
                        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(pcTmp.getPlanetId());
                        planet.setPlanetName(pp.getName());
                        if (firstPlanet) {
                            firstPlanetId = pcTmp.getPlanetId();
                            system.setFirstPlanetId(firstPlanetId);
                            firstPlanet = false;
                        } else {
                            planet.setPreviousPlanetId(lastPlanetId);
                            system.getPlanets().get(lastPlanetId).setNextPlanetId(pcTmp.getPlanetId());
                        }
                        lastPlanetId = pcTmp.getPlanetId();
                        system.getPlanets().put(pcTmp.getPlanetId(), planet);

                    }
                    if (system.getPlanets().size() > 0) {
                        system.getPlanets().get(firstPlanetId).setPreviousPlanetId(lastPlanetId);
                        system.getPlanets().get(lastPlanetId).setNextPlanetId(firstPlanetId);
                        category.getSystems().put(s.getId(), system);
                    }
                }
                if (category.getSystems().size() > 0) {
                    category.getSystems().get(firstSystemId).setPreviousSystemId(lastSystemId);
                    category.getSystems().get(lastSystemId).setNextSystemId(firstSystemId);
                } else {
                    continue;
                }
                headerData.getCategorys().put(pc.getId(), category);
            }
            headerData.getCategorys().get(0).setPreviousCategoryId(lastCategoryId);
            if (!headerData.getCategorys().containsKey(lastCategoryId)) {
                DebugBuffer.addLine(DebugLevel.WARNING, "!yheaderData.getCategorys().containsKey(lastCategoryId) : " + lastCategoryId);
            }
            if (headerData.getCategorys().get(lastCategoryId) == null) {
                DebugBuffer.addLine(DebugLevel.WARNING, "yheaderData.getCategorys().get(lastCategoryId) == null" + lastCategoryId);
            }
            headerData.getCategorys().get(lastCategoryId).setNextCategoryId(0);



        } catch (Exception e) {
            //dbg log.debug("Error in here: ");
            DebugBuffer.addLine(e);
        }
    }

    public HeaderData getHeaderData() {
        return headerData;
    }

    public void setHeaderData(HeaderData headerData) {
        this.headerData = headerData;
    }

    public void test2() {


        for (Map.Entry<Integer, CategoryEntity> me : headerData.getCategorys().entrySet()) {

            try {
                printCategory((CategoryEntity) me.getValue());
            } catch (Exception e) {
                log.debug("Error for Category: " + me.getKey());
            }

            for (Map.Entry<Integer, SystemEntity> me2 : me.getValue().getSystems().entrySet()) {

                try {
                    printSystem((SystemEntity) me2.getValue());
                } catch (Exception e) {
                    log.debug("Error for System: " + me2.getKey());
                }
                for (Map.Entry<Integer, PlanetEntity> me3 : me2.getValue().getPlanets().entrySet()) {

                    try {
                        printPlanet((PlanetEntity) me3.getValue());
                    } catch (Exception e) {
                        log.debug("Error for Planet: " + me3.getKey());
                    }
                }
            }
        }

    }

    public void printCategory(CategoryEntity ce) {
        log.debug("==========================================");
        log.debug("::: Category: " + ce.getCategoryId());
        log.debug("::: Name: " + ce.getCategoryName());
        log.debug("::: firstSystem: " + ce.getFirstSystemId());
        log.debug("::: nextCat: " + ce.getNextCategoryId());
        log.debug("::: Previous: " + ce.getPreviousCategoryId());
    }

    public void printSystem(SystemEntity se) {
        log.debug("==========================================");
        log.debug("::: System: " + se.getSystemName());
        log.debug("::: Id: " + se.getSystemId());
        log.debug("::: firstPlanet: " + se.getFirstPlanetId());
        log.debug("::: NXTSystem: " + se.getNextSystemId());
        log.debug("::: PRSystem: " + se.getPreviousSystemId());
    }

    public void printPlanet(PlanetEntity pe) {
        log.debug("==========================================");
        log.debug("::: Id: " + pe.getPlanetId());
        log.debug("::: NXTSystem: " + pe.getNextPlanetId());
        log.debug("::: PRSystem: " + pe.getPreviousPlanetId());
    }
}
