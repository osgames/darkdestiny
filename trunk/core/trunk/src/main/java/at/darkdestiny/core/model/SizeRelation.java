/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation("sizerelation")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class SizeRelation extends Model<SizeRelation> {

    @FieldMappingAnnotation("chassisId")
    @IdFieldAnnotation()
    private Integer chassisId;
    @FieldMappingAnnotation("engineFactor")
    private Integer engineFactor;
    @FieldMappingAnnotation("propulsionFactor")
    private Double propulsionFactor;
    @FieldMappingAnnotation("hyperFactor")
    private Integer hyperFactor;
    @FieldMappingAnnotation("creditFactor")
    private Integer creditFactor;
    @FieldMappingAnnotation("hyperCrystalFactor")
    private Integer hyperCrystalFactor;

    public Integer getChassisId() {
        return chassisId;
    }

    public void setChassisId(Integer chassisId) {
        this.chassisId = chassisId;
    }

    public Integer getEngineFactor() {
        return engineFactor;
    }

    public void setEngineFactor(Integer engineFactor) {
        this.engineFactor = engineFactor;
    }

    public Double getPropulsionFactor() {
        return propulsionFactor;
    }

    public void setPropulsionFactor(Double propulsionFactor) {
        this.propulsionFactor = propulsionFactor;
    }

    public Integer getHyperFactor() {
        return hyperFactor;
    }

    public void setHyperFactor(Integer hyperFactor) {
        this.hyperFactor = hyperFactor;
    }

    public Integer getCreditFactor() {
        return creditFactor;
    }

    public void setCreditFactor(Integer creditFactor) {
        this.creditFactor = creditFactor;
    }

    public Integer getHyperCrystalFactor() {
        return hyperCrystalFactor;
    }

    public void setHyperCrystalFactor(Integer hyperCrystalFactor) {
        this.hyperCrystalFactor = hyperCrystalFactor;
    }
}
