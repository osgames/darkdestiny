/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin;

import at.darkdestiny.core.dao.DamagedShipsDAO;
import at.darkdestiny.core.dao.DiplomacyRelationDAO;
import at.darkdestiny.core.dao.HangarRelationDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EDamagedShipRefType;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.enumeration.EDiplomacyRelationType;
import at.darkdestiny.core.model.DamagedShips;
import at.darkdestiny.core.model.DiplomacyRelation;
import at.darkdestiny.core.model.HangarRelation;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ValidityChecks {
    public static DiplomacyRelationDAO drDAO = DAOFactory.get(DiplomacyRelationDAO.class);
    public static PlanetDefenseDAO pdDAO = DAOFactory.get(PlanetDefenseDAO.class);
    public static PlayerFleetDAO pfDAO = DAOFactory.get(PlayerFleetDAO.class);
    public static PlayerPlanetDAO ppDAO = DAOFactory.get(PlayerPlanetDAO.class);
    public static SystemDAO sDAO = DAOFactory.get(SystemDAO.class);
    public static PlanetDAO pDAO = DAOFactory.get(PlanetDAO.class);
    public static ShipDesignDAO sdDAO = DAOFactory.get(ShipDesignDAO.class);
    public static ShipFleetDAO sfDAO = DAOFactory.get(ShipFleetDAO.class);
    public static DamagedShipsDAO dsDAO = DAOFactory.get(DamagedShipsDAO.class);
    public static HangarRelationDAO hrDAO = DAOFactory.get(HangarRelationDAO.class);
    
    public static boolean checkDiplomaticRelations() {
        boolean errors = false;
        
        for (DiplomacyRelation dr : drDAO.findAll()) {
            boolean found = false;
            
            for (DiplomacyRelation dr2 : drDAO.findAll()) {
                if ((dr.getType() == EDiplomacyRelationType.USER_TO_USER) || (dr.getType() == EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE)) {
                    if (dr2.getType() == dr.getType()) {
                        if (dr.getFromId().equals(dr2.getToId()) && dr.getToId().equals(dr2.getFromId())) {
                            if (dr.getDiplomacyTypeId().equals(dr2.getDiplomacyTypeId())) {
                                found = true;
                            } else {
                                DebugBuffer.trace("Found relation " + dr.getType() + " between " + dr.getFromId() + " and " + dr.getToId() + " but diplomacyTypeId was not valid");
                            }
                        } 
                    }                   
                } else if (dr.getType() == EDiplomacyRelationType.ALLIANCE_TO_USER) {
                    if (dr2.getType() == EDiplomacyRelationType.USER_TO_ALLIANCE) {
                        if (dr.getFromId().equals(dr2.getToId()) && dr.getToId().equals(dr2.getFromId())) {
                            if (dr.getDiplomacyTypeId().equals(dr2.getDiplomacyTypeId())) {
                                found = true;
                            } else {
                                DebugBuffer.trace("Found relation " + dr.getType() + " between " + dr.getFromId() + " and " + dr.getToId() + " but diplomacyTypeId was not valid");
                            }                            
                        }   
                    }
                } else if (dr.getType() == EDiplomacyRelationType.USER_TO_ALLIANCE) {
                    if (dr2.getType() == EDiplomacyRelationType.ALLIANCE_TO_USER) {
                        if (dr.getFromId().equals(dr2.getToId()) && dr.getToId().equals(dr2.getFromId())) {
                            if (dr.getDiplomacyTypeId().equals(dr2.getDiplomacyTypeId())) {
                                found = true;
                            } else {
                                DebugBuffer.trace("Found relation " + dr.getType() + " between " + dr.getFromId() + " and " + dr.getToId() + " but diplomacyTypeId was not valid");
                            }                            
                        }   
                    }                    
                }
            }
            
            if (!found) {
                errors = true;
                DebugBuffer.warning("Found broken Relation " + dr.getType() + " from id " + dr.getFromId() + " to " + dr.getToId());
            }            
        }
        
        return errors;
    }
    
    public static boolean checkSpaceStations() {
        boolean errors = false;
        
        ArrayList<PlanetDefense> allDefenses = pdDAO.findAll();
        for (PlanetDefense pd : allDefenses) {
            if (pd.getType() != EDefenseType.SPACESTATION) continue;
            
            int pdUserId = pd.getUserId();
            int pdSystemId = pd.getSystemId();
            int pdPlanetId = pd.getPlanetId();
            
            if (pd.getFleetId() != 0) {
                PlayerFleet pf = pfDAO.findById(pd.getFleetId());
                if (pf == null) {
                    DebugBuffer.warning("Space Station is bound to non existant fleet");
                    DebugBuffer.warning("Details => UserId: " + pd.getUserId() + " UnitId: " + pd.getUnitId() + " SystemId: " + pd.getSystemId() + " PlanetId: " + pd.getPlanetId() + " FleetId: " + pd.getFleetId() + " Count: " + pd.getCount());
                    setSpaceStationToHomesystem(pd);
                    errors = true;
                    continue;
                }
                
                if (pf.getUserId() != pdUserId) {
                    DebugBuffer.warning("Space Station is bound to fleet of another player");
                    DebugBuffer.warning("Details => UserId: " + pd.getUserId() + " UnitId: " + pd.getUnitId() + " SystemId: " + pd.getSystemId() + " PlanetId: " + pd.getPlanetId() + " FleetId: " + pd.getFleetId() + " Count: " + pd.getCount());
                    errors = true;
                    setSpaceStationToHomesystem(pd);
                    continue;                    
                }
                
                // Future checks on fleet (enough capacity // has tractorbeam)
            }
            
            if (pd.getPlanetId() != 0) {
                PlayerPlanet pp = ppDAO.findByPlanetId(pd.getPlanetId());
                
                if (pp == null) {
                    DebugBuffer.warning("Space Station is placed on unihabitated planet");
                    DebugBuffer.warning("Details => UserId: " + pd.getUserId() + " UnitId: " + pd.getUnitId() + " SystemId: " + pd.getSystemId() + " PlanetId: " + pd.getPlanetId() + " FleetId: " + pd.getFleetId() + " Count: " + pd.getCount());                   
                    errors = true;
                    continue;                           
                }
            }
            
            if (pd.getSystemId() != 0) {
                ArrayList<Planet> pList = pDAO.findBySystemId(pd.getSystemId());
                boolean found = false;
                
                for (Planet p : pList) {
                    PlayerPlanet pp = ppDAO.findByPlanetId(p.getId());
                    
                    if ((pp != null) && (pp.getUserId() == pdUserId)) {
                        found = true;
                    }
                }
                
                if (!found) {
                    DebugBuffer.warning("Space Station is placed in system where player has no colony");
                    DebugBuffer.warning("Details => UserId: " + pd.getUserId() + " UnitId: " + pd.getUnitId() + " SystemId: " + pd.getSystemId() + " PlanetId: " + pd.getPlanetId() + " FleetId: " + pd.getFleetId() + " Count: " + pd.getCount());
                    errors = true;      
                    setSpaceStationToHomesystem(pd);
                }
            }
        }
        
        return errors;
    }
    
    public static boolean checkShips() {
        // Check if there are ships / damaged ships assigned to the wrong player (different designId than fleet owner)
        boolean errors = false;
        ArrayList<DamagedShips> damagedShipsList = dsDAO.findAll();
        
        for (DamagedShips ds : damagedShipsList) {
            if (ds.getRefTable() == EDamagedShipRefType.SHIPFLEET) {                                              
                ShipFleet sf = sfDAO.getById(ds.getShipFleetId());
                if (sf != null) {
                    PlayerFleet pf = pfDAO.findById(sf.getFleetId());
                    ShipDesign sd = sdDAO.findById(sf.getDesignId());

                    if (pf == null) {
                        errors = true;
                        DebugBuffer.warning("Found not existing fleet id " + sf.getFleetId() + " => DELETING ENTRY"); 
                        sfDAO.remove(sf);
                        continue;
                    }                    
                    
                    if (sd == null) {
                        errors = true;
                        DebugBuffer.warning("Found not existing design id " + sf.getDesignId()+"] in fleet " + sf.getFleetId() + " ["+pf.getName()+"] => DELETING ENTRY"); 
                        sfDAO.remove(sf);
                        continue;                        
                    }
                    
                    if (!pf.getUserId().equals(sd.getUserId())) {
                        errors = true;
                        DebugBuffer.warning("Found invalid design " + sd.getId() + " ["+sd.getName()+"] in fleet " + pf.getId() + " ["+pf.getName()+"]");                        
                    }
                } else {
                    errors = true;
                    DebugBuffer.warning("Found invalid damage Entry " + ds.getDamageLevel() + " -- " + ds.getRefTable() + " -- " + ds.getShipFleetId() + "  => DELETING ENTRY");
                    dsDAO.remove(ds);
                }                
            } else if (ds.getRefTable() == EDamagedShipRefType.HANGARRELATION) {
                
            }
        }
        
        ArrayList<HangarRelation> hrList = hrDAO.findAll();
        
        for (HangarRelation hr : hrList) {
            if (hr.getRelationToSF() == 1) continue;
            
            int fleetId = hr.getFleetId();
            int designId = hr.getRelationToDesign();
            
            ShipFleet sf = sfDAO.findBy(fleetId, designId);
            
            if (sf == null) {
                errors = true;
                DebugBuffer.warning("Found invalid hangarrelation for shipdesign " + designId + " and fleet " + fleetId);

                ShipDesign sd = sdDAO.findById(hr.getDesignId()); 
                
                if (sd == null) {
                    DebugBuffer.warning("Shipdesign " + designId + " does not exist anymore => DELETING ENTRY");
                    hrDAO.remove(hr);
                } else {
                    DebugBuffer.warning("Shipdesign ["+sd.getName()+"] in this entry belongs to player " + sd.getUserId() + " => PENDING");
                }
            } else {
                PlayerFleet pf = pfDAO.findById(sf.getFleetId());
                ShipDesign sd = sdDAO.findById(sf.getDesignId());            
                
                ShipDesign sdHangar = sdDAO.findById(hr.getDesignId());
                
                if (!sdHangar.getUserId().equals(pf.getUserId())) {
                    errors = true;
                    DebugBuffer.warning("Found invalid hangar Entry -- Loaded ship " + sdHangar.getId() + " in hangar of ship " + sd.getId() + " and fleet " + pf.getId());
                }
            }
        }
        
        return errors;
    }
    
    private static void setSpaceStationToHomesystem(PlanetDefense pd) {
        int userId = pd.getUserId();
        ShipDesign sd = sdDAO.findById(pd.getUnitId());
        
        if (sd == null) {
            DebugBuffer.warning("Fixing not possible cause station design does not exist anymore ... deleting entry");
            pdDAO.remove(pd);
            DebugBuffer.warning("Fixing sucessful ... ");
            return;
        }
        
        if (sd.getUserId() != userId) {
            DebugBuffer.warning("Override restore UserId because unit has different userId (new Id => " + sd.getUserId() + ")");
            userId = sd.getUserId();
        }
        
        PlayerPlanet pp = ppDAO.findHomePlanetByUserId(userId);
        RelativeCoordinate rc = new RelativeCoordinate(0,pp.getPlanetId());
        
        PlanetDefense pdNew = pd.clone();
        pdNew.setSystemId(rc.getSystemId());
        pdNew.setPlanetId(0);
        pdNew.setFleetId(0);
        pdNew.setUserId(userId);
        
        pdDAO.remove(pd);
        pdDAO.add(pdNew);
        
        DebugBuffer.warning("Fixing sucessful ... ");
    }
}
