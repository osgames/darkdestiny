/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;

/**
 *
 * @author Stefan
 */
public class StarMapViewCoordinate {
    private final AbsoluteCoordinate topLeft;
    private final int xSize;
    private final int ySize;
    private final float zoom;
    
    public StarMapViewCoordinate(AbsoluteCoordinate topLeft, int xSize, int ySize, float zoom) {
        this.topLeft = topLeft;
        this.xSize = xSize;
        this.ySize = ySize;
        this.zoom = zoom;
    }

    /**
     * @return the topLeft
     */
    public AbsoluteCoordinate getTopLeft() {
        return topLeft;
    }
    
    public AbsoluteCoordinate getBottomRight() {
        AbsoluteCoordinate bottomRight = new AbsoluteCoordinate(topLeft.getX() + (int)Math.round(xSize / zoom),topLeft.getY() + (int)Math.round(ySize / zoom));
        return bottomRight;
    }    

    /**
     * @return the xSize
     */
    public int getxSize() {
        return xSize;
    }

    /**
     * @return the ySize
     */
    public int getySize() {
        return ySize;
    }

    /**
     * @return the zoom
     */
    public float getZoom() {
        return zoom;
    }
}
