/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.addon;

import at.darkdestiny.core.enumeration.EAddOnProperty;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class AddOnProperties {

    private HashMap<EAddOnProperty, String> properties;

    public AddOnProperties(){
        properties = new HashMap<EAddOnProperty, String>();
        setProperty(EAddOnProperty.WEBPAGE_FOLDER, "web");
    }
    public void setProperty(EAddOnProperty key, String value) {
        properties.put(key, value);
    }

    public String getProperty(EAddOnProperty key) {
        return properties.get(key);
    }
}
