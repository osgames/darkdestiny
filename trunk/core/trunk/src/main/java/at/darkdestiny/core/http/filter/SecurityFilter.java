/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.http.filter;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.admin.SessionTracker;
import java.io.*;
import javax.servlet.http.*;
import at.darkdestiny.framework.dao.DAOFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.User;
import at.darkdestiny.util.DebugBuffer;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 *
 * @author Stefan
 */


public class SecurityFilter implements Filter {
    
    private static final Logger log = LoggerFactory.getLogger(SecurityFilter.class);
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured.
    private FilterConfig filterConfig = null;
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    
    public SecurityFilter() {
    }
    
    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("SecurityFilter:DoBeforeProcessing");
        }
        
        //
        // Write code here to process the request and/or response before
        // the rest of the filter chain is invoked.
        //

        //
        // For example, a logging filter might log items on the request object,
        // such as the parameters.
        //
	/*
         for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
         String name = (String)en.nextElement();
         String values[] = request.getParameterValues(name);
         int n = values.length;
         StringBuffer buf = new StringBuffer();
         buf.append(name);
         buf.append("=");
         for(int i=0; i < n; i++) {
         buf.append(values[i]);
         if (i < n-1)
         buf.append(",");
         }
         log(buf.toString());
         }
         */
    }
    
    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("SecurityFilter:DoAfterProcessing");
        }
        //
        // Write code here to process the request and/or response after
        // the rest of the filter chain is invoked.
        //

        //
        // For example, a logging filter might log the attributes on the
        // request object after the request has been processed.
        //
	/*
         for (Enumeration en = request.getAttributeNames(); en.hasMoreElements(); ) {
         String name = (String)en.nextElement();
         Object value = request.getAttribute(name);
         log("attribute: " + name + "=" + value.toString());

         }
         */
        //
        //
        // For example, a filter might append something to the response.
        //
	/*
         PrintWriter respOut = new PrintWriter(response.getWriter());
         respOut.println("<P><B>This has been appended by an intrusive filter.</B>");
         */
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        
        if (debug) {
            log("SecurityFilter:doFilter()");
        }        
        
        doBeforeProcessing(request, response);
        
        Throwable problem = null;
        
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();
        
        SessionTracker.updateSession(session);

        // Normally false
        boolean allowed = false;
        boolean debugBuffer = false;
        log.debug("Req Url : " + httpRequest.getRequestURL().toString());
        if (httpRequest.getRequestURL().toString().contains("debugBuffer.jsp")
                || httpRequest.getRequestURL().toString().contains("debugBufferAI.jsp")) {
            debugBuffer = true;
        }
        boolean adminFolder = (httpRequest.getRequestURL().toString().contains("admin/"));

        // log.debug("Called FILTER");
        if (!debugBuffer) {
            if (session.getAttribute("userId") != null) {
                User u = uDAO.findById(Integer.parseInt((String) session.getAttribute("userId")));
                if (u != null) {
                    if (u.getAdmin() || u.getBetaAdmin()) {
                        allowed = true;
                    } else {
                        if (session.getAttribute("admin") != null) {
                            allowed = true;
                        }
                    }
                }
                
                if (!allowed && adminFolder) {
                    log.debug("User is no admin");
                    HttpServletResponse resp = (HttpServletResponse) response;
                    resp.sendError(resp.SC_FORBIDDEN, ((HttpServletRequest) request).getRequestURI());
                    return;
                }
            } else {
                HttpServletResponse resp = (HttpServletResponse) response;
                resp.sendRedirect(GameConfig.getInstance().getHostURL() + "/login.jsp");
                return;
            }
        }
        
        Integer smHeight = null;
        Integer smWidth = null;
        Integer smType = null;
        
        try {
            int nonNullFound = 0;
            
            if (httpRequest.getParameter("starmap_height") != null) {
                System.out.println("Found HEIGHT");
                smHeight = Integer.parseInt(httpRequest.getParameter("starmap_height"));
                nonNullFound++;
            }
            if (httpRequest.getParameter("starmap_width") != null) {
                System.out.println("Found WIDTH");
                smWidth = Integer.parseInt(httpRequest.getParameter("starmap_width"));
                nonNullFound++;
            }
            if (httpRequest.getParameter("starmap_static") != null) {
                System.out.println("Found STATIC");
                smType = Integer.parseInt(httpRequest.getParameter("starmap_static"));
                nonNullFound++;
            }            
            
            if (nonNullFound >= 2) {
                System.out.println("VALUES: " + smHeight + " WIDTH: " + smWidth + " TYPE: " + smType);

                HttpServletResponse httpResponse = (HttpServletResponse) response;
                Cookie starMapType;
                
                if (smType == null) {
                    starMapType = new Cookie("map_type", String.valueOf(0));
                } else {
                    starMapType = new Cookie("map_type", String.valueOf(1));
                }
                Cookie starMapXSize = new Cookie("map_xsize", String.valueOf(smWidth));
                Cookie starMapYSize = new Cookie("map_ysize", String.valueOf(smHeight));

                // Set expiry date after 24 Hrs for both the cookies.            
                starMapType.setMaxAge(60 * 60 * 24 * 365);            
                starMapXSize.setMaxAge(60 * 60 * 24 * 365);            
                starMapYSize.setMaxAge(60 * 60 * 24 * 365);

                // Add both the cookies in the response header.
                httpResponse.addCookie(starMapType);
                httpResponse.addCookie(starMapXSize);
                httpResponse.addCookie(starMapYSize);       
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Cookie Handling: ", e);
        }
        
        try {
            // log.debug("REQUEST: " + request + " RESPONSE: " + response);
            chain.doFilter(request, response);
        } catch (Throwable t) {
            //
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            //
            problem = t;
            t.printStackTrace();
        }
        
        doAfterProcessing(request, response);

        //
        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        //
        if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            sendProcessingError(problem, response);
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     *
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     *
     */
    public void init(FilterConfig filterConfig) {
        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("SecurityFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    public String toString() {
        
        if (filterConfig == null) {
            return ("SecurityFilter()");
        }
        StringBuffer sb = new StringBuffer("SecurityFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
        
    }
    
    private void sendProcessingError(Throwable t, ServletResponse response) {
        
        String stackTrace = getStackTrace(t);
        
        if (stackTrace != null && !stackTrace.equals("")) {
            
            try {
                
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
                pw.print(stackTrace);
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }
    
    public static String getStackTrace(Throwable t) {
        
        String stackTrace = null;
        
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }
    
    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
    private static final boolean debug = true;
}
