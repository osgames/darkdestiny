/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai;

import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class ThreadSharedObjects {
    private static HashMap<Thread,HashMap<String,Object>> objects = new HashMap<Thread,HashMap<String,Object>>();

    public static String KEY_LOCALE = "locale";

    public static void registerThreadSharedObject(String name, Object o) {
        if (!objects.containsKey(Thread.currentThread())) {
            HashMap<String,Object> objectList = new HashMap<String,Object>();
            objectList.put(name, o);
            objects.put(Thread.currentThread(), objectList);
        } else {
            objects.get(Thread.currentThread()).put(name, o);
        }
    }

    public static Object getThreadSharedObject(String name) {
        return objects.get(Thread.currentThread()).get(name);
    }

    public static void removeThreadSharedObject(String name) {
        HashMap<String,Object> objectList = objects.get(Thread.currentThread());

        if (objectList != null) {
            objectList.remove(name);

            if (objectList.isEmpty()) objects.remove(Thread.currentThread());
        }
    }

    public static void clear() {
        objects.clear();
    }
}
