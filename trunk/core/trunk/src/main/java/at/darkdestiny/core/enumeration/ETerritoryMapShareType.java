/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Aion
 */
public enum ETerritoryMapShareType {
    ALL, ALLIANCE, USER, USER_BY_ALLIANCE, USER_BY_ALL
}
