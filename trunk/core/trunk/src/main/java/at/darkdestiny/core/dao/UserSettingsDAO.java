/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.UserSettings;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class UserSettingsDAO extends ReadWriteTable<UserSettings> implements GenericDAO {

    public UserSettings findByUserId(Integer userId){
        UserSettings us = new UserSettings();
        us.setUserId((int)userId);
        
        return get(us);
    }
}
