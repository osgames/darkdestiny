/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.interfaces.ITileGenerator;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TileGeneratorStarmap implements ITileGenerator {
    private final static Object mutex = new Object();
    
    // Initial Test Values for Fake Map
    private static final int absoluteWidth = 10000;
    private static final int absoluteHeight = 10000;

    // Testing function for creating Buffered Images
    @Override
    public BufferedImage createBufferedImage(int userId, int zoom, int x, int y) {
        int zoom2 = zoom + 2;

        BufferedImage bi = new BufferedImage(512,512,TYPE_INT_ARGB);  

        synchronized(mutex) {
        /* Not necessary anymore as stars are drawn different now
        String path = "";
       
        
        try {
            path = this.getClass().getClassLoader().getResource("").getPath().replace("WEB-INF/classes/", "images/");
            System.out.println("PATH: " + path);
        } catch (Exception e) {
            System.out.println("ERROR GETTING PATH: " + e);
        }
        */                    
       
        // Draw black background
        Graphics2D g2d = (Graphics2D)bi.getGraphics();
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, 512, 512);                
 
        // Draw raster
        g2d.setColor(Color.WHITE);
        // g2d.drawRect(0, 0, 511, 512);
       
        // Calculate coordinates to display
        // int totalXRange = 512 * (int)Math.pow(2d, zoom + 1);
        // int totalYRange = 512 * (int)Math.pow(2d, zoom + 1);
        int noOfTiles = (int)Math.pow(2d, zoom + 1);
        
        double range = (double)absoluteWidth / (double)Math.pow(2d,zoom + 1);
        double scaling = (double)512 / range;
        // System.out.println("RANGE: " + range);
       
        /*
        int realXMin = (int)Math.floor(x * range);
        int realXMax = (int)Math.floor((x+1) * range);
        int realYMin = (int)Math.floor(y * range);
        int realYMax = (int)Math.floor((y+1) * range);
        */
        // System.out.println("Draw String " + "[" + realXMin + "/" + realYMin + "->" + realXMax + "/" + realYMax + "]");       
       
        // Calculate real Coordinate 
        double xMinMapping = ((double)absoluteWidth / (double)noOfTiles) * (double)(x) + ((zoom == 0) ? 1 : 0);                
        double xMaxMapping = ((double)absoluteWidth / (double)noOfTiles) * (double)(x + 1);
        double yMinMapping = ((double)absoluteHeight / (double)noOfTiles) * (double)(y) + ((zoom == 0) ? 1 : 0);
        double yMaxMapping = ((double)absoluteHeight / (double)noOfTiles) * (double)(y + 1);
        
        /*
        g2d.setColor(Color.WHITE);
        g2d.drawString("[" + xMinMapping + " : " + yMinMapping + " -> " + xMaxMapping + " : " + yMaxMapping + "]", 150, 200);   
        */
        /*
        realXMin -= 1000;
        realXMax += 1000;
        realYMin -= 1000;
        realYMax += 1000;
       

        */
        
        // Draw observatories and hyperspacescanner on tiles
        StarMapDataSource smds = BufferedStarmapData.getStarMapUserData(userId);
        ArrayList<SMObservatory> observatories = smds.getObservatories();
       
        g2d.setColor(Color.WHITE);
        for (SMObservatory obs : observatories) {
/*
            Ellipse2D currEllipse = new Ellipse2D.Double(scanner.getX() - scanner.getRange(),
                scanner.getY() - scanner.getRange(),
                scanner.getRange() * 2,
                scanner.getRange() * 2);                        
*/            
            // Calculate observatory x
            int obsX = (int)Math.floor((double)(obs.getX() - 100d - xMinMapping) * scaling);
            int obsY = (int)Math.floor((double)(obs.getY() - 100d - yMinMapping) * scaling);
            
            g2d.fillOval(obsX, obsY, (int)Math.ceil(200d * scaling), (int)Math.ceil(200d * scaling));
        }
        
        ArrayList<SMSystem> systems = smds.getStarSystems();
        for (SMSystem sys : systems) { 
            boolean own = sys.isOwn();
            boolean allied = sys.isAllied();
            boolean nap = sys.isNAP();
            boolean neutral = sys.isNeutral();
            boolean enemy = sys.isEnemy();
            
            if (!own && !allied && !nap && !neutral && !enemy) {
                continue;
            }
            
            int circleDiameter = 0;
            if (own) circleDiameter += 5;
            if (allied) circleDiameter += 5;
            if (nap) circleDiameter += 5;
            if (neutral) circleDiameter += 5;
            if (enemy) circleDiameter += 5;
            
            while (circleDiameter > 0) {
                if (enemy) {
                    drawCircle(g2d,sys.getX(),sys.getY(),(double)circleDiameter,xMinMapping,yMinMapping,scaling,Color.RED);
                    circleDiameter -= 5;
                } else if (neutral) {
                    drawCircle(g2d,sys.getX(),sys.getY(),(double)circleDiameter,xMinMapping,yMinMapping,scaling,Color.YELLOW);
                    circleDiameter -= 5;                    
                } else if (nap) {
                    drawCircle(g2d,sys.getX(),sys.getY(),(double)circleDiameter,xMinMapping,yMinMapping,scaling,Color.BLUE.brighter());
                    circleDiameter -= 5;                    
                } else if (allied) {
                    drawCircle(g2d,sys.getX(),sys.getY(),(double)circleDiameter,xMinMapping,yMinMapping,scaling,Color.BLUE);
                    circleDiameter -= 5;                    
                } else if (own) {
                    drawCircle(g2d,sys.getX(),sys.getY(),(double)circleDiameter,xMinMapping,yMinMapping,scaling,Color.GREEN);
                    circleDiameter -= 5;                    
                }
            }
        }
        
        // Draw Exact starsystem locations
        // g2d.setColor(Color.ORANGE);
        /*
        ArrayList<SMSystem> systems = smds.getStarSystems();
        for (SMSystem sys : systems) {            
            // g2d.drawString("[" + sys.getX() + " : " + sys.getY() + "]", (int)Math.round(((double)sys.getX() + 1d - xMinMapping) * scaling), (int)Math.round(((double)sys.getY() - 2d - yMinMapping) * scaling)); 
            // g2d.drawString("[" + (int)Math.round(((double)sys.getX() - xMinMapping) * scaling) + " : " + (int)Math.round(((double)sys.getY() - yMinMapping) * scaling) + "]", (int)Math.round(((double)sys.getX() + 1d - xMinMapping) * scaling), (int)Math.round(((double)sys.getY() - 1d - yMinMapping) * scaling));  
            
            // g2d.drawLine((int)Math.round(((double)sys.getX() - 5d - xMinMapping) * scaling), (int)Math.round(((double)sys.getY() - yMinMapping) * scaling),     
                         (int)Math.round(((double)sys.getX() + 5d - xMinMapping) * scaling), (int)Math.round(((double)sys.getY() - yMinMapping) * scaling));
            // g2d.drawLine((int)Math.round(((double)sys.getX() - xMinMapping) * scaling),     (int)Math.round(((double)sys.getY() - 5d - yMinMapping) * scaling), 
                         (int)Math.round(((double)sys.getX() - xMinMapping) * scaling), (int)Math.round(((double)sys.getY() + 5d - yMinMapping) * scaling));            
        }
        */
        HyperspaceScannerCreator hsc = new HyperspaceScannerCreator();
        Area hyperArea = hsc.getHyperspaceScannerAreaTile(userId);
        
        AffineTransform at = new AffineTransform();        
        at.scale(scaling, scaling);
        // at.setToTranslation(-realXMin * scaling, -realYMin * scaling);       
        
        Shape hyperAreaShape = at.createTransformedShape((Shape)hyperArea.clone());
        
        at = new AffineTransform();
        at.setToTranslation(Math.floor(-xMinMapping * scaling), Math.floor(-yMinMapping * scaling));   
        
        hyperAreaShape = at.createTransformedShape(hyperAreaShape);
        
        g2d.setColor(Color.RED);
        g2d.draw(hyperAreaShape);
        
        // System.out.println("X1: " + realXMin + " X2: " + realXMax + " Y1: " + realYMin + " Y2: " + realYMax);                       
        // System.out.println("[z:"+zoom+"/scaling:"+scaling+"] X: " + realXMin + "-" + realXMax + " Y: " + realYMin + "-" + realYMax);        
        }

        return bi;
    }
    
    private void drawCircle(Graphics2D g2d, int x, int y, double diameter, double xMin, double yMin, double scaling, Color color) {
        int actX = (int)Math.floor((double)(x - diameter - xMin) * scaling);
        int actY = (int)Math.floor((double)(y - diameter - yMin) * scaling);        
        
        g2d.setColor(color);
        g2d.fillOval(actX, actY, (int)Math.ceil(diameter * 2d * scaling), (int)Math.ceil(diameter * 2d * scaling));
    }
}
