/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author HorstRabe
 */
@TableNameAnnotation(value = "styleelement")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class StyleElement extends Model<StyleElement> {


     public static final int STYLE_BASIC = 1;

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    Integer id;
    @IdFieldAnnotation
    @FieldMappingAnnotation("styleId")
    Integer styleId;
    @FieldMappingAnnotation("clazz")
    @ColumnProperties(length = 450)
    @StringType("text")
    String clazz;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("description")
    private String description;
    @FieldMappingAnnotation("core")
    private Boolean core;
    @FieldMappingAnnotation("template")
    private String template;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStyleId() {
        return styleId;
    }

    public void setStyleId(Integer styleId) {
        this.styleId = styleId;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isCore() {
        return core;
    }

    public void setCore(Boolean core) {
        this.core = core;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }
}
