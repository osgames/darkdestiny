/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.ParameterToTitle;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class ParameterToTitleDAO extends ReadWriteTable<ParameterToTitle> implements GenericDAO {

    public ParameterToTitle findBy(int conditionToTitleId, int parameterId){
        ParameterToTitle ptt = new ParameterToTitle();
        ptt.setConditionToTitleId(conditionToTitleId);
        ptt.setParameterId(parameterId);
        return (ParameterToTitle)get(ptt);
    }

    public ArrayList<ParameterToTitle> findConditionToTitleId(int conditionToTitleId) {
        ParameterToTitle ptt = new ParameterToTitle();
        ptt.setConditionToTitleId(conditionToTitleId);
        return (ArrayList<ParameterToTitle>)find(ptt);
    }

}
