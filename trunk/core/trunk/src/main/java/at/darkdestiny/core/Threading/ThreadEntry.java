/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.Threading;

/**
 *
 * @author Stefan
 */
public class ThreadEntry {
    private final long threadId;
    private final String threadName;
    private final boolean timingEnabled;
    private final long cpuTime;
    
    public ThreadEntry(long id, String threadName, boolean timingEnabled, long cpuTime) {
        this.threadId = id;
        this.threadName = threadName;
        this.timingEnabled = timingEnabled;
        this.cpuTime = cpuTime;
    }

    public long getThreadId() {
        return threadId;
    }

    public long getCpuTime() {
        return cpuTime;
    }

    public boolean isTimingEnabled() {
        return timingEnabled;
    }

    public String getThreadName() {
        return threadName;
    }
}
