/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.SpyAction;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class SpyActionDAO extends ReadOnlyTable<SpyAction> implements GenericDAO {
    public SpyAction findById(int id) {
        SpyAction spyAction = new SpyAction();
        spyAction.setId(id);
        return (SpyAction)get(spyAction);
    }
}
