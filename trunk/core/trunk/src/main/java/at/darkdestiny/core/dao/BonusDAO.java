/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EBonusRange;
import at.darkdestiny.core.model.Bonus;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class BonusDAO extends ReadWriteTable<Bonus> implements GenericDAO  {

    public Bonus findById(int bonusId) {
        Bonus b = new Bonus();
        b.setId(bonusId);
        return (Bonus)get(b);
    }
    public ArrayList<Bonus> findPlanetBonus() {
        Bonus b = new Bonus();
        b.setBonusRange(EBonusRange.PLANET);
        return find(b);
    }

}
