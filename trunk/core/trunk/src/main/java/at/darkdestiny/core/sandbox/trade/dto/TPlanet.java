/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.sandbox.trade.dto;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Feuerelfe
 */
public class TPlanet {

    private long outgoingCapacity;
    //Holds planets which receive resources from this planet
    private ArrayList<TPlanet> outgoingEdges = Lists.newArrayList();
    //Holds planets which provide resoureces
    private ArrayList<TPlanet> incomingEdges = Lists.newArrayList();

    //Hold the amount of resources which can be delivered to this planet
    private HashMap<Integer, Long> incomingPossibleByPlanet = Maps.newHashMap();
    //Hold the amount of resources which can be delivered from this planet
    private HashMap<Integer, Long> outgoingPossibleByPlanet = Maps.newHashMap();
    //Sets the amount of resources which shall go to a specific planet
    private HashMap<TPlanet, HashMap<Integer, Long>> routing = Maps.newHashMap();    

    //Processed resources whbich have already been calculated
    private HashSet<Integer> processedResources = Sets.newHashSet();

    //Hold the amount of resources which can be delivered to this planet
    //Used for testing
    private HashMap<Integer, Long> receivedResources = Maps.newHashMap();
    //Hold the amount of resources which can be delivered from this planet
    //Used for testing
    private HashMap<Integer, Long> transportedResources = Maps.newHashMap();

    private boolean calculationFinished = false;

    private int id;
    private double distanceFromStartPlanet = 1;

    public TPlanet(int id) {
        this.id = id;
    }

    public TPlanet(int id, double distanceFromStartPlanet) {
        this.id = id;
        this.distanceFromStartPlanet = distanceFromStartPlanet;
    }

    public void addRoutingEntry(TPlanet planet, int resource, long amount) {
        if (this.routing.get(planet) != null) {
            this.routing.get(planet).put(resource, amount);
        } else {
            HashMap<Integer, Long> entry = Maps.newHashMap();
            entry.put(resource, amount);
            this.routing.put(planet, entry);
        }
    }

    /**
     * @return the outgoingCapacity
     */
    public long getOutgoingCapacity() {
        return outgoingCapacity;
    }

    /**
     * @param outgoingCapacity the outgoingCapacity to set
     */
    public void setOutgoingCapacity(long outgoingCapacity) {
        this.outgoingCapacity = outgoingCapacity;
    }

    /**
     * @return the incomingPossibleByPlanet
     */
    public HashMap<Integer, Long> getIncomingPossibleByPlanet() {
        return incomingPossibleByPlanet;
    }

    /**
     * @param incomingPossibleByPlanet the incomingPossibleByPlanet to set
     */
    public void setIncomingPossibleByPlanet(HashMap<Integer, Long> incomingPossibleByPlanet) {
        this.incomingPossibleByPlanet = incomingPossibleByPlanet;
    }

    /**
     * @return the outgoingPossibleByPlanet
     */
    public HashMap<Integer, Long> getOutgoingPossibleByPlanet() {
        return outgoingPossibleByPlanet;
    }

    /**
     * @param outgoingPossibleByPlanet the outgoingPossibleByPlanet to set
     */
    public void setOutgoingPossibleByPlanet(HashMap<Integer, Long> outgoingPossibleByPlanet) {
        this.outgoingPossibleByPlanet = outgoingPossibleByPlanet;
    }

    /**
     * @return the outgoingEdges
     */
    public ArrayList<TPlanet> getOutgoingEdges() {
        return outgoingEdges;
    }

    /**
     * @param outgoingEdges the outgoingEdges to set
     */
    public void setOutgoingEdges(ArrayList<TPlanet> outgoingEdges) {
        this.outgoingEdges = outgoingEdges;
    }

    /**
     * @return the incomingEdges
     */
    public ArrayList<TPlanet> getIncomingEdges() {
        return incomingEdges;
    }

    /**
     * @param incomingEdges the incomingEdges to set
     */
    public void setIncomingEdges(ArrayList<TPlanet> incomingEdges) {
        this.incomingEdges = incomingEdges;
    }

    /**
     * @return the calculationFinished
     */
    public boolean isCalculationFinished() {
        return calculationFinished;
    }

    /**
     * @param calculationFinished the calculationFinished to set
     */
    public void setCalculationFinished(boolean calculationFinished) {
        this.calculationFinished = calculationFinished;
    }

    /**
     * @return the receivedResources
     */
    public HashMap<Integer, Long> getReceivedResources() {
        return receivedResources;
    }

    /**
     * @param receivedResources the receivedResources to set
     */
    public void setReceivedResources(HashMap<Integer, Long> receivedResources) {
        this.receivedResources = receivedResources;
    }

    /**
     * @return the transportedResources
     */
    public HashMap<Integer, Long> getTransportedResources() {
        return transportedResources;
    }

    /**
     * @param transportedResources the transportedResources to set
     */
    public void setTransportedResources(HashMap<Integer, Long> transportedResources) {
        this.transportedResources = transportedResources;
    }

    /**
     * @return the processedResources
     */
    public HashSet<Integer> getProcessedResources() {
        return processedResources;
    }

    /**
     * @param processedResources the processedResources to set
     */
    public void setProcessedResources(HashSet<Integer> processedResources) {
        this.processedResources = processedResources;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the distanceFromStartPlanet
     */
    public double getDistanceFromStartPlanet() {
        return distanceFromStartPlanet;
    }

    /**
     * @return the routing
     */
    public HashMap<TPlanet, HashMap<Integer, Long>> getRouting() {
        return routing;
    }

    /**
     * @param routing the routing to set
     */
    public void setRouting(HashMap<TPlanet, HashMap<Integer, Long>> routing) {
        this.routing = routing;
    }
}
