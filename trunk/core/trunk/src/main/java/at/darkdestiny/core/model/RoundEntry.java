/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "roundentry")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class RoundEntry extends Model<RoundEntry> {

    @FieldMappingAnnotation("combatRoundId")
    @IdFieldAnnotation
    private Integer combatRoundId;
    @FieldMappingAnnotation(value = "userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation(value = "troopId")
    @IdFieldAnnotation
    private Integer troopId;
    @FieldMappingAnnotation(value = "number")
    private Long number;
    @FieldMappingAnnotation(value = "dead")
    private Long dead;

    /**
     * @return the combatRoundId
     */
    public Integer getCombatRoundId() {
        return combatRoundId;
    }

    /**
     * @param combatRoundId the combatRoundId to set
     */
    public void setCombatRoundId(Integer combatRoundId) {
        this.combatRoundId = combatRoundId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the troopId
     */
    public Integer getTroopId() {
        return troopId;
    }

    /**
     * @param troopId the troopId to set
     */
    public void setTroopId(Integer troopId) {
        this.troopId = troopId;
    }

    /**
     * @return the number
     */
    public Long getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Long number) {
        this.number = number;
    }

    /**
     * @return the dead
     */
    public Long getDead() {
        return dead;
    }

    /**
     * @param dead the dead to set
     */
    public void setDead(Long dead) {
        this.dead = dead;
    }
}
