/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.planetcalc;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.chart.ChartCreator;
import at.darkdestiny.core.enumeration.EEconomyType;
import at.darkdestiny.core.statistics.EStatisticType;
import com.google.common.collect.Maps;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author Stefan
 */
public class EconomyChart {

    private DefaultPieDataset resultSurface = new DefaultPieDataset();
    private DefaultPieDataset resultWorkers = new DefaultPieDataset();
    private HashMap<String, String> typeMapping = Maps.newHashMap();
    private final int userId;

    public EconomyChart(ExtPlanetCalcResult epcr, int userId) {
        this.userId = userId;

        TreeMap<Float, String> sortedSurface = Maps.newTreeMap();
        TreeMap<Float, String> sortedWorkers = Maps.newTreeMap();

        for (Map.Entry<EEconomyType, Float> entry : epcr.getPlanetEconomySurfacePerc().entrySet()) {
            // System.out.println("[SURFACE] Read data: " + entry.getValue() + " / " + entry.getKey());
            if (entry.getValue().equals(Float.NaN)) continue;
            
            if (sortedSurface.containsKey(entry.getValue())) {
                float value = entry.getValue();
                while (sortedSurface.containsKey(value)) {
                    value += 0.00001f;
                }
                sortedSurface.put(value, ML.getMLStr(entry.getKey().toString(), userId));
            } else {
                sortedSurface.put(entry.getValue(), ML.getMLStr(entry.getKey().toString(), userId));
            }
            typeMapping.put(entry.getKey().toString(), ML.getMLStr(entry.getKey().toString(), userId));
        }

        for (Map.Entry<EEconomyType, Float> entry : epcr.getPlanetEconomyWorkerPerc().entrySet()) {
            if (entry.getValue().equals(Float.NaN)) continue;
            
            // System.out.println("[WORKERS] Read data: " + entry.getValue() + " / " + entry.getKey());
            if (sortedWorkers.containsKey(entry.getValue())) {
                float value = entry.getValue();
                while (sortedWorkers.containsKey(value)) {
                    value += 0.00001f;
                }
                sortedWorkers.put(value, ML.getMLStr(entry.getKey().toString(), userId));                
            } else {
                sortedWorkers.put(entry.getValue(), ML.getMLStr(entry.getKey().toString(), userId));
            }
            typeMapping.put(entry.getKey().toString(), ML.getMLStr(entry.getKey().toString(), userId));
        }

        // DefaultPieDataset resultSurface = new DefaultPieDataset();
        // DefaultPieDataset resultWorkers = new DefaultPieDataset();
        for (Map.Entry<Float, String> entry : sortedSurface.descendingMap().entrySet()) {
            if (entry.getKey() == 0f) {
                continue;
            }
            // System.out.println("[1] Add data: " + entry.getValue() + " / " + entry.getKey());
            resultSurface.setValue(entry.getValue(), entry.getKey());
        }

        for (Map.Entry<Float, String> entry : sortedWorkers.descendingMap().entrySet()) {
            if (entry.getKey() == 0f) {
                continue;
            }
            // System.out.println("[2] Add data: " + entry.getValue() + " / " + entry.getKey());
            resultWorkers.setValue(entry.getValue(), entry.getKey());
        }
    }

    public JFreeChart getSurfaceChart() {
        return getSurfaceChart(true);
    }

    public JFreeChart getSurfaceChart(boolean withTitle) {
        JFreeChart surfaceChart = null;

        try {
            if (!resultSurface.getKeys().isEmpty()) {
                ChartCreator cc = new ChartCreator(EStatisticType.PIECHART, resultSurface);
                if (withTitle) {
                    cc.setTitle(ML.getMLStr("surface_usage", userId));
                } else {
                    cc.setTitle(null);
                }
                surfaceChart = cc.getChart();

                PiePlot pp = (PiePlot) surfaceChart.getPlot();
                PieDataset piedataset = (PieDataset) resultSurface;
                for (int i = 0; i < piedataset.getItemCount(); i++) {
                    Comparable key = piedataset.getKey(i);
                    pp.setSectionPaint(key, getColorForGroup(key.toString()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return surfaceChart;
    }

    public JFreeChart getWorkerChart() {
        return getWorkerChart(true);
    }

    public JFreeChart getWorkerChart(boolean withTitle) {
        JFreeChart workersChart = null;

        try {
            ChartCreator cc = new ChartCreator(EStatisticType.PIECHART, resultWorkers);
            if (withTitle) {
                cc.setTitle(ML.getMLStr("worker_usage", userId));
            } else {
                cc.setTitle(null);
            }
            workersChart = cc.getChart();

            PiePlot pp = (PiePlot) workersChart.getPlot();
            PieDataset piedataset = (PieDataset) resultWorkers;
            for (int i = 0; i < piedataset.getItemCount(); i++) {
                Comparable key = piedataset.getKey(i);
                pp.setSectionPaint(key, getColorForGroup(key.toString()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return workersChart;
    }

    private Color getColorForGroup(String key) {
        if (key.equalsIgnoreCase(typeMapping.get("AGRICULTURE"))) {
            return Color.GREEN.brighter();
        } else if (key.equalsIgnoreCase(typeMapping.get("INDUSTRY"))) {
            return Color.ORANGE;
        } else if (key.equalsIgnoreCase(typeMapping.get("MILITARY"))) {
            return Color.RED;
        } else if (key.equalsIgnoreCase(typeMapping.get("RECREATIONAL"))) {
            return Color.CYAN;
        } else if (key.equalsIgnoreCase(typeMapping.get("RESEARCH"))) {
            return Color.BLUE.brighter();
        } else if (key.equalsIgnoreCase(typeMapping.get("NOT_USED"))) {
            return Color.WHITE;
        } else if (key.equalsIgnoreCase(typeMapping.get("NOT_SPECIFIED"))) {
            return Color.LIGHT_GRAY;
        }

        return Color.WHITE;
    }
}
