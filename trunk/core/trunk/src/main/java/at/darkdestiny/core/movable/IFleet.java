/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.movable;

import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.fleet.Movable;
import at.darkdestiny.core.model.FleetOrder;
import at.darkdestiny.core.ships.ShipData;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface IFleet extends Movable {
    public int getId();
    public String getName();
    public int getUserId();
    public FleetOrder getFleetOrder();
    public ArrayList<ShipData> getShipList();

    public ELocationType getLocationType();
    /**
     * Returns the location id of playerFleet
     * @return
     */
    public Integer getLocationId();
}
