/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai.goals;

import at.darkdestiny.core.ai.AIDebugBuffer;
import at.darkdestiny.core.ai.AIDebugLevel;
import at.darkdestiny.core.ai.AIMessageHandler;
import at.darkdestiny.core.ai.AIMessagePayload;
import at.darkdestiny.core.ai.AIThread;
import at.darkdestiny.core.ai.ThreadSharedObjects;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Admin
 */
public class PlanetMoraleGoal extends AIGoal {

    PlanetDAO pDAO = DAOFactory.get(PlanetDAO.class);
    private int goalId = 3;

    @Override
    public boolean processGoal(int userId) {
        // AIDebugBuffer.add(AIDebugLevel.INFO, "Processing PlanetMoraleGoal");

        PlanetCalculation pc = (PlanetCalculation) ThreadSharedObjects.getThreadSharedObject("PlanetCalculation");

        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        double morale = epcr.getMorale();
        double crime = epcr.getCriminalityValue();
        double crimeDev = epcr.getDevCriminality();
        double riot = epcr.getRiotValue();
        int loyality = epcr.getLoyalityValue().intValue();

        PlayerPlanet pp = pc.getPlayerPlanet();
        Planet p = pDAO.findById(pp.getPlanetId());

        int planetFill = 100;
        if (p.getMaxPopulation() > 0) {
            planetFill = (int) Math.floor(100l / p.getMaxPopulation() * pp.getPopulation());
        }

        if ((morale > 100) && (epcr.getMaxMorale() >= 100)) {
            if (loyality < 100) {
                if (morale == epcr.getMaxMorale()) {
                    AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "[" + goalId + "/" + pp.getPlanetId() + "] Loyality < 100 && Morale >= MaxMorale => Increase Tax (" + pp.getTax() + ">" + (pp.getTax() + 1) + ")");
                    pp.setTax(pp.getTax() + 1);
                }
            } else {
                AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "[" + goalId + "/" + pp.getPlanetId() + "] Morale > 100 => Increase Tax (" + pp.getTax() + ">" + (pp.getTax() + 1) + ")");
                pp.setTax(pp.getTax() + 1);
            }
        } else if (morale < 70) {
            AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "[" + goalId + "/" + pp.getPlanetId() + "] Morale < 70 => Decrease Tax (" + pp.getTax() + ">" + (pp.getTax() - 1) + ")");
            pp.setTax(pp.getTax() - 2);
        } else {
            if ((epcr.getFoodcoverageNoStock() > 1.5f) && (planetFill < 80)) {
                AIDebugBuffer.log((AIThread) Thread.currentThread(), AIDebugLevel.INFO, "[" + goalId + "/" + pp.getPlanetId() + "] Enough food, planet not full => Decrease Tax (" + pp.getTax() + ">" + (pp.getTax() - 1) + ")");
                pp.setTax(pp.getTax() - 1);
            }
        }

        /*
         if (crime > 0) {
         AIDebugBuffer.add(AIDebugLevel.INFO, "["+goalId+"/"+pp.getPlanetId()+"] Crime Level Increasing => Build barracks");
         sendDirectConstructionOrder(pc.getPlanetId(), Construction.ID_BARRACKS, AIGoal.MESSAGE_PRIORITY_HIGH);
         }
         */

        Service.playerPlanetDAO.update(pp);

        return false;
    }

    @Override
    public void evaluateGoal(int userId) {
    }

    @Override
    public int getGoalId() {
        return goalId;
    }

    private void sendDirectConstructionOrder(int planetId, Integer construction, Integer priority) {
        AIMessagePayload amp = new AIMessagePayload();
        amp.addParameter("CONSTRUCTIONID", construction.toString());
        amp.addParameter("PRIORITY", priority.toString());

        AIMessageHandler aimh = new AIMessageHandler();
        aimh.sendMessage(goalId, 5, planetId, amp);
    }
}
