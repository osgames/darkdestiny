/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin;

import at.darkdestiny.core.chat.JSChatUser;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.painting.map.AreaMapContainer;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

/**
 *
 * @author Stefan
 */
public class SessionTracker {
    private static UserDAO uDAO = DAOFactory.get(UserDAO.class);
    
    private static HashMap<String,HttpSession> sessionMap = Maps.newHashMap();
    private static HashMap<Integer,SessionEntry> userSessionMap = Maps.newHashMap();
    
    public static void addSession(HttpSessionEvent hse) {
        System.out.println("Add sessionId " + hse.getSession().getId() + " to sessionMap");
        sessionMap.put(hse.getSession().getId(),hse.getSession());        
    }
    
    public static void removeSession(HttpSessionEvent hse) {
        System.out.println("Remove sessionId " + hse.getSession().getId() + " from sessionMap");
        sessionMap.remove(hse.getSession().getId());
        
        try {
            int userId = 0;
            
            if (hse.getSession().getAttribute("userId") != null) {
                userId = Integer.parseInt((String) hse.getSession().getAttribute("userId"));
                
                AreaMapContainer.clearAreaMap(userId);
                AreaMapContainer.clearStarMapObjects(userId);
                AreaMapContainer.clearViews(userId);                
            }
            
            if (userId != 0) {                
                System.out.println("Remove user " + userId + " from userSessionMap");
                userSessionMap.remove(userId);
            }
        } catch (Exception e) {
            DebugBuffer.warning("UserId could not be converted to Integer");
        }        
    }    
    
    public static void updateSession(HttpSession hs) {
        String sessionId = hs.getId();
        
        if (!sessionMap.containsKey(sessionId)) {
            System.out.println("Add sessionId " + hs.getId() + " to sessionMap");
            sessionMap.put(sessionId, hs);
        }
        
        try {
            int userId = 0;
            
            if (hs.getAttribute("userId") != null) {
                userId = Integer.parseInt((String) hs.getAttribute("userId"));
            }
            
            if (userId != 0) {                
                if (userSessionMap.containsKey(userId)) {
                    SessionEntry se = userSessionMap.get(userId);
                    se.setLastActivity(System.currentTimeMillis());
                } else {
                    System.out.println("Add user " + userId + " to userSessionMap");
                    SessionEntry se = new SessionEntry(hs,System.currentTimeMillis());
                    userSessionMap.put(userId, se);
                }
            }
        } catch (Exception e) {
            DebugBuffer.warning("UserId could not be converted to Integer");
        }
    }
    
    public static int getLoggedInUsers() {
        int loggedIn = 0;
        
        for (Map.Entry<Integer,SessionEntry> userSessionEntry : userSessionMap.entrySet()) {
            long lastActivity = userSessionEntry.getValue().getLastActivity();
            long timeDiff = System.currentTimeMillis() - lastActivity;
            
            if (timeDiff < 1000 * 60 * 20) {
                loggedIn++;
            }            
        }
        return loggedIn;
    }
    
    public static ArrayList<JSChatUser> getChatUsers() {
        ArrayList<JSChatUser> users = new ArrayList<JSChatUser>();
        
        for (Map.Entry<Integer,SessionEntry> userSessionEntry : userSessionMap.entrySet()) {
            long lastActivity = userSessionEntry.getValue().getLastActivity();
            long timeDiff = System.currentTimeMillis() - lastActivity;
            
            if (timeDiff < 1000 * 60 * 20) {
                User u = uDAO.findById(userSessionEntry.getKey());
                JSChatUser jscu = new JSChatUser(u.getGameName());
                if (timeDiff > (1000 * 60)) {
                    jscu.setInactive(true);
                }
                
                users.add(jscu);
            }            
        }                
        
        ArrayList<JSChatUser> sorted = Lists.newArrayList();
        TreeMap<String,JSChatUser> sortedMap = Maps.newTreeMap();
        
        for (JSChatUser jscu : users) {
            sortedMap.put(jscu.getName(),jscu);
        }
        
        for (JSChatUser jscu : sortedMap.values()) {
            sorted.add(jscu);
        }
        
        return sorted;
    }
    
    public static boolean isOnline(int userId) {
        if (!userSessionMap.containsKey(userId)) {
            return false;
        }
        
        long lastActivity = userSessionMap.get(userId).getLastActivity();
        long timeDiff = System.currentTimeMillis() - lastActivity;
        
        if (timeDiff > 1000 * 60 * 10) {
            return false;
        } else {
            return true;
        }
    }
}
