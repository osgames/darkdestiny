/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

/**
 *
 * @author Stefan
 */
public class LongObj {
    private long value = 0; 

    public LongObj() {

    }    
    
    public LongObj(long value) {
        this.value = value;
    }
    
    public long getLong() {
        return value;
    }
    
    public void addLong(long value) {
        this.value += value;
    }
    
    public void subtractLong(long value) {
        this.value -= value;
    }
    
    public void setLong(long value) {
        this.value = value;
    }
}
