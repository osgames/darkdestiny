/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.UserData;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class UserDataDAO extends ReadWriteTable<UserData> implements GenericDAO {

    public UserData findByUserId(Integer userId){
        UserData ud = new UserData();
        ud.setUserId(userId);
        ArrayList<UserData> result = find(ud);

        if(result.isEmpty()){
            return null;
        }else{
            return result.get(0);
        }
    }
}
