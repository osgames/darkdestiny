/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.Threading;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ThreadUtilities {
    private static ThreadMXBean tmxbean = ManagementFactory.getThreadMXBean();
    
    public static ArrayList<ThreadEntry> getAllThreads() {
        if (!tmxbean.isThreadCpuTimeSupported()) tmxbean.setThreadCpuTimeEnabled(true);
        ArrayList<ThreadEntry> threadEntries = new ArrayList<ThreadEntry>();
        
        long[] ids = tmxbean.getAllThreadIds();
        
        for (int i=0;i<ids.length;i++) {
            ThreadInfo ti = tmxbean.getThreadInfo(ids[i]);
            
            ThreadEntry te = new ThreadEntry(ti.getThreadId(),ti.getThreadName(),tmxbean.isThreadCpuTimeEnabled(),tmxbean.getThreadCpuTime(ti.getThreadId()));
            threadEntries.add(te);
        }
        
        return threadEntries;
    }
}
