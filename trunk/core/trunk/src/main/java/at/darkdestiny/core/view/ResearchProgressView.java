/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view;

import at.darkdestiny.core.model.Research;

/**
 *
 * @author Stefan
 */
public class ResearchProgressView {
    private final Research researchEntry;
    private final int status;    
    private final int resPointsLeft;    
    private final int compResPointsLeft;    
    

    public ResearchProgressView(Research researchEntry, int status, int resPointsLeft, 
            int compResPointsLeft) {
        this.researchEntry = researchEntry;
        this.status = status;
        this.resPointsLeft = resPointsLeft;
        this.compResPointsLeft = compResPointsLeft;
    }
    
    public Research getResearchEntry() {
        return researchEntry;
    }

    public int getStatus() {
        return status;
    }

    public int getResPointsResearched() {
        return resPointsLeft;
    }
    
    public int getCompResPointsResearched() {
        return compResPointsLeft;
    }
}
