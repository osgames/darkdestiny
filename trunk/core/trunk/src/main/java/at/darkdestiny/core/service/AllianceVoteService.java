/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.AllianceRankDAO;
import at.darkdestiny.core.enumeration.ELeadership;
import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.AllianceRank;
import at.darkdestiny.core.model.VoteOption;
import at.darkdestiny.core.model.VotePermission;
import at.darkdestiny.core.model.Voting;
import at.darkdestiny.core.voting.MLString;
import at.darkdestiny.core.voting.MetaDataDataType;
import at.darkdestiny.core.voting.Vote;
import at.darkdestiny.core.voting.VotingFactory;
import at.darkdestiny.core.voting.VotingTextMLSecure;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class AllianceVoteService {

    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static AllianceRankDAO arDAO = (AllianceRankDAO) DAOFactory.get(AllianceRankDAO.class);

    /* =================================================
     * Check if certain actions can be done by this user
    ================================================= */
    public static boolean mayDemoteUser(int userId, int targetUserId) {
        return false;
    }

    public static boolean mayDemoteAdmin(int userId) {
        return false;
    }

    public static boolean mayVoteNewAdmin(int userId) {
        return false;
    }

    public static boolean mayPromoteUser(int userId) {
        return false;
    }

    /* =================================================
     * Create votes with according permissions
    ================================================= */
    protected static void createLeadershipChangeVote(int reqUserId, ELeadership newLeadership) {
        try {
            AllianceMember amReq = amDAO.findByUserId(reqUserId);
            Alliance a = aDAO.findById(amReq.getAllianceId());

            Vote v = null;
            if (true) {
                HashMap<String,CharSequence> msgReplacements = new HashMap<String,CharSequence>();
                HashMap<String,CharSequence> sbjReplacements = new HashMap<String,CharSequence>();

                sbjReplacements.put("%USER%",IdToNameService.getUserName(reqUserId));

                msgReplacements.put("%USER%",IdToNameService.getUserName(reqUserId));
                msgReplacements.put("%NEWLEADERSHIP%",newLeadership.toString());

                VotingTextMLSecure vtML = new VotingTextMLSecure("alliance_vote_leadership_change","alliance_vote_leadership_change_sbj",msgReplacements,sbjReplacements);
                v = new Vote(amReq.getUserId(), vtML, Voting.TYPE_CHANGELEADERSHIP);
            }

            ArrayList<Integer> receivers = new ArrayList<Integer>();
            ArrayList<AllianceMember> memberList = amDAO.findByAllianceId(a.getId());

            for (AllianceMember am : memberList) {
                if (!am.getIsTrial()) {
                    receivers.add(am.getUserId());
                    v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                }
            }

            v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));
            v.setMinVotes((int) Math.ceil((double) receivers.size() * (2d / 3d)));
            v.setTotalVotes(receivers.size());

            v.addOption(new VoteOption("alliance_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
            v.addOption(new VoteOption("alliance_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

            v.addMetaData("NEWSTATE", newLeadership, MetaDataDataType.STRING);
            v.addMetaData("ALLIANCEID", a.getId(), MetaDataDataType.INTEGER);

            v.setReceivers(receivers);

            VotingFactory.persistVote(v);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on leadership state change vote: ", e);
        }
    }

    protected static boolean createUserChangeVote(int reqUserId, int targetUserId, int rank, boolean special) {
        // As we came here we can assume this vote is allowed to be processed - no further checks needed
        // if new vote is a promotion (we have to check if its an admin promotion) (1/2 on all promotions except admin) (2/3 on admin)
        // if new vote is demotion we have to check also for admin (2/3 on admin) - (1/2 on rest)
        try {
            AllianceMember amReq = amDAO.findByUserId(reqUserId);
            AllianceMember amTarget = amDAO.findByUserId(targetUserId);
            Alliance a = aDAO.findById(amReq.getAllianceId());

            int newRank = -1;

            if (special) {
                newRank = arDAO.findBy(rank, amTarget.getAllianceId()).getComparisonRank();
            } else {
                newRank = rank;
            }

            int oldRank = AllianceService.getRank(amTarget);
            boolean promote = oldRank < newRank;

            // On Dictorial no Vote is required => return false
            if (a.getLeadership() == ELeadership.DICTORIAL) {
                return false;
            }

            String message = "";
            String topic = "";


            // final int voteStarter, final VotingTextMLSecure votingTextML, final int type

            Vote v = null;
            if (true) {
                HashMap<String,CharSequence> msgReplacements = new HashMap<String,CharSequence>();
                HashMap<String,CharSequence> sbjReplacements = new HashMap<String,CharSequence>();

                sbjReplacements.put("%USER%",IdToNameService.getUserName(reqUserId));
                sbjReplacements.put("%TARGETUSER%",IdToNameService.getUserName(targetUserId));

                msgReplacements.put("%USER%",IdToNameService.getUserName(reqUserId));
                msgReplacements.put("%TARGETUSER%",IdToNameService.getUserName(targetUserId));

                // Create text for ranks
                msgReplacements.put("%OLDRANK%",rankToString(amTarget,null,null));
                msgReplacements.put("%NEWRANK%",rankToString(amTarget,rank,special));

                VotingTextMLSecure vtML = new VotingTextMLSecure("alliance_vote_member_change","alliance_vote_member_change_sbj",msgReplacements,sbjReplacements);
                v = new Vote(amReq.getUserId(), vtML, Voting.TYPE_MEMBERPROMOTE);
            }
            /*
            else {
                VotingTextMLSecure vtML = new VotingTextMLSecure("alliance_vote_member_change","alliance_vote_member_change_sbj",null,null);
                v = new Vote(amReq.getUserId(), vtML, Voting.TYPE_MEMBERDEGRADE);
            }
            */

            ArrayList<Integer> receivers = new ArrayList<Integer>();
            ArrayList<AllianceMember> memberList = amDAO.findByAllianceId(a.getId());

            for (AllianceMember am : memberList) {
                if (am.getIsAdmin() || am.getIsCouncil() || (!am.getIsTrial() && !(a.getLeadership() == ELeadership.ARISTOCRATIC))) {
                    if (!AllianceService.isActive(am)) continue;
                    receivers.add(am.getUserId());
                    v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, am.getUserId()));
                }
            }

            v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));

            if (a.getLeadership() == ELeadership.DEMOCRATIC) {
                // Create a vote and add all Members, Council, Admin
                if ((promote && (newRank == AllianceRank.RANK_ADMIN_INT) && (oldRank != AllianceRank.RANK_ADMIN_INT)) ||
                    (!promote && (oldRank == AllianceRank.RANK_ADMIN_INT) && (newRank <= AllianceRank.RANK_ADMIN_INT))) {
                    v.setMinVotes((int) Math.ceil((double) receivers.size() * (2d / 3d)));
                } else {
                    v.setMinVotes((int) Math.ceil((double) receivers.size() / 2d));
                }
                v.setTotalVotes(receivers.size());

                v.addOption(new VoteOption("alliance_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
                v.addOption(new VoteOption("alliance_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

                v.addMetaData("USERID", amTarget.getUserId(), MetaDataDataType.INTEGER);
                v.addMetaData("NEWRANK", rank, MetaDataDataType.INTEGER);
                v.addMetaData("SPECIAL", special?1:0, MetaDataDataType.INTEGER);

                v.setReceivers(receivers);

                VotingFactory.persistVote(v);
            } else if (a.getLeadership() == ELeadership.ARISTOCRATIC) {
                // Start a vote and add all Council and Admins
                if ((promote && (newRank == AllianceRank.RANK_ADMIN_INT) && (oldRank != AllianceRank.RANK_ADMIN_INT)) ||
                    (!promote && (oldRank == AllianceRank.RANK_ADMIN_INT) && (newRank <= AllianceRank.RANK_ADMIN_INT))) {
                    v.setMinVotes((int) Math.ceil((double) receivers.size() * (2d / 3d)));
                } else {
                    v.setMinVotes((int) Math.ceil((double) receivers.size() / 2d));
                }
                v.setTotalVotes(receivers.size());

                v.addOption(new VoteOption("alliance_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
                v.addOption(new VoteOption("alliance_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

                v.addMetaData("USERID", amTarget.getUserId(), MetaDataDataType.INTEGER);
                v.addMetaData("NEWRANK", rank, MetaDataDataType.INTEGER);
                v.addMetaData("SPECIAL", special?1:0, MetaDataDataType.INTEGER);

                v.setReceivers(receivers);

                VotingFactory.persistVote(v);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on create member rank change vote: ", e);
        }

        // Return true in case something bad happens
        return true;
    }

    private static CharSequence rankToString(AllianceMember am, Integer rank, Boolean special) {
        MLString name;

        if (rank == null && special == null) {
            if (am.getSpecialRankId() != 0) {
                rank = am.getSpecialRankId();
                special = true;
            } else {
                rank = AllianceRank.RANK_MEMBER_INT;
                if (am.getIsAdmin()) rank = AllianceRank.RANK_ADMIN_INT;
                if (am.getIsCouncil()) rank = AllianceRank.RANK_COUNCIL_INT;
                if (am.getIsTrial()) rank = AllianceRank.RANK_TRIAL_INT;
                special = false;
            }
        }

        if (special) {
            // We have to deal with a special Rank
            AllianceRank ar = arDAO.findBy(rank, am.getAllianceId());
            name = new MLString(ar.getRankName() + " (%1)");
            name.addMLComponent("%1", AllianceRank.getComparisonRankName(ar.getComparisonRank()));
        } else {
            // Default rank
            name = new MLString("%1");
            name.addMLComponent("%1", AllianceRank.getComparisonRankName(rank));
        }

        return name;
    }
}
