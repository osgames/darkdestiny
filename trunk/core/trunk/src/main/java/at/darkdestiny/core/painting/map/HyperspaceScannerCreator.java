/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.StarMapInfoUtilities;
import at.darkdestiny.core.enumeration.EHyperAreaState;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class HyperspaceScannerCreator {
    private static HashMap<String,Area> bufferedAreas = new HashMap<String,Area>();
    private static HashMap<String,EHyperAreaState> bufferedAreaStates = new HashMap<String,EHyperAreaState>();
    
    public Area getHyperspaceScannerAreaTile(int userId) {
        String userTimeKey = createUserTimeKey(userId);
        
        if (!bufferedAreaStates.containsKey(userTimeKey)) {
            bufferedAreas.put(userTimeKey, new Area());
            bufferedAreaStates.put(userTimeKey, EHyperAreaState.NOT_INITIALIZED);
            bufferedAreaStates.put(userTimeKey, EHyperAreaState.INITIALIZING);
            initializeScannerArea(userTimeKey, userId);
            bufferedAreaStates.put(userTimeKey, EHyperAreaState.INITIALIZED);
        } else if (bufferedAreaStates.get(userTimeKey) == EHyperAreaState.INITIALIZED) {
            return bufferedAreas.get(userTimeKey);
        } else {
            // Obviously we are still in generating the Area and should wait for completition
        }
        
        return bufferedAreas.get(userTimeKey);
    }
    
    private void initializeScannerArea(String userTimeKey, int userId) {
        StarMapDataSource smds = BufferedStarmapData.getStarMapUserData(userId);
        
        ArrayList<SMHyperscanner> scanners = smds.getHyperscanners();
        
        Area hyperArea = bufferedAreas.get(userTimeKey);
        Area workingArea = new Area();

        int workCounter = 0;

        for (SMHyperscanner scanner : scanners) {
            if (workCounter > 10) {
                hyperArea.add(workingArea);
                workingArea = new Area();
                workCounter = 0;
            }

            workCounter++;

            Ellipse2D currEllipse = new Ellipse2D.Double(scanner.getX() - scanner.getRange(),
                scanner.getY() - scanner.getRange(),
                scanner.getRange() * 2,
                scanner.getRange() * 2);
            
            /*
            Ellipse2D currEllipse = new Ellipse2D.Double(Math.round(
                    ((PainterData) painterData).getScanners().get(i).getActCoord_x1() - delta[0] * AppletConstants.HYPERSCANNER_RANGE - 1),
                    Math.round(((PainterData) painterData).getScanners().get(i).getActCoord_y1() - delta[1] * AppletConstants.HYPERSCANNER_RANGE - 1),
                    Math.round(delta[0] * 2 * AppletConstants.HYPERSCANNER_RANGE + 1),
                    Math.round(delta[1] * 2 * AppletConstants.HYPERSCANNER_RANGE + 1));
            */
            
            Area tmpArea1 = new Area(currEllipse);
            workingArea.add(tmpArea1);
        }

        hyperArea.add(workingArea);        
    }
    
    private String createUserTimeKey(int userId) {
        String key = userId + "_" + GameUtilities.getCurrentTick2();
        return key;
    }
}
