/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.core.enumeration.EConditionType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "conditionparameter")
@DataScope(type = DataScope.EScopeType.CONSTANTS)
public class ConditionParameter extends Model<ConditionParameter> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("conditionId")
    private Integer conditionId;
    @FieldMappingAnnotation("paramOrder")
    private Integer order;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("type")
    private EConditionType type;

    /**
     * @return the conditionId
     */
    public Integer getConditionId() {
        return conditionId;
    }

    /**
     * @param conditionId the conditionId to set
     */
    public void setConditionId(Integer conditionId) {
        this.conditionId = conditionId;
    }

    /**
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Integer order) {
        this.order = order;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public EConditionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EConditionType type) {
        this.type = type;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
}
