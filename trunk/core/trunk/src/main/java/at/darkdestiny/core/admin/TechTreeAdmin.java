package at.darkdestiny.core.admin;

import at.darkdestiny.core.admin.techtree.ConstructionTechTreeEntry;
import at.darkdestiny.core.admin.techtree.GroundTroopTechTreeEntry;
import at.darkdestiny.core.admin.techtree.ModulTechTreeEntry;
import at.darkdestiny.core.admin.techtree.TechTree;
import at.darkdestiny.core.admin.techtree.TechnologieTechTreeEntry;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.util.DebugBuffer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Hiermit kann der TechnologieBaum dargestellt werden und auch verändert werden
 *
 *
 * @author martin
 *
 */
public class TechTreeAdmin {

	/**
	 * Dies sind die konstanten für den action parameter
	 */
	public static final int ACTION_FULL_TREE = 0;

	public static final int ACTION_ONE_ITEM = 1;

	public static final int ACTION_ADD_DEPENDENCY = 2;

	public static final int ACTION_DELETE_DEPENDENCY = 3;

	public static final int ACTION_DELETE_TECH = 4;


	/**
	 * Wenn das hier gesetzt ist, ist es möglich diese Seite ohne Authentifizierung zu benutzen
	 */
	public static boolean DEBUG = false;


	/**
	 * Wohin sollen die Links zeigen?
	 */
	public static String BASEPATH = "main.jsp?page=techtree&";

	/**
	 * Der ganze Technologiebaum
	 */
	public static ArrayList<TechTree> techTree = null;

	/**
	 * Eine Map aller Technologien nach Ihrerer ID
	 */
	public static TreeMap<Integer, TechTree> allTechnologies = null;

	/**
	 *
	 */
	public TechTreeAdmin() {
	}

	/**
	 * Darf dieser Benutzer den Technologiebaum bearbeiten?
	 * @param au eine Beschreibung des Benutzers
	 * @throws Exception wenn der Benutzer dieses Interface nicht benutzen darf
	 */
	public void checkUser(AdminUser au) throws Exception {
		if (!au.isTechTreeAdmin())
			throw new Exception(
					"Du darfst den Technologiebaum nicht bearbeiten");
	}

	/**
	 * Den Technologiebaum laden (nur wenn er noch nicht geladen wurde)
	 * @throws SQLException
	 */
	public static void loadTechTree() throws SQLException {
		if (techTree != null)
			return;
		allTechnologies = new TreeMap<Integer, TechTree>();
		techTree = new ArrayList<TechTree>();

		loadResearch();
		loadConstruction();
		loadModules();
		loadGroundTroops();

		Statement stmt = DbConnect.createStatement();
		ResultSet rs = stmt
				.executeQuery("SELECT sourceID,reqConstructionID,reqResearchID,treeType FROM techrelation");

		while (rs.next()) {
			TechTree source = null;
			TechTree requirement;
			if (rs.getInt(2) != 0)
				requirement = getConstruction(rs.getInt(2));
			else
				requirement = getTechnologie(rs.getInt(3));
			switch (rs.getInt(4)) {
			case 1: // Technologie -> Technologie
			{
				source = getTechnologie(rs.getInt(1));
				break;
			}
			case 2: // Modul -> Technologie/Gebäude
			{
				source = getModul(rs.getInt(1));
				break;
			}
			case 0: // Gebäude -> Technologie
			{
				source = getConstruction(rs.getInt(1));
				break;
			}
			case 3: // Technologie -> Bodentruppen
			{
				source = getGroundTroup(rs.getInt(1));
				break;
			}
			}
			source.addRequirement(requirement);
		}

		for (TechTree tt : allTechnologies.values()) {
			if (tt.getDependsOn().isEmpty())
				techTree.add(tt);
		}
	}

	/**
	 * Alle Forschngen aus der Datenbank holen
	 * @param con
	 */
	private static void loadResearch() {
		try {
			// Eintrag aus DB holen
			Statement stmt = DbConnect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM research");

			while (rs.next()) {
				allTechnologies.put(TechnologieTechTreeEntry.makeID(rs.getInt(1)), new TechnologieTechTreeEntry(rs));
			}
		} catch (Exception e) {
			DebugBuffer.writeStackTrace("Error in TechTreeAdmin, loadResearch: ",e);
		}
	}

	/**
	 * Alle Gebäude aus der Datenbank holen
	 * @param con
	 */
	private static void loadConstruction() {
		try {
			// Eintrag aus DB holen
			Statement stmt = DbConnect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT ID FROM construction");

			while (rs.next()) {
				allTechnologies.put(ConstructionTechTreeEntry.makeID(rs.getInt(1)), new ConstructionTechTreeEntry(rs.getInt(1)));
			}
		} catch (SQLException e) {
			DebugBuffer.writeStackTrace("Error in TechTreeAdmin, loadConstruction: ",e);
		}
	}

	/**
	 * Alle Module aus der Datenbank holen
	 * @param con
	 */
	private static void loadModules() {
		try {
			// Eintrag aus DB holen
			Statement stmt = DbConnect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM module");

			while (rs.next()) {
				allTechnologies.put(ModulTechTreeEntry.makeID(rs.getInt(1)), new ModulTechTreeEntry(rs));
			}
		} catch (SQLException e) {
			DebugBuffer.writeStackTrace("Error in TechTreeAdmin, loadModule: ",e);
		}
	}

	/**
	 * Alle Bodentruppen aus der Datenbank holen
	 * @param con
	 */
	private static void loadGroundTroops() {
		try {
			// Eintrag aus DB holen
			Statement stmt = DbConnect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM groundtroops");

			while (rs.next()) {
				allTechnologies.put(GroundTroopTechTreeEntry.makeID(rs.getInt(1)), new GroundTroopTechTreeEntry(rs));
			}
		} catch (SQLException e) {
			DebugBuffer.writeStackTrace("Error in TechTreeAdmin, loadGroundToops: ",e);
		}
	}

	private static TechTree getConstruction(int int1) {
		int id = ConstructionTechTreeEntry.makeID(int1);
		if (allTechnologies.get(id) != null)
			return allTechnologies.get(id);

		TechTree tmp = new ConstructionTechTreeEntry(int1);
		allTechnologies.put(id, tmp);
		return tmp;
	}

	private static TechTree getModul(int int1) {
		int id = ModulTechTreeEntry.makeID(int1);
		if (allTechnologies.get(id) != null)
			return allTechnologies.get(id);

		TechTree tmp = new ModulTechTreeEntry(int1);
		allTechnologies.put(id, tmp);
		return tmp;
	}

	private static TechTree getGroundTroup(int int1) {
		int id = GroundTroopTechTreeEntry.makeID(int1);
		if (allTechnologies.get(id) != null)
			return allTechnologies.get(id);

		TechTree tmp = new GroundTroopTechTreeEntry(int1);
		allTechnologies.put(id, tmp);
		return tmp;
	}

	/**
	 * Eine Forschung aus dem Technologie-Baum holen.
	 * Wenn es sie nicht gibt, einen Eintrag anzeigen ..
	 * @param int1 die ID der Technologie
	 * @return nie null
	 */
	private static TechTree getTechnologie(int int1) {
		int id = TechnologieTechTreeEntry.makeID(int1);
		if (allTechnologies.get(id) != null)
			return allTechnologies.get(id);

		TechTree tmp = new TechnologieTechTreeEntry(id);
		allTechnologies.put(id, tmp);
		return tmp;
	}

	/**
	 * Die Technologie tt direkt anzeigen (gibt den vollständigen Link zurück)
	 * @param tt
	 * @return den HTML Code
	 */
	public static String makeLink(TechTree tt) {
		return "<a href=\"" + BASEPATH + "id=" + tt.getID() + "&action="
				+ ACTION_ONE_ITEM + "\">" + tt.getTyp() + " \"" + tt.getName()
				+ "\"</a>";
	}

	/**
	 * Einen Link erzeugen
	 * @param Name der Text, der angezeigt wird
	 * @param action welche Aktion soll durchgeführt werden?
	 * @param additionalParams zusätzliche Parameter, ohne führendes "&"
	 * @return den HTML-Code dafür
	 */
	public static String makeLink(String Name, int action,
			String additionalParams) {
		return "<a href=\"" + BASEPATH + "action=" + action + "&"
				+ additionalParams + "\">" + Name + "</a>";
	}

	/**
	 * Einen TechTree-Eintrag nach ID holen
	 * @param id die ID des Eintrags
	 * @return den entsprechenden Eintrag
	 */
	public static TechTree getTreeItemByID(int id) {
		return allTechnologies.get(id);
	}

	/**
	 * Erzwingt ein neu-Laden des Technologiebaums
	 */
	public static void forceReload() {
		techTree = null;
	}

}
