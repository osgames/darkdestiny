/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.notification;

import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.dao.MessageDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Bullet
 */
public abstract class Notification implements INotification {
    public static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    public static MessageDAO messageDAO = (MessageDAO) DAOFactory.get(MessageDAO.class);

    protected int userId;
    protected GenerateMessage gm = new GenerateMessage(true);

    public Notification(int userId) {
        this.userId = userId;

        gm.setMessageType(EMessageType.SYSTEM);

        gm.setSourceUserId(uDAO.getSystemUser().getUserId());
        gm.setDestinationUserId(userId);
    }

    @Override
    public void persistMessage() {
        gm.writeMessageToUser();
    }
}
