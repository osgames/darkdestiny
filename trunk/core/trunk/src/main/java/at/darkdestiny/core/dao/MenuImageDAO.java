/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.MenuImage;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Eobane
 */
public class MenuImageDAO extends ReadWriteTable<MenuImage> implements GenericDAO {

    public MenuImage findById(int id) {
        MenuImage menuImage = new MenuImage();
        menuImage.setId(id);
        return get(menuImage);
    }
}
