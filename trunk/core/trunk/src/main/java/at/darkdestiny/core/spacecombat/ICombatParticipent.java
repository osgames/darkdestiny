/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

/**
 *
 * @author Stefan
 */
public interface ICombatParticipent {
    public Integer getId();
    public Integer getUserId();
    public String getName();
}
