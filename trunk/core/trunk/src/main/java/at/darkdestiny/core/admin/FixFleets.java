/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin;

import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.FleetDetailDAO;
import at.darkdestiny.core.dao.FleetLoadingDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.FleetDetail;
import at.darkdestiny.core.model.FleetLoading;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class FixFleets {
    private static final Logger log = LoggerFactory.getLogger(FixFleets.class);

    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO)DAOFactory.get(PlayerFleetDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO)DAOFactory.get(FleetDetailDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO)DAOFactory.get(ShipFleetDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO)DAOFactory.get(ShipDesignDAO.class);
    private static UserDAO uDAO = (UserDAO)DAOFactory.get(UserDAO.class);
    private static ActionDAO aDAO = (ActionDAO)DAOFactory.get(ActionDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO)DAOFactory.get(FleetLoadingDAO.class);

    public static void fixFleets() {
        ArrayList<PlayerFleet> pfList = pfDAO.findAll();

        for (PlayerFleet pf : pfList) {
            log.info( "Checking fleet " + pf.getName() + " ("+pf.getId()+")");

            if (pf.getName().equalsIgnoreCase("Attacker Fleet") ||
                pf.getName().equalsIgnoreCase("Defender Fleet")) {
                log.info("Found testing fleet .. removing");

                pfDAO.remove(pf);
                FleetDetail fd = fdDAO.findByFleetId(pf.getId());
                ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(pf.getId());
                Action a = null;
                if (fd != null) fdDAO.remove(fd);
                if (a != null) aDAO.remove(a);
                for (ShipFleet sf : sfList) {
                    sfDAO.remove(sf);
                }

                continue;
            }

            // Check ShipFleet Data
            ArrayList<ShipFleet> sfData = sfDAO.findByFleetId(pf.getId());
            for (ShipFleet sf : sfData) {
                ShipDesign sd = sdDAO.findById(sf.getDesignId());

                if (sd == null) {
                    log.info("Deleting non existant design ("+sf.getDesignId()+") from fleet");
                    sfDAO.remove(sf);

                    ArrayList<ShipFleet> sfData2 = sfDAO.findByFleetId(pf.getId());
                    if (sfData2.isEmpty()) {
                        log.info("Fleet has no remaining ships .. cleaning up");

                        FleetDetail fd = fdDAO.findByFleetId(pf.getId());
                        Action a = null;

                        if (fd != null) {
                            a = aDAO.findFlightEntryByFleetDetail(fd.getId());
                        }

                        pfDAO.remove(pf);
                        if (fd != null) fdDAO.remove(fd);
                        if (a != null) aDAO.remove(a);
                    }
                }
            }

            // Check fleet status
            if (pf.getStatus() == 0) {
                // Check if there are fleetdetails for a stationary fleet
                FleetDetail fd = fdDAO.findByFleetId(pf.getId());
                if (fd != null) {
                    log.info("Found moving information for non moving fleet .. cleaning up");
                    Action a = aDAO.findFlightEntryByFleetDetail(fd.getId());
                    if (a != null) {
                        aDAO.remove(a);
                    }

                    fdDAO.remove(fd);
                }

                if ((pf.getSystemId() == 0) && (pf.getPlanetId() == 0)) {
                    log.info("Fleet had system 0 bug .. restoring fleet to homeplanet");
                    if (!setFleetToHomePlanet(pf)) continue;
                }
            } else if (pf.getStatus() == 1) {
                boolean error = false;

                FleetDetail fd = fdDAO.findByFleetId(pf.getId());
                if (fd != null) {
                    Action a = aDAO.findFlightEntryByFleetDetail(fd.getId());
                    if (a == null) {
                        error = true;
                    }
                } else {
                    error = true;
                }

                if (error) {
                     log.info("Moving fleet had incorrect flight data .. restoring fleet to homeplanet");
                     if (!setFleetToHomePlanet(pf)) continue;
                }
            }
        }

        // Checking for shipfleet Entries without any Fleet assigned
        ArrayList<ShipFleet> sfList = sfDAO.findAll();
        for (ShipFleet sf : sfList) {
            PlayerFleet pf = pfDAO.findById(sf.getFleetId());
            if (pf == null) {
                log.info("Deleting shipfleet entry with no assigned fleet");
                sfDAO.remove(sf);
            }
        }

        // Checking for abandoned fleet loading
        ArrayList<FleetLoading> flList = flDAO.findAll();
        for (FleetLoading fl : flList) {
            PlayerFleet pf = pfDAO.findById(fl.getFleetId());
            if (pf == null) {
                log.info("Deleting fleetloading entry with no assigned fleet");
                flDAO.remove(fl);
            }
        }

        // Check for abandoned Action entries
        ArrayList<Action> aList = aDAO.findAll();
        for (Action a : aList) {
            if (a.getType() != EActionType.FLIGHT) continue;

            FleetDetail fd = fdDAO.findById(a.getRefTableId());
            if (fd == null) {
                log.info("Deleting flight actionentry with no fleetdetail assigned");
                aDAO.remove(a);
            }
        }

        // Check for abandoned FleetDetails
        ArrayList<FleetDetail> fdList = fdDAO.findAll();
        for (FleetDetail fd : fdList) {
            PlayerFleet pf = pfDAO.findById(fd.getFleetId());
            if (pf == null) {
                log.info("Deleting fleetdetail entry with no assigned fleet");
                fdDAO.remove(fd);
                Action a = aDAO.findFlightEntryByFleetDetail(fd.getId());
                if (a != null) {
                    aDAO.remove(a);
                }
            }
        }
    }

    private static boolean setFleetToHomePlanet(PlayerFleet pf) {
        PlayerPlanet pp = ppDAO.findHomePlanetByUserId(pf.getId());

        if (pp == null) {
            log.warn("No homeplanet found for player "+pf.getUserId());
            if (pf.getUserId() == 0) {
                log.info("User Id is invalid, deleting fleet");
                FleetDetail fd = fdDAO.findByFleetId(pf.getId());
                Action a = null;

                if (fd != null) {
                    a = aDAO.findFlightEntryByFleetDetail(fd.getId());
                }

                pfDAO.remove(pf);
                if (fd != null) fdDAO.remove(fd);
                if (a != null) aDAO.remove(a);

                ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(pf.getId());
                for (ShipFleet sf : sfList) {
                    sfDAO.remove(sf);
                }

                return false;
            } else {
                log.info("Trying to assign to any other planet of this player");
                ArrayList<PlayerPlanet> ppList = ppDAO.findByUserId(pf.getUserId());
                if (ppList.isEmpty()) {
                    log.warn("No planet found ... deleting fleet");
                    pfDAO.remove(pf);
                    FleetDetail fd = fdDAO.findByFleetId(pf.getId());
                    ArrayList<ShipFleet> sfList = sfDAO.findByFleetId(pf.getId());
                    Action a = null;
                    if (fd != null) fdDAO.remove(fd);
                    if (a != null) aDAO.remove(a);
                    for (ShipFleet sf : sfList) {
                        sfDAO.remove(sf);
                    }
                    return false;
                } else {
                    pp = ppList.get(0);
                    RelativeCoordinate rc = new RelativeCoordinate(0,pp.getPlanetId());

                    pf.setLocation(ELocationType.PLANET, rc.getPlanetId());
                    // pf.setStatus(0);
                    // pf.setPlanetId(rc.getPlanetId());
                    // pf.setSystemId(rc.getSystemId());
                    pfDAO.update(pf);
                    return true;
                }
            }
        } else {
            RelativeCoordinate rc = new RelativeCoordinate(0,pp.getPlanetId());
            pf.setLocation(ELocationType.PLANET, rc.getPlanetId());
            // pf.setStatus(0);
            // pf.setPlanetId(rc.getPlanetId());
            // pf.setSystemId(rc.getSystemId());
            pfDAO.update(pf);
            return true;
        }
    }
}
