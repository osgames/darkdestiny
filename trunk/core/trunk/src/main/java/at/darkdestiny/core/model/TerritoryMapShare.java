/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.ETerritoryMapShareType;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "territorymapshare")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TerritoryMapShare extends Model<TerritoryMapShare> {

    @FieldMappingAnnotation("territoryId")
    @IdFieldAnnotation
    @IndexAnnotation(indexName = "territoryMapShare")
    private Integer territoryId;
    @FieldMappingAnnotation("refId")
    @IndexAnnotation(indexName = "territoryMapShare")
    @IdFieldAnnotation
    private Integer refId;
    @FieldMappingAnnotation("type")
    @IdFieldAnnotation
    @IndexAnnotation(indexName = "territoryMapShare")
    private ETerritoryMapShareType type;
    @FieldMappingAnnotation("showOnStarmap")
    private Boolean show;

    /**
     * @return the territoryId
     */
    public Integer getTerritoryId() {
        return territoryId;
    }

    /**
     * @param territoryId the territoryId to set
     */
    public void setTerritoryId(Integer territoryId) {
        this.territoryId = territoryId;
    }

    /**
     * @return the show
     */
    public Boolean getShow() {
        return show;
    }

    /**
     * @param show the show to set
     */
    public void setShow(Boolean show) {
        this.show = show;
    }

    /**
     * @return the type
     */
    public ETerritoryMapShareType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ETerritoryMapShareType type) {
        this.type = type;
    }

    /**
     * @return the refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }
}
