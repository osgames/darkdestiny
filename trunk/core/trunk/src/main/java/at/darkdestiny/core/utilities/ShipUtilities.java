/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.dao.DamagedShipsDAO;
import at.darkdestiny.core.dao.FleetLoadingDAO;
import at.darkdestiny.core.dao.GroundTroopDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.fleet.FleetData;
import at.darkdestiny.core.fleet.LoadingInformation;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.DamagedShips;
import at.darkdestiny.core.model.FleetLoading;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.service.FleetService;
import static at.darkdestiny.core.service.FleetService.getAttachedStations;
import static at.darkdestiny.core.service.FleetService.shipsInHangar;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.ships.ShipRepairCost;
import at.darkdestiny.core.ships.ShipScrapCost;
import at.darkdestiny.core.ships.ShipStatusEntry;
import at.darkdestiny.core.ships.ShipStorage;
import at.darkdestiny.core.ships.ShipUpgradeCost;
import at.darkdestiny.core.view.ShipModifyEntry;
import at.darkdestiny.core.view.ShipModifyView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ShipUtilities {

    private static final Logger log = LoggerFactory.getLogger(ShipUtilities.class);

    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    private static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    private static GroundTroopDAO gtDAO = (GroundTroopDAO) DAOFactory.get(GroundTroopDAO.class);

    public static boolean canBeScrapped(Model ship, int userId, int fleetId, int count) {
        ShipDesign sd = (ShipDesign) ship;

        if (sd.getUserId() != userId) {
            return false;
        }

        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);
        boolean found = false;

        for (ShipData sData : pfe.getShipList()) {
            if (sData.getDesignId() == sd.getId()) {
                if (sData.getCount() >= count) {
                    found = true;
                }
            }
        }

        if (!found) {
            return false;
        }

        UserData ud = udDAO.findByUserId(userId);
        ShipDesignExt sde = new ShipDesignExt(sd.getId());
        ShipScrapCost ssc = sde.getShipScrapCost(count);

        if (ud.getCredits() < ssc.getRess(Ressource.CREDITS)) {
            return false;
        }

        return true;
    }

    public static BaseResult canBeRepaired(int userId, int shipFleetId, HashMap<EDamageLevel, Integer> toRepair) {
        BaseResult result = null;

        log.debug("SFID=" + shipFleetId);
        ShipFleet sfEntry = sfDAO.getById(shipFleetId);
        PlayerFleet pf = pfDAO.findById(sfEntry.getFleetId());
        PlayerFleetExt pfe = new PlayerFleetExt(pf.getId());

        if (pf.getUserId() != userId) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 1 (" + userId + "<>" + pf.getId() + ")");
            return new BaseResult("Ung&uuml;ltiger Benutzer", true);
        }

        ShipModifyView smv = getModifiableShips(pf.getId());
        boolean found = false;

        for (ShipModifyEntry sme : smv.getShipList()) {
            if (sme.getDesignId() == sfEntry.getDesignId()) {
                found = true;
                break;
            }
        }

        if (!found) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 2");
            return new BaseResult("Schiff nicht gefunden", true);
        }

        ArrayList<DamagedShips> damagedShips = dsDAO.findById(shipFleetId);

        for (Map.Entry<EDamageLevel, Integer> dEntry : toRepair.entrySet()) {
            found = false;
            for (DamagedShips ds : damagedShips) {
                if (ds.getDamageLevel() == dEntry.getKey()) {
                    if (ds.getCount() >= dEntry.getValue()) {
                        found = true;
                        break;
                    }
                }
            }

            if (!found) {
                DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 3 (Ship not available for repair)");
                return new BaseResult("Schiff nicht gefunden", true);
            }
        }

        ShipDesignExt sde = new ShipDesignExt(sfEntry.getDesignId());
        ShipDesign sd = (ShipDesign) sde.getBase();

        ShipRepairCost src = new ShipRepairCost(sde);
        RessourcesEntry re = src.getTotalCosts(toRepair);

        MutableRessourcesEntry mre = new MutableRessourcesEntry(userId, pfe.getRelativeCoordinate().getPlanetId());
        mre.subtractRess(re, 1);

        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getQty() < 0) {
                DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 4 With ress " + rae.getRessId() + " >> " + rae.getQty());
                Ressource r = rDAO.findRessourceById(rae.getRessId());
                return new BaseResult(FormatUtilities.getFormattedNumber(Math.abs(rae.getQty())) + " zuwenig " + ML.getMLStr(r.getName(), userId), true);
            }
        }

        return new BaseResult("", false);
    }

    public static boolean canBeUpgraded(Model from, Model to, int userId, int fleetId, int count) {
        ShipDesign SDFrom = (ShipDesign) from;
        ShipDesign SDTo = (ShipDesign) to;

        if (SDFrom.getUserId() != userId) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 1 (" + userId + "<>" + SDFrom.getUserId() + ")");
            return false;
        }

        if (!SDFrom.getUserId().equals(SDTo.getUserId())) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 2");
            return false;
        }

        ShipModifyView smv = getModifiableShips(fleetId);
        boolean found = false;

        for (ShipModifyEntry sme : smv.getShipList()) {
            if (sme.getDesignId() == SDFrom.getId()) {
                if ((sme.getCount() - sme.getDamagedCount()) >= count) {
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 3");
            return false;
        }

        ShipDesignExt SDEFrom = new ShipDesignExt(SDFrom.getId());
        ShipDesignExt SDETo = new ShipDesignExt(SDTo.getId());

        ShipUpgradeCost suc = SDEFrom.getShipUpgradeCost(SDETo, count);
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);

        MutableRessourcesEntry mre = new MutableRessourcesEntry(userId, pfe.getRelativeCoordinate().getPlanetId());
        RessourcesEntry re = new RessourcesEntry(suc.getRess());
        mre.subtractRess(re, 1d);

        for (RessAmountEntry rae : mre.getRessArray()) {
            if (rae.getQty() < 0) {
                DebugBuffer.addLine(DebugLevel.TRACE, "FAIL 4 With ress " + rae.getRessId() + " >> " + rae.getQty());
                return false;
            }
        }

        return true;
    }

    public static ShipModifyView getModifiableShips(int fleetId) {
        return getModifiableShipsInternal(fleetId, -1);
    }

    public static ShipModifyView getModifiableShips(int fleetId, int designId) {
        return getModifiableShipsInternal(fleetId, designId);
    }

    // TODO Check for fleetloading / hangar / attached stations
    private static ShipModifyView getModifiableShipsInternal(int fleetId, int designId) {
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);
        int planetId = pfe.getRelativeCoordinate().getPlanetId();

        boolean hasOrbDock = (pcDAO.findBy(planetId, Construction.ID_ORBITAL_SHIPYARD) != null);
        boolean hasDock = (pcDAO.findBy(planetId, Construction.ID_PLANETARYSHIPYARD) != null);
        boolean hasModule = (pcDAO.findBy(planetId, Construction.ID_MODULEFACTORY) != null);

        if (fleetHasDockedShipsOrStations(fleetId)) {
            return new ShipModifyView(fleetId);
        }

        boolean canUseShipyard = FleetService.canUseShipyard(pfe.getUserId(), pfe.getRelativeCoordinate().getPlanetId());

        ShipModifyView smv = new ShipModifyView(fleetId);
        ArrayList<ShipData> sdList = pfe.getShipList();

        if (designId != -1) {
            for (Iterator<ShipData> sdIt = sdList.iterator(); sdIt.hasNext();) {
                ShipData sd = sdIt.next();
                if (sd.getDesignId() != designId) {
                    sdIt.remove();
                }
            }
        }

        ArrayList<FleetLoading> flList = flDAO.findByFleetId(fleetId);
        boolean checkForCap = !flList.isEmpty();

        boolean hasTroops = false;
        boolean hasResources = false;

        if (checkForCap) {
            for (FleetLoading fl : flList) {                
                if ((fl.getLoadType() == FleetLoading.LOADTYPE_TROOPS) || (fl.getLoadType() == FleetLoading.LOADTYPE_POPULATION)) {
                    hasTroops = true;
                }
                if (fl.getLoadType() == FleetLoading.LOADTYPE_RESS) {
                    hasResources = true;
                }
            }
        }
        
        for (ShipData sd : sdList) {
            ShipModifyEntry sme = new ShipModifyEntry();
            sme.setShipData(sd);

            boolean canBeModified = true;
            boolean canBeScrapped = true;

            if (!hasModule) {
                canBeModified = false;
            }

            if (sd.getChassisSize() > 5) {
                if (!hasOrbDock) {
                    canBeModified = false;
                    canBeScrapped = false;
                }
            } else if (!hasDock) {
                canBeModified = false;
                canBeScrapped = false;
            }
            //If other players shipyard
            if (!canUseShipyard) {
                canBeModified = false;
                canBeScrapped = false;
            }

            sme.setCanBeModified(canBeModified);
            sme.setCanBeScrapped(canBeScrapped);

            for (ShipStatusEntry sse : sd.getShipDataStatus()) {
                if ((sse.getCount() > 0) && (sse.getStatus() != EDamageLevel.NODAMAGE)) {
                    sme.setDamagedCount(sme.getDamagedCount() + sse.getCount());
                    sme.setCanBeRepaired(true);
                    continue;
                }
            }

            if ((sme.getCount() - sme.getDamagedCount()) <= 0) {
                sme.setCanBeModified(false);
            }

            // Check if ships have loading
            if (checkForCap) {
                ShipDesign sdTmp = sdDAO.findById(sd.getDesignId());
                if ((sdTmp.getRessSpace() > 0) && hasResources) {
                    sme.setBlockedByLoading(true);
                    sme.setCanBeModified(false);
                    sme.setCanBeRepaired(false);
                    sme.setCanBeScrapped(false);
                }
                if ((sdTmp.getTroopSpace() > 0) && hasTroops) {
                    sme.setBlockedByLoading(true);
                    sme.setCanBeModified(false);
                    sme.setCanBeRepaired(false);
                    sme.setCanBeScrapped(false);
                }
            }
                        
            // Check if design has docked stations or ships in hangar
            boolean fleetHasAttachedStations = !getAttachedStations(fleetId).isEmpty();
            ShipDesign sdTmp = sdDAO.findById(sd.getDesignId());

            System.out.println("Check fleet " + fleetId + " and design " + sdTmp.getId());
            System.out.println("Check docked stations -- " + fleetHasAttachedStations);
            if (fleetHasAttachedStations && sdTmp.getHasTractorBeam()) {
                sme.setBlockedByLoading(true);
                sme.setCanBeModified(false);
                sme.setCanBeRepaired(false);
                sme.setCanBeScrapped(false);
            }

            System.out.println("Check fleets in Hangar -- " + shipsInHangar(fleetId,sdTmp.getId()));
            if (shipsInHangar(fleetId,sdTmp.getId()) && (sdTmp.getLargeHangar() + sdTmp.getMediumHangar() + sdTmp.getSmallHangar()) > 0) {
                sme.setBlockedByLoading(true);
                sme.setCanBeModified(false);
                sme.setCanBeRepaired(false);
                sme.setCanBeScrapped(false);
            }            
            
            smv.addEntry(sme);
        }

        return smv;
    }

    private static boolean fleetHasDockedShipsOrStations(int fleetId) {
        FleetData fdSource = new FleetData(fleetId);

        fdSource = at.darkdestiny.core.ships.ShipUtilities.loadLoadingInformation(fdSource);
        if ((fdSource.getCurrLoadedLargeHangar() > 0) || (fdSource.getCurrLoadedMediumHangar() > 0) || (fdSource.getCurrLoadedSmallHangar() > 0)) {
            return true;
        }

        return FleetService.getAttachedStations(fdSource.getFleetId()).size() > 0;                 
    }

    public static ShipModifyEntry getShipModifyEntry(int designId, int planetId) {
        ShipDesign sd = sdDAO.findById(designId);
        return getShipModifyEntry(sd, planetId);
    }

    public static ShipModifyEntry getShipModifyEntry(ShipDesign sd, int planetId) {
        boolean hasOrbDock = (pcDAO.findBy(planetId, Construction.ID_ORBITAL_SHIPYARD) != null);
        boolean hasDock = (pcDAO.findBy(planetId, Construction.ID_PLANETARYSHIPYARD) != null);
        boolean hasModule = (pcDAO.findBy(planetId, Construction.ID_MODULEFACTORY) != null);

        ShipModifyEntry sme = new ShipModifyEntry();
        ArrayList<ShipData> shipList = new ArrayList<ShipData>();

        PlanetDefense pd = pdDAO.findBy(planetId, sd.getId(), EDefenseType.SPACESTATION);

        ShipData sda = new ShipData();
        sda.setDesignId(sd.getId());
        sda.setDesignName(sd.getName());
        sda.setCount(pd.getCount());
        sda.setChassisSize(sd.getChassis());
        sda.setLoaded(false);

        shipList.add(sda);

        sme.setShipData(sda);

        boolean canBeModified = true;
        boolean canBeScrapped = true;

        if (!hasModule) {
            canBeModified = false;
        }

        if (sda.getChassisSize() > 5) {
            if (!hasOrbDock) {
                canBeModified = false;
                canBeScrapped = false;
            }
        } else if (!hasDock) {
            canBeModified = false;
            canBeScrapped = false;
        }

        sme.setCanBeModified(canBeModified);
        sme.setCanBeScrapped(canBeScrapped);
        return sme;
    }

    public static void checkExcessFleetLoading(int fleetId) {
        PlayerFleetExt pfe = new PlayerFleetExt(fleetId);

        LoadingInformation li = pfe.getLoadingInformation();
        if ((li.getCurrLoadedTroops() + li.getCurrLoadedPopulation()) > li.getTroopSpace()) {
            // Rescale Troops
            ArrayList<ShipStorage> ssList = li.getLoadedStorage();

            for (Iterator<ShipStorage> ssIt = ssList.iterator(); ssIt.hasNext();) {
                ShipStorage ss = ssIt.next();
                if (!((ss.getLoadtype() == ShipStorage.LOADTYPE_GROUNDTROOP) || (ss.getLoadtype() == ShipStorage.LOADTYPE_POPULATION))) {
                    ssIt.remove();
                }
            }

            // Now that we have cleaned the list of unnecessary stuff
            // lets come to the fun part and destroy random stuff
            int stuffToDestroy = (li.getCurrLoadedTroops() + li.getCurrLoadedPopulation()) - li.getTroopSpace();

            while (stuffToDestroy > 0) {
                // Pick a random element
                ShipStorage ss = ssList.get((int) Math.floor(ssList.size() * Math.random()));

                int loadingSize = 1;
                if (ss.getLoadtype() == ShipStorage.LOADTYPE_GROUNDTROOP) { // Get the size of this
                    GroundTroop gt = gtDAO.findById(ss.getId());
                    loadingSize = gt.getSize();
                }

                int maxToDestroy = Math.min((int) Math.ceil(ss.getCount() / loadingSize), (int) Math.ceil(stuffToDestroy / loadingSize));
                if (maxToDestroy < (int) Math.ceil(ss.getCount() / loadingSize)) {
                    maxToDestroy += 1;
                }

                if ((maxToDestroy < 1) && (ss.getCount() > 0)) {
                    maxToDestroy = 1;
                }

                int destroyCountTmp = (int) Math.ceil(Math.random() * maxToDestroy);
                int destroyCount = Math.min(maxToDestroy, destroyCountTmp);

                stuffToDestroy -= destroyCount * loadingSize;
                ss.setCount(ss.getCount() - destroyCount);

                log.debug("Destroy " + destroyCount + " " + ss.getName());
            }

            // Store changes
            for (ShipStorage ss : ssList) {
                flDAO.setNewLoadingAmount(fleetId, ss.getLoadtype(), ss.getId(), ss.getCount());
            }
        }

        pfe = new PlayerFleetExt(fleetId);
        li = pfe.getLoadingInformation();
        if (li.getCurrLoadedRess() > li.getRessSpace()) {
            // Rescale Ress
            ArrayList<ShipStorage> ssList = li.getLoadedStorage();

            for (Iterator<ShipStorage> ssIt = ssList.iterator(); ssIt.hasNext();) {
                ShipStorage ss = ssIt.next();
                if (ss.getLoadtype() != ShipStorage.LOADTYPE_RESSOURCE) {
                    ssIt.remove();
                }
            }

            int stuffToDestroy = li.getCurrLoadedRess() - li.getRessSpace();

            while (stuffToDestroy > 0) {
                // Pick a random element
                ShipStorage ss = ssList.get((int) Math.floor(ssList.size() * Math.random()));

                int maxToDestroy = Math.min(ss.getCount(), stuffToDestroy);
                int destroyCountTmp = (int) Math.ceil(Math.random() * maxToDestroy);
                int destroyCount = Math.min(maxToDestroy, destroyCountTmp);

                stuffToDestroy -= destroyCount;
                ss.setCount(ss.getCount() - destroyCount);

                log.debug("Destroy " + destroyCount + " " + ss.getName());
            }

            // Store changes
            for (ShipStorage ss : ssList) {
                flDAO.setNewLoadingAmount(fleetId, ss.getLoadtype(), ss.getId(), ss.getCount());
            }
        }
    }
}
