/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.chart;

/**
 *
 * @author Stefan
 */
public class ChartException extends Exception {
    public ChartException(String message) {
        super(message);
    }
}
