/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.TradeOfferType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "pricelist")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PriceList extends Model<PriceList> {

    @FieldMappingAnnotation("id")
    @ColumnProperties(autoIncrement = true)
    @IdFieldAnnotation
    private Integer id;
    @FieldMappingAnnotation("userId")
    @IndexAnnotation(indexName = "offerIndex")
    private Integer userId;
    @FieldMappingAnnotation("listType")
    @DefaultValue("PUBLIC")
    @IndexAnnotation(indexName = "offerIndex")
    private TradeOfferType tradeOfferType;
    @FieldMappingAnnotation("refId")
    @DefaultValue("0")
    @IndexAnnotation(indexName = "offerIndex")
    private Integer refId;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the tradeOfferType
     */
    public TradeOfferType getTradeOfferType() {
        return tradeOfferType;
    }

    /**
     * @param tradeOfferType the tradeOfferType to set
     */
    public void setTradeOfferType(TradeOfferType tradeOfferType) {
        this.tradeOfferType = tradeOfferType;
    }

    /**
     * @return the refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }
}
