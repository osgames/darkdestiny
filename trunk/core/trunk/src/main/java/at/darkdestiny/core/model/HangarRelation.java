    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "hangarrelation")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class HangarRelation extends Model<HangarRelation> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("fleetId")
    @IndexAnnotation(indexName = "fleetId")
    private Integer fleetId;
    @FieldMappingAnnotation("designId")
    private Integer designId;
    @FieldMappingAnnotation("count")
    private Integer count;
    @FieldMappingAnnotation("relationToDesign")
    private Integer relationToDesign;
    @FieldMappingAnnotation("relationToSF")
    private Integer relationToSF;

    public Integer getFleetId() {
        return fleetId;
    }

    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    public Integer getDesignId() {
        return designId;
    }

    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getRelationToDesign() {
        return relationToDesign;
    }

    public void setRelationToDesign(Integer relationToDesign) {
        this.relationToDesign = relationToDesign;
    }

    public Integer getRelationToSF() {
        return relationToSF;
    }

    public void setRelationToSF(Integer relationToSF) {
        this.relationToSF = relationToSF;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
