/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.battlelog;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.interfaces.ISimulationSet;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.spacecombat.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class BattleLogSimulationSet implements ISimulationSet, Serializable {

    String battleClass;
    private final CombatReportGenerator.CombatType combatType;
    private  RelativeCoordinate relLocation;
    private  AbsoluteCoordinate absLocation;
    private final CombatReportGenerator crg;
    // <User, <FleetId, <Shipdesign, count>>>
    private HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants;
    private ArrayList<PlayerFleet> fleets = new ArrayList<PlayerFleet>();
    private ArrayList<ShipFleet> ships = new ArrayList<ShipFleet>();
    HashSet<Integer> genFleetIds = new HashSet<Integer>();
    HashMap<Integer, ArrayList<Integer>> groups = new HashMap<Integer, ArrayList<Integer>>();
    private final boolean HUShield;
    private final boolean PAShield;
    private final Integer planetOwner;
    private final HashMap<Integer, Integer> groundDefense;

    public BattleLogSimulationSet(HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants, HashMap<Integer, ArrayList<Integer>> groups, String battleClass, Integer planetOwner, boolean HUShield, boolean PAShield, HashMap<Integer, Integer> groundDefense, RelativeCoordinate relLocation, AbsoluteCoordinate absLocation) {
        this(participants, groups, battleClass, planetOwner, HUShield, PAShield, groundDefense, relLocation, absLocation, CombatReportGenerator.CombatType.SPACE);
    }
    public BattleLogSimulationSet(HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants, HashMap<Integer, ArrayList<Integer>> groups, String battleClass, Integer planetOwner, boolean HUShield, boolean PAShield, HashMap<Integer, Integer> groundDefense, RelativeCoordinate relLocation, AbsoluteCoordinate absLocation, CombatReportGenerator.CombatType combatType) {
        this.participants = participants;
        this.combatType = combatType;
        crg = new CombatReportGenerator();
        this.relLocation = relLocation;

        if (relLocation == null) {
            this.relLocation = new RelativeCoordinate(0, 2600);
        }
        this.absLocation = absLocation;
        if (absLocation == null) {
            this.absLocation = new AbsoluteCoordinate(0, 0);
        }
        this.battleClass = battleClass;
        this.groups = groups;
        this.HUShield = HUShield;
        this.PAShield = PAShield;
        this.planetOwner = planetOwner;
        this.groundDefense = groundDefense;
        generateFleets();
    }

    private void generateFleets() {
        for (Map.Entry<Integer, HashMap<Integer, HashMap<Integer, Integer>>> user : participants.entrySet()) {
            int userId = user.getKey();
            for (Map.Entry<Integer, HashMap<Integer, Integer>> fleet : user.getValue().entrySet()) {
                PlayerFleet pf = new PlayerFleet();
                pf = new PlayerFleet();
                pf.setUserId(userId);
                if(combatType.equals(CombatReportGenerator.CombatType.SYSTEM_COMBAT)){
                    pf.setLocation(ELocationType.SYSTEM, relLocation.getSystemId());
                }else if(combatType.equals(CombatReportGenerator.CombatType.ORBITAL_COMBAT)){
                    pf.setLocation(ELocationType.PLANET, relLocation.getPlanetId());
                }
                pf.setName("Flotte : " + fleet.getKey());
                pf = Service.playerFleetDAO.add(pf);
                genFleetIds.add(pf.getId());

                for (Map.Entry<Integer, Integer> designs : fleet.getValue().entrySet()) {
                    ShipFleet sf = new ShipFleet();
                    sf.setDesignId(designs.getKey());
                    sf.setFleetId(pf.getId());
                    sf.setCount(designs.getValue());
                    sf = Service.shipFleetDAO.add(sf);
                    ships.add(sf);
                }
                fleets.add(pf);
            }
        }
    }

    @Override
    public void cleanUp() {
        for (Integer i : genFleetIds) {
            PlayerFleet pf = new PlayerFleet();
            pf.setId(i);
            Service.playerFleetDAO.remove(pf);
        }
        for (ShipFleet sf : ships) {
            Service.shipFleetDAO.remove(sf);
        }
    }

    public CombatReportGenerator.CombatType getCombatType() {
        return combatType;
    }

    public RelativeCoordinate getRelLocation() {
        return relLocation;
    }

    public AbsoluteCoordinate getAbsLocation() {
        return absLocation;
    }

    public CombatReportGenerator getCombatReportGenerator() {
        return crg;
    }

    public ArrayList<PlayerFleet> getFleets() {
        return fleets;
    }

    public void addPlayer(int groupId, int userId) {
        ArrayList<Integer> users = groups.get(groupId);
        if (users == null) {
            users = new ArrayList<Integer>();
        }
        users.add(userId);
        groups.put(groupId, users);
    }

    @Override
    public HashMap<Integer, ArrayList<Integer>> getGroups() {
        return groups;
    }

    @Override
    public String getBattleClass() {
        return battleClass;
    }

    @Override
    public int getPlanetOwner() {
        return planetOwner;
    }

    @Override
    public boolean hasHUShield() {
        return HUShield;
    }

    @Override
    public boolean hasPAShield() {
        return PAShield;
    }

    @Override
    public HashMap<Integer, Integer> getGroundDefense() {
        return groundDefense;
    }
}
