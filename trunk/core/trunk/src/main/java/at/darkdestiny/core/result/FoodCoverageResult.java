/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

/**
 *
 * @author Bullet
 */
public class FoodCoverageResult {
    private float foodCoverage;
    private float foodCoverageNoStock;

    public FoodCoverageResult(float foodCoverage, float foodCoverageNoStock){
        this.foodCoverage = foodCoverage;
        this.foodCoverageNoStock = foodCoverageNoStock;
    }

    /**
     * @return the foodCoverage
     */
    public float getFoodCoverage() {
        return foodCoverage;
    }

    /**
     * @param foodCoverage the foodCoverage to set
     */
    public void setFoodCoverage(float foodCoverage) {
        this.foodCoverage = foodCoverage;
    }

    /**
     * @return the foodCoverageNoStock
     */
    public float getFoodCoverageNoStock() {
        return foodCoverageNoStock;
    }

    /**
     * @param foodCoverageNoStock the foodCoverageNoStock to set
     */
    public void setFoodCoverageNoStock(float foodCoverageNoStock) {
        this.foodCoverageNoStock = foodCoverageNoStock;
    }


}
