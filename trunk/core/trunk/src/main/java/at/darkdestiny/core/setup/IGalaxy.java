/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.setup;

import at.darkdestiny.core.result.GalaxyCreationResult;

/**
 *
 * @author Admin
 */
public interface IGalaxy {
    
    public GalaxyCreationResult create();
}
