/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.trade;

import at.darkdestiny.core.enumeration.EAddLocation;

/**
 *
 * @author Stefan
 */
public class ShipsToRouteEntry {
    private final int fleetId;
    private final EAddLocation addLoc;
    private final int designId;
    private final int count;
    
    public ShipsToRouteEntry(int fleetId, EAddLocation addLoc, int designId, int count) {
        this.fleetId = fleetId;
        this.addLoc = addLoc;
        this.designId = designId;
        this.count = count;
    }

    public int getFleetId() {
        return fleetId;
    }

    public int getDesignId() {
        return designId;
    }

    public int getCount() {
        return count;
    }

    public EAddLocation getAddLoc() {
        return addLoc;
    }
}
