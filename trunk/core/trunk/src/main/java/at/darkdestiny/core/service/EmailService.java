/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.dao.NewsletterDAO;
import at.darkdestiny.core.model.Newsletter;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author Stefan
 */
public class EmailService {
    private static String smtpHost = "mailout.inetmx.de";
    private static String user = "web495p1";
    private static String password = "XXXX"; //i
    private static String sender = "webmaster@thedarkdestiny.at";
    private static String receiver = "bacher.stefan@aon.at";
    private static String subject = "Dark Destiny - Neue Runde 8";

    private static NewsletterDAO nlDAO = (NewsletterDAO) DAOFactory.get(NewsletterDAO.class);

    public static void smtp(String receiver, String content) throws MessagingException {
        if (smtpHost == null) {
            throw new MessagingException("smtpHost not found");
        }
        if (user == null) {
            throw new MessagingException("user not found");
        }
        if (password == null) {
            throw new MessagingException("password not found");
        }
        Properties properties = new Properties();
        properties.put("mail.smtp.host", smtpHost); //set smtp server
        properties.put("mail.smtp.auth", "true"); //use smtp authen properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.port", "25");
        // properties.put("mail.smtp.starttls.enable", "true");
        Session session = Session.getDefaultInstance(properties,
                new Authenticator() {

                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user, password);
                    }
                });
        MimeMessage mimeMsg = new MimeMessage(session);
        if (sender != null) {
            DebugBuffer.addLine(DebugLevel.TRACE, "Set sender to " + sender);
            mimeMsg.setFrom(new InternetAddress(sender));
        }
        /*
        if (receiver != null) {
            DebugBuffer.addLine(DebugLevel.TRACE, "Set receiver to " + receiver);
            mimeMsg.setRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
        }
        */
        if (subject != null) {
            DebugBuffer.addLine(DebugLevel.TRACE, "Set subject to " + subject);
            mimeMsg.setSubject(subject, "ISO-8859-1");
        }
        MimeBodyPart part = new MimeBodyPart();
        part.setText(content == null ? "" : content, "ISO-8859-1");

        part.setContent(content.toString(), "text/plain;charset=ISO-8859-1");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(part);
        mimeMsg.setContent(multipart);
        mimeMsg.setSentDate(new Date());
        // Transport.send(mimeMsg);

        ArrayList<Newsletter> nlEntries = nlDAO.findByAllowedEmail();
        InternetAddress[] addresses = new InternetAddress[nlEntries.size()];
        int index = 0;

        String lastAddress = null;

        try {
            for (Newsletter nl : nlEntries) {
                lastAddress = nl.getEmail();
                addresses[index] = new InternetAddress(nl.getEmail());
                addresses[index].validate();
                index++;

                DebugBuffer.trace("Prepared address " + lastAddress);
            }
        } catch (Exception e) {
            DebugBuffer.error("Invalid address " + lastAddress);
        }

        Transport.send(mimeMsg, addresses);
    }
}
