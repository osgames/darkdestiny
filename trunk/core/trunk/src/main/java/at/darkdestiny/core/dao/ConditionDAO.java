/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.TitleCondition;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Bullet
 */
public class ConditionDAO extends ReadWriteTable<TitleCondition> implements GenericDAO {

    public TitleCondition findById(int id) {
        TitleCondition titleCondition = new TitleCondition();
        titleCondition.setId(id);
        return(TitleCondition)get(titleCondition);
    }

}
