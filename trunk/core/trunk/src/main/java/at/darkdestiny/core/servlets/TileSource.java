/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.interfaces.ITileGenerator;
import at.darkdestiny.core.painting.map.BufferedStarmapData;
import at.darkdestiny.core.painting.map.TileGeneratorJoinmap;
import at.darkdestiny.core.painting.map.TileGeneratorStarmap;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.util.DebugBuffer;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */
public class TileSource extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("image/png");
        ServletOutputStream out = response.getOutputStream();

        HttpSession session = request.getSession();
        
        try {
            List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();

            int x = 0;
            int y = 0;
            int zoomLevel = 0;
            int userId = 0;
            
            boolean noSession = false;
            boolean error = false;
            boolean joinMap = false;
            
            try {
                if (request.getParameter("x") != null) {
                    x = Integer.parseInt(request.getParameter("x"));
                }
                if (request.getParameter("y") != null) {
                    y = Integer.parseInt(request.getParameter("y"));
                }
                if (request.getParameter("z") != null) {
                    zoomLevel = Integer.parseInt(request.getParameter("z"));
                }    
                if (request.getParameter("joinmap") != null) {
                    joinMap = Integer.parseInt(request.getParameter("joinmap")) == 1;
                }                    
            } catch (Exception e) {
                error = true;
                System.out.println("Parameter error in getTile: " + e);
            }
            
            try {
                userId = Integer.parseInt((String) session.getAttribute("userId"));                        
            } catch (Exception e) {
                noSession = true;
                System.out.println("Session error in getTile: " + e);
            }
            
            if (joinMap) {
                BufferedStarmapData.getJoinMapData(userId);
                
                System.out.println("X: " + x + " Y: " + y + " Zoom: " + zoomLevel);
                
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ITileGenerator itg = new TileGeneratorJoinmap();

                BufferedImage bi = itg.createBufferedImage(0, zoomLevel, x, y);
                ImageIO.write(bi, "png", baos);

                out.write(baos.toByteArray());
                out.flush();                
            } else {            
                System.out.println("USERID: " + userId + " X: " + x + " Y: " + y + " Zoom: " + zoomLevel);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ITileGenerator itg = new TileGeneratorStarmap();

                BufferedImage bi = itg.createBufferedImage(userId, zoomLevel, x, y);
                ImageIO.write(bi, "png", baos);

                out.write(baos.toByteArray());
                out.flush();
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while generating galaxy pic: ", e);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
