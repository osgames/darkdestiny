/*
 * VoteUtilities.java
 *
 * Created on 15. Juli 2007, 11:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.core.voting;

import at.darkdestiny.core.*;
;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.transaction.TransactionHandler;
import at.darkdestiny.util.DebugBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.AllianceRankDAO;
import at.darkdestiny.core.dao.VoteMetadataDAO;
import at.darkdestiny.core.dao.VoteOptionDAO;
import at.darkdestiny.core.dao.VotePermissionDAO;
import at.darkdestiny.core.dao.VotingDAO;
import at.darkdestiny.core.dao.VotingDetailDAO;
import at.darkdestiny.core.enumeration.EDiplomacyRelationType;
import at.darkdestiny.core.enumeration.EDoVoteRefType;
import at.darkdestiny.core.enumeration.ELeadership;
import at.darkdestiny.core.enumeration.EMessageRefType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.AllianceRank;
import at.darkdestiny.core.model.DiplomacyRelation;
import at.darkdestiny.core.model.DiplomacyType;
import at.darkdestiny.core.model.VoteMetadata;
import at.darkdestiny.core.model.VoteOption;
import at.darkdestiny.core.model.VotePermission;
import at.darkdestiny.core.model.Voting;
import at.darkdestiny.core.model.VotingDetail;
import at.darkdestiny.core.notification.AllianceJoinNotification;
import at.darkdestiny.core.service.AllianceService;
import at.darkdestiny.core.service.DiplomacyService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.LanguageUtilities;
import at.darkdestiny.core.utilities.PlanetUtilities;
import at.darkdestiny.core.utilities.ResearchUtilities;
import at.darkdestiny.framework.exception.TransactionException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author Stefan
 */


public class VoteUtilities extends Service {

    private static final Logger log = LoggerFactory.getLogger(VoteUtilities.class);

    private static VotingDetailDAO vdDAO = (VotingDetailDAO) DAOFactory.get(VotingDetailDAO.class);
    private static VotingDAO vDAO = (VotingDAO) DAOFactory.get(VotingDAO.class);
    private static VoteMetadataDAO vmdDAO = (VoteMetadataDAO) DAOFactory.get(VoteMetadataDAO.class);
    private static VotePermissionDAO vpDAO = (VotePermissionDAO) DAOFactory.get(VotePermissionDAO.class);
    private static VoteOptionDAO voDAO = (VoteOptionDAO) DAOFactory.get(VoteOptionDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static AllianceRankDAO arDAO = (AllianceRankDAO) DAOFactory.get(AllianceRankDAO.class);

    public VoteUtilities() {
    }

    // Check if all Vote settings are correct
    // Add vote to database
    // Notify all listed users
    protected static int createVote(Vote v) throws VoteException {
        int allianceId = 0;
        // Check data in vote
        if (v.getExpires() != 0) {
            if (v.getExpires() < GameUtilities.getCurrentTick2()) {
                throw new VoteException("Invalid vote expire time");
            }
        }

        if (v.getOptions().size() < 2) {
            throw new VoteException("At least 2 options have to be provided!");
        }
        if (v.getMinVotes() > v.getTotalVotes()) {
            throw new VoteException("MinVotes may not be larger than TotalVotes");
        }

        /* REMOVED USERBUFFER
         if (UserBuffer.getUserData(v.getVoteStarter()) == null) {
         throw new Exception("VoteStarter does not exist!");
         }
         */
        // Check Receivers
        if (v.getReceivers().size() < 1) {
            throw new VoteException("At least 1 receiver has to be provided!");
        }

        /* REMOVED USERBUFFER
         for (Integer user : v.getReceivers()) {
         if (UserBuffer.getUserData(user) == null) {
         throw new Exception("User " + user + " does not exist!");
         }
         }
         */
        if (v.getMinVotes() > v.getReceivers().size()) {
            throw new VoteException("Minimum need votes must not be higher than number of receivers!");
        }

        // Check Permissions
        for (VotePermission vp : v.getPerms()) {
            /* REMOVED USERBUFFER
             if (vp.getType() == VotePermission.PERM_SINGLE) {
             if (UserBuffer.getUserData(vp.getTypeId()) == null) {
             throw new Exception("User " + vp.getTypeId() + " does not exist!");
             }
             }
             */

            if (vp.getType() == VotePermission.PERM_ALLIANCE) {
                Alliance a = allianceDAO.findById(vp.getTypeId());
                if (a == null) {
                    throw new VoteException("Alliance " + vp.getTypeId() + " does not exist!");
                }
            }
        }

        if (checkIfVoteAlreadyExists(v.getType(), v.getMetaData())) {
            throw new VoteException("An identic vote already exists and is still open");
        }

        TransactionHandler th = TransactionHandler.getTransactionHandler();
        int voteId = 0;
        try {
            th.startTransaction();

            // All data has been checked .. create vote
            Voting voting = new Voting();
            voting.setMinVotes(v.getMinVotes());
            voting.setTotalVotes(v.getTotalVotes());
            voting.setExpire(v.getExpires());
            voting.setType(v.getType());

            if (v.isMsgTranslation()) {
                voting.setVotetext(v.getVotingTextML().getTextConstantSubject());
            } else {
                voting.setVotetext(v.getVoteTopic());
            }

            Voting vtmp = votingDAO.add(voting);

            voteId = vtmp.getVoteId();

            for (VoteOption option : v.getOptions()) {
                option.setVoteId(voteId);
                voteOptionDAO.add(option);
            }

            for (VotePermission vp : v.getPerms()) {
                vp.setVoteId(voteId);
                votePermissionDAO.add(vp);
            }

            for (Map.Entry<String, DataEntry> me : v.getMetaData().entrySet()) {
                VoteMetadata vd = new VoteMetadata();
                vd.setVoteId(voteId);
                vd.setDataName(me.getKey());

                if (vd.getDataName().equalsIgnoreCase("ALLIANCEID")) {
                    allianceId = (Integer) (me.getValue().getValue());
                }

                vd.setDataValue(me.getValue().getValue().toString());
                vd.setDataType(me.getValue().getDataType());
                voteMetadataDAO.add(vd);

            }

            boolean masterEntry = true;

            for (Integer receiver : v.getReceivers()) { // Send vote to all users
                GenerateMessage gm = new GenerateMessage(true);

                gm.setSourceUserId(v.getVoteStarter());
                if (v.getVoteStarter() != 0) {
                    gm.setMessageType(EMessageType.USER);
                } else {
                    gm.setMessageType(EMessageType.SYSTEM);
                }

                String topic = "";

                if (v.getType() == Voting.TYPE_MEMBERJOIN) {
                    topic = userDAO.findById(v.getVoteStarter()).getGameName() + " " + ML.getMLStr("vote_msg_joinrequest1", receiver);
                } else if (v.getType() == Voting.TYPE_ALLIANCEJOIN) {
                    topic = ML.getMLStr("vote_msg_alliancejoinrequest1", receiver) + " <b>" + v.getVoteTopic() + "</b> " + ML.getMLStr("vote_msg_alliancejoinrequest2", receiver);
                }

                if (topic.equalsIgnoreCase("")) {
                    if (v.isMsgTranslation()) {
                        gm.setTopic(v.getVotingTextML().getSubject(receiver));
                    } else {
                        gm.setTopic(v.getVoteTopic());
                    }
                } else {
                    gm.setTopic(topic);
                }

                String vMessage;
                if (v.isMsgTranslation()) {
                    vMessage = v.getVotingTextML().getMessage(receiver);
                } else {
                    vMessage = v.getVoteMessage();
                }

                // Javascript protection
                vMessage = FormatUtilities.killJavaScript(vMessage);
                gm.setMsg(vMessage);

                // Do this dynamically in future
                // gm.setMsg(generateVoteBody(v, voteId, receiver));
                gm.setDestinationUserId(receiver);
                gm.setMasterEntry(masterEntry);
                gm.setReference(EMessageRefType.VOTE, voteId);

                gm.writeMessageToUser();
                masterEntry = false;
            }
        } catch (TransactionException te) {
            DebugBuffer.writeStackTrace("Error during vote creation", te);
            //dbg log.debug("Error in createVoteTransaciton : " + e);
            th.rollback();
        } finally {
            th.endTransaction();
        }

        if (voteId == 0) {
            throw new VoteException("Error occured during creating of vote");
        }

        return voteId;
    }

    @Deprecated
    public static String generateVoteBody(Vote v, int id, int userId) throws Exception {
        return generateVoteBody(v, id, userId, null, 0);
    }

    @Deprecated
    public static String generateVoteBody(Vote v, int id, int userId, EDoVoteRefType refType, int refId) throws Exception {
        StringBuilder voteBody = new StringBuilder();

        /*
         if (v.isMsgTranslation()) {
         voteBody.append(v.getVotingTextML().getMessage(userId));
         } else {
         voteBody.append(v.getVoteMessage());
         }
         */
        if (v.isClosed()) {
            return "";

            // Also return if user has already voted
        }
        VotingDetail vd = vdDAO.findByVoteIdAndUser(id, userId);
        if (vd != null) {
            return "";

        }

        voteBody.append("<BR>");

        String backLink = "";
        if (refType != null) {
            if (refType == EDoVoteRefType.MESSAGE) {
                backLink = "&msgId=" + refId;
            }
        }

        voteBody.append("<form method=\"post\" action=\"main.jsp?page=doVote&voteId=" + id + "" + backLink + "\">");
        voteBody.append("<p>");

        ArrayList<VoteOption> vos = voteOptionDAO.findByVoteId(id);

        //dbg log.debug("id " + id);
        //dbg log.debug("Vos : " + vos);
        //dbg log.debug("VosSize : " + vos.size());
        for (VoteOption vo : vos) {
            //dbg log.debug("Vo : " + vo);
            //dbg log.debug("Vopres : " + vo.getPreSelect());
            String voteOptionText = vo.getVoteOption();
            if (vo.isConstant()) {
                voteOptionText = ML.getMLStr(voteOptionText, userId);
            } else {
                Locale l = LanguageUtilities.getMasterLocaleForUser(userId);
                voteOptionText = translateParametrizedString(vo.getVoteOption(), l);
            }

            if (vo.getPreSelect()) {
                voteBody.append("<input checked=\"checked\" type=\"radio\" name=\"voteoption\" value=\"");
                voteBody.append(vo.getId().toString());
                voteBody.append("\"> ");
                voteBody.append(voteOptionText);
                voteBody.append("<br>");
            } else {
                voteBody.append("<input type=\"radio\" name=\"voteoption\" value=\"");
                voteBody.append(vo.getId().toString());
                voteBody.append("\"> ");
                voteBody.append(voteOptionText);
                voteBody.append("<br>");
            }
        }

        voteBody.append("</p>");
        voteBody.append("<input type=\"submit\" value=\"" + ML.getMLStr("vote_but_vote", userId) + "\" />");
        voteBody.append("</form>");

        return voteBody.toString();
    }

    @Deprecated
    public static VoteResult currentState(int userId, int voteId) throws Exception {
        log.debug("DEBUG 1");
        VoteResult vr;

        // Check if vote exists
        Vote v = getVote(voteId);
        if (v == null) {
            log.debug("DEBUG 2");
            vr = new VoteResult(VoteResult.VOTE_NOT_EXISTS, null, null);
            return vr;
        } else {
            log.debug("DEBUG 3 " + v.getCloseOption());
            // Get all votes and options
            HashMap<Integer, Integer> userVotes = new HashMap<Integer, Integer>();
            HashMap<Integer, VoteOption> options = new HashMap<Integer, VoteOption>();

            try {
                ArrayList<VoteOption> voList = voDAO.findByVoteId(voteId);

                for (VoteOption vo : voList) {
                    ArrayList<VotingDetail> vdList = vdDAO.findByVoteIdAndOption(voteId, vo.getId());

                    for (VotingDetail vd : vdList) {
                        if (vd.getUserId() != 0) {
                            userVotes.put(vd.getUserId(), vd.getOptionVoted());
                        }
                    }

                    if (!options.containsKey(vo.getId())) {
                        options.put(vo.getId(), vo);
                    }
                }

            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in vote: ", e);
            }

            if (v.isClosed()) {
                log.debug("CHECK 1 CLOSED");
                //dbg log.debug("va");
                if (v.getCloseOption() != 0) {
                    VoteOption vo = voDAO.findById(v.getCloseOption());
                    log.debug("CHECK 2 " + vo.getType());

                    if (vo.getType() == VoteOption.OPTION_TYPE_ACCEPT) {
                        log.debug("ACCEEEEEEEEEEEEEEPTED");
                        vr = new VoteResult(VoteResult.VOTE_CLOSED_ACCEPT, v.getCloseOption(), userVotes, options);
                    } else if (vo.getType() == VoteOption.OPTION_TYPE_DENY) {
                        vr = new VoteResult(VoteResult.VOTE_CLOSED_DENIED, v.getCloseOption(), userVotes, options);
                    } else {
                        vr = new VoteResult(VoteResult.VOTE_CLOSED_OPTION, v.getCloseOption(), userVotes, options);
                    }

                    return vr;
                } else {
                    vr = new VoteResult(VoteResult.VOTE_CLOSED_DENIED, userVotes, options);
                    return vr;
                }
            }

            vr = new VoteResult(VoteResult.VOTE_STATE, userVotes, options);
        }

        return vr;
    }

    public static VoteResult vote(int userId, int option, int voteId, Locale locale) throws Exception {
        TransactionHandler th = TransactionHandler.getTransactionHandler();
        th.startTransaction();

        VoteResult vr = null;
        Exception errorState = null;

        try {
            // Check if vote exists
            Vote v = getVote(voteId);
            if (v == null) {
                vr = new VoteResult(VoteResult.VOTE_NOT_EXISTS, null, null);
            } else {
                // Get all votes and options
                HashMap<Integer, Integer> userVotes = new HashMap<Integer, Integer>();
                HashMap<Integer, VoteOption> options = new HashMap<Integer, VoteOption>();

                try {
                    // Statement stmt = DbConnect.createStatement();
                    // ResultSet rs = stmt.executeQuery("SELECT vd.userId, vd.optionVoted, vo.voteOption, vo.type, vo.id FROM voteoptions vo LEFT OUTER JOIN votingdetails vd ON vd.optionVoted=vo.id WHERE vo.voteId=" + voteId);

                    ArrayList<VoteOption> voList = voDAO.findByVoteId(voteId);

                    for (VoteOption vo : voList) {
                        ArrayList<VotingDetail> vdList = vdDAO.findByVoteId(voteId);
                        // ArrayList<VotingDetail> vdList = vdDAO.findByVoteIdAndOption(voteId, vo.getId());

                        for (VotingDetail vd : vdList) {
                            if (vd.getUserId() != 0) {
                                //dbg log.debug("Adding user vote for " + vd.getOptionVoted());
                                userVotes.put(vd.getUserId(), vd.getOptionVoted());
                            }
                        }

                        if (!options.containsKey(vo.getId())) {
                            /*
                             VoteOption vo = new VoteOption();
                             vo.setVoteOption(rs.getString(3));
                             vo.setPreSelect(Boolean.FALSE);
                             vo.setType(rs.getInt(4));
                             */
                            options.put(vo.getId(), vo);
                        }
                    }
                    // stmt.close();
                } catch (Exception e) {
                    DebugBuffer.writeStackTrace("Error in vote: ", e);
                }

                if (v.isClosed()) {
                    DebugBuffer.debug("CHECK 1 -- getCloseOption() = " + v.getCloseOption());
                    //dbg log.debug("va");
                    if (v.getCloseOption() != 0) {
                        VoteOption vo = voDAO.findById(v.getCloseOption());
                        DebugBuffer.debug("CHECK 2 " + vo.getType());

                        if (vo.getType() == VoteOption.OPTION_TYPE_ACCEPT) {
                            DebugBuffer.debug("ACCEPTED");
                            vr = new VoteResult(VoteResult.VOTE_CLOSED_ACCEPT, v.getCloseOption(), userVotes, options);
                        } else if (vo.getType() == VoteOption.OPTION_TYPE_DENY) {
                            DebugBuffer.debug("DENIED");
                            vr = new VoteResult(VoteResult.VOTE_CLOSED_DENIED, v.getCloseOption(), userVotes, options);
                        } else {
                            DebugBuffer.debug("CLOSED_OPTION");
                            vr = new VoteResult(VoteResult.VOTE_CLOSED_OPTION, v.getCloseOption(), userVotes, options);
                        }
                    } else {
                        vr = new VoteResult(VoteResult.VOTE_CLOSED_DENIED, userVotes, options);
                    }
                } else {
                    DebugBuffer.debug("vb");
                    if (userVotes.containsKey(userId)) {
                        vr = new VoteResult(VoteResult.VOTE_ALREADY_VOTED, userVotes, options);
                    } else {
                        userVotes.put(userId, option);

                        HashMap<Integer, Integer> optionCount = new HashMap<Integer, Integer>();

                        // Check minVote versus Voted options
                        for (Map.Entry<Integer, Integer> votedOptEntry : userVotes.entrySet()) {
                            Integer votedOpt = votedOptEntry.getValue();
                            Integer votingUser = votedOptEntry.getKey();

                            DebugBuffer.debug("User " + votingUser + " voted for option " + votedOpt);

                            if (optionCount.containsKey(votedOpt)) {
                                optionCount.put(votedOpt, optionCount.get(votedOpt) + 1);
                            } else {
                                optionCount.put(votedOpt, 1);
                            }
                        }

                        int acceptOptionId = 0;
                        int denyOptionId = 0;

                        for (VoteOption vo : v.getOptions()) {
                            DebugBuffer.debug("Check vote option: " + vo.getId() + " TYPE " + vo.getType());
                            if (vo.getType() == VoteOption.OPTION_TYPE_ACCEPT) {
                                DebugBuffer.debug("Found accept option: " + vo.getId());
                                acceptOptionId = vo.getId();
                            }
                            if (vo.getType() == VoteOption.OPTION_TYPE_DENY) {
                                DebugBuffer.debug("Found deny option: " + vo.getId());
                                denyOptionId = vo.getId();
                            }
                        }

                        DebugBuffer.debug("MinVotes: " + v.getMinVotes() + " -- OptionCount: " + optionCount.get(option));
                        //dbg log.debug("v1");
                        // System.out.println("MINVOTES = " + v.getMinVotes());
                        VoteOption currentVoted = voteOptionDAO.findById(option);
                        DebugBuffer.debug("Curr option voted: " + currentVoted.getType() + " VS ACCEPT: " + VoteOption.OPTION_TYPE_ACCEPT + " DENIED: " + VoteOption.OPTION_TYPE_DENY);

                        boolean addAccept = currentVoted.getType() == VoteOption.OPTION_TYPE_ACCEPT;
                        boolean addRefuse = currentVoted.getType() == VoteOption.OPTION_TYPE_DENY;

                        // Check if MinVote count has been reached and if it was a yes or no selection                                                
                        if (v.getMinVotes() <= userVotes.size()) {
                            // if (v.getMinVotes() != 0) {
                            if (addRefuse || addAccept) {
                                // Determine the winning option, if addRefuse and addAccept are equal, the vote is denied
                                Integer aVotes = optionCount.get(acceptOptionId);
                                Integer dVotes = optionCount.get(denyOptionId);

                                int acceptVotes = 0;
                                if (aVotes != null) {
                                    acceptVotes = aVotes;
                                }

                                int deniedVotes = 0;
                                if (dVotes != null) {
                                    deniedVotes = dVotes;
                                }

                                DebugBuffer.debug("Accepts: " + acceptVotes + " Denied: " + deniedVotes);

                                if (acceptVotes > deniedVotes) {
                                    DebugBuffer.debug("[CLOSE 1] CALL VOTE_CLOSED_ACCEPT");
                                    addVoteToDb(voteId, userId, option);
                                    vr = new VoteResult(VoteResult.VOTE_CLOSED_ACCEPT, option, userVotes, options);
                                    v.closeVote(option);
                                    closeVote(voteId, v, VoteResult.VOTED_ACCEPT);
                                } else {
                                    DebugBuffer.debug("[CLOSE 2] CALL VOTE_CLOSED_DENIED");
                                    addVoteToDb(voteId, userId, option);
                                    vr = new VoteResult(VoteResult.VOTE_CLOSED_DENIED, option, userVotes, options);
                                    v.closeVote(option);
                                    closeVote(voteId, v, VoteResult.VOTED_DENIED);
                                }
                                /*                            
                                 if (addRefuse && (optionCount.get(option) > (v.getTotalVotes() - v.getMinVotes()))) {
                                 DebugBuffer.debug("CLOSE 1");
                                 // DENIED
                                 userVotes.put(userId, option);
                                 vr = new VoteResult(VoteResult.VOTED_DENIED, option, userVotes, options);
                                 addVoteToDb(voteId, userId, option);
                                 v.closeVote(option);
                                 closeVote(voteId, v, VoteResult.VOTED_DENIED);
                                 } else if (addAccept && (userVotes.size() >= (v.getMinVotes()))) {
                                 DebugBuffer.debug("CLOSE 2");
                                 // ACCEPTED
                                 userVotes.put(userId, option);
                                 vr = new VoteResult(VoteResult.VOTED_ACCEPT, option, userVotes, options);
                                 addVoteToDb(voteId, userId, option);
                                 v.closeVote(option);
                                 closeVote(voteId, v, VoteResult.VOTED_ACCEPT);
                                 */
                            } else {
                                DebugBuffer.debug("CLOSE 3");
                                // JUST VOTED :)
                                userVotes.put(userId, option);
                                addVoteToDb(voteId, userId, option);
                                v.closeVote(option);

                                if (optionCount.get(option) > (v.getTotalVotes() - v.getMinVotes())) {
                                    vr = new VoteResult(VoteResult.VOTE_CLOSED_OPTION, currentVoted.getId(), userVotes, options);
                                    // vr = new VoteResult(VoteResult.VOTE_CLOSED_OPTION, userVotes, options);
                                    closeVote(voteId, v, VoteResult.VOTE_CLOSED_OPTION);
                                } else {
                                    vr = new VoteResult(VoteResult.VOTED, userVotes, options);
                                }
                            }
                        } else {
                            if (addRefuse) {
                                DebugBuffer.debug("STAY OPEN 1");
                                // DENIED
                                userVotes.put(userId, option);
                                vr = new VoteResult(VoteResult.VOTED_DENIED, option, userVotes, options);
                                addVoteToDb(voteId, userId, option);
                                // v.closeVote(option);
                                // closeVote(voteId, v, VoteResult.VOTED_DENIED);
                            } else if (addAccept) {
                                DebugBuffer.debug("STAY OPEN 2");
                                // ACCEPTED
                                userVotes.put(userId, option);
                                vr = new VoteResult(VoteResult.VOTED_ACCEPT, option, userVotes, options);
                                addVoteToDb(voteId, userId, option);
                                // v.closeVote(option);
                                // closeVote(voteId, v, VoteResult.VOTED_ACCEPT);
                            } else {
                                DebugBuffer.debug("STAY OPEN 3");
                                // JUST VOTED :)
                                userVotes.put(userId, option);
                                addVoteToDb(voteId, userId, option);
                                // v.closeVote(option);
                                vr = new VoteResult(VoteResult.VOTED, userVotes, options);
                            }
                        }

                        /*
                         if (v.getMinVotes() <= optionCount.get(option)) {
                         v.closeVote(option);
                         //dbg log.debug("v2 - OPTION = " + option);
                         if (options.get(option).getType() == VoteOption.OPTION_TYPE_ACCEPT) {
                         //dbg log.debug("v3");
                         userVotes.put(userId, option);
                         vr = new VoteResult(VoteResult.VOTED_ACCEPT, option, userVotes, options);
                         addVoteToDb(voteId, userId, option);
                         closeVote(voteId, v, VoteResult.VOTED_ACCEPT);

                         } else if (options.get(option).getType() == VoteOption.OPTION_TYPE_DENY) {
                         //dbg log.debug("v4");
                         userVotes.put(userId, option);
                         vr = new VoteResult(VoteResult.VOTED_DENIED, option, userVotes, options);
                         addVoteToDb(voteId, userId, option);
                         closeVote(voteId, v, VoteResult.VOTED_DENIED);
                         }
                         } else {
                         //dbg log.debug("v5");
                         //dbg log.debug("UserVotes: " + userVotes + " -- Options: " + options);

                         userVotes.put(userId, option);
                         addVoteToDb(voteId, userId, option);
                         vr = new VoteResult(VoteResult.VOTED, userVotes, options);
                         }
                         */
                    }
                }
            }

            th.execute();
            th.endTransaction();
        } catch (Exception e) {
            errorState = e;
            DebugBuffer.writeStackTrace("Voting Transaction failed", e);

            try {
                th.rollback();
            } catch (TransactionException te2) {
                DebugBuffer.writeStackTrace("Rollback failed", te2);
            }
        } finally {
            th.endTransaction();
        }

        if (errorState != null) {
            throw errorState;
        }
        return vr;
    }

    private static void addVoteToDb(int voteId, int userId, int option) throws Exception {
        //dbg log.debug("Add vote to db [voteId="+voteId+" / userId="+userId+" / option="+option+"]");

        VotingDetail vd = new VotingDetail();
        vd.setVoteId(voteId);
        vd.setUserId(userId);
        vd.setOptionVoted(option);
        vdDAO.add(vd);

        /*
         Statement stmt = DbConnect.createStatement();
         stmt.execute("INSERT INTO votingdetails (voteId,userId,optionVoted) VALUES (" + voteId + "," + userId + "," + option + ")");
         stmt.close();
         */
    }

    private static void closeVote(int voteId, Vote v, int voteResult) throws Exception {
        Voting vote = vDAO.findById(voteId);
        vote.setClosed(true);
        vote.setResult(v.getCloseOption());
        vDAO.update(vote);
            // Statement stmt = DbConnect.createStatement();
        // stmt.executeUpdate("UPDATE voting SET closed=1, result=" + v.getCloseOption() + " WHERE voteId=" + voteId);

        System.out.println("VOTERESULT = " + voteResult + " VTYPE = " + v.getType());

        switch (voteResult) {
            case (VoteResult.VOTED_ACCEPT):
                switch (v.getType()) {
                    case (Voting.TYPE_ADMIN):
                        break;
                    case (Voting.TYPE_ALLIANCEJOIN):
                        AllianceService.createTreaty(
                                (Integer) v.getMetaData("SUBALLIANCEID"),
                                (Integer) v.getMetaData("JOINALLIANCEID"));
                        break;
                    case (Voting.TYPE_DEGRADE_ADMIN):
                        break;
                    case (Voting.TYPE_INVASION):
                        break;
                    case (Voting.TYPE_MEMBERJOIN):
                        new AllianceJoinNotification((Integer) v.getMetaData("USERID"),
                                (Integer) v.getMetaData("ALLIANCEID"),
                                true);
                        AllianceService.addUserToAlliance(
                                (Integer) v.getMetaData("ALLIANCEID"),
                                (Integer) v.getMetaData("USERID"));
                        break;
                    case (Voting.TYPE_USERRELATION):
                        DiplomacyService.updateUserRelation(
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                true);
                        break;
                    case (Voting.TYPE_USERALLIANCERELATION):
                        DiplomacyService.updateUserAllianceRelation(
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                true);
                        break;
                    case (Voting.TYPE_ALLIANCEUSERRELATION):
                        DiplomacyService.updateAllianceUserRelation(
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                true);
                        break;
                    case (Voting.TYPE_ALLIANCERELATION):
                        DiplomacyService.updateAllianceRelation(
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                true);
                        break;
                    case (Voting.TYPE_STANDARD):
                        break;
                    case (Voting.TYPE_ALLIANCERELATION_REQUEST):
                        DiplomacyService.createAllianceRelationVote((Integer) v.getMetaData(VoteMetadata.NAME_INITIAL_USER),
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TYPE),
                                (String) v.getMetaData(VoteMetadata.NAME_MESSAGE));
                        break;
                    case (Voting.TYPE_ALLIANCEUSERRELATION_REQUEST):
                        DiplomacyService.createAllianceUserRelationVote((Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_INITIAL_USER),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TYPE),
                                (String) v.getMetaData(VoteMetadata.NAME_MESSAGE));
                        break;
                    case (Voting.TYPE_MEMBERDEGRADE):
                        AllianceService.setUserRank((Integer) v.getMetaData("USERID"),
                                (Integer) v.getMetaData("NEWRANK"),
                                ((Integer) v.getMetaData("SPECIAL") == 1));
                        break;
                    case (Voting.TYPE_MEMBERPROMOTE):
                        AllianceService.setUserRank((Integer) v.getMetaData("USERID"),
                                (Integer) v.getMetaData("NEWRANK"),
                                ((Integer) v.getMetaData("SPECIAL") == 1));
                        break;
                    case (Voting.TYPE_CHANGELEADERSHIP):
                        Alliance a = allianceDAO.findById((Integer) v.getMetaData("ALLIANCEID"));
                        a.setLeadership(ELeadership.valueOf((String) v.getMetaData("NEWSTATE")));
                        allianceDAO.update(a);

                        break;
                    case (Voting.TYPE_TRANSFERPLANET):
                        int newUserId = (Integer) v.getMetaData("RECEIVERID");
                        int planetId = (Integer) v.getMetaData("PLANETID");
                        PlanetUtilities.transferPlanet(newUserId, planetId);
                        break;
                    /*
                     case (Voting.TYPE_VOTE_RESEARCH):
                     ResearchUtilities.grantVotedResearch(v, voteResult);
                     // int newUserId = (Integer)v.getMetaData("RECEIVERID");
                     // int planetId = (Integer)v.getMetaData("PLANETID");
                     // PlanetUtilities.transferPlanet(newUserId, planetId);
                     break;                            
                     */
                }
                break;
            case (VoteResult.VOTED_DENIED):
                //dbg log.debug("Vote has been declined");
                switch (v.getType()) {
                    case (Voting.TYPE_ADMIN):
                        break;
                    case (Voting.TYPE_ALLIANCEJOIN):
                        break;
                    case (Voting.TYPE_DEGRADE_ADMIN):
                        break;
                    case (Voting.TYPE_INVASION):
                        break;
                    case (Voting.TYPE_MEMBERJOIN):
                        new AllianceJoinNotification((Integer) v.getMetaData("USERID"),
                                (Integer) v.getMetaData("ALLIANCEID"),
                                false);
                        break;
                    case (Voting.TYPE_USERRELATION):
                        DiplomacyService.updateUserRelation(
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                false);
                        break;
                    case (Voting.TYPE_USERALLIANCERELATION):
                        DiplomacyService.updateUserAllianceRelation(
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                false);
                        break;

                    case (Voting.TYPE_ALLIANCEUSERRELATION):
                        DiplomacyService.updateAllianceUserRelation(
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                false);
                        break;
                    case (Voting.TYPE_ALLIANCERELATION):
                        DiplomacyService.updateAllianceRelation(
                                (Integer) v.getMetaData(VoteMetadata.NAME_FROMID),
                                (Integer) v.getMetaData(VoteMetadata.NAME_TOID),
                                false);
                        break;
                    case (Voting.TYPE_STANDARD):
                        break;
                    case (Voting.TYPE_ALLIANCERELATION_REQUEST):
                        break;
                    case (Voting.TYPE_ALLIANCEUSERRELATION_REQUEST):
                        break;
                }
                break;
            case (VoteResult.VOTE_CLOSED_OPTION):
                switch (v.getType()) {
                    case (Voting.TYPE_VOTE_RESEARCH):
                        ResearchUtilities.grantVotedResearch(v, voteResult);
                            // int newUserId = (Integer)v.getMetaData("RECEIVERID");
                        // int planetId = (Integer)v.getMetaData("PLANETID");
                        // PlanetUtilities.transferPlanet(newUserId, planetId);
                        break;
                }
                break;
        }
    }

    protected static Vote getVote(int voteId) {
        Vote v = null;

        try {
            // Statement stmt = DbConnect.createStatement();
            // ResultSet rs = stmt.executeQuery("SELECT minVotes, totalVotes, expire, type, votetext, closed, result FROM voting WHERE voteId=" + voteId);
            Voting vote = vDAO.findById(voteId);

            if (vote != null) {
                log.debug("VOTE TYPE => " + vote.getType());

                v = new Vote(0, vote.getVotetext(), "", vote.getType());
                v.setMinVotes(vote.getMinVotes());
                v.setTotalVotes(vote.getTotalVotes());
                v.setExpires(vote.getExpire());

                if (vote.getClosed()) {
                    log.debug("Vote is closed ... goddammit");
                    v.closeVote(vote.getResult());
                }

                // Load vote metadata
                // rs = stmt.executeQuery("SELECT dataName, dataValue, dataType FROM votemetadata WHERE voteId=" + voteId);
                ArrayList<VoteMetadata> vmdList = vmdDAO.findByVoteId(voteId);
                for (VoteMetadata vmd : vmdList) {
                    v.addMetaData(vmd.getDataName(), vmd.getDataValue(), vmd.getDataType());
                }

                ArrayList<VoteOption> voList = voDAO.findByVoteId(voteId);
                for (VoteOption vo : voList) {
                    v.addOption(vo);
                }
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in getVote: ", e);
        }

        return v;
    }

    // Checks if a user has permission to vote
    // Trials have no alliance vote permissions
    // VOTE_MEMBERJOIN -> Alliancemember, Council or Admin -- 1 vote is enough
    // VOTE_ALLIANCEJOIN -> Alliancemember, Admin -- 1 vote is enough
    // VOTE_DEGRADE_ADMIN -> Alliancemember, Admin 2 votes, Council 2 votes, members 1 vote -- 50% + 1
    // VOTE_ADMIN -> Alliancemember, Admin and Council 2 votes, members 1 vote
    public static boolean checkVotePermission(int userId, int voteId) {
        try {
            // Statement stmt = DbConnect.createStatement();
            // ResultSet rs = stmt.executeQuery("SELECT type, typeId, minRank FROM votepermission WHERE voteId=" + voteId);
            ArrayList<VotePermission> vpList = vpDAO.findByVoteId(voteId);

            boolean noPermission = true;
            boolean alliancePermission = false;
            boolean singlePermission = false;

            HashMap<Integer, Integer> allianceList = new HashMap<Integer, Integer>();
            ArrayList<Integer> userList = new ArrayList<Integer>();
            int minRank = 4;

            for (VotePermission vp : vpList) {
                //dbg log.debug("Found permission entry");

                noPermission = false;
                minRank = vp.getMinRank();

                if (vp.getType() == VotePermission.PERM_SINGLE) {
                    //dbg log.debug("Single permission entry");
                    singlePermission = true;
                    userList.add(vp.getTypeId());
                } else if (vp.getType() == VotePermission.PERM_ALLIANCE) {
                    //dbg log.debug("Alliance permission entry");
                    alliancePermission = true;
                    allianceList.put(vp.getTypeId(), vp.getMinRank());
                }
            }

            if (noPermission) {
                //dbg log.debug("No permission entry");
                return true;
            }

            if (alliancePermission) {
                boolean userFound = false;
                int rank = AllianceRank.RANK_MEMBER_INT;
                int allianceId = 0;

                //dbg log.debug("Check alliance permission entry");
                // Check if current user is in a specified Alliance
                for (Integer aId : allianceList.keySet()) {
                    // Statement stmt2 = DbConnect.createStatement();
                    // ResultSet rs2 = stmt2.executeQuery("SELECT userId, isTrial, isAdmin, isCouncil, specialRankId FROM alliancemembers WHERE allianceId=" + aId);
                    ArrayList<AllianceMember> amList = amDAO.findByAllianceId(aId);

                    //dbg log.debug("Checking alliance " + aId);
                    for (AllianceMember am : amList) {
                        if (am.getUserId() == userId) {
                            //dbg log.debug("Found user");

                            if (am.getIsTrial()) {
                                rank = AllianceRank.RANK_TRIAL_INT;
                            }
                            if (am.getIsCouncil()) {
                                rank = AllianceRank.RANK_COUNCIL_INT;
                            }
                            if (am.getIsAdmin()) {
                                rank = AllianceRank.RANK_ADMIN_INT;
                            }

                            //dbg log.debug("Determined Rank was " + rank);
                            allianceId = aId;
                            userFound = true;
                            if (am.getSpecialRankId() != 0) {
                                // Statement stmt3 = DbConnect.createStatement();
                                // ResultSet rs3 = stmt3.executeQuery("SELECT comparisonRank FROM allianceRank WHERE allianceId=" + aId + " AND rankId=" + rs2.getInt(5));
                                AllianceRank ar = arDAO.findBy(am.getSpecialRankId(), aId);
                                if (ar != null) {
                                    rank = ar.getComparisonRank();
                                }
                                // stmt3.close();
                            }

                            //dbg log.debug("Determined Rank after specialRank was " + rank);
                            // stmt2.close();
                            break;
                        }
                    }

                    if (userFound) {
                        break;
                    }
                    // stmt2.close();
                }

                if (userFound) {
                    return !(rank < allianceList.get(allianceId));
                } else {
                    return false;
                }
            }

            if (singlePermission) {
                //dbg log.debug("Check single permission entry");

                // stmt.close();
                //dbg log.debug("Return result due to user check!");
                return userList.contains(userId);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in checkVotePermission: ", e);
        }

        //dbg log.debug("Return false not terminated or error!");
        return false;
    }

    @Deprecated
    public static String getVoteStateHTML(VoteResult vr, int voteId, Locale locale) {
        String message = "<BR/><BR/>";

        Vote v = getVote(voteId);
        HashMap<Integer, VoteOption> vo = vr.getOptions();

        //dbg log.debug("VO Size = " + vo.size());
        boolean closed = false;

        int optionYes = -1;
        int optionNo = -1;

        HashMap<Integer, ArrayList<String>> voted = new HashMap<Integer, ArrayList<String>>();

        /**
         * for (Map.Entry<Integer, VoteOption> me : vo.entrySet()) { if
         * (me.getValue().getType() == VoteOption.OPTION_TYPE_ACCEPT) { //dbg
         * log.debug("Setting optionYes1from : " + optionYes + " to " +
         * me.getKey()); optionYes = me.getKey(); } if (me.getValue().getType()
         * == VoteOption.OPTION_TYPE_DENY) { //dbg log.debug("Setting optionNo
         * from : " + optionNo + " to " + me.getKey()); optionNo = me.getKey();
         * } }
         */
        for (Map.Entry<Integer, Integer> me : vr.getUserVotes().entrySet()) {
            String userName;
            userName = userDAO.findById(me.getKey()).getGameName();

            //dbg log.debug("Found voted option " + me.getValue() + " for user " + userName);
            VoteOption voteOption = voteOptionDAO.findById(me.getValue());
            //dbg log.debug("Vote option " + voteOption);
            if (voteOption != null) {
                if (voteOption.getType() == VoteOption.OPTION_TYPE_ACCEPT) {
                    //dbg log.debug("Incrementing Option Yes");
                    optionYes++;
                } else if (voteOption.getType() == VoteOption.OPTION_TYPE_DENY) {
                    //dbg log.debug("Incrementing Option No");
                    optionNo++;
                }
            }
            if (!voted.containsKey(me.getValue())) {
                ArrayList<String> users = new ArrayList<String>();
                users.add(userName);
                voted.put(me.getValue(), users);
            } else {
                voted.get(me.getValue()).add(userName);
            }
        }

        if ((vr.getVoteCode() == VoteResult.VOTED_ACCEPT) || (vr.getVoteCode() == VoteResult.VOTED_DENIED) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_DENIED) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_OPTION)) {
            closed = true;

            message += "<B><FONT color=\"yellow\">";
            message += "Diese Abstimmung wurde ";
            if ((vr.getVoteCode() == VoteResult.VOTED_ACCEPT) || (vr.getVoteCode() == VoteResult.VOTE_CLOSED_ACCEPT)) {
                message += "angenommen!";
            } else {
                message += "abgelehnt!";
            }
            message += "</FONT></B><BR><BR>";
        } else {
            if (vr.getVoteCode() == VoteResult.VOTE_ALREADY_VOTED) {
                /*
                 message += "Diese Abstimmung wurde ";
                 if (optionYes > optionNo) {
                 message += "angenommen!";
                 } else {
                 message += "abgelehnt!";
                 }
                 message += "<BR><BR>";
                 */
            } else {
                message += "Diese Abstimmung l&auml;uft noch!<BR><BR>";
            }
        }

        if (v.getVoteMessage().length() > 0) {
            message += GenerateMessage.parseText(v.getVoteMessage(), true) + "<BR><BR>";
        }

        if (closed) {
            message += "Abstimmungsergebnis:<BR>";
        } else {
            message += "Vorl&auml;ufiges Abstimmungsergebnis:<BR>";
        }

        boolean firstUser = true;
        boolean userFound = false;

        message += "Ben&ouml;tigte Stimmen: " + v.getMinVotes() + "<BR>";

        for (Map.Entry<Integer, VoteOption> me : vo.entrySet()) {
            //dbg log.debug("Checking option " + me.getValue().getVoteOption() + " with id " + me.getKey());

            int count = 0;
            String users = "";

            if (voted.containsKey(me.getKey())) {
                //dbg log.debug("Voted contains this option (" + me.getKey() + ")");

                ArrayList<String> userVotes = voted.get(me.getKey());
                for (String userName : userVotes) {
                    count++;
                    if (users.equalsIgnoreCase("")) {
                        users += userName;
                    } else {
                        users += ", " + userName;
                    }
                }
            } else {
                count = 0;
            }

            if (me.getValue().isConstant()) {
                message += ML.getMLStr(me.getValue().getVoteOption(), locale);
            } else {
                // Check for parametrized constants and translate
                String baseContent = me.getValue().getVoteOption();
                String replacedContent = translateParametrizedString(baseContent, locale);

                // message += me.getValue().getVoteOption();
                message += replacedContent;
            }

            message += " (" + count + "): " + users;
            message += "<BR>";
        }

        return message;
    }

    public static String translateParametrizedString(String message, Locale locale) {
        boolean replacing = true;

        while (replacing) {
            replacing = false;

            int startId = message.indexOf("[%$");
            int endId = message.indexOf("$%]");

            if ((startId == -1) || (endId == -1)) {
                continue;
            }

            String completeConstant = message.substring(startId, endId + 3);
            String constant = message.substring(startId + 3, endId);
            String result = ML.getMLStr(constant, locale);

            System.out.println("CompleteConstant=" + completeConstant + " Constant=" + constant + " result=" + result);

            message = message.replace(completeConstant, result);

            replacing = true;
        }

        return message;
    }

    @Deprecated
    public static String getHtmlCode(Vote vote, int voteId, int userId) {
        String html = "";

        if (vote.isClosed()) {
            return "";

        }
        if (vote.getType() == Voting.TYPE_ALLIANCERELATION
                || vote.getType() == Voting.TYPE_USERRELATION
                || vote.getType() == Voting.TYPE_ALLIANCEUSERRELATION
                || vote.getType() == Voting.TYPE_USERALLIANCERELATION
                || vote.getType() == Voting.TYPE_ALLIANCERELATION_REQUEST) {

            Integer fromId = (Integer) vote.getMetaData(VoteMetadata.NAME_FROMID);
            Integer toId = (Integer) vote.getMetaData(VoteMetadata.NAME_TOID);
            Integer diplomacyTypeId = (Integer) vote.getMetaData(VoteMetadata.NAME_TYPE);
            DiplomacyType dt = diplomacyTypeDAO.findById(diplomacyTypeId);

            EDiplomacyRelationType drType = null;

            String name1 = "";
            String name2 = "";
            String msg = "";
            String voteText;
            DiplomacyRelation dr = DiplomacyService.findRelationBy(fromId, toId, EDiplomacyRelationType.USER_TO_USER, false);
            DiplomacyType actRel = diplomacyTypeDAO.findById(DiplomacyType.NEUTRAL);
            if (dr != null) {
                actRel = diplomacyTypeDAO.findById(dr.getDiplomacyTypeId());
            }
            try {
                msg += (String) vote.getMetaData(VoteMetadata.NAME_MESSAGE);
            } catch (Exception e) {
            }
            if (vote.getType() == Voting.TYPE_ALLIANCERELATION) {
                name1 = allianceDAO.findById(fromId).getName();
                name2 = allianceDAO.findById(fromId).getName();
                drType = EDiplomacyRelationType.ALLIANCE_TO_ALLIANCE;
            } else if (vote.getType() == Voting.TYPE_USERRELATION) {
                name1 = userDAO.findById(fromId).getGameName();
                name2 = userDAO.findById(fromId).getGameName();
                drType = EDiplomacyRelationType.USER_TO_USER;
            } else if (vote.getType() == Voting.TYPE_ALLIANCEUSERRELATION) {
                name1 = allianceDAO.findById(fromId).getName();
                name2 = userDAO.findById(fromId).getGameName();
                drType = EDiplomacyRelationType.ALLIANCE_TO_USER;
            } else if (vote.getType() == Voting.TYPE_USERALLIANCERELATION) {
                name1 = userDAO.findById(fromId).getGameName();
                name2 = allianceDAO.findById(fromId).getName();
                drType = EDiplomacyRelationType.USER_TO_ALLIANCE;
            }
            voteText = name1 + " " + ML.getMLStr("diplomacy_msg_changerequest1", userId) + " <b> " + ML.getMLStr(actRel.getName(), userId) + " </b> " + ML.getMLStr("diplomacy_msg_changerequest2", userId) + ": <b>" + ML.getMLStr(dt.getName(), userId) + "</b> " + ML.getMLStr("diplomacy_msg_changerequest3", userId) + " <BR>";

            html += "<center>" + name1 + " : " + ML.getMLStr(actRel.getName(), userId) + " => " + ML.getMLStr(dt.getName(), userId) + "</center>";

            html += "<br><br>";
            html += voteText + "<br><br>";
            html += msg;
            try {
                html += generateVoteBody(vote, voteId, userId);
            } catch (Exception e) {
            }
        }

        return html;
    }

    public static boolean checkIfVoteAlreadyExists(int type, HashMap<String, DataEntry> metaDataMap) {
        ArrayList<Voting> vList;
        vList = vDAO.findByType(type);

        boolean match;

        for (Voting v : vList) {
            if (v.getClosed()) {
                continue;
            }
            match = true;

            ArrayList<VoteMetadata> metaList = vmdDAO.findByVoteId(v.getVoteId());

            for (VoteMetadata vmd : metaList) {
                String currKey = vmd.getDataName();

                if (!metaDataMap.containsKey(currKey)) {
                    match = false;
                    continue;
                }

                if (!metaDataMap.get(currKey).getValue().toString().equals(vmd.getDataValue())) {
                    match = false;
                }
            }

            if (match) {
                DebugBuffer.warning("Current vote had same parameters as existing vote " + v.getVoteId());
                return true;
            }
        }

        return false;
    }
}
