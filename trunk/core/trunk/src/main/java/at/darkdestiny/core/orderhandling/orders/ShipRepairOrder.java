/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.orderhandling.orders;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.enumeration.EDamageLevel;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class ShipRepairOrder extends RepairOrder {
    private final HashMap<EDamageLevel,Integer> repairCount;
    private final int shipFleetId;
    
    public ShipRepairOrder(Buildable unit, int planetId, int userId, int shipFleetId, HashMap<EDamageLevel,Integer> repairCount) {
        super(unit,planetId,userId);
        this.repairCount = repairCount;
        this.shipFleetId = shipFleetId;
    }
    
    public HashMap<EDamageLevel,Integer> getRepairCount() {
        return repairCount;
    }

    public int getShipFleetId() {
        return shipFleetId;
    }
}
