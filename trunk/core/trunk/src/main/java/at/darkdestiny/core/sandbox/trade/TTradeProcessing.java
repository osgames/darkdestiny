/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.sandbox.trade;

import at.darkdestiny.core.sandbox.trade.dto.TCapacityResult;
import at.darkdestiny.core.sandbox.trade.dto.TPlanet;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Feuerelfe
 */
public class TTradeProcessing {

    public enum ETTransportType {

        TRADE, TRANSPORT
    }


    public static long doIt(TPlanet planet, ETTransportType type) {

        long totalCapacity = planet.getOutgoingCapacity();
        int routes = 0;
        //Sum all incoming routes on all outgoing planets

        boolean allRoutesClosed = false;

        while (totalCapacity > 0 && !allRoutesClosed) {
            allRoutesClosed = true;
            routes = findRoutes(planet);
            if (routes == 0) {
                return totalCapacity;
            }

            //Calculate total capacity by number or routes
            long capacityPerRoute = divideByRoutes(totalCapacity, routes);
            //Loop all receiving Planets
            for (TPlanet p : planet.getOutgoingEdges()) {

                //Loop all resources which can be delivered
                for (Map.Entry<Integer, Long> resourceStart : planet.getOutgoingPossibleByPlanet().entrySet()) {

                    Integer resourceId = resourceStart.getKey();

                    //For this resource there cannot be any increase as it is limited by the planet which receives the resource
                    if (p.getProcessedResources().contains(resourceId)) {
                        continue;
                    }
                    //Resources of planet are already totally split, close this route
                    if (resourceStart.getValue() == 0) {
                        p.getProcessedResources().add(resourceId);
                        continue;
                    }
                    //Loop all resources which can receive on the target planet
                    for (Map.Entry<Integer, Long> resourceTarget : p.getIncomingPossibleByPlanet().entrySet()) {

                        //Found same resource
                        if (resourceTarget.getKey().equals(resourceId)) {

                            //Divide the amount of resources which can be delivered by the number of routes
                            long routesResource = findRoutesUsingThisResource(planet, resourceId);
                            long amountResourceAvailablePerRoute = resourceStart.getValue() / routesResource;

                            
                            long incomePossibleByPlanet = resourceTarget.getValue();
                            
                            //If its a transport it could be limited by a routeentry which has an max amount set
                            if (type.equals(ETTransportType.TRANSPORT)) {

                                if (planet.getRouting().get(p) != null && planet.getRouting().get(p).get(resourceId) != null) {
                                    incomePossibleByPlanet = Math.min(amountResourceAvailablePerRoute, planet.getRouting().get(p).get(resourceId));
                                }
                            }

                            TCapacityResult capacityResult = calculateCapacity(capacityPerRoute, (long) Math.ceil(amountResourceAvailablePerRoute * 2d * p.getDistanceFromStartPlanet()), (long) Math.ceil(incomePossibleByPlanet * 2d * p.getDistanceFromStartPlanet()));

                            long capacity = capacityResult.getCapacity();

                            //Capacity really transfered
                            long amountTransfered = (long) Math.ceil(capacity / 2d / p.getDistanceFromStartPlanet());

                            //Only true if the capacity was restricted by the target planet
                            boolean closeRoute = capacityResult.isCloseRoute();

                            //Write value onto outgoing planet
                            if (planet.getTransportedResources().get(resourceStart.getKey()) == null) {
                                planet.getTransportedResources().put(resourceStart.getKey(), amountTransfered);
                            } else {
                                planet.getTransportedResources().put(resourceStart.getKey(), planet.getTransportedResources().get(resourceStart.getKey()) + amountTransfered);
                            }
                            //Write value onto incoming planet
                            if (p.getReceivedResources().get(resourceStart.getKey()) == null) {
                                p.getReceivedResources().put(resourceStart.getKey(), amountTransfered);
                            } else {
                                p.getReceivedResources().put(resourceStart.getKey(), p.getReceivedResources().get(resourceStart.getKey()) + amountTransfered);
                            }
                            //Decrement capacity on available resources map
                            planet.getOutgoingPossibleByPlanet().put(resourceStart.getKey(), resourceStart.getValue() - amountTransfered);
                            
                            //Decrement capacity on available resources map
                            p.getIncomingPossibleByPlanet().put(resourceTarget.getKey(), resourceTarget.getValue() - amountTransfered);

                            if (closeRoute) {
                                p.getProcessedResources().add(resourceId);
                            } else {
                                allRoutesClosed = false;
                            }
                            totalCapacity -= capacity;
                        }
                    }
                }
            }
        }
        return totalCapacity;

    }

    public static TCapacityResult calculateCapacity(long capacityPerRoute, long amountResourceAvailablePerRoute, long incomePossibleByPlanet) {

        boolean closeRoute = false;
        long capacity = 0l;

        // if the capacity per route is restricted by the production, take the production value
        if (capacityPerRoute <= amountResourceAvailablePerRoute) {
            capacity = capacityPerRoute;
            if (incomePossibleByPlanet <= capacity) {
                capacity = incomePossibleByPlanet;
                closeRoute = true;
            } else {
                //Do nothing
            }
        } else if (capacityPerRoute > amountResourceAvailablePerRoute) {
            capacity = amountResourceAvailablePerRoute;
            if (incomePossibleByPlanet <= capacity) {
                capacity = incomePossibleByPlanet;
                closeRoute = true;
            } else {
                //Do nothing
            }
        }
        return new TCapacityResult(capacity, closeRoute);
    }

    public static long divideByRoutes(long capacity, int routes) {
        return (long) ((double) capacity / (double) routes);
    }

    public static long findRoutesUsingThisResource(TPlanet planet, Integer key) {
        int routes = 0;

        for (TPlanet p : planet.getOutgoingEdges()) {
            for (Map.Entry<Integer, Long> resourceStart : planet.getOutgoingPossibleByPlanet().entrySet()) {

                if (!key.equals(resourceStart.getKey())) {
                    continue;
                }
                //Loop all resources which can receive on the target planet
                for (Map.Entry<Integer, Long> resourceTarget : p.getIncomingPossibleByPlanet().entrySet()) {

                    //Found same resource
                    if (resourceStart.getKey().equals(resourceTarget.getKey())) {
                        //Check if not yet closed on target planet
                        if (!p.getProcessedResources().contains(key)) {
                            routes++;
                        }
                    }
                }
            }
        }
        return routes;
    }

    public static int findRoutes(TPlanet planet) {
        int routes = 0;

        for (TPlanet p : planet.getOutgoingEdges()) {
            for (Map.Entry<Integer, Long> resourceStart : planet.getOutgoingPossibleByPlanet().entrySet()) {
                //Loop all resources which can receive on the target planet
                for (Map.Entry<Integer, Long> resourceTarget : p.getIncomingPossibleByPlanet().entrySet()) {

                    //Found same resource
                    if (resourceStart.getKey().equals(resourceTarget.getKey())) {
                        //Check if not yet closed on target planet
                        if (!p.getProcessedResources().contains(resourceStart.getKey())) {
                            routes++;
                        }
                    }
                }
            }
        }
        return routes;
    }
}
