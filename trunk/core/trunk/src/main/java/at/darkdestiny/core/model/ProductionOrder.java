/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.ships.BuildTime;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "productionorder")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ProductionOrder extends Model<ProductionOrder> {

    // PRODUCTION ORDER TYPES
    /*
     public static final int PROD_ORDER_PRODUCE = 1;
     public static final int PROD_ORDER_SCRAP = 2;
     public static final int PROD_ORDER_UPDGRADE = 3;
     public static final int PROD_ORDER_GROUNDTROOPS = 4;
     public static final int PROD_C_ORDER_BUILD = 5;
     public static final int PROD_C_ORDER_BUILD_STATIONS = 6;
     public static final int PROD_C_ORDER_SCRAP = 10;
     public static final int PROD_C_ORDER_UPGRADE = 11;
     */
    // PRIORITY
    public static final int PRIORITY_HIGH = 0;
    public static final int PRIORITY_MEDIUM = 5;
    public static final int PRIORITY_LOW = 10;
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    @IndexAnnotation(indexName = "idType")
    private Integer id;
    @IndexAnnotation(indexName = "planetId")
    @FieldMappingAnnotation("planetId")
    @DefaultValue("0")
    private Integer planetId;
    @FieldMappingAnnotation("moduleNeed")
    @DefaultValue("0")
    private Integer moduleNeed;
    @FieldMappingAnnotation("moduleProc")
    @DefaultValue("0")
    private Integer moduleProc;
    @FieldMappingAnnotation("dockNeed")
    @DefaultValue("0")
    private Integer dockNeed;
    @FieldMappingAnnotation("dockProc")
    @DefaultValue("0")
    private Integer dockProc;
    @FieldMappingAnnotation("orbDockNeed")
    @DefaultValue("0")
    private Integer orbDockNeed;
    @FieldMappingAnnotation("orbDockProc")
    @DefaultValue("0")
    private Integer orbDockProc;
    @FieldMappingAnnotation("kasNeed")
    @DefaultValue("0")
    private Integer kasNeed;
    @FieldMappingAnnotation("kasProc")
    @DefaultValue("0")
    private Integer kasProc;
    @FieldMappingAnnotation("indNeed")
    @DefaultValue("0")
    private Integer indNeed;
    @FieldMappingAnnotation("indProc")
    @DefaultValue("0")
    private Integer indProc;
    @FieldMappingAnnotation("priority")
    @DefaultValue("1")
    private Integer priority;
    @IndexAnnotation(indexName = "idType")
    @FieldMappingAnnotation("type")
    private EProductionOrderType type;
    @FieldMappingAnnotation("toId")
    @DefaultValue("0")
    private Integer toId;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @param planetId the planetId to set
     */
    public void setPlanetId(Integer planetId) {
        this.planetId = planetId;
    }

    /**
     * @return the moduleNeed
     */
    public Integer getModuleNeed() {
        return moduleNeed;
    }

    /**
     * @param moduleNeed the moduleNeed to set
     */
    public void setModuleNeed(Integer moduleNeed) {
        this.moduleNeed = moduleNeed;
    }

    /**
     * @return the moduleProc
     */
    public Integer getModuleProc() {
        return moduleProc;
    }

    /**
     * @return the moduleProc
     */
    public double getModulePerc() {
        if (moduleNeed == 0) {
            return 0d;
        }
        if (moduleProc >= moduleNeed) {
            return 100d;
        }
        return Math.floor(100d / moduleNeed * moduleProc);
    }

    /**
     * @param moduleProc the moduleProc to set
     */
    public void setModuleProc(Integer moduleProc) {
        this.moduleProc = moduleProc;
    }

    /**
     * @return the dockNeed
     */
    public Integer getDockNeed() {
        return dockNeed;
    }

    /**
     * @param dockNeed the dockNeed to set
     */
    public void setDockNeed(Integer dockNeed) {
        this.dockNeed = dockNeed;
    }

    /**
     * @return the dockProc
     */
    public Integer getDockProc() {
        return dockProc;
    }

    /**
     * @return the dockProc
     */
    public double getDockPerc() {
        if (dockNeed == 0) {
            return 0d;
        }
        if (dockProc >= dockNeed) {
            return 100d;
        }
        return Math.floor(100d / dockNeed * dockProc);
    }

    /**
     * @param dockProc the dockProc to set
     */
    public void setDockProc(Integer dockProc) {
        this.dockProc = dockProc;
    }

    /**
     * @return the orbDockNeed
     */
    public Integer getOrbDockNeed() {
        return orbDockNeed;
    }

    /**
     * @param orbDockNeed the orbDockNeed to set
     */
    public void setOrbDockNeed(Integer orbDockNeed) {
        this.orbDockNeed = orbDockNeed;
    }

    /**
     * @return the orbDockProc
     */
    public Integer getOrbDockProc() {
        return orbDockProc;
    }

    /**
     * @return the orbDockProc
     */
    public double getOrbDockPerc() {
        if (orbDockNeed == 0) {
            return 0d;
        }
        // if (orbDockProc >= orbDockNeed) return 100d;

        return Math.floor((100d * orbDockProc) / orbDockNeed);
    }

    /**
     * @param orbDockProc the orbDockProc to set
     */
    public void setOrbDockProc(Integer orbDockProc) {
        this.orbDockProc = orbDockProc;
    }

    /**
     * @return the kasNeed
     */
    public Integer getKasNeed() {
        return kasNeed;
    }

    /**
     * @param kasNeed the kasNeed to set
     */
    public void setKasNeed(Integer kasNeed) {
        this.kasNeed = kasNeed;
    }

    /**
     * @return the kasProc
     */
    public Integer getKasProc() {
        return kasProc;
    }

    /**
     * @return the kasProc
     */
    public double getKasPerc() {
        if (kasNeed == 0) {
            return 0d;
        }
        if (kasProc >= kasNeed) {
            return 100d;
        }
        return Math.floor(100d / kasNeed * kasProc);
    }

    /**
     * @param kasProc the kasProc to set
     */
    public void setKasProc(Integer kasProc) {
        this.kasProc = kasProc;
    }

    /**
     * @return the indNeed
     */
    public Integer getIndNeed() {
        return indNeed;
    }

    /**
     * @param indNeed the indNeed to set
     */
    public void setIndNeed(Integer indNeed) {
        this.indNeed = indNeed;
    }

    /**
     * @return the indProc
     */
    public Integer getIndProc() {
        return indProc;
    }

    /**
     * @return the indProc
     */
    public double getIndPerc() {
        if (indNeed == 0 || indProc == 0) {
            return 0d;
        }
        return Math.floor(100d / indNeed * indProc);
    }

    /**
     * @param indProc the indProc to set
     */
    public void setIndProc(Integer indProc) {
        this.indProc = indProc;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the type
     */
    public EProductionOrderType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EProductionOrderType type) {
        this.type = type;
    }

    /**
     * @return the toId
     */
    public Integer getToId() {
        return toId;
    }

    /**
     * @param toId the toId to set
     */
    public void setToId(Integer toId) {
        this.toId = toId;
    }

    public void setShipBuildTime(BuildTime bt) {
        this.moduleNeed = bt.getModulePoints();
        this.kasNeed = bt.getKasPoints();
        this.orbDockNeed = bt.getOrbDockPoints();
        this.dockNeed = bt.getDockPoints();
    }
}
