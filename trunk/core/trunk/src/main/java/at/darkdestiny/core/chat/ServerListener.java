/*
 * ServerListener.java
 *
 * Created on 04. Juli 2008, 00:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.core.chat;

 import at.darkdestiny.util.DebugBuffer;import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Stefan
 */
public final class ServerListener {

    private static final Logger log = LoggerFactory.getLogger(ServerListener.class);

    /** The port the server is listening on. */
    private static final int PORT = 10500;
    private static ServerSocket serverSocket;
    private volatile boolean noError = true;
    // It is absolutely necessary to work with synchronized lists, as it is
    // accessed by multiple threads!
    private static ArrayList<ClientHandler> clientList = new ArrayList<ClientHandler>();
    private static boolean stop = false;
    private static Timer clientChecker;

    /** Creates a new instance of ServerListener */
    public ServerListener() {
        try {
            stop = true;
            InetAddress serverAddress = null;
            // serverSocket = null;

            // serverAddress = InetAddress.getByName("192.168.77.58");
            int waitCycles = 2 * 6;

            while (!available(PORT) && (waitCycles > 0)) {
                try {
                    serverSocket.close();
                } catch (Exception e) {

                }

                try {
                    Thread.sleep(5000);
                } catch (Exception e) {

                }

                waitCycles--;

                DebugBuffer.debug("Server Socket not available (SS: "+serverSocket+")");
            }

            if (waitCycles == 0) {
                log.error( "Server could not bind to port " + PORT);
            }

            stop = false;
            try {
                serverSocket = new ServerSocket(PORT, 100, serverAddress);
                serverSocket.setReuseAddress(true);
            } catch (Exception e) {

            }
            // ss = new ServerSocket(port);

            DebugBuffer.addLine(
                    DebugLevel.UNKNOWN,
                    "Socket created on " + serverSocket.getInetAddress().getCanonicalHostName() + " Port: " + serverSocket.getLocalPort());

            // User a timer Object, to check for invalid connections!
            clientChecker = new Timer(true);
            clientChecker.schedule(new TimerTask() {

                @Override
                public void run() {
                    checkClientThreads();
                }
            }, 20000, 20000);

        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Socket Error init: " + e);
            noError = false;
            throw new RuntimeException(e);
        }
    }

    public static boolean available(int port) {
        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }

        return false;
    }

    public void start() {
        // Start the listening Thread
        Thread chatServer = new Thread(new Runnable() {

            public void run() {
                try {
                    runListenerThread();
                } catch (RuntimeException e) {
                    DebugBuffer.writeStackTrace("ServerListener Error: ",e);
                }
            }
        });
        chatServer.setName("Chat Server");
        chatServer.start();
    }

    private void runListenerThread() {
        if (!noError) {
            return;
        }

        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Start Listening (" + PORT + ")");

        while (noError && !stop) {
            try {
                Socket newConn = serverSocket.accept();
                DebugBuffer.addLine(DebugLevel.UNKNOWN, "New incoming connection ("+newConn+")");
                ClientHandler ch = new ClientHandler(newConn);
                ch.setName("Chat Client Thread [" + newConn.getRemoteSocketAddress() + "]");
                ch.start();

                clientList.add(ch);
            } catch (IOException e) {
                noError = false;
                DebugBuffer.writeStackTrace("ServerListener Error: ",e);

                return;
            }
        }

        DebugBuffer.addLine(DebugLevel.UNKNOWN, "NoError = " + noError + " Stop = " + stop);
        DebugBuffer.addLine(DebugLevel.UNKNOWN, "Listening Loop was left (" + PORT + ")");
    }

    public static List<ClientHandler> getClientList() {
        return clientList;
    }

    protected synchronized void checkClientThreads() {
        for (Iterator<ClientHandler> iterator = clientList.iterator(); iterator.hasNext();) {
            ClientHandler ch = iterator.next();

            if (!ch.isAlive()) {
                DebugBuffer.trace("Removing Client due to shutdown client thread!");
                iterator.remove();
                continue;
            } else {
                if (ch.connState < 2) {
                    if ((System.currentTimeMillis() - ch.creationTime) > 60000) {
                        DebugBuffer.trace("Removing Client due to idle connection!");
                        ch.closeStreams();
                        iterator.remove();
                    }
                } else {
                    if (!ch.checkConnAlive()) {
                        DebugBuffer.trace("Removing Client due to timeout!");
                        ch.closeStreams();
                        iterator.remove();
                    }
                }
            }
        }
    }

    public static void stop() {
        log.info( "Stopping client thread checker");
        if (clientChecker != null) {
            clientChecker.cancel();
        }
        log.info( "Stopping Server listener");
        try {
            serverSocket.close();
        } catch (Exception e) {
            log.warn( "Server socket could not be closed");
        }

        stop = true;
    }

    public static boolean serverConnected() {
        return serverSocket != null;
    }
}
