/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.BattleLogDAO;
import at.darkdestiny.core.dao.BattleLogEntryDAO;
import at.darkdestiny.core.dao.ConstructionModuleDAO;
import at.darkdestiny.core.dao.DamagedShipsDAO;
import at.darkdestiny.core.dao.FleetDetailDAO;
import at.darkdestiny.core.dao.FleetLoadingDAO;
import at.darkdestiny.core.dao.FleetOrderDAO;
import at.darkdestiny.core.dao.PlanetDefenseDAO;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.Location;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.diplomacy.combat.CombatGroupEntry;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResolver;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResult;
import at.darkdestiny.core.enumeration.ECombatHandlerState;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.enumeration.EDamagedShipRefType;
import at.darkdestiny.core.enumeration.EDefenseType;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.fleet.FleetType;
import at.darkdestiny.core.interfaces.ISimulationSet;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.BattleLog;
import at.darkdestiny.core.model.BattleLogEntry;
import at.darkdestiny.core.model.FleetDetail;
import at.darkdestiny.core.model.FleetOrder;
import at.darkdestiny.core.model.PlanetDefense;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ScrapResource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.service.ShipDesignService;
import at.darkdestiny.core.ships.ShipData;
import at.darkdestiny.core.spacecombat.CombatGroupFleet.UnitTypeEnum;
import at.darkdestiny.core.spacecombat.CombatReportGenerator.CombatType;
import at.darkdestiny.core.spacecombat.combatcontroller.CombatControllerFactory;
import at.darkdestiny.core.utilities.HangarUtilities;
import at.darkdestiny.core.utilities.ShipUtilities;
import at.darkdestiny.framework.QueryKey;
import at.darkdestiny.framework.QueryKeySet;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Dreloc
 */
public class CombatHandler_NEW {

    private static final Logger log = LoggerFactory.getLogger(CombatHandler_NEW.class);

    private static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);
    private static PlayerFleetDAO pfDAO = (PlayerFleetDAO) DAOFactory.get(PlayerFleetDAO.class);
    private static ShipFleetDAO sfDAO = (ShipFleetDAO) DAOFactory.get(ShipFleetDAO.class);
    private static FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);
    private static FleetLoadingDAO flDAO = (FleetLoadingDAO) DAOFactory.get(FleetLoadingDAO.class);
    private static BattleLogDAO blDAO = (BattleLogDAO) DAOFactory.get(BattleLogDAO.class);
    private static BattleLogEntryDAO bleDAO = (BattleLogEntryDAO) DAOFactory.get(BattleLogEntryDAO.class);
    private static ActionDAO aDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private static PlanetDefenseDAO pdDAO = (PlanetDefenseDAO) DAOFactory.get(PlanetDefenseDAO.class);
    private static ConstructionModuleDAO cmDAO = (ConstructionModuleDAO) DAOFactory.get(ConstructionModuleDAO.class);
    private static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static FleetOrderDAO foDAO = (FleetOrderDAO) DAOFactory.get(FleetOrderDAO.class);
    // Constructor variables
    private final ArrayList<ICombatParticipent> combatFleets = new ArrayList<ICombatParticipent>();
    private final RelativeCoordinate relCombatLocation;
    private final AbsoluteCoordinate absCombatLocation; // May be useful in future
    private final CombatReportGenerator crg;
    private final CombatReportGenerator.CombatType combatType;
    private ArrayList<ICombatParticipent> orgFleetList;
    private final HashMap<Integer, ArrayList<Integer>> groupPlayerMap = new HashMap<Integer, ArrayList<Integer>>();
    private CombatGroupResult cgr;
    private ArrayList<CombatGroup> combatGroups = new ArrayList<CombatGroup>();
    private HashMap<Integer, ArrayList<CombatResultEntry>> results = new HashMap<Integer, ArrayList<CombatResultEntry>>();
    private SpaceCombatDataLogger sc_DataLogger = null;
    // Simulation and Testing
    private String battleClass = "at.darkdestiny.core.spacecombat.combatcontroller.SpaceCombatNew";
    //Scrap rate in percent
    public static final double SCRAP_RATE = 0.5d;
    private boolean simulation = false;
    private ISimulationSet iss = null;
    private boolean hasPlanetaryHU_sim = false;
    private boolean hasPlanetaryPA_sim = false;
    private int planetOwner_sim = 0;
    private HashMap<Integer, Integer> planetaryDefenses_sim = null;
    private HashMap<Integer, Integer> mobileDefenseTransferMap_sim = new HashMap<Integer, Integer>();
    private int planet_sim = 0;
    // Handling state of CombatHandler
    private ECombatHandlerState state = null;
    private ArrayList<ScrapResource> generatedScrap = Lists.newArrayList();

    public CombatHandler_NEW(ISimulationSet css) {
        this(css.getCombatType(), css.getFleets(), css.getRelLocation(), css.getAbsLocation(), css.getCombatReportGenerator());
        this.iss = css;

        ArrayList<PlayerPlanet> ppList = ppDAO.findByUserId(css.getPlanetOwner());
        planet_sim = ppList.get(0).getPlanetId();

        if (css.getBattleClass() != null) {
            this.battleClass = css.getBattleClass();
            log.debug("Set BattleClass version to: " + battleClass);
        }

        this.hasPlanetaryHU_sim = css.hasHUShield();
        this.hasPlanetaryPA_sim = css.hasPAShield();
        this.planetOwner_sim = css.getPlanetOwner();
        this.planetaryDefenses_sim = css.getGroundDefense();

        simulation = true;
        state = ECombatHandlerState.INITIALIZED;
    }

    public CombatHandler_NEW(CombatReportGenerator.CombatType type, ArrayList<PlayerFleet> involvedFleets, RelativeCoordinate relLocation, AbsoluteCoordinate absLocation, CombatReportGenerator crg) {
        combatFleets.addAll(involvedFleets);
        absCombatLocation = absLocation;
        relCombatLocation = relLocation;
        this.crg = crg;
        combatType = type;
        state = ECombatHandlerState.INITIALIZED;
    }

    public void executeBattle() {
        if (state != ECombatHandlerState.INITIALIZED) {
            return;
        }

        log.info("Combat location is " + relCombatLocation.getSystemId() + ":" + relCombatLocation.getPlanetId());

        if (relCombatLocation.getSystemId() == 0) {
            DebugBuffer.writeStackTrace("Preventing combat in System 0", new Exception("COMBAT IN SYS 0"));
            throw new RuntimeException("ERROR COMBAT IN SYS 0");
        }

        if (!simulation) {
            // Save participating shipdesigns to the battlelog
            try {
                BattleLog bl = new BattleLog();
                bl.setComment("");
                bl.setAccuracy(0f);
                bl = blDAO.add(bl);
                int id = bl.getId();
                for (ICombatParticipent cp : combatFleets) {
                    PlayerFleetExt pfe = new PlayerFleetExt(cp.getId());
                    for (ShipData sd : pfe.getShipList()) {
                        BattleLogEntry battleLogEntry = bleDAO.findBy(id, sd.getDesignId());
                        if (battleLogEntry != null) {
                            battleLogEntry.setCount(battleLogEntry.getCount() + sd.getCount());
                            bleDAO.update(battleLogEntry);
                        } else {
                            battleLogEntry = new BattleLogEntry();
                            battleLogEntry.setDesignId(sd.getDesignId());
                            if (id != 0) {
                                battleLogEntry.setId(id);
                            }
                            battleLogEntry.setCount(sd.getCount());
                            bleDAO.add(battleLogEntry);
                        }
                    }
                }
            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, "Error while creating additional Data for BattleLog");
                DebugBuffer.writeStackTrace("Errror", e);
            }
        } else {
            // Remove space stations from PlayerFleet Data of owning player
            for (ICombatParticipent cp : combatFleets) {
                if (cp.getUserId() == planetOwner_sim) {
                    PlayerFleetExt pfe = new PlayerFleetExt(cp.getId());
                    for (ShipData sd : pfe.getShipList()) {
                        int designId = sd.getDesignId();

                        ShipDesign sdTmp = sdDAO.findById(designId);
                        if (sdTmp.getType() == EShipType.SPACEBASE) {
                            mobileDefenseTransferMap_sim.put(designId, sd.getCount());

                            ShipFleet sfDel = new ShipFleet();
                            sfDel.setFleetId(cp.getId());
                            sfDel.setDesignId(designId);
                            sfDel = (ShipFleet) sfDAO.find(sfDel).get(0);
                            sfDAO.remove(sfDel);
                        }
                    }
                }
            }
        }

        ArrayList<Integer> playerList = new ArrayList<Integer>();
        for (ICombatParticipent cp : combatFleets) {
            if (!playerList.contains(cp.getUserId())) {
                playerList.add(cp.getUserId());
            }
        }

        // Find stationary defense at current combat location
        ArrayList<Integer> dUsers = hasPlayerDefenses(relCombatLocation);
        for (Integer uId : dUsers) {
            if (!playerList.contains(uId)) {
                playerList.add(uId);
            }

            if (relCombatLocation.getPlanetId() != 0) {
                combatFleets.add(new ActiveDefenseSystem(new Integer(0), uId, "Planetary Defense for Planet #" + relCombatLocation.getPlanetId()));
            } else {
                combatFleets.add(new ActiveDefenseSystem(new Integer(0), uId, "System Defense for System #" + relCombatLocation.getSystemId()));
            }
        }

        // groupPlayerMap = CombatUtilities.getCombatGroupedPlayers(playerList);
        if (simulation) {
            cgr = CombatGroupResolver.getCombatGroupsSimulation(playerList, relCombatLocation, iss.getGroups());
        } else {
            cgr = CombatGroupResolver.getCombatGroups(playerList, relCombatLocation);
        }

        for (CombatGroupEntry cge : cgr.getCombatGroups()) {
            groupPlayerMap.put(cge.getGroupId(), cge.getPlayers());
        }

        orgFleetList = new ArrayList<ICombatParticipent>();
        for (ICombatParticipent cp : combatFleets) {
            if (cp instanceof PlayerFleet) {
                orgFleetList.add(((PlayerFleet) cp).clone());
            } else if (cp instanceof ActiveDefenseSystem) {
                orgFleetList.add(((ActiveDefenseSystem) cp).clone());
            }
        }

        buildCombatGroups();

        try {
            startCombat();

            crg.addCombatResult(combatType, relCombatLocation, absCombatLocation, orgFleetList, combatGroups, generatedScrap);
            compareCombatResultToFleets();
            storeDamageLevels();
            state = ECombatHandlerState.FIGHT_PROCESSED;
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("ERROR IN SPACE COMBAT: ", e);
            state = ECombatHandlerState.ERROR;
        }
    }

    private ArrayList<Integer> hasPlayerDefenses(RelativeCoordinate rc) {
        ArrayList<PlanetDefense> pdList = new ArrayList<PlanetDefense>();
        if (rc == null) {
            return new ArrayList<Integer>();
        }
        if (rc.getPlanetId() != 0) {
            pdList = pdDAO.findByPlanetId(rc.getPlanetId());
        } else {
            pdList = pdDAO.findBySystemId(rc.getSystemId());
        }

        if (simulation) {
            log.debug("====== LOAD SIMULATION DEFENSE ======");

            for (Map.Entry<Integer, Integer> pDefList : planetaryDefenses_sim.entrySet()) {
                PlanetDefense pd = new PlanetDefense();
                pd.setFleetId(0);
                pd.setPlanetId(relCombatLocation.getPlanetId());
                pd.setSystemId(relCombatLocation.getSystemId());
                pd.setCount(pDefList.getValue());
                pd.setType(EDefenseType.TURRET);
                pd.setUserId(planetOwner_sim);
                pd.setUnitId(pDefList.getKey());
                pdList.add(pd);
            }

            for (Map.Entry<Integer, Integer> oDefList : mobileDefenseTransferMap_sim.entrySet()) {
                PlanetDefense pd = new PlanetDefense();
                pd.setFleetId(0);
                pd.setPlanetId(relCombatLocation.getPlanetId());
                pd.setSystemId(relCombatLocation.getSystemId());
                pd.setCount(oDefList.getValue());
                pd.setType(EDefenseType.SPACESTATION);
                pd.setUserId(planetOwner_sim);
                pd.setUnitId(oDefList.getKey());
                pdList.add(pd);
            }
        }

        ArrayList<Integer> players = new ArrayList<Integer>();

        log.debug("LOCATION IS=" + rc.getPlanetId());
        PlayerPlanet pp = ppDAO.findByPlanetId(rc.getPlanetId());

        for (PlanetDefense pd : pdList) {
            if ((pd.getType() == EDefenseType.TURRET) && (!pd.getUserId().equals(pp.getUserId()))) {
                DebugBuffer.warning("Defense of some other player (" + pd.getUserId() + ") still exists on this planet which belongs to " + pp.getUserId() + " (ACTION TAKEN: DELETED)");
                pdDAO.remove(pd);
                continue;
            }

            if (!players.contains(pd.getUserId())) {
                players.add(pd.getUserId());
            }
        }

        return players;
    }

    private ArrayList<ICombatParticipent> findDefenses(RelativeCoordinate rc) {
        ArrayList<ICombatParticipent> defenses = new ArrayList<ICombatParticipent>();

        ArrayList<PlanetDefense> pdList = null;
        if (rc.getPlanetId() != 0) {
            pdList = pdDAO.findByPlanetId(rc.getPlanetId());
        } else {
            pdList = pdDAO.findBySystemId(rc.getSystemId());
        }

        return defenses;
    }

    private void buildCombatGroups() {
        log.debug("======== Build Combat Groups =========");

        for (Map.Entry<Integer, ArrayList<Integer>> gpe : groupPlayerMap.entrySet()) {
            log.debug("Preparing Combat Group " + gpe.getKey());

            CombatGroup cg = new CombatGroup();
            cg.setId(gpe.getKey());

            // Tell each group what to attack
            for (CombatGroupEntry cge : cgr.getCombatGroups()) {
                if (cge.getGroupId() == cg.getId()) {
                    for (Integer enemyGrp : cge.getAttackingGroups()) {
                        for (CombatGroupEntry cgeEnemy : cgr.getCombatGroups()) {
                            if (enemyGrp == cgeEnemy.getGroupId()) {
                                for (Integer eUser : cgeEnemy.getPlayers()) {
                                    cg.addToAttack(eUser);
                                }
                            }
                        }
                    }
                }
            }

            ArrayList<Integer> pList = gpe.getValue();

            int globalShipCount = 0;
            int fleetCount = 0;

            log.debug("Active Group: " + gpe.getKey());
            for (Integer i : gpe.getValue()) {
                log.debug("Associated player " + i);
            }

            for (Iterator<ICombatParticipent> cpIt = combatFleets.iterator(); cpIt.hasNext();) {
                ICombatParticipent cp = cpIt.next();
                if (!pList.contains((int) cp.getUserId())) {
                    continue;
                }

                fleetCount++;

                CombatGroupFleet cgf = new CombatGroupFleet();
                cgf.setUserId(cp.getUserId());
                cgf.setGroupId(cg.getId());
                cgf.setFleetName(cp.getName());
                cgf.setFleetId(cp.getId());

                log.debug("Created CGF for user " + cgf.getUserId() + " with name " + cgf.getFleetName());

                if (cp instanceof PlayerFleet) {
                    log.debug("Found Fleet " + cp.getId() + " for Combat Group " + gpe.getKey());

                    // UNLOAD HANGAR FOR COMBAT
                    HangarUtilities hu = new HangarUtilities();
                    hu.reconstructUnloadedFleetInDB(cp.getId());

                    // LOAD FLEETS
                    log.debug("Load fleet");
                    PlayerFleetExt pfExt = new PlayerFleetExt(cp.getId());
                    at.darkdestiny.core.model.FleetOrder fo = null;

                    if (pfExt.isInFleetFormation()) {
                        // READ RETREAT INFO IF AVAILABLE
                        fo = foDAO.get(pfExt.getBase().getFleetFormationId(), FleetType.FLEET_FORMATION);
                        log.debug("Check retreat factor for fleetformation " + cp.getId());

                        cgf.setFleetFormationId(pfExt.getBase().getFleetFormationId());
                    } else {
                        // READ RETREAT INFO IF AVAILABLE
                        fo = foDAO.get(cp.getId(), FleetType.FLEET);
                        log.debug("Check retreat factor for fleet " + cp.getId());
                    }

                    if (fo != null) {
                        log.debug("Set RetreatFactor to " + fo.getRetreatFactor());
                        cgf.setRetreatFactor(fo.getRetreatFactor());
                        cgf.setRetreatTo(fo.getRetreatTo());
                        cgf.setRetreatToType(fo.getRetreatToType());
                    } else {
                        log.debug("Nothing found");
                        cgf.setRetreatFactor(100);
                    }

                    int localShipCount = 0;
                    Map<CombatGroupFleet.DesignDescriptor, CombatUnit> shipDesignCount = new HashMap<CombatGroupFleet.DesignDescriptor, CombatUnit>();

                    for (ShipData sd : pfExt.getShipList()) {
                        CombatGroupShipDesign cgsd = new CombatGroupShipDesign();
                        cgsd.setDesignId(sd.getId());
                        cgsd.setCount(sd.getCount());

                        if (sd.getCount() <= 0) {
                            DebugBuffer.warning("Found Ship [" + sd.getDesignId() + "] with count " + sd.getCount() + " in fleet "
                                    + pfExt.getName() + "(" + pfExt.getName() + ")");
                            continue;
                        }

                        localShipCount += sd.getCount();

                        shipDesignCount.put(
                                new CombatGroupFleet.DesignDescriptor(
                                        sd.getDesignId(), CombatGroupFleet.UnitTypeEnum.SHIP),
                                new CombatUnit(sd.getDesignId(), sd.getId(), sd.getCount(), CombatGroupFleet.UnitTypeEnum.SHIP));
                    }

                    globalShipCount += localShipCount;
                    cgf.setShipDesign(shipDesignCount);

                    log.debug("Putting " + shipDesignCount.size() + " designs into fleet " + cp.getId());
                } else if (cp instanceof ActiveDefenseSystem) {
                    // LOAD DEFENSE SYSTEMS
                    log.debug("Load Defense");
                    ArrayList<PlanetDefense> pdList = null;

                    if (relCombatLocation.getPlanetId() != 0) {
                        pdList = pdDAO.findByPlanetAndUserId(relCombatLocation.getPlanetId(), cp.getUserId());
                        cgf.setPlanetDefense(true);
                    } else {
                        pdList = pdDAO.findBySystemAndUserId(relCombatLocation.getSystemId(), cp.getUserId());
                        cgf.setSystemDefense(true);
                    }

                    int localShipCount = 0;
                    Map<CombatGroupFleet.DesignDescriptor, CombatUnit> shipDesignCount = new HashMap<CombatGroupFleet.DesignDescriptor, CombatUnit>();

                    // ADD HERE - SIMULATED BUILDINGS
                    // if we run a simulation we need to simulate the defense entries
                    if (simulation) {
                        log.debug("====== LOAD SIMULATION DEFENSE ======");

                        for (Map.Entry<Integer, Integer> pDefList : planetaryDefenses_sim.entrySet()) {
                            PlanetDefense pd = new PlanetDefense();
                            pd.setFleetId(0);
                            pd.setPlanetId(relCombatLocation.getPlanetId());
                            pd.setSystemId(relCombatLocation.getSystemId());
                            pd.setCount(pDefList.getValue());
                            pd.setType(EDefenseType.TURRET);
                            pd.setUserId(planetOwner_sim);
                            pd.setUnitId(pDefList.getKey());
                            pdList.add(pd);
                        }

                        for (Map.Entry<Integer, Integer> oDefList : mobileDefenseTransferMap_sim.entrySet()) {
                            PlanetDefense pd = new PlanetDefense();
                            pd.setFleetId(0);
                            pd.setPlanetId(relCombatLocation.getPlanetId());
                            pd.setSystemId(relCombatLocation.getSystemId());
                            pd.setCount(oDefList.getValue());
                            pd.setType(EDefenseType.SPACESTATION);
                            pd.setUserId(planetOwner_sim);
                            pd.setUnitId(oDefList.getKey());
                            pdList.add(pd);
                        }
                    }

                    for (PlanetDefense pd : pdList) {
                        CombatGroupShipDesign cgsd = new CombatGroupShipDesign();
                        cgsd.setDesignId(pd.getUnitId());
                        cgsd.setCount(pd.getCount());
                        localShipCount += pd.getCount();

                        if (pd.getCount() <= 0) {
                            DebugBuffer.warning("Found Planetdefense [" + pd.getType().name() + "/" + pd.getUnitId() + "] with count " + pd.getCount() + " for location "
                                    + relCombatLocation.getSystemId() + ":" + relCombatLocation.getPlanetId());
                            continue;
                        }

                        CombatGroupFleet.UnitTypeEnum unitType = null;
                        if (pd.getType() == EDefenseType.TURRET) {
                            log.debug("Load Surface Defense");
                            unitType = CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE;
                        } else if (pd.getType() == EDefenseType.SPACESTATION) {
                            log.debug("Load Plattform Defense");
                            unitType = CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE;
                        }

                        shipDesignCount.put(
                                new CombatGroupFleet.DesignDescriptor(
                                        pd.getUnitId(), unitType),
                                new CombatUnit(pd.getUnitId(), pd.getCount(), unitType));
                    }

                    globalShipCount += localShipCount;
                    cgf.setShipDesign(shipDesignCount);
                }

                cg.addFleet(cgf);
                cpIt.remove();
            }

            cg.setShipCount(globalShipCount);
            cg.setFleetCount(fleetCount);
            combatGroups.add(cg);
        }
    }

    private void startCombat() {
        ISpaceCombat sc = null;

        if (simulation) {
            sc = CombatControllerFactory.getCombatController(battleClass, CombatHandler.ORBITAL_COMBAT, planet_sim);
        } else {
            int combatNumeric = 0;
            if (combatType.equals(CombatType.ORBITAL_COMBAT)) {
                combatNumeric = CombatHandler.ORBITAL_COMBAT;
                sc = CombatControllerFactory.getCombatController(battleClass, combatNumeric, relCombatLocation.getPlanetId());
            } else if (combatType.equals(CombatType.SYSTEM_COMBAT)) {
                combatNumeric = CombatHandler.SYSTEM_COMBAT;
                sc = CombatControllerFactory.getCombatController(battleClass, combatNumeric, relCombatLocation.getSystemId());
            }
        }

        sc.setISimulationSet(iss);
        CombatContext.createCombatContext(sc);
        // sc.setSimulationParameters(speedFactor,probabilityPA,minDamagePA,maxDamagePA,simultanAttack);

        sc.setCombatGroups(combatGroups);
        sc.setCombatGroupResult(cgr);

        log.info("Starting combat calculation");
        sc.startFightCalc();
        sc_DataLogger = sc.getLoggingData();

        CombatContext.destroyCombatContext();
        log.info("Starting combat calculation finished");
    }

    public SpaceCombatDataLogger getLoggingData() {
        if (state != ECombatHandlerState.FIGHT_PROCESSED) {
            return null;
        }
        return sc_DataLogger;
    }

    private void storeDamageLevels() {
        for (CombatGroup cg : combatGroups) {
            log.debug("CGF Size: " + cg.getFleetList().size());
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                if (cgf.isPlanetDefense() || cgf.isSystemDefense()) {
                    continue;
                }

                PlayerFleetExt refFleet = null;

                // log.debug("CF Size: " + orgFleetList.size());
                for (ICombatParticipent cp : orgFleetList) {
                    // log.debug("Compare " + cp.getId() + " to " + cgf.getFleetId());
                    if (cp.getId() == cgf.getFleetId()) {
                        refFleet = new PlayerFleetExt(cp.getId());
                        break;
                    }
                }

                ArrayList<ShipData> ships = refFleet.getShipList();

                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                    for (ShipData sd : ships) {
                        if (sd.getDesignId() == entry.getKey().getId()) {
                            log.debug("Ship " + sd.getDesignName() + " had " + (sd.getCount() - entry.getValue().getSurvivors()) + "/" + sd.getCount() + " losses");

                            QueryKeySet qks = new QueryKeySet();
                            qks.addKey(new QueryKey("shipFleetId", sd.getId()));
                            dsDAO.removeAll(qks);
                            log.debug("Cleaned id " + sd.getId());
                            log.debug("MINOR: " + entry.getValue().getMinorDamage());
                            log.debug("LIGHT: " + entry.getValue().getLightDamage());
                            log.debug("MEDIUM: " + entry.getValue().getMediumDamage());
                            log.debug("HEAVY: " + entry.getValue().getHeavyDamage());

                            ArrayList<at.darkdestiny.core.model.DamagedShips> newEntries = new ArrayList<at.darkdestiny.core.model.DamagedShips>();
                            if (entry.getValue().getMinorDamage() > 0) {
                                at.darkdestiny.core.model.DamagedShips newEntry = new at.darkdestiny.core.model.DamagedShips();
                                newEntry.setCount(entry.getValue().getMinorDamage());
                                newEntry.setShipFleetId(sd.getId());
                                newEntry.setDamageLevel(EDamageLevel.MINORDAMAGE);
                                newEntry.setRefTable(EDamagedShipRefType.SHIPFLEET);
                                newEntries.add(newEntry);
                            }
                            if (entry.getValue().getLightDamage() > 0) {
                                at.darkdestiny.core.model.DamagedShips newEntry = new at.darkdestiny.core.model.DamagedShips();
                                newEntry.setCount(entry.getValue().getLightDamage());
                                newEntry.setShipFleetId(sd.getId());
                                newEntry.setDamageLevel(EDamageLevel.LIGHTDAMAGE);
                                newEntry.setRefTable(EDamagedShipRefType.SHIPFLEET);
                                newEntries.add(newEntry);
                            }
                            if (entry.getValue().getMediumDamage() > 0) {
                                at.darkdestiny.core.model.DamagedShips newEntry = new at.darkdestiny.core.model.DamagedShips();
                                newEntry.setCount(entry.getValue().getMediumDamage());
                                newEntry.setShipFleetId(sd.getId());
                                newEntry.setDamageLevel(EDamageLevel.MEDIUMDAMAGE);
                                newEntry.setRefTable(EDamagedShipRefType.SHIPFLEET);
                                newEntries.add(newEntry);
                            }
                            if (entry.getValue().getHeavyDamage() > 0) {
                                at.darkdestiny.core.model.DamagedShips newEntry = new at.darkdestiny.core.model.DamagedShips();
                                newEntry.setCount(entry.getValue().getHeavyDamage());
                                newEntry.setShipFleetId(sd.getId());
                                newEntry.setDamageLevel(EDamageLevel.HEAVYDAMAGE);
                                newEntry.setRefTable(EDamagedShipRefType.SHIPFLEET);
                                newEntries.add(newEntry);
                            }

                            dsDAO.insertAll(newEntries);
                        }
                    }
                }
            }
        }
    }

    private void compareCombatResultToFleets() {
        for (CombatGroup cg : combatGroups) {
            log.info("CGF Size: " + cg.getFleetList().size());
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                PlayerFleetExt refFleet = null;

                log.info("CF Size: " + orgFleetList.size());

                for (ICombatParticipent cp : orgFleetList) {
                    // log.info( "Compare " + cp.getId() + " to " + cgf.getFleetId());

                    if (cp.getId() == cgf.getFleetId()) {
                        if (cgf.isPlanetDefense() || cgf.isSystemDefense()) {
                            ArrayList<PlanetDefense> pdList = null;

                            if (cgf.isPlanetDefense()) {
                                pdList = pdDAO.findByPlanetAndUserId(relCombatLocation.getPlanetId(), cp.getUserId());
                            } else {
                                pdList = pdDAO.findBySystemAndUserId(relCombatLocation.getSystemId(), cp.getUserId());
                            }

                            for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                                for (PlanetDefense pd : pdList) {
                                    if (pd.getUnitId() == entry.getKey().getId()) {
                                        String name = "Unknown";

                                        if (entry.getKey().getType() == UnitTypeEnum.SURFACE_DEFENSE) {
                                            name = ConstructionService.findConstructionById(entry.getKey().getId()).getName();
                                        } else if (entry.getKey().getType() == UnitTypeEnum.PLATTFORM_DEFENSE) {
                                            name = ShipDesignService.getShipDesign(entry.getKey().getId()).getName();
                                        }

                                        log.info("Defense " + name + " had " + (pd.getCount() - entry.getValue().getSurvivors()) + "/" + pd.getCount() + " losses");

                                        if (results.containsKey(cgf.getFleetId())) {
                                            ArrayList<CombatResultEntry> creList = results.get(cgf.getFleetId());
                                            creList.add(new CombatResultEntry(pd.getUnitId(), pd.getCount(), entry.getValue().getSurvivors(), entry.getKey().getType()));
                                        } else {
                                            ArrayList<CombatResultEntry> creList = new ArrayList<CombatResultEntry>();
                                            creList.add(new CombatResultEntry(pd.getUnitId(), pd.getCount(), entry.getValue().getSurvivors(), entry.getKey().getType()));
                                            results.put(cgf.getFleetId(), creList);
                                        }
                                    }
                                }
                            }
                        } else {
                            refFleet = new PlayerFleetExt(cp.getId());
                            ArrayList<ShipData> ships = refFleet.getShipList();

                            for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                                for (ShipData sd : ships) {
                                    if (sd.getDesignId() == entry.getKey().getId()) {
                                        log.info("Ship " + sd.getDesignName() + " had " + (sd.getCount() - entry.getValue().getSurvivors()) + "/" + sd.getCount() + " losses");

                                        if (results.containsKey(cgf.getFleetId())) {
                                            ArrayList<CombatResultEntry> creList = results.get(cgf.getFleetId());
                                            creList.add(new CombatResultEntry(sd.getDesignId(), sd.getCount(), entry.getValue().getSurvivors(), entry.getKey().getType()));
                                        } else {
                                            ArrayList<CombatResultEntry> creList = new ArrayList<CombatResultEntry>();
                                            creList.add(new CombatResultEntry(sd.getDesignId(), sd.getCount(), entry.getValue().getSurvivors(), entry.getKey().getType()));
                                            results.put(cgf.getFleetId(), creList);
                                        }
                                    }
                                }
                            }
                        }

                        break;
                    }
                }
            }
        }
    }

    /**
     * This method returns fleets which have retreated or resigned during combat
     *
     * @return A Hashset of fleet id's which have retreated or resigned during
     * combat
     */
    public HashSet<Integer> getRetreatedOrResignedFleets() {
        log.debug("Get Retreated Fleets");

        if (state != ECombatHandlerState.RESULTS_PROCESSED) {
            return null;
        }

        HashSet<Integer> retreatedFleets = new HashSet<Integer>();

        for (CombatGroup cg : combatGroups) {
            log.info("Looping Combat group " + cg.getId());
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                log.info("Looping CombatGroupFleet " + cgf.getFleetId());
                if (!cgf.isPlanetDefense()
                        && !cgf.isSystemDefense()) {
                    if (cgf.isResigned() || cgf.isRetreated()) {
                        log.info("Fleet resigned!");
                        retreatedFleets.add(cgf.getFleetId());
                    } else {
                        log.info("Fleet not resigned -> IGNORE");
                    }
                } else {
                    log.info("Fleet is defense -> SKIP");
                }
            }
        }

        return retreatedFleets;
    }

    /**
     * This method writes losses of combat participents back to Database
     *
     * Additionally it produces scrap in regard of the destroyed ships
     *
     * @return A Hashset of fleet id's which have been destroyed due to combat
     */
    public HashSet<Integer> processCombatResults() {
        if (state != ECombatHandlerState.FIGHT_PROCESSED) {
            return null;
        }

        TreeMap<Integer, ScrapResource> scrap = Maps.newTreeMap();
        Location loc = new Location(relCombatLocation);

        log.debug(">>>> PROCESSING COMBAT RESULTS");

        HashSet<Integer> destroyedFleets = new HashSet<Integer>();

        for (Map.Entry<Integer, ArrayList<CombatResultEntry>> resEntry : results.entrySet()) {
            ArrayList<CombatResultEntry> creList = resEntry.getValue();

            boolean survivors = false;

            for (CombatResultEntry cre : creList) {
                if (cre.getSurvivors() > 0) {
                    survivors = true;
                    break;
                }
            }

            DebugBuffer.addLine(DebugLevel.DEBUG, "FLEET " + resEntry.getKey() + " is survivor? " + survivors);

            // Destroy fleet and all related data
            if (!survivors) {
                if (resEntry.getKey() != 0) { // SHIP HANDLING
                    PlayerFleet pf = pfDAO.findById(resEntry.getKey());
                    DebugBuffer.trace("TLBUG DEBUG:" + pf.getName() + " was destroyed in combat");

                    produceScrapForFleet(pf, scrap);
                    pfDAO.remove(pf);

                    FleetOrder fo = foDAO.get(pf.getId(), FleetType.FLEET);
                    if (fo != null) {
                        foDAO.remove(fo);
                    }

                    DebugBuffer.trace("TLBUG DEBUG: Removed Fleet Orders");

                    // DebugBuffer.addLine(DebugLevel.DEBUG, "Remove Fleet " + pf.getId());
                    // Fleet Details
                    FleetDetail fd = fdDAO.findByFleetId(pf.getId());
                    if (fd != null) {
                        DebugBuffer.trace("TLBUG DEBUG: Removed Fleet Detail " + fd.getId());
                        // DebugBuffer.addLine(DebugLevel.DEBUG, "Delete Fleetdetail " + fd);
                        fdDAO.remove(fd);

                        // Action Entry
                        Action a = aDAO.findFlightEntryByFleetDetail(fd.getId());
                        DebugBuffer.trace("TLBUG DEBUG: Removed Action " + a.getRefTableId());
                        // DebugBuffer.addLine(DebugLevel.DEBUG, "Delete Action " + a);
                        aDAO.remove(a);
                    }

                    // Fleet Loading
                    QueryKeySet qks = new QueryKeySet();
                    qks.addKey(new QueryKey("fleetId", pf.getId()));
                    int flDel = flDAO.removeAll(qks);
                    // DebugBuffer.addLine(DebugLevel.DEBUG, "Remove all fleetloading (" + flDel + ")");

                    // Ship Fleet
                    int sfDel = sfDAO.removeAll(qks);
                    DebugBuffer.trace("TLBUG DEBUG: Removed all shipfleets");
                    // DebugBuffer.addLine(DebugLevel.DEBUG, "Remove all shipfleet (" + sfDel + ")");

                    log.debug("Adding " + pf.getId() + " to destroyed fleets");
                    DebugBuffer.trace("TLBUG DEBUG: Adding " + pf.getName() + " to destroyed fleets");
                    // DebugBuffer.addLine(DebugLevel.DEBUG, "Adding " + pf.getId() + " to destroyed fleets");
                    destroyedFleets.add(pf.getId());
                } else { // DEFENSE HANDLING
                    if (relCombatLocation.getPlanetId() != 0) {
                        QueryKeySet qks = new QueryKeySet();
                        qks.addKey(new QueryKey("planetId", relCombatLocation.getPlanetId()));
                        pdDAO.removeAll(qks);
                    } else {
                        QueryKeySet qks = new QueryKeySet();
                        qks.addKey(new QueryKey("systemId", relCombatLocation.getSystemId()));
                        pdDAO.removeAll(qks);
                    }
                }
            } else {
                log.debug(">>>> WE HAVE SURVIVORS HERE");

                if (resEntry.getKey() != 0) { // SHIP HANDLING
                    log.debug(">>>> WE HAVE EVEN SHIPS HERE");

                    // Load all ships in this fleet back into hangars
                    // Applies only to fleet formations to avoid problems
                    PlayerFleet pf = pfDAO.findById(resEntry.getKey());

                    for (CombatResultEntry cre : creList) {
                        ShipFleet sf = sfDAO.getByFleetDesign(resEntry.getKey(), cre.getDesignId());
                        if (sf == null) {
                            DebugBuffer.addLine(DebugLevel.ERROR, "Try to get non-existant shipfleet entry for fleet " + resEntry.getKey() + " and design " + cre.getDesignId());
                            continue;
                        }

                        if (cre.getSurvivors() > 0) {
                            DebugBuffer.addLine(DebugLevel.DEBUG, "Set count for design " + sf.getDesignId() + " to " + cre.getSurvivors());
                            sf.setCount(cre.getSurvivors());
                            produceScrapForShipFleet(sf, scrap, cre.getSurvivors(), loc.getLocationType(), loc.getLocationId());
                            sfDAO.update(sf);
                        } else {
                            DebugBuffer.addLine(DebugLevel.DEBUG, "Delete design " + sf.getDesignId() + " from Fleet");
                            produceScrapForShipFleet(sf, scrap, loc.getLocationType(), loc.getLocationId());
                            sfDAO.remove(sf);
                        }
                    }

                    // Check if we have to destroy fleet loading
                    ShipUtilities.checkExcessFleetLoading(resEntry.getKey());

                    log.debug("TRY LOADING HANGAR FOR FLEET " + resEntry.getKey() + " " + pf.getName() + " FORMATION: " + pf.getFleetFormationId());

                    if (pf.getFleetFormationId() != 0) {
                        log.debug("DO IT FAGGOT");
                        HangarUtilities hu = new HangarUtilities();
                        hu.reconstructUnloadedFleetInDB(pf.getId());
                        hu.initializeFleet(pf.getId());
                        hu.reconstructFleetInDB(pf.getId(), 0);
                    }
                } else { // DEFENSE HANDLING
                    if (relCombatLocation.getPlanetId() != 0) { // ORBITAL
                        ArrayList<PlanetDefense> pdList = pdDAO.findByPlanet(relCombatLocation.getPlanetId(), EDefenseType.SPACESTATION);
                        updateOrDeleteDefense(pdList, creList, UnitTypeEnum.PLATTFORM_DEFENSE);

                        pdList = pdDAO.findByPlanet(relCombatLocation.getPlanetId(), EDefenseType.TURRET);
                        updateOrDeleteDefense(pdList, creList, UnitTypeEnum.SURFACE_DEFENSE);
                    } else { // SYSTEM
                        ArrayList<PlanetDefense> pdList = pdDAO.findBySystem(relCombatLocation.getSystemId(), EDefenseType.SPACESTATION);
                        updateOrDeleteDefense(pdList, creList, UnitTypeEnum.PLATTFORM_DEFENSE);

                        pdList = pdDAO.findBySystem(relCombatLocation.getSystemId(), EDefenseType.TURRET);
                        updateOrDeleteDefense(pdList, creList, UnitTypeEnum.SURFACE_DEFENSE);
                    }
                }
            }
        }

        this.generatedScrap.clear();
        this.generatedScrap.addAll(scrap.descendingMap().values());
        persistScrap(scrap);

        state = ECombatHandlerState.RESULTS_PROCESSED;
        return destroyedFleets;
    }

    private static void persistScrap(TreeMap<Integer, ScrapResource> scrap) {
        try {
            for (Map.Entry<Integer, ScrapResource> entry : scrap.entrySet()) {
                ScrapResource scrapResource = Service.scrapResourceDAO.findBy(entry.getValue().getLocationId(), entry.getValue().getLocationType(), entry.getKey());
                if (scrapResource == null) {
                    Service.scrapResourceDAO.add(entry.getValue());
                } else {
                    scrapResource.setQuantity(scrapResource.getQuantity() + entry.getValue().getQuantity());
                    Service.scrapResourceDAO.update(scrapResource);
                }
            }
        } catch (Exception e) {
            DebugBuffer.error("Error while persisting scrap", e);
        }
    }

    public static void produceScrapForShipFleet(ShipFleet shipFleet, TreeMap<Integer, ScrapResource> scrap, Integer survivors, ELocationType locationType, Integer locationId) {

        log.debug(">>>> PRODUCE SCRAP <<<<<<");
        log.debug("shipFleet : " + shipFleet.getId());
        log.debug("survivors : " + survivors);
        log.debug("locationType : " + locationType.toString());
        log.debug("locationId : " + locationId);
        ShipDesign shipDesign = Service.shipDesignDAO.findById(shipFleet.getDesignId());
        ShipDesignExt shipDesignExt = new ShipDesignExt(shipDesign);
        for (Map.Entry<Integer, RessAmountEntry> entry : shipDesignExt.getRessCost().getRess().entrySet()) {
            Ressource resource = Service.ressourceDAO.findById(entry.getKey());
            if (!resource.getTransportable()) {
                continue;
            }

            int count = shipFleet.getCount();
            count -= survivors;

            long newScrap = count * (long) (entry.getValue().getQty() * SCRAP_RATE);
            log.debug("resscost : " + entry.getValue().getQty());
            log.debug("newScrap : " + newScrap + " resourceId : " + resource.getId());
            if (newScrap > 0) {
                ScrapResource scrapResource = scrap.get(entry.getKey());
                if (scrapResource == null) {
                    scrapResource = new ScrapResource();

                    DebugBuffer.addLine(DebugLevel.TRACE, "Set location type " + locationType);
                    if (locationType == ELocationType.TRANSIT) {
                        new Exception().printStackTrace();
                    }
                    scrapResource.setLocation(locationType, locationId);
                    scrapResource.setQuantity(newScrap);
                    scrapResource.setResourceId(entry.getKey());
                    scrap.put(entry.getKey(), scrapResource);
                } else {
                    scrapResource.setQuantity(scrapResource.getQuantity() + newScrap);
                }
            }

        }
    }

    @Deprecated
    public void produceScrapForShipFleet(ShipFleet shipFleet, TreeMap<Integer, ScrapResource> scrap, ELocationType locationType, Integer locationId) {
        // Use location of combat not fleet!
        // produceScrapForShipFleet(shipFleet, scrap, 0, locationType, locationId);
        log.debug(">>>> SCRAP 1 <<<<<<");
        produceScrapForShipFleet(shipFleet, scrap);
    }

    public void produceScrapForShipFleet(ShipFleet shipFleet, TreeMap<Integer, ScrapResource> scrap) {
        Location loc = new Location(relCombatLocation);
        produceScrapForShipFleet(shipFleet, scrap, 0, loc.getLocationType(), loc.getLocationId());
        log.debug(">>>> SCRAP 2 <<<<<<");
    }

    public void produceScrapForFleet(PlayerFleet playerFleet, TreeMap<Integer, ScrapResource> scrap) {
        log.debug(">>>> SCRAP 3 <<<<<<");
        // Use location of combat not fleet!
        Location loc = new Location(relCombatLocation);
        for (ShipFleet shipFleet : Service.shipFleetDAO.findByFleetId(playerFleet.getId())) {
            produceScrapForShipFleet(shipFleet, scrap, 0, loc.getLocationType(), loc.getLocationId());
        }
    }

    private void updateOrDeleteDefense(ArrayList<PlanetDefense> pdList, ArrayList<CombatResultEntry> creList, UnitTypeEnum unitType) {
        for (CombatResultEntry cre : creList) {
            if (cre.getType() != unitType) {
                continue;
            }

            for (PlanetDefense pd : pdList) {
                if (cre.getDesignId() == pd.getUnitId()) {
                    if (cre.getSurvivors() > 0) {
                        pd.setCount(cre.getSurvivors());
                        pdDAO.update(pd);
                    } else {
                        pdDAO.remove(pd);
                    }
                }
            }
        }
    }

    public void printCombatGroups() {
        log.debug("Generated " + combatGroups.size());

        for (CombatGroup cg : combatGroups) {
            log.debug("Checking Group " + cg.getId());
            log.debug("This group has " + cg.getFleetList().size() + " fleets and " + cg.getShipCount() + " ships");

            for (CombatGroupFleet cgf : cg.getFleetList()) {
                log.debug("Checking fleet " + cgf.getFleetName() + "[" + cgf.getFleetIdStr() + "]");
                for (Map.Entry<CombatGroupFleet.DesignDescriptor, CombatUnit> entry : cgf.getShipDesign().entrySet()) {
                    log.debug("Found design " + entry.getKey().getId() + " [" + entry.getKey().getType() + "] with count " + entry.getValue().getSurvivors());
                }
            }
        }
    }
}
