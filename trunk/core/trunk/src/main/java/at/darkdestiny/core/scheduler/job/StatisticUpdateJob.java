/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.scheduler.job;

 import at.darkdestiny.core.CheckForUpdate;import at.darkdestiny.core.statistic.CalculateImperialOverview;
import at.darkdestiny.core.statistic.UserStatisticsUpdater;
import at.darkdestiny.util.DebugBuffer;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Admin
 */
public class StatisticUpdateJob implements Job {

    private static final Logger log = LoggerFactory.getLogger(StatisticUpdateJob.class);

    public void execute(JobExecutionContext jec) throws JobExecutionException {
        try {
            CheckForUpdate.requestUpdate();

            log.info( "Calculation of statistic started");

            UserStatisticsUpdater updater = new UserStatisticsUpdater();
            updater.setName("Statistic Update");
            updater.start();

            /* Interne Statistikberechnung */
            CalculateImperialOverview impCalc = new CalculateImperialOverview();
            impCalc.setName("Imperial Statistic Update");
            impCalc.start();

            log.info( "Calculation of statistic ended");
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in statistic: ", e);
            e.printStackTrace();
        }
    }
}
