/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

 import at.darkdestiny.core.enumeration.ETerritoryMapShareType;import at.darkdestiny.core.enumeration.ETerritoryMapType;
import at.darkdestiny.core.enumeration.ETerritoryType;
import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.TerritoryMap;
import at.darkdestiny.core.model.TerritoryMapShare;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.notification.NotificationBuffer;
import at.darkdestiny.core.notification.TerritoryNotification;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.TerritoryPermissionResult;
import at.darkdestiny.core.result.TerritorySaveResult;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.enumeration.ETerritoryMapShareType;
import at.darkdestiny.core.enumeration.ETerritoryMapType;
import at.darkdestiny.core.enumeration.ETerritoryType;
import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.TerritoryMap;
import at.darkdestiny.core.model.TerritoryMapShare;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.notification.NotificationBuffer;
import at.darkdestiny.core.notification.TerritoryNotification;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.TerritoryPermissionResult;
import at.darkdestiny.core.result.TerritorySaveResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class TerritoryService {

    private static final Logger log = LoggerFactory.getLogger(TerritoryService.class);

    public static ArrayList<BaseResult> renameTerritory(int userId, Map<String, String[]> params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        TerritoryMap tm = Service.territoryMapDAO.findById(Integer.parseInt(params.get("territoryId")[0]));

        if (tm.getType().equals(ETerritoryMapType.USER) && tm.getRefId() == userId) {
            tm.setName(params.get("name")[0]);
            tm.setDescription(params.get("description")[0]);
            Service.territoryMapDAO.update(tm);
        } else {
            result.add(new BaseResult("No Rights", true));
        }
        return result;

    }

    public static TerritoryMap getTerritory(int userId, int territoryId) {
        TerritoryMap tm = Service.territoryMapDAO.findById(territoryId);
        if (tm.getType() != ETerritoryMapType.USER) {
            return null;
        }
        if (tm.getRefId() != userId) {
            return null;
        }

        return tm;
    }

    public static ArrayList<BaseResult> changeShow(int userId, Map<String, String[]> params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        TerritoryMap tm = Service.territoryMapDAO.findById(Integer.parseInt(params.get("territoryId")[0]));
        Boolean change = Boolean.parseBoolean(params.get("change")[0]);

        TerritoryMapShare tms = Service.territoryMapShareDAO.findBy(tm.getId(), userId, ETerritoryMapShareType.USER);

        if (tms != null) {
            if (tms.getShow() && !change) {
                tms.setShow(false);
                Service.territoryMapShareDAO.update(tms);
            } else if (!tms.getShow() && change) {
                tms.setShow(true);
                Service.territoryMapShareDAO.update(tms);
            }
        }

        tms = Service.territoryMapShareDAO.findBy(tm.getId(), userId, ETerritoryMapShareType.USER_BY_ALL);

        if (tms != null) {
            if (tms.getShow() && !change) {
                tms.setShow(false);
                Service.territoryMapShareDAO.update(tms);
            } else if (!tms.getShow() && change) {
                tms.setShow(true);
                Service.territoryMapShareDAO.update(tms);
            }
        }

        tms = Service.territoryMapShareDAO.findBy(tm.getId(), userId, ETerritoryMapShareType.USER_BY_ALLIANCE);

        if (tms != null) {
            if (tms.getShow() && !change) {
                tms.setShow(false);
                Service.territoryMapShareDAO.update(tms);
            } else if (!tms.getShow() && change) {
                tms.setShow(true);
                Service.territoryMapShareDAO.update(tms);
            }
        }

        return result;

    }

    public static ArrayList<BaseResult> updatePermissions(int userId, int territoryId, Map params) {

        ArrayList<BaseResult> result = new ArrayList<BaseResult>();

        HashMap<ETerritoryMapShareType, HashMap<Integer, TerritoryMapShare>> permissions = new HashMap<ETerritoryMapShareType, HashMap<Integer, TerritoryMapShare>>();

        DebugBuffer.addLine(DebugLevel.DEBUG, "updating territory : " + territoryId + " for user : " + userId);

        for (Object entry : params.entrySet()) {
            Map.Entry me = (Map.Entry) entry;
            String parName = (String) me.getKey();
            if (parName.startsWith("perm_")) {
                String parsed = parName.replace("perm_", "");
                String idString = parsed.substring(0, parsed.indexOf("-"));
                int id = Integer.parseInt(idString);
                parsed = parsed.substring(parsed.indexOf("-") + 1, parsed.length());
                String typeString = parsed.substring(0, parsed.indexOf("-"));
                ETerritoryMapShareType type = ETerritoryMapShareType.valueOf(typeString);


                DebugBuffer.addLine(DebugLevel.DEBUG, "Found a permission " + parName + " => " + ((String[]) me.getValue())[0]);

                HashMap<Integer, TerritoryMapShare> typePermissions = null;
                if (type.equals(ETerritoryMapShareType.ALLIANCE)) {
                    typePermissions = permissions.get(type);
                } else if (type.equals(ETerritoryMapShareType.USER)) {
                    typePermissions = permissions.get(type);
                } else if (type.equals(ETerritoryMapShareType.ALL)) {
                    typePermissions = permissions.get(type);
                } else {
                    continue;
                }
                DebugBuffer.addLine(DebugLevel.DEBUG, "Type: " + type.toString());

                if (typePermissions == null) {
                    typePermissions = new HashMap<Integer, TerritoryMapShare>();
                }
                TerritoryMapShare tms = typePermissions.get(id);
                if (tms == null) {
                    tms = new TerritoryMapShare();
                    tms.setType(type);
                    tms.setRefId(id);
                    tms.setTerritoryId(territoryId);
                    tms.setShow(false);
                    typePermissions.put(id, tms);
                }


                DebugBuffer.addLine(DebugLevel.DEBUG, "Putting");
                permissions.put(type, typePermissions);

            } else if (parName.equalsIgnoreCase("terrType")) {
                String terrType = ((String[]) me.getValue())[0];
                DebugBuffer.addLine(DebugLevel.DEBUG, "Found a terrType " + parName + " => " + ((String[]) me.getValue())[0]);

                TerritoryMap tm = TerritoryService.getTerritory(userId, territoryId);
                tm.setTerritoryType(ETerritoryType.valueOf(terrType));
                Service.territoryMapDAO.update(tm);
            }
        }
        updatePermissions(territoryId, permissions);

        return result;
    }

    private static void addUserAddedNotification(TerritoryMapShare tms) {
        TerritoryNotification tn = new TerritoryNotification(tms.getRefId(), tms, TerritoryNotification.ETerritoryNotificationType.ADDED);
        NotificationBuffer.addTerritoryNotification(tn);


    }

    private static void addUserRemovedNotification(TerritoryMapShare tms) {
        TerritoryNotification tn = new TerritoryNotification(tms.getRefId(), tms, TerritoryNotification.ETerritoryNotificationType.DELETED);
        NotificationBuffer.addTerritoryNotification(tn);

    }

    public static void updateAllTerritoryPermissions() {
        for (TerritoryMap tm : (ArrayList<TerritoryMap>) Service.territoryMapDAO.findAll()) {
            updateTerritoryPermissions(tm.getId());
        }
    }

    public static void updateTerritoryPermissions(int territoryId) {

        //Build up permissions
        HashMap<ETerritoryMapShareType, HashMap<Integer, TerritoryMapShare>> permissions = new HashMap<ETerritoryMapShareType, HashMap<Integer, TerritoryMapShare>>();
        for (TerritoryMapShare tms : Service.territoryMapShareDAO.findByTerritoryId(territoryId)) {
            if (tms.getType().equals(ETerritoryMapShareType.USER) || tms.getType().equals(ETerritoryMapShareType.ALLIANCE)
                    || tms.getType().equals(ETerritoryMapShareType.ALL)) {
                HashMap<Integer, TerritoryMapShare> perm = permissions.get(tms.getType());
                if (perm == null) {
                    perm = new HashMap<Integer, TerritoryMapShare>();

                }
                perm.put(tms.getRefId(), tms);
                permissions.put(tms.getType(), perm);
            }
        }


        TerritorySaveResult saveResult = new TerritorySaveResult();
        for (Map.Entry<ETerritoryMapShareType, HashMap<Integer, TerritoryMapShare>> entry : permissions.entrySet()) {
            savePerms(saveResult, entry.getValue(), territoryId, entry.getKey());
        }
        deletePerms(saveResult, territoryId);
    }

    public static TerritoryPermissionResult getTerritoryPermissions(int userId, int territoryId) {
        TerritoryPermissionResult tpr = new TerritoryPermissionResult();
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();


        HashMap<Integer, User> users = new HashMap<Integer, User>();
        tpr.setUsersSorted(Service.userDAO.findAllSortedByName());
        for (User u : tpr.getUsersSorted()) {
            users.put(u.getUserId(), u);
        }
        tpr.setUsers(users);
        HashMap<Integer, Alliance> alliance = new HashMap<Integer, Alliance>();
        tpr.setAlliancesSorted(Service.allianceDAO.findAllSortedByName());
        for (Alliance a : tpr.getAlliancesSorted()) {
            alliance.put(a.getId(), a);
        }
        tpr.setAlliances(alliance);

        HashMap<Integer, ArrayList<Integer>> allianceMembers = new HashMap<Integer, ArrayList<Integer>>();
        for (AllianceMember am : (ArrayList<AllianceMember>) Service.allianceMemberDAO.findAll()) {
            ArrayList<Integer> members = allianceMembers.get(am.getAllianceId());
            if (members == null) {
                members = new ArrayList<Integer>();
            }
            members.add(am.getUserId());
            allianceMembers.put(am.getAllianceId(), members);
        }
        tpr.setAllianceMembers(allianceMembers);
        tpr.setPermissions(Service.territoryMapShareDAO.findByTerritoryIdSorted(territoryId));

        tpr.setResult(result);
        return tpr;
    }

    public static ArrayList<BaseResult> deleteTerritory(int userId, Map<String, String[]> params) {
        ArrayList<BaseResult> result = new ArrayList<BaseResult>();
        TerritoryMap tm = Service.territoryMapDAO.findById(Integer.parseInt(params.get("territoryId")[0]));

        if (tm.getType().equals(ETerritoryMapType.USER) && tm.getRefId() == userId) {
            Service.territoryMapPointDAO.removeAll(Service.territoryMapPointDAO.findByTerritoryId(tm.getId()));
            Service.territoryMapShareDAO.removeAll(Service.territoryMapShareDAO.findByTerritoryId(tm.getId()));
            Service.territoryMapDAO.remove(tm);
        } else {
            result.add(new BaseResult("No Rights", true));
        }
        return result;

    }

    public static TerritorySaveResult savePerms(TerritorySaveResult saveResult, HashMap<Integer, TerritoryMapShare> perms, int territoryId, ETerritoryMapShareType type) {


        for (Map.Entry<Integer, TerritoryMapShare> perm : perms.entrySet()) {

            saveResult.addPermission(type, perm.getValue().getRefId());
            DebugBuffer.addLine(DebugLevel.DEBUG, "SavePerms : " + perm.getValue().getTerritoryId() + " =>" + perm.getValue().getRefId() + " => " + perm.getValue().getType());
            TerritoryMapShare tmsSearch = Service.territoryMapShareDAO.findBy(perm.getValue().getTerritoryId(), perm.getValue().getRefId(), perm.getValue().getType());
            if (tmsSearch == null) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "tmSearch == null addint to db");
                if (type.equals(ETerritoryMapShareType.USER)) {
                    if (Service.territoryMapShareDAO.findBy(perm.getValue().getTerritoryId(), perm.getValue().getRefId(), ETerritoryMapShareType.USER) == null) {
                        DebugBuffer.addLine(DebugLevel.DEBUG, "Adding userNotification");
                        addUserAddedNotification(perm.getValue());
                    }
                }
                Service.territoryMapShareDAO.add(perm.getValue());
                saveResult.addNewPermission(type, perm.getValue().getRefId());
            } else {
                DebugBuffer.addLine(DebugLevel.DEBUG, "tmSearch != null => updating");
                //Just updating
                Service.territoryMapShareDAO.update(tmsSearch);
            }
            //Special treatment of ALLIANE and ALL
            //ALL => Add all users
            if (type.equals(ETerritoryMapShareType.ALL)) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "All found");
                for (UserData ud : (ArrayList<UserData>) Service.userDataDAO.findAll()) {
                    TerritoryMapShare tmsUserSearch = Service.territoryMapShareDAO.findBy(perm.getValue().getTerritoryId(), ud.getUserId(), ETerritoryMapShareType.USER_BY_ALL);
                    if (tmsUserSearch == null) {
                        DebugBuffer.addLine(DebugLevel.DEBUG, "ALL => tmSearch == null addint to db");
                        TerritoryMapShare tms = perm.getValue().clone();
                        tms.setType(ETerritoryMapShareType.USER_BY_ALL);
                        tms.setRefId(ud.getUserId());

                    if (Service.territoryMapShareDAO.findBy(perm.getValue().getTerritoryId(), ud.getUserId(), ETerritoryMapShareType.USER) == null) {
                            DebugBuffer.addLine(DebugLevel.DEBUG, "ALL => Add notification");
                            addUserAddedNotification(perm.getValue());
                        }
                        Service.territoryMapShareDAO.add(tms);
                        saveResult.addNewPermission(type, perm.getValue().getRefId());
                    } else {
                        DebugBuffer.addLine(DebugLevel.DEBUG, "ALL => tmSearch != null updating");
                        Service.territoryMapShareDAO.update(tmsUserSearch);
                    }
                }
            } //ALLIANCE => Add all alliancemembers
            else if (type.equals(ETerritoryMapShareType.ALLIANCE)) {
                for (AllianceMember am : (ArrayList<AllianceMember>) Service.allianceMemberDAO.findByAllianceIdNotTrial(perm.getValue().getRefId())) {
                    TerritoryMapShare tmsUserSearch = Service.territoryMapShareDAO.findBy(perm.getValue().getTerritoryId(), am.getUserId(), ETerritoryMapShareType.USER_BY_ALLIANCE);
                    if (tmsUserSearch == null) {
                        TerritoryMapShare tms = perm.getValue().clone();
                        tms.setRefId(am.getUserId());
                        tms.setType(ETerritoryMapShareType.USER_BY_ALLIANCE);

                    if (Service.territoryMapShareDAO.findBy(perm.getValue().getTerritoryId(), am.getUserId(), ETerritoryMapShareType.USER) == null) {
                            addUserAddedNotification(perm.getValue());
                        }
                        Service.territoryMapShareDAO.add(tms);
                        saveResult.addNewPermission(type, perm.getValue().getRefId());
                    } else {
                        Service.territoryMapShareDAO.update(tmsUserSearch);
                    }
                }
            }

        }
        return saveResult;


    }

    private static void updatePermissions(int territoryId, HashMap<ETerritoryMapShareType, HashMap<Integer, TerritoryMapShare>> permissions) {

        TerritorySaveResult saveResult = new TerritorySaveResult();
        for (Map.Entry<ETerritoryMapShareType, HashMap<Integer, TerritoryMapShare>> entry : permissions.entrySet()) {
            DebugBuffer.addLine(DebugLevel.DEBUG, "Type : " + entry.getKey().toString() + " with " + entry.getValue().size() + " entries");
            savePerms(saveResult, entry.getValue(), territoryId, entry.getKey());
        }

        deletePerms(saveResult, territoryId);
    }

    private static void deletePerms(TerritorySaveResult saveResult, int territoryId) {
        for (ETerritoryMapShareType type : ETerritoryMapShareType.values()) {
            if (type.equals(ETerritoryMapShareType.USER_BY_ALL) || type.equals(ETerritoryMapShareType.USER_BY_ALLIANCE)) {
                continue;
            }
            log.debug("in delete Perms: Type : " + type + " territoryId " + territoryId);
            deletePerms(saveResult, territoryId, type);
        }
    }

    private static void deletePerms(TerritorySaveResult saveResult, int territoryId, ETerritoryMapShareType type) {
        ArrayList<TerritoryMapShare> toDelete = new ArrayList<TerritoryMapShare>();

        //Search for all permissions with this type and territory
        for (TerritoryMapShare tms : Service.territoryMapShareDAO.findByTerritoryId(territoryId, type)) {
            //Load all RefId for this Type
            ArrayList<Integer> add = saveResult.getPermissions().get(type);

            DebugBuffer.addLine(DebugLevel.DEBUG, "Found " + add.size() + "  entries for " + type.toString() + " ");
            DebugBuffer.addLine(DebugLevel.DEBUG, "Search for RefId : " + tms.getRefId());
            if (add == null || !add.contains(tms.getRefId())) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "Delete Permission");
                toDelete.add(tms);
            } else {

                DebugBuffer.addLine(DebugLevel.DEBUG, "Dont delete Permission");
            }
        }
        for (TerritoryMapShare tmsDelete : toDelete) {
            //Special treatment of ALLIANE and ALL
            //ALL => Delete all users permissions
            if (type.equals(ETerritoryMapShareType.ALL)) {
                for (UserData ud : (ArrayList<UserData>) Service.userDataDAO.findAll()) {
                    TerritoryMapShare tmsUserSearch = Service.territoryMapShareDAO.findBy(tmsDelete.getTerritoryId(), ud.getUserId(), ETerritoryMapShareType.USER_BY_ALL);
                    if (tmsUserSearch != null) {
                        Service.territoryMapShareDAO.remove(tmsUserSearch);
                        if (Service.territoryMapShareDAO.findByUserId(ud.getUserId()).isEmpty()) {
                            addUserRemovedNotification(tmsUserSearch);
                        }
                    }
                }
            } //ALLIANCE => Delete all alliancepermissions
            else if (type.equals(ETerritoryMapShareType.ALLIANCE)) {
                for (AllianceMember am : (ArrayList<AllianceMember>) Service.allianceMemberDAO.findByAllianceIdNotTrial(tmsDelete.getRefId())) {
                    TerritoryMapShare tmsUserSearch = Service.territoryMapShareDAO.findBy(tmsDelete.getTerritoryId(), am.getUserId(), ETerritoryMapShareType.USER_BY_ALLIANCE);
                    if (tmsUserSearch != null) {
                        Service.territoryMapShareDAO.remove(tmsUserSearch);
                        if (Service.territoryMapShareDAO.findByUserId(am.getUserId()).isEmpty()) {
                            addUserRemovedNotification(tmsUserSearch);
                        }
                    }
                }
            }
            Service.territoryMapShareDAO.remove(tmsDelete);
            if (tmsDelete.getType().equals(ETerritoryMapShareType.USER)) {
                if (Service.territoryMapShareDAO.findBy(tmsDelete.getRefId(), ETerritoryMapShareType.USER).isEmpty()) {
                    addUserRemovedNotification(tmsDelete);
                }
            }
        }
    }
}
