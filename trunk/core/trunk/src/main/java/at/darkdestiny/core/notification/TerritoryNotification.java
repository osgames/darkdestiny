/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.notification;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EMessageRefType;
import at.darkdestiny.core.model.TerritoryMap;
import at.darkdestiny.core.model.TerritoryMapShare;
import at.darkdestiny.core.service.Service;

/**
 *
 * @author Admin
 */
public class TerritoryNotification extends Notification {

    TerritoryMapShare tms;
    ETerritoryNotificationType type;

    public TerritoryNotification(int userId, TerritoryMapShare tms, ETerritoryNotificationType type) {
        super(userId);
        this.tms = tms;
        this.type = type;
    }

    public void generateMessage() {
        String msg = "";
        String topic = "";
        TerritoryMap tm = Service.territoryMapDAO.findById(tms.getTerritoryId());
        if (tm == null || Service.userDAO.findById(tm.getRefId()) == null) {
            return;
        }
        topic += ML.getMLStr("notification_territory_topic", userId).replace("%TERRITORYNAME%", tm.getName()).replace("%USERNAME%", Service.userDAO.findById(tm.getRefId()).getGameName());

        if (type.equals(ETerritoryNotificationType.DELETED)) {
            msg += ML.getMLStr("notification_territory_msg_delete", userId).replace("%TERRITORYNAME%", tm.getName()).replace("%USERNAME%", Service.userDAO.findById(tm.getRefId()).getGameName());

        } else {
            msg += ML.getMLStr("notification_territory_msg_added", userId).replace("%TERRITORYNAME%", tm.getName()).replace("%USERNAME%", Service.userDAO.findById(tm.getRefId()).getGameName());
        }
        gm.setMessageContentType(EMessageContentType.INFO);
        gm.setTopic(topic);
        gm.setMsg(msg);
        if (type.equals(ETerritoryNotificationType.ADDED)) {
            this.gm.setReference(EMessageRefType.TERRITORY_ID_ADD, tms.getTerritoryId());
        }
        if (type.equals(ETerritoryNotificationType.DELETED)) {
            this.gm.setReference(EMessageRefType.TERRITORY_ID_DELETE, tms.getTerritoryId());
        }

        persistMessage();

    }

    public enum ETerritoryNotificationType {

        ADDED, DELETED
    }
}
