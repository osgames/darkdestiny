/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.test;

import at.darkdestiny.core.enumeration.ECombatStatus;
import at.darkdestiny.core.model.CombatPlayer;
import at.darkdestiny.core.model.CombatRound;
import at.darkdestiny.core.model.GroundCombat;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.core.model.RoundEntry;
import at.darkdestiny.core.model.TerritoryEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class BattleTestResult {

    private ECombatStatus status;
    private StringBuffer report = new StringBuffer();
    private GroundCombat groundCombat = new GroundCombat();
    private ArrayList<CombatPlayer> combatPlayers = new ArrayList<CombatPlayer>();
    private ArrayList<CombatRound> combatRounds = new ArrayList<CombatRound>();
    private ArrayList<TerritoryEntry> territoryEntry = new ArrayList<TerritoryEntry>();
    private ArrayList<RoundEntry> roundEntry = new ArrayList<RoundEntry>();
    private HashMap<Integer, HashMap<Integer, PlayerTroop>> troops = new HashMap<Integer, HashMap<Integer, PlayerTroop>>();
    private HashMap<Integer, ArrayList<Integer>> groups = new HashMap<Integer, ArrayList<Integer>>();
    private ArrayList<PlayerTroop> playerTroops = new ArrayList<PlayerTroop>();

    public CombatRound getLastRound() {
        ArrayList<CombatRound> result = combatRounds;
        TreeMap<Integer, CombatRound> sorted = new TreeMap<Integer, CombatRound>();

        if (result.isEmpty()) {
            return null;
        }
        for (CombatRound crTmp : result) {
            sorted.put(crTmp.getRound(), crTmp);
        }
        return sorted.get(sorted.size());
    }

    public BattleTestResult() {
    }

    /**
     * @return the groundCombat
     */
    public GroundCombat getGroundCombat() {
        return groundCombat;
    }

    /**
     * @param groundCombat the groundCombat to set
     */
    public void setGroundCombat(GroundCombat groundCombat) {
        this.groundCombat = groundCombat;
    }

    /**
     * @return the combatPlayers
     */
    public ArrayList<CombatPlayer> getCombatPlayers() {
        return combatPlayers;
    }

    /**
     * @param combatPlayers the combatPlayers to set
     */
    public void setCombatPlayers(ArrayList<CombatPlayer> combatPlayers) {
        this.combatPlayers = combatPlayers;
    }

    /**
     * @return the combatRounds
     */
    public ArrayList<CombatRound> getCombatRounds() {
        return combatRounds;
    }

    /**
     * @param combatRounds the combatRounds to set
     */
    public void setCombatRounds(ArrayList<CombatRound> combatRounds) {
        this.combatRounds = combatRounds;
    }

    /**
     * @return the territoryEntry
     */
    public ArrayList<TerritoryEntry> getTerritoryEntry() {
        return territoryEntry;
    }

    /**
     * @param territoryEntry the territoryEntry to set
     */
    public void setTerritoryEntry(ArrayList<TerritoryEntry> territoryEntry) {
        this.territoryEntry = territoryEntry;
    }

    /**
     * @return the roundEntry
     */
    public ArrayList<RoundEntry> getRoundEntry() {
        return roundEntry;
    }

    /**
     * @param roundEntry the roundEntry to set
     */
    public void setRoundEntry(ArrayList<RoundEntry> roundEntry) {
        this.roundEntry = roundEntry;
    }

    /**
     * @return the troops
     */
    public HashMap<Integer, HashMap<Integer, PlayerTroop>> getTroops() {
        return troops;
    }

    /**
     * @param troops the troops to set
     */
    public void setTroops(HashMap<Integer, HashMap<Integer, PlayerTroop>> troops) {
        this.troops = troops;
    }

    /**
     * @return the playerTroops
     */
    public ArrayList<PlayerTroop> getPlayerTroops() {
        return playerTroops;
    }

    /**
     * @param playerTroops the playerTroops to set
     */
    public void setPlayerTroops(ArrayList<PlayerTroop> playerTroops) {
        this.playerTroops = playerTroops;
    }

    public void remove(PlayerTroop pt) {
        for (PlayerTroop pts : playerTroops) {
            if (pts.getUserId().equals(pt.getUserId())
                    && pts.getTroopId().equals(pt.getTroopId())) {
                playerTroops.remove(pts);
                break;
            }
        }
    }

    public void update(PlayerTroop pt) {
        for (PlayerTroop pts : playerTroops) {
            if (pts.getPlanetId().equals(pt.getPlanetId())
                    && pts.getUserId().equals(pt.getUserId())
                    && pts.getTroopId().equals(pt.getTroopId())) {
                pts.setNumber(pt.getNumber());
                break;
            }
        }
    }

    /**
     * @return the groups
     */
    public HashMap<Integer, ArrayList<Integer>> getGroups() {
        return groups;
    }

    /**
     * @param groups the groups to set
     */
    public void setGroups(HashMap<Integer, ArrayList<Integer>> groups) {
        this.groups = groups;
    }

    /**
     * @return the status
     */
    public ECombatStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(ECombatStatus status) {
        this.status = status;
    }

    /**
     * @return the report
     */
    public StringBuffer getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(StringBuffer report) {
        this.report = report;
    }
}
