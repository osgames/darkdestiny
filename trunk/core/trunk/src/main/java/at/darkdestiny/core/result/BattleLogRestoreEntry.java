/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

import at.darkdestiny.core.model.BattleLogEntry;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.User;

/**
 *
 * @author Admin
 */
public class BattleLogRestoreEntry {

        private Planet planet;
        private ShipDesign design;
        private BattleLogEntry battleLogEntry;
        private User user;
        private PlayerPlanet playerPlanet;

        public BattleLogRestoreEntry(BattleLogEntry battleLogEntry, User user, PlayerPlanet playerPlanet, Planet planet, ShipDesign design) {
            this.battleLogEntry = battleLogEntry;
            this.user = user;
            this.playerPlanet = playerPlanet;
            this.planet = planet;
            this.design = design;
        }

        /**
         * @return the battleLogEntry
         */
        public BattleLogEntry getBattleLogEntry() {
            return battleLogEntry;
        }

        /**
         * @return the user
         */
        public User getUser() {
            return user;
        }

        /**
         * @return the playerPlanet
         */
        public PlayerPlanet getPlayerPlanet() {
            return playerPlanet;
        }

        /**
         * @return the planet
         */
        public Planet getPlanet() {
            return planet;
        }

        /**
         * @return the design
         */
        public ShipDesign getDesign() {
            return design;
        }
    }