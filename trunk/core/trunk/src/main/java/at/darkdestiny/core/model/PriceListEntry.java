/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation(value = "pricelistentry")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class PriceListEntry extends Model<PriceListEntry> {

    @FieldMappingAnnotation("id")
    @ColumnProperties(autoIncrement = true)
    @IdFieldAnnotation
    private Integer id;
    @FieldMappingAnnotation("priceListId")
    @IndexAnnotation(indexName = "priceRessourceIdx")
    private Integer priceListId;
    @FieldMappingAnnotation("ressId")
    @IndexAnnotation(indexName = "priceRessourceIdx")
    private Integer ressId;
    @FieldMappingAnnotation("price")
    @DefaultValue("0")
    private Integer price;
    @FieldMappingAnnotation("sold")
    @DefaultValue("0")
    private Long sold;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the priceListId
     */
    public Integer getPriceListId() {
        return priceListId;
    }

    /**
     * @param priceListId the priceListId to set
     */
    public void setPriceListId(Integer priceListId) {
        this.priceListId = priceListId;
    }

    /**
     * @return the ressId
     */
    public Integer getRessId() {
        return ressId;
    }

    /**
     * @param ressId the ressId to set
     */
    public void setRessId(Integer ressId) {
        this.ressId = ressId;
    }

    /**
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * @return the sold
     */
    public Long getSold() {
        return sold;
    }

    /**
     * @param sold the sold to set
     */
    public void setSold(Long sold) {
        this.sold = sold;
    }
}
