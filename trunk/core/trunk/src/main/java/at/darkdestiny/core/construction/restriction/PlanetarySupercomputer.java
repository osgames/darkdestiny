/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.construction.restriction;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.construction.ConstructionInterface;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.enumeration.ERestrictionReason;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.result.ConstructionCheckResult;
import at.darkdestiny.core.utilities.LanguageUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class PlanetarySupercomputer implements ConstructionInterface {
    private HashMap<String, ERestrictionReason> errMsg = new HashMap<String, ERestrictionReason>();

    public ConstructionCheckResult onConstructionCheck(PlayerPlanet pp) {
        PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);

        if (pp.getPopulation() < 1500000000) {
            errMsg.put(ML.getMLStr("construction_err_notenoughpopulation", pp.getUserId()).replace("%POP", FormatUtilities.getFormatScaledNumber(1500000000, 1, GameConstants.VALUE_TYPE_NORMAL, LanguageUtilities.getMasterLocaleForUser(pp.getUserId()))), ERestrictionReason.NOTENOUGHPOPULATION);
            return new ConstructionCheckResult(false,0);
        }

        return new ConstructionCheckResult(true,1);
    }

    public void onConstruction(PlayerPlanet pp) {

    }

    public void onFinishing(PlayerPlanet pp) {

    }

    public void onDeconstruction(PlayerPlanet pp) {

    }

    public void onDestruction(PlayerPlanet pp) {

    }
    public HashMap<String, ERestrictionReason> getReasons() {
        return errMsg;
    }


    @Override
    public void onImproving(PlayerPlanet pp) throws Exception {
    }

    @Override
    public void onImproved(PlayerPlanet pp) throws Exception {
    }

    public boolean isDeactivated(PlayerPlanet pp) throws Exception {
        return false;
    }
}
