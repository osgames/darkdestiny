/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.requestbuffer;

 import java.util.HashMap;import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import java.util.HashMap;

/**
 *
 * @author Stefan
 */
public class BufferHandling {

    private static final Logger log = LoggerFactory.getLogger(BufferHandling.class);

    private static HashMap<Integer,ParameterBuffer> buffered = new HashMap<Integer,ParameterBuffer>();

    protected static int registerObject(ParameterBuffer pb) {
        int id = (int)(Math.random() * 99999d);
        while (buffered.keySet().contains(id)) {
            id = (int)(Math.random() * 99999d);
        }

        log.debug("REGISTERING BUFFER OBJECT " + id + " USERID: " + pb.getUserId() + " TYPE: " + pb.getClass().getName());
        buffered.put(id, pb);
        return id;
    }

    public static ParameterBuffer getBuffer(int userId, int id) {
        log.debug("QUERY BUFFER OBJECT " + id + " USERID: " + userId);

        ParameterBuffer pb = buffered.get(id);

        log.debug("PB IS" + pb);

        if (pb == null) return null;
        if (pb.getUserId() != userId) return null;

        return pb;
    }

    protected static void invalidate(ParameterBuffer pb) {
        log.debug("INVALIDATE BUFFER OBJECT " + pb.id);

        buffered.remove(pb.id);
    }
}
