/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class PlayerTroopDAO extends ReadWriteTable<PlayerTroop> implements GenericDAO {

    public ArrayList<PlayerTroop> findByPlanetId(int planetId) {
        PlayerTroop pt = new PlayerTroop();
        pt.setPlanetId(planetId);
        return find(pt);
    }

    public ArrayList<PlayerTroop> findBy(int userId, int planetId) {
        PlayerTroop pt = new PlayerTroop();
        pt.setUserId(userId);
        pt.setPlanetId(planetId);

        return find(pt);
    }

    public PlayerTroop findBy(int userId, int planetId, int groundTroopId) {
        PlayerTroop pt = new PlayerTroop();
        pt.setUserId(userId);
        pt.setPlanetId(planetId);
        pt.setTroopId(groundTroopId);
        ArrayList<PlayerTroop> result = new ArrayList<PlayerTroop>();
        result = find(pt);
        if(result.size() > 0){
            return result.get(0);
        }else{
            return null;
        }
    }

    public ArrayList<PlayerTroop> findByUserId(int userId) {
        PlayerTroop pt = new PlayerTroop();
        pt.setUserId(userId);
        return find(pt);
    }
}
