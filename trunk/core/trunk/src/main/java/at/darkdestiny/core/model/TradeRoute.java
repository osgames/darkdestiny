/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.ETradeRouteStatus;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "traderoute")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TradeRoute extends Model<TradeRoute> implements Comparable {

    @FieldMappingAnnotation(value = "id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation(value = "userId")
    @IndexAnnotation(indexName = "userId")
    private Integer userId;
    @FieldMappingAnnotation(value = "startPlanet")
    @IndexAnnotation(indexName = "endpoints")
    private Integer startPlanet;
    @FieldMappingAnnotation(value = "targetPlanet")
    @IndexAnnotation(indexName = "endpoints")
    private Integer targetPlanet;
    @FieldMappingAnnotation(value = "distance")
    private Double distance;
    @FieldMappingAnnotation(value = "tradePostId")
    @DefaultValue("0")
    private Integer tradePostId;
    @FieldMappingAnnotation(value = "targetUserId")
    @DefaultValue("0")
    private Integer targetUserId;
    @FieldMappingAnnotation(value = "efficiency")
    @DefaultValue("0")
    private Double efficiency;
    @FieldMappingAnnotation(value = "capacity")
    @DefaultValue("0")
    private Integer capacity;
    @FieldMappingAnnotation(value = "status")
    private ETradeRouteStatus status;
    @FieldMappingAnnotation(value = "type")
    private ETradeRouteType type;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the startPlanet
     */
    public Integer getStartPlanet() {
        return startPlanet;
    }

    /**
     * @param startPlanet the startPlanet to set
     */
    public void setStartPlanet(Integer startPlanet) {
        this.startPlanet = startPlanet;
    }

    /**
     * @return the targetPlanet
     */
    public Integer getTargetPlanet() {
        return targetPlanet;
    }

    /**
     * @param targetPlanet the targetPlanet to set
     */
    public void setTargetPlanet(Integer targetPlanet) {
        this.targetPlanet = targetPlanet;
    }

    /**
     * @return the distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     * @return the targetUserId
     */
    public Integer getTargetUserId() {
        return targetUserId;
    }

    /**
     * @param targetUserId the targetUserId to set
     */
    public void setTargetUserId(Integer targetUserId) {
        this.targetUserId = targetUserId;
    }

    /**
     * @return the efficiency
     */
    public Double getEfficiency() {
        return efficiency;
    }

    /**
     * @param efficiency the efficiency to set
     */
    public void setEfficiency(Double efficiency) {
        this.efficiency = efficiency;
    }

    /**
     * @return the capacity
     */
    public Integer getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public ETradeRouteStatus getStatus() {
        return status;
    }

    public void setStatus(ETradeRouteStatus status) {
        this.status = status;
    }

    @Override
    public int compareTo(Object o) {
        TradeRoute tr = (TradeRoute) o;
        return this.getId().compareTo(tr.getId());
    }

    /**
     * @return the type
     */
    public ETradeRouteType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ETradeRouteType type) {
        this.type = type;
    }

    /**
     * @return the tradePostId
     */
    public Integer getTradePostId() {
        return tradePostId;
    }

    /**
     * @param tradePostId the tradePostId to set
     */
    public void setTradePostId(Integer tradePostId) {
        this.tradePostId = tradePostId;
    }
}
