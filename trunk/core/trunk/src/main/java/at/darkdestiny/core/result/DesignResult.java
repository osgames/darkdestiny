/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.TreeMap;

/**
 *
 * @author Dreloc
 */
public class DesignResult {


    private ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    private int designId;
    private String designName;
    private int chassisId;
    private String chassisName;

    private float designDamage;
    private float damage;
    private float damageAbsorbedShield;
    private float damageAbsorbedArmor;

    private int count;
    private int lost;
    private TreeMap<EDamageLevel, Integer> damaged;

    public DesignResult(int designId){
        this.designId = designId;
       ShipDesign sd = sdDAO.findById(designId);
       damaged = new TreeMap<EDamageLevel, Integer>();
       this.designName = sd.getName();
       this.chassisId = sd.getChassis();

    }

    /**
     * @return the designId
     */
    public int getDesignId() {
        return designId;
    }

    /**
     * @return the designName
     */
    public String getDesignName() {
        return designName;
    }

    /**
     * @return the chassisId
     */
    public int getChassisId() {
        return chassisId;
    }

    /**
     * @return the chassisName
     */
    public String getChassisName() {
        return chassisName;
    }

    /**
     * @return the damage
     */
    public float getDamage() {
        return damage;
    }

    /**
     * @param damage the damage to set
     */
    public void setDamage(float damage) {
        this.damage = damage;
    }

    /**
     * @return the damageAbsorbedShield
     */
    public float getDamageAbsorbedShield() {
        return damageAbsorbedShield;
    }

    /**
     * @param damageAbsorbedShield the damageAbsorbedShield to set
     */
    public void setDamageAbsorbedShield(float damageAbsorbedShield) {
        this.damageAbsorbedShield = damageAbsorbedShield;
    }

    /**
     * @return the damageAbsorbedArmor
     */
    public float getDamageAbsorbedArmor() {
        return damageAbsorbedArmor;
    }

    /**
     * @param damageAbsorbedArmor the damageAbsorbedArmor to set
     */
    public void setDamageAbsorbedArmor(float damageAbsorbedArmor) {
        this.damageAbsorbedArmor = damageAbsorbedArmor;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the lost
     */
    public int getLost() {
        return lost;
    }

    /**
     * @param lost the lost to set
     */
    public void setLost(int lost) {
        this.lost = lost;
    }

    /**
     * @return the damaged
     */
    public TreeMap<EDamageLevel, Integer> getDamaged() {
        return damaged;
    }

    /**
     * @param damaged the damaged to set
     */
    public void setDamaged(TreeMap<EDamageLevel, Integer> damaged) {
        this.damaged = damaged;
    }

    /**
     * @return the designDamage
     */
    public float getDesignDamage() {
        return designDamage;
    }

    /**
     * @param designDamage the designDamage to set
     */
    public void setDesignDamage(float designDamage) {
        this.designDamage = designDamage;
    }

}
