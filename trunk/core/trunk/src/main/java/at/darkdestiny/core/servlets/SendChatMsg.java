/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.admin.SessionTracker;
import at.darkdestiny.core.chat.JSChatUser;
import at.darkdestiny.core.chat.JSChatUtilities;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.dao.ChatMessageDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.model.ChatMessage;
import at.darkdestiny.core.model.User;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stefan
 */


public class SendChatMsg extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(SendChatMsg.class);

    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private ChatMessageDAO cmDAO = (ChatMessageDAO) DAOFactory.get(ChatMessageDAO.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate"); //HTTP 1.1
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server

        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        try {
            int userId = 0;
            User u = null;

            try {
                userId = Integer.parseInt((String) session.getAttribute("userId"));
                u = uDAO.findById(userId);
            } catch (NumberFormatException nfe) {
                return;
            }

            String message = request.getParameter("msg");            
            
            if (message == null) {
                StringBuilder json = getContent();              
                createOutput(out,json);
            } else if (message.length() < 1) {
                StringBuilder json = getContent();
                createOutput(out,json);
            } else {
                message = message.replace("<", "");
                message = message.replace(">", "");

                System.out.println("Received Message: " + message);

                ChatMessage cm = new ChatMessage();
                cm.setText(message);
                cm.setUserName(u.getGameName());
                cm.setDate(System.currentTimeMillis());
                cmDAO.add(cm);

                TreeMap<Long, ChatMessage> messages = Maps.newTreeMap();
                for (ChatMessage cmTmp : cmDAO.findAll()) {
                    messages.put(cmTmp.getDate(), cmTmp);
                }

                int msgCnt = 30;

                StringBuilder json = getContent();
                // System.out.println(json.toString());
                createOutput(out,json);

                JSChatUtilities.setUnreadTimeStamp(userId);    
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in SendChatMsg: ", e);
        } finally {
            out.close();
        }
    }

    private void createOutput(PrintWriter out, StringBuilder sb) {
        // Write save output to stream 
        int contentTagPos = sb.indexOf("\"line\":");
        
        char backslash = 92;
        char doublequote = 34;
        char singlequote = 39;        
        
        while (contentTagPos != -1) {
            // Copy all content to output up to contentTagPos
            for (int i=0;i<contentTagPos + 8;i++) {
                out.write(sb.charAt(i));
            }
            
            sb.replace(0, contentTagPos + 8, "");
            
            int closingBracketPos = sb.indexOf("}");
            
            String textContentTmp = sb.substring(0, closingBracketPos - 2);
            char[] textContent = textContentTmp.toCharArray();
         
            for (int i=0;i<textContent.length;i++) {
                char currChar = sb.charAt(i);
                if (currChar == 34) {
                    out.write(backslash);
                    out.write(doublequote);
                } else if (currChar == 92) {
                    out.write(backslash);
                    out.write(backslash);
                } else if (currChar == 39) {
                    out.write(backslash);
                    out.write(singlequote);
                } else {
                    out.write(currChar);
                }
            }                    
            
            sb.replace(0, closingBracketPos - 2, "");
            
            contentTagPos = sb.indexOf("\"line\":");
        }
        
        for (int i=0;i<sb.length();i++) {
            out.write(sb.charAt(i));
        }        
    }
    
    private StringBuilder getUserInformation() {
        StringBuilder userInfo = new StringBuilder();
        userInfo.append(" \"users\": [ ");
        
        ArrayList<JSChatUser> chatUser = SessionTracker.getChatUsers();
        boolean firstUser = true;
        
        for (JSChatUser jscu : chatUser) {
            if (!firstUser) {
                userInfo.append(", ");
            }
            userInfo.append("{ ");
            userInfo.append("\"name\":");
            userInfo.append("\"");
            userInfo.append(jscu.getName());
            userInfo.append("\", ");
            userInfo.append("\"inactive\":");
            userInfo.append("\"");
            userInfo.append(jscu.isInactive());
            userInfo.append("\" } ");            
            firstUser = false;
        }
        
        userInfo.append(" ], ");
        return userInfo;
    }
    
    private StringBuilder getContent() {
        TreeMap<Long, ChatMessage> messages = Maps.newTreeMap();
        for (ChatMessage cmTmp : cmDAO.findAll()) {
            messages.put(cmTmp.getDate(), cmTmp);
        }

        int msgCnt = 30;

        StringBuilder json = new StringBuilder();
        json.append("{ ");
        
        // TODO: Append chat user information (move to other function in future)
        json.append(getUserInformation());
        
        json.append(" \"messages\": [ ");

        boolean firstMsg = true;

        LinkedList<ChatMessage> sorted = Lists.newLinkedList();
        
        for (ChatMessage cmTmp : messages.descendingMap().values()) {
            msgCnt--;
            if (msgCnt == 0) {
                break;
            }
            
            sorted.addFirst(cmTmp);
        }
        
        for (ChatMessage cmTmp : sorted) {
            if (firstMsg) {
                firstMsg = false;
            } else {
                json.append(", ");
            }
            
            Date date = new Date(cmTmp.getDate());
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            String time = "[" + df.format(date) + "]";
            String line = "<B>" + cmTmp.getUserName() + ":</B> " + cmTmp.getText();

            json.append("{ \"timestamp\":\"");
            json.append(time);
            json.append("\", \"line\":\"");                        
            json.append(line);                        
            json.append("\" }");
        }

        json.append(" ] ");
        json.append("}");
        
        return json;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
