/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.development.conditions;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.EEconomyType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ResearchColony implements IBonusCondition {
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO)DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetConstructionDAO pcDAO = (PlanetConstructionDAO)DAOFactory.get(PlanetConstructionDAO.class);    
    
    @Override
    public boolean isPossible(int planetId) {
        // Requires planetary government
        // Requires 50% of population working in Research
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
        boolean hasGovernment = pcDAO.isConstructed(planetId, Construction.ID_PLANETGOVERNMENT);
        
        if (!hasGovernment) return false;
        
        try {                        
            PlanetCalculation pc = new PlanetCalculation(planetId);
            ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
            
            if (epcr.getPlanetEconomyWorkerPerc().get(EEconomyType.RESEARCH) == null) return false;
            if (epcr.getPlanetEconomyWorkerPerc().get(EEconomyType.RESEARCH) < 50d) {
                return false;
            }            
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in checking bonus condition: ", e);
            return false;
        }
        
        return true;
    }
    
    @Override
    public boolean isPossible() {
        return false;
    }    
    
    @Override 
    public ArrayList<String> activeBoni(int bonusId, int planetId, int userId) {
        ArrayList<String> boniList = new ArrayList<String>();
        
        boniList.add("research placeholder");
        
        return boniList;
    }    
    
    @Override 
    public void onActivation(int bonusId, int planetId, int userId) {
        
    }    
}
