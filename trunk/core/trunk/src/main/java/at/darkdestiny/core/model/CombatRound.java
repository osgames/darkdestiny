/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Aion
 */
@TableNameAnnotation(value = "combatround")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class CombatRound extends Model<CombatRound> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("combatId")
    private Integer combatId;
    @FieldMappingAnnotation("round")
    private Integer round;
    @FieldMappingAnnotation("report")
    private String report;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the combatId
     */
    public Integer getCombatId() {
        return combatId;
    }

    /**
     * @param combatId the combatId to set
     */
    public void setCombatId(Integer combatId) {
        this.combatId = combatId;
    }

    /**
     * @return the round
     */
    public Integer getRound() {
        return round;
    }

    /**
     * @param round the round to set
     */
    public void setRound(Integer round) {
        this.round = round;
    }

    /**
     * @return the report
     */
    public String getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(String report) {
        this.report = report;
    }
}
