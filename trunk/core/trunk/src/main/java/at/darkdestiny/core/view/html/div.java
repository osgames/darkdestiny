 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.view.html;

import java.util.ArrayList;

/**
 *
 * @author Horst
 */
public class div extends htmlObject {

    private ArrayList<div> divs;
    private a hyperlink;
    public div() {
        hyperlink = new a();
        data = "";
        properties = "";
        divs = new ArrayList<div>();
    }

    public String toString() {
        String returnString = "";
        returnString += "\t\t\t\t\t";
        if(!hyperlink.getUrl().equals("")){

            returnString += "<a " + hyperlink.getPropertes() + " href='"+getHyperlink().getUrl()+"'>";
        }
        returnString += "\t\t\t\t\t<div ";
        if (!properties.equals("")) {
            returnString += properties + "";
        }
        returnString += ">";

        for(int i = 0; i < divs.size(); i++){
            returnString += "\t\t\t\t\t\n"+divs.get(i).toString();
        }
        
        if (!data.equals("")) {
            returnString += data;
        }

        returnString += "</div>";
 
        if(!hyperlink.getUrl().equals("")){
            returnString += "</a>";
        }
        returnString += "\n";
  
        return returnString;

    }

    public ArrayList<div> getDivs() {
        return divs;
    }

    public void setDivs(ArrayList<div> divs) {
        this.divs = divs;
    }
    public void addDiv(div div1){
        this.divs.add(div1);
    }

    public a getHyperlink() {
        return hyperlink;
    }

    public void setHyperlink(a hyperlink) {
        this.hyperlink = hyperlink;
    }
}
