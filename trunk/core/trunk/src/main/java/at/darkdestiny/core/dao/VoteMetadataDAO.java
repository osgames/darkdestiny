/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.VoteMetadata;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
/**
 *
 * @author Bullet
 */
public class VoteMetadataDAO extends ReadWriteTable<VoteMetadata> implements GenericDAO {
       public ArrayList<VoteMetadata> findByVoteId(int voteId) {
           VoteMetadata vm = new VoteMetadata();
           vm.setVoteId(voteId);
           return find(vm);
    }
}
