package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.service.Service;

public class CombatHandler extends Service{

    private final static boolean DEBUG = true;
    public final static int ORBITAL_COMBAT = 2;
    public final static int SYSTEM_COMBAT = 3;

    /*
    private SpaceCombat sc;
    private List<CombatGroup> combatGroups;
    private HashMap<String, HashMap<CombatGroupFleet.DesignDescriptor, Integer>> orgCombatGroups;
    private int combatTick;
    private final int systemId;
    private final int initFleetId;
    private final int initUserId;
    private ArrayList<Combat> combatList;
    private HashMap<String, String> combatReports = null;
    private boolean simulation;    // Tempor&auml;r um Ordnungsgem&auml;&szlig;es funktionieren des Weltraumkampfes mit Hangars zu erm&ouml;glichen
    private HangarUtilities hu;
    private boolean surpressViewTable = false;
    // Damit ein Weltraumkampf in Spacecombat berechnet werden kann, sollte SpaceCombat mit einer Liste
    // gef&uuml;llt werden, die die einzelnen Kampfgruppen enth&ouml;lt. Eine solche Kampfgruppe enth&ouml;lt die id's und userId's 
    // jeder beteiligten Flotte. Die Spacecombat Klasse wird dann selbst aus dieser Liste, weitere Listen mit 
    // Anzahl und Typ der Schiffe erstellen aus diesen Listen werden dann Anteilsm&auml;&szlig;ig (max. 1000) Schiffe herausgeladen und
    // f&uuml;r den Kampf benutzt. Die Listen d&uuml;rfen einzeln nacheinander feuern und werden sich ebenfalls anteilsm&auml;&szlig;ig f&uuml;r eine der 
    // anderen vorhandenen Listen entscheiden.     
    public CombatHandler(int userId, int fleetId, int destSystem) {
        this.initUserId = userId;
        this.initFleetId = fleetId;
        this.systemId = destSystem;

        combatGroups = new ArrayList<CombatGroup>();
    }

    public void startCombatHandler() {
        // Aktuellen Tick finden
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT timefinished FROM actions WHERE actionType=6 AND number=" + initFleetId);
            if (rs.next()) {
                combatTick = rs.getInt(1);
                if (DEBUG) {
                    // log.debug("Delete actions entry for fleetId " + initFleetId);
                    // stmt.execute("DELETE FROM actions WHERE number="+initFleetId+" AND actionType=6");
                }
            } else {
                // Hamma sowieso a gr&ouml;&szlig;eres Problem
            }
            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Exception in CombatHandler - Constructor" + e);
            }
        }

        if (combatReports == null) {
            initCombatReports();
        }

        boolean combatFinished = false;
        int combatLevel = SYSTEM_COMBAT; // Start with System Combat   
        if (DEBUG) {
            // log.debug("Start combat");
        }

        while (!combatFinished && (combatLevel > 1)) {
            if (DEBUG) {
                // log.debug("WHILE LOOP: Processing combatLevel: " + combatLevel);
            }
            // If combat Level 2 enable special Handling for Orbital Wars                        
            if (combatLevel == ORBITAL_COMBAT) {
                // log.debug("Init PFB for system " + systemId);
                PlanetFleetsBuffer pfb = new PlanetFleetsBuffer(systemId);

                if (DEBUG) {
                    // log.debug("Orbital combat");
                    // Loop through all Planets and check destinations of available Fleets
                }
                ArrayList<Integer> pa = getPlanetArray();
                for (int i = 0; i < pa.size(); i++) {
                    int pId = pa.get(i).intValue();

                    // log.debug("Check planet " + pId);
                    

                    loadAllFleetsOnPlanet(pfb,pId);
                    loadPlanetDefense(pId);
                    if (combatGroups.size() <= 1) {
                        // Delete fleetdetail and order entries for all fleets in combatGroup
                        if (DEBUG) {
                            // log.debug("Clean orbital fleetdetail entries");
                        }
                        clearExtFleetInformation(combatLevel, pId);

                        // No enemies go next Planet                                                
                        combatGroups.clear();
                        if (orgCombatGroups != null) {
                            orgCombatGroups.clear();
                        }
                        continue;
                    }

                    startCombat(GameConstants.ORBITAL_COMBAT, pId);
                    if (DEBUG) {
                        // log.debug("Combat Ended");
                    }
                }
            } else {
                if (DEBUG) {
                    // log.debug("System combat");
                }
                loadAllFleetsInSystem();
                loadSystemDefense(systemId);
                if (combatGroups.size() <= 1) {
                    // Delete fleetdetail and order entries for all fleets with target in System
                    if (DEBUG) {
                        // log.debug("Clean system fleet entries combatLevel " + combatLevel);
                    }
                    clearExtFleetInformation(combatLevel, 0);

                    // No enemies next combat Level
                    combatGroups.clear();
                    if (orgCombatGroups != null) {
                        orgCombatGroups.clear();
                    }
                    if (DEBUG) {
                        // log.debug("Continue to orbital fights");
                    }
                    combatLevel--;
                    continue;
                }
                startCombat(GameConstants.SYSTEM_COMBAT, 0);
                if (DEBUG) {
                    // log.debug("Combat Ended");
                }
            }

            combatLevel--;
            if (DEBUG) {
                // log.debug("Next Combat Level");
                // Break on CombatLevel 1 as GroundCombat is handled elsewhere
            }
            if (combatLevel == 1) {
                combatFinished = true;
            }
        }

        if (DEBUG) {
            // log.debug("Combat Processing finished");
        }
        sendCombatReports();
    }

    private void startCombat(int combatLevel, int pId) {
        // Copy Combat Group
        backupCombatGroup();

        // Kampf starten
        sc = new SpaceCombat(combatLevel, pId);

        // sc.setSimulationParameters(speedFactor,probabilityPA,minDamagePA,maxDamagePA,simultanAttack);

        sc.setCombatGroups(combatGroups);
        sc.setCombatLevel(combatLevel);

        sc.startFightCalc();

        boolean surpressEnemyInfo = false;
        DebugBuffer.addLine(DebugLevel.UNKNOWN,"Calculated combat rounds: " + sc.getProcessedCombatRounds());
        if (sc.getProcessedCombatRounds() < 5) {
            if (sc.getProcessedCombatRounds() < 4) {
                surpressViewTable = true;
            }
            surpressEnemyInfo = true;
        }
        // log.debug("Processed combat rounds -> " + sc.getProcessedCombatRounds());

        if (!surpressViewTable) updateViewTables();
        createCombatReports(combatLevel, pId, surpressEnemyInfo);
        updateDatabase(combatLevel, pId);

        clearExtFleetInformation(combatLevel, pId);
        processRetreatInformation(combatLevel);

        combatGroups.clear();
        orgCombatGroups.clear();
    }

    private void updateViewTables() {
        ArrayList<Integer> users = new ArrayList<Integer>();
        
        for (CombatGroup cg : combatGroups) {
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                users.add(cgf.getUserId());                
            }
        }

        ArrayList<Integer> planets = new ArrayList<Integer>();
        
        try {
            Statement stmt = DbConnect.createStatement();
            Statement stmt2 = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT id FROM planet WHERE systemId=" + systemId);        
            while (rs.next()) {
                planets.add(rs.getInt(1));
            }

            stmt.close();

            for (Integer user : users) {
                for (Integer pId : planets) {
                    int lastUserId = 0;

                    if (playerPlanetDAO.findById(pId) != null) {
                        lastUserId = pId;
                    }
                    // DebugBuffer.addLine("Update view table");
                    //stmt2.execute("INSERT IGNORE INTO viewtable" + user + " (systemid, planetid, lastuserid, lastupdate) " +
                    //        "VALUES (" + systemId + "," + pId + ","+lastUserId+","+combatTick+")");
                    stmt2.execute("INSERT IGNORE INTO viewtable" + user + " (systemid, planetid) " +
                            "VALUES (" + systemId + "," + pId + ")");                    
                }            
            }

            stmt2.close();
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in ViewTableUpdate: ", e);
        }
    }
    
    private void clearExtFleetInformation(int combatLevel, int planetId) {
        try {
            if (DEBUG) {
                // log.debug("Trying to delete all entries for planetId " + planetId + " in combatLevel " + combatLevel);
            }
            Statement stmt = DbConnect.createStatement();

            if (DEBUG) {
                // log.debug("Combat Group size is " + combatGroups.size());
            }
            for (int i = 0; i < combatGroups.size(); i++) {
                if (DEBUG) {
                    // log.debug("Combat Groups found (currently processing " + i + ")");
                }
                CombatGroup cg = combatGroups.get(i);
                if (DEBUG) {
                    // log.debug("FleetList size is " + cg.getFleetList().size());
                }
                if (DEBUG) {
                    // log.debug("ArrayList size is " + ((ArrayList<CombatGroupFleet>) cg.getFleetList()).size());
                }
                for (int j = 0; j < ((ArrayList<CombatGroupFleet>) cg.getFleetList()).size(); j++) {
                    CombatGroupFleet cgf = ((ArrayList<CombatGroupFleet>) cg.getFleetList()).get(j);
                    if (DEBUG) {
                        // log.debug("Fleet " + cgf.getFleetId() + " with j=" + j + " found");
                    }
                    if (cgf.getFleetId() == 0) {
                        continue;
                    }
                    if (DEBUG) {
                        // log.debug("Initialising hangars");
                    }
                    hu = new HangarUtilities();
                    hu.reconstructUnloadedFleetInDB(cgf.getFleetId());
                    if (hu.initializeFleet(cgf.getFleetId())) {
                        hu.getNestedHangarStructure();
                        hu.reconstructFleetInDB(cgf.getFleetId());
                    }

                    if (combatLevel == ORBITAL_COMBAT) {
                        if (DEBUG) {
                            // log.debug("Check Deleting information for fleet (" + systemId + ":" + planetId + ")" + cgf.getFleetId());
                        }
                        ResultSet rs = stmt.executeQuery("SELECT fleetId FROM fleetdetails WHERE fleetId=" + cgf.getFleetId() + " AND destSystem=" + systemId + " AND destPlanet=" + planetId);
                        if (rs.next()) {
                            Statement stmt2 = DbConnect.createStatement();
                            // Fix location of Fleet before deleting fleetdetails
                            if (DEBUG) {
                                // log.debug("Set position of fleet " + cgf.getFleetId() + " to (" + systemId + ":" + planetId + ") with: UPDATE playerfleet SET systemId=" + systemId + ", planetId=" + planetId + ", status=0 WHERE id=" + cgf.getFleetId());
                            }
                            stmt2.executeUpdate("UPDATE playerfleet SET systemId=" + systemId + ", planetId=" + planetId + ", status=0 WHERE id=" + cgf.getFleetId());

                            if (DEBUG) {
                                // log.debug("EXECUTE fleetdetails clean up for " + cgf.getFleetId() + ": " + "DELETE FROM fleetdetails WHERE fleetId=" + cgf.getFleetId() + " AND destplanet=" + planetId);                            
                            }
                            stmt2.execute("DELETE FROM fleetdetails WHERE fleetId=" + cgf.getFleetId() + " AND destplanet=" + planetId);
                            // stmt2.execute("DELETE FROM fleetorders WHERE fleetId="+cgf.getFleetId()+" AND planetId="+planetId);
                            stmt2.close();
                        }
                    } else if (combatLevel == SYSTEM_COMBAT) {
                        if (DEBUG) {
                            // log.debug("Check Deleting information for fleet (" + systemId + ":\"x\") " + cgf.getFleetId());
                        }
                        ResultSet rs = null;

                        if (cgf.isRetreated()) {
                            rs = stmt.executeQuery("SELECT fleetId FROM fleetdetails WHERE fleetId=" + cgf.getFleetId() + " AND destSystem=" + systemId);
                        } else {
                            rs = stmt.executeQuery("SELECT fleetId FROM fleetdetails WHERE fleetId=" + cgf.getFleetId() + " AND destSystem=" + systemId + " AND destPlanet=0");
                        }
                        if (rs.next()) {
                            // Fix location of Fleet before deleting fleetdetails
                            if (DEBUG) {
                                // log.debug("Set position of fleet " + cgf.getFleetId() + " to (" + systemId + ":\"x\") with UPDATE playerfleet SET systemId=" + systemId + ", planetId=0, status=0 WHERE id=" + cgf.getFleetId());
                            }
                            stmt.executeUpdate("UPDATE playerfleet SET systemId=" + systemId + ", planetId=0, status=0 WHERE id=" + cgf.getFleetId());

                            if (DEBUG) {
                                // log.debug("EXECUTE fleetdetails clean up for " + cgf.getFleetId() + ": " + "DELETE FROM fleetdetails WHERE fleetId=" + cgf.getFleetId());
                            }
                            stmt.execute("DELETE FROM fleetdetails WHERE fleetId=" + cgf.getFleetId());
                        // stmt.execute("DELETE FROM fleetorders WHERE fleetId="+cgf.getFleetId());                        
                        }
                    }
                }
            }

            stmt.close();
        } catch (SQLException e) {
            if (DEBUG) {
                // log.debug("Exception in CombatHandler - clearExtFleetInformation: " + e);
            }
        }
    }

    private void processRetreatInformation(int combatLevel) {
        try {
            for (CombatGroup cg : combatGroups) {
                for (CombatGroupFleet cgf : cg.getFleetList()) {
                    // log.debug("Check fleet " + cgf.getFleetName() + " for combatLevel " + combatLevel);
                    if (cgf.getActShipCount() > 0) {
                        if (cgf.isResigned() && (combatLevel == ORBITAL_COMBAT)) {
                            // Set fleet to system
                            Statement stmt = DbConnect.createStatement();
                            stmt.execute("DELETE FROM fleetdetails WHERE fleetId=" + cgf.getFleetId());
                            stmt.executeUpdate("UPDATE playerfleet SET planetId=0, systemId=" + systemId + ", status=0 WHERE id=" + cgf.getFleetId());
                        // log.debug("Set fleet to system!");
                        } else if (cgf.isRetreated()) {
                            if (combatLevel == SYSTEM_COMBAT) {
                                // Remove fleetdetails and send fleet to retreat location
                                // Prevent handling of orbital combat 
                                Statement stmt = DbConnect.createStatement();
                                stmt.execute("DELETE FROM fleetdetails WHERE fleetId=" + cgf.getFleetId());
                                stmt.close();

                                // Send fleet to retreat location
                                Map<String, Object> movePar = new HashMap<String, Object>();
                                movePar.put("fleetid", new String[]{cgf.getFleetIdStr()});
                                movePar.put("target", new String[]{"" + cgf.getRetreatToType()});
                                movePar.put("ziel", new String[]{"" + cgf.getRetreatTo()});
                                movePar.put("retreat", new String[]{"" + cgf.getRetreatFactor()});
                                movePar.put("retreatToType", new String[]{"" + cgf.getRetreatToType()});
                                movePar.put("retreatTo", new String[]{"" + cgf.getRetreatTo()});

                                /*
                                FleetMovement fm = new FleetMovement(movePar, cgf.getUserId(), 0);
                                fm.checkParameter();
                                if (fm.isError()) {
                                    throw new Exception(fm.getErrorMess());
                                }
                                fm.generateFleetMovement();
                                // OLD ENDCOMMENT
                            // log.debug("[System Combat] Fleet retreats due to high losses!");
                            } else if (combatLevel == ORBITAL_COMBAT) {
                                // Send fleet to retreat location
                                Map<String, Object> movePar = new HashMap<String, Object>();
                                movePar.put("fleetid", new String[]{cgf.getFleetIdStr()});
                                movePar.put("target", new String[]{"" + cgf.getRetreatToType()});
                                movePar.put("ziel", new String[]{"" + cgf.getRetreatTo()});
                                movePar.put("retreat", new String[]{"" + cgf.getRetreatFactor()});
                                movePar.put("retreatToType", new String[]{"" + cgf.getRetreatToType()});
                                movePar.put("retreatTo", new String[]{"" + cgf.getRetreatTo()});

                                /*
                                FleetMovement fm = new FleetMovement(movePar, cgf.getUserId(), 0);
                                fm.checkParameter();
                                if (fm.isError()) {
                                    throw new Exception(fm.getErrorMess());
                                }
                                fm.generateFleetMovement();
                                // OLD ENDCOMMENT
                            // log.debug("[Orbital Combat] Fleet retreats due to high losses!");
                            }
                        }
                    } else {
                        // log.debug("Fleet has no ships!");
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.error("Error while rerouting fleet after retreat: ", e);
        }
    }

    private void updateDatabase(int combatLevel, int pId) {
        // Compare modified and original CombatGroups and write according updates
        // to database (loops through original, if values can't be found in changed then
        // delete that information        
        try {
            Statement stmt = DbConnect.createStatement();

            for (int i = 0; i < combatGroups.size(); i++) {
                CombatGroup cg = combatGroups.get(i);
                for (int j = 0; j < ((ArrayList<CombatGroupFleet>) cg.getFleetList()).size(); j++) {
                    CombatGroupFleet cgf = ((ArrayList<CombatGroupFleet>) cg.getFleetList()).get(j);
                    // Ist fehlerhaft da die Daten aus der ShipDesign HashTable &ouml;bernommen werden m&ouml;ssen! (siehe unten)
                    // Am besten nach Abarbeitung der HashMap &ouml;berpr&ouml;fen ob Flotte leer ist und dann l&ouml;schen

                    // Check Design for surviving ships
                    Map<CombatGroupFleet.DesignDescriptor, Integer> designs = cgf.getShipDesign();
                    int tmpCount = 0;
                    for (Map.Entry<CombatGroupFleet.DesignDescriptor, Integer> me : designs.entrySet()) {
                        CombatGroupFleet.DesignDescriptor key = me.getKey();
                        int value = me.getValue();

                        boolean isPlanetary = false;
                        boolean isOrbital = false;

                        if (key.getType() == CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE) {
                            isPlanetary = true;
                        } else if (key.getType() == CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE) {
                            isOrbital = true;
                        }

                        if (value == 0) {
                            if (DEBUG) {
                                // log.debug("DELETE PlanetID=" + pId + ", systemID=" + systemId + ", userId=" + cgf.getUserId() + " unitId=" + key.getId());
                            }
                            if (isOrbital) {
                                if (DEBUG) {
                                    // log.debug("IS ORBITAL");
                                }
                                stmt.execute("DELETE FROM planetdefense WHERE type=2 AND planetId=" + pId + " AND systemId=" + systemId + " AND userId=" + cgf.getUserId() + " AND unitId=" + key.getId());
                            } else if (isPlanetary) {
                                if (DEBUG) {
                                    // log.debug("IS PLANETARY -- cgf userid is " + cgf.getUserId() + "/" + cgf.getUserIdStr());
                                }
                                stmt.execute("DELETE FROM planetdefense WHERE type=1 AND planetId=" + pId + " AND systemId=" + systemId + " AND userId=" + cgf.getUserId() + " AND unitId=" + key.getId());
                            } else {
                                if (DEBUG) {
                                    // log.debug("IS SHIP");
                                }
                                stmt.execute("DELETE FROM shipfleet WHERE fleetId=" + cgf.getFleetId() + " AND designId=" + key.getId());
                            }
                        } else {
                            if (DEBUG) {
                                // log.debug("UPDATE Value=" + value + " PlanetID=" + pId + ", systemID=" + systemId + ", userId=" + cgf.getUserId() + " unitId=" + key.getId());
                            }
                            if (isOrbital) {
                                if (DEBUG) {
                                    // log.debug("IS ORBITAL");
                                }
                                stmt.execute("UPDATE planetdefense SET count=" + value + " WHERE type=2 AND planetId=" + pId + " AND systemId=" + systemId + " AND userId=" + cgf.getUserId() + " AND unitId=" + key.getId());
                            } else if (isPlanetary) {
                                if (DEBUG) {
                                    // log.debug("UPDATE IS PLANETARY -- cgf userid is " + cgf.getUserId() + "/" + cgf.getUserIdStr());
                                }
                                stmt.execute("UPDATE planetdefense SET count=" + value + " WHERE type=1 AND planetId=" + pId + " AND systemId=" + systemId + " AND userId=" + cgf.getUserId() + " AND unitId=" + key.getId());
                            } else {
                                if (DEBUG) {
                                    // log.debug("IS SHIP");
                                }
                                tmpCount += value;
                                stmt.executeUpdate("UPDATE shipfleet SET count=" + value + " WHERE fleetId=" + cgf.getFleetId() + " AND designId=" + key.getId());
                            }
                        }
                    }

                    ShipUtilities.cleanUpLoading(cgf.getFleetId());

                    if (tmpCount == 0) {
                        stmt.execute("DELETE FROM playerfleet WHERE id=" + cgf.getFleetId());
                        stmt.execute("DELETE FROM shipfleet WHERE fleetId=" + cgf.getFleetId());
                        stmt.execute("DELETE FROM fleetdetails WHERE fleetId=" + cgf.getFleetId());
                        stmt.execute("DELETE FROM fleetorders WHERE fleetId=" + cgf.getFleetId());
                    }
                }
            }

            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Exception in CombatHandler - updateDatabase: " + e);
            }
        }
    }

    private ArrayList<Integer> getPlanetArray() {
        ArrayList<Integer> planets = new ArrayList<Integer>();

        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id FROM planet WHERE systemId=" + systemId);
            while (rs.next()) {
                planets.add(Integer.valueOf(rs.getInt(1)));
            }
            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Exception in CombatHandler - getPlanetArray: " + e);
            }
        }

        return planets;
    }

    private void sendCombatReports() {
        GenerateMessage gm = new GenerateMessage();

        if (DEBUG) {
            // log.debug("Generated Reports: " + combatReports.size());
        }
        for (Map.Entry<String, String> me : combatReports.entrySet()) {
            if (DEBUG) {
                // log.debug("Sending combat report to user: " + me.getKey());
            }
            int userId = Integer.parseInt(me.getKey());

            gm.setSourceUserId(uDAO.getSystemUser().getUserId());
            gm.setDestinationUserId(userId);
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setTopic("Kampf in System " + systemId);
            gm.setMsg("Die Flottenzentrale hat soeben einen Kampfbericht &uuml;bermittelt<BR><BR>Eigene Verluste:<BR>" + me.getValue());
            gm.writeMessageToUser();
        }
    }

    /**
     * 
     // OLD ENDCOMMENT
    private String createChangedMessageFor(
            CombatGroupFleet.DesignDescriptor designDescriptor, int startNumberOfUnits, int endNumberOfUnits) {
        String designName = "- BAD DATA -";
        String chassisName = "- BAD DATA -";

        boolean isPlanetary = false;
//        boolean isDefensePlatform = false;

        int alteredUnitId = designDescriptor.getId();
        if (designDescriptor.getType() == CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE) {
            isPlanetary = true;
        } else if (designDescriptor.getType() == CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE) {
//            isDefensePlatform = true; //Never read
        }

        try {
            Statement stmt = DbConnect.createStatement();
            if (isPlanetary) {
                ResultSet rs = stmt.executeQuery("SELECT name FROM construction WHERE id=" + alteredUnitId);
                if (rs.next()) {
                    designName = rs.getString(1);
                    chassisName = "Geb&ouml;ude";
                }
            } else {
                ResultSet rs = stmt.executeQuery("SELECT sd.name, m.name FROM shipdesigns sd, module m WHERE sd.id=" + alteredUnitId + " AND m.id=sd.chassis");
                if (rs.next()) {
                    designName = rs.getString(1);
                    chassisName = rs.getString(2);
                }
                rs.close();
                stmt.close();
            }
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Problem in CombatHandler 3: " + e);
            }
            DebugBuffer.addLine(e);
            designName = "- Error -";
            chassisName = "- Error -";
        }

        return String.format(
                "<TR><TD>%s</TD><TD>%s</TD><TD>%d</TD><TD>%d</TD></TR>",
                designName, chassisName, startNumberOfUnits, startNumberOfUnits - endNumberOfUnits);
    }
    //TODO Diese Funktion enth&auml;lt 3x fast gleichen Code!!
    // -> Refactor!
    private void createCombatReports(int combatLevel, int pId, boolean surpressInfo) {
        String header = new String();
        StringBuilder tmpMessage = new StringBuilder();
        HashMap<String, String> procStarted = new HashMap<String, String>();
//        HashMap alliedProcDone = new HashMap();
//        HashMap enemyProcDone = new HashMap();
        HashMap<String, String> alliedMessage = new HashMap<String, String>();

        switch (combatLevel) {
            case (1):
                header += "<BR>Bericht f&uuml;r Bodenkampf auf Planet " + pId + ":<P><BR>";
                break;
            case (2):
                header += "<BR>Bericht f&uuml;r Orbitalkampf um Planet " + pId + ":<P><BR>";
                break;
            case (3):
                header += "<BR>Bericht f&uuml;r Systemkampf in System " + systemId + ":<P><BR>";
                break;
        }

        // Loop through combatGroups and retrieve original Information from Hashmap                
        if (DEBUG) {
            // log.debug("Start Creating Combat Reports");
        }
        for (int i = 0; i < combatGroups.size(); i++) {
            LinkedList<String> processedUsers = new LinkedList<String>();

            String enemyMessage = new String("Verluste gegnerischer Flotten:<BR>");
            if (DEBUG) {
                // log.debug("Get Combat Group");
            }
            CombatGroup cg = combatGroups.get(i);
            for (CombatGroupFleet cgf : cg.getFleetList()) {
                tmpMessage = new StringBuilder();

                Map<CombatGroupFleet.DesignDescriptor, Integer> sd = cgf.getShipDesign();
                HashMap<CombatGroupFleet.DesignDescriptor, Integer> orgSd = orgCombatGroups.get(cgf.getFleetIdStr());

                if (DEBUG) {
                    // log.debug("SIZE OF orgSd " + orgSd.size());
                    // log.debug("CONTENT FOLLOWS");

                    for (Map.Entry<CombatGroupFleet.DesignDescriptor, Integer> me : orgSd.entrySet()) {
                        // log.debug("ME-KEY=" + me.getKey() + " ME-VALUE=" + me.getValue());                        
                    }
                // log.debug("END OF CONTENT");
                }

                if (!procStarted.containsKey(cgf.getUserIdStr())) {
                    if (DEBUG) {
                        // log.debug("Processing started for user " + cgf.getUserIdStr());
                    }
                    tmpMessage.append(header);
                }

                if (DEBUG) {
                    // System.out.printf("Add fleet information (%s) for player %s", cgf.getFleetId(), cgf.getUserIdStr() + "\n");
                }
                String retreatInfo = "";
                if (cgf.isResigned()) {
                    retreatInfo = "<TR><TD colspan=4 class=\"blue2\"><B><FONT color=\"yellow\">Die Flotte zog sich ins System zur&uuml;ck, da sie den planetaren Schild nicht durchdringen konnte!</FONT></B></TD></TR>";
                } else if (cgf.isRetreated()) {
                    retreatInfo = "<TR><TD colspan=4 class=\"blue2\"><B><FONT color=\"yellow\">Die Flotte zog sich zur&uuml;ck, da die Verluste zu hoch wurden (&uuml;ber " + cgf.getRetreatFactor() + "%)!</FONT></B></TD></TR>";
                }

                tmpMessage.append("<TABLE class=\"blue\" width=\"450\"><TR><TD colspan=4 class=\"blue2\"><B>Verlustliste f&uuml;r Flotte " + cgf.getFleetName() + " (" + getUserNameForId(cgf.getUserId()) + "):</B></TD></TR>" + retreatInfo);
                tmpMessage.append("<TR><TD class=\"blue2\" width=\"40%\"><B>Name</B></TD><TD class=\"blue2\" width=\"30%\"><B>Typ</B></TD><TD class=\"blue2\" width=\"15%\"><B>Anzahl</B></TD><TD class=\"blue2\" width=\"15%\"><B>Verluste</B></TD></TR>");

                for (Map.Entry<CombatGroupFleet.DesignDescriptor, Integer> me : sd.entrySet()) {
                    tmpMessage.append(createChangedMessageFor(me.getKey(), orgSd.get(me.getKey()), me.getValue()));
                }

                tmpMessage.append("</TABLE><BR>");

                procStarted.put(cgf.getUserIdStr(), "");
                if (!processedUsers.contains(cgf.getUserIdStr())) {
                    if (DEBUG) {
                        // log.debug("Add user " + cgf.getUserIdStr() + " to procUserThisLoop");
                    }
                    processedUsers.add(cgf.getUserIdStr());
                }

                if (DEBUG) {
                    // log.debug("Combat Report for user " + cgf.getUserIdStr() + ": " + tmpMessage);                
                }
                if (DEBUG) {
                    // log.debug("Add fleet " + cgf.getFleetId() + " to combat report of user " + cgf.getUserIdStr());
                }
                if (!combatReports.containsKey(cgf.getUserIdStr())) {
                    combatReports.put(cgf.getUserIdStr(), tmpMessage.toString());
                    if (DEBUG) {
                        // log.debug("Current total Report for user " + cgf.getUserIdStr() + ": " + combatReports.get(cgf.getUserIdStr()));
                    }
                } else {
                    combatReports.put(cgf.getUserIdStr(), combatReports.get(cgf.getUserIdStr()) + tmpMessage);
                    if (DEBUG) {
                        // log.debug("Current total Report for user " + cgf.getUserIdStr() + ": " + combatReports.get(cgf.getUserIdStr()));
                    }
                }
            }

            // Add allied fleets information for current Battle Group
            if (DEBUG) {
                // log.debug("Start gathering of allied fleet information");
            }
            for (int l = 0; l < cg.getFleetList().size(); l++) {
                CombatGroupFleet cgf2 = cg.getFleetList().get(l);
                String procUserId = cgf2.getUserIdStr();

                if (alliedMessage.containsKey(cgf2.getUserIdStr())) {
                    if (DEBUG) {
                        // log.debug("Allied message for user " + cgf2.getUserId() + " already created");
                    }
                    break;
                } else {
                    if (DEBUG) {
                        // log.debug("Adding allied Fleets for User " + cgf2.getUserIdStr());
                    }
                }

                // Add all fleets which have same CombatGroup but different fleet userId
                for (int m = 0; m < cg.getFleetList().size(); m++) {
                    CombatGroupFleet cgf3 = cg.getFleetList().get(m);
                    if (cgf3.getUserId() == cgf2.getUserId()) {
                        if (DEBUG) {
                            // log.debug("This fleet (" + cgf3.getFleetId() + ") belongs to current player " + cgf3.getUserId());
                        }
                        continue;
                    }


                    StringBuilder currentAlliedMessage = new StringBuilder();
                    Map<CombatGroupFleet.DesignDescriptor, Integer> sd = cgf3.getShipDesign();
                    HashMap<CombatGroupFleet.DesignDescriptor, Integer> orgSd = orgCombatGroups.get(cgf3.getFleetIdStr());

                    if (!alliedMessage.containsKey(procUserId)) {
                        currentAlliedMessage.append("Verluste allierter Flotten:<BR>");
                    }

                    if (DEBUG) {
                        // log.debug("Adding fleet " + cgf3.getFleetName());
                    }
                    String retreatInfo = "";
                    if (cgf3.isResigned()) {
                        retreatInfo = "<TR><TD colspan=4 class=\"blue2\"><B><FONT color=\"yellow\">Die Flotte zog sich ins System zur&uuml;ck, da sie den planetaren Schild nicht durchdringen konnte!</FONT></B></TD></TR>";
                    } else if (cgf3.isRetreated()) {
                        retreatInfo = "<TR><TD colspan=4 class=\"blue2\"><B><FONT color=\"yellow\">Die Flotte zog sich zur&uuml;ck, da die Verluste zu hoch wurden (&uuml;ber " + cgf3.getRetreatFactor() + "%)!</FONT></B></TD></TR>";
                    }

                    currentAlliedMessage.append("<BR><TABLE class=\"blue\" width=\"450\"><TR><TD colspan=4 class=\"blue2\"><B>Verlustliste f&uuml;r Flotte " + cgf3.getFleetName() + " (" + getUserNameForId(cgf3.getUserId()) + "):</B></TD><TR>" + retreatInfo);
                    currentAlliedMessage.append("<TR><TD class=\"blue2\" width=\"40%\"><B>Name</B></TD><TD class=\"blue2\" width=\"30%\"><B>Typ</B></TD><TD class=\"blue2\" width=\"15%\"><B>Anzahl</B></TD><TD class=\"blue2\" width=\"15%\"><B>Verluste</B></TD></TR>");

                    for (Map.Entry<CombatGroupFleet.DesignDescriptor, Integer> me : sd.entrySet()) {
                        currentAlliedMessage.append(createChangedMessageFor(me.getKey(), orgSd.get(me.getKey()), me.getValue()));
                    }

                    if (DEBUG) {
                        // log.debug("Add fleet " + cgf3.getFleetId() + " for user " + procUserId);
                    }

                    if (!alliedMessage.containsKey(procUserId)) {
                        alliedMessage.put(procUserId, currentAlliedMessage.append("</TABLE><BR>").toString());
                    } else {
                        alliedMessage.put(procUserId, alliedMessage.get(procUserId) + currentAlliedMessage.append("</TABLE><BR>").toString());
                    }
                }
            }

            // Add enemy fleets information for current Battle Group
            HashMap<String, String> enemyProcStarted = new HashMap<String, String>();

            boolean infoHidden = false;
            for (int k = 0; k < combatGroups.size(); k++) {
                if (k != i) {
                    CombatGroup cg2 = combatGroups.get(k);
                    for (int l = 0; l < cg2.getFleetList().size(); l++) {
                        if (infoHidden) {
                            continue;
                        }
                        CombatGroupFleet cgf2 = cg2.getFleetList().get(l);
                        Map<CombatGroupFleet.DesignDescriptor, Integer> sd = cgf2.getShipDesign();
                        HashMap<CombatGroupFleet.DesignDescriptor, Integer> orgSd = orgCombatGroups.get(cgf2.getFleetIdStr());

                        if (enemyProcStarted.isEmpty()) {
                            enemyProcStarted.put(cgf2.getUserIdStr(), "");
                            enemyMessage += header;
                        }

                        String retreatInfo = "";
                        if (cgf2.isResigned() || cgf2.isRetreated()) {
                            retreatInfo = "<TR><TD colspan=4 class=\"blue2\"><B><FONT color=\"yellow\">Die feindliche Flotte zog sich vorzeitig vom Kampf zur&uuml;ck!</FONT></B></TD></TR>";
                        }

                        log.debug("Combatgroup " + cg.getId() + " is loser=" + cg.isLoser() + " and surpressing=" + surpressInfo);
                        if (cg.isLoser() && surpressInfo) {
                            enemyMessage += "<BR><B><FONT color=\"yellow\">Es konnten keine Informationen &uuml;ber die gegnerische Flottenst&auml;rke ermittelt werden.</FONT></B>";
                            infoHidden = true;
                        } else {
                            enemyMessage += "<BR><TABLE class=\"blue\" width=\"450\"><TR><TD colspan=4 class=\"blue2\"><B>Verlustliste f&uuml;r Flotte " + cgf2.getFleetName() + " (" + getUserNameForId(cgf2.getUserId()) + "):</B></TD><TR>" + retreatInfo;
                            enemyMessage += "<TR><TD class=\"blue2\" width=\"40%\"><B>Name</B></TD><TD class=\"blue2\" width=\"30%\"><B>Typ</B></TD><TD class=\"blue2\" width=\"15%\"><B>Anzahl</B></TD><TD class=\"blue2\" width=\"15%\"><B>Verluste</B></TD></TR>";

                            for (Map.Entry<CombatGroupFleet.DesignDescriptor, Integer> me : sd.entrySet()) {
                                enemyMessage += createChangedMessageFor(me.getKey(), orgSd.get(me.getKey()), me.getValue());
                            }

                            enemyMessage += "</TABLE>";
                        }
                    }
                }
            }

            for (String user : processedUsers) {
                if (alliedMessage.containsKey(user)) {
                    // combatReports.put(user, (combatReports.get(user) + enemyMessage));                                         
                    combatReports.put(user, (combatReports.get(user) + alliedMessage.get(user) + enemyMessage));
                } else {
                    combatReports.put(user, (combatReports.get(user) + enemyMessage));
                    if (DEBUG) {
                        // log.debug("Final message for user (" + user + "): " + combatReports.get(user));
                    }
                }
            }
        }
    }

    private void initCombatReports() {
        combatReports = new HashMap<String, String>();

        for (int i = 0; i < combatGroups.size(); i++) {
            CombatGroup cg = combatGroups.get(i);
            for (int j = 0; j < cg.getFleetList().size(); j++) {
                CombatGroupFleet cgf = cg.getFleetList().get(j);
                if (!combatReports.containsKey(cgf.getUserIdStr())) {
                    combatReports.put(cgf.getUserIdStr(), "");
                }
            }
        }
    }

    @SuppressWarnings("unused") //TODO L&ouml;schen, wenn die Funktion ned mehr gebraucht wird
    private void updateAllPlayers() {
        try {
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT playerplanet.userID FROM planet, playerplanet WHERE planet.systemId=" + systemId + " AND playerplanet.planetID=planet.id GROUP BY playerplanet.userID");
            while (rs.next()) {
                // Schmarren &ouml;berarbeiten
                if (rs.getInt(1) != initUserId) {
                    updateUserToCombatTick(rs.getInt(1));
                }
            }

            rs = stmt.executeQuery("SELECT playerfleet.userId FROM playerfleet, fleetdetails WHERE fleetdetails.destSystem=" + systemId + " AND playerfleet.id=fleetdetails.fleetId GROUP BY playerfleet.userId");
            while (rs.next()) {
                // Schmarren &ouml;berarbeiten
                if (rs.getInt(1) != initUserId) {
                    updateUserToCombatTick(rs.getInt(1));
                }
            }
            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Error in CombatHandler - updateAllPlayers: " + e);
            }
        }
    }

    private void updateUserToCombatTick(int userId) {
        try {
            // If the update is called through an incoming fleet the fleet should already been removed from 
            // actions and already included in battlegroup
            if (DEBUG) {
                // log.debug("External Update of user " + userId + " by CombatHandler initialised!");
            }
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT lastupdate FROM user WHERE id=" + userId);
            if (rs.next()) {
                int startTime = rs.getInt(1) + 1;
//                int timeUnits = combatTick - rs.getInt(1);

                // Updater2 upc = new Updater2();
                // upc.setStartTick(startTime);
                // upc.setTicksToCalc(timeUnits);
                // upc.setUpdateHistory(updateHistory);
                // upc.setCombatList(combatList);

                // while (upc.nextPlanet()) {
                    // upc.loadPlanetData();
                    // upc.calculateEnergyEff();
                    // upc.calculateUpdates();
                // }
            }

            stmt.executeUpdate("UPDATE user SET lastupdate=" + combatTick + " WHERE id=" + userId);
            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Error in CombatHandler - updateAllPlayers: " + e);
            }
        }
    }

    private void loadAllFleetsOnPlanet(PlanetFleetsBuffer pfb, int planetId) {
        // Load fleets with baseplanet pa[i]
        // = all fleets with destination planet of current planet in playerfleet
        if (pfb.getPlanetFleetsFor(planetId) == null) return;
        
        for (Integer fleetId : pfb.getPlanetFleetsFor(planetId)) {
            hu = new HangarUtilities();
            hu.reconstructUnloadedFleetInDB(fleetId);
            addFleetToCombatGroup(fleetId);
        }
    }

    private void loadAllFleetsInSystem() {
        try {
            // Alle Flotten die zum Kampftick im System sind laden
            // Ausnahme init Fleet Id
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = null;

            rs = stmt.executeQuery("(SELECT pf.id AS flottenId FROM playerfleet pf WHERE pf.systemId=" + systemId + " AND pf.planetId=0 AND pf.id!=" + initFleetId + " AND pf.status<>1)" +
                    " UNION DISTINCT " +
                    "(SELECT pf.id AS flottenId FROM playerfleet pf WHERE EXISTS (SELECT fleetId FROM fleetdetails WHERE fleetId=pf.id AND destSystem=" + systemId + ") AND NOT EXISTS (SELECT number FROM actions WHERE number=pf.id AND actionType=6 AND timeFinished<>" + combatTick + "))");

            while (rs.next()) {
                if (DEBUG) {
                    // log.debug("found fleet " + rs.getInt(1));
                }
                hu = new HangarUtilities();
                hu.reconstructUnloadedFleetInDB(rs.getInt(1));
                addFleetToCombatGroup(rs.getInt(1));
            }

            stmt.close();
        } catch (SQLException e) {
            if (DEBUG) {
                // log.debug("Error in CombatHandler - loadAllFleetsInSystem: " + e);            
            }
        }
    }

    private int determineCombatGroup(int fleetId) {
        int cgId = 0;

        try {
            // Besitzer ermitteln und alle CombatGroups auf eventuell Verb&ouml;ndete untersuchen
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT playerfleet.userId, alliancemembers.allianceId FROM playerfleet, alliancemembers WHERE playerfleet.id=" + fleetId + " AND alliancemembers.userId=playerfleet.userId");
//            boolean bgFound = false;                       

            // Nach Allianzen suchen
            if (rs.next()) {
                Statement stmt2 = DbConnect.createStatement();
                for (int i = 0; i < combatGroups.size(); i++) {
                    if (cgId != 0) {
                        break;
                    }
                    ResultSet rs2 = stmt2.executeQuery("SELECT masterAllianceId FROM alliance WHERE id=" + rs.getInt(2));
                    if (rs2.next()) {
                        if (DEBUG) {
                            // log.debug("Masteralliance " + rs2.getInt(1) + " found!");
                        }
                        if (rs2.getInt(1) == combatGroups.get(i).getMasterAlliance()) {
                            if (DEBUG) {
                                // log.debug("Current Alliance already has a group");
                            }
                            cgId = i + 1;
                            break;
                        }
                    } else {
                        if (DEBUG) {
                            // log.debug("Checking if direct member of masterAllianceId " + rs.getInt(2));
                        }
                        if (rs.getInt(2) == combatGroups.get(i).getMasterAlliance()) {
                            if (DEBUG) {
                                // log.debug("Current Alliance already has a group");
                            }
                            cgId = i + 1;
                            break;
                        }
                    }
                }
                stmt2.close();
            } else {
                if (DEBUG) {
                    // log.debug("No Alliance Found!");
                }
            }

            // &ouml;berpr&ouml;fen ob aktuelle Flotte einem Spieler in einer Kampfgruppe geh&ouml;rt
            rs = stmt.executeQuery("SELECT userId FROM playerfleet WHERE id=" + fleetId);
            if (rs.next()) {
                if (cgId == 0) {
                    for (int i = 0; i < combatGroups.size(); i++) {
                        if (cgId != 0) {
                            break;
                        }
                        if (DEBUG) {
                            // log.debug("Scanning Combat Group " + (i + 1));
                        }
                        for (Iterator<CombatGroupFleet> it = combatGroups.get(i).getFleetList().iterator(); it.hasNext();) {
                            CombatGroupFleet tmpCGF = it.next();
                            if (DEBUG) {
                                // log.debug("Comparing user Id in CGF " + tmpCGF.getUserId() + " with userId of fleet " + rs.getInt(1));
                            }
                            if (tmpCGF.getUserId() == rs.getInt(1)) {
                                if (DEBUG) {
                                    // log.debug("Current Alliance already has a group");
                                }
                                cgId = i + 1;
                                break;
                            }
                        }
                    }
                }
            } else {
                if (DEBUG) {
                    // log.debug("An schass is passiert!");
                }
            }

            // falls Verb&ouml;ndeter gefunden -> diese Combatgroup zur&ouml;ckgeben
            if (cgId != 0) {
                return cgId;
            }

            // falls nicht neue Combatgroup anlegen            
            cgId = combatGroups.size() + 1;
            combatGroups.add(new CombatGroup());

            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Exception in CombatHandler - determineCombatGroup: " + e);
            }
        }
        return cgId;
    }

    private int determineCombatGroupForDefense(int sId, int pId) {
        int cgId = 0;

        try {
            // Besitzer ermitteln und alle CombatGroups auf eventuell Verb&ouml;ndete untersuchen
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = null;
            if (sId == 0) {
                rs = stmt.executeQuery("SELECT planetdefense.userId, alliancemembers.allianceId FROM planetdefense, alliancemembers WHERE planetdefense.planetId=" + pId + " AND alliancemembers.userId=planetdefense.userId");
            } else {
                rs = stmt.executeQuery("SELECT planetdefense.userId, alliancemembers.allianceId FROM planetdefense, alliancemembers WHERE planetdefense.systemId=" + sId + " AND planetdefense.planetId=0 AND alliancemembers.userId=planetdefense.userId");
            }

//            boolean bgFound = false;                       

            // Nach Allianzen suchen
            if (rs.next()) {
                Statement stmt2 = DbConnect.createStatement();
                for (int i = 0; i < combatGroups.size(); i++) {
                    if (cgId != 0) {
                        break;
                    }
                    ResultSet rs2 = stmt2.executeQuery("SELECT masterAllianceId FROM alliance WHERE id=" + rs.getInt(2));
                    if (rs2.next()) {
                        if (DEBUG) {
                            // log.debug("Masteralliance " + rs2.getInt(1) + " found!");
                        }
                        if (rs2.getInt(1) == combatGroups.get(i).getMasterAlliance()) {
                            if (DEBUG) {
                                // log.debug("Current Alliance already has a group");
                            }
                            cgId = i + 1;
                            break;
                        }
                    } else {
                        if (DEBUG) {
                            // log.debug("Checking if direct member of masterAllianceId " + rs.getInt(2));
                        }
                        if (rs.getInt(2) == combatGroups.get(i).getMasterAlliance()) {
                            if (DEBUG) {
                                // log.debug("Current Alliance already has a group");
                            }
                            cgId = i + 1;
                            break;
                        }
                    }
                }
                stmt2.close();
            } else {
                if (DEBUG) {
                    // log.debug("No Alliance Found!");
                }
            }

            // &ouml;berpr&ouml;fen ob aktuelle Flotte einem Spieler in einer Kampfgruppe geh&ouml;rt
            if (sId == 0) {
                rs = stmt.executeQuery("SELECT planetdefense.userId FROM planetdefense WHERE planetdefense.planetId=" + pId);
            } else {
                rs = stmt.executeQuery("SELECT planetdefense.userId FROM planetdefense WHERE planetdefense.systemId=" + sId + " AND planetdefense.planetId=0");
            }
            if (rs.next()) {
                if (cgId == 0) {
                    for (int i = 0; i < combatGroups.size(); i++) {
                        if (cgId != 0) {
                            break;
                        }
                        if (DEBUG) {
                            // log.debug("Scanning Combat Group " + (i + 1));
                        }
                        for (Iterator<CombatGroupFleet> it = combatGroups.get(i).getFleetList().iterator(); it.hasNext();) {
                            CombatGroupFleet tmpCGF = it.next();
                            if (DEBUG) {
                                // log.debug("Comparing user Id in CGF " + tmpCGF.getUserId() + " with userId of fleet " + rs.getInt(1));
                            }
                            if (tmpCGF.getUserId() == rs.getInt(1)) {
                                if (DEBUG) {
                                    // log.debug("Current Alliance already has a group");
                                }
                                cgId = i + 1;
                                break;
                            }
                        }
                    }
                }
            } else {
                if (DEBUG) {
                    // log.debug("An schass is passiert!");
                }
            }

            // falls Verb&ouml;ndeter gefunden -> diese Combatgroup zur&ouml;ckgeben
            if (cgId != 0) {
                return cgId;
            }

            // falls nicht neue Combatgroup anlegen            
            cgId = combatGroups.size() + 1;
            combatGroups.add(new CombatGroup());

            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Exception in CombatHandler - determineCombatGroup: " + e);
            }
        }
        return cgId;
    }

    private void loadSystemDefense(int sId) {
        // Systemverteidigung nach UserId ermitteln und eine "Pseudoflotte in die Combat Group stellen"
        try {
            if (DEBUG) {
                // log.debug("Check Systemdefense of system " + sId);
            }
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM planetdefense WHERE planetId=0 AND systemId=" + sId + " AND (type=1 OR type=2) ORDER BY userId ASC");
            boolean firstRun = true;
            int cgId = 0;
            int userIdOld = 0;
            int fleetIndex = 0;
            boolean fleetExists = false;

            while (rs.next()) {
                if (DEBUG) {
                    // log.debug("Found defense");
                }
                if ((firstRun) || ((userIdOld != rs.getInt(5)) && (userIdOld != 0))) {
                    firstRun = false;
                    fleetExists = false;
                    // Determine Combat Group
                    cgId = determineCombatGroupForDefense(rs.getInt(2), 0);
                    if (DEBUG) {
                        // log.debug("Add defense to Combat Group " + cgId);
                    }
                }

                CombatGroup cg = combatGroups.get(cgId - 1);

                // Check for planetDefenseFleet in current Combat Group                
                if ((cg.getFleetList() != null) && (!fleetExists)) {
                    for (int i = 0; i < cg.getFleetList().size(); i++) {
                        CombatGroupFleet cgf = cg.getFleetList().get(i);
                        if (cgf.isSystemDefense() && (cgf.getUserId() == rs.getInt(5))) {
                            fleetIndex = i;
                            fleetExists = true;
                            if (DEBUG) {
                                // log.debug("There is already a systemdefense fleet");
                            }
                        }
                    }
                }

                if (!fleetExists) {
                    if (DEBUG) {
                        // log.debug("Creating a new planet Defense Fleet for user " + rs.getInt(5));
                        // If not found -> create
                    }
                    Map<CombatGroupFleet.DesignDescriptor, Integer> shipDesignCount = new HashMap<CombatGroupFleet.DesignDescriptor, Integer>();

                    CombatGroupFleet cgf = new CombatGroupFleet();
                    cg.setFleetCount(cg.getFleetCount() + 1);
                    cg.setShipCount(cg.getShipCount() + rs.getInt(7));
                    cgf.setFleetName("System Verteidigung f&uuml;r System " + sId);
                    cgf.setSystemDefense(true);
                    cgf.setUserId(rs.getInt(5));
                    cgf.setGroupId(cgId);
                    cgf.setActShipCount(rs.getInt(7));

                    shipDesignCount.put(
                            new CombatGroupFleet.DesignDescriptor(
                            rs.getInt(4), CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE),
                            rs.getInt(7));

                    cgf.setShipDesign(shipDesignCount);
                    if (cg.getFleetList() == null) {
                        // Create new Fleetlist
                        ArrayList<CombatGroupFleet> fleetList = new ArrayList<CombatGroupFleet>();
                        fleetList.add(cgf);
                        cg.setFleetList(fleetList);
                    } else {
                        // Update Fleetlist
                        cg.getFleetList().add(cgf);
                    }
                } else {
                    if (DEBUG) {
                        // log.debug("Adding data to system Defense Fleet of user " + rs.getInt(5));
                        // If found -> update
                    }
                    CombatGroupFleet cgf = cg.getFleetList().get(fleetIndex);

                    Map<CombatGroupFleet.DesignDescriptor, Integer> shipDesignCount = cgf.getShipDesign();

                    cg.setShipCount(cg.getShipCount() + rs.getInt(7));
                    cgf.setActShipCount(cgf.getActShipCount() + rs.getInt(7));
                    if ((rs.getInt(6) == 1) || (rs.getInt(6) == 2)) {

                        CombatGroupFleet.DesignDescriptor key;
                        if (rs.getInt(6) == 1) {
                            key = new CombatGroupFleet.DesignDescriptor(rs.getInt(4), CombatGroupFleet.UnitTypeEnum.SURFACE_DEFENSE);
                        } else {
                            key = new CombatGroupFleet.DesignDescriptor(rs.getInt(4), CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE);
                        }
                        int count = rs.getInt(7);
                        Integer oldValue = shipDesignCount.get(key);
                        shipDesignCount.put(key, count + ((oldValue != null) ? oldValue : 0));
                    }
                }

                userIdOld = rs.getInt(5);
            }
            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Error in loadPlanetDefense: " + e);
            }
        }
    }

    private void loadPlanetDefense(int pId) {
        // Load all defense plattforms and planetary Defense and Group together in one "Pseudofleet"
        try {
            if (DEBUG) {
                // log.debug("Check Planetdefense of planet " + pId);
            }
            Statement stmt = DbConnect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM planetdefense WHERE planetId=" + pId + " AND (type=1 OR type=2) ORDER BY userId ASC");
            boolean firstRun = true;
            int cgId = 0;
            int userIdOld = 0;
            int fleetIndex = 0;
            boolean fleetExists = false;

            while (rs.next()) {
                if (DEBUG) {
                    // log.debug("Found defense");
                }
                if ((firstRun) || ((userIdOld != rs.getInt(5)) && (userIdOld != 0))) {
                    firstRun = false;
                    fleetExists = false;
                    // Determine Combat Group
                    cgId = determineCombatGroupForDefense(0, rs.getInt(1));
                    if (DEBUG) {
                        // log.debug("Add defense to Combat Group " + cgId);
                    }
                }

                CombatGroup cg = combatGroups.get(cgId - 1);

                // Check for planetDefenseFleet in current Combat Group                
                if ((cg.getFleetList() != null) && (!fleetExists)) {
                    for (int i = 0; i < cg.getFleetList().size(); i++) {
                        CombatGroupFleet cgf = cg.getFleetList().get(i);
                        if (cgf.isPlanetDefense() && (cgf.getUserId() == rs.getInt(5))) {
                            fleetIndex = i;
                            fleetExists = true;
                            if (DEBUG) {
                                // log.debug("There is already a planetdefense fleet");
                            }
                        }
                    }
                }

                if (!fleetExists) {
                    if (DEBUG) {
                        // log.debug("Creating a new planet Defense Fleet for user " + rs.getInt(5));
                        // If not found -> create
                    }
                    Map<CombatGroupFleet.DesignDescriptor, Integer> shipDesignCount =
                            new HashMap<CombatGroupFleet.DesignDescriptor, Integer>();

                    CombatGroupFleet cgf = new CombatGroupFleet();
                    cg.setFleetCount(cg.getFleetCount() + 1);
                    cg.setShipCount(cg.getShipCount() + rs.getInt(7));
                    cgf.setFleetName("Planetare Verteidigung f&uuml;r Planet " + pId);
                    cgf.setPlanetDefense(true);
                    cgf.setUserId(rs.getInt(5));
                    cgf.setGroupId(cgId);
                    cgf.setActShipCount(rs.getInt(7));

                    shipDesignCount.put(new CombatGroupFleet.DesignDescriptor(
                            rs.getInt(4), rs.getInt(6)), rs.getInt(7));

                    cgf.setShipDesign(shipDesignCount);
                    if (cg.getFleetList() == null) {
                        // Create new Fleetlist
                        ArrayList<CombatGroupFleet> fleetList = new ArrayList<CombatGroupFleet>();
                        fleetList.add(cgf);
                        cg.setFleetList(fleetList);
                    } else {
                        // Update Fleetlist
                        cg.getFleetList().add(cgf);
                    }
                } else {
                    if (DEBUG) {
                        // log.debug("Adding data to planet Defense Fleet of user " + rs.getInt(5));
                        // If found -> update
                    }
                    CombatGroupFleet cgf = cg.getFleetList().get(fleetIndex);

                    Map<CombatGroupFleet.DesignDescriptor, Integer> shipDesignCount =
                            cgf.getShipDesign();

                    cg.setShipCount(cg.getShipCount() + rs.getInt(7));
                    cgf.setActShipCount(cgf.getActShipCount() + rs.getInt(7));
                    if ((rs.getInt(6) == 1) || (rs.getInt(6) == 2)) {

                        CombatGroupFleet.DesignDescriptor key = new CombatGroupFleet.DesignDescriptor(
                                rs.getInt(4), rs.getInt(6));
                        int count = rs.getInt(7);
                        Integer oldValue = shipDesignCount.get(key);
                        shipDesignCount.put(key, count + ((oldValue != null) ? oldValue : 0));
                    }
                }

                userIdOld = rs.getInt(5);
            }
            stmt.close();
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Error in loadPlanetDefense: " + e);
            }
        }
    }

    private void addFleetToCombatGroup(int fleetId) {
        try {
            Statement stmt = DbConnect.createStatement();
            Statement stmt2 = DbConnect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT fleetId FROM shipfleet WHERE fleetId=" + fleetId + " LIMIT 1");
            if (!rs.next()) {
                if (DEBUG) {
                    // log.debug("Invalid fleet " + fleetId + " found!");
                }
                return;
            }

            int cgId = determineCombatGroup(fleetId);
            if (DEBUG) {
                // log.debug("Loading Fleet " + fleetId + " into Combat Group " + cgId);
            }
            CombatGroup cg = combatGroups.get(cgId - 1);
            cg.setFleetCount(cg.getFleetCount() + 1);

            // if fleet has no fleetdetails read planet from playerfleet table
            boolean incomingFleet;
            rs = stmt.executeQuery("SELECT Id FROM fleetdetails WHERE fleetdetails.fleetId=" + fleetId);

            if (rs.next()) {
                incomingFleet = true;
                rs = stmt.executeQuery("SELECT playerfleet.name, playerfleet.userId, shipfleet.designId, shipfleet.count, fleetdetails.destplanet, playerfleet.status FROM playerfleet, shipfleet, fleetdetails WHERE fleetdetails.fleetId=playerfleet.id AND shipfleet.fleetId=playerfleet.id AND playerfleet.id=" + fleetId);
            } else {
                incomingFleet = false;
                rs = stmt.executeQuery("SELECT playerfleet.name, playerfleet.userId, shipfleet.designId, shipfleet.count, playerfleet.planetId, playerfleet.status FROM playerfleet, shipfleet WHERE shipfleet.fleetId=playerfleet.id AND playerfleet.id=" + fleetId);
            }

            // Erstellen der kompletten Flottenstruktur mit Anzahl der Schiffe und Designs als Ausgangsbasis f&uuml;r 
            // SpaceCombat Klasse
            if (DEBUG) {
                // log.debug("Trying to build a new Fleet Entry");
            }
            boolean firstLoop = true;
            int shipCount = 0;
            CombatGroupFleet cgf = new CombatGroupFleet();
            Map<CombatGroupFleet.DesignDescriptor, Integer> shipDesignCount =
                    new HashMap<CombatGroupFleet.DesignDescriptor, Integer>();

            while (rs.next()) {
                if (firstLoop) {
                    if (DEBUG) {
                        // log.debug("Building ...");                    // Retrieve special orders
                    }
                    ResultSet rs2 = stmt2.executeQuery("SELECT retreatFactor, retreatToType, retreatTo FROM fleetorders WHERE fleetId=" + fleetId);
                    if (rs2.next()) {
                        cgf.setRetreatFactor(rs2.getInt(1));
                        cgf.setRetreatToType(rs2.getInt(2));
                        cgf.setRetreatTo(rs2.getInt(3));
                    } else {
                        cgf.setRetreatFactor(100);
                        cgf.setRetreatToType(0);
                        cgf.setRetreatTo(0);
                    }

                    cgf.setUserId(rs.getInt(2));
                    cgf.setFleetId(fleetId);
                    cgf.setFleetName(rs.getString(1));
                    cgf.setGroupId(cgId);
                    cgf.setDestPlanet(rs.getInt(5));
                    cgf.setDefenseMode(rs.getInt(6) == 2);
                    firstLoop = false;
                }

                if (DEBUG) {
                    // log.debug("Loading Ship " + rs.getInt(3) + " into fleet " + cgf.getFleetId());
                }
                shipCount += rs.getInt(4);
                shipDesignCount.put(new CombatGroupFleet.DesignDescriptor(
                        rs.getInt(3), CombatGroupFleet.UnitTypeEnum.SHIP), rs.getInt(4));

            }

            // Set Master Alliance Id if empty 
            if (cg.getMasterAlliance() == 0) {
                rs = stmt.executeQuery("SELECT a.masterAllianceId FROM alliance a, alliancemembers am WHERE am.allianceId=a.id AND am.userId=" + cgf.getUserId());
                if (rs.next()) {
                    cg.setMasterAlliance(rs.getInt(1));
                    if (DEBUG) {
                        // log.debug("Set MasterAllianceId to " + rs.getInt(1) + " for CombatGroup " + cgId);
                    }
                }
                if (cg.getMasterAlliance() == 0) {
                    rs = stmt.executeQuery("SELECT am.allianceId FROM alliancemembers am WHERE am.userId=" + cgf.getUserId());
                    if (rs.next()) {
                        cg.setMasterAlliance(rs.getInt(1));
                        if (DEBUG) {
                            // log.debug("Set MasterAllianceId to " + rs.getInt(1) + " for CombatGroup " + cgId);
                        }
                    }
                }
            }

            cgf.setActShipCount(shipCount);
            cgf.setShipCount(shipCount);
            cgf.setShipDesign(shipDesignCount);
            cg.setShipCount(cg.getShipCount() + cgf.getShipCount());
            cg.addFleet(cgf);

            // After adding a new fleet delete the fleet from actions table
            stmt.execute("DELETE FROM actions WHERE number=" + fleetId + " AND actionType=6");
            stmt.close();
            stmt2.close();

            combatGroups.set(cgId - 1, cg);
        } catch (Exception e) {
            if (DEBUG) {
                // log.debug("Exception in CombatHandler - addFleetToCombatGroup: " + e);
            }
        }
    }

    private void backupCombatGroup() {
        orgCombatGroups = new HashMap<String, HashMap<CombatGroupFleet.DesignDescriptor, Integer>>();

        for (int i = 0; i < combatGroups.size(); i++) {
            CombatGroup cg = combatGroups.get(i);
            for (int j = 0; j < cg.getFleetList().size(); j++) {
                CombatGroupFleet cgf = cg.getFleetList().get(j);
                Map<CombatGroupFleet.DesignDescriptor, Integer> sd = cgf.getShipDesign();
                HashMap<CombatGroupFleet.DesignDescriptor, Integer> sd2 =
                        new HashMap<CombatGroupFleet.DesignDescriptor, Integer>();

                for (Map.Entry<CombatGroupFleet.DesignDescriptor, Integer> me : sd.entrySet()) {
                    sd2.put(me.getKey().clone(), new Integer(me.getValue()));
                }

                orgCombatGroups.put(cgf.getFleetIdStr(), sd2);
            }
        }
    }

    @Deprecated
    public String getUserNameForId(int id) {
        String name = "";

        try {
            /* REMOVED USERBUFFER
            name = UserBuffer.getUserData(id).getUserName();
            // OLD ENDCOMMENT
        } catch (NullPointerException e) {
            if (DEBUG) {
                // log.debug("Error in getUserNameForId=" + e);
            }
        }

        return name;
    }

    public ArrayList<Combat> getCombatList() {
        return combatList;
    }

    public void setCombatList(ArrayList<Combat> combatList) {
        this.combatList = combatList;
    }

    public boolean initUserMayUpdateVT() {
        return surpressViewTable;
    }
    
    private class PlanetFleetsBuffer {

        private HashMap<Integer, ArrayList<Integer>> fleets = new HashMap<Integer, ArrayList<Integer>>();

        public PlanetFleetsBuffer(int systemId) {
            try {
                Statement stmt = DbConnect.createStatement();
                if (DEBUG) {
                    // log.debug("Look for fleets on planet " + planetId);
                }

                ResultSet rs = stmt.executeQuery("SELECT id, planetId FROM playerfleet WHERE systemId=" + systemId + " AND planetId<>0 AND NOT EXISTS (SELECT fleetId FROM fleetdetails WHERE fleetId=playerfleet.id)");

                while (rs.next()) {
                    if (DEBUG) {
                        // log.debug("Found planetfleet (already there) " + rs.getInt(1));
                    }

                    if (!fleets.containsKey(rs.getInt(2))) {
                        ArrayList<Integer> locFleets = new ArrayList<Integer>();
                        locFleets.add(rs.getInt(1));
                        fleets.put(rs.getInt(2), locFleets);
                    } else {
                        fleets.get(rs.getInt(2)).add(rs.getInt(1));
                    }
                }

                // Zus&ouml;tzlich Flotten laden die Planeten Information haben, aber auch einen 
                // Fleetdetails Eintrag ohne zugeh&ouml;rigen Actions Eintrag
                rs = stmt.executeQuery("SELECT pf.id, fd.destPlanet FROM playerfleet pf, fleetdetails fd WHERE fd.fleetId=pf.id AND fd.destSystem=" + systemId + " AND fd.destPlanet<>0 AND NOT EXISTS (SELECT number FROM actions WHERE number=pf.id AND actionType=6 AND timeFinished<>" + combatTick + ")");
                while (rs.next()) {
                    if (DEBUG) {
                        // log.debug("Found planetfleet (incomming) " + rs.getInt(1));
                    }

                    if (!fleets.containsKey(rs.getInt(2))) {
                        ArrayList<Integer> locFleets = new ArrayList<Integer>();
                        locFleets.add(rs.getInt(1));
                        fleets.put(rs.getInt(2), locFleets);
                    } else {
                        fleets.get(rs.getInt(2)).add(rs.getInt(1));
                    }
                }

                stmt.close();
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Exception while determining planet fleets: ", e);
            }
        }        
        
        public ArrayList<Integer> getPlanetFleetsFor(int planetId) {
            if (fleets.containsKey(planetId)) {
                return fleets.get(planetId);
            } else {
                return null;
            }
        }
    }
    */
}
