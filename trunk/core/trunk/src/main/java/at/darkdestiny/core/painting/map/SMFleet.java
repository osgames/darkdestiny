/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import at.darkdestiny.core.ships.ShipData;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class SMFleet {
    private final int id;
    private final int x;
    private final int y;    
    private final int status;    
    
    private boolean moving = false;
    private boolean formation = false;
    private int targetX;
    private int targetY;
    private int timeToTarget;
    private int targetSystemId;
    private int targetPlanetId;
    private int range;
    
    private String owner = "?";
    private String name;
    
    private ArrayList<ShipData> ships = new ArrayList<ShipData>();
    
    public SMFleet(int id, int x, int y, int status) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.status = status;
        this.name = "Flotte #" + id;
    }

    public void setMoving(int targetX, int targetY) {
        moving = true;
        this.targetX = targetX;
        this.targetY = targetY;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @return the moving
     */
    public boolean isMoving() {
        return moving;
    }

    /**
     * @return the targetX
     */
    public int getTargetX() {
        return targetX;
    }

    /**
     * @return the targetY
     */
    public int getTargetY() {
        return targetY;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ships
     */
    public ArrayList<ShipData> getShips() {
        return ships;
    }

    /**
     * @param ships the ships to set
     */
    public void addShips(ShipData sd) {
        this.ships.add(sd);
    }

    /**
     * @return the timeToTarget
     */
    public int getTimeToTarget() {
        return timeToTarget;
    }

    /**
     * @param timeToTarget the timeToTarget to set
     */
    public void setTimeToTarget(int timeToTarget) {
        this.timeToTarget = timeToTarget;
    }

    /**
     * @return the targetSystemId
     */
    public int getTargetSystemId() {
        return targetSystemId;
    }

    /**
     * @param targetSystemId the targetSystemId to set
     */
    public void setTargetSystemId(int targetSystemId) {
        this.targetSystemId = targetSystemId;
    }

    /**
     * @return the targetPlanetId
     */
    public int getTargetPlanetId() {
        return targetPlanetId;
    }

    /**
     * @param targetPlanetId the targetPlanetId to set
     */
    public void setTargetPlanetId(int targetPlanetId) {
        this.targetPlanetId = targetPlanetId;
    }

    /**
     * @return the range
     */
    public int getRange() {
        return range;
    }

    /**
     * @param range the range to set
     */
    public void setRange(int range) {
        this.range = range;
    }
}
