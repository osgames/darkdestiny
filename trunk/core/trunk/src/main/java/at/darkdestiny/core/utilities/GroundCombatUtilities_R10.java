/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.PlanetLoyalityDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.diplomacy.combat.CombatGroupEntry;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResolver;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResult;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.ECombatReportStatus;
import at.darkdestiny.core.enumeration.ECombatStatus;
import at.darkdestiny.core.enumeration.EGroundCombatChart;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.groundcombat.BattleData;
import at.darkdestiny.core.model.*;
import at.darkdestiny.core.result.GroundCombatResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.service.TransportService;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import com.google.common.collect.Maps;
import java.awt.Font;
import java.lang.System;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedDomainCategoryPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Aion
 */
public class GroundCombatUtilities_R10 extends Service {

    private static final Logger log = LoggerFactory.getLogger(GroundCombatUtilities.class);

    private static boolean debug = true;
    public static double minTerritory = 0.01d;
    public static double maxTerritory = 0.99d;
    public static double attStartTerritory = 0.10d;
    public static int landingDuration = 10;
    //Main Combatfunction
    private static long time = 0l;
    private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);
    private static PlanetLoyalityDAO plDAO = (PlanetLoyalityDAO) DAOFactory.get(PlanetLoyalityDAO.class);

    public static void checkForCombat(int landerUserId, int planetId) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(planetId);
        Planet p = planetDAO.findById(planetId);

        //Return due to empty Planet
        if (pp == null) {
            if (debug) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "No combat due to pp == null");
            }
            return;
        }
        //Return due to 0
        if (landerUserId < 0) {
            if (debug) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "No combat due to landerId < 0");
            }
            return;
        }

        //Return because own planet
        if (pp.getUserId() == landerUserId) {
            if (debug) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "No combat due to same users");
            }
            return;
        }

        //Return because user is not hostile
        if (!DiplomacyUtilities.getDiplomacyResult(pp.getUserId(), landerUserId).isAttacks()) {
            if (debug) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "No combat -> not hostile");
            }
            return;
        }

        //User unloading on hostile planet
        if (pp.getUserId() != landerUserId) {
            ArrayList<Integer> usersOnPlanet = findUsersOnPlanet(planetId);
            //Add enemyUser
            if (!usersOnPlanet.contains(landerUserId)) {
                usersOnPlanet.add(landerUserId);
            }

            //Getting Combat groups for users on planet
            boolean ownerHasTroops = true;
            int defenderCount = 0;

            if (!usersOnPlanet.contains(pp.getUserId())) {
                ownerHasTroops = false;
                usersOnPlanet.add(pp.getUserId());
            }

            CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(usersOnPlanet, new RelativeCoordinate(0, planetId));

            for (CombatGroupEntry cge : cgr.getCombatGroups()) {
                for (Integer userTmp : cge.getPlayers()) {
                    if (userTmp.equals(pp.getUserId())) {
                        defenderCount = cge.getPlayers().size();
                        if (ownerHasTroops == false) {
                            defenderCount--;
                        }
                        break;
                    }
                }
            }

            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "GOT HERE: " + planetId + " GCR Conflict = " + cgr.hasConflictingParties() + " CG GROUPS=" + cgr.getCombatGroups().size());
            }

            //We have Conflicting Parties
            if (cgr.hasConflictingParties()) {
                //Move without fight
                if (debug) {
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "defenderCount = " + defenderCount);
                }


                if (defenderCount == 0) {

                    ArrayList<Model> actRemoveObjects = PlanetUtilities.movePlanetToPlayer(landerUserId, planetId, Service.playerPlanetDAO.findByPlanetId(planetId), 0, false);
                    for (Model m : actRemoveObjects) {
                        if (m instanceof TradeRoute) {
                            TradeRoute tr = (TradeRoute) m;
                            TransportService.deleteRoute(tr.getUserId(), tr.getId());
                        } else if (m instanceof ProductionOrder) {
                            ProductionOrder po = (ProductionOrder) m;
                            productionOrderDAO.remove(po);

                            Action a = actionDAO.findByTimeFinished(po.getId(), EActionType.GROUNDTROOPS);
                            Action a1 = actionDAO.findByTimeFinished(po.getId(), EActionType.SHIP);
                            Action a2 = actionDAO.findByTimeFinished(po.getId(), EActionType.SPACESTATION);

                            if (a != null) {
                                actionDAO.remove(a);
                            }
                            if (a1 != null) {
                                actionDAO.remove(a1);
                            }
                            if (a2 != null) {
                                actionDAO.remove(a2);
                            }
                        }
                        // actionDAO.remove(a);
                    }

                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Return instant ownage");
                    //Fight occures
                } else {

                    //Search for existing Combat
                    GroundCombat gc = groundCombatDAO.findRunningCombat(planetId);
                    //Existing Combat => New Player or Reinforcements
                    if (gc != null) {

                        //There is already a combat ongoing
                        //User joining if he has no units on the planet or he is not yet in the combatplayer table
                        if (!usersOnPlanet.contains(landerUserId) || combatPlayerDAO.findBy(gc.getId(), landerUserId) == null) {
                            CombatPlayer cp = new CombatPlayer();
                            cp.setCombatId(gc.getId());
                            cp.setStartTime(System.currentTimeMillis());
                            cp.setUserId(landerUserId);
                            cp.setReportStatus(ECombatReportStatus.SHOWING);
                            combatPlayerDAO.add(cp);

                            if (!usersOnPlanet.contains(landerUserId)) {
                                usersOnPlanet.add(landerUserId);
                            }
                            sendJoinMessage(landerUserId, pp, usersOnPlanet);

                        }
                        //No Combat Existing
                    } else {
                        //Adding Combat
                        gc = new GroundCombat();
                        gc.setCombatStatus(ECombatStatus.LANDING);
                        gc.setPlanetId(planetId);
                        gc.setStartTime(System.currentTimeMillis());
                        gc.setPlanetOwner(pp.getUserId());
                        gc.setWinner(0);
                        gc = groundCombatDAO.add(gc);

                        //Send invasionMessage
                        sendInvasionMessage(landerUserId, pp, usersOnPlanet);

                        //Adding combatPlayer
                        for (Integer i : usersOnPlanet) {

                            if (combatPlayerDAO.findBy(gc.getId(), i) == null) {
                                CombatPlayer cp = new CombatPlayer();
                                cp.setCombatId(gc.getId());
                                cp.setStartTime(System.currentTimeMillis());
                                cp.setUserId(i);
                                cp.setReportStatus(ECombatReportStatus.SHOWING);

                                combatPlayerDAO.add(cp);
                            }
                        }
                    }
                }
            }
        }
    }

    public static ArrayList<Model> processTick(ArrayList<PlayerPlanet> ppList) {
        ArrayList<Model> removeObjects = new ArrayList<Model>();

        DebugBuffer.addLine(DebugLevel.DEBUG, "Start Processing Ground combat");


        time = System.currentTimeMillis();
        if (debug) {
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Prepare BattleData" + (System.currentTimeMillis() - time) + " ms");
        }

        //Global Combat Entries
        ArrayList<TerritoryEntry> territoryToSave = new ArrayList<TerritoryEntry>();
        ArrayList<RoundEntry> roundsToSave = new ArrayList<RoundEntry>();



        //Loop all combats
        for (GroundCombat gc : groundCombatDAO.findRunningCombats()) {

            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "#####################");
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Processign Combat : " + gc.getId() + " Status : " + gc.getCombatStatus());
            }

            //Generate Battle Data with all Information to process a round
            BattleData bd = new BattleData();
            bd.setPlanetId(gc.getPlanetId());
            bd.setSystemId(planetDAO.findById(gc.getPlanetId()).getSystemId());
            bd.setGroundCombatId(gc.getId());
            bd.setStatus(gc.getCombatStatus());
            bd.setPlanetOwner(gc.getPlanetOwner());
            bd.setCombatRounds(combatRoundDAO.findByCombatId(gc.getId()));
            bd.setCombatPlayers(combatPlayerDAO.findByCombatId(gc.getId()));

            TreeMap<Integer, User> users = new TreeMap<Integer, User>();

            for (User u : (ArrayList<User>) userDAO.findAllNonSystem()) {
                users.put(u.getUserId(), u);
            }
            bd.setUsers(users);

            ArrayList<Integer> parts = findUsersOnPlanet(gc.getPlanetId());

            //If PlanetOwner himself has no groundtroops add him anyways
            if (!parts.contains(gc.getPlanetOwner())) {
                parts.add(gc.getPlanetOwner());
            }

            //Compute Groups
            CombatGroupResult gcr = CombatGroupResolver.getCombatGroups(parts, new RelativeCoordinate(bd.getSystemId(), bd.getPlanetId()));

            HashMap<Integer, ArrayList<Integer>> groups = gcr.getAsMap();
            //Set Groups
            bd.setGroups(groups);

            //Search Troops
            TreeMap<Integer, TreeMap<Integer, PlayerTroop>> troops = new TreeMap<Integer, TreeMap<Integer, PlayerTroop>>();
            for (PlayerTroop pt : playerTroopDAO.findByPlanetId(gc.getPlanetId())) {
                TreeMap<Integer, PlayerTroop> entry1 = troops.get(pt.getUserId());

                if (entry1 == null) {
                    entry1 = new TreeMap<Integer, PlayerTroop>();
                }
                entry1.put(pt.getTroopId(), pt);
                troops.put(pt.getUserId(), entry1);

            }
            //Add Troops
            bd.setTroops(troops);
            //Search for PlayerPlanet on ppList (from Update)
            for (PlayerPlanet pp : ppList) {
                // DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "" + pp.getPlanetId());
                if (pp.getPlanetId().equals(gc.getPlanetId())) {
                    bd.setPlayerPlanet(pp);
                    break;
                }
            }
            if (bd.getPlayerPlanet() == null) {
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "A planet does not exist for this combat : PlanetId " + gc.getPlanetId() + " in PlayerPlanet : " + Service.playerPlanetDAO.findByPlanetId(gc.getPlanetId()));
            }

            //Add all Territory Entries
            ArrayList<TerritoryEntry> allTerritoryEntries = territoryEntryDAO.findAll();
            bd.setTerritoryEntry(allTerritoryEntries);

            /*================
             * ## Start processing round
             * ================
             */
            //Process Round Test = false
            bd = processRound(bd, removeObjects, false);


            /*================
             * ## End processing round save to DATABAZE
             * ================
             */
            //Processing done - Read Battledata and apply changes
            if (bd == null) {
                continue;
            }
            if (bd.getTerritoryEntry() != null && !bd.getTerritoryEntry().isEmpty()) {
                if (debug) {
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, " ===========> Adding : " + bd.getTerritoryEntry().size() + " Entries");
                }
                //Add Combatround
                territoryToSave.addAll(bd.getTerritoryEntry());
            } else {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, " ===========> Territoryentries are null : " + gc.getId() + " status : " + bd.getStatus());
                System.err.println("Errrr");
            }
            //Add Combatround
            roundsToSave.addAll(bd.getRoundEntry());

            //Update or delete Troops
            for (Map.Entry<Integer, TreeMap<Integer, PlayerTroop>> entry : bd.getTroops().entrySet()) {
                for (Map.Entry<Integer, PlayerTroop> entry2 : entry.getValue().entrySet()) {
                    if (entry2.getValue().getNumber() <= 0) {
                        playerTroopDAO.remove(entry2.getValue());
                    } else {
                        playerTroopDAO.update(entry2.getValue());
                    }
                }
            }

            //Set new GroundCombat Status
            gc.setCombatStatus(bd.getStatus());
            if (bd.getStatus().equals(ECombatStatus.FINISHED)) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "SSetting finishedxxxx");
                gc.setWinner(bd.getNewOwner());
                gc.setCombatStatus(ECombatStatus.FINISHED);
            }
            if (bd.getStatus().equals(ECombatStatus.LANDING) && bd.getCombatRounds().size() >= landingDuration) {
                gc.setWinner(bd.getNewOwner());
                gc.setCombatStatus(ECombatStatus.ONGOING);
            }
            groundCombatDAO.update(gc);

        }

        //Insert all new TerritoryEntries
        for (TerritoryEntry te : territoryToSave) {
            if (te.getTerritorySize() < 0.01d) {

                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Warning territory below 0.01: " + te.getTerritorySize());
                te.setTerritorySize(0.01d);
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Changed to: " + te.getTerritorySize());
            }
        }
        territoryEntryDAO.insertAll(territoryToSave);
        //Insert all new RoundEntries
        roundEntryDAO.insertAll(roundsToSave);
        DebugBuffer.addLine(DebugLevel.DEBUG, "Done Processing Ground combat in " + (System.currentTimeMillis() - time) + " ms");

        return removeObjects;
    }

    private static ArrayList<Integer> findUsersOnPlanet(int planetId) {

        //Searching all troops on planet
        ArrayList<PlayerTroop> pts = playerTroopDAO.findByPlanetId(planetId);

        //Searching all Users
        ArrayList<Integer> usersOnPlanet = new ArrayList<Integer>();
        for (PlayerTroop pt : pts) {
            if (!usersOnPlanet.contains(pt.getUserId())) {
                usersOnPlanet.add(pt.getUserId());
            }
        }
        return usersOnPlanet;
    }

    private static ArrayList<PlayerTroop> sortPlayerTroop(ArrayList<PlayerTroop> pts) {
        TreeMap<Integer, PlayerTroop> sorted = new TreeMap<Integer, PlayerTroop>();
        for (PlayerTroop pt : pts) {
            sorted.put(pt.getTroopId(), pt);
        }
        pts.clear();
        pts.addAll(sorted.values());

        return pts;

    }

    public static GroundCombatResult getReport(int combatId, int userId, EGroundCombatChart chartType) {
        GroundCombatResult gcr = new GroundCombatResult();
        GroundCombat gc = groundCombatDAO.findById(combatId);
        //Types of units
        TreeMap<Integer, ArrayList<Integer>> groundtroopTypes = new TreeMap<Integer, ArrayList<Integer>>();
        //Types of units total
        TreeMap<Integer, Integer> groundtroopTypesTotal = new TreeMap<Integer, Integer>();
        ArrayList<CombatRound> rounds = combatRoundDAO.findByCombatId(combatId);
        TreeMap<Integer, CombatRound> sorted = new TreeMap<Integer, CombatRound>();


        gcr.setUserId(userId);

        HashMap<Integer, HashMap<Integer, TerritoryEntry>> territoryUser = new HashMap<Integer, HashMap<Integer, TerritoryEntry>>();

        //Load territory entries and Combatrounts
        for (CombatRound cr : rounds) {
            gcr.setReport(gcr.getReport().append(cr.getReport()));
            sorted.put(cr.getId(), cr);
            ArrayList<TerritoryEntry> tes = territoryEntryDAO.findByCombatRoundId(cr.getId());

            HashMap<Integer, TerritoryEntry> terr = new HashMap<Integer, TerritoryEntry>();
            int count = 0;
            for (TerritoryEntry te : tes) {
                if (terr == null) {
                    terr = new HashMap<Integer, TerritoryEntry>();
                }
                terr.put(te.getUserId(), te);

                count++;
                if (count == tes.size()) {
                    territoryUser.put(te.getCombatRoundId(), terr);
                }


            }

        }

        //Set Territory
        gcr.setTerritoryUser(territoryUser);


        rounds.clear();
        rounds.addAll(sorted.values());
        //Set First Round
        CombatRound firstRound = rounds.get(0);
        //Set Last Round
        CombatRound lastRound = rounds.get(rounds.size() - 1);

        HashMap<Integer, ArrayList<RoundEntry>> roundEntries = new HashMap<Integer, ArrayList<RoundEntry>>();
        //Load Start Round Details
        for (RoundEntry re : roundEntryDAO.findByCombatRoundId(firstRound.getId())) {
            ArrayList<RoundEntry> entries = roundEntries.get(re.getUserId());
            if (entries == null) {
                entries = new ArrayList<RoundEntry>();
            }
            entries.add(re);
            roundEntries.put(re.getUserId(), entries);
        }
        //Set Start Round Details
        gcr.setStartUnits(roundEntries);

        roundEntries = new HashMap<Integer, ArrayList<RoundEntry>>();

        //Load End Round Details
        for (RoundEntry re : roundEntryDAO.findByCombatRoundId(lastRound.getId())) {
            ArrayList<RoundEntry> entries = roundEntries.get(re.getUserId());
            if (entries == null) {
                entries = new ArrayList<RoundEntry>();
            }
            entries.add(re);
            roundEntries.put(re.getUserId(), entries);
        }
        //Set End Round Units
        gcr.setEndUnits(roundEntries);

        //Load Combat History || RoundId || UserId || TroopId
        TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> history = new TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>>();

        for (Map.Entry<Integer, CombatRound> entry : sorted.entrySet()) {

            CombatRound cr = entry.getValue();

            for (RoundEntry re : roundEntryDAO.findByCombatRoundId(cr.getId())) {
                TreeMap<Integer, TreeMap<Integer, RoundEntry>> entries1 = history.get(entry.getKey());
                if (entries1 == null) {
                    entries1 = new TreeMap<Integer, TreeMap<Integer, RoundEntry>>();
                }


                TreeMap<Integer, RoundEntry> rEntries = entries1.get(re.getUserId());
                if (rEntries == null) {
                    rEntries = new TreeMap<Integer, RoundEntry>();
                }

                rEntries.put(re.getTroopId(), re);

                entries1.put(re.getUserId(), rEntries);
                history.put(cr.getId(), entries1);
            }
        }
        TreeMap<Integer, TreeMap<Integer, RoundEntry>> result = new TreeMap<Integer, TreeMap<Integer, RoundEntry>>();
        int round = 1;

        for (Map.Entry<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> roundEntry : history.entrySet()) {
            //If Groundtroop-Type not existing
            ArrayList<Integer> types = groundtroopTypes.get(roundEntry.getKey());
            if (types == null) {
                types = new ArrayList<Integer>();
            }
            for (Map.Entry<Integer, TreeMap<Integer, RoundEntry>> userEntry : roundEntry.getValue().entrySet()) {
                TreeMap<Integer, RoundEntry> tmpUserEntry = result.get(userEntry.getKey());
                //If user not found
                //Create the FIRST ROUND
                if (tmpUserEntry == null) {
                    tmpUserEntry = new TreeMap<Integer, RoundEntry>();
                    for (Map.Entry<Integer, RoundEntry> groundTroopEntry : userEntry.getValue().entrySet()) {
                        //If GroundTrooptype not yet existing
                        if (!groundtroopTypesTotal.containsKey(groundTroopEntry.getKey())) {
                            groundtroopTypesTotal.put(groundTroopEntry.getKey(), groundTroopEntry.getKey());
                        }
                        //If GroundTrooptype not yet existing
                        if (!types.contains(groundTroopEntry.getKey())) {
                            types.add(groundTroopEntry.getKey());
                        }
                        if (history.get(roundEntry.getKey() + 1) != null && history.get(roundEntry.getKey() + 1).get(userEntry.getKey()) != null && history.get(roundEntry.getKey() + 1).get(userEntry.getKey()).get(groundTroopEntry.getKey()) != null) {

                            tmpUserEntry.put(groundTroopEntry.getKey(), groundTroopEntry.getValue().clone());

                        } else {
                            RoundEntry newEntry = groundTroopEntry.getValue().clone();
                            long number = newEntry.getNumber();
                            newEntry.setNumber(newEntry.getNumber() + newEntry.getDead());
                            newEntry.setDead(newEntry.getDead());
                            tmpUserEntry.put(groundTroopEntry.getKey(), newEntry);

                        }
                    }
                    //If user found
                    //Load OLD Values
                } else {
                    //      //dbg log.debug("Entry for User Found");
                    for (Map.Entry<Integer, RoundEntry> groundTroopEntry : userEntry.getValue().entrySet()) {
                        if (!groundtroopTypesTotal.containsKey(groundTroopEntry.getKey())) {
                            groundtroopTypesTotal.put(groundTroopEntry.getKey(), groundTroopEntry.getKey());
                        }
                        if (!types.contains(groundTroopEntry.getKey())) {
                            types.add(groundTroopEntry.getKey());
                        }
                        if (tmpUserEntry.get(groundTroopEntry.getKey()) == null) {
                            //No old entry found (New UnitType arrived)
                            tmpUserEntry.put(groundTroopEntry.getKey(), groundTroopEntry.getValue().clone());
                        } else {
                            //Updating old entry
                            RoundEntry oldEntry = tmpUserEntry.get(groundTroopEntry.getKey());
                            RoundEntry newEntry = groundTroopEntry.getValue().clone();
                            if (newEntry.getNumber() < 0) {
                                newEntry.setNumber(0l);
                            }
                            if (history.get(roundEntry.getKey() + 1) != null && history.get(roundEntry.getKey() + 1).get(userEntry.getKey()) != null && history.get(roundEntry.getKey() + 1).get(userEntry.getKey()).get(groundTroopEntry.getKey()) != null) {

                                oldEntry.setNumber(oldEntry.getNumber() + (newEntry.getNumber() - oldEntry.getNumber() + oldEntry.getDead()));
                            } else {
                                oldEntry.setNumber(oldEntry.getNumber() + (newEntry.getNumber() - oldEntry.getNumber() + oldEntry.getDead() + newEntry.getDead()));
                            }
                            oldEntry.setDead(oldEntry.getDead() + newEntry.getDead());
                            tmpUserEntry.remove(groundTroopEntry.getKey());
                            tmpUserEntry.put(groundTroopEntry.getKey(), oldEntry);
                        }
                    }
                    //     //dbg log.debug("=======================");
                }
                result.put(userEntry.getKey(), tmpUserEntry);
            }
            groundtroopTypes.put(roundEntry.getKey(), types);
            // test(result);
            round++;
        }



        gcr.setParticipatingTypes(groundtroopTypes);
        ArrayList<Integer> parts = new ArrayList<Integer>();
        parts.addAll(groundtroopTypesTotal.values());
        gcr.setParticipatingTypesTotal(parts);
        gcr.setResult(result);
        gcr.setHistory(history);
        gcr.setStatus(gc.getCombatStatus());
        gcr.setStartTime(gc.getStartTime());
        gcr.setRounds(history.size());
        gcr.setPlanetId(gc.getPlanetId());
        if (gcr.getStatus() == ECombatStatus.FINISHED) {
            gcr.setWinner(gc.getWinner());
        }
        int planetOwner = 0;
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(gc.getPlanetId());
        if (pp != null) {
            planetOwner = pp.getUserId();
        }
        gcr.setPlanetOwner(planetOwner);

        //Chart
        appendChart(gcr, chartType);

        return gcr;


    }

    public static GroundCombatResult prepareBattleTest(Map<String, String[]> allPars) {
        GroundCombatResult gcr = new GroundCombatResult();
        BattleData bd = new BattleData();
        ArrayList<CombatPlayer> combatPlayers = new ArrayList<CombatPlayer>();
        HashMap<Integer, Boolean> diplomacy = new HashMap<Integer, Boolean>();
        HashMap<Integer, ArrayList<Integer>> groups = new HashMap<Integer, ArrayList<Integer>>();
        TreeMap<Integer, TreeMap<Integer, PlayerTroop>> troops = new TreeMap<Integer, TreeMap<Integer, PlayerTroop>>();
        for (Map.Entry<String, String[]> entry : allPars.entrySet()) {

            String key = entry.getKey();
            String value = entry.getValue()[0];
            if (key.startsWith("uId")) {
                int userId = Integer.parseInt(key.substring(3, 4));
                int troopId = Integer.parseInt(key.trim().substring(9));
                long number = Long.parseLong(value);
                if (number > 0) {
                    PlayerTroop pt = new PlayerTroop();
                    pt.setNumber(number);
                    pt.setUserId(userId);
                    pt.setPlanetId(-1);
                    pt.setTroopId(troopId);
                    TreeMap<Integer, PlayerTroop> troop = troops.get(userId);
                    if (troop == null) {
                        troop = new TreeMap<Integer, PlayerTroop>();
                    }
                    troop.put(pt.getTroopId(), pt);
                    troops.put(userId, troop);


                    boolean found = false;
                    for (CombatPlayer cp : combatPlayers) {
                        if (cp.getUserId() == userId) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        CombatPlayer cp = new CombatPlayer();
                        cp.setCombatId(1);
                        cp.setStartTime(0l);
                        cp.setUserId(userId);
                        combatPlayers.add(cp);
                    }
                }
            }

        }
        ArrayList<Integer> memberx = new ArrayList<Integer>();
        memberx.add(1);
        groups.put(1, memberx);
        for (Map.Entry<String, String[]> entry : allPars.entrySet()) {

            String key = entry.getKey();
            String value = entry.getValue()[0];

            if (key.startsWith("group")) {
                int userId = Integer.parseInt(key.substring(5));
                int group = Integer.parseInt(value);
                ArrayList<Integer> members = groups.get(group);
                if (members == null) {
                    members = new ArrayList<Integer>();
                }
                boolean found = false;
                for (CombatPlayer cp : combatPlayers) {
                    if (cp.getUserId() == userId) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    if (group != 1) {
                        diplomacy.put(userId, true);
                    } else {
                        diplomacy.put(userId, false);
                    }
                    members.add(userId);
                    groups.put(group, members);
                }
            }
        }
        TreeMap<Integer, User> users = new TreeMap<Integer, User>();
        User anton = new User();
        anton.setUserId(1);
        anton.setGameName("Anton");
        User bertha = new User();
        bertha.setUserId(2);
        bertha.setGameName("Bertha");
        User cesar = new User();
        cesar.setUserId(3);
        cesar.setGameName("Cesar");
        User dora = new User();
        dora.setUserId(4);
        dora.setGameName("Dora");

        users.put(anton.getUserId(), anton);
        users.put(bertha.getUserId(), bertha);
        users.put(cesar.getUserId(), cesar);
        users.put(dora.getUserId(), dora);


        bd.setUsers(users);

        bd.setCombatPlayers(combatPlayers);
        bd.setDiplomacy(diplomacy);
        bd.setTroops(troops);
        bd.setGroups(groups);
        bd.setStatus(ECombatStatus.ONGOING);
        bd.setPlanetOwner(1);
        bd.setSystemId(1);
        bd.setPlanetId(1);
        //Battle
        while (bd.getStatus() == ECombatStatus.ONGOING) {
            bd = processRound(bd, null, true);
        }
        gcr.setStartTime(0);
        gcr.setPlanetId(-1);
        gcr.setPlanetOwner(1);
        gcr.setRounds(bd.getCombatRounds().size());
        gcr.setWinner(0);

        TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> history = new TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>>();
        HashMap<Integer, HashMap<Integer, TerritoryEntry>> territoryUser = new HashMap<Integer, HashMap<Integer, TerritoryEntry>>();
        for (TerritoryEntry te : bd.getTerritoryEntry()) {
            HashMap<Integer, TerritoryEntry> entry = territoryUser.get(te.getCombatRoundId());
            if (entry == null) {
                entry = new HashMap<Integer, TerritoryEntry>();
            }
            entry.put(te.getUserId(), te);
            territoryUser.put(te.getCombatRoundId(), entry);
        }
        gcr.setTerritoryUser(territoryUser);
        int firstRound;
        int lastRound;
        ArrayList<CombatRound> comSorted = new ArrayList<CombatRound>();
        TreeMap<Integer, CombatRound> sorted = new TreeMap<Integer, CombatRound>();
        for (CombatRound cr : bd.getCombatRounds()) {
            sorted.put(cr.getRound(), cr);
        }
        comSorted.addAll(sorted.values());
        firstRound = comSorted.get(0).getId();
        lastRound = comSorted.get(comSorted.size() - 1).getId();

        for (RoundEntry re : bd.getRoundEntry()) {

            if (re.getCombatRoundId() == firstRound) {

                HashMap<Integer, ArrayList<RoundEntry>> entry = gcr.getStartUnits();
                if (entry == null) {
                    entry = new HashMap<Integer, ArrayList<RoundEntry>>();
                }
                ArrayList<RoundEntry> rounds = entry.get(re.getUserId());
                if (rounds == null) {
                    rounds = new ArrayList<RoundEntry>();
                }
                rounds.add(re);
                gcr.getStartUnits().put(re.getUserId(), rounds);
            }
            if (re.getCombatRoundId() == lastRound) {
                HashMap<Integer, ArrayList<RoundEntry>> entry = gcr.getEndUnits();
                if (entry == null) {
                    entry = new HashMap<Integer, ArrayList<RoundEntry>>();
                }
                ArrayList<RoundEntry> rounds = entry.get(re.getUserId());
                if (rounds == null) {
                    rounds = new ArrayList<RoundEntry>();
                }
                rounds.add(re);
                gcr.getEndUnits().put(re.getUserId(), rounds);
            }


            TreeMap<Integer, TreeMap<Integer, RoundEntry>> entry = history.get(re.getCombatRoundId());
            if (entry == null) {
                entry = new TreeMap<Integer, TreeMap<Integer, RoundEntry>>();
            }
            TreeMap<Integer, RoundEntry> rounds = entry.get(re.getUserId());
            if (rounds == null) {
                rounds = new TreeMap<Integer, RoundEntry>();
            }
            rounds.put(re.getTroopId(), re);
            entry.put(re.getUserId(), rounds);
            history.put(re.getCombatRoundId(), entry);

        }
        gcr.setCombatRounds(bd.getCombatRounds());
        gcr.setStatus(bd.getStatus());
        gcr.setHistory(history);
        gcr.setReport(bd.getReport());
        gcr.setWinner(bd.getNewOwner());
        gcr.setUsers(bd.getUsers());
        return gcr;

    }

    /* !!!!!!!! This function calculates the ground combat !!!!!!!!
     *
     *
     */
    public static BattleData processRound(BattleData bd, ArrayList<Model> removeObjects, boolean test) {
        if (debug) {
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "####################");
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Start processing round !");

        }
        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Start processing round" + (System.currentTimeMillis() - time) + " ms");
        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Start processing round planet " + bd.getPlanetOwner());
        //Describes the total Hitpoints of the Groups and Users
        HashMap<Integer, Long> totalHitpointsPerGroup = new HashMap<Integer, Long>();
        HashMap<Integer, Long> totalHitpointsPerUser = new HashMap<Integer, Long>();
        //Territoryvalues

        Territory_R10 territory = new Territory_R10();

        //Describes the minimum(1%) and maximum(99% - 1%*participants) Territory of Groups and Users

        //load Users
        TreeMap<Integer, User> users = bd.getUsers();
        //load CombatPlayers
        ArrayList<CombatPlayer> combatPlayers = bd.getCombatPlayers();

        Long combatHitpoints = 0l;
        //Load Troops
        TreeMap<Integer, TreeMap<Integer, PlayerTroop>> troops = bd.getTroops();
        HashMap<Integer, HashMap<Integer, RoundEntry>> deadTroops = new HashMap<Integer, HashMap<Integer, RoundEntry>>();

        StringBuilder report = new StringBuilder();


        //Load lastround
        CombatRound lastRound = bd.getLastRound();
        int planetUserId = bd.getPlanetOwner();

        //Load Groups
        HashMap<Integer, ArrayList<Integer>> groups = bd.getGroups();

        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "groups " + groups.size());
        // ========================================================================
        // Combat ended determine winner from suriving groups
        // If planetowners allied(?) group is last one 
        // Defender has won
        // ========================================================================
        if (groups.size() <= 1) {
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "groupsize <= 1 ");
            // no combat anymore
            boolean ownerStays = false;
            if (groups.size() == 1) {
                ArrayList<Integer> survivingGroup = groups.get(0);
                if (survivingGroup.contains(planetUserId)) {
                    ownerStays = true;
                }
            }

            ownerStays = ownerStays || groups.isEmpty();

            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Owner stays = " + ownerStays);
            //Owner Stays on Planet finish GroundCombat
            if (ownerStays) {
                if (debug) {
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Owner stays");

                }
                if (!test) {
                    //Finish Groundcombat b4 fighting
                    GroundCombat gc = groundCombatDAO.findById(bd.getGroundCombatId());
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "SSetting finished1");
                    sendDefendedMessage(bd.getGroundCombatId(), bd.getPlayerPlanet(), planetUserId, bd.getPlanetId());
                    /*
                     *
                     * TITLE
                     *
                     */

                    try {
                        if (bd.getCombatRounds().size() >= 20) {
                            if (bd.getPlayerPlanet().isHomeSystem()) {
                                TitleUtilities.incrementCondition(ConditionToTitle.INVINCIBLE, planetUserId);
                            }
                        }
                    } catch (Exception e) {
                        DebugBuffer.writeStackTrace("Error in GroundCombatutilities while increment TitleCondition : ", e);
                    }
                    gc.setWinner(planetUserId);
                    gc.setCombatStatus(ECombatStatus.FINISHED);
                    groundCombatDAO.update(gc);
                }
                return null;
                //Enemy is on planet, search for user with biggest territory
            } else {
                ArrayList<Integer> winners = groups.get(0);
                double territoryValue = 0d;
                int userWithMostTerritory = winners.get(0);
                for (Integer user : winners) {
                    double userTerritory = 0d;
                    for (TerritoryEntry tmp : bd.getTerritoryEntry()) {
                        if (tmp.getCombatRoundId().equals(lastRound.getId())
                                && tmp.getUserId().equals(user)) {
                            if (tmp.getTerritorySize() > userTerritory) {
                                userTerritory = tmp.getTerritorySize();
                            }
                        }
                    }
                    if (userTerritory > territoryValue) {
                        territoryValue = userTerritory;
                        userWithMostTerritory = user;
                    }
                }
                if (debug) {
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "User with most territory : " + userDAO.findById(userWithMostTerritory).getGameName());

                }
                if (userWithMostTerritory == 0) {
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "UserId with most territory is 0");

                }

                ArrayList<Model> actRemoveObjects = PlanetUtilities.movePlanetToPlayer(userWithMostTerritory, bd.getPlanetId(), bd.getPlayerPlanet(), bd.getGroundCombatId(), false);
                if (removeObjects != null) {
                    removeObjects.addAll(actRemoveObjects);
                }
            }
        // ===============================================================
        // Start calculation of current combat group
        // ===============================================================
        } else {
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "groupsize > 1");

            //Load TerritoryValues
            bd = loadTerritoryEntries(bd, lastRound, groups);

            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Done loading Territory");
            for (Map.Entry<Integer, ArrayList<Integer>> cg : groups.entrySet()) {

                //For Each player calculate Territory and Hitpoints
                for (Integer uId : cg.getValue()) {
                    calculateTerritoryBoundaries(bd, lastRound, cg, uId, planetUserId, groups, combatPlayers, territory);


                    if (troops.get(uId) != null) {
                        combatHitpoints = calculateHitpoints(troops, cg.getKey(), uId, test, combatHitpoints, totalHitpointsPerUser, totalHitpointsPerGroup);
                    }
                }



            }

            /*
             *###########
             *## BATTLE #
             *###########
             */

            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Start fighting" + (System.currentTimeMillis() - time) + " ms");
            }
            CombatRound newRound = new CombatRound();
            // Set to round 1 if it is the first combat round
            if (lastRound == null) {
                newRound.setRound(1); 
                if (test) {
                    newRound.setId(1);
                }
            // Combat was already in progress
            } else {
                newRound.setRound(lastRound.getRound() + 1);
                if (test) {
                    newRound.setId(lastRound.getRound() + 1);
                }
            }
            // If test combatround id = 1
            if (test) {
                newRound.setCombatId(1);
                //get from db
            } else {
                newRound.setCombatId(bd.getGroundCombatId());
                newRound = combatRoundDAO.add(newRound);
            }

            // Add current Combat Round
            bd.getCombatRounds().add(newRound);

            float doomfactor = 2.5f;

            double doomMulti = (double) (1f / Math.log10((Math.round(combatHitpoints))));

            if (doomMulti == 0 || Double.isInfinite(doomMulti)) {
                doomMulti = 1f;
            }
            doomfactor *= doomMulti;

            doomfactor += newRound.getRound() / 10d;

            // Determine current state of battle
            // -- Do we have Landing phase? Landingphase continues till 10% of territory is captured
            // Get enemy combat groups 
            double accumulatedEnemyTerritory = 0d;
            int alliedAirForce = 0;
            int enemyAirForce = 0;
            int enemyBombers = 0;
            int alliedAntiAir = 0;           
            
            HashMap<Integer,ArrayList<Integer>> tmpGroups = bd.getGroups();
            for (Integer grpId : tmpGroups.keySet()) {
                ArrayList<Integer> playerList = tmpGroups.get(grpId);
                for (Integer pId : playerList) {
                    if (!DiplomacyUtilities.hasAllianceRelation(planetUserId, pId)) {
                        ArrayList<TerritoryEntry> territories = bd.getTerritoryEntry();
                        for (TerritoryEntry te : territories) {
                            if (te.getUserId().equals(pId)) {
                                accumulatedEnemyTerritory += te.getTerritorySize();
                            }
                        }
                        
                        // Sum up enemy aircraft
                        enemyAirForce += troops.get(pId).get(GroundTroop.ID_INTERCEPTORPLANE).getNumber();
                        enemyBombers += troops.get(pId).get(GroundTroop.ID_BOMBER).getNumber();
                    } else {
                        // Sum up allied aircraft
                        // Sum up allied antiair
                        alliedAntiAir += troops.get(pId).get(GroundTroop.ID_ROCKETTANK).getNumber();
                        alliedAirForce += troops.get(pId).get(GroundTroop.ID_INTERCEPTORPLANE).getNumber();
                    }
                }
            }
            
            // 
            if (accumulatedEnemyTerritory < 10d) {
                
            }
            
            // -- During Landingphase bunkers/anti-aircraft vehicles/interceptors cause higher cascualties on attackers
            // Determine number of bunkers, AA and Interceptors on defender -- rate with 
            int bunkerCnt = 0;
            PlanetConstruction pc = Service.planetConstructionDAO.findBy(bd.getPlayerPlanet().getPlanetId(), planetUserId);
            if (pc != null) {
                bunkerCnt = pc.getNumber();
            }
            
            double landingSpeedModifierDefense = 1d;
            
            // -- Landingphase duration is dependant on colony size / planetside and available troops
            // -1 maxmimum bonus for attacker / +1 maximum bonus for defender
            // influenced by landratio and troopratio 
            // planetfill gives additional bonus for miliz            
            // Determine planetsize, population/planetfill and troop strength
            PlayerPlanet pp = bd.getPlayerPlanet();
            Planet p = Service.planetDAO.findById(pp.getPlanetId());
            
            double planetFill = 100d / (double)p.getMaxPopulation() * (double)pp.getPopulation();
            
            double landingSpeedModifierPopDensity = 1d;
            
            // -- Bunkers increase hitpoints of defending infantry (percentage depending on ratio infantry/bunker count)
            double infantryBonusDef = 1d;
            
            // -- Bunkers maybe destroyed during combat
            // If enemy has a very high number of bombers / tanks destruction chance for bunkers can go up to 10%
            double airSuperiorityAttFactor = 1d;
            double tankSuperiorityAttFactor = 1d;
            
            // -- Infantry only battle (i.e. Transmitterpyramids)
            double incInfantryHitpointsPerc = 0d;
            double incDamageToAttackerPerc = 0d;
            
            
            //Looping AttackerGroup
            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Start looping groups " + (System.currentTimeMillis() - time) + " ms");
            }

            for (Map.Entry<Integer, ArrayList<Integer>> attGroup : groups.entrySet()) {

                //Looping Group-Users
                for (Integer uId : attGroup.getValue()) {

                    //Loading attackbonus = territoryroun
                    double attBonus = territory.territoryUser.get(uId);

                    //Looping Attacking units
                    if (debug) {
                        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Start Looping players " + (System.currentTimeMillis() - time) + " ms");
                    }
                    if (troops.get(uId) != null) {

                        //Looping Troops
                        for (Map.Entry<Integer, PlayerTroop> troopEntry : troops.get(uId).entrySet()) {

                            PlayerTroop pt = troopEntry.getValue();

                            if (pt.getNumber() <= 0) {
                                continue;
                            }
                            //Getting total Attackprob to calculate standardised factor for actual enemy troop
                            float totalAttackProb = gtRelationDAO.findTotalAttackprob(pt.getTroopId());
                            if (debug) {
                                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "_ATTACK_Troop: " + pt.getTroopId() + " | " + ML.getMLStr(groundTroopDAO.findById(pt.getTroopId()).getName(), uId));
                                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "__________Number: " + pt.getNumber());
                            }

                            //Looping attackrelations of attacking unit
                            for (GTRelation gtrel : gtRelationDAO.findBySrcIdFake(pt.getTroopId())) {
                                //Constructions
                                if (gtrel.getTargetId() == 99999) {
                                    continue;
                                }
                                //Adjust till you are happy
                                //Round-Multiplicator
                                float multiplicator = (float) Math.ceil(((float) newRound.getRound() / 10f));

                                //Standardised attackprobability * Doomfactor
                                float standardisedAttackProb = gtrel.getAttackProbab() / totalAttackProb * doomfactor;
                                //Damage without attackProbability
                                float hitProbDamage = gtrel.getAttackPoints() * multiplicator * pt.getNumber() * gtrel.getHitProbab();
                                //Total damage
                                float totalDamage = hitProbDamage * standardisedAttackProb;
                                double totalBonusDamage = Math.round(totalDamage * attBonus);
                                totalDamage += totalBonusDamage;
                                //search for this unit in enemy groups
                                long troopsTotal = 0l;
                                //Units of this Type per User
                                HashMap<Integer, Long> userUnits = new HashMap<Integer, Long>();
                                HashMap<Integer, Integer> userToGroup = new HashMap<Integer, Integer>();
                                //looping enemy groups
                                for (Map.Entry<Integer, ArrayList<Integer>> defGroup : groups.entrySet()) {

                                    if (defGroup.getKey().equals(attGroup.getKey())) {
                                        //Found own battlegroup => continue
                                        continue;
                                    }

                                    //looping enemy users summing ALL troops of this kind
                                    for (Integer userId : defGroup.getValue()) {
                                        if (troops.get(userId) == null) {
                                            continue;
                                        }

                                        PlayerTroop ptTmp = troops.get(userId).get(gtrel.getTargetId());
                                        if (ptTmp == null) {
                                            continue;
                                        }
                                        troopsTotal += ptTmp.getNumber();
                                        userUnits.put(userId, ptTmp.getNumber());
                                        userToGroup.put(userId, defGroup.getKey());
                                    }
                                }
                                //if troops of this kind exist
                                if (troopsTotal > 0) {


                                    // Deal damage to each user
                                    for (Map.Entry<Integer, Long> number : userUnits.entrySet()) {
                                        //Divide TotalTroops/UserTroops to get normalized damage
                                        double damageDealt = troopsTotal / number.getValue() * (totalDamage - totalBonusDamage);
                                        double bonusDealt = troopsTotal / number.getValue() * totalBonusDamage;
                                        damageDealt += bonusDealt;
                                        //Get Relation from Database
                                        GroundTroop enemyGroundTroop;
                                        if (!test) {
                                            enemyGroundTroop = groundTroopDAO.findById(gtrel.getTargetId(), number.getKey());
                                        } else {
                                            enemyGroundTroop = groundTroopDAO.findById(gtrel.getTargetId());
                                        }

                                        double totalHitpoints = number.getValue() * enemyGroundTroop.getHitpoints();
                                        /*
                                         double factor = 10d - Math.log10(combatHitpoints);
                                         if (factor <= 0) {
                                         factor = 1;
                                         }
                                         */
                                        //Calculate a maximum of dead units

                                        //Todo percentage of units
                                        int maxDead = (int) Math.round(number.getValue() * ((12d - Math.log10(combatHitpoints))) / 10d);

                                        int survivingTroops = (int) (Math.ceil((totalHitpoints - damageDealt) / enemyGroundTroop.getHitpoints()));
                                        double hitPointDifference = totalHitpointsPerGroup.get(attGroup.getKey()) / totalHitpointsPerGroup.get(userToGroup.get(number.getKey()));

                                        if (maxDead <= 0 || territory.territoryUser.get(number.getKey()) <= 0.05d || hitPointDifference > 1000d) {
                                            maxDead = (int) Math.round(number.getValue());
                                        }


                                        if (survivingTroops < number.getValue() - maxDead) {
                                            survivingTroops = (int) (number.getValue() - maxDead);
                                        }
                                        if (survivingTroops < 0) {
                                            survivingTroops = 0;
                                        } else {
                                            //Troops still alive
                                        }
                                        int deadTroop = (int) Math.floor((number.getValue() - survivingTroops));

                                        /*
                                         *
                                         * TITLE
                                         *
                                         */

                                        try {
                                            TitleUtilities.incrementCondition(ConditionToTitle.SLAUGHTERER, uId, deadTroop);
                                        } catch (Exception e) {
                                            DebugBuffer.writeStackTrace("Error in GroundCombatutilities while increment TitleCondition : ", e);
                                        }
                                        //Dead Troops
                                        HashMap<Integer, RoundEntry> deadTroopEntry = deadTroops.get(number.getKey());

                                        if (deadTroopEntry == null) {
                                            deadTroopEntry = new HashMap<Integer, RoundEntry>();
                                        }

                                        RoundEntry re = deadTroopEntry.get(gtrel.getTargetId());
                                        if (re == null) {
                                            re = new RoundEntry();
                                            re.setCombatRoundId(newRound.getId());
                                            re.setDead((long) deadTroop);
                                            re.setTroopId(gtrel.getTargetId());
                                            re.setUserId(number.getKey());
                                        } else {
                                            re.setDead(re.getDead() + (long) deadTroop);
                                        }

                                        for (RoundEntry rx : bd.getRoundEntry()) {
                                            if (rx.getCombatRoundId().equals(re.getCombatRoundId())
                                                    && rx.getUserId().equals(re.getUserId())
                                                    && rx.getTroopId().equals(re.getTroopId())) {
                                                bd.getRoundEntry().remove(rx);
                                                break;
                                            }
                                        }
                                        bd.getRoundEntry().add(re);
                                        deadTroopEntry.put(gtrel.getTargetId(), re);
                                        deadTroops.put(number.getKey(), deadTroopEntry);
                                    }
                                }


                            }

                        }
                    }
                }
            }
            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Aftermath " + (System.currentTimeMillis() - time) + " ms");
            }

            //List of surviving Players
            ArrayList<Integer> survivingPlayers = new ArrayList<Integer>();

            //Writing dead units to db
            for (Map.Entry<Integer, HashMap<Integer, RoundEntry>> entry : deadTroops.entrySet()) {

                for (Map.Entry<Integer, RoundEntry> entry2 : entry.getValue().entrySet()) {
                    RoundEntry re = entry2.getValue();
                    PlayerTroop pt = troops.get(entry.getKey()).get(re.getTroopId());
                    long number = pt.getNumber();
                    //If Miliz Reduce Population
                    if (re.getTroopId() == GroundTroop.ID_MILIZ) {
                        bd.getPlayerPlanet().setPopulation(bd.getPlayerPlanet().getPopulation() - re.getDead());
                    }
                    pt.setNumber(number - re.getDead());
                    re.setNumber(number - re.getDead());
                    if (pt.getNumber() <= 0) {
                    } else {
                        if (!survivingPlayers.contains(pt.getUserId()));
                        survivingPlayers.add(pt.getUserId());
                    }
                }

            }
            //Surviving Groups
            ArrayList<Integer> survivingGroups = new ArrayList<Integer>();
            boolean isEnemy = false;
            HashMap<Integer, ArrayList<Integer>> toRemove = new HashMap<Integer, ArrayList<Integer>>();
            //search for all dead users/groups
            for (Map.Entry<Integer, ArrayList<Integer>> group : groups.entrySet()) {
                ArrayList<Integer> groupUsers = group.getValue();
                for (Integer uId : groupUsers) {

                    if (survivingPlayers.contains(uId)) {
                        //Player is still alive

                        if (!isEnemy) {
                            if (test) {
                                if (bd.getDiplomacy().get(uId) != null && bd.getDiplomacy().get(uId).booleanValue()) {
                                    isEnemy = true;
                                }
                            } else {
                                ArrayList<Integer> uIdTest = new ArrayList<Integer>();
                                uIdTest.add(planetUserId);
                                uIdTest.add(uId);
                                HashMap<Integer, ArrayList<Integer>> res = CombatGroupResolver.getCombatGroups(uIdTest, new RelativeCoordinate(bd.getSystemId(), bd.getPlanetId())).getAsMap();

                                if (res.size() > 1) {
                                    isEnemy = true;
                                }
                            }
                        }
                        //Building group of surviving players
                        if (!survivingGroups.contains(group.getKey())) {
                            survivingGroups.add(group.getKey());

                        }

                    } else {
                        ArrayList<Integer> toR = toRemove.get(group.getKey());
                        if (toR == null) {
                            toR = new ArrayList<Integer>();
                        }
                        if (!toR.contains(uId)) {
                            toR.add(uId);
                        }
                        toRemove.put(group.getKey(), toR);


                    }
                }

            }
            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Calc territory change " + (System.currentTimeMillis() - time) + " ms");
            }

            //Calculate Group Changes
            calculateGroupTerritoryChanges(groups, territory, totalHitpointsPerUser, totalHitpointsPerGroup);

            bd.getTerritoryEntry().clear();
            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Write territory changes " + (System.currentTimeMillis() - time) + " ms");
            }

            //Calculate User Changes
            calculateUserTerritoryChanges(bd, newRound, groups, territory, totalHitpointsPerUser, totalHitpointsPerGroup);
            //deleting all dead users/groups
            for (Map.Entry<Integer, ArrayList<Integer>> toR : toRemove.entrySet()) {
                for (Integer uId : toR.getValue()) {
                    groups.get(toR.getKey()).remove(uId);
                    if (groups.get(toR.getKey()).isEmpty()) {
                        groups.remove(toR.getKey());
                    }
                    for (CombatPlayer cp : bd.getCombatPlayers()) {
                        if (cp.getUserId().equals(uId)) {
                            bd.getCombatPlayers().remove(cp);
                            break;
                        }
                    }
                }
            }
            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Check if finished " + (System.currentTimeMillis() - time) + " ms");
            }
            //Battle finished
            if (survivingGroups.size() <= 1) {
                boolean hasEnemySurived = false;
                ArrayList<Integer> uIdTest = new ArrayList<Integer>();
                //Add planetOwner
                uIdTest.add(planetUserId);
                //Add surviving players
                for (Integer i : survivingPlayers) {
                    if (!uIdTest.contains(i)) {
                        uIdTest.add(i);
                    }
                }
                //Resolve
                HashMap<Integer, ArrayList<Integer>> resolvedGroups = CombatGroupResolver.getCombatGroups(uIdTest, new RelativeCoordinate(bd.getSystemId(), bd.getPlanetId())).getAsMap();
                //True = EnemyGroup existing
                if (resolvedGroups.size() > 1) {
                    hasEnemySurived = true;
                }
                bd.setStatus(ECombatStatus.FINISHED);
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "SSetting finished2");
                if (hasEnemySurived) {

                    ArrayList<Integer> surviving = groups.get(survivingGroups.get(0));
                    double territoryValue = 0;
                    int mostId = 0;

                    for (Integer i : surviving) {
                        if (territory.territoryUser.get(i) > territoryValue) {
                            territoryValue = territory.territoryUser.get(i);
                            mostId = i;
                        }
                    }

                    bd.setNewOwner(mostId);
                    if (mostId == 0) {
                        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Most Id = 0");

                    }
                    if (!test) {

                        ArrayList<Model> actRemoveObjects = PlanetUtilities.movePlanetToPlayer(mostId, bd.getPlanetId(), bd.getPlayerPlanet(), bd.getGroundCombatId(),false);
                        if (removeObjects != null) {
                            removeObjects.addAll(actRemoveObjects);
                        }
                        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "moved planet to new owner " + (System.currentTimeMillis() - time) + " ms");

                    }
                } else {
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "keep Owner");
                    bd.setNewOwner(planetUserId);

                    /*
                     *
                     * TITLE
                     *
                     */

                    try {
                        if (bd.getCombatRounds().size() >= 20) {
                            if (bd.getPlayerPlanet().isHomeSystem()) {
                                TitleUtilities.incrementCondition(ConditionToTitle.INVINCIBLE, planetUserId);
                            }
                        }
                    } catch (Exception e) {
                        DebugBuffer.writeStackTrace("Error in GroundCombatutilities while increment TitleCondition : ", e);
                    }
                    sendDefendedMessage(bd.getGroundCombatId(), bd.getPlayerPlanet(), planetUserId, bd.getPlanetId());
                }

            }

            report.append("</TABLE>");
            newRound.setReport("");
            //     newRound.setReport(report.toString());
            if (!test) {

                combatRoundDAO.update(newRound);


            }
        }
        bd.setReport(new StringBuffer());
        //     bd.setReport(bd.getReport().append(report));
        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "End process round " + (System.currentTimeMillis() - time) + " ms");

        return bd;

    }

    /*
    @Deprecated
    private static ArrayList<Model> movePlanetToPlayer(int newOwner, int planetId, PlayerPlanet pp, int groundCombatId) {
        ArrayList<Model> removeObjects = new ArrayList<Model>();

        DebugBuffer.addLine(DebugLevel.TRACE, "The planet " + planetId + " is taken over by " + userDAO.findById(newOwner).getGameName());
        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "The planet " + planetId + " is taken over by " + userDAO.findById(newOwner).getGameName());
        if (newOwner == pp.getUserId()) {
            DebugBuffer.addLine(DebugLevel.ERROR, "GroundcombatId : " + groundCombatId + " planetId " + pp.getPlanetId() + " NEW USER IS OLD USER THIS SHOULD NOT OCCUR " + newOwner);
            return removeObjects;
        }
        try {
            int oldUser = pp.getUserId();

            /*
             *
             * TITLE
             *
             */

             /*
            try {
                if (pp.isHomeSystem()) {
                    TitleUtilities.incrementCondition(ConditionToTitle.CONQUERER, newOwner);
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in GroundCombatutilities while increment TitleCondition : ", e);
            }
            try {
                TitleUtilities.incrementCondition(ConditionToTitle.STINKER, oldUser);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in GroundCombatutilities while increment TitleCondition : ", e);
            }

            //Delete possible Notifications
            try {
                NotificationBuffer.removePONotification(oldUser, planetId);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while deleting possible Notifications : userId " + oldUser + " planetId : " + planetId, e);
            }

            //Deleting TradepostEntry
            try {
                PlanetConstruction pc = planetConstructionDAO.findBy(planetId, Construction.ID_INTERSTELLARTRADINGPOST);
                if (pc != null) {
                    TradePost tp = Service.tradePostDAO.findByPlanetId(planetId);
                    if (tp != null) {
                        TradePost tpNew = new TradePost();
                        tpNew.setName(tp.getName());
                        tpNew.setPlanetId(tp.getPlanetId());
                        tpNew.setTransportPerLJ(tp.getTransportPerLJ());
                        Service.tradePostDAO.remove(tp);
                        Service.tradePostDAO.add(tp);
                    }
                }


            } catch (Exception e) {
                DebugBuffer.error("Error while deleting possible TradepostEntry : userId " + oldUser + " planetId : " + planetId);

            }

            //Deleting Traderoutes
            ArrayList<TradeRoute> tradeRoutes = tradeRouteDAO.findByStartPlanet(planetId);
            tradeRoutes.addAll(tradeRouteDAO.findByEndPlanet(planetId));
            for (TradeRoute tr : tradeRoutes) {
                log.info( "Marking route " + tr.getId() + " for deletion! (Due to invasion)");
                removeObjects.add(tr);
                // TransportService.deleteRoute(tr.getUserId(), tr.getId());
            }

            //Updating Planets in Observatory range
            boolean hasObservatory = planetConstructionDAO.isConstructed(planetId, Construction.ID_OBSERVATORY);
            Observatory o = new Observatory();
            if (hasObservatory) {
                //Delete for existing User
                o.onDeconstruction(pp);
            }
            //Change planet
            pp.setHomeSystem(false);
            pp.setUserId(newOwner);
            pp.setTax(37);
            playerPlanetDAO.update(pp);
            for (PlanetCategory pc : planetCategoryDAO.findAllByPlanetId(planetId)) {
                planetCategoryDAO.remove(pc);
            }

            PlanetLoyality pLoy = plDAO.getByPlanetAndUserId(pp.getPlanetId(), newOwner);
            if (pLoy == null) {
                pLoy = new PlanetLoyality();
                pLoy.setPlanetId(pp.getPlanetId());
                pLoy.setUserId(newOwner);
                pLoy.setValue(0d);
                plDAO.add(pLoy);
            }

            //Observatory after planet changed
            if (hasObservatory) {
                //Establish Entries for new Owner
                o.onFinishing(pp);
            }

            HeaderBuffer.reloadUser(newOwner);
            HeaderBuffer.reloadUser(oldUser);


            //Remove Groundtroop Orders
            for (Action a : actionDAO.findByPlanet(planetId)) {
                if (a.getType().equals(EActionType.GROUNDTROOPS)) {
                    ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                    removeObjects.add(po);
                    // productionOrderDAO.remove(po);
                }
                // actionDAO.remove(a);
            }

            //Remove Ship Orders
            for (Action a : actionDAO.findByPlanet(planetId)) {
                if (a.getType().equals(EActionType.SHIP)) {
                    ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                    removeObjects.add(po);
                    // productionOrderDAO.remove(po);
                }
                // actionDAO.remove(a);
            }            
            
            //Remove Spacestation Orders
            for (Action a : actionDAO.findByPlanet(planetId)) {
                if (a.getType().equals(EActionType.SPACESTATION)) {
                    ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                    removeObjects.add(po);
                    // productionOrderDAO.remove(po);
                }
                // actionDAO.remove(a);
            }                        
            
            //Create PlanetLog Entry
            {
                PlanetLog pl = new PlanetLog();
                pl.setPlanetId(planetId);
                pl.setTime(GameUtilities.getCurrentTick2());
                pl.setUserId(newOwner);
                pl.setType(EPlanetLogType.INVADED);

                planetLogDAO.add(pl);
            }
            GenerateMessage looser = new GenerateMessage();
            GenerateMessage winner = new GenerateMessage();

            int currTick = GameUtilities.getCurrentTick2();
            RelativeCoordinate rc = new RelativeCoordinate(0, pp.getPlanetId());
            ViewTableUtilities.addOrRefreshSystem(oldUser, rc.getSystemId(), currTick);
            ViewTableUtilities.addOrRefreshSystem(newOwner, rc.getSystemId(), currTick);

            ArrayList<Integer> participants = new ArrayList<Integer>();
            participants.add(oldUser);
            participants.add(newOwner);

            //There was a battle and there are probably more users involved
            if (groundCombatId > 0) {
                for (CombatPlayer cp : combatPlayerDAO.findByCombatId(groundCombatId)) {
                    if (!participants.contains(cp.getUserId())) {
                        participants.add(cp.getUserId());
                    }
                }
            }

            CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(participants, new RelativeCoordinate(0, planetId));

            for (Map.Entry<Integer, ArrayList<Integer>> entry : cgr.getAsMap().entrySet()) {
                for (Integer i : entry.getValue()) {
                    //Loosers
                    if (entry.getValue().contains(oldUser)) {
                        looser.setDestinationUserId(i);
                        looser.setSourceUserId(userDAO.getSystemUser().getUserId());
                        looser.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
                        looser.setMsg(ML.getMLStr("fleetmanagement_msg_invasionlostplanet", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(newOwner).getGameName()));
                        looser.setMessageType(EMessageType.SYSTEM);
                        looser.setMessageContentType(EMessageContentType.COMBAT);
                        looser.writeMessageToUser();
                        //Winners
                    } else {
                        // Add helping players so planet can be given to them
                        if (i != newOwner) {
                            PlanetLog pl = new PlanetLog();
                            pl.setPlanetId(pp.getPlanetId());
                            pl.setTime(GameUtilities.getCurrentTick2());
                            pl.setType(EPlanetLogType.INVADED_HELP);
                            pl.setUserId(i);
                            planetLogDAO.add(pl);
                        }
                        winner.setDestinationUserId(i);
                        winner.setSourceUserId(userDAO.getSystemUser().getUserId());
                        winner.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
                        winner.setMsg(ML.getMLStr("fleetmanagement_msg_invasionwon", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(oldUser).getGameName()));
                        winner.setMessageType(EMessageType.SYSTEM);
                        winner.setMessageContentType(EMessageContentType.COMBAT);
                        winner.writeMessageToUser();
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Groundcombat - Error while fight after moving planet : " + e);
            DebugBuffer.produceStackTrace(e);
        }

        //Attack Tradefleets if new are available
        try {
            ArrayList<PlayerFleet> pfs = playerFleetDAO.findByPlanetId(planetId);
            for (PlayerFleet pf : pfs) {
                PlayerFleetExt pfe = new PlayerFleetExt(pf);
                if (FleetService.canFight(pfe)) {

                    if (pfe.getRelativeCoordinate().getSystemId() == 0) {
                        DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Invoking Combat in GroundCombatUtilities with System 0");

                    } else {
                        CombatReportGenerator crg = new CombatReportGenerator();
                        if (pfe.getRelativeCoordinate().getPlanetId() == 0) {
                            CombatHandler_NEW ch = new CombatHandler_NEW(CombatType.SYSTEM_COMBAT, playerFleetDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
                            ch.processCombatResults();
                        } else {
                            CombatHandler_NEW ch = new CombatHandler_NEW(CombatType.ORBITAL_COMBAT, playerFleetDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
                            ch.processCombatResults();
                        }

                        crg.writeReportsToDatabase();
                        break;
                    }
                }
            }

        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error while setting new owner : ", e);
            return new ArrayList<Model>();
        }

        return removeObjects;
    }
    */

    public static void test(TreeMap<Integer, TreeMap<Integer, RoundEntry>> entries) {

        for (Map.Entry<Integer, TreeMap<Integer, RoundEntry>> startUnits : entries.entrySet()) {
            //dbg log.debug("========");
            //dbg log.debug("user : " + startUnits.getKey());
            for (Map.Entry<Integer, RoundEntry> ent : startUnits.getValue().entrySet()) {
                //dbg log.debug("troopId : " + ent.getKey() + " number : " + ent.getValue().getNumber() + " dead " + ent.getValue().getDead());
            }
        }
    }

    public static void checkForCombats(ArrayList<PlayerPlanet> ppList) {
        HashMap<Integer, ArrayList<Integer>> troops = new HashMap<Integer, ArrayList<Integer>>();
        for (PlayerTroop pt : (ArrayList<PlayerTroop>) playerTroopDAO.findAll()) {
            ArrayList<Integer> planetTroops = troops.get(pt.getPlanetId());
            if (planetTroops == null) {
                planetTroops = new ArrayList<Integer>();
            }
            if (!planetTroops.contains(pt.getUserId())) {
                planetTroops.add(pt.getUserId());
            }
            troops.put(pt.getPlanetId(), planetTroops);
        }

        for (PlayerPlanet pp : ppList) {

            //No troops found
            if (troops.get(pp.getPlanetId()) == null || troops.get(pp.getPlanetId()).isEmpty()) {
                continue;
            }
            if (groundCombatDAO.findRunningCombat(pp.getPlanetId()) != null) {
                continue;
            }
            //Get users on planet
            ArrayList<Integer> usersOnPlanet = troops.get(pp.getPlanetId());

            //1 Player found on Planet => He is owner => continue
            if ((usersOnPlanet.size() == 1) && (usersOnPlanet.contains(pp.getUserId()))) {
                continue;
            } else {
                CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(usersOnPlanet, new RelativeCoordinate(0, pp.getPlanetId()));
                if (!cgr.hasConflictingParties()) {
                    continue;
                } else {
                    ArrayList<Integer> attGroups = new ArrayList<Integer>();
                    int ownerGroupId = 0;
                    for (CombatGroupEntry cge : cgr.getCombatGroups()) {
                        //Found attacking Group
                        if (!cge.getPlayers().contains(pp.getUserId())) {
                            int landerId = cge.getPlayers().get(0);
                            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Found a new combat on planet " + pp.getPlanetId() + " (User: " + pp.getUserId() + " landerId : " + landerId + ") due to tick check");
                            checkForCombat(landerId, pp.getPlanetId());
                            break;
                        }
                    }
                }
            }


        }
    }

    private static void sendJoinMessage(int landerUserId, PlayerPlanet pp, ArrayList<Integer> usersOnPlanet) {


        for (Integer i : usersOnPlanet) {
            GenerateMessage gm = new GenerateMessage();
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
            String msg = ML.getMLStr("fleetmanagement_msg_invasionuserjoined", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(landerUserId).getGameName());
            msg += "<BR><BR>";
            msg += ML.getMLStr("fleetmanagement_msg_participatingplayers", i);
            msg += "<BR><BR>";
            for (Integer j : usersOnPlanet) {
                msg += "<B>" + userDAO.findById(j).getGameName() + "</B>" + "<BR>";
            }
            gm.setDestinationUserId(i);
            gm.setMsg(msg);
            gm.setMessageContentType(EMessageContentType.COMBAT);
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            gm.writeMessageToUser();
        }
    }

    private static void sendInvasionMessage(int landerUserId, PlayerPlanet pp, ArrayList<Integer> usersOnPlanet) {

        for (Integer i : usersOnPlanet) {
            GenerateMessage gm = new GenerateMessage();
            gm.setMessageType(EMessageType.SYSTEM);
            gm.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
            String msg = ML.getMLStr("fleetmanagement_msg_invasionstarted", i).replace("%PLANET%", pp.getName());
            msg += "<BR><BR>";
            msg += ML.getMLStr("fleetmanagement_msg_participatingplayers", i);
            msg += "<BR><BR>";
            for (Integer j : usersOnPlanet) {
                msg += "<B>" + userDAO.findById(j).getGameName() + "</B>" + "<BR>";
            }
            gm.setMsg(msg);
            gm.setMessageContentType(EMessageContentType.COMBAT);
            gm.setDestinationUserId(i);
            gm.setSourceUserId(userDAO.getSystemUser().getUserId());
            gm.writeMessageToUser();
        }
    }

    //Search for Territory-Entries and Users which not have some
    private static BattleData loadTerritoryEntries(BattleData bd, CombatRound lastRound, HashMap<Integer, ArrayList<Integer>> groups) {
        double terrValues = 0d;
        if (debug) {
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### Loading TerritoryEntries ###");
        }
        if (lastRound != null) {
            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### Last Round != null ###");
            }
            ArrayList<TerritoryEntry> relevantEntries = new ArrayList<TerritoryEntry>();
            ArrayList<Integer> usersWithoutTerritory = new ArrayList<Integer>();
            int userCount = 0;
            for (Map.Entry<Integer, ArrayList<Integer>> cg : groups.entrySet()) {
                for (Integer uId : cg.getValue()) {
                    userCount++;
                    boolean foundTerr = false;
                    for (TerritoryEntry teTmp : bd.getTerritoryEntry()) {
                        if (teTmp.getUserId().equals(uId)
                                && teTmp.getCombatRoundId().equals(lastRound.getId())) {
                            relevantEntries.add(teTmp.clone());
                            terrValues += teTmp.getTerritorySize();
                            foundTerr = true;
                            break;
                        }
                    }
                    if (!foundTerr) {
                        usersWithoutTerritory.add(uId);
                    }
                }
            }

            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### Loaded " + relevantEntries.size() + " relevant entries for " + userCount + " Users ###");
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### " + usersWithoutTerritory.size() + " Users without territory ###");
            terrValues = (Math.round(terrValues * 100d)) / 100d;
            if (terrValues == 0 || relevantEntries.isEmpty()) {
                DebugBuffer.addLine(DebugLevel.FATAL_ERROR, "Error in CombatUtilities :### Switching to panic mode: TerritoryValue in GroundCombatutilities is " + terrValues);
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### combatId : " + bd.getGroundCombatId());
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### LastRound : " + lastRound.getId());
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### PlanetId : " + bd.getPlanetId());
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### PlanetOwner : " + bd.getPlanetOwner());
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### Users : ");
                usersWithoutTerritory.clear();
                for (CombatPlayer cp : bd.getCombatPlayers()) {

                    String name = "null";
                    if (Service.userDAO.findById(cp.getUserId()) != null) {
                        name = Service.userDAO.findById(cp.getUserId()).getGameName();
                    }
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "###### " + name);
                }
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### Generating new values for users");

                if (userCount > 0) {
                    bd.getTerritoryEntry().clear();
                    double splitTerritory = 1d / (double) userCount;
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### splitTerritory = " + splitTerritory);
                    for (Map.Entry<Integer, ArrayList<Integer>> cg : groups.entrySet()) {
                        for (Integer uId : cg.getValue()) {

                            TerritoryEntry teNew = new TerritoryEntry();
                            teNew.setUserId(uId);
                            teNew.setTerritorySize(splitTerritory);
                            teNew.setCombatRoundId(lastRound.getId());
                            teNew.setChange(0d);
                            terrValues = 1d;
                            relevantEntries.add(teNew);
                        }
                    }
                } else {
                    return bd;
                }


            }
            //If not 100% of the territorium is used (players left) fill up the existing territories
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### " + terrValues + " territory ###");
            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "### size of relevantEntries : " + relevantEntries.size() + " ###");
            while (terrValues < 0.99) {
                for (TerritoryEntry te : relevantEntries) {
                    if (te.getTerritorySize() < 0.99d) {
                        te.setTerritorySize(Math.round((te.getTerritorySize() + 0.01d) * 100d) / 100d);
                        terrValues += 0.01d;
                    }
                }
            }
            //There are new users give them some territory
            ArrayList<TerritoryEntry> toAdd = new ArrayList<TerritoryEntry>();
            for (Integer uid : usersWithoutTerritory) {
                //Set start territory
                double value = attStartTerritory;
                //while not enough territory decrease other territories
                while (value > 0.01d) {
                    for (TerritoryEntry te : relevantEntries) {
                        if (te.getTerritorySize() > 0.01d) {
                            te.setTerritorySize(Math.round((te.getTerritorySize() - 0.01d) * 100d) / 100d);
                            value -= 0.01d;
                            //dbg log.debug("TerritorySize : "  + te.getTerritorySize());
                        }
                    }
                }
                TerritoryEntry teNew = new TerritoryEntry();
                teNew.setUserId(uid);
                teNew.setTerritorySize(0.1d);
                teNew.setCombatRoundId(lastRound.getId());
                teNew.setChange(0d);
                toAdd.add(teNew);


            }
            bd.getTerritoryEntry().clear();
            bd.setTerritoryEntry(relevantEntries);
            bd.getTerritoryEntry().addAll(toAdd);
        }
        return bd;
    }

    private static void calculateTerritoryBoundaries(BattleData bd, CombatRound lastRound, Entry<Integer, ArrayList<Integer>> cg, Integer uId, int planetUserId,
            HashMap<Integer, ArrayList<Integer>> groups, ArrayList<CombatPlayer> combatPlayers, Territory_R10 t) {

        //No Territoryvalues yet
        if (lastRound == null) {
            if (debug) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Last round == null, create new territoryvaluse");
            }
            boolean defender = false;
            if (cg.getValue().contains(planetUserId)) {
                defender = true;
                if (debug) {
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "User is in defendergroup");
                }
            }

            double territoryPerUser;
            //If defender
            if (defender) {
                territoryPerUser = Math.round(((1 - attStartTerritory * (groups.size() - 1)) / cg.getValue().size()) * 100d) / 100d;
                //If Attacker
            } else {
                territoryPerUser = Math.round(attStartTerritory / cg.getValue().size() * 100d) / 100d;
            }

            //Process lower and Upper User bound
            double lowerBound = Math.round((territoryPerUser - minTerritory) * 100d) / 100d;
            double upperBound = Math.round(((1d - minTerritory * (double) (combatPlayers.size() - cg.getValue().size())) - territoryPerUser) * 100d) / 100d;

            t.territoryUser.put(uId, territoryPerUser);
            t.territoryUserLowerBound.put(uId, lowerBound);
            t.territoryUserUpperBound.put(uId, upperBound);
            //Add Bounds to group
            if (t.territoryGroup.get(cg.getKey()) == null) {
                t.territoryGroupLowerBound.put(cg.getKey(), lowerBound);
                t.territoryGroupUpperBound.put(cg.getKey(), upperBound);
                t.territoryGroup.put(cg.getKey(), Math.round((territoryPerUser) * 100d) / 100d);
            } else {
                t.territoryGroupLowerBound.put(cg.getKey(), Math.round((t.territoryGroupLowerBound.get(cg.getKey()) + lowerBound) * 100d) / 100d);
                t.territoryGroupUpperBound.put(cg.getKey(), Math.round((t.territoryGroupUpperBound.get(cg.getKey()) + upperBound) * 100d) / 100d);
                t.territoryGroup.put(cg.getKey(), Math.round((t.territoryGroup.get(cg.getKey()) + territoryPerUser) * 100d) / 100d);
            }
            //Territoryvalues existing in DB
        } else {

            TerritoryEntry te = null;

            //Search for User-TerritoryEntry
            for (TerritoryEntry teTmp : bd.getTerritoryEntry()) {
                if (teTmp.getUserId().equals(uId)
                        && teTmp.getCombatRoundId().equals(lastRound.getId())) {
                    te = teTmp.clone();
                    break;
                }
            }
            //If user has no territory-entry create new once
            if (te == null) {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "territoryEntry == null");
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "=>>>>>Major fail should not occur => create dummy entry");

                t.territoryUser.put(uId, 0.1d);
                if (t.territoryGroup.get(cg.getKey()) == null) {
                    t.territoryGroup.put(cg.getKey(), 0.1d);
                } else {
                    t.territoryGroup.put(cg.getKey(), (t.territoryGroup.get(cg.getKey()) + 0.1d));
                }
                // User has a Territory Entry
            } else {
                DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Territory entry loaded : " + te.getTerritorySize());

                t.territoryUser.put(uId, te.getTerritorySize());

                double lowerBound = Math.round((te.getTerritorySize() - minTerritory) * 100d) / 100d;
                double upperBound = Math.round(((1d - minTerritory * (double) (combatPlayers.size() - cg.getValue().size())) - te.getTerritorySize()) * 100d) / 100d;

                //Process lower and Upper User bound

                t.territoryUserLowerBound.put(uId, lowerBound);
                t.territoryUserUpperBound.put(uId, upperBound);
                //Add Bounds to group
                if (t.territoryGroup.get(cg.getKey()) == null) {
                    t.territoryGroupLowerBound.put(cg.getKey(), lowerBound);
                    t.territoryGroupUpperBound.put(cg.getKey(), upperBound);
                    t.territoryGroup.put(cg.getKey(), te.getTerritorySize());
                } else {
                    t.territoryGroupLowerBound.put(cg.getKey(), Math.round((t.territoryGroupLowerBound.get(cg.getKey()) + lowerBound) * 100d) / 100d);
                    t.territoryGroupUpperBound.put(cg.getKey(), Math.round((t.territoryGroupUpperBound.get(cg.getKey()) + upperBound) * 100d) / 100d);
                    t.territoryGroup.put(cg.getKey(), (t.territoryGroup.get(cg.getKey()) + te.getTerritorySize()));
                }
            }
        }
    }

    private static Long calculateHitpoints(TreeMap<Integer, TreeMap<Integer, PlayerTroop>> troops, Integer cgId, Integer uId, boolean test, Long combatHitpoints,
            HashMap<Integer, Long> totalHitpointsPerUser, HashMap<Integer, Long> totalHitpointsPerGroup) {
        for (Map.Entry<Integer, PlayerTroop> troop : troops.get(uId).entrySet()) {
            PlayerTroop pt = troop.getValue();
            if (pt.getNumber() <= 0) {
                continue;
            }
            GroundTroop gt;
            if (!test) {
                gt = groundTroopDAO.findById(pt.getTroopId(), uId);
            } else {
                gt = groundTroopDAO.findById(pt.getTroopId());
            }
            combatHitpoints += (long) (pt.getNumber() * gt.getHitpoints());
            long hitpoints = pt.getNumber() * gt.getHitpoints();
            if (pt.getTroopId().equals(GroundTroop.ID_MILIZ)) {
                hitpoints = Math.round(hitpoints * 0.5d);
            }
            if (totalHitpointsPerUser.get(uId) == null) {
                totalHitpointsPerUser.put(uId, ((long) (hitpoints)));
            } else {
                totalHitpointsPerUser.put(uId, (totalHitpointsPerUser.get(uId) + ((long) (hitpoints))));
            }
        }

        if (totalHitpointsPerGroup.get(cgId) == null) {
            totalHitpointsPerGroup.put(cgId, totalHitpointsPerUser.get(uId));
        } else {
            totalHitpointsPerGroup.put(cgId, totalHitpointsPerGroup.get(cgId) + totalHitpointsPerUser.get(uId));
        }
        return combatHitpoints;
    }

    private static void calculateGroupTerritoryChanges(HashMap<Integer, ArrayList<Integer>> groups, Territory_R10 territory, HashMap<Integer, Long> totalHitpointsPerUser, HashMap<Integer, Long> totalHitpointsPerGroup) {
        //Territory change calculation
        HashMap<Integer, Integer> processed = new HashMap<Integer, Integer>();


        for (Map.Entry<Integer, Double> terr : territory.territoryGroup.entrySet()) {
            for (Map.Entry<Integer, ArrayList<Integer>> group : groups.entrySet()) {
                //Own group
                if (terr.getKey().equals(group.getKey())) {
                    continue;
                }
                if ((processed.containsKey(terr.getKey()) && processed.get(terr.getKey()).equals(group.getKey()))
                        || (processed.containsKey(group.getKey()) && processed.get(group.getKey()).equals(terr.getKey()))) {
                } else {

                    double factorG1 = 0d;
                    double factorG2 = 0d;

                    double total;

                    if ((totalHitpointsPerGroup.get(group.getKey()) != null) && (totalHitpointsPerGroup.get(terr.getKey()) != null)) {
                        total = totalHitpointsPerGroup.get(group.getKey()) + totalHitpointsPerGroup.get(terr.getKey());

                        factorG1 = Math.round(((double) totalHitpointsPerGroup.get(group.getKey()) / total) * 100d) / 100d;
                        factorG2 = Math.round(((double) totalHitpointsPerGroup.get(terr.getKey()) / total) * 100d) / 100d;
                    }


                    if (factorG1 > factorG2) {
                        double change = factorG1 - factorG2;
                        change = Math.floor(change * 10d) / 100d;

                        if (change > 0) {
                            if (change > territory.territoryGroupUpperBound.get(group.getKey())) {
                                change = territory.territoryGroupUpperBound.get(group.getKey());
                            }
                            if (change > territory.territoryGroupLowerBound.get(terr.getKey())) {
                                change = territory.territoryGroupLowerBound.get(terr.getKey());
                            }
                        } else {
                            if (-change > territory.territoryGroupUpperBound.get(group.getKey())) {
                                change = -territory.territoryGroupUpperBound.get(group.getKey());
                            }
                            if (-change > territory.territoryGroupUpperBound.get(terr.getKey())) {
                                change = -territory.territoryGroupUpperBound.get(terr.getKey());
                            }
                        }


                        //saving
                        if (territory.territoryChangeGroup.get(group.getKey()) == null) {
                            territory.territoryChangeGroup.put(group.getKey(), change);
                        } else {
                            territory.territoryChangeGroup.put(group.getKey(), territory.territoryChangeGroup.get(group.getKey()) + change);
                        }
                        if (territory.territoryChangeGroup.get(terr.getKey()) == null) {
                            territory.territoryChangeGroup.put(terr.getKey(), -change);
                        } else {
                            territory.territoryChangeGroup.put(terr.getKey(), territory.territoryChangeGroup.get(terr.getKey()) + (-change));
                        }
                    } else {

                        double change = factorG2 - factorG1;
                        change = Math.floor(change * 10d) / 100d;

                        if (change > 0) {
                            if (change > territory.territoryGroupLowerBound.get(group.getKey())) {
                                change = territory.territoryGroupLowerBound.get(group.getKey());
                            }
                            if (change > territory.territoryGroupUpperBound.get(terr.getKey())) {
                                change = territory.territoryGroupUpperBound.get(terr.getKey());
                            }
                        } else {
                            if (-change > territory.territoryGroupUpperBound.get(group.getKey())) {
                                change = -territory.territoryGroupUpperBound.get(group.getKey());
                            }
                            if (-change > territory.territoryGroupUpperBound.get(terr.getKey())) {
                                change = -territory.territoryGroupUpperBound.get(terr.getKey());
                            }
                        }
                        //saving
                        if (territory.territoryChangeGroup.get(group.getKey()) == null) {
                            territory.territoryChangeGroup.put(group.getKey(), -change);
                        } else {
                            territory.territoryChangeGroup.put(group.getKey(), territory.territoryChangeGroup.get(group.getKey()) - change);
                        }
                        if (territory.territoryChangeGroup.get(terr.getKey()) == null) {
                            territory.territoryChangeGroup.put(terr.getKey(), change);
                        } else {
                            territory.territoryChangeGroup.put(terr.getKey(), territory.territoryChangeGroup.get(terr.getKey()) + (change));
                        }
                    }
                    processed.put(group.getKey(), terr.getKey());
                    processed.put(terr.getKey(), group.getKey());
                }

                // Calculate Change
            }

        }
    }

    private static void calculateUserTerritoryChanges(BattleData bd, CombatRound newRound, HashMap<Integer, ArrayList<Integer>> groups, Territory_R10 territory, HashMap<Integer, Long> totalHitpointsPerUser, HashMap<Integer, Long> totalHitpointsPerGroup) {

        //For Each group
        for (Map.Entry<Integer, ArrayList<Integer>> change : groups.entrySet()) {
            //Get Change
            double groupChange = Math.round(territory.territoryChangeGroup.get(change.getKey()) * 100d) / 100d;

            int usersAtBoundaries = 0;
            if (groupChange == 0) {
                //Create 0 entries
                for (Integer userId : change.getValue()) {

                    TerritoryEntry te = new TerritoryEntry();
                    te.setChange(0d);
                    te.setCombatRoundId(newRound.getId());
                    te.setTerritorySize(territory.territoryUser.get(userId));
                    te.setUserId(userId);
                    /*
                     ArrayList<TerritoryEntry> tes = bd.getTerritoryEntry();
                     if (tes == null) {
                     tes = new ArrayList<TerritoryEntry>();
                     }
                     */
                    bd.getTerritoryEntry().add(te);
                }

            } else {
                //Users Already Done
                ArrayList<Integer> alreadyDone = new ArrayList<Integer>();
                //While still change existing and not all users done
                while (groupChange != 0 && change.getValue().size() != usersAtBoundaries && alreadyDone.size() != change.getValue().size()) {
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "b4 calc: groupChange : " + groupChange);
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "b4 calc: usersAtBoundaries : " + usersAtBoundaries);
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "b4 calc: change.getValue().size() : " + change.getValue().size());
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "b4 calc: alreadyDone.size() : " + alreadyDone.size());
                    for (Integer userId : change.getValue()) {
                        if (alreadyDone.contains(userId)) {
                            continue;
                        }
                        //Get Grouphitpoints
                        long totalHitPointsGroup = totalHitpointsPerGroup.get(change.getKey());
                        if (totalHitpointsPerUser.get(userId) == null) {
                            continue;
                        }
                        //Get UserHitpoints
                        double userHitPoints = totalHitpointsPerUser.get(userId);

                        //Calculate territorychange withing Group for user
                        // double changePlayer = Math.ceil((userHitPoints * 100d / (double) totalHitPointsGroup * groupChange) / 100d);
                        double changePlayer = (((userHitPoints * 100d / (double) totalHitPointsGroup) * groupChange) / 100d);
                        DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "changePlayer " + changePlayer);

                        if (changePlayer > 0 && changePlayer <= 0.01d) {
                            changePlayer = 0.01d;
                        }
                        if (changePlayer < 0 && changePlayer >= -0.01d) {
                            changePlayer = -0.01d;
                        }
                        //Hitting Boundary (lower Max)
                        if (territory.territoryUser.get(userId) + changePlayer <= 0.01d) {

                            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Territory change case 1");
                            changePlayer = -(territory.territoryUser.get(userId) - 0.01d);
                            groupChange -= changePlayer;
                            usersAtBoundaries++;
                            totalHitpointsPerGroup.put(change.getKey(), (long) (totalHitPointsGroup - userHitPoints));
                            alreadyDone.add(userId);

                            //Hitting Boundary (upper Max)
                        } else if (territory.territoryUser.get(userId) + changePlayer >= 0.99d) {
                            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Territory change case 2");

                            changePlayer = 0.99d - territory.territoryUser.get(userId);
                            groupChange -= changePlayer;
                            totalHitpointsPerGroup.put(change.getKey(), (long) (totalHitPointsGroup - userHitPoints));
                            usersAtBoundaries++;
                            alreadyDone.add(userId);
                        } else {
                            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "Territory change case 3");
                            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, groupChange + " -= " + changePlayer);
                            groupChange -= changePlayer;
                        }
                        changePlayer = Math.round(changePlayer * 100d) / 100d;
                        groupChange = Math.round(groupChange * 100d) / 100d;

                        //Search Territory Entry
                        TerritoryEntry te = null;
                        for (TerritoryEntry te2 : bd.getTerritoryEntry()) {
                            if (te2.getUserId().equals(userId) && te2.getCombatRoundId().equals(newRound.getId())) {
                                te = te2;
                                break;
                            }
                        }

                        //Create new TerritoryEntry
                        if (te == null) {
                            te = new TerritoryEntry();
                            te.setChange(changePlayer);
                            te.setTerritorySize(territory.territoryUser.get(userId) + changePlayer);
                            te.setCombatRoundId(newRound.getId());
                            te.setUserId(userId);
                            /*
                             ArrayList<TerritoryEntry> tes = bd.getTerritoryEntry();
                             if (tes == null) {
                             tes = new ArrayList<TerritoryEntry>();
                             }
                             */
                            bd.getTerritoryEntry().add(te);
                            //Update Existing TerritoryEntry
                        } else {
                            te.setChange(te.getChange() + changePlayer);
                            DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "(Player) Add " + changePlayer + " to " + te.getTerritorySize());
                            te.setTerritorySize(te.getTerritorySize() + changePlayer);
                        }
                        territory.territoryUser.put(userId, territory.territoryUser.get(userId) + changePlayer);
                    }
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "after calc: groupChange : " + groupChange);
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "after calc: usersAtBoundaries : " + usersAtBoundaries);
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "after calc: change.getValue().size() : " + change.getValue().size());
                    DebugBuffer.addLine(DebugLevel.GROUNDCOMBAT, "after calc: alreadyDone.size() : " + alreadyDone.size());

                }
            }
        }
    }

    private static void sendDefendedMessage(int groundCombatId, PlayerPlanet pp, int planetUserId, int planetId) {
        ArrayList<Integer> participants = new ArrayList<Integer>();
        for (CombatPlayer cp : (ArrayList<CombatPlayer>) Service.combatPlayerDAO.findByCombatId(groundCombatId)) {
            if (!participants.contains(cp.getUserId())) {
                participants.add(cp.getUserId());
            }
        }
        GenerateMessage defender = new GenerateMessage();
        GenerateMessage attacker = new GenerateMessage();

        CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(participants, new RelativeCoordinate(0, planetId));
        if (pp == null) {
            return;
        }

        for (Map.Entry<Integer, ArrayList<Integer>> entry : cgr.getAsMap().entrySet()) {
            for (Integer i : entry.getValue()) {
                //defender
                if (entry.getValue().contains(planetUserId)) {
                    defender.setDestinationUserId(i);
                    defender.setSourceUserId(userDAO.getSystemUser().getUserId());
                    defender.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
                    defender.setMsg(ML.getMLStr("fleetmanagement_msg_invasionplanetdefended", i).replace("%PLANET%", pp.getName()));
                    defender.setMessageType(EMessageType.SYSTEM);
                    defender.setMessageContentType(EMessageContentType.COMBAT);
                    defender.writeMessageToUser();
                    //Winners
                } else {
                    attacker.setDestinationUserId(i);
                    attacker.setSourceUserId(userDAO.getSystemUser().getUserId());
                    attacker.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
                    attacker.setMsg(ML.getMLStr("fleetmanagement_msg_invasionfailed", i).replace("%PLANET%", pp.getName()));
                    attacker.setMessageType(EMessageType.SYSTEM);
                    attacker.setMessageContentType(EMessageContentType.COMBAT);
                    attacker.writeMessageToUser();
                }
            }
        }
    }

    private static void appendChart(GroundCombatResult gcr, EGroundCombatChart chartType) {
        try {
            ArrayList< DefaultCategoryDataset> data = new ArrayList<DefaultCategoryDataset>();
            DefaultCategoryDataset result;
            DefaultCategoryDataset territory;
            result = new DefaultCategoryDataset();
            territory = new DefaultCategoryDataset();


            int round = 1;
            HashMap<Integer, Double> attackStrength = new HashMap<Integer, Double>();
            for (Map.Entry<Integer, TreeMap<Integer, TreeMap<Integer, RoundEntry>>> roundEntry : gcr.getHistory().entrySet()) {
                //If Groundtroop-Type not existing
                for (Map.Entry<Integer, TreeMap<Integer, RoundEntry>> userEntry : roundEntry.getValue().entrySet()) {

                    String userName = "User gel�scht";
                    if (Service.userDAO.findById(userEntry.getKey()) != null) {
                        userName = Service.userDAO.findById(userEntry.getKey()).getGameName();
                    }
                    long totalHitpoints = 0l;
                    long totalAttackStrength = 0l;
                    for (Map.Entry<Integer, RoundEntry> groundTroopEntry : userEntry.getValue().entrySet()) {
                        //If GroundTrooptype not yet existing

                        if (chartType.equals(EGroundCombatChart.UNITS)) {

                            result.setValue((long) groundTroopEntry.getValue().getNumber(), ML.getMLStr(Service.groundTroopDAO.findById(groundTroopEntry.getKey()).getName(), gcr.getUserId()) + " - " + userName, String.valueOf(round));
                        } else if (chartType.equals(EGroundCombatChart.HITPOINTS)) {
                            result.setValue((long) groundTroopEntry.getValue().getNumber() * Service.groundTroopDAO.findById(groundTroopEntry.getKey()).getHitpoints(), ML.getMLStr(Service.groundTroopDAO.findById(groundTroopEntry.getKey()).getName(), gcr.getUserId()) + " - " + userName, String.valueOf(round));

                        } else if (chartType.equals(EGroundCombatChart.ATTACKSTRENGTH)) {
                            Double strength = attackStrength.get(groundTroopEntry.getKey());
                            if (strength == null) {
                                strength = 0d;
                                for (GTRelation gt : Service.gtRelationDAO.findBySrcId(groundTroopEntry.getKey())) {
                                    strength += gt.getAttackPoints() * gt.getHitProbab() * gt.getAttackPoints();
                                }
                            }
                            attackStrength.put(groundTroopEntry.getKey(), strength);
                            result.setValue((long) groundTroopEntry.getValue().getNumber() * strength, ML.getMLStr(Service.groundTroopDAO.findById(groundTroopEntry.getKey()).getName(), gcr.getUserId()) + " - " + userName, String.valueOf(round));
                        } else if (chartType.equals(EGroundCombatChart.TOTAL)) {
                            Double strength = attackStrength.get(groundTroopEntry.getKey());
                            if (strength == null) {
                                strength = 0d;
                                for (GTRelation gt : Service.gtRelationDAO.findBySrcId(groundTroopEntry.getKey())) {
                                    strength += gt.getAttackPoints() * gt.getHitProbab() * gt.getAttackPoints();
                                }
                            }
                            attackStrength.put(groundTroopEntry.getKey(), strength);
                            totalAttackStrength += (long) (groundTroopEntry.getValue().getNumber() * strength);
                            totalHitpoints += (long) (groundTroopEntry.getValue().getNumber() * Service.groundTroopDAO.findById(groundTroopEntry.getKey()).getHitpoints());
                        }

                    }
                    if (!chartType.equals(EGroundCombatChart.TOTAL)) {
                        TerritoryEntry te = Service.territoryEntryDAO.findBy(userEntry.getKey(), roundEntry.getKey());
                        territory.setValue(te.getTerritorySize(), "Territorium" + " - " + userName, String.valueOf(round));
                    } else {
                        result.setValue(totalAttackStrength, userName, String.valueOf(round));

                        territory.setValue(totalHitpoints, userName, String.valueOf(round));
                    }
                    //     //dbg log.debug("=======================");
                }
                round++;
            }
            // test(result);

            //date += "-" + gc.get(GregorianCalendar.YEAR);
            /* data[0][i] = ps.getMilitaryPoints();
             data[1][i] = ps.getPopulationPoints();
             data[2][i] = ps.getResearchPoints();
             data[3][i] = ps.getRessourcePoints();
             data[4][i] = ps.getRank();*/


            JFreeChart chart = createChart(result, territory, gcr.getUserId(), chartType);
            if (chart == null) {
                return;
            }
            gcr.setChart(chart);
            // setContentPane(chartPanel);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in BattleLog Statistic", e);
        }
    }

    private static JFreeChart createChart(DefaultCategoryDataset result, DefaultCategoryDataset territory, int userId, EGroundCombatChart chartType) {
        CategoryItemRenderer renderer = new LineAndShapeRenderer();
        CategoryItemRenderer renderer2 = new LineAndShapeRenderer();
        String firstChart = "Undefined";
        String secondChart = "Territorium";
        if (chartType.equals(EGroundCombatChart.ATTACKSTRENGTH)) {
            firstChart = "Kampfkraft";
        } else if (chartType.equals(EGroundCombatChart.HITPOINTS)) {
            firstChart = "TrefferPunkte";
        } else if (chartType.equals(EGroundCombatChart.UNITS)) {
            firstChart = "Einheiten";
        } else if (chartType.equals(EGroundCombatChart.TOTAL)) {
            firstChart = "Kampfkraft (Total)";
            secondChart = "Trefferpunkte (Total)";
        }
        final NumberAxis rangeAxis2 = new NumberAxis(firstChart);
        final NumberAxis rangeAxis = new NumberAxis(secondChart);
        rangeAxis2.setInverted(false);
        rangeAxis2.setAutoRange(true);



        final CategoryPlot subplot = new CategoryPlot(result, null, rangeAxis2,
                renderer2);


        final CategoryPlot subplot2 = new CategoryPlot(territory, null, rangeAxis,
                renderer);


        final CombinedDomainCategoryPlot plot = new CombinedDomainCategoryPlot();
        CategoryAxis xAxis = (CategoryAxis) plot.getDomainAxis();
        xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
        plot.setGap(15);
        plot.add(subplot);
        plot.add(subplot2);


        JFreeChart chart = new JFreeChart(
                ML.getMLStr("statistic_lbl_history", userId),
                new Font("SansSerif", Font.BOLD, 12),
                plot,
                true);

        return chart;
    }
    
    public GTRelation getMostAdvancedAttackEntry(int attackerId, int defenderId) {
        ArrayList<GTRelation> gtRelations = gtRelationDAO.findBy(attackerId, defenderId);
        
        // Order by Attack power and take the hightest
        TreeMap<Float,GTRelation> sortedRelations = Maps.newTreeMap();
        for (GTRelation gt : gtRelations) {
            sortedRelations.put(gt.getAttackPoints(),gt);
        }
        
        if (!sortedRelations.isEmpty()) {
            return sortedRelations.firstEntry().getValue();
        } else {
            return null;
        }
    }
}

class Territory_R10 {

    //Describes the total Hitpoints of the Groups and Users
    /*
     public HashMap<Integer, Long> totalHitpointsPerGroup = new HashMap<Integer, Long>();
     public HashMap<Integer, Long> totalHitpointsPerUser = new HashMap<Integer, Long>();*/
    //Describes the minimum(1%) and maximum(99% - 1%*participants) Territory of Groups and Users
    public HashMap<Integer, Double> territoryGroupLowerBound = new HashMap<Integer, Double>();
    public HashMap<Integer, Double> territoryGroupUpperBound = new HashMap<Integer, Double>();
    public HashMap<Integer, Double> territoryUserLowerBound = new HashMap<Integer, Double>();
    public HashMap<Integer, Double> territoryUserUpperBound = new HashMap<Integer, Double>();
    HashMap<Integer, Double> territoryChangeGroup = new HashMap<Integer, Double>();
    //Describes the territory
    public HashMap<Integer, Double> territoryUser = new HashMap<Integer, Double>();
    public HashMap<Integer, Double> territoryGroup = new HashMap<Integer, Double>();

    public Territory_R10() {
    }
}
