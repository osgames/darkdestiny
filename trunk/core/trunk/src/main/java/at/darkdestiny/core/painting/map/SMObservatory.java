/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

/**
 *
 * @author Stefan
 */
public class SMObservatory {

    private final int x;
    private final int y;
    private final static int RANGE = 100;
    
    public SMObservatory(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }
    
    /**
     * @return the RANGE
     */
    public static int getRANGE() {
        return RANGE;
    }
        
}
