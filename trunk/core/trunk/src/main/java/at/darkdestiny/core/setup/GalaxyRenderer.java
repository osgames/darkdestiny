/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.setup;

import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.result.GalaxyCreationResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.util.DebugBuffer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;

/**
 *
 * @author Admin
 */
public class GalaxyRenderer implements IModifyImageFunction {

    private Galaxy galaxy;
    private int width;
    private int height;
    private final Integer picWidth;
    private final Integer picHeight;
    private final String path;
    private GalaxyCreationResult galaxyCreationResult;
    boolean byDatabase = true;
    private final static boolean drawCoordinates = true;

    public GalaxyRenderer(int galaxyId, String path, int picWidth, int picHeight) {
        galaxy = Service.galaxyDAO.findById(galaxyId);
        this.path = path;
        this.picWidth = picWidth;
        this.picHeight = picHeight;
    }    
    
    public GalaxyRenderer(int galaxyId, String path) {
        galaxy = Service.galaxyDAO.findById(galaxyId);
        this.path = path;
        picWidth = null;
        picHeight = null;
    }

    public GalaxyRenderer(GalaxyCreationResult gcr, String path) {
        galaxyCreationResult = gcr;
        byDatabase = false;
        picWidth = null;
        picHeight = null;        
        this.path = path;
    }
    /* (non-Javadoc)
     * @see at.darkdestiny.core.img.IModifyImageFunction#run(java.awt.Graphics)
     */

    public void run(Graphics graphics) {
        Image img = null; //Toolkit.getDefaultToolkit().createImage(path + GetSystemView.getLargePic(pd.getPlanetId()));
        
        System.out.println("BOUNDS X: " + width);
        System.out.println("BOUNDS Y: " + height);
        
        int topLeftOriginX = (galaxy.getOriginX() - (galaxy.getWidth() / 2));
        int topLeftOriginY = (galaxy.getOriginY() - (galaxy.getHeight() / 2));
        
        double translationX = (double)width / (double)galaxy.getWidth();
        double translationY = (double)height / (double)galaxy.getHeight();
        
        try {
            img = ImageIO.read(new File(path + "gstar.png"));

            if (byDatabase) {
                // Check all stars and adjust bounds if some stars are out of border
                int maxXOutOfBounds = 0;
                int maxYOutOfBounds = 0;
                
                for (at.darkdestiny.core.model.System s : (ArrayList<at.darkdestiny.core.model.System>) Service.systemDAO.findAll()) {
                    if (s.getGalaxyId().equals(galaxy.getId())) {
                        if (s.getX() < topLeftOriginX) {
                            maxXOutOfBounds = Math.max(maxXOutOfBounds, Math.abs(s.getX() - topLeftOriginX));
                        }
                        if (s.getX() > (topLeftOriginX + galaxy.getWidth())) {
                            maxXOutOfBounds = Math.max(maxXOutOfBounds, Math.abs(s.getX() - (topLeftOriginX + galaxy.getWidth())));
                        }              
                        if (s.getY() < topLeftOriginY) {
                            maxYOutOfBounds = Math.max(maxYOutOfBounds, Math.abs(s.getY() - topLeftOriginY));
                        }
                        if (s.getY() > (topLeftOriginY + galaxy.getHeight())) {
                            maxYOutOfBounds = Math.max(maxYOutOfBounds, Math.abs(s.getY() - (topLeftOriginY + galaxy.getHeight())));
                        }                       
                    }
                }
                          
                int scaleBy = Math.max(maxXOutOfBounds,maxYOutOfBounds);
                
                galaxy.setWidth(galaxy.getWidth() + (scaleBy * 2));
                galaxy.setHeight(galaxy.getHeight() + (scaleBy * 2));
                
                topLeftOriginX = (galaxy.getOriginX() - (galaxy.getWidth() / 2));
                topLeftOriginY = (galaxy.getOriginY() - (galaxy.getHeight() / 2));

                translationX = (double)width / (double)galaxy.getWidth();
                translationY = (double)height / (double)galaxy.getHeight();                
                
                System.out.println("[DATABASE] Draw stars");
                for (at.darkdestiny.core.model.System s : (ArrayList<at.darkdestiny.core.model.System>) Service.systemDAO.findAll()) {
                    if (s.getGalaxyId().equals(galaxy.getId())) {
                        // System.out.println("[OLD] Draw system at " + (s.getX() - (galaxy.getOriginX() - (galaxy.getWidth() / 2))) + "/" + (s.getY() - (galaxy.getOriginY() - (galaxy.getHeight() / 2))));
                        // graphics.drawImage(img, s.getX() - (galaxy.getOriginX() - (galaxy.getWidth() / 2)), s.getY() - (galaxy.getOriginY() - (galaxy.getHeight() / 2)), null);
                        int relativeX = (int)Math.round((double)(s.getX() - topLeftOriginX) * translationX) - (img.getWidth(null) / 2);
                        int relativeY = (int)Math.round((double)(s.getY() - topLeftOriginY) * translationY) - (img.getHeight(null) / 2);
                        graphics.drawImage(img, relativeX, relativeY, null);
                        
                        // System.out.println("[NEW] Draw system at " + relativeX + "/" + relativeY);
                    }
                }

                // draw coordinates
                System.out.println("[DATABASE] Draw coordinates");
                graphics.setColor(Color.WHITE);
                if (drawCoordinates) {
                    int x = width / 2;
                    int y = height / 2;

                    graphics.drawLine(x, y, x - 20, y);
                    graphics.drawLine(x, y, x, y + 20);
                    graphics.drawLine(x, y, x + 20, y);
                    graphics.drawLine(x, y, x, y - 20);
                }
                
                int topLeftX = 0;
                int topLeftY = 0;
                
                if (drawCoordinates) {
                    graphics.drawLine(topLeftX, topLeftY, topLeftX + 20, topLeftY);
                    graphics.drawLine(topLeftX, topLeftY, topLeftX, topLeftY + 20);      
                
                    graphics.drawString((galaxy.getOriginX() - (galaxy.getWidth() / 2)) + "/" + (galaxy.getOriginY() - (galaxy.getHeight() / 2)), topLeftX + 3, topLeftY + 13);
                }
                
                int topRightX = width - 1;
                int topRightY = 0;
                
                if (drawCoordinates) {                
                    graphics.drawLine(topRightX, topRightY, topRightX - 20, topRightY);
                    graphics.drawLine(topRightX, topRightY, topRightX, topRightY + 20);
                                
                    String text = (galaxy.getOriginX() + (galaxy.getWidth() / 2)) + "/" + (galaxy.getOriginY() - (galaxy.getHeight() / 2));
                    double length = graphics.getFontMetrics().getStringBounds(text, graphics).getBounds2D().getMaxX();
                    System.out.println("LENGTH = " + length);
                    graphics.drawString(text, topRightX - ((int)length + 5), topRightY + 13);                
                }
                
                int bottomLeftX = 0;
                int bottomLeftY = height - 1;
                
                if (drawCoordinates) {
                    graphics.drawLine(bottomLeftX, bottomLeftY, bottomLeftX + 20, bottomLeftY);
                    graphics.drawLine(bottomLeftX, bottomLeftY, bottomLeftX, bottomLeftY - 20);                
                    graphics.drawString((galaxy.getOriginX() - (galaxy.getWidth() / 2)) + "/" + (galaxy.getOriginY() + (galaxy.getHeight() / 2)), bottomLeftX + 3, bottomLeftY - 7);                
                }
                
                int bottomRightX = width - 1;
                int bottomRightY = height - 1;
                
                if (drawCoordinates) {                
                    graphics.drawLine(bottomRightX, bottomRightY, bottomRightX - 20, bottomRightY);
                    graphics.drawLine(bottomRightX, bottomRightY, bottomRightX, bottomRightY - 20);  
                                
                    String text = (galaxy.getOriginX() + (galaxy.getWidth() / 2)) + "/" + (galaxy.getOriginY() + (galaxy.getHeight() / 2));
                    double length = graphics.getFontMetrics().getStringBounds(text, graphics).getBounds2D().getMaxX();
                    graphics.drawString(text, bottomRightX - ((int)length + 5), bottomRightY - 7);
                }
                
            } else {
                galaxy = galaxyCreationResult.getGalaxy();
                for (at.darkdestiny.core.model.System s : (ArrayList<at.darkdestiny.core.model.System>) galaxyCreationResult.getSystems()) {
                    graphics.drawImage(img, s.getX() - (galaxy.getOriginX() - (galaxy.getWidth() / 2)), s.getY() - (galaxy.getOriginY() - (galaxy.getHeight() / 2)), null);

                }
            }

        } catch (Exception e) {
            DebugBuffer.warning("Picture not found " + e + "piclink " + path + "gstar.png");
        }
    }

    /* (non-Javadoc)
     * @see at.darkdestiny.core.img.IModifyImageFunction#setSize(int, int)
     */
    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public String createMapEntry() {
        return "";
    }
}
