/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.tools.graph;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public interface Node<T> {
    public String getName();
    public T getLoad();
    public ArrayList<Edge> getEdges();
    public void addEdge(Edge e);
    public AbsoluteCoordinate getAbsoluteCoordinate();
    public String toString();
    public String toColor();
}
