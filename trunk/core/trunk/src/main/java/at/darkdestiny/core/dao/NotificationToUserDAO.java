/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.NotificationToUser;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Aion
 */
public class NotificationToUserDAO extends ReadWriteTable<NotificationToUser> implements GenericDAO {

    public NotificationToUser getOrCreate(int userId, int type) {
        NotificationToUser ntu = new NotificationToUser();

        ntu.setUserId(userId);
        ntu.setNotificationId(type);

        NotificationToUser result = (NotificationToUser) get(ntu);

        if (result == null) {
            ntu = new NotificationToUser();
            ntu.setAwayMode(true);
            ntu.setEnabled(true);
            ntu.setUserId(userId);
            ntu.setNotificationId(type);
            ntu = add(ntu);
            return ntu;
        } else {
            return result;
        }
    }

    public ArrayList<NotificationToUser> findByUserId(int userId) {
        NotificationToUser ntu = new NotificationToUser();
        ntu.setUserId(userId);
        return find(ntu);
    }
}
