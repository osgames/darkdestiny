package at.darkdestiny.core.xml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class XMLTokenizer {

	public class XML_TAG_BEGIN extends XMLToken {
		public XML_TAG_BEGIN() {super("<");}
		}
	public class XML_TAG_END extends XMLToken {
		public XML_TAG_END() {super(">");}
		}
	public class XML_END_TAG extends XMLToken {
		public XML_END_TAG() {super("/");}
		}
	public class XML_ATTR_EQUAL extends XMLToken {
		public XML_ATTR_EQUAL() {super("=");}
		}
	public class NAME_TOKEN extends XMLToken {
		public NAME_TOKEN(String text) {super(text);}
		}
	public class XML_TEXT extends XMLToken {
		public XML_TEXT(String text) {super(text);}
		}

	private Reader br;
	private int line = 1;
	private int chr = 0;

	public XMLTokenizer(BufferedReader br) {
		this.br = br;
	}

	public String getPosition() {
		return "Line "+line+" Character "+chr;
	}

	int previewChar = -2;
	
	public int getNextChar() throws IOException
	{
		if (previewChar != -2)
		{
			int ch = previewChar;
			previewChar = -2;
			if (ch == '\n')
			{
				line ++;
				chr = 0;
			}
			else chr ++;
			return ch;
		}
		int ch = br.read();
		if (ch == '\n')
		{
			line ++;
			chr = 0;
		}
		else chr ++;
		return ch;
	}
	
	private int previewChar() throws IOException {
		if (previewChar == -2)
		{
			previewChar = br.read();
		}
		return previewChar;
	}


	XMLToken preview = null;
	
	public XMLToken nextToken(boolean inContents) throws IOException, XMLMalFormatedException {
		if (preview != null)
		{
			XMLToken ret = preview;
			preview = null;
			return ret;
		}
		int ch = getNextChar();
		//Whitespace wird ignoriert
		while (Character.isWhitespace((char)ch))
			ch = getNextChar();
		if (ch == -1)
			return null;
		if (ch == '<')
			return new XML_TAG_BEGIN();
		if (ch == '>')
			return new XML_TAG_END();
		
		if (inContents)
		{
			String text = "";
			while (previewChar() != '<')
			{
				text = text + (char)ch;
				ch = getNextChar();
			}
			text = text+(char)ch;
			return new XML_TEXT(text);
		}
		
		if (ch == '/')
			return new XML_END_TAG();
		if (ch == '=')
			return new XML_ATTR_EQUAL();
		if (((ch >= 'a') && (ch <= 'z')) ||
				((ch >= 'A') && (ch <= 'Z')))
		{//Namen oder Text gefunden
			String text = "";
			while (Character.isJavaIdentifierPart((char)previewChar()))
			{
				text = text +(char)ch;
				ch = this.getNextChar();
			}
			text = text + (char)ch;
			return new NAME_TOKEN(text);
		}
		throw new XMLMalFormatedException(this, "Unknown char-sequence");
	}

	public XMLToken previewToken(boolean inContents) throws IOException, XMLMalFormatedException {
		if (preview != null)
			return preview;
		preview = nextToken(inContents);
		return preview;
	}

	public String readAttribValue() throws IOException, XMLMalFormatedException {
		if (getNextChar() != '"')
			throw new XMLMalFormatedException(this, "\" expected");
		String text = "";
		int startLine = line;
		int startCol = chr;
		int ch = getNextChar();
		while (ch != '"')
		{
			text = text + (char)ch;
			ch = getNextChar();
			if (ch == -1)
				throw new XMLMalFormatedException(this, "\" missing, Attribute startet at Line "+startLine+", Column "+startCol);
		}
		return text;
	}

}
