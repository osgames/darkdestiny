/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ships;

/**
 *
 * @author Stefan
 */
public class SpecialModule extends BuiltInModule {
    private int energyConsumption;
    private double defenseBonus;
    private double decreasedTargetFireEff;
    
    public SpecialModule(ModInitParameter mip, int id) {
        super(mip,id);
    }

    /**
     * @return the defenseBonus
     */
    public double getDefenseBonus() {
        return defenseBonus;
    }

    /**
     * @param defenseBonus the defenseBonus to set
     */
    public void setDefenseBonus(double defenseBonus) {
        this.defenseBonus = defenseBonus;
    }

    /**
     * @return the decreasedTargetFireEff
     */
    public double getDecreasedTargetFireEff() {
        return decreasedTargetFireEff;
    }

    /**
     * @param decreasedTargetFireEff the decreasedTargetFireEff to set
     */
    public void setDecreasedTargetFireEff(double decreasedTargetFireEff) {
        this.decreasedTargetFireEff = decreasedTargetFireEff;
    }
    
    public int getEnergyConsumption() {
        return energyConsumption;
    }

    protected void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }    
}
