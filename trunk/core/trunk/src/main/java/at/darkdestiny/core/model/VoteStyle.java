/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "votestyle")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class VoteStyle extends Model<VoteOption> {
    @FieldMappingAnnotation("voteId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer voteId;
    @FieldMappingAnnotation("showVotings")
    private Boolean showVotings;
    @ColumnProperties(nullable = true)
    @FieldMappingAnnotation("overrideVotedText")
    private String votedText;
    @ColumnProperties(nullable = true)
    @FieldMappingAnnotation("overrideNotVotedText")
    private String notVotedText;

    /**
     * @return the id
     */
    public Integer getVoteId() {
        return voteId;
    }

    /**
     * @param id the id to set
     */
    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    /**
     * @return the showVotings
     */
    public Boolean getShowVotings() {
        return showVotings;
    }

    /**
     * @param showVotings the showVotings to set
     */
    public void setShowVotings(Boolean showVotings) {
        this.showVotings = showVotings;
    }

    /**
     * @return the votedText
     */
    public String getVotedText() {
        return votedText;
    }

    /**
     * @param votedText the votedText to set
     */
    public void setVotedText(String votedText) {
        this.votedText = votedText;
    }

    /**
     * @return the notVotedText
     */
    public String getNotVotedText() {
        return notVotedText;
    }

    /**
     * @param notVotedText the notVotedText to set
     */
    public void setNotVotedText(String notVotedText) {
        this.notVotedText = notVotedText;
    }
}
