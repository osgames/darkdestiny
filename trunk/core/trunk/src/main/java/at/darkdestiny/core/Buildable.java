/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.ships.BuildTime;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
public interface Buildable {
    public RessourcesEntry getRessCost();
    public Model getBase();
    public BuildTime getBuildTime();
    public Class getModelClass();
}
