/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.TradePost;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TradePostDAO extends ReadWriteTable<TradePost> implements GenericDAO {

    public TradePost findByPlanetId(Integer planetId) {
        TradePost tp = new TradePost();
        tp.setPlanetId(planetId);
        ArrayList<TradePost> result = find(tp);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<TradePost> findByUserId(Integer userId) {

        ArrayList<TradePost> result = new ArrayList<TradePost>();
        for (TradePost tp : (ArrayList<TradePost>) findAll()) {
            PlayerPlanet pp = Service.playerPlanetDAO.findByPlanetId(tp.getPlanetId());
            if (pp != null && pp.getUserId().equals(userId)) {
                result.add(tp);
            }
        }
        return result;
    }

    public TradePost findById(Integer id) {
        TradePost tp = new TradePost();
        tp.setId(id);
        return (TradePost) get(tp);
    }
}
