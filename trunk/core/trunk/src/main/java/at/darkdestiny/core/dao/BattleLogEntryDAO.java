/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.BattleLogEntry;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class BattleLogEntryDAO extends ReadWriteTable<BattleLogEntry> implements GenericDAO  {


    public BattleLogEntry findBy(Integer id, Integer designId){
       BattleLogEntry bl = new BattleLogEntry();
       bl.setId(id);
       bl.setDesignId(designId);
       return (BattleLogEntry)get(bl);
    }
    public ArrayList<BattleLogEntry> findById(Integer id){
       BattleLogEntry bl = new BattleLogEntry();
       bl.setId(id);
       return find(bl);
    }
}
