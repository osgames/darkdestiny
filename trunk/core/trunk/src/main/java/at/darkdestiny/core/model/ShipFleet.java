/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "shipfleet")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class ShipFleet extends Model<ShipFleet> {

    @IdFieldAnnotation
    @FieldMappingAnnotation(value = "id")
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation(value = "fleetId")
    @IndexAnnotation(indexName = "fleetDesignIdx")
    private Integer fleetId;
    @FieldMappingAnnotation(value = "designId")
    @IndexAnnotation(indexName = "fleetDesignIdx")
    private Integer designId;
    @FieldMappingAnnotation(value = "count")
    private Integer count;

    /**
     * @return the fleetId
     */
    public Integer getFleetId() {
        return fleetId;
    }

    /**
     * @param fleetId the fleetId to set
     */
    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    /**
     * @return the designId
     */
    public Integer getDesignId() {
        return designId;
    }

    /**
     * @param designId the designId to set
     */
    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
