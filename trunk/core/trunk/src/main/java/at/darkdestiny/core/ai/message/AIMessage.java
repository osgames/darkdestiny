/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.ai.message;

import at.darkdestiny.core.ai.AIMessagePayload;

/**
 *
 * @author Admin
 */
public abstract class AIMessage {
    private final Integer messageId;
    private final Integer senderId;
    private final Integer receiverId;
    private final Integer planetId;
    
    private AIMessagePayload payload;
    
    protected AIMessage(int messageId, int senderId, int receiverId) {
        this.messageId = messageId;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.planetId = 0;
        this.payload = null;
    }
    
    protected AIMessage(int messageId, int senderId, int receiverId, int planetId) {
        this.messageId = messageId;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.planetId = planetId;
        this.payload = null;
    }    
    
    protected AIMessage(int messageId, int senderId, int receiverId, int planetId, AIMessagePayload payload) {
        this.messageId = messageId;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.planetId = planetId;
        this.payload = payload;
    }        

    /**
     * @return the messageId
     */
    public Integer getMessageId() {
        return messageId;
    }

    /**
     * @return the senderId
     */
    public Integer getSenderId() {
        return senderId;
    }

    /**
     * @return the receiverId
     */
    public Integer getReceiverId() {
        return receiverId;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @return the payload
     */
    protected AIMessagePayload getPayload() {
        return payload;
    }
}
