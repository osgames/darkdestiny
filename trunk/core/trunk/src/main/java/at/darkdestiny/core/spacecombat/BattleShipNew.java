/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.ships.*;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import at.darkdestiny.core.ModuleType;
import at.darkdestiny.core.WeaponModule;
import at.darkdestiny.core.dao.ChassisDAO;
import at.darkdestiny.core.dao.DamagedShipsDAO;
import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.ShipTypeAdjustmentDAO;
import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipTypeAdjustment;
import at.darkdestiny.core.spacecombat.helper.ShieldDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Aion
 */
public class BattleShipNew extends AbstractBattleShip implements IBattleShip {

    private static final Logger log = LoggerFactory.getLogger(BattleShipNew.class);

    public static ModuleDAO mDAO = (ModuleDAO) DAOFactory.get(ModuleDAO.class);
    public static ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);
    public static ShipDesignDAO sdDAO = (ShipDesignDAO) DAOFactory.get(ShipDesignDAO.class);
    public static ChassisDAO cDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    public static DamagedShipsDAO dsDAO = (DamagedShipsDAO) DAOFactory.get(DamagedShipsDAO.class);

    private final int groupingMultiplier;
    private int energyProduction = 0;
    private int weaponEnergy = 0;
    private int weaponEnergyTotal = 0;
    private int internalEnergyConsumption = 0;
    private final ShipDesign sd;
    private final Chassis c;
    private final static int SHIELD_PARATRON = 3;
    private final static int SHIELD_HUSHIELD = 2;
    private final static int SHIELD_PRALLSHIELD = 1;

    // Constructor for Battleships
    public BattleShipNew(int designId, int shipFleetId, int count) {
        super(designId, shipFleetId, count);
        sd = sdDAO.findById(designId);
        c = cDAO.findById(sd.getChassis());

        this.name = sd.getName();
        this.count = count;
        this.speed = sd.getStandardSpeed();
        this.chassisSize = sd.getChassis();
        this.designId = designId;
        this.purpose = sd.getType();
        this.primaryTarget = sd.getPrimaryTarget();
        this.isDefenseStation = false;
        this.isPlanetaryDefense = false;
        this.shipFleetId = shipFleetId;

        this.groupingMultiplier = c.getMinBuildQty();
        this.restCount = this.groupingMultiplier * this.orgCount;

        initializeAllModules();
    }

    // Constructor for plattform defense
    public BattleShipNew(int designId, int count) throws Exception {
        super(designId, count);
        sd = sdDAO.findById(designId);
        if (sd == null) {
            DebugBuffer.error("Found invalid space station design ["+designId+"] Count: " + count);           
        }
        
        c = cDAO.findById(sd.getChassis());

        this.name = sd.getName();
        this.count = count;
        this.speed = 0f;
        this.chassisSize = sd.getChassis();
        this.designId = designId;
        this.purpose = sd.getType();
        this.primaryTarget = sd.getPrimaryTarget();
        this.isDefenseStation = true;
        this.isPlanetaryDefense = false;

        this.groupingMultiplier = 1;
        this.restCount = this.orgCount;

        initializeAllModules();
    }

    private void initializeAllModules() {
        int groupingMultiplier = c.getMinBuildQty();

        ShipTypeAdjustment sta = staDAO.findByType(sd.getType());

        // Check trough modules
        ArrayList<WeaponModule> weaponArray = new ArrayList<WeaponModule>();
        int weaponEnergyConsumption = 0;
        int shieldEnergyConsumption = 0;
        int fixedEnergyConsumption = 0;
        int armorValue = 0;
        int shieldHpPS = 0;
        int shieldHpHU = 0;
        int shieldHpPA = 0;
        int energyPS = 0;
        int energyHU = 0;
        int energyPA = 0;

        ShipDesignDetailed sdd = new ShipDesignDetailed(getDesignId());
        HashMap<Integer, Integer> modules = sdd.getModuleCount();

        //  rs = stmt.executeQuery("SELECT m.name, m.type, m.value, m.energy, m.chassisSize, m.isConstruct, dm.count, m.id FROM designmodule dm, module m WHERE dm.designId=" + designId + " AND m.id=dm.moduleId");
        setHp((int) ((float) sdd.getBase().getStructure() * sta.getStructureFactor()) * groupingMultiplier);
        DebugBuffer.addLine(DebugLevel.DEBUG, "Hp of ship = " + getHp());

        int totalRange = 0;
        int weaponCount = 0;
        int maxTargetAllocation = 0;
        int fireSpeed = 0;

        for (Map.Entry<Integer, Integer> moduleEntry : modules.entrySet()) {
            Module m = mDAO.findById(moduleEntry.getKey());
            int modCount = moduleEntry.getValue();

            ModuleType type = m.getType();

            DebugBuffer.addLine(DebugLevel.DEBUG, "Check moduel " + m.getName() + " with id " + moduleEntry.getKey() + " and type " + m.getType());
            DebugBuffer.addLine(DebugLevel.DEBUG, "Type of corresponding moduleObjekt is " + sdd.getModule(m.getId()));
            if (type == ModuleType.ARMOR) {
                switch (moduleEntry.getKey()) {
                    case (2):
                        armorType = 1;
                        break;
                    case (21):
                        armorType = 2;
                        break;
                    case (22):
                        armorType = 3;
                        break;
                    case (23):
                        armorType = 4;
                        break;
                }
            } else if (type == ModuleType.WEAPON) {
                WeaponModule wm = new WeaponModule();
                Weapon w = sdd.getWeapon(moduleEntry.getKey());

                log.debug("["+this.getName()+" Checking module " + m.getName() + " Range is " + w.getRange());

                wm.setWeaponId(m.getId());

                wm.setMultifire(w.getFireRate());
                wm.setMultifired(0);

                fireSpeed += w.getFireRate() * modCount;

                maxTargetAllocation += wm.getMultifire() * modCount;

                wm.setRange(w.getRange());

                totalRange += w.getRange() * modCount;
                weaponCount += modCount;

                DebugBuffer.addLine(DebugLevel.DEBUG, "Multifire = " + wm.getMultifire() + " for weapon " + w.getName());

                wm.setAttackPower((int) ((sta.getAttackFactor() * (float) w.getAttackStrength()) / wm.getMultifire()) * groupingMultiplier);
                wm.setAccuracy(w.getAccuracy());
                wm.setCount(modCount * wm.getMultifire());
                wm.setTotalCount(modCount * count * wm.getMultifire());
                wm.setEnergyConsumption((int) (w.getEnergyConsumption() / wm.getMultifire()) * groupingMultiplier);
                weaponEnergyConsumption += wm.getEnergyConsumption() * wm.getCount();
                // DebugBuffer.addLine("Add weapons to " + bs.getName());

                weaponArray.add(wm);
            } else if (type == ModuleType.ENGINE) { //
                // TODO internalEnergyConsumption += tmpEnergy;
                Engine e = sdd.getEngine(moduleEntry.getKey());
                fixedEnergyConsumption += e.getEnergyConsumption() * modCount * groupingMultiplier;     
            } else if (type == ModuleType.CHASSIS) {
                // chassis - NOT USED
            } else if (type == ModuleType.COMPUTER) {
                Computer c = sdd.getComputer(moduleEntry.getKey());

                switch (moduleEntry.getKey()) {
                    case (803): // Microprozessor
                        damageBonus += c.getDamageBonus() / 100f;
                        targetFireBonus += c.getTargetFireAccBonus() / 100f;
                        break;
                    case (800): // Positronik
                        damageBonus += c.getDamageBonus() / 100f;
                        targetFireBonus += c.getTargetFireAccBonus() / 100f;
                        break;
                    case (802): // Syntronik
                        damageBonus += c.getDamageBonus() / 100f;
                        targetFireBonus += c.getTargetFireAccBonus() / 100f;
                        break;
                }
                // Computer (read out values)
                fixedEnergyConsumption += c.getEnergyConsumption() * modCount * groupingMultiplier; 
            } else if (type == ModuleType.SPECIAL) {
                SpecialModule specM = sdd.getSpecial(moduleEntry.getKey());

                switch (moduleEntry.getKey()) {
                    case (700): // Anti - Positioning
                        targetFireDefenseBonus -= specM.getDecreasedTargetFireEff() / 100d;
                        break;
                    case (701): // Virtual Image Creator
                        targetFireDefenseBonus -= specM.getDecreasedTargetFireEff() / 100d;
                        damageDefenseBonus -= specM.getDefenseBonus() / 100d;
                        break;
                }
                
                fixedEnergyConsumption += specM.getEnergyConsumption() * modCount * groupingMultiplier;                    
                // Stuff like Virtual Image Creater, Anti-Detection-Shield
            } else if (type == ModuleType.SHIELD) {
                Shield s = sdd.getShield(moduleEntry.getKey());

                switch (moduleEntry.getKey()) {
                    case (50):
                        shieldEnergyConsumption += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                        shieldHpPS += (int) ((float) s.getDefenseStrength() * (float) modCount * sta.getShieldFactor()) * groupingMultiplier;
                        energyPS += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                        break;
                    case (51):
                        shieldEnergyConsumption += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                        shieldHpHU += (int) ((float) s.getDefenseStrength() * (float) modCount * sta.getShieldFactor()) * groupingMultiplier;
                        energyHU += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                        break;
                    case (52):
                        shieldEnergyConsumption += (s.getEnergyConsumption() * modCount) * groupingMultiplier;
                        log.debug("Adding Paratron with Strength of " + s.getDefenseStrength() + " modcount="+modCount+" ShieldFactor="+sta.getShieldFactor()+" groupingMultiplier="+groupingMultiplier);
                        shieldHpPA += (int) ((float) s.getDefenseStrength() * (float) modCount * sta.getShieldFactor()) * groupingMultiplier;
                        energyPA += (s.getEnergyConsumption() * modCount * groupingMultiplier);
                        break;
                }
            } else if (false) { // 9??
                // FREE
            } else if (type == ModuleType.REACTOR) {
                Reactor r = sdd.getReactor(moduleEntry.getKey());
                energyProduction += (r.getEnergyProduction() * modCount * groupingMultiplier);
            } else if ((type == ModuleType.TRANSPORT_POPULATION) || (type == ModuleType.TRANSPORT_RESSOURCE) || (type == ModuleType.COLONISATION)) {
                SimpleModule sm = (SimpleModule) sdd.getModule(moduleEntry.getKey());
                fixedEnergyConsumption += sm.getEnergyConsumption() * modCount * groupingMultiplier;
            } else if (type == ModuleType.HANGAR) {
                SimpleModule sm = (SimpleModule) sdd.getModule(moduleEntry.getKey());
                fixedEnergyConsumption += sm.getEnergyConsumption() * modCount * groupingMultiplier;                
                // TODO fixedEnergyConsumption += tmpEnergy;
            }
        }

        // setMaxTargetAllocation(maxTargetAllocation);
        weapons = weaponArray;

        if (weaponCount == 0) {
            averageWeaponRange = 1;
            log.debug("["+this.getName()+"] Set Average Weapon range to " + averageWeaponRange + " TOTALRANGE="+totalRange+" WEAPONCOUNT="+weaponCount);
        } else {
            averageWeaponRange = ((float)totalRange / weaponCount);
            log.debug("["+this.getName()+"] Set Average Weapon range to " + averageWeaponRange + " TOTALRANGE="+totalRange+" WEAPONCOUNT="+weaponCount);
        }
        // setAverageRange((float) totalRange / weaponCount);
        // setFireSpeed(fireSpeed);
        // setSizeLevel((float) Math.log10(sdd.getBase().getStructure()));
        // bs.setHp(bs.getHp() + (bs.getHp() / 100 * armorValue));
        
        internalEnergyConsumption = fixedEnergyConsumption;
        log.debug("["+this.getName()+"] Set Internal Energy consumption to "+fixedEnergyConsumption);
        
        ShieldDefinition sDef = new ShieldDefinition();

        sDef.setShield_PS(shieldHpPS);
        sDef.setShield_HU(shieldHpHU);
        sDef.setShield_PA(shieldHpPA);
        sDef.setSharedShield_PS(sDef.getShield_PS() * count);
        sDef.setSharedShield_HU(sDef.getShield_HU() * count);
        sDef.setSharedShield_PA(sDef.getShield_PA() * count);
        log.debug("Shared shields for " + this.name + " are " + sDef.getSharedShield_PA());
        sDef.setEnergyNeed_PS(energyPS);
        sDef.setEnergyNeed_HU(energyHU);
        sDef.setEnergyNeed_PA(energyPA);

        totalHp = getHp() * count;
        weaponEnergy = weaponEnergyConsumption;
        weaponEnergyTotal = weaponEnergyConsumption * count;

        sDef.setShieldEnergyPerShip(shieldEnergyConsumption);
        sDef.setShieldEnergy(shieldEnergyConsumption * count);
       
        this.setShields(sDef);
    }

    /*
     * HANDLED IN COMBATUNIT CLASS NO OVERRIDE NECESSARY
    protected double getReductionThroughShieldsNew(AbstractCombatUnit abs, AbstractCombatUnit absAtt, WeaponModule wm, int maxHitable, HashMap<Integer, Double> detailDamageAtt, ShieldDefenseDetail sdd) {
        double reduction = 0d;
        double damageAttacker = detailDamageAtt.get(wm.getWeaponId());
        HashMap<Double, Double> weightedReductionMap = new HashMap<Double, Double>();

        double attackerDistraction = absAtt.getDistraction();
        double concentrationModifier = 0.2d + (0.8d / 1d * attackerDistraction);
        concentrationModifier = 1d;

        log.debug("PARATRON (Shared): " + abs.getShields().getSharedShield_PA() + " COUNT: " + abs.getCount() + " MaxHitable: " + maxHitable);

        float psShieldStr = (abs.getShields().getSharedShield_PS() / abs.getCount()) * maxHitable;
        float huShieldStr = (abs.getShields().getSharedShield_HU() / abs.getCount()) * maxHitable;
        float paShieldStr = (abs.getShields().getSharedShield_PA() / abs.getCount()) * maxHitable;

        double relShieldStrPA = paShieldStr * concentrationModifier;
        double relShieldStrHU = huShieldStr * concentrationModifier;
        double relShieldStrPS = psShieldStr * concentrationModifier;

        double currReduction = 0d;

        double absorbPS = abs.prallPenetration.get(wm.getWeaponId());
        double absorbHU = abs.huPenetration.get(wm.getWeaponId());
        double absorbPA = abs.paraPenetration.get(wm.getWeaponId());

        double wastedDamage = 0d;

        if (paShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbPA;

            if (absorbedDamage > relShieldStrPA) {
                // abs.sc_LoggingEntry.incPA_DamagePenetrated(absorbedDamage - relShieldStrPA);
                absorbedDamage = relShieldStrPA;
            }

            // abs.sc_LoggingEntry.incPA_DamageAbsorbed(absorbedDamage);
            log.debug("[PA] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrPA + " (Abs. Damage: " + absorbedDamage + " / absorbPA: " + absorbPA + ")");


            relShieldStrPA -= absorbedDamage;
            double adjust = 1d / damageAttacker * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsPA(sdd.getAbsPA() + damageAttacker);
                abs.getShields().setSharedShield_PA((float) abs.getShields().getSharedShield_PA() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, currReduction + adjust * absorbPA);
                log.debug("CurrReduction: " + currReduction + " Adjust: " + adjust);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                abs.getShields().setSharedShield_PA((float) abs.getShields().getSharedShield_PA() - effAbsorbedDamage);
                sdd.setAbsPA(sdd.getAbsPA() + effAbsorbedDamage);
                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(effAbsorbedDamage);
                damageAttacker -= effAbsorbedDamage;
                sdd.setPenPA(sdd.getPenPA() + damageAttacker);
                // abs.sc_LoggingEntry.incPA_DamagePenetrated(damageAttacker);
            }
        }

        if (huShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbHU;

            if (absorbedDamage > relShieldStrHU) {
                // abs.sc_LoggingEntry.incHU_DamagePenetrated(absorbedDamage - relShieldStrHU);
                absorbedDamage = relShieldStrHU;
            }

            log.debug("[HU] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrHU + " (Abs. Damage: " + absorbedDamage + " / absorbHU: " + absorbHU + ")");


            // abs.sc_LoggingEntry.incHU_DamageAbsorbed(absorbedDamage);
            relShieldStrHU -= absorbedDamage;
            double adjust = 1d / damageAttacker * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsHU(sdd.getAbsHU() + damageAttacker);
                abs.getShields().setSharedShield_HU((float) abs.getShields().getSharedShield_HU() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, currReduction + adjust * absorbHU);
                // currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbHU);
                log.debug("CurrReduction: " + currReduction + " Adjust: " + adjust);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                abs.getShields().setSharedShield_HU((float) abs.getShields().getSharedShield_HU() - effAbsorbedDamage);
                sdd.setAbsHU(sdd.getAbsHU() + effAbsorbedDamage);
                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(effAbsorbedDamage);
                damageAttacker -= effAbsorbedDamage;
                sdd.setPenHU(sdd.getPenHU() + damageAttacker);
                // abs.sc_LoggingEntry.incHU_DamagePenetrated(damageAttacker);
            }
        }

        if (psShieldStr > 0) {
            double absorbedDamage = damageAttacker * absorbPS;

            if (absorbedDamage > relShieldStrPS) {
                // abs.sc_LoggingEntry.incPS_DamagePenetrated(absorbedDamage - relShieldStrPS);
                absorbedDamage = relShieldStrPS;
            }

            // abs.sc_LoggingEntry.incPS_DamageAbsorbed(absorbedDamage);
            relShieldStrPS -= absorbedDamage;
            double adjust = 1d / damageAttacker * absorbedDamage;

            if (damageAttacker <= absorbedDamage) {
                currReduction = 1d;
                sdd.setAbsPS(sdd.getAbsPS() + damageAttacker);
                abs.getShields().setSharedShield_PS((float) abs.getShields().getSharedShield_PS() - (float) damageAttacker);
                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(damageAttacker);
                damageAttacker = 0;
            } else {
                currReduction = Math.max(currReduction, adjust * absorbHU);
                // currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbPS);
                float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                abs.getShields().setSharedShield_PS((float) abs.getShields().getSharedShield_PS() - effAbsorbedDamage);
                sdd.setAbsPS(sdd.getAbsPS() + effAbsorbedDamage);
                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(effAbsorbedDamage);
                damageAttacker -= effAbsorbedDamage;
                sdd.setPenPS(sdd.getPenPS() + damageAttacker);
                // abs.sc_LoggingEntry.incPS_DamagePenetrated(damageAttacker);
            }
        }

        return currReduction;
    }

    private double getReductionThroughShields(AbstractCombatUnit abs, AbstractCombatUnit absAtt, double damageAttackerOrg, HashMap<Integer, Double> detailDamageAtt, ShieldDefenseDetail sdd) {
        double reduction = 0d;
        double damageAttacker = damageAttackerOrg;
        HashMap<Double, Double> weightedReductionMap = new HashMap<Double, Double>();

        double attackerDistraction = absAtt.getDistraction();
        double concentrationModifier = 0.2d + (0.8d / 1d * attackerDistraction);

        for (WeaponModule wm : absAtt.getWeapons()) {
            float psShieldStr = abs.getShields().getSharedShield_PS();
            float huShieldStr = abs.getShields().getSharedShield_HU();
            float paShieldStr = abs.getShields().getSharedShield_PA();

            double relShieldStrPA = paShieldStr / abs.getCount() * concentrationModifier;
            double relShieldStrHU = huShieldStr / abs.getCount() * concentrationModifier;
            double relShieldStrPS = psShieldStr / abs.getCount() * concentrationModifier;

            double currReduction = 0d;

            double absorbPS = abs.prallPenetration.get(wm.getWeaponId());
            double absorbHU = abs.huPenetration.get(wm.getWeaponId());
            double absorbPA = abs.paraPenetration.get(wm.getWeaponId());

            if (paShieldStr > 0) {
                double absorbedDamage = damageAttacker * absorbPA;

                if (absorbedDamage > relShieldStrPA) {
                    // abs.sc_LoggingEntry.incPA_DamagePenetrated(absorbedDamage - relShieldStrPA);
                    absorbedDamage = relShieldStrPA;
                }

                // abs.sc_LoggingEntry.incPA_DamageAbsorbed(absorbedDamage);
                log.debug("[PA] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrPA + " (Abs. Damage: " + absorbedDamage + " / absorbPA: " + absorbPA + ")");


                relShieldStrPA -= absorbedDamage;
                double adjust = 1d / damageAttacker * absorbedDamage;

                if (damageAttacker <= absorbedDamage) {
                    currReduction = 1d;
                    sdd.setAbsPA(sdd.getAbsPA() + damageAttacker);
                    abs.getShields().setSharedShield_PA((float) abs.getShields().getSharedShield_PA() - (float) damageAttacker);
                    // abs.sc_LoggingEntry.incPA_DamageAbsorbed(damageAttacker);
                    damageAttacker = 0;
                } else {
                    currReduction = Math.max(currReduction, (1d - ((1d - adjust) / 2d)) * absorbPA);
                    log.debug("CurrReduction: " + currReduction + " Adjust: " + adjust);
                    float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                    abs.getShields().setSharedShield_PA((float) abs.getShields().getSharedShield_PA() - effAbsorbedDamage);
                    sdd.setAbsPA(sdd.getAbsPA() + effAbsorbedDamage);
                    // abs.sc_LoggingEntry.incPA_DamageAbsorbed(effAbsorbedDamage);
                    damageAttacker -= effAbsorbedDamage;
                    sdd.setPenPA(sdd.getPenPA() + damageAttacker);
                    // abs.sc_LoggingEntry.incPA_DamagePenetrated(damageAttacker);
                }
            }

            if (huShieldStr > 0) {
                double absorbedDamage = damageAttacker * absorbHU;

                if (absorbedDamage > relShieldStrHU) {
                    // abs.sc_LoggingEntry.incHU_DamagePenetrated(absorbedDamage - relShieldStrHU);
                    absorbedDamage = relShieldStrHU;
                }

                log.debug("[HU] Damage Attacker on " + abs.getName() + " is " + damageAttacker + " Defender shields: " + relShieldStrHU + " (Abs. Damage: " + absorbedDamage + " / absorbHU: " + absorbHU + ")");


                // abs.sc_LoggingEntry.incHU_DamageAbsorbed(absorbedDamage);
                relShieldStrHU -= absorbedDamage;
                double adjust = 1d / damageAttacker * absorbedDamage;

                if (damageAttacker <= absorbedDamage) {
                    currReduction = 1d;
                    sdd.setAbsHU(sdd.getAbsHU() + damageAttacker);
                    abs.getShields().setSharedShield_HU((float) abs.getShields().getSharedShield_HU() - (float) damageAttacker);
                    // abs.sc_LoggingEntry.incHU_DamageAbsorbed(damageAttacker);
                    damageAttacker = 0;
                } else {
                    currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbHU);
                    log.debug("CurrReduction: " + currReduction + " Adjust: " + adjust);
                    float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                    abs.getShields().setSharedShield_HU((float) abs.getShields().getSharedShield_HU() - effAbsorbedDamage);
                    sdd.setAbsHU(sdd.getAbsHU() + effAbsorbedDamage);
                    // abs.sc_LoggingEntry.incHU_DamageAbsorbed(effAbsorbedDamage);
                    damageAttacker -= effAbsorbedDamage;
                    sdd.setPenHU(sdd.getPenHU() + damageAttacker);
                    // abs.sc_LoggingEntry.incHU_DamagePenetrated(damageAttacker);
                }
            }

            if (psShieldStr > 0) {
                double absorbedDamage = damageAttacker * absorbPS;

                if (absorbedDamage > relShieldStrPS) {
                    // abs.sc_LoggingEntry.incPS_DamagePenetrated(absorbedDamage - relShieldStrPS);
                    absorbedDamage = relShieldStrPS;
                }

                // abs.sc_LoggingEntry.incPS_DamageAbsorbed(absorbedDamage);
                relShieldStrPS -= absorbedDamage;
                double adjust = 1d / damageAttacker * absorbedDamage;

                if (damageAttacker <= absorbedDamage) {
                    currReduction = 1d;
                    sdd.setAbsPS(sdd.getAbsPS() + damageAttacker);
                    abs.getShields().setSharedShield_PS((float) abs.getShields().getSharedShield_PS() - (float) damageAttacker);
                    // abs.sc_LoggingEntry.incPS_DamageAbsorbed(damageAttacker);
                    damageAttacker = 0;
                } else {
                    currReduction = Math.max(currReduction, currReduction + (1d - currReduction) * (1d - ((1d - adjust) / 2d)) * absorbPS);
                    float effAbsorbedDamage = (float) damageAttacker * (float) currReduction;

                    abs.getShields().setSharedShield_PS((float) abs.getShields().getSharedShield_PS() - effAbsorbedDamage);
                    sdd.setAbsPS(sdd.getAbsPS() + effAbsorbedDamage);
                    // abs.sc_LoggingEntry.incPS_DamageAbsorbed(effAbsorbedDamage);
                    damageAttacker -= effAbsorbedDamage;
                    sdd.setPenPS(sdd.getPenPS() + damageAttacker);
                    // abs.sc_LoggingEntry.incPS_DamagePenetrated(damageAttacker);
                }
            }

            double share = 1d / damageAttackerOrg * detailDamageAtt.get(wm.getWeaponId());
            log.debug("Adding weapon " + wm.getWeaponId() + " with share: " + share + " as DA=" + damageAttackerOrg + " and DDA=" + detailDamageAtt.get(wm.getWeaponId()));
            weightedReductionMap.put(share, currReduction);
        }

        // Calculate Reduction from weighted reduction
        for (Map.Entry<Double, Double> entry : weightedReductionMap.entrySet()) {
            log.debug("Weighted Entry found: " + entry.getValue() + " Share: " + entry.getKey());
            reduction += entry.getValue() * entry.getKey();
        }

        return reduction;
    }
    */

    @Override
    public void reloadShieldsAndWeapons() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isDefenseStation() {
        return isDefenseStation;
    }

    @Override
    public boolean isPlanetaryDefense() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasTargetFound() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setShipFleetId(int shipFleetId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * @return the sd
     */
    @Override
    public ShipDesign getShipDesign() {
        return sd;
    }

    @Override
    public int getArmor() {
        return getArmorType();
    }

    /*
    public DamagedShips getDs() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    */
    
    @Override
    public double getDistraction() {
        return distraction;
    }

    @Override
    public int[] getDamageDistribution(int count, double temp_destroyed) {
        // log.debug(this.name + " - Requesting damage distribution COUNT: " + count + " TmpDestroyed: " + temp_destroyed);
        double org_temp_destroyed = this.getTempDestroyedStart();
        boolean destroyed = temp_destroyed >= 1d;

        temp_destroyed = Math.min(temp_destroyed, 1d);

        int[] distribution = new int[6];
        distribution[0] = count;

        // We need to read the current damage situation
        // into the array and then adjust it to the new damage value
        if (org_temp_destroyed > 0d) {
            log.debug("Org Temp Destroyed was " + org_temp_destroyed);

            ArrayList<at.darkdestiny.core.model.DamagedShips> dsList = dsDAO.findById(shipFleetId);
            for (at.darkdestiny.core.model.DamagedShips ds : dsList) {
                if (ds.getDamageLevel() == EDamageLevel.MINORDAMAGE) {
                    distribution[1] = ds.getCount();
                } else if (ds.getDamageLevel() == EDamageLevel.LIGHTDAMAGE) {
                    distribution[2] = ds.getCount();
                } else if (ds.getDamageLevel() == EDamageLevel.MEDIUMDAMAGE) {
                    distribution[3] = ds.getCount();
                } else if (ds.getDamageLevel() == EDamageLevel.HEAVYDAMAGE) {
                    distribution[4] = ds.getCount();
                }

                distribution[0] -= ds.getCount();
            }
        }

        if (destroyed) {
            distribution[0] = 0;
            distribution[1] = 0;
            distribution[2] = 0;
            distribution[3] = 0;
            distribution[4] = 0;
            distribution[5] = count;
            return distribution;
        }

        double destroyedLeft = temp_destroyed - org_temp_destroyed;

        double singleLossShare = 1d / count;
        double singleDamageLevelShare = 1d / count / 5d;

        // double totalLoss = 0.3d + Math.random() * 0.4d;
        double totalLoss = 0.5d;

        if (temp_destroyed < singleLossShare) {
            totalLoss = 0d;
        }
        if (temp_destroyed >= 1d) {
            totalLoss = 1d;
        }

        double totalLossToDistribute = (temp_destroyed - org_temp_destroyed) * totalLoss;
        // log.debug("totalLossToDistribute: " + totalLossToDistribute);

        // Get Total Losses
        while ((totalLossToDistribute > 0) && (!(distribution[5] >= count))) {
            distribution[5]++;
            distribution[0]--;
            totalLossToDistribute -= singleLossShare;
            destroyedLeft -= singleLossShare;
        }

        // log.debug("DamageDistribution 0 = " + distribution[0]);
        // log.debug("DamageDistribution 5 = " + distribution[5]);

        // Distribute Damage
        while ((destroyedLeft > 0) && (!(distribution[5] >= count))) {
            // log.debug("Destroyed Left = " + destroyedLeft);
            // log.debug("DestroyedLeft = " + destroyedLeft + " distribution[5]="+distribution[5]+" COUNT="+count);

            int maxDamageLevel = (int) Math.floor(destroyedLeft / singleDamageLevelShare);
            // log.debug("MaxDamageLevel = " + maxDamageLevel);

            if (maxDamageLevel > 0) {
                int dl = getRandomDamageLevel(count, distribution);
                if (dl == -1) {
                    log.debug("COUNT: " + count);
                    for (int i = 0; i < 5;i++) {
                        log.debug("DamageLevel: " + i + " VALUE:" + distribution[i]);
                    }
                } else {
                    // log.debug("Move 1 unit from damagelevel " + dl + " ["+distribution[dl]+"] to " + (dl + 1) + " ["+distribution[dl + 1]+"]");
                }

                distribution[dl]--;
                distribution[dl + 1]++;
                destroyedLeft -= singleDamageLevelShare;
            } else {
                break;
            }
            // Get a unit and put it to proposed DamageLevel
        }

        for (int i = 0; i < 5; i++) {
            this.damageDistribution[i] = distribution[i];
        }

        return distribution;
    }

    private int getRandomDamageLevel(int count, int[] distribution) {
        // log.debug("PROCESSING " + this.name);

        double NO_DAMAGE_MULTIPLIER = 1d;
        double LIGHT_DAMAGE_MULTIPLIER = 2d;
        double SERIOUS_DAMAGE_MULTIPLIER = 3d;
        double HEAVY_DAMAGE_MULTIPLIER = 4d;
        double CRITICAL_DAMAGE_MULTIPLIER = 5d;

        double highestPercentage = -1;
        int takeMe = -1;

        for (int i = 0; i <= 4; i++) {
            double actPerc = (100d / count * distribution[i]);
            switch (i) {
                case (0):
                    actPerc = actPerc * NO_DAMAGE_MULTIPLIER;
                    break;
                case (1):
                    actPerc = actPerc * LIGHT_DAMAGE_MULTIPLIER;
                    break;
                case (2):
                    actPerc = actPerc * SERIOUS_DAMAGE_MULTIPLIER;
                    break;
                case (3):
                    actPerc = actPerc * HEAVY_DAMAGE_MULTIPLIER;
                    break;
                case (4):
                    actPerc = actPerc * CRITICAL_DAMAGE_MULTIPLIER;
                    break;
            }

            // log.debug("COUNT: " + count + " I: " + i + " actPerc: " + actPerc);

            if (actPerc > highestPercentage) {
                highestPercentage = actPerc;
                takeMe = i;
            }

            // log.debug("TAKEME: " + takeMe);
        }

        return takeMe;
    }

    /**
     * @return the groupingMultiplier
     */
    public int getGroupingMultiplier() {
        return groupingMultiplier;
    }

    /**
     * @return the energyProduction
     */
    public int getEnergyProduction() {
        return energyProduction;
    }

    /**
     * @return the weaponEnergy
     */
    public int getWeaponEnergy() {
        return weaponEnergy;
    }

    /**
     * @return the weaponEnergyTotal
     */
    public int getWeaponEnergyTotal() {
        return weaponEnergyTotal;
    }

    /**
     * @return the internalEnergyConsumption
     */
    public int getInternalEnergyConsumption() {
        return internalEnergyConsumption;
    }
}
