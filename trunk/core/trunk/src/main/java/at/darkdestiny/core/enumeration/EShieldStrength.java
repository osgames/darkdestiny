/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Stefan
 */
public enum EShieldStrength {
    NOT_SHIELDED, LIGHT_SHIELDED, MEDIUM_SHIELDED, HEAVY_SHIELDED, VERY_HEAVY_SHIELDED;
}
