package at.darkdestiny.core.databuffer.fleet;

import at.darkdestiny.core.*;

public class AbsoluteCoordinate implements ICoordinate {
    private int x;
    private int y;
    
    public AbsoluteCoordinate(RelativeCoordinate rc1, RelativeCoordinate rc2, int startTime, int stopTime) {
        AbsoluteCoordinate ac1 = rc1.toAbsoluteCoordinate();
        AbsoluteCoordinate ac2 = rc2.toAbsoluteCoordinate();
        
        double progress = 100d / (double)(stopTime - startTime) * (double)(GameUtilities.getCurrentTick2() - startTime);  
        
        int diffX = ac2.getX() - ac1.getX();
        int diffY = ac2.getY() - ac1.getY();
        
        this.x = (int)(ac1.getX() + (diffX * progress));
        this.y = (int)(ac1.getY() + (diffY * progress));
    }
    
    public AbsoluteCoordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }    

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public double distanceTo(AbsoluteCoordinate ac) {        
        double a = this.getX() - ac.getX();
        double b = this.getY() - ac.getY();
        
        return Math.sqrt(a * a + b * b);        
    }    
    
    @Override
    public String toString() {
        return ("AC X/Y: " + x + "/" + y);
    }
}
