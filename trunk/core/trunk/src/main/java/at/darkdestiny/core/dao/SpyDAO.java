/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Spy;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class SpyDAO extends ReadWriteTable<Spy> implements GenericDAO {
    public Spy findById(int id) {
        Spy spy = new Spy();
        spy.setId(id);
        return (Spy)get(spy);
    }
}
