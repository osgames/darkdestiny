/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.construction.ConstructionAbortRefund;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;

/**
 *
 * @author Stefan
 */
public class StartLocationResult extends BaseResult {
    private final int choosenGalaxy;
    private final int choosenSystem;
    private final double distanceToNextPlayer;
    
    
    public StartLocationResult(int choosenGalaxy, int choosenSystem, double distanceToNextPlayer) {
        super(false);

        this.choosenGalaxy = choosenGalaxy;
        this.choosenSystem = choosenSystem;
        this.distanceToNextPlayer = distanceToNextPlayer;
    }

    public StartLocationResult(String error) {
        super(error,true);

        this.choosenGalaxy = 0;
        this.choosenSystem = 0;
        this.distanceToNextPlayer = 0d;
    }    

    /**
     * @return the choosenGalaxy
     */
    public int getChoosenGalaxy() {
        return choosenGalaxy;
    }

    /**
     * @return the choosenSystem
     */
    public int getChoosenSystem() {
        return choosenSystem;
    }

    /**
     * @return the distanceToNextPlayer
     */
    public double getDistanceToNextPlayer() {
        return distanceToNextPlayer;
    }
}
