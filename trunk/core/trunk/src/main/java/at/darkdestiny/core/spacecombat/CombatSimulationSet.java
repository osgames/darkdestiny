/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.dao.PlayerFleetDAO;
import at.darkdestiny.core.dao.ShipFleetDAO;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.interfaces.ISimulationSet;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class CombatSimulationSet implements ISimulationSet{
    PlayerFleetDAO pfDAO = (PlayerFleetDAO)DAOFactory.get(PlayerFleetDAO.class);
    ShipFleetDAO sfDAO = (ShipFleetDAO)DAOFactory.get(ShipFleetDAO.class);
    private String battleClass = "at.darkdestiny.core.spacecombat.combatcontroller.SpaceCombat";
    private final CombatReportGenerator.CombatType combatType;
    private final RelativeCoordinate relLocation;
    private final AbsoluteCoordinate absLocation;
    private final CombatReportGenerator crg;

    private RelativeCoordinate relativeCoordinate;

    private final int userIdAttacker;
    private final int userIdDefender;

    private HashMap<Integer,Integer> shipListAttacker;
    private HashMap<Integer,Integer> shipListDefender;

    private int genAttFleetId = 0;
    private int genDefFleetId = 0;

    private ArrayList<PlayerFleet> fleets = new ArrayList<PlayerFleet>();
    private ArrayList<ShipFleet> ships = new ArrayList<ShipFleet>();

    private long attackerCost = 0l;
    private long defenderCost = 0l;

    public CombatSimulationSet(int userIdAttacker, int userIdDefender,
            HashMap<Integer,Integer> shipListAttacker, HashMap<Integer,Integer> shipListDefender) {
        combatType = CombatReportGenerator.CombatType.SPACE;
        crg = new CombatReportGenerator();
        relLocation = new RelativeCoordinate(0,2600);
        absLocation = new AbsoluteCoordinate(0,0);

        this.userIdAttacker = userIdAttacker;
        this.userIdDefender = userIdDefender;

        this.shipListAttacker = shipListAttacker;
        this.shipListDefender = shipListDefender;

        generateFleets();
    }

    private void generateFleets() {
        PlayerFleet pfAtt = new PlayerFleet();
        pfAtt.setUserId(userIdAttacker);
        pfAtt.setName("Attacker Fleet");

        PlayerFleet pfDef = new PlayerFleet();
        pfDef.setUserId(userIdDefender);
        pfDef.setName("Defender Fleet");

        pfAtt = pfDAO.add(pfAtt);
        genAttFleetId = pfAtt.getId();

        pfDef = pfDAO.add(pfDef);
        genDefFleetId = pfDef.getId();

        ArrayList<ShipFleet> sfListAtt = new ArrayList<ShipFleet>();
        ArrayList<ShipFleet> sfListDef = new ArrayList<ShipFleet>();

        for (Map.Entry<Integer,Integer> attShips : shipListAttacker.entrySet()) {
            ShipDesignExt sde = new ShipDesignExt(attShips.getKey());
            attackerCost += (long)(sde.getRessCost().getRess(Ressource.CREDITS) * attShips.getValue());

            ShipFleet sf = new ShipFleet();
            sf.setFleetId(genAttFleetId);
            sf.setDesignId(attShips.getKey());
            sf.setCount(attShips.getValue());
            sfListAtt.add(sf);
        }

        for (Map.Entry<Integer,Integer> defShips : shipListDefender.entrySet()) {
            ShipDesignExt sde = new ShipDesignExt(defShips.getKey());
            defenderCost += (long)(sde.getRessCost().getRess(Ressource.CREDITS) * defShips.getValue());

            ShipFleet sf = new ShipFleet();
            sf.setFleetId(genDefFleetId);
            sf.setDesignId(defShips.getKey());
            sf.setCount(defShips.getValue());
            sfListDef.add(sf);
        }

        ships.addAll(sfDAO.insertAll(sfListAtt));
        ships.addAll(sfDAO.insertAll(sfListDef));

        fleets.add(pfAtt);
        fleets.add(pfDef);
    }

    public void cleanUp() {
        PlayerFleet pfDel = new PlayerFleet();
        pfDel.setId(genAttFleetId);
        pfDAO.remove(pfDel);

        pfDel.setId(genDefFleetId);
        pfDAO.remove(pfDel);

        for (ShipFleet sf : ships) {
            sfDAO.remove(sf);
        }
    }

    public CombatReportGenerator.CombatType getCombatType() {
        return combatType;
    }

    public RelativeCoordinate getRelLocation() {
        return relLocation;
    }

    public AbsoluteCoordinate getAbsLocation() {
        return absLocation;
    }

    public CombatReportGenerator getCombatReportGenerator() {
        return crg;
    }

    public ArrayList<PlayerFleet> getFleets() {
        return fleets;
    }

    public long getAttackerCost() {
        return attackerCost;
    }

    public long getDefenderCost() {
        return defenderCost;
    }

    @Override
    public HashMap<Integer, ArrayList<Integer>> getGroups() {
        return new HashMap<Integer, ArrayList<Integer>>();
    }

    /**
     * @return the battleClass
     */
    public String getBattleClass() {
        return battleClass;
    }

    public int getPlanetOwner() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean hasHUShield() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean hasPAShield() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public HashMap<Integer, Integer> getGroundDefense() {
        throw new UnsupportedOperationException("Not supported yet.");
    }


}
