package at.darkdestiny.core.admin;

import at.darkdestiny.core.admin.techtree.TechTree;
import at.darkdestiny.core.admin.techtree.TechnologieTechTreeEntry;
import at.darkdestiny.core.admin.techtree.display.DisplayConnection;
import at.darkdestiny.core.admin.techtree.display.DisplayTechTreeHelper;
import at.darkdestiny.core.admin.techtree.display.algorithms.Arranger;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

/**
 * Rendert diesen Technologiebaum in eine SVG-Graphic
 *
 * Für eine Einführung in SVG siehe:
 * 	   http://wiki.svg.org/SVG_and_HTML,
 *     http://de.wikipedia.org/wiki/Scalable_Vector_Graphics
 *
 * @author martin
 */
public class TechTreeDisplayAdmin {

	private static final int TEXT_SIZE = 12;
	private static final int IMG_WIDTH = 100 - 6;
	public static int BreiteProElement = 300;
	public static int HoeheProElement = 100;
	public static final int LINE_WIDTH = 1; // 1

	protected static TechTreeDisplayAdmin instance = new TechTreeDisplayAdmin();
	private TreeMap<Integer, DisplayTechTreeHelper> alleTechs;
	private int width;
	private int height;
	private List<List<DisplayTechTreeHelper>> allLevels;
	private List<DisplayTechTreeHelper> allNotShown;
	protected List<DisplayConnection> allLines;


	/**
	 *
	 */
	protected TechTreeDisplayAdmin() {
	}

	public static String getSize()
	{
		calculateSize();
//		return "width=\"1024\" height=\"600"+
//		"\" viewBox=\"-2 -2 "+(maxLaenge*TotaleBreiteProElement+2)+" "+(noLevels*TotaleHoeheProElement+2) +"\"";
		return "width=\""+(instance.width+4)+"\" height=\""+(instance.height+4) +
		"\" viewBox=\"-2 -2 "+(instance.width+2)+" "+(instance.height+2) +"\"";
	}

	public static void recreate()
	{
		instance.doRecreate();
	}

	public static void doCalculation()
	{
		instance.calculatePositions();
	}

	public static String showTree()
	{
		StringBuffer result = new StringBuffer();

		for (List<DisplayTechTreeHelper> al : instance.allLevels)
			for (DisplayTechTreeHelper d : al)
			{
			result.append("<!-- Zeichne: "+d.getTitle()+" -->\n");
			result.append("<rect x=\""+d.getX()+"\" y=\""+
					d.getY()+"\" width=\""+BreiteProElement+"\" height=\""+
					HoeheProElement+"\" " +
					"fill=\""+d.getColor()+"\" stroke=\"black\" stroke-width=\"1.5px\" />\n");
			result.append("<text x=\""+(d.getX()+3)+"\" y=\""+
					(d.getY()+3+TEXT_SIZE)+"\" font-size=\""+TEXT_SIZE+"px\" >"+d.getTechno().getPlainTitle()+"</text>\n");
			if ((d.getTechno() instanceof TechnologieTechTreeEntry) &&
					(!"".equals(d.getTechno().get("smallPic")) && (d.getTechno().get("smallPic") != null)))
			{
				result.append("<image x=\""+(d.getX()-3-IMG_WIDTH+BreiteProElement)+"\" y=\""+
						(d.getY()+3)+"\" width=\""+IMG_WIDTH+"px\" height=\""+(HoeheProElement-6)+"px\"" +
								" xlink:href=\""+((TechnologieTechTreeEntry)d.getTechno()).getSmallImgPath()+"\" />\n");
			}
			result.append("<text x=\""+(d.getX()+3)+"\" y=\""+
					(d.getY()+3+3*TEXT_SIZE)+"\" font-size=\""+(1.5f*TEXT_SIZE)+"px\" >"+d.getTechno().getName()+"</text>\n");

			List<DisplayTechTreeHelper> notShown = d.getDependentNoShow();
			for (int i = 0; i < notShown.size(); i++)
			{
				result.append("<rect x=\""+(d.getX()+3)+"\" y=\""+
						(d.getY()+3+(4f+i)*TEXT_SIZE)+"\" width=\""+(BreiteProElement-6)+"\" height=\""+
						TEXT_SIZE*1.5f+"\" " +
						"fill=\""+notShown.get(i).getColor()+"\" stroke=\""+notShown.get(i).getColor()+"\" stroke-width=\"0px\" />\n");
				result.append("<text x=\""+(d.getX()+3)+"\" y=\""+
					(d.getY()+3+(5+i)*TEXT_SIZE)+"\" font-size=\""+TEXT_SIZE+"px\" >Ausgeblendet: "+notShown.get(i).getTitle()+"</text>\n");
			}

		}

		for (DisplayConnection dc : instance.allLines)
		{
			result.append(dc.render());
		}

		return result.toString();
	}

	private int distanceBetween(String s, String s2, int max) {
		return distanceBetween(s, 0, s2, 0, max,true,true);
	}

	private int distanceBetween(String s, int idx, String s2, int idx2, int max, boolean doRecursiv1, boolean doRecursiv2) {
		int res = 0;
		int c1 = idx; int c2 = idx2;
		while ((c1 < s.length()) || (c2 < s2.length()))
		{
			if (c1 >= s.length())
			{
				c2++;
				res ++;
			}
			else if (c2 >= s2.length())
			{
				c1++;
				res ++;
			}
			else if (Character.toUpperCase(s.charAt(c1)) == Character.toUpperCase(s2.charAt(c2)))
			{
				c1 ++;
				c2 ++;
				doRecursiv2 = true;
				doRecursiv1 = true;
			}
			else { //Minimale differenz suchen
				if (res > max)
					return res;
				res ++;
				int res1 = distanceBetween(s, c1+1, s2, c2, max-1, true,false);
				if (res1 == 0)
					return res;
				int res2 = distanceBetween(s, c1, s2, c2+1, max-1, false, true);
				res += Math.min(res1, res2);
				return res;
			}
		}
		return res;
	}

	private void calculatePositions() {
		//Erster Schritt: Liste mit allen Einträgen erzeugen und so sortieren, dass in jeder Zeile nur die passenden Einträge stehen
		try {
			TechTreeAdmin.loadTechTree();
		} catch (Exception e) {
			DebugBuffer.writeStackTrace("Error in TechTreeDisplayAdmin, calculatePositions: ",e);
		}
		allNotShown = new ArrayList<DisplayTechTreeHelper>();
		allLines = new ArrayList<DisplayConnection>();
		alleTechs = new TreeMap<Integer,DisplayTechTreeHelper>();
		for (TechTree tt : TechTreeAdmin.allTechnologies.values())
			alleTechs.put(tt.getID(),new DisplayTechTreeHelper(tt));

		List<DisplayTechTreeHelper> allTechs = new ArrayList<DisplayTechTreeHelper>();
		allTechs.addAll(alleTechs.values());
		//Unnötige Technologien nicht anzeigen:
		//Etwas ist unnötig, wenn es keine Abhängigkeiten gibt,
		//oder die Technologie nur von der Lock-Technologie abhängt
		//Damit der Technologiebaum übersichtlicher wird
	for (Iterator<DisplayTechTreeHelper> i = allTechs.iterator(); i.hasNext();)
		{
			DisplayTechTreeHelper dtth = i.next();

			boolean dontShow = false;
			if ((dtth.getDependencies().isEmpty()) &&
					(dtth.getDependsOn().isEmpty()))
				dontShow = true;
			if ((dtth.getDependencies().isEmpty()) &&
					(dtth.getDependsOn().size() == 1) &&
					(dtth.getDependsOn().get(0).getID() == 999))
				dontShow = true;
			if (!dontShow && (dtth.getDependencies().isEmpty()) &&
					(dtth.getDependsOn().size() == 1) &&
					(dtth.getDependsOn().get(0).getName().equalsIgnoreCase(dtth.getTechno().getName()) ||
							dtth.getDependsOn().get(0).getName().equalsIgnoreCase(dtth.getTechno().getName()+"-Design")||
							dtth.getDependsOn().get(0).getName().equalsIgnoreCase(dtth.getTechno().getName()+"n-Design")||
							(distanceBetween(dtth.getDependsOn().get(0).getName(),dtth.getTechno().getName(),3) < 3)))
			{
				dontShow = true;
				alleTechs.get(dtth.getDependsOn().get(0).getID()).addDependentNoShow(dtth);
			}
			if (dontShow)
			{
				i.remove();
				allNotShown.add(dtth);
			}
		}

		//Linien suchen
		for (DisplayTechTreeHelper dtth : allTechs)
		{
			for (TechTree tt : dtth.getDependsOn())
			{
				DisplayTechTreeHelper d = alleTechs.get(tt.getID());
				if (!allNotShown.contains(d))
					allLines.add(new DisplayConnection(d,dtth,DisplayConnection.STYLE_ARROW));
			}
		}

		 Arranger arranger = new Arranger();
         allLevels = arranger.run(allTechs);



	}

	public static void calculateSize()
    {
        int maxY = 0;
        int maxX = 0;
        for (DisplayTechTreeHelper di : instance.alleTechs.values())
        {
                maxX = Math.max(di.getWidth() + di.getX(), maxX);
                maxY = Math.max(di.getHeight() + di.getY(), maxY);
        }
        instance.width = maxX;
        instance.height = maxY;
    }

	private void doRecreate() {
	}

	public static List<DisplayConnection> getAllLines() {
		return instance.allLines;
	}
}
