/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.servlets.jaxgzip;

/**
 *
 * @author Stefan
 */
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
 
import javax.ws.rs.NameBinding;
 
//@Compress annotation is the name binding annotation
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface Compress {}