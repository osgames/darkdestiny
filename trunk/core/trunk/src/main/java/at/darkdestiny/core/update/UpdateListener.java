package at.darkdestiny.core.update;

/**
 * This interface describes a listener for tick change events. It is called
 * before the tick, to init data, for each planet to process, and after the 
 * tick. It is also provided a callback to store the changed data.
 *  
 * @author martin
 */
public interface UpdateListener {

    void preTickInit(Updater2 updater);

    void calculatePlanet(Updater2 updater);

    /**
     * This function is called after each planet was processed, to update global
     * settings.
     */
    void globalUpdates();

    void storeData();

}
