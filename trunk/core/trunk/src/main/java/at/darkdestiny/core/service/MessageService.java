/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.dao.MessageDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.dao.VotingDAO;
import at.darkdestiny.core.enumeration.EDoVoteRefType;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EMessageFolder;
import at.darkdestiny.core.enumeration.EMessageRefType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.model.Message;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.model.Voting;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.utilities.LanguageUtilities;
import at.darkdestiny.core.voting.DefaultVoteWrapper;
import at.darkdestiny.core.voting.EndVoteChart;
import at.darkdestiny.core.voting.IVoteWrapper;
import at.darkdestiny.core.voting.ResearchVoteWrapper;
import at.darkdestiny.core.voting.Vote;
import at.darkdestiny.core.voting.VoteResult;
import at.darkdestiny.core.voting.VotingFactory;
import java.util.ArrayList;
import java.util.Iterator;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author Bullet
 */
public class MessageService {

    private static final Logger log = LoggerFactory.getLogger(MessageService.class);

    public static final int TYPE_SOURCE_USER = 1;
    public static final int TYPE_TARGET_USER = 2;
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static MessageDAO mDAO = (MessageDAO) DAOFactory.get(MessageDAO.class);
    private static VotingDAO vDAO = (VotingDAO) DAOFactory.get(VotingDAO.class);

    public static BaseResult writeMessageToUser(int sender, ArrayList<Integer> receiver, String topic, String message, boolean ignoreSmiley) {
        return writeMessageToUser(sender,receiver,topic,message,ignoreSmiley,EMessageType.USER);
    }
    
    public static BaseResult writeAllianceMessageToUser(int sender, ArrayList<Integer> receiver, String topic, String message, boolean ignoreSmiley) {
        return writeMessageToUser(sender,receiver,topic,message,ignoreSmiley,EMessageType.ALLIANCE);
    }    
    
    private static BaseResult writeMessageToUser(int sender, ArrayList<Integer> receiver, String topic, String message, boolean ignoreSmiley, EMessageType type) {
        // Check values

        User sendingUser = uDAO.findById(sender);
        if (sendingUser == null) {
            return new BaseResult("Sender does not exist", true);
        }
        ArrayList<User> receiverList = new ArrayList<User>();

        for (Integer uId : receiver) {
            User receivingUser = uDAO.findById(uId);
            if (sendingUser == null) {
                return new BaseResult("Receiver (" + uId + ") does not exist", true);
            }
            receiverList.add(receivingUser);
        }

        GenerateMessage gm = new GenerateMessage(ignoreSmiley);

        topic = FormatUtilities.killJavaScript(topic);
        message = FormatUtilities.killJavaScript(message);

        gm.setTopic(topic);
        gm.setMsg(message);
        gm.setMasterEntry(true);
        gm.setMessageType(type);
        gm.setSourceUserId(sender);

        for (Integer uId : receiver) {
            gm.setDestinationUserId(uId);
            gm.writeMessageToUser();
        }

        return new BaseResult("Sending OK", false);
    }

    public static User findUserById(int userId) {
        return uDAO.findById(userId);
    }

    public static Message getMessageForView(int userId, int msgId, int type) {
        Message m = mDAO.findById(msgId);

        if (type == TYPE_TARGET_USER) {
            if (m.getTargetUserId() != userId) {
                return null;
            }
        } else if (type == TYPE_SOURCE_USER) {
            if (m.getSourceUserId() != userId) {
                return null;
            }
        }

        return m;
    }

    public static JFreeChart getChart(int userId, Message m) {
        if (m.getRefType() == EMessageRefType.VOTE) {
            // System.out.println("Retrieve Vote Type " + m.getRefId());
            
            int voteId = m.getRefId();

            Voting vTmp = vDAO.findById(voteId);
            Vote v = VotingFactory.loadVote(voteId);      
            if (v == null) return null;

            if (v.getType() == Voting.TYPE_VOTEROUNDEND) {
                log.error( "Calling Endvote Chart with parameters USERID: " + userId + " VOTEID: " + voteId);
                EndVoteChart evc = new EndVoteChart(userId, voteId);
                return evc.getChart();
            }
        }

        return null;
    }

    public static String getExtendedVoteText(int userId, Message m) {
        // Append Vote HTML
        String result = "";

        if (m.getRefType() == EMessageRefType.VOTE) {
            int voteId = m.getRefId();
            Voting vTmp = vDAO.findById(voteId);

            Vote v = VotingFactory.loadVote(voteId);
            if (v == null) return result;
            
            try {
                IVoteWrapper ivw; 
                
                if (v.getType() != Voting.TYPE_VOTE_RESEARCH) {
                    ivw = new DefaultVoteWrapper();
                    VoteResult vr = ivw.currentState(userId, voteId);
                    String voteStateText = ivw.getVoteStateHTML(vr, voteId, LanguageUtilities.getMasterLocaleForUser(userId));

                    result += voteStateText;
                } else {
                    ivw = new ResearchVoteWrapper();
                    VoteResult vr = ivw.currentState(userId, voteId);
                    String voteStateText = ivw.getVoteStateHTML(vr, voteId, LanguageUtilities.getMasterLocaleForUser(userId));

                    result += voteStateText;                    
                }

                String voteText = ivw.generateVoteBody(v, voteId, userId, EDoVoteRefType.MESSAGE, m.getMessageId());

                log.debug("Append " + voteText);
                result += voteText;

                if (v.getType() == Voting.TYPE_VOTEROUNDEND) {
                    result += "<BR><BR>";
                    result += "<IMG src=\"ChartViewer\" usemap=\"#map\">";
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error on adding vote html to message: ", e);
            }
        }

        return result;
    }

    public static void deleteMessage(int userId, int msgId, EMessageFolder msgFolder) {
        User sysUser = uDAO.getSystemUser();
        if (sysUser == null) {
            DebugBuffer.error("No SYSTEM user defined!");
            return;
        }
        
        try {
            if (msgFolder == null) {
                return;
            }
            Message m = mDAO.findById(msgId);
            if (m == null) {
                return;
            }

            if (msgFolder == EMessageFolder.OUTBOX) {
                if (m.getSourceUserId() == userId) {
                    m.setDelBySource(true);
                } else {
                    return;
                }
            } else if (msgFolder == EMessageFolder.INBOX) {
                if (m.getTargetUserId() == userId) {
                    m.setDelByTarget(true);
                    m.setViewed(true);
                } else {
                    return;
                }
            } else if (msgFolder == EMessageFolder.ARCHIVE) {
                if (m.getTargetUserId() == userId) {
                    m.setDelByTarget(true);
                    m.setViewed(true);
                    m.setArchiveByTarget(true);
                } else {
                    return;
                }
            }

            if ((m.getDelBySource() || (m.getSourceUserId().equals(sysUser.getId()))) && m.getDelByTarget()) {
                mDAO.remove(m);
            } else {
                mDAO.update(m);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in deleteMessage: ", e);
        }
    }

    public static void deleteAllMessages(int userId, EMessageFolder msgFolder, EMessageType msgType) {
        User sysUser = uDAO.getSystemUser();
        if (sysUser == null) {
            DebugBuffer.error("No SYSTEM user defined!");
            return;
        }        
        
        if (msgFolder == null) {
            return;
        }

        try {
            ArrayList<Message> mList = new ArrayList<Message>();

            mList = mDAO.findByUserId(userId, msgFolder);

            if (mList.isEmpty()) {
                return;
            }
            for (Iterator<Message> mIt = mList.iterator(); mIt.hasNext();) {
                Message m = mIt.next();

                if (msgType != null) {
                    if (m.getType() != msgType) {
                        mIt.remove();
                        continue;
                    }
                }

                if ((msgFolder == EMessageFolder.INBOX) || (msgFolder == EMessageFolder.ARCHIVE)) {
                    m.setViewed(true);
                    m.setDelByTarget(true);
                } else if (msgFolder == EMessageFolder.OUTBOX) {
                    m.setDelBySource(true);
                }

                if ((m.getDelBySource() || (m.getSourceUserId().equals(sysUser.getId()))) && m.getDelByTarget()) {
                    mDAO.remove(m);
                    mIt.remove();
                }
            }

            if (mList.size() > 0) {
                mDAO.updateAll(mList);
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in deleteMessages: ", e);
        }
    }

    @Deprecated
    public static Message findMessageBySourceId(int userId, int msgId) {
        return mDAO.findBySourceId(userId, msgId);
    }

    @Deprecated
    public static Message findMessageByTargetId(int userId, int msgId) {
        return mDAO.findByTargetId(userId, msgId);
    }

    public static void updateMessage(Message m) {
        mDAO.update(m);
    }

    public static ArrayList<Message> findInboxMessages(int userId) {
        return mDAO.findInboxMessages(userId);
    }

    public static ArrayList<Message> findOutboxMessages(int userId) {
        return mDAO.findOutboxMessages(userId);
    }

    public static ArrayList<Message> findArchivedMessages(int userId) {
        return mDAO.findArchivedMessages(userId);
    }

    public static ArrayList<Message> findMessagesByTargetUserId(int userId) {
        return mDAO.findByTargetUserId(userId);
    }

    public static ArrayList<Message> findMessagesBySourceUserId(int userId) {
        return mDAO.findBySourceUserId(userId);
    }

    public static ArrayList<Message> findMessagesByUserId(int userId, EMessageType messageType) {
        return mDAO.findByUserId(userId, messageType);
    }

    public static UserData findUserDataById(int userId) {
        return udDAO.findByUserId(userId);
    }

    public static ArrayList<User> findAllUsers() {
        return uDAO.findAll();
    }
    
    public static String getMessageTypePic(Message m) {
        if (m.getType() == EMessageType.SYSTEM) {
            if (m.getMsgType() == EMessageContentType.CONSTRUCTION) {
                return "modify.jpg";
            } else if (m.getMsgType() == EMessageContentType.RESEARCH) {
                return "research.png";
            } else if (m.getMsgType() == EMessageContentType.INFO) {
                return "infoT.png";
            } else if (m.getMsgType() == EMessageContentType.COMBAT) {
                return "fight.png";
            } else if (m.getMsgType() == EMessageContentType.SCAN) {
                return "scan.jpg";
            } else if (m.getMsgType() == EMessageContentType.DIPLOMACY) {
                return "contract.png";
            }
            return "system.png";
        } else if (m.getType() == EMessageType.USER) {
            return "pop.png";
        } else if (m.getType() == EMessageType.ALLIANCE) {
            return "allianz.jpg";
        }       
        
        return "";
    }
    
    public static int previousMessageId(int userId, int mId) {
        Message m = mDAO.findById(mId);
        if (m == null) return -1;
        
        return previousMessageId(userId,m);
    }
    
    public static int nextMessageId(int userId, int mId) {
        Message m = mDAO.findById(mId);
        if (m == null) return -1;
        
        return nextMessageId(userId,m);
    }    
    
    public static int previousMessageId(int userId, Message m) {
        int msgId = -1;
        
        boolean searchArchived = m.getArchiveByTarget();  
       
        long timestamp = m.getTimeSent();
        
        ArrayList<Message> msgs = mDAO.findByTargetUserId(m.getTargetUserId());
        Message mNew = null;
        
        for (Message msg : msgs) {
            if (msg.getDelByTarget()) continue;
            if (searchArchived != msg.getArchiveByTarget()) continue;
            
            if (mNew == null) {
                if (msg.getTimeSent() < timestamp) {
                    mNew = msg;
                } 
            } else {
                if ((msg.getTimeSent() > mNew.getTimeSent()) && (msg.getTimeSent() < timestamp)) {
                    mNew = msg;
                }
            }
        }
        
        if (mNew != null) {
            msgId = mNew.getMessageId();
        }
        
        return msgId;
    }
    
    public static int nextMessageId(int userId, Message m) {
        int msgId = -1;
        
        boolean searchArchived = m.getArchiveByTarget();           
        
        long timestamp = m.getTimeSent();
        
        ArrayList<Message> msgs = mDAO.findByTargetUserId(m.getTargetUserId());
        Message mNew = null;
        
        for (Message msg : msgs) {
            if (msg.getDelByTarget()) continue;
            if (searchArchived != msg.getArchiveByTarget()) continue;
            
            if (mNew == null) {
                if (msg.getTimeSent() > timestamp) {
                    mNew = msg;
                } 
            } else {
                if ((msg.getTimeSent() < mNew.getTimeSent()) && (msg.getTimeSent() > timestamp)) {
                    mNew = msg;
                }
            }
        }
        
        if (mNew != null) {
            msgId = mNew.getMessageId();
        }        
        
        return msgId;
    }    
}
