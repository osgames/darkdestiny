/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.QuizAnswer;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class QuizAnswerDAO extends ReadWriteTable<QuizAnswer> implements GenericDAO  {

    public ArrayList<QuizAnswer> findByQuizEntryId(Integer id) {

        QuizAnswer qa = new QuizAnswer();
        qa.setQuizEntryId(id);

        return (ArrayList<QuizAnswer>)find(qa);
    }


}
