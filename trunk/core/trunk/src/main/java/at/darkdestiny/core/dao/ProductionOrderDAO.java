/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class ProductionOrderDAO extends ReadWriteTable<ProductionOrder> implements GenericDAO {
    public ProductionOrder findById(Integer id) {
        ProductionOrder po = new ProductionOrder();
        po.setId(id);
        return (ProductionOrder)get(po);

    }

    public ArrayList<ProductionOrder> findByActionList(ArrayList<Action> actions) {
        TreeMap<Integer, ProductionOrder> tmpResult = new TreeMap<Integer, ProductionOrder>();
        for (Action a : actions) {
            ProductionOrder po = findByConstructionOrderById(a.getConstructionId());
            tmpResult.put(po.getPriority(), po);
        }
        return (ArrayList<ProductionOrder>) tmpResult.values();
    }

    public ProductionOrder findByAction(Action a) {
        ProductionOrder po = new ProductionOrder();
        po.setId(a.getRefTableId());

        ArrayList<ProductionOrder> result = find(po);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ProductionOrder findByConstructionOrderById(Integer id) {
        ProductionOrder po = new ProductionOrder();
        po.setId(id);
        po.setType(EProductionOrderType.C_BUILD);
        ArrayList<ProductionOrder> result = find(po);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
