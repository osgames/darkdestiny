/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Dreloc
 */
@TableNameAnnotation(value = "embassy")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Embassy extends Model<Embassy> {

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    private Integer userId;
    @FieldMappingAnnotation("remUserId")
    @IdFieldAnnotation
    private Integer remUserId;
    @FieldMappingAnnotation("synced")
    private Boolean synced;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the remUserId
     */
    public Integer getRemUserId() {
        return remUserId;
    }

    /**
     * @param remUserId the remUserId to set
     */
    public void setRemUserId(Integer remUserId) {
        this.remUserId = remUserId;
    }

    /**
     * @return the synced
     */
    public Boolean isSynced() {
        return synced;
    }

    /**
     * @param synced the synced to set
     */
    public void setSynced(Boolean synced) {
        this.synced = synced;
    }
}
