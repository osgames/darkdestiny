/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.construction.restriction.Observatory;
import at.darkdestiny.core.dao.*;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResolver;
import at.darkdestiny.core.diplomacy.combat.CombatGroupResult;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.enumeration.EPlanetLogType;
import at.darkdestiny.core.model.*;
import at.darkdestiny.core.movable.PlayerFleetExt;
import at.darkdestiny.core.notification.NotificationBuffer;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.service.FleetService;
import at.darkdestiny.core.service.IdToNameService;
import at.darkdestiny.core.service.Service;
import static at.darkdestiny.core.service.Service.actionDAO;
import static at.darkdestiny.core.service.Service.combatPlayerDAO;
import static at.darkdestiny.core.service.Service.planetCategoryDAO;
import static at.darkdestiny.core.service.Service.planetConstructionDAO;
import static at.darkdestiny.core.service.Service.planetLogDAO;
import static at.darkdestiny.core.service.Service.playerFleetDAO;
import static at.darkdestiny.core.service.Service.playerPlanetDAO;
import static at.darkdestiny.core.service.Service.productionOrderDAO;
import static at.darkdestiny.core.service.Service.tradeRouteDAO;
import static at.darkdestiny.core.service.Service.userDAO;
import at.darkdestiny.core.spacecombat.CombatHandler_NEW;
import at.darkdestiny.core.spacecombat.CombatReportGenerator;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;
import at.darkdestiny.core.voting.MetaDataDataType;
import at.darkdestiny.core.voting.Vote;
import at.darkdestiny.core.voting.VotingFactory;
import at.darkdestiny.core.voting.VotingTextMLSecure;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class PlanetUtilities extends Service {

    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    private static PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static SystemDAO sDAO = (SystemDAO) DAOFactory.get(SystemDAO.class);
    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static SunTransmitterDAO stDAO = (SunTransmitterDAO) DAOFactory.get(SunTransmitterDAO.class);
    private static PlanetLoyalityDAO pLoyDAO = (PlanetLoyalityDAO) DAOFactory.get(PlanetLoyalityDAO.class);

    public static ArrayList<Model> movePlanetToPlayer(int newOwner, int planetId, PlayerPlanet pp, int groundCombatId, boolean peaceful) {
        ArrayList<Model> removeObjects = new ArrayList<Model>();

        DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "The planet " + planetId + " is taken over by " + userDAO.findById(newOwner).getGameName());
        DebugBuffer.addLine(DebugBuffer.DebugLevel.GROUNDCOMBAT, "The planet " + planetId + " is taken over by " + userDAO.findById(newOwner).getGameName());
        if (newOwner == pp.getUserId()) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.ERROR, "GroundcombatId : " + groundCombatId + " planetId " + pp.getPlanetId() + " NEW USER IS OLD USER THIS SHOULD NOT OCCUR " + newOwner);
            return removeObjects;
        }
        try {
            int oldUser = pp.getUserId();

            /*
             *
             * TITLE
             *
             */
            try {
                if (pp.isHomeSystem()) {
                    TitleUtilities.incrementCondition(ConditionToTitle.CONQUERER, newOwner);
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in GroundCombatutilities while increment TitleCondition : ", e);
            }
            try {
                TitleUtilities.incrementCondition(ConditionToTitle.STINKER, oldUser);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error in GroundCombatutilities while increment TitleCondition : ", e);
            }

            //Delete possible Notifications
            try {
                NotificationBuffer.removePONotification(oldUser, planetId);
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error while deleting possible Notifications : userId " + oldUser + " planetId : " + planetId, e);
            }

            //Deleting TradepostEntry
            try {
                PlanetConstruction pc = planetConstructionDAO.findBy(planetId, Construction.ID_INTERSTELLARTRADINGPOST);
                if (pc != null) {
                    TradePost tp = Service.tradePostDAO.findByPlanetId(planetId);
                    if (tp != null) {
                        TradePost tpNew = new TradePost();
                        tpNew.setName(tp.getName());
                        tpNew.setPlanetId(tp.getPlanetId());
                        tpNew.setTransportPerLJ(tp.getTransportPerLJ());
                        Service.tradePostDAO.remove(tp);
                        Service.tradePostDAO.add(tp);
                    }
                }

            } catch (Exception e) {
                DebugBuffer.error("Error while deleting possible TradepostEntry : userId " + oldUser + " planetId : " + planetId);

            }

            //Deleting Traderoutes
            ArrayList<TradeRoute> tradeRoutes = tradeRouteDAO.findByStartPlanet(planetId);
            tradeRoutes.addAll(tradeRouteDAO.findByEndPlanet(planetId));
            for (TradeRoute tr : tradeRoutes) {
                DebugBuffer.trace("Marking route " + tr.getId() + " for deletion! (Due to invasion)");
                removeObjects.add(tr);
                // TransportService.deleteRoute(tr.getUserId(), tr.getId());
            }

            //Updating Planets in Observatory range
            boolean hasObservatory = planetConstructionDAO.isConstructed(planetId, Construction.ID_OBSERVATORY);
            Observatory o = new Observatory();
            if (hasObservatory) {
                //Delete for existing User
                o.onDeconstruction(pp);
            }

            // If planet is transmitter planet also change transmitter owner
            DebugBuffer.trace("Check if we are dealing with a transmitter planet on planet " + planetId);
            SunTransmitter st = stDAO.findByPlanetId(planetId);
            if (st != null) {
                DebugBuffer.trace("Found transmitter planet, setting new owner to  " + newOwner);
                st.setUserId(newOwner);
                stDAO.update(st);
            } else {
                DebugBuffer.trace("Didn't find transmitter planet");
            }

            //Change planet
            pp.setHomeSystem(false);
            pp.setUserId(newOwner);

            if (!peaceful) {
                pp.setTax(37);
            }

            playerPlanetDAO.update(pp);
            for (PlanetCategory pc : planetCategoryDAO.findAllByPlanetId(planetId)) {
                planetCategoryDAO.remove(pc);
            }

            if (!peaceful) {
                PlanetLoyality pLoy = pLoyDAO.getByPlanetAndUserId(pp.getPlanetId(), newOwner);
                if (pLoy == null) {
                    pLoy = new PlanetLoyality();
                    pLoy.setPlanetId(pp.getPlanetId());
                    pLoy.setUserId(newOwner);
                    pLoy.setValue(0d);
                    pLoyDAO.add(pLoy);
                }
            } else {
                // TODO: Not sure what to do here
            }

            //Observatory after planet changed
            if (hasObservatory) {
                //Establish Entries for new Owner
                o.onFinishing(pp);
            }

            HeaderBuffer.reloadUser(newOwner);
            HeaderBuffer.reloadUser(oldUser);

            //Remove Groundtroop Orders
            if (!peaceful) {
                for (Action a : actionDAO.findByPlanet(planetId)) {
                    if (a.getType().equals(EActionType.GROUNDTROOPS)) {
                        ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                        removeObjects.add(po);
                        // productionOrderDAO.remove(po);
                    }
                    // actionDAO.remove(a);
                }
            } else {
                // Transfer all groundtroops to new user
                for (PlayerTroop pt : playerTroopDAO.findBy(oldUser, planetId)) {
                    playerTroopDAO.remove(pt);
                    pt.setUserId(newOwner);

                    PlayerTroop existing = playerTroopDAO.findBy(newOwner, planetId, pt.getTroopId());
                    if (existing == null) {
                        playerTroopDAO.add(pt);
                    } else {
                        existing.setNumber(existing.getNumber() + pt.getNumber());
                        playerTroopDAO.update(existing);
                    }
                }
            }

            //Remove Ship Orders
            for (Action a : actionDAO.findByPlanet(planetId)) {
                if (a.getType().equals(EActionType.SHIP)) {
                    ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                    removeObjects.add(po);
                    // productionOrderDAO.remove(po);
                }
                // actionDAO.remove(a);
            }

            //Remove Spacestation Orders
            for (Action a : actionDAO.findByPlanet(planetId)) {
                if (a.getType().equals(EActionType.SPACESTATION)) {
                    ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
                    removeObjects.add(po);
                    // productionOrderDAO.remove(po);
                }
                // actionDAO.remove(a);
            }

            //Create PlanetLog Entry
            if (!peaceful) {
                PlanetLog pl = new PlanetLog();
                pl.setPlanetId(planetId);
                pl.setTime(GameUtilities.getCurrentTick2());
                pl.setUserId(newOwner);
                pl.setType(EPlanetLogType.INVADED);

                planetLogDAO.add(pl);
            } else {
                PlanetLog pl = new PlanetLog();
                pl.setPlanetId(planetId);
                pl.setTime(GameUtilities.getCurrentTick2());
                pl.setUserId(newOwner);
                pl.setType(EPlanetLogType.TRANSFER);

                planetLogDAO.add(pl);
            }

            if (!peaceful) {
                GenerateMessage looser = new GenerateMessage();
                GenerateMessage winner = new GenerateMessage();

                int currTick = GameUtilities.getCurrentTick2();
                RelativeCoordinate rc = new RelativeCoordinate(0, pp.getPlanetId());
                ViewTableUtilities.addOrRefreshSystem(oldUser, rc.getSystemId(), currTick);
                ViewTableUtilities.addOrRefreshSystem(newOwner, rc.getSystemId(), currTick);

                ArrayList<Integer> participants = new ArrayList<Integer>();
                participants.add(oldUser);
                participants.add(newOwner);

                //There was a battle and there are probably more users involved
                if (groundCombatId > 0) {
                    for (CombatPlayer cp : combatPlayerDAO.findByCombatId(groundCombatId)) {
                        if (!participants.contains(cp.getUserId())) {
                            participants.add(cp.getUserId());
                        }
                    }
                }

                CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(participants, new RelativeCoordinate(0, planetId));

                for (Map.Entry<Integer, ArrayList<Integer>> entry : cgr.getAsMap().entrySet()) {
                    for (Integer i : entry.getValue()) {
                        //Loosers
                        if (entry.getValue().contains(oldUser)) {
                            looser.setDestinationUserId(i);
                            looser.setSourceUserId(userDAO.getSystemUser().getId());
                            looser.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
                            looser.setMsg(ML.getMLStr("fleetmanagement_msg_invasionlostplanet", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(newOwner).getGameName()));
                            looser.setMessageType(EMessageType.SYSTEM);
                            looser.setMessageContentType(EMessageContentType.COMBAT);
                            looser.writeMessageToUser();
                            //Winners
                        } else {
                            // Add helping players so planet can be given to them
                            if (i != newOwner) {
                                PlanetLog pl = new PlanetLog();
                                pl.setPlanetId(pp.getPlanetId());
                                pl.setTime(GameUtilities.getCurrentTick2());
                                pl.setType(EPlanetLogType.INVADED_HELP);
                                pl.setUserId(i);
                                planetLogDAO.add(pl);
                            }
                            winner.setDestinationUserId(i);
                            winner.setSourceUserId(userDAO.getSystemUser().getId());
                            winner.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
                            winner.setMsg(ML.getMLStr("fleetmanagement_msg_invasionwon", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(oldUser).getGameName()));
                            winner.setMessageType(EMessageType.SYSTEM);
                            winner.setMessageContentType(EMessageContentType.COMBAT);
                            winner.writeMessageToUser();
                        }
                    }
                }
            }
        } catch (Exception e) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.ERROR, "Groundcombat - Error while fight after moving planet : " + e);
            DebugBuffer.produceStackTrace(e);
        }

        //Attack Tradefleets if new are available      
        if (!peaceful) {
            try {
                ArrayList<PlayerFleet> pfs = playerFleetDAO.findByPlanetId(planetId);
                for (PlayerFleet pf : pfs) {
                    PlayerFleetExt pfe = new PlayerFleetExt(pf);
                    if (FleetService.canFight(pfe)) {

                        if (pfe.getRelativeCoordinate().getSystemId() == 0) {
                            DebugBuffer.addLine(DebugBuffer.DebugLevel.FATAL_ERROR, "Invoking Combat in GroundCombatUtilities with System 0");

                        } else {
                            CombatReportGenerator crg = new CombatReportGenerator();
                            if (pfe.getRelativeCoordinate().getPlanetId() == 0) {
                                CombatHandler_NEW ch = new CombatHandler_NEW(CombatReportGenerator.CombatType.SYSTEM_COMBAT, playerFleetDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
                                ch.processCombatResults();
                            } else {
                                CombatHandler_NEW ch = new CombatHandler_NEW(CombatReportGenerator.CombatType.ORBITAL_COMBAT, playerFleetDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
                                ch.processCombatResults();
                            }

                            crg.writeReportsToDatabase();
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                DebugBuffer.writeStackTrace("Error fighting trade fleets: ", e);
                return new ArrayList<Model>();
            }
        }

        return removeObjects;
    }

/*
 public static void movePlanetToPlayer(int newOwner, int planetId, PlayerPlanet pp, boolean peaceful) {
 DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "The planet " + planetId + " is taken over by " + userDAO.findById(newOwner).getGameName());
 DebugBuffer.addLine(DebugBuffer.DebugLevel.GROUNDCOMBAT, "The planet " + planetId + " is taken over by " + userDAO.findById(newOwner).getGameName());
 if (newOwner == pp.getUserId()) {
 return;
 }
 try {
 int oldUser = pp.getUserId();

 //Delete possible Notifications
 try {
 NotificationBuffer.removePONotification(oldUser, planetId);
 } catch (Exception e) {
 DebugBuffer.writeStackTrace("Error while deleting possible Notifications : userId " + oldUser + " planetId : " + planetId, e);
 }

 //Deleting TradepostEntry
 try {
 PlanetConstruction pc = planetConstructionDAO.findBy(planetId, Construction.ID_INTERSTELLARTRADINGPOST);
 if (pc != null) {
 TradePost tp = Service.tradePostDAO.findByPlanetId(planetId);
 if (tp != null) {
 TradePost tpNew = new TradePost();
 tpNew.setName(tp.getName());
 tpNew.setPlanetId(tp.getPlanetId());
 tpNew.setTransportPerLJ(tp.getTransportPerLJ());
 Service.tradePostDAO.remove(tp);
 Service.tradePostDAO.add(tp);
 }
 }
 } catch (Exception e) {
 DebugBuffer.error("Error while deleting possible TradepostEntry : userId " + oldUser + " planetId : " + planetId);

 }

 //Deleting Traderoutes
 ArrayList<TradeRoute> tradeRoutes = tradeRouteDAO.findByStartPlanet(planetId);
 tradeRoutes.addAll(tradeRouteDAO.findByEndPlanet(planetId));
 for (TradeRoute tr : tradeRoutes) {
 TransportService.deleteRoute(tr.getUserId(), tr.getId());
 }

 //Updating Planets in Observatory range
 boolean hasObservatory = planetConstructionDAO.isConstructed(planetId, Construction.ID_OBSERVATORY);
 Observatory o = new Observatory();
 if (hasObservatory) {
 //Delete for existing User
 o.onDeconstruction(pp);
 }

 // If planet is transmitter planet also change transmitter owner
 DebugBuffer.trace("Check if we are dealing with a transmitter planet on planet " + planetId);
 SunTransmitter st = stDAO.findByPlanetId(planetId);
 if (st != null) {
 DebugBuffer.trace("Found transmitter planet, setting new owner to  " + newOwner);
 st.setUserId(newOwner);
 stDAO.update(st);
 } else {
                
 }

 //Change planet
 pp.setHomeSystem(false);
            
 DebugBuffer.trace("Setting new owner of planet " + pp.getPlanetId() + " to " + newOwner);
 pp.setUserId(newOwner);

 if (!peaceful) {
 pp.setTax(0);
 }

 playerPlanetDAO.update(pp);
 for (PlanetCategory pc : planetCategoryDAO.findAllByPlanetId(planetId)) {
 planetCategoryDAO.remove(pc);
 }

 //Observatory after planet changed
 if (hasObservatory) {
 //Establish Entries for new Owner
 o.onFinishing(pp);
 }

 HeaderBuffer.reloadUser(newOwner);
 HeaderBuffer.reloadUser(oldUser);

 //Remove Groundtroop Orders
 if (!peaceful) {
 for (Action a : actionDAO.findByPlanet(planetId)) {
 if (a.getType().equals(EActionType.GROUNDTROOPS)) {
 ProductionOrder po = productionOrderDAO.findById(a.getProductionOrderId());
 productionOrderDAO.remove(po);
 actionDAO.remove(a);
 }
 }
 }

 // Transfer all groundtroops to new user
 for (PlayerTroop pt : playerTroopDAO.findBy(oldUser, planetId)) {
 playerTroopDAO.remove(pt);
 pt.setUserId(newOwner);

 PlayerTroop existing = playerTroopDAO.findBy(newOwner, planetId, pt.getTroopId());
 if (existing == null) {
 playerTroopDAO.add(pt);
 } else {
 existing.setNumber(existing.getNumber() + pt.getNumber());
 playerTroopDAO.update(existing);
 }
 }

 //Create PlanetLog Entry
 {
 PlanetLog pl = new PlanetLog();
 pl.setPlanetId(planetId);
 pl.setTime(GameUtilities.getCurrentTick2());
 pl.setUserId(newOwner);
 pl.setType(EPlanetLogType.TRANSFER);

 planetLogDAO.add(pl);
 }

 // GenerateMessage looser = new GenerateMessage();
 // GenerateMessage winner = new GenerateMessage();

 int currTick = GameUtilities.getCurrentTick2();
 RelativeCoordinate rc = new RelativeCoordinate(0, pp.getPlanetId());
 ViewTableUtilities.addOrRefreshSystem(oldUser, rc.getSystemId(), currTick);
 ViewTableUtilities.addOrRefreshSystem(newOwner, rc.getSystemId(), currTick);

 /*
 ArrayList<Integer> participants = new ArrayList<Integer>();
 participants.add(oldUser);
 participants.add(newOwner);
 */

/*
 CombatGroupResult cgr = CombatGroupResolver.getCombatGroups(participants, new RelativeCoordinate(0, planetId));

 for (Map.Entry<Integer, ArrayList<Integer>> entry : cgr.getAsMap().entrySet()) {
 for (Integer i : entry.getValue()) {
 //Loosers
 if (entry.getValue().contains(oldUser)) {
 looser.setDestinationUserId(i);
 looser.setSourceUserId(uDAO.getSystemUser().getUserId());
 looser.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
 looser.setMsg(ML.getMLStr("fleetmanagement_msg_invasionlostplanet", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(newOwner).getGameName()));
 looser.setMessageType(EMessageType.SYSTEM);
 looser.writeMessageToUser();
 //Winners
 } else {
 winner.setDestinationUserId(i);
 winner.setSourceUserId(uDAO.getSystemUser().getUserId());
 winner.setTopic(ML.getMLStr("fleetmanagement_msg_invasionmsg", i).replace("%PLANET%", pp.getName()));
 winner.setMsg(ML.getMLStr("fleetmanagement_msg_invasionwon", i).replace("%PLANET%", pp.getName()).replace("%USER%", userDAO.findById(oldUser).getGameName()));
 winner.setMessageType(EMessageType.SYSTEM);
 winner.writeMessageToUser();
 }
 }
 }
             
 } catch (Exception e) {
 DebugBuffer.addLine(DebugBuffer.DebugLevel.ERROR, "Groundcombat - Error while fight after moving planet : " + e);
 DebugBuffer.produceStackTrace(e);
 }

 //Attack Tradefleets if new are available
 try {
 ArrayList<PlayerFleet> pfs = playerFleetDAO.findByPlanetId(planetId);
 for (PlayerFleet pf : pfs) {
 PlayerFleetExt pfe = new PlayerFleetExt(pf);
 if (FleetService.canFight(pfe)) {

 if (pfe.getRelativeCoordinate().getSystemId() == 0) {
 DebugBuffer.addLine(DebugBuffer.DebugLevel.FATAL_ERROR, "Invoking Combat in GroundCombatUtilities with System 0");

 } else {
 CombatReportGenerator crg = new CombatReportGenerator();
 if (pfe.getRelativeCoordinate().getPlanetId() == 0) {
 CombatHandler_NEW ch = new CombatHandler_NEW(CombatReportGenerator.CombatType.SYSTEM_COMBAT, playerFleetDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
 ch.processCombatResults();
 } else {
 CombatHandler_NEW ch = new CombatHandler_NEW(CombatReportGenerator.CombatType.ORBITAL_COMBAT, playerFleetDAO.findBy(pfe.getRelativeCoordinate().getPlanetId(), pfe.getRelativeCoordinate().getSystemId()), pfe.getRelativeCoordinate(), pfe.getAbsoluteCoordinate(), crg);
 ch.processCombatResults();
 }

 crg.writeReportsToDatabase();
 break;
 }
 }
 }
 } catch (Exception e) {
 DebugBuffer.writeStackTrace("Error while setting new owner : ", e);
 }
 }
 */
public static double getDistanceToHome(int userId, int planetId) {
        PlayerPlanet home = ppDAO.findHomePlanetByUserId(userId);
        // Yeah not optimal but just return something so we have a value to calculate
        if (home == null) {
            return 0d;
        }

        RelativeCoordinate homeCoord = new RelativeCoordinate(0, home.getPlanetId());
        RelativeCoordinate compareCoord = new RelativeCoordinate(0, planetId);

        return homeCoord.distanceTo(compareCoord);
    }

    public static void transferPlanet(int newUser, int planetId) {
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);

        int currPlanetOwner = pp.getUserId();

        ArrayList<User> uList = PlanetUtilities.getAllFormerPlanetOwners(currPlanetOwner, planetId);
        boolean allowed = false;

        for (User u : uList) {
            if (u.getUserId() == newUser) {
                allowed = true;
            }
        }

        if (allowed) {
            ArrayList<Model> removedObjects = movePlanetToPlayer(newUser, planetId, pp, 0, true);
            for (Model m : removedObjects) {
                if (m instanceof ProductionOrder) {
                    DebugBuffer.warning("Tried to destroy productionorder on peaceful takeover");
                } else if (m instanceof TradeRoute) {                   
                    at.darkdestiny.core.trade.TradeUtilities.deleteRoute((TradeRoute)m);
                }
            }            
        }
    }

    public static BaseResult createTransferPlanetVote(int userId, int targetUserId, int planetId) {
        // Basic checks on parameters
        ArrayList<User> uList = PlanetUtilities.getAllFormerPlanetOwners(userId, planetId);
        boolean allowed = false;

        // java.lang.System.out.println("Received transfer request from " + userId + " to " + targetUserId + " for planet " + planetId);

        for (User u : uList) {
            if (u.getUserId() == targetUserId) {
                allowed = true;
            }
        }

        if (!allowed) {
            return new BaseResult("No way!", true);
        }

        try {
            Vote v = null;

            PlayerPlanet pp = ppDAO.findByPlanetId(planetId);
            Planet p = pDAO.findById(planetId);
            at.darkdestiny.core.model.System sys = sDAO.findById(p.getSystemId());

            if (true) {
                HashMap<String, CharSequence> msgReplacements = new HashMap<String, CharSequence>();
                HashMap<String, CharSequence> sbjReplacements = new HashMap<String, CharSequence>();

                sbjReplacements.put("%USER%", IdToNameService.getUserName(userId));
                sbjReplacements.put("%PLANETID%", p.getId().toString());
                sbjReplacements.put("%PLANETNAME%", pp.getName());

                msgReplacements.put("%USER%", IdToNameService.getUserName(userId));
                msgReplacements.put("%PLANETID%", p.getId().toString());
                msgReplacements.put("%PLANETNAME%", pp.getName());
                msgReplacements.put("%SYSTEMID%", sys.getId().toString());
                msgReplacements.put("%SYSTEMNAME%", sys.getName());

                VotingTextMLSecure vtML = new VotingTextMLSecure("vote_transferplanet", "vote_transferplanet_sbj", msgReplacements, sbjReplacements);
                v = new Vote(userId, vtML, Voting.TYPE_TRANSFERPLANET);
            }

            ArrayList<Integer> receivers = new ArrayList<Integer>();
            receivers.add(targetUserId);
            v.addPerm(new VotePermission(VotePermission.PERM_SINGLE, targetUserId));

            v.setExpires(GameUtilities.getCurrentTick2() + (144 * 7));
            v.setMinVotes((int) Math.ceil((double) receivers.size() * (2d / 3d)));
            v.setTotalVotes(receivers.size());

            v.addOption(new VoteOption("alliance_link_accept", true, VoteOption.OPTION_TYPE_ACCEPT, true));
            v.addOption(new VoteOption("alliance_link_decline", false, VoteOption.OPTION_TYPE_DENY, true));

            v.addMetaData("PLANETID", planetId, MetaDataDataType.INTEGER);
            v.addMetaData("RECEIVERID", targetUserId, MetaDataDataType.INTEGER);

            v.setReceivers(receivers);

            VotingFactory.persistVote(v);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error on transferPlanet vote: ", e);
            return new BaseResult(e.getMessage(), true);
        }

        return new BaseResult("�bergabeangebot an %1 wurde gesendet.", false);
    }

    public static ArrayList<User> getAllFormerPlanetOwners(int userId, int planetId) {
        ArrayList<User> userList = new ArrayList<User>();

        ArrayList<PlanetLog> allLogEntries = plDAO.findByPlanetIdSorted(planetId);
        PlanetLog lastInvasion = null;
        HashMap<Integer, ArrayList<Integer>> timeUserHelp = new HashMap<Integer, ArrayList<Integer>>();

        HashSet<Integer> processedUsers = new HashSet<Integer>();
        PlayerPlanet pp = ppDAO.findByPlanetId(planetId);

        boolean firstEntryInvasion = false;
        int currElement = 0;

        for (PlanetLog pl : allLogEntries) {
            // java.lang.System.out.println("Looping entry type " + pl.getType() + " and time " + pl.getTime());
            currElement++;

            if (!firstEntryInvasion && currElement == 1 && (pl.getType() == EPlanetLogType.INVADED || pl.getType() == EPlanetLogType.INVADED_HELP)) {
                // java.lang.System.out.println("Set first invasion to true");
                firstEntryInvasion = true;
            }

            if (firstEntryInvasion && lastInvasion == null && pl.getType() == EPlanetLogType.INVADED) {
                // java.lang.System.out.println("Set first invasion Entry to entry " + pl.getType() + " with time " + pl.getTime());
                lastInvasion = pl;
            }

            if (pl.getUserId() == userId) {
                continue;
            }

            if (pl.getType() == EPlanetLogType.COLONIZED) {
                // java.lang.System.out.println("Adding colonization entry " + pl.getType() + " with time " + pl.getTime());
                User u = uDAO.findById(pl.getUserId());
                userList.add(uDAO.findById(pl.getUserId()));
                processedUsers.add(u.getUserId());
            }

            if (pl.getType() == EPlanetLogType.INVADED_HELP) {
                // java.lang.System.out.println("Adding help entry " + pl.getType() + " with time " + pl.getTime() + " of user " + pl.getUserId());

                ArrayList<Integer> users = timeUserHelp.get(pl.getTime());
                if (users == null) {
                    users = new ArrayList<Integer>();
                    users.add(pl.getUserId());
                    timeUserHelp.put(pl.getTime(), users);
                } else {
                    users.add(pl.getUserId());
                }
            }
        }

        if (lastInvasion != null) {
            // java.lang.System.out.println("There was an invasion at " + lastInvasion.getTime());

            ArrayList<Integer> users = timeUserHelp.get(lastInvasion.getTime());
            if (users != null) {
                for (Integer uId : users) {
                    // java.lang.System.out.println("Found help by " + uId);

                    if (processedUsers.contains(uId)) {
                        continue;
                    }
                    if (uId == userId) {
                        continue;
                    }

                    User u = uDAO.findById(uId);
                    if (u != null) {
                        userList.add(u);
                        processedUsers.add(u.getUserId());
                    }
                }
            } else {
                // java.lang.System.out.println("Found no help");
            }
        }

        // If there is a groundcombat we dont care
        if (Service.groundCombatDAO.findRunningCombat(planetId) != null) {
            userList.clear();
        }

        return userList;
    }
}
