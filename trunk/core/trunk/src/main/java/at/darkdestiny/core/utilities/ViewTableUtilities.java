/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.utilities;

import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.Threading.ICopyJob;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.Threading.ViewTableCopyJob;
import at.darkdestiny.core.Threading.ThreadController;
import at.darkdestiny.core.dao.AllianceDAO;
import at.darkdestiny.core.dao.AllianceMemberDAO;
import at.darkdestiny.core.dao.EmbassyDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetLogDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.dao.UserRelationDAO;
import at.darkdestiny.core.dao.ViewTableDAO;
import at.darkdestiny.core.diplomacy.UserShareEntry;
import at.darkdestiny.core.model.Alliance;
import at.darkdestiny.core.model.AllianceMember;
import at.darkdestiny.core.model.DiplomacyRelation;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetLog;
import at.darkdestiny.core.model.UserRelation;
import at.darkdestiny.core.model.ViewTable;
import at.darkdestiny.framework.QueryKey;
import at.darkdestiny.framework.QueryKeySet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Dreloc
 */
public class ViewTableUtilities {

    private static final Logger log = LoggerFactory.getLogger(ViewTableUtilities.class);

    private static PlanetDAO pDAO = (PlanetDAO) DAOFactory.get(PlanetDAO.class);
    private static UserDataDAO udDAO = (UserDataDAO) DAOFactory.get(UserDataDAO.class);
    private static EmbassyDAO eDAO = (EmbassyDAO) DAOFactory.get(EmbassyDAO.class);
    private static UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    private static ViewTableDAO vtDAO = (ViewTableDAO) DAOFactory.get(ViewTableDAO.class);
    private static AllianceDAO aDAO = (AllianceDAO) DAOFactory.get(AllianceDAO.class);
    private static AllianceMemberDAO amDAO = (AllianceMemberDAO) DAOFactory.get(AllianceMemberDAO.class);
    private static PlanetLogDAO plDAO = (PlanetLogDAO) DAOFactory.get(PlanetLogDAO.class);
    private static UserRelationDAO urDAO = (UserRelationDAO) DAOFactory.get(UserRelationDAO.class);

    public void addSystem(int userId, int systemId, int time) {
        ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
        ArrayList<ViewTable> vtList = new ArrayList<ViewTable>();

        for (Planet p : pList) {
            ViewTable vt = new ViewTable();
            vt.setUserId(userId);
            vt.setSystemId(systemId);
            vt.setPlanetId(p.getId());
            vt.setLastTimeVisited(time);
            vt.setMostRecent(true);
            vtList.add(vt);
        }
        //Create fake entry
        if (pList.isEmpty()) {
            ViewTable vt = new ViewTable();
            vt.setUserId(userId);
            vt.setSystemId(systemId);
            vt.setPlanetId(0);
            vt.setLastTimeVisited(time);
            vt.setMostRecent(true);
            vtList.add(vt);
        }

        vtDAO.insertAll(vtList);
    }

    //Refresh for a single user
    public static synchronized void addOrRefreshSystemForUser(int userId, int systemId, int time) {
        try {
            ArrayList<Planet> pList = pDAO.findBySystemId(systemId);
            ArrayList<ViewTable> vtList = new ArrayList<ViewTable>();

            ArrayList<ViewTable> addList = new ArrayList<ViewTable>();
            ArrayList<ViewTable> updList = new ArrayList<ViewTable>();

            vtList = vtDAO.findByUserAndSystem(userId, systemId);
            if (vtList.isEmpty()) {
                for (Planet p : pList) {
                    boolean found = false;
                    ViewTable vtAct = null;

                    for (ViewTable vt : vtList) {
                        if (vt.getPlanetId().equals(p.getId())) {
                            vtAct = vt;
                            found = true;
                            break;
                        }
                    }

                    if (found) {
                        if (vtAct.getLastTimeVisited() > time) {
                            continue;
                        } else {
                            vtAct.setLastTimeVisited(Math.max(vtAct.getLastTimeVisited(), time));
                            updList.add(vtAct);
                            checkUserRelation(userId, systemId, time);
                        }
                    } else {
                        ViewTable vt = new ViewTable();
                        vt.setUserId(userId);
                        vt.setSystemId(systemId);
                        vt.setPlanetId(p.getId());
                        vt.setLastTimeVisited(time);
                        vt.setMostRecent(true);
                        checkUserRelation(userId, systemId, time);
                        addList.add(vt);
                    }
                }

                //Create fake entry
                if (pList.isEmpty()) {
                    ViewTable vt = new ViewTable();
                    vt.setUserId(userId);
                    vt.setSystemId(systemId);
                    vt.setPlanetId(0);
                    vt.setLastTimeVisited(time);
                    vt.setMostRecent(true);
                    addList.add(vt);
                }
            } else {
                for (Iterator<ViewTable> vtIt = vtList.iterator(); vtIt.hasNext();) {
                    ViewTable vt = vtIt.next();

                    if (vt.getLastTimeVisited() > time) {
                        vtIt.remove();
                        continue;
                    } else {
                        vt.setLastTimeVisited(Math.max(vt.getLastTimeVisited(), time));
                        checkUserRelation(userId, systemId, time);
                    }
                }

                updList.addAll(vtList);
            }

            if (!addList.isEmpty()) vtDAO.insertAll(addList);
            if (!updList.isEmpty()) vtDAO.updateAll(updList);
        } catch (Exception e) {
            DebugBuffer.error("System " + systemId + " could not be updated for user " + userId);
        }
    }

    // If a starting system is rebuilt, we also have to rebuild the viewtable entries of players which have already scouted this system
    public static void rebuildViewTableEntries(int systemId) {               
        ArrayList<ViewTable> vtEntries = vtDAO.findBySystem(systemId);
        if (vtEntries.isEmpty()) {
            log.debug("Skip system " + systemId + " -- NOT SCOUTED");
            return;
        }
        
        // Split Entries to User Ids
        HashMap<Integer,ArrayList<ViewTable>> vtEntriesByUser = Maps.newHashMap();
        
        for (ViewTable vt : vtEntries) {
            if (!vtEntriesByUser.containsKey(vt.getUserId())) {
                ArrayList<ViewTable> vtTmp = Lists.newArrayList();
                vtTmp.add(vt);
                vtEntriesByUser.put(vt.getUserId(), vtTmp);
            } else {
                vtEntriesByUser.get(vt.getUserId()).add(vt);
            }
        }
        
        // Read scout timestamp for each System
        HashMap<Integer,Integer> scoutTimeByUser = Maps.newHashMap();
        for (Map.Entry<Integer,ArrayList<ViewTable>> byUserEntry : vtEntriesByUser.entrySet()) {
            scoutTimeByUser.put(byUserEntry.getKey(), byUserEntry.getValue().get(0).getLastTimeVisited());
        }
        
        // Delete Entries then reload with respective Timestamp
        QueryKeySet qks = new QueryKeySet();
        qks.addKey(new QueryKey("systemId",systemId));
        vtDAO.removeAll(qks);
        
        for (Map.Entry<Integer,Integer> scoutTimeByUserEntry : scoutTimeByUser.entrySet()) {
            log.debug("Rebuild system " + systemId + " for user " + scoutTimeByUserEntry.getKey() + " with timestamp " + scoutTimeByUserEntry.getValue());
            addOrRefreshSystem(scoutTimeByUserEntry.getKey(), systemId, scoutTimeByUserEntry.getValue());
        }
    }
    
    private static void checkUserRelation(int currUser, int systemId, int time) {
        // Get most recent Planet Logs for all planets in system for specified time
        ArrayList<Planet> planetList = pDAO.findBySystemId(systemId);

        for (Planet planet : planetList) {
            PlanetLog pl = plDAO.getMostRecentLog(planet.getId(), time);

            if (pl == null) continue;

            int userId = pl.getUserId();
            if (userId == currUser) continue;

            // Check if this user has an entry in user relation
            if (!urDAO.containsUserRelation(userId, currUser)) {
                UserRelation ur = new UserRelation();
                ur.setUserId1(currUser);
                ur.setUserId2(userId);
                ur.setRelationValue(0f);
                urDAO.add(ur);
            }
        }
    }

    /**
     *  MAIN - REFRESH FUNCTION
     * Refresh System for user and his friends
     *
     */
    public static void addOrRefreshSystem(int userId, int systemId, int time) {
        ArrayList<UserShareEntry> useList = DiplomacyUtilities.getAffectedShareUsers(userId);

        ArrayList<Integer> users = new ArrayList<Integer>();
        users.add(userId);

        for (UserShareEntry use : useList) {
            for (Integer user : use.getUsers()) {
                if (user == userId) {
                    continue;
                }

                log.debug("Updating Viewtable for User " + user);
                users.add(user);
            }
        }

        // addOrRefreshSystemForUser(userId, systemId, GameUtilities.getCurrentTick2());

        log.debug("Requesting Update for system " + systemId);
        ViewTableCopyJob cj = new ViewTableCopyJob(users, systemId, GameUtilities.getCurrentTick2());
        ThreadController.getViewTableThread().addJob(cj);
    }

    /*
     * MAINFUNCTION FOR COPYING VIEWTABLE ENTRIES
     * USE OTHERS AND DIE!
     *
     */
    public static boolean copyEntries(int fromId, int toId, Direction direction, boolean mutual) {
        boolean result = false;

        if (true) {
            log.debug("######## COPY VIEWTABLE ENTRIES #######");
            log.debug("### FROM => TO : " + fromId + " => " + toId + "");
            log.debug("### DIRECTION : " + direction);
            log.debug("### MUTUAL COPY? : " + mutual);
        }

        if (direction.equals(Direction.USER_TO_USER)) {


            // Copy fromId => toId
            result = copyEntriesUserToUser(fromId, toId, false);
            if (mutual) {
                //Copy toId => fromId
                result = copyEntriesUserToUser(toId, fromId, false);
                //Copy from Ally to User
            }

        } else if (direction.equals(Direction.USER_TO_ALLIANCE)) {
            result = copyEntriesUserToAlliance(fromId, toId);
            //Copy from User to Ally (e.g.: Trial joins Alliance)
            if (mutual) {
                result = copyEntriesAllianceToUser(toId, fromId);
                //Copy from Ally to User
            }
        } else if (direction.equals(Direction.ALLIANCE_TO_USER)) {
            result = copyEntriesAllianceToUser(fromId, toId);
            //Copy from Ally to User (e.g.: Promoting Trial user
            if (mutual) {
                result = copyEntriesUserToAlliance(toId, fromId);
            }

        } else if (direction.equals(Direction.ALLIANCE_TO_ALLIANCE)) {

            result = copyEntriesAllianceToAlliance(fromId, toId);
            if (mutual) {
                result = copyEntriesAllianceToAlliance(toId, fromId);
            }
        }
        return result;
    }

    private static boolean copyEntriesAllianceToAlliance(int fromId, int toId) {
        Alliance a1 = aDAO.findById(fromId);
        Alliance a2 = aDAO.findById(toId);
        AllianceMember am1 = amDAO.findByAllianceIdNotTrial(a1.getId()).get(0);
        AllianceMember am2 = amDAO.findByAllianceIdNotTrial(a2.getId()).get(0);
        if (am1 == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "In alliance : " + a1.getName() + " are only Trial members");
        }
        if (am2 == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "In alliance : " + a2.getName() + " are only Trial members");
        }

        DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRelationAllianceToAlliance(fromId, toId);

        log.debug("Retrieved DR: DTID=" + dr.getDiplomacyTypeId() + " ACK=" + dr.getAcknowledged() + " TYPE=" + dr.getType() + " SM=" + dr.getSharingMap());
        if (!dr.getSharingMap()) {
            log.debug("Ignore share try forbidden!");
            return false;
        }

        return copyEntriesUserToUser(am1.getUserId(), am2.getUserId(),true);

    }

    private static boolean copyEntriesUserToAlliance(int fromId, int toId) {
        Alliance a = aDAO.findById(toId);
        AllianceMember am = amDAO.findByAllianceIdNotTrial(a.getId()).get(0);
        if (am == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "In alliance : " + a.getName() + " are only Trial members");
        }
        return copyEntriesUserToUser(fromId, am.getUserId(),false);

    }

    private static boolean copyEntriesAllianceToUser(int fromId, int toId) {
        Alliance a = aDAO.findById(fromId);
        AllianceMember am = amDAO.findByAllianceIdNotTrial(a.getId(), toId).get(0);
        if (am == null) {
            DebugBuffer.addLine(DebugLevel.ERROR, "In alliance : " + a.getName() + " are only Trial members");
        }
        return copyEntriesUserToUser(am.getUserId(), toId, false);

    }

    private static boolean copyEntriesUserToUser(int fromId, int toId, boolean forced) {
        //Copy fromId => toId
        log.debug("Retrieved DR: " + fromId + "=>" + toId);
        if (!forced) {
            DiplomacyRelation dr = DiplomacyUtilities.getDiplomacyRel(fromId, toId);

            log.debug("Retrieved DR: DTID=" + dr.getDiplomacyTypeId() + " ACK=" + dr.getAcknowledged() + " TYPE=" + dr.getType() + " SM=" + dr.getSharingMap());
            if (!dr.getSharingMap()) {
                log.debug("Ignore share try forbidden!");
                return false;
            }
        }
        /*
        if (!eDAO.hasEmbassy(fromId, toId)) {
            DebugBuffer.addLine(DebugLevel.WARNING, "Ignore share try forbidden - NO EMBASSY!");
            return false;
        }*/

        ArrayList<UserShareEntry> shares = DiplomacyUtilities.getAffectedShareUsers(toId);
        ArrayList<ViewTable> vtList = vtDAO.findByUser(fromId);

        HashMap<Integer, Integer> procSysTime = new HashMap<Integer, Integer>();

        for (ViewTable vt : vtList) {
            if (!procSysTime.containsKey(vt.getSystemId())) {
                procSysTime.put(vt.getSystemId(), vt.getLastTimeVisited());
            } else {
                continue;
            }
        }

        ArrayList<Integer> users = new ArrayList<Integer>();
        for (UserShareEntry use : shares) {
            for (Integer user : use.getUsers()) {
                if (user != fromId);
                log.debug("Add user " + user);
                users.add(user);
            }
        }

        ArrayList<ICopyJob> cjBatch = new ArrayList<ICopyJob>();
        for (Map.Entry<Integer, Integer> vtEntry : procSysTime.entrySet()) {
            ViewTableCopyJob cj = new ViewTableCopyJob(users, vtEntry.getKey(), vtEntry.getValue());
            cjBatch.add(cj);
        }
        ThreadController.getViewTableThread().addJobBatch(cjBatch);

        return true;
    }

    public enum Direction {

        ALLIANCE_TO_ALLIANCE, USER_TO_ALLIANCE, ALLIANCE_TO_USER, USER_TO_USER
    }

    class ViewTableKey {

        private final int systemId;
        private final int planetId;

        public ViewTableKey(int systemId, int planetId) {
            this.systemId = systemId;
            this.planetId = planetId;
        }

        /**
         * @return the systemId
         */
        public int getSystemId() {
            return systemId;
        }

        /**
         * @return the planetId
         */
        public int getPlanetId() {
            return planetId;
        }
    }
}
