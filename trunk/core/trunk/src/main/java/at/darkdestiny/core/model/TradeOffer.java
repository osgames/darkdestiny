/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.ETradeOfferType;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "tradeoffer")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class TradeOffer extends Model<TradeOffer> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("tradePostId")
    @IndexAnnotation(indexName = "postRessourceIdx")
    private Integer tradePostId;
    @FieldMappingAnnotation("ressourceId")
    @IndexAnnotation(indexName = "postRessourceIdx")
    private Integer ressourceId;
    @FieldMappingAnnotation("quantity")
    private Long quantity;
    @FieldMappingAnnotation("type")
    private ETradeOfferType type;
    @FieldMappingAnnotation("sold")
    private Long sold;

    public static final int MAX_IRON_PRICE = 1000;
    public static final int MAX_STEEL_PRICE = 5000;
    public static final int MAX_TERKONIT_PRICE = 25000;
    public static final int MAX_YNKELONIUM_PRICE = 70000;
    public static final int MAX_HOWALGONIUM_PRICE = 120000;
    public static final int MAX_CVEMBINIUM_PRICE = 300000;
    public static final int MAX_CONSUMABLES_PRICE = 25000;
    public static final int MAX_FOOD_PRICE = 200;
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the ressourceId
     */
    public Integer getRessourceId() {
        return ressourceId;
    }

    /**
     * @param ressourceId the ressourceId to set
     */
    public void setRessourceId(Integer ressourceId) {
        this.ressourceId = ressourceId;
    }

    /**
     * @return the quantity
     */
    public Long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the type
     */
    public ETradeOfferType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ETradeOfferType type) {
        this.type = type;
    }

    /**
     * @return the sold
     */
    public Long getSold() {
        return sold;
    }

    /**
     * @param sold the sold to set
     */
    public void setSold(Long sold) {
        this.sold = sold;
    }

    /**
     * @return the tradePostId
     */
    public Integer getTradePostId() {
        return tradePostId;
    }

    /**
     * @param tradePostId the tradePostId to set
     */
    public void setTradePostId(Integer tradePostId) {
        this.tradePostId = tradePostId;
    }
}
