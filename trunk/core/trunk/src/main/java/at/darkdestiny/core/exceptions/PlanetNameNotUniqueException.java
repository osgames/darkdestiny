/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.exceptions;

/**
 *
 * @author Stefan
 */
public class PlanetNameNotUniqueException extends Exception {

    private static final long serialVersionUID = -8603578311339751496L;

    public PlanetNameNotUniqueException() {
    }

    public PlanetNameNotUniqueException(String message) {
        super(message);
    }

    public PlanetNameNotUniqueException(Throwable cause) {
        super(cause);
    }

    public PlanetNameNotUniqueException(String message, Throwable cause) {
        super(message, cause);
    }

}
