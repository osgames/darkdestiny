/*
 * CommandParser.java
 *
 * Created on 04. Juli 2008, 00:39
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.darkdestiny.core.chat;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Stefan
 */
public class CommandParser {
    public static final int LOGIN = 1;
    public static final int MSG = 2;
    public static final int MSGPRIVATE = 3;
    public static final int BAN = 4;
    public static final int UNBAN = 5;
    public static final int PONG = 6;
    public static final int CLOSE = 7;
    public static final int INVALID = 999;
    
    /** Creates a new instance of CommandParser */
    public CommandParser() {
        
    }
    
    public static ParameterEntry parseCommand(String response) {
        // DebugBuffer.addLine(DebugLevel.TRACE,"Incoming message: " + response);
        
        ParameterEntry pe = null;
                        
        StringTokenizer st = new StringTokenizer(response,"|");
        String[] elem = new String[st.countTokens()];
        
        int count = 0;
        while (st.hasMoreElements()) {
            elem[count] = (String)st.nextElement();
            count++;
        }
        
        // String[] elem = response.split("a");
        
        // DebugBuffer.addLine(DebugLevel.TRACE,"Command Elements: " + elem.length);
        
        if (elem.length == 0) {
            return new ParameterEntry(INVALID,null);
        }
        
        if (elem[0].equalsIgnoreCase("MSG")) {
            // DebugBuffer.addLine(DebugLevel.TRACE,"MSG identified");
            if (elem.length < 3) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                pars.add(elem[1]);
                pars.add(elem[2]);
                
                String lastPar = "";
                for (int i=3;i<elem.length;i++) {
                   lastPar += elem[i];
                }
                pars.add(lastPar);
                
                pe = new ParameterEntry(MSG,pars);
            }
        } else if (elem[0].equalsIgnoreCase("MSGPRIVATE")) {
            // DebugBuffer.addLine(DebugLevel.TRACE,"MSGPRIVATE identified");
            
            if (elem.length < 3) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                pars.add(elem[1]);
                String lastPar = "";
                for (int i=2;i<elem.length;i++) {
                   lastPar += elem[i];
                }
                pars.add(lastPar);
                
                pe = new ParameterEntry(MSGPRIVATE,pars);
            }            
        } else if (elem[0].equalsIgnoreCase("LOGIN")) {
            // DebugBuffer.addLine(DebugLevel.TRACE,"LOGIN identified");
            
            if (elem.length < 3) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                // DebugBuffer.addLine(DebugLevel.TRACE,"Name = " + elem[1]);
                pars.add(elem[1]);
                String lastPar = "";
                for (int i=2;i<elem.length;i++) {
                   lastPar += elem[i];
                }
                
                // DebugBuffer.addLine(DebugLevel.TRACE,"Pass = " + lastPar);
                pars.add(lastPar);
                
                pe = new ParameterEntry(LOGIN,pars);      
            }
        } else if (elem[0].equalsIgnoreCase("PONG")) {
            // DebugBuffer.addLine(DebugLevel.TRACE,"PONG identified");
            pe = new ParameterEntry(PONG,null);
        } else if (elem[0].equalsIgnoreCase("CLOSE")) {
            pe = new ParameterEntry(CLOSE,null);
        } else {
            // DebugBuffer.addLine(DebugLevel.TRACE,"Invalid Command identified");
            pe = new ParameterEntry(INVALID,null);
        }
        
        return pe;
    } 
}
