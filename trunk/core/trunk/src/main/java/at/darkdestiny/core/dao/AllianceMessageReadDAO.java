/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.AllianceMessageRead;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class AllianceMessageReadDAO extends ReadWriteTable<AllianceMessageRead> implements GenericDAO {

    public ArrayList<AllianceMessageRead> findByAllianceIdUserId(Integer allianceId, int userId) {
        AllianceMessageRead amr = new AllianceMessageRead();
        amr.setAllianceId(allianceId);
        amr.setUserId(userId);
        return find(amr);

    }
        public ArrayList<AllianceMessageRead> findByAllianceId(Integer allianceId) {
        AllianceMessageRead amr = new AllianceMessageRead();
        amr.setAllianceId(allianceId);
        return find(amr);

    }

    public AllianceMessageRead findByUserIdBoardId(int allianceId, int boardId, int userId) {
                AllianceMessageRead amr = new AllianceMessageRead();
        amr.setAllianceId(allianceId);
        amr.setUserId(userId);
        amr.setBoardId(boardId);
        ArrayList<AllianceMessageRead> result = find(amr);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public ArrayList<AllianceMessageRead> findByAllianceIdBoardId(int allianceId, int boardId) {
        AllianceMessageRead amr = new AllianceMessageRead();
        amr.setAllianceId(allianceId);
        amr.setBoardId(boardId);
        return find(amr);
    }

}
