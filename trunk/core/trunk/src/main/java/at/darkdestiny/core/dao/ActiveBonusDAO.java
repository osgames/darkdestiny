/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.enumeration.EBonusRange;
import at.darkdestiny.core.model.ActiveBonus;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ActiveBonusDAO extends ReadWriteTable<ActiveBonus> implements GenericDAO  {
    public ActiveBonus findByPlanetAndBonusId(int planetId, int bonusId) {
        ActiveBonus ab = new ActiveBonus();
        ab.setRefId(planetId);
        ab.setBonusRange(EBonusRange.PLANET);
        ab.setBonusId(bonusId);
        ArrayList<ActiveBonus> result = find(ab);
        
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
    
    public ArrayList<ActiveBonus> findByPlanetId(int planetId) {
        ActiveBonus ab = new ActiveBonus();
        ab.setRefId(planetId);
        ab.setBonusRange(EBonusRange.PLANET);
        ArrayList<ActiveBonus> result = find(ab);
        return result;
    }

    public ArrayList<ActiveBonus> findByBonusId(int bonusId) {
        ActiveBonus ab = new ActiveBonus();
        ab.setBonusId(bonusId);
        ArrayList<ActiveBonus> result = find(ab);
        return result;
    }
}
