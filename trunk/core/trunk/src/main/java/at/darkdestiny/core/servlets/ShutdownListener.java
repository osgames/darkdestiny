/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.CheckForUpdate;
import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameInit;
import at.darkdestiny.core.Threading.ThreadController;
import at.darkdestiny.core.Threading.ThreadEntry;
import at.darkdestiny.core.Threading.ThreadUtilities;
import at.darkdestiny.core.admin.SessionTracker;
import at.darkdestiny.core.ai.AIThreadHandler;
import at.darkdestiny.core.chat.ServerListener;
import at.darkdestiny.core.dao.GameDataDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.notification.NotificationBuffer;
import at.darkdestiny.core.scheduler.job.DeleteInactiveJob;
import at.darkdestiny.core.scheduler.job.StatisticUpdateJob;
import at.darkdestiny.core.scheduler.job.TickUpdateJob;
import at.darkdestiny.core.service.StartService;
import at.darkdestiny.core.webservice.ServerInformation;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import com.googlecode.flyway.core.Flyway;
import com.mysql.jdbc.AbandonedConnectionCleanupThread;
import java.awt.Toolkit;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.xml.ws.Endpoint;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 *
 * @author Stefan
 */
@WebListener
public class ShutdownListener implements ServletContextListener, HttpSessionListener {
    private static final Logger log = LoggerFactory.getLogger(ShutdownListener.class);
    // private static FkTestDAO fkTestDAO = (FkTestDAO) DAOFactory.get(FkTestDAO.class);
    // private static FkChildTestDAO fkChildTestDAO = (FkChildTestDAO) DAOFactory.get(FkChildTestDAO.class);
    private static Scheduler scheduler = null;
    Endpoint serverInformationEndpoint;


    @Override
    public void sessionCreated(HttpSessionEvent event) {
        SessionTracker.addSession(event);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        SessionTracker.removeSession(event);
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("STARTUP Context");

        Thread thread = Thread.currentThread();
        ClassLoader ccl = thread.getContextClassLoader(); // PUSH
        try {
            thread.setContextClassLoader(ClassLoader.getSystemClassLoader());
            Toolkit.getDefaultToolkit().createImage(new byte[]{});
        } finally {
            thread.setContextClassLoader(ccl); // POP
        }        
        
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml"});
        Flyway flyway = (Flyway)context.getBean("flyway");



        // ImageIO.setUseCache(false);
        // Thread thread = Thread.currentThread();
        // ClassLoader initialTCCL = thread.getContextClassLoader();
        /*
        try {
            ClassLoader systemCL = ClassLoader.getSystemClassLoader();

            systemCL.loadClass("sun.java2d.Disposer").newInstance();
        } catch (Exception e) {
            log.error("Loading of Disposer failed");
        }
        */
            // start AWT Windows thread Toolkit.getDefaultToolkit().getSystemEventQueue(); }


            //Properties dbProperties = new Properties();
           // dbProperties.load(GameConfig.class.getResourceAsStream("/db.properties"));
           //Flyway flyway = new Flyway();
           // flyway.setDataSource(dbProperties.getProperty(SetupProperties.DATABASE_URL), dbProperties.getProperty(SetupProperties.DATABASE_USER), dbProperties.getProperty(SetupProperties.DATABASE_PASS));
            try {
                flyway.init();
            } catch (Exception e) {
                System.out.println("Init failed: " + e.getMessage());
            }
            flyway.migrate();


        try {
            DbConnect.getInstance(GameConfig.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error setting Gameconfig to DbConnect");
        }


        generateSchedulers();
;

        //Only update if the game has been configured
        if (GameConfig.getInstance().isUpdateTickOnStartup() && !((GameDataDAO) DAOFactory.get(GameDataDAO.class)).findAll().isEmpty()) {
            log.info( "Update On Startup is true invoking StartService.updateTickToActualTime()");
            StartService.updateTickToActualTime();
        } else {
            log.info( "Update On Startup is false");
        }


        // FkTest fkTest = fkTestDAO.findById(1);
        // for (FkChildTest fkct : fkTest.getFkChildTest()) {
        //     System.out.println(fkct.getId() + " -- " + fkct.getRefId() + " -- " + fkct.getName());
        // }


        log.info( "STARTUP: " + sce.getServletContext().getServletContextName());
        log.info( "Try to initialize webservice!");
        try {


            String address = GameConfig.getInstance().getWebserviceURL();
            ServerInformation si = new ServerInformation();
            try {
                serverInformationEndpoint = Endpoint.publish(address, si);
            } catch (IllegalArgumentException iae) {
                DebugBuffer.error("Cannot instantiate Endpoint at address '" + address + "': " + iae.getMessage());
            } catch (Exception e) {
                DebugBuffer.error("Cannot instantiate Endpoint at address '" + address + "': " + e.getMessage());
            }

            try {
                URL u = new URL(address);
                HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                huc.setRequestMethod("GET");  //OR  huc.setRequestMethod ("HEAD");
                huc.connect();
                int code = huc.getResponseCode();
                DebugBuffer.addLine(DebugLevel.DEBUG, "ServerInformation Service answered with Code : " + code);

            } catch (Exception e) {
                DebugBuffer.addLine(DebugLevel.ERROR, "ServerInformation Service answered with err : " + e.getMessage());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            DebugBuffer.addLine(DebugLevel.DEBUG, "Trying to start AI threads");

            for (User u : ((UserDAO) DAOFactory.get(UserDAO.class)).findAll()) {
                if (u.getUserType().equals(EUserType.AI)) {

                    AIThreadHandler.addThread(u.getId());
                }
            }

        } catch (Exception e) {
            log.error("ERROR STARTING AI THREADS " + e);
            e.printStackTrace();
        }

        // GameConfig.getInstance();
        GameInit.initGame();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info( "DESTROY: " + sce.getServletContext().getServletContextName());

        try {
            scheduler.shutdown(true);
            scheduler = null;
        } catch (Exception e) {
           log.error("Shutdown of quartz scheduler failed");
        }

        // Stop chatt
        try {
            if (ServerListener.serverConnected()) {
                ServerListener.stop();
            }
        } catch (Exception e) {
            log.error("Error stopping ServerListener");
        }

        try {
            serverInformationEndpoint.stop();
        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error stopping IRC Endpoint . " + e.getMessage());
        }
        if (CheckForUpdate.getCheckForUpdateThread() != null) {
            CheckForUpdate.getCheckForUpdateThread().interrupt();
        }

        //Write notifications to User
        log.info("WRITING NOTIFICATIONS: " + sce.getServletContext().getServletContextName());
        try {
            NotificationBuffer.writeAll();
        } catch (Exception e) {
            log.info( "ERROR WRITING NOTIFICATIONS " + e);
        }
        try {
            log.info("Stopping Threads");

            AIThreadHandler.shutdownAI();
            ThreadController.stopAllThreads();

            log.info( "Threads stopped");
        } catch (Exception e) {
            log.info( "ERROR Stopping Threads " + e);

        }

        // Clean virtual and real tables
        DAOFactory.setVirtualMode(true);
        DAOFactory.dropAll();
        DAOFactory.setVirtualMode(false);
        DAOFactory.dropAll();

        /*
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);
        for(Thread t:threadArray) {
            if(t.getName().contains("Thread-5")) {
                synchronized(t) {
                    for (StackTraceElement ste : t.getStackTrace()) {
                        System.out.println(ste.toString());
                    }
                }
            }
        }
        */

        try {
            System.out.println("Stop Abandoned Connection Cleanup Thread");
            AbandonedConnectionCleanupThread.shutdown();
        } catch (InterruptedException e) {
            log.warn("SEVERE problem cleaning up: " + e.getMessage());
            e.printStackTrace();
        }

        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                log.info(String.format("Deregistering jdbc driver: %s", driver));
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
                log.info(String.format("Error deregistering driver %s", driver), e);
            }

        }

        /*
         ArrayList<DebugLine> lines = DebugBuffer.readBuffer();
         log.info( "Printing current state of debugBuffer");
         log.info( "=====================================");
         for (DebugLine dl : lines) {
         log.info( "<" + dl.getTimeStamp() + "> " + dl.getMessage());
         }
        */

         log.info( "=====================================");
         log.info( "Gather all Database Locks");
         log.info( "=====================================");
         for (Exception e : DAOFactory.gatherAllLocks()) {
         log.info( "Lock data found");
         e.printStackTrace();
         }

         log.info( "=====================================");
         log.info( "Printing current state of Threads");
         log.info( "=====================================");
         ArrayList<ThreadEntry> teList = ThreadUtilities.getAllThreads();
         for (ThreadEntry te : teList) {
            log.info( te.getThreadName() + " CPUTime > " + FormatUtilities.getFormattedDecimalCutEndZero(te.getCpuTime() / 1000000000d, 2) + " sek.");
         }

         /*
         for (Thread t : Thread.getAllStackTraces().keySet()) {
             if (t.getName().contains("Java2D Disposer")) {
                 log.info("Try to shutdown Java2D Disposer");
                 t.interrupt();
             }
         }
         */
    }

    private void generateSchedulers() {
        try {
            Properties props = new Properties();
            props.setProperty(StdSchedulerFactory.PROP_SCHED_SKIP_UPDATE_CHECK,"true");
            props.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
            props.setProperty("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
            props.setProperty("org.quartz.threadPool.threadCount", "5");

            SchedulerFactory schFactory = new StdSchedulerFactory(props);
            scheduler = schFactory.getScheduler();

            scheduler.start();
            scheduleStatisticUpdate(scheduler);
            if (GameConfig.getInstance().isDeleteInactive()) {
                System.out.println(">>>> Delete inactive ACTIVE");
                scheduleDeleteTrial(scheduler);
            } else {
                System.out.println(">>>> Delete inactive DISABLED");
            }
            scheduleTickUpdate(scheduler);
        } catch (SchedulerException ex) {
            java.util.logging.Logger.getLogger(ShutdownListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void scheduleStatisticUpdate(Scheduler scheduler) {
        try {

            JobDetail job = JobBuilder.newJob(StatisticUpdateJob.class)
                    .withIdentity("statisticUpdater")
                    .build();
            // always at 04:00
            CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                    .withIdentity("satisticTrigger", "crontriggergroup1")
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0 4 1/1 * ? *")) // Correct shedule
                    // .withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * 1/1 * ? *"))
                    .build();
            scheduler.scheduleJob(job, cronTrigger);
        } catch (SchedulerException ex) {
            java.util.logging.Logger.getLogger(ShutdownListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    private void scheduleTickUpdate(Scheduler scheduler) {
        try {

            JobDetail job = JobBuilder.newJob(TickUpdateJob.class)
                    .withIdentity("TickUpdateJob")
                    .build();
            CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                    .withIdentity("TickUpdateJobTrigger", "crontriggergroup1")
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * 1/1 * ? *"))
                    .build();
            scheduler.scheduleJob(job, cronTrigger);
        } catch (SchedulerException ex) {
            java.util.logging.Logger.getLogger(ShutdownListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void scheduleDeleteTrial(Scheduler scheduler) {
        try {

            JobDetail job = JobBuilder.newJob(DeleteInactiveJob.class)
                    .withIdentity("DeleteTrialJob")
                    .build();
            
            CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                    .withIdentity("DeleteTrialJobTrigger", "crontriggergroup1")
                    // .withSchedule(CronScheduleBuilder.cronSchedule("0 0 5 1/1 * ? *"))
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * 1/1 * ? *"))
                    .build();                    
           scheduler.scheduleJob(job, cronTrigger);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ShutdownListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
