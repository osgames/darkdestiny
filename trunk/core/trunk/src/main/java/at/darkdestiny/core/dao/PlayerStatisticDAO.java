/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

 import at.darkdestiny.core.model.PlayerStatistic;import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;
import java.util.GregorianCalendar;
;
import at.darkdestiny.core.model.PlayerStatistic;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 *
 * @author Eobane
 */
public class PlayerStatisticDAO extends ReadWriteTable<PlayerStatistic> implements GenericDAO {

    public ArrayList<PlayerStatistic> findByUserId(Integer userId) {

        PlayerStatistic ps = new PlayerStatistic();
        ps.setUserId(userId);
        ArrayList<PlayerStatistic> result = find(ps);
        return result;
    }

    public ArrayList<PlayerStatistic> findByDay(Long date) {

        GregorianCalendar searchMonth = new GregorianCalendar();
        searchMonth.setTimeInMillis(date);

        ArrayList<PlayerStatistic> result = new ArrayList<PlayerStatistic>();
        for (PlayerStatistic ps : findAll()) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(ps.getDate());
            //System.out.println("compare YEAR : [" + gc.get(GregorianCalendar.YEAR) + "] to [" + searchMonth.get(GregorianCalendar.YEAR) + "]");
            //System.out.println("compare DAY_OF_MONTH : [" + gc.get(GregorianCalendar.DAY_OF_MONTH) + "] to [" + searchMonth.get(GregorianCalendar.DAY_OF_MONTH) + "]");
            //System.out.println("compare MONTH : [" + gc.get(GregorianCalendar.MONTH) + "] to [" + searchMonth.get(GregorianCalendar.MONTH) + "]");

            if (gc.get(GregorianCalendar.YEAR) == searchMonth.get(GregorianCalendar.YEAR)
                    && gc.get(GregorianCalendar.DAY_OF_MONTH) == searchMonth.get(GregorianCalendar.DAY_OF_MONTH)
                    && gc.get(GregorianCalendar.MONTH) == searchMonth.get(GregorianCalendar.MONTH)) {
                result.add(ps);
            }

        }
        System.out.println("Returning : " + result.size() + " entries in findByDay");
        return result;
    }
}
