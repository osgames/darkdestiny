/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.exceptions;

/**
 *
 * @author Stefan
 */
public class InvalidInstantiationException extends RuntimeException {
    private static final long serialVersionUID = -8603571115339751496L;

    public InvalidInstantiationException(String message) {
        super(message);
    }
}
