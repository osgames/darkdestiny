/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.trade.special;

import at.darkdestiny.core.tools.graph.Node;
import java.util.LinkedList;

/**
 *
 * @author Stefan
 */
public class TransferringEntry {
    private LinkedList<Node> path = new LinkedList<Node>();
    private int quantity = 0;
    
    public void setPath(LinkedList<Node> path) {
        this.path = path;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public Node getTargetNode() {
        return path.getLast();
    }
    
    public Node getStartNode() {
        return path.getFirst();
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }
}
