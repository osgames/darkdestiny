package at.darkdestiny.core.construction;

/**
 * @author martin
 * @author Rayden
 */
public interface SetupValues {
    public final double BUILDTIME_MULTI = 0.2d;
    public final double CREDIT_MULTI = 0.2d;
    public final int CREDIT_COST_PERSPACE = 200;
    public final double RESSOURCE_MULTI = 0.6d;
}
