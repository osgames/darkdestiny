package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.framework.dao.DAOFactory;

public class CPlanetCount extends AbstractCondition {

    private ParameterEntry planetCountValue;
    private ParameterEntry comparator;
    private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);

    public CPlanetCount(ParameterEntry planetCountValue, ParameterEntry comparator) {
        this.planetCountValue = planetCountValue;
        this.comparator = comparator;
    }

    public boolean checkCondition(int userId, int conditionToTitleId) {
        int planetCount = ppDAO.findByUserId(userId).size();

        return compare(planetCountValue.getParamValue().getValue(), planetCount, planetCountValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

    }
}
