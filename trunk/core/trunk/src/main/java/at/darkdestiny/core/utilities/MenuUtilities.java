package at.darkdestiny.core.utilities;

import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.Message;
import at.darkdestiny.core.model.Note;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.service.AllianceBoardService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.service.TransportService;
import at.darkdestiny.core.view.menu.CreditsEntity;
import at.darkdestiny.core.view.menu.DevelopementEntity;
import at.darkdestiny.core.view.menu.EnergyEntity;
import at.darkdestiny.core.view.menu.MessageEntity;
import at.darkdestiny.core.view.menu.PopulationFoodEntity;
import at.darkdestiny.core.view.menu.RessourceEntity;
import at.darkdestiny.core.view.menu.StatusBarData;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Dreloc
 */
public class MenuUtilities extends Service {

    private Menu menu;
    private HashMap<Integer, HashMap<Boolean, Long>> tradeBalances;
    private HashMap<Integer, HashMap<Boolean, Long>> transportBalances;

    public MenuUtilities(Menu menu) {
        this.menu = menu;

    }

    public StatusBarData loadStatusBarData(StatusBarData statusBarData) {
        StatusBarData stb = statusBarData;


        //Updaten aller Werte
        stb = updateAll(stb);

        return stb;
    }

    public StatusBarData updateNote(StatusBarData statusBarData) {

        Note n = Service.noteDAO.findBy(menu.getUserId(), menu.getPlanetId());
        statusBarData.setNote(n);
        return statusBarData;

    }

    public static StatusBarData addRessources(StatusBarData statusBarData) {
        StatusBarData stb = statusBarData;
        /**
         *
         * Initialisiern des RessourceArrays mit den Ids
         * der Rohstoffe von Eisen,Stahl,Terkonit,Ynkelonium und Howalgonium
         * Diese werden aus der Datenbank gelanden!
         *
         */
        if (stb.getRessources().isEmpty()) {
            for (Ressource r : ressourceDAO.findAllDisplayableRessources()) {
                stb.addRessource(new RessourceEntity(r));
            }
        }
        if (stb.getRawMaterials().isEmpty()) {
            for (Ressource r : ressourceDAO.findAllMineableRessources()) {
                stb.addRawMaterial(new RessourceEntity(r));
            }
        }
        return stb;
    }

    private StatusBarData updateAll(StatusBarData stb) {
        /**
         *
         * Updatet alle Werte der StatusBarData
         *
         *
         */
        // Erneuern des Message Status
        stb.setMessages(updateMessageStatus(stb.getMessages()));
        //Erneuern der Credits und planetaren Creditszunahme
        stb.setCredits(updateCreditsStatus(stb.getCredits()));
        // Erneuern der Bev?lkerungsanzahl und NahrungsVerbrauch/Produktion
        stb.setPopulationFood(updatePopulationFood(stb.getPopulationFood()));
        // Erneuern der Energieanzeige
        stb.setEnergy(updatesetEnergy(stb.getEnergy()));
        // Erneuern der Ressourcedaten f?r Eisen Stahl usw
        stb.setRessources(updateRessources(stb.getRessources()));
        stb.setRawMaterials(updateRawMaterials(stb.getRawMaterials()));

        stb.setDevelopement(updateDevelopementStatus(stb.getDevelopement()));

        stb.setNote(updateNote(stb).getNote());

        return stb;
    }

    public MessageEntity updateMessageStatus(MessageEntity messageEntity) {
        boolean aMessage = false;
        boolean pMessage = false;
        boolean sMessage = false;

        /**
         *
         * @TODO Auslesen ob eine Nachricht f?r den Spieler anliegt
         *
         */
        ArrayList<Message> messages = messageDAO.findByUserId(menu.getUserId(), EMessageType.SYSTEM);
        for (Message m : messages) {
            if (!m.getViewed() && !m.getDelByTarget()) {
                sMessage = true;
                break;
            }
        }
        messages = messageDAO.findByUserId(menu.getUserId(), EMessageType.USER);
        messages.addAll(messageDAO.findByUserId(menu.getUserId(), EMessageType.ALLIANCE));
        
        for (Message m : messages) {
            if (!m.getViewed()) {
                pMessage = true;
                break;
            }
        }

        if (AllianceBoardService.newMessagesAvailable(menu.getUserId())) {
            aMessage = true;
        }


        //AllianceNachricht
        messageEntity.setAMessageReceived(aMessage);
        //PrivateNachricht
        messageEntity.setPMessageReceived(pMessage);
        //SystemNachricht
        messageEntity.setSMessageReceived(sMessage);

        return messageEntity;
    }

    public DevelopementEntity updateDevelopementStatus(DevelopementEntity credits) {
        double globalDev = 0;
        double planetDev = 0;

        UserData ud = userDataDAO.findByUserId(menu.getUserId());
        if (ud != null) {
            globalDev = ud.getDevelopementPoints();
        }
       
        // planetDev = Math.round(menu.getEpcr().getDevelopementPointsGlobal() * 10000d) / 10000d;
        credits.setGlobalPoints(globalDev);
        credits.setPlanetPoints(menu.getEpcr().getDevelopementPointsGlobal());


        return credits;
    }

    public CreditsEntity updateCreditsStatus(CreditsEntity credits) {
        long globalCredits = 0;
        long planetCredits = 0;

        UserData ud = userDataDAO.findByUserId(menu.getUserId());
        if (ud != null) {
            globalCredits = ud.getCredits();
        }

        planetCredits = menu.getEpcr().getTaxIncome();

        credits.setGlobalCredits(globalCredits);
        credits.setPlanetCredits(planetCredits);

        return credits;
    }

    public PopulationFoodEntity updatePopulationFood(PopulationFoodEntity populationFood) {
        PlayerPlanet pp = playerPlanetDAO.findByPlanetId(menu.getPlanetId());
        populationFood.setPlanetPopulation(pp.getPopulation());

        long possibleGrowth = menu.getEpcr().getMaxPopulation() - (pp.getPopulation() + pp.getMigration());
        long theoreticalGrowth = (long) (((float) pp.getPopulation() / (float) 100) * pp.getGrowth() / 360f);
        long realGrwoth = Math.min(theoreticalGrowth, possibleGrowth);

        populationFood.setGrowth(pp.getGrowth());
        populationFood.setTaxes(pp.getTax());
        populationFood.setGrowthPopulation(realGrwoth);
        populationFood.setMoral(pp.getMoral());
        populationFood.setEmigration(pp.getMigration());

        populationFood.getFood().getProductionUsage().setNameLater("Nahrungsproduktion in Zukunft");
        populationFood.getFood().getProductionUsage().setNameNow("Nahrungsproduktion");

        populationFood.getFood().getProductionUsage().setProductionNow((int) Math.floor(menu.getPr().getRessProduction(Ressource.FOOD)));
        populationFood.getFood().getProductionUsage().setUsageNow(menu.getPr().getRessBaseConsumption(Ressource.FOOD));
        populationFood.getFood().getStorage().setStorageCapicity(menu.getPr().getRessMaxStock(Ressource.FOOD));
        populationFood.getFood().getStorage().setStorageStock(menu.getPr().getRessStock(Ressource.FOOD));

        populationFood.getFood().getStorage().setStorageMin_Production(menu.getPr().getRessMinStock(Ressource.FOOD));
        long qtyTrade = 0;
        PlanetRessource prTrade = planetRessourceDAO.findBy(menu.getPlanetId(), Ressource.FOOD, EPlanetRessourceType.MINIMUM_TRADE);
        if (prTrade != null) {
            qtyTrade = prTrade.getQty();
        }
        long qtyTransport = 0;
        PlanetRessource prTransport = planetRessourceDAO.findBy(menu.getPlanetId(), Ressource.FOOD, EPlanetRessourceType.MINIMUM_TRANSPORT);
        if (prTransport != null) {
            qtyTransport = prTransport.getQty();
        }
        populationFood.getFood().getStorage().setStorageMin_Trade(qtyTrade);
        populationFood.getFood().getStorage().setStorageMin_Transport(qtyTransport);
        {
            if (tradeBalances == null);
            tradeBalances = TransportService.findShippingBalanceForPlanetSorted(menu.getPlanetId(), ETradeRouteType.TRADE);
            HashMap<Boolean, Long> balances = tradeBalances.get(Ressource.FOOD);
            if (balances == null) {
                populationFood.getFood().getTrade().setOutgoing(0);
                populationFood.getFood().getTrade().setIncoming(0);
            } else {
                Long balance = balances.get(Boolean.TRUE);
                if (balance == null) {
                    balance = 0l;
                }
                populationFood.getFood().getTrade().setOutgoing(Math.round(balance));
                balance = balances.get(Boolean.FALSE);
                if (balance == null) {
                    balance = 0l;
                }
                populationFood.getFood().getTrade().setIncoming(Math.round(balance));
            }

        }
        //Transport

        {
            if (transportBalances == null);
            transportBalances = TransportService.findShippingBalanceForPlanetSorted(menu.getPlanetId(), ETradeRouteType.TRANSPORT);
            HashMap<Boolean, Long> transBalances = transportBalances.get(Ressource.FOOD);
            if (transBalances == null) {
                populationFood.getFood().getTransport().setOutgoing(0);
                populationFood.getFood().getTransport().setIncoming(0);
            } else {
                Long balance = transBalances.get(Boolean.TRUE);
                if (balance == null) {
                    balance = 0l;
                }
                populationFood.getFood().getTransport().setOutgoing(Math.round(balance));
                balance = transBalances.get(Boolean.FALSE);
                if (balance == null) {
                    balance = 0l;
                }
                populationFood.getFood().getTransport().setIncoming(Math.round(balance));
            }
        }

        // populationFood.getFood().getProductionUsage().setProductionLater(productionLater);
        // populationFood.getFood().getProductionUsage().setUsageLater(usageLater);
        
        return populationFood;
    }

    public EnergyEntity updatesetEnergy(EnergyEntity energy) {
        energy.getEnergy().getProductionUsage().setNameNow("Energieverbrauch");
        energy.getEnergy().getProductionUsage().setNameLater("Energieverbrauch in Zukunft");
        energy.getEnergy().getProductionUsage().setProductionNow(menu.getPr().getRessProduction(Ressource.ENERGY));
        energy.getEnergy().getProductionUsage().setProductionLater(menu.getPrFuture().getRessProduction(Ressource.ENERGY));
        energy.getEnergy().getProductionUsage().setUsageNow(menu.getPr().getRessConsumption(Ressource.ENERGY));
        energy.getEnergy().getProductionUsage().setUsageLater(menu.getPrFuture().getRessConsumption(Ressource.ENERGY));

        return energy;
    }

    public ArrayList<RessourceEntity> updateRessources(ArrayList<RessourceEntity> ressources) {
        if (tradeBalances == null);
        tradeBalances = TransportService.findShippingBalanceForPlanetSorted(menu.getPlanetId(), ETradeRouteType.TRADE);
        for (int i = 0; i < ressources.size(); i++) {
            RessourceEntity ressource = ressources.get(i);
            ressource = updateRessource(ressource);

        }
if (transportBalances == null);
        transportBalances = TransportService.findShippingBalanceForPlanetSorted(menu.getPlanetId(), ETradeRouteType.TRANSPORT);
        for (int i = 0; i < ressources.size(); i++) {
            RessourceEntity ressource = ressources.get(i);
            ressource = updateRessource(ressource);

        }
        return ressources;
    }

    public ArrayList<RessourceEntity> updateRawMaterials(ArrayList<RessourceEntity> ressources) {
        for (int i = 0; i < ressources.size(); i++) {
            RessourceEntity ressource = ressources.get(i);
            ressource = updateRawMaterial(ressource);

        }

        return ressources;
    }

    public RessourceEntity updateRawMaterial(RessourceEntity ressource) {
        //Id der Ressource in der Datenbank:
        int ressourceId = ressource.getRessource().getId();

        String name = "";

        name = ressourceDAO.findRessourceById(ressourceId).getName();
        ressource.getProductionUsage().setNameNow(name + " Produktion");
        ressource.getProductionUsage().setNameLater(name + " Produktion in Zukunft");


        ressource.getProductionUsage().setProductionNow(menu.getPr().getRessProduction(ressource.getRessource().getId()));
        ressource.getProductionUsage().setUsageNow(menu.getPr().getRessConsumption(ressource.getRessource().getId()));

        ressource.getStorage().setStorageStock(menu.getPr().getRessStock(ressource.getRessource().getId()));
        ressource.getStorage().setStorageCapicity(menu.getPr().getRessMaxStock(ressource.getRessource().getId()));
        ressource.getStorage().setStorageMin_Production(menu.getPr().getRessMinStock(ressource.getRessource().getId()));

        // Load values from traderoutes



        // ressource.getProductionUsage().setProductionLater(productionLater);
        // ressource.getProductionUsage().setUsageLater(usageLater);

        return ressource;
    }

    public RessourceEntity updateRessource(RessourceEntity ressource) {
        //Id der Ressource in der Datenbank:
        int ressourceId = ressource.getRessource().getId();

        String name = "";

        name = ressourceDAO.findRessourceById(ressourceId).getName();
        ressource.getProductionUsage().setNameNow(name + " Produktion");
        ressource.getProductionUsage().setNameLater(name + " Produktion in Zukunft");


        ressource.getProductionUsage().setProductionNow(menu.getPr().getRessProduction(ressource.getRessource().getId()));
        ressource.getProductionUsage().setUsageNow(menu.getPr().getRessConsumption(ressource.getRessource().getId()));

        ressource.getStorage().setStorageStock(menu.getPr().getRessStock(ressource.getRessource().getId()));
        ressource.getStorage().setStorageCapicity(menu.getPr().getRessMaxStock(ressource.getRessource().getId()));
        ressource.getStorage().setStorageMin_Production(menu.getPr().getRessMinStock(ressource.getRessource().getId()));

        long qtyTrade = 0;
        PlanetRessource prTrade = planetRessourceDAO.findBy(menu.getPlanetId(), ressourceId, EPlanetRessourceType.MINIMUM_TRADE);
        if (prTrade != null) {
            qtyTrade = prTrade.getQty();
        }
        long qtyTransport = 0;
        PlanetRessource prTransport = planetRessourceDAO.findBy(menu.getPlanetId(), ressourceId, EPlanetRessourceType.MINIMUM_TRANSPORT);
        if (prTransport != null) {
            qtyTransport = prTransport.getQty();
        }
        ressource.getStorage().setStorageMin_Trade(qtyTrade);
        ressource.getStorage().setStorageMin_Transport(qtyTransport);

        // Load values from traderoutes

        //  ArrayList<at.darkdestiny.core.model.TradeRoute> outRoutes = tradeRouteDAO.findByStartPlanet(menu.getPlanetId());
        //   ArrayList<at.darkdestiny.core.model.TradeRoute> inRoutes = tradeRouteDAO.findByEndPlanet(menu.getPlanetId());
        {
            HashMap<Boolean, Long> balances = tradeBalances.get(ressourceId);
            if (balances == null) {
                ressource.getTrade().setOutgoing(0);
                ressource.getTrade().setIncoming(0);
            } else {
                Long balance = balances.get(Boolean.TRUE);
                if (balance == null) {
                    balance = 0l;
                }
                ressource.getTrade().setOutgoing(Math.round(balance));
                balance = balances.get(Boolean.FALSE);
                if (balance == null) {
                    balance = 0l;
                }
                ressource.getTrade().setIncoming(Math.round(balance));
            }
        }
        {
            //Transport
            HashMap<Boolean, Long> balances = transportBalances.get(ressourceId);
            if (balances == null) {
                ressource.getTransport().setOutgoing(0);
                ressource.getTransport().setIncoming(0);
            } else {
                Long balance = balances.get(Boolean.TRUE);
                if (balance == null) {
                    balance = 0l;
                }
                ressource.getTransport().setOutgoing(Math.round(balance));
                balance = balances.get(Boolean.FALSE);
                if (balance == null) {
                    balance = 0l;
                }
                ressource.getTransport().setIncoming(Math.round(balance));
            }
        }
        return ressource;
    }
}
