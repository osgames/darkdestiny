/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.notification;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.ML;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.RessourceDAO;
import at.darkdestiny.core.enumeration.EMessageContentType;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Admin
 */
public class TradeNotification extends Notification {

    public static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
    public static RessourceDAO rDAO = (RessourceDAO) DAOFactory.get(RessourceDAO.class);
    String msg = "";
    private int ressId;
    private int planetId;
    private long amount;
    private double price;

    public TradeNotification(int userId, int ressId, int planetId, long amount, double price) {
        super(userId);
        this.ressId = ressId;
        this.planetId = planetId;
        this.amount = amount;
        this.price = price;
        generateMessage();
        persistMessage();
    }

    public void generateMessage() {


        msg = ML.getMLStr("notification_trade_msg", userId).replace("%PLANET%", ppDAO.findByPlanetId(planetId).getName()).replace("%AMOUNT%", FormatUtilities.getFormattedNumber(amount)).replace("%RESSOURCE%", ML.getMLStr(rDAO.findRessourceById(ressId).getName(), userId));
        String topic = ML.getMLStr("notification_trade_topic", userId).replace("%AMOUNT%", FormatUtilities.getFormattedNumber(amount)).replace("%RESSOURCE%", ML.getMLStr(rDAO.findRessourceById(ressId).getName(), userId));

        gm.setMessageContentType(EMessageContentType.INFO);
        gm.setTopic(topic);
        gm.setMsg(msg);
    }

    public String getMessage() {
        return msg;
    }
}
