/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.requestbuffer;

import at.darkdestiny.core.databuffer.fleet.Location;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.utilities.FleetUtilities;

/**
 *
 * @author Stefan
 */
public class FleetParameterBuffer extends ParameterBuffer {

    public static final String FLEET_ID = "fleetId";
    public static final String TARGET_ID = "targetId";
    public static final String TO_SYSTEM = "toSystem";
    public static final String LOCATION_TYPE = "locationType";
    public static final String LOCATION_ID = "locationId";
    public static final String RETREAT_FACTOR = "retreat";
    public static final String RETREAT_TO_SYS = "retreatToType";
    public static final String RETREAT_TO_ID = "retreatTo";
    public static final String TRIAL_WARNING = "trialMsg";
    public static final String SCAN_WARNING = "scanMsg";
    public static final String SOURCEPAGE = "sourcepage";

    public FleetParameterBuffer(int userId) {
        super(userId);
    }

    public int getFleetId() {
        try {
            return Integer.parseInt(((String) getParameter(FLEET_ID)).replace("FF", ""));
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isFleetFormation() {
        return ((String) getParameter(FLEET_ID)).startsWith("FF");
    }

    public int getTarget() throws Exception {
        return FleetUtilities.resolveLocationByNameOrId((String) getParameter(TARGET_ID), getUserId(), targetIsSystem());
    }

    public boolean targetIsSystem() {
        return ((String) getParameter(TO_SYSTEM)).equalsIgnoreCase("true");
    }

    public int getRetreatValue() {
        if ((String) getParameter(RETREAT_FACTOR) != null) {
            return Integer.parseInt((String) getParameter(RETREAT_FACTOR));
        } else {
            return 100;
        }
    }

    public boolean isRetreatLocSystem() {
        if (getParameter(RETREAT_TO_SYS) != null) {
            return ((ELocationType) getParameter(RETREAT_TO_SYS)) == ELocationType.SYSTEM;
        } else {
            return false;
        }

    }

    public int getRetreatTo() {
        if ((((String) getParameter(RETREAT_TO_ID)) == null) || (((String) getParameter(RETREAT_TO_ID)).equalsIgnoreCase(""))) {
            return 0;
        }
        return Integer.parseInt((String) getParameter(RETREAT_TO_ID));
    }

    public boolean isTrialWarning() {
        if ((String) getParameter(TRIAL_WARNING) != null) {
            return ((String) getParameter(TRIAL_WARNING)).equalsIgnoreCase("true");
        } else {
            return false;
        }
    }

    public boolean isScanWarning() {
        if ((String) getParameter(SCAN_WARNING) != null) {
            return ((String) getParameter(SCAN_WARNING)).equalsIgnoreCase("true");
        } else {
            return false;
        }
    }

    public int getSourcePage() {
        return (Integer) getParameter(SOURCEPAGE);
    }
    
    public Location getLocation() {
        int locationId;
        
        try {
            locationId = Integer.parseInt((String) getParameter(LOCATION_ID));
        } catch (NumberFormatException nfe) {
            return null;
        }
        
        if (Location.verifyLocation(ELocationType.valueOf((String) getParameter(LOCATION_TYPE)),locationId)) {
            Location loc = new Location(ELocationType.valueOf((String) getParameter(LOCATION_TYPE)),locationId);
            return loc;
        } else {
            return null;
        }
    }
}
