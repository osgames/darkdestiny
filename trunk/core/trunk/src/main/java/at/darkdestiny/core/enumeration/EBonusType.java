/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Dreloc
 */
public enum EBonusType {
        POPULATION_GROWTH, PRODUCTION, CONSTRUCTION, RESEARCH, TRADE, CREDIT, MINING;
}
