/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.SunTransmitterRoute;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Stefan
 */
public class SunTransmitterRouteDAO extends ReadWriteTable<SunTransmitterRoute> implements GenericDAO  {
    public SunTransmitterRoute findById(int id) {
        SunTransmitterRoute str = new SunTransmitterRoute();
        str.setId(id);
        return (SunTransmitterRoute)get(str);
    }
}
