/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.service;

 import at.darkdestiny.core.model.News;import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
;
import at.darkdestiny.core.model.News;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Bullet
 */
public class NewsService extends Service{

    private static final Logger log = LoggerFactory.getLogger(NewsService.class);
/*
 * news.java
 *
 * Created on 9. Mai 2007, 20:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */


    public static String process(Map<String, String[]> pmap, int type, News inputNE, int userId) {
        News NE = new News();
        //String parname = "";
        String subject = "";
        String message = "";
        int languageId = 0;
        switch (type) {

            case 1:



                for (final Map.Entry<String, String[]> entry : pmap.entrySet()) {
                    String parName = entry.getKey();

                    // DebugBuffer.addLine("Check Parameter " + parName + " --> " + ((String[])(pmap.get(parName)))[0]);

                    if (parName.equalsIgnoreCase("subject")) {
                        subject = entry.getValue()[0];
                    }
                    if (parName.equalsIgnoreCase("message")) {
                        message = entry.getValue()[0];
                    }
                    if (parName.equalsIgnoreCase("langId")) {
                        languageId = Integer.parseInt(entry.getValue()[0]);
                        log.debug("langId : " + languageId);
                        log.debug("langId : " + entry.getValue()[0]);
                    }
                }

                NE.setSubmittedById(userId);
                NE.setSubject(subject);
                NE.setMessage(message);
                NE.setIsArchived(false);
                NE.setDate(System.currentTimeMillis());
                NE.setLanguageId(languageId);

                newsDAO.add(NE);
                break;

            case 2:

                newsDAO.remove(inputNE);

                break;

            case 3:

                setToArchive(inputNE);

                break;
            case 4:


                setToArchive(inputNE);

                break;

            case 5:

                for (final Map.Entry<String, String[]> entry : pmap.entrySet()) {
                    String parName = entry.getKey();

                    // DebugBuffer.addLine("Check Parameter " + parName + " --> " + ((String[])(pmap.get(parName)))[0]);

                    if (parName.equalsIgnoreCase("subject")) {
                        subject = entry.getValue()[0];
                    }
                    if (parName.equalsIgnoreCase("message")) {
                        message = entry.getValue()[0];
                    }
                }

                inputNE.setSubject(subject);
                inputNE.setMessage(message);
                newsDAO.update(inputNE);
        }

        return "";
    }

    public static ArrayList<News> getAllNews(int languageId) {

        ArrayList<News> allNews = newsDAO.findByLanguageId(languageId);
        TreeMap<Long, News> tmp = new TreeMap<Long, News>();
        for(News n : allNews){
            tmp.put(n.getDate(), n);
        }
        allNews = new ArrayList<News>();
        allNews.addAll(tmp.descendingMap().values());

        return allNews;
    }



    public static void setToArchive(News NE) {
        if (NE.getIsArchived()) {
            NE.setIsArchived(false);
        } else {
            NE.setIsArchived(true);
        }


        newsDAO.update(NE);

    }

    public static News getNews(int newsId) {
        return newsDAO.findById(newsId);

    }

    public static String getUserName(int userId) {
        try{
           return userDAO.findById(userId).getGameName();
        }catch(Exception e){
            return "nonamefound";
        }
    }

}
