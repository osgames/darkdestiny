/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.dao.ModuleDAO;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public abstract class AbstractBattleShip extends CombatUnit implements IBattleShip {
    // Basic Data
    protected int shipFleetId;
    protected float speed;
    protected EShipType purpose;

    protected boolean isDefenseStation;
    protected boolean isPlanetaryDefense;

    // Combat Logging
    protected ModuleDAO mDAO = (ModuleDAO)DAOFactory.get(ModuleDAO.class);

    public AbstractBattleShip() {
        super(0,0,0,CombatGroupFleet.UnitTypeEnum.SHIP);
        throw new UnsupportedOperationException("WTF MAN .. this is old stuff");
    }

    public AbstractBattleShip(int designId, int shipFleetId, int count) {
        super(designId,shipFleetId,count,CombatGroupFleet.UnitTypeEnum.SHIP);
    }

    public AbstractBattleShip(int designId, int count) {
        super(designId,count,CombatGroupFleet.UnitTypeEnum.PLATTFORM_DEFENSE);
    }
}
