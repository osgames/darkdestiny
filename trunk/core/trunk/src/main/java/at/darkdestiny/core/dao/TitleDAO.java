/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Title;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class TitleDAO extends ReadWriteTable<Title> implements GenericDAO {

    public Title findById(int id) {
        Title title = new Title();
        title.setId(id);
        return(Title)get(title);
    }
    public ArrayList<Title> findByCampaignId(int campaignId) {
        Title title = new Title();
        title.setCampaignId(campaignId);
        return(ArrayList<Title>)find(title);
    }

}
