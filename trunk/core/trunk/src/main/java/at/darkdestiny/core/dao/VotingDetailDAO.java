/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.VotingDetail;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class VotingDetailDAO extends ReadWriteTable<VotingDetail> implements GenericDAO {
    public ArrayList<VotingDetail> findByVoteId(int voteId) {
        VotingDetail vd = new VotingDetail();
        vd.setVoteId(voteId);
        return find(vd);
    }

    public ArrayList<VotingDetail> findByVoteIdAndOption(int voteId, int optionId) {
        VotingDetail vd = new VotingDetail();
        vd.setVoteId(voteId);
        vd.setOptionVoted(optionId);
        return find(vd);
    }

    public VotingDetail findByVoteIdAndUser(int voteId, int userId) {
        VotingDetail vd = new VotingDetail();
        vd.setVoteId(voteId);
        vd.setUserId(userId);

        ArrayList<VotingDetail> vdList = find(vd);
        if (vdList.isEmpty()) {
            return null;
        } else {
            return vdList.get(0);
        }
    }
}
