/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.notification;

import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.notification.TerritoryNotification.ETerritoryNotificationType;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.update.ProdOrderBufferEntry;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Aion
 */
public class NotificationBuffer extends Service {

    // HashMap<UserId, TreeMap <PlanetId, TreeMap<ProdOrderType, ProdOrderBufferEnty>>
    public static HashMap<Integer, TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>>> prodOrderNots;
    public static HashMap<Integer, TreeMap<Integer, EnumMap<TerritoryNotification.ETerritoryNotificationType, TerritoryNotification>>> territoryNotifications;
    public static HashMap<Integer, ArrayList<Research>> researchNots;

    static {
        territoryNotifications = new HashMap<Integer, TreeMap<Integer, EnumMap<ETerritoryNotificationType, TerritoryNotification>>>();
    }

    public static TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> getByUserId(int userId) {
        if (prodOrderNots == null || researchNots == null) {
            init();
        }
        return prodOrderNots.get(userId);
    }

    public static void init() {
        if (prodOrderNots == null) {
            prodOrderNots = new HashMap<Integer, TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>>>();
        }
        if (researchNots == null) {
            researchNots = new HashMap<Integer, ArrayList<Research>>();
        }

    }

    public static void removePONotification(int userId) {
        prodOrderNots.remove(userId);
    }

    public static void removePONotification(int userId, int planetId) {
        if (prodOrderNots.get(userId) != null) {
            if (prodOrderNots.get(userId).get(planetId) != null && !prodOrderNots.get(userId).isEmpty()) {
                prodOrderNots.get(userId).remove(planetId);
            }
        }
    }

    public static void addPONotification(int userId, int actPlanet, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>> notification) {
        if (prodOrderNots == null) {
            init();
        }

        TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> userEntries = prodOrderNots.get(userId);
        if (userEntries == null) {
            userEntries = new TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>>();
        }
        EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>> planetEntries = userEntries.get(actPlanet);
        if (planetEntries == null) {
            planetEntries = notification;
        } else {
            for (Map.Entry<EProductionOrderType, ArrayList<ProdOrderBufferEntry>> entries : notification.entrySet()) {
                ArrayList<ProdOrderBufferEntry> existingOrders = planetEntries.get(entries.getKey());

                if (existingOrders == null) {
                    existingOrders = entries.getValue();
                    planetEntries.put(entries.getKey(), existingOrders);
                } else {
                    existingOrders.addAll(entries.getValue());
                    planetEntries.put(entries.getKey(), existingOrders);
                }
            }
        }
        userEntries.put(actPlanet, planetEntries);

        prodOrderNots.put(userId, userEntries);

        // testOutput();
    }

    public static void writeNotifications(int userId) {
        TreeMap<Integer, EnumMap<EProductionOrderType, ArrayList<ProdOrderBufferEntry>>> userNotifications = getByUserId(userId);


        //CONSTRUCTION/GROUNDTROOPS/PRODUCTION
        if (userNotifications != null) {
            ProductionOrderNotification pon = new ProductionOrderNotification(userNotifications, userId);
            prodOrderNots.remove(userId);
        }

        //RESEARCH
        ArrayList<Research> finRes = researchNots.get(userId);
        if (finRes != null) {
            ResearchNotification rn = new ResearchNotification(finRes, userId);
            //   ResearchNotification.createNotification(finRes, userId, logged);
            researchNots.remove(userId);
        }

    }

    public static void addTerritoryNotification(TerritoryNotification tn) {

        DebugBuffer.addLine(DebugLevel.DEBUG, "Adding territoryNotification");
        TreeMap<Integer, EnumMap<TerritoryNotification.ETerritoryNotificationType, TerritoryNotification>> territory = territoryNotifications.get(tn.tms.getTerritoryId());
        DebugBuffer.addLine(DebugLevel.DEBUG, "territory notifcations for territory : " + tn.tms.getTerritoryId() + " = " + territory);
        if (territory == null) {
            territory = new TreeMap<Integer, EnumMap<ETerritoryNotificationType, TerritoryNotification>>();
        }
        EnumMap<TerritoryNotification.ETerritoryNotificationType, TerritoryNotification> user = territory.get(tn.tms.getRefId());
        DebugBuffer.addLine(DebugLevel.DEBUG, "territory notifcations for user : " + tn.tms.getRefId() + " = " + tn.tms.getRefId());
        if (user == null) {
            user = new EnumMap<ETerritoryNotificationType, TerritoryNotification>(TerritoryNotification.ETerritoryNotificationType.class);
        }
        //
        TerritoryNotification tnTmp = user.get(tn.type);
        //Notification for this territory and usser already existing

        DebugBuffer.addLine(DebugLevel.DEBUG, "territory notifcations for type : " + tn.type + " = " + tnTmp);
        if (tnTmp != null) {
            return;
        }
        DebugBuffer.addLine(DebugLevel.DEBUG, "a");
        if (tn.type.equals(TerritoryNotification.ETerritoryNotificationType.ADDED)) {
            DebugBuffer.addLine(DebugLevel.DEBUG, "b");
            //Search if a delete notifictiona exists. If so delete it
            if (user.containsKey(TerritoryNotification.ETerritoryNotificationType.DELETED)) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "c");
                user.remove(TerritoryNotification.ETerritoryNotificationType.DELETED);
            }
        }
        if (tn.type.equals(TerritoryNotification.ETerritoryNotificationType.DELETED)) {
            DebugBuffer.addLine(DebugLevel.DEBUG, "d");
            //Search if a delete notifictiona exists. If so delete it
            if (user.containsKey(TerritoryNotification.ETerritoryNotificationType.ADDED)) {
                DebugBuffer.addLine(DebugLevel.DEBUG, "e");
                user.remove(TerritoryNotification.ETerritoryNotificationType.ADDED);
            }
        }
        user.put(tn.type, tn);
        territory.put(tn.userId, user);
        territoryNotifications.put(tn.tms.getTerritoryId(), territory);
    }

    public static void writeTerritoryNotifications() {
        //  System.out.println("Looping : " + territoryNotifications.size() + " territories");
        for (Map.Entry<Integer, TreeMap<Integer, EnumMap<TerritoryNotification.ETerritoryNotificationType, TerritoryNotification>>> territories : territoryNotifications.entrySet()) {

            // System.out.println("Looping : " + territories.getValue().size() + " users");
            for (Map.Entry<Integer, EnumMap<TerritoryNotification.ETerritoryNotificationType, TerritoryNotification>> users : territories.getValue().entrySet()) {

                //    System.out.println("Looping : " + users.getValue().size() + " types");
                for (Map.Entry<TerritoryNotification.ETerritoryNotificationType, TerritoryNotification> not : users.getValue().entrySet()) {
                    not.getValue().generateMessage();
                }
            }
        }
        territoryNotifications = new HashMap<Integer, TreeMap<Integer, EnumMap<ETerritoryNotificationType, TerritoryNotification>>>();
    }

    public static void addResearchNotification(int userId, ArrayList<Research> finishedResearches) {
        if (researchNots == null) {
            researchNots = new HashMap<Integer, ArrayList<Research>>();
        }
        ArrayList<Research> finRes = researchNots.get(userId);
        if (finRes == null) {
            init();
            researchNots.put(userId, finishedResearches);
        } else {
            finRes.addAll(finishedResearches);
        }
    }

    public static void writeAll() {
        for (UserData ud : (ArrayList<UserData>) userDataDAO.findAll()) {
            writeNotifications(ud.getUserId());
        }
    }
}
