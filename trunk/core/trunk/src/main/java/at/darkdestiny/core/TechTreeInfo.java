/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.ITechFunctions;
import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.model.PlayerResearch;
import at.darkdestiny.core.model.ResAlignment;
import at.darkdestiny.core.model.Research;
import at.darkdestiny.core.model.ResearchProgress;
import at.darkdestiny.core.model.TechRelation;
import at.darkdestiny.core.model.TechType;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.xml.XMLMemo;
import at.darkdestiny.framework.ReadOnlyTable;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author HorstRabe
 */
public class TechTreeInfo extends Service {

    private static final Logger log = LoggerFactory.getLogger(TechTreeInfo.class);

    public static XMLMemo addConstants(XMLMemo techtree, int userId, Locale locale) {

        XMLMemo constants = new XMLMemo("Constants");
        XMLMemo language = new XMLMemo("Language");
        String localeString = userDAO.findById(userId).getLocale();
        Language l = Service.languageDAO.findBy(localeString);
        if (!l.getId().equals(l.getMasterLanguageId())) {
            Language lTmp = Service.languageDAO.findById(l.getMasterLanguageId());
            localeString = lTmp.getLanguage() + "_" + lTmp.getCountry();
        }
        language.addAttribute("Locale", localeString);

        constants.addChild(language);
        XMLMemo resTypes = new XMLMemo("ResearchTypes");
        for (TechType rt : techTypeDAO.findAll()) {

            XMLMemo resType = new XMLMemo("ResearchType");
            resType.addAttribute("id", rt.getId());
            resType.addAttribute("name", ML.getMLStr(rt.getName(), userId));
            resTypes.addChild(resType);
        }
        constants.addChild(resTypes);

        techtree.addChild(constants);
        return techtree;
    }

    public static XMLMemo addUnlockedTechnologies(XMLMemo techtree, int userId) {
        XMLMemo unlockedTechnologies = new XMLMemo("UnlockedTechnologies");

        for (TechRelation tr : techRelationDAO.findAll()) {

            XMLMemo unlockedTechnology = new XMLMemo("UnlockedTechnology");
            unlockedTechnology.addAttribute("resId", tr.getReqResearchId());
            unlockedTechnology.addAttribute("sourceId", tr.getSourceId());
            unlockedTechnology.addAttribute("resType", tr.getTreeType());

            String name = "";
            String description = "";
            TechType resType = techTypeDAO.findById(tr.getTreeType());
            if(resType.getRefModel() != null && resType.getRefDAO() != null){
            if (!resType.getRefDAO().equals("") && !resType.getRefModel().equals("")) {
                try {
                    Class daoClazz = Class.forName(resType.getRefDAO());
                    Class modelClazz = Class.forName(resType.getRefModel());
                    Constructor constructor = modelClazz.getDeclaredConstructor();
                    constructor.setAccessible(true);

                    //    ITechFunctions m = (ITechFunctions)constructor.newInstance();


                    ReadOnlyTable dao = (ReadOnlyTable) DAOFactory.get(daoClazz);
                    ITechFunctions tech = null;

                    for (ITechFunctions m : (ArrayList<ITechFunctions>) dao.findAll()) {
                        if (m.getTechId() == tr.getSourceId()) {
                            tech = m;
                            break;
                        }
                    }
                    if (tech != null) {
                        name = ML.getMLStr(tech.getTechName(), userId);
                        if (!tech.getTechDescription().equals("")) {
                            description = ML.getMLStr(tech.getTechDescription(), userId);
                        }
                    }

                } catch (Exception e) {
                    log.debug("Throw new : " + e);
                }
                //     Constructor con = clzz.getConstructor(StatusBarData.class, Locale.class, int.class);
            }
            }
            unlockedTechnology.addAttribute("name", name);
            unlockedTechnology.addAttribute("description", description);

            unlockedTechnologies.addChild(unlockedTechnology);
        }
        techtree.addChild(unlockedTechnologies);
        return techtree;
    }

    public static XMLMemo addResearches(XMLMemo techtree, int userId) {
        try {
            XMLMemo researches = new XMLMemo("Researches");

            int x = 0;
            int y = 0;

            for (Research r : (ArrayList<Research>) Service.researchDAO.findAll()) {
                //    Statement stmt1 = DbConnect.createStatement();

                //  stmt1.execute("INSERT INTO resalignment VALUES("+ r.getId() + ",0,0)");
                XMLMemo research = new XMLMemo("Research");
                ResAlignment ra = Service.resAlignmentDAO.findById(r.getId());
                if (ra != null) {
                    x = ra.getX();
                    y = ra.getY();
                } else {
                    ResAlignment res = new ResAlignment();
                    res.setId(r.getId());
                    res.setX(0);
                    res.setY(0);
                    Service.resAlignmentDAO.add(res);

                }
                PlayerResearch pr = Service.playerResearchDAO.findBy(userId, r.getId());
                ResearchProgress rp = Service.researchProgressDAO.findBy(userId, r.getId());
                int status = 0;
                if (pr != null) {
                    status = 3;
                } else if (rp != null) {
                    Action a = Service.actionDAO.findResearchBy(userId, r.getId());
                    if (a.getOnHold()) {
                        status = 2;
                    } else {
                        status = 1;
                    }
                }
                research.addAttribute("id", r.getId());
                research.addAttribute("name", ML.getMLStr(r.getName(), userId));
                research.addAttribute("x", x);
                research.addAttribute("y", y);
                research.addAttribute("status", status);
                research.addAttribute("description", ML.getMLStr(r.getShortText(), userId));
                research.addAttribute("rp", r.getRp());
                research.addAttribute("crp", r.getRpComputer());

                researches.addChild(research);
            }


            techtree.addChild(researches);

        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error while Adding TradeRoutes to XML-Info : " + e);
        }
        return techtree;
    }

    public static XMLMemo addTechRelation(XMLMemo techtree, int userId) {
        int size = 0;
        try {
            XMLMemo techRelations = new XMLMemo("TechRelations");

            for (TechRelation tr : (ArrayList<TechRelation>) Service.techRelationDAO.findAll()) {
                if (tr.getTreeType() == TechRelation.TREETYPE_RESEARCH) {
                    XMLMemo techRelation = new XMLMemo("TechRelation");

                    techRelation.addAttribute("reqResearchId", tr.getReqResearchId());
                    techRelation.addAttribute("sourceId", tr.getSourceId());
                    techRelation.addAttribute("type", tr.getTreeType());


                    techRelations.addChild(techRelation);
                    size++;
                }
            }
            techtree.addChild(techRelations);

        } catch (Exception e) {
            DebugBuffer.addLine(DebugLevel.ERROR, "Error while Adding TradeRoutes to XML-Info : " + e);
        }
        return techtree;
    }
}
