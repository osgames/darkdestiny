/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets.portal;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.framework.dao.DAOFactory;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class LoginServlet extends HttpServlet {

    public enum RegistrationValue {

        NAME_USERNAME, NAME_GAMENAME, NAME_REGISTRATIONTOKEN, NAME_PASS1, NAME_PASS2, NAME_LOCALE, NAME_GENDER, NAME_EMAIL, PORTAL_PASSWORD
    }
    private UserDAO uDAO = (UserDAO) DAOFactory.get(UserDAO.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        System.out.println("Trying to process login");
        try {

            System.out.println("Parmap size : " + request.getParameterMap().size());
            for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
                System.out.println(entry.getKey() + " => " + entry.getValue()[0]);
            }
            String userName = request.getParameter(RegistrationValue.NAME_USERNAME.toString());
            String portalPassword = request.getParameter(RegistrationValue.PORTAL_PASSWORD.toString());

            response.sendRedirect(GameConfig.getInstance().getHostURL() + "/enter.jsp?user=" + userName + "&portalPassword=" + portalPassword);
        } catch (Exception e) {
            System.out.println("error : " + e);
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
