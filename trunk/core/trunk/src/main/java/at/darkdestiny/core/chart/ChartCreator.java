/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.chart;

import at.darkdestiny.core.statistics.BarChart;
import at.darkdestiny.core.statistics.Chart;
import at.darkdestiny.core.statistics.EStatisticType;
import at.darkdestiny.core.statistics.IChart;
import at.darkdestiny.core.statistics.PieChart;
import at.darkdestiny.util.DebugBuffer;
import java.awt.Color;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.AbstractDataset;
import org.jfree.data.general.Dataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Stefan
 */
public class ChartCreator {
    protected EStatisticType statisticType;
    protected Dataset chartData;

    protected String title;
    protected String xTitle;
    protected String yTitle;
    protected PlotOrientation orientation;
    protected boolean legend;
    protected boolean tooltips;
    protected boolean urls;
    protected String name;
    protected int width;
    protected int height;

    private IChart chart;

    public ChartCreator(EStatisticType statisticType, Dataset chartData) throws ChartException {
        this.statisticType = statisticType;
        this.chartData = chartData;

        System.out.println(chartData);

        if (statisticType == EStatisticType.PIECHART) {
            setDefaultPie();
            if (!(chartData instanceof DefaultPieDataset)) {
                throw new ChartException("Invalid Dataset Type (should be DefaultPieDataset)");
            }

            chart = new PieChart((DefaultPieDataset)chartData,title);
        } else if (statisticType == EStatisticType.BARCHART) {
            setDefaultBar();
            if (!(chartData instanceof DefaultCategoryDataset)) {
                throw new ChartException("Invalid Dataset Type (should be DefaultCategoryDataset)");
            }
        }
    }

    public JFreeChart getChart() {
        try {
            if (statisticType == EStatisticType.PIECHART) {
                chart = new PieChart((DefaultPieDataset)chartData,title);
                Chart chart2 = chart.getChart();

                org.jfree.chart.JFreeChart jChart = org.jfree.chart.ChartFactory.createPieChart(chart2.getTitle(), (DefaultPieDataset)chart2.getDataset(), chart2.isLegend(), chart2.isTooltips(), chart2.isUrls());

                jChart.setBackgroundPaint(Color.white);
                return jChart;
            } else if (statisticType == EStatisticType.BARCHART) {
                chart = new BarChart((DefaultCategoryDataset)chartData, xTitle, yTitle, title);
                Chart chart2 = chart.getChart();

                final JFreeChart jChart = ChartFactory.createBarChart(chart2.getTitle(), chart2.getxTitle(), chart2.getyTitle(), (DefaultCategoryDataset)chart2.getDataset(), chart2.getOrientation(), chart2.isLegend(), chart2.isTooltips(), chart2.isUrls());
                jChart.setBackgroundPaint(Color.white);
                return jChart;
            }
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Not a single chart was given that day: ", e);
            return null;
        }

        return null;
    }

    private void setDefaultBar() {
        title = "BarChart";
        xTitle = "xTitle";
        yTitle = "yTitle";
        orientation = PlotOrientation.HORIZONTAL;
        legend = true;
        tooltips = false;
        urls = false;
        name = "name";
        width = 500;
        height = 500;
    }

    private void setDefaultPie() {
        title = "PieChart";
        xTitle = null;
        yTitle = null;
        orientation = null;
        legend = true;
        tooltips = false;
        urls = false;
        name = "Name";
        width = 500;
        height = 500;
    }

    /**
     * @return the statisticType
     */
    public EStatisticType getStatisticType() {
        return statisticType;
    }

    /**
     * @param statisticType the statisticType to set
     */
    public void setStatisticType(EStatisticType statisticType) {
        this.statisticType = statisticType;
    }

    /**
     * @return the chartData
     */
    public Dataset getChartData() {
        return chartData;
    }

    /**
     * @param chartData the chartData to set
     */
    public void setChartData(AbstractDataset chartData) {
        this.chartData = chartData;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the xTitle
     */
    public String getxTitle() {
        return xTitle;
    }

    /**
     * @param xTitle the xTitle to set
     */
    public void setxTitle(String xTitle) {
        this.xTitle = xTitle;
    }

    /**
     * @return the yTitle
     */
    public String getyTitle() {
        return yTitle;
    }

    /**
     * @param yTitle the yTitle to set
     */
    public void setyTitle(String yTitle) {
        this.yTitle = yTitle;
    }

    /**
     * @return the orientation
     */
    public PlotOrientation getOrientation() {
        return orientation;
    }

    /**
     * @param orientation the orientation to set
     */
    public void setOrientation(PlotOrientation orientation) {
        this.orientation = orientation;
    }

    /**
     * @return the legend
     */
    public boolean isLegend() {
        return legend;
    }

    /**
     * @param legend the legend to set
     */
    public void setLegend(boolean legend) {
        this.legend = legend;
    }

    /**
     * @return the tooltips
     */
    public boolean isTooltips() {
        return tooltips;
    }

    /**
     * @param tooltips the tooltips to set
     */
    public void setTooltips(boolean tooltips) {
        this.tooltips = tooltips;
    }

    /**
     * @return the urls
     */
    public boolean isUrls() {
        return urls;
    }

    /**
     * @param urls the urls to set
     */
    public void setUrls(boolean urls) {
        this.urls = urls;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }
}
