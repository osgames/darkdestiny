package at.darkdestiny.core.admin.techtree;

import at.darkdestiny.core.admin.db.DBDescription;
import at.darkdestiny.core.admin.db.IDatabaseColumn;
import at.darkdestiny.core.admin.db.ImgField;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.util.DebugBuffer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Eine Forschung im Technologie-Baum
 *
 * @author martin
 */
public class TechnologieTechTreeEntry
        extends TechTree {

	/**
	 * @param ID
	 */
	public TechnologieTechTreeEntry(int ID) {
		super(ID);

		try {
			Statement statement = DbConnect.getConnection().createStatement();
			ResultSet rs = statement
					.executeQuery("SELECT * FROM construction WHERE id=" + ID);
			if (rs.next())
				this.init(rs);
		} catch (SQLException e) {
			DebugBuffer.writeStackTrace("Error in TechnologieTechTreeEntry: ",e);
		}

	}

	public TechnologieTechTreeEntry(ResultSet rs)
	        throws SQLException{
		super(rs.getInt(1));

		this.init(rs);
	}

	public String toString()
	{
		return "Eintrag, Typ Research, ID = "+ID;
	}
	@Override
	public int getID() {
		return ID;
	}

	@Override
	public String getName() {
		if (get("name") != null)
			return ""+get("name");

		return "Research "+getID();
	}

	@Override
	public String getTyp() {
		return "Research";
	}

	public static int makeID(int aid) {
		return aid;
	}

	@Override
	public int getBaseID() {
		return ID;
	}

	@Override
	public List<IDatabaseColumn> getAllDBColumns() {
		return DBDescription.alleFelder_research;
	}

	@Override
	public String getTitle() {
		return "die Forschung";
	}

	@Override
	public String getTable() {
		return "research";
	}

	public String getSmallImgPath()
	{
		for (IDatabaseColumn idc : getAllDBColumns())
			if ((idc instanceof ImgField) && (("smallPic".equalsIgnoreCase(idc.getName()))))
			{
				return ((ImgField)idc).getImgLink(get("smallPic"));
			}
		return "";
	}
}
