/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.SunTransmitterDAO;
import at.darkdestiny.core.dao.SunTransmitterRouteDAO;
import at.darkdestiny.core.dao.ViewTableDAO;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.SunTransmitter;
import at.darkdestiny.core.model.SunTransmitterRoute;
import at.darkdestiny.core.utilities.DiplomacyUtilities;
import at.darkdestiny.core.view.TransmitterTargetEntry;
import at.darkdestiny.core.view.TransmitterTargetsView;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class TransmitterService {
    private static PlayerPlanetDAO ppDAO = DAOFactory.get(PlayerPlanetDAO.class);
    private static SunTransmitterRouteDAO strDAO = DAOFactory.get(SunTransmitterRouteDAO.class);
    private static SunTransmitterDAO stDAO = DAOFactory.get(SunTransmitterDAO.class);
    private static ViewTableDAO vtDAO = DAOFactory.get(ViewTableDAO.class);
    
    public static TransmitterTargetsView getTransmitterTargets(int systemId, int userId) {
        System.out.println("LOAD TRANSMITTER TARGETS FOR SYSTEM " + systemId + " USERID " + userId);
        
        TransmitterTargetsView ttv = new TransmitterTargetsView();
        
        // Check for targets for current position
        SunTransmitter stStart = stDAO.findBySystemId(systemId);
        if (stStart == null) return ttv;
        
        SunTransmitterRoute str = new SunTransmitterRoute();
        
        if (!transmitterAccessible(userId, systemId)) {
            return ttv;
        }
        
        str.setStartId(stStart.getId());
        ArrayList<SunTransmitterRoute> targetList = strDAO.find(str);
        
        str = new SunTransmitterRoute();
        str.setEndId(stStart.getId());
        targetList.addAll(strDAO.find(str));        
        
        System.out.println("ROUTES FOUND " + targetList.size());
        
        for (SunTransmitterRoute str2 : targetList) {
            System.out.println("PROCESSING ROUTE " + str2.getStartId() + ">" + str2.getEndId());
            SunTransmitter st;
            
            // Reverse
            System.out.println("CHECK IF " + str2.getEndId() + " EQUALS " + stStart.getId() + " (CURRLOC="+systemId+")");
            if (str2.getEndId().equals(stStart.getId())) {
                st = stDAO.findById(str2.getStartId());            
            } else { // Reverse Route
                st = stDAO.findById(str2.getEndId());
            }

            System.out.println("FOUND DESTINATION TRANSMITTER AT " + st.getSystemId());
            
            // Check if viewtable contains this system
            boolean alreadyDiscovered = vtDAO.containsSystem(userId, st.getSystemId());       
            
            System.out.println("Create RouteEntry " + st.getSystemId() + " alreadyDiscovered " + alreadyDiscovered);
            TransmitterTargetEntry tte = new TransmitterTargetEntry(st.getSystemId(),alreadyDiscovered);
            ttv.addTransmitterDestination(tte);
        }
        
        return ttv;
    }       
    
    // Its a transmitterflight if starting point is an accessible Transmitter
    // and targetLocation contains a transmitter
    public static boolean flightIsTransmitter(int userId, int startSystemId, int targetSystemId) {
        System.out.println("Check if flight is transmitter for userId " + userId + " from " + startSystemId + " to " + targetSystemId);
        
        boolean transmitterFlight = false;
        boolean transmitterAccessible = transmitterAccessible(userId, startSystemId);
        
        System.out.println("TransmiterFlight = " + transmitterFlight + " -- TransmitterAccessible = " + transmitterAccessible);
        
        SunTransmitter st = stDAO.findBySystemId(targetSystemId);
        if ((st != null) && transmitterAccessible) transmitterFlight = true;
        
        System.out.println("RETURN RESULT: " + transmitterFlight);
        
        return transmitterFlight;
    }
    
    // Transmitter is accessible if owned by player or an ally
    public static boolean transmitterAccessible(int userId, int systemId) {
        SunTransmitter st = stDAO.findBySystemId(systemId);        
        if (st == null) return false;
        
        int startOwner = st.getUserId();     
        
        // Security check -- Check if wrong id and print warning
        PlayerPlanet pp = ppDAO.findByPlanetId(st.getPlanetId());
        
        // Obviously the transmitter planet got deleted .. so no route anymore
        if (pp == null) {
            return false;
        }
        
        if (!pp.getUserId().equals(st.getUserId())) {
            DebugBuffer.warning("Suntransmitter has wrong assigned user id " + startOwner + " (should be " + pp.getUserId() + ")");
            st.setUserId(pp.getUserId());
            stDAO.update(st);
            
            startOwner = pp.getUserId();
        }
        
        if (!(startOwner == userId)) {
            System.out.println("CHECK ALLIANCE RELATION BETWEEN " + userId + " AND " + startOwner);
            return DiplomacyUtilities.hasAllianceRelation(userId, startOwner);
        } else {
            return true;
        }
    }
}
