package at.darkdestiny.core.admin.techtree.display.algorithms;

import at.darkdestiny.core.admin.techtree.display.DisplayConnection;
import at.darkdestiny.core.admin.techtree.display.DisplayTechTreeHelper;
import java.util.List;

/**
 * Hiermit werden die Technologien angeordnet
 * @author Martin Tsch&ouml;pe
 * @version 0.2.2
 */
public interface ItemArranger
{

	/**
	 * Alle Technologien anordnen
	 * @param allItems alle Technologien 
	 * 	(gespeichert als DisplayTechnologien)
	 * @param allLevels eine Liste von Listen,
	 * 		in denen die einzelnen Ebenen gespeichert sind
	 */
	void processData(List<DisplayTechTreeHelper> allItems, List<List<DisplayTechTreeHelper>> allLevels);

	/**
	 * Alle Linien anordnen
	 * @param allItems alle technologien
	 * @param allLines alle Linien (als DisplayConnections)
	 */
	void processLines(List<DisplayTechTreeHelper> allItems, List<DisplayConnection> allLines);

	/**
	 * Die Linienpositionen vorherberechnen
	 * @param allLevels alle Technologien, nach vertikaler Position geordnet
	 * @param allItems alle Technologien
	 * @param allLines alle Linien (als DisplayConnections)
	 */
	void preprocessLines(List<List<DisplayTechTreeHelper>> allLevels, List<DisplayTechTreeHelper> allItems, List<DisplayConnection> allLines);

	/**
	 * @param row welche Zeile
	 * @return wie viel Platz muss mindestens da sein
	 */
	int getRequiredSpace(int row);

}
