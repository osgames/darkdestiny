/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ressources;

import at.darkdestiny.core.model.PlanetConstruction;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class RessProdNode {
    private static final Logger log = LoggerFactory.getLogger(RessProdNode.class);
    
    public final int ressource;    
    public final int type;
    
    private boolean locked = false;
    
    private ArrayList<RessProdNode> previousNodes = new ArrayList<RessProdNode>();
    private ArrayList<RessProdNode> nextNodes = new ArrayList<RessProdNode>();
    
    private RessProdNode previous;
    private RessProdNode next;
    
    private ArrayList<PlanetConstruction> pc;
    private long maxStorage;
    private long minStorage;
    private long actStorage;
    
    private long baseProduction;
    private long baseConsumption;
    private HashMap<RessProdNode,Long> baseConsumptionByNode = Maps.newHashMap();
    private long maxProductionByPrevious;
    
    private long fixedConsumption;
    private long actProduction;
    private HashMap<RessProdNode,Long> actProductionByNextNode = Maps.newHashMap();
    private long actConsumption;
    private HashMap<RessProdNode,Long> actConsumptionByNode = Maps.newHashMap();
    private HashMap<RessProdNode,Long> lockedConsumptionByNode = Maps.newHashMap();    
    
    public final static int NODE_TYPE_INDUSTRY = 1; 
    public final static int NODE_TYPE_AGRICULTURE = 2; 
    public final static int NODE_TYPE_RESEARCH = 4;     
    public final static int NODE_TYPE_ENERGY = 3; 
    public final static int NODE_TYPE_CONSTRUCTION = 5;
    
    public RessProdNode(int r, int type) {
        ressource = r;
        this.type = type;
    } 
    
    public RessProdNode getPrevious() {
        return previous;
    }

    public ArrayList<RessProdNode> getPreviousNodes() {
        return previousNodes;
    }    
    
    public void setPrevious(RessProdNode previous) {
        this.previousNodes.add(previous);
        
        this.previous = previous;
        previous.setNext(this);        
    }

    public ArrayList<RessProdNode> getNextNodes() {
        return nextNodes;
    }
    
    public RessProdNode getNext() {
        return next;
    }

    public void setNext(RessProdNode next) {
        this.nextNodes.add(next);
        
        this.next = next;
    }

    public long getMaxStorage() {
        return maxStorage;
    }

    public void setMaxStorage(long maxStorage) {
        this.maxStorage = maxStorage;
    }

    public long getMinStorage() {
        return minStorage;
    }

    public void setMinStorage(long minStorage) {
        this.minStorage = minStorage;
    }

    public long getActStorage() {
        return actStorage;
    }

    public void setActStorage(long actStorage) {
        this.actStorage = actStorage;
    }

    public long getBaseProduction() {
        return baseProduction;
    }

    public void setBaseProduction(long baseProduction) {
        this.baseProduction = baseProduction;
    }

    public long getBaseConsumption() {
        return baseConsumption;
    }

    public Long getBaseConsumptionByNode(RessProdNode rpn) {
        return baseConsumptionByNode.get(rpn);        
    }    
    
    public void setBaseConsumption(long baseConsumption) {
        this.baseConsumption = baseConsumption;
    }

    public void setBaseConsumptionByNode(RessProdNode rpn, long baseConsumption) {
        this.baseConsumptionByNode.put(rpn,baseConsumption);
    }    
    
    public long getActProduction() {
        return actProduction;
    }
    
    public long setActProductionByNextNodes() {
        long value = 0l;
        
        for (long actValue : actProductionByNextNode.values()) {
            value += actValue;
        }
        
        return value;
    }        

    public void setActProduction(long actProduction) {
        this.actProduction = actProduction;
    }

    public void setActProductionByNextNode(RessProdNode rpn, long actProduction) {
        this.actProductionByNextNode.put(rpn, actProduction);
    }    
    
    public long getActConsumption() {
        return actConsumption;
    }
    
    public Long getActConsumptionByNode(RessProdNode rpn) {
        return actConsumptionByNode.get(rpn);        
    }

    public Long getLockedConsumptionByNode(RessProdNode rpn) {
        return lockedConsumptionByNode.get(rpn);        
    }    
    
    public Long getLockedConsumption() {
        long lockedConsumption = 0l;
        
        for (Long lCons : lockedConsumptionByNode.values()) {
            lockedConsumption += lCons;
        }
        
        return lockedConsumption;
    }
    
    public void setActConsumptionByNode(RessProdNode rpn, long actConsumption) {
        this.actConsumptionByNode.put(rpn,actConsumption);
    }        

    public void setLockedConsumptionByNode(RessProdNode rpn, long lockedConsumption) {
        this.lockedConsumptionByNode.put(rpn,lockedConsumption);
    }            
    
    public void setActConsumption(long actConsumption) {  
        this.actConsumption = actConsumption;
    }

    public long getMaxProductionByPrevious() {
        return maxProductionByPrevious;
    }

    public void setMaxProductionByPrevious(long maxProductionByPrevious) {
        this.maxProductionByPrevious = maxProductionByPrevious;
    }
    
    public void setLocked() {
        this.locked = true;
    }
    
    public boolean getLocked() {
        return locked;
    }

    /**
     * @return the fixedConsumption
     */
    public long getFixedConsumption() {
        return fixedConsumption;
    }

    /**
     * @param fixedConsumption the fixedConsumption to set
     */
    public void setFixedConsumption(long fixedConsumption) {
        this.fixedConsumption = fixedConsumption;
    }
}