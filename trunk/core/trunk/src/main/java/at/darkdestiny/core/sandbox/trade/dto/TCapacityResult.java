/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.sandbox.trade.dto;

/**
 *
 * @author Feuerelfe
 */
public class TCapacityResult {

    boolean closeRoute;
    long capacity;

    public TCapacityResult(long capacity, boolean closeRoute) {
        this.capacity = capacity;
        this.closeRoute = closeRoute;
    }

    public boolean isCloseRoute() {
        return closeRoute;
    }

    public long getCapacity() {
        return capacity;
    }

}
