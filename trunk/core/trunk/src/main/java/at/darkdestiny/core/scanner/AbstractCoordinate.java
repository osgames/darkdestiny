/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.scanner;

import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;

/**
 *
 * @author Stefan
 */
public abstract class AbstractCoordinate {

    public final int x;
    public final int y;

    public AbstractCoordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distanceTo(AbstractCoordinate ac) {
        double a = this.x - ac.x;
        double b = this.y - ac.y;

        return Math.sqrt(a * a + b * b);
    }
}
