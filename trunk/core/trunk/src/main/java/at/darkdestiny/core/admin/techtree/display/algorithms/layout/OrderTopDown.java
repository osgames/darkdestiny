package at.darkdestiny.core.admin.techtree.display.algorithms.layout;

import at.darkdestiny.core.admin.techtree.display.DisplayConnection;
import at.darkdestiny.core.admin.techtree.display.DisplayTechTreeHelper;
import at.darkdestiny.core.admin.techtree.display.algorithms.ItemArranger;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Stellt ein System um alle Technologien anzuordnen bereit
 * @author Martin Tsch&ouml;pe
 * @version 0.2.2
 */
public class OrderTopDown implements ItemArranger
{

	/**
	 *
	 */
	public OrderTopDown()
	{
		super();
	}

	/**
	 * Die Positionen der Technologien
	 */
	int [] xPositionen = null;
	/**
	 * Eine Liste aller Technologien
	 */
	DisplayTechTreeHelper [] allItems = null;
	/**
	 * Ein Zufallsgenerator
	 */
	protected Random rand = new Random();

	/**
	 * Wieviele Linien gibt es in jeder Zeile
	 */
	private int [] noLinesPerRow = null;

	public void processData (List<DisplayTechTreeHelper> allItems, List<List<DisplayTechTreeHelper>> allLevels)
	{
		for (int i = 0; i < this.allItems.length; i++)
		{
			this.allItems[i].moveTo(i, this.allItems[i].getY());
		}
		int maxCount = processData(allItems, allLevels, 100)-1;
		DebugBuffer.addLine(DebugLevel.DEBUG,"Anzahl Runden: "+maxCount);

		//Jetzt noch alles anpassen:
		int minX = Integer.MAX_VALUE;
		for (int i = 0; i< xPositionen.length; i ++)
		{
			if (xPositionen[i] < minX)
				minX = xPositionen[i];
		}
		for (int i = 0; i< allItems.size(); i ++)
		{
			DisplayTechTreeHelper di = allItems.get(i);
			di.moveTo((xPositionen[i]-minX)*(di.getWidth()+di.getSpace()), di.getY());
		}

		//Totale LinienLänge Berechnen:
		DebugBuffer.addLine(DebugLevel.DEBUG,"Linien sind total: "+CalculateTotalLineLength()+" bei "+allItems.size()+" Technologien");
	}


	/**
	 * @return Wieviel Kilometer Linien gibt's hier eigentlich
	 */
	private float CalculateTotalLineLength()
	{
		float res = 0;
		for (int i = 0; i< allItems.length; i++)
			for (int j = 0; j< allItems.length; j++)
			{
				DisplayTechTreeHelper dt = allItems[i];
				DisplayTechTreeHelper tech = allItems[j];
				if (tech.getDependencies().contains(dt.getTechno()))
				{
					res += Math.abs(dt.getX() - tech.getX());
				}
			}
		return res/(allItems[0].getWidth() + allItems[0].getSpace());
	}


	/**
	 * Alle Technologien anordnen
	 * @param allItems alle Technologien
	 * 	(gespeichert als DisplayTechnologien)
	 * @param allLevels eine Liste von Listen,
	 * 		in denen die einzelnen Ebenen gespeichert sind
	 * @param maxCount wie oft maximal anordnen
	 * @return die Anzahl der Runden, die die Technologie angeordnet wurden
	 */
	public int processData (List<DisplayTechTreeHelper> allItems, List<List<DisplayTechTreeHelper>> allLevels, int maxCount)
	{
		boolean [] done = new boolean[allItems.size()];
		for (int i = 0; i< done.length; i++)
			done[i] = false;

		int maxItemsPerLevel = 0;

		//Runter ordnen, das erste Mal
		float totalEnergie = 0;
		for (List<DisplayTechTreeHelper>aktLevel :  allLevels)
		{
			totalEnergie += doLevel(aktLevel, done, maxItemsPerLevel,true);
			maxItemsPerLevel = Math.max(aktLevel.size(),maxItemsPerLevel);
			searchDone(done, aktLevel);
		}
		DebugBuffer.addLine(DebugLevel.DEBUG,"Start-Energie = "+totalEnergie);
		float lastEnergie = totalEnergie * 20; //Eigentlich nicht zu vergleichen
		totalEnergie *= 19;

		int count = 1;
		int equalcount = 0;

		int [] alteWerte  = new int[xPositionen.length];
		for (int i = 0; i< done.length; i++)
			done[i] = true;

		while ((count < maxCount) && (equalcount < 5))
		{
			if (lastEnergie <= totalEnergie)
				equalcount ++;
			else {
				lastEnergie = totalEnergie;
				for (int i = 0; i < alteWerte.length; i++)
					alteWerte[i] = xPositionen[i];
			}
			totalEnergie = 0;

			// durchgehen
			for (int i = ((count %2) == 1)?allLevels.size() -1:0 ; (i >= 0) && (i < allLevels.size()); i+= 1 - (count %2)*2)
			{
				totalEnergie += doLevel(allLevels.get(i), done, maxItemsPerLevel);
			}
			DebugBuffer.addLine(DebugLevel.DEBUG,"\tEnergie = "+totalEnergie);
			count ++;
		}

		for (int i = 0; i < alteWerte.length; i++)
			xPositionen[i] = alteWerte[i];
		DebugBuffer.addLine(DebugLevel.DEBUG,"Verwendete Energie = "+lastEnergie);
		return count;
	}

	/**
	 * Alle Werte als gesetzt markieren, die gerade abgearbeitet wurden
	 * @param done worein schreiben
	 * @param aktLevel welche wurden abgeabarbeitet
	 */
	private void searchDone(boolean[] done, List<DisplayTechTreeHelper> aktLevel)
	{
		for (DisplayTechTreeHelper dt : aktLevel)
			done[dt.getX()] = true;
	}


	/**
	 * Eine Zeile ordnen
	 * @param items die Technologien in dieser Zeile
	 * @param done alle schon bearbeiteten
	 * @param maxItemsPerLevel wieviele Items gab es schon?
	 * @return kosten f&uuml;r diese Konfiguration
	 */
	protected float doLevel(List<DisplayTechTreeHelper> items, boolean[] done, int maxItemsPerLevel)
	{
		float Energie = doLevel(items, done, maxItemsPerLevel, true);
		int [] werteUp = xPositionen;
		xPositionen = new int[werteUp.length];
		for (int i = 0; i< werteUp.length; i++)
			xPositionen[i] = werteUp[i];
		float Energie2 = doLevel(items,done,maxItemsPerLevel,false);
		if (Energie2 > Energie)
		{
			xPositionen = werteUp;
			return Energie;
		}
		return Energie2;
	}
	/**
	 * Eine Zeile ordnen
	 * @param items die Technologien in dieser Zeile
	 * @param done alle schon bearbeiteten
	 * @param maxItemsPerLevel wieviele Items gab es schon?
	 * @param FirstToLast Beim sortieren mit dem ersten Elements in items anfangen
	 * @return kosten für diese Konfiguration
	 */
	protected float doLevel(List<DisplayTechTreeHelper> items, boolean[] done, int maxItemsPerLevel, boolean FirstToLast)
	{
		int [] used = new int [Math.max(items.size(), maxItemsPerLevel+2)];
		for (int j = 0; j < used.length; j ++)
			used[j] = -1;

		float totalEnergie = 0;
		for (int counter = FirstToLast?0:items.size()-1; (counter < items.size()) && (counter >= 0); counter += FirstToLast?1:-1)
		{
			DisplayTechTreeHelper dt = items.get(counter);
			int bestPos = 0;
			float minEnergie = Float.MAX_VALUE;
			for (int i = 0; i < used.length; i++)
				if (used[i] == -1)
				{
					float energie = getEnergie(i-1,dt,done);
					if (energie < minEnergie)
					{
						minEnergie = energie;
						bestPos = i;
					}
				}
//			DebugBuffer.addLine("Best: "+bestPos+", "+minEnergie);
			used[bestPos] = counter;
			totalEnergie += minEnergie;
		}


		for (int i = 0; i< used.length; i++)
			if (used[i] != -1)
			{
				DisplayTechTreeHelper dt = items.get(used[i]);
				xPositionen[dt.getX()] = i-1;
			}
		return totalEnergie;
	}

	/**
	 * Die Energie, die f&uuml;r die aktuelle Konfiguration ben&ouml;tigt wird, wird
	 * berechnet
	 * @param x die neue Position der Technologie
	 * @param tech die Technologie
	 * @param done alle schon bearbeiteten Technologien
	 * @return die Kraft, die auf die Technologie wirkt
	 */
	private float getEnergie(int x, DisplayTechTreeHelper tech, boolean[] done)
	{
		float res = 0;
		for (int i = 0; i< done.length; i++)
			if (done[i])
			{
				DisplayTechTreeHelper dt = allItems[i];
				if (tech.getDependsOn().contains(dt.getTechno())
					|| dt.getDependsOn().contains(tech.getTechno()))
				{
					res += Math.abs(Math.pow(x - xPositionen[dt.getX()],2));
					//Quadratisch liefert die k&uuml;rzesten Verbindungen
				}
			}
		return res;
	}


	public void processLines(List<DisplayTechTreeHelper> allItems, List<DisplayConnection> allLines)
	{
		for (int i = 0; i < noLinesPerRow.length; i++)
			noLinesPerRow[i] = 0;
		int [] over = new int [xPositionen.length];
		int [] under = new int [xPositionen.length];
		for (int i = 0; i< this.allItems.length; i++)
		{
			over[i] = -1; under[i] = -1;
		}
		for (DisplayConnection dc : allLines)
			doLine(dc, allItems,over, under);
	}


	/**
	 * Eine Linie einpassen
	 * @param dc die Linie
	 * @param allTechnos alle Technologien
	 * @param over Positionen der Linien über einer Technologie
	 * @param under Positionen der Linien unter einer Technologie
	 */
	protected void doLine(DisplayConnection dc, List<DisplayTechTreeHelper> allTechnos, int [] over, int [] under)
	{
		//Rechtwinklige Linien zeichnen
		int sx = dc.getStart().getX()+dc.getStart().getWidth()/2;
		int sy = dc.getStart().getY() + dc.getStart().getHeight();
		int ex = dc.getTarget().getX()+dc.getTarget().getWidth()/2;
		int ey = dc.getTarget().getY();

		//Wie-rum Technologien umfahren:
		boolean nachRechts = (sx < ex) || ((sx == ex) && rand.nextBoolean());

		List<DisplayTechTreeHelper> ausweichen = new ArrayList<DisplayTechTreeHelper>();

		//Nun Kollisionen mit Technologien suchen
		//K&ouml;nnen nur in der ersten Vertikalen auftreten
		for (int i = 0; i < allItems.length; i++)
		{
			if ((allItems[i].getX() <= sx) && (allItems[i].getX() + allItems[i].getWidth() >= sx)
					&& (allItems[i].getY() > sy) && (allItems[i].getY() < ey))
			{
				ausweichen.add(allItems[i]);
			}
		}

		if ((sx == ex) && ausweichen.isEmpty())
			return;

		//Aus der StartTechnologie eine Zahl erzeugen
		int StartIndex = -1;
		int EndIndex = -1;
		for (int i = 0; i< this.allItems.length; i++)
		{
			if (allItems[i] == dc.getStart())
				StartIndex = i;
			if (allItems[i] == dc.getTarget())
				EndIndex = i;
		}
		if (StartIndex == -1) return;
		if (EndIndex == -1) return;
		if (under[StartIndex] == -1)
		{
			under[StartIndex] = noLinesPerRow[dc.getStart().getRow()+1]*2 ;
			noLinesPerRow[dc.getStart().getRow()+1] ++;
		}
		if (over[EndIndex] == -1)
		{
			over[EndIndex] = noLinesPerRow[dc.getTarget().getRow()]*2 ;
			noLinesPerRow[dc.getTarget().getRow()] ++;
		}
		int StartIndent = under[StartIndex];
		int EndIndent = over[EndIndex];

		//Ausweichenliste sortieren:
		//Nur nach Y-Richtung
		Collections.sort(ausweichen, new Comparator<DisplayTechTreeHelper>() {

			public int compare(DisplayTechTreeHelper t1, DisplayTechTreeHelper t2)
			{
				return new Integer(t1.getY()).compareTo(new Integer(t2.getY()));
			}

		});
		for (int i = 0; i < ausweichen.size(); i++)
		{
			DisplayTechTreeHelper t1 = ausweichen.get(i);
			int tmp;
			if (nachRechts)
				tmp = t1.getX()+ t1.getWidth()+ 5+StartIndent;
			else
				tmp = t1.getX() -5-StartIndent;
			if (tmp != sx)
			{
				dc.addPointAbs(sx, t1.getY()-10-StartIndent-5, allTechnos);
				if (nachRechts)
				{
					dc.addPointAbs(sx+5, t1.getY()-10-StartIndent, allTechnos);
					dc.addPointAbs(tmp-5, t1.getY()-10-StartIndent, allTechnos);
				}
				else {
					dc.addPointAbs(sx-5, t1.getY()-10-StartIndent, allTechnos);
					dc.addPointAbs(tmp+5, t1.getY()-10-StartIndent, allTechnos);
				}
				dc.addPointAbs(tmp, t1.getY()-10-StartIndent+5, allTechnos);
				sx = tmp;
			}
		}

//		DebugBuffer.addLine("Folgenden Technologien ausweichen: "+ausweichen);
//		DebugBuffer.addLine("Erzeugte Punkte: "+dc.getPoints());

		if (sx != ex)
		{
			dc.addPointAbs(sx, ey-15-EndIndent, allTechnos);
			dc.addPointAbs(sx + ((ex > sx)?5:-5), ey-10-EndIndent, allTechnos);
			dc.addPointAbs(ex - ((ex > sx)?5:-5), ey-10-EndIndent, allTechnos);
		}
	}

	public void preprocessLines(List<List<DisplayTechTreeHelper>> allLevels, List<DisplayTechTreeHelper> allItems, List<DisplayConnection> allLines)
	{
		xPositionen = new int[allItems.size()];
		this.allItems = new DisplayTechTreeHelper[allItems.size()];
		for (int i = 0; i < allItems.size(); i++)
		{
			this.allItems[i] = (DisplayTechTreeHelper)allItems.get(i);
			this.allItems[i].moveTo(i, this.allItems[i].getY());
			xPositionen[i] = -1;
		}

		noLinesPerRow = new int [allLevels.size()];
		for (int i = 0; i < noLinesPerRow.length; i++)
			noLinesPerRow[i] = 0;

		for (int i = 0; i < allLines.size(); i++)
		{
			DisplayConnection dc = (DisplayConnection)allLines.get(i);
			noLinesPerRow[dc.getStart().getRow()+1] ++;
			noLinesPerRow[dc.getTarget().getRow()] ++;
		}
	}


	public int getRequiredSpace(int row)
	{
		if (row + 1 >= noLinesPerRow.length)
			return 0;
		DebugBuffer.addLine(DebugLevel.TRACE,"Anzahl Linien in Freiraum "+row+" = "+noLinesPerRow[row+1]);
		return Math.max(noLinesPerRow[row+1]*2,20);
	}
}
