/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.viewbuffer;

import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.service.HeaderService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.view.header.HeaderData;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Horst
 */
public class HeaderBuffer extends Service {

    private static boolean initialized = false;
    private static HashMap<Integer, HeaderData> userHeaderData;
    public final static int SWITCH_TYPE_CATEGORY = 1;
    public final static int SWITCH_TYPE_SYSTEM = 2;
    public final static int SWITCH_TYPE_PLANET = 3;
    public final static int SWITCH_VALUE_FORWARD = 1;
    public final static int SWITCH_VALUE_BACKWARD = 2;

    public static HashMap<Integer, HeaderData> getUserHeaderData() {
        return userHeaderData;
    }

    public static void setUserHeaderData(HashMap<Integer, HeaderData> aUserHeaderData) {
        userHeaderData = aUserHeaderData;
    }

    public static void initialize() {
        if (initialized) {
            return;
        }
        userHeaderData = new HashMap<Integer, HeaderData>();

        DebugBuffer.addLine(DebugLevel.DEBUG, "Initializing HeaderBuffer");

        for (UserData u : (ArrayList<UserData>) userDataDAO.findAll()) {
            //  DebugBuffer.addLine(DebugLevel.DEBUG, "Adding HeaderData for User: " + u.getUserId());

            HeaderService headerService = new HeaderService();
            headerService.load(u.getUserId());
            getUserHeaderData().put(u.getUserId(), headerService.getHeaderData());

            // DebugBuffer.addLine(DebugLevel.DEBUG, "Loaded Headerdata for User: " + u.getUserId());
        }
        DebugBuffer.addLine(DebugLevel.DEBUG, "Initializing HeaderBuffer - Done");

        initialized = true;
    }

    public static void reloadUser(int userId) {
        HeaderService headerService = new HeaderService();
        headerService.load(userId);
        if (userHeaderData == null) {
            userHeaderData = new HashMap<Integer, HeaderData>();
        }
        try {
            userHeaderData.remove(userId);
        } catch (Exception e) {

            DebugBuffer.addLine(DebugLevel.ERROR, "Error while reloading HeaderBuffer for User: " + userId + " : " + e);
        }
        userHeaderData.put(userId, headerService.getHeaderData());
        DebugBuffer.addLine(DebugLevel.DEBUG, "Reloaded Headerdata for User: " + userId);
    }

    public static HeaderData getUserHeaderData(int userId) {
        if (userHeaderData == null) {
            initialize();
        }
        if (userHeaderData.containsKey(userId)) {
            return userHeaderData.get(userId);
        } else {
            reloadUser(userId);
            return userHeaderData.get(userId);
        }

    }
}
