/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.statistics.designs;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.admin.module.ReferenceType;
import at.darkdestiny.core.model.DesignModule;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.model.ModuleAttribute;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.StatisticEntry;
import at.darkdestiny.core.statistics.BarChart;
import at.darkdestiny.core.statistics.Chart;
import at.darkdestiny.util.DebugBuffer;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Aion
 */
public class ShipDesignChart extends BarChart {

    private int chassisId;
    private StatisticEntry statisticEntry;

    public ShipDesignChart(int chassisId) {
        super(null, null, null, null);
        this.chassisId = chassisId;
    }

    public ShipDesignChart(StatisticEntry se, int chassisId) {
        super(null, null, null, null);
        this.chassisId = chassisId;
        this.statisticEntry = se;
    }

    public Chart getChart(int userId) {

        name = ML.getMLStr(statisticEntry.getName(), userId);
        title = ML.getMLStr(statisticEntry.getName(), userId);
        yTitle = ML.getMLStr(statisticEntry.getyTitle(), userId);
        xTitle = ML.getMLStr(statisticEntry.getxTitle(), userId);
        dataset = new DefaultCategoryDataset();
        TreeMap<Long, ArrayList<String>> data = new TreeMap<Long, ArrayList<String>>();

        for (ShipDesign sd : (ArrayList<ShipDesign>) shipDesignDAO.findByChassisId(chassisId)) {
            long credits = 0;
            for (DesignModule dm : (ArrayList<DesignModule>) designModuleDAO.findByDesignId(sd.getId())) {
                int count = dm.getCount();
                Module m = moduleDAO.findById(dm.getModuleId());
                ArrayList<ModuleAttribute> mas = moduleAttributeDAO.findBy(m.getId(), sd.getChassis());
                for (ModuleAttribute ma : mas) {
                    if (ma.getRefType() == ReferenceType.RESSOURCE) {
                        if (ma.getRefId() == Ressource.CREDITS) {
                            credits += ma.getValue() * count;
                            break;
                        }
                    }
                }
            }

            ArrayList<String> designs = data.get(credits);
            if (designs == null) {
                designs = new ArrayList<String>();
            }
            designs.add(sd.getName());
            data.put(credits, designs);
        }

        int players = 0;
        //Setzen der Werte f�r die Chart ins Dataset
        DebugBuffer.trace("Debugging Statistic Generation ShipDesign - Processing chassis " + chassisId);

        for (Map.Entry<Long, ArrayList<String>> entry : data.descendingMap().entrySet()) {
            DebugBuffer.trace("Looping decending: Processing cost: " + entry.getKey());

            if (players == 10) {
                break;
            }
            for (String s : entry.getValue()) {
                if (players == 10) {
                    break;
                }

                DebugBuffer.trace("Writing value: " + entry.getKey() + " for shipdesign " + s);
                dataset.setValue(entry.getKey(), ML.getMLStr("chart_lbl_shipdesign", userId), s);
                players++;
            }
        }

        Chart se = new Chart(dataset, title, xTitle, yTitle, orientation, legend, tooltips, urls, name, width, height);

        return se;
    }
}
