/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.core.enumeration.EPassword;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "password")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class Password extends Model<Password> {

    @FieldMappingAnnotation("password")
    @IdFieldAnnotation
    private EPassword password;
    @FieldMappingAnnotation("value")
    private String value;

    /**
     * @return the password
     */
    public EPassword getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(EPassword password) {
        this.password = password;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
