/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.AIMessageParameter;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;

/**
 *
 * @author Aion
 */
public class AIMessageParameterDAO extends ReadWriteTable<AIMessageParameter> implements GenericDAO {
    public AIMessageParameter findById(int id){
        AIMessageParameter aimp = new AIMessageParameter();
        aimp.setId(id);
        return (AIMessageParameter)get(aimp);
    }
}
