/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.requestbuffer;

import static at.darkdestiny.core.requestbuffer.ShipScrapBuffer.COUNT;
import static at.darkdestiny.core.requestbuffer.ShipScrapBuffer.DESIGN_ID;

/**
 *
 * @author Stefan
 */
public class StartLocationBuffer extends ParameterBuffer {
    public static final String GALAXY_ID = "galaxyId";
    public static final String SYSTEM_ID = "systemId";
    
    public StartLocationBuffer(int userId) {
        super(userId);
    }         
    
    public int getGalaxyId() {
        return (Integer)getParameter(GALAXY_ID);
    }

    public int getSystemId() {
        return (Integer)getParameter(SYSTEM_ID);
    }    
}
