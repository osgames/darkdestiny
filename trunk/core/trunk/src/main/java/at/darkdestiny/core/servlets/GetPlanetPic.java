/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.service.SystemService;
import at.darkdestiny.core.utilities.img.BackgroundPainter;
import at.darkdestiny.core.utilities.img.BaseRenderer;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.core.utilities.img.PlanetRenderer;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class GetPlanetPic extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("image/png");
        ServletOutputStream out = response.getOutputStream();

        try {
            List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();

            // Check for DATA_FOR_SCAN_PIC
            int planetId = Integer.parseInt((String) request.getSession().getAttribute("actPlanet"));
            int mode = 1;
            int picSize = 200;

            if (request.getParameter("planetId") != null) {
                planetId = Integer.parseInt(request.getParameter("planetId"));
            }

            if (request.getParameter("mode") != null) {
                mode = Integer.parseInt(request.getParameter("mode"));
                if (mode == PlanetRenderer.LARGE_PIC) {
                    picSize = 200;
                } else if (mode == PlanetRenderer.SMALL_PIC) {
                    picSize = 40;

                }
            }

            /*
            System.out.println("DEBUG 1 " + this.getServletContext().getContextPath());
            System.out.println("DEBUG 2 " + this.getServletContext().getRealPath(this.getServletContext().getContextPath()));
            System.out.println("DEBUG 3 " + GameConfig.getInstance().picPath() + "/pic");
            System.out.println("DEBUG 4 " + this.getServletContext().getRealPath(GameConfig.getInstance().picPath() + "/pic"));
            */
            // allItems.add(new PlanetRenderer(planetId, this.getServletContext().getRealPath(GameConfig.getInstance().picPath() + "pic/") + "/", mode));
            /*
            URLClassLoader ownCL = new URLClassLoader(new URL[]{new URL("file:///C:\\DarkDestiny2\\SVN\\trunk\\core\\target\\core-1.0-SNAPSHOT.jar"),
            new URL("file:///C:\\DarkDestiny2\\SVN\\trunk\\framework\\target\\framework-1.0-SNAPSHOT.jar"),
            new URL("file:///C:\\DarkDestiny2\\SVN\\trunk\\web\\target\\web-1.0-SNAPSHOT\\WEB-INF\\lib\\slf4j-api-1.6.4.jar")});
            
            System.out.println("TEST1");
            Class<?> pRenderer = ownCL.loadClass("at.darkdestiny.core.utilities.img.PlanetRenderer");
            System.out.println("TEST2");
            Constructor c = pRenderer.getConstructor(int.class,String.class,int.class);
            System.out.println("TEST3");
            allItems.add((IModifyImageFunction)c.newInstance(planetId, this.getServletContext().getRealPath(GameConfig.getInstance().picPath() + "/pic") + "/", mode));
            System.out.println("TEST4");
            */
            allItems.add(new PlanetRenderer(planetId, this.getServletContext().getRealPath(GameConfig.getInstance().picPath() + "/pic") + "/", mode));
            
            // System.out.println("CL: " + allItems.get(0).getClass().getClassLoader());
            
            Planet p = SystemService.findPlanetById(planetId);
// Wenn A oder B gr��eres Bild zeichnen
            if (mode == PlanetRenderer.SMALL_PIC) {
                if (p.getLandType().equals(Planet.LANDTYPE_A)) {
                    picSize += 20;
                } else if (p.getLandType().equals(Planet.LANDTYPE_B)) {
                    picSize += 10;
                }
            }
            BaseRenderer renderer = new BaseRenderer(picSize, picSize);
// Schreibt den Mime-Type in den Log, Nur Benutzen, wenn
// genau das gew&uuml;nscht wird
// renderer.getImageMimeType();

            renderer.modifyImage(new BackgroundPainter(0x000000));

            for (IModifyImageFunction mif : allItems) {
                renderer.modifyImage(mif);
            }

            renderer.sendToUser(out);
            /*
        } catch (InvocationTargetException ite) {
            ite.printStackTrace();
            System.out.println(ite.getMessage());            
        } catch (NoSuchMethodException nsme) {    
            nsme.printStackTrace();
            System.out.println(nsme.getMessage());
        } catch (InstantiationException ie) {            
            ie.printStackTrace();
            System.out.println(ie.getMessage());            
        } catch (IllegalAccessException iae) {            
            iae.printStackTrace();
            System.out.println(iae.getMessage());               
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
            System.out.println(cnfe.getMessage());
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
