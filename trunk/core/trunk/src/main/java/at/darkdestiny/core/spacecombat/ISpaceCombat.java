/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.diplomacy.combat.CombatGroupResult;
import at.darkdestiny.core.interfaces.ISimulationSet;
import at.darkdestiny.core.spacecombat.helper.TargetList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Stefan
 */
public interface ISpaceCombat {
    public void startFightCalc();
    public void setCombatGroups(List<CombatGroup> combatGroups);
    public void setCombatGroupResult(CombatGroupResult cgr);
    public SpaceCombatDataLogger getLoggingData();

    public TargetList getMyTargetList(BattleShipNew bs);
    public void updateTargetLists();
    public HashMap<Integer, Integer> getGroupCount();
    public boolean skip(ICombatUnit icu);

    public void setISimulationSet(ISimulationSet iss);
    public ISimulationSet getISimulationSet();
}
