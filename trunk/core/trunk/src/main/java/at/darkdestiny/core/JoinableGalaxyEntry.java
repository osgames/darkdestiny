/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core;

/**
 *
 * @author Stefan
 */
public class JoinableGalaxyEntry {
    private final int galaxyId;
    private final int playerCount;
    private final int xSize;
    private final int ySize;
    
    public JoinableGalaxyEntry(int galaxyId, int playerCount, int xSize, int ySize) {
        this.galaxyId = galaxyId;
        this.playerCount = playerCount;
        this.xSize = xSize;
        this.ySize = ySize;
    }

    /**
     * @return the galaxyId
     */
    public int getGalaxyId() {
        return galaxyId;
    }

    /**
     * @return the playerCount
     */
    public int getPlayerCount() {
        return playerCount;
    }

    /**
     * @return the xSize
     */
    public int getxSize() {
        return xSize;
    }

    /**
     * @return the ySize
     */
    public int getySize() {
        return ySize;
    }
}
