/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Admin
 */
@TableNameAnnotation(value = "quizanswer")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class QuizAnswer extends Model<QuizAnswer> {

    @IdFieldAnnotation
    @FieldMappingAnnotation(value = "quizEntryId")
    private Integer quizEntryId;
    @IdFieldAnnotation
    @FieldMappingAnnotation(value = "answer")
    private String answer;

    /**
     * @return the quizEntryId
     */
    public Integer getQuizEntryId() {
        return quizEntryId;
    }

    /**
     * @param quizEntryId the quizEntryId to set
     */
    public void setQuizEntryId(Integer quizEntryId) {
        this.quizEntryId = quizEntryId;
    }

    /**
     * @return the answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * @param answer the answer to set
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
