/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Aion
 */
public enum EFilterValue {
    POPULATION_VALUE,
    MIGRATION_VALUE,
    MORAL_VALUE,
    VISIBLE_FLAG,
    ENERGY_PERCENTAGE_USAGE,
    ENERGY_PERCENTAGE_FREE,
    HAS_IN_CONSTRUCTION,
    HABITABLE,
    HOSTILE_COMPACT,
    HOSTILE_GASEOUS,
}
