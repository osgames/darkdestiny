/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.enumeration;

/**
 *
 * @author Stefan
 */
public enum EIntelType {
    RESOURCE, STORAGE, GROUNDTROOP, SHIP, POPULATION, DEFENSE, CONSTRUCTION;
}
