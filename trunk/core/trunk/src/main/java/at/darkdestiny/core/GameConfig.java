package at.darkdestiny.core;

import at.darkdestiny.core.dao.GameDataDAO;
import at.darkdestiny.core.model.GameData;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.AbstractGameConfig;
import at.darkdestiny.util.DebugBuffer;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public class GameConfig extends AbstractGameConfig {

    @Autowired
    private ApplicationContext appContext;

    private static GameConfig instance = null;

    private static final Logger log = LoggerFactory.getLogger(GameConfig.class);

    public static GameConfig getInstance() {
        if (instance == null) {
            instance = new GameConfig();
        }
        return instance;
    }

    private GameConfig() {
        loadValues();
    }

    private void loadValues() {
        try {
            String additionalParameters = "?autoReconnect=true&dontTrackOpenResources=true";

            Resource dbResource = new ClassPathResource("/db.properties");
            Properties dbProperties = PropertiesLoaderUtils.loadProperties(dbResource);

            Resource gameResource = new ClassPathResource("/game.properties");
            Properties gameProperties = PropertiesLoaderUtils.loadProperties(gameResource);

            driver = loadValue(dbProperties, SetupProperties.DATABASE_DRIVERCLASS);
            database = loadValue(dbProperties, SetupProperties.DATABASE_URL) + additionalParameters;
            username = loadValue(dbProperties, SetupProperties.DATABASE_USER);
            password = loadValue(dbProperties, SetupProperties.DATABASE_PASS);

            gamehostURL = loadValue(gameProperties, SetupProperties.GAME_GAMEHOSTURL);
            webserviceURL = loadValue(gameProperties, SetupProperties.GAME_WEBHOSTURL);
            startURL = loadValue(gameProperties, SetupProperties.GAME_STARTURL);

            srcPathWeb = loadValue(gameProperties, SetupProperties.GAME_SRCPATH_WEB);
            srcPathClasses = loadValue(gameProperties, SetupProperties.GAME_SRCPATH_CLASSES);
            srcPathResources = loadValue(gameProperties, SetupProperties.GAME_SRCPATH_RESOURCES);
            assignDeletedEmpireToAI = loadBooleanValue(gameProperties, SetupProperties.GAME_OPTIONS_ASSIGN_DELETED_EMPIRE_TO_AI);
            updateTickOnStartup = loadBooleanValue(gameProperties, SetupProperties.GAME_UPDATETICKONSTARTUP);
            deleteInactive = loadBooleanValue(gameProperties, SetupProperties.GAME_OPTIONS_DELETE_INACTIVE);

            String logLevel = loadValue(gameProperties, SetupProperties.GAME_LOGGING);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void reloadValues() {
        loadValues();
    }

    @Override
    public String getDriverClassName() {
        return driver;
    }

    @Override
    public java.lang.String getDatabase() {
        return database;
    }

    @Override
    public java.lang.String getUsername() {
        return username;
    }

    @Override
    public java.lang.String getPassword() {
        return password;
    }

    @Override
    public java.lang.String getHostURL() {
        return GameConfig.getInstance().gamehostURL;
    }

    private void initDBValues() {
        GameData gd = null;
        try {
            GameDataDAO gdDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
            gd = (GameData) gdDAO.findAll().get(0);
            tickTime = gd.getTickTime();
            startTime = gd.getStartTime();
        } catch (Exception e) {
            DebugBuffer.error("Base configuration values not found in database!");
            throw new RuntimeException("Configuration not found in database!");
        }
    }

    /**
     * @return the duration of a tick in milliseconds!
     */
    @Override
    public int getTicktime() {
        if (getInstance().tickTime < 0) {
            getInstance().initDBValues();
        }
        return getInstance().tickTime;
    }

    @Override
    public long getStarttime() {
        if (getInstance().tickTime < 0) {
            getInstance().initDBValues();
        }
        return getInstance().startTime;
    }

    @Override
    public String picPath() {
        return picPath;
    }

    @Override
    public void rereadValues() {
        getInstance().tickTime = -1;
        getInstance().initDBValues();
    }

    /**
     * @return the startURL
     */
    @Override
    public String getStartURL() {
        return startURL;
    }

    /**
     * @return the srcPath
     */
    @Override
    public String getSrcPathWeb() {
        return srcPathWeb;
    }

    /**
     * @return the srcPath
     */
    @Override
    public String getSrcPathClasses() {
        return srcPathClasses;
    }

    /**
     * @return the srcPath
     */
    @Override
    public String getSrcPathResources() {
        return srcPathResources;
    }

    /**
     * @return the webserviceURL
     */
    @Override
    public String getWebserviceURL() {
        return webserviceURL;
    }

    @Override
    public boolean isAssignDeletedEmpireToAI() {
        return assignDeletedEmpireToAI;
    }

    @Override
    public boolean isUpdateTickOnStartup() {
        return updateTickOnStartup;
    }
    
    @Override
    public boolean isDeleteInactive() {
        return deleteInactive;
    }    
}
