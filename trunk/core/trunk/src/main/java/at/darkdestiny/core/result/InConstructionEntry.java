/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
public class InConstructionEntry {
    private final ProductionOrder order;
    private final Action action;
    private final Model unit;

    public InConstructionEntry(ProductionOrder order,Action action,Model unit) {
        this.order = order;
        this.action = action;
        this.unit = unit;
    }

    public ProductionOrder getOrder() {
        return order;
    }

    public Action getAction() {
        return action;
    }

    public Model getUnit() {
        return unit;
    }
}
