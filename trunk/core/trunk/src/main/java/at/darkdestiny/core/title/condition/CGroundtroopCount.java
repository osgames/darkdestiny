package at.darkdestiny.core.title.condition;

import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.PlayerTroopDAO;
import at.darkdestiny.core.enumeration.ETitleComparator;
import at.darkdestiny.core.model.PlayerTroop;
import at.darkdestiny.framework.dao.DAOFactory;


public class CGroundtroopCount extends AbstractCondition {

	private ParameterEntry groundTroopCountValue;
	private ParameterEntry groundTroopIdValue;
	private ParameterEntry comparator;
private static PlayerPlanetDAO ppDAO = (PlayerPlanetDAO) DAOFactory.get(PlayerPlanetDAO.class);
private static PlayerTroopDAO ptDAO = (PlayerTroopDAO) DAOFactory.get(PlayerTroopDAO.class);

	public CGroundtroopCount(ParameterEntry groundTroopCountValue, ParameterEntry groundTroopIdValue, ParameterEntry comparator){
		this.groundTroopCountValue = groundTroopCountValue;
		this.groundTroopIdValue = groundTroopIdValue;
		this.comparator = comparator;
	}



	public boolean checkCondition(int userId, int conditionToTitleId) {
            if(ppDAO.findHomePlanetByUserId(userId) == null){
                return false;
            }
            int pId = ppDAO.findHomePlanetByUserId(userId).getPlanetId();
            PlayerTroop pt = ptDAO.findBy(userId, pId, Integer.parseInt(groundTroopIdValue.getParamValue().getValue()));
            if(pt == null)return false;
            long groundTroopCount = pt.getNumber();
        return compare(groundTroopCountValue.getParamValue().getValue(), groundTroopCount, groundTroopCountValue.getParam().getType(), ETitleComparator.valueOf(comparator.getParamValue().getValue()));

	}
}
