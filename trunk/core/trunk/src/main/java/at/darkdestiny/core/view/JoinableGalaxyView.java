/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view;

import at.darkdestiny.core.JoinableGalaxyEntry;
import com.google.common.collect.Lists;
import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class JoinableGalaxyView {
    private ArrayList<JoinableGalaxyEntry> jgeList = Lists.newArrayList();
    
    public void addEntry(JoinableGalaxyEntry jge) {
        jgeList.add(jge);
    }
    
    public ArrayList<JoinableGalaxyEntry> getAllEntries() {
        return jgeList;
    }
}
