/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.Attribute;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author HorstRabe
 */
public class AttributeDAO extends ReadWriteTable<Attribute> implements GenericDAO {

    public Attribute findById(Integer id) {
        Attribute a = new Attribute();
        a.setId(id);
        ArrayList<Attribute> result = find(a);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
}
