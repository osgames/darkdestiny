/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

/**
 *
 * @author Stefan
 */
public class ConstructionCheckResult {
    private final boolean buildable;
    private final int maxCount;
    
    public ConstructionCheckResult(boolean buildable, int maxCount) {
        this.buildable = buildable;
        this.maxCount = maxCount;
    }

    /**
     * @return the buildable
     */
    public boolean isBuildable() {
        return buildable;
    }

    /**
     * @return the maxCount
     */
    public int getMaxCount() {
        return maxCount;
    }
}
