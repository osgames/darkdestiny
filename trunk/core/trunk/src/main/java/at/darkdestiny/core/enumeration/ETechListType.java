/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.enumeration;

/**
 *
 * @author Admin
 */

public enum ETechListType{
    RESEARCH_TYPE_UNKNOWN,
    RESEARCH_TYPE_CHASSIS,
    RESEARCH_TYPE_CONSTRUCTION,
    RESEARCH_TYPE_GROUNDTROOPS,
    RESEARCH_TYPE_MODULE_WEAPON,
    RESEARCH_TYPE_MODULE_SHIELD,
    RESEARCH_TYPE_MODULE_ARMOR,
    RESEARCH_TYPE_MODULE_REACTOR,
    RESEARCH_TYPE_MODULE_SPECIAL,
    RESEARCH_TYPE_MODULE_ENGINE,
    RESEARCH_TYPE_MODULE_VARIOUS
}
