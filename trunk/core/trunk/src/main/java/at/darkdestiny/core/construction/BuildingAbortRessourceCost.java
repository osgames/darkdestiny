/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.construction;

import at.darkdestiny.core.buildable.ConstructionExt;
import at.darkdestiny.core.dao.ActionDAO;
import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.ProductionOrderDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.enumeration.EActionType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author HorstRabe
 */
public class BuildingAbortRessourceCost extends ConstructionExt {

    public static PlanetConstructionDAO planetConstructionDAO = (PlanetConstructionDAO) DAOFactory.get(PlanetConstructionDAO.class);
    public static ProductionOrderDAO productionOrderDAO = (ProductionOrderDAO) DAOFactory.get(ProductionOrderDAO.class);
    public static ActionDAO actionDAO = (ActionDAO) DAOFactory.get(ActionDAO.class);
    private MutableRessourcesEntry mre = new MutableRessourcesEntry();
    private int count;

    public BuildingAbortRessourceCost(int constructionId, ProductionOrder po) {
        super(constructionId);
        //Getting the deconstruction Cost
        BuildingDeconstructionRessourceCost bdrc = new BuildingDeconstructionRessourceCost(constructionId);

        Action a = actionDAO.findByTimeFinished(po.getId(), EActionType.BUILDING);
        count = a.getNumber();
        int totalNeed = po.getIndNeed();
        int done = po.getIndProc();
        //If no tick total ressources back
        if (done == 0) {
            for (RessAmountEntry re : super.getRessCost().getRessArray()) {
                mre.setRess(re.getRessId(), (long) ((double) re.getQty()));
            }
            //Do nothing
        } else {
            double factor = (double) ((double) done / (double) totalNeed);
            for (RessAmountEntry re : super.getRessCost().getRessArray()) {
                if (re.getRessId() == Ressource.CREDITS) {
                    long diff = (long) (re.getQty() - (-bdrc.getRessCost().getRess(re.getRessId())));
                    mre.setRess(re.getRessId(), (long) ((double) re.getQty() - diff * factor));
                } else {
                    long diff = (long) (re.getQty() - bdrc.getRessCost().getRess(re.getRessId()));
                    mre.setRess(re.getRessId(), (long) ((double) re.getQty() - diff * factor));
                }
            }

        }

    }

    @Override
    public MutableRessourcesEntry getRessCost() {
        return mre;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }
}
