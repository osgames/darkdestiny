/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.dao;

import at.darkdestiny.core.model.VotePermission;
import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Bullet
 */
public class VotePermissionDAO extends ReadWriteTable<VotePermission> implements GenericDAO {
    public VotePermission getById(int id) {
        VotePermission vp = new VotePermission();
        vp.setId(id);
        return (VotePermission)get(vp);
    }

    public ArrayList<VotePermission> findByVoteId(int voteId) {
        VotePermission vp = new VotePermission();
        vp.setVoteId(voteId);
        return find(vp);
    }
}
