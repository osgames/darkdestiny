/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.service;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.JoinableGalaxyEntry;
import at.darkdestiny.core.dao.FleetDetailDAO;
import at.darkdestiny.core.dao.GalaxyDAO;
import at.darkdestiny.core.dao.GameDataDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.model.FleetDetail;
import at.darkdestiny.core.model.Galaxy;
import at.darkdestiny.core.model.GameData;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.result.StartLocationResult;
import at.darkdestiny.core.view.JoinableGalaxyView;
import at.darkdestiny.framework.dao.DAOFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author LAZYPAD
 */
public class StartService {
    public static GalaxyDAO gDAO = DAOFactory.get(GalaxyDAO.class);
    public static UserDAO uDAO = DAOFactory.get(UserDAO.class);
    public static SystemDAO sDAO = DAOFactory.get(SystemDAO.class);
    public static PlanetDAO pDAO = DAOFactory.get(PlanetDAO.class);
    public static PlayerPlanetDAO ppDAO = DAOFactory.get(PlayerPlanetDAO.class);
    
    //Sets the tick to the actual time so no tickprocessing is done
    public static void updateTickToActualTime() {

        int currTick = GameUtilities.getCurrentTick2();

        GameDataDAO gdDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
        FleetDetailDAO fdDAO = (FleetDetailDAO) DAOFactory.get(FleetDetailDAO.class);

        GameData gd = (GameData) gdDAO.findAll().get(0);
        int lastUpdated = gd.getLastUpdatedTick();

        int timeWarp = currTick - lastUpdated;
        for (FleetDetail fd : (ArrayList<FleetDetail>) fdDAO.findAll()) {
            fd.setStartTime(fd.getStartTime() + timeWarp);
            fdDAO.update(fd);
        }

        gd.setLastUpdatedTick(currTick);
        gdDAO.update(gd);

        DAOFactory.dropAll();
    }
    
    public static StartLocationResult setStartingLocation(int userId, Map params) {
        StartLocationResult result = null;
        
        int galaxyId = 0;
        String locationCode = "";
        int locationType = 0;
        int selectedSystem = 0;
        
        double minDistanceRandom = Double.MAX_VALUE;
        
        try {
            for (Object entry : params.entrySet()) {
                Map.Entry me = (Map.Entry) entry;
                String key = (String) me.getKey();
                if (key.equals("galaxy")) {
                    galaxyId = Integer.parseInt(((String[]) me.getValue())[0]);

                } else if (key.equals("distance")) {
                    locationType = Integer.parseInt(((String[]) me.getValue())[0]);

                } else if (key.equals("locationcode")) {
                    locationCode = ((String[]) me.getValue())[0];
                }
            }    
            
            Galaxy g = gDAO.findById(galaxyId);
            ArrayList<at.darkdestiny.core.model.System> possibleSystems = Lists.newArrayList();
            
            for (at.darkdestiny.core.model.System system : sDAO.findAll()) {
                if ((system.getId() >= g.getStartSystem()) && (system.getId() <= g.getEndSystem())) {
                    possibleSystems.add(system);
                }
            }
            
            // Collect all M planets
            ArrayList<Planet> possiblePlanets = Lists.newArrayList();
            
            for (at.darkdestiny.core.model.System system : possibleSystems) {
                ArrayList<Planet> pList = pDAO.findBySystemId(system.getId());                                
                
                for (Planet p : pList) {
                    if (p.getLandType().equalsIgnoreCase("M")) {
                        possiblePlanets.add(p);                        
                    }
                }
            }            
            
            // Filter out planets in colonized Systems
            ArrayList<PlayerPlanet> ppList = ppDAO.findAll();
            HashSet<Integer> prune = Sets.newHashSet();
            
            for (Iterator<PlayerPlanet> ppIt = ppList.iterator(); ppIt.hasNext(); ) {
                PlayerPlanet ppTmp = ppIt.next();
                User u = uDAO.findById(ppTmp.getUserId());
                if (u.getUserType() != EUserType.HUMAN) {
                    ppIt.remove();
                }
            }
            
            for (PlayerPlanet pp : ppList) {
                Planet p = pDAO.findById(pp.getPlanetId());
                
                prune.add(p.getSystemId());
            }
            
            for (Iterator<Planet> pIt = possiblePlanets.iterator();pIt.hasNext();) {
                Planet p = pIt.next();
                
                if (prune.contains(p.getSystemId())) {
                    pIt.remove();
                }
            }
            
            // Pick random planet                       
            switch (locationType) {
                case(1): // Random
                    Planet selected = possiblePlanets.get((int)Math.floor(Math.random() * possiblePlanets.size()));
                    selectedSystem = selected.getSystemId();   
                    
                    for (PlayerPlanet pp : ppList) { 
                        RelativeCoordinate ppCoord = new RelativeCoordinate(0,pp.getPlanetId());
                        if (ppCoord.getGalaxyId() != galaxyId) continue;                            

                        double distance = new RelativeCoordinate(0,selected.getId()).distanceTo(new RelativeCoordinate(0,pp.getPlanetId()));
                        
                        System.out.println("Check Distance from " + new RelativeCoordinate(0,selected.getId()).getSystemId() + " to Coordinate " + ppCoord.getGalaxyId() + ":" + ppCoord.getSystemId() + ":" + ppCoord.getPlanetId());
                        
                        if (distance < minDistanceRandom) {
                            minDistanceRandom = distance;
                        }
                    }                    
                    
                    if (minDistanceRandom == Double.MAX_VALUE) {
                        minDistanceRandom = 0d;
                    }
                    
                    break;
                case(2): // Maximized distance
                    double maxDistanceTotal = 0;
                    selected = null;
                    
                    for (Planet p : possiblePlanets) {                                                                                          
                        double minDistance = Double.MAX_VALUE;
                        
                        for (PlayerPlanet pp : ppList) { 
                            Planet pTmp = pDAO.findById(pp.getPlanetId());
                            if ((pTmp.getSystemId() < g.getStartSystem()) || (pTmp.getSystemId() > g.getEndSystem())) continue;                            
                            
                            double distance = new RelativeCoordinate(0,p.getId()).distanceTo(new RelativeCoordinate(0,pp.getPlanetId()));
                            if (distance < minDistance) {
                                minDistance = distance;
                            }
                        }
                        
                        if ((minDistance > maxDistanceTotal) && (minDistance != Double.MAX_VALUE)) {
                            selected = p;
                            maxDistanceTotal = minDistance;
                        }
                    }                    
                    
                    // Pick random
                    if (selected == null) {
                        selected = possiblePlanets.get((int)Math.floor(Math.random() * possiblePlanets.size()));
                    }
                    
                    selectedSystem = selected.getSystemId();
                    
                    return new StartLocationResult(galaxyId,selectedSystem,maxDistanceTotal);
                case(3): // Location code
                    int targetUser = 0;
                    
                    for (User u : uDAO.findAll()) {
                        if (ProfileService.locationCode(u.getId()).equalsIgnoreCase(locationCode)) {
                            targetUser = u.getId();
                            break;
                        }
                    }                                        
                    
                    if (targetUser == 0) {
                        return new StartLocationResult("alt_startloc_invalidcode");
                    }
                                                     
                    PlayerPlanet targetPlanet = ppDAO.findHomePlanetByUserId(targetUser);                    
                    RelativeCoordinate rcTarget = new RelativeCoordinate(0,targetPlanet.getPlanetId());
                    
                    if (rcTarget.getGalaxyId() != galaxyId) {
                        return new StartLocationResult("alt_startloc_playerothergalaxy");
                    }
                    
                    double minDistance = Double.MAX_VALUE;
                    selected = null;
                    
                    for (Planet p : possiblePlanets) {
                        double distance = new RelativeCoordinate(0,p.getId()).distanceTo(rcTarget);
                        if (distance < minDistance) {
                            selected = p;
                            minDistance = distance;
                        }
                    }
                    
                    selectedSystem = selected.getSystemId();
                    return new StartLocationResult(galaxyId, selectedSystem, minDistance);
            }            
        } catch (Exception e) {
            return new StartLocationResult(e.getMessage());
        }
        
        return new StartLocationResult(galaxyId,selectedSystem,minDistanceRandom);
    }
    
    public static JoinableGalaxyView getJoinableGalaxies() {
        JoinableGalaxyView jgv = new JoinableGalaxyView();
        
        for (Galaxy g : gDAO.findAll()) {
            if (g.getPlayerStart()) {
                int playerCount = 0;
                
                ArrayList<User> uList = uDAO.findAllPlayers();
                for (User u : uList) {                                        
                    int galaxyId = 0;
                    
                    try {
                        galaxyId = Integer.parseInt(SystemService.getGalaxy(u.getId()));
                    } catch (NumberFormatException nfe) {
                        
                    }
                    
                    if (galaxyId == g.getId()) {
                        playerCount++;
                    }
                }
                
                JoinableGalaxyEntry jge = new JoinableGalaxyEntry(g.getId(),playerCount,g.getWidth(),g.getHeight());
                jgv.addEntry(jge);
            }
        }
        
        return jgv;
    }
}
