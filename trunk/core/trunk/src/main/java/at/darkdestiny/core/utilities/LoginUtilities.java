package at.darkdestiny.core.utilities;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.SkipPageException;

/**
 * This class is intended to hold methods required for managing the login of 
 * the current user, allowing the JSP pages to concentrate on important tasks
 * not keeping track of the login of the user.
 * 
 * @author martin
 */
public final class LoginUtilities {
    /* REMOVED USERBUFFER
    public static UserData getUser(
            HttpServletRequest request, HttpServletResponse response
            ) throws IOException, 
                     SkipPageException {
        checkLogin(request, response);

        try {
            int userId = Integer.parseInt(
                        request.getSession().getAttribute("userId").toString()
                        );
            return UserBuffer.getUserData(userId);
        } catch (NumberFormatException e) {
            response.sendRedirect("login.jsp?errmsg=Ung&uunl;ltige%20Session");
            response.flushBuffer();
            throw new SkipPageException("No login!");
        }
     } */
    
    public static void checkLogin(
            HttpServletRequest request, HttpServletResponse response
            ) throws IOException, 
                     SkipPageException {
        if (request.getSession() == null) {
            response.sendRedirect("login.jsp?errmsg=Session%20abgelaufen");
            response.flushBuffer();
            throw new SkipPageException("No login!");
        }
        if (request.getSession().getAttribute("userId") == null) {
            response.sendRedirect("login.jsp?errmsg=Session%20abgelaufen");
            response.flushBuffer();
            throw new SkipPageException("No login!");
        }
    }
}
