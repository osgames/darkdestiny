/**
 * 
 */
package at.darkdestiny.core.admin.db;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author martin
 *
 */
public class StringField extends IDatabaseColumn {

	private final int column;
	private final boolean multiline;
	private final boolean null_on_empty;

	/**
	 * @param comment 
	 * @param readonly 
	 * @param name 
	 * @param column 
	 * @param multiline 
	 * @param null_on_empty 
	 * 
	 */
	public StringField(int column, String name, boolean readonly, boolean multiline, boolean null_on_empty, String comment) {
		super(name,comment, readonly);
		this.column = column;
		this.multiline = multiline;
		this.null_on_empty = null_on_empty;
	}

	public Object getFromResultSet(ResultSet rs) throws SQLException {
		return rs.getString(column);
	}

	public String getName() {
		return name;
	}

	public String makeEdit(Object object) {
		String head = "<input type=\"text\" name=\"f_"+name+"\" size=\"80\" value=\"";
		String end = "\" "+(readonly?"readonly ":"")+"/>"; 
		if (multiline)
		{
			head = "<textarea name=\"f_"+name+"\" rows=\"10\" cols=\"80\" "+(readonly?"readonly=\"readonly\" ":"")+">";
			end = "</textarea>\n";
		}
		return head+(null_on_empty?((object == null)?"":""+object):""+object)+end;
	}

	protected String getValueOnEmpty()
	{
		if (null_on_empty)
			return "NULL";
		return "''";
	}
}
