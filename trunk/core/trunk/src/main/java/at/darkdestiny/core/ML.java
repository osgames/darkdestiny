/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Eobane
 */
public class ML extends Service {

    private static final Logger log = LoggerFactory.getLogger(ML.class);

    private static HashMap<Integer, Locale> locales;
    private static final Locale defaultLocale = Locale.GERMANY;

    public static Language getLanguageByUserId(int userId) {
        return Service.languageDAO.findBy(Service.userDAO.findById(userId).getLocale());
    }

    //Main method to get a ml value
    public static String getMLStr(String propertiesFile, String key, Locale l, Object... params) {

        if (l == null) {
            l = defaultLocale;
            log.warn("Using default locale");
        }

        Language lang = getLanguageByLocale(l);
        if (!lang.getMasterLanguageId().equals(lang.getId())) {
            l = Service.languageDAO.findById(lang.getMasterLanguageId()).getLocale();
        }
        String data = key;
        String propertiesFileName = "Bundle";
        if (propertiesFile != null) {
            propertiesFileName = propertiesFile;
        } else {
            propertiesFileName += "_" + l.getLanguage() + "_" + l.getCountry();
        }
        // System.out.println("Constant {" + key + "] Locale [" + l.getCountry() + " " + l.getLanguage() + "] par length" + params.length);
        
        if (l.getCountry().equalsIgnoreCase("")) {
            Language lSearch = new Language();
            lSearch.setLanguage(l.getLanguage());
            lSearch = languageDAO.find(lSearch).get(0);
            
            l = new Locale(lSearch.getLanguage(),lSearch.getCountry());
        }
        
        try {
            if (data.equals(key)) {
                data = java.util.ResourceBundle.getBundle(propertiesFileName, l).getString(key);
                // data = "Hubba";
            }
        } catch (Exception e) {
            DebugBuffer.trace("Loading property file => " + propertiesFileName);
            DebugBuffer.trace("[1] Error beim holen eines " + propertiesFile + "-Multilanguagekeys: " + e + " key : " + key);
            return "e:" + data;
        }

        int counter = 0;
        for (Object param : params) {
            counter++;
            data = data.replace("%" + counter, param.toString().replace("[", "").replace("]", ""));
        }

        return data;
        //return data;
    }

    public static Language getLanguage(Integer userId) {

        User u = Service.userDAO.findById(userId);
        Language language = Service.languageDAO.findBy(u.getLocale());
        return language;

    }

    public static String getMLStr(String key, Integer userId, Object... params) {

        return getMLStr(null, key, userId, params);

    }

    public static String getMLStr(String key, Integer userId) {
        return getMLStr(null, key, userId);
    }

    public static String getMLStr(String key, Locale l) {
        return getMLStr(null, key, l, "");
    }

    public static String getMLStr(String key, Locale l,  Object... params) {
        return getMLStr(null, key, l, params);
    }

    public static String getMLStr(String propertiesFile, String key, Integer userId) {
        return getMLStr(propertiesFile, key, userId, "");
    }

    public static void removeLocale(int userId) {
        try {
            locales.remove(userId);
        } catch (Exception e) {
        }
    }

    public static Locale getLocale(int userId) {
        if (locales == null) {

            locales = new HashMap<Integer, Locale>();

        }
        Locale l = locales.get(userId);
        if (l == null) {
            loadLocaleForUser(userId);
        }
        return locales.get(userId);

    }

    private static void loadLocaleForUser(int userId) {
        // DebugBuffer.debug("Load Locale");
        //Holen des Locales vom User
        String userLocale = userDAO.findById(userId).getLocale();
        // DebugBuffer.debug("User Locale: " + userLocale);
        Locale l = null;
        try {
            //Suchen der dazugehörigen Sprache
            Language tempLang = Service.languageDAO.findBy(userLocale);
            //Wenn es eine MasterId gibt wird diese Sprache verwendet
            if (tempLang.getId().equals(tempLang.getMasterLanguageId())) {
                tempLang = Service.languageDAO.findById(tempLang.getMasterLanguageId());
                userLocale = tempLang.getLanguage() + "_" + tempLang.getCountry();
            }

            String language = userLocale.substring(0, userLocale.indexOf("_"));
            String country = userLocale.substring(userLocale.indexOf("_") + 1, userLocale.length());
            
            // DebugBuffer.debug("Language: " + language + " Country: " + country);

            l = new Locale(language, country);
        } catch (Exception e) {
            log.error("Could not load locale for user : " + userId);
        }
        
        if (l == null) {
            l = defaultLocale;
        }

        locales.put(userId, l);
    }

    public static String getLocaleString(int userId) {
        Locale l = locales.get(userId);

        return l.getLanguage() + "_" + l.getCountry();
    }

    public static String getMLStr(String key, int userId) {
        Object[] params = new Object[1];
        params[0] = "";
        return getMLStr(key, userId, params);
    }

    public static String getMLStr(String propertiesFile, String key, int userId, Object... params) {                
        String data = key;
        boolean debug = false;
        Locale l = null;
        
        if (debug) DebugBuffer.debug("Requesting language: " + propertiesFile + " userId: " + userId);        
        try {
            if (locales == null) {
                locales = new HashMap<Integer, Locale>();
            }
            l = locales.get(userId);
            if (debug) {
                DebugBuffer.addLine(DebugLevel.WARNING, "MULTILANGUAGE - " + "l : " + l);
            }
            if (l == null) {
                loadLocaleForUser(userId);
                l = locales.get(userId);
                // DebugBuffer.debug("Locale: " + l);
            }


            return getMLStr(propertiesFile, key, l, params);
        } catch (Exception e) {
            // DebugBuffer.writeStackTrace("Fatal Error: ", e);
            DebugBuffer.warning("Error beim holen eines Multilanguagekeys: " + e + " key : " + key + " for User : " + userId + " Locale : " + l);
            if (l != null) {
                DebugBuffer.warning("locale-coutner" + l.getCountry() + " locale-language : " + l.getLanguage());
            }
            // e.printStackTrace();
            return "e:" + data;
        }
    }

    public static Language getLanguageByLocale(Locale locale) {
        Language l = new Language();
        //find by detailed
        l.setLanguage(locale.getLanguage());
        l.setCountry(locale.getCountry());
        ArrayList<Language> langs = Service.languageDAO.find(l);
        if (!langs.isEmpty()) {
            l = langs.get(0);
        } else {
            l = null;
        }
        if (l == null) {
            l = new Language();
            l.setLanguage(locale.getLanguage());
            Language result = null;
            for (Language lang : Service.languageDAO.find(l)) {
                if (result == null) {
                    result = lang;
                } else {
                    if (lang.getMasterLanguageId().equals(lang.getId())) {
                        return lang;
                    }
                }
            }
            if (result != null) {
                return result;
            }
            //Unknown language take english
            l = new Language();
            l.setLanguage("en");
            l.setCountry("US");
            return Service.languageDAO.get(l);
        }
        return l;
    }

    public static String getAtlanMLStr(String key, Locale l) {
        return getMLStr("Atlan", key, l);
    }

    public static String getAtlanMLStr(String key, int userId) {

        return getMLStr("Atlan", key, userId);
    }

    public static void generateJS(String path, String file) {

        URL u = ML.class.getResource("/db.properties");

        String loc = path;
        String loc2 = GameConfig.getInstance().getSrcPathWeb() + "\\java\\";
        String jsFile = file;
        if (path == null) {
            loc = u.toString().replace("WEB-INF/classes/db.properties", "java").substring(6);
            loc = loc.replace("root/", "");
        }
        if (jsFile == null) {
            jsFile = "translate.js";
        }
        try {
            log.info( "[Javascript translations] Creating " + loc + "/" + jsFile);

            File f = new File(loc, "/" + jsFile);
            if (f.exists()) {
                f.delete();
                f = new File(loc, jsFile);
            }

            generateJsInFile(f);

            log.info( "[Javascript translations] Creating " + loc2 + "/" + jsFile);

            File f2 = new File(loc2, jsFile);
            if (f2.exists()) {
                f2.delete();
                f2 = new File(loc2, jsFile);
            }
            generateJsInFile(f2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void generateJsInFile(File f) {
        try {


            BufferedWriter writer = new BufferedWriter(new FileWriter(f));

            for (Language l : languageDAO.findAll()) {
                Properties properties = new Properties();
                writer.write("var ML_" + l.getLanguage() + "_" + l.getCountry() + " = [];");
                writer.newLine();
                properties.load(ML.class.getResourceAsStream("/" + l.getPropertiesfile()));
                for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                    String key = (String) entry.getKey();
                    String value = (String) entry.getValue();

                    if (key.startsWith("js_")) {
                        writer.write("ML_" + l.getLanguage() + "_" + l.getCountry() + "['" + key.substring(3) + "']= '" + value + "';");
                        writer.newLine();
                    }
                }
            }




            writer.write("function ML(tag, language){");
            writer.newLine();
            writer.write("try {");
            writer.newLine();
            writer.write("var result = eval('ML_' + language + '[\"' + tag + '\"]');");
            writer.write("if (result == undefined) return \"e:\"+tag;");
            writer.write("return result;");
            writer.write("} catch (err) {");
            writer.newLine();
            writer.write("return \"e:\"+tag");
            writer.newLine();
            writer.write("}");
            writer.newLine();
            writer.newLine();
            writer.write("}");
            writer.close();


        } catch (FileNotFoundException fnfe) {
            DebugBuffer.warning("Javascript Translation file cannot be found: " + fnfe.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
