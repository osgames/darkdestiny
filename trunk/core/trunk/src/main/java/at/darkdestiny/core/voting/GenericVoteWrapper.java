/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

import at.darkdestiny.core.dao.VoteMetadataDAO;
import at.darkdestiny.core.dao.VoteOptionDAO;
import at.darkdestiny.core.dao.VotePermissionDAO;
import at.darkdestiny.core.dao.VotingDAO;
import at.darkdestiny.core.dao.VotingDetailDAO;
import at.darkdestiny.framework.dao.DAOFactory;

/**
 *
 * @author Stefan
 */
public abstract class GenericVoteWrapper implements IVoteWrapper {
    protected static VotingDetailDAO vdDAO = (VotingDetailDAO) DAOFactory.get(VotingDetailDAO.class);
    protected static VotingDAO vDAO = (VotingDAO) DAOFactory.get(VotingDAO.class);
    protected static VoteMetadataDAO vmdDAO = (VoteMetadataDAO) DAOFactory.get(VoteMetadataDAO.class);
    protected static VotePermissionDAO vpDAO = (VotePermissionDAO) DAOFactory.get(VotePermissionDAO.class);
    protected static VoteOptionDAO voDAO = (VoteOptionDAO) DAOFactory.get(VoteOptionDAO.class);    
}
