/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.voting;

import at.darkdestiny.core.enumeration.EDoVoteRefType;
import java.util.Locale;

/**
 *
 * @author Stefan
 */
public interface IVoteWrapper {
    /*
     * Get the HTML representation of the vote options
    */    
    public String generateVoteBody(Vote v, int id, int userId) throws VoteException;
    /*
     * Get the HTML representation of the vote options
    */        
    public String generateVoteBody(Vote v, int id, int userId, EDoVoteRefType refType, int refId) throws VoteException;
    /*
     * Retrieve the current state of the vote (who voted for what option)
    */
    public VoteResult currentState(int userId, int voteId) throws VoteException;
    /*
     * Get a readable parsed representation of current vote state
    */    
    public String getVoteStateHTML(VoteResult vr, int voteId, Locale locale) throws VoteException;
    /*
     * Get an extended HTML representation with vote specific text as well as the 
     * HTML representation of the vote options
    */        
    public String getHtmlCode(Vote vote, int voteId, int userId);
}
