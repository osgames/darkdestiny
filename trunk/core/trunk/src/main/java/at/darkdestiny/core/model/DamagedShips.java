/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.core.enumeration.EDamageLevel;
import at.darkdestiny.core.enumeration.EDamagedShipRefType;
import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Stefan
 */
@TableNameAnnotation("damagedships")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class DamagedShips extends Model<DamagedShips> {

    @IdFieldAnnotation
    @FieldMappingAnnotation("shipFleetId")
    private Integer shipFleetId;
    @IdFieldAnnotation
    @FieldMappingAnnotation("refTable")
    private EDamagedShipRefType refTable;
    @IdFieldAnnotation
    @FieldMappingAnnotation("damageLevel")
    private EDamageLevel damageLevel;
    @FieldMappingAnnotation("count")
    private Integer count;

    public Integer getShipFleetId() {
        return shipFleetId;
    }

    public void setShipFleetId(Integer shipFleetId) {
        this.shipFleetId = shipFleetId;
    }

    public EDamageLevel getDamageLevel() {
        return damageLevel;
    }

    public void setDamageLevel(EDamageLevel damageLevel) {
        this.damageLevel = damageLevel;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public EDamagedShipRefType getRefTable() {
        return refTable;
    }

    public void setRefTable(EDamagedShipRefType refTable) {
        this.refTable = refTable;
    }
}
