/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.painting.map;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Stefan
 */
public class SMSystem {
    private final int id;
    private final int x;
    private final int y;
    private final int sysStatus;
    HashSet<Integer> diplomacyIds = new HashSet<Integer>();
    private ArrayList<SMPlanet> planets = new ArrayList<SMPlanet>();
    private boolean homeSystem = false;
    private String name;
    
    public SMSystem(int id, int x, int y, int sysStatus) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.sysStatus = sysStatus;
        this.name = "System #" + id;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @return the sysStatus
     */
    public int getSysStatus() {
        return sysStatus;
    }
    
    public void addPlanet(SMPlanet smp) {
        if (smp.isHomePlanet()) {
            homeSystem = true;
        }
        
        if (smp.getDiplomacyId() != 0) {
            diplomacyIds.add(smp.getDiplomacyId());
        }
        
        planets.add(smp);
    }
    
    public ArrayList<SMPlanet> getPlanets() {
        return planets;
    }

    /**
     * @return the homeSystem
     */
    public boolean isHomeSystem() {
        return homeSystem;
    }
    
    public boolean isOwn() {
        return diplomacyIds.contains(1);
    }
    
    public boolean isAllied() {
        return diplomacyIds.contains(10) || diplomacyIds.contains(11);
    }    
    
    public boolean isNAP() {
        return diplomacyIds.contains(12);
    }        
    
    public boolean isNeutral() {
        return diplomacyIds.contains(13) || diplomacyIds.contains(14) || diplomacyIds.contains(15);
    }            
    
    public boolean isEnemy() {
        return diplomacyIds.contains(16) || diplomacyIds.contains(17);
    }                
    
    public int getColorLayers() {
        int layers = 0;
        
        if (isOwn()) layers++;
        if (isAllied()) layers++;
        if (isNAP()) layers++;
        if (isNeutral()) layers++;
        if (isEnemy()) layers++;
        
        return layers;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
}
