/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.result;

/**
 *
 * @author Aion
 */
public class FilterResult {

    private BaseResult result;
    private int id = 0;

    /**
     * @return the result
     */
    public BaseResult getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(BaseResult result) {
        this.result = result;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

}
