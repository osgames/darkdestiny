/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.result;

/**
 *
 * @author Admin
 */
public class SystemGenerationResult {

    private int systemsGenerated;
    private int planetsGenerated;
    private boolean systemGenerated;

    public SystemGenerationResult(int systemsGenerated, int planetsGenerated, boolean systemGenerated) {
        this.systemsGenerated = systemsGenerated;
        this.planetsGenerated = planetsGenerated;
        this.systemGenerated = systemGenerated;
    }

    /**
     * @return the systemsGenerated
     */
    public int getSystemsGenerated() {
        return systemsGenerated;
    }

    /**
     * @return the planetsGenerated
     */
    public int getPlanetsGenerated() {
        return planetsGenerated;
    }

    /**
     * @return the systemGenerated
     */
    public boolean isSystemGenerated() {
        return systemGenerated;
    }
}
