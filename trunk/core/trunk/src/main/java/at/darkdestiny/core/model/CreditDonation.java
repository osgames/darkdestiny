/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.model;

import at.darkdestiny.framework.annotations.DataScope;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "creditdonation")
@DataScope(type = DataScope.EScopeType.USER_GENERATED)
public class CreditDonation extends Model<Construction> {

    @FieldMappingAnnotation("date")
    @IdFieldAnnotation
    private Long date;
    @FieldMappingAnnotation(value = "fromUserId")
    @IdFieldAnnotation
    private Integer fromUserId;
    @FieldMappingAnnotation(value = "toUserId")
    @IdFieldAnnotation
    private Integer toUserId;
    @FieldMappingAnnotation(value = "amount")
    private Long amount;

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the fromUserId
     */
    public Integer getFromUserId() {
        return fromUserId;
    }

    /**
     * @param fromUserId the fromUserId to set
     */
    public void setFromUserId(Integer fromUserId) {
        this.fromUserId = fromUserId;
    }

    /**
     * @return the toUserId
     */
    public Integer getToUserId() {
        return toUserId;
    }

    /**
     * @param toUserId the toUserId to set
     */
    public void setToUserId(Integer toUserId) {
        this.toUserId = toUserId;
    }

    /**
     * @return the amount
     */
    public Long getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
