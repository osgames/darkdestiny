/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.admin.module;

/**
 *
 * @author Stefan
 */
public class InvalidDataException extends Exception {
    public InvalidDataException(String message) {
        super(message);
    } 
}
