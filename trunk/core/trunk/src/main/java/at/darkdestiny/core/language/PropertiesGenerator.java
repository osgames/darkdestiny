/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.language;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sun.codemodel.ClassType;
import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropertiesGenerator {

    private static final boolean validate = true;
    private static final int NEXT_TO_LAST = 9999;
    private static final Map<Integer, Set<String>> allowedValues = Maps.newHashMap();

    static {
        //Top categories
        allowedValues.put(0, Sets.newHashSet("sites", "components", "common", "db"));
        //Next to last category
        allowedValues.put(NEXT_TO_LAST, Sets.newHashSet("label, button, message"));
    }

    public static void main(String[] args) {

        List<String> values = Lists.newArrayList();
        searchPropertiesFile(values);
        if (validate) {
            validate(values);
        }
        Collections.sort(values);
        createLanguageClass(values);
        createLanguageClass_static(values);
    }

    private static void validate(List<String> values) {
        for (String value : values) {
            validate(value);
        }
    }

    private static void validate(String value) {
        String[] split = value.split("\\.");
        for (int i = 0; i < split.length; i++) {

            String splitValue = split[i];
            Set<String> categories = allowedValues.get(i);
            if (categories != null) {
                if ((i == split.length - 2) && split.length > 1) {
                    categories.addAll(allowedValues.get(NEXT_TO_LAST));
                }
                if (!categories.contains(splitValue)) {
                    System.err.println("Invalid values! " + splitValue + " not on list : " + categories.toString());
                    System.exit(1);
                }
            }
        }
    }

    private static void createLanguageClass(List<String> values) {
        try {
            File existingClass = new File("src/main/java/at/darkdestiny/core/language/LANG.java");
            if (existingClass.exists()) {
                existingClass.delete();
            }

            Map<String, JDefinedClass> addedClasses = Maps.newHashMap();
            JCodeModel codeModel = new JCodeModel();
            JDefinedClass langClass = codeModel._class(JMod.PUBLIC + JMod.FINAL, "at.darkdestiny.core.language.LANG", ClassType.CLASS);
            Set<String> createdFields = Sets.newHashSet();
            for (String key : values) {
                addClassOrField(langClass, key, key, "", addedClasses, createdFields);
            }
            codeModel.build(new File("src/main/java/"));

        } catch (Exception ex) {
            Logger.getLogger(PropertiesGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void addClassOrField(JDefinedClass clazz, String key, String orgKey, String parent, Map<String, JDefinedClass> addedClasses, Set<String> createdFields) throws JClassAlreadyExistsException {
        String[] split = key.split("\\.");
        if (split.length == 1) {
            if (!createdFields.contains(orgKey)) {
                JFieldVar clazzField = clazz.field(JMod.PUBLIC, String.class, key.toUpperCase().replace("-", "_"), JExpr.lit(orgKey));
                String getterName = "get" + key.substring(0, 1).toUpperCase() + key.substring(1).toLowerCase();
                JMethod getterMethod = clazz.method(JMod.PUBLIC, String.class, getterName);
                JBlock block = getterMethod.body();
                block._return(clazzField);
                createdFields.add(orgKey);
            }
        } else {
            String newClass = split[0].toUpperCase().replace("-", "_");

            if (!parent.equals("")) {
                parent += ".";
            }
            parent += newClass;
            JDefinedClass subClazz = addedClasses.get(parent);
            if (subClazz == null) {
                subClazz = clazz._class(JMod.PUBLIC, newClass);
                JFieldVar clazzField = clazz.field(JMod.PUBLIC, subClazz, newClass, JExpr._new(subClazz));
                String getterName = "get" + newClass.substring(0, 1) + newClass.substring(1).toLowerCase();
                JMethod getterMethod = clazz.method(JMod.PUBLIC, subClazz, getterName);
                JBlock block = getterMethod.body();
                block._return(clazzField);
                addedClasses.put(parent, subClazz);
            }
            key = key.substring(key.indexOf(".") + 1);
            addClassOrField(subClazz, key, orgKey, parent, addedClasses, createdFields);
        }
    }

    private static void createLanguageClass_static(List<String> values) {
        try {
            File existingClass = new File("src/main/java/at/darkdestiny/core/language/LANG2.java");
            if (existingClass.exists()) {
                existingClass.delete();
            }

            Map<String, JDefinedClass> addedClasses = Maps.newHashMap();
            JCodeModel codeModel = new JCodeModel();
            JDefinedClass langClass = codeModel._class(JMod.PUBLIC + JMod.FINAL, "at.darkdestiny.core.language.LANG2", ClassType.CLASS);
            Set<String> createdFields = Sets.newHashSet();
            for (String key : values) {
                addClassOrField_static(langClass, key, key, "", addedClasses, createdFields);
            }
            codeModel.build(new File("src/main/java/"));

        } catch (Exception ex) {
            Logger.getLogger(PropertiesGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void addClassOrField_static(JDefinedClass clazz, String key, String orgKey, String parent, Map<String, JDefinedClass> addedClasses, Set<String> createdFields) throws JClassAlreadyExistsException {
        String[] split = key.split("\\.");
        if (split.length == 1) {
            if (!createdFields.contains(orgKey)) {
                clazz.field(JMod.PUBLIC + JMod.FINAL + JMod.STATIC, String.class, key.toUpperCase().replace("-", "_"), JExpr.lit(orgKey));

            }
        } else {
            String newClass = split[0].toUpperCase().replace("-", "_");

            if (!parent.equals("")) {
                parent += ".";
            }
            parent += newClass;
            JDefinedClass subClazz = addedClasses.get(parent);
            if (subClazz == null) {
                subClazz = clazz._class(JMod.PUBLIC + JMod.FINAL + JMod.STATIC, newClass);
                addedClasses.put(parent, subClazz);
            }
            key = key.substring(key.indexOf(".") + 1);
            addClassOrField_static(subClazz, key, orgKey, parent, addedClasses, createdFields);
        }
    }

    private static void searchPropertiesFile(List<String> values) {
        List<String> propertyFileNames = Lists.newArrayList("language/lang_de_DE.properties",
                "language/lang_en_US.properties");
        //, "Bundle_de_DE.properties"
        for (String propertyFileName : propertyFileNames) {
            Properties p = new LanguageProperties();
            FileInputStream fis = null;
            try {
                File f = new File("src/main/resources/" + propertyFileName);
                System.out.println("f : " + f);
                System.out.println("f.path : " + f.getAbsolutePath());
                System.out.println("exists : " + f.exists());
                if (!f.exists() && !f.isDirectory()) {
                    continue;
                }
                fis = new FileInputStream(new File("src/main/resources/" + propertyFileName));
                p.load(fis);
            } catch (Exception ex) {
                Logger.getLogger(PropertiesGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(PropertiesGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }

            for (Object key : p.keySet()) {
                String stringKey = String.valueOf(key);
                if (!values.contains(stringKey)) {
                    values.add(stringKey);
                }
            }
            try {
                p.store(new FileWriter(new File("src/main/resources/" + propertyFileName)), "language file");
            } catch (IOException ex) {
                Logger.getLogger(PropertiesGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private static void searchClass(Class clazz, String key, List<String> values) {

        key += clazz.getSimpleName().toLowerCase();

        for (Field field : clazz.getDeclaredFields()) {
            if (field.getType() == String.class) {
                String fieldKey = key + "." + field.getName().toLowerCase();
                values.add(fieldKey);
            }

        }

        for (Class c : clazz.getDeclaredClasses()) {
            key += ".";
            searchClass(c, key, values);
        }

    }

}
