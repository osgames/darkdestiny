/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.ships;

import at.darkdestiny.core.admin.module.AttributeTree;
import at.darkdestiny.core.admin.module.ModuleAttributeResult;
import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.dao.ChassisDAO;
import at.darkdestiny.core.dao.DesignModuleDAO;
import at.darkdestiny.core.dao.ShipTypeAdjustmentDAO;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.RessourcesEntry;
import at.darkdestiny.core.enumeration.EAttribute;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.DesignModule;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipTypeAdjustment;
import at.darkdestiny.core.ressources.MutableRessourcesEntry;
import at.darkdestiny.core.service.IdToNameService;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Stefan
 */
public class ShipUpgradeCost extends MutableRessourcesEntry {
    private DesignModuleDAO dmDAO = (DesignModuleDAO) DAOFactory.get(DesignModuleDAO.class);
    private ChassisDAO chDAO = (ChassisDAO) DAOFactory.get(ChassisDAO.class);
    private static ShipTypeAdjustmentDAO staDAO = (ShipTypeAdjustmentDAO) DAOFactory.get(ShipTypeAdjustmentDAO.class);

    private final int count;
    private final BuildTime buildTime;
    private final ShipDesignExt sde;
    private final ShipDesignExt sdeTo;

    public ShipUpgradeCost(ShipDesignExt sde, ShipDesignExt sdeTo) {
        this.count = 1;
        this.sde = sde;
        this.sdeTo = sdeTo;
        this.buildTime = new BuildTime();

        calcUpgradeCost();
    }

    public ShipUpgradeCost(ShipDesignExt sde, ShipDesignExt sdeTo, int count) {
        this.count = count;
        this.sde = sde;
        this.sdeTo = sdeTo;
        this.buildTime = new BuildTime();

        calcUpgradeCost();
    }

    private void calcUpgradeCost() {
        // @TODO
        // Compare Modules between sde and sdeTo
        // and calculate BuildTime and RessourceCost
        ShipDesign orgDesign = (ShipDesign)sde.getBase();
        ShipDesign newDesign = (ShipDesign)sdeTo.getBase();

        boolean addChassisReconCost = false;

        if (((orgDesign.getType() == EShipType.TRANSPORT) && (newDesign.getType() != EShipType.TRANSPORT)) ||
            ((orgDesign.getType() != EShipType.TRANSPORT) && (newDesign.getType() == EShipType.TRANSPORT))) {
            addChassisReconCost = true;
        }

        ArrayList<DesignModule> orgModuleList = dmDAO.findByDesignId(orgDesign.getId());
        ArrayList<DesignModule> newModuleList = dmDAO.findByDesignId(newDesign.getId());

        HashMap<Integer,Integer> oldModules = new HashMap<Integer,Integer>();
        HashMap<Integer,Integer> newModules = new HashMap<Integer,Integer>();

        for (DesignModule dm : orgModuleList) {
            oldModules.put(dm.getModuleId(), dm.getCount());
        }
        for (DesignModule dm : newModuleList) {
            newModules.put(dm.getModuleId(), dm.getCount());
        }

        HashMap<Integer,Integer> toAdd = new HashMap<Integer,Integer>();
        HashMap<Integer,Integer> toRemove = new HashMap<Integer,Integer>();

        for (Map.Entry<Integer,Integer> oldEntry : oldModules.entrySet()) {
            int diffCount = 0;
            if (newModules.containsKey(oldEntry.getKey())) {
                diffCount = newModules.get(oldEntry.getKey()) - oldEntry.getValue();
            } else {
                diffCount -= oldEntry.getValue();
            }

            if (diffCount < 0) {
                toRemove.put(oldEntry.getKey(), Math.abs(diffCount));
            } else {
                toAdd.put(oldEntry.getKey(), diffCount);
            }
        }

        for (Map.Entry<Integer,Integer> newEntry : newModules.entrySet()) {
            if (!oldModules.containsKey(newEntry.getKey())) {
                toAdd.put(newEntry.getKey(), newEntry.getValue());
            }
        }

        for (Iterator<Map.Entry<Integer,Integer>> removeIt = toRemove.entrySet().iterator();removeIt.hasNext();) {
            Map.Entry<Integer,Integer> removeEntry = removeIt.next();
            if (removeEntry.getValue() == 0) {
                removeIt.remove();
                continue;
            }
        }

        for (Iterator<Map.Entry<Integer,Integer>> addIt = toAdd.entrySet().iterator();addIt.hasNext();) {
            Map.Entry<Integer,Integer> addEntry = addIt.next();
            if (addEntry.getValue() == 0) {
                addIt.remove();
                continue;
            }
        }

        AttributeTree atOld = new AttributeTree(orgDesign.getUserId(),orgDesign.getDesignTime());
        AttributeTree atNew = new AttributeTree(newDesign.getUserId(),newDesign.getDesignTime());

        // For removed Modules calculate recycling cost
        for (Map.Entry<Integer,Integer> oldEntry : toRemove.entrySet()) {
            ArrayList<RessAmountEntry> raeList = atOld.getRessourceCost(newDesign.getChassis(), oldEntry.getKey());
            RessourcesEntry re = new RessourcesEntry(raeList);

            MutableRessourcesEntry mre = new MutableRessourcesEntry();
            mre.addRess(re, oldEntry.getValue());
            mre.multiplyRessources(count);

            RessourcesEntry re2 = new RessourcesEntry(mre.getRessArray());
            this.addRess(re2, 1d);

            buildTime.setDockPoints(buildTime.getDockPoints() + (10 * oldEntry.getValue()) * count);
        }

        this.multiplyRessources(-0.8d, 0.1d);

        // COST after removing some crap
        /*
        for (RessAmountEntry rae : this.getRessArray()) {
            log.debug("RessId: " + rae.getRessId() + " Qty: " + rae.getQty());
        }
        */
        IdToNameService itns = new IdToNameService(((ShipDesign)sde.getBase()).getUserId());

        // For added modules calculate production cost
        ShipTypeAdjustment sta = staDAO.findByType(newDesign.getType());

        for (Map.Entry<Integer,Integer> newEntry : toAdd.entrySet()) {
            ArrayList<RessAmountEntry> raeList = atNew.getRessourceCost(newDesign.getChassis(), newEntry.getKey());
            RessourcesEntry re = new RessourcesEntry(raeList);
            MutableRessourcesEntry mre = new MutableRessourcesEntry();
            mre.addRess(re);
            mre.multiplyRessources(1d, sta.getCostFactor());

            /*
            log.debug("You just built in " + newEntry.getValue() + " fucking " + moduleDAO.findById(newEntry.getKey()).getName() + " for the price of ");
            for (RessAmountEntry rae : mre.getRessArray()) {
                log.debug("RessId: " + rae.getRessId() + " Qty: " + rae.getQty());
            }
            */

            this.addRess(mre, newEntry.getValue() * count);

            ModuleAttributeResult mar = atNew.getAllAttributes(newDesign.getChassis(), newEntry.getKey());
            int modConsPoints = (int)mar.getAttributeValue(EAttribute.MOD_CONS_POINTS);

            buildTime.setModulePoints(buildTime.getModulePoints() + (modConsPoints * newEntry.getValue()) * count);
        }

        if (addChassisReconCost) {
            // DebugBuffer.warning("Increasing cost due to chassis change");

            long credits = (long)Math.abs(sde.getRessCost().getRess(Ressource.CREDITS) - sdeTo.getRessCost().getRess(Ressource.CREDITS));


            MutableRessourcesEntry diff = new MutableRessourcesEntry();
            diff.setRess(Ressource.CREDITS, credits);

            MutableRessourcesEntry mre = new MutableRessourcesEntry();

            mre.addRess(diff, 1.1d);

            this.addRess(mre);
        }

        // Load chassis multiplicator
        Chassis ch = chDAO.findById(newDesign.getChassis());
        this.multiplyRessources(ch.getMinBuildQty());
    }

    public int getCount() {
        return count;
    }

    public BuildTime getBuildTime() {
        return buildTime;
    }
}
