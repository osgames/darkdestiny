/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core;

import at.darkdestiny.core.xml.XMLMemo;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *
 * @author Stefan
 */
public class StarMapInfoUtilities {     
    public static StringBuffer getStarMapInfo(Integer userId) {
        XMLMemo starmap = new XMLMemo("Starmap");
        XMLMemo coding = new XMLMemo("Codierung");
        XMLMemo typeEntry = new XMLMemo("LandCode");
        typeEntry.addAttribute("A", "Gasriese, lebensfeindlich");
        typeEntry.addAttribute("B", "Gasriese, lebensfeindlich");
        typeEntry.addAttribute("C", "Kalt, bewohnbar");
        typeEntry.addAttribute("E", "Keine Atmosph\u00E4re, lebensfeindlich");
        typeEntry.addAttribute("G", "Heiss, bewohnbar");
        typeEntry.addAttribute("J", "Geologisch sehr aktiv, lebensfeindlich");
        typeEntry.addAttribute("L", "Fels und Eis, lebensfeindlich");
        typeEntry.addAttribute("M", "Erd\u00E4hnlich, bewohnbar");
        coding.addChild(typeEntry);
        starmap.addChild(coding);

        StarMapInfoData smid;
        StarMapInfo smi;
        
        if (userId != null) {
            smid = new StarMapInfoData(userId);
            smi = new StarMapInfo(userId);
        } else {
            smid = new StarMapInfoData();
            smi = new StarMapInfo();
        }                

        if (userId != null) starmap = smi.addConstants(starmap);
        //  DebugBuffer.addLine(DebugLevel.WARNING, "Adding constants in " + (System.currentTimeMillis() - time) + " ms");
        if (userId != null) starmap = smi.addObservatory(starmap, smid);
        //   DebugBuffer.addLine(DebugLevel.WARNING, "Adding observatories in " + (System.currentTimeMillis() - time) + " ms");
        starmap = smi.addSystems(starmap, smid);
        // DebugBuffer.addLine(DebugLevel.WARNING, "Adding systems in " + (System.currentTimeMillis() - time) + " ms");
        if (userId != null) starmap = smi.addAllFleets(starmap, smid);
        // DebugBuffer.addLine(DebugLevel.WARNING, "Adding fleets in " + (System.currentTimeMillis() - time) + " ms");
        if (userId != null) starmap = smi.addHyperScanner(starmap, smid);
        //  DebugBuffer.addLine(DebugLevel.WARNING, "Adding Hyperscanner in " + (System.currentTimeMillis() - time) + " ms");
        if (userId != null) starmap = smi.addTradeRoutes(starmap);

        if (userId != null) smi.addSunTransmitter(starmap);
        //   DebugBuffer.addLine(DebugLevel.WARNING, "Adding traderoutes in " + (System.currentTimeMillis() - time) + " ms");
        if (userId != null) smi.addTerritories(starmap, smid);        
         
         StringBuffer sb = null;
         StringWriter sw = new StringWriter();
         PrintWriter pw = new PrintWriter(sw);
         
         starmap.write(pw);
         
         sb = sw.getBuffer();
         
         StringBuilder declaration = new StringBuilder(); 
         declaration.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
         
         sb.insert(0, declaration);

         return sb;
    }
}
