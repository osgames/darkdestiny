package at.darkdestiny.core.utilities.img;

import java.awt.Graphics;
import java.io.Serializable;

/**
 * Hiermit werden Zeichnungen direkt auf dem Server ausgef&uuml;hrt
 * 
 * Die Zeichnung kann in den Funtionen dieses Interfaces bearbeitet
 * werden
 * 
 * @author martin
 */
public interface IModifyImageFunction extends Serializable{

	void setSize(int width, int height);

	void run(Graphics graphics);

	public String createMapEntry();
}
