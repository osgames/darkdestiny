 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.view.menu;

import at.darkdestiny.core.model.Ressource;

/**
 *
 * Datenklasse f?r die Klasse StatusBarData.java
 * Daten zum Erstellen der StatusBar
 * 
 * @author Dreloc
 */
public class EnergyEntity {
    
    private RessourceEntity energy;

   
    public EnergyEntity(Ressource ressource){
        energy = new RessourceEntity(ressource);
    }
    public RessourceEntity getEnergy() {
        return energy;
    }

    public void setEnergy(RessourceEntity energy) {
        this.energy = energy;
    }
    
    
}
