/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.sandbox.trade.util;

import at.darkdestiny.core.sandbox.trade.dto.TPlanet;
import com.google.common.collect.Maps;
import java.util.HashMap;

/**
 *
 * @author Feuerelfe
 */

    public class TestTradeData {

        private TPlanet planet;
        private HashMap<Integer, Long> expectedTransportedResources = Maps.newHashMap();
        int resourcesCompared = 0;
        private HashMap<Integer, TestTradePlanetData> testPlanets = Maps.newHashMap();

        public TestTradeData(int startPlanetId, long capacity, TestResources testResources) {
            this.expectedTransportedResources = testResources.getExpectedTransportedResources();
            planet = new TPlanet(startPlanetId);
            planet.setOutgoingCapacity(capacity);
            planet.setOutgoingPossibleByPlanet(testResources.getOutgoingPossibleByPlanet());
        }

        public void addPlanet(TestTradePlanetData tplanet) {
            planet.getOutgoingEdges().add(tplanet.getPlanet());
            getTestPlanets().put(tplanet.getPlanet().getId(), tplanet);
        }

        public int getResourcesCompared() {
            return resourcesCompared;
        }

        public void incrementResourcesCompared() {
            resourcesCompared++;
        }

        /**
         * @return the planet
         */
        public TPlanet getPlanet() {
            return planet;
        }

        /**
         * @return the expectedTransportedResources
         */
        public HashMap<Integer, Long> getExpectedTransportedResources() {
            return expectedTransportedResources;
        }

        /**
         * @return the testPlanets
         */
        public HashMap<Integer, TestTradePlanetData> getTestPlanets() {
            return testPlanets;
        }

    }