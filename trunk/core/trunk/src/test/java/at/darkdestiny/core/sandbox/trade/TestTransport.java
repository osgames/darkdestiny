/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.sandbox.trade;

import at.darkdestiny.core.sandbox.trade.util.TestTradePlanetData;
import at.darkdestiny.core.sandbox.trade.util.TestResources;
import at.darkdestiny.core.sandbox.trade.util.TestTradeData;
import at.darkdestiny.framework.dao.DAOFactory;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.BeforeClass;

/**
 *
 * @author Stefan
 */
public class TestTransport  extends AbstractTestTrade{


    @BeforeClass
    public static void setupGlobal() {

    }

    @AfterClass
    public static void shutdownGlobal() {
        DAOFactory.setVirtualMode(false);
    }

    @Before
    public void setup() {
    }

    /**
     * Test with restriction on target planet
     */
    public void OneRouteOneRessource_1() {

        TestResources res = new TestResources();

        long capacity = 150;

        res.addOutgoingPossibleByPlanet(1, 1000l);
        res.addOutgoingPossibleByPlanet(2, 10000l);

        res.addExpectedTransportedResource(1, 10l / 2);


        //Startplanet
        TestTradeData testPlanet = new TestTradeData(startPlanet_id, capacity, res);

        //Next Planet
        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 20l);

        res.addExpectedReceivedResource(1, 10l / 2);
        TestTradePlanetData targetPlanet = new TestTradePlanetData(planetA_id, res);

        testPlanet.addPlanet(targetPlanet);

        //Add routing
        testPlanet.getPlanet().addRoutingEntry(targetPlanet.getPlanet(), 1, 10l);

        assertEquals(140l, TTradeProcessing.doIt(testPlanet.getPlanet(), TTradeProcessing.ETTransportType.TRANSPORT));

        evaluate(testPlanet);
    }


}
