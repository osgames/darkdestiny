package at.darkdestiny.core.sandbox.trade.util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import at.darkdestiny.core.sandbox.trade.dto.TPlanet;
import java.util.HashMap;

/**
 *
 * @author Feuerelfe*/
  public class TestTradePlanetData {

        private TPlanet planet;
        private HashMap<Integer, Long> expectedReceivedResources;
        
        public TestTradePlanetData(int planetId, double distanceFromStartPlanet, TestResources testResources) {

            planet = new TPlanet(planetId, distanceFromStartPlanet);
            this.expectedReceivedResources = testResources.getExpectedReceivedResources();
            planet.setIncomingPossibleByPlanet(testResources.getIncomePossibleByPlanet());
        }

        public TestTradePlanetData(int planetId, TestResources testResources) {

            planet = new TPlanet(planetId);
            this.expectedReceivedResources = testResources.getExpectedReceivedResources();
            planet.setIncomingPossibleByPlanet(testResources.getIncomePossibleByPlanet());
        }
        /**
         * @return the planet
         */
        public TPlanet getPlanet() {
            return planet;
        }

        /**
         * @return the expectedReceivedResources
         */
        public HashMap<Integer, Long> getExpectedReceivedResources() {
            return expectedReceivedResources;
        }
    }