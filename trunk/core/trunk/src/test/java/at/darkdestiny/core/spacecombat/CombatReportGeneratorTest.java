/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.DBHelper;
import at.darkdestiny.core.DBUtils;
import at.darkdestiny.core.ShipDesignUtils;
import at.darkdestiny.core.VirtualService;
import at.darkdestiny.core.battlelog.BattleLogSimulationSet;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.ShipDesignService;
import at.darkdestiny.core.spacecombat.combatcontroller.SpaceCombatNew;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
/**
 *
 * @author Admin
 */
public class CombatReportGeneratorTest extends DBHelper{

    public CombatReportGeneratorTest() {
    }

    public void test_CombatReport(){
    User att = DBUtils.generateFritz();
        User def = DBUtils.generateFrank();

        ShipDesign sd1 = ShipDesignUtils.generateShipDesign_Fighter(att.getId());
        ShipDesign sd2 = ShipDesignUtils.generateShipDesign_Fighter(def.getId());

        ShipDesignService.updateDesignCosts(sd1.getId());
        ShipDesignService.updateDesignCosts(sd2.getId());

        HashMap<Integer, Integer> attackerShips = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> defenderShips = new HashMap<Integer, Integer>();

        at.darkdestiny.core.model.System system = new at.darkdestiny.core.model.System();
        system.setX(1);
        system.setY(1);
        system.setId(1);

        system = VirtualService.systemDAO.add(system);
        assertTrue(VirtualService.systemDAO.findAll().size() == 1);

        Planet planet = new Planet();
        planet.setId(1);
        planet.setSystemId(system.getId());
        planet.setLandType(Planet.LANDTYPE_A);

        planet = VirtualService.planetDAO.add(planet);
        assertTrue(VirtualService.planetDAO.findAll().size() == 1);

        PlayerPlanet pp = new PlayerPlanet();
        pp.setPlanetId(planet.getId());
        pp.setUserId(def.getId());

        VirtualService.playerPlanetDAO.add(pp);
        assertTrue(VirtualService.playerPlanetDAO.findAll().size() == 1);


        int defFleetId = 1;
        int attFleetId = 2;

        //Shipfleets
        ShipFleet shipFleetAtt = new ShipFleet();
        ShipFleet shipFleetDef = new ShipFleet();

        shipFleetAtt.setDesignId(sd1.getId());
        shipFleetAtt.setCount(10);
        shipFleetAtt.setFleetId(defFleetId);

        shipFleetDef.setDesignId(sd2.getId());
        shipFleetDef.setCount(10);
        shipFleetDef.setFleetId(attFleetId);


        defenderShips.put(sd1.getId(), 200);
        attackerShips.put(sd2.getId(), 100);

        PlayerFleet pfleetDef = new PlayerFleet();
        PlayerFleet pfleetAtt = new PlayerFleet();

        pfleetDef.setUserId(def.getId());
        pfleetDef.setLocation(ELocationType.PLANET, planet.getId());
        pfleetAtt.setUserId(att.getId());
        pfleetAtt.setLocation(ELocationType.PLANET, planet.getId());

        HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants = new HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>>();



        HashMap<Integer, HashMap<Integer, Integer>> fleetDef = new HashMap<Integer, HashMap<Integer, Integer>>();
        HashMap<Integer, HashMap<Integer, Integer>> fleetAtt = new HashMap<Integer, HashMap<Integer, Integer>>();
        fleetDef.put(defFleetId, defenderShips);
        fleetAtt.put(attFleetId, attackerShips);

        participants.put(def.getId(), fleetDef);
        participants.put(att.getId(), fleetAtt);

        HashMap<Integer, Integer> groundDefense = new HashMap<Integer, Integer>();
        boolean HUShield = false;
        boolean PAShield = false;

        Integer planetOwner = def.getId();

        HashMap<Integer, ArrayList<Integer>> groups = new HashMap<Integer, ArrayList<Integer>>();
        groups.put(1, Lists.newArrayList(def.getId()));
        groups.put(2, Lists.newArrayList(att.getId()));

        RelativeCoordinate relativeCoordinate = new RelativeCoordinate(system.getId(), planet.getId());
        AbsoluteCoordinate absoluteCoordinate = new AbsoluteCoordinate(system.getX(), system.getY());

        BattleLogSimulationSet blss = new BattleLogSimulationSet(participants, groups, SpaceCombatNew.battleClass, planetOwner, HUShield, PAShield, groundDefense, relativeCoordinate, absoluteCoordinate, CombatReportGenerator.CombatType.ORBITAL_COMBAT);

        CombatHandler_NEW combatHandler = new CombatHandler_NEW(blss);

        combatHandler.executeBattle();
        combatHandler.processCombatResults();
        CombatReportGenerator crg = blss.getCombatReportGenerator();

        crg.generateReports();
        HashMap<UserReportKey, StringBuilder> reports = crg.getUserReports();
        for(Map.Entry<UserReportKey, StringBuilder> entry : reports.entrySet()){
           System.out.println("User: " + entry.getKey().getUser());
           System.out.println(entry.getValue().toString());
        }
}

}
