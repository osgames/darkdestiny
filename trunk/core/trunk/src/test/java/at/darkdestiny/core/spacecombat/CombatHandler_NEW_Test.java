/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.spacecombat;

import at.darkdestiny.core.DBHelper;
import at.darkdestiny.core.DBUtils;
import at.darkdestiny.core.ShipDesignUtils;
import at.darkdestiny.core.VirtualService;
import at.darkdestiny.core.battlelog.BattleLogSimulationSet;
import at.darkdestiny.core.buildable.ShipDesignExt;
import at.darkdestiny.core.databuffer.RessAmountEntry;
import at.darkdestiny.core.databuffer.fleet.AbsoluteCoordinate;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.ELocationType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerFleet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ScrapResource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.ShipFleet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.ShipDesignService;
import at.darkdestiny.core.spacecombat.combatcontroller.SpaceCombatNew;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author Admin
 */
public class CombatHandler_NEW_Test extends DBHelper {

    public CombatHandler_NEW_Test() {
    }

    public void testSimpleCombat() {


        User att = DBUtils.generateFritz();
        User def = DBUtils.generateFrank();

        ShipDesign sd1 = ShipDesignUtils.generateShipDesign_Fighter(att.getId());
        ShipDesign sd2 = ShipDesignUtils.generateShipDesign_Fighter(def.getId());

        HashMap<Integer, Integer> attackerShips = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> defenderShips = new HashMap<Integer, Integer>();

        at.darkdestiny.core.model.System system = new at.darkdestiny.core.model.System();
        system.setX(1);
        system.setY(1);
        system.setId(1);

        system = VirtualService.systemDAO.add(system);
        assertTrue(VirtualService.systemDAO.findAll().size() == 1);

        Planet planet = new Planet();
        planet.setId(1);
        planet.setSystemId(system.getId());
        planet.setLandType(Planet.LANDTYPE_A);

        planet = VirtualService.planetDAO.add(planet);
        assertTrue(VirtualService.planetDAO.findAll().size() == 1);

        PlayerPlanet pp = new PlayerPlanet();
        pp.setPlanetId(planet.getId());
        pp.setUserId(def.getId());

        VirtualService.playerPlanetDAO.add(pp);
        assertTrue(VirtualService.playerPlanetDAO.findAll().size() == 1);


        int defFleetId = 1;
        int attFleetId = 2;

        //Shipfleets
        ShipFleet shipFleetAtt = new ShipFleet();
        ShipFleet shipFleetDef = new ShipFleet();

        shipFleetAtt.setDesignId(sd1.getId());
        shipFleetAtt.setCount(10);
        shipFleetAtt.setFleetId(defFleetId);

        shipFleetDef.setDesignId(sd2.getId());
        shipFleetDef.setCount(10);
        shipFleetDef.setFleetId(attFleetId);


        defenderShips.put(sd1.getId(), 20);
        attackerShips.put(sd2.getId(), 10);

        PlayerFleet pfleetDef = new PlayerFleet();
        PlayerFleet pfleetAtt = new PlayerFleet();

        pfleetDef.setUserId(def.getId());
        pfleetDef.setLocation(ELocationType.PLANET, planet.getId());
        pfleetAtt.setUserId(att.getId());
        pfleetAtt.setLocation(ELocationType.PLANET, planet.getId());

        HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>> participants = new HashMap<Integer, HashMap<Integer, HashMap<Integer, Integer>>>();

        HashMap<Integer, HashMap<Integer, Integer>> fleetDef = new HashMap<Integer, HashMap<Integer, Integer>>();
        HashMap<Integer, HashMap<Integer, Integer>> fleetAtt = new HashMap<Integer, HashMap<Integer, Integer>>();
        fleetDef.put(defFleetId, defenderShips);
        fleetAtt.put(attFleetId, attackerShips);

        participants.put(def.getId(), fleetDef);
        participants.put(att.getId(), fleetAtt);

        HashMap<Integer, Integer> groundDefense = new HashMap<Integer, Integer>();
        boolean HUShield = false;
        boolean PAShield = false;

        Integer planetOwner = def.getId();

        HashMap<Integer, ArrayList<Integer>> groups = new HashMap<Integer, ArrayList<Integer>>();
        groups.put(1, Lists.newArrayList(def.getId()));
        groups.put(2, Lists.newArrayList(att.getId()));

        RelativeCoordinate relativeCoordinate = new RelativeCoordinate(system.getId(), planet.getId());
        AbsoluteCoordinate absoluteCoordinate = new AbsoluteCoordinate(system.getX(), system.getY());

        BattleLogSimulationSet blss = new BattleLogSimulationSet(participants, groups, SpaceCombatNew.battleClass, planetOwner, HUShield, PAShield, groundDefense, relativeCoordinate, absoluteCoordinate);

        CombatHandler_NEW combatHandler = new CombatHandler_NEW(blss);

        combatHandler.executeBattle();
        combatHandler.processCombatResults();


    }

    public void testProduceScrapForFleet() {


        int systemId = 10;

        User user = DBUtils.generateFritz();

        ShipDesign sd1 = ShipDesignUtils.generateShipDesign_Fighter(user.getId());

        ShipDesignService.updateDesignCosts(sd1.getId());

        at.darkdestiny.core.model.System system = new at.darkdestiny.core.model.System();
        system.setX(1);
        system.setY(1);
        system.setId(systemId);

        system = VirtualService.systemDAO.add(system);
        assertTrue(VirtualService.systemDAO.findAll().size() == 1);

        Planet planet = new Planet();
        planet.setSystemId(system.getId());
        planet.setId(1);
        planet.setLandType(Planet.LANDTYPE_A);

        planet = VirtualService.planetDAO.add(planet);
        assertTrue(VirtualService.planetDAO.findAll().size() == 1);

        PlayerFleet fleetUser = new PlayerFleet();
        fleetUser.setUserId(user.getId());
        fleetUser.setLocation(ELocationType.PLANET, planet.getId());

        VirtualService.playerFleetDAO.add(fleetUser);

        ShipFleet shipFleetUser = new ShipFleet();

        int shipCount = 10;

        shipFleetUser.setDesignId(sd1.getId());
        shipFleetUser.setCount(shipCount);
        shipFleetUser.setFleetId(fleetUser.getId());

        VirtualService.shipFleetDAO.add(shipFleetUser);
        TreeMap<Integer, ScrapResource> scrapMap = Maps.newTreeMap();
        // Not static anymore - cause the function needs access to non static variable in CombatHandler
        // Move to Utility if static access is necessary .. mmmk?
        // produceScrapForFleet(fleetUser, scrapMap);
        ArrayList<ScrapResource> scrap = Lists.newArrayList();
        scrap.addAll(scrapMap.values());

        assertTrue(!scrap.isEmpty());

        ShipDesignExt shipDesignExt = new ShipDesignExt(sd1);

        //Check if for each resource of shipdesign a scrap entry was generated
        for (Map.Entry<Integer, RessAmountEntry> entry : shipDesignExt.getRessCost().getRess().entrySet()) {
            boolean exists = false;
            for (ScrapResource scrapResource : scrap) {
                if (entry.getKey().equals(scrapResource.getResourceId())) {
                    exists = true;
                    Ressource resource = VirtualService.ressourceDAO.findById(scrapResource.getResourceId());
                    assertTrue(resource.getTransportable());
                    //Assert that the quantity has been calculated right
                    assertEquals((long) (entry.getValue().getQty() * shipCount * CombatHandler_NEW.SCRAP_RATE), (long) scrapResource.getQuantity());
                    break;
                }
            }
            Ressource resource = VirtualService.ressourceDAO.findById(entry.getKey());
            //If the resource is transportable a scrapentry has to exist
            if (resource.getTransportable()) {
                assertTrue(exists);
            }
        }
    }

    public void testProduceScrapForShipFleet_withSurvivors() {


        int systemId = 10;

        User user = DBUtils.generateFritz();

        ShipDesign sd1 = ShipDesignUtils.generateShipDesign_Fighter(user.getId());

        ShipDesignService.updateDesignCosts(sd1.getId());

        at.darkdestiny.core.model.System system = new at.darkdestiny.core.model.System();
        system.setX(1);
        system.setY(1);
        system.setId(systemId);

        system = VirtualService.systemDAO.add(system);
        assertTrue(VirtualService.systemDAO.findAll().size() == 1);

        Planet planet = new Planet();
        planet.setId(1);
        planet.setSystemId(system.getId());
        planet.setLandType(Planet.LANDTYPE_A);

        planet = VirtualService.planetDAO.add(planet);
        assertTrue(VirtualService.planetDAO.findAll().size() == 1);

        PlayerFleet fleetUser = new PlayerFleet();
        fleetUser.setUserId(user.getId());
        fleetUser.setLocation(ELocationType.PLANET, planet.getId());

        VirtualService.playerFleetDAO.add(fleetUser);

        ShipFleet shipFleetUser = new ShipFleet();

        int shipCount = 10;
        int survivors = 5;

        shipFleetUser.setDesignId(sd1.getId());
        shipFleetUser.setCount(shipCount);
        shipFleetUser.setFleetId(fleetUser.getId());

        VirtualService.shipFleetDAO.add(shipFleetUser);
        TreeMap<Integer, ScrapResource> scrapMap = Maps.newTreeMap();
        CombatHandler_NEW.produceScrapForShipFleet(shipFleetUser, scrapMap, survivors, fleetUser.getLocationType(), fleetUser.getLocationId());
        ArrayList<ScrapResource> scrap = Lists.newArrayList();
        scrap.addAll(scrapMap.values());

        assertTrue(!scrap.isEmpty());

        ShipDesignExt shipDesignExt = new ShipDesignExt(sd1);

        //Check if for each resource of shipdesign a scrap entry was generated
        for (Map.Entry<Integer, RessAmountEntry> entry : shipDesignExt.getRessCost().getRess().entrySet()) {
            boolean exists = false;
            for (ScrapResource scrapResource : scrap) {
                if (entry.getKey().equals(scrapResource.getResourceId())) {
                    exists = true;
                    Ressource resource = VirtualService.ressourceDAO.findById(scrapResource.getResourceId());
                    assertTrue(resource.getTransportable());
                    //Assert that the quantity has been calculated right
                    assertEquals((long) (entry.getValue().getQty() * (shipCount - survivors) * CombatHandler_NEW.SCRAP_RATE), (long) scrapResource.getQuantity());
                    break;
                }
            }
            Ressource resource = VirtualService.ressourceDAO.findById(entry.getKey());
            //If the resource is transportable a scrapentry has to exist
            if (resource.getTransportable()) {
                assertTrue(exists);
            }
        }

        try {
            VirtualService.scrapResourceDAO.insertAll(scrap);
           ScrapResource scrapResource = VirtualService.scrapResourceDAO.findBy(planet.getId(), ELocationType.PLANET, 1);
           assertNotNull(scrapResource);
        } catch (Exception e) {
            fail("Could not save scrapresources: " + e);
        }
        assertEquals(VirtualService.scrapResourceDAO.findAll().size(), scrap.size());
    }
}
