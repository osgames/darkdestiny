/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.servlets;

import at.darkdestiny.core.DBHelper;
import at.darkdestiny.core.DBUtils;
import at.darkdestiny.core.VirtualService;
import at.darkdestiny.core.enumeration.EViewSystemLocationType;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserSettings;
import at.darkdestiny.core.update.Updater2;
import static org.junit.Assert.*;
import at.darkdestiny.core.utilities.ViewSystemTableUtilities;
import org.junit.Test;
/**
 *
 * @author Admin
 */
public class CheckFlightTarget_Test extends DBHelper {

    public CheckFlightTarget_Test() {
        super();
    }


    public void test_flightTarget() {

User u = DBUtils.generateFrank(false);

        UserSettings us = DBUtils.generateUserSettings(u.getId());

        at.darkdestiny.core.model.System homeSystem = DBUtils.generateSystem();
        homeSystem.setX(500);
        homeSystem.setY(500);
        VirtualService.systemDAO.update(homeSystem);
        Planet p = DBUtils.generatePlanet(homeSystem.getId(), Planet.LANDTYPE_M);
        PlayerPlanet pp = DBUtils.generatePlayerPlanet(p.getId(), u.getId());

        Updater2.calculateSystemViewUpdates();

        assertTrue(VirtualService.viewSystemDAO.findBy(homeSystem.getId(), EViewSystemLocationType.SYSTEM, u.getId()) != null);

        at.darkdestiny.core.model.System s1 = DBUtils.generateSystem();
        s1.setX(550);
        s1.setY(550);
        VirtualService.systemDAO.update(s1);


        at.darkdestiny.core.model.System s2 = DBUtils.generateSystem();
        s2.setX(1550);
        s2.setY(1550);
        VirtualService.systemDAO.update(s2);
        ViewSystemTableUtilities.reInitQuadTree();

        Updater2.calculateSystemViewUpdates();
        assertTrue(VirtualService.viewSystemDAO.findBy(s1.getId(), EViewSystemLocationType.SYSTEM, u.getId()) != null);
        assertFalse(VirtualService.viewSystemDAO.findBy(s2.getId(), EViewSystemLocationType.SYSTEM, u.getId()) != null);



    }
}
