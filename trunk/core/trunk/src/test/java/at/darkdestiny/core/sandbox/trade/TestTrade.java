/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.sandbox.trade;

import at.darkdestiny.core.sandbox.trade.dto.TCapacityResult;
import at.darkdestiny.core.sandbox.trade.util.TestTradePlanetData;
import at.darkdestiny.core.sandbox.trade.dto.TPlanet;
import at.darkdestiny.core.sandbox.trade.util.TestResources;
import at.darkdestiny.core.sandbox.trade.util.TestTradeData;
import at.darkdestiny.framework.dao.DAOFactory;
import com.google.common.collect.Maps;
import java.util.HashMap;
import java.util.Map;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.BeforeClass;

/**
 *
 * @author Stefan
 */
public class TestTrade extends AbstractTestTrade{


    @BeforeClass
    public static void setupGlobal() {

    }

    @AfterClass
    public static void shutdownGlobal() {
        DAOFactory.setVirtualMode(false);
    }

    @Before
    public void setup() {
    }

    /**
     * Test with restriction on target planet
     */
    @Test
    public void OneRouteOneRessource_1() {

        TestResources res = new TestResources();

        long capacity = 150;

        res.addOutgoingPossibleByPlanet(1, 1000l);
        res.addOutgoingPossibleByPlanet(2, 10000l);

        res.addExpectedTransportedResource(1, 10l);

        //Startplanet
        TestTradeData testPlanet = new TestTradeData(startPlanet_id, capacity, res);

        //Next Planet       
        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 10l);

        res.addExpectedReceivedResource(1, 10l);

        testPlanet.addPlanet(new TestTradePlanetData(planetA_id, res));

        assertEquals(130l, TTradeProcessing.doIt(testPlanet.getPlanet(), TTradeProcessing.ETTransportType.TRADE));

        evaluate(testPlanet);
    }


    /**
     * Test with not enough capacity and limitation on resource 1
     */
    @Test
    public void OneRouteOneRessource_3() {

        TestResources res = new TestResources();

        Long capacity = 15000l;

        res.addOutgoingPossibleByPlanet(1, 7000l);
        res.addOutgoingPossibleByPlanet(2, 10000l);

        res.addExpectedTransportedResource(1, 7500l / 2);
        res.addExpectedTransportedResource(2, 7500l / 2);

        //Startplanet
        TestTradeData testPlanet = new TestTradeData(startPlanet_id, capacity, res);

        //Next Planet       
        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 10000000000l);
        res.addIncomingPossibleByPlanet(2, 10000000000l);

        res.addExpectedReceivedResource(1, 7500l / 2);
        res.addExpectedReceivedResource(2, 7500l / 2);

        testPlanet.addPlanet(new TestTradePlanetData(planetA_id, res));

        assertEquals(0l, TTradeProcessing.doIt(testPlanet.getPlanet(), TTradeProcessing.ETTransportType.TRADE));

        evaluate(testPlanet);
    }

    

    /**
     * 
     */
    @Test
    public void OneRouteOneRessource6() {

        TestResources res = new TestResources();

        Long capacity = 6000l;
        
        res.addOutgoingPossibleByPlanet(1, 200);
        res.addOutgoingPossibleByPlanet(2, 200);
        
        res.addExpectedTransportedResource(1, 200);
        res.addExpectedTransportedResource(2, 200);
        
        //Startplanet
        TestTradeData testPlanet = new TestTradeData(startPlanet_id, capacity, res);

        //Next Planet       
        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 50);
        res.addIncomingPossibleByPlanet(2, 300);
        res.addExpectedTransportedResource(1, 50);
        res.addExpectedTransportedResource(2, 150);
                
        testPlanet.addPlanet(new TestTradePlanetData(planetA_id, 5, res));        
        
        
        //Next Planet     
        res = new TestResources();
        
        res.addIncomingPossibleByPlanet(1, 300);
        res.addIncomingPossibleByPlanet(2, 50);
        res.addExpectedTransportedResource(1, 150);
        res.addExpectedTransportedResource(2, 50);

        testPlanet.addPlanet(new TestTradePlanetData(planetB_id, 10, res));

        TTradeProcessing.doIt(testPlanet.getPlanet(), TTradeProcessing.ETTransportType.TRADE);

        evaluate(testPlanet);
    }
}
