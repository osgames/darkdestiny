/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.sandbox.trade;

import at.darkdestiny.core.sandbox.trade.dto.TCapacityResult;
import at.darkdestiny.core.sandbox.trade.util.TestTradePlanetData;
import at.darkdestiny.core.sandbox.trade.dto.TPlanet;
import at.darkdestiny.core.sandbox.trade.util.TestResources;
import at.darkdestiny.core.sandbox.trade.util.TestTradeData;
import at.darkdestiny.framework.dao.DAOFactory;
import com.google.common.collect.Maps;
import java.util.HashMap;
import java.util.Map;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.BeforeClass;

/**
 *
 * @author Stefan
 */
public class TestBasicFunctions  extends AbstractTestTrade{


    @BeforeClass
    public static void setupGlobal() {

    }

    @AfterClass
    public static void shutdownGlobal() {
        DAOFactory.setVirtualMode(false);
    }

    @Before
    public void setup() {
    }


    /**
     * Checks if the findRoutesUsintThisResource method is working correctly
     *
     */
    @Test
    public void testFindRoutesUsingThisResource() {

        TestResources res = new TestResources();

        res.addOutgoingPossibleByPlanet(1, 123l);
        res.addOutgoingPossibleByPlanet(2, 123l);
        res.addOutgoingPossibleByPlanet(3, 123l);
        res.addOutgoingPossibleByPlanet(4, 123l);

        TestTradeData testData = new TestTradeData(startPlanet_id, 50l, res);

        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 123l);
        res.addIncomingPossibleByPlanet(2, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        testData.addPlanet(new TestTradePlanetData(planetA_id, res));

        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        testData.addPlanet(new TestTradePlanetData(planetB_id, res));

        res = new TestResources();

        res.addIncomingPossibleByPlanet(2, 123l);
        res.addIncomingPossibleByPlanet(3, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        testData.addPlanet(new TestTradePlanetData(planetC_id, res));

        assertEquals(2, TTradeProcessing.findRoutesUsingThisResource(testData.getPlanet(), 1));
        assertEquals(2, TTradeProcessing.findRoutesUsingThisResource(testData.getPlanet(), 2));
        assertEquals(1, TTradeProcessing.findRoutesUsingThisResource(testData.getPlanet(), 3));
        assertEquals(3, TTradeProcessing.findRoutesUsingThisResource(testData.getPlanet(), 4));
    }

    /**
     * Checks if the findRoutesUsintThisResource method is working correctly
     * with processed flag
     *
     */
    @Test
    public void testFindRoutesUsingThisResource2() {

        TestResources res = new TestResources();

        res.addOutgoingPossibleByPlanet(1, 123l);
        res.addOutgoingPossibleByPlanet(2, 123l);
        res.addOutgoingPossibleByPlanet(3, 123l);
        res.addOutgoingPossibleByPlanet(4, 123l);

        TestTradeData testData = new TestTradeData(startPlanet_id, 50l, res);
        //Next Planet
        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 123l);
        res.addIncomingPossibleByPlanet(2, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        TestTradePlanetData planet = new TestTradePlanetData(planetA_id, res);
        //Add already processed values
        planet.getPlanet().getProcessedResources().add(1);
        planet.getPlanet().getProcessedResources().add(2);

        testData.addPlanet(planet);

        //Next Planet
        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        testData.addPlanet(new TestTradePlanetData(planetB_id, res));

        //Next Planet
        res = new TestResources();

        res.addIncomingPossibleByPlanet(2, 123l);
        res.addIncomingPossibleByPlanet(3, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        planet = new TestTradePlanetData(planetC_id, res);

        planet.getPlanet().getProcessedResources().add(1);
        planet.getPlanet().getProcessedResources().add(2);
        planet.getPlanet().getProcessedResources().add(3);
        planet.getPlanet().getProcessedResources().add(4);

        testData.addPlanet(planet);

        assertEquals(1, TTradeProcessing.findRoutesUsingThisResource(testData.getPlanet(), 1));
        assertEquals(0, TTradeProcessing.findRoutesUsingThisResource(testData.getPlanet(), 2));
        assertEquals(0, TTradeProcessing.findRoutesUsingThisResource(testData.getPlanet(), 3));
        assertEquals(2, TTradeProcessing.findRoutesUsingThisResource(testData.getPlanet(), 4));
    }

    /**
     * Tests the Find Routes method
     *
     */
    @Test
    public void testFindRoutes() {

        TestResources res = new TestResources();

        res.addOutgoingPossibleByPlanet(1, 123l);
        res.addOutgoingPossibleByPlanet(2, 123l);
        res.addOutgoingPossibleByPlanet(3, 123l);
        res.addOutgoingPossibleByPlanet(4, 123l);

        TestTradeData testData = new TestTradeData(startPlanet_id, 50l, res);

        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 123l);
        res.addIncomingPossibleByPlanet(2, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        testData.addPlanet(new TestTradePlanetData(planetA_id, res));

        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        testData.addPlanet(new TestTradePlanetData(planetB_id, res));

        res = new TestResources();

        res.addIncomingPossibleByPlanet(2, 123l);
        res.addIncomingPossibleByPlanet(3, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        testData.addPlanet(new TestTradePlanetData(planetC_id, res));

        assertEquals(8, TTradeProcessing.findRoutes(testData.getPlanet()));
    }

    /**
     * Tests the Find Routes method with already processed routes
     *
     */
    @Test
    public void testFindRoutes2() {

        TestResources res = new TestResources();

        res.addOutgoingPossibleByPlanet(1, 123l);
        res.addOutgoingPossibleByPlanet(2, 123l);
        res.addOutgoingPossibleByPlanet(3, 123l);
        res.addOutgoingPossibleByPlanet(4, 123l);

        TestTradeData testData = new TestTradeData(startPlanet_id, 50l, res);
        //Next Planet
        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 123l);
        res.addIncomingPossibleByPlanet(2, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        TestTradePlanetData planet = new TestTradePlanetData(planetA_id, res);
        //Add already processed values
        planet.getPlanet().getProcessedResources().add(1);
        planet.getPlanet().getProcessedResources().add(2);

        testData.addPlanet(planet);

        //Next Planet
        res = new TestResources();

        res.addIncomingPossibleByPlanet(1, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        testData.addPlanet(new TestTradePlanetData(planetB_id, res));

        //Next Planet
        res = new TestResources();

        res.addIncomingPossibleByPlanet(2, 123l);
        res.addIncomingPossibleByPlanet(3, 123l);
        res.addIncomingPossibleByPlanet(4, 123l);

        planet = new TestTradePlanetData(planetC_id, res);

        planet.getPlanet().getProcessedResources().add(1);
        planet.getPlanet().getProcessedResources().add(2);
        planet.getPlanet().getProcessedResources().add(3);
        planet.getPlanet().getProcessedResources().add(4);

        testData.addPlanet(planet);

        assertEquals(3, TTradeProcessing.findRoutes(testData.getPlanet()));
    }

    @Test
    public void testCalculateCapacity() {

        long capacityPerRoute = 100;
        long amountResourceAvailablePerRoute = 90;
        long incomePossibleByPlanet = 30;

        long expectedValue = incomePossibleByPlanet;

        TCapacityResult result = TTradeProcessing.calculateCapacity(capacityPerRoute, amountResourceAvailablePerRoute, incomePossibleByPlanet);
        assertEquals(expectedValue, result.getCapacity());
        assertTrue(result.isCloseRoute());

        capacityPerRoute = 20;

        expectedValue = capacityPerRoute;

        result = TTradeProcessing.calculateCapacity(capacityPerRoute, amountResourceAvailablePerRoute, incomePossibleByPlanet);
        assertEquals(expectedValue, result.getCapacity());
        assertFalse(result.isCloseRoute());

        amountResourceAvailablePerRoute = 10;

        expectedValue = amountResourceAvailablePerRoute;

        result = TTradeProcessing.calculateCapacity(capacityPerRoute, amountResourceAvailablePerRoute, incomePossibleByPlanet);
        assertEquals(expectedValue, result.getCapacity());
        assertFalse(result.isCloseRoute());
    }

}
