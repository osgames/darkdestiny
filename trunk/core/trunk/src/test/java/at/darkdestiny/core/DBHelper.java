package at.darkdestiny.core;

import at.darkdestiny.core.enumeration.EGalaxyType;
import at.darkdestiny.core.enumeration.EPlanetDensity;
import at.darkdestiny.core.result.GalaxyCreationResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.access.DbConnect;
import com.googlecode.flyway.core.Flyway;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertTrue;
import org.junit.Before;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Admin
 */
public class DBHelper {

    public DBHelper() {
    }

    @Before
    public void setup() {
        try {
            Properties dbProperties = new Properties();
            dbProperties.load(DBHelper.class.getResourceAsStream("/db.properties"));
        } catch (IOException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

        DbConnect.getInstance(TestGameConfig.getInstance());


    }
}
