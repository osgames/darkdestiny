/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.dao.PlanetConstructionDAO;
import at.darkdestiny.core.dao.PlanetDAO;
import at.darkdestiny.core.dao.PlanetRessourceDAO;
import at.darkdestiny.core.dao.PlayerPlanetDAO;
import at.darkdestiny.core.dao.ProductionDAO;
import at.darkdestiny.core.dao.ShipDesignDAO;
import at.darkdestiny.core.dao.SystemDAO;
import at.darkdestiny.core.dao.TradeRouteDAO;
import at.darkdestiny.core.dao.TradeRouteDetailDAO;
import at.darkdestiny.core.dao.TradeRouteShipDAO;
import at.darkdestiny.core.dao.UserDAO;
import at.darkdestiny.core.dao.UserDataDAO;
import at.darkdestiny.core.databuffer.fleet.RelativeCoordinate;
import at.darkdestiny.core.enumeration.EAddLocation;
import at.darkdestiny.core.enumeration.EColonyType;
import at.darkdestiny.core.enumeration.ENoTransportReason;
import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.enumeration.ETradeRouteStatus;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.enumeration.EUserGender;
import at.darkdestiny.core.enumeration.EUserType;
import at.darkdestiny.core.model.Action;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Production;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.TradeRoute;
import at.darkdestiny.core.model.TradeRouteDetail;
import at.darkdestiny.core.model.TradeRouteShip;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.update.TransportProcessing;
import at.darkdestiny.core.update.UpdaterDataSet;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.dao.DAOFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Ignore;

/**
 *
 * @author Stefan
 */
@Ignore
public class TestTransport {

    private static final Logger log = LoggerFactory.getLogger(TestTransport.class);
    private static ArrayList<at.darkdestiny.core.model.System> selSysList;
    private static PlanetDAO pDAO;

    private static ArrayList<Planet> testPlanet = new ArrayList<Planet>();
    private static ArrayList<PlayerPlanet> testPlayerPlanet = new ArrayList<PlayerPlanet>();
    private static ArrayList<UserData> testUserData = new ArrayList<UserData>();
    private static ArrayList<Action> actionListPlaceHolder = new ArrayList<Action>();
    private static ArrayList<PlanetRessource> maxStockEntries = new ArrayList<PlanetRessource>();

    private static PlanetRessourceDAO prDAO = null;
    private static PlayerPlanetDAO ppDAO = null;
    private static UserDataDAO udDAO = null;
    private static TradeRouteDAO trDAO = null;
    private static TradeRouteDetailDAO trdDAO = null;
    private static ShipDesignDAO sdDAO = null;
    private static TradeRouteShipDAO trsDAO = null;
    private static PlanetConstructionDAO pcDAO = null;

    private static UpdaterDataSet uds = null;

    @BeforeClass
    public static void setupGlobal() {
        try {
            Properties dbProperties = new Properties();
            dbProperties.load(DBHelper.class.getResourceAsStream("/db.properties"));
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

        DbConnect.getInstance(TestGameConfig.getInstance());

        try {
            // Setup Systems
            SystemDAO sRealDAO = DAOFactory.get(SystemDAO.class);
            PlanetDAO pRealDAO = DAOFactory.get(PlanetDAO.class);

            ArrayList<at.darkdestiny.core.model.System> sysList = sRealDAO.findAll();
            ArrayList<Planet> planetList = pRealDAO.findAll();

            DAOFactory.setVirtualMode(true);

            pDAO = DAOFactory.get(PlanetDAO.class);
            prDAO = DAOFactory.get(PlanetRessourceDAO.class);
            SystemDAO sDAO = DAOFactory.get(SystemDAO.class);
            ppDAO = DAOFactory.get(PlayerPlanetDAO.class);
            udDAO = DAOFactory.get(UserDataDAO.class);
            trDAO = DAOFactory.get(TradeRouteDAO.class);
            trdDAO = DAOFactory.get(TradeRouteDetailDAO.class);
            sdDAO = DAOFactory.get(ShipDesignDAO.class);
            trsDAO = DAOFactory.get(TradeRouteShipDAO.class);
            pcDAO = DAOFactory.get(PlanetConstructionDAO.class);

            sDAO.insertAll(sysList);
            pDAO.insertAll(planetList);

            assertEquals(sDAO.findAll().size(), sRealDAO.findAll().size());

            // Get 5 systems set one system central and the other ones 10, 20, 30, 40 away
            selSysList = Lists.newArrayList();
            for (at.darkdestiny.core.model.System tmpSys : sDAO.findAll()) {
                // Check if system has at least one planet
                if (pDAO.findBySystemId(tmpSys.getId()).isEmpty()) {
                    continue;
                }

                selSysList.add(tmpSys);
                if (sysList.size() == 5) {
                    break;
                }
            }

            at.darkdestiny.core.model.System s = selSysList.get(0);
            s.setX(60);
            s.setY(60);
            s = sDAO.update(s);

            s = selSysList.get(1);
            s.setX(50);
            s.setY(60);
            s = sDAO.update(s);

            s = selSysList.get(2);
            s.setX(80);
            s.setY(60);
            s = sDAO.update(s);

            s = selSysList.get(3);
            s.setX(60);
            s.setY(30);
            s = sDAO.update(s);

            s = selSysList.get(4);
            s.setX(60);
            s.setY(100);
            s = sDAO.update(s);

            // Check distances
            int distance = (int) Math.round(selSysList.get(0).getAbsoluteCoordinate().distanceTo(selSysList.get(1).getAbsoluteCoordinate()));
            assertEquals(10, distance);

            distance = (int) Math.round(selSysList.get(0).getAbsoluteCoordinate().distanceTo(selSysList.get(2).getAbsoluteCoordinate()));
            assertEquals(20, distance);

            distance = (int) Math.round(selSysList.get(0).getAbsoluteCoordinate().distanceTo(selSysList.get(3).getAbsoluteCoordinate()));
            assertEquals(30, distance);

            distance = (int) Math.round(selSysList.get(0).getAbsoluteCoordinate().distanceTo(selSysList.get(4).getAbsoluteCoordinate()));
            assertEquals(40, distance);

            // We need only 1 user and will create all players planets on that user
            UserDAO uDAO = DAOFactory.get(UserDAO.class);
            User u = new User();
            u.setAcceptedUserAgreement(Boolean.TRUE);
            u.setActivationCode("");
            u.setActive(Boolean.TRUE);
            u.setAdmin(Boolean.TRUE);
            u.setBetaAdmin(Boolean.FALSE);
            u.setDeleteDate(0);
            u.setEmail("x.x@x.x");
            u.setGameName("TestBenutzer");
            u.setGender(EUserGender.MALE);
            u.setGuest(Boolean.FALSE);
            u.setIp("0.0.0.0");
            u.setJoinDate((long) 0);
            u.setLastUpdate(0);
            u.setLocale("DE-de");
            u.setLocked(0);
            u.setMulti(Boolean.FALSE);
            u.setPassword("");
            u.setPasswordSalt("");
            u.setPortalPassword("");
            u.setRegistrationToken("");
            u.setSystemAssigned(Boolean.TRUE);
            u.setTrial(Boolean.FALSE);
            u.setTrusted(Boolean.TRUE);
            u.setUserConfigured(Boolean.TRUE);
            u.setUserName("TestBenutzer");
            u.setUserType(EUserType.HUMAN);
            User testUser = uDAO.add(u);

            testUserData.add(createUserData(testUser.getId()));

            // Select one planet per System
            Planet testA = pDAO.findBySystemId(selSysList.get(0).getId()).get(0);
            Planet testB = pDAO.findBySystemId(selSysList.get(1).getId()).get(0);
            Planet testC = pDAO.findBySystemId(selSysList.get(2).getId()).get(0);
            Planet testD = pDAO.findBySystemId(selSysList.get(3).getId()).get(0);
            Planet testE = pDAO.findBySystemId(selSysList.get(4).getId()).get(0);

            testPlanet.add(testA);
            testPlanet.add(testB);
            testPlanet.add(testC);
            testPlanet.add(testD);
            testPlanet.add(testE);

            System.out.println("TestPlanet A id is " + testA.getId());
            System.out.println("TestPlanet B id is " + testB.getId());
            System.out.println("TestPlanet C id is " + testC.getId());
            System.out.println("TestPlanet D id is " + testD.getId());
            System.out.println("TestPlanet E id is " + testE.getId());

            // Set storage constructions on planet
            addConstruction(testA.getId(), Construction.ID_SMALLRESSOURCESTORAGE, 1);
            addConstruction(testB.getId(), Construction.ID_SMALLRESSOURCESTORAGE, 2);
            addConstruction(testC.getId(), Construction.ID_SMALLRESSOURCESTORAGE, 3);
            addConstruction(testD.getId(), Construction.ID_SMALLRESSOURCESTORAGE, 4);
            addConstruction(testE.getId(), Construction.ID_SMALLRESSOURCESTORAGE, 5);

            // Create Planets
            for (Planet p : testPlanet) {
                testPlayerPlanet.add(createPlayerPlanet(p.getId(),testUser.getId()));
            }
        } catch (Exception e) {
            System.out.println("ALARM!!!!");
        } finally {
            System.out.println("Ran");
        }
    }

    @AfterClass
    public static void shutdownGlobal() {
        DAOFactory.setVirtualMode(false);
    }

    @Before
    public void setup() {
        // Setup data for tests
        // Universe
        // User
        // Planets
        // Resources
        //
    }

    @Test
    public void OneRouteOneRessource1() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 1000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.IRON,0,0,false);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 50;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteOneRessource2() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 2000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.IRON,0,0,false);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 100;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteOneRessource3() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 3000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.IRON,0,0,false);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 150;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteOneRessourceReverse1() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 1000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.STEEL,0,0,true);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 50;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteOneRessourceReverse2() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 3000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.STEEL,0,0,true);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 150;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteBothDirections1() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 2000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd1 = createDetailEntry(tr.getId(),Ressource.IRON,0,0,false);
        TradeRouteDetail trd2 = createDetailEntry(tr.getId(),Ressource.STEEL,0,0,true);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 100;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd1 = trdDAO.getById(trd1.getId());
            trd2 = trdDAO.getById(trd2.getId());
            assertEquals(expectedTransportVolume,(int)trd1.getLastTransport());
            assertEquals(expectedTransportVolume,(int)trd2.getLastTransport());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteBothDirections2() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 2000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd1 = createDetailEntry(tr.getId(),Ressource.IRON,0,0,true);
        TradeRouteDetail trd2 = createDetailEntry(tr.getId(),Ressource.STEEL,0,0,false);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 0;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd1 = trdDAO.getById(trd1.getId());
            trd2 = trdDAO.getById(trd2.getId());
            assertEquals(expectedTransportVolume,(int)trd1.getLastTransport());
            assertEquals(expectedTransportVolume,(int)trd2.getLastTransport());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteMaxQtyPerTick() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 2000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.IRON,0,80,false);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 80;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteMaxQty1() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 2000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.IRON,75,0,false);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 75;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertNull("",trd);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteMaxQty2() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 1000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.IRON,51,0,false);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 50;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());

            tp.processRoutes(1);
            trd = trdDAO.getById(trd.getId());
            assertNull("",trd);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void OneRouteMaxQty3() {
        resetStoredQuantitiesAndShips();
        deleteRoutes();

        Planet planetStart = testPlanet.get(0);
        Planet planetTarget = testPlanet.get(1);
        int capacity = 1000;
        TradeRoute tr = createTradeRoute(testUserData.get(0).getUserId(),planetStart.getId(),planetTarget.getId());
        TradeRouteDetail trd = createDetailEntry(tr.getId(),Ressource.IRON,51,25,false);
        addShipToRoute(tr.getId(),capacity,1f);
        int expectedTransportVolume = 25;

        double distance = (new RelativeCoordinate(0,planetStart.getId())).distanceTo(new RelativeCoordinate(0,planetTarget.getId()));
        System.out.println("");
        System.out.println("Excpected Transport Qty: " + expectedTransportVolume + " (Distance: " + distance + " Capacity: " + capacity + ")");
        uds = generateUpdaterDataSet();

        // Run Test
        try {
            TransportProcessing tp = new TransportProcessing(uds);
            tp.processRoutes(1);

            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());

            tp.processRoutes(1);
            trd = trdDAO.getById(trd.getId());
            assertEquals(expectedTransportVolume,(int)trd.getLastTransport());

            tp.processRoutes(1);
            trd = trdDAO.getById(trd.getId());
            assertNull("",trd);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    private void addShipToRoute(int routeId, int shipCapacity, float speed) {
        ShipDesign sd = new ShipDesign();
        sd.setChassis(0);
        sd.setCrew(0);
        sd.setDesignTime(0l);
        sd.setHasTractorBeam(false);
        sd.setHyperSpeed(speed);
        sd.setIsConstruct(false);
        sd.setName("Testship");
        sd.setPrimaryTarget(0);
        sd.setRange(0);
        sd.setRessSpace(shipCapacity);
        sd.setSmallHangar(0);
        sd.setMediumHangar(0);
        sd.setLargeHangar(0);
        sd.setStandardSpeed(1f);
        sd.setTroopSpace(0);
        sd.setType(EShipType.TRANSPORT);
        sd = sdDAO.add(sd);

        TradeRouteShip trs = new TradeRouteShip();
        trs.setDesignId(sd.getId());
        trs.setNumber(1);
        trs.setShipType(TradeRouteShip.SHIPTYPE_NORMAL);
        trs.setTradeRouteId(routeId);
        trs.setAddLocation(EAddLocation.IGNORE);
        trsDAO.add(trs);
    }

    private TradeRoute createTradeRoute(int userId, int startPlanet, int targetPlanet) {
        System.out.println("StartPlanet " + startPlanet + " TargetPlanet " + targetPlanet);

        double distance = (new RelativeCoordinate(0,startPlanet)).distanceTo(new RelativeCoordinate(0,targetPlanet));

        TradeRoute tr = new TradeRoute();
        tr.setCapacity(0);
        tr.setDistance(distance);
        tr.setEfficiency(100d);
        tr.setStartPlanet(startPlanet);
        tr.setTargetPlanet(targetPlanet);
        tr.setTargetUserId(0);
        tr.setTradePostId(0);
        tr.setType(ETradeRouteType.TRANSPORT);
        tr.setUserId(userId);
        tr.setStatus(ETradeRouteStatus.ACTIVE);
        return trDAO.add(tr);
    }

    private TradeRouteDetail createDetailEntry(int tradeRouteId, int resource, int totalCap, int capPerTick, boolean reversed) {
        TradeRouteDetail trd = new TradeRouteDetail();
        trd.setCapReason(ENoTransportReason.NONE);
        trd.setDisabled(false);
        trd.setLastTransport(0);
        trd.setMaxDuration(0);
        trd.setMaxQty(totalCap);
        trd.setMaxQtyPerTick(capPerTick);
        trd.setQtyDelivered(0l);
        trd.setRessId(resource);
        trd.setReversed(reversed);
        trd.setRouteId(tradeRouteId);
        trd.setTimeStarted(0);
        return trdDAO.add(trd);
    }

    private static UserData createUserData(int userId) {
        UserData ud = new UserData();
        ud.setUserId(userId);
        ud.setCredits(0l);
        ud.setDescription("xy");
        ud.setDevelopementPoints(0d);
        ud.setPic(null);
        ud.setRating(0);
        ud.setTemporary(0);
        ud.setTitleId(0);
        ud.setWeeklyIncome(0l);
        return udDAO.add(ud);

    }

    private static PlayerPlanet createPlayerPlanet(int planetId, int userId) {
        PlayerPlanet pp = new PlayerPlanet();
        pp.setColonyType(EColonyType.DEFAULT);
        pp.setDismantle(false);
        pp.setGrowth(0f);
        pp.setGrowthStatus(0f);
        pp.setHomeSystem(false);
        pp.setInvisibleFlag(false);
        pp.setMigration(0);
        pp.setMoral(100);
        pp.setName("Testplanet #" + planetId);
        pp.setPlanetId(planetId);
        pp.setPopulation(0l);
        pp.setPriorityAgriculture(0);
        pp.setPriorityIndustry(0);
        pp.setPriorityResearch(0);
        pp.setProductionStatus(0f);
        pp.setResearchStatus(0f);
        pp.setSpecialGrowth(0f);
        pp.setSpecialPoints(0f);
        pp.setSpecialProduction(0f);
        pp.setSpecialResearch(0f);
        pp.setTax(0);
        pp.setUnrest(0d);
        pp.setUserId(userId);
        return ppDAO.add(pp);
    }

    private static synchronized void addConstruction(int planetId, int constructionId, int count) {
        PlanetConstructionDAO pcDAO = DAOFactory.get(PlanetConstructionDAO.class);

        PlanetConstruction pc = new PlanetConstruction();
        pc.setPlanetId(planetId);
        pc.setConstructionId(constructionId);
        pc.setNumber(count);
        pc.setIdle(0);

        System.out.println("Adding Resource Storage to Planet " + pc.getPlanetId());

        pcDAO.add(pc);
    }

    private void addRessource(int planetId, int ressourceId, int quantity) {
        PlanetRessource pr = new PlanetRessource();
        pr.setPlanetId(planetId);
        pr.setRessId(ressourceId);
        pr.setQty((long) quantity);
        pr.setType(EPlanetRessourceType.INSTORAGE);
        prDAO.add(pr);
    }

    private void addMaxStorage(UpdaterDataSet uds) {
        ProductionDAO prodDAO = DAOFactory.get(ProductionDAO.class);

        for (Planet p : testPlanet) {
            System.out.println("Check Planet " + p.getId());
            ArrayList<PlanetConstruction> pcList = pcDAO.findByPlanetId(p.getId());

            HashMap<Integer,Long> storageByRess = Maps.newHashMap();

            for (PlanetConstruction pc : pcList) {
                ArrayList<Production> pList = prodDAO.findProductionForConstructionId(pc.getConstructionId());
                for (Production prod : pList) {
                    if (prod.getDecinc() == Production.PROD_STORAGE) {
                        if (!storageByRess.containsKey(prod.getRessourceId())) {
                            storageByRess.put(prod.getRessourceId(),prod.getQuantity() * pc.getNumber());
                        } else {
                            storageByRess.put(prod.getRessourceId(), storageByRess.get(prod.getRessourceId()) + (prod.getQuantity() * pc.getNumber()));
                        }
                    }
                }
            }

            for (Map.Entry<Integer,Long> entry : storageByRess.entrySet()) {
                System.out.println("Set Resource ["+entry.getKey()+"] storage for Planet " + p.getId() + " to " + entry.getValue().intValue());
                uds.setMaxStorage(p.getId(), entry.getKey(), entry.getValue().intValue());
            }
        }
    }

    private void deleteRoutes() {
        trdDAO.removeAll();
        trDAO.removeAll();
        trsDAO.removeAll();
    }

    private void resetStoredQuantitiesAndShips() {
        sdDAO.removeAll();

        for (Planet p : testPlanet) {
            prDAO.removeAll(prDAO.findBy(p.getId(), EPlanetRessourceType.INSTORAGE));
        }

        // Set ressources
        addRessource(testPlanet.get(0).getId(), Ressource.IRON, 150);

        addRessource(testPlanet.get(1).getId(), Ressource.STEEL, 150);

        addRessource(testPlanet.get(2).getId(), Ressource.IRON, 150);
        addRessource(testPlanet.get(2).getId(), Ressource.STEEL, 150);

        addRessource(testPlanet.get(3).getId(), Ressource.TERKONIT, 150);

        addRessource(testPlanet.get(4).getId(), Ressource.IRON, 10);
        addRessource(testPlanet.get(4).getId(), Ressource.STEEL, 50);
    }

    private UpdaterDataSet generateUpdaterDataSet() {
        // UpdaterDataSet
        ArrayList<PlanetRessource> prList = Lists.newArrayList();
        for (Planet p : testPlanet) {
            prList.addAll(prDAO.findByPlanetId(p.getId()));
        }

        uds = new UpdaterDataSet(testPlayerPlanet,prList,testUserData,actionListPlaceHolder);
        addMaxStorage(uds);

        return uds;
    }

    private void importNecessaryTables() {
    }
}
