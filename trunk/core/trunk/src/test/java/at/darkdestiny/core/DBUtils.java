/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.builder.UserBuilder;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.model.UserSettings;
import at.darkdestiny.core.service.Service;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Admin
 */
public class DBUtils {

    public static User generateUser(String gameName, String userName, String password, String ip, String email) {

        User u = new User();
        u.setGameName(gameName);
        u.setUserName(userName);
        u.setPassword(password);
        u.setIp(ip);
        u.setEmail(email);

        return VirtualService.userDAO.add(u);
    }

    public static User generateTestUser() {


        User u = new UserBuilder()
                .setGameName("Test")
                .setUserName("Test")
                .setPassword("test")
                .setIp("127.0.0.1")
                .setEmail("test@test.at")
                .build();


        return VirtualService.userDAO.add(u);
    }

    public static UserSettings generateUserSettings(int userId){
        UserSettings us = new UserSettings();
        us.setUserId(userId);

        return VirtualService.userSettingsDAO.add(us);
    }

    public static UserData generateUserData(int userId){
        UserData ud = new UserData();
        ud.setUserId(userId);

        return VirtualService.userDataDAO.add(ud);
    }

    public static User generateFritz() {


        User u = new UserBuilder()
                .setGameName("Fritz")
                .setUserName("Fritzy")
                .setPassword("test")
                .setIp("127.0.0.1")
                .setEmail("Fritz@darkdestiny.at")
                .build();

        return VirtualService.userDAO.add(u);
    }

    public static User generateFrank(boolean withPlanet) {

        User u = new UserBuilder()
                .setGameName("Frank")
                .setUserName("Franky")
                .setPassword("test")
                .setIp("127.0.0.1")
                .setEmail("Franky@darkdestiny.at")
                .build();


        u = VirtualService.userDAO.add(u);

        if (withPlanet) {
            at.darkdestiny.core.model.System s = generateSystem();
            u.setSystemAssigned(true);
            u = VirtualService.userDAO.update(u);
            Planet p = generatePlanet(s.getId());
            PlayerPlanet pp = generatePlayerPlanet(p.getId(), u.getId());
            pp.setHomeSystem(true);
            VirtualService.playerPlanetDAO.update(pp);
        }

        return u;
    }

    public static User generateFrank() {

        return generateFrank(false);
    }

    public static PlayerPlanet generatePlayerPlanet(int planetId, int userId) {

        PlayerPlanet pp = new PlayerPlanet();
        pp.setPlanetId(planetId);
        pp.setUserId(userId);

        int size = VirtualService.playerPlanetDAO.findAll().size();
        VirtualService.playerPlanetDAO.add(pp);
        assertEquals(size + 1, VirtualService.playerPlanetDAO.findAll().size());
        return pp;

    }

    public static Planet generatePlanet(int systemId) {
        return generatePlanet(systemId, Planet.LANDTYPE_M);

    }

    public static Planet generatePlanet(int systemId, String landType) {

        Planet planet = new Planet();
        planet.setId(VirtualService.planetDAO.findLastPlanet());
        planet.setSystemId(systemId);
        planet.setLandType(landType);
        planet.setAvgTemp((int)(Math.random() * 40));
        planet.setDiameter(18000);
        planet.setOrbitLevel(1);

        int size = VirtualService.planetDAO.findAll().size();
        VirtualService.planetDAO.add(planet);
        assertEquals(size + 1, VirtualService.planetDAO.findAll().size());

        return planet;

    }

    public static at.darkdestiny.core.model.System generateSystem() {
        at.darkdestiny.core.model.System system = new at.darkdestiny.core.model.System();
        system.setX(1);
        system.setY(1);
        system.setId(VirtualService.systemDAO.findLastId());

        int size = VirtualService.systemDAO.findAll().size();
        VirtualService.systemDAO.add(system);
        assertEquals(size + 1, VirtualService.systemDAO.findAll().size());

        return system;
    }
}
