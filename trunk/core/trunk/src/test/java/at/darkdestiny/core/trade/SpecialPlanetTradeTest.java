/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.trade;

import at.darkdestiny.core.DBHelper;
import at.darkdestiny.core.enumeration.EBonusRange;
import at.darkdestiny.core.model.ActiveBonus;
import at.darkdestiny.core.model.Bonus;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.tools.graph.Graph;
import at.darkdestiny.framework.dao.DAOFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import at.darkdestiny.core.model.System;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.tools.graph.Edge;
import at.darkdestiny.core.tools.graph.Node;
import at.darkdestiny.core.trade.special.TradeGoodsNode;
import at.darkdestiny.core.trade.special.TransferringEntry;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Admin
 */
@Ignore
public class SpecialPlanetTradeTest extends DBHelper {

    SpecialPlanetTrade spt;

    public SpecialPlanetTradeTest() {
    }

    @Before
    public void setUp() {
        DAOFactory.setVirtualMode(true);
        spt = new SpecialPlanetTrade();
        // spt.setActiveBonusDAO((ActiveBonusDAO) DAOFactory.get(ActiveBonusDAO.class));
        Service.activeBonusDAO.removeAll();
        Service.systemDAO.removeAll();
        Service.planetDAO.removeAll();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of buildGraph method, of class SpecialPlanetTrade.
     */
    @Test
    public void testBuildGraph_1() {
        Map<Integer, Integer> megacitiesPerPlanet = Maps.newHashMap();
        List<ActiveBonus> abListIndustrial = Lists.newArrayList();
        List<ActiveBonus> abListTrade = Lists.newArrayList();

        megacitiesPerPlanet.put(2, 5);
        megacitiesPerPlanet.put(3, 5);

        abListIndustrial.add(new ActiveBonus(1, Bonus.DEVELOPMENT_INDUSTRIAL, EBonusRange.PLANET, 100));

        Map<Integer, Point> planetPositions = Maps.newHashMap();
        planetPositions.put(1, new Point(0, 100));
        planetPositions.put(2, new Point(100, 100));
        planetPositions.put(3, new Point(300, 300));

        generateTestData(megacitiesPerPlanet, abListIndustrial, abListTrade, planetPositions);
        Graph result = SpecialPlanetTrade.buildGraph(megacitiesPerPlanet, abListIndustrial, abListTrade);

        visualize(result);
        assertEquals(planetPositions.size(), result.getNodes().size());
    }

    /**
     * Test of buildGraph method, of class SpecialPlanetTrade.
     */
    @Test
    public void testBuildGraph_2() {
        Map<Integer, Integer> megacitiesPerPlanet = Maps.newHashMap();
        List<ActiveBonus> abListIndustrial = Lists.newArrayList();
        List<ActiveBonus> abListTrade = Lists.newArrayList();

        megacitiesPerPlanet.put(2, 5);
        megacitiesPerPlanet.put(3, 5);
        megacitiesPerPlanet.put(4, 5);
        megacitiesPerPlanet.put(5, 5);
        megacitiesPerPlanet.put(6, 5);

        abListIndustrial.add(new ActiveBonus(1, Bonus.DEVELOPMENT_INDUSTRIAL, EBonusRange.PLANET, 100));

        abListTrade.add(new ActiveBonus(7, Bonus.DEVELOPMENT_TRADE, EBonusRange.PLANET, 100));

        Map<Integer, Point> planetPositions = Maps.newHashMap();
        planetPositions.put(1, new Point(0, 100));
        planetPositions.put(2, new Point(100, 100));
        planetPositions.put(3, new Point(100, 100));
        planetPositions.put(4, new Point(100, 100));
        planetPositions.put(5, new Point(200, 200));
        planetPositions.put(6, new Point(300, 300));
        planetPositions.put(7, new Point(300, 300));

        generateTestData(megacitiesPerPlanet, abListIndustrial, abListTrade, planetPositions);

        Graph result = SpecialPlanetTrade.buildGraph(megacitiesPerPlanet, abListIndustrial, abListTrade, 300);

        visualize(result);

        assertEquals(planetPositions.size(), result.getNodes().size());
    }

    /**
     * Test of buildGraph method, of class SpecialPlanetTrade.
     */
    @Test
    public void testBuildGraph_3() {
        Map<Integer, Integer> megacitiesPerPlanet = Maps.newHashMap();
        List<ActiveBonus> abListIndustrial = Lists.newArrayList();
        List<ActiveBonus> abListTrade = Lists.newArrayList();

        megacitiesPerPlanet.put(2, 5);
        megacitiesPerPlanet.put(3, 5);
        megacitiesPerPlanet.put(4, 5);
        megacitiesPerPlanet.put(5, 5);
        megacitiesPerPlanet.put(6, 5);
        megacitiesPerPlanet.put(7, 5);

        abListIndustrial.add(new ActiveBonus(1, Bonus.DEVELOPMENT_INDUSTRIAL, EBonusRange.PLANET, 100));

        Map<Integer, Point> planetPositions = Maps.newHashMap();
        planetPositions.put(1, new Point(100, 100));
        planetPositions.put(2, new Point(200, 100));
        planetPositions.put(3, new Point(100, 200));
        planetPositions.put(4, new Point(200, 200));
        planetPositions.put(5, new Point(150, 250));
        planetPositions.put(6, new Point(350, 350));
        planetPositions.put(7, new Point(350, 250));

        generateTestData(megacitiesPerPlanet, abListIndustrial, abListTrade, planetPositions);
        Graph result = SpecialPlanetTrade.buildGraph(megacitiesPerPlanet, abListIndustrial, abListTrade, 100);

        visualize(result);
        assertEquals(planetPositions.size(), result.getNodes().size());
    }

    /**
     * Test of buildGraph method, of class SpecialPlanetTrade.
     */
    @Test
    public void testBuildGraph_random() {
        Map<Integer, Integer> megacitiesPerPlanet = Maps.newHashMap();
        List<ActiveBonus> abListIndustrial = Lists.newArrayList();
        List<ActiveBonus> abListTrade = Lists.newArrayList();

        Map<Integer, Point> planetPositions = Maps.newHashMap();

        for (int i = 0; i < 100; i++) {

            if (Math.random() * 10 > 2) {
                abListTrade.add(new ActiveBonus(i, Bonus.DEVELOPMENT_TRADE, EBonusRange.PLANET, 100));
            } else {
                if (Math.random() * 10 > 3) {
                    abListIndustrial.add(new ActiveBonus(i, Bonus.DEVELOPMENT_INDUSTRIAL, EBonusRange.PLANET, 100));

                } else {

                    megacitiesPerPlanet.put(i, (int)Math.ceil(Math.random() * 6d));

                }
            }
            int x = (int) (Math.random() * 1000);
            int y = (int) (Math.random() * 1000);
            planetPositions.put(i, new Point(x, y));
        }

        generateTestData(megacitiesPerPlanet, abListIndustrial, abListTrade, planetPositions);

        Graph result = SpecialPlanetTrade.buildGraph(megacitiesPerPlanet, abListIndustrial, abListTrade, 150);

        // Try to assign all industry production to receivers
        assignConsumersToProducers(result);

        visualize(result);
        assertEquals(planetPositions.size(), result.getNodes().size());
    }

    private void assignConsumersToProducers(Graph network) {
        List<Node> allNodes = network.getNodes();


        // HashMap<Node,ArrayList<LinkedList<Node>>> deliveryPaths = Maps.newHashMap();


        // Select a production Node, if found start broad-search for possible consumers
        for (Node n : allNodes) {
            TradeGoodsNode tgn = (TradeGoodsNode)n;
            int unassignedProduction = tgn.getProductionValue();

            if (tgn.getProductionValue() > 0) {
                int assignedQty = 0;

                do {
                    List<Node> processedNodes = new ArrayList<Node>();
                    List<Node> processingList = new ArrayList<Node>();
                    HashMap<Node,ArrayList<Node>> backTrack = Maps.newHashMap();

                    LinkedList<Node> path = new LinkedList<Node>();
                    processingList.add(tgn);
                    assignedQty = recursiveBroadSearch(processingList,processedNodes,backTrack,path,tgn);
                    unassignedProduction -= assignedQty;

                    java.lang.System.out.println("Just assigned " + assignedQty + " -- remaining " + unassignedProduction);

                    // path.add(0, tgn);
                    // java.lang.System.out.println("Add node " + tgn.toString() + "["+tgn.hashCode()+"] to path");
                    Node lastNode = null;

                    for (Node nPath : path) {
                        if (lastNode != null) {
                            Edge e = network.getEdge(lastNode, nPath);
                            if (e != null) {
                                e.setValue(e.getValue() + assignedQty);
                            }
                        }

                        lastNode = nPath;

                        TradeGoodsNode activePathNode = (TradeGoodsNode)nPath;
                        activePathNode.setOverrideColor(Color.WHITE);
                        TransferringEntry te = new TransferringEntry();
                        te.setPath(path);
                        te.setQuantity(assignedQty);
                        activePathNode.addRoute(te);
                    }
                } while ((unassignedProduction > 0) && (assignedQty > 0));
            }
        }
    }

    private int recursiveBroadSearch(List<Node> toProcess, List<Node> processedNodes, HashMap<Node,ArrayList<Node>> backTrack, LinkedList<Node> path, TradeGoodsNode startNode) {
        java.lang.System.out.println("Recursive search: TOPROCESS >> " + toProcess.size() + " PROCESSEDNODES >> " + processedNodes.size() + " BACKTRACKENTRIES >> " + backTrack.size());

        List<Node> toProcessNew = Lists.newArrayList();

        for (Node n : toProcess) {
            for (Object o : n.getEdges()) {
                Edge e = (Edge)o;
                TradeGoodsNode nextNode;

                if (e.getNode1() == n) {
                    nextNode = (TradeGoodsNode)e.getNode2();
                    if (processedNodes.contains(nextNode)) continue;
                } else {
                    nextNode = (TradeGoodsNode)e.getNode1();
                    if (processedNodes.contains(nextNode)) continue;
                }

                if (nextNode.getConsumptionValue() > 0) {
                    int qty = Math.min(nextNode.getFreeConsumption(), startNode.getFreeProduction());
                    if (qty > 0) {
                        java.lang.System.out.println("Found consuming node! (Transferring " + qty + " goods)");

                        path.add(n);
                        path.add(nextNode);

                        java.lang.System.out.println("Add node " + nextNode.toString() + "["+nextNode.hashCode()+"] to path");
                        java.lang.System.out.println("Add node " + n.toString() + "["+n.hashCode()+"] to path");

                        return qty;
                    }
                }

                processedNodes.add(nextNode);

                if (!nextNode.isTradePlanet()) {
                    continue;
                }

                toProcessNew.add(nextNode);

                ArrayList<Node> backTrackNodes = backTrack.get(nextNode);
                if (backTrackNodes == null) {
                    backTrackNodes = new ArrayList<Node>();
                    backTrackNodes.add(n);
                    backTrack.put(nextNode,backTrackNodes);
                } else {
                    backTrackNodes.add(n);
                }
            }
        }

        if (toProcessNew.isEmpty()) return -1;
        int result = recursiveBroadSearch(toProcessNew,processedNodes,backTrack,path,startNode);

        if (result > 0) {
            Node nbt = backTrack.get(path.get(0)).get(0);

            java.lang.System.out.println("Add node " + nbt.toString() + "["+nbt.hashCode()+"] to path");
            path.add(0, nbt);
            return result;
        }

        return -1;
    }

    private void visualize(Graph graph) {
        graph.visualize();

    }

    private void generateTestData(Map<Integer, Integer> megacitiesPerPlanet, List<ActiveBonus> abListIndustrial, List<ActiveBonus> abListTrade, Map<Integer, Point> planetPositions) {

        if (!abListIndustrial.isEmpty()) {
            Service.activeBonusDAO.insertAll((ArrayList<ActiveBonus>) abListIndustrial);
            assertEquals(abListIndustrial.size(), Service.activeBonusDAO.findByBonusId(Bonus.DEVELOPMENT_INDUSTRIAL).size());
        }
        if (!abListTrade.isEmpty()) {
            Service.activeBonusDAO.insertAll((ArrayList<ActiveBonus>) abListTrade);
            assertEquals(abListTrade.size(), Service.activeBonusDAO.findByBonusId(Bonus.DEVELOPMENT_TRADE).size());
        }

        Set<System> systems = Sets.newHashSet();

        int newSystemCount = 0;
        int sysId = 1;
        for (Map.Entry<Integer, Point> entry : planetPositions.entrySet()) {
            Point point = entry.getValue();
            System system = null;
            for (System s : systems) {
                if (s.getX().equals((int) point.getX()) && s.getY().equals((int) point.getY())) {
                    system = s;
                    break;
                }
            }

            if (system == null) {
                system = new System();
                system.setName("TestSystem : " + sysId);
                system.setX((int) point.getX());
                system.setY((int) point.getY());
                system.setId(sysId);
                systems.add(system);
                system = Service.systemDAO.add(system);

                system.setId(sysId);
                system = Service.systemDAO.update(system);
                java.lang.System.out.println("sysId : " + system.getId() + " at : " + system.getX() + " | " + system.getY());
                newSystemCount++;
                sysId++;

            }
            Planet planet = new Planet();
            planet.setId(entry.getKey());
            planet.setSystemId(system.getId());
            planet = Service.planetDAO.add(planet);
            java.lang.System.out.println("pID: " + planet.getId());

        }
        assertEquals(newSystemCount, Service.systemDAO.findAll().size());

    }

}
