package at.darkdestiny.core;



import at.darkdestiny.core.dao.GameDataDAO;
import at.darkdestiny.core.model.GameData;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.AbstractGameConfig;
import at.darkdestiny.util.DebugBuffer;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestGameConfig extends AbstractGameConfig {

    private static final Logger log = LoggerFactory.getLogger(TestGameConfig.class);

    private static TestGameConfig instance = null;

    public static TestGameConfig getInstance() {
        if (instance == null) {
            instance = new TestGameConfig();
        }
        return instance;
    }

    private TestGameConfig() {
        loadValues();
    }

    private void loadValues() {
        try {
            String additionalParameters = "?autoReconnect=true&dontTrackOpenResources=true";

            Properties gameProperties = new Properties();
            Properties dbProperties = new Properties();
            try {
                gameProperties.load(TestGameConfig.class.getResourceAsStream("/game.properties"));
                dbProperties.load(TestGameConfig.class.getResourceAsStream("/db.properties"));
            } catch (IOException ex) {
                ex.printStackTrace();
                java.util.logging.Logger.getLogger(TestGameConfig.class.getName()).log(Level.SEVERE, null, ex);
            }

            driver = loadValue(dbProperties, SetupProperties.DATABASE_DRIVERCLASS);
            database = loadValue(dbProperties, SetupProperties.DATABASE_URL) + additionalParameters;
            username = loadValue(dbProperties, SetupProperties.DATABASE_USER);
            password = loadValue(dbProperties, SetupProperties.DATABASE_PASS);

            gamehostURL = loadValue(gameProperties, SetupProperties.GAME_GAMEHOSTURL);
            webserviceURL = loadValue(gameProperties, SetupProperties.GAME_WEBHOSTURL);
            startURL = loadValue(gameProperties, SetupProperties.GAME_STARTURL);

            srcPathWeb = loadValue(gameProperties, SetupProperties.GAME_SRCPATH_WEB);
            srcPathClasses = loadValue(gameProperties, SetupProperties.GAME_SRCPATH_CLASSES);
            srcPathResources = loadValue(gameProperties, SetupProperties.GAME_SRCPATH_RESOURCES);
            assignDeletedEmpireToAI = loadBooleanValue(gameProperties, SetupProperties.GAME_OPTIONS_ASSIGN_DELETED_EMPIRE_TO_AI);

            String logLevel = loadValue(gameProperties, SetupProperties.GAME_LOGGING);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void reloadValues() {
        loadValues();
    }

    public String loadValue(Properties propertiesFile, String key) {
        try {
            return propertiesFile.getProperty(key.toString());
        } catch (Exception e) {
            DebugBuffer.error("Error loading setup properties key : " + key + " from file " + propertiesFile);
        }
        return null;
    }

    public String getDriverClassName() {
        return driver;
    }

    public java.lang.String getDatabase() {
        return database;
    }

    public java.lang.String getUsername() {
        return username;
    }

    public java.lang.String getPassword() {
        return password;
    }

    public java.lang.String getHostURL() {
        return TestGameConfig.getInstance().gamehostURL;
    }

    private void initDBValues() {
        GameData gd = null;
        try {
            GameDataDAO gdDAO = (GameDataDAO) DAOFactory.get(GameDataDAO.class);
            gd = (GameData) gdDAO.findAll().get(0);
            tickTime = gd.getTickTime();
            startTime = gd.getStartTime();
        } catch (Exception e) {
            DebugBuffer.error("Base configuration values not found in database!");
            throw new RuntimeException("Configuration not found in database!");
        }
    }

    /**
     * @return the duration of a tick in milliseconds!
     */
    public int getTicktime() {
        if (getInstance().tickTime < 0) {
            getInstance().initDBValues();
        }
        return getInstance().tickTime;
    }

    public long getStarttime() {
        if (getInstance().tickTime < 0) {
            getInstance().initDBValues();
        }
        return getInstance().startTime;
    }

    public String picPath() {
        return picPath;
    }

    public void rereadValues() {
        getInstance().tickTime = -1;
        getInstance().initDBValues();
    }

    /**
     * @return the startURL
     */
    public String getStartURL() {
        return startURL;
    }

    /**
     * @return the srcPath
     */
    public String getSrcPathWeb() {
        return srcPathWeb;
    }

    /**
     * @return the srcPath
     */
    public String getSrcPathClasses() {
        return srcPathClasses;
    }

    /**
     * @return the srcPath
     */
    public String getSrcPathResources() {
        return srcPathResources;
    }

    /**
     * @return the webserviceURL
     */
    public String getWebserviceURL() {
        return webserviceURL;
    }

    @Override
    public boolean isAssignDeletedEmpireToAI() {
       return isAssignDeletedEmpireToAI();
    }

    @Override
    public boolean isUpdateTickOnStartup() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isDeleteInactive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }        
}
