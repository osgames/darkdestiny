/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core.sandbox.trade;

import at.darkdestiny.core.sandbox.trade.util.TestTradePlanetData;
import at.darkdestiny.core.sandbox.trade.dto.TPlanet;
import at.darkdestiny.core.sandbox.trade.util.TestTradeData;
import java.util.Map;
import static org.junit.Assert.fail;

/**
 *
 * @author Stefan
 */
public abstract class AbstractTestTrade {

    public static final int startPlanet_id = 0;
    public static final int planetA_id = 1;
    public static final int planetB_id = 2;
    public static final int planetC_id = 3;


    /**
     * Method to evaluate if all expected resources where sent and received
     * @param planet
     */
    protected void evaluate(TestTradeData planet) {

        for (Map.Entry<Integer, Long> entry2 : planet.getExpectedTransportedResources().entrySet()) {
            boolean foundExpectedResource = false;
            for (Map.Entry<Integer, Long> entry : planet.getPlanet().getTransportedResources().entrySet()) {
                if (entry.getKey().equals(entry2.getKey())) {
                    foundExpectedResource = true;
                    if (!entry.getValue().equals(entry2.getValue())) {
                        fail("Comparison of transported resources failed: Resource[" + entry.getKey() + "] Expected [" + entry2.getValue() + "] Acutal["
                                + entry.getValue() + "]");
                    }
                }
            }
            if (!foundExpectedResource) {
                fail("Resource [" + entry2.getKey() + "] was expected to be [" + entry2.getValue() + "] but not found on transported resources");
            }
        }

        //Loop all testplanets (contains the expected transported resources)
        for (Map.Entry<Integer, TestTradePlanetData> testPlanet : planet.getTestPlanets().entrySet()) {
            //Loop all planets on the outgoing edges of the startplanet
            for (TPlanet tplanet : planet.getPlanet().getOutgoingEdges()) {
                //Check for the right planet
                if (testPlanet.getKey() != tplanet.getId()) {
                    continue;
                }
                //Check for the received resources on the outgoing planet
                for (Map.Entry<Integer, Long> expectedRess : testPlanet.getValue().getExpectedReceivedResources().entrySet()) {
                    boolean foundExpectedResource = false;
                    for (Map.Entry<Integer, Long> receivedRess : tplanet.getReceivedResources().entrySet()) {
                        //Loop the expected ressources
                        if (receivedRess.getKey().equals(expectedRess.getKey())) {
                            foundExpectedResource = true;
                            if (!receivedRess.getValue().equals(expectedRess.getValue())) {
                                fail("Comparison of transported resources failed: Resource[" + receivedRess.getKey() + "] Expected [" + expectedRess.getValue() + "] Acutal["
                                        + receivedRess.getValue() + "]");
                            }
                        }
                    }
                    if (!foundExpectedResource) {
                        fail("Resource [" + expectedRess.getKey() + "] was expected to be [" + expectedRess.getValue() + "] but not found on transported resources");
                    }
                }
            }
        }
    }


}
