/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.enumeration.EShipType;
import at.darkdestiny.core.model.Chassis;
import at.darkdestiny.core.model.DesignModule;
import at.darkdestiny.core.model.Module;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.service.Service;
import java.util.ArrayList;
/**
 *
 * @author Admin
 */
public class ShipDesignUtils{



    public static ShipDesign generateShipDesign_Fighter(int userId){


        ShipDesign sd = new ShipDesign();
        sd.setName(ML.getMLStr("shipdesign_lbl_newdesign", userId));
        sd.setUserId(userId);
        sd.setChassis(Chassis.ID_FIGHTER);
        sd.setDesignTime(System.currentTimeMillis());
        sd.setType(EShipType.BATTLE);
        sd.setBuildable(true);

        sd = VirtualService.shipDesignDAO.add(sd);

        VirtualService.designModuleDAO.insertAll(generateDesignModules_Fighter(userId, sd.getId()));


        return sd;
    }

    public static ShipDesign generateShipDesign(int userId, int chassisId, ArrayList<DesignModule> modules){


        ShipDesign sd = new ShipDesign();
        sd.setName(ML.getMLStr("shipdesign_lbl_newdesign", userId));
        sd.setUserId(userId);
        sd.setChassis(chassisId);
        sd.setDesignTime(System.currentTimeMillis());
        sd.setType(EShipType.BATTLE);
        sd.setBuildable(true);



        return sd;
    }
    public static ArrayList<DesignModule> generateDesignModules_Fighter(int userId, int shipDesignId){

        ArrayList<DesignModule> designModule = new ArrayList<DesignModule>();

        designModule.add(new DesignModule(shipDesignId, Module.MODULE_ENGINE_CHEMICALENGINE, false, 1));
        designModule.add(new DesignModule(shipDesignId, Module.MODULE_REACTOR_NUCLEARBATTERY, false, 2));
        designModule.add(new DesignModule(shipDesignId, Module.MODULE_WEAPON_LASERCANON, false, 2));
        designModule.add(new DesignModule(shipDesignId, Module.MODULE_CHASSIS_FIGHTER, false, 1));


        return designModule;
    }
}
