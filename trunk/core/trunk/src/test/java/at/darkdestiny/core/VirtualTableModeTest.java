/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.core;

import at.darkdestiny.core.dao.PlanetLogDAO;
import at.darkdestiny.core.enumeration.EPlanetLogType;
import at.darkdestiny.core.model.PlanetLog;
import at.darkdestiny.framework.dao.DAOFactory;
import java.util.ArrayList;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Stefan
 */
 @Ignore
public class VirtualTableModeTest extends DBHelper{
    private static PlanetLogDAO plDAO;
    private static final Logger log = LoggerFactory.getLogger(VirtualTableModeTest.class);

    @Test
    public void baseTest() {
        DAOFactory.setVirtualMode(true);

        plDAO = DAOFactory.get(PlanetLogDAO.class);

        //Add 10 times user 6 (10)
        for (int i=0;i<10;i++) {
            PlanetLog pl = new PlanetLog();
            pl.setPlanetId(1000 + i);
            pl.setTime(9999);
            pl.setType(EPlanetLogType.COLONIZED);
            pl.setUserId(7);
            plDAO.add(pl);
        }
        assertEquals(10, plDAO.findAll().size());

        //Add 10 times user 7 (10)
        ArrayList<PlanetLog> plBatchInsert = new ArrayList<PlanetLog>();
        for (int i=0;i<10;i++) {
            PlanetLog pl = new PlanetLog();
            pl.setPlanetId(1100 + i);
            pl.setTime(9999);
            pl.setType(EPlanetLogType.COLONIZED);
            pl.setUserId(6);
            plBatchInsert.add(pl);
        }

        plDAO.insertAll(plBatchInsert);

        assertEquals(20, plDAO.findAll().size());

        //Search user 6 (10)
        PlanetLog plSearch = new PlanetLog();
        plSearch.setUserId(6);
        ArrayList<PlanetLog> toCheck = plDAO.find(plSearch);

        assertEquals(10, toCheck.size());
        // All results should have userId 6
        for (PlanetLog plTmp : toCheck) {
            assertEquals(6, (int)plTmp.getUserId());
        }

        //Search user 7 (10)
        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);

        assertEquals(10, toCheck.size());
        // All results should have userId 7
        for (PlanetLog plTmp : toCheck) {
            assertEquals(7, (int)plTmp.getUserId());
        }

        //Remove 1 from user 7 (9)
        PlanetLog pl = toCheck.get(0);
        plDAO.remove(pl);

        // we should have 19 entries in total now
        assertEquals(19, plDAO.findAll().size());
        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);

        // 9 entries with userId 7 should be found now
        assertEquals(9, toCheck.size());
        // Each of the 9 entries should have userId 7
        for (PlanetLog plTmp : toCheck) {
            assertEquals(7, (int)plTmp.getUserId());
        }

        //Remove all from user 6 (0)
        plSearch = new PlanetLog();
        plSearch.setUserId(6);
        ArrayList<PlanetLog> toDelete = plDAO.find(plSearch);
        // We should have retrieved 10 entries with user Id 6
        assertEquals(10, toDelete.size());
        plDAO.removeAll(toDelete);

        // Therefore only 9 entries should exists after deleting all entries with userId 6
        assertEquals(9, plDAO.findAll().size());

        //Find all from user 7 (9)
        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);

        assertEquals(9, toCheck.size());
        for (PlanetLog plTmp : toCheck) {
            assertEquals(7, (int)plTmp.getUserId());
        }

        for (PlanetLog plTmp : plDAO.findAll()) {
            assertEquals(7, (int)plTmp.getUserId());
        }

        int count = 3;

        //Set 3 from 7 to 3
        // user 7 (6)
        // user 3 (3)
        for (PlanetLog plTmp : toCheck) {
            if (plTmp.getUserId() == 7) {
                plTmp.setUserId(3);
                count--;
                if (count == 0) break;
            }
        }

        plDAO.updateAll(toCheck);

        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);

        assertEquals(6, toCheck.size());

        plSearch = new PlanetLog();
        plSearch.setUserId(3);
        toCheck = plDAO.find(plSearch);

        assertEquals(3, toCheck.size());

        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);

        //Set 1 of 7 to 2
        // user 7 (5)
        // user 2 (1)
        PlanetLog plTest = toCheck.get(0);
        plTest.setUserId(2);
        plDAO.update(plTest);

        plSearch = new PlanetLog();
        plSearch.setUserId(7);
        toCheck = plDAO.find(plSearch);

        assertEquals(5, toCheck.size());

        plSearch = new PlanetLog();
        plSearch.setUserId(3);
        toCheck = plDAO.find(plSearch);

        assertEquals(3, toCheck.size());

        plSearch = new PlanetLog();
        plSearch.setUserId(2);
        toCheck = plDAO.find(plSearch);

        assertEquals(1, toCheck.size());

        plDAO.removeAll();

        assertTrue(plDAO.findAll().isEmpty());

        DAOFactory.setVirtualMode(false);
    }
}
