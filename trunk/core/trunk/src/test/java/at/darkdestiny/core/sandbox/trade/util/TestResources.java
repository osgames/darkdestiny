/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.core.sandbox.trade.util;

import at.darkdestiny.core.sandbox.trade.dto.TPlanet;
import com.google.common.collect.Maps;
import java.util.HashMap;

/**
 *
 * @author Feuerelfe
 */

    public class TestResources {

        private final HashMap<Integer, Long> expectedReceivedResources = Maps.newHashMap();
        private final HashMap<Integer, Long> expectedTransportedResources = Maps.newHashMap();
        
        private final HashMap<Integer, Long> outgoingPossibleByPlanet = Maps.newHashMap();
        private final HashMap<Integer, Long> incomePossibleByPlanet = Maps.newHashMap();

        
        public TestResources addExpectedReceivedResource(int resourceId, long amount){
            this.getExpectedReceivedResources().put(resourceId, amount);
            return this;
        }
        
        public TestResources addExpectedTransportedResource(int resourceId, long amount){
            this.getExpectedTransportedResources().put(resourceId, amount);
            return this;
        }
        
        public TestResources addOutgoingPossibleByPlanet(int resourceId, long amount){
            this.getOutgoingPossibleByPlanet().put(resourceId, amount);
            return this;
        }
        
        public TestResources addIncomingPossibleByPlanet(int resourceId, long amount){
            this.getIncomePossibleByPlanet().put(resourceId, amount);
            return this;
        }

    /**
     * @return the expectedReceivedResources
     */
    public HashMap<Integer, Long> getExpectedReceivedResources() {
        return expectedReceivedResources;
    }

    /**
     * @return the expectedTransportedResources
     */
    public HashMap<Integer, Long> getExpectedTransportedResources() {
        return expectedTransportedResources;
    }

    /**
     * @return the outgoingPossibleByPlanet
     */
    public HashMap<Integer, Long> getOutgoingPossibleByPlanet() {
        return outgoingPossibleByPlanet;
    }

    /**
     * @return the incomePossibleByPlanet
     */
    public HashMap<Integer, Long> getIncomePossibleByPlanet() {
        return incomePossibleByPlanet;
    }
    }