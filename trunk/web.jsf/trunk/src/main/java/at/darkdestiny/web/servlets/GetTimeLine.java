/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.servlets;

import at.darkdestiny.core.utilities.img.BackgroundPainter;
import at.darkdestiny.core.utilities.img.BaseRenderer;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.dto.TimeLineSelection;
import at.darkdestiny.web.servlets.util.TimeLine;
import at.darkdestiny.web.servlets.util.TimeLineRenderer;
import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class GetTimeLine extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("image/png");
        ServletOutputStream out = response.getOutputStream();

        try {
            List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();

            TimeLineSelection timeLineSelection = SessionContextHolder.getSessionContext().getTimeLineSelection();
            allItems.add(new TimeLineRenderer(timeLineSelection));

            BaseRenderer renderer = new BaseRenderer(TimeLine.WIDTH, TimeLine.HEIGHT);

            renderer.modifyImage(new BackgroundPainter(0x000000));

            for (IModifyImageFunction mif : allItems) {
                renderer.modifyImage(mif);
            }

            renderer.sendToUser(out);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
