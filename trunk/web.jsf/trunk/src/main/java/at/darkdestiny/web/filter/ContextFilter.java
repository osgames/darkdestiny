/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.filter;

import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.web.dto.SessionContext;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "ContextFilter")
@RequestScoped
public class ContextFilter extends AbstractFilter {

    @Override
    /*
     * Loads all necessary Values into the facesContext
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (!((HttpServletRequest) request).getRequestURL().toString().contains(".xhtml")) {
            chain.doFilter(request, response);
            return;
        }
        System.out.println("Do Filter : request [" + ((HttpServletRequest) request).getRequestURL().toString() + "]");

        SessionContext sessionContext = (SessionContext) ((HttpServletRequest) request).getSession().getAttribute(SessionContext.KEY);

        if (sessionContext != null) {
            if (sessionContext.getSelectedPlanetId() != null) {
                int selectionPlanetId = sessionContext.getSelectedPlanetId();
                try {
                    PlanetCalculation planetCalculation = new PlanetCalculation(selectionPlanetId);
                    sessionContext.setPlanetCalculationActPlanet(planetCalculation);
                } catch (Exception e) {
                    System.out.println("Error in ContextFilter for planetCalculation planetId[" + selectionPlanetId + "] " + e);
                }

            }
        }
        ((HttpServletRequest) request).getSession().setAttribute(SessionContext.KEY, sessionContext);

        chain.doFilter(request, response);
    }
}
