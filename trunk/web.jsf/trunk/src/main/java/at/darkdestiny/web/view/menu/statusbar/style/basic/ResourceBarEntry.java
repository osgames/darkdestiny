/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.service.TransportService;
import at.darkdestiny.web.BeanUtilities;
import java.util.HashMap;

/**
 *
 * @author Aion
 */
public class ResourceBarEntry extends BaseBar {

    protected static final int minLength = 0;
    protected static final int maxLength = 88;
    protected static final int maxLengthStorage = 88;
    protected HashMap<Integer, HashMap<Boolean, Long>> tradeBalances;
    protected HashMap<Integer, HashMap<Boolean, Long>> transportBalances;
    protected long increase;
    protected long decrease;
    protected long production;
    protected long consumption;
    protected long tradeIncoming;
    protected long tradeOutgoing;
    protected long transportIncoming;
    protected long transportOutgoing;
    protected final int resourceId;

    public ResourceBarEntry(int resourceId) {
        super();
        this.resourceId = resourceId;

        if (tradeBalances == null) {
            tradeBalances = TransportService.findShippingBalanceForPlanetSorted(sessionContext.getSelectedPlanetId(), ETradeRouteType.TRADE);
        }
        if (transportBalances == null) {
            transportBalances = TransportService.findShippingBalanceForPlanetSorted(sessionContext.getSelectedPlanetId(), ETradeRouteType.TRANSPORT);

        }
        production = planetCalculation.getPlanetCalcData().getProductionData().getRessProduction(resourceId);
        consumption = planetCalculation.getPlanetCalcData().getProductionData().getRessConsumption(resourceId);

        tradeIncoming = 0l;
        tradeOutgoing = 0l;
        if (tradeBalances.get(resourceId) != null) {
            if (transportBalances.get(resourceId).get(Boolean.TRUE) != null) {
                tradeIncoming = tradeBalances.get(resourceId).get(Boolean.TRUE);
            }
            if (transportBalances.get(resourceId).get(Boolean.FALSE) != null) {
                tradeOutgoing = tradeBalances.get(resourceId).get(Boolean.FALSE);
            }
        }

        transportIncoming = 0l;
        transportOutgoing = 0l;
        if (transportBalances.get(resourceId) != null) {
            if (transportBalances.get(resourceId).get(Boolean.TRUE) != null) {
                transportIncoming = transportBalances.get(resourceId).get(Boolean.TRUE);
            }
            if (transportBalances.get(resourceId).get(Boolean.FALSE) != null) {
                transportOutgoing = transportBalances.get(resourceId).get(Boolean.FALSE);
            }
        }
        increase = production + tradeIncoming + transportIncoming;

        decrease = consumption + tradeOutgoing;

    }

    public String getBackgroundImage() {
        return "pic/menu/statusbar/stb_ressources_background.png";
    }

    public String getIcon() {
        return Service.ressourceDAO.findById(resourceId).getImageLocation();
    }

    public String getStyleClazz() {
        return Service.ressourceDAO.findById(resourceId).getStyleClazz();
    }

    public long getRessStock() {

        return planetCalculation.getPlanetCalcData().getProductionData().getRessStock(resourceId);
    }

    public String getStockImage() {

        String stockImage = "pic/menu/storage";

        if (increase > decrease) {
            stockImage += "Plus";
        } else if (increase < decrease) {
            stockImage += "Minus";
        }

        stockImage += ".png";

        return stockImage;
    }

    public int getBarOffset(int width) {

        int offset = -44;
        return offset += width;

    }

    public String getStockLeft() {

        int offset = getBarOffset(getStockWidthValue());
        return offset + "px";
    }

    public String getStockWidth() {
        return getStockWidthValue() + "px";
    }

    public int getStockWidthValue() {
        return calculateStorageBarLength(minLength, maxLength, this.planetCalculation.getPlanetCalcData().getProductionData().getRessStock(resourceId),
                this.planetCalculation.getPlanetCalcData().getProductionData().getRessMaxStock(resourceId));
    }

    public int calculateStorageBarLength(int minLength, int maxLength, long stock, long capacity) {
        if (capacity > 0) {
            double multiplicator = ((double) stock / (double) capacity);
            minLength += (int) Math.round(multiplicator * maxLength);
        }
        return Math.min(minLength, maxLength);
    }

    public double getStockPercentage() {
        if (planetCalculation.getPlanetCalcData().getProductionData().getRessStock(resourceId) > 0) {
            double stock = planetCalculation.getPlanetCalcData().getProductionData().getRessStock(resourceId);
            double maxStock = planetCalculation.getPlanetCalcData().getProductionData().getRessMaxStock(resourceId);
            double percentage = stock / maxStock;
            return (Math.round(percentage * 100d));
        } else {
            return 0d;
        }
    }

    public long getStockMax() {
        return planetCalculation.getPlanetCalcData().getProductionData().getRessMaxStock(resourceId);
    }

    public String getResourceName() {
        return Service.ressourceDAO.findById(resourceId).getName();
    }

    public boolean isShowTransport() {
        return (transportIncoming != 0 || transportOutgoing != 0);
    }

    public boolean isShowProduction() {
        return (production != 0 || consumption != 0);
    }

    public boolean isShowResource() {
        return isShowProduction() || isShowTrade() || isShowTransport() || planetCalculation.getPlanetCalcData().getProductionData().getRessStock(resourceId) > 0;
    }

    public boolean isShowTrade() {
        return (tradeIncoming != 0 || tradeOutgoing != 0);
    }

    /**
     * @return the production
     */
    public long getProduction() {
        return production;
    }

    /**
     * @return the consumption
     */
    public long getConsumption() {
        return consumption;
    }

    /**
     * @return the tradeIncoming
     */
    public long getTradeIncoming() {
        return tradeIncoming;
    }

    /**
     * @return the tradeOutgoing
     */
    public long getTradeOutgoing() {
        return tradeOutgoing;
    }

    /**
     * @return the transportIncoming
     */
    public long getTransportIncoming() {
        return transportIncoming;
    }

    /**
     * @return the transportOutgoing
     */
    public long getTransportOutgoing() {
        return transportOutgoing;
    }
}
