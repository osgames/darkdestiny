/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean;

import at.darkdestiny.core.result.BaseResult;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author LAZYPAD
 */
public class MessageContext {

    private ArrayList<BaseResult> messages = Lists.newArrayList();
    private static MessageContext instance = new MessageContext();

    private MessageContext() {
    }

    public static MessageContext getCurrentInstance() {
        return instance;
    }

    /**
     * @return the messages
     */
    public ArrayList<BaseResult> getMessages() {
        return messages;
    }

    /**
     * @return the messages
     */
    public void clearList() {
        messages.clear();
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(ArrayList<BaseResult> messages) {
        this.messages = messages;
    }

    public void addMessage(BaseResult message) {
        this.messages.add(message);
    }
}
