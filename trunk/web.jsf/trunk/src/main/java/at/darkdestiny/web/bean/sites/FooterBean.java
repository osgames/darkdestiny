/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.utilities.img.BackgroundPainter;
import at.darkdestiny.core.utilities.img.BaseRenderer;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.TimeLineSelection;
import at.darkdestiny.web.servlets.util.TimeLine;
import at.darkdestiny.web.servlets.util.TimeLineRenderer;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "FooterBean")
@SessionScoped
public class FooterBean implements Serializable {



    public String getCurrentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(GameUtilities.getDate().getTime());
    }

    public void backwards(){
        TimeLineSelection timeLineSelection = SessionContextHolder.getSessionContext().getTimeLineSelection();
        Calendar c = timeLineSelection.getSelectedDate();
        c.add(Calendar.YEAR, -25);
        timeLineSelection.setSelectedDate(c);
    }

    public void forward(){
        TimeLineSelection timeLineSelection = SessionContextHolder.getSessionContext().getTimeLineSelection();
        Calendar c = timeLineSelection.getSelectedDate();
        c.add(Calendar.YEAR, +25);
        timeLineSelection.setSelectedDate(c);

    }
    public void zoomIn(){
        TimeLineSelection timeLineSelection = SessionContextHolder.getSessionContext().getTimeLineSelection();

        int yearsToDisplay = timeLineSelection.getYearsToDisplay() - TimeLineSelection.TIMELINE_WIDTH_ZOOM_INTERVALL;
        yearsToDisplay = Math.max(TimeLineSelection.YEAR_ZOOM_MAX, yearsToDisplay);
        timeLineSelection.setYearsToDisplay(yearsToDisplay);

    }
    public void zoomOut(){

        TimeLineSelection timeLineSelection = SessionContextHolder.getSessionContext().getTimeLineSelection();
        int yearsToDisplay = timeLineSelection.getYearsToDisplay() + TimeLineSelection.TIMELINE_WIDTH_ZOOM_INTERVALL;
        yearsToDisplay = Math.min(TimeLineSelection.YEAR_ZOOM_MIN, yearsToDisplay);
        timeLineSelection.setYearsToDisplay(yearsToDisplay);

    }

    public void timeline_1500(OutputStream out, Object data) throws IOException {

        createTimeline(out, data, 1500);
    }

    public void timeline_1000(OutputStream out, Object data) throws IOException {

        createTimeline(out, data, 1000);
    }
    public void timeline_650(OutputStream out, Object data) throws IOException {

        createTimeline(out, data, 650);


    }
    public void timeline_400(OutputStream out, Object data) throws IOException {

        createTimeline(out, data, 400);


    }
    public void timeline_250(OutputStream out, Object data) throws IOException {

        createTimeline(out, data, 250);


    }
    private void createTimeline(OutputStream out, Object data, int width) throws IOException {

        try {
            List<IModifyImageFunction> allItems = new LinkedList<IModifyImageFunction>();

            TimeLineSelection timeLineSelection = SessionContextHolder.getSessionContext().getTimeLineSelection();
            TimeLineRenderer tlRenderer = new TimeLineRenderer(timeLineSelection);
            tlRenderer.setWidth(width);
            allItems.add(tlRenderer);

            BaseRenderer renderer = new BaseRenderer(width, TimeLine.HEIGHT);

            renderer.modifyImage(new BackgroundPainter(0x000000));

            for (IModifyImageFunction mif : allItems) {
                renderer.modifyImage(mif);
            }

            renderer.sendToUser(out);
        } finally {
            out.close();
        }

    }
}
