/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.enumerations;

/**
 *
 * @author LAZYPAD
 */
public enum EMessageAction {

    ACTION_DELETE_SELECTED("messages_opt_deleteselected"),
    ACTION_MOVE_SELECTED("messages_opt_moveselected"),
    ACTION_DELETE_ALL("messages_opt_deleteall"),
    ACTION_DELETE_SYSTEM("messages_opt_deletesystem"),
    ACTION_DELETE_PLAYER("messages_opt_deleteplayer"),
    ACTION_DELETE_ALLIANCE("messages_opt_deletealliance");

    private EMessageAction(final String action) {
        this.action = action;
    }
    private final String action;

    @Override
    public String toString() {
        return action;
    }
}

