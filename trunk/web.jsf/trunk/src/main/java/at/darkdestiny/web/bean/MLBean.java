package at.darkdestiny.web.bean;

import at.darkdestiny.core.ML_NEW;
import at.darkdestiny.core.language.LANG;
import java.io.Serializable;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.el.ELContextEvent;
import javax.el.ELContextListener;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "MLBean")
@SessionScoped
public class MLBean implements Serializable {



    private LANG lang = new LANG();

    /**
     * @return the lang
     */
    public LANG getLang() {
        return lang;
    }

    public String getML(String constant) {
        return getML(constant, "");
    }

    public String getMLTitle(String constant) {
        return getML("title_prefix") + getML(constant, "");
    }

    public String getMLFromPropertiesFile(String propertiesFile, String constant) {
        return ML_NEW.getMLStr(propertiesFile, constant, getLocale());
    }

    public static Locale getLocale() {
        ServiceBean serviceBean = (ServiceBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("ServiceBean");
        if (serviceBean != null && serviceBean.isLocaleSetManually()) {
            return serviceBean.getLocale();
        } else {
            Locale locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
            return locale;
        }
    }

    /*
     * As(s) jsf seems to have problems to pass a list of arguments please
     * specify the params seperated by a % 'sign'
     *
     * E.g.: %username%logincount
     *
     */
    public String getML(String constant, String paramsString) {
        return ML_NEW.getMLStr(constant, getLocale(), (Object[]) paramsString.split(","));
    }
}
