/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.servlets;

import at.darkdestiny.util.DebugBuffer;
import com.keypoint.PngEncoder;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author Aion
 */
public class ChartViewer extends HttpServlet {

    public void init() throws ServletException {
    }

    //Process the HTTP Get request
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // get the chart from session
        try {
            HttpSession session = request.getSession();
            String chartAttributeName = request.getParameter("chartAttributeName");
            String xSizeStr = request.getParameter("xSize");
            String ySizeStr = request.getParameter("ySize");

            int xSize = 640;
            int ySize = 400;

            if (chartAttributeName == null) chartAttributeName = "chart";
            if (xSizeStr != null) xSize = Integer.parseInt(xSizeStr);
            if (ySizeStr != null) ySize = Integer.parseInt(ySizeStr);

            JFreeChart chart = (JFreeChart) session.getAttribute(chartAttributeName);

            BufferedImage chartImage = null;
            if (chart == null) {
                chartImage = new BufferedImage(640, 400, BufferedImage.TYPE_INT_RGB);
                Graphics g = chartImage.getGraphics();
                g.setColor(Color.BLACK);
                g.drawRect(0, 0, 640, 400);
                g.setColor(Color.WHITE);
                g.drawString("Keine Daten verf�gbar", 280, 190);
            } else {
                chartImage = chart.createBufferedImage(xSize, ySize, null);
            }

            // set the content type so the browser can see this as a picture
            response.setContentType("image/png");

            // send the picture
            PngEncoder encoder = new PngEncoder(chartImage, false, 0, 9);
            response.getOutputStream().write(encoder.pngEncode());
            session.removeAttribute(chartAttributeName);
        } catch (Exception e) {
            DebugBuffer.writeStackTrace("Error in Chart Viewer - Servlet", e);
        }

    }

    //Process the HTTP Post request
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    //Process the HTTP Put request
    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    //Clean up resources
    public void destroy() {
    }
}
