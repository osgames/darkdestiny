/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view;

import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.web.view.management.EconomyValues;
import at.darkdestiny.web.view.management.PopulationValues;
import at.darkdestiny.web.view.management.StockValues;

/**
 *
 * @author Admin
 */
public class ManagementViewData implements ViewData {

    private final PopulationValues populationValues;
    private final EconomyValues economyValues;
    private final StockValues stockValues;
    private boolean dismantle;

    public ManagementViewData(PopulationValues populationValues, EconomyValues economyValues, StockValues stockValues, boolean dismantle) {
        this.populationValues = populationValues;
        this.economyValues = economyValues;
        this.stockValues = stockValues;
        this.dismantle = dismantle;
    }

    /**
     * @return the populationValues
     */
    public PopulationValues getPopulationValues() {
        return populationValues;
    }

    /**
     * @return the economyValues
     */
    public EconomyValues getEconomyValues() {
        return economyValues;
    }

    /**
     * @return the stockValues
     */
    public StockValues getStockValues() {
        return stockValues;
    }


    /**
     * @return the dismantle
     */
    public boolean isDismantle() {
        return dismantle;
    }

    /**
     * @param dismantle the dismantle to set
     */
    public void setDismantle(boolean dismantle) {
        this.dismantle = !this.dismantle;
    }

}
