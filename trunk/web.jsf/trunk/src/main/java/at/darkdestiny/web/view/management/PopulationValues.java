/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.management;

import at.darkdestiny.web.view.ViewData;

/**
 *
 * @author Admin
 */
public class PopulationValues implements ViewData {

    private final long population;
    private final double populationGrowth;
    private final long migration;

    private final double moral;
    private final int moralBonus;
    private final int moralFood;
    private final int moralTaxes;

    private final double criminality;
    private final int loyality;
    private final double riots;

    public PopulationValues(long population, double populationGrowth, long migration, double moral, int moralBonus, int moralFood, int moralTaxes, int loyality, double criminality, double riots) {
        this.population = population;
        this.populationGrowth = populationGrowth;
        this.migration = migration;
        this.moral = moral;
        this.moralBonus = moralBonus;
        this.moralFood = moralFood;
        this.moralTaxes = moralTaxes;
        this.criminality = criminality;
        this.loyality = loyality;
        this.riots = riots;
    }

    /**
     * @return the population
     */
    public long getPopulation() {
        return population;
    }

    /**
     * @return the populationGrowth
     */
    public double getPopulationGrowth() {
        return populationGrowth;
    }

    /**
     * @return the migration
     */
    public long getMigration() {
        return migration;
    }

    /**
     * @return the moral
     */
    public double getMoral() {
        return moral;
    }

    /**
     * @return the moralBonus
     */
    public int getMoralBonus() {
        return moralBonus;
    }

    /**
     * @return the moralFood
     */
    public int getMoralFood() {
        return moralFood;
    }

    /**
     * @return the moralTaxes
     */
    public int getMoralTaxes() {
        return moralTaxes;
    }

    /**
     * @return the criminality
     */
    public double getCriminality() {
        return criminality;
    }

    /**
     * @return the loyality
     */
    public int getLoyality() {
        return loyality;
    }

    /**
     * @return the riots
     */
    public double getRiots() {
        return riots;
    }

}
