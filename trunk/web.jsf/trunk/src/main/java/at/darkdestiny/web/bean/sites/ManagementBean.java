/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.enumeration.EPlanetRessourceType;
import at.darkdestiny.core.language.LANG2;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlanetRessource;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.planetcalc.EconomyChart;
import at.darkdestiny.core.planetcalc.ExtPlanetCalcResult;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.ManagementService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.BeanUtilities;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.view.ManagementViewData;
import at.darkdestiny.web.view.management.EconomyValues;
import at.darkdestiny.web.view.management.PopulationValues;
import at.darkdestiny.web.view.management.ResourceValue;
import at.darkdestiny.web.view.management.StockValues;
import com.google.common.collect.Lists;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "ManagementBean")
@RequestScoped
public class ManagementBean implements JsfBean {

    private ManagementViewData viewData;
    private PlayerPlanet playerPlanet = Service.playerPlanetDAO.findByPlanetId(SessionContextHolder.getSessionContext().getSelectedPlanetId());

    private String planetName;

    public ManagementBean() {

        planetName = playerPlanet.getName();
    }

    public void updateTaxes() {

        playerPlanet.setTax(this.viewData.getEconomyValues().getTaxes());
        Service.playerPlanetDAO.update(playerPlanet);
        calcViewData();

    }

    public void updatePlayerPlanet(){

        System.out.println("Updating player planet");
        playerPlanet.setDismantle(viewData.isDismantle());
        Service.playerPlanetDAO.update(playerPlanet);
        calcViewData();
    }

    public void updatePriorities() {
        int prioAgriculture = viewData.getEconomyValues().getPriorityAgriculture();
        int prioIndustry = viewData.getEconomyValues().getPriorityIndustry();
        int prioResearch = viewData.getEconomyValues().getPriorityResearch();

        playerPlanet.setPriorityAgriculture(prioAgriculture);
        playerPlanet.setPriorityIndustry(prioIndustry);
        playerPlanet.setPriorityResearch(prioResearch);

        Service.playerPlanetDAO.update(playerPlanet);
    }

    public void changePlanetName() {
        playerPlanet.setName(planetName);
        Service.playerPlanetDAO.update(playerPlanet);
    }

    public void updateStockValues() {
        System.out.println("Trying to update stock values");
        for (ResourceValue resourceValue : this.viewData.getStockValues().getResourceValues()) {
            updateStockValue(EPlanetRessourceType.MINIMUM_PRODUCTION, resourceValue.getResourceId(), resourceValue.getMinProductionStock());
            updateStockValue(EPlanetRessourceType.MINIMUM_TRADE, resourceValue.getResourceId(), resourceValue.getMinTradeStock());
            updateStockValue(EPlanetRessourceType.MINIMUM_TRANSPORT, resourceValue.getResourceId(), resourceValue.getMinTransportStock());
        }
        System.out.println("Done");
    }

    private void updateStockValue(EPlanetRessourceType type, int resourceId, long value) {

        PlanetRessource minProd = Service.planetRessourceDAO.findBy(playerPlanet.getPlanetId(),
                resourceId, type);
        if (minProd == null) {
            minProd = new PlanetRessource(playerPlanet.getPlanetId(), resourceId, type, value);
            Service.planetRessourceDAO.add(minProd);
        } else {
            minProd.setQty(value);
            Service.planetRessourceDAO.update(minProd);
        }
    }

    @Override
    public ManagementViewData getViewData() {

        if (viewData == null) {
            calcViewData();
        }

        return viewData;
    }

    private void calcViewData() {
        int planetId = SessionContextHolder.getSessionContext().getSelectedPlanetId();
        PlayerPlanet playerPlanet = Service.playerPlanetDAO.findByPlanetId(SessionContextHolder.getSessionContext().getSelectedPlanetId());

        long population = playerPlanet.getPopulation();
        long migration = playerPlanet.getMigration();

        double absGrowth = (playerPlanet.getPopulation() / 100d * playerPlanet.getGrowth()) / 360d;

        PlanetCalculation pc = null;
        try {
            pc = new PlanetCalculation(playerPlanet);
        } catch (Exception ex) {
            Logger.getLogger(ManagementBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();

        int moral = epcr.getMorale();
        int moralBonus = epcr.getMoraleBonus();
        int moralFood = epcr.getMoralFood();
        int moralTaxes = epcr.getMoraleBonusTax();
        BigDecimal loyality = epcr.getLoyalityValue();
        double criminality = epcr.getCriminalityValue();
        double riots = epcr.getRiotValue();

        PopulationValues populationValues = new PopulationValues(population, absGrowth, migration, moral, moralBonus, moralFood, moralTaxes, loyality.intValue(), criminality, riots);

        double efficiencyIndustry = epcr.getEffIndustry() * 100d;
        double efficiencyAgriculture = epcr.getEffAgriculture() * 100d;
        double efficiencyResearch = epcr.getEffResearch() * 100d;

        int taxes = playerPlanet.getTax();
        int baseTaxIncome = epcr.getBaseTaxIncome();
        int taxRiotDiscount = epcr.getTaxRiotDiscount();
        int taxCrimDiscount = epcr.getTaxRiotDiscount();
        int taxTotal = epcr.getTaxIncome();

        StockValues stockValues = new StockValues();
        ArrayList<Ressource> resources = ManagementService.getMinStockRessources(planetId,
                pc.getPlanetCalcData().getProductionData());
        for (Ressource resource : resources) {
            long minProductionStock = ManagementService.findMinStock(planetId, resource.getId(), EPlanetRessourceType.MINIMUM_PRODUCTION);
            long minTransportStock = ManagementService.findMinStock(planetId, resource.getId(), EPlanetRessourceType.MINIMUM_TRANSPORT);
            long minTradeStock = ManagementService.findMinStock(planetId, resource.getId(), EPlanetRessourceType.MINIMUM_TRADE);

            stockValues.addResourceValue(resource.getId(), resource.getImageLocation(), minProductionStock, minTransportStock, minTradeStock);

        }

        EconomyValues economyValues = new EconomyValues(playerPlanet.getPriorityIndustry(), playerPlanet.getPriorityResearch(),
                playerPlanet.getPriorityAgriculture(), efficiencyIndustry, efficiencyResearch, efficiencyAgriculture,
                taxes, baseTaxIncome, taxRiotDiscount, taxCrimDiscount, taxTotal);
        this.viewData = new ManagementViewData(populationValues, economyValues, stockValues, this.playerPlanet.getDismantle());
    }

    public ArrayList<SelectItem> getPriorityOptions() {
        ArrayList<SelectItem> result = Lists.newArrayList();

        result.add(new SelectItem(PlayerPlanet.PRIORITY_HIGH, BeanUtilities.getMLStr(LANG2.SITES.MANAGEMENT.ECONOMY.PRIORITY.OPTION.HIGH)));
        result.add(new SelectItem(PlayerPlanet.PRIORITY_MEDIUM, BeanUtilities.getMLStr(LANG2.SITES.MANAGEMENT.ECONOMY.PRIORITY.OPTION.MEDIUM)));
        result.add(new SelectItem(PlayerPlanet.PRIORITY_LOW, BeanUtilities.getMLStr(LANG2.SITES.MANAGEMENT.ECONOMY.PRIORITY.OPTION.LOW)));
        return result;

    }

    public void surfaceChart(OutputStream out, Object data) throws IOException {

        PlanetCalculation pc = SessionContextHolder.getSessionContext().getPlanetCalculationActPlanet();
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        EconomyChart ec = new EconomyChart(epcr, SessionContextHolder.getSessionContext().getUser().getId());

        JFreeChart chart1 = ec.getSurfaceChart(false);

        ChartRenderingInfo info = new ChartRenderingInfo();
        // populate the info
        if (chart1 != null) {
            BufferedImage image = chart1.createBufferedImage(400, 300, info);

            ImageIO.write(image, "png", out);
        }
    }

    public void workerChart(OutputStream out, Object data) throws IOException {

        PlanetCalculation pc = SessionContextHolder.getSessionContext().getPlanetCalculationActPlanet();
        ExtPlanetCalcResult epcr = pc.getPlanetCalcData().getExtPlanetCalcData();
        EconomyChart ec = new EconomyChart(epcr, SessionContextHolder.getSessionContext().getUser().getId());

        JFreeChart chart1 = ec.getWorkerChart(false);

        ChartRenderingInfo info = new ChartRenderingInfo();
        // populate the info
        if (chart1 != null) {
            BufferedImage image = chart1.createBufferedImage(400, 300, info);

            ImageIO.write(image, "png", out);
        }

    }

    /**
     * @return the planetName
     */
    public String getPlanetName() {
        return planetName;
    }

    /**
     * @param planetName the planetName to set
     */
    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }



}
