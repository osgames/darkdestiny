package at.darkdestiny.web.dto;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.richfaces.application.push.TopicKey;

public class Chats implements Serializable {

    private static final Map<TopicKey, ArrayList<ChatEntry>> chats = Maps.newLinkedHashMap();

    static {
        chats.put(new TopicKey("Allgemein"), new ArrayList<ChatEntry>());
        chats.put(new TopicKey("Allianz"), new ArrayList<ChatEntry>());
    }

    public static List<String> getMessages(TopicKey topicKey, Color c) {
        List<String> result = Lists.newArrayList();
        if (chats.get(topicKey) == null) {
            return result;
        } else {
            for (ChatEntry entry : chats.get(topicKey)) {
                result.add(entry.toString(c));
            }
            return result;
        }

    }

    public static Set<TopicKey> getTopics() {
        return chats.keySet();
    }

    public static void addChatEntry(TopicKey topicKey, ChatEntry chatEntry) {
        ArrayList<ChatEntry> entries = chats.get(topicKey);
        if (entries == null) {
            entries = Lists.newArrayList();
        }
        entries.add(chatEntry);

        chats.put(topicKey, entries);
    }

}
