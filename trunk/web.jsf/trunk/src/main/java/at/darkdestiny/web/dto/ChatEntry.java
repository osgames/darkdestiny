/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.dto;

import at.darkdestiny.web.BeanUtilities;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author Admin
 */
public class ChatEntry {

    private String message;

    private Date date;

    private SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");

    public ChatEntry(String message) {
        this.message = message;
        this.date = new Date();
    }

    public String toString(Color c) {

        String hex = "#" + Integer.toHexString(c.getRGB()).substring(2);
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='");
        sb.append(hex);
        sb.append("'>");
        sb.append("[");
        sb.append(sdf.format(date));
        sb.append("]");
        if (BeanUtilities.getLoggedUser() != null) {
            sb.append(BeanUtilities.getLoggedUser().getGameName());
        }else{
            sb.append("Gast");
        }
        sb.append(": ");
        sb.append(this.message);
        sb.append("</font>");
        return sb.toString();
    }
}
