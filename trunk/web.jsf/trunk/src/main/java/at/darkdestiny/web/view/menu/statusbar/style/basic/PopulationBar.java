/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.view.html.style;
import at.darkdestiny.core.view.html.table;
import at.darkdestiny.core.view.menu.StatusBarData;
import at.darkdestiny.core.view.menu.statusbar.*;
import at.darkdestiny.web.BeanUtilities;

/**
 *
 * @author Aion
 */
public class PopulationBar extends BaseBar {

    public PopulationBar() {
        super();
    }

    public String getBackgroundImage() {
        return "pic/menu/statusbar/stb_population_background.png";
    }

    public String getIcon() {
        double morale = planetCalculation.getPlanetCalcData().getExtPlanetCalcData().getMorale();
        String imageIcon;
        if (morale >= 100) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_verygood.png";
        } else if (morale >= 75 && morale < 100) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_good.png";
        } else if (morale >= 50 && morale < 75) {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_equal.png";
        } else {
            imageIcon = GameConfig.getInstance().picPath() + "pic/menu/icons/mood_bad.png";
        }
        return imageIcon;
    }

    public long getMigration() {
        PlayerPlanet pp = planetCalculation.getPlayerPlanet();
        return pp.getMigration();
    }

    public long getPopulationChange() {
        PlayerPlanet pp = planetCalculation.getPlayerPlanet();
        long possibleGrowth = planetCalculation.getPlanetCalcData().getExtPlanetCalcData().getMaxPopulation() - (pp.getPopulation() + pp.getMigration());
        long theoreticalGrowth = (long) (((float) pp.getPopulation() / (float) 100) * pp.getGrowth() / 360f);
        long realGrowth = Math.min(theoreticalGrowth, possibleGrowth);

        return realGrowth;
    }

    public long getPopulation() {
        PlayerPlanet pp = planetCalculation.getPlayerPlanet();

        return pp.getPopulation();
    }

    public long getMorale() {
        return planetCalculation.getPlayerPlanet().getMoral();
    }

    public String getEmigrationImageIcon() {

        PlayerPlanet pp = planetCalculation.getPlayerPlanet();
        long possibleGrowth = planetCalculation.getPlanetCalcData().getExtPlanetCalcData().getMaxPopulation() - (pp.getPopulation() + pp.getMigration());
        long theoreticalGrowth = (long) (((float) pp.getPopulation() / (float) 100) * pp.getGrowth() / 360f);
        long realGrwoth = Math.min(theoreticalGrowth, possibleGrowth);

        String emigrationIconImage = "";
        long migrationChange = realGrwoth;
        migrationChange += pp.getMigration();
        if (migrationChange > 0) {
            emigrationIconImage = GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_plus.png";
        } else {
            emigrationIconImage = GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_minus.png";
        }
        return emigrationIconImage;
    }

    public long getGrowthPopulation() {
        return Math.round(planetCalculation.getPlayerPlanet().getGrowth());
    }
}
