/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.filter;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.bean.LoginBean;
import at.darkdestiny.web.dto.SessionContext;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "ContextFilter")
@RequestScoped
public class SecurityFilter extends AbstractFilter {

    @Override
    /*
     * Loads all necessary Values into the facesContext
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        System.out.println("Do security filter");
        if (request.getAttribute("SessionContext") == null) {
            System.out.println("Redirect to : " + GameConfig.getInstance().getHostURL());
            ((HttpServletResponse) response).sendRedirect(GameConfig.getInstance().getHostURL());
        }
    }
}
