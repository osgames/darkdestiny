/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.dto;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.web.servlets.util.TimeLine;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LAZYPAD
 */
public class TimeLineSelection {

    private Calendar selectedDate;
    private int yearsToDisplay = 50;
    private int width = TimeLine.WIDTH;
    private int height = TimeLine.HEIGHT;
    public static final int YEAR_ZOOM_INTERVAL = 50;
    public static final int YEAR_ZOOM_MIN = 1000;
    public static final int YEAR_ZOOM_MAX = 25;
    public static final int TIMELINE_WIDTH_ZOOM_INTERVALL = 25;
    public static final int TIMELINE_WIDTH_ZOOM_MAX = 400;
    public static final int TIMELINE_WIDTH_ZOOM_MIN = 1000;

    private int userId;


    public TimeLineSelection(int userId){
        this.userId = userId;
        selectedDate = GameUtilities.getDate();
    }
    /**
     * @return the selectedDate
     */
    public Calendar getSelectedDate() {
        return selectedDate;
    }

    /**
     * @return the yearsToDisplay
     */
    public int getYearsToDisplay() {
        return yearsToDisplay;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    public void incrementYearsToDisplay() {
        setYearsToDisplay(Math.min(getYearsToDisplay() + YEAR_ZOOM_INTERVAL, YEAR_ZOOM_MIN));
    }

    public void decrementYearsToDisplay() {
        setYearsToDisplay(Math.max(getYearsToDisplay() - YEAR_ZOOM_INTERVAL, YEAR_ZOOM_MAX));
    }

    public void incrementTimeLineWidth() {
        width = Math.min(width + TIMELINE_WIDTH_ZOOM_INTERVALL, TIMELINE_WIDTH_ZOOM_MIN);
    }

    public void decrementTimeLineWidth() {
        width = Math.max(width - TIMELINE_WIDTH_ZOOM_INTERVALL, TIMELINE_WIDTH_ZOOM_MAX);
    }

    /**
     * @param selectedDate the selectedDate to set
     */
    public void setSelectedDate(Calendar selectedDate) {
        this.selectedDate = selectedDate;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @param yearsToDisplay the yearsToDisplay to set
     */
    public void setYearsToDisplay(int yearsToDisplay) {
        this.yearsToDisplay = yearsToDisplay;
    }
}
