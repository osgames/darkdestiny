/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.view.header.CategoryEntity;
import at.darkdestiny.core.view.header.HeaderData;
import at.darkdestiny.core.view.header.PlanetEntity;
import at.darkdestiny.core.view.header.SystemEntity;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.view.management.EconomyValues;
import at.darkdestiny.web.view.management.PopulationValues;
import at.darkdestiny.web.view.management.StockValues;
import com.google.common.collect.Maps;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class HeaderViewData implements ViewData {

    SessionContext sessionContext;
    private HeaderData headerData;

    public HeaderViewData() {
        this.sessionContext = SessionContextHolder.getSessionContext();

        System.out.println("SessionContext: " + sessionContext);
        int userId = sessionContext.getUser().getId();

        //dbg Logger.getLogger().write("Loaded Headerdata for user: "+userId);
        this.headerData = HeaderBuffer.getUserHeaderData(userId);
        if (this.headerData == null) {
            HeaderBuffer.reloadUser(userId);
            this.headerData = HeaderBuffer.getUserHeaderData().get(userId);
        }
        if (this.headerData == null) {
            throw new RuntimeException("Headerdata must not be null");
        }
    }

    public HashMap<Integer, CategoryEntity> getCategories() {
        return this.headerData.getCategorys();
    }


    public HashMap<Integer, SystemEntity> getSystems() {
        if (getCategories().get(sessionContext.getSelectedCategoryId()) != null) {
            return getCategories().get(sessionContext.getSelectedCategoryId()).getSystems();
        } else {
            SystemEntity se = null;
            for (CategoryEntity ce : getCategories().values()) {
                if (!ce.getSystems().isEmpty()) {
                   // Iterator it = ce.getSystems().entrySet().iterator();
                    // Map.Entry<Integer, SystemEntity> entry = (Map.Entry<Integer, SystemEntity>) it.next();
                    //this.setSelectedSystemId(entry.getValue().getSystemId());
                    return ce.getSystems();
                }
            }
            if (se == null) {
                se = new SystemEntity();
                se.setSystemId(0);
                se.setSystemName(ML.getMLStr("header_sel_none", SessionContextHolder.getSessionContext().getLocale()));
            }
            HashMap<Integer, SystemEntity> result = Maps.newHashMap();
            result.put(se.getSystemId(), se);
            return result;
        }
    }

    public HashMap<Integer, PlanetEntity> getPlanets() {
        if (getCategories().get(sessionContext.getSelectedCategoryId()) != null && getCategories().get(sessionContext.getSelectedCategoryId()).getSystems().get(sessionContext.getSelectedSystemId()) != null) {
            return getCategories().get(sessionContext.getSelectedCategoryId()).getSystems().get(sessionContext.getSelectedSystemId()).getPlanets();
        } else {
            PlanetEntity pe = null;
            if (getSystems() != null) {
                for (SystemEntity se : getSystems().values()) {
                    if (!se.getPlanets().isEmpty()) {
                        // Iterator it = se.getPlanets().entrySet().iterator();
                        //Map.Entry<Integer, PlanetEntity> entry = (Map.Entry<Integer, PlanetEntity>) it.next();
                        //this.setSelectedPlanetId(entry.getValue().getPlanetId());
                        return se.getPlanets();
                    }
                }
            }
            if (pe == null) {
                pe = new PlanetEntity();
                pe.setPlanetId(0);
                pe.setPlanetName(ML.getMLStr("header_sel_none", SessionContextHolder.getSessionContext().getLocale()));
            }

            HashMap<Integer, PlanetEntity> result = Maps.newHashMap();
            result.put(pe.getPlanetId(), pe);
            return result;
        }
    }




}
