/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean;

import at.darkdestiny.core.ML;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.MD5;
import at.darkdestiny.core.utilities.img.BaseRenderer;
import at.darkdestiny.core.view.header.HeaderData;
import at.darkdestiny.core.viewbuffer.HeaderBuffer;
import at.darkdestiny.web.BeanUtilities;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.dto.TimeLineSelection;
import at.darkdestiny.web.servlets.util.TimeLine;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import javax.validation.constraints.AssertTrue;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "LoginBean")
@SessionScoped
public class LoginBean implements Serializable, Cloneable {

    private User user;
    private String loginMessage = "";

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    public LoginBean() {
        user = new User();
        user.setUserName("");
        user.setPassword("");
    }

    /**
     * @param aUser the user to set
     */
    private void setUser(User user) {
        SessionContext sessionContext = SessionContextHolder.getSessionContext();

        PlayerPlanet homePlanet = Service.playerPlanetDAO.findHomePlanetByUserId(user.getId());
        if (homePlanet != null) {
            sessionContext.setSelectedPlanetId(homePlanet.getPlanetId());
            sessionContext.setSelectedSystemId(Service.planetDAO.findById(homePlanet.getPlanetId()).getSystemId());
            try {
                sessionContext.setPlanetCalculationActPlanet(new PlanetCalculation(sessionContext.getSelectedPlanetId()));
            } catch (Exception ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        sessionContext.setUser(user);
        SessionContextHolder.updateSessionContext(sessionContext);

    }

    public String login() {
        login(user.getUserName(), user.getPassword());
        if (isLogged()) {
            return "index?faces-redirect=true";
        } else {
            return "login?faces-redirect=true";
        }
    }

    @AssertTrue(message = "login_err_usernamealreadyexisting")
    public boolean isValidLogin() {
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    public void login(String userName, String password) {

        User u = Service.userDAO.findBy(userName);
        System.out.println(u);
        loginMessage = "login_err_usernotfound";
        if (u != null) {

            System.out.println(password + " - " + u.getPasswordSalt());
            //  password = MD5.encryptPassword(password, u.getPasswordSalt());
            password = MD5.encryptPassword(password);
            System.out.println(password);
            u = Service.userDAO.findBy(userName, password);

            if (u != null) {
                setUser(u);
                SessionContext sessionContext = new SessionContext();
                sessionContext.setUser(u);
                sessionContext.setSelectedCategoryId(HeaderData.CATEGORY_ALL);
                System.out.println("Language on user : " + u.getLocale());
                sessionContext.setLocale(ML.getLocale(u.getId()));
                sessionContext.setLocaleSetManually(true);
                System.out.println("Language on sessionContext : " + sessionContext.getLocale());
                sessionContext.setTimeLineSelection(new TimeLineSelection(u.getId()));

                HeaderData headerData = HeaderBuffer.getUserHeaderData(u.getId());
                try {
                    int firstSystem = headerData.getCategorys().get(HeaderData.CATEGORY_ALL).getFirstSystemId();
                    int firstPlanet = headerData.getCategorys().get(HeaderData.CATEGORY_ALL).getSystems().get(firstSystem).getFirstPlanetId();
                    PlanetCalculation planetCalculation = new PlanetCalculation(firstPlanet);
                    sessionContext.setPlanetCalculationActPlanet(planetCalculation);

                    sessionContext.setSelectedSystemId(firstSystem);
                    sessionContext.setSelectedPlanetId(firstPlanet);

                } catch (Exception e) {
                    System.out.println("Could not set header values on login: " + e);
                }
                SessionContextHolder.updateSessionContext(sessionContext);
            } else {
                loginMessage = "login_err_passwordinvalid";
            }
            System.out.println(u);

        }

    }

    public void logout() {
        SessionContextHolder.removeSessionContext();
    }

    public boolean isLogged() {
        return SessionContextHolder.getSessionContext().isLogged();
    }

    /**
     * @return the adminLogged
     */
    public boolean isAdminLogged() {
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        return loginBean.isAdminLogged();
    }

    /**
     * @return the loginMessage
     */
    public String getLoginMessage() {
        return BeanUtilities.getMLStr(loginMessage);
    }
}
