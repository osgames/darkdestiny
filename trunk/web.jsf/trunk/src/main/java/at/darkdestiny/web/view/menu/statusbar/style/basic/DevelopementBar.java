/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import static at.darkdestiny.core.service.Service.userDataDAO;
import at.darkdestiny.web.BeanUtilities;

/**
 *
 * @author Aion
 */
public class DevelopementBar extends BaseBar {

    double globalDevelopementPoints;
    double planetDevelopementPoints;

    public DevelopementBar() {
        globalDevelopementPoints = 0;
        planetDevelopementPoints = 0;

        UserData ud = userDataDAO.findByUserId(BeanUtilities.getLoggedUser().getId());
        if (ud != null) {
            globalDevelopementPoints = ud.getDevelopementPoints();
        }

        planetDevelopementPoints = planetCalculation.getPlanetCalcData().getExtPlanetCalcData().getDevelopementPoints();

    }

    public String getBackgroundImage() {
        return "pic/menu/statusbar/stb_credit_background.png";
    }

    public String getIcon() {
        return "pic/menu/icons/happiness.png";
    }

    public double getGlobalDevelopement() {
        return globalDevelopementPoints;
    }

    public double getPlanetDevelopement() {
        return planetDevelopementPoints;
    }

    public String getPlanetDevelopementImageIcon() {
        if (planetDevelopementPoints > 0) {
            return GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_plus.png";
        } else {
            return GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_minus.png";
        }
    }
}
