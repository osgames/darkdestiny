/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.management;

import at.darkdestiny.web.view.ViewData;
import com.google.common.collect.Lists;
import java.util.List;

/**
 *
 * @author Admin
 */
public class StockValues implements ViewData {

    private final List<ResourceValue> resourceValues = Lists.newArrayList();

    public void addResourceValue(Integer id, String imageLocation, long minProductionStock, long minTransportStock, long minTradeStock) {
        getResourceValues().add(new ResourceValue(id, imageLocation,  minProductionStock, minTransportStock, minTradeStock));
    }

    /**
     * @return the resourceValues
     */
    public List<ResourceValue> getResourceValues() {
        return resourceValues;
    }

}
