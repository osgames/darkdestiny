/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.web.BeanUtilities;

/**
 *
 * @author Bullet
 */
public class EnergyBar extends BaseBar {



    private long productionNow;
    private long productionLater;
    private long consumptionNow;
    private long consumptionLater;
    private static final int resourceId = Ressource.ENERGY;
    private int maxLength = 104;
    private int maxLengthConsumptionTotal = maxLength;
    private int maxLengthProductionTotal = maxLength;
    private String productionNowImage = "pic/menu/energyProductionNow.png";
    private String productionLaterImage = "pic/menu/energyProductionLater.png";
    private String consumptionNowImage = "pic/menu/energyConsumptionNow.png";
    private String consumptionLaterImage = "pic/menu/energyConsumptionLater.png";
    private static final int zIndexShow = 2;
    private static final int zIndexHide = 0;
    private int barLengthProductionNow = 0;
    private int barLengthProductionLater = 0;
    private int barLengthConsumptionNow = 0;
    private int barLengthConsumptionLater = 0;
    private int productionNowZIndex = zIndexHide;
    private int consumptionNowZIndex = zIndexHide;

    public EnergyBar() {
        super();
        productionNow = planetCalculation.getPlanetCalcData().getProductionData().getRessProduction(resourceId);
        productionLater = planetCalculation.getProductionDataFuture().getRessProduction(resourceId);
        consumptionNow = planetCalculation.getPlanetCalcData().getProductionData().getRessConsumption(resourceId);
        consumptionLater = planetCalculation.getProductionDataFuture().getRessConsumption(resourceId);
        initImages();
    }

    public String getIcon() {
        return "pic/menu/icons/energy.png";
    }

    public String getBackgroundImage() {
        return "pic/menu/statusbar/stb_energy_background.png";
    }

    private void initImages() {

        /**
         *
         * Zum richtigen Skalieren muss als erstes der Wert herausgefunden
         * werden der am h?chsten ist und an ihm wird dann skaliert
         *
         *
         */
        if (getConsumptionLater() > getProductionLater()) {
            maxLengthProductionTotal = (int) Math.round((double) getMaxLength() * getMultiplicator(getConsumptionLater(), getProductionLater()));

            if (getConsumptionLater() >= getConsumptionNow()) {
                barLengthConsumptionLater = getMaxLengthConsumptionTotal();
                consumptionNowZIndex = zIndexShow;
                barLengthConsumptionNow = 0 + (int) Math.round((double) getMaxLengthConsumptionTotal() * getMultiplicator(getConsumptionLater(), getConsumptionNow()));
            } else {
                barLengthConsumptionNow = getMaxLengthConsumptionTotal();
                barLengthConsumptionLater = 0 + (int) Math.round((double) getMaxLengthConsumptionTotal() * getMultiplicator(getConsumptionNow(), getConsumptionLater()));
            }

            if (getProductionLater() >= getProductionNow()) {
                barLengthProductionLater = getMaxLengthProductionTotal();
                productionNowZIndex = zIndexShow;
                barLengthProductionNow = 0 + (int) Math.round((double) getMaxLengthProductionTotal() * getMultiplicator(getProductionLater(), getProductionNow()));
            } else {
                barLengthProductionNow = getMaxLengthProductionTotal();
                barLengthProductionLater = 0 + (int) Math.round((double) getMaxLengthProductionTotal() * getMultiplicator(getProductionNow(), getProductionLater()));
            }
        } else if (getConsumptionLater() == getProductionLater()) {
            if (getConsumptionLater() >= getConsumptionNow()) {
                barLengthConsumptionLater = getMaxLengthConsumptionTotal();
                consumptionNowZIndex = zIndexShow;
                barLengthConsumptionNow = 0 + (int) Math.round((double) getMaxLengthConsumptionTotal() * getMultiplicator(getConsumptionLater(), getConsumptionNow()));
            } else {
                barLengthConsumptionNow = getMaxLengthConsumptionTotal();
                barLengthConsumptionLater = 0 + (int) Math.round((double) getMaxLengthConsumptionTotal() * getMultiplicator(getConsumptionNow(), getConsumptionLater()));
            }

            /* -------------
             * |  bar_later  |
             *  -------------
             *  ----------
             * | bar_now  |
             *  ---------- */
            if (getProductionLater() >= getProductionNow()) {
                barLengthProductionLater = getMaxLengthProductionTotal();
                productionNowZIndex = zIndexShow;
                barLengthProductionNow = 0 + (int) Math.round((double) getMaxLengthProductionTotal() * getMultiplicator(getProductionLater(), getProductionNow()));
            } else {
                barLengthProductionNow = getMaxLengthProductionTotal();
                barLengthProductionLater = 0 + (int) Math.round((double) getMaxLengthProductionTotal() * getMultiplicator(getProductionNow(), getProductionLater()));
            }
        } else if (getConsumptionLater() < getProductionLater()) {
            maxLengthConsumptionTotal = (int) Math.round((double) getMaxLength() * getMultiplicator(getProductionLater(), getConsumptionLater()));

            if (getConsumptionLater() >= getConsumptionNow()) {
                barLengthConsumptionLater = getMaxLengthConsumptionTotal();
                consumptionNowZIndex = zIndexShow;
                barLengthConsumptionNow = 0 + (int) Math.round((double) getMaxLengthConsumptionTotal() * getMultiplicator(getConsumptionLater(), getConsumptionNow()));
            } else {
                barLengthConsumptionNow = getMaxLengthConsumptionTotal();
                barLengthConsumptionLater = 0 + (int) Math.round((double) getMaxLengthConsumptionTotal() * getMultiplicator(getConsumptionNow(), getConsumptionLater()));
            }

            if (getProductionLater() >= getProductionNow()) {
                barLengthProductionLater = getMaxLengthProductionTotal();
                productionNowZIndex = zIndexShow;
                barLengthProductionNow = 0 + (int) Math.round((double) getMaxLengthProductionTotal() * getMultiplicator(getProductionLater(), getProductionNow()));
            } else {
                barLengthProductionNow = getMaxLengthProductionTotal();
                barLengthProductionLater = 0 + (int) Math.round((double) getMaxLengthProductionTotal() * getMultiplicator(getProductionNow(), getProductionLater()));
            }
        } else {
            DebugBuffer.addLine(DebugLevel.ERROR, "Energie wird nicht angezeigt: ");
            DebugBuffer.addLine(DebugLevel.ERROR, ":Verbrauch jetzt: " + getConsumptionNow());
            DebugBuffer.addLine(DebugLevel.ERROR, ":Verbrauch sp�ter: " + getConsumptionLater());
            DebugBuffer.addLine(DebugLevel.ERROR, ":Produktion jetzt: " + getProductionNow());
            DebugBuffer.addLine(DebugLevel.ERROR, ":Produktion sp�ter: " + getProductionLater());
        }


    }

    private double getMultiplicator(double higherValue, double lowerValue) {
        double multiplicator = (lowerValue / higherValue);
        return multiplicator;

    }

    /**
     * @return the maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * @return the maxLengthConsumptionTotal
     */
    public int getMaxLengthConsumptionTotal() {
        return maxLengthConsumptionTotal;
    }

    /**
     * @return the maxLengthProductionTotal
     */
    public int getMaxLengthProductionTotal() {
        return maxLengthProductionTotal;
    }

    /**
     * @return the productionNowImage
     */
    public String getProductionNowImage() {
        return productionNowImage;
    }

    /**
     * @return the productionLaterImage
     */
    public String getProductionLaterImage() {
        return productionLaterImage;
    }

    /**
     * @return the consumptionNowImage
     */
    public String getConsumptionNowImage() {
        return consumptionNowImage;
    }

    /**
     * @return the consumptionLaterImage
     */
    public String getConsumptionLaterImage() {
        return consumptionLaterImage;
    }

    /**
     * @return the barLengthProductionNow
     */
    public int getBarLengthProductionNow() {
        return barLengthProductionNow;
    }

    /**
     * @return the barLengthProductionLater
     */
    public int getBarLengthProductionLater() {
        return barLengthProductionLater;
    }

    /**
     * @return the barLengthConsumptionNow
     */
    public int getBarLengthConsumptionNow() {
        return barLengthConsumptionNow;
    }

    /**
     * @return the barLengthConsumptionLater
     */
    public int getBarLengthConsumptionLater() {
        return barLengthConsumptionLater;
    }
//
//    public String getProductionNowLeft() {
//
//        int offset = getBarOffset(getBarLengthProductionNow());
//        return offset + "px";
//    }
//
//    public String getProductionLaterLeft() {
//
//        int offset = getBarOffset(getBarLengthProductionLater());
//        return offset + "px";
//    }
//
//    public String getConsumptionNowLeft() {
//
//        int offset = getBarOffset(getBarLengthConsumptionNow());
//        return offset + "px";
//    }
//
//    public String getConsumptionLaterLeft() {
//
//        int offset = getBarOffset(getBarLengthConsumptionLater());
//
//        return offset + "px";
//    }
//
//    public int getBarOffset() {
//
//        int offset = 56;
//        return offset;
//
//    }

    /**
     * @return the productionNowZIndex
     *
     */
    public int getProductionNowZIndex() {
        return productionNowZIndex;
    }

    /**
     * @return the consumptionNowZIndex
     */
    public int getConsumptionNowZIndex() {
        return consumptionNowZIndex;
    }

    /**
     * @return the productionNow
     */
    public long getProductionNow() {
        return productionNow;
    }

    /**
     * @return the productionLater
     */
    public long getProductionLater() {
        return productionLater;
    }

    /**
     * @return the consumptionNow
     */
    public long getConsumptionNow() {
        return consumptionNow;
    }

    /**
     * @return the consumptionLater
     */
    public long getConsumptionLater() {
        return consumptionLater;
    }
}
