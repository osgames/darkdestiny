/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.view.HeaderViewData;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "HeaderBean")
@RequestScoped
public class HeaderBean implements JsfBean {

    HeaderViewData headerView;

    SessionContext sessionContext;

    public HeaderBean() {

        this.sessionContext = SessionContextHolder.getSessionContext();
        if (headerView == null) {
            calcData();
        }
    }

    public void updateHeaderValues(ValueChangeEvent e) {
        System.out.println("update header values");
        try {

            PlanetCalculation planetCalculation = new PlanetCalculation(SessionContextHolder.getSessionContext().getSelectedPlanetId());
            sessionContext.setPlanetCalculationActPlanet(planetCalculation);
        } catch (Exception ex) {
            Logger.getLogger(HeaderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void calcData() {
        headerView = new HeaderViewData();
    }

    public Integer getSelectedCategoryId() {
        return sessionContext.getSelectedCategoryId();
    }

    public void setSelectedCategoryId(Integer selectedCategoryId) {
        sessionContext.setSelectedCategoryId(selectedCategoryId);
    }

    public Integer getSelectedSystemId() {
        return sessionContext.getSelectedSystemId();
    }

    public void setSelectedSystemId(Integer selectedSystemId) {
        sessionContext.setSelectedSystemId(selectedSystemId);
    }

    public Integer getSelectedPlanetId() {
        return sessionContext.getSelectedPlanetId();
    }

    public void setSelectedPlanetId(Integer selectedPlanetId) {
        sessionContext.setSelectedPlanetId(selectedPlanetId);
    }

    public void decrementCategory() {
        if (sessionContext.getSelectedCategoryId() != null) {
            sessionContext.setSelectedCategoryId((Integer) headerView.getCategories().get(sessionContext.getSelectedCategoryId()).getPreviousCategoryId());
        }
    }

    public void decrementSystem() {
        if (sessionContext.getSelectedCategoryId() != null && sessionContext.getSelectedSystemId() != null) {
            sessionContext.setSelectedCategoryId((Integer) headerView.getCategories().get(sessionContext.getSelectedCategoryId()).getSystems().get(sessionContext.getSelectedSystemId()).getPreviousSystemId());
        }
    }

    public void decrementPlanet() {
        if (sessionContext.getSelectedCategoryId() != null && sessionContext.getSelectedSystemId() != null && sessionContext.getSelectedPlanetId() != null) {
            sessionContext.setSelectedPlanetId((Integer) headerView.getCategories().get(sessionContext.getSelectedCategoryId()).getSystems().get(sessionContext.getSelectedSystemId()).getPlanets().get(sessionContext.getSelectedPlanetId()).getPreviousPlanetId());
        }
    }

    public void incrementCategory() {
        if (sessionContext.getSelectedCategoryId() != null) {
            sessionContext.setSelectedCategoryId((Integer) headerView.getCategories().get(sessionContext.getSelectedCategoryId()).getNextCategoryId());
        }
    }

    public void incrementSystem() {
        if (sessionContext.getSelectedCategoryId() != null && sessionContext.getSelectedSystemId() != null) {
            sessionContext.setSelectedCategoryId((Integer) headerView.getCategories().get(sessionContext.getSelectedCategoryId()).getSystems().get(sessionContext.getSelectedSystemId()).getNextSystemId());
        }
    }

    public void incrementPlanet() {
        if (sessionContext.getSelectedCategoryId() != null && sessionContext.getSelectedSystemId() != null && sessionContext.getSelectedPlanetId() != null) {
            sessionContext.setSelectedPlanetId((Integer) headerView.getCategories().get(sessionContext.getSelectedCategoryId()).getSystems().get(sessionContext.getSelectedSystemId()).getPlanets().get(sessionContext.getSelectedPlanetId()).getNextPlanetId());
        }
    }

    @Override
    public HeaderViewData getViewData() {
        return this.headerView;
    }
}
