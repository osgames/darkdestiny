/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.enumeration.EProductionOrderType;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.GroundTroop;
import at.darkdestiny.core.model.MenuImage;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.PlayerCategory;
import at.darkdestiny.core.model.PlayerPlanet;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.model.ShipDesign;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.web.BeanUtilities;
import at.darkdestiny.web.view.construction.InBuildView;
import java.io.Serializable;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "ConversionBean")
@ApplicationScoped
public class ConversionBean implements Serializable {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ConversionBean.class);

    public String userIdToGameName(String userIdString, String type) {
        int userId = 0;
        try {
            userId = Integer.parseInt(userIdString);
        } catch (Exception e) {
            log.error("Could not parse " + userIdString + " to int in conversionservice");
        }
        User u = Service.userDAO.findById(userId);
        if (u == null) {
            return BeanUtilities.getMLStr("global_unknown_user");
        }
        if (type.equals("gameName")) {
            return u.getGameName();
        }
        return "unknown type";
    }

    public String messageToFormattedMessage(String message, String error) {
        return "";
    }

    public String messageTypeToAbbreviation(String type) {
        String messageAbbreviation;
        EMessageType m;
        try {
            m = EMessageType.valueOf(type);
        } catch (Exception e) {
            log.error("Could not parse EMessageType from [" + type + "] in ConversionBean");
            return "???";
        }
        if (m.equals(EMessageType.SYSTEM)) {
            messageAbbreviation = "SYS";
        } else if (m.equals(EMessageType.USER)) {
            messageAbbreviation = "P2P";
        } else if (m.equals(EMessageType.ALLIANCE)) {
            messageAbbreviation = "ALL";
        } else {
            messageAbbreviation = "???";
        }
        return messageAbbreviation;

    }

    public String planetIdToName(int planetId) {
        PlayerPlanet pp = Service.playerPlanetDAO.findById(planetId);
        if (pp != null) {
            return pp.getName();
        } else {
            Planet p = Service.planetDAO.findById(planetId);
            if (p != null) {
                return "Planet #" + p.getId();
            }
        }
        return "NoName";
    }

    public String categoryIdToName(int categoryId) {
        PlayerCategory pc = Service.playerCategoryDAO.findById(categoryId);
        if (pc != null) {
            return pc.getName();
        } 
        return "NoName";
    }
    public String systemIdToName(int systemId) {
        at.darkdestiny.core.model.System s = Service.systemDAO.findById(systemId);
        if (s != null) {
            return s.getName();
        }
        return "NoName";
    }

    public MenuImage linkMenuToImage(int menuImageId) {
        MenuImage menuImage = Service.menuImageDAO.findById(menuImageId);
        return menuImage;
    }


    public Ressource resourceIdToResource(int resourceId) {
        return Service.ressourceDAO.findById(resourceId);
    }

    public InBuildView buildableToBuildableView(Buildable buildable) {
        Class clazz = buildable.getModelClass();
        Model m = buildable.getBase();
        return buildableToBuildableView(m, clazz);
    }

    public InBuildView buildableToBuildableView(Model m, Class clazz) {
        if (clazz.equals(Construction.class)) {
            Construction c = (Construction) m;
            return new InBuildView(c.getName(), c.getInfotext(), c.getId(), c.getImage());
        } else if (clazz.equals(GroundTroop.class)) {
            GroundTroop g = (GroundTroop) m;
            return new InBuildView(g.getName(), g.getDescription(), g.getId(), g.getImage());
        } else if (clazz.equals(ShipDesign.class)) {
            ShipDesign s = (ShipDesign) m;
            return new InBuildView(s.getName(), null, s.getId(), null);
        }
        return new InBuildView(null, null, null, null);
    }

    public InBuildView modelToInBuildView(Model m, EProductionOrderType type) {
        if (type.equals(EProductionOrderType.C_BUILD)) {
            return buildableToBuildableView(m, Construction.class);
        } else if (type.equals(EProductionOrderType.GROUNDTROOPS)) {
            return buildableToBuildableView(m, GroundTroop.class);
        } else if (type.equals(EProductionOrderType.PRODUCE)) {
            return buildableToBuildableView(m, ShipDesign.class);
        }
        return new InBuildView(null, null, null, null);
    }

}
