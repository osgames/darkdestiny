/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.model.Language;
import at.darkdestiny.core.model.Planet;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "ServiceBean")
@SessionScoped
public class ServiceBean implements Serializable {

    /**
     * @return the locale
     */
    public Locale getLocale() {
        return SessionContextHolder.getSessionContext().getLocale();
    }

    /**
     * @return the localeSetManually
     */
    public boolean isLocaleSetManually() {
        return SessionContextHolder.getSessionContext().isLocaleSetManually();
    }

    private int myvalue = 15;

    public void doSomething(){
        System.out.println("Me being called");
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(Language language) {
        SessionContext sessionContext = SessionContextHolder.getSessionContext();
        sessionContext.setLocaleSetManually(true);
        Language masterLanguage = Service.languageDAO.getMasterLanguageOf(language.getId());
        Locale locale = new Locale(masterLanguage.getLanguage(), masterLanguage.getCountry());
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        if (loginBean.isLogged()) {
            sessionContext.setLocale(locale);
            User u = sessionContext.getUser();
            System.out.println("userId : " + u.getId());
            u.setLocale(locale.getLanguage() + "_" + locale.getCountry());
            Service.userDAO.update(u);
        }
        SessionContextHolder.updateSessionContext(sessionContext);
    }

    public ArrayList<Language> getLanguages() {
        return Service.languageDAO.findAll();
    }

    public ArrayList<BaseResult> getMessages() {
         System.out.println("trying to get messages");
        ArrayList<BaseResult> messages = Lists.newArrayList(MessageContext.getCurrentInstance().getMessages());
        MessageContext.getCurrentInstance().getMessages().clear();
        System.out.println(messages.size());
        return messages;
    }

    /**
     * @return the time
     */
    public String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(GameUtilities.getMillisNextTick());
        return sdf.format(new Date(gc.getTimeInMillis()));
    }


    public String getActPlanetName(){
        return Service.playerPlanetDAO.findByPlanetId(SessionContextHolder.getSessionContext().getSelectedPlanetId()).getName();
    }

    public String getActSystemName(){
        return Service.systemDAO.findById(SessionContextHolder.getSessionContext().getSelectedSystemId()).getName();
    }
    public String getActCoordinates(){
        Planet p = Service.planetDAO.findById(SessionContextHolder.getSessionContext().getSelectedPlanetId());
        return p.getSystemId() + " : " + p.getId();


    }

    /**
     * @return the myvalue
     */
    public int getMyvalue() {
        return myvalue;
    }

    /**
     * @param myvalue the myvalue to set
     */
    public void setMyvalue(int myvalue) {
        System.out.println("Setted value");
        this.myvalue = myvalue;
    }
}
