/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.result.InConstructionEntry;
import at.darkdestiny.core.result.InConstructionResult;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.service.ProductionService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.view.BuildableViewData;
import at.darkdestiny.web.view.construction.ConstructionViewData;
import at.darkdestiny.web.view.construction.PlanetConstructionView;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "BuildableBean")
@RequestScoped
public class BuildableBean implements JsfBean {

    private int constructCount = 0;
    private int constructionId = 0;

    public enum EBuildableViewType {

        ADMINISTRATION, ENERGY, CIVIL, INDUSTRY, MILITARY, SHIPS, GROUNDTROOPS
    }

    private static final Map<EBuildableViewType, List<EConstructionType>> types;

    static {
        types = Maps.newLinkedHashMap();
        types.put(EBuildableViewType.ADMINISTRATION, Lists.newArrayList(EConstructionType.ADMINISTRATION));
        types.put(EBuildableViewType.ENERGY, Lists.newArrayList(EConstructionType.ENERGY));
        types.put(EBuildableViewType.CIVIL, Lists.newArrayList(EConstructionType.ZIVIL));
        types.put(EBuildableViewType.INDUSTRY, Lists.newArrayList(EConstructionType.INDUSTRY));
        types.put(EBuildableViewType.MILITARY, Lists.newArrayList(EConstructionType.MILITARY, EConstructionType.PLANETARY_DEFENSE));
        types.put(EBuildableViewType.SHIPS, null);
        types.put(EBuildableViewType.GROUNDTROOPS, null);
    }

    private BuildableViewData viewData;

    public BuildableBean() {
    }

    @Override
    public BuildableViewData getViewData() {
        if (viewData == null) {
            viewData = new BuildableViewData();
            calcViewData();
        }
        return viewData;
    }

    private void calcViewData() {
        SessionContext sessionContext = SessionContextHolder.getSessionContext();
        int planetId = sessionContext.getSelectedPlanetId();
        int userId = sessionContext.getUser().getId();

        for (Map.Entry<EBuildableViewType, List<EConstructionType>> entry : types.entrySet()) {
            ProductionBuildResult pbr = null;
            if (entry.getKey().equals(EBuildableViewType.SHIPS)) {
                pbr = ProductionService.getProductionListShips(userId, planetId);
            } else if (entry.getKey().equals(EBuildableViewType.GROUNDTROOPS)) {
                pbr = ProductionService.getProductionListGroundTroops(userId, planetId);
            } else {
                pbr = ConstructionService.getConstructionListSorted(userId, planetId, entry.getValue());
            }
            this.viewData.addProductionBuildResult(entry.getKey(), pbr);
        }

    }



    /**
     * @return the constructCount
     */
    public int getConstructCount() {
        return constructCount;
    }

    /**
     * @param constructCount the constructCount to set
     */
    public void setConstructCount(int constructCount) {
        this.constructCount = constructCount;
    }

    /**
     * @return the constructionId
     */
    public int getConstructionId() {
        return constructionId;
    }

    /**
     * @param constructionId the constructionId to set
     */
    public void setConstructionId(int constructionId) {
        this.constructionId = constructionId;
    }

}
