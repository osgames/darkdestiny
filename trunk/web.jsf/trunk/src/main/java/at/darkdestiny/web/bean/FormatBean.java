package at.darkdestiny.web.bean;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GameConstants;
import at.darkdestiny.web.SessionContextHolder;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.slf4j.LoggerFactory;

@ManagedBean(name = "FormatBean")
@ApplicationScoped
public class FormatBean implements Serializable {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(FormatBean.class);
    public String format(String value, String valueType, String precision, String type, String cutZero) {
        return format(value, valueType, precision, type, cutZero, null);
    }

    public String format(String value, String valueType, String precision, String type, String cutZero, String percentageSign) {
        if (valueType.equals("long")) {
            long longValue = 0l;
            try {
                longValue = Long.parseLong(value);
            } catch (Exception e) {
                log.warn("FormatBean - Could not convert : [" + value + "] to long");
            }
            if (type.equals("normal")) {
                return FormatUtilities.getFormattedNumber(longValue, SessionContextHolder.getSessionContext().getLocale());
            } else if (type.equals("intons")) {
                return FormatUtilities.getFormatScaledNumber(longValue, GameConstants.VALUE_TYPE_INTONS,
                        SessionContextHolder.getSessionContext().getLocale());
            }
        } else if (valueType.equals("double")) {
            double doubleValue = 0d;
            int precisionValue = 0;
            try {
                doubleValue = Double.parseDouble(value);
            } catch (Exception e) {
                log.warn("FormatBean - Could not convert : value[" + value + "] to double");
            }
            try {
                precisionValue = Integer.parseInt(precision);
            } catch (Exception e) {
                log.warn("FormatBean - Could not convert : precision[" + precision + "] to int");
            }
            String result = null;
            if (type.equals("normal")) {
                if (cutZero.equals("true")) {
                    result = FormatUtilities.getFormattedDecimalCutEndZero(doubleValue, precisionValue, SessionContextHolder.getSessionContext().getLocale());
                } else {
                    result = FormatUtilities.getFormattedDecimal(doubleValue, precisionValue, SessionContextHolder.getSessionContext().getLocale());
                }

            } else if (type.equals("intons")) {
                result = FormatUtilities.getFormatScaledNumber(doubleValue, precisionValue, GameConstants.VALUE_TYPE_INTONS,
                        SessionContextHolder.getSessionContext().getLocale());
            }
            if (result != null) {
                if (percentageSign != null && percentageSign.equalsIgnoreCase("true")) {
                    result += " %";
                }
                return result;
            }
        }
        return "unformatted : " + value;
    }

    public String formatDate(String value, String valueType, String type, String customFormat) {
        if (valueType.equals("long") || valueType.equals("date")) {
            SimpleDateFormat sdf = null;
            if (type.equals("date")) {
                sdf = new SimpleDateFormat("dd.MM.yyyy");
            } else if (type.equals("time")) {
                sdf = new SimpleDateFormat("hh:mm:ss");
            } else if (type.equals("datetime")) {
                sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
            } else if (type.equals("custom")) {
                sdf = new SimpleDateFormat(customFormat);
            }
            Date d = null;
            if (valueType.equals("long")) {
                d = new Date(Long.parseLong(value));
            } else if (valueType.equals("date")) {
                d = new Date(value);
            }
            if (d != null) {
                return sdf.format(d);
            } else {

                return "unformatted" + value;
            }
        }
        return "unformatted : " + value;
    }
}
