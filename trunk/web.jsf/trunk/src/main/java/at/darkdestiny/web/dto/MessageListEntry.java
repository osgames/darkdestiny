/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.dto;

import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.model.Message;

/**
 *
 * @author Admin
 */
public class MessageListEntry {
    private EMessageType type;
    private Message message;

            public MessageListEntry(EMessageType type, Message message){
                this.type = type;
                this.message = message;

    }

    /**
     * @return the type
     */
    public EMessageType getType() {
        return type;
    }

    /**
     * @return the message
     */
    public Message getMessage() {
        return message;
    }
}
