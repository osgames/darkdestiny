/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.model.User;
import at.darkdestiny.web.BeanUtilities;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.ChatEntry;
import at.darkdestiny.web.dto.Chats;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.richfaces.application.push.TopicKey;
import org.richfaces.application.push.TopicsContext;
import org.richfaces.log.Logger;
import org.richfaces.log.RichfacesLogger;

/**
 * Sends message to topic using TopicsContext.
 *
 * @author <a href="http://community.jboss.org/people/lfryc">Lukas Fryc</a>
 */
@ApplicationScoped
@ManagedBean(name = "ChatBean")
public class ChatBean {

    public static final String PUSH_TOPICS_CONTEXT_TOPIC = "pushTopicsContext";

    private Logger log = RichfacesLogger.WEBAPP.getLogger();

    private String message = "";

    private Map<User, Color> chatColors = Maps.newHashMap();
    /*
     * (non-Javadoc)
     *
     * @see org.richfaces.demo.push.MessageProducer#sendMessage()
     */

    public void sendMessage(String topicName) throws Exception {
        try {

            if (Strings.isNullOrEmpty(message)) {
                return;
            }
            TopicKey topicKey = new TopicKey(topicName);
            TopicsContext topicsContext = TopicsContext.lookup();
            topicsContext.publish(new TopicKey("chat"), "message");

            Chats.addChatEntry(topicKey, new ChatEntry(message));

            message = "";

            log.info("published update event to topic : " + topicName);
        } catch (Exception e) {
            log.info("Sending push message using TopicContext failed (" + e.getMessage()
                    + ") - operation will be repeated in few seconds");
            e.printStackTrace();
            if (log.isDebugEnabled()) {
                log.debug(e);
            }
        }
    }

    public void setColor(String colorString) {
        Color color = Color.decode(colorString);
        User user = BeanUtilities.getLoggedUser();
        chatColors.remove(user);
        chatColors.put(user, color);
    }

    public List<SelectItem> getColorSelect() {
        List<SelectItem> options = Lists.newArrayList();

        addOption(options, Color.RED);
        addOption(options, Color.GREEN);
        addOption(options, Color.BLUE);
        addOption(options, Color.YELLOW);
        addOption(options, Color.MAGENTA);

        return options;
    }

    public void addOption(List<SelectItem> options, Color c) {
        String hex = getHexValue(c);
        options.add(new SelectItem(hex, hex));
    }

    public String getHexValue(Color c) {

        return "#" + Integer.toHexString(c.getRGB()).substring(2);
    }

    public List<String> getMessages(String topic) {

        Color c;
        if (BeanUtilities.getLoggedUser() != null) {
            c = chatColors.get(BeanUtilities.getLoggedUser());
            if (c == null) {
                c = genRandomColor();
                chatColors.put(BeanUtilities.getLoggedUser(), c);
            }
        } else {
            String sessionId = SessionContextHolder.getSessionId();
            String numberString = "";
            for (int i = 0; i < sessionId.length(); i++) {
                numberString += Character.getNumericValue(sessionId.charAt(i));
            }
            if (numberString.length() > 12) {
                numberString = numberString.substring(0, 12);
            }
            long number = Long.valueOf(numberString);
            int r = (int) (number % 254);
            int g = (int) (number % 123);
            int b = (int) (number % 12);
            if (r < 22 || g < 22 || b < 22) {
                r = 134;
                g = 212;
            }

            c = new Color(r, g, b);
        }

        TopicKey topicKey = new TopicKey(topic);
        return Chats.getMessages(topicKey, c);
    }

    private Color genRandomColor() {

        Random rand = new Random();
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();

        return new Color(r, g, b);
    }

    public String getMessagesAsTextAreaString(String topic) {

        StringBuilder stringBuilder = new StringBuilder();
        for (String s : getMessages(topic)) {
            stringBuilder.append(s);
            stringBuilder.append("<BR/>");
        }
        return stringBuilder.toString();
    }

    public int getInterval() {
        return 5000;
    }

    public Set<TopicKey> getTopics() {
        return Chats.getTopics();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.richfaces.demo.push.MessageProducer#finalizeProducer()
     */
    public void finalizeProducer() {
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
