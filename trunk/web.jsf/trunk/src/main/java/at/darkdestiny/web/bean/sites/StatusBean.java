/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.model.StyleElement;
import at.darkdestiny.core.model.StyleToUser;
import at.darkdestiny.core.service.ProfileService;
import static at.darkdestiny.core.service.Service.styleElementDAO;
import at.darkdestiny.web.BeanUtilities;
import at.darkdestiny.web.dto.StatusBarItem;
import at.darkdestiny.web.view.menu.statusbar.style.basic.CreditBar;
import at.darkdestiny.web.view.menu.statusbar.style.basic.DevelopementBar;
import at.darkdestiny.web.view.menu.statusbar.style.basic.EnergyBar;
import at.darkdestiny.web.view.menu.statusbar.style.basic.FoodBar;
import at.darkdestiny.web.view.menu.statusbar.style.basic.PopulationBar;
import at.darkdestiny.web.view.menu.statusbar.style.basic.ResourceBar;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "StatusBean")
@RequestScoped
public class StatusBean {



    public String getBgColor(long value) {
        if (value > 0) {
            return "#00FF00";
        } else if (value < 0) {
            return "#FF0000";
        } else {
            return "#FFFF00";
        }
    }

   public PopulationBar getPopulationClazz() {
        try {
            return new PopulationBar();
        } catch (Exception ex) {
            Logger.getLogger(StatusBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public FoodBar getFoodClazz() {
        try {
            return new FoodBar();
        } catch (Exception ex) {
            Logger.getLogger(StatusBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public CreditBar getCreditClazz() {
        try {
            return new CreditBar();
        } catch (Exception ex) {
            Logger.getLogger(StatusBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public EnergyBar getEnergyClazz() {
        try {
            return new EnergyBar();
        } catch (Exception ex) {
            Logger.getLogger(StatusBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    public ResourceBar getResourceClazz() {
        try {
            return new ResourceBar();
        } catch (Exception ex) {
            Logger.getLogger(StatusBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public DevelopementBar getDevelopementClazz() {
        try {
            return new DevelopementBar();
        } catch (Exception ex) {
            Logger.getLogger(StatusBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
