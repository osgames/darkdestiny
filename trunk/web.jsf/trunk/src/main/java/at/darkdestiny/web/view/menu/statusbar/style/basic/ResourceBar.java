/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.BeanUtilities;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Aion
 */
public class ResourceBar extends BaseBar {

    HashMap<Integer, ResourceBarEntry> entries = Maps.newHashMap();

    public ResourceBarEntry getRessourceBarEntry(int resourceId) {

        if (!entries.containsKey(resourceId)) {
            entries.put(resourceId, new ResourceBarEntry(resourceId));
        }
        return entries.get(resourceId);
    }

    public HashSet<Integer> getResourceIds() {

        HashSet set = Sets.newHashSet();
        for (Ressource r : Service.ressourceDAO.findAllStoreableRessources()) {
            if (r.getId() == Ressource.FOOD) {
                continue;
            }
            set.add(r.getId());
        }
        return set;
    }
    public HashSet<Integer> getRawResourceIds() {

        HashSet set = Sets.newHashSet();
        for (Ressource r : Service.ressourceDAO.findAllMineableRessources()) {
            set.add(r.getId());
        }
        return set;
    }
}
