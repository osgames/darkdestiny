/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.production;

import at.darkdestiny.web.view.construction.*;
import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.result.BuildableResult;
import at.darkdestiny.core.result.InConstructionEntry;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.web.bean.sites.ConstructionBean;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.view.ViewData;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class ProductionViewData implements ViewData {

    SessionContext sessionContext;

    private List<InConstructionEntry> inProductionLocalEntries;
    private List<InConstructionEntry> inProductionRemoteEntries;

    public ProductionViewData() {

    }

    /**
     * @return the inProductionLocalEntries
     */
    public List<InConstructionEntry> getInProductionLocalEntries() {
        return inProductionLocalEntries;
    }

    /**
     * @param inConstructionEntires the inProductionLocalEntries to set
     */
    public void setInProductionEntries(List<InConstructionEntry> inConstructionEntires) {
        this.inProductionLocalEntries = inConstructionEntires;
    }


   

    /**
     * @return the inProductionRemoteEntries
     */
    public List<InConstructionEntry> getInProductionRemoteEntries() {
        return inProductionRemoteEntries;
    }

    /**
     * @param inProductionRemoteEntries the inProductionRemoteEntries to set
     */
    public void setInProductionRemoteEntries(List<InConstructionEntry> inProductionRemoteEntries) {
        this.inProductionRemoteEntries = inProductionRemoteEntries;
    }

}
