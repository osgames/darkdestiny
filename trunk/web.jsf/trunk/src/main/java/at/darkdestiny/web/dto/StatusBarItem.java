/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.dto;

/**
 *
 * @author LAZYPAD
 */
public class StatusBarItem {

    private String template;
    private boolean header;

    public StatusBarItem(String template, boolean header) {
        this.template = template;
        this.header = header;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @return the header
     */
    public boolean isHeader() {
        return header;
    }
}
