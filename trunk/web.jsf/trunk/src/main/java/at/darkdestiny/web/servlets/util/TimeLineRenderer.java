/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.servlets.util;

import at.darkdestiny.core.GameUtilities;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.utilities.img.IModifyImageFunction;
import at.darkdestiny.web.dto.TimeLineSelection;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author LAZYPAD
 */
public class TimeLineRenderer implements IModifyImageFunction {

    private int userId;
    private int width;
    private int height;
    private Calendar roundStartDate;
    private Calendar selDate;
    private Calendar actDate;
    private int yearsToDisplay;

    public TimeLineRenderer(TimeLineSelection timeLineSelection) {
        this.userId = timeLineSelection.getUserId();
        this.selDate = timeLineSelection.getSelectedDate();
        this.yearsToDisplay = timeLineSelection.getYearsToDisplay();
    }

    @Override
    public void setSize(int width, int height) {

        this.width = width;
        this.height = height;
    }


    public void setWidth(int width) {

        this.width = width;
    }


    @Override
    public void run(Graphics g) {


        this.actDate = GameUtilities.getDate();
        if (selDate == null) {
            selDate = actDate;
        }
        int selYear = selDate.get(Calendar.YEAR);
        if (roundStartDate == null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(Service.gameDataDAO.findAll().get(0).getStartTime());
            roundStartDate = gc;
        }


        int startYear = selYear - yearsToDisplay / 2;
        int timeLineOffset = 20;
        int timeLineWidth = width - 2 * timeLineOffset;

        double pixelPerYear = timeLineWidth / (double) yearsToDisplay;

        System.out.println("ppy : " + pixelPerYear);

        int horizontalMiddle = (int) (height / 2d);
        int verticalMiddle = (int) (timeLineWidth / 2d);
        int whiskersOffsetYear = 2;
        int whiskersOffsetFive = 3;
        int whiskersOffsetDecade = 4;
        int whiskersOffsetTwentyFive = 5;
        int whiskersOffsetFifty = 6;

        int offsetString = 13;

        int barHeight = 4;

        g.setColor(Color.BLUE);
        g.fillRect(timeLineOffset, horizontalMiddle - barHeight / 2, timeLineWidth, barHeight);

        Font f = new Font("Tahoma", Font.BOLD, 9);

        g.setFont(f);
        g.setColor(Color.WHITE);

        int markerOffset = 0;

        int year = startYear;
        for (double d = 0; d < timeLineWidth; d += pixelPerYear) {
            if (year < roundStartDate.get(Calendar.YEAR)) {
                year++;
                continue;
            }
            boolean drawLabel = false;
            if (yearsToDisplay < 15) {
                drawLabel = true;
            }
            int whiskersOffset = whiskersOffsetYear;
            if (year % 5 == 0) {
                whiskersOffset = whiskersOffsetFive;
                if (yearsToDisplay < 75) {
                    drawLabel = true;
                }
            }
            if (year % 10 == 0) {
                whiskersOffset = whiskersOffsetDecade;
                if (yearsToDisplay < 150) {
                    drawLabel = true;
                }
            }
            if (year % 25 == 0) {
                whiskersOffset = whiskersOffsetTwentyFive;
                if (yearsToDisplay < 300 && yearsToDisplay >= 150) {
                    drawLabel = true;
                }
            }
            if (year % 50 == 0) {
                if (yearsToDisplay < 500) {
                    drawLabel = true;
                }
                whiskersOffset = whiskersOffsetFifty;
            }

            if (year % 100 == 0) {
                if (yearsToDisplay >= 500) {
                    drawLabel = true;
                }
                whiskersOffset = whiskersOffsetFifty;
            }
            if (drawLabel) {
                String yearString = String.valueOf(year);
                g.drawString(yearString, (int) (d + timeLineOffset - g.getFontMetrics().stringWidth(yearString) / 2d), horizontalMiddle + offsetString);

            }
            if (year == actDate.get(Calendar.YEAR)) {
                markerOffset = (int) d + timeLineOffset;
            }

            g.drawLine((int) d + timeLineOffset, horizontalMiddle - whiskersOffset, (int) d + timeLineOffset, horizontalMiddle + whiskersOffset);
            year++;
        }

        if (markerOffset > 0) {
            g.setColor(Color.RED);
            int markerWidth = 7;
            int markerHeight = 5;
            Polygon p = new Polygon();
            p.addPoint(markerOffset + timeLineOffset - markerWidth / 2, 0);
            p.addPoint(markerOffset + timeLineOffset + markerWidth / 2, 0);
            p.addPoint(markerOffset + timeLineOffset, markerHeight);
            g.fillPolygon(p);
        }

    }

    @Override
    public String createMapEntry() {
        System.out.println("Not supported yet.");
        return "";
        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
