/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.enumeration.ETradeRouteType;
import at.darkdestiny.core.model.Ressource;
import at.darkdestiny.core.service.TransportService;
import at.darkdestiny.web.BeanUtilities;

/**
 *
 * @author Aion
 */
public class FoodBar extends ResourceBarEntry {

    String foodImageEqual = "pic/menu/foodEqual.png";

    public FoodBar() {
        super(Ressource.FOOD);

    }

    @Override
    public String getBackgroundImage() {
        return "pic/menu/statusbar/stb_food_background.png";
    }

    @Override
    public String getIcon() {
        return "pic/menu/foodicon.png";
    }

    public String getFood() {

        return FormatUtilities.getFormattedNumber(planetCalculation.getPlanetCalcData().getProductionData().getRessStock(Ressource.FOOD), BeanUtilities.getLocale());
    }

    public String getProductionImage() {

        String image = "pic/menu/foodProduction.png";
        if (increase <= decrease) {
            image = foodImageEqual;
        }

        return image;
    }

    public String getProductionWidth() {
        //Production is reference
        return getProductionWidthValue() + "px";
    }

    public int getProductionWidthValue() {
        if (increase > decrease) {
            return maxLength;
        } else {
            if (decrease > 0) {
                double multiplicator = increase / decrease;
                return (int) (maxLength * multiplicator);
            } else {
                return 1;
            }
        }

    }

    public String getProductionLeft() {

        int offset = getBarOffset(getProductionWidthValue());
        return offset + "px";
    }

    public String getProductionZIndex() {
        if (increase > decrease) {
            return "1";
        } else {
            return "2";
        }
    }

    public String getConsumptionImage() {

        String image = "pic/menu/foodRed.png";
        if (increase >= decrease) {
            image = foodImageEqual;
        }

        return image;
    }

    public String getConsumptionWidth() {
        //Consumption is reference
        return getConsumptionWidthValue() + "px";
    }

    public int getConsumptionWidthValue() {
        if (increase <= decrease) {
            return maxLength;
        } else {
            double multiplicator = increase / decrease;
            return (int) (maxLength * multiplicator);
        }

    }

    public String getConsumptionLeft() {

        int offset = getBarOffset(getConsumptionWidthValue());
        return offset + "px";
    }

    public String getConsumptionZIndex() {
        if (increase < decrease) {
            return "1";
        } else {
            return "2";
        }
    }
}
