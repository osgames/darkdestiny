/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.management;

import javax.validation.constraints.Size;

/**
 *
 * @author Admin
 */
public class ResourceValue {

    private final int resourceId;

    @Size(min = 3, max = 15, message = "Stock needs to be between 3 and 15")
    private long minProductionStock;
    @Size(min = 3, max = 15, message = "Stock needs to be between 3 and 15")
    private long minTransportStock;
    @Size(min = 3, max = 15, message = "Stock needs to be between 3 and 15")
    private long minTradeStock;
    private final String imageLocation;

    ResourceValue(Integer resourceId, String imageLocation, long minProductionStock, long minTransportStock, long minTradeStock) {
        this.resourceId = resourceId;
        this.minProductionStock = minProductionStock;
        this.minTransportStock = minTradeStock;
        this.minTradeStock = minTradeStock;
        this.imageLocation = imageLocation;
    }

    /**
     * @return the resourceId
     */
    public int getResourceId() {
        return resourceId;
    }

    /**
     * @return the minProductionStock
     */
    public long getMinProductionStock() {
        return minProductionStock;
    }

    /**
     * @return the minTransportStock
     */
    public long getMinTransportStock() {
        return minTransportStock;
    }

    /**
     * @return the minTradeStock
     */
    public long getMinTradeStock() {
        return minTradeStock;
    }

    /**
     * @return the imageLocation
     */
    public String getImageLocation() {
        return imageLocation;
    }

    /**
     * @param minProductionStock the minProductionStock to set
     */
    public void setMinProductionStock(long minProductionStock) {
        this.minProductionStock = minProductionStock;
    }

    /**
     * @param minTransportStock the minTransportStock to set
     */
    public void setMinTransportStock(long minTransportStock) {
        this.minTransportStock = minTransportStock;
    }

    /**
     * @param minTradeStock the minTradeStock to set
     */
    public void setMinTradeStock(long minTradeStock) {
        this.minTradeStock = minTradeStock;
    }

}
