/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.construction;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.result.BuildableResult;
import at.darkdestiny.core.result.InConstructionEntry;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.web.bean.sites.ConstructionBean;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.view.ViewData;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class ConstructionViewData implements ViewData {

    SessionContext sessionContext;

    private List<InConstructionEntry> inConstructionEntries;
    private TreeMap<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>> planetConstructions;

    public ConstructionViewData() {

    }

    /**
     * @return the inConstructionEntries
     */
    public List<InConstructionEntry> getInConstructionEntries() {
        return inConstructionEntries;
    }

    /**
     * @param inConstructionEntires the inConstructionEntries to set
     */
    public void setInConstructionEntires(List<InConstructionEntry> inConstructionEntires) {
        this.inConstructionEntries = inConstructionEntires;
    }

    /**
     * @param possibleConstructionEntries the planetConstructions to set
     */
    public void setPlanetConstructions(TreeMap<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>> possibleConstructionEntries) {
        this.planetConstructions = possibleConstructionEntries;
    }

    public ArrayList<PlanetConstructionView> getConstructionEntries(EConstructionType type) {
        ArrayList<PlanetConstructionView> constructions = Lists.newArrayList();
        if (this.planetConstructions != null && this.planetConstructions.get(type) != null) {
            for (Map.Entry<Integer, ArrayList<PlanetConstruction>> entry : this.planetConstructions.get(type).entrySet()) {
                for (PlanetConstruction pc : entry.getValue()) {
                    constructions.add(new PlanetConstructionView(pc));
                }
            }
        }
        return constructions;
    }

}
