/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.construction;

import at.darkdestiny.web.view.ViewData;

/**
 *
 * @author Admin
 */
public class InBuildView {

    private String name;
    private String description;
    private Integer id;
    private String image;

    public InBuildView(String name, String description, Integer id, String image) {
        this.name = name;
        this.description = description;
        this.id = id;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

}
