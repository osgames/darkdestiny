/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.management;

import at.darkdestiny.web.view.ViewData;

/**
 *
 * @author Admin
 */
public class EconomyValues implements ViewData {

    private Integer priorityIndustry;
    private Integer priorityResearch;
    private Integer priorityAgriculture;
    private final double efficiencyIndustry;
    private final double efficiencyResearch;
    private final double efficiencyAgriculture;

    private int taxes;
    private final int taxesBase;
    private final int taxesRiots;
    private final int taxesCriminality;
    private final int taxesTotal;

    public EconomyValues(Integer priorityIndustry, Integer priorityResearch, Integer priorityAgriculture,
            double efficiencyIndustry, double efficiencyResearch, double efficiencyAgriculture,
            int taxes, int taxesBase, int taxesRiots, int taxesCriminality, int taxesTotal) {
        this.priorityIndustry = priorityIndustry;
        this.priorityResearch = priorityResearch;
        this.priorityAgriculture = priorityAgriculture;
        this.efficiencyAgriculture = efficiencyAgriculture;
        this.efficiencyIndustry = efficiencyIndustry;
        this.efficiencyResearch = efficiencyResearch;

        this.taxes = taxes;
        this.taxesBase = taxesBase;
        this.taxesRiots = taxesRiots;
        this.taxesCriminality = taxesCriminality;
        this.taxesTotal = taxesTotal;
    }

    /**
     * @return the priorityIndustry
     */
    public Integer getPriorityIndustry() {
        return priorityIndustry;
    }

    /**
     * @return the priorityResearch
     */
    public Integer getPriorityResearch() {
        return priorityResearch;
    }

    /**
     * @return the priorityAgriculture
     */
    public Integer getPriorityAgriculture() {
        return priorityAgriculture;
    }

    /**
     * @return the efficiencyIndustry
     */
    public double getEfficiencyIndustry() {
        return efficiencyIndustry;
    }

    /**
     * @return the efficiencyResearch
     */
    public double getEfficiencyResearch() {
        return efficiencyResearch;
    }

    /**
     * @return the efficiencyAgriculture
     */
    public double getEfficiencyAgriculture() {
        return efficiencyAgriculture;
    }

    /**
     * @return the taxes
     */
    public int getTaxes() {
        return taxes;
    }

    /**
     * @return the taxesBase
     */
    public int getTaxesBase() {
        return taxesBase;
    }

    /**
     * @return the taxesRiots
     */
    public int getTaxesRiots() {
        return taxesRiots;
    }

    /**
     * @return the taxesCriminality
     */
    public int getTaxesCriminality() {
        return taxesCriminality;
    }

    /**
     * @return the taxesTotal
     */
    public int getTaxesTotal() {
        return taxesTotal;
    }

    /**
     * @param taxes the taxes to set
     */
    public void setTaxes(int taxes) {
        this.taxes = taxes;
    }

    /**
     * @param priorityIndustry the priorityIndustry to set
     */
    public void setPriorityIndustry(Integer priorityIndustry) {
        this.priorityIndustry = priorityIndustry;
    }

    /**
     * @param priorityResearch the priorityResearch to set
     */
    public void setPriorityResearch(Integer priorityResearch) {
        this.priorityResearch = priorityResearch;
    }

    /**
     * @param priorityAgriculture the priorityAgriculture to set
     */
    public void setPriorityAgriculture(Integer priorityAgriculture) {
        this.priorityAgriculture = priorityAgriculture;
    }

}
