/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "HelpBean")
@SessionScoped
public class HelpBean implements Serializable {

    private boolean showHelp = false;

    public void switchHelp() {
        showHelp = !showHelp;
    }

    /**
     * @return the showHelp
     */
    public boolean isShowHelp() {
        return showHelp;
    }

    /**
     * @param showHelp the showHelp to set
     */
    public void setShowHelp(boolean showHelp) {
        this.showHelp = showHelp;
    }
}
