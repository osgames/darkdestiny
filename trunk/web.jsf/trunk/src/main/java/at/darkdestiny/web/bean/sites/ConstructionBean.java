/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.result.InConstructionEntry;
import at.darkdestiny.core.result.InConstructionResult;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.service.ProductionService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.view.construction.ConstructionViewData;
import at.darkdestiny.web.view.construction.PlanetConstructionView;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "ConstructionBean")
@RequestScoped
public class ConstructionBean implements JsfBean {

    private int constructCount = 0;
    private int constructionId = 0;


    private ConstructionViewData viewData;

    public ConstructionBean() {
    }

    public void buildConstructions() {
        SessionContext sessionContext = SessionContextHolder.getSessionContext();
        int planetId = sessionContext.getSelectedPlanetId();
        int userId = sessionContext.getUser().getId();

        if (this.constructionId > 0 && this.constructCount > 0) {
            ConstructionService.buildBuilding(userId, planetId,
                    constructionId, constructCount);

            this.constructCount = 0;
            this.constructionId = 0;
        }
        this.calcViewData();
    }

    public void updateIdle() {
        for (EConstructionType type : EConstructionType.values()) {
            for (PlanetConstructionView pcv : viewData.getConstructionEntries(type)) {
                Service.planetConstructionDAO.update(pcv.getPlanetConstruction());
            }
        }
    }

    @Override
    public ConstructionViewData getViewData() {
        if (viewData == null) {
            calcViewData();
        }
        return viewData;
    }

    private void calcViewData() {
        SessionContext sessionContext = SessionContextHolder.getSessionContext();
        int planetId = sessionContext.getSelectedPlanetId();
        int userId = sessionContext.getUser().getId();

        this.viewData = new ConstructionViewData();
        InConstructionResult icr = ConstructionService.findRunnigConstructions(planetId);
        List<InConstructionEntry> constructions = icr.getEntries();
        this.viewData.setInConstructionEntires(constructions);

        TreeMap<EConstructionType, TreeMap<Integer, ArrayList<PlanetConstruction>>> planetConstructions = ConstructionService.findPlanetConstructions(planetId);
        this.viewData.setPlanetConstructions(planetConstructions);


    }

    public ArrayList<SelectItem> getPriorityOptions() {
        ArrayList<SelectItem> result = Lists.newArrayList();

        for (int i = 0; i < 10; i++) {
            result.add(new SelectItem(i, String.valueOf(i)));
        }
        return result;

    }

    public void updatePriorities() {
        System.out.println("update constrcution priorities");
        for (InConstructionEntry entry : this.viewData.getInConstructionEntries()) {
            ProductionOrder productionOrder = Service.productionOrderDAO.findById(entry.getOrder().getId());
            productionOrder.setPriority(entry.getOrder().getPriority());
            Service.productionOrderDAO.update(productionOrder);
        }
        this.calcViewData();
    }

    /**
     * @return the constructCount
     */
    public int getConstructCount() {
        return constructCount;
    }

    /**
     * @param constructCount the constructCount to set
     */
    public void setConstructCount(int constructCount) {
        this.constructCount = constructCount;
    }

    /**
     * @return the constructionId
     */
    public int getConstructionId() {
        return constructionId;
    }

    /**
     * @param constructionId the constructionId to set
     */
    public void setConstructionId(int constructionId) {
        this.constructionId = constructionId;
    }

}
