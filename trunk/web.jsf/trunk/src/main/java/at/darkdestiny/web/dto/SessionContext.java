/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.dto;

import at.darkdestiny.core.model.User;
import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.core.view.header.HeaderData;
import java.io.Serializable;
import java.util.Locale;

/**
 *
 * @author desertratx
 */
public class SessionContext implements Serializable {

    public static final String KEY = "SessionContext";
    private User user;
    private Integer selectedPlanetId;
    private Integer selectedSystemId;
    private Integer selectedCategoryId;
    private Integer homeSystemId;
    private PlanetCalculation planetCalculationActPlanet;
    private Locale locale;
    private boolean localeSetManually = false;
    private TimeLineSelection timeLineSelection;

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the planetCalculationActPlanet
     */
    public PlanetCalculation getPlanetCalculationActPlanet() {
        return planetCalculationActPlanet;
    }

    /**
     * @param planetCalculationActPlanet the planetCalculationActPlanet to set
     */
    public void setPlanetCalculationActPlanet(PlanetCalculation planetCalculationActPlanet) {
        this.planetCalculationActPlanet = planetCalculationActPlanet;
    }

    /**
     * @return the selectedPlanetId
     */
    public Integer getSelectedPlanetId() {
        return selectedPlanetId;
    }

    /**
     * @param selectedPlanetId the selectedPlanetId to set
     */
    public void setSelectedPlanetId(Integer selectedPlanetId) {
        this.selectedPlanetId = selectedPlanetId;
    }

    /**
     * @return the selectedSystemId
     */
    public Integer getSelectedSystemId() {
        return selectedSystemId;
    }

    /**
     * @param selectedSystemId the selectedSystemId to set
     */
    public void setSelectedSystemId(Integer selectedSystemId) {
        this.selectedSystemId = selectedSystemId;
    }

    /**
     * @return the homeSystemId
     */
    public Integer getHomeSystemId() {
        return homeSystemId;
    }

    /**
     * @param homeSystemId the homeSystemId to set
     */
    public void setHomeSystemId(Integer homeSystemId) {
        this.homeSystemId = homeSystemId;
    }

    /**
     * @return the selectedCategoryId
     */
    public Integer getSelectedCategoryId() {
        return selectedCategoryId;
    }

    /**
     * @param selectedCategoryId the selectedCategoryId to set
     */
    public void setSelectedCategoryId(Integer selectedCategoryId) {
        this.selectedCategoryId = selectedCategoryId;
    }

    public boolean isLogged() {
        return (user != null);
    }

    public boolean isAdminLogged() {
        return (user != null && user.getAdmin());
    }

    /**
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * @param locale the locale to set
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * @return the localeSetManually
     */
    public boolean isLocaleSetManually() {
        return localeSetManually;
    }

    /**
     * @param localeSetManually the localeSetManually to set
     */
    public void setLocaleSetManually(boolean localeSetManually) {
        this.localeSetManually = localeSetManually;
    }

    /**
     * @return the timeLineSelection
     */
    public TimeLineSelection getTimeLineSelection() {
        return timeLineSelection;
    }

    /**
     * @param timeLineSelection the timeLineSelection to set
    */
    public void setTimeLineSelection(TimeLineSelection timeLineSelection) {
        this.timeLineSelection = timeLineSelection;
    }
}
