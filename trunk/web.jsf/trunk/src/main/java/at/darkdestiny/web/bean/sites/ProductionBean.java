/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.enumeration.EConstructionType;
import at.darkdestiny.core.model.ProductionOrder;
import at.darkdestiny.core.result.InConstructionEntry;
import at.darkdestiny.core.result.InConstructionResult;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.service.ProductionService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;
import at.darkdestiny.web.view.production.ProductionViewData;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "ProductionBean")
@RequestScoped
public class ProductionBean implements JsfBean {

    private int constructCount = 0;
    private int constructionId = 0;

    private ProductionViewData viewData;

    public ProductionBean() {
    }

    public void buildProductions() {
        System.out.println("In build Productions");
        SessionContext sessionContext = SessionContextHolder.getSessionContext();
        int planetId = sessionContext.getSelectedPlanetId();
        int userId = sessionContext.getUser().getId();

        if (this.constructionId > 0 && this.constructCount > 0) {
            Map<String, String[]> params = Maps.newHashMap();
            params.put("build" + String.valueOf(constructionId), new String[]{String.valueOf(constructCount)});
            ProductionService.buildGroundTroop(userId, planetId, params);
            this.constructCount = 0;
            this.constructionId = 0;
        }
        this.calcViewData();
    }

    @Override
    public ProductionViewData getViewData() {
        if (viewData == null) {
            calcViewData();
        }
        return viewData;
    }

    private void calcViewData() {
        SessionContext sessionContext = SessionContextHolder.getSessionContext();
        int planetId = sessionContext.getSelectedPlanetId();

        this.viewData = new ProductionViewData();

        InConstructionResult inConsLocal = ProductionService.getInConstructionData(planetId);
        InConstructionResult inConsRemote = ProductionService.getInConstructionDataOtherPlanets(sessionContext.getUser().getId(), planetId);

        this.viewData.setInProductionEntries(inConsLocal.getEntries());
        this.viewData.setInProductionRemoteEntries(inConsRemote.getEntries());

    }

    public ArrayList<SelectItem> getPriorityOptions() {
        ArrayList<SelectItem> result = Lists.newArrayList();

        for (int i = 0; i < 10; i++) {
            result.add(new SelectItem(i, String.valueOf(i)));
        }
        return result;

    }

    public void updatePriorities() {
        System.out.println("Updating production priorities");
        for (InConstructionEntry entry : this.viewData.getInProductionLocalEntries()) {
            ProductionOrder productionOrder = Service.productionOrderDAO.findById(entry.getOrder().getId());
            productionOrder.setPriority(entry.getOrder().getPriority());
            Service.productionOrderDAO.update(productionOrder);
        }
        for (InConstructionEntry entry : this.viewData.getInProductionRemoteEntries()) {
            ProductionOrder productionOrder = Service.productionOrderDAO.findById(entry.getOrder().getId());
            productionOrder.setPriority(entry.getOrder().getPriority());
            Service.productionOrderDAO.update(productionOrder);
        }
        this.calcViewData();
    }

    /**
     * @return the constructCount
     */
    public int getConstructCount() {
        return constructCount;
    }

    /**
     * @param constructCount the constructCount to set
     */
    public void setConstructCount(int constructCount) {
        this.constructCount = constructCount;
    }

    /**
     * @return the constructionId
     */
    public int getConstructionId() {
        return constructionId;
    }

    /**
     * @param constructionId the constructionId to set
     */
    public void setConstructionId(int constructionId) {
        this.constructionId = constructionId;
    }

}
