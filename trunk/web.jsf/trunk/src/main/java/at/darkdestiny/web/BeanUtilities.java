/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web;

import at.darkdestiny.core.model.User;
import at.darkdestiny.web.bean.MLBean;
import java.util.Locale;
import javax.faces.context.FacesContext;

/**
 *
 * @author LAZYPAD
 */
public class BeanUtilities {

    public static User getLoggedUser() {
        return SessionContextHolder.getSessionContext().getUser();

    }

    public static String getMLStr(String key) {

        MLBean mlBean = (MLBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("MLBean");
        if (mlBean == null) {
            mlBean = new MLBean();
            FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().put("MLBean", mlBean);
        }
        return mlBean.getML(key);
    }

    public static String getMLStr(String key, String paramsString) {

        MLBean mlBean = (MLBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("MLBean");
        return mlBean.getML(key, paramsString);
    }

    public static Locale getLocale() {
        return MLBean.getLocale();

    }
}
