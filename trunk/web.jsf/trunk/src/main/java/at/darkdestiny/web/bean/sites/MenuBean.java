/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.model.MenuLink;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.model.UserData;
import at.darkdestiny.core.model.UserSettings;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.core.viewbuffer.MenuBuffer;
import at.darkdestiny.web.BeanUtilities;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "MenuBean")
@SessionScoped
public class MenuBean implements Serializable {

    public ArrayList<MenuLink> getMenu() {
        User user = BeanUtilities.getLoggedUser();
        UserData userData = Service.userDataDAO.findByUserId(user.getUserId());
        UserSettings userSettings = Service.userSettingsDAO.findByUserId(user.getUserId());

        ArrayList<MenuLink> result = Lists.newArrayList();

        for (MenuLink menuLink : MenuBuffer.getMenuLinks()) {
            if (menuLink.getUrl() == null) {
                continue;
            }
            if (menuLink.getLinkName().equals("menu_welcome") && (userData == null || !userSettings.getShowWelcomeSite())) {
                continue;
            }

            if (menuLink.getAdminOnly() && (!user.getAdmin())) {
                continue;
            }
            result.add(menuLink);
        }
        return result;
    }

    public String management() {
        System.out.println("management() invoked");
        return "/sites/management/template?faces-redirect=true";

    }

    public String messages() {
        System.out.println("messages() invoked");
        return "/sites/messages/template?faces-redirect=true";

    }
}
