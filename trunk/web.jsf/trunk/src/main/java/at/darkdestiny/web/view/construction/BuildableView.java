/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.construction;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.result.BuildableResult;

/**
 *
 * @author Admin
 */
public class BuildableView {

    private Buildable buildable;
    private BuildableResult buildableResult;
    private int constructCount = 0;


    public BuildableView(Buildable buildable, BuildableResult buildableResult){
        this.buildable = buildable;
        this.buildableResult = buildableResult;
    }

    /**
     * @return the buildable
     */
    public Buildable getBuildable() {
        return buildable;
    }

    /**
     * @return the buildableResult
     */
    public BuildableResult getBuildableResult() {
        return buildableResult;
    }

    /**
     * @return the constructCount
     */
    public int getConstructCount() {
        return constructCount;
    }

    /**
     * @param constructCount the constructCount to set
     */
    public void setConstructCount(int constructCount) {
        this.constructCount = constructCount;
    }


}
