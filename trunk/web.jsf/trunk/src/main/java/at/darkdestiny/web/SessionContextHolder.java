/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web;

import at.darkdestiny.web.dto.SessionContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author LAZYPAD
 */
public class SessionContextHolder {

    public static SessionContext getSessionContext() {
        SessionContext sessionContext = (SessionContext) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get(SessionContext.KEY);
        if (sessionContext == null) {
            sessionContext = new SessionContext();
            FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().put(SessionContext.KEY, sessionContext);
        }
        return sessionContext;
    }

    public static void updateSessionContext(SessionContext sessionContext) {
        FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().put(SessionContext.KEY, sessionContext);
    }

    public static void removeSessionContext() {
        FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().remove(SessionContext.KEY);
    }

    public static String getSessionId() {

        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);

        return session.getId();
    }
}
