/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view;

import at.darkdestiny.web.view.construction.*;
import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.result.BuildableResult;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.web.bean.sites.BuildableBean.EBuildableViewType;
import at.darkdestiny.web.bean.sites.ConstructionBean;
import at.darkdestiny.web.dto.SessionContext;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class BuildableViewData implements ViewData {

    SessionContext sessionContext;

    private Map<EBuildableViewType, ProductionBuildResult> buildableMap = Maps.newLinkedHashMap();

    public BuildableViewData() {

    }

    public void addProductionBuildResult(EBuildableViewType type, ProductionBuildResult result) {
        this.buildableMap.put(type, result);
    }

    public List<BuildableView> getProductionBuildResultBuildable(EBuildableViewType type, boolean buildable) {

        List<BuildableView> result = Lists.newArrayList();
        if (type.equals(EBuildableViewType.SHIPS) || type.equals(EBuildableViewType.GROUNDTROOPS)) {
            for (Map.Entry<Buildable, BuildableResult> entry : this.buildableMap.get(type).getAvailUnits().entrySet()) {
                result.add(new BuildableView(entry.getKey(), entry.getValue()));
            }
        } else {
            for (Map.Entry<Buildable, BuildableResult> entry : this.buildableMap.get(type).getUnitsSorted(buildable).entrySet()) {
                result.add(new BuildableView(entry.getKey(), entry.getValue()));
            }
        }
        return result;
    }

}
