/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.construction;

import at.darkdestiny.core.Buildable;
import at.darkdestiny.core.model.Construction;
import at.darkdestiny.core.model.PlanetConstruction;
import at.darkdestiny.core.result.BuildableResult;
import at.darkdestiny.core.result.ProductionBuildResult;
import at.darkdestiny.core.service.ConstructionService;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.BeanUtilities;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class PlanetConstructionView {

    TreeMap<Object, HashMap<Buildable, BuildableResult>> cons1;
    private PlanetConstruction planetConstruction;
    private String constructionName;
    private Construction construction;

    public PlanetConstructionView(PlanetConstruction planetConstruction) {
        this.construction = Service.constructionDAO.findById(planetConstruction.getConstructionId());
        this.constructionName = BeanUtilities.getMLStr(construction.getName());
        this.planetConstruction = planetConstruction;

    }

    /**
     * @return the constructionName
     */
    public String getConstructionName() {
        return constructionName;
    }

    /**
     * @param constructionName the constructionName to set
     */
    public void setConstructionName(String constructionName) {
        this.constructionName = constructionName;
    }

    /**
     * @return the planetConstruction
     */
    public PlanetConstruction getPlanetConstruction() {
        return planetConstruction;
    }

    /**
     * @param planetConstruction the planetConstruction to set
     */
    public void setPlanetConstruction(PlanetConstruction planetConstruction) {
        this.planetConstruction = planetConstruction;
    }

    /**
     * @return the construction
     */
    public Construction getConstruction() {
        return construction;
    }

    /**
     * @param construction the construction to set
     */
    public void setConstruction(Construction construction) {
        this.construction = construction;
    }

}
