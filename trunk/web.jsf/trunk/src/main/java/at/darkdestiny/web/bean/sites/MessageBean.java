/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean.sites;

import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.GenerateMessage;
import at.darkdestiny.core.enumeration.EMessageFolder;
import at.darkdestiny.core.enumeration.EMessageType;
import at.darkdestiny.core.model.Message;
import at.darkdestiny.core.model.User;
import at.darkdestiny.core.result.BaseResult;
import at.darkdestiny.core.service.Service;
import at.darkdestiny.web.enumerations.EMessageAction;
import at.darkdestiny.web.BeanUtilities;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.bean.MessageContext;
import at.darkdestiny.web.dto.MessageListEntry;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import org.slf4j.LoggerFactory;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "MessageBean")
@SessionScoped
public class MessageBean {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MessageBean.class);
    private EMessageAction messageAction;
    private Message newMessage = new Message();
    private Message messageToShow;
    private ArrayList<User> users;
    private String[] receivers;
    private String[] selectedMessages;
    private int messageId;
    private ArrayList<Message> messages;
    private Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
    String type;
    private String answerText;
    private String answerTopic;

    public void setInboxMessages() {
        setMessages(Service.messageDAO.findInboxMessages(SessionContextHolder.getSessionContext().getUser().getId()));

    }

    public void setOutboxMessages() {
        setMessages(Service.messageDAO.findOutboxMessages(SessionContextHolder.getSessionContext().getUser().getId()));

    }

    public void setArchivedMessages() {
        setMessages(Service.messageDAO.findArchivedMessages(SessionContextHolder.getSessionContext().getUser().getId()));

    }

    public ArrayList<User> getUsers() {
        if (users == null) {
            setUsers(Service.userDAO.findAllSortedByName());
        }
        return users;
    }

    public void test(String test) {
        System.out.println("Testit: " + test);
        MessageContext.getCurrentInstance().addMessage(new BaseResult(test, true));
    }

    public String getUnreadMessageString() {
        ArrayList<Message> messages = Service.messageDAO.findInboxMessages(SessionContextHolder.getSessionContext().getUser().getId());
        int count = 0;
        for (Message message : messages) {
            if (!message.getViewed()) {
                count++;
            }
        }
        if (count > 0) {
            return " (" + count + ")";
        } else {
            return "";
        }
    }

    public ArrayList<SelectItem> getActionOptions(String type) {
        ArrayList<SelectItem> result = Lists.newArrayList();

        result.add(new SelectItem(EMessageAction.ACTION_DELETE_SELECTED, BeanUtilities.getMLStr(EMessageAction.ACTION_DELETE_SELECTED.toString())));
        if (type.equals("inbox")) {
            result.add(new SelectItem(EMessageAction.ACTION_MOVE_SELECTED, BeanUtilities.getMLStr(EMessageAction.ACTION_MOVE_SELECTED.toString())));
            result.add(new SelectItem(EMessageAction.ACTION_DELETE_SYSTEM, BeanUtilities.getMLStr(EMessageAction.ACTION_DELETE_SYSTEM.toString())));
            result.add(new SelectItem(EMessageAction.ACTION_DELETE_PLAYER, BeanUtilities.getMLStr(EMessageAction.ACTION_DELETE_PLAYER.toString())));
            result.add(new SelectItem(EMessageAction.ACTION_DELETE_ALLIANCE, BeanUtilities.getMLStr(EMessageAction.ACTION_DELETE_ALLIANCE.toString())));
        } else if (type.equals("outbox")) {
            //Already added
        } else if (type.equals("archive")) {
            //already adde
        }

        result.add(new SelectItem(EMessageAction.ACTION_DELETE_ALL, BeanUtilities.getMLStr(EMessageAction.ACTION_DELETE_ALL.toString())));
        return result;

    }

    public void archiveMessage() {
        System.out.println("Trying to archive message with id : " + messageId);
        User user = SessionContextHolder.getSessionContext().getUser();
        Message m = Service.messageDAO.findById(messageId);
        if (m == null) {
            MessageContext.getCurrentInstance().addMessage(new BaseResult("Nachricht nicht vorhanden", true));
        } else {
            if (m.getSourceUserId().equals(user.getId())) {
                m.setArchiveBySource(true);
            }
            if (m.getTargetUserId().equals(user.getId())) {
                m.setArchiveByTarget(true);
            }
            m.setViewed(true);
            Service.messageDAO.update(m);
            MessageContext.getCurrentInstance().addMessage(new BaseResult("Nachricht archiviert", false));
        }

    }

    public void deleteMessage(String type) {
        System.out.println("in delete Message");
        Message m = Service.messageDAO.findById(messageId);
        if (m == null) {
            MessageContext.getCurrentInstance().addMessage(new BaseResult("Nachricht nicht vorhanden", true));
        } else {
            User user = SessionContextHolder.getSessionContext().getUser();
            if (m.getSourceUserId().equals(user.getId())) {
                m.setDelBySource(true);
            }
            if (m.getTargetUserId().equals(user.getId())) {
                m.setDelByTarget(true);
            }
            m.setViewed(true);
            Service.messageDAO.update(m);
            MessageContext.getCurrentInstance().addMessage(new BaseResult("Nachricht geloescht", false));
            System.out.println("Messages before deletion : " + messages.size());
            updateMessages(type);
            System.out.println("Messages after deletion : " + messages.size());
        }
    }

    public void createNewMessage() {
        User user = SessionContextHolder.getSessionContext().getUser();
        if (receivers.length > 5 && !user.getAdmin()) {
            MessageContext.getCurrentInstance().addMessage(new BaseResult("not moar than 5 allowed", true));
            return;
        }

        GenerateMessage gm = new GenerateMessage();
        gm.setIgnoreSmiley(false);

        String topic = newMessage.getTopic();
        String message = newMessage.getText();

        topic = FormatUtilities.killJavaScript(topic);
        message = FormatUtilities.killJavaScript(message);

        gm.setTopic(topic);
        gm.setMsg(message);
        gm.setMasterEntry(true);
        gm.setMessageType(EMessageType.USER);
        gm.setSourceUserId(user.getId());

        for (String uId : receivers) {
            try {
                int id = Integer.parseInt(uId);
                gm.setDestinationUserId(id);
                gm.writeMessageToUser();
                MessageContext.getCurrentInstance().addMessage(new BaseResult("Message sent to : " + Service.userDAO.findById(id).getGameName(), false));
            } catch (Exception e) {
                log.error("Could not cast : " + uId + " to int when sending message to user : " + e);
            }
        }
        newMessage = new Message();
        receivers = null;
    }

    public void executeMessageAction(String type) {
        System.out.println("Compare : " + messageAction + " to : " + EMessageAction.ACTION_DELETE_SELECTED);
        int count = 0;
        for (Message m : messages) {

            boolean check = false;
            if (checked.containsKey(m.getMessageId())) {
                check = checked.get(m.getMessageId());
            }
            System.out.println("m checked : " + check);
            if (type.equals("inbox") || type.equals("archive")) {
                if (messageAction.equals(EMessageAction.ACTION_DELETE_SELECTED) && check) {
                    m.setDelByTarget(true);
                    Service.messageDAO.update(m);
                    count++;
                } else if (messageAction.equals(EMessageAction.ACTION_MOVE_SELECTED) && check) {
                    m.setArchiveByTarget(true);
                    Service.messageDAO.update(m);
                    count++;
                } else if (messageAction.equals(EMessageAction.ACTION_DELETE_PLAYER) && m.getType().equals(EMessageType.USER)) {
                    m.setDelByTarget(true);
                    Service.messageDAO.update(m);
                } else if (messageAction.equals(EMessageAction.ACTION_DELETE_ALLIANCE) && m.getType().equals(EMessageType.ALLIANCE)) {
                    m.setDelByTarget(true);
                    Service.messageDAO.update(m);
                } else if (messageAction.equals(EMessageAction.ACTION_DELETE_SYSTEM) && m.getType().equals(EMessageType.SYSTEM)) {
                    m.setDelByTarget(true);
                    Service.messageDAO.update(m);
                } else if (messageAction.equals(EMessageAction.ACTION_DELETE_ALL)) {
                    m.setDelByTarget(true);
                    Service.messageDAO.update(m);
                }
            } else if (type.equals("outbox")) {
                if (messageAction.equals(EMessageAction.ACTION_DELETE_SELECTED) && check) {
                    m.setDelBySource(true);
                    count++;
                    Service.messageDAO.update(m);
                } else if (messageAction.equals(EMessageAction.ACTION_MOVE_SELECTED) && check) {
                    m.setArchiveBySource(true);
                    count++;
                    Service.messageDAO.update(m);
                } else if (messageAction.equals(EMessageAction.ACTION_DELETE_PLAYER) && m.getType().equals(EMessageType.USER)) {
                    m.setDelBySource(true);
                    Service.messageDAO.update(m);
                } else if (messageAction.equals(EMessageAction.ACTION_DELETE_ALLIANCE) && m.getType().equals(EMessageType.ALLIANCE)) {
                    m.setDelBySource(true);
                    Service.messageDAO.update(m);
                } else if (messageAction.equals(EMessageAction.ACTION_DELETE_SYSTEM) && m.getType().equals(EMessageType.SYSTEM)) {
                    m.setDelBySource(true);
                    Service.messageDAO.update(m);
                } else if (messageAction.equals(EMessageAction.ACTION_DELETE_ALL)) {
                    m.setDelBySource(true);
                    Service.messageDAO.update(m);
                }
            }

        }
        if (count > 0) {
            if (messageAction.equals(EMessageAction.ACTION_DELETE_SELECTED)) {
                MessageContext.getCurrentInstance().addMessage(new BaseResult(count + " Nachrichten gelöscht", false));
            } else if (messageAction.equals(EMessageAction.ACTION_MOVE_SELECTED)) {
                MessageContext.getCurrentInstance().addMessage(new BaseResult(count + " Nachrichten verschoben", false));
            }
        }
        updateMessages(type);
    }

    public void setMessageToDelete(Object object) {
        this.messageId = Integer.parseInt(String.valueOf(object));
    }

    public void setMessageToShowId(Object object) {
        this.messageId = Integer.parseInt(String.valueOf(object));
        System.out.println("set message to show id : " + messageToShow);
        try {
            this.setMessageToShow(Service.messageDAO.findById(messageId));
            this.messageToShow.setViewed(Boolean.TRUE);
            Service.messageDAO.update(this.messageToShow);
            updateMessages(this.type);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @return the newMessage
     */
    public Message getNewMessage() {
        return newMessage;
    }

    /**
     * @param newMessage the newMessage to set
     */
    public void setNewMessage(Message newMessage) {
        this.newMessage = newMessage;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    /**
     * @return the reiceivers
     */
    public String[] getReceivers() {
        return receivers;
    }

    /**
     * @param reiceivers the reiceivers to set
     */
    public void setReceivers(String[] receivers) {
        this.receivers = receivers;
    }

    /**
     * @return the selectedMessages
     */
    public String[] getSelectedMessages() {
        return selectedMessages;
    }

    /**
     * @param selectedMessages the selectedMessages to set
     */
    public void setSelectedMessages(String[] selectedMessages) {
        this.selectedMessages = selectedMessages;
    }

    /**
     * @return the messageId
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the messageAction
     */
    public EMessageAction getMessageAction() {
        return messageAction;
    }

    /**
     * @param messageAction the messageAction to set
     */
    public void setMessageAction(EMessageAction messageAction) {
        this.messageAction = messageAction;
    }

    /**
     * @return the messages
     */
    public ArrayList<Message> getMessages(String type) {
        updateMessages(type);
        return messages;
    }

    public void updateMessages(String type) {
        this.type = type;
        if (type.equals("inbox")) {
            setInboxMessages();
        } else if (type.equals("outbox")) {
            setOutboxMessages();
        } else if (type.equals("archive")) {
            setArchivedMessages();
        }
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    /**
     * @return the messageToShow
     */
    public Message getMessageToShow() {
        return messageToShow;
    }

    /**
     * @param messageToShow the messageToShow to set
     */
    public void setMessageToShow(Message messageToShow) {
        this.messageToShow = messageToShow;
    }

    public boolean hasUnreadInboxMessage() {
        for (Message m : Service.messageDAO.findByUserId(messageId, EMessageFolder.INBOX)) {
            if (!m.getViewed()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return the messageList
     */
    public ArrayList<MessageListEntry> getMessageList(String type) {
        ArrayList<MessageListEntry> messageList = Lists.newArrayList();
        for (Message m : Service.messageDAO.findByUserId(SessionContextHolder.getSessionContext().getUser().getId(), EMessageFolder.INBOX, EMessageType.valueOf(type), false)) {
            messageList.add(new MessageListEntry(EMessageType.valueOf(type), m));
        }
        return messageList;
    }

    /**
     * @return the checked
     */
    public Map<Integer, Boolean> getChecked() {
        return checked;
    }

    /**
     * @param checked the checked to set
     */
    public void setChecked(Map<Integer, Boolean> checked) {
        this.checked = checked;
    }

    public void answer() {
        GenerateMessage gm = new GenerateMessage();
        gm.setDestinationUserId(messageToShow.getSourceUserId());
        gm.setSourceUserId(messageToShow.getTargetUserId());
        gm.setMsg(getAnswerText());
        gm.setTopic(getAnswerTopic());
        gm.setMessageType(EMessageType.USER);
        gm.writeMessageToUser();
        setAnswerText("");
        setAnswerTopic("");
    }

    /**
     * @return the answerText
     */
    public String getAnswerText() {
        return answerText;
    }

    /**
     * @param answerText the answerText to set
     */
    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    /**
     * @return the answerTopic
     */
    public String getAnswerTopic() {
        return answerTopic;
    }

    /**
     * @param answerTopic the answerTopic to set
     */
    public void setAnswerTopic(String answerTopic) {
        this.answerTopic = answerTopic;
    }

    /**
     * @param answerTopic the answerTopic to set
     */
    public void setReAnswerTopic(String answerTopic) {
        this.answerTopic = "RE:" + answerTopic;
    }


}
