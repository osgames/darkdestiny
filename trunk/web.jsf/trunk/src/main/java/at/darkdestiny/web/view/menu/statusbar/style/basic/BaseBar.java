/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.planetcalc.PlanetCalculation;
import at.darkdestiny.web.SessionContextHolder;
import at.darkdestiny.web.dto.SessionContext;

/**
 *
 * @author Aion
 */
public class BaseBar {

    SessionContext sessionContext;
    PlanetCalculation planetCalculation;

    public BaseBar() {
        this.sessionContext = SessionContextHolder.getSessionContext();
        this.planetCalculation = sessionContext.getPlanetCalculationActPlanet();

    }
}
