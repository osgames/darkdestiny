/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.view.menu.statusbar.style.basic;

import at.darkdestiny.core.GameConfig;
import at.darkdestiny.core.FormatUtilities;
import at.darkdestiny.core.model.UserData;
import static at.darkdestiny.core.service.Service.userDataDAO;
import at.darkdestiny.web.BeanUtilities;
import at.darkdestiny.web.dto.SessionContext;

/**
 *
 * @author Aion
 */
public class CreditBar extends BaseBar {

    long globalCredits;
    long planetCredits;

    public CreditBar() {
        globalCredits = 0;
        planetCredits = 0;

        UserData ud = userDataDAO.findByUserId(BeanUtilities.getLoggedUser().getId());
        if (ud != null) {
            globalCredits = ud.getCredits();
        }

        planetCredits = sessionContext.getPlanetCalculationActPlanet().getPlanetCalcData().getExtPlanetCalcData().getTaxIncome();

    }

    public String getBackgroundImage() {
        return "pic/menu/statusbar/stb_credit_background.png";
    }

    public String getIcon() {
        return "pic/menu/icons/credits.png";
    }

    public long getGlobalCredits() {
        return globalCredits;
    }

    public long getPlanetCredits() {
        return planetCredits;
    }

    public String getPlanetCreditsImageIcon() {
        if (planetCredits > 0) {
            return GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_plus.png";
        } else {
            return GameConfig.getInstance().picPath() + "pic/menu/icons/emigration_minus.png";
        }
    }
}
