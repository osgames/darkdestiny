/*
 * Used to override some of the functions so they don't all have to be implemented in each filter
 *
 */
package at.darkdestiny.web.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 *
 * @author LAZYPAD
 */
public abstract class AbstractFilter implements Filter {

    protected FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Init filter [" + filterConfig.getFilterName() + "]");
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //do Nothing
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        //do Nothing
    }
}
