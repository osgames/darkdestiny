/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "TestBean")
@RequestScoped
public class TestBean {

    private int selectedValue = 0;

    public TestBean() {
    }

    public ArrayList<SelectItem> getOptions() {
        ArrayList<SelectItem> result = Lists.newArrayList();
        for (int i = 0; i < 10; i++) {
            result.add(new SelectItem(i, String.valueOf(i)));
        }
        return result;
    }

    public void invokeListener(){
        System.out.println("Invoke Listener");
    }

    public int getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(int selectedValue) {
        this.selectedValue = selectedValue;
    }


}
