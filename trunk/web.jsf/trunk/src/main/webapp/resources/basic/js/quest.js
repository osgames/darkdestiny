

function processQuestData(questData) {
    updateQuests(questData);
}

function updateQuests(questData){
    var field = document.getElementById("quests");
    if ( field.hasChildNodes() )
    {
        while ( field.childNodes.length >= 1 )
        {
            field.removeChild( field.firstChild );       
        } 
    }
    if(questData.quests != null){
        for(var i = 0; i < questData.quests.length; i++){
            var id = questData.quests[i].id;
            var name = questData.quests[i].name;
            var questTbody = document.createElement("tbody");
            var questRow = document.createElement("tr");
            questRow.className = "blue2";
            var questIdCell = document.createElement("td");
            var questNameCell = document.createElement("td");
           
            questIdCell.appendChild(document.createTextNode(id));
            questNameCell.appendChild(document.createTextNode(name));
           
            questRow.appendChild(questNameCell);
            questRow.appendChild(questIdCell);
            questTbody.appendChild(questRow);
            field.appendChild(questTbody);
                
        }        
    }
}



function addQuestResult(questResultId, questName){
    var field = document.getElementById("effects");
    var reqRow = document.createElement("tr");
    reqRow.setAttribute("id", 'delete_' + questResultId);
    
    var reqField = document.createElement("td");
    
    var valField = document.createElement("td");    
    var inpField = document.createElement("input");    
    inpField.setAttribute('size', '15');
    inpField.setAttribute('name', "questresult_" + questResultId);
    valField.appendChild(document.createTextNode(questName));
    valField.appendChild(inpField);
        
    var delField = document.createElement("td");
    var imgField = document.createElement("img");
    imgField.setAttribute("src", '../pic/cancel.jpg');
    createSafeListener(delField,'click',function() {
        removeResult('delete_' + questResultId);
    });     
    delField.appendChild(imgField);
    
    reqRow.appendChild(reqField);
    reqRow.appendChild(valField);
    reqRow.appendChild(delField);
    field.appendChild(reqRow);
    
}
function removeResult(questResultId){
    var field = document.getElementById("effects");
    var reqField = document.getElementById(questResultId);
    field.removeChild(reqField);
}

function submitForm(form, requirements){
    alert("form : " + form);
    alert("requirements : " + requirements);
}

function addQuestRequirement(questRequirementId, questName){
    var field = document.getElementById("requirements");
    var reqRow = document.createElement("tr");
    reqRow.setAttribute("id", 'delete_' + questRequirementId);
    
    var reqField = document.createElement("td");
    
    var valField = document.createElement("td");    
    var inpField = document.createElement("input");    
    inpField.setAttribute('size', '15');
    inpField.setAttribute('name', 'questrequirement_' + questRequirementId);
    valField.appendChild(document.createTextNode(questName));
    
    valField.appendChild(inpField);
        
    var delField = document.createElement("td");
    var imgField = document.createElement("img");
    imgField.setAttribute("src", '../pic/cancel.jpg');
    createSafeListener(delField,'click',function() {
        removeRequirement('delete_' + questRequirementId);
    });     
    delField.appendChild(imgField);
    
    reqRow.appendChild(reqField);
    reqRow.appendChild(valField);
    reqRow.appendChild(delField);
    field.appendChild(reqRow);
    
}
function removeRequirement(questRequirementId){
    var field = document.getElementById("requirements");
    var reqField = document.getElementById(questRequirementId);
    field.removeChild(reqField);
}

function updateQuestStepDetails(questStepId){
    
    for(var k = 0; k < questSteps.length; k++){
        
        if(questSteps[k][3] != questStepId){
            continue;
        }
        var field = document.getElementById("queststepdetails");
        if ( field.hasChildNodes() )
        {
            while ( field.childNodes.length >= 1 )
            {
                field.removeChild( field.firstChild );       
            } 
        }
        var questTbody = document.createElement("tbody");
        var questHeader = document.createElement("tr");
        questHeader.className = "blue2";
        var prevStepHeader = document.createElement("td");
        var stepHeader = document.createElement("td");
        var nextStepHeader = document.createElement("td");
            
        questHeader.appendChild(prevStepHeader);
        questHeader.appendChild(stepHeader);
        questHeader.appendChild(nextStepHeader);
            
        prevStepHeader.appendChild(document.createTextNode("Vorschritt"));
        stepHeader.appendChild(document.createTextNode("Schritt"));
        nextStepHeader.appendChild(document.createTextNode("Folgeschritt"));
            
        var questRow = document.createElement("tr");
        var prevStepCell = document.createElement("td");
        var stepCell = document.createElement("td");
        var nextStepCell = document.createElement("td");
            
            
        var prevStepTable = document.createElement("table");
        var stepTable = document.createElement("table");
        var nextStepTable = document.createElement("table");
    
    
        var prevStepTBody = document.createElement("tbody");
        var stepTBody = document.createElement("tbody");
        var nextStepTBody = document.createElement("tbody");
    
        var tableRow = document.createElement("tr");
    
    
        var tableCell = document.createElement("td");
        if(questSteps[k][1] != ''){
            var array = questSteps[k][1];
            for(var i = 0; i < array.length; i++){
                var prevTableRow = document.createElement("tr");
                var prevTableCell = document.createElement("td");
                prevTableCell.appendChild(document.createTextNode(questSteps[k][1][i][0]));
                appendShowLink(prevTableCell, questSteps[k][1][i][1]);
                prevTableRow.appendChild(prevTableCell);
                prevStepTBody.appendChild(prevTableRow);
            }
        }else{
            prevTableRow = document.createElement("tr");
            prevTableCell = document.createElement("td");        
            prevTableCell.appendChild(document.createTextNode("-"));
            appendShowLink(prevTableCell, 2);
            prevTableRow.appendChild(prevTableCell);
            prevStepTBody.appendChild(prevTableRow);
        }
    
        tableCell.appendChild(document.createTextNode(questSteps[k][0]));
        tableRow.appendChild(tableCell);
    
    
        if(questSteps[k][2] != ''){
            array = questSteps[k][2];
            for(i = 0; i < array.length; i++){
                var nextTableRow = document.createElement("tr");
                var nextTableCell = document.createElement("td");
                appendShowLink(nextTableCell, questSteps[k][2][i][1]);
                nextTableCell.appendChild(document.createTextNode(questSteps[k][2][i][0]));
                nextTableRow.appendChild(nextTableCell);
                nextStepTBody.appendChild(nextTableRow);
            }
        }else{
                
            nextTableRow = document.createElement("tr");
            nextTableCell = document.createElement("td");
            nextTableCell.appendChild(document.createTextNode("-"));
            nextTableRow.appendChild(nextTableCell);
            nextStepTBody.appendChild(nextTableRow);
        }
        
        /*   if(questStepId == 1){
        prevTableCell.appendChild(document.createTextNode("-"));
        tableCell.appendChild(document.createTextNode("Druuf-Invasion"));
        appendShowLink(nextTableCell, 2);
        nextTableCell.appendChild(document.createTextNode("Nachforschungen"));
    }
    if(questStepId == 2){
        prevTableCell.appendChild(document.createTextNode("Druuf-Invasion"));
        appendShowLink(prevTableCell, 2);
        tableCell.appendChild(document.createTextNode("Nachforschungen"));
        appendShowLink(nextTableCell, 2);
        nextTableCell.appendChild(document.createTextNode("Nachforschungen - Totalverlust"));
    }
    if(questStepId == 3){
        prevTableCell.appendChild(document.createTextNode("Nachforschungen"));
        appendShowLink(prevTableCell, 2);
        tableCell.appendChild(document.createTextNode("Nachforschungen - Totalverlust"));
        nextTableCell.appendChild(document.createTextNode("-"));
    }
    if(questStepId == 4){
        prevTableCell.appendChild(document.createTextNode("Nachforschungen"));
        appendShowLink(prevTableCell, 2);
        tableCell.appendChild(document.createTextNode("Nachforschungen - Neutral"));
        nextTableCell.appendChild(document.createTextNode("-"));
    }
    if(questStepId == 5){
        prevTableCell.appendChild(document.createTextNode("Nachforschungen"));
        appendShowLink(prevTableCell, 2);
        tableCell.appendChild(document.createTextNode("Nachforschungen - Gewinn"));
        nextTableCell.appendChild(document.createTextNode("-"));
    }*/
    
        /*  var nextTableRow2 = document.createElement("tr");
    var nextTableRow3 = document.createElement("tr");
    if(questStepId == 2){
        
        var nextTableCell2 = document.createElement("td");
        appendShowLink(nextTableCell2, 2);
        nextTableCell2.appendChild(document.createTextNode("Nachforschungen - Neutral"));
        var nextTableCell3 = document.createElement("td");
        appendShowLink(nextTableCell3, 2);
        nextTableCell3.appendChild(document.createTextNode("Nachforschungen - Gewinn"));
        nextTableRow2.appendChild(nextTableCell2);
        nextTableRow3.appendChild(nextTableCell3);
    }*/
    
        stepTBody.appendChild(tableRow);
        /* if(questStepId == 2){
        
        nextStepTBody.appendChild(nextTableRow2);
        nextStepTBody.appendChild(nextTableRow3);
    }*/
    
        prevStepTable.appendChild(prevStepTBody);
        stepTable.appendChild(stepTBody);
        nextStepTable.appendChild(nextStepTBody);
    
        prevStepCell.appendChild(prevStepTable);
        stepCell.appendChild(stepTable);
        nextStepCell.appendChild(nextStepTable);
    
    
        questRow.appendChild(prevStepCell);
        questRow.appendChild(stepCell);
        questRow.appendChild(nextStepCell);
            
        questTbody.appendChild(questHeader);
        questTbody.appendChild(questRow);
        field.appendChild(questTbody);
    }
}

function appendShowLink(field, questStepRelationId){
    
    var image = document.createElement("img");
    image.setAttribute("onmouseover", "doTooltip(event,'Show link')");
    image.setAttribute("onmouseout", "hideTip()");
    image.style.width = "17px";
    image.style.height = "17px";
    //window.location.href = 'questBuilder.jsp?type=<%= TYPE_SHOWQUESTSTEP %>&questStepId=
    image.setAttribute("style", "width:17px; height:17px");
    //  noteLink.setAttribute("onClick", "return popupNote(this, 'editor')");
    createSafeListener(image, "click", function(){
        window.location.href = 'questBuilder.jsp?type=4&questStepId=' + questStepRelationId;
    });
    image.setAttribute("src", "../pic/view.jpg");
    field.appendChild(image);
}
function createSafeListener(toField, listenerType, functionToDo) {
    var appName = navigator.appName;
        
    if (appName == 'Microsoft Internet Explorer') {
        toField.attachEvent('on' + listenerType, functionToDo);
    } else {
        toField.addEventListener(listenerType, functionToDo, false);
    }
}

function updateTypeSelect(typeName){
    var field = document.getElementById("selectTextField");
    if ( field.hasChildNodes() )
    {
        while ( field.childNodes.length >= 1 )
        {
            field.removeChild( field.firstChild );       
        } 
    }
    if(typeName == 'USER_DRIVEN'){
        
    }else if(typeName == 'RANDOM_DRIVEN'){
        
        var input = document.createElement("input");
        input.setAttribute("name", "randomValue");
        field.appendChild(input);
        field.appendChild(document.createTextNode("%"));
    }
}