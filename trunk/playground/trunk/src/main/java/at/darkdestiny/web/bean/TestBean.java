/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "TestBean")
@RequestScoped
public class TestBean {

    public String getInvokeListener() {
        return invokeListener;
    }

    public void setInvokeListener(String invokeListener) {
        this.invokeListener = invokeListener;
    }

    String invokeListener = "";

    public TestBean() {
    }

    public ArrayList<SelectItem> getOptions() {
        ArrayList<SelectItem> result = Lists.newArrayList();
        for (int i = 0; i < 5; i++) {
            result.add(new SelectItem(i, String.valueOf(i)));
        }
        return result;
    }

    public List<Item> getItems() {
        List<Item> result = Lists.newArrayList();
        TreeMap<Integer, List<Item>> treeMap = Maps.newTreeMap();
        for (Item i : Items.getItems()) {
            List<Item> items = treeMap.get(i.getPriority());
            if (items == null) {
                items = Lists.newArrayList();
            }
            items.add(i);
            treeMap.put(i.getPriority(), items);
        }

        for (Map.Entry<Integer, List<Item>> entry : treeMap.entrySet()) {
            result.addAll(entry.getValue());
        }
        return result;
    }

    public void invokeListener() {
        invokeListener = "Listener invoked";
    }


}
