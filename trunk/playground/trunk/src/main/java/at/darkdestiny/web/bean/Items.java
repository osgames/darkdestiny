/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.web.bean;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class Items {

    private static ArrayList<Item> items;

    static {
        init();
    }

    public static void init() {

        items = new ArrayList<Item>();
        getItems().add(new Item("Apple", 1));
        getItems().add(new Item("Banana", 2));
        getItems().add(new Item("Cheese", 3));
        getItems().add(new Item("Donut", 4));
    }

    public static void reset() {
        init();

    }

    public static List<String> getClicks() {
        return clicks;
    }

    public static void setClicks(List<String> clicks) {
        Items.clicks = clicks;
    }

    private static List<String> clicks = Lists.newArrayList();

    /**
     * @return the items
     */
    public static ArrayList<Item> getItems() {
        return items;
    }

}
