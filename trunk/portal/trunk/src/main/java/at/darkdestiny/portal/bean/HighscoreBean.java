/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.DateGenerator;
import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.model.Highscore;
import at.darkdestiny.portal.model.HighscoreEntry;
import at.darkdestiny.portal.model.Round;
import at.darkdestiny.portal.service.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "HighscoreBean")
@SessionScoped
public class HighscoreBean implements Serializable, Cloneable {

    private int roundId = 0;
    private EHighscoreInterval interval = EHighscoreInterval.MONTH;
    private long actTick = 0;
    private DateGenerator dateGenerator;

    public List<Round> getRounds() {
        TreeMap<Long, Round> sorted = Maps.newTreeMap();
        for (Round r : Service.roundDAO.findAll()) {
            sorted.put(r.getEndDate(), r);
        }
        List<Round> result = Lists.newArrayList();
        result.addAll(sorted.descendingMap().values());
        return result;
    }

    /**
     * @return the roundId
     */
    public int getRoundId() {
        return roundId;
    }

    public void iterate(String iteration) {
        actTick = dateGenerator.iterate(iteration);
    }

    public boolean isRoundFinished() {
        return Service.roundDAO.findById(roundId).isFinished();
    }

    /**
     * @param roundId the roundId to set
     */
    public String setRoundId(int roundId) {
        this.roundId = roundId;

        if (roundId > 0) {
            Round round = Service.roundDAO.findById(roundId);
            dateGenerator = new DateGenerator(round.getStartDate(), round.getStartDate(), round.getEndDate());
            actTick = dateGenerator.getActDay();
        }

        return Redirect.toHighScore();
    }

    public Highscore getHighscores() {
        Highscore result = Service.highscoreDAO.findByDate(getActTick(), roundId);
        return result;
    }

    public int getHighscoreSize() {
        return Service.highscoreDAO.findByRoundId(roundId).size();
    }

    public ArrayList<HighscoreEntry> getHighscoreEntries() {
        ArrayList<HighscoreEntry> result = new ArrayList<HighscoreEntry>();
        Highscore highscore = Service.highscoreDAO.findByDate(getActTick(), roundId);
        if (highscore != null) {
            result = Service.highscoreEntryDAO.findByHighscoreId(highscore.getId());
        }
        return result;
    }

    public String setInterval(String interval) {
        this.interval = EHighscoreInterval.valueOf(interval);
        return Redirect.toHighScore();
    }

    public String getIntervalToString() {
        return this.interval.toString();
    }

    /**
     * @return the actTick
     */
    public long getActTick() {
        return actTick;
    }

    public enum EHighscoreInterval {

        MONTH, WEEKS, DAYS
    }
}
