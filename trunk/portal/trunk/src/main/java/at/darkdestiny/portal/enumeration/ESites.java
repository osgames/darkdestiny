/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.enumeration;

/**
 *
 * @author Eobane
 */
public enum ESites {

    INDEX("index"),
    BUGS("bugtracker/bugs"),
    BUGS_DETAIL("bugtracker/bugs-detail"),
    TASKS("tasktracker/tasks"),
    TASKS_DETAIL("tasktracker/tasks-detail"),
    DOWNLOAD("download"),
    DATA_AGREEMENT("dataAgreement"),
    USER_AGREEMENT("userAgremment"),
    FOOTER("footer"),
    IMPRESSUM("impressum"),
    REGISTER("register"),
    LOGIN_MAIN("loginMain"),
    PASSWORD_RECOVERY("passwordRecovery"),
    MENU("menu"),
    MAILING_LIST("admin/mailingList"),
    INFO("information/info"),
    HIGHSCORE("media/highscore"),
    ROUNDREPLAY("media/roundreplay"),
    SCREENSHOTS("media/screenshots"),
    NEWS("news/news"),
    NEWS_DETAIL("news/news-detail"),
    PROFILE("profile/profile"),
    RELEASE_NOTES("releasenotes/releaseNotes"),
    RELEASE_NOTES_DETAIL("releasenotes/releaseNotes-detail"),
    ROUNDS("rounds/rounds"),
    ROUNDS_REGISTER("rounds/rounds-register"),
    ROUNDS_DETAIL("rounds/rounds-detail"),
    USERS("admin/users");

    private static final String PREFIX = "/sites/";
    private final String url;

    ESites(String url) {
        this.url = url;
    }

    public String getUrl() {
        return PREFIX.concat(this.url);
    }

    public String getRedirectUrl() {
        return PREFIX.concat(this.url).concat("?faces-redirect=true");
    }

}
