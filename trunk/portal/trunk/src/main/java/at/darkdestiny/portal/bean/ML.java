package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.language.LANG;
import java.io.Serializable;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "ML")
@SessionScoped
public class ML implements Serializable {

    private LANG lang = new LANG();

    public LANG getLang() {
        return lang;
    }

    /**
     * @param lang the lang to set
     */
    public void setLang(LANG lang) {
        this.lang = lang;
    }

}
