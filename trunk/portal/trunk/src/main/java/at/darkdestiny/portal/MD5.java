package at.darkdestiny.portal;

import java.security.*;

/**
 * <u>MD5-Klasse, zum erzeugen von MD5-Hashes aus Zeichenketten</u><br><br>
 * <b>Class:</b> MD5<br><br>
 * <b>Java-Version:</b> 1.5x<br><br>
 * <b>&copy; Copyright:</b> Karsten Bettray - 2006<br><br>
 * <b>License:</b> Free for non commercial use, for all educational institutions (Schools, Universities ...) and it members<br>
 * @author Karsten Bettray (Universit&auml;t Duisburg-Essen)<br><br>
 * @version 0.1<br>
 *
 */
public class MD5 {

    /**
     * <u>Zur&uuml;ckgabe des MD5-Hashes</u>
     * @param text
     * @return
     */
    public static String encryptPassword(String password, String passwordSalt) {
        String encrypted = "";


        password += passwordSalt;

        byte[] orgPwd = password.getBytes();


        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(orgPwd);
            byte messageDigest[] = algorithm.digest();

            // String s = new BASE64Encoder().encode(messageDigest);
            String s = "";
            for (int i = 0; i < messageDigest.length; i++) {
                String currentHex = Integer.toHexString(0xFF & messageDigest[i]);
                if (currentHex.length() == 1) {
                    currentHex = "0" + currentHex;
                }
                s += currentHex;
            }
            encrypted = s;
        } catch (Exception e) {
            System.out.println("Error on encryption: " + e.getMessage());
        }

        return encrypted;
    }
}

// MD5-Hash of "Test": 0cbc6611f5540bd0809a388dc95a615b