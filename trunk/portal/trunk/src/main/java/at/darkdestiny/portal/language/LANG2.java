
package at.darkdestiny.portal.language;


public final class LANG2 {


    public final static class COMMON {


        public final static class EBUGPRIORITY {

            public final static String HIGH = "common.ebugpriority.HIGH";
            public final static String LOW = "common.ebugpriority.LOW";
            public final static String MEDIUM = "common.ebugpriority.MEDIUM";
            public final static String NORMAL = "common.ebugpriority.NORMAL";
            public final static String VERYHIGH = "common.ebugpriority.VERYHIGH";
            public final static String VERYLOW = "common.ebugpriority.VERYLOW";

        }

        public final static class EBUGSTATUS {

            public final static String INPROCESS = "common.ebugstatus.INPROCESS";
            public final static String NEW = "common.ebugstatus.NEW";
            public final static String PAUSED = "common.ebugstatus.PAUSED";
            public final static String SCHEDULED = "common.ebugstatus.SCHEDULED";
            public final static String VIEWED = "common.ebugstatus.VIEWED";
            public final static String WONTFIX = "common.ebugstatus.WONTFIX";

            public final static class FINISHED {

                public final static String LIVE = "common.ebugstatus.FINISHED.LIVE";
                public final static String REPOSITORY = "common.ebugstatus.FINISHED.REPOSITORY";

            }

            public final static class WAITINGFOR {

                public final static String USERINPUT = "common.ebugstatus.WAITINGFOR.USERINPUT";

            }

        }

        public final static class ETASKPRIORITY {

            public final static String HIGH = "common.etaskpriority.HIGH";
            public final static String LOW = "common.etaskpriority.LOW";
            public final static String MEDIUM = "common.etaskpriority.MEDIUM";
            public final static String NORMAL = "common.etaskpriority.NORMAL";
            public final static String VERYHIGH = "common.etaskpriority.VERYHIGH";
            public final static String VERYLOW = "common.etaskpriority.VERYLOW";

        }

        public final static class ETASKSTATUS {

            public final static String INPROCESS = "common.etaskstatus.INPROCESS";
            public final static String NEW = "common.etaskstatus.NEW";
            public final static String PAUSED = "common.etaskstatus.PAUSED";
            public final static String SCHEDULED = "common.etaskstatus.SCHEDULED";
            public final static String VIEWED = "common.etaskstatus.VIEWED";

            public final static class FINISHED {

                public final static String LIVE = "common.etaskstatus.FINISHED.LIVE";
                public final static String REPOSITORY = "common.etaskstatus.FINISHED.REPOSITORY";

            }

            public final static class WAITINGFOR {

                public final static String USERINPUT = "common.etaskstatus.WAITINGFOR.USERINPUT";

            }

        }

        public final static class USER {


            public final static class LBL {

                public final static String UNKNOWN = "common.user.lbl.unknown";

            }

        }

        public final static class VISIBILITY {

            public final static String PRIVATE = "common.visibility.PRIVATE";
            public final static String PUBLIC = "common.visibility.PUBLIC";

        }

    }

    public final static class DB {


        public final static class LANGUAGE {

            public final static String AMERICAN = "db.language.american";
            public final static String AUSTRIA = "db.language.austria";
            public final static String ENGLISH = "db.language.english";
            public final static String GERMAN = "db.language.german";
            public final static String SUISS = "db.language.suiss";

        }

    }

    public final static class GLOBAL {


        public final static class LBL {

            public final static String ACTIONS = "global.lbl.actions";
            public final static String ADDMESSAGE = "global.lbl.addmessage";
            public final static String BACK = "global.lbl.back";
            public final static String CLOSE = "global.lbl.close";
            public final static String DELETE = "global.lbl.delete";
            public final static String EDIT = "global.lbl.edit";
            public final static String MESSAGE = "global.lbl.message";
            public final static String MESSAGES = "global.lbl.messages";
            public final static String NAME = "global.lbl.name";
            public final static String NOTLOGGED = "global.lbl.notlogged";
            public final static String PLEASELOGIN = "global.lbl.pleaselogin";
            public final static String SUBJECT = "global.lbl.subject";

        }

        public final static class MSG {

            public final static String MSGLOGIN = "global.msg.msglogin";

        }

        public final static class VALIDATION {


            public final static class ERR {

                public final static String ACCEPTUSERAGREEMENT = "global.validation.err.acceptuseragreement";
                public final static String LENGTH = "global.validation.err.length";
                public final static String NOVALIDEMAIL = "global.validation.err.novalidemail";

            }

        }

    }

    public final static class SITES {


        public final static class ADMIN {


            public final static class LBL {

                public final static String ADMIN = "sites.admin.lbl.admin";

            }

        }

        public final static class BUGS {


            public final static class LBL {

                public final static String ADDBUG = "sites.bugs.lbl.addbug";
                public final static String ADDCOMMENT = "sites.bugs.lbl.addcomment";
                public final static String BUGS = "sites.bugs.lbl.bugs";
                public final static String CHANGEBUGREPORT = "sites.bugs.lbl.changeBugReport";
                public final static String CHANGEEDITMODE = "sites.bugs.lbl.changeEditMode";
                public final static String DETAILS = "sites.bugs.lbl.details";
                public final static String MESSAGE = "sites.bugs.lbl.message";
                public final static String PRIORITY = "sites.bugs.lbl.priority";
                public final static String PUBLIC = "sites.bugs.lbl.public";
                public final static String REPORTBUG = "sites.bugs.lbl.reportBug";
                public final static String STATUS = "sites.bugs.lbl.status";
                public final static String SUBJECT = "sites.bugs.lbl.subject";

            }

        }

        public final static class DOWNLOAD {


            public final static class LBL {

                public final static String DOWNLOAD = "sites.download.lbl.download";

            }

        }

        public final static class FOOTER {


            public final static class LBL {

                public final static String ADMIN = "sites.footer.lbl.admin";
                public final static String IMPRESSUM = "sites.footer.lbl.impressum";

            }

        }

        public final static class HEADER {


            public final static class LBL {

                public final static String LOGIN = "sites.header.lbl.login";
                public final static String LOGOUT = "sites.header.lbl.logout";
                public final static String PASSWORD = "sites.header.lbl.password";
                public final static String REGISTER = "sites.header.lbl.register";

            }

            public final static class LINK {

                public final static String FORGOTPASSWORD = "sites.header.link.forgotpassword";

            }

            public final static class MSG {

                public final static String LOGGEDWITH = "sites.header.msg.loggedwith";

            }

        }

        public final static class HIGHSCORE {


            public final static class LBL {

                public final static String GAMENAME = "sites.highscore.lbl.gameName";
                public final static String HALLOFFAME = "sites.highscore.lbl.halloffame";
                public final static String MILITARYPOINTS = "sites.highscore.lbl.militaryPoints";
                public final static String POINTSTOTAL = "sites.highscore.lbl.pointsTotal";
                public final static String POPULATIONPOINTS = "sites.highscore.lbl.populationPoints";
                public final static String RANK = "sites.highscore.lbl.rank";
                public final static String RESEARCHPOINTS = "sites.highscore.lbl.researchPoints";
                public final static String RESOURCEPOINTS = "sites.highscore.lbl.resourcePoints";

                public final static class NEXT {

                    public final static String DAY = "sites.highscore.lbl.next.day";
                    public final static String MONTH = "sites.highscore.lbl.next.month";
                    public final static String WEEK = "sites.highscore.lbl.next.week";

                }

                public final static class PREVIOUS {

                    public final static String DAY = "sites.highscore.lbl.previous.day";
                    public final static String MONTH = "sites.highscore.lbl.previous.month";
                    public final static String WEEK = "sites.highscore.lbl.previous.week";

                }

            }

        }

        public final static class IMPRESSUM {


            public final static class LBL {

                public final static String IMPRESSUM = "sites.impressum.lbl.impressum";

            }

        }

        public final static class INFORMATION {


            public final static class LBL {

                public final static String EMBINIUM = "sites.information.lbl.embinium";
                public final static String HOWALGONIUM = "sites.information.lbl.howalgonium";
                public final static String INFORMATION = "sites.information.lbl.information";
                public final static String IRON = "sites.information.lbl.iron";
                public final static String STEEL = "sites.information.lbl.steel";
                public final static String TERKONIT = "sites.information.lbl.terkonit";
                public final static String YNKELONIUM = "sites.information.lbl.ynkelonium";

            }

        }

        public final static class INFORMATIONPANEL {


            public final static class LBL {

                public final static String BUG = "sites.informationpanel.lbl.bug";
                public final static String LASTCHANGES = "sites.informationpanel.lbl.lastChanges";
                public final static String SERVERS = "sites.informationpanel.lbl.servers";
                public final static String TASK = "sites.informationpanel.lbl.task";

            }

        }

        public final static class LOGIN {


            public final static class ERR {

                public final static String PASSWORDINVALID = "sites.login.err.passwordinvalid";
                public final static String USERNAMEALREADYEXISTING = "sites.login.err.usernamealreadyexisting";
                public final static String USERNOTFOUND = "sites.login.err.usernotfound";

            }

            public final static class LBL {

                public final static String GUEST = "sites.login.lbl.guest";

            }

            public final static class MSG {

                public final static String HELLO = "sites.login.msg.hello";

            }

        }

        public final static class LOGINMAIN {


            public final static class LBL {

                public final static String JOIN = "sites.loginmain.lbl.join";
                public final static String REGISTER = "sites.loginmain.lbl.register";
                public final static String REGISTERNOW = "sites.loginmain.lbl.registernow";
                public final static String SERVERSELECT_1 = "sites.loginmain.lbl.serverselect_1";
                public final static String SERVERSELECT_2 = "sites.loginmain.lbl.serverselect_2";
                public final static String TESTACCOUNT = "sites.loginmain.lbl.testaccount";

            }

            public final static class MSG {

                public final static String REGISTERNOW = "sites.loginmain.msg.registernow";
                public final static String TESTACCOUNT = "sites.loginmain.msg.testaccount";

            }

        }

        public final static class MEDIA {


            public final static class LBL {

                public final static String MEDIA = "sites.media.lbl.media";
                public final static String SCREENSHOTS = "sites.media.lbl.screenshots";

                public final static class STARMAP {

                    public final static String REPLAYS = "sites.media.lbl.starmap.replays";

                }

            }

        }

        public final static class MENU {


            public final static class LBL {

                public final static String BUGTRACKER = "sites.menu.lbl.bugtracker";
                public final static String COMMUNITY = "sites.menu.lbl.community";
                public final static String DOWNLOAD = "sites.menu.lbl.download";
                public final static String FORUM = "sites.menu.lbl.forum";
                public final static String HALLOFFAME = "sites.menu.lbl.halloffame";
                public final static String HELP = "sites.menu.lbl.help";
                public final static String HOME = "sites.menu.lbl.home";
                public final static String INFO = "sites.menu.lbl.info";
                public final static String MAILING = "sites.menu.lbl.mailing";
                public final static String MEDIA = "sites.menu.lbl.media";
                public final static String NEWS = "sites.menu.lbl.news";
                public final static String PROFILE = "sites.menu.lbl.profile";
                public final static String PROJECT = "sites.menu.lbl.project";
                public final static String REGISTER = "sites.menu.lbl.register";
                public final static String RELEASENOTES = "sites.menu.lbl.releasenotes";
                public final static String REPLAYS = "sites.menu.lbl.replays";
                public final static String ROUNDS = "sites.menu.lbl.rounds";
                public final static String SCREENSHOTS = "sites.menu.lbl.screenshots";
                public final static String SOURCEFORGE = "sites.menu.lbl.sourceforge";
                public final static String TASKTRACKER = "sites.menu.lbl.tasktracker";
                public final static String USERS = "sites.menu.lbl.users";
                public final static String WIKI = "sites.menu.lbl.wiki";

            }

        }

        public final static class NEWS {


            public final static class LBL {

                public final static String ADDCOMMENT = "sites.news.lbl.addcomment";
                public final static String ADDNEWS = "sites.news.lbl.addnews";
                public final static String COMMENTS = "sites.news.lbl.comments";
                public final static String COMMENTSHEADER = "sites.news.lbl.commentsheader";
                public final static String MESSAGE = "sites.news.lbl.message";
                public final static String NEWS = "sites.news.lbl.news";
                public final static String SUBJECT = "sites.news.lbl.subject";
                public final static String UPDATENEWS = "sites.news.lbl.updatenews";

            }

        }

        public final static class PASSWORDRECOVERY {


            public final static class ERR {

                public final static String INVALIDADDRESS = "sites.passwordrecovery.err.invalidaddress";
                public final static String USERNOTFOUND = "sites.passwordrecovery.err.usernotfound";

            }

            public final static class LBL {

                public final static String PASSWORDRECOVERY = "sites.passwordrecovery.lbl.passwordrecovery";
                public final static String RESETPASSWORD = "sites.passwordrecovery.lbl.resetpassword";
                public final static String USERNAME = "sites.passwordrecovery.lbl.username";

            }

            public final static class MSG {

                public final static String MAILSENT = "sites.passwordrecovery.msg.mailsent";

            }

        }

        public final static class PROFILE {


            public final static class LBL {

                public final static String DELETEACCOUNT = "sites.profile.lbl.deleteaccount";
                public final static String EMAIL = "sites.profile.lbl.email";
                public final static String LANGUAGE = "sites.profile.lbl.language";
                public final static String PASSWORDNEW = "sites.profile.lbl.passwordnew";
                public final static String PASSWORDNEWCONFIRM = "sites.profile.lbl.passwordnewconfirm";
                public final static String PASSWORDOLD = "sites.profile.lbl.passwordold";
                public final static String PROFILE = "sites.profile.lbl.profile";
                public final static String SENDDATA = "sites.profile.lbl.senddata";
                public final static String USERNAME = "sites.profile.lbl.username";

            }

            public final static class MSG {

                public final static String DELETIONWARNING = "sites.profile.msg.deletionwarning";

            }

        }

        public final static class REGISTER {


            public final static class BUT {

                public final static String SUBMITDATA = "sites.register.but.submitdata";

            }

            public final static class ERR {

                public final static String NOLANGUAGE = "sites.register.err.nolanguage";

            }

            public final static class LBL {

                public final static String EMAIL = "sites.register.lbl.email";
                public final static String LANGUAGE = "sites.register.lbl.language";
                public final static String PASSWORD = "sites.register.lbl.password";
                public final static String PASSWORDREPEAT = "sites.register.lbl.passwordrepeat";
                public final static String REGISTRATION = "sites.register.lbl.registration";
                public final static String USERNAME = "sites.register.lbl.username";

            }

            public final static class MSG {

                public final static String CAPTCHA = "sites.register.msg.captcha";
                public final static String MSGNEWROUND = "sites.register.msg.msgnewround";
                public final static String USERAGREEMENT1 = "sites.register.msg.useragreement1";
                public final static String USERAGREEMENT2 = "sites.register.msg.useragreement2";
                public final static String USERAGREEMENT3 = "sites.register.msg.useragreement3";

            }

            public final static class TITLE {

                public final static String HIGHSCORE = "sites.register.title.highscore";
                public final static String REGISTRATION = "sites.register.title.registration";

            }

        }

        public final static class REGISTRATION {


            public final static class ERR {

                public final static String PASSWORDSNOTEQUAL = "sites.registration.err.passwordsnotequal";
                public final static String USERALREADYEXISTING = "sites.registration.err.useralreadyexisting";
                public final static String WRONGCAPTCHA = "sites.registration.err.wrongcaptcha";

            }

        }

        public final static class ROUND {


            public final static class LBL {

                public final static String ACTIVATEENDDATE = "sites.round.lbl.activateenddate";
                public final static String ADDROUND = "sites.round.lbl.addround";
                public final static String CANCONNECT = "sites.round.lbl.canconnect";
                public final static String CANTCONNECT = "sites.round.lbl.cantconnect";
                public final static String CHOSENROUND = "sites.round.lbl.chosenround";
                public final static String DOWNFORMAINTENANCE = "sites.round.lbl.downformaintenance";
                public final static String ENDDATE = "sites.round.lbl.enddate";
                public final static String HOSTER = "sites.round.lbl.hoster";
                public final static String ID = "sites.round.lbl.id";
                public final static String INFO = "sites.round.lbl.info";
                public final static String NAME = "sites.round.lbl.name";
                public final static String REGISTER = "sites.round.lbl.register";
                public final static String REGISTRATIONPOSSIBLE = "sites.round.lbl.registrationpossible";
                public final static String ROUND = "sites.round.lbl.round";
                public final static String ROUNDADD = "sites.round.lbl.roundadd";
                public final static String ROUNDEDIT = "sites.round.lbl.roundedit";
                public final static String SERVERCONNECTION = "sites.round.lbl.serverconnection";
                public final static String SERVERSTATUS = "sites.round.lbl.serverstatus";
                public final static String STARTDATE = "sites.round.lbl.startdate";
                public final static String STATUS = "sites.round.lbl.status";
                public final static String URL = "sites.round.lbl.url";
                public final static String USERSTATUS = "sites.round.lbl.userstatus";
                public final static String VERSION = "sites.round.lbl.version";
                public final static String WEBSERVICEPASSWORD = "sites.round.lbl.webservicepassword";
                public final static String WEBSERVICEURL = "sites.round.lbl.webserviceurl";

            }

            public final static class MSG {

                public final static String THIRDPARTYWARNING = "sites.round.msg.thirdpartywarning";

            }

            public final static class STATUS {

                public final static String JOIN = "sites.round.status.join";
                public final static String MAINTENANCE = "sites.round.status.maintenance";
                public final static String NOCONNECTION = "sites.round.status.noconnection";
                public final static String ONLINE = "sites.round.status.online";
                public final static String REGISTRATION = "sites.round.status.registration";
                public final static String RUNNINGNOTICK = "sites.round.status.runningnotick";
                public final static String STOPPED = "sites.round.status.stopped";

            }

        }

        public final static class ROUNDREPLAY {


            public final static class LBL {

                public final static String REPLAYS = "sites.roundreplay.lbl.replays";
                public final static String ROUND_8 = "sites.roundreplay.lbl.round_8";

            }

        }

        public final static class ROUNDS {


            public final static class LBL {

                public final static String ROUNDS = "sites.rounds.lbl.rounds";

            }

            public final static class MSG {

                public final static String USERALREADYEXISTING = "sites.rounds.msg.useralreadyexisting";
                public final static String USERNOTYETEXISTING = "sites.rounds.msg.usernotyetexisting";

            }

        }

        public final static class SCREENSHOTS {


            public final static class LBL {

                public final static String SCREENSHOTS = "sites.screenshots.lbl.screenshots";

            }

        }

        public final static class TASKS {


            public final static class LBL {

                public final static String ADDTASK = "sites.tasks.lbl.addtask";
                public final static String CHANGEEDITMODE = "sites.tasks.lbl.changeEditMode";
                public final static String DETAILS = "sites.tasks.lbl.details";
                public final static String TASKS = "sites.tasks.lbl.tasks";

            }

        }

        public final static class TITLE {

            public final static String PREFIX = "sites.title.prefix";

        }

        public final static class TRACKER {


            public final static class LBL {

                public final static String ACTIONS = "sites.tracker.lbl.actions";
                public final static String DATE = "sites.tracker.lbl.date";
                public final static String DETAILS = "sites.tracker.lbl.details";
                public final static String PRIORITY = "sites.tracker.lbl.priority";
                public final static String PUBLIC = "sites.tracker.lbl.public";
                public final static String RELEASE = "sites.tracker.lbl.release";
                public final static String STATUS = "sites.tracker.lbl.status";
                public final static String SUBJECT = "sites.tracker.lbl.subject";
                public final static String SUBMITTER = "sites.tracker.lbl.submitter";
                public final static String TIMECONSUMED = "sites.tracker.lbl.timeconsumed";
                public final static String WORKER = "sites.tracker.lbl.worker";

            }

        }

        public final static class USERAGREEMENT {

            public final static String TITLE = "sites.useragreement.title";

            public final static class LBL {

                public final static String DATAAGREEMENT = "sites.useragreement.lbl.dataagreement";
                public final static String USAGEAGREEMENT = "sites.useragreement.lbl.usageagreement";

            }

        }

    }

}
