/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.portal.enumeration.EUserType;
import java.util.Date;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "user")
public class User extends Model<User> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = false, autoIncrement = true)
    private Integer id;
    // @Length(min=2,max=20, message="Zeichenlšnge 2-20")
    @FieldMappingAnnotation("userName")
    @IndexAnnotation(indexName = "userName")
    @ColumnProperties(length = 15)
    @StringType("text")
    private String userName;
    //@Length(min=4,max=40, message="Zeichenlšnge 4-40")
    @FieldMappingAnnotation("password")
    @IndexAnnotation(indexName = "userName")
    @ColumnProperties(length = 255)
    @StringType("text")
    private String password;
    @FieldMappingAnnotation("passwordSalt")
    @ColumnProperties(length = 255)
    @StringType("text")
    private String passwordSalt;
    @FieldMappingAnnotation("locale")
    private String locale;
    @FieldMappingAnnotation("joinDate")
    private Long joinDate;
    @FieldMappingAnnotation("userType")
    @DefaultValue("NORMAL")
    private EUserType userType;
    @FieldMappingAnnotation("email")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String email;
    @FieldMappingAnnotation("roundNotification")
    @DefaultValue("1")
    private Boolean roundNotification;
    @FieldMappingAnnotation("acceptedUserAgreement")
    @DefaultValue("0")
    private Boolean acceptedUserAgreement;
    @FieldMappingAnnotation("passwordResetToken")
    private String passwordResetToken;
    @FieldMappingAnnotation("passwordResetDate")
    private Long passwordResetDate;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the locale
     */
    public String getLocale() {
        return locale;
    }

    /**
     * @param locale the locale to set
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * @return the joinDate
     */
    public Long getJoinDate() {
        return joinDate;
    }

    /**
     * @param joinDate the joinDate to set
     */
    public void setJoinDate(Long joinDate) {
        this.joinDate = joinDate;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the roundNotification
     */
    public Boolean getRoundNotification() {
        return roundNotification;
    }

    /**
     * @param roundNotification the roundNotification to set
     */
    public void setRoundNotification(Boolean roundNotification) {
        this.roundNotification = roundNotification;
    }

    /**
     * @return the passwordSalt
     */
    public String getPasswordSalt() {
        return passwordSalt;
    }

    /**
     * @param passwordSalt the passwordSalt to set
     */
    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    /**
     * @return the acceptedUserAgreement
     */
    public Boolean getAcceptedUserAgreement() {
        return acceptedUserAgreement;
    }

    /**
     * @param acceptedUserAgreement the acceptedUserAgreement to set
     */
    public void setAcceptedUserAgreement(Boolean acceptedUserAgreement) {
        this.acceptedUserAgreement = acceptedUserAgreement;
    }

    /**
     * @return the userType
     */
    public EUserType getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(EUserType userType) {
        this.userType = userType;
    }

    /**
     * @return the passwordResetToken
     */
    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    /**
     * @param passwordResetToken the passwordResetToken to set
     */
    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    /**
     * @return the passwordResetDate
     */
    public Long getPasswordResetDate() {
        return passwordResetDate;
    }

    /**
     * @param passwordResetDate the passwordResetDate to set
     */
    public void setPasswordResetDate(Long passwordResetDate) {
        this.passwordResetDate = passwordResetDate;
    }
}
