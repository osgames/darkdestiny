/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "bugmessage")
public class BugMessage extends Model<BugMessage>{
    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true, autoIncrement=true)
    private Integer id;
    @FieldMappingAnnotation("bugId")
    private Integer bugId;
    @FieldMappingAnnotation("message")
    @ColumnProperties(length=500)
    @StringType("text")
    private String message;
    @FieldMappingAnnotation("userId")
    private Integer userId;
    @FieldMappingAnnotation("date")
    private Long date;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the bugId
     */
    public Integer getBugId() {
        return bugId;
    }

    /**
     * @param bugId the bugId to set
     */
    public void setBugId(Integer bugId) {
        this.bugId = bugId;
    }

    public String getMessageReplaced() {
        if(message != null){
            return message.replace("\n", "<BR/>");
        }else{
            return null;
        }
    }

    /**
     * @return the message
     */
    public String getMessage() {
            return message;

    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }


}
