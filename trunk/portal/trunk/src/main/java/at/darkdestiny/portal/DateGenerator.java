/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

import java.util.GregorianCalendar;

/**
 *
 * @author Admin
 */
public class DateGenerator {

    long startDate;
    long minDate = Long.MIN_VALUE;
    long maxDate = Long.MAX_VALUE;

    public DateGenerator(long startDate, long minDate, long maxDate) {
        this.startDate = startDate;
        this.minDate = minDate;
        this.maxDate = maxDate;
    }

    public DateGenerator(long startDate) {
        this.startDate = startDate;
    }

    public DateGenerator() {
        this.startDate = System.currentTimeMillis();

    }

    public long getActDay() {

        GregorianCalendar result = new GregorianCalendar();
        result.setTimeInMillis(startDate);
        result.set(GregorianCalendar.HOUR_OF_DAY, 0);
        result.set(GregorianCalendar.MINUTE, 0);
        result.set(GregorianCalendar.SECOND, 0);
        result.set(GregorianCalendar.MILLISECOND, 0);
        return result.getTimeInMillis();
    }

    public long getNextMonth() {
        return iterate(EIteration.MONTH_DOWN);
    }

    public long getNextDay() {
        return iterate(EIteration.DAY_UP);
    }

    public long iterate(EIteration iteration) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(startDate);

        if (iteration.equals(EIteration.DAY_UP)) {
            gc.add(GregorianCalendar.DAY_OF_MONTH, 1);
        } else if (iteration.equals(EIteration.DAY_DOWN)) {
            gc.add(GregorianCalendar.DAY_OF_MONTH, -1);
        } else if (iteration.equals(EIteration.WEEK_UP)) {
            gc.add(GregorianCalendar.DAY_OF_MONTH, 7);
        } else if (iteration.equals(EIteration.WEEK_DOWN)) {
            gc.add(GregorianCalendar.DAY_OF_MONTH, -7);
        } else if (iteration.equals(EIteration.MONTH_UP)) {
            gc.add(GregorianCalendar.MONTH, 1);
        } else if (iteration.equals(EIteration.MONTH_DOWN)) {
            gc.add(GregorianCalendar.MONTH, -1);
        }

        long value = gc.getTimeInMillis();
        value = Math.min(value, maxDate);
        value = Math.max(value, minDate);

        gc.setTimeInMillis(value);

        GregorianCalendar result = new GregorianCalendar();
        result.set(GregorianCalendar.MONTH, gc.get(GregorianCalendar.MONTH));
        result.set(GregorianCalendar.YEAR, gc.get(GregorianCalendar.YEAR));
        result.set(GregorianCalendar.DAY_OF_MONTH, gc.get(GregorianCalendar.DAY_OF_MONTH));
        result.set(GregorianCalendar.HOUR_OF_DAY, 0);
        result.set(GregorianCalendar.MINUTE, 0);
        result.set(GregorianCalendar.SECOND, 0);
        result.set(GregorianCalendar.MILLISECOND, 0);

        startDate = result.getTimeInMillis();
        return result.getTimeInMillis();
    }

    public long getPreviousDay() {
        return iterate(EIteration.DAY_DOWN);
    }

    public long getPrevoiusMonth() {

        return iterate(EIteration.MONTH_DOWN);
    }

    public long getPrevoiusWeekh() {

        return iterate(EIteration.WEEK_DOWN);
    }

    public long getNextWeek() {

        return iterate(EIteration.WEEK_UP);
    }

    public long iterate(String iteration) {
        return iterate(EIteration.valueOf(iteration));
    }

    private enum EIteration {

        DAY_UP, DAY_DOWN, WEEK_UP, WEEK_DOWN, MONTH_UP, MONTH_DOWN
    }
}
