/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "UserBean")
@SessionScoped
public class UserBean implements Serializable, Cloneable {

    
    public ArrayList<User> getUsers(){
        return Service.userDAO.findAll();
    }
}
