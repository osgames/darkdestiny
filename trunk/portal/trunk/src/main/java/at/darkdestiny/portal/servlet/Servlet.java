/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.servlet;

import at.darkdestiny.portal.EmailService;
import at.darkdestiny.portal.dao.UserDAO;
import at.darkdestiny.portal.model.Round;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Stefan
 */
public class Servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("passwordResetToken") != null) {
            String token = request.getParameter("passwordResetToken");
            for (User u : Service.userDAO.findAll()) {
                if (u.getPasswordResetToken() == null || u.getPasswordResetDate() == null) {
                    continue;
                }
                if (u.getPasswordResetToken().equals(token)) {

                    Calendar cal1 = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal1.setTimeInMillis(u.getPasswordResetDate());
                    cal2.setTime(new Date());
                    cal2.add(Calendar.DAY_OF_MONTH, 3);
                    if (cal1.after(cal2)) {
                        response.getWriter().println("Link nicht mehr g�ltig. URL not valid anymore");
                    } else {
                        String plainPassword = RandomStringUtils.randomAlphabetic(5);
                        String salt = RandomStringUtils.randomAlphabetic(20);
                        String password = UserDAO.getEncryptedPassword(plainPassword, salt);
                        u.setPasswordSalt(salt);
                        u.setPassword(password);
                        u.setPasswordResetToken(null);
                        u.setPasswordResetDate(null);
                        Service.userDAO.update(u);
                        try {
                            EmailService.sendPassword(plainPassword, u, request.getLocale());
                        } catch (MessagingException ex) {
                            response.getOutputStream().println(ex.getMessage());
                            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        response.getOutputStream().println("Passwort wurde per Email versandt. Password sent by email");
                    }
                    break;

                }
            }
        }
       /* if (request.getParameter("roundName") != null && request.getParameter("roundPassword") != null) {
            String name = request.getParameter("roundName");
            String password = request.getParameter("roundPassword");
            List<Round> rounds = Service.roundDAO.findByName(name);
            if (rounds.isEmpty()) {
                response.getWriter().write("roundName not found");
            } else if (rounds.size() > 1) {
                response.getWriter().write("too many rounds found with this name");
            } else {
                Round round = rounds.get(0);
                if (!Strings.nullToEmpty(round.getWebServicePassword()).equals(password)) {
                    response.getWriter().write("password is wrong");
                } else {
                    String emailsString = request.getParameter("emails");
                    if (emailsString != null) {
                        String[] emails = emailsString.split(";");
                        String subject = request.getParameter("subject");
                        String message = request.getParameter("message");

                        EmailService.send(emails, subject, message, request.getLocale());
                    }
                }
            }



        }*/


    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
