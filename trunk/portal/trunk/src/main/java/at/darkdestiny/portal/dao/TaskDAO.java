/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.Bug;
import at.darkdestiny.portal.model.Task;
import java.util.List;

/**
 *
 * @author Eobane
 */
public class TaskDAO extends ReadWriteTable<Task> implements GenericDAO {
    public Task findById(Integer id) {
        Task b = new Task();
        b.setId(id);
        return (Task)get(b);
    }
    
    public List<Task> findByReleaseId(Integer releaseId) {
   
        Task t = new Task();
        t.setReleaseId(releaseId);
        return find(t);  
    }
}
