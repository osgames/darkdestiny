/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

/**
 *
 * @author Aion
 */
public class SetupProperties {

    public static final String DATABASE_DRIVERCLASS = "driverClass";
    public static final String DATABASE_URL = "url";
    public static final String DATABASE_PASS = "pass";
    public static final String DATABASE_USER = "user";
    public static final String DATABASE_NAME = "name";
    public static final String DATABASE_MYSQLIP = "mysqlIp";
    
}
