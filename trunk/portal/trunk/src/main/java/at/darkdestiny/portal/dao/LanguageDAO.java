/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadOnlyTable;
import com.google.common.collect.Lists;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.Language;
import com.google.common.base.Strings;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class LanguageDAO extends ReadOnlyTable<Language> implements GenericDAO {

    private static final int LANGUAGE_DEFAULT = 0;

    public Language findById(Integer id) {
        Language l = new Language();
        l.setId(id);
        return (Language) get(l);
    }

    public Language findBy(String country, String language) {
        Language l = new Language();
        l.setLanguage(language);
        if (Strings.isNullOrEmpty(country)) {
            if (language.equals("de")) {
                country = "DE";
            }
            if (language.equals("en")) {
                country = "GB";
            }
        }
        l.setCountry(country);
        ArrayList<Language> ls = find(l);
        if (ls.size() > 0) {
            return ls.get(0);
        } else {
            return findById(LANGUAGE_DEFAULT);
        }

    }

    public Language findBy(String locale) {
        return findBy(locale.substring(0, locale.indexOf("_")), locale.substring(locale.indexOf("_") + 1, locale.length()));
    }

    public Language getMasterLanguageOf(int languageId) {
        Language l = new Language();
        l.setId(languageId);
        l = get(l);
        if (l.getId() == l.getMasterLanguageId()) {
            return l;
        } else {
            int masterId = l.getMasterLanguageId();
            l = new Language();
            l.setId(masterId);
            return get(l);
        }
    }

    public ArrayList<Language> findAllMasterLanguages() {
        ArrayList<Language> result = Lists.newArrayList();
        for (Language l : findAll()) {
            if (l.getMasterLanguageId() == l.getId()) {
                result.add(l);
            }
        }
        return result;
    }
}
