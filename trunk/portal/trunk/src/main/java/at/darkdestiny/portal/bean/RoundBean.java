/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.RoundRefreshCounter;
import at.darkdestiny.portal.dto.ServerInformationData;
import at.darkdestiny.portal.enumeration.EGameDataStatus;
import at.darkdestiny.portal.enumeration.ERoundStatus;
import at.darkdestiny.portal.model.Round;
import at.darkdestiny.portal.model.RoundUserRegistration;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "RoundBean")
@SessionScoped
public class RoundBean implements Serializable, Cloneable {

    private boolean registryMode = false;
    private boolean editMode = false;
    private Round round;

    public ArrayList<Round> getRounds() {
        return Service.roundDAO.findAll();
    }

    public ServerInformationData getServerInformationData() {
        return RoundRefreshCounter.getServerInformation(round);
    }

    public ArrayList<ServerInformationData> getServers() {
        ArrayList<ServerInformationData> rounds = new ArrayList<ServerInformationData>();

        for (Round r : Service.roundDAO.findNotFinished()) {
            rounds.add(RoundRefreshCounter.getServerInformation(r));
        }

        return rounds;
    }

    /**
     * @return the registryMode
     */
    public boolean isRegistryMode() {
        return registryMode;
    }

    public String getPortalPassword(Integer roundId) {

        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        User user = loginBean.getUser();
        RoundUserRegistration roundUserRegistration = Service.roundUserRegistrationDAO.findBy(roundId, user.getId());
        if (roundUserRegistration != null) {
            return roundUserRegistration.getRoundPassword();
        } else {
            return "";
        }
    }

    public String deleteRound(Integer id) {
        Round r = Service.roundDAO.findById(id);
        Service.roundDAO.remove(r);
        return Redirect.toRounds();
    }

    public ArrayList<String> getRegistrationErrors() {
        ArrayList<String> result = new ArrayList<String>();
        if (!RoundRefreshCounter.getServerInformation(round).canConnectToServer()) {
            result.add("Server nicht erreichbar");
        }
        if (RoundRefreshCounter.getServerInformation(round).getStatus().equals(EGameDataStatus.STOPPED)) {
            result.add("Server gestoppt keine Registrierung m�glich");
        }
        if (RoundRefreshCounter.getServerInformation(round).isUserRegistered()) {
            result.add("Sie haben bereits einen Spieleraccount in dieser Runde registriert");
        }
        return result;
    }

    public boolean isRegistrationPossible() {
        if (!RoundRefreshCounter.getServerInformation(round).canConnectToServer()) {
            return false;
        }
        if (RoundRefreshCounter.getServerInformation(round).getStatus().equals(EGameDataStatus.STOPPED)) {
            return false;
        }
        if (RoundRefreshCounter.getServerInformation(round).isUserRegistered()) {
            return false;
        }
        return true;
    }

    public String addRound() {
        Service.roundDAO.add(getRound());
        return Redirect.toRounds();
    }

    public String roundRegistration(Integer roundId) {
        Round r = Service.roundDAO.findById(roundId);
        this.round = r;
        return Redirect.toRoundsRegister();
    }

    /**
     * @param registryMode the registryMode to set
     */
    public void setRegistryMode(boolean registryMode) {
        this.registryMode = registryMode;
    }

    /**
     * @return the round
     */
    public Round getRound() {
        if (round == null) {
            createNewRound();
        }
        return round;
    }

    public String getRegistrationUrl() {
        return round.getWebServiceUrl() + "/register.jsp";
    }

    public String createNewRound() {

        setRound(new Round());
        setEditMode(false);
        CalendarBean calendarBean = (CalendarBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("CalendarBean");
        if (calendarBean != null) {
            calendarBean.createNewDefaultValues();
        }
        getRound().setUrl("http://");
        getRound().setWebServiceUrl("http://");
        return Redirect.toRoundsDetail();
    }

    /**
     * @param round the round to set
     */
    public String setRound(Round round) {
        this.round = round;
        setEditMode(true);
        return Redirect.toRoundsDetail();
    }

    public ERoundStatus[] getRoundStatus() {

        return ERoundStatus.values();
    }

    public String updateRound() {
        Service.roundDAO.update(getRound());
        return Redirect.toRounds();
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

}
