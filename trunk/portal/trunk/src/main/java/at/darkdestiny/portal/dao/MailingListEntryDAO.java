/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.MailingListEntry;
import java.util.List;

/**
 *
 * @author Eobane
 */
public class MailingListEntryDAO extends ReadWriteTable<MailingListEntry> implements GenericDAO {

    public MailingListEntry findById(Integer id) {
        MailingListEntry ml = new MailingListEntry();
        ml.setId(id);
        return (MailingListEntry) get(ml);
    }
    public boolean exists(String email, Integer mailingListId) {
        MailingListEntry ml = new MailingListEntry();
        ml.setMailingListId(mailingListId);
        ml.setEmail(email);
        return !find(ml).isEmpty();
    }
    public List<MailingListEntry> findByMailingListId(Integer mailingListId) {
        MailingListEntry ml = new MailingListEntry();
        ml.setMailingListId(mailingListId);
        return find(ml);
    }
}
