/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

import at.darkdestiny.portal.bean.CalendarBean;
import at.darkdestiny.portal.bean.LoginBean;
import at.darkdestiny.portal.bean.MLBean;
import at.darkdestiny.portal.bean.ServiceBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author Stefan
 */
public class BeanUtilities {

    public static CalendarBean getCalendarBean() {

        CalendarBean calendarBean = (CalendarBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("CalendarBean");

        return calendarBean;
    }

    public static ServiceBean getServiceBean() {

        ServiceBean serviceBean = (ServiceBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("ServiceBean");

        return serviceBean;
    }

    public static LoginBean getLoginBean() {

        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");

        return loginBean;
    }
    public static MLBean getMLBean() {

        MLBean mlBean = (MLBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("MLBean");

        return mlBean;
    }
}
