/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.EmailService;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "EmailServiceBean")
@SessionScoped
public class EmailServiceBean implements Serializable {

    private String userName = "";
    private String message = "";
    boolean error = false;

    public void resetPasswordRequestEmail() {
        System.out.println("Reset password clicked: [" + userName + "]");
        message = EmailService.resetPasswordRequestEmail(userName, FacesContext.getCurrentInstance().getExternalContext().getRequestLocale());
        if (message.contains("err")) {
            error = true;
        } else {
            error = false;
        }
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param aUserName the userName to set
     */
    public void setUserName(String aUserName) {
        userName = aUserName;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
