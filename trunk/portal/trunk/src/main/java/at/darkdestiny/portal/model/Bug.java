/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.portal.enumeration.EBugPriority;
import at.darkdestiny.portal.enumeration.EBugStatus;
import at.darkdestiny.portal.service.Service;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "bug")
public class Bug extends Model<Bug> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("subject")
    @ColumnProperties(length = 500)
    @StringType("text")
    private String subject;
    @FieldMappingAnnotation("status")
    @DefaultValue("NEW")
    private EBugStatus status;
    @FieldMappingAnnotation("userId")
    private Integer userId;
    @FieldMappingAnnotation("workerId")
    @DefaultValue("0")
    private Integer workerId;
    @FieldMappingAnnotation("releaseId")
    @DefaultValue("0")
    private Integer releaseId;
    @FieldMappingAnnotation("date")
    private Long date;
    @FieldMappingAnnotation("bugpriority")
    @DefaultValue("NORMAL")
    private EBugPriority priority;
    @FieldMappingAnnotation("visible")
    private Boolean visible;
    @FieldMappingAnnotation("timeConsumed")
    @DefaultValue("0")
    private Integer timeConsumed;
    @FieldMappingAnnotation("lastChange")
    @DefaultValue("0")
    private Long lastChange;
    @DefaultValue("archived")
    private Boolean archived;

    public Bug() {
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the status
     */
    public EBugStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EBugStatus status) {
        this.status = status;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the workerId
     */
    public Integer getWorkerId() {
        return workerId;
    }

    /**
     * @param workerId the workerId to set
     */
    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    /**
     * @return the releaseId
     */
    public Integer getReleaseId() {
        return releaseId;
    }

    /**
     * @param releaseId the releaseId to set
     */
    public void setReleaseId(Integer releaseId) {
        this.releaseId = releaseId;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the priority
     */
    public EBugPriority getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(EBugPriority priority) {
        this.priority = priority;
    }

    /**
     * @return the visible
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    /**
     * @return the timeConsumed
     */
    public Integer getTimeConsumed() {
        return timeConsumed;
    }

    /**
     * @param timeConsumed the timeConsumed to set
     */
    public void setTimeConsumed(Integer timeConsumed) {
        this.timeConsumed = timeConsumed;
    }

    public boolean isIncrementPriorityPossible() {
        return false;
    }

    public void incrementVersion() {

        releaseId = Service.versionDAO.findNextVersion(releaseId);
        Service.bugDAO.update(this);

    }

    public void decrementVersion() {

        releaseId = Service.versionDAO.findPreviousVersion(releaseId);
        Service.bugDAO.update(this);

    }
    public void incrementPriority() {
        boolean found = false;
        for (EBugPriority p : EBugPriority.values()) {
            if (found) {
                priority = p;
                Service.bugDAO.update(this);
                break;
            }
            if (p.equals(priority)) {
                found = true;
            }
        }
    }

    public void decrementTimeConsumed() {
        decrementTimeConsumed(15);
    }

    public void incrementTimeConsumed() {
        incrementTimeConsumed(15);
    }

    public void changeVisible() {
        this.visible = !this.visible;
        Service.bugDAO.update(this);
    }

    public void decrementTimeConsumedHeavy() {
        decrementTimeConsumed(60);
    }

    public void incrementTimeConsumedHeavy() {
        incrementTimeConsumed(60);
    }

    public void decrementTimeConsumed(int amount) {
        timeConsumed -= amount;
        timeConsumed = Math.max(timeConsumed, 0);
        updateLastChange();
        Service.bugDAO.update(this);
    }

    public void incrementTimeConsumed(int amount) {
        timeConsumed += amount;
        updateLastChange();
        Service.bugDAO.update(this);
    }

    public void decrementPriority() {
        EBugPriority tmpPrio = null;
        for (EBugPriority p : EBugPriority.values()) {
            if (p.equals(priority) && tmpPrio != null) {
                priority = tmpPrio;
                Service.bugDAO.update(this);
                break;
            }
            tmpPrio = p;
        }

    }

    private void updateLastChange() {
        this.lastChange = System.currentTimeMillis();
    }

    public void incrementStatus() {
        boolean found = false;
        for (EBugStatus s : EBugStatus.values()) {
            if (found) {
                status = s;
                this.lastChange = System.currentTimeMillis();
                Service.bugDAO.update(this);
                break;
            }
            if (s.equals(status)) {
                found = true;
            }
        }
    }

    public void decrementStatus() {
        EBugStatus tmpPrio = null;
        for (EBugStatus s : EBugStatus.values()) {
            if (s.equals(status) && tmpPrio != null) {
                status = tmpPrio;
                this.lastChange = System.currentTimeMillis();
                Service.bugDAO.update(this);
                break;
            }
            tmpPrio = s;
        }

    }

    /**
     * @return the lastChange
     */
    public Long getLastChange() {
        return lastChange;
    }

    /**
     * @param lastChange the lastChange to set
     */
    public void setLastChange(Long lastChange) {
        this.lastChange = lastChange;
    }

    /**
     * @return the archived
     */
    public Boolean getArchived() {
        return archived;
    }

    /**
     * @param archived the archived to set
     */
    public void setArchived(Boolean archived) {
        this.archived = archived;
    }
}
