/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.enumeration;

import at.darkdestiny.portal.language.LANG2;

/**
 *
 * @author Admin
 */
public enum ETaskStatus {

    NEW(LANG2.COMMON.ETASKSTATUS.NEW),
    VIEWED(LANG2.COMMON.ETASKSTATUS.VIEWED),
    SCHEDULED(LANG2.COMMON.ETASKSTATUS.SCHEDULED),
    PAUSED(LANG2.COMMON.ETASKSTATUS.PAUSED),
    INPROCESS(LANG2.COMMON.ETASKSTATUS.INPROCESS),
    WAITINGFOR_USERINPUT(LANG2.COMMON.ETASKSTATUS.WAITINGFOR.USERINPUT),
    FINISHED_REPOSITORY(LANG2.COMMON.ETASKSTATUS.FINISHED.REPOSITORY),
    FINISHED_LIVE(LANG2.COMMON.ETASKSTATUS.FINISHED.LIVE);

    private final String languageConstant;

    ETaskStatus(String languageConstant) {
        this.languageConstant = languageConstant;
    }

    public String getLanguageConstant() {
        return languageConstant;
    }
}
