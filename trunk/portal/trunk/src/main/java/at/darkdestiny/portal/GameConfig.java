package at.darkdestiny.portal;

import at.darkdestiny.util.AbstractGameConfig;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

public class GameConfig extends AbstractGameConfig {

    private static GameConfig instance = null;

    public static GameConfig getInstance() {
        if (instance == null) {
            instance = new GameConfig();
        }
        return instance;
    }

    private GameConfig() {
        loadValues();
    }

    private void loadValues() {
        try {
            String additionalParameters = "?autoReconnect=true&dontTrackOpenResources=true";

            Properties dbProperties = new Properties();
            try {
                dbProperties.load(GameConfig.class.getResourceAsStream("/db.properties"));
            } catch (IOException ex) {
                ex.printStackTrace();
                java.util.logging.Logger.getLogger(GameConfig.class.getName()).log(Level.SEVERE, null, ex);
            }

            driver = loadValue(dbProperties, SetupProperties.DATABASE_DRIVERCLASS);
            database = loadValue(dbProperties, SetupProperties.DATABASE_URL) + additionalParameters;
            username = loadValue(dbProperties, SetupProperties.DATABASE_USER);
            password = loadValue(dbProperties, SetupProperties.DATABASE_PASS);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void reloadValues() {
        loadValues();
    }



    @Override
    public String getDriverClassName() {
        return driver;
    }

    @Override
    public java.lang.String getDatabase() {
        return database;
    }

    @Override
    public java.lang.String getUsername() {
        return username;
    }

    @Override
    public java.lang.String getPassword() {
        return password;
    }

    @Override
    public java.lang.String getHostURL() {
        return GameConfig.getInstance().gamehostURL;
    }



    /**
     * @return the duration of a tick in milliseconds!
     */
    @Override
    public int getTicktime() {
        return 0;
    }

    @Override
    public long getStarttime() {
        return 0;
    }

    @Override
    public String picPath() {
        return picPath;
    }

    @Override
    public void rereadValues() {

    }

    /**
     * @return the startURL
     */
    @Override
    public String getStartURL() {
        return startURL;
    }

    /**
     * @return the srcPath
     */
    @Override
    public String getSrcPathWeb() {
        return srcPathWeb;
    }

    /**
     * @return the srcPath
     */
    @Override
    public String getSrcPathClasses() {
        return srcPathClasses;
    }

    /**
     * @return the srcPath
     */
    @Override
    public String getSrcPathResources() {
        return srcPathResources;
    }

    /**
     * @return the webserviceURL
     */
    @Override
    public String getWebserviceURL() {
        return webserviceURL;
    }

    @Override
    public boolean isAssignDeletedEmpireToAI() {
        return assignDeletedEmpireToAI;
    }

    @Override
    public boolean isUpdateTickOnStartup() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isDeleteInactive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
