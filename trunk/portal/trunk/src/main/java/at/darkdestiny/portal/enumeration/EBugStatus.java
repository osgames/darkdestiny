/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.enumeration;

import at.darkdestiny.portal.language.LANG2;

/**
 *
 * @author Admin
 */
public enum EBugStatus {

    NEW(LANG2.COMMON.EBUGSTATUS.NEW),
    VIEWED(LANG2.COMMON.EBUGSTATUS.VIEWED),
    SCHEDULED(LANG2.COMMON.EBUGSTATUS.SCHEDULED),
    PAUSED(LANG2.COMMON.EBUGSTATUS.PAUSED),
    INPROCESS(LANG2.COMMON.EBUGSTATUS.INPROCESS),
    WAITINGFOR_USERINPUT(LANG2.COMMON.EBUGSTATUS.WAITINGFOR.USERINPUT),
    WONTFIX(LANG2.COMMON.EBUGSTATUS.WONTFIX),
    FINISHED_REPOSITORY(LANG2.COMMON.EBUGSTATUS.FINISHED.REPOSITORY),
    FINISHED_LIVE(LANG2.COMMON.EBUGSTATUS.FINISHED.LIVE);

    private final String languageConstant;

    EBugStatus(String languageConstant) {
        this.languageConstant = languageConstant;
    }

    public String getLanguageConstant() {
        return languageConstant;
    }


}
