package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.BeanUtilities;
import at.darkdestiny.portal.FormatUtilities;
import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.enumeration.EBugPriority;
import at.darkdestiny.portal.enumeration.EBugStatus;
import at.darkdestiny.portal.enumeration.ESites;
import at.darkdestiny.portal.model.Bug;
import at.darkdestiny.portal.model.BugMessage;
import at.darkdestiny.portal.service.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@ManagedBean(name = "BugBean")
@SessionScoped
public class BugBean implements Serializable {

    private Bug bug;
    private BugMessage bugMessage;
    private boolean editMode;
    private boolean editAdminMode;
    private Integer releaseId = Service.versionDAO.findCurrentVersion();
    private boolean displayFixed = false;

    public ArrayList<Bug> getBugs() {

        if (releaseId >= 0) {
            return Service.bugDAO.findByReleaseId(releaseId);
        } else {
            return Service.bugDAO.findAll();
        }
    }

    public ArrayList<Bug> getBugsSorted() {
        TreeMap<Long, Bug> sorted = new TreeMap<Long, Bug>();
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        int userId = 0;
        if (loginBean.isLogged()) {
            userId = loginBean.getUser().getId();
        }
        for (Bug b : getBugs()) {
            if (!isDisplayFixed() && (b.getStatus().equals(EBugStatus.FINISHED_LIVE) || b.getStatus().equals(EBugStatus.WONTFIX))) {
                continue;
            }
            if (!loginBean.isAdminLogged() && !b.getVisible() && b.getUserId() != userId) {
                continue;
            }

            sorted.put(b.getDate(), b);
        }
        ArrayList<Bug> result = new ArrayList<Bug>();
        result.addAll(sorted.descendingMap().values());
        return result;
    }

    public int getMessageSize(Integer bugId) {
        return Service.bugMessageDAO.findByBugId(bugId).size();
    }

    public ArrayList<BugMessage> getBugMessages() {
        if (bug == null) {
            return new ArrayList<BugMessage>();
        }
        TreeMap<Long, BugMessage> sorted = Maps.newTreeMap();
        for (BugMessage bugMessage : (ArrayList<BugMessage>) Service.bugMessageDAO.findByBugId(bug.getId())) {
            sorted.put(bugMessage.getDate(), bugMessage);
        }
        ArrayList<BugMessage> result = Lists.newArrayList();
        result.addAll(sorted.values());
        return result;
    }

    public void deleteBugMessage(Integer id) {
        BugMessage bm = Service.bugMessageDAO.findById(id);
        Service.bugMessageDAO.remove(bm);
    }

    public ArrayList<BugMessage> getMessages(Integer bugId) {
        return Service.bugMessageDAO.findByBugId(bugId);
    }

    public void addBugMessage() {
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        bugMessage.setUserId(loginBean.getUser().getId());
        bugMessage.setMessage(FormatUtilities.parseText(bugMessage.getMessage()));
        bugMessage.setBugId(bug.getId());
        bugMessage.setDate(System.currentTimeMillis());
        bug.setLastChange(System.currentTimeMillis());
        Service.bugMessageDAO.add(bugMessage);
        Service.bugDAO.update(bug);
    }

    public String deleteBug(Integer id) {
        Bug r = Service.bugDAO.findById(id);
        Service.bugDAO.remove(r);
        return ESites.BUGS.getRedirectUrl();
    }

    public String assignBugTo(Integer userId) {

        bug.setWorkerId(userId);
        Service.bugDAO.update(bug);
        return Redirect.toBugs();
    }

    public String addBug() {
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");

        getBug().setDate(System.currentTimeMillis());
        bug.setDate(System.currentTimeMillis());
        bug.setUserId(loginBean.getUser().getId());
        bug.setLastChange(System.currentTimeMillis());
        bug.setReleaseId(Service.versionDAO.findCurrentVersion());
        bug = Service.bugDAO.add(getBug());

        bugMessage.setDate(bug.getDate());
        bugMessage.setMessage(FormatUtilities.parseText(bugMessage.getMessage()));
        bugMessage.setBugId(bug.getId());
        bugMessage.setUserId(loginBean.getUser().getId());
        Service.bugMessageDAO.add(bugMessage);
        return Redirect.toBugs();
    }

    public String updateBug() {
        Service.bugDAO.update(getBug());
        return Redirect.toBugs();
    }

    public void setBugDetailId(Object detailId) {
        int bugId = Integer.parseInt(String.valueOf(detailId));
        bug = Service.bugDAO.findById(bugId);

    }

    public void createNewBugMessage() {
        bugMessage = new BugMessage();
    }

    public String createNewBug() {
        this.bug = new Bug();
        this.bugMessage = new BugMessage();
        return Redirect.toBugsDetail();
    }

    public void changeEditAdminMode() {
        editAdminMode = !editAdminMode;
    }

    public boolean isEditAdminMode() {
        return editAdminMode;
    }

    public String assignBugToMe(Integer id) {
        Bug b = Service.bugDAO.findById(id);

        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        b.setWorkerId(loginBean.getUser().getId());
        Service.bugDAO.update(b);
        return Redirect.toBugs();
    }

    public EBugPriority[] getPriorityValues() {
        return EBugPriority.values();
    }

    public List<SelectItem> getPriorityValueItems() {
        List<SelectItem> values = Lists.newArrayList();
        MLBean mlBean = BeanUtilities.getMLBean();
        for (EBugPriority priority : getPriorityValues()) {
            values.add(new SelectItem(priority, mlBean.getML(priority.getLanguageConstant())));
        }
        return values;
    }

    public EBugStatus[] getStatusValues() {
        return EBugStatus.values();
    }

    public List<SelectItem> getStatusValueItems() {
        List<SelectItem> values = Lists.newArrayList();
        MLBean mlBean = BeanUtilities.getMLBean();
        for (EBugStatus status : getStatusValues()) {
            values.add(new SelectItem(status, mlBean.getML(status.getLanguageConstant())));
        }
        return values;
    }

    /**
     * @return the bug
     */
    public Bug getBug() {
        return bug;
    }

    /**
     * @param bug the bug to set
     */
    public void setBug(Bug bug) {
        this.bug = bug;
    }

    /**
     * @return the bugMessage
     */
    public BugMessage getBugMessage() {
        return bugMessage;
    }

    /**
     * @param bugMessage the bugMessage to set
     */
    public void setBugMessage(BugMessage bugMessage) {
        this.bugMessage = bugMessage;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the releaseId
     */
    public Integer getReleaseId() {
        return releaseId;
    }

    /**
     * @param releaseId the releaseId to set
     */
    public void setReleaseId(Integer releaseId) {
        this.releaseId = releaseId;
    }

    /**
     * @return the displayFixed
     */
    public boolean isDisplayFixed() {
        return displayFixed;
    }

    /**
     * @param displayFixed the displayFixed to set
     */
    public void setDisplayFixed(boolean displayFixed) {
        this.displayFixed = displayFixed;
    }
}
