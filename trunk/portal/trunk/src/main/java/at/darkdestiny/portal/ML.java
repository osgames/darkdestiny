/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

import at.darkdestiny.portal.bean.LoginBean;
import at.darkdestiny.portal.bean.MLBean;
import at.darkdestiny.portal.bean.ServiceBean;
import at.darkdestiny.portal.model.Language;
import at.darkdestiny.portal.service.Service;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import javax.faces.context.FacesContext;

/**
 *
 * @author Eobane
 */
public class ML extends Service {

    private static HashMap<Integer, Locale> locales;
    private static Locale defaultLocale = new Locale("DE", "de");

    public static Language getLanguageByUserId(int userId) {
        return Service.languageDAO.findBy(Service.userDAO.findById(userId).getLocale());
    }

    //Main method to get a ml value
    public static String getMLStr(String propertiesFile, String key, Object... params) {
        return getMLStr(propertiesFile, key, null, params);
        //return data;
    }
    //Main method to get a ml value

    public static String getMLStr(String propertiesFile, String key, Locale l, Object... params) {
        l = getActiveLocale();

        // System.out.println("[Translate: "+key+"] Active Locale: " + l.getCountry() + " " + l.getLanguage() + " " + ML.getActiveLanguage());
        if (l == null) {
            LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().get("LoginBean");
            if (loginBean != null && loginBean.isLogged()) {
                if (loginBean.getUser() != null) {
                    l = getLocale(loginBean.getUser().getId());
                }
            } else {
                ServiceBean serviceBean = (ServiceBean) FacesContext.getCurrentInstance()
                        .getExternalContext().getSessionMap().get("ServiceBean");
                if (serviceBean != null && serviceBean.isLocaleSetManually()) {
                    l = serviceBean.getLocale();
                } else {
                    l = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
                }
            }
        }
        String data = key;
        String propertiesFileName = "Bundle";
        if (propertiesFile != null) {
            propertiesFileName = propertiesFile;
        }
        if (l == null) {
            l = defaultLocale;
        }

        l = getMasterLocale(l);

        //nSystem.out.println("Constant {" + key + "] Locale [" + l.getCountry() + " " + l.getLanguage() + "] par length" + params.length);
        try {
            if (data.equals(key)) {
                data = java.util.ResourceBundle.getBundle("language/" + propertiesFileName, l).getString(key);
                // data = "Hubba";
            }
        } catch (Exception e) {
            DebugBuffer.trace("Error beim holen eines " + propertiesFile + "-Multilanguagekeys: " + e + " key : " + key);
            try {
                data = java.util.ResourceBundle.getBundle("language/" + propertiesFileName,
                        defaultLocale).getString(key);

            } catch (Exception ex) {
                return "e:" + data;
            }

        }

        int counter = 0;
        for (Object param : params) {
            counter++;
            data = data.replace("%" + counter, param.toString().replace("[", "").replace("]", ""));
        }

        return data;
    }

    public static String getMLStr(String propertiesFile, String key, int userId) {

        Locale l = locales.get(userId);
        return getMLStr(propertiesFile, key, l);
    }

    public static String getMLStr(String key, Object... params) {

        return getMLStr(null, key, params);

    }

    public static String getMLStr(String key) {
        return getMLStr("Bundle", key);
    }

    public static String getMLStr(String propertiesFile, String key) {
        return getMLStr(propertiesFile, key, "");
    }

    public static void removeLocale(int userId) {
        try {
            locales.remove(userId);
        } catch (Exception e) {
        }
    }

    public static Locale getMasterLocale(Locale in) {
        Locale out = null;
        if (in == null) {
            return in;
        }

        Language l = new Language();
        l.setCountry(in.getCountry());
        l.setLanguage(in.getLanguage());
        ArrayList<Language> languageList = Service.languageDAO.find(l);

        if (languageList != null && !languageList.isEmpty()) {
            if (languageList.get(0).getId().equals(languageList.get(0).getMasterLanguageId())) {
                return in;
            } else {
                Language master = Service.languageDAO.findById(languageList.get(0).getMasterLanguageId());
                out = new Locale(master.getLanguage(), master.getCountry());
                return out;
            }
        } else {
            return in;
        }

    }

    public static Locale getLocale(int userId) {
        if (locales == null) {

            locales = new HashMap<Integer, Locale>();

        }
        Locale l = locales.get(userId);
        if (l == null) {
            loadLocaleForUser(userId);
        }
        return locales.get(userId);

    }

    private static void loadLocaleForUser(int userId) {
        //Holen des Locales vom User
        String userLocale = userDAO.findById(userId).getLocale();
        //Suchen der dazugehörigen Sprache
        Language tempLang = Service.languageDAO.findBy(userLocale);
        //Wenn es eine MasterId gibt wird diese Sprache verwendet
        if (tempLang.getId() != tempLang.getMasterLanguageId()) {
            tempLang = Service.languageDAO.findById(tempLang.getMasterLanguageId());
            userLocale = tempLang.getLanguage() + "_" + tempLang.getCountry();
        }

        String language = userLocale.substring(0, userLocale.indexOf("_"));
        String country = userLocale.substring(userLocale.indexOf("_") + 1, userLocale.length());

        Locale l = new Locale(language, country);

        locales.put(userId, l);
    }

    public static String getLocaleString(int userId) {
        Locale l = locales.get(userId);

        return l.getLanguage() + "_" + l.getCountry();
    }

    public static String getMLStr(String key, int userId) {
        Object[] params = new Object[1];
        params[0] = "";
        return getMLStr(key, userId, params);
    }

    public static String getMLStr(String key, int userId, Object... params) {
        String data = key;
        boolean debug = false;
        Locale l = null;
        try {
            if (locales == null) {
                locales = new HashMap<Integer, Locale>();
            }
            l = locales.get(userId);
            if (debug) {
                DebugBuffer.addLine(DebugLevel.WARNING, "MULTILANGUAGE - " + "l : " + l);
            }
            if (l == null) {
                loadLocaleForUser(userId);
                l = locales.get(userId);
            }

            if (l == null) {
                l = defaultLocale;
            }

            return getMLStr(data, key, l, params);
        } catch (Exception e) {
            DebugBuffer.warning("Error beim holen eines Multilanguagekeys: " + e + " key : " + key + " for User : " + userId + " Locale : " + l);
            if (l != null) {
                DebugBuffer.warning("locale-coutner" + l.getCountry() + " locale-language : " + l.getLanguage());
            }
            // e.printStackTrace();
            return "e:" + data;
        }
    }

    public static Language getLanguageByLocale(Locale locale) {
        Language l = new Language();
        //find by detailed
        l.setLanguage(locale.getLanguage());
        l.setCountry(locale.getCountry());
        if (!Service.languageDAO.find(l).isEmpty()) {
            l = Service.languageDAO.find(l).get(0);
        }
        if (l == null) {
            l = new Language();
            l.setLanguage(locale.getLanguage());
            Language result = null;
            for (Language lang : Service.languageDAO.find(l)) {
                if (result == null) {
                    result = lang;
                } else {
                    if (lang.getMasterLanguageId() == lang.getId()) {
                        return lang;
                    }
                }
            }
            if (result != null) {
                return result;
            }
            //Unknown language take english
            l = new Language();
            l.setLanguage("en");
            l.setCountry("US");
            return Service.languageDAO.get(l);
        }
        return l;
    }

    public static String getActiveLanguage() {
        Locale l = null;

        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        if (loginBean != null && loginBean.isLogged()) {
            if (loginBean.getUser() != null) {
                l = getLocale(loginBean.getUser().getId());
            }
        } else {
            ServiceBean serviceBean = (ServiceBean) FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().get("ServiceBean");
            if (serviceBean != null && serviceBean.isLocaleSetManually()) {
                l = serviceBean.getLocale();
            } else {
                l = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
            }
        }

        if (l == null) {
            l = defaultLocale;
        }

        return l.getLanguage();
    }

    public static Locale getActiveLocale() {
        Locale l = null;

        try {
            LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().get("LoginBean");
            if (loginBean != null && loginBean.isLogged()) {
                if (loginBean.getUser() != null) {
                    l = getLocale(loginBean.getUser().getId());
                }
            } else {
                ServiceBean serviceBean = (ServiceBean) FacesContext.getCurrentInstance()
                        .getExternalContext().getSessionMap().get("ServiceBean");
                if (serviceBean != null && serviceBean.isLocaleSetManually()) {
                    l = serviceBean.getLocale();
                } else {
                    l = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
                }
            }
        } catch (Exception e) {
            DebugBuffer.produceStackTrace(e);
        }

        if (l == null) {
            l = defaultLocale;
        }

        return l;
    }
}
