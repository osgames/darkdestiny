package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.RoundUserRegistration;
import java.util.ArrayList;
import java.util.List;

/*
 *
 * Data Access Object for Order
 *
 *
 */
public class RoundUserRegistrationDAO extends ReadWriteTable<RoundUserRegistration> implements GenericDAO {

    public RoundUserRegistration findBy(Integer roundId, Integer userId) {
        RoundUserRegistration tmp = new RoundUserRegistration();
        tmp.setRoundId(roundId);
        tmp.setUserId(userId);
        ArrayList<RoundUserRegistration> result = find(tmp);
        if (result == null || result.isEmpty()) {
            return null;
        } else {
            return find(tmp).get(0);
        }
    }

    public List<RoundUserRegistration> findBy(Integer userId) {
        RoundUserRegistration tmp = new RoundUserRegistration();
        tmp.setUserId(userId);
        return find(tmp);
    }
}
