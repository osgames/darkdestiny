/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.dto.LastChange;
import at.darkdestiny.portal.model.Bug;
import at.darkdestiny.portal.model.Language;
import at.darkdestiny.portal.model.RoundUserRegistration;
import at.darkdestiny.portal.model.Screenshot;
import at.darkdestiny.portal.model.Task;
import at.darkdestiny.portal.service.Service;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@ManagedBean(name = "ServiceBean")
@SessionScoped
public class ServiceBean implements Serializable {

    public enum Theme {

        BASIC("basic", "Standard"),
        BLUE("blue", "Blau");

        private final String src;
        private final String name;

        Theme(String src, String name) {
            this.src = src;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String getSrc() {
            return src;
        }

    }

    private Language language;
    private Locale locale;
    private Theme theme = Theme.BASIC;
    private boolean localeSetManually = false;
    public ArrayList<Screenshot> screenshots;

    public ArrayList<Screenshot> getScreenshots() {
        ArrayList<Screenshot> result = new ArrayList<Screenshot>();

        TreeMap<Integer, Screenshot> sorted = new TreeMap<Integer, Screenshot>();
        for (Screenshot s : Service.screenshotDAO.findAll()) {
            sorted.put(s.getPosition(), s);
        }

        result.addAll(sorted.values());
        return result;
    }

    public String getThemeSrc() {
        if (theme == null) {
            theme = Theme.BASIC;
        }
        return theme.getSrc();
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public List<SelectItem> getThemes() {
        List<SelectItem> items = Lists.newArrayList();
        for (Theme theme : Theme.values()) {
            items.add(new SelectItem(theme, theme.getName()));
        }

        return items;
    }

    /**
     * @return the locale
     */
    public int getNumberOfRegisteredRounds(Integer userId) {
        int count = 0;
        for (RoundUserRegistration rur : Service.roundUserRegistrationDAO.findBy(userId)) {
            if (!Strings.isNullOrEmpty(rur.getRoundPassword())) {
                count++;
            }
        }
        return count;
    }

    /**
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    public ArrayList<Language> getLanguages() {
        return Service.languageDAO.findAllMasterLanguages();
    }

    /**
     * @return the localeSetManually
     */
    public boolean isLocaleSetManually() {
        return localeSetManually;
    }

    /**
     * @param localeSetManually the localeSetManually to set
     */
    public void setLocaleSetManually(boolean localeSetManually) {
        this.localeSetManually = localeSetManually;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(Language language) {
        setLocaleSetManually(true);
        this.language = language;
        Language masterLanguage = Service.languageDAO.getMasterLanguageOf(language.getId());
        this.locale = new Locale(masterLanguage.getLanguage(), masterLanguage.getCountry());
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        if (loginBean.isLogged()) {
            loginBean.updateUserLocale(this.locale);
        }
    }

    public Language getLanguage() {
        if (language != null) {
            return language;
        } else {
            Locale locale = MLBean.getLocale();
            if (locale == null) {
                locale = Locale.getDefault();
            }
            return Service.languageDAO.findBy(locale.getCountry(), locale.getLanguage());
        }
    }

    public void setLanguage(String countryCode, String languageCode) {
        setLocaleSetManually(true);

        this.language = Service.languageDAO.findBy(countryCode, languageCode);
        this.locale = new Locale(languageCode, countryCode);
        System.out.println("LOCALE=" + locale.toString());

        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        if (loginBean.isLogged()) {
            loginBean.updateUserLocale(this.locale);
        }
    }

    public String getActiveLanguage() {
        Locale loc = this.locale;

        if (loc == null) {
            loc = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        }

        System.out.println("DISPLAY LANGUAGE: " + loc.getLanguage());

        return loc.getLanguage();
    }

    public ArrayList<LastChange> getLastChanges() {
        ArrayList<LastChange> result = new ArrayList<LastChange>();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(System.currentTimeMillis());
        gc.add(GregorianCalendar.DAY_OF_MONTH, -7);
        for (Bug b : Service.bugDAO.findAll()) {
            if (!b.getVisible()) {
                continue;
            }
            GregorianCalendar tmp = new GregorianCalendar();
            tmp.setTimeInMillis(b.getLastChange());

            if (gc.compareTo(tmp) <= 0) {
                LastChange lc = new LastChange(LastChange.EChangeType.EBUG, b.getSubject(), b.getLastChange());
                lc.setBugStatus(b.getStatus());
                result.add(lc);

            }
        }

        for (Task t : Service.taskDAO.findAll()) {
            if (!t.getVisible()) {
                continue;
            }
            GregorianCalendar tmp = new GregorianCalendar();
            tmp.setTimeInMillis(t.getLastChange());
            if (gc.compareTo(tmp) <= 0) {
                LastChange lc = new LastChange(LastChange.EChangeType.ETASK, t.getSubject(), t.getLastChange());
                lc.setTaskStatus(t.getStatus());
                result.add(lc);
            }
        }

        int maxResults = 5;

        TreeMap<Long, LastChange> sorted = new TreeMap<Long, LastChange>();
        for (LastChange lc : result) {
            sorted.put(lc.getDate(), lc);
        }
        int i = 0;
        result.clear();
        for (LastChange lc : sorted.descendingMap().values()) {
            if (i < maxResults) {
                result.add(lc);
                i++;
            } else {
                break;
            }
        }
        return result;
    }
}
