/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.HighscoreEntry;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Eobane
 */
public class HighscoreEntryDAO extends ReadWriteTable<HighscoreEntry> implements GenericDAO {

    public HighscoreEntry findById(Integer id) {
        HighscoreEntry h = new HighscoreEntry();
        h.setId(id);
        return (HighscoreEntry) get(h);
    }

    public ArrayList<HighscoreEntry> findByHighscoreId(Integer highscoreId) {
        TreeMap<Integer, ArrayList<HighscoreEntry>> sorted = new TreeMap<Integer, ArrayList<HighscoreEntry>>();

        HighscoreEntry h = new HighscoreEntry();
        h.setHighscoreId(highscoreId);
        for (HighscoreEntry heTmp : find(h)) {
            ArrayList<HighscoreEntry> entries = sorted.get(heTmp.getRank());
            if (entries == null) {
                entries = new ArrayList<HighscoreEntry>();
            }
            entries.add(heTmp);
            sorted.put(heTmp.getRank(), entries);
        }
        ArrayList<HighscoreEntry> result = new ArrayList<HighscoreEntry>();
        for (Map.Entry<Integer, ArrayList<HighscoreEntry>> entry : sorted.entrySet()) {
            result.addAll(entry.getValue());
        }
        return result;
    }
}
