/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

import at.darkdestiny.core.webservice.ServerInformation;
import at.darkdestiny.core.webservice.ServerInformationService;
import at.darkdestiny.portal.dto.ServerInformationData;
import at.darkdestiny.portal.model.Round;
import at.darkdestiny.portal.service.Service;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.namespace.QName;

/**
 *
 * @author Admin
 */
public class RoundRefreshCounter extends Thread {

    private boolean keepRunning = true;
    private static HashMap<Integer, Long> rounds = new HashMap<Integer, Long>();
    //private static HashMap<Integer, ServerInformation> connections = new HashMap<Integer, ServerInformation>();
    private static HashMap<Integer, ServerInformationData> serverInformationDataList = new HashMap<Integer, ServerInformationData>();
    private static final long refreshInterval = 60000l;
    private static final long decrementValue = 1000l;

    /**
     * @param aKeepRunning the keepRunning to set
     */
    public void setKeepRunning(boolean aKeepRunning) {
        keepRunning = aKeepRunning;
    }

    @Override
    public void run() {
        while (keepRunning) {
            try {
                for (Map.Entry<Integer, Long> entry : rounds.entrySet()) {
                    long value = entry.getValue();
                    if (value <= 0l) {
                        //DebugBuffer.addLine(DebugLevel.TRACE, "round : " + entry.getKey() + " needs to be refreshed");
                    } else {
                        rounds.put(entry.getKey(), value - decrementValue);
                    }
                }
                Thread.sleep(decrementValue);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }

    public static boolean isRefresh(Integer roundId) {
        if (!rounds.containsKey(roundId)) {
            rounds.put(roundId, refreshInterval);
            return true;
        } else if (rounds.get(roundId) <= 0l) {
            rounds.put(roundId, refreshInterval);
            return true;
        } else {
            return false;
        }

    }

    public static void forceRefresh(Integer roundId) {
        rounds.remove(roundId);

    }

    public static ServerInformationData getServerInformation(Integer roundId) {
        return getServerInformation(Service.roundDAO.findById(roundId));
    }

    public static ServerInformationData getServerInformation(Round round) {
        if (round == null || round.isFinished()) {
            DebugBuffer.debug("Round finished");
            return null;
        }

        ServerInformationData serverInformationData = serverInformationDataList.get(round.getId());
        String urlString = round.getWebServiceUrl() + "serverinformation?wsdl";
        try {
            if (isRefresh(round.getId())) {
                DebugBuffer.addLine(DebugLevel.TRACE, "round : " + round.getId() + " is trying to get a refresh");
                //if (serverInformation == null) {
                URL u = new URL(urlString);
                QName qName = new QName("http://webservice.core.darkdestiny.at/", "ServerInformationService");
                ServerInformationService service = new ServerInformationService(u, qName);
                ServerInformation serverInformation = service.getServerInformationPort();
                DebugBuffer.addLine(DebugLevel.TRACE, "round : " + round.getId() + " has been refreshed: serverinformation: " + serverInformation);
                if (serverInformation != null) {
                    serverInformationDataList.put(round.getId(), new ServerInformationData(serverInformation, round));
                    DebugBuffer.addLine(DebugLevel.TRACE, "generated new ServerInformationData");
                }
            }
            return serverInformationData;
        } catch (Exception e) {
            //NOSPAM!
            DebugBuffer.addLine(DebugLevel.TRACE, "error getting connection to [" + urlString + "]" + round.getId() + " message " + e.getMessage());
            serverInformationDataList.put(round.getId(), new ServerInformationData(round));

            //e.printStackTrace();
        }
        return serverInformationDataList.get(round.getId());

    }

    public static void dropAll() {
        serverInformationDataList.clear();
        rounds.clear();
    }
}
