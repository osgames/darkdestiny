package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.FormatUtilities;
import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.enumeration.ETaskPriority;
import at.darkdestiny.portal.enumeration.ETaskStatus;
import at.darkdestiny.portal.model.Bug;
import at.darkdestiny.portal.model.BugMessage;
import at.darkdestiny.portal.model.Task;
import at.darkdestiny.portal.model.TaskMessage;
import at.darkdestiny.portal.service.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "TaskBean")
@SessionScoped
public class TaskBean implements Serializable {

    private Task task;
    private TaskMessage taskMessage;
    private boolean editMode;
    private boolean editAdminMode;
    private boolean displayFixed = false;
    private Integer releaseId = Service.versionDAO.findCurrentVersion();

    public List<Task> getTasks() {

        if (getReleaseId() >= 0) {
            return Service.taskDAO.findByReleaseId(getReleaseId());
        } else {
            return Service.taskDAO.findAll();
        }
    }

    public ArrayList<Task> getTasksSorted() {

        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        TreeMap<Long, Task> sorted = new TreeMap<Long, Task>();
        int userId = 0;
        if (loginBean.isLogged()) {
            userId = loginBean.getUser().getId();
        }

        for (Task b : getTasks()) {
            if (!displayFixed && b.getStatus().equals(ETaskStatus.FINISHED_LIVE)) {
                continue;
            }
            if (!loginBean.isAdminLogged() && !b.getVisible() && b.getUserId() != userId) {
                continue;
            }

            sorted.put(b.getDate(), b);
        }
        ArrayList<Task> result = new ArrayList<Task>();
        result.addAll(sorted.descendingMap().values());
        return result;
    }

    public int getMessageSize(Integer taskId) {
        return Service.taskMessageDAO.findByTaskId(taskId).size();
    }

    public ArrayList<TaskMessage> getTaskMessages() {

        if (task == null) {
            return new ArrayList<TaskMessage>();
        }
        TreeMap<Long, TaskMessage> sorted = Maps.newTreeMap();
        for (TaskMessage taskMessage : (ArrayList<TaskMessage>) Service.taskMessageDAO.findByTaskId(task.getId())) {
            sorted.put(taskMessage.getDate(), taskMessage);
        }
        ArrayList<TaskMessage> result = Lists.newArrayList();
        result.addAll(sorted.descendingMap().values());
        return result;

    }

    public ArrayList<TaskMessage> getMessages(Integer taskId) {
        return Service.taskMessageDAO.findByTaskId(taskId);
    }

    public void deleteTaskMessage(Integer id) {
        TaskMessage bm = Service.taskMessageDAO.findById(id);
        Service.taskMessageDAO.remove(bm);
    }

    public void addTaskMessage() {
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        taskMessage.setUserId(loginBean.getUser().getId());
        taskMessage.setTaskId(task.getId());
        taskMessage.setMessage(FormatUtilities.parseText(taskMessage.getMessage()));
        taskMessage.setDate(System.currentTimeMillis());
        task.setLastChange(System.currentTimeMillis());

        Service.taskMessageDAO.add(taskMessage);
        Service.taskDAO.update(task);
    }

    public String assignTaskToMe(Integer id) {
        Task t = Service.taskDAO.findById(id);

        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        t.setWorkerId(loginBean.getUser().getId());
        Service.taskDAO.update(t);
        return Redirect.toTasks();
    }

    public String assignTaskTo(Integer userId) {


        task.setWorkerId(userId);
        Service.taskDAO.update(task);
        return Redirect.toTasks();
    }

    public String deleteTask(Integer id) {
        Task r = Service.taskDAO.findById(id);
        Service.taskDAO.remove(r);
        return Redirect.toTasks();
    }

    public String addTask() {
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");

        getTask().setDate(System.currentTimeMillis());
        task.setDate(System.currentTimeMillis());
        task.setLastChange(System.currentTimeMillis());
        task.setUserId(loginBean.getUser().getId());
        task.setReleaseId(Service.versionDAO.findCurrentVersion());
        task = Service.taskDAO.add(getTask());

        taskMessage.setDate(task.getDate());
        taskMessage.setTaskId(task.getId());
        taskMessage.setMessage(FormatUtilities.parseText(taskMessage.getMessage()));
        taskMessage.setUserId(loginBean.getUser().getId());
        Service.taskMessageDAO.add(taskMessage);
        return Redirect.toTasks();
    }

    public String updateTask() {
        Service.taskDAO.update(getTask());
        return Redirect.toTasks();
    }

    public void setTaskDetailId(Object detailId) {
        int taskId = Integer.parseInt(String.valueOf(detailId));
        task = Service.taskDAO.findById(taskId);

    }

    public void createNewTaskMessage() {
        taskMessage = new TaskMessage();
    }

    public void changeEditAdminMode() {
        editAdminMode = !editAdminMode;
    }

    public boolean isEditAdminMode() {
        return editAdminMode;
    }

    public String createNewTask() {
        this.task = new Task();
        this.taskMessage = new TaskMessage();
        return Redirect.toTasksDetail();
    }

    public ETaskPriority[] getPriorityValues() {
        return ETaskPriority.values();
    }

    /**
     * @return the task
     */
    public Task getTask() {
        return task;
    }

    public ETaskStatus[] getStatusValues() {
        return ETaskStatus.values();
    }

    /**
     * @param task the task to set
     */
    public void setTask(Task task) {
        this.task = task;
    }

    /**
     * @return the taskMessage
     */
    public TaskMessage getTaskMessage() {
        return taskMessage;
    }

    /**
     * @param taskMessage the taskMessage to set
     */
    public void setTaskMessage(TaskMessage taskMessage) {
        this.taskMessage = taskMessage;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the releaseId
     */
    public Integer getReleaseId() {
        return releaseId;
    }

    /**
     * @param releaseId the releaseId to set
     */
    public void setReleaseId(Integer releaseId) {
        this.releaseId = releaseId;
    }

    /**
     * @return the displayFixed
     */
    public boolean isDisplayFixed() {
        return displayFixed;
    }

    /**
     * @param displayFixed the displayFixed to set
     */
    public void setDisplayFixed(boolean displayFixed) {
        this.displayFixed = displayFixed;
    }
}
