/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.MD5;
import at.darkdestiny.portal.model.User;
import java.util.ArrayList;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Eobane
 */
public class UserDAO extends ReadWriteTable<User> implements GenericDAO {

    public User findBy(String userName) {
        User u = new User();
        u.setUserName(userName);
        ArrayList<User> result = find(u);
        if ((result.isEmpty()) || (result.size() > 1)) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public User findBy(String userName, String password) {
        User u = new User();
        u.setUserName(userName);
        u.setPassword(password);
        ArrayList<User> result = find(u);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public User addWithSalt(User u) {
        if (get(u) == null) {
            String passwordSalt = RandomStringUtils.randomAscii(20);
            passwordSalt = MD5.encryptPassword(passwordSalt, passwordSalt);
            u.setPasswordSalt(passwordSalt);
            u.setPassword(getEncryptedPassword(u.getPassword(), passwordSalt));
        }

        return super.add(u);

    }

    public static String getEncryptedPassword(String password, String passwordSalt) {
        return MD5.encryptPassword(password, passwordSalt);
    }

    public User findById(Integer userId) {
        User u = new User();
        u.setId(userId);
        return (User) get(u);
    }
}
