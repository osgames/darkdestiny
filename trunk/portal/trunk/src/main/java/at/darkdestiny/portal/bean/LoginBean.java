/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.MD5;
import at.darkdestiny.portal.ML;
import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.enumeration.EUserType;
import at.darkdestiny.portal.language.LANG2;
import at.darkdestiny.portal.model.Setting;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import java.io.Serializable;
import java.util.Locale;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "LoginBean")
@SessionScoped
public class LoginBean implements Serializable, Cloneable {

    private User user;
    private boolean logged = false;
    private boolean autoLogged = false;
    private int roundId = 0;
    private boolean adminLogged = false;
    private String errorMsg = "";

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    public LoginBean() {
        user = new User();
        user.setUserName("");
        user.setPassword("");
    }

    public String autologin() {

        if (logged) {
            return null;
        }
        System.out.println("Check for Cookies");
        Map<String, Object> cookieMap = FacesContext.getCurrentInstance().getExternalContext().getRequestCookieMap();

        String userTmp = null;
        String passwordTmp = null;

        for (Map.Entry<String, Object> cookieEntry : cookieMap.entrySet()) {
            System.out.println(cookieEntry.getKey() + " " + ((Cookie) cookieEntry.getValue()).getValue());

            if (cookieEntry.getKey().equals("user")) {
                userTmp = ((Cookie) cookieEntry.getValue()).getValue();
            }
            if (cookieEntry.getKey().equals("password")) {
                passwordTmp = ((Cookie) cookieEntry.getValue()).getValue();
            }
        }

        System.out.println("Check values (" + userTmp + "/" + passwordTmp + ")");
        System.out.println("isLoggedIn=" + autoLogged + " logged=" + logged);

        if ((userTmp != null) && (passwordTmp != null)) {
            loginInternal(userTmp, passwordTmp);

            try {
                System.out.println("Try redirect");
                if (logged) {
                    return Redirect.toIndex();
                }
            } catch (Exception e) {

            }
        }
        return null;
    }

    private void destroyCookie() {
        System.out.println("Destroy password cookie");

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        Cookie cookie = new Cookie("user", "");
        // System.out.println("COOKIE PATH: " + cookie.getPath());
        cookie.setPath("/portal/faces/sites/");
        cookie.setMaxAge(0); // Expire time. -1 = by end of current session, 0 = immediately expire it, otherwise just the lifetime in seconds.
        response.addCookie(cookie);

        Cookie cookie2 = new Cookie("password", "");
        cookie2.setPath("/portal/faces/sites/");
        cookie2.setMaxAge(0);
        response.addCookie(cookie2);
    }

    private void storeCookie(String user, String password) {
        System.out.println("Create password cookie");

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        Cookie cookie = new Cookie("user", user);
        cookie.setMaxAge(3600 * 24 * 7); // Expire time. -1 = by end of current session, 0 = immediately expire it, otherwise just the lifetime in seconds.
        response.addCookie(cookie);

        Cookie cookie2 = new Cookie("password", password);
        cookie2.setMaxAge(3600 * 24 * 7);
        response.addCookie(cookie2);
    }

    /**
     * @param aUser the user to set
     */
    public void setUser(User aUser) {
        user = aUser;
        if (user.getUserType().equals(EUserType.ADMIN)) {
            adminLogged = true;
        }
    }
    private String password = "";
    private String resultMessage = "";

    public void adminLogin() {
        String reqPassword = "";
        try {
            Setting s = Service.settingDAO.findAll().get(0);
            reqPassword = s.getAdminPassword();
        } catch (Exception e) {
            setResultMessage("Setting entry in DB not found. Password couldnt be retrieved. Using 'password' as password");
            reqPassword = "password";
        }
        if (password.equals(reqPassword)) {
            User u = Service.userDAO.findBy("Admin");
            if (u == null) {
                u = new User();

                u.setUserType(EUserType.ADMIN);
                u.setLocale("de_DE");
                u.setEmail("admin@darkdestiny.at");
                u.setUserName("Admin");
                u.setPassword(reqPassword);
                u = Service.userDAO.addWithSalt(u);

            }
            if (u != null) {
                user = u;
                logged = true;
                adminLogged = true;
            }
        }

    }

    public String login() {
        login(user.getUserName(), user.getPassword());
        return Redirect.toIndex();
    }

    public void login(String userName, String password) {
        System.out.println("User size : " + Service.userDAO.findAll().size());
        for (User user : Service.userDAO.findAll()) {
            System.out.println("user : " + user.getUserName());
        }
        User u = Service.userDAO.findBy(userName);

        if (u != null) {
            password = MD5.encryptPassword(password, u.getPasswordSalt());
            loginInternal(userName, password);
        } else {
            errorMsg = LANG2.SITES.LOGIN.ERR.USERNOTFOUND;
        }

        // System.out.println("ERRORMSG: " + errorMsg);
    }

    public void loginInternal(String user, String password) {
        System.out.println("Login internal (" + user + "/" + password + ")");

        User u = Service.userDAO.findBy(user, password);

        if (u != null) {
            System.out.println("User OK");
            logged = true;
            setUser(u);

            storeCookie(u.getUserName(), u.getPassword());
        } else {
            errorMsg = LANG2.SITES.LOGIN.ERR.PASSWORDINVALID;
        }
    }

    public String logout() {
        destroyCookie();

        logged = false;
        adminLogged = false;

        TaskBean taskBean = (TaskBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("TaskBean");
        if (taskBean != null) {
            taskBean.setEditMode(false);
        }

        BugBean bugBean = (BugBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("BugBean");

        if (bugBean != null) {
            bugBean.setEditMode(false);
        }

        // HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        // session.invalidate();
        user = new User();

        try {
            // FacesContext.getCurrentInstance().getExternalContext().redirect("/sites/index.xhtml?faces-redirect=true");
            // ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        } catch (Exception e) {

        }

        System.out.println("LOGOUT");
        return Redirect.toIndex();
    }

    public boolean isLogged() {
        return logged;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the resultMessage
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * @param resultMessage the resultMessage to set
     */
    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    /**
     * @return the roundId
     */
    public int getRoundId() {
        return roundId;
    }

    /**
     * @param roundId the roundId to set
     */
    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }

    /**
     * @return the adminLogged
     */
    public boolean isAdminLogged() {
        return adminLogged;
    }

    /**
     * @param adminLogged the adminLogged to set
     */
    public void setAdminLogged(boolean adminLogged) {
        this.adminLogged = adminLogged;
    }

    void updateUserLocale(Locale locale) {
        this.user.setLocale(locale.toString());
        Service.userDAO.update(user);
        ML.removeLocale(user.getId());
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsgToML() {

        if (!errorMsg.equals("")) {
            return errorMsg;
        }
        return "";
    }

    public boolean containsError() {
        return !errorMsg.equals("");
    }

    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
