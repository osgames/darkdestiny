/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.enumeration;

import at.darkdestiny.portal.language.LANG2;

/**
 *
 * @author Admin
 */
public enum ETaskPriority {

    VERYLOW(LANG2.COMMON.ETASKPRIORITY.VERYLOW),
    LOW(LANG2.COMMON.ETASKPRIORITY.LOW),
    NORMAL(LANG2.COMMON.ETASKPRIORITY.NORMAL),
    HIGH(LANG2.COMMON.ETASKPRIORITY.HIGH),
    VERYHIGH(LANG2.COMMON.ETASKPRIORITY.VERYHIGH);

    private final String languageConstant;

    ETaskPriority(String languageConstant) {
        this.languageConstant = languageConstant;
    }

    public String getLanguageConstant() {
        return languageConstant;
    }
}
