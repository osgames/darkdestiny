/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.model;

import at.darkdestiny.core.webservice.PlayerStatisticListEntry;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author LAZYPAD
 */
@TableNameAnnotation(value = "highscoreentry")
public class HighscoreEntry extends Model<HighscoreEntry> {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("highscoreId")
    private Integer highscoreId;
    @FieldMappingAnnotation("gameName")
    private String gameName;
    @FieldMappingAnnotation("rank")
    private Integer rank;
    @FieldMappingAnnotation("militaryPoints")
    private Long militaryPoints;
    @FieldMappingAnnotation("populationPoints")
    private Long populationPoints;
    @FieldMappingAnnotation("researchPoints")
    private Long researchPoints;
    @FieldMappingAnnotation("resourcePoints")
    private Long resourcePoints;

    public HighscoreEntry() {
    }

    public HighscoreEntry(PlayerStatisticListEntry psle, int highscoreId) {
        this.rank = psle.getRank();
        this.gameName = psle.getGameName();
        this.highscoreId = highscoreId;
        this.militaryPoints = psle.getMilitaryPoints();
        this.researchPoints = psle.getResearchPoints();
        this.resourcePoints = psle.getRessourcePoints();
        this.populationPoints = psle.getPopulationPoints();
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the highscoreId
     */
    public Integer getHighscoreId() {
        return highscoreId;
    }

    /**
     * @param highscoreId the highscoreId to set
     */
    public void setHighscoreId(Integer highscoreId) {
        this.highscoreId = highscoreId;
    }

    /**
     * @return the gameName
     */
    public String getGameName() {
        return gameName;
    }

    /**
     * @param gameName the gameName to set
     */
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    /**
     * @return the rank
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    /**
     * @return the points
     */
    public Long getPoints() {
        return militaryPoints + resourcePoints + populationPoints + researchPoints;

    }

    /**
     * @return the militaryPoints
     */
    public Long getMilitaryPoints() {
        return militaryPoints;
    }

    /**
     * @return the populationPoints
     */
    public Long getPopulationPoints() {
        return populationPoints;
    }

    /**
     * @return the researchPoints
     */
    public Long getResearchPoints() {
        return researchPoints;
    }

    /**
     * @return the resourcePoints
     */
    public Long getResourcePoints() {
        return resourcePoints;
    }
}
