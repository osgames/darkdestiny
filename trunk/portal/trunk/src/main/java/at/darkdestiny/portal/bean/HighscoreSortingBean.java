/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import org.richfaces.component.SortOrder;

/**
 *
 * @author LAZYPAD
 */
@ManagedBean(name = "HighscoreSortingBean")
@SessionScoped
public class HighscoreSortingBean implements Serializable{

    private Map<String, SortOrder> sortsOrders;
    private List<String> sortPriorities;
    private boolean multipleSorting = false;
    private static final String SORT_PROPERTY_PARAMETER = "sortProperty";

    public HighscoreSortingBean() {
        sortsOrders = new HashMap<String, SortOrder>();
        sortPriorities = new ArrayList<String>();

    }

    public void modeChanged(ValueChangeEvent event) {
        reset();
    }

    public void reset() {
        sortPriorities.clear();
        sortsOrders.clear();
    }

    public boolean isMultipleSorting() {
        return multipleSorting;
    }

    public void setMultipleSorting(boolean multipleSorting) {
        this.multipleSorting = multipleSorting;
    }

    public List<String> getSortPriorities() {
        return sortPriorities;
    }

    public Map<String, SortOrder> getSortsOrders() {
        return sortsOrders;
    }

    public void sortBy(String field) {
        if (!sortsOrders.containsKey(field)) {
            sortsOrders.put(field, SortOrder.unsorted);
        }
        for (Map.Entry<String, SortOrder> entry : sortsOrders.entrySet()) {
            if (entry.getKey().equals(field)) {
                SortOrder order = entry.getValue();
                if (order.equals(SortOrder.ascending)) {
                    order = SortOrder.descending;
                } else if (order.equals(SortOrder.descending)) {
                    order = SortOrder.ascending;
                } else if (order == null || order.equals(SortOrder.unsorted)) {
                    order = SortOrder.ascending;
                }
                sortsOrders.put(entry.getKey(), order);
            } else {
                sortsOrders.put(entry.getKey(), SortOrder.unsorted);
            }
        }
    }

    public boolean isSortOrderAscending(String field) {
        return isSortOrder(field, SortOrder.ascending);
    }

    public boolean isSortOrderDescending(String field) {
        return isSortOrder(field, SortOrder.descending);
    }

    public boolean isSortOrderUnsorted(String field) {
        return isSortOrder(field, SortOrder.unsorted);
    }

    public boolean isSortOrder(String field, SortOrder sortOrder) {
        if (sortsOrders.get(field) == null) {
            return false;
        } else {
            return sortsOrders.get(field).equals(sortOrder);
        }
    }

    public SortOrder getSortOrder(String field) {
        if (sortsOrders.get(field) == null) {
            sortsOrders.put(field, SortOrder.unsorted);
        }
        return sortsOrders.get(field);
    }
}
