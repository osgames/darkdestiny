/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.portal.model.News;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.MailingList;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class MailingListDAO extends ReadWriteTable<MailingList> implements GenericDAO {

    public MailingList findById(Integer id) {
        MailingList ml = new MailingList();
        ml.setId(id);
        return (MailingList) get(ml);
    }
}
