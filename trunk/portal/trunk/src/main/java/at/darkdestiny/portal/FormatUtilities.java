/*
 * Utilities.java
 *
 * Created on 11. März 2003, 20:25
 */
package at.darkdestiny.portal;

import java.util.ArrayList;

/**
 *
 * @author Rayden
 */
public final class FormatUtilities {

    private FormatUtilities() {
        // Empty constructor for utility class
    }

    public static String escapeChars(String in) {
        ArrayList<String> ignoreText = new ArrayList<String>();

        boolean finished = false;

        String res = in.replace("[html}[/html]", "");

        while (!finished) {
            int startIndex = res.indexOf("[html]");
            int endIndex = res.indexOf("[/html]");

            if ((startIndex == -1) || (endIndex == -1)) {
                finished = true;
            } else {
                //  Logger.getLogger().write("SI: " + startIndex + " EI: " + endIndex);

                ignoreText.add(res.substring(startIndex + 6, endIndex));

                //   Logger.getLogger().write("Adding to ignore: " + res.substring(startIndex+6, endIndex));
                //   Logger.getLogger().write("Try to replace >>[html]"+ignoreText.get(ignoreText.size()-1)+"[/html]<< with >>["+ignoreText.size()+"]<<");

                res = res.replace("[html]" + ignoreText.get(ignoreText.size() - 1) + "[/html]", "[" + ignoreText.size() + "]");
            }
        }

        for (int i = 0; i < ignoreText.size(); i++) {
            res = res.replace("[" + (i + 1) + "]", ignoreText.get(i));
        }

        return res;
    }

    public static String toISO(String in) {
        String output = in.replace("�", "&auml;");
        output = output.replace("�", "&ouml;");
        output = output.replace("�", "&uuml;");
        output = output.replace("�", "&Auml;");
        output = output.replace("�", "&Ouml;");
        output = output.replace("�", "&Uuml;");
        output = output.replace("�", "&szlig;");
        output = output.replace("<BR>", "&lt;br/&gt;");
        output = output.replace("<br>", "&lt;br/&gt;");
        output = output.replace("<BR/>", "&lt;br/&gt;");
        output = output.replace("<br/>", "&lt;br/&gt;");
        output = output.replace("<B>", "&lt;b&gt;");
        output = output.replace("<b>", "&lt;b&gt;");
        output = output.replace("</B>", "&lt;/b&gt;");
        output = output.replace("</b>", "&lt;/b&gt;");

        return output;
    }

    public static String parseText(String textToParse) {
        return parseText(textToParse, false);
    }
    public static String parseText(String textToParse, boolean ignoreSmiley) {
       // textToParse = textToParse.replace("\\n", "<BR/>");
        StringBuffer sb = new StringBuffer(textToParse);


        for (int i = 0; i < sb.length(); i++) {
            if ((int) sb.charAt(i) == 13 || (int) sb.charAt(i) == 10) {
                sb.replace(i, i + 2, "<BR/>");
            }

            if (!ignoreSmiley) {
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 58) && ((int) sb.charAt(i + 1) == 41)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_smile.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 58) && ((int) sb.charAt(i + 1) == 68)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_mrgreen.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 58) && ((int) sb.charAt(i + 1) == 80)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_razz.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 58) && ((int) sb.charAt(i + 1) == 112)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_razz.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 56) && ((int) sb.charAt(i + 1) == 58)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_cool.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 56) && ((int) sb.charAt(i + 1) == 41)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_cool.gif\"/>");
                    }
                }
                if ((i + 1) < sb.length()) {
                    if (((int) sb.charAt(i) == 59) && ((int) sb.charAt(i + 1) == 41)) {
                        sb.replace(i, i + 2, "<IMG border=0 src=\"http://forum.thedarkdestiny.at/images/smilies/icon_wink.gif\"/>");
                    }
                }
            }
        }



        return sb.toString();
    }

    public static String resizeImg(String data) {

        String result = "";
        String href = "";
        boolean foundImg = false;
        boolean sourceFound = false;
        boolean hrefStarts = false;

        int sourceCounter = 0;
        if(data == null){
            return "";
        }
        for (int i = 0; i < data.length(); i++) {
            String subString = data.substring(i);
            if (subString.toLowerCase().startsWith("<img")) {
                foundImg = true;
                result += "<center><a %HREF% target='new'><img style='width:200px;height=200px;' ";
                i += 4;
            } else if (foundImg && subString.startsWith("/>")) {
                i += 2;
                result += "/></a></center>";
                foundImg = false;
                sourceFound = false;

                if (!href.equals("")) {
                    if (href.endsWith("/")) {
                        href = href.substring(0, href.length() - 1);
                    }
                    href = "href=\"" + href;
                    href += "\" ";
                }

                result = result.replace("%HREF%", href);
                href = "";
            } else {
                if (i <= data.length()) {
                    result += data.charAt(i);
                }
            }

            if (subString.toLowerCase().startsWith("src=") && foundImg) {
                sourceFound = true;
            }
            if (sourceFound) {
                if ((subString.startsWith("'") || subString.startsWith("\""))) {
                    hrefStarts = true;
                } else if (hrefStarts && ((subString.startsWith("'") || subString.startsWith("\"")))) {
                    hrefStarts = false;
                    sourceFound = false;
                } else if (hrefStarts) {
                    href += data.charAt(i);
                }

            }

        }




        return result;
    }
}
