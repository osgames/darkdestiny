/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.servlet;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stefan
 */
public class GetThumbnail extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("image/png");
        ServletOutputStream out = response.getOutputStream();

        try {
            String baseImage = request.getParameter("baseImage");

            // System.out.println("Loading image " + this.getServletContext().getRealPath("/resources/screenshot") + "/" + baseImage);
            Image image = (Image) ImageIO.read(new File(this.getServletContext().getRealPath("/resources/screenshot/") + "/" + baseImage));
            // Image image = (Image) ImageIO.read(new URL("http://localhost:8084/DDPortal/resources/screenshot/"+baseImage));

            int targetWidth = Integer.parseInt("100");
            int targetHeight = Integer.parseInt("100");

            int thumbWidth = targetWidth;
            int thumbHeight = targetHeight;

            // Make sure the aspect ratio is maintained, so the image is not skewed
            // thumbRatio positive if picture broader than wide
            // double thumbRatio = (double) thumbWidth / (double) thumbHeight;
            int imageWidth = image.getWidth(null);
            int imageHeight = image.getHeight(null);
            // imageRatio positive if picture broader than wide
            double imageRatio = (double) imageWidth / (double) imageHeight;

            int addX = 0;
            int addY = 0;

            if (imageRatio >= 1d) {
                thumbHeight = (int) ((double) thumbHeight / imageRatio);
                addY = (int) ((100 - thumbHeight) / 2d);
            } else {
                thumbWidth = (int) ((double) thumbWidth * imageRatio);
                addX = (int) ((100 - thumbWidth) / 2d);
            }

            // Draw the scaled image
            BufferedImage thumbImage = new BufferedImage(targetWidth,
                    targetHeight, BufferedImage.TYPE_INT_ARGB);

            Graphics2D graphics2D = thumbImage.createGraphics();
            // Use of BILNEAR filtering to enable smooth scaling
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            // graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);

            // White Background
            graphics2D.setPaint(Color.WHITE);
               graphics2D.drawImage(image, addX, addY, thumbWidth, thumbHeight, null);

                // Write the scaled image to the outputstream
            // ByteArrayOutputStream out = new ByteArrayOutputStream();
            ImageIO.write(thumbImage, "png", out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
