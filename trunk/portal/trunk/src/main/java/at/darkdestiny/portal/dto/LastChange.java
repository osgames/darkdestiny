/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.dto;

import at.darkdestiny.portal.enumeration.EBugStatus;
import at.darkdestiny.portal.enumeration.ETaskStatus;

/**
 *
 * @author LAZYPAD
 */
public class LastChange {

    private EChangeType type;
    private String name;
    private Long date;
    private ETaskStatus taskStatus;
    private EBugStatus bugStatus;

    public LastChange(EChangeType type, String name, Long date) {
        this.type = type;
        this.name = name;
        this.date = date;
    }

    /**
     * @return the type
     */
    public EChangeType getType() {
        return type;
    }

    public boolean isBug() {
        return type.equals(EChangeType.EBUG);
    }

    public boolean isTask() {
        return type.equals(EChangeType.ETASK);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @return the taskStatus
     */
    public ETaskStatus getTaskStatus() {
        return taskStatus;
    }

    /**
     * @param taskStatus the taskStatus to set
     */
    public void setTaskStatus(ETaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    /**
     * @return the bugStatus
     */
    public EBugStatus getBugStatus() {
        return bugStatus;
    }

    /**
     * @param bugStatus the bugStatus to set
     */
    public void setBugStatus(EBugStatus bugStatus) {
        this.bugStatus = bugStatus;
    }

    public enum EChangeType {

        EBUG, ETASK
    };
}
