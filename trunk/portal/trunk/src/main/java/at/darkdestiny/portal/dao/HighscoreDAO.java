/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.Highscore;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 *
 * @author Eobane
 */
public class HighscoreDAO extends ReadWriteTable<Highscore> implements GenericDAO {

    public Highscore findById(Integer id) {
        Highscore h = new Highscore();
        h.setId(id);
        return (Highscore) get(h);
    }

    public Highscore findByDate(long actTick, int roundId) {

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(actTick);

        GregorianCalendar result = new GregorianCalendar();
        result.set(GregorianCalendar.HOUR_OF_DAY, 0);
        result.set(GregorianCalendar.MINUTE, 0);
        result.set(GregorianCalendar.SECOND, 0);
        result.set(GregorianCalendar.MILLISECOND, 0);

        Highscore h = new Highscore();
        h.setRoundId(roundId);
        h.setDate(actTick);

        ArrayList<Highscore> highscores = find(h);

        if (!highscores.isEmpty()) {
            return highscores.get(highscores.size()-1);
        } else {
            return null;
        }


    }

    public ArrayList<Highscore> findByRoundId(int roundId) {
        Highscore h = new Highscore();
        h.setRoundId(roundId);
        return find(h);
    }
}
