/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.Bug;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class BugDAO extends ReadWriteTable<Bug> implements GenericDAO {
    public Bug findById(Integer id) {
        Bug b = new Bug();
        b.setId(id);
        return (Bug)get(b);
    }

    public ArrayList<Bug> findByReleaseId(Integer releaseId) {
   
        Bug b = new Bug();
        b.setReleaseId(releaseId);
        return find(b);  
    }
}
