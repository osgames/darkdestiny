/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "CalendarBean")
@SessionScoped
public class CalendarBean implements Serializable {

    private static final String[] WEEK_DAY_LABELS = new String[]{"Sun *",
        "Mon +", "Tue +", "Wed +", "Thu +", "Fri +", "Sat *"};
    private Locale locale;
    private boolean popup;
    private boolean readonly;
    private boolean showInput;
    private boolean enableManualInput;
    private String pattern;
    private Date currentDate;
    private Date selectedStartDate;
    private Date selectedEndDate;
    private String jointPoint;
    private String direction;
    private String boundary;
    private String startDateString;
    private String endDateString;
    private boolean useCustomDayLabels;
    private boolean endDate;

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public boolean isPopup() {
        return popup;
    }

    public void setPopup(boolean popup) {
        this.popup = popup;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public CalendarBean() {
        createNewDefaultValues();
    }

    public void createNewDefaultValues() {

        locale = Locale.US;
        popup = true;
        pattern = "MMM d, yyyy HH:mm";
        jointPoint = "bottomleft";
        direction = "bottomright";
        readonly = true;
        enableManualInput = false;
        showInput = true;
        boundary = "inactive";
        selectedStartDate = new Date();
        selectedEndDate = new Date();
        endDate = false;
    }

    public boolean isShowInput() {
        return showInput;
    }

    public void setShowInput(boolean showInput) {
        this.showInput = showInput;
    }

    public boolean isEnableManualInput() {
        return enableManualInput;
    }

    public void setEnableManualInput(boolean enableManualInput) {
        this.enableManualInput = enableManualInput;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    public void selectLocale(ValueChangeEvent event) {

        String tLocale = (String) event.getNewValue();
        if (tLocale != null) {
            String lang = tLocale.substring(0, 2);
            String country = tLocale.substring(3);
            locale = new Locale(lang, country, "");
        }
    }

    public boolean isUseCustomDayLabels() {
        return useCustomDayLabels;
    }

    public void setUseCustomDayLabels(boolean useCustomDayLabels) {
        this.useCustomDayLabels = useCustomDayLabels;
    }

    public Object getWeekDayLabelsShort() {
        if (isUseCustomDayLabels()) {
            return WEEK_DAY_LABELS;
        } else {
            return null;
        }
    }

    public String getCurrentDateAsText() {
        Date currentDate = getCurrentDate();
        if (currentDate != null) {
            return DateFormat.getDateInstance(DateFormat.FULL).format(
                    currentDate);
        }

        return null;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public void setCurrentDateAsObject(Object currentDate) {
        this.currentDate = (java.util.Date) currentDate;
    }

    public Date getSelectedStartDate() {
        return selectedStartDate;
    }

    public Date getSelectedEndDate() {
        return selectedEndDate;
    }

    public Long getSelectedEndDateAsLong() {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(selectedEndDate);
        return gc.getTimeInMillis();
    }

    public Long getSelectedStartDateAsLong() {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(selectedStartDate);
        return gc.getTimeInMillis();
    }

    public void setSelectedStartDate(Date selectedDate) {
        this.selectedStartDate = selectedDate;
    }

    public void setSelectedEndDate(Date selectedDate) {
        this.selectedEndDate = selectedDate;
    }

    public void setSelectedDateAsLong(Long val) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(val);
        this.selectedStartDate = gc.getTime();
    }

    public String getJointPoint() {
        return jointPoint;
    }

    public void setJointPoint(String jointPoint) {
        this.jointPoint = jointPoint;
    }

    public void selectJointPoint(ValueChangeEvent event) {
        jointPoint = (String) event.getNewValue();
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void selectDirection(ValueChangeEvent event) {
        direction = (String) event.getNewValue();
    }

    public String getBoundary() {
        return boundary;
    }

    public void setBoundary(String boundary) {
        this.boundary = boundary;
    }

    /**
     * @return the dateString
     */
    public String getStartDateString() {
        return startDateString;
    }

    /**
     * @param dateString the dateString to set
     */
    public void setStartDateString(String startDateString) {

        Long l = Long.valueOf(startDateString);
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(l);
        selectedStartDate = gc.getTime();
        this.startDateString = startDateString;
    }

    public String getEndDateString() {
        return endDateString;
    }

    /**
     * @param dateString the dateString to set
     */
    public void setEndDateString(String endDateString) {

        Long l = Long.valueOf(endDateString);
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(l);
        selectedEndDate = gc.getTime();
        this.endDateString = endDateString;
    }

    /**
     * @return the endDate
     */
    public boolean isEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(boolean endDate) {
        this.endDate = endDate;
    }
}
