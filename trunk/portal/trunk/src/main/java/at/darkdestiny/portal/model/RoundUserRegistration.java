/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author LAZYPAD
 */
@TableNameAnnotation(value = "rounduserregistration")
public class RoundUserRegistration extends Model<RoundUserRegistration> {

    @FieldMappingAnnotation("userId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer userId;
    @FieldMappingAnnotation("roundId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true)
    private Integer roundId;
    @FieldMappingAnnotation("registrationToken")
    private String registrationToken;
    @DefaultValue("")
    @FieldMappingAnnotation("roundPassword")
    private String roundPassword;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the roundId
     */
    public Integer getRoundId() {
        return roundId;
    }

    /**
     * @param roundId the roundId to set
     */
    public void setRoundId(Integer roundId) {
        this.roundId = roundId;
    }

    /**
     * @return the registrationToken
     */
    public String getRegistrationToken() {
        return registrationToken;
    }

    /**
     * @param registrationToken the registrationToken to set
     */
    public void setRegistrationToken(String registrationToken) {
        this.registrationToken = registrationToken;
    }

    /**
     * @return the roundPassword
     */
    public String getRoundPassword() {
        return roundPassword;
    }

    /**
     * @param roundPassword the roundPassword to set
     */
    public void setRoundPassword(String roundPassword) {
        this.roundPassword = roundPassword;
    }
    
    
}
