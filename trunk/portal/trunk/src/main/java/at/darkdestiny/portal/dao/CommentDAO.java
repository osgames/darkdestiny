/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.Comment;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class CommentDAO extends ReadWriteTable<Comment> implements GenericDAO {

    public ArrayList<Comment> findByNewsId(Integer newsId) {
        Comment c = new Comment();
        c.setNewsId(newsId);
        return find(c);
    }

    public Comment findById(Integer id) {
        Comment c = new Comment();
        c.setId(id);
        return get(c);
    }

}
