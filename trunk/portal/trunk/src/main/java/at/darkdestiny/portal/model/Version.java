/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.portal.bean.MLBean;
import at.darkdestiny.portal.service.Service;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Admin
 */
@TableNameAnnotation(value = "version")
public class Version extends Model {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("version")
    private String version;
    @FieldMappingAnnotation("description")
    private String description;
    @FieldMappingAnnotation("current")
    private Boolean current;
    @FieldMappingAnnotation("released")
    private Boolean released;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the current
     */
    public Boolean getCurrent() {
        return current;
    }

    /**
     * @param current the current to set
     */
    public void setCurrent(Boolean current) {
        this.current = current;
    }

    /**
     * @return the released
     */
    public Boolean getReleased() {
        return released;
    }

    /**
     * @param released the released to set
     */
    public void setReleased(Boolean released) {
        this.released = released;
    }

    public void changeReleaseStatus() {
        this.released = !this.released;
        Service.versionDAO.update(this);
    }
    
}
