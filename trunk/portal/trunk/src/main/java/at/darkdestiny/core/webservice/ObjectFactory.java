
package at.darkdestiny.core.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.darkdestiny.core.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Playerstatistic_QNAME = new QName("http://webservice.core.darkdestiny.at/", "playerstatistic");
    private final static QName _PlayerStatisticList_QNAME = new QName("http://webservice.core.darkdestiny.at/", "playerStatisticList");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.darkdestiny.core.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PlayerStatisticListEntry }
     * 
     */
    public PlayerStatisticListEntry createPlayerStatisticListEntry() {
        return new PlayerStatisticListEntry();
    }

    /**
     * Create an instance of {@link PlayerStatisticList }
     * 
     */
    public PlayerStatisticList createPlayerStatisticList() {
        return new PlayerStatisticList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlayerStatisticListEntry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.core.darkdestiny.at/", name = "playerstatistic")
    public JAXBElement<PlayerStatisticListEntry> createPlayerstatistic(PlayerStatisticListEntry value) {
        return new JAXBElement<PlayerStatisticListEntry>(_Playerstatistic_QNAME, PlayerStatisticListEntry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlayerStatisticList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.core.darkdestiny.at/", name = "playerStatisticList")
    public JAXBElement<PlayerStatisticList> createPlayerStatisticList(PlayerStatisticList value) {
        return new JAXBElement<PlayerStatisticList>(_PlayerStatisticList_QNAME, PlayerStatisticList.class, null, value);
    }

}
