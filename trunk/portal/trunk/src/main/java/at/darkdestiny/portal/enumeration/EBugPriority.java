/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.enumeration;

import at.darkdestiny.portal.language.LANG2;

/**
 *
 * @author Admin
 */
public enum EBugPriority {

    VERYLOW(LANG2.COMMON.EBUGPRIORITY.VERYLOW),
    LOW(LANG2.COMMON.EBUGPRIORITY.LOW),
    NORMAL(LANG2.COMMON.EBUGPRIORITY.NORMAL),
    HIGH(LANG2.COMMON.EBUGPRIORITY.HIGH),
    VERYHIGH(LANG2.COMMON.EBUGPRIORITY.VERYHIGH);

    private final String languageConstant;

    EBugPriority(String languageConstant) {
        this.languageConstant = languageConstant;
    }

    public String getLanguageConstant() {
        return languageConstant;
    }

}
