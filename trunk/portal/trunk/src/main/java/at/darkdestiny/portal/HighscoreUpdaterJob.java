/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

import at.darkdestiny.core.webservice.PlayerStatisticList;
import at.darkdestiny.core.webservice.PlayerStatisticListEntry;
import at.darkdestiny.portal.dto.ServerInformationData;
import at.darkdestiny.portal.model.Highscore;
import at.darkdestiny.portal.model.HighscoreEntry;
import at.darkdestiny.portal.model.Round;
import at.darkdestiny.portal.service.Service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Admin
 */
public class HighscoreUpdaterJob implements Job {

    public void execute(JobExecutionContext jec) throws JobExecutionException {
       update();
    }
    
    public static void update(){
        try {
            System.out.println("Starting highscoreupdates");
            for (Round r : Service.roundDAO.findAll()) {
                long startDate = r.getStartDate();
                long current = System.currentTimeMillis();
                DateGenerator dg = new DateGenerator(startDate);

                getEntriesAndSave(dg.getActDay(), r);
                long nextDay;
                while ((nextDay = dg.getNextDay()) < current) {

                    if (r.getEndDate() > 0 && r.getEndDate() < nextDay) {
                        break;
                    }
                    getEntriesAndSave(nextDay, r);
                }
            }


        } catch (Exception e) {
            System.out.println("Error in highscoreupdater : " + e.toString());
            e.printStackTrace();
        } 
    }

    public static void getEntriesAndSave(long date, Round r) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(date);
        Date d = new Date(date);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        Highscore hs = new Highscore();
        hs.setDate(gc.getTimeInMillis());
        hs.setRoundId(r.getId());
        //Check if highscore already build
        if (!Service.highscoreDAO.find(hs).isEmpty()) {
            return;
        }
        //Get data from server
        ServerInformationData si = RoundRefreshCounter.getServerInformation(r);
        if (si == null) {
            return;
        }
        PlayerStatisticList statistics = si.getStatistics(gc.getTimeInMillis());


        //Check if entries were found
        if (statistics.getPlayerStatistics().isEmpty()) {
            return;
        }
        hs = Service.highscoreDAO.add(hs);

        for (PlayerStatisticListEntry psle : statistics.getPlayerStatistics()) {
            HighscoreEntry hse = new HighscoreEntry(psle, hs.getId());
            Service.highscoreEntryDAO.add(hse);
        }
    }
}
