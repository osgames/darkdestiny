/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.TaskMessage;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class TaskMessageDAO extends ReadWriteTable<TaskMessage> implements GenericDAO {
    public TaskMessage findById(Integer id) {
        TaskMessage bm = new TaskMessage();
        bm.setId(id);
        return (TaskMessage)get(bm);
    }

     public ArrayList<TaskMessage> findByTaskId(Integer taskId) {
        TaskMessage bm = new TaskMessage();
        bm.setTaskId(taskId);
        return find(bm);
    }

}
