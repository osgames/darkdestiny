/*
 *
 * Should be extended from service
 * Holds all DAOs for database access
 * and various functions needed by the service classes
 *
 */
package at.darkdestiny.portal.service;

import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.portal.dao.BugDAO;
import at.darkdestiny.portal.dao.BugMessageDAO;
import at.darkdestiny.portal.dao.CommentDAO;
import at.darkdestiny.portal.dao.HighscoreDAO;
import at.darkdestiny.portal.dao.HighscoreEntryDAO;
import at.darkdestiny.portal.dao.LanguageDAO;
import at.darkdestiny.portal.dao.MailingListDAO;
import at.darkdestiny.portal.dao.MailingListEntryDAO;
import at.darkdestiny.portal.dao.NewsDAO;
import at.darkdestiny.portal.dao.RoundDAO;
import at.darkdestiny.portal.dao.RoundUserRegistrationDAO;
import at.darkdestiny.portal.dao.ScreenshotDAO;
import at.darkdestiny.portal.dao.SettingDAO;
import at.darkdestiny.portal.dao.TaskDAO;
import at.darkdestiny.portal.dao.TaskMessageDAO;
import at.darkdestiny.portal.dao.UserDAO;
import at.darkdestiny.portal.dao.VersionDAO;

public class Service {

    public static RoundDAO roundDAO = (RoundDAO) DAOFactory.get(RoundDAO.class);
   public static RoundUserRegistrationDAO roundUserRegistrationDAO = (RoundUserRegistrationDAO) DAOFactory.get(RoundUserRegistrationDAO.class);
    public static SettingDAO settingDAO = (SettingDAO) DAOFactory.get(SettingDAO.class);
    public static NewsDAO newsDAO = (NewsDAO) DAOFactory.get(NewsDAO.class);
    public static CommentDAO commentDAO = (CommentDAO) DAOFactory.get(CommentDAO.class);
    public static ScreenshotDAO screenshotDAO = (ScreenshotDAO) DAOFactory.get(ScreenshotDAO.class);
    public static LanguageDAO languageDAO = (LanguageDAO) DAOFactory.get(LanguageDAO.class);
    public static UserDAO userDAO = (UserDAO) DAOFactory.get(UserDAO.class);
    public static BugDAO bugDAO = (BugDAO) DAOFactory.get(BugDAO.class);
    public static BugMessageDAO bugMessageDAO = (BugMessageDAO) DAOFactory.get(BugMessageDAO.class);
    public static TaskDAO taskDAO = (TaskDAO) DAOFactory.get(TaskDAO.class);
    public static VersionDAO versionDAO = (VersionDAO) DAOFactory.get(VersionDAO.class);
    public static TaskMessageDAO taskMessageDAO = (TaskMessageDAO) DAOFactory.get(TaskMessageDAO.class);
    public static HighscoreDAO highscoreDAO = (HighscoreDAO) DAOFactory.get(HighscoreDAO.class);
    public static HighscoreEntryDAO highscoreEntryDAO = (HighscoreEntryDAO) DAOFactory.get(HighscoreEntryDAO.class);
    public static MailingListDAO mailingListDAO = (MailingListDAO) DAOFactory.get(MailingListDAO.class);
    public static MailingListEntryDAO mailingListEntryDAO = (MailingListEntryDAO) DAOFactory.get(MailingListEntryDAO.class);
}
