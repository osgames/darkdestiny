/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.ML;
import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.language.LANG2;
import at.darkdestiny.portal.model.Language;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import at.darkdestiny.portal.servlet.MyCaptcha;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.AssertTrue;

@ManagedBean(name = "RegistrationBean")
@SessionScoped
public class RegistrationBean implements Serializable, Cloneable {

    private User user;
    //@Length(min = 4, max = 40, message = "Zeichenlšnge 4-40")
    private String confirmationPassword = "";
    private String resultMsg = "";
    private Integer languageId;
    private String validate;
    private boolean error = false;

    @AssertTrue(message = LANG2.SITES.REGISTRATION.ERR.PASSWORDSNOTEQUAL)
    public boolean isPasswordsEquals() {
        return user.getPassword().equals(confirmationPassword);
    }

    @AssertTrue(message = LANG2.SITES.REGISTRATION.ERR.USERALREADYEXISTING)
    public boolean isUserExisting() {
        if (Service.userDAO.findBy(user.getUserName()) == null) {
            return true;
        } else {
            return false;
        }
    }

    @AssertTrue(message = LANG2.GLOBAL.VALIDATION.ERR.ACCEPTUSERAGREEMENT)
    public boolean isUserAgreementAccepted() {
        return user.getAcceptedUserAgreement();
    }

    public void createNewUser() {
        user = new User();
        user.setRoundNotification(true);
    }

    /**
     * @return the user
     */
    public User getUser() {
        if (user == null) {
            createNewUser();
        }
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the confirmationPassword
     */
    public String getConfirmationPassword() {
        return confirmationPassword;
    }

    /**
     * @param confirmationPassword the confirmationPassword to set
     */
    public void setConfirmationPassword(String confirmationPassword) {
        this.confirmationPassword = confirmationPassword;
    }

    public String getValidate() {
        return validate;
    }

    public void setValidate(String validate) {
        this.validate = validate;
    }

    public List<SelectItem> getLanguageDropDown() {
        MLBean mlBean = (MLBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("MLBean");
        List<SelectItem> names = new ArrayList<SelectItem>();
        for (Language l : Service.languageDAO.findAll()) {
            names.add(new SelectItem(String.valueOf(l.getId()), mlBean.getML(l.getName())));
        }
        if (names.isEmpty()) {
            names.add(new SelectItem(1, mlBean.getML(LANG2.SITES.REGISTER.ERR.NOLANGUAGE)));
        }
        return names;
    }

    public String register() {

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        javax.servlet.http.HttpSession session = request.getSession();
        String userCaptcha = validate;
        String captcha = (String) session.getAttribute(MyCaptcha.CAPTCHA_KEY);
        boolean equal = true;
        if (captcha.length() != userCaptcha.length()) {
            equal = false;
        } else {
            for (int i = 0; i < captcha.length(); i++) {
                int j = captcha.length() - i - 1;
                if (captcha.charAt(i) != userCaptcha.charAt(j)) {
                    equal = false;
                    break;
                }
            }
        }
        if (!equal) {
            error = true;
            resultMsg = LANG2.SITES.REGISTRATION.ERR.WRONGCAPTCHA;
            return Redirect.toRegister();
        }

        if (!user.getPassword().equals(confirmationPassword)) {
            error = true;
            resultMsg = LANG2.SITES.REGISTRATION.ERR.PASSWORDSNOTEQUAL;
        } else if (user.getPassword().length() < 4) {
            error = true;
            resultMsg = LANG2.GLOBAL.VALIDATION.ERR.LENGTH;
        }
        if (!user.getAcceptedUserAgreement()) {
            error = true;
            resultMsg = LANG2.GLOBAL.VALIDATION.ERR.ACCEPTUSERAGREEMENT;
        }
        if (Service.userDAO.findBy(user.getUserName()) != null) {
            error = true;
            resultMsg = LANG2.SITES.REGISTRATION.ERR.USERALREADYEXISTING;;
        }
        if (!error) {
            error = checkEmail(user.getEmail());
        }
        if (error) {
            return Redirect.toRegister();
        } else {
            user.setJoinDate(System.currentTimeMillis());
            Language language = Service.languageDAO.findById(getLanguageId());
            String passwordRaw = user.getPassword();
            user.setLocale(language.getLanguage() + "_" + language.getCountry());
            Service.userDAO.addWithSalt(user);
            //Logg user automatically in
            LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().get("LoginBean");
            loginBean.login(user.getUserName(), passwordRaw);
            //Set registry mode to true
            /*
             RoundBean roundBean = (RoundBean) FacesContext.getCurrentInstance()
             .getExternalContext().getSessionMap().get("RoundBean");
             roundBean.setRegistryMode(true);
             */

            resultMsg = "Registered";
            error = false;
        }
        return Redirect.toIndex();
    }

    public boolean checkEmail(String email) {
        if (!email.contains("@") || !email.contains(".")) {
            resultMsg = LANG2.GLOBAL.VALIDATION.ERR.NOVALIDEMAIL;
            return true;
        }
        return false;
    }

    /**
     * @return the resultMsg
     */
    public String getResultMsg() {
        return resultMsg;
    }

    /**
     * @return the resultMsg
     */
    public String getResultMsgToML() {
        if (!resultMsg.equals("")) {
            return ML.getMLStr(resultMsg);
        } else {
            return "";
        }
    }

    /**
     * @param resultMsg the resultMsg to set
     */
    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    /**
     * @return the languageId
     */
    public Integer getLanguageId() {
        return languageId;
    }

    /**
     * @param languageId the languageId to set
     */
    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

}
