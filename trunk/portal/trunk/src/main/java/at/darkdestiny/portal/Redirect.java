/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

import at.darkdestiny.portal.enumeration.ESites;

/**
 *
 * @author Admin
 */
public abstract class Redirect {

    public static String toIndex() {
        return to(ESites.INDEX);
    }

    public static String toRegister() {
        return to(ESites.REGISTER);
    }

    public static String toBugs() {
        return to(ESites.BUGS);
    }

    public static String toTasks() {
        return to(ESites.TASKS);
    }

    public static String toTasksDetail() {
        return to(ESites.TASKS_DETAIL);
    }

    public static String toProfile() {
        return to(ESites.PROFILE);
    }

    public static String toBugsDetail() {
        return to(ESites.BUGS_DETAIL);
    }

    public static String toHighScore() {
        return to(ESites.HIGHSCORE);
    }

    public static String toRounds() {
        return to(ESites.ROUNDS);
    }

    public static String toRoundsDetail() {
        return to(ESites.ROUNDS_DETAIL);
    }

    public static String toRoundsRegister() {
        return to(ESites.ROUNDS_REGISTER);
    }

    public static String toReleaseNotes() {
        return to(ESites.RELEASE_NOTES);
    }

    public static String toNews() {
        return to(ESites.NEWS);
    }


    public static String toNewsDetail() {
        return to(ESites.NEWS_DETAIL);
    }

    public static String toReleaseNotesDetail() {
        return to(ESites.RELEASE_NOTES_DETAIL);
    }

    private static String to(ESites site) {

        return site.getRedirectUrl();
    }
}
