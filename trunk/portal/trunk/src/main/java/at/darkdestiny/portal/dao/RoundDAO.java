package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.Round;
import java.util.ArrayList;

/*
 *
 * Data Access Object for Order
 *
 *
 */
public class RoundDAO extends ReadWriteTable<Round> implements GenericDAO {

    public Round findById(Integer id) {
        Round r = new Round();
        r.setId(id);
        return get(r);
    }

    public ArrayList<Round> findNotFinished() {
        ArrayList<Round> result = new ArrayList<Round>();
        for (Round r : findAll()) {
            if (r.isFinished()) {
                continue;
            }
            result.add(r);
        }
        return result;
    }

    public ArrayList<Round> findByName(String name) {
        Round r = new Round();
        r.setName(name);
        return find(r);
    }
}
