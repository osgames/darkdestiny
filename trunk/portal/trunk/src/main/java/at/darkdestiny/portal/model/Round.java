/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.model;

import at.darkdestiny.core.webservice.ServerInformation;
import at.darkdestiny.portal.RoundRefreshCounter;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.DefaultValue;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;
import at.darkdestiny.portal.enumeration.EGameDataStatus;
import at.darkdestiny.portal.language.LANG2;
import at.darkdestiny.portal.service.Service;
import com.google.common.collect.Maps;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
@TableNameAnnotation(value = "round")
public class Round extends Model {

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned = true, autoIncrement = true)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("hoster")
    private String hoster;
    @FieldMappingAnnotation("version")
    private String version;
    @FieldMappingAnnotation("url")
    @ColumnProperties(length = 255)
    private String url;
    @FieldMappingAnnotation("webServiceUrl")
    @ColumnProperties(length = 255)
    private String webServiceUrl;
    @FieldMappingAnnotation("webServicePassword")
    @ColumnProperties(length = 255)
    private String webServicePassword;
    @FieldMappingAnnotation("startDate")
    private Long startDate;
    @FieldMappingAnnotation("endDate")
    @DefaultValue("0")
    private Long endDate;
    @FieldMappingAnnotation("downForMaintenance")
    private Boolean downForMaintenance;
    ServerInformation serverInformation = null;
    boolean serverOffline = false;


    private static final Map<EGameDataStatus, String> statusToImageMapping = Maps.newHashMap();
    private static final Map<EGameDataStatus, String> statusToTooltipMapping = Maps.newHashMap();

    static {
        statusToImageMapping.put(EGameDataStatus.RUNNING, "images/icon_online.png");
        statusToImageMapping.put(EGameDataStatus.STOPPED, "images/icon_offline.png");
        statusToImageMapping.put(EGameDataStatus.NOCONNECTION, "images/icon_noconnection.png");
        statusToImageMapping.put(EGameDataStatus.RUNNING_NOUPDATE, "images/icon_online.png");
        statusToImageMapping.put(EGameDataStatus.REGISTRATION, "images/icon_registration.png");

        statusToTooltipMapping.put(EGameDataStatus.RUNNING, LANG2.SITES.ROUND.STATUS.ONLINE);
        statusToTooltipMapping.put(EGameDataStatus.STOPPED, LANG2.SITES.ROUND.STATUS.STOPPED);
        statusToTooltipMapping.put(EGameDataStatus.NOCONNECTION, LANG2.SITES.ROUND.STATUS.NOCONNECTION);
        statusToTooltipMapping.put(EGameDataStatus.RUNNING_NOUPDATE, LANG2.SITES.ROUND.STATUS.RUNNINGNOTICK);
        statusToTooltipMapping.put(EGameDataStatus.REGISTRATION, LANG2.SITES.ROUND.STATUS.REGISTRATION);
    }


    public void incrementStatus() {
        try {
            RoundRefreshCounter.getServerInformation(this).incrementStatus(webServicePassword);
            RoundRefreshCounter.forceRefresh(id);
        } catch (Exception ex) {
            Logger.getLogger(Round.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


    public void decrementStatus() {
        try {
            RoundRefreshCounter.getServerInformation(this).decrementStatus(webServicePassword);
            RoundRefreshCounter.forceRefresh(id);
        } catch (Exception ex) {
            Logger.getLogger(Round.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the hoster
     */
    public String getHoster() {
        return hoster;
    }

    /**
     * @param hoster the hoster to set
     */
    public void setHoster(String hoster) {
        this.hoster = hoster;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the startDate
     */
    public Long getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the webServiceUrl
     */
    public String getWebServiceUrl() {
        return webServiceUrl;
    }

    public String getRegistrationUrl() {
        return url + "registration.xhtml";
    }



    /**
     * @param webServiceUrl the webServiceUrl to set
     */
    public void setWebServiceUrl(String webServiceUrl) {
        this.webServiceUrl = webServiceUrl;
    }

    /**
     * @return the endDate
     */
    public Long getEndDate() {
        return endDate;
    }

    public boolean endDateAvailable() {
        if (endDate > 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the downForMaintenance
     */
    public Boolean getDownForMaintenance() {
        return downForMaintenance;
    }

    public void changeMaintenance() {
        setDownForMaintenance(!getDownForMaintenance());
        Service.roundDAO.update(this);
    }

    /**
     * @param downForMaintenance the downForMaintenance to set
     */
    public void setDownForMaintenance(Boolean downForMaintenance) {
        this.downForMaintenance = downForMaintenance;
    }

    /**
     * @return the webServicePassword
     */
    public String getWebServicePassword() {
        return webServicePassword;
    }

    /**
     * @param webServicePassword the webServicePassword to set
     */
    public void setWebServicePassword(String webServicePassword) {
        this.webServicePassword = webServicePassword;
    }


    public boolean isFinished() {
        return (endDate < System.currentTimeMillis());
    }
}
