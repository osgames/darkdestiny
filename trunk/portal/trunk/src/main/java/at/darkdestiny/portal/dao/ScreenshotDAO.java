package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.Screenshot;

/*
 *
 * Data Access Object for Order
 *
 *
 */


public class ScreenshotDAO extends ReadWriteTable<Screenshot> implements GenericDAO{

    public Screenshot findByPosition(int position) {

        Screenshot s = new Screenshot();
        s.setPosition(position);
        return get(s);

    }


}
