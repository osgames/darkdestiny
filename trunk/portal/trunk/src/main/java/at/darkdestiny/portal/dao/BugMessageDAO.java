/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.BugMessage;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class BugMessageDAO extends ReadWriteTable<BugMessage> implements GenericDAO {
    public BugMessage findById(Integer id) {
        BugMessage bm = new BugMessage();
        bm.setId(id);
        return (BugMessage)get(bm);
    }

     public ArrayList<BugMessage> findByBugId(Integer bugId) {
        BugMessage bm = new BugMessage();
        bm.setBugId(bugId);
        return find(bm);
    }

}
