/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.dto;

import at.darkdestiny.core.webservice.PlayerStatisticList;
import at.darkdestiny.core.webservice.ServerInformation;
import at.darkdestiny.portal.BeanUtilities;
import at.darkdestiny.portal.bean.LoginBean;
import at.darkdestiny.portal.bean.MLBean;
import at.darkdestiny.portal.enumeration.EGameDataStatus;
import at.darkdestiny.portal.language.LANG2;
import at.darkdestiny.portal.model.Round;
import at.darkdestiny.portal.model.RoundUserRegistration;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Admin
 */
public class ServerInformationData {

    private final EGameDataStatus status;
    private ServerInformation serverInformation;
    private final HashMap<String, Boolean> userRegistered;
    private final HashMap<String, Boolean> passwordValid;
    private final int userCount;
    private final int userOnlineCount;
    private final Round round;

    private static final Map<EGameDataStatus, String> statusToImageMapping = Maps.newHashMap();
    private static final Map<EGameDataStatus, String> statusToTooltipMapping = Maps.newHashMap();

    private ArrayList<String> errors = new ArrayList<String>();

    //private String srcUrl = "http://localhost:8084/portal/faces/rounds-register.xhtml";
    private static final String srcUrl = "http://thedarkdestiny.at/portal/faces/rounds-register.xhtml";

    static {
        statusToImageMapping.put(EGameDataStatus.RUNNING, "images/icon_online.png");
        statusToImageMapping.put(EGameDataStatus.STOPPED, "images/icon_offline.png");
        statusToImageMapping.put(EGameDataStatus.NOCONNECTION, "images/icon_noconnection.png");
        statusToImageMapping.put(EGameDataStatus.RUNNING_NOUPDATE, "images/icon_online.png");
        statusToImageMapping.put(EGameDataStatus.REGISTRATION, "images/icon_registration.png");

        statusToTooltipMapping.put(EGameDataStatus.RUNNING, LANG2.SITES.ROUND.STATUS.ONLINE);
        statusToTooltipMapping.put(EGameDataStatus.STOPPED, LANG2.SITES.ROUND.STATUS.STOPPED);
        statusToTooltipMapping.put(EGameDataStatus.NOCONNECTION, LANG2.SITES.ROUND.STATUS.NOCONNECTION);
        statusToTooltipMapping.put(EGameDataStatus.RUNNING_NOUPDATE, LANG2.SITES.ROUND.STATUS.RUNNINGNOTICK);
        statusToTooltipMapping.put(EGameDataStatus.REGISTRATION, LANG2.SITES.ROUND.STATUS.REGISTRATION);
    }
    private final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");

    public ServerInformationData(Round round) {
        this.status = EGameDataStatus.NOCONNECTION;
        userRegistered = Maps.newHashMap();
        passwordValid = Maps.newHashMap();
        this.round = round;
        this.userCount = 0;
        this.userOnlineCount = 0;
    }
    public ServerInformationData(ServerInformation serverInformation, Round round) {
        this.status = EGameDataStatus.valueOf(serverInformation.getStatus());
        this.serverInformation = serverInformation;
        userRegistered = Maps.newHashMap();
        passwordValid = Maps.newHashMap();
        this.round = round;
        this.userCount = serverInformation.getUserCount();
        this.userOnlineCount = serverInformation.getUserOnlineCount();
    }

    public EGameDataStatus getStatus() {
        return status;
    }

    public boolean hasStatus(String status) {

        return (EGameDataStatus.valueOf(status).equals(getStatus()));
    }

    public String getStatusTooltip() {

        return statusToTooltipMapping.get(getStatus());

    }

    public PlayerStatisticList getStatistics(long date) {
        Date resultdate = new Date(System.currentTimeMillis());
        System.out.println(sdf.format(resultdate) + " -- GET STATISTICS");
        return serverInformation.getStatistics(date);
    }

    public void incrementStatus(String webservicePassword) {
        Date resultdate = new Date(System.currentTimeMillis());
        System.out.println(sdf.format(resultdate) + " -- INCREMENT STATUS");
        serverInformation.incrementStatus(webservicePassword);
    }

    public void decrementStatus(String webservicePassword) {
        Date resultdate = new Date(System.currentTimeMillis());
        System.out.println(sdf.format(resultdate) + " -- DECREMENT STATUS");
        serverInformation.decrementStatus(webservicePassword);
    }

    public boolean isUserRegistered() {
        String userName = BeanUtilities.getLoginBean().getUser().getUserName();
        if (!userRegistered.containsKey(userName) || !userRegistered.get(userName)) {
            Date resultdate = new Date(System.currentTimeMillis());
            System.out.println(sdf.format(resultdate) + " -- IS USER REGISTERED");
            userRegistered.put(userName, serverInformation.isUserRegistered(userName));
        }
        return userRegistered.get(userName);
    }

    /**
     * @return the userCount
     */
    public int getUserCount() {
        return userCount;
    }

    /**
     * @return the userOnlineCount
     */
    public int getUserOnlineCount() {
        return userOnlineCount;
    }

    public String getLoginForm(String webServicePassword, String locale) {
        Date resultdate = new Date(System.currentTimeMillis());
        System.out.println(sdf.format(resultdate) + " -- GET LOGIN FORM");
        return this.serverInformation.getLoginForm(webServicePassword, locale);
    }

    public String getPortalPassword(String username, String webServicePassword, String registrationToken) {
        Date resultdate = new Date(System.currentTimeMillis());
        System.out.println(sdf.format(resultdate) + " -- GET PORTAL PASSWORD");
        return this.serverInformation.getPortalPassword(username, webServicePassword, registrationToken);
    }

    public boolean isPortalPasswordValid(String userName, String webServicePassword, String roundPassword) {
        if (!passwordValid.containsKey(userName) || !passwordValid.get(userName)) {
            Date resultdate = new Date(System.currentTimeMillis());
            System.out.println(sdf.format(resultdate) + " -- GET PASSWORD VALID");
            passwordValid.put(userName, serverInformation.isPortalPasswordValid(userName, webServicePassword, roundPassword));
        }
        return passwordValid.get(userName);

    }

    public boolean canJoin(String userName) {
        if (!getStatus().equals(EGameDataStatus.RUNNING) && !getStatus().equals(EGameDataStatus.RUNNING_NOUPDATE)) {
            return false;
        }
        if (!isUserRegistered()) {
            return false;
        }
        return isPortalPasswordValid();
    }

    public boolean isPortalPasswordValid() {
        try {

            LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().get("LoginBean");
            if (loginBean == null || loginBean.getUser() == null) {
                return false;
            }
            User user = loginBean.getUser();

            RoundUserRegistration roundUserRegistration = Service.roundUserRegistrationDAO.findBy(round.getId(), user.getId());

            if (roundUserRegistration != null && roundUserRegistration.getRegistrationToken() != null && !roundUserRegistration.getRegistrationToken().equals("")) {
                if (roundUserRegistration.getRoundPassword() == null || roundUserRegistration.getRoundPassword().equals("")) {
                    String roundPassword = getPortalPassword(user.getUserName(), round.getWebServicePassword(), roundUserRegistration.getRegistrationToken());

                    if (Strings.isNullOrEmpty(roundPassword) || roundPassword.equals("null")) {
                        return false;
                    }
                    roundUserRegistration.setRoundPassword(roundPassword);
                    Service.roundUserRegistrationDAO.update(roundUserRegistration);
                }
                return isPortalPasswordValid(user.getUserName(), round.getWebServicePassword(), roundUserRegistration.getRoundPassword());

            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getStatusImage() {

        System.out.println("Status :  " + status);
        return statusToImageMapping.get(status);
    }

    public String getCanConnectToServer() {

        MLBean mlBean = (MLBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("MLBean");

        if (canConnectToServer()) {
            return mlBean.getML(LANG2.SITES.ROUND.LBL.CANCONNECT);
        } else {
            return mlBean.getML(LANG2.SITES.ROUND.LBL.CANTCONNECT);
        }
    }

    public boolean canConnectToServer() {
        if (this.status != EGameDataStatus.NOCONNECTION) {
            return true;
        } else {
            return false;
        }
    }

    public String getLoginForm() {
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        User user = loginBean.getUser();

        errors.clear();
        Map<String, String> parMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (parMap.get("errors") != null) {

            try {
                String[] errs = (parMap.get("errors")).split(";");
                errors.addAll(Lists.newArrayList(errs));
            } catch (Exception ex) {
                Logger.getLogger(Round.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        String loginForm = getLoginForm(round.getWebServicePassword(), user.getLocale().toString());
        String registrationToken = RandomStringUtils.randomAlphabetic(20);

        RoundUserRegistration roundUserRegistration = Service.roundUserRegistrationDAO.findBy(round.getId(), user.getId());
        if (roundUserRegistration == null) {
            roundUserRegistration = new RoundUserRegistration();
            roundUserRegistration.setRoundId(round.getId());
            roundUserRegistration.setUserId(user.getId());
            roundUserRegistration.setRoundPassword("");
            roundUserRegistration.setRegistrationToken(registrationToken);
            Service.roundUserRegistrationDAO.add(roundUserRegistration);
        } else {
            registrationToken = roundUserRegistration.getRegistrationToken();
        }

        user = Service.userDAO.update(user);
        loginBean.setUser(user);

        loginForm = loginForm.replace("%USERNAME_VALUE%", user.getUserName());
        loginForm = loginForm.replace("%REGISTRATIONTOKEN_VALUE%", registrationToken);
        loginForm = loginForm.replace("%NAME_SOURCE_URL_VALUE%", srcUrl);
        return loginForm;
    }

    /**
     * @return the errors
     */
    public ArrayList<String> getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(ArrayList<String> errors) {
        this.errors = errors;
    }

    public Round getRound() {
        return round;
    }

}
