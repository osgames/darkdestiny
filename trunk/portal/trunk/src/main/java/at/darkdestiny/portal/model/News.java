/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.model;

import at.darkdestiny.portal.FormatUtilities;
import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.StringType;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Eobane
 */
@TableNameAnnotation(value = "news")
public class News extends Model<News>{
    @FieldMappingAnnotation("newsId")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=true, autoIncrement=true)
    private Integer newsId;
    @FieldMappingAnnotation("subject")
    @ColumnProperties(length=500)
    @StringType("text")
    private String subject;
    @FieldMappingAnnotation("message")
    @ColumnProperties(length=500)
    @StringType("text")
    private String message;
    @FieldMappingAnnotation("isArchived")
    private Boolean isArchived;
    @FieldMappingAnnotation("date")
    private Long date;
    @FieldMappingAnnotation("languageId")
    private Integer languageId;

    /**
     * @return the newsId
     */
    public Integer getNewsId() {
        return newsId;
    }

    /**
     * @param newsId the newsId to set
     */
    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return FormatUtilities.parseText(FormatUtilities.resizeImg(message), false);
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the isArchived
     */
    public Boolean getIsArchived() {
        return isArchived;
    }

    /**
     * @param isArchived the isArchived to set
     */
    public void setIsArchived(Boolean isArchived) {
        this.isArchived = isArchived;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the languageId
     */
    public Integer getLanguageId() {
        return languageId;
    }

    /**
     * @param languageId the languageId to set
     */
    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }



}
