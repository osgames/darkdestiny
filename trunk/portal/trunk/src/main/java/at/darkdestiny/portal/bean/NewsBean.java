/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.FormatUtilities;
import at.darkdestiny.portal.ML;
import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.model.Comment;
import at.darkdestiny.portal.model.Language;
import at.darkdestiny.portal.model.News;
import at.darkdestiny.portal.model.Round;
import at.darkdestiny.portal.model.Screenshot;
import at.darkdestiny.portal.service.Service;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@ManagedBean(name = "NewsBean")
@SessionScoped
public class NewsBean implements Serializable {

    private News news;
    private Comment comment;
    private boolean editMode = false;
    ArrayList<News> newsToCreate;
    private Integer numberOfEntries = 1;

    public ArrayList<News> getNewsList() {
        TreeMap<Long, News> sorted = new TreeMap<Long, News>();
        Locale locale = MLBean.getLocale();
        Language language = ML.getLanguageByLocale(locale);
        for (News n : Service.newsDAO.findByLanguageId(language.getId())) {
            sorted.put(n.getDate(), n);
        }
        ArrayList<News> result = new ArrayList<News>();
        result.addAll(sorted.descendingMap().values());
        return result;
    }

    public void addComment() {
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        comment.setName(loginBean.getUser().getUserName());
        comment.setDate(System.currentTimeMillis());
        comment.setNewsId(news.getNewsId());
        comment.setMessage(FormatUtilities.parseText(comment.getMessage()));
        Service.commentDAO.add(comment);
        comment.setName("");
        comment.setMessage("");
        comment = new Comment();
    }

    public int getCommentSize(Integer newsId) {
        return Service.commentDAO.findByNewsId(newsId).size();
    }

    public String deleteNews(Integer id) {
        News r = Service.newsDAO.findById(id);
        Service.newsDAO.remove(r);
        return Redirect.toNews();
    }

    public void deleteComment(Integer id) {
        Comment c = Service.commentDAO.findById(id);
        Service.commentDAO.remove(c);
    }

    public String addNews() {
        for (News n : newsToCreate) {
            if (!Strings.isNullOrEmpty(n.getMessage()) && !Strings.isNullOrEmpty(n.getSubject())) {
                n.setDate(System.currentTimeMillis());
                n.setMessage(FormatUtilities.parseText(n.getMessage()));
                n.setIsArchived(false);
                Service.newsDAO.add(n);
            }
        }
        return Redirect.toNews();
    }

    public ArrayList<Language> getMasterLanguages() {
        return Service.languageDAO.findAllMasterLanguages();
    }

    public String updateNews() {
        Service.newsDAO.update(getNews());
        return Redirect.toNews();
    }

    public ArrayList<Comment> getComments() {
        ArrayList<Comment> result = new ArrayList<Comment>();

        TreeMap<Long, Comment> sorted = new TreeMap<Long, Comment>();
        if (news != null && news.getNewsId() != null) {
            for (Comment c : Service.commentDAO.findByNewsId(news.getNewsId())) {
                sorted.put(c.getDate(), c);
            }
        }
        result.addAll(sorted.descendingMap().values());
        return result;
    }

    public ArrayList<Language> getLanguages() {

        return Service.languageDAO.findAllMasterLanguages();
    }

    public ArrayList<News> getNewsToCreate() {
        newsToCreate = new ArrayList<News>();
        if (news != null) {
            newsToCreate.add(news);
        } else {
            for (Language l : getLanguages()) {
                News n = new News();
                n.setLanguageId(l.getId());
                newsToCreate.add(n);
            }
        }
        return newsToCreate;
    }

    public ArrayList<SelectItem> getNumberDropDown() {
        ArrayList<SelectItem> items = Lists.newArrayList();

        items.add(new SelectItem(1));
        items.add(new SelectItem(5));
        items.add(new SelectItem(10));
        items.add(new SelectItem(20));

        return items;
    }

    public ArrayList<Screenshot> getScreenshots() {
        ArrayList<Screenshot> result = new ArrayList<Screenshot>();

        TreeMap<Integer, Screenshot> sorted = new TreeMap<Integer, Screenshot>();
        for (Screenshot s : Service.screenshotDAO.findAll()) {
            sorted.put(s.getPosition(), s);
        }

        result.addAll(sorted.values());
        return result;
    }

    public void createNewComment() {

        comment = new Comment();

    }

    public String createNewNews() {

        news = new News();
        editMode = false;
        return Redirect.toNewsDetail();
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @param news the news to set
     */
    public String setNews(News news) {
        this.news = news;
        editMode = true;
        return Redirect.toNewsDetail();
    }

    /**
     * @param news the news to set
     */
    public void setNewsForComment(Object object) {
        int newsId = Integer.parseInt(String.valueOf(object));
        this.news = Service.newsDAO.findById(newsId);
    }

    /**
     * @return the news
     */
    public News getNews() {
        return news;
    }

    public List<SelectItem> getRoundDropDown() {
        List<SelectItem> names = new ArrayList<SelectItem>();
        for (Round r : Service.roundDAO.findAll()) {
            names.add(new SelectItem(String.valueOf(r.getId()), r.getName()));

        }
        if (names.isEmpty()) {
            names.add(new SelectItem(1, "Keine Runde Verf�gbar"));
        }
        return names;
    }

    /**
     * @return the comment
     */
    public Comment getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(Comment comment) {
        this.comment = comment;
    }

    /**
     * @return the numberOfEntries
     */
    public int getNumberOfEntries() {
        return numberOfEntries;
    }

    /**
     * @param numberOfEntries the numberOfEntries to set
     */
    public void setNumberOfEntries(int numberOfEntries) {
        this.numberOfEntries = numberOfEntries;
    }
}
