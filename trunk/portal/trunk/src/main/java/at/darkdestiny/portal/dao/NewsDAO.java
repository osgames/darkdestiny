/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.portal.model.News;
import at.darkdestiny.framework.dao.GenericDAO;
import java.util.ArrayList;

/**
 *
 * @author Eobane
 */
public class NewsDAO extends ReadWriteTable<News> implements GenericDAO {
    public News findById(Integer newsId) {
        News n = new News();
        n.setNewsId(newsId);
        return (News)get(n);
    }
    public ArrayList<News> findByLanguageId(Integer languageId){
        News n = new News();
        n.setLanguageId(languageId);
        return find(n);
    }

}
