/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.model.Bug;
import at.darkdestiny.portal.model.Task;
import at.darkdestiny.portal.model.Version;
import at.darkdestiny.portal.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "VersionBean")
@SessionScoped
public class VersionBean implements Serializable, Cloneable {

    private Integer releaseId;

    public ArrayList<Version> getVersions(){
        return Service.versionDAO.findAll();
    }

    public Version getVersion(Integer versionId){
        return Service.versionDAO.findById(versionId);
    }

    public Version getActVersion(){
        return Service.versionDAO.findById(releaseId);
    }

    public Integer getBugCount(Integer versionId){
       return Service.bugDAO.findByReleaseId(versionId).size();
    }

    public Integer getTaskCount(Integer versionId){
       return Service.taskDAO.findByReleaseId(versionId).size();
    }

    public List<SelectItem> getVersionsDropDown() {

        List<SelectItem> names = new ArrayList<SelectItem>();
         for (Version v : Service.versionDAO.findAll()) {
           names.add(new SelectItem(String.valueOf(v.getId()),v.getVersion()));
        }
        return names;
    }

    public List<Task> getTasks(){
        return Service.taskDAO.findByReleaseId(releaseId);
    }
    public List<Bug> getBugs(){
        return Service.bugDAO.findByReleaseId(releaseId);
    }
    /**
     * @return the releaseId
     */
    public Integer getReleaseId() {
        return releaseId;
    }

    /**
     * @param releaseId the releaseId to set
     */
    public String setReleaseId(Integer releaseId) {
        this.releaseId = releaseId;
        return Redirect.toReleaseNotesDetail();
    }

}
