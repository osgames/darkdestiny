/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Stefan
 */
@ManagedBean(name = "LinkBean")
@SessionScoped
public class LinkBean implements Serializable{
    public String login(String url, String user, String password){
        String link = url + "enter.jsp?user=" + user + "&portalPassword=" + password;
        
        System.out.println("LINK="+link);
        
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        try {
            externalContext.redirect(link.trim());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return null;
    }    
}
