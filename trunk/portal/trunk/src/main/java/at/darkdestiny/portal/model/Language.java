/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.portal.model;

import at.darkdestiny.framework.annotations.ColumnProperties;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.IndexAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Bullet
 */
@TableNameAnnotation(value = "language")
public class Language extends Model<Language>{

    @FieldMappingAnnotation("id")
    @IdFieldAnnotation
    @ColumnProperties(unsigned=false)
    private Integer id;
    @FieldMappingAnnotation("name")
    private String name;
    @FieldMappingAnnotation("language")
    @IndexAnnotation(indexName="langCountryIdx")
    private String language;
    @FieldMappingAnnotation("country")
    @IndexAnnotation(indexName="langCountryIdx")
    private String country;
    @FieldMappingAnnotation("propertiesfile")
    private String propertiesfile;
    @FieldMappingAnnotation("pic")
    private String pic;
    @FieldMappingAnnotation("masterLanguageId")
    private Integer masterLanguageId;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the propertiesfile
     */
    public String getPropertiesfile() {
        return propertiesfile;
    }

    /**
     * @param propertiesfile the propertiesfile to set
     */
    public void setPropertiesfile(String propertiesfile) {
        this.propertiesfile = propertiesfile;
    }

    /**
     * @return the pic
     */
    public String getPic() {
        return pic;
    }

    /**
     * @param pic the pic to set
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * @return the masterLanguageId
     */
    public Integer getMasterLanguageId() {
        return masterLanguageId;
    }

    /**
     * @param masterLanguageId the masterLanguageId to set
     */
    public void setMasterLanguageId(Integer masterLanguageId) {
        this.masterLanguageId = masterLanguageId;
    }
}
