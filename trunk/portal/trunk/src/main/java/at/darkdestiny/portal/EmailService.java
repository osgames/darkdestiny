/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

import at.darkdestiny.portal.bean.*;
import at.darkdestiny.portal.language.LANG2;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Admin
 */
public class EmailService {

    private static String smtpHost = Service.settingDAO.findAll().get(0).getSmtpHost();
    private static String smtpUser = Service.settingDAO.findAll().get(0).getSmtpUser();
    private static String password = Service.settingDAO.findAll().get(0).getSmtpPassword();
    private static String sender = Service.settingDAO.findAll().get(0).getEmailSender();

    public static String resetPasswordRequestEmail(String userName, Locale locale) {
        String message = "";
        if (userName == null || userName.equals("")) {
            return LANG2.SITES.PASSWORDRECOVERY.ERR.USERNOTFOUND;
        }
        User user = Service.userDAO.findBy(userName.trim());
        if (user != null) {
            try {
                return smtp(user.getEmail(), "Dark Destiny - Passwortzur�cksetzung", generatePasswortResetContent(user), locale);
            } catch (MessagingException ex) {
                Logger.getLogger(EmailServiceBean.class.getName()).log(Level.SEVERE, null, ex);
                message = ex.getMessage();
            }
        } else {
            message = LANG2.SITES.PASSWORDRECOVERY.ERR.USERNOTFOUND;
        }
        return message;
    }

    private static String generatePasswortResetContent(User user) {
        String token = "";
        for (int i = 0; i < 30; i++) {
            token += RandomStringUtils.randomAlphabetic((int) (Math.random() * 2));
            token += RandomStringUtils.randomNumeric((int) (Math.random() * 2));
        }
        user.setPasswordResetToken(token);
        user.setPasswordResetDate(System.currentTimeMillis());

        Service.userDAO.update(user);
        String content = "Um ihr Passwort zur�ckzusetzen klicken sie folgenden link<BR>To reset your password click following link <BR>";

        content += "<BR> <a href='http://thedarkdestiny.at/portal/Servlet?passwordResetToken=" + token + "' target=new >http://thedarkdestiny.at/portal/Servlet?passwordResetToken=" + token + "</a>";
        return content;
    }

    private static String generatePasswortContent(String plainPassword, User user) {

        String content = "Ihr neues Passwort<BR>Your new password<BR>";

        content += "<BR>" + plainPassword;
        return content;
    }

    public static List<String> send(List<String> emails, String subject, String content, Locale locale) {
        List<String> returnMessages = Lists.newArrayList();
        for (String s : emails) {
            send(s, subject, content, locale);

        }
        return returnMessages;
    }

    public static List<String> send(String email, String subject, String content, Locale locale) {
        List<String> returnMessages = Lists.newArrayList();
        try {
            smtp(email, subject, content, locale);
            returnMessages.add("Sent mail to : " + email);
        } catch (MessagingException ex) {
            returnMessages.add("Sening mail to : " + email + " failed : " + ex.getMessage());
            Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, null, ex);
        }


        return returnMessages;
    }

    private static String smtp(String receiver, String subject, String content, Locale locale) throws MessagingException {
        String message = "";
        if (smtpHost == null) {
            throw new MessagingException("smtpHost not found");
        }
        if (smtpUser == null) {
            throw new MessagingException("user not found");
        }
        if (password == null) {
            throw new MessagingException("password not found");
        }
        Properties properties = new Properties();
        properties.put("mail.smtp.host", smtpHost); //set smtp server
        properties.put("mail.smtp.auth", "true"); //use smtp authen properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.port", "25");
        // properties.put("mail.smtp.starttls.enable", "true");
        Session session = Session.getDefaultInstance(properties,
                new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(smtpUser, password);
                    }
                });
        MimeMessage mimeMsg = new MimeMessage(session);
        if (sender != null) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Set sender to " + sender);
            mimeMsg.setFrom(new InternetAddress(sender));
        }
        /*
         if (receiver != null) {
         DebugBuffer.addLine(DebugLevel.TRACE, "Set receiver to " + receiver);
         mimeMsg.setRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
         }
         */
        if (subject != null) {
            DebugBuffer.addLine(DebugBuffer.DebugLevel.TRACE, "Set subject to " + subject);
            mimeMsg.setSubject(subject, "ISO-8859-1");
        }
        MimeBodyPart part = new MimeBodyPart();
        part.setText(content == null ? "" : content, "ISO-8859-1", "html");

        part.setContent(content.toString(), "text/html;charset=ISO-8859-1");
        part.setHeader("Content-Type", "text/html");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(part);
        mimeMsg.setContent(multipart);
        mimeMsg.setSentDate(new Date());
        // Transport.send(mimeMsg);

        InternetAddress[] address = new InternetAddress[1];
        address[0] = new InternetAddress(receiver);

        try {
            address[0].validate();

        } catch (Exception e) {
            message = LANG2.SITES.PASSWORDRECOVERY.ERR.INVALIDADDRESS;
            return message;
        }

        Transport.send(mimeMsg, address);
        message = LANG2.SITES.PASSWORDRECOVERY.MSG.MAILSENT;

        return message;
    }

    public static void sendPassword(String plainPassword, User u, Locale locale) throws MessagingException {
        smtp(u.getEmail(), "Dark Destiny - Neues Passwort", generatePasswortContent(plainPassword, u), locale);
    }
}
