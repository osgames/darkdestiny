package at.darkdestiny.portal.dao;

import at.darkdestiny.framework.ReadWriteTable;
import at.darkdestiny.framework.dao.GenericDAO;
import at.darkdestiny.portal.model.Round;
import at.darkdestiny.portal.model.Version;
import java.util.ArrayList;

/*
 *
 * Data Access Object for Order
 *
 *
 */
public class VersionDAO extends ReadWriteTable<Version> implements GenericDAO {

    public Version findById(Integer id) {
        Version v = new Version();
        v.setId(id);
        return get(v);
    }

    public int findNextVersion(Integer releaseId) {
        int nextVersion = Integer.MAX_VALUE;
        for(Version v : findAll()){
            if(v.getId() > releaseId && v.getId() <  nextVersion){
                nextVersion = v.getId();
            }
        }
        if(nextVersion == Integer.MAX_VALUE){
            nextVersion = releaseId;
        }
        return nextVersion;
    }

    public int findPreviousVersion(Integer releaseId) {
        int nextVersion = Integer.MIN_VALUE;
        for(Version v : findAll()){
            if(v.getId() < releaseId && v.getId() >  nextVersion){
                nextVersion = v.getId();
            }
        }
        if(nextVersion ==  Integer.MIN_VALUE){
            nextVersion = releaseId;
        }
        return nextVersion;
    }

    public Integer findCurrentVersion() {
        for(Version v : findAll()){
            if(v.getCurrent() == true){
                return v.getId();
            }
        }
        return -1;
    }

}
