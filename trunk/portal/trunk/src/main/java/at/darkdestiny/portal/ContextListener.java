/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal;

import at.darkdestiny.framework.DataSourceConfig;
import at.darkdestiny.framework.access.DbConnect;
import at.darkdestiny.framework.dao.DAOFactory;
import at.darkdestiny.util.DebugBuffer;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.ws.Endpoint;
import org.flywaydb.core.Flyway;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefan
 */
public class ContextListener implements ServletContextListener {

    RoundRefreshCounter rrc;
    private Scheduler scheduler;
    private static final Logger log = LoggerFactory.getLogger(ContextListener.class);
    public static final String WEBSERVICE_URL = "http://www.thedarkdestiny.de/portal/webservice";
    private static Endpoint webserviceEndpoint;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.debug("On Startup");
        try {
            Properties dbProperties = new Properties();
            dbProperties.load(GameConfig.class.getResourceAsStream("/db.properties"));
            Flyway flyway = new Flyway();
            flyway.setDataSource(dbProperties.getProperty(SetupProperties.DATABASE_URL), dbProperties.getProperty(SetupProperties.DATABASE_USER), dbProperties.getProperty(SetupProperties.DATABASE_PASS));
            try {
            } catch (Exception e) {
                System.out.println("Init failed: " + e.getMessage());
            }

            DAOFactory.dropAll();

        } catch (IOException ex) {
            ex.printStackTrace();

            java.util.logging.Logger.getLogger(ContextListener.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
        DebugBuffer.debug("Try to initialize webservice! " + WEBSERVICE_URL);

        DbConnect.getInstance(GameConfig.getInstance());

        generateSchedulers();

        DataSourceConfig.getInstance().setModelPackagePath("at.darkdestiny.portal.model.");

        //Init updater
        // HighscoreUpdaterJob.update();
        rrc = new RoundRefreshCounter();
        rrc.start();

    }

    private void generateSchedulers() {
        try {
            SchedulerFactory schFactory = new StdSchedulerFactory();
            scheduler = schFactory.getScheduler();
            scheduler.start();

            JobDetail job = JobBuilder.newJob(HighscoreUpdaterJob.class)
                    .withIdentity("HighscoreUpdaterJob")
                    .build();
            CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                    .withIdentity("HighscoreUpdaterJobTrigger", "crontriggergroup1")
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0 5 1/1 * ? *"))
                    .build();
            scheduler.scheduleJob(job, cronTrigger);
        } catch (SchedulerException ex) {
            java.util.logging.Logger.getLogger(ContextListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("On Shutdown");
        try {
            scheduler.shutdown(true);
        } catch (Exception e) {
            System.err.println("Shutdown of quartz scheduler failed");
        }

        if (rrc != null) {
            while (rrc.isAlive()) {

                rrc.setKeepRunning(false);
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
                System.out.println("rrc still alive");
            }
        }
        
        System.out.println("finished");

    }
}
