/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.ML;
import at.darkdestiny.portal.Redirect;
import at.darkdestiny.portal.model.Language;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.validation.constraints.AssertTrue;

@ManagedBean(name = "ProfileBean")
@SessionScoped
public class ProfileBean implements Serializable, Cloneable {

    private User user;
    //@Length(min = 4, max = 40, message = "Zeichenl�nge 4-40")
    private String confirmationPassword = "";
    private String resultMsg = "";
    private String passwordOld;
    private String passwordNew;
    private String passwordNewConfirmation;
    private Integer languageId;
    private String deletionWarning;
    private Integer userToDelete;

    @AssertTrue(message = "Passw�rter stimmen nicht �berein!")
    public boolean isPasswordsEquals() {
        return passwordNew.equals(passwordNewConfirmation);
    }

    @AssertTrue(message = "Passwortl�nge 4-40 Zeichen")
    public boolean isPasswordLengthOkay() {
        if ((0 < passwordNew.length() && passwordNew.length() <= 4) || (0 < passwordNewConfirmation.length() && passwordNewConfirmation.length() <= 4)) {
            return false;
        }
        return true;
    }

    @AssertTrue(message = "Das alte Passwort ist nicht richtig")
    public boolean isOldPasswordMatching() {
        String tmp = passwordOld;
        if (Service.userDAO.getEncryptedPassword(tmp, user.getPasswordSalt()).equals(user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }

    public void createNewUser() {
        user = new User();
    }

    /**
     * @return the user
     */
    public User getUser() {
        if (user == null) {
            createNewUser();
        }
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the confirmationPassword
     */
    public String getConfirmationPassword() {
        return confirmationPassword;
    }

    /**
     * @param confirmationPassword the confirmationPassword to set
     */
    public void setConfirmationPassword(String confirmationPassword) {
        this.confirmationPassword = confirmationPassword;
    }

    public List<SelectItem> getLanguageDropDown() {
        List<SelectItem> names = new ArrayList<SelectItem>();
        MLBean mlBean = (MLBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("MLBean");
        for (Language l : Service.languageDAO.findAll()) {
            names.add(new SelectItem(String.valueOf(l.getId()), mlBean.getML(l.getName())));
        }
        if (names.isEmpty()) {
            names.add(new SelectItem(1, "Keine Sprache Verf�gbar"));
        }
        return names;
    }

    public String update() {
        boolean error = false;
        error = checkEmail(user.getEmail());
        if (error) {
            return Redirect.toIndex();
        } else {
            Language language = Service.languageDAO.findById(getLanguageId());
            String passwordRaw = user.getPassword();
            user.setLocale(language.getLanguage() + "_" + language.getCountry());
            //Logg user automatically in
            LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().get("LoginBean");
            loginBean.login(user.getUserName(), passwordRaw);
            //Set registry mode to true
            user.setPassword(Service.userDAO.getEncryptedPassword(passwordNew, user.getPasswordSalt()));

            Service.userDAO.update(user);
            resultMsg = "";
            return Redirect.toProfile();
        }
    }

    public String prepareDelete() {

        if (getUserToDelete() == null) {
            LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().get("LoginBean");

            setUserToDelete(loginBean.getUser().getId());
            deletionWarning = ML.getMLStr("profile_msg_deletionwarning");
            return Redirect.toProfile();
        } else {
            User u = Service.userDAO.findById(getUserToDelete());
            Service.userDAO.remove(u);
            setUserToDelete(null);
            LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                    .getExternalContext().getSessionMap().get("LoginBean");
            loginBean.logout();
            return Redirect.toIndex();

        }

    }

    public void setUserFromLogin() {

        //Logg user automatically in
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        //Set registry mode to true
        this.user = loginBean.getUser();
    }

    public boolean checkEmail(String email) {
        if (!email.contains("@") || !email.contains(".")) {
            resultMsg = "Ung�ltige Email";
            return true;
        }
        return false;
    }

    /**
     * @return the resultMsg
     */
    public String getResultMsg() {
        return resultMsg;
    }

    /**
     * @param resultMsg the resultMsg to set
     */
    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    /**
     * @return the languageId
     */
    public Integer getLanguageId() {
        return languageId;
    }

    /**
     * @param languageId the languageId to set
     */
    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    /**
     * @return the passwordOld
     */
    public String getPasswordOld() {
        return passwordOld;
    }

    /**
     * @param passwordOld the passwordOld to set
     */
    public void setPasswordOld(String passwordOld) {
        this.passwordOld = passwordOld;
    }

    /**
     * @return the passwordNew
     */
    public String getPasswordNew() {
        return passwordNew;
    }

    /**
     * @param passwordNew the passwordNew to set
     */
    public void setPasswordNew(String passwordNew) {
        this.passwordNew = passwordNew;
    }

    /**
     * @return the passwordNewConfirmation
     */
    public String getPasswordNewConfirmation() {
        return passwordNewConfirmation;
    }

    /**
     * @param passwordNewConfirmation the passwordNewConfirmation to set
     */
    public void setPasswordNewConfirmation(String passwordNewConfirmation) {
        this.passwordNewConfirmation = passwordNewConfirmation;
    }

    /**
     * @return the deletionWarning
     */
    public String getDeletionWarning() {
        return deletionWarning;
    }

    /**
     * @return the userToDelete
     */
    public Integer getUserToDelete() {
        return userToDelete;
    }

    /**
     * @param userToDelete the userToDelete to set
     */
    public void setUserToDelete(Integer userToDelete) {
        this.userToDelete = userToDelete;
    }
}
