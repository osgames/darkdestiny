
package at.darkdestiny.portal.language;


public final class LANG {

    public LANG.COMMON COMMON = new LANG.COMMON();
    public LANG.DB DB = new LANG.DB();
    public LANG.GLOBAL GLOBAL = new LANG.GLOBAL();
    public LANG.SITES SITES = new LANG.SITES();

    public LANG.COMMON getCommon() {
        return COMMON;
    }

    public LANG.DB getDb() {
        return DB;
    }

    public LANG.GLOBAL getGlobal() {
        return GLOBAL;
    }

    public LANG.SITES getSites() {
        return SITES;
    }

    public class COMMON {

        public LANG.COMMON.EBUGPRIORITY EBUGPRIORITY = new LANG.COMMON.EBUGPRIORITY();
        public LANG.COMMON.EBUGSTATUS EBUGSTATUS = new LANG.COMMON.EBUGSTATUS();
        public LANG.COMMON.ETASKPRIORITY ETASKPRIORITY = new LANG.COMMON.ETASKPRIORITY();
        public LANG.COMMON.ETASKSTATUS ETASKSTATUS = new LANG.COMMON.ETASKSTATUS();
        public LANG.COMMON.USER USER = new LANG.COMMON.USER();
        public LANG.COMMON.VISIBILITY VISIBILITY = new LANG.COMMON.VISIBILITY();

        public LANG.COMMON.EBUGPRIORITY getEbugpriority() {
            return EBUGPRIORITY;
        }

        public LANG.COMMON.EBUGSTATUS getEbugstatus() {
            return EBUGSTATUS;
        }

        public LANG.COMMON.ETASKPRIORITY getEtaskpriority() {
            return ETASKPRIORITY;
        }

        public LANG.COMMON.ETASKSTATUS getEtaskstatus() {
            return ETASKSTATUS;
        }

        public LANG.COMMON.USER getUser() {
            return USER;
        }

        public LANG.COMMON.VISIBILITY getVisibility() {
            return VISIBILITY;
        }

        public class EBUGPRIORITY {

            public String HIGH = "common.ebugpriority.HIGH";
            public String LOW = "common.ebugpriority.LOW";
            public String MEDIUM = "common.ebugpriority.MEDIUM";
            public String NORMAL = "common.ebugpriority.NORMAL";
            public String VERYHIGH = "common.ebugpriority.VERYHIGH";
            public String VERYLOW = "common.ebugpriority.VERYLOW";

            public String getHigh() {
                return HIGH;
            }

            public String getLow() {
                return LOW;
            }

            public String getMedium() {
                return MEDIUM;
            }

            public String getNormal() {
                return NORMAL;
            }

            public String getVeryhigh() {
                return VERYHIGH;
            }

            public String getVerylow() {
                return VERYLOW;
            }

        }

        public class EBUGSTATUS {

            public LANG.COMMON.EBUGSTATUS.FINISHED FINISHED = new LANG.COMMON.EBUGSTATUS.FINISHED();
            public String INPROCESS = "common.ebugstatus.INPROCESS";
            public String NEW = "common.ebugstatus.NEW";
            public String PAUSED = "common.ebugstatus.PAUSED";
            public String SCHEDULED = "common.ebugstatus.SCHEDULED";
            public String VIEWED = "common.ebugstatus.VIEWED";
            public LANG.COMMON.EBUGSTATUS.WAITINGFOR WAITINGFOR = new LANG.COMMON.EBUGSTATUS.WAITINGFOR();
            public String WONTFIX = "common.ebugstatus.WONTFIX";

            public LANG.COMMON.EBUGSTATUS.FINISHED getFinished() {
                return FINISHED;
            }

            public String getInprocess() {
                return INPROCESS;
            }

            public String getNew() {
                return NEW;
            }

            public String getPaused() {
                return PAUSED;
            }

            public String getScheduled() {
                return SCHEDULED;
            }

            public String getViewed() {
                return VIEWED;
            }

            public LANG.COMMON.EBUGSTATUS.WAITINGFOR getWaitingfor() {
                return WAITINGFOR;
            }

            public String getWontfix() {
                return WONTFIX;
            }

            public class FINISHED {

                public String LIVE = "common.ebugstatus.FINISHED.LIVE";
                public String REPOSITORY = "common.ebugstatus.FINISHED.REPOSITORY";

                public String getLive() {
                    return LIVE;
                }

                public String getRepository() {
                    return REPOSITORY;
                }

            }

            public class WAITINGFOR {

                public String USERINPUT = "common.ebugstatus.WAITINGFOR.USERINPUT";

                public String getUserinput() {
                    return USERINPUT;
                }

            }

        }

        public class ETASKPRIORITY {

            public String HIGH = "common.etaskpriority.HIGH";
            public String LOW = "common.etaskpriority.LOW";
            public String MEDIUM = "common.etaskpriority.MEDIUM";
            public String NORMAL = "common.etaskpriority.NORMAL";
            public String VERYHIGH = "common.etaskpriority.VERYHIGH";
            public String VERYLOW = "common.etaskpriority.VERYLOW";

            public String getHigh() {
                return HIGH;
            }

            public String getLow() {
                return LOW;
            }

            public String getMedium() {
                return MEDIUM;
            }

            public String getNormal() {
                return NORMAL;
            }

            public String getVeryhigh() {
                return VERYHIGH;
            }

            public String getVerylow() {
                return VERYLOW;
            }

        }

        public class ETASKSTATUS {

            public LANG.COMMON.ETASKSTATUS.FINISHED FINISHED = new LANG.COMMON.ETASKSTATUS.FINISHED();
            public String INPROCESS = "common.etaskstatus.INPROCESS";
            public String NEW = "common.etaskstatus.NEW";
            public String PAUSED = "common.etaskstatus.PAUSED";
            public String SCHEDULED = "common.etaskstatus.SCHEDULED";
            public String VIEWED = "common.etaskstatus.VIEWED";
            public LANG.COMMON.ETASKSTATUS.WAITINGFOR WAITINGFOR = new LANG.COMMON.ETASKSTATUS.WAITINGFOR();

            public LANG.COMMON.ETASKSTATUS.FINISHED getFinished() {
                return FINISHED;
            }

            public String getInprocess() {
                return INPROCESS;
            }

            public String getNew() {
                return NEW;
            }

            public String getPaused() {
                return PAUSED;
            }

            public String getScheduled() {
                return SCHEDULED;
            }

            public String getViewed() {
                return VIEWED;
            }

            public LANG.COMMON.ETASKSTATUS.WAITINGFOR getWaitingfor() {
                return WAITINGFOR;
            }

            public class FINISHED {

                public String LIVE = "common.etaskstatus.FINISHED.LIVE";
                public String REPOSITORY = "common.etaskstatus.FINISHED.REPOSITORY";

                public String getLive() {
                    return LIVE;
                }

                public String getRepository() {
                    return REPOSITORY;
                }

            }

            public class WAITINGFOR {

                public String USERINPUT = "common.etaskstatus.WAITINGFOR.USERINPUT";

                public String getUserinput() {
                    return USERINPUT;
                }

            }

        }

        public class USER {

            public LANG.COMMON.USER.LBL LBL = new LANG.COMMON.USER.LBL();

            public LANG.COMMON.USER.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String UNKNOWN = "common.user.lbl.unknown";

                public String getUnknown() {
                    return UNKNOWN;
                }

            }

        }

        public class VISIBILITY {

            public String PRIVATE = "common.visibility.PRIVATE";
            public String PUBLIC = "common.visibility.PUBLIC";

            public String getPrivate() {
                return PRIVATE;
            }

            public String getPublic() {
                return PUBLIC;
            }

        }

    }

    public class DB {

        public LANG.DB.LANGUAGE LANGUAGE = new LANG.DB.LANGUAGE();

        public LANG.DB.LANGUAGE getLanguage() {
            return LANGUAGE;
        }

        public class LANGUAGE {

            public String AMERICAN = "db.language.american";
            public String AUSTRIA = "db.language.austria";
            public String ENGLISH = "db.language.english";
            public String GERMAN = "db.language.german";
            public String SUISS = "db.language.suiss";

            public String getAmerican() {
                return AMERICAN;
            }

            public String getAustria() {
                return AUSTRIA;
            }

            public String getEnglish() {
                return ENGLISH;
            }

            public String getGerman() {
                return GERMAN;
            }

            public String getSuiss() {
                return SUISS;
            }

        }

    }

    public class GLOBAL {

        public LANG.GLOBAL.LBL LBL = new LANG.GLOBAL.LBL();
        public LANG.GLOBAL.MSG MSG = new LANG.GLOBAL.MSG();
        public LANG.GLOBAL.VALIDATION VALIDATION = new LANG.GLOBAL.VALIDATION();

        public LANG.GLOBAL.LBL getLbl() {
            return LBL;
        }

        public LANG.GLOBAL.MSG getMsg() {
            return MSG;
        }

        public LANG.GLOBAL.VALIDATION getValidation() {
            return VALIDATION;
        }

        public class LBL {

            public String ACTIONS = "global.lbl.actions";
            public String ADDMESSAGE = "global.lbl.addmessage";
            public String BACK = "global.lbl.back";
            public String CLOSE = "global.lbl.close";
            public String DELETE = "global.lbl.delete";
            public String EDIT = "global.lbl.edit";
            public String MESSAGE = "global.lbl.message";
            public String MESSAGES = "global.lbl.messages";
            public String NAME = "global.lbl.name";
            public String NOTLOGGED = "global.lbl.notlogged";
            public String PLEASELOGIN = "global.lbl.pleaselogin";
            public String SUBJECT = "global.lbl.subject";

            public String getActions() {
                return ACTIONS;
            }

            public String getAddmessage() {
                return ADDMESSAGE;
            }

            public String getBack() {
                return BACK;
            }

            public String getClose() {
                return CLOSE;
            }

            public String getDelete() {
                return DELETE;
            }

            public String getEdit() {
                return EDIT;
            }

            public String getMessage() {
                return MESSAGE;
            }

            public String getMessages() {
                return MESSAGES;
            }

            public String getName() {
                return NAME;
            }

            public String getNotlogged() {
                return NOTLOGGED;
            }

            public String getPleaselogin() {
                return PLEASELOGIN;
            }

            public String getSubject() {
                return SUBJECT;
            }

        }

        public class MSG {

            public String MSGLOGIN = "global.msg.msglogin";

            public String getMsglogin() {
                return MSGLOGIN;
            }

        }

        public class VALIDATION {

            public LANG.GLOBAL.VALIDATION.ERR ERR = new LANG.GLOBAL.VALIDATION.ERR();

            public LANG.GLOBAL.VALIDATION.ERR getErr() {
                return ERR;
            }

            public class ERR {

                public String ACCEPTUSERAGREEMENT = "global.validation.err.acceptuseragreement";
                public String LENGTH = "global.validation.err.length";
                public String NOVALIDEMAIL = "global.validation.err.novalidemail";

                public String getAcceptuseragreement() {
                    return ACCEPTUSERAGREEMENT;
                }

                public String getLength() {
                    return LENGTH;
                }

                public String getNovalidemail() {
                    return NOVALIDEMAIL;
                }

            }

        }

    }

    public class SITES {

        public LANG.SITES.ADMIN ADMIN = new LANG.SITES.ADMIN();
        public LANG.SITES.BUGS BUGS = new LANG.SITES.BUGS();
        public LANG.SITES.DOWNLOAD DOWNLOAD = new LANG.SITES.DOWNLOAD();
        public LANG.SITES.FOOTER FOOTER = new LANG.SITES.FOOTER();
        public LANG.SITES.HEADER HEADER = new LANG.SITES.HEADER();
        public LANG.SITES.HIGHSCORE HIGHSCORE = new LANG.SITES.HIGHSCORE();
        public LANG.SITES.IMPRESSUM IMPRESSUM = new LANG.SITES.IMPRESSUM();
        public LANG.SITES.INFORMATION INFORMATION = new LANG.SITES.INFORMATION();
        public LANG.SITES.INFORMATIONPANEL INFORMATIONPANEL = new LANG.SITES.INFORMATIONPANEL();
        public LANG.SITES.LOGIN LOGIN = new LANG.SITES.LOGIN();
        public LANG.SITES.LOGINMAIN LOGINMAIN = new LANG.SITES.LOGINMAIN();
        public LANG.SITES.MEDIA MEDIA = new LANG.SITES.MEDIA();
        public LANG.SITES.MENU MENU = new LANG.SITES.MENU();
        public LANG.SITES.NEWS NEWS = new LANG.SITES.NEWS();
        public LANG.SITES.PASSWORDRECOVERY PASSWORDRECOVERY = new LANG.SITES.PASSWORDRECOVERY();
        public LANG.SITES.PROFILE PROFILE = new LANG.SITES.PROFILE();
        public LANG.SITES.REGISTER REGISTER = new LANG.SITES.REGISTER();
        public LANG.SITES.REGISTRATION REGISTRATION = new LANG.SITES.REGISTRATION();
        public LANG.SITES.ROUND ROUND = new LANG.SITES.ROUND();
        public LANG.SITES.ROUNDREPLAY ROUNDREPLAY = new LANG.SITES.ROUNDREPLAY();
        public LANG.SITES.ROUNDS ROUNDS = new LANG.SITES.ROUNDS();
        public LANG.SITES.SCREENSHOTS SCREENSHOTS = new LANG.SITES.SCREENSHOTS();
        public LANG.SITES.TASKS TASKS = new LANG.SITES.TASKS();
        public LANG.SITES.TITLE TITLE = new LANG.SITES.TITLE();
        public LANG.SITES.TRACKER TRACKER = new LANG.SITES.TRACKER();
        public LANG.SITES.USERAGREEMENT USERAGREEMENT = new LANG.SITES.USERAGREEMENT();

        public LANG.SITES.ADMIN getAdmin() {
            return ADMIN;
        }

        public LANG.SITES.BUGS getBugs() {
            return BUGS;
        }

        public LANG.SITES.DOWNLOAD getDownload() {
            return DOWNLOAD;
        }

        public LANG.SITES.FOOTER getFooter() {
            return FOOTER;
        }

        public LANG.SITES.HEADER getHeader() {
            return HEADER;
        }

        public LANG.SITES.HIGHSCORE getHighscore() {
            return HIGHSCORE;
        }

        public LANG.SITES.IMPRESSUM getImpressum() {
            return IMPRESSUM;
        }

        public LANG.SITES.INFORMATION getInformation() {
            return INFORMATION;
        }

        public LANG.SITES.INFORMATIONPANEL getInformationpanel() {
            return INFORMATIONPANEL;
        }

        public LANG.SITES.LOGIN getLogin() {
            return LOGIN;
        }

        public LANG.SITES.LOGINMAIN getLoginmain() {
            return LOGINMAIN;
        }

        public LANG.SITES.MEDIA getMedia() {
            return MEDIA;
        }

        public LANG.SITES.MENU getMenu() {
            return MENU;
        }

        public LANG.SITES.NEWS getNews() {
            return NEWS;
        }

        public LANG.SITES.PASSWORDRECOVERY getPasswordrecovery() {
            return PASSWORDRECOVERY;
        }

        public LANG.SITES.PROFILE getProfile() {
            return PROFILE;
        }

        public LANG.SITES.REGISTER getRegister() {
            return REGISTER;
        }

        public LANG.SITES.REGISTRATION getRegistration() {
            return REGISTRATION;
        }

        public LANG.SITES.ROUND getRound() {
            return ROUND;
        }

        public LANG.SITES.ROUNDREPLAY getRoundreplay() {
            return ROUNDREPLAY;
        }

        public LANG.SITES.ROUNDS getRounds() {
            return ROUNDS;
        }

        public LANG.SITES.SCREENSHOTS getScreenshots() {
            return SCREENSHOTS;
        }

        public LANG.SITES.TASKS getTasks() {
            return TASKS;
        }

        public LANG.SITES.TITLE getTitle() {
            return TITLE;
        }

        public LANG.SITES.TRACKER getTracker() {
            return TRACKER;
        }

        public LANG.SITES.USERAGREEMENT getUseragreement() {
            return USERAGREEMENT;
        }

        public class ADMIN {

            public LANG.SITES.ADMIN.LBL LBL = new LANG.SITES.ADMIN.LBL();

            public LANG.SITES.ADMIN.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String ADMIN = "sites.admin.lbl.admin";

                public String getAdmin() {
                    return ADMIN;
                }

            }

        }

        public class BUGS {

            public LANG.SITES.BUGS.LBL LBL = new LANG.SITES.BUGS.LBL();

            public LANG.SITES.BUGS.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String ADDBUG = "sites.bugs.lbl.addbug";
                public String ADDCOMMENT = "sites.bugs.lbl.addcomment";
                public String BUGS = "sites.bugs.lbl.bugs";
                public String CHANGEBUGREPORT = "sites.bugs.lbl.changeBugReport";
                public String CHANGEEDITMODE = "sites.bugs.lbl.changeEditMode";
                public String DETAILS = "sites.bugs.lbl.details";
                public String MESSAGE = "sites.bugs.lbl.message";
                public String PRIORITY = "sites.bugs.lbl.priority";
                public String PUBLIC = "sites.bugs.lbl.public";
                public String REPORTBUG = "sites.bugs.lbl.reportBug";
                public String STATUS = "sites.bugs.lbl.status";
                public String SUBJECT = "sites.bugs.lbl.subject";

                public String getAddbug() {
                    return ADDBUG;
                }

                public String getAddcomment() {
                    return ADDCOMMENT;
                }

                public String getBugs() {
                    return BUGS;
                }

                public String getChangebugreport() {
                    return CHANGEBUGREPORT;
                }

                public String getChangeeditmode() {
                    return CHANGEEDITMODE;
                }

                public String getDetails() {
                    return DETAILS;
                }

                public String getMessage() {
                    return MESSAGE;
                }

                public String getPriority() {
                    return PRIORITY;
                }

                public String getPublic() {
                    return PUBLIC;
                }

                public String getReportbug() {
                    return REPORTBUG;
                }

                public String getStatus() {
                    return STATUS;
                }

                public String getSubject() {
                    return SUBJECT;
                }

            }

        }

        public class DOWNLOAD {

            public LANG.SITES.DOWNLOAD.LBL LBL = new LANG.SITES.DOWNLOAD.LBL();

            public LANG.SITES.DOWNLOAD.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String DOWNLOAD = "sites.download.lbl.download";

                public String getDownload() {
                    return DOWNLOAD;
                }

            }

        }

        public class FOOTER {

            public LANG.SITES.FOOTER.LBL LBL = new LANG.SITES.FOOTER.LBL();

            public LANG.SITES.FOOTER.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String ADMIN = "sites.footer.lbl.admin";
                public String IMPRESSUM = "sites.footer.lbl.impressum";

                public String getAdmin() {
                    return ADMIN;
                }

                public String getImpressum() {
                    return IMPRESSUM;
                }

            }

        }

        public class HEADER {

            public LANG.SITES.HEADER.LBL LBL = new LANG.SITES.HEADER.LBL();
            public LANG.SITES.HEADER.LINK LINK = new LANG.SITES.HEADER.LINK();
            public LANG.SITES.HEADER.MSG MSG = new LANG.SITES.HEADER.MSG();

            public LANG.SITES.HEADER.LBL getLbl() {
                return LBL;
            }

            public LANG.SITES.HEADER.LINK getLink() {
                return LINK;
            }

            public LANG.SITES.HEADER.MSG getMsg() {
                return MSG;
            }

            public class LBL {

                public String LOGIN = "sites.header.lbl.login";
                public String LOGOUT = "sites.header.lbl.logout";
                public String PASSWORD = "sites.header.lbl.password";
                public String REGISTER = "sites.header.lbl.register";

                public String getLogin() {
                    return LOGIN;
                }

                public String getLogout() {
                    return LOGOUT;
                }

                public String getPassword() {
                    return PASSWORD;
                }

                public String getRegister() {
                    return REGISTER;
                }

            }

            public class LINK {

                public String FORGOTPASSWORD = "sites.header.link.forgotpassword";

                public String getForgotpassword() {
                    return FORGOTPASSWORD;
                }

            }

            public class MSG {

                public String LOGGEDWITH = "sites.header.msg.loggedwith";

                public String getLoggedwith() {
                    return LOGGEDWITH;
                }

            }

        }

        public class HIGHSCORE {

            public LANG.SITES.HIGHSCORE.LBL LBL = new LANG.SITES.HIGHSCORE.LBL();

            public LANG.SITES.HIGHSCORE.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String GAMENAME = "sites.highscore.lbl.gameName";
                public String HALLOFFAME = "sites.highscore.lbl.halloffame";
                public String MILITARYPOINTS = "sites.highscore.lbl.militaryPoints";
                public LANG.SITES.HIGHSCORE.LBL.NEXT NEXT = new LANG.SITES.HIGHSCORE.LBL.NEXT();
                public String POINTSTOTAL = "sites.highscore.lbl.pointsTotal";
                public String POPULATIONPOINTS = "sites.highscore.lbl.populationPoints";
                public LANG.SITES.HIGHSCORE.LBL.PREVIOUS PREVIOUS = new LANG.SITES.HIGHSCORE.LBL.PREVIOUS();
                public String RANK = "sites.highscore.lbl.rank";
                public String RESEARCHPOINTS = "sites.highscore.lbl.researchPoints";
                public String RESOURCEPOINTS = "sites.highscore.lbl.resourcePoints";

                public String getGamename() {
                    return GAMENAME;
                }

                public String getHalloffame() {
                    return HALLOFFAME;
                }

                public String getMilitarypoints() {
                    return MILITARYPOINTS;
                }

                public LANG.SITES.HIGHSCORE.LBL.NEXT getNext() {
                    return NEXT;
                }

                public String getPointstotal() {
                    return POINTSTOTAL;
                }

                public String getPopulationpoints() {
                    return POPULATIONPOINTS;
                }

                public LANG.SITES.HIGHSCORE.LBL.PREVIOUS getPrevious() {
                    return PREVIOUS;
                }

                public String getRank() {
                    return RANK;
                }

                public String getResearchpoints() {
                    return RESEARCHPOINTS;
                }

                public String getResourcepoints() {
                    return RESOURCEPOINTS;
                }

                public class NEXT {

                    public String DAY = "sites.highscore.lbl.next.day";
                    public String MONTH = "sites.highscore.lbl.next.month";
                    public String WEEK = "sites.highscore.lbl.next.week";

                    public String getDay() {
                        return DAY;
                    }

                    public String getMonth() {
                        return MONTH;
                    }

                    public String getWeek() {
                        return WEEK;
                    }

                }

                public class PREVIOUS {

                    public String DAY = "sites.highscore.lbl.previous.day";
                    public String MONTH = "sites.highscore.lbl.previous.month";
                    public String WEEK = "sites.highscore.lbl.previous.week";

                    public String getDay() {
                        return DAY;
                    }

                    public String getMonth() {
                        return MONTH;
                    }

                    public String getWeek() {
                        return WEEK;
                    }

                }

            }

        }

        public class IMPRESSUM {

            public LANG.SITES.IMPRESSUM.LBL LBL = new LANG.SITES.IMPRESSUM.LBL();

            public LANG.SITES.IMPRESSUM.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String IMPRESSUM = "sites.impressum.lbl.impressum";

                public String getImpressum() {
                    return IMPRESSUM;
                }

            }

        }

        public class INFORMATION {

            public LANG.SITES.INFORMATION.LBL LBL = new LANG.SITES.INFORMATION.LBL();

            public LANG.SITES.INFORMATION.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String EMBINIUM = "sites.information.lbl.embinium";
                public String HOWALGONIUM = "sites.information.lbl.howalgonium";
                public String INFORMATION = "sites.information.lbl.information";
                public String IRON = "sites.information.lbl.iron";
                public String STEEL = "sites.information.lbl.steel";
                public String TERKONIT = "sites.information.lbl.terkonit";
                public String YNKELONIUM = "sites.information.lbl.ynkelonium";

                public String getEmbinium() {
                    return EMBINIUM;
                }

                public String getHowalgonium() {
                    return HOWALGONIUM;
                }

                public String getInformation() {
                    return INFORMATION;
                }

                public String getIron() {
                    return IRON;
                }

                public String getSteel() {
                    return STEEL;
                }

                public String getTerkonit() {
                    return TERKONIT;
                }

                public String getYnkelonium() {
                    return YNKELONIUM;
                }

            }

        }

        public class INFORMATIONPANEL {

            public LANG.SITES.INFORMATIONPANEL.LBL LBL = new LANG.SITES.INFORMATIONPANEL.LBL();

            public LANG.SITES.INFORMATIONPANEL.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String BUG = "sites.informationpanel.lbl.bug";
                public String LASTCHANGES = "sites.informationpanel.lbl.lastChanges";
                public String SERVERS = "sites.informationpanel.lbl.servers";
                public String TASK = "sites.informationpanel.lbl.task";

                public String getBug() {
                    return BUG;
                }

                public String getLastchanges() {
                    return LASTCHANGES;
                }

                public String getServers() {
                    return SERVERS;
                }

                public String getTask() {
                    return TASK;
                }

            }

        }

        public class LOGIN {

            public LANG.SITES.LOGIN.ERR ERR = new LANG.SITES.LOGIN.ERR();
            public LANG.SITES.LOGIN.LBL LBL = new LANG.SITES.LOGIN.LBL();
            public LANG.SITES.LOGIN.MSG MSG = new LANG.SITES.LOGIN.MSG();

            public LANG.SITES.LOGIN.ERR getErr() {
                return ERR;
            }

            public LANG.SITES.LOGIN.LBL getLbl() {
                return LBL;
            }

            public LANG.SITES.LOGIN.MSG getMsg() {
                return MSG;
            }

            public class ERR {

                public String PASSWORDINVALID = "sites.login.err.passwordinvalid";
                public String USERNAMEALREADYEXISTING = "sites.login.err.usernamealreadyexisting";
                public String USERNOTFOUND = "sites.login.err.usernotfound";

                public String getPasswordinvalid() {
                    return PASSWORDINVALID;
                }

                public String getUsernamealreadyexisting() {
                    return USERNAMEALREADYEXISTING;
                }

                public String getUsernotfound() {
                    return USERNOTFOUND;
                }

            }

            public class LBL {

                public String GUEST = "sites.login.lbl.guest";

                public String getGuest() {
                    return GUEST;
                }

            }

            public class MSG {

                public String HELLO = "sites.login.msg.hello";

                public String getHello() {
                    return HELLO;
                }

            }

        }

        public class LOGINMAIN {

            public LANG.SITES.LOGINMAIN.LBL LBL = new LANG.SITES.LOGINMAIN.LBL();
            public LANG.SITES.LOGINMAIN.MSG MSG = new LANG.SITES.LOGINMAIN.MSG();

            public LANG.SITES.LOGINMAIN.LBL getLbl() {
                return LBL;
            }

            public LANG.SITES.LOGINMAIN.MSG getMsg() {
                return MSG;
            }

            public class LBL {

                public String JOIN = "sites.loginmain.lbl.join";
                public String REGISTER = "sites.loginmain.lbl.register";
                public String REGISTERNOW = "sites.loginmain.lbl.registernow";
                public String SERVERSELECT_1 = "sites.loginmain.lbl.serverselect_1";
                public String SERVERSELECT_2 = "sites.loginmain.lbl.serverselect_2";
                public String TESTACCOUNT = "sites.loginmain.lbl.testaccount";

                public String getJoin() {
                    return JOIN;
                }

                public String getRegister() {
                    return REGISTER;
                }

                public String getRegisternow() {
                    return REGISTERNOW;
                }

                public String getServerselect_1() {
                    return SERVERSELECT_1;
                }

                public String getServerselect_2() {
                    return SERVERSELECT_2;
                }

                public String getTestaccount() {
                    return TESTACCOUNT;
                }

            }

            public class MSG {

                public String REGISTERNOW = "sites.loginmain.msg.registernow";
                public String TESTACCOUNT = "sites.loginmain.msg.testaccount";

                public String getRegisternow() {
                    return REGISTERNOW;
                }

                public String getTestaccount() {
                    return TESTACCOUNT;
                }

            }

        }

        public class MEDIA {

            public LANG.SITES.MEDIA.LBL LBL = new LANG.SITES.MEDIA.LBL();

            public LANG.SITES.MEDIA.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String MEDIA = "sites.media.lbl.media";
                public String SCREENSHOTS = "sites.media.lbl.screenshots";
                public LANG.SITES.MEDIA.LBL.STARMAP STARMAP = new LANG.SITES.MEDIA.LBL.STARMAP();

                public String getMedia() {
                    return MEDIA;
                }

                public String getScreenshots() {
                    return SCREENSHOTS;
                }

                public LANG.SITES.MEDIA.LBL.STARMAP getStarmap() {
                    return STARMAP;
                }

                public class STARMAP {

                    public String REPLAYS = "sites.media.lbl.starmap.replays";

                    public String getReplays() {
                        return REPLAYS;
                    }

                }

            }

        }

        public class MENU {

            public LANG.SITES.MENU.LBL LBL = new LANG.SITES.MENU.LBL();

            public LANG.SITES.MENU.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String BUGTRACKER = "sites.menu.lbl.bugtracker";
                public String COMMUNITY = "sites.menu.lbl.community";
                public String DOWNLOAD = "sites.menu.lbl.download";
                public String FORUM = "sites.menu.lbl.forum";
                public String HALLOFFAME = "sites.menu.lbl.halloffame";
                public String HELP = "sites.menu.lbl.help";
                public String HOME = "sites.menu.lbl.home";
                public String INFO = "sites.menu.lbl.info";
                public String MAILING = "sites.menu.lbl.mailing";
                public String MEDIA = "sites.menu.lbl.media";
                public String NEWS = "sites.menu.lbl.news";
                public String PROFILE = "sites.menu.lbl.profile";
                public String PROJECT = "sites.menu.lbl.project";
                public String REGISTER = "sites.menu.lbl.register";
                public String RELEASENOTES = "sites.menu.lbl.releasenotes";
                public String REPLAYS = "sites.menu.lbl.replays";
                public String ROUNDS = "sites.menu.lbl.rounds";
                public String SCREENSHOTS = "sites.menu.lbl.screenshots";
                public String SOURCEFORGE = "sites.menu.lbl.sourceforge";
                public String TASKTRACKER = "sites.menu.lbl.tasktracker";
                public String USERS = "sites.menu.lbl.users";
                public String WIKI = "sites.menu.lbl.wiki";

                public String getBugtracker() {
                    return BUGTRACKER;
                }

                public String getCommunity() {
                    return COMMUNITY;
                }

                public String getDownload() {
                    return DOWNLOAD;
                }

                public String getForum() {
                    return FORUM;
                }

                public String getHalloffame() {
                    return HALLOFFAME;
                }

                public String getHelp() {
                    return HELP;
                }

                public String getHome() {
                    return HOME;
                }

                public String getInfo() {
                    return INFO;
                }

                public String getMailing() {
                    return MAILING;
                }

                public String getMedia() {
                    return MEDIA;
                }

                public String getNews() {
                    return NEWS;
                }

                public String getProfile() {
                    return PROFILE;
                }

                public String getProject() {
                    return PROJECT;
                }

                public String getRegister() {
                    return REGISTER;
                }

                public String getReleasenotes() {
                    return RELEASENOTES;
                }

                public String getReplays() {
                    return REPLAYS;
                }

                public String getRounds() {
                    return ROUNDS;
                }

                public String getScreenshots() {
                    return SCREENSHOTS;
                }

                public String getSourceforge() {
                    return SOURCEFORGE;
                }

                public String getTasktracker() {
                    return TASKTRACKER;
                }

                public String getUsers() {
                    return USERS;
                }

                public String getWiki() {
                    return WIKI;
                }

            }

        }

        public class NEWS {

            public LANG.SITES.NEWS.LBL LBL = new LANG.SITES.NEWS.LBL();

            public LANG.SITES.NEWS.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String ADDCOMMENT = "sites.news.lbl.addcomment";
                public String ADDNEWS = "sites.news.lbl.addnews";
                public String COMMENTS = "sites.news.lbl.comments";
                public String COMMENTSHEADER = "sites.news.lbl.commentsheader";
                public String MESSAGE = "sites.news.lbl.message";
                public String NEWS = "sites.news.lbl.news";
                public String SUBJECT = "sites.news.lbl.subject";
                public String UPDATENEWS = "sites.news.lbl.updatenews";

                public String getAddcomment() {
                    return ADDCOMMENT;
                }

                public String getAddnews() {
                    return ADDNEWS;
                }

                public String getComments() {
                    return COMMENTS;
                }

                public String getCommentsheader() {
                    return COMMENTSHEADER;
                }

                public String getMessage() {
                    return MESSAGE;
                }

                public String getNews() {
                    return NEWS;
                }

                public String getSubject() {
                    return SUBJECT;
                }

                public String getUpdatenews() {
                    return UPDATENEWS;
                }

            }

        }

        public class PASSWORDRECOVERY {

            public LANG.SITES.PASSWORDRECOVERY.ERR ERR = new LANG.SITES.PASSWORDRECOVERY.ERR();
            public LANG.SITES.PASSWORDRECOVERY.LBL LBL = new LANG.SITES.PASSWORDRECOVERY.LBL();
            public LANG.SITES.PASSWORDRECOVERY.MSG MSG = new LANG.SITES.PASSWORDRECOVERY.MSG();

            public LANG.SITES.PASSWORDRECOVERY.ERR getErr() {
                return ERR;
            }

            public LANG.SITES.PASSWORDRECOVERY.LBL getLbl() {
                return LBL;
            }

            public LANG.SITES.PASSWORDRECOVERY.MSG getMsg() {
                return MSG;
            }

            public class ERR {

                public String INVALIDADDRESS = "sites.passwordrecovery.err.invalidaddress";
                public String USERNOTFOUND = "sites.passwordrecovery.err.usernotfound";

                public String getInvalidaddress() {
                    return INVALIDADDRESS;
                }

                public String getUsernotfound() {
                    return USERNOTFOUND;
                }

            }

            public class LBL {

                public String PASSWORDRECOVERY = "sites.passwordrecovery.lbl.passwordrecovery";
                public String RESETPASSWORD = "sites.passwordrecovery.lbl.resetpassword";
                public String USERNAME = "sites.passwordrecovery.lbl.username";

                public String getPasswordrecovery() {
                    return PASSWORDRECOVERY;
                }

                public String getResetpassword() {
                    return RESETPASSWORD;
                }

                public String getUsername() {
                    return USERNAME;
                }

            }

            public class MSG {

                public String MAILSENT = "sites.passwordrecovery.msg.mailsent";

                public String getMailsent() {
                    return MAILSENT;
                }

            }

        }

        public class PROFILE {

            public LANG.SITES.PROFILE.LBL LBL = new LANG.SITES.PROFILE.LBL();
            public LANG.SITES.PROFILE.MSG MSG = new LANG.SITES.PROFILE.MSG();

            public LANG.SITES.PROFILE.LBL getLbl() {
                return LBL;
            }

            public LANG.SITES.PROFILE.MSG getMsg() {
                return MSG;
            }

            public class LBL {

                public String DELETEACCOUNT = "sites.profile.lbl.deleteaccount";
                public String EMAIL = "sites.profile.lbl.email";
                public String LANGUAGE = "sites.profile.lbl.language";
                public String PASSWORDNEW = "sites.profile.lbl.passwordnew";
                public String PASSWORDNEWCONFIRM = "sites.profile.lbl.passwordnewconfirm";
                public String PASSWORDOLD = "sites.profile.lbl.passwordold";
                public String PROFILE = "sites.profile.lbl.profile";
                public String SENDDATA = "sites.profile.lbl.senddata";
                public String USERNAME = "sites.profile.lbl.username";

                public String getDeleteaccount() {
                    return DELETEACCOUNT;
                }

                public String getEmail() {
                    return EMAIL;
                }

                public String getLanguage() {
                    return LANGUAGE;
                }

                public String getPasswordnew() {
                    return PASSWORDNEW;
                }

                public String getPasswordnewconfirm() {
                    return PASSWORDNEWCONFIRM;
                }

                public String getPasswordold() {
                    return PASSWORDOLD;
                }

                public String getProfile() {
                    return PROFILE;
                }

                public String getSenddata() {
                    return SENDDATA;
                }

                public String getUsername() {
                    return USERNAME;
                }

            }

            public class MSG {

                public String DELETIONWARNING = "sites.profile.msg.deletionwarning";

                public String getDeletionwarning() {
                    return DELETIONWARNING;
                }

            }

        }

        public class REGISTER {

            public LANG.SITES.REGISTER.BUT BUT = new LANG.SITES.REGISTER.BUT();
            public LANG.SITES.REGISTER.ERR ERR = new LANG.SITES.REGISTER.ERR();
            public LANG.SITES.REGISTER.LBL LBL = new LANG.SITES.REGISTER.LBL();
            public LANG.SITES.REGISTER.MSG MSG = new LANG.SITES.REGISTER.MSG();
            public LANG.SITES.REGISTER.TITLE TITLE = new LANG.SITES.REGISTER.TITLE();

            public LANG.SITES.REGISTER.BUT getBut() {
                return BUT;
            }

            public LANG.SITES.REGISTER.ERR getErr() {
                return ERR;
            }

            public LANG.SITES.REGISTER.LBL getLbl() {
                return LBL;
            }

            public LANG.SITES.REGISTER.MSG getMsg() {
                return MSG;
            }

            public LANG.SITES.REGISTER.TITLE getTitle() {
                return TITLE;
            }

            public class BUT {

                public String SUBMITDATA = "sites.register.but.submitdata";

                public String getSubmitdata() {
                    return SUBMITDATA;
                }

            }

            public class ERR {

                public String NOLANGUAGE = "sites.register.err.nolanguage";

                public String getNolanguage() {
                    return NOLANGUAGE;
                }

            }

            public class LBL {

                public String EMAIL = "sites.register.lbl.email";
                public String LANGUAGE = "sites.register.lbl.language";
                public String PASSWORD = "sites.register.lbl.password";
                public String PASSWORDREPEAT = "sites.register.lbl.passwordrepeat";
                public String REGISTRATION = "sites.register.lbl.registration";
                public String USERNAME = "sites.register.lbl.username";

                public String getEmail() {
                    return EMAIL;
                }

                public String getLanguage() {
                    return LANGUAGE;
                }

                public String getPassword() {
                    return PASSWORD;
                }

                public String getPasswordrepeat() {
                    return PASSWORDREPEAT;
                }

                public String getRegistration() {
                    return REGISTRATION;
                }

                public String getUsername() {
                    return USERNAME;
                }

            }

            public class MSG {

                public String CAPTCHA = "sites.register.msg.captcha";
                public String MSGNEWROUND = "sites.register.msg.msgnewround";
                public String USERAGREEMENT1 = "sites.register.msg.useragreement1";
                public String USERAGREEMENT2 = "sites.register.msg.useragreement2";
                public String USERAGREEMENT3 = "sites.register.msg.useragreement3";

                public String getCaptcha() {
                    return CAPTCHA;
                }

                public String getMsgnewround() {
                    return MSGNEWROUND;
                }

                public String getUseragreement1() {
                    return USERAGREEMENT1;
                }

                public String getUseragreement2() {
                    return USERAGREEMENT2;
                }

                public String getUseragreement3() {
                    return USERAGREEMENT3;
                }

            }

            public class TITLE {

                public String HIGHSCORE = "sites.register.title.highscore";
                public String REGISTRATION = "sites.register.title.registration";

                public String getHighscore() {
                    return HIGHSCORE;
                }

                public String getRegistration() {
                    return REGISTRATION;
                }

            }

        }

        public class REGISTRATION {

            public LANG.SITES.REGISTRATION.ERR ERR = new LANG.SITES.REGISTRATION.ERR();

            public LANG.SITES.REGISTRATION.ERR getErr() {
                return ERR;
            }

            public class ERR {

                public String PASSWORDSNOTEQUAL = "sites.registration.err.passwordsnotequal";
                public String USERALREADYEXISTING = "sites.registration.err.useralreadyexisting";
                public String WRONGCAPTCHA = "sites.registration.err.wrongcaptcha";

                public String getPasswordsnotequal() {
                    return PASSWORDSNOTEQUAL;
                }

                public String getUseralreadyexisting() {
                    return USERALREADYEXISTING;
                }

                public String getWrongcaptcha() {
                    return WRONGCAPTCHA;
                }

            }

        }

        public class ROUND {

            public LANG.SITES.ROUND.LBL LBL = new LANG.SITES.ROUND.LBL();
            public LANG.SITES.ROUND.MSG MSG = new LANG.SITES.ROUND.MSG();
            public LANG.SITES.ROUND.STATUS STATUS = new LANG.SITES.ROUND.STATUS();

            public LANG.SITES.ROUND.LBL getLbl() {
                return LBL;
            }

            public LANG.SITES.ROUND.MSG getMsg() {
                return MSG;
            }

            public LANG.SITES.ROUND.STATUS getStatus() {
                return STATUS;
            }

            public class LBL {

                public String ACTIVATEENDDATE = "sites.round.lbl.activateenddate";
                public String ADDROUND = "sites.round.lbl.addround";
                public String CANCONNECT = "sites.round.lbl.canconnect";
                public String CANTCONNECT = "sites.round.lbl.cantconnect";
                public String CHOSENROUND = "sites.round.lbl.chosenround";
                public String DOWNFORMAINTENANCE = "sites.round.lbl.downformaintenance";
                public String ENDDATE = "sites.round.lbl.enddate";
                public String HOSTER = "sites.round.lbl.hoster";
                public String ID = "sites.round.lbl.id";
                public String INFO = "sites.round.lbl.info";
                public String NAME = "sites.round.lbl.name";
                public String REGISTER = "sites.round.lbl.register";
                public String REGISTRATIONPOSSIBLE = "sites.round.lbl.registrationpossible";
                public String ROUND = "sites.round.lbl.round";
                public String ROUNDADD = "sites.round.lbl.roundadd";
                public String ROUNDEDIT = "sites.round.lbl.roundedit";
                public String SERVERCONNECTION = "sites.round.lbl.serverconnection";
                public String SERVERSTATUS = "sites.round.lbl.serverstatus";
                public String STARTDATE = "sites.round.lbl.startdate";
                public String STATUS = "sites.round.lbl.status";
                public String URL = "sites.round.lbl.url";
                public String USERSTATUS = "sites.round.lbl.userstatus";
                public String VERSION = "sites.round.lbl.version";
                public String WEBSERVICEPASSWORD = "sites.round.lbl.webservicepassword";
                public String WEBSERVICEURL = "sites.round.lbl.webserviceurl";

                public String getActivateenddate() {
                    return ACTIVATEENDDATE;
                }

                public String getAddround() {
                    return ADDROUND;
                }

                public String getCanconnect() {
                    return CANCONNECT;
                }

                public String getCantconnect() {
                    return CANTCONNECT;
                }

                public String getChosenround() {
                    return CHOSENROUND;
                }

                public String getDownformaintenance() {
                    return DOWNFORMAINTENANCE;
                }

                public String getEnddate() {
                    return ENDDATE;
                }

                public String getHoster() {
                    return HOSTER;
                }

                public String getId() {
                    return ID;
                }

                public String getInfo() {
                    return INFO;
                }

                public String getName() {
                    return NAME;
                }

                public String getRegister() {
                    return REGISTER;
                }

                public String getRegistrationpossible() {
                    return REGISTRATIONPOSSIBLE;
                }

                public String getRound() {
                    return ROUND;
                }

                public String getRoundadd() {
                    return ROUNDADD;
                }

                public String getRoundedit() {
                    return ROUNDEDIT;
                }

                public String getServerconnection() {
                    return SERVERCONNECTION;
                }

                public String getServerstatus() {
                    return SERVERSTATUS;
                }

                public String getStartdate() {
                    return STARTDATE;
                }

                public String getStatus() {
                    return STATUS;
                }

                public String getUrl() {
                    return URL;
                }

                public String getUserstatus() {
                    return USERSTATUS;
                }

                public String getVersion() {
                    return VERSION;
                }

                public String getWebservicepassword() {
                    return WEBSERVICEPASSWORD;
                }

                public String getWebserviceurl() {
                    return WEBSERVICEURL;
                }

            }

            public class MSG {

                public String THIRDPARTYWARNING = "sites.round.msg.thirdpartywarning";

                public String getThirdpartywarning() {
                    return THIRDPARTYWARNING;
                }

            }

            public class STATUS {

                public String JOIN = "sites.round.status.join";
                public String MAINTENANCE = "sites.round.status.maintenance";
                public String NOCONNECTION = "sites.round.status.noconnection";
                public String ONLINE = "sites.round.status.online";
                public String REGISTRATION = "sites.round.status.registration";
                public String RUNNINGNOTICK = "sites.round.status.runningnotick";
                public String STOPPED = "sites.round.status.stopped";

                public String getJoin() {
                    return JOIN;
                }

                public String getMaintenance() {
                    return MAINTENANCE;
                }

                public String getNoconnection() {
                    return NOCONNECTION;
                }

                public String getOnline() {
                    return ONLINE;
                }

                public String getRegistration() {
                    return REGISTRATION;
                }

                public String getRunningnotick() {
                    return RUNNINGNOTICK;
                }

                public String getStopped() {
                    return STOPPED;
                }

            }

        }

        public class ROUNDREPLAY {

            public LANG.SITES.ROUNDREPLAY.LBL LBL = new LANG.SITES.ROUNDREPLAY.LBL();

            public LANG.SITES.ROUNDREPLAY.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String REPLAYS = "sites.roundreplay.lbl.replays";
                public String ROUND_8 = "sites.roundreplay.lbl.round_8";

                public String getReplays() {
                    return REPLAYS;
                }

                public String getRound_8() {
                    return ROUND_8;
                }

            }

        }

        public class ROUNDS {

            public LANG.SITES.ROUNDS.LBL LBL = new LANG.SITES.ROUNDS.LBL();
            public LANG.SITES.ROUNDS.MSG MSG = new LANG.SITES.ROUNDS.MSG();

            public LANG.SITES.ROUNDS.LBL getLbl() {
                return LBL;
            }

            public LANG.SITES.ROUNDS.MSG getMsg() {
                return MSG;
            }

            public class LBL {

                public String ROUNDS = "sites.rounds.lbl.rounds";

                public String getRounds() {
                    return ROUNDS;
                }

            }

            public class MSG {

                public String USERALREADYEXISTING = "sites.rounds.msg.useralreadyexisting";
                public String USERNOTYETEXISTING = "sites.rounds.msg.usernotyetexisting";

                public String getUseralreadyexisting() {
                    return USERALREADYEXISTING;
                }

                public String getUsernotyetexisting() {
                    return USERNOTYETEXISTING;
                }

            }

        }

        public class SCREENSHOTS {

            public LANG.SITES.SCREENSHOTS.LBL LBL = new LANG.SITES.SCREENSHOTS.LBL();

            public LANG.SITES.SCREENSHOTS.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String SCREENSHOTS = "sites.screenshots.lbl.screenshots";

                public String getScreenshots() {
                    return SCREENSHOTS;
                }

            }

        }

        public class TASKS {

            public LANG.SITES.TASKS.LBL LBL = new LANG.SITES.TASKS.LBL();

            public LANG.SITES.TASKS.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String ADDTASK = "sites.tasks.lbl.addtask";
                public String CHANGEEDITMODE = "sites.tasks.lbl.changeEditMode";
                public String DETAILS = "sites.tasks.lbl.details";
                public String TASKS = "sites.tasks.lbl.tasks";

                public String getAddtask() {
                    return ADDTASK;
                }

                public String getChangeeditmode() {
                    return CHANGEEDITMODE;
                }

                public String getDetails() {
                    return DETAILS;
                }

                public String getTasks() {
                    return TASKS;
                }

            }

        }

        public class TITLE {

            public String PREFIX = "sites.title.prefix";

            public String getPrefix() {
                return PREFIX;
            }

        }

        public class TRACKER {

            public LANG.SITES.TRACKER.LBL LBL = new LANG.SITES.TRACKER.LBL();

            public LANG.SITES.TRACKER.LBL getLbl() {
                return LBL;
            }

            public class LBL {

                public String ACTIONS = "sites.tracker.lbl.actions";
                public String DATE = "sites.tracker.lbl.date";
                public String DETAILS = "sites.tracker.lbl.details";
                public String PRIORITY = "sites.tracker.lbl.priority";
                public String PUBLIC = "sites.tracker.lbl.public";
                public String RELEASE = "sites.tracker.lbl.release";
                public String STATUS = "sites.tracker.lbl.status";
                public String SUBJECT = "sites.tracker.lbl.subject";
                public String SUBMITTER = "sites.tracker.lbl.submitter";
                public String TIMECONSUMED = "sites.tracker.lbl.timeconsumed";
                public String WORKER = "sites.tracker.lbl.worker";

                public String getActions() {
                    return ACTIONS;
                }

                public String getDate() {
                    return DATE;
                }

                public String getDetails() {
                    return DETAILS;
                }

                public String getPriority() {
                    return PRIORITY;
                }

                public String getPublic() {
                    return PUBLIC;
                }

                public String getRelease() {
                    return RELEASE;
                }

                public String getStatus() {
                    return STATUS;
                }

                public String getSubject() {
                    return SUBJECT;
                }

                public String getSubmitter() {
                    return SUBMITTER;
                }

                public String getTimeconsumed() {
                    return TIMECONSUMED;
                }

                public String getWorker() {
                    return WORKER;
                }

            }

        }

        public class USERAGREEMENT {

            public LANG.SITES.USERAGREEMENT.LBL LBL = new LANG.SITES.USERAGREEMENT.LBL();
            public String TITLE = "sites.useragreement.title";

            public LANG.SITES.USERAGREEMENT.LBL getLbl() {
                return LBL;
            }

            public String getTitle() {
                return TITLE;
            }

            public class LBL {

                public String DATAAGREEMENT = "sites.useragreement.lbl.dataagreement";
                public String USAGEAGREEMENT = "sites.useragreement.lbl.usageagreement";

                public String getDataagreement() {
                    return DATAAGREEMENT;
                }

                public String getUsageagreement() {
                    return USAGEAGREEMENT;
                }

            }

        }

    }

}
