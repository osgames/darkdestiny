/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.ML;
import at.darkdestiny.portal.enumeration.EBugPriority;
import at.darkdestiny.portal.enumeration.EBugStatus;
import at.darkdestiny.portal.enumeration.ETaskPriority;
import at.darkdestiny.portal.enumeration.ETaskStatus;
import at.darkdestiny.portal.language.LANG2;
import at.darkdestiny.portal.model.Language;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import at.darkdestiny.util.DebugBuffer;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "ConversionBean")
@SessionScoped
public class ConversionBean implements Serializable {

    public String longToDateString(Long date) {

        Date todaysDate = null; 
        
        try {
            todaysDate = new java.util.Date(date);
        } catch (Exception e) {
            DebugBuffer.error("Date " + date + "[long] could not be cast: ", e);
            return "";
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String formattedDate = formatter.format(todaysDate);
        return formattedDate;
    }

    public String minuteToTime(Integer minutes) {
        int hours = 0;

        while (minutes >= 60) {
            minutes -= 60;
            hours++;
        }

        String result = "";
        if (hours == 0) {
            result += "00";
        } else if (hours < 10) {
            result += "0";
            result += hours;
        } else {

            result += hours;
        }
        result += ":";
        if (minutes == 0) {
            result += "00";
        } else if (minutes < 10) {
            result += 0;
            result += minutes;
        } else {
            result += minutes;
        }
        return result;
    }

    public String idToUserName(Integer userId) {

        String userName = "?";
        User u = Service.userDAO.findById(userId);
        if (u != null) {
            userName = u.getUserName();
        }
        return userName;
    }

    public String bugPriorityToName(EBugPriority priority) {
        String result = "Unbekannte Prioritšt";
        if (priority == null) {
            return result;
        }
        String constant = priority.getLanguageConstant();

        if (constant != null) {
            result = ML.getMLStr(constant);
        }
        return result;

    }

    public String bugStatusToName(EBugStatus status) {
        String result = "Unbekannter Status";
        if (status == null) {
            return result;
        }

        String constant = status.getLanguageConstant();

        if (constant != null) {
            result = ML.getMLStr(constant);
        }
        return result;
    }

    public Language languageIdToLanguage(int languageId) {
        return Service.languageDAO.findById(languageId);
    }

    public String taskPriorityToStyle(ETaskPriority priority) {
        String result = "border: 1px solid #FFFFFF;";

        String colorString = "#222222";
        String fontWeight = "font-weight:bolder;";
        String fontColor = "#000000";
        result += fontWeight;
        if (priority == null) {
            return result += "background-color:" + colorString + ";";
        }
        if (priority.equals(ETaskPriority.VERYLOW)) {
            colorString = "#ffffff";
        } else if (priority.equals(ETaskPriority.LOW)) {
            colorString = "#93ff93";
        } else if (priority.equals(ETaskPriority.NORMAL)) {
            colorString = "#fdfd95";
        } else if (priority.equals(ETaskPriority.HIGH)) {
            colorString = "#ffbd91";
        } else if (priority.equals(ETaskPriority.VERYHIGH)) {
            colorString = "#ff8181";
        }
        result += "color:" + fontColor + ";";
        return result += "background-color:" + colorString + ";";

    }

    public String taskStatusToIconName(ETaskStatus status) {
        String iconString = "images/icon_empty.png";

        if (status.equals(ETaskStatus.WAITINGFOR_USERINPUT)) {
            iconString = "images/icon_questionmark.png";
        }
        return iconString;
    }

    public String bugStatusToIconName(EBugStatus status) {
        String iconString = "images/icon_empty.png";

        if (status.equals(EBugStatus.WAITINGFOR_USERINPUT)) {
            iconString = "images/icon_questionmark.png";
        }
        return iconString;
    }

    public String taskStatusToStyle(ETaskStatus status) {
        String result = "border: 1px solid #FFFFFF;";

        String colorString = "#222222";
        String fontWeight = "font-weight:bolder;";
        String fontColor = "#000000";
        result += fontWeight;
        if (status == null) {
            return result += "background-color:" + colorString + ";";
        }
        if (status.equals(ETaskStatus.NEW)) {
            colorString = "#FFFFFF";
        } else if (status.equals(ETaskStatus.VIEWED)) {
            colorString = "#d0d0d0";
        } else if (status.equals(ETaskStatus.SCHEDULED)) {
            colorString = "#aba9ff";
        } else if (status.equals(ETaskStatus.PAUSED) || status.equals(ETaskStatus.WAITINGFOR_USERINPUT)) {
            colorString = "#ffbd91";
        } else if (status.equals(ETaskStatus.INPROCESS)) {
            colorString = "#fdfd95";
        } else if (status.equals(ETaskStatus.FINISHED_REPOSITORY)) {
            colorString = "#AAFFAA";
        } else if (status.equals(ETaskStatus.FINISHED_LIVE)) {
            colorString = "#55FF55";
        }
        result += "color:" + fontColor + ";";
        return result += "background-color:" + colorString + ";";
    }

    public String bugPriorityToStyle(EBugPriority priority) {
        String result = "border: 1px solid #FFFFFF;";

        String colorString = "#222222";
        String fontWeight = "font-weight:bolder;";
        String fontColor = "#000000";
        result += fontWeight;
        if (priority == null) {
            return result += "background-color:" + colorString + ";";
        }
        if (priority.equals(EBugPriority.VERYLOW)) {
            colorString = "#ffffff";
        } else if (priority.equals(EBugPriority.LOW)) {
            colorString = "#93ff93";
        } else if (priority.equals(EBugPriority.NORMAL)) {
            colorString = "#fdfd95";
        } else if (priority.equals(EBugPriority.HIGH)) {
            colorString = "#ffbd91";
        } else if (priority.equals(EBugPriority.VERYHIGH)) {
            colorString = "#ff8181";
        }

        result += "color:" + fontColor + ";";
        return result += "background-color:" + colorString + ";";

    }

    public String bugStatusToStyle(EBugStatus status) {
        String result = "border: 1px solid #FFFFFF;";

        String colorString = "#222222";
        String fontWeight = "font-weight:bolder;";
        String fontColor = "#000000";
        result += fontWeight;
        if (status == null) {
            return result += "background-color:" + colorString + ";";
        }
        if (status.equals(EBugStatus.NEW)) {
            colorString = "#FFFFFF";
        } else if (status.equals(EBugStatus.VIEWED)) {
            colorString = "#d0d0d0";
        } else if (status.equals(EBugStatus.SCHEDULED)) {
            colorString = "#aba9ff";
        } else if (status.equals(EBugStatus.PAUSED) || status.equals(EBugStatus.WAITINGFOR_USERINPUT)) {
            colorString = "#ffbd91";
        } else if (status.equals(EBugStatus.INPROCESS)) {
            colorString = "#fdfd95";
        } else if (status.equals(EBugStatus.WONTFIX)) {
            colorString = "#AAFFAA";
        } else if (status.equals(EBugStatus.FINISHED_REPOSITORY)) {
            colorString = "#AAFFAA";
        } else if (status.equals(EBugStatus.FINISHED_LIVE)) {
            colorString = "#55FF55";
        }
        result += "color:" + fontColor + ";";
        return result += "background-color:" + colorString + ";";
    }

    public String taskPriorityToName(ETaskPriority priority) {
        String result = "Unbekannte Prioritšt";
        if (priority == null) {
            return result;
        }
        String constant = priority.getLanguageConstant();

        if (constant != null) {
            result = ML.getMLStr(constant);
        }
        return result;

    }

    public String taskStatusToName(ETaskStatus status) {
        String result = "Unbekannter Status";
        if (status == null) {
            return result;
        }

        String constant = status.getLanguageConstant();

        if (constant != null) {
            result = ML.getMLStr(constant);
        }
        return result;
    }

    public String roundIdToName(int roundId) {
        return Service.roundDAO.findById(roundId).getName();
    }

    public String tickToDateString(Long tick) {

        Date todaysDate = new java.util.Date(tick);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = formatter.format(todaysDate);
        return formattedDate;
    }
}
