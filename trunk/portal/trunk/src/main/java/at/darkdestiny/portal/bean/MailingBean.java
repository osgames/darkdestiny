/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.EmailService;
import at.darkdestiny.portal.FormatUtilities;
import at.darkdestiny.portal.enumeration.EUserType;
import at.darkdestiny.portal.model.MailingList;
import at.darkdestiny.portal.model.MailingListEntry;
import at.darkdestiny.portal.model.User;
import at.darkdestiny.portal.service.Service;
import at.darkdestiny.util.DebugBuffer;
import at.darkdestiny.util.DebugBuffer.DebugLevel;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "MailingBean")
@SessionScoped
public class MailingBean implements Serializable, Cloneable {

    private Integer selectedMailingListId;
    private Integer selectedMailingListEntryId;
    private String mailingListName;
    private MailingList mailingList;
    private MailingListEntry mailingListEntry;
    private String emails;
    private String message;
    private String subject;
    private List<String> result;

    public List<MailingListEntry> getMailingListEntries() {
        return Service.mailingListEntryDAO.findByMailingListId(getSelectedMailingListId());

    }

    public List<MailingList> getMailingLists() {
        List<MailingList> list = Service.mailingListDAO.findAll();
        if (!list.isEmpty() && selectedMailingListId == null) {
            selectedMailingListId = list.get(0).getId();
        }
        return list;
    }
//    public List<SelectItem> getMailingLists() {
//        List<SelectItem> mailingLists = new ArrayList<SelectItem>();
//        for (MailingList ml : Service.mailingListDAO.findAll()) {
//            if (getSelectedMailingListId() == null) {
//                this.setSelectedMailingListId(ml.getId());
//            }
//            if (ml.getId().equals(getSelectedMailingListId())) {
//                mailingList = ml;
//            }
//            mailingLists.add(new SelectItem(ml.getId(), ml.getName()));
//        }
//        return mailingLists;
//    }

    public void delete() {
        MailingListEntry mle = Service.mailingListEntryDAO.findById(getSelectedMailingListEntryId());
        if (mle != null) {
            Service.mailingListEntryDAO.remove(mle);
        }

    }

    /**
     * @return the mailingListName
     */
    public String getMailingListName() {
        return mailingListName;
    }

    /**
     * @param mailingListName the mailingListName to set
     */
    public void setMailingListName(String mailingListName) {
        this.mailingListName = mailingListName;
    }

    public void createMailingList() {
        if (mailingListName != null && !mailingListName.equals("")) {
            MailingList ml = new MailingList();
            ml.setName(mailingListName);
            Service.mailingListDAO.add(ml);
            this.mailingListName = "";
        }
    }

    public void deleteMailingList() {
        if (selectedMailingListId != null) {
            MailingList ml = Service.mailingListDAO.findById(selectedMailingListId);
            Service.mailingListDAO.remove(ml);

            for (MailingListEntry mle : Service.mailingListEntryDAO.findByMailingListId(selectedMailingListId)) {
                Service.mailingListEntryDAO.remove(mle);
            }
        }
    }

    public boolean isResultNotEmpty() {
        if (result == null) {
            return false;
        } else {
            return !result.isEmpty();
        }
    }

    public void createMailingListEntry() {
        if (emails != null && !emails.equals("") && getSelectedMailingListId() != null) {
            String[] emailList = emails.split(";");

            for (String email : emailList) {
                if (Service.mailingListEntryDAO.exists(email, selectedMailingListId)) {
                    continue;
                }
                MailingListEntry mle = new MailingListEntry();
                mle.setMailingListId(getSelectedMailingListId());
                mle.setEmail(email);
                Service.mailingListEntryDAO.add(mle);
            }
        }
    }

    public void sendMail() {

        subject = FormatUtilities.parseText(subject);
        message = FormatUtilities.parseText(message);
        LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("LoginBean");
        User user = loginBean.getUser();
        if (!user.getUserType().equals(EUserType.ADMIN)) {
            result.add("not allowed");
            return;
        }
        result = (Lists.newArrayList());
        List<MailingListEntry> mles = Service.mailingListEntryDAO.findByMailingListId(getSelectedMailingListId());
        for (MailingListEntry mle : mles) {
            DebugBuffer.addLine(DebugLevel.DEBUG, "Sent mail to id : " + mle.getId());
            getResult().addAll(EmailService.send(mle.getEmail(), getSubject(), getMessage(), null));
        }
        this.message = "";
        this.subject = "";

    }

    /**
     * @return the mailingList
     */
    public MailingList getMailingList() {
        return mailingList;
    }

    /**
     * @param mailingList the mailingList to set
     */
    public void setMailingList(MailingList mailingList) {
        this.mailingList = mailingList;
    }

    /**
     * @return the mailingListEntry
     */
    public MailingListEntry getMailingListEntry() {
        return mailingListEntry;
    }

    /**
     * @param mailingListEntry the mailingListEntry to set
     */
    public void setMailingListEntry(MailingListEntry mailingListEntry) {
        this.mailingListEntry = mailingListEntry;
    }

    /**
     * @return the emails
     */
    public String getEmails() {
        return emails;
    }

    /**
     * @param emails the emails to set
     */
    public void setEmails(String emails) {
        this.emails = emails;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the selectedMailingListId
     */
    public Integer getSelectedMailingListId() {
        return selectedMailingListId;
    }

    /**
     * @param selectedMailingListId the selectedMailingListId to set
     */
    public void setSelectedMailingListId(Integer selectedMailingListId) {
        this.selectedMailingListId = selectedMailingListId;
    }

    /**
     * @return the selectedMailingListEntryId
     */
    public Integer getSelectedMailingListEntryId() {
        return selectedMailingListEntryId;
    }

    /**
     * @param selectedMailingListEntryId the selectedMailingListEntryId to set
     */
    public void setSelectedMailingListEntryId(Integer selectedMailingListEntryId) {
        this.selectedMailingListEntryId = selectedMailingListEntryId;
    }

    /**
     * @return the result
     */
    public List<String> getResult() {
        return result;
    }

    /**
     * @return the result
     */
    public void clearResult() {
        if (result != null) {
            result.clear();
        }
    }

    /**
     * @param result the result to set
     */
    public void setResult(List<String> result) {
        this.result = result;
    }

    public String getEmailsAsString() {
        String result = "";

        boolean first = true;
        for (MailingListEntry mle : Service.mailingListEntryDAO.findByMailingListId(selectedMailingListId)) {


            if (first) {
                result += mle.getEmail();
                first = false;
            } else {
                result += ";" + mle.getEmail();

            }
        }
        return result;
    }

}
