/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.portal.model;

import at.darkdestiny.portal.ML;
import at.darkdestiny.framework.annotations.FieldMappingAnnotation;
import at.darkdestiny.framework.annotations.IdFieldAnnotation;
import at.darkdestiny.framework.annotations.TableNameAnnotation;
import at.darkdestiny.framework.model.Model;

/**
 *
 * @author Admin
 */
@TableNameAnnotation(value = "screenshot")
public class Screenshot extends Model<Screenshot> {

    @IdFieldAnnotation
    @FieldMappingAnnotation("position")
    private Integer position;
    @FieldMappingAnnotation("file")
    private String file;

    /**
     * @return the name
     */
    public String getName() {
        return ML.getMLStr("Screenshots", file + "_name");
    }

    /**
     * @return the file
     */
    public String getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     * @return the position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return ML.getMLStr("Screenshots", file + "_description");
    }
}
