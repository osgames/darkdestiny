package at.darkdestiny.portal.bean;

import at.darkdestiny.portal.ML;
import at.darkdestiny.portal.language.LANG;
import at.darkdestiny.portal.language.LANG2;
import java.io.Serializable;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "MLBean")
@SessionScoped
public class MLBean implements Serializable {

    public String getML(String constant) {
        return getML(constant, "");
    }

    public String getMLTitle(String constant) {
        return getML(LANG2.SITES.TITLE.PREFIX) + getML(constant, "");
    }

    public String getUsageAgreementML(String constant) {
        String propertiesFile = "Usageagreement";
        ServiceBean serviceBean = (ServiceBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("ServiceBean");
        if (serviceBean != null && serviceBean.isLocaleSetManually()) {
            return ML.getMLStr(propertiesFile, constant, serviceBean.getLocale());
        } else {
            Locale locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
            return ML.getMLStr(propertiesFile, constant, locale);
        }
    }

    public String getMLFromPropertiesFile(String propertiesFile, String constant) {
        return ML.getMLStr(propertiesFile, constant, getLocale());
    }

    public static Locale getLocale() {
        ServiceBean serviceBean = (ServiceBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("ServiceBean");
        if (serviceBean != null && serviceBean.isLocaleSetManually()) {
            return serviceBean.getLocale();
        } else {
            Locale locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
            return locale;
        }
    }

    /*
     * As(s) jsf seems to have problems to pass a list of arguments please
     * specify the params seperated by a % 'sign'
     *
     * E.g.: %username%logincount
     *
     */
    public String getML(String constant, String paramsString) {
        return ML.getMLStr(constant, paramsString.split("%"));
    }

    public String getActiveLanguage() {
        return ML.getActiveLanguage();
    }

}
