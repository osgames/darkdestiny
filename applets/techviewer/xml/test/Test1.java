package xml.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import xml.XMLMalFormatedException;
import xml.XMLMemo;

public class Test1 {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws XMLMalFormatedException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, XMLMalFormatedException, IOException {
		
            File f1 = new File(".");
System.out.println(f1.getAbsoluteFile());
            XMLMemo base = XMLMemo.parse(new FileInputStream("bin/source.xml"));
		
		PrintWriter pw = new PrintWriter(new FileOutputStream("bin/out.xml"));
		base.write(pw);
		pw.flush();
		pw.close();
	}

}
