package techtree;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import java.util.Queue;
import java.util.TreeMap;
import techtree.objects.I_Object_OneCoordinate;
import techtree.objects.I_Object_TwoCoordinates;
import techtree.objects.Object_OneCoordinate;
import techtree.objects.Object_TwoCoordinates;
import techtree.objects.ResDesc;
import techtree.objects.TechRelation;

/**
 * Hier wird das Universum auch ObjectData-Objekten gezeichnet
 * und kann entsprechend manipuliert werden
 *
 * @author martin
 */
public class Painter extends Canvas {

    int lastMousePressedX = 0;
    int lastMousePressedY = 0;
    boolean initialized = false;
    /**
     * Damit der Compiler friedlich ist ;)
     */
    private static final long serialVersionUID = 4354204667927554366L;
    //Radius eines Pixels f�r den der Tooltip angezeigt werden soll
    private static final int TOOLTIP_HEIGHT = 10;
    private static final int TOOLTIP_WIDTH = 10;
    private static final float ZOOMSTRENGTH = 1.1f;
    private static final int MAX_PAINTLEVEL = 10;
    /**
     * Minimale Gr��e des Ausschnittes
     */
    private static final float MINXDIFF = 10;
    private static final float MINYDIFF = 10;
    boolean drawRelations = true;
    boolean editMode = false;
    boolean connectingObjects = false;
    boolean initWidth = true;
    /**
     * Maximale Gr��e des Ausschnittes:
     * (Werte reichen von -2000 bis 2000, => MAX 4*so gro�er Ausschnitt)
     */
    private static final float MAXXDIFF = 20000 * 2 * 4;
    private static final float MAXYDIFF = 20000 * 2 * 4;
    float visible_startx, visible_starty;
    float visible_endx, visible_endy;
    List<Object_OneCoordinate> allObjects = new ArrayList<Object_OneCoordinate>();
    List<Object_OneCoordinate> scanner = new ArrayList<Object_OneCoordinate>();
    private HashMap<String, I_Object_OneCoordinate> informationData = new HashMap<String, I_Object_OneCoordinate>();
    Image backBuffer = null;
    boolean dataChanged = true;
    private List<String> techRelations = new ArrayList<String>();
    private ResDesc_ToolTip showToolTipForRes;
    private TreeMap<String, ResDesc> researches = new TreeMap<String, ResDesc>();
    private HashMap<Integer, ArrayList<Technology>> unlockedTech = new HashMap<Integer, ArrayList<Technology>>();
    public HashMap<Integer, String> idToName = new HashMap<Integer, String>();
    public HashMap<String, Integer> nameToId = new HashMap<String, Integer>();
    private SelectedRect selectedRect = null;
    boolean mouseDragged = false;
    boolean mousePressed = true;
    MouseEvent mouseEvent;
        float ratiox;
        float ratioy;
    float x_start = Float.NaN;
    float y_start = Float.NaN;
    int w_o = 100, h_o = 100;
    float globalRatio;
    boolean showCoordinates = true;
    public final AppletMain main;
    private ArrayList<Object_OneCoordinate> tmpSearchResult;
    private String tmpSearchWord = "";
    private int lastSearchId = -1;
    ResDesc sd = null;
    ResDesc conn1 = null;
    ResDesc conn2 = null;
    boolean ctrlPressed = false;

    /**
     *
     */
    public Painter(final AppletMain main) {



        tmpSearchResult = new ArrayList<Object_OneCoordinate>();
        this.main = main;




        selectedRect = new SelectedRect(this);

        this.addMouseMotionListener(new MouseMotionListener() {

            public void mouseDragged(MouseEvent e) {
            } //Karte mit der Maus durch die Gegend schieben entfernt

            public void mouseMoved(MouseEvent e) {

                ResDesc rd = findResearch(e.getX(), e.getY());

                if (showCoordinates) {
                    mouseEvent = e;
                }
                boolean xoff = false;
                boolean yoff = false;
                int toolTipWidth = 200;
                int toolTipHeight = 60;

                toolTipHeight = 100;

                if ((e.getX() + toolTipWidth) > getWidth()) {
                    xoff = true;
                } else {
                    xoff = false;
                }
                if ((e.getY() + toolTipHeight) > getHeight()) {
                    yoff = true;
                } else {
                    yoff = false;
                }
                if (rd != null) {
                    if ((showToolTipForRes == null) || (showToolTipForRes.rd != rd)) {
                        showToolTipForRes = new ResDesc_ToolTip(rd, e.getX(), e.getY(), getWidth(), getHeight(), getMain());
                        // showToolTipFor = new SystemDesc_ToolTip(sd, e.getX(), e.getY());
                        repaint();
                    } else {
                        showToolTipForRes.setPos(e.getX(), e.getY());
                        repaint();

                    }

                } else if (showToolTipForRes != null) {
                    showToolTipForRes = null;
                    repaint();
                }
                if (sd != null) {
                    if (editMode && !connectingObjects && !ctrlPressed) {
                        getInformationData().remove(sd);
                        int[] con = new int[2];
                        convertCoordinatesBack(e.getX(), e.getY(), con);
                        sd.setX((float) con[0]);
                        sd.setY((float) con[1]);
                        int[] pos = new int[2];
                        int[] pos2 = new int[2];
                        convertCoordinates(sd, pos);
                        getInformationData().put(sd.getName(), sd);

                        ArrayList<I_Object_OneCoordinate> toRemove = new ArrayList<I_Object_OneCoordinate>();

                        for (Map.Entry<String, I_Object_OneCoordinate> informationEntity : getInformationData().entrySet()) {
                            if (informationEntity.getValue() instanceof Object_TwoCoordinates) {
                                if (informationEntity.getValue() instanceof TechRelation) {
                                    TechRelation tr = (TechRelation) informationEntity.getValue();

                                    int r = tr.getReqResearchId();
                                    int s = tr.getSourceId();
                                    if (sd.getId() == r || sd.getId() == s) {
                                        toRemove.add(tr);
                                    }
                                }
                            }
                        }
                        for (I_Object_OneCoordinate o : toRemove) {
                            TechRelation tr = (TechRelation) o;
                            int r = tr.getReqResearchId();
                            int s = tr.getSourceId();
                            getInformationData().remove(tr);
                            TechRelation newTr = new TechRelation(r, s, informationData, AppletManagement.main);
                            getInformationData().put(newTr.getIdentifier(), newTr);
                            convertCoordinates(tr, pos, pos2);
                        }

                        forceRefresh();
                    }
                }
            }
        });
        this.addMouseWheelListener(new MouseWheelListener() {

            public void mouseWheelMoved(MouseWheelEvent e) {

                int zoom = e.getWheelRotation() * 1;
                mouseWheelChanged(zoom);

            }
        });

        this.addMouseListener(new MouseListener() {

            public void mouseClicked(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
                if ((lastMousePressedX == e.getX()) && (lastMousePressedY == e.getY()) && !connectingObjects && !ctrlPressed) {
                    //dbg System.out.println("ITS A CLICK!!!!");
                    sd = findResearch(e.getX(), e.getY());

                    if (sd != null && !editMode) {

                        tmpSearchWord = sd.getName();
                        unmark();
                        sd.setMarked(true);
                        AppletManagement.main.getControlPanel().getSearchPanel().getSearchTextField().setText(sd.getName());
                        AppletManagement.main.updateInformationPanel(sd);
                        forceRefresh();
                    } else {
                        AppletManagement.main.changeShowStatus(false);
                    }

                }
            }

            public void mousePressed(MouseEvent e) {

                mousePressed = true;
                lastMousePressedX = e.getX();
                lastMousePressedY = e.getY();
                if (conn1 == null) {
                    conn1 = findResearch(e.getX(), e.getY());
                    if (conn1 != null) {
                        if (e.getButton() == e.BUTTON1 && !ctrlPressed) {
                            connectingObjects = false;

                            conn1.setMarked(false);

                            conn1 = null;
                            conn2 = null;
                            forceRefresh();
                        }
                        if (editMode) {
                            if (e.getButton() == e.BUTTON1 && ctrlPressed) {
                                System.out.println("LMB");
                                System.out.println("WANNA CONNECT: " + conn1.getIdentifier());
                                connectingObjects = true;
                                conn1.setMarked(true);
                                forceRefresh();

                            }
                            if (e.getButton() == e.BUTTON3 && ctrlPressed) {
                                ArrayList<I_Object_OneCoordinate> toRemove = new ArrayList<I_Object_OneCoordinate>();

                                for (Map.Entry<String, I_Object_OneCoordinate> informationEntity : getInformationData().entrySet()) {
                                    if (informationEntity.getValue() instanceof Object_TwoCoordinates) {
                                        if (informationEntity.getValue() instanceof TechRelation) {
                                            TechRelation tr = (TechRelation) informationEntity.getValue();

                                            int r = tr.getReqResearchId();
                                            int s = tr.getSourceId();
                                            if (conn1.getId() == r || conn1.getId() == s) {
                                                toRemove.add(tr);
                                            }
                                        }
                                    }
                                }
                                for (I_Object_OneCoordinate o : toRemove) {
                                    TechRelation tr = (TechRelation) o;
                                    getInformationData().remove(tr.getIdentifier());
                                }


                                System.out.println("RMB");
                                conn1 = null;
                                forceRefresh();
                            }
                        }
                    }
                } else if (e.getButton() == e.BUTTON1 && ctrlPressed) {
                    conn2 = findResearch(e.getX(), e.getY());
                    if (conn2 != null && !conn1.getIdentifier().equals(conn2.getIdentifier())) {
                        //Found object to connect

                        TechRelation tr = new TechRelation(conn1.getId(), conn2.getId(), getInformationData(), main);
                        conn1.setMarked(false);
                        getInformationData().put(tr.getIdentifier(), tr);
                        connectingObjects = false;

                        conn1 = null;
                        conn2 = null;
                        forceRefresh();

                    }
                }

            }
        });
        visible_startx = -20000;
        visible_endx = 20000;
        visible_starty = -20000;
        visible_endy = 20000;
        this.setPreferredSize(new Dimension((int) main.getPreferredSize().getWidth(), (int) (main.getPreferredSize().getHeight() - main.getControlPanel().getPreferredSize().getHeight() - main.but.getPreferredSize().getHeight())));
    }

    public void updatePanel(boolean informationPanel) {

        if (informationPanel) {

            this.setPreferredSize(new Dimension((int) (main.getPreferredSize().getWidth() - main.informationScrollPane.getPreferredSize().getWidth()), (int) (main.getPreferredSize().getHeight() - main.getControlPanel().getPreferredSize().getHeight() - main.but.getPreferredSize().getHeight())));
        } else {

            this.setPreferredSize(new Dimension((int) main.getPreferredSize().getWidth(), (int) (main.getPreferredSize().getHeight() - main.getControlPanel().getPreferredSize().getHeight() - main.but.getPreferredSize().getHeight())));
        }

    }

    public void updatePanel(boolean informationPanel, ArrayList<ResDesc> researchesws) {

        if (informationPanel) {

            this.setPreferredSize(new Dimension((int) (main.getPreferredSize().getWidth() - main.informationScrollPane.getPreferredSize().getWidth()), (int) (main.getPreferredSize().getHeight() - main.getControlPanel().getPreferredSize().getHeight() - main.but.getPreferredSize().getHeight())));
        } else {

            this.setPreferredSize(new Dimension((int) main.getPreferredSize().getWidth(), (int) (main.getPreferredSize().getHeight() - main.getControlPanel().getPreferredSize().getHeight() - main.but.getPreferredSize().getHeight())));
        }

    }

    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {
        if (initWidth) {
            for (Map.Entry<String, ResDesc> entry : researches.entrySet()) {
                ((ResDesc) informationData.get(entry.getKey())).updateWidth(g);
            }
            initWidth = false;
        }

        if ((backBuffer == null) || (getWidth() != backBuffer.getWidth(null)) || (getHeight() != backBuffer.getHeight(null))) {

            //Die Ratio der dinge soll beibehalten bleiben
            if (backBuffer != null) {
                w_o = backBuffer.getWidth(null);
                h_o = backBuffer.getHeight(null);
            }

            int w_n = getWidth();
            int h_n = getHeight();



            //Aspect-Ratio berechnen:
            float a_o = w_o * 1.0f / h_o;
            float a_n = w_n * 1.0f / h_n;

            if (a_n > a_o) {
                //Die Breite ist gr��er geworden, oder die H�he kleiner =>
                //Die neue Breite muss gr��er werdenf
                float delta = (a_n / a_o - 1) * (visible_endx - visible_startx);
                visible_endx += delta / 2;
                visible_startx -= delta / 2;
            } else {
                //Die H�he muss gr��er werden
                float delta = (a_o / a_n - 1) * (visible_endy - visible_starty);
                visible_endy += delta / 2;
                visible_starty -= delta / 2;
            }




            dataChanged = true;
            backBuffer = null;
        }
        if (dataChanged) {


            if (backBuffer == null) {
                backBuffer = createImage(getWidth(), getHeight());
            }
            Graphics gc = backBuffer.getGraphics();

            gc.setColor(Color.black);
            gc.fillRect(0, 0, this.getWidth(), this.getHeight());

            //  Alle Systeme in denen der User ein System besitzt bekommen einen wei�en Kreis
            float[] res = new float[2];
            float[] delta = new float[2]; //Die L�nge von jeweils einem LJ (d.h. 1), jeweils in Richtung X und Y

            convertCoordinates(0, 0, res);
            convertCoordinates(1, 1, delta);
            delta[0] -= res[0];
            delta[1] -= res[1];

            int[] pos = new int[2];
            int[] pos2 = new int[2];
            for (Map.Entry<String, I_Object_OneCoordinate> informationEntity : informationData.entrySet()) {
                if (informationEntity.getValue() instanceof Object_TwoCoordinates) {

                    Object_TwoCoordinates object_TwoCoordinates = (Object_TwoCoordinates) informationEntity.getValue();
                    convertCoordinates(object_TwoCoordinates, pos, pos2);

                    if (informationEntity.getValue() instanceof TechRelation) {
                        if (drawRelations && !informationEntity.getValue().isHide()) {
                            TechRelation techRelation = (TechRelation) object_TwoCoordinates;
                            ResDesc rd1x = (ResDesc) informationData.get(idToName.get(techRelation.getSourceId()));

                            techRelation.drawLine(gc, techRelation, 0, delta);
                        }
                    }
                }
            }

            for (Object_OneCoordinate od : allObjects) {
                convertCoordinates(od, pos);
                if (od.isToShow(1) && !od.isHide()) {

                    od.draw(gc, 1, delta);
                }

            }
            dataChanged = false;

        }

        g.drawImage(backBuffer, 0, 0, getWidth(), getHeight(), null);

        if (selectedRect.isActive()) {
            selectedRect.paint(this, g);
        }


        if (showCoordinates) {
            g.setColor(Color.RED);
            if (mouseEvent != null) {
                //dataChanged = true;
                // repaint();
            }
        }
        g.setColor(Color.BLACK);



        /*if (showTradeToolTipFor != null) {
        showTradeToolTipFor.paint(this, g);
        }*/
        if (showToolTipForRes != null) {
            showToolTipForRes.paint(this, g);
        }


        globalRatio = (float) getWidth() / (float) getHeight();
        
        ratiox = ((float) getHeight() / (visible_endx - visible_startx));
        ratioy = ((float) getWidth() /( visible_endy - visible_starty));
        if (!initialized) {
            for (float i = ratiox; i < 1.5f; i = i + 0.1f) {
                zoomIn( this.getHeight(), this.getWidth(), this.getHeight(), this.getWidth());
            }
           initialized = true;
           switchToCoordinates(32000/2,17000/2);
        }

    }

    protected float convertToFloatX(int x, int y) {
        int width = getWidth();

        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;

        return x * width_fact + visible_startx;
    }

    protected float convertToFloatY(int x, int y) {
        int height = getHeight();
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;

        return y * height_fact + visible_starty;
    }

    public void convertCoordinates(float sx, float sy, float[] res) {

        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;


        float x = (sx - visible_startx) / width_fact;
        float y = (sy - visible_starty) / height_fact;

        res[0] = x;
        res[1] = y;
    }

    public void convertCoordinates(Object_OneCoordinate sd, int[] res) {

        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;


        float x = (sd.getX() - visible_startx) / width_fact;
        float y = (sd.getY() - visible_starty) / height_fact;

        res[0] = Math.round(x);
        res[1] = Math.round(y);
        sd.setActCoord_x(res[0]);
        sd.setActCoord_y(res[1]);
    }

    public void convertCoordinatesBack(float x1, float y1, int[] res) {
        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;


        float x = x1 * width_fact;
        float y = y1 * height_fact;
        x += visible_startx;
        y += visible_starty;

        res[0] = Math.round(x);
        res[1] = Math.round(y);
    }

    protected ResDesc findResearch(int x, int y) {
        float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 4 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 4);

        ResDesc res = null;
        float priority = Float.NEGATIVE_INFINITY;

        for (int i = 0; i < allObjects.size(); i++) {
            Object_OneCoordinate rd = allObjects.get(i);
            float dist = ((x - (rd.getActCoord_x() - 10)) * (x - (rd.getActCoord_x() - 10)) + (y - rd.getActCoord_y()) * (y - rd.getActCoord_y()));
            if ((rd instanceof ResDesc) && (dist < maxdist)) {
                ResDesc sys = (ResDesc) rd;
                if (sys.isHide()) {
                    return null;
                }
                priority = 1;
                res = sys;

            }
        }
        return res;
    }

    public void addObject(Object_OneCoordinate desc) {
        if (!allObjects.contains(desc)) {
            allObjects.add(desc);
        }
        desc.init(this);
    }

    public void zoomRect(float x_start, float y_start, float endx, float endy) {

        if ((endx == x_start) || (endy == y_start)) {
            return;
            // Check if shorter side will exceed zoomfactor if yes rescale selection
        }

        float xDiff = Math.abs(endx - x_start);
        float yDiff = Math.abs(endy - y_start);
        float currXDiff = Math.abs(visible_endx - visible_startx);
        float currYDiff = Math.abs(visible_endy - visible_starty);

        if (xDiff < MINXDIFF) {
            xDiff = MINXDIFF;
            endx = x_start + MINXDIFF;
        }

        if (yDiff < MINYDIFF) {
            yDiff = MINYDIFF;
            endy = y_start + MINYDIFF;
        }


        float ratio = currXDiff / currYDiff;

        float scaleFactor = 1f;


        if (xDiff < yDiff) {
            scaleFactor = 1f / currXDiff * xDiff;
        } else {
            scaleFactor = 1f / currYDiff * yDiff;
        }


        // Select main side for resize
        float equalToX = (currXDiff / Math.abs(x_start - endx)) - 1;
        float equalToY = (currYDiff / Math.abs(y_start - endy)) - 1;

        boolean scaleByX = true;

        if (Math.abs(equalToX) > Math.abs(equalToY)) {
            scaleByX = false;
        }

        if (endx > x_start && endy > y_start) {
            visible_startx = x_start;
            visible_starty = y_start;

            if (scaleByX) {
                visible_endx = endx;
                visible_endy = visible_starty + (visible_endx - visible_startx) / ratio;
            } else {
                visible_endy = endy;
                visible_endx = visible_startx + (visible_endy - visible_starty) * ratio;
            }
        } else if (endx < x_start && endy < y_start) {
            visible_startx = endx;
            visible_starty = endy;

            if (scaleByX) {
                visible_endx = x_start;
                visible_endy = visible_starty + (visible_endx - visible_startx) / ratio;
            } else {
                visible_endy = y_start;
                visible_endx = visible_startx + (visible_endy - visible_starty) * ratio;
            }
        } else if (endx > x_start && endy < y_start) {
            visible_startx = x_start;
            visible_starty = endy;

            if (scaleByX) {
                visible_endx = endx;
                visible_endy = visible_starty + (visible_endx - visible_startx) / ratio;
            } else {
                visible_endy = y_start;
                visible_endx = visible_startx + (visible_endy - visible_starty) * ratio;
            }
        } else if (endx < x_start && endy > y_start) {
            visible_startx = endx;
            visible_starty = y_start;

            if (scaleByX) {
                visible_endx = x_start;
                visible_endy = visible_starty + (visible_endx - visible_startx) / ratio;
            } else {
                visible_endy = endy;
                visible_endx = visible_startx + (visible_endy - visible_starty) * ratio;
            }
        }

        forceRefresh();
    }

    public void moveImage(String direction) {
        float x = 0;
        float y = 0;
        float dx = convertToFloatX(getWidth() / 10, 0) - convertToFloatX(0, 0);
        float dy = convertToFloatY(0, getHeight() / 10) - convertToFloatY(0, 0);
        if (direction.equals("left")) {
            x = -dx;
        } else if (direction.equals("right")) {
            x = dx;
        } else if (direction.equals("down")) {
            y = dy;
        } else if (direction.equals("up")) {
            y = -dy;
        }
        visible_startx = visible_startx + x;
        visible_endx = visible_endx + x;
        visible_starty = visible_starty + y;
        visible_endy = visible_endy + y;

        forceRefresh();
    }

    public void zoomOut() {
        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;


        w *= ZOOMSTRENGTH;
        h *= ZOOMSTRENGTH;


        if (w > h) {
            if (w > (MAXXDIFF)) {
                w = (MAXXDIFF);
            }
            if (h > (MAXYDIFF / globalRatio)) {
                h = (MAXYDIFF / globalRatio);
            }
        } else {
            if (w > (MAXXDIFF / globalRatio)) {
                w = (MAXXDIFF / globalRatio);
            }
            if (h > (MAXYDIFF)) {
                h = (MAXYDIFF);
            }
        }

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        forceRefresh();
    }

    public void zoomIn() {
        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;

        w /= ZOOMSTRENGTH;
        h /= ZOOMSTRENGTH;

        if (w > h) {
            if (w < (MINXDIFF)) {
                w = (MINXDIFF);
            }
            if (h < (MINYDIFF / globalRatio)) {
                h = (MINYDIFF / globalRatio);
            }
        } else {
            if (w < (MINXDIFF / globalRatio)) {
                w = (MINXDIFF / globalRatio);
            }
            if (h < (MINYDIFF)) {
                h = (MINYDIFF);
            }
        }

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        forceRefresh();
    }

    public void mouseWheelChanged(int zoom) {
        if (zoom < 0) {
            zoomIn(mouseEvent, this.getHeight(), this.getWidth());

        }
        if (zoom > 0) {
            zoomOut(mouseEvent, this.getHeight(), this.getWidth());

        }
    }

    public void fitView(int height, int width) {
        float visible_startx = Float.POSITIVE_INFINITY;
        float visible_starty = Float.POSITIVE_INFINITY;
        float visible_endx = Float.NEGATIVE_INFINITY;
        float visible_endy = Float.NEGATIVE_INFINITY;

        for (int i = 0; i < allObjects.size(); i++) {
            Object_OneCoordinate sd = allObjects.get(i);
            visible_startx = Math.min(visible_startx, sd.getX());
            visible_starty = Math.min(visible_starty, sd.getY());
            visible_endx = Math.max(visible_endx, sd.getX());
            visible_endy = Math.max(visible_endy, sd.getY());
        }
        zoomRect(visible_startx, visible_starty, visible_endx, visible_endy);

    }

    public void forceRefresh() {
        dataChanged = true;
        repaint();
    }

    public HashMap<String, I_Object_OneCoordinate> getInformationData() {
        return informationData;
    }

    public void setInformationData(HashMap<String, I_Object_OneCoordinate> informationData) {
        this.informationData = informationData;
    }

    private void convertCoordinates(I_Object_TwoCoordinates object_twoCoordinates, int[] res1, int[] res2) {
        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;


        float startX = (object_twoCoordinates.getX1() - visible_startx) / width_fact;
        float startY = (object_twoCoordinates.getY1() - visible_starty) / height_fact;
        float endX = (object_twoCoordinates.getX2() - visible_startx) / width_fact;
        float endY = (object_twoCoordinates.getY2() - visible_starty) / height_fact;

        res1[0] = Math.round(startX);
        res1[1] = Math.round(startY);
        res2[0] = Math.round(endX);
        res2[1] = Math.round(endY);
        object_twoCoordinates.setActCoord_x(res1[0]);
        object_twoCoordinates.setActCoord_y(res1[1]);
        object_twoCoordinates.setActCoord_x2(res2[0]);
        object_twoCoordinates.setActCoord_y2(res2[1]);
    }

    private void moveImage(float y, float x) {

        /*-y/-x        -y/+x
         *        |
         *    1   |   2
         *        |
         * -----center----
         *        |
         *    3   |   4
         *        |
         * +y/-x      +y/+x
         */



        //     y = Math.round(y / 10) * 10;
        //    x = Math.round(x / 10) * 10;



        visible_startx = visible_startx + x;
        visible_endx = visible_endx + x;
        visible_starty = visible_starty + y;
        visible_endy = visible_endy + y;

        forceRefresh();


    }

    private void zoomIn(int xMouse, int yMouse, int height, int width) {
        float visDifX = visible_endx - visible_startx;
        float visDifY = visible_endy - visible_starty;

        float widthF = (float) xMouse / (float) width;
        float heightF = (float) yMouse / (float) height;


        float coordX1 = (visible_startx + visDifX * widthF);
        float coordY1 = (visible_starty + visDifY * heightF);



        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;


        w /= ZOOMSTRENGTH;
        h /= ZOOMSTRENGTH;

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        visDifX = visible_endx - visible_startx;
        visDifY = visible_endy - visible_starty;

        float coordX2 = (visible_startx + visDifX * widthF);
        float coordY2 = (visible_starty + visDifY * heightF);

        float moveImageX = coordX2 - coordX1;
        float moveImageY = coordY2 - coordY1;

        moveImage(-moveImageY, -moveImageX);

        forceRefresh();

    }

    private void zoomIn(MouseEvent mouseEvent, int height, int width) {

        float visDifX = visible_endx - visible_startx;
        float visDifY = visible_endy - visible_starty;

        float widthF = (float) mouseEvent.getX() / (float) width;
        float heightF = (float) mouseEvent.getY() / (float) height;


        float coordX1 = (visible_startx + visDifX * widthF);
        float coordY1 = (visible_starty + visDifY * heightF);



        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;


        w /= ZOOMSTRENGTH;
        h /= ZOOMSTRENGTH;

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        visDifX = visible_endx - visible_startx;
        visDifY = visible_endy - visible_starty;

        float coordX2 = (visible_startx + visDifX * widthF);
        float coordY2 = (visible_starty + visDifY * heightF);

        float moveImageX = coordX2 - coordX1;
        float moveImageY = coordY2 - coordY1;

        moveImage(-moveImageY, -moveImageX);

        forceRefresh();

    }

    private void zoomOut(MouseEvent mouseEvent, int height, int width) {
        float visDifX = visible_endx - visible_startx;
        float visDifY = visible_endy - visible_starty;

        float widthF = (float) mouseEvent.getX() / (float) width;
        float heightF = (float) mouseEvent.getY() / (float) height;


        float coordX1 = (visible_startx + visDifX * widthF);
        float coordY1 = (visible_starty + visDifY * heightF);



        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;


        w *= ZOOMSTRENGTH;
        h *= ZOOMSTRENGTH;

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        visDifX = visible_endx - visible_startx;
        visDifY = visible_endy - visible_starty;

        float coordX2 = (visible_startx + visDifX * widthF);
        float coordY2 = (visible_starty + visDifY * heightF);

        float moveImageX = coordX2 - coordX1;
        float moveImageY = coordY2 - coordY1;

        moveImage(-moveImageY, -moveImageX);

        forceRefresh();
    }

    public void switchToResearchName(String name, ResDesc toMark) {

        AppletManagement.main.getControlPanel().getSearchPanel().getSearchTextField().setText(name);
        ResDesc tmp = null;
        boolean foundLastId = false;
        boolean sameWord = false;

        if (tmpSearchWord.equals(name)) {
            sameWord = true;
        } else {
            tmpSearchResult = new ArrayList<Object_OneCoordinate>();
        }


        if (!sameWord) {
            unmark();
            for (int i = 0; i < allObjects.size(); i++) {
                Object_OneCoordinate sd = allObjects.get(i);


                if (sd instanceof ResDesc) {
                    tmp = (ResDesc) sd;

                    if (toMark != null) {
                        if (sd.getId() == toMark.getId()) {
                            tmp.setMarked(true);
                        }
                    }
                    if (tmp.getName().equalsIgnoreCase(name)) {
                        tmp.setMarked(true);
                        tmpSearchResult.add(tmp);
                    }
                }
            }
        }
        if (tmpSearchResult.size() >= 1 && !sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            ResDesc tmp2 = (ResDesc) sd;
            switchToCoordinates(Math.round(tmp2.getX()), Math.round(tmp2.getY()));
            lastSearchId = tmp2.getId();
        } else if (tmpSearchResult.size() == 1 && sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            ResDesc tmp2 = (ResDesc) sd;
            switchToCoordinates(Math.round(tmp2.getX()), Math.round(tmp2.getY()));
            lastSearchId = tmp2.getId();
        } else if (tmpSearchResult.size() > 1 && sameWord) {

            for (int i = 0; i < tmpSearchResult.size(); i++) {

                Object_OneCoordinate tmpSystemResults = tmpSearchResult.get(i);


                if (tmpSystemResults instanceof ResDesc) {
                    ResDesc tmp2 = (ResDesc) tmpSystemResults;

                    if (foundLastId == false) {
                        if (tmp2.getId() == this.lastSearchId) {
                            foundLastId = true;

                        } else {
                            switchToCoordinates(Math.round(tmp2.getX()), Math.round(tmp2.getY()));
                            lastSearchId = tmp2.getId();

                            break;
                        }
                    }

                }
            }

        }
        tmpSearchWord = name;
    }

    public void switchToCoordinates(int x, int y) {
        for (float i = ratiox; i < 1.5f; i = i + 0.1f) {
            zoomIn((int) x, (int) y, this.getHeight(), this.getWidth());
        }
        float diffVisibleX = this.visible_endx - this.visible_startx;
        float diffVisibleY = this.visible_endy - this.visible_starty;


        this.visible_startx = x - Math.round(diffVisibleX / 2);

        this.visible_endx = x + Math.round(diffVisibleX / 2);

        this.visible_starty = y - Math.round(diffVisibleY / 2);

        this.visible_endy = y + Math.round(diffVisibleY / 2);

        //dataChanged = true;
        //repaint();

        forceRefresh();
    }

    /**
     * @return the techRelations
     */
    public List<String> getTechRelations() {
        return techRelations;
    }

    public void findTreeFor(String resName, boolean hideOthers) {
        Queue<String> queue = new LinkedList<String>();
        queue.add(resName);
        ArrayList<String> toActivate = new ArrayList<String>();
        while (queue.size() != 0) {
            String res = queue.poll();
            boolean found = false;
            for (String tr : techRelations) {
                TechRelation techrl = (TechRelation) informationData.get(tr);
                Object_OneCoordinate oo = (Object_OneCoordinate) informationData.get(idToName.get(techrl.getSourceId()));

                if (oo != null) {
                    String ident = oo.getIdentifier();
                    if (ident.equals(res)) {
                        if (!toActivate.contains(res)) {

                            toActivate.add(res);
                        }
                        if (!toActivate.contains(techrl.getIdentifier())) {
                            toActivate.add(techrl.getIdentifier());
                        }
                        String ident2 = informationData.get(idToName.get(techrl.getReqResearchId())).getIdentifier();

                        queue.add(ident2);

                    }
                }
            }
            if (!found) {
                if (!toActivate.contains(res)) {
                    toActivate.add(res);
                }
            }
        }


        for (Map.Entry<String, I_Object_OneCoordinate> o : this.informationData.entrySet()) {


            if (toActivate.contains(o.getValue().getIdentifier())) {
                o.getValue().setMarked(true);
            } else if (hideOthers) {
                o.getValue().setHide(true);
            } else if (!hideOthers) {
                o.getValue().setHide(false);
            }

        }
        forceRefresh();
    }

    public void unmark() {
        for (Map.Entry<String, I_Object_OneCoordinate> o : this.informationData.entrySet()) {
            if (o.getValue() instanceof ResDesc) {
                ResDesc res = (ResDesc) o.getValue();

                res.setMarked(false);
                o.getValue().setHide(false);
            } else if (o.getValue() instanceof TechRelation) {
                TechRelation tr = (TechRelation) o.getValue();
                tr.setMarked(false);
                o.getValue().setHide(false);
            }
        }

        forceRefresh();
    }

    /**
     * @return the researches
     */
    public TreeMap<String, ResDesc> getResearches() {
        return researches;
    }

    /**
     * @param researches the researches to set
     */
    public void setResearches(TreeMap<String, ResDesc> researches) {
        this.researches = researches;
    }

    public AppletMain getMain() {
        return main;
    }

    /**
     * @return the unlockedTech
     */
    public HashMap<Integer, ArrayList<Technology>> getUnlockedTech() {
        return unlockedTech;
    }

    /**
     * @param unlockedTech the unlockedTech to set
     */
    public void setUnlockedTech(HashMap<Integer, ArrayList<Technology>> unlockedTech) {
        this.unlockedTech = unlockedTech;
    }
}
