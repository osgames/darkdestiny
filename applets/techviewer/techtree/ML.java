/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

/**
 *
 * @author Eobane
 */
public class ML {

    private static HashMap<String, HashMap<String, String>> lang = new HashMap<String, HashMap<String, String>>();
    private static boolean init = true;

    private static void init() {
        HashMap<String, String> ger = new HashMap<String, String>();
        ger.put("lbl_research", "Forschung");
        ger.put("lbl_abbr_researchpoints", "FP");
        ger.put("lbl_abbr_computerresearchpoints", "CFP");
        ger.put("lbl_neededrp", "Forschungspunkte");
        ger.put("but_close", "Schlie�en");
        ger.put("lbl_researches", "Forschungen");
        ger.put("lbl_alreadyresearched", "Bereits Erforscht");
        ger.put("lbl_researchable", "Erforschbar");
        ger.put("lbl_inresearch", "In Forschung");
        ger.put("lbl_notresearchable", "Nicht Erforschbar");
        ger.put("lbl_icons", "Icons");
        ger.put("lbl_searchresearch", "Forschung suchen");
        ger.put("lbl_searchpreresearches", "Vorforschungen suchen");
        ger.put("lbl_searchpreresearches_hideothers1", "Vor-Forschungen suchen");
        ger.put("lbl_searchpreresearches_hideothers2", "andere ausblenden");
        ger.put("lbl_showresearchlist", "Forschungsliste anzeigen");
        ger.put("lbl_resetsearch", "Suche zur�cksetzen");
        ger.put("lbl_help", "Hilfe");
        ger.put("lbl_searchforresearch", "Suche nach Forschung");
        ger.put("lbl_unlockedtech", "Freigeschaltete Technologien");
        ger.put("lbl_armor", "Panzerung");
        ger.put("lbl_weapon", "Waffe (Modul)");
        ger.put("lbl_construction", "Geb�ude");
        ger.put("lbl_shield", "Schild (Modul)");
        ger.put("lbl_module", "Modul");
        ger.put("lbl_groundtroop", "Bodentruppen");
        ger.put("lbl_chassis", "Chassis");
        ger.put("lbl_technologies", "Technologien");
        lang.put("de_DE", ger);

        HashMap<String, String> eng = new HashMap<String, String>();


        eng.put("lbl_research", "Research");
        eng.put("lbl_abbr_researchpoints", "RP");
        eng.put("lbl_abbr_computerresearchpoints", "CRP");
        eng.put("lbl_neededrp", "Researchpoints");
        eng.put("but_close", "Close");
        eng.put("lbl_researches", "Researches");
        eng.put("lbl_alreadyresearched", "Already researched");
        eng.put("lbl_researchable", "Researchable");
        eng.put("lbl_inresearch", "In research");
        eng.put("lbl_notresearchable", "Not researchable");
        eng.put("lbl_icons", "Icons");
        eng.put("lbl_searchresearch", "Search Research");
        eng.put("lbl_searchpreresearches", "Search Pre-Researches");
        eng.put("lbl_searchpreresearches_hideothers1", "Search Pre-Researches");
        eng.put("lbl_searchpreresearches_hideothers2", "hide others");
        eng.put("lbl_showresearchlist", "Show Researchlist");
        eng.put("lbl_resetsearch", "Reset Search");
        eng.put("lbl_help", "Help");
        eng.put("lbl_searchforresearch", "Search for Research");
        eng.put("lbl_unlockedtech", "Unlocked Technologies");
        eng.put("lbl_armor", "Armor");
        eng.put("lbl_weapon", "Weapon (Module)");
        eng.put("lbl_construction", "Construction");
        eng.put("lbl_shield", "Shield (Module)");
        eng.put("lbl_module", "Module");
        eng.put("lbl_groundtroop", "Groundtroops");
        eng.put("lbl_chassis", "Chassis");
        eng.put("lbl_technologies", "Technologies");
        lang.put("en_US", eng);
    }

    public static String getMLStr(String key, Locale locale) {
        if (init) {
            init();
            init = false;
        }
        String data = key;
        try {

            HashMap<String, String> langVars = lang.get(locale.toString());
            if (langVars == null) {
                if (locale.toString().equalsIgnoreCase("de_AT")) {

                    langVars = lang.get("de_DE");
                } else {
                    langVars = lang.get("en_US");
                }
            }
             data = langVars.get(key);
        } catch (Exception e) {
            System.out.println("Error beim holen eines Multilanguagekeys: " + e + "key : " + key);
            return "e:" + data;
        }
        return data;
    }
}
