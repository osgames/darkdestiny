/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package techtree.objects;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
import techtree.Painter;

/**
 *
 * @author HorstRabe
 */
public class Object_OneCoordinate implements I_Object_OneCoordinate{
    
   private int id;
   private String name;
   private float x;
   private float y;
   private Image imgName;
   private String identifier;

    private boolean marked = false;
   private boolean hide;
       private int actCoord_x;
    private int actCoord_y;
	protected Painter painter;
    
   
     public Object_OneCoordinate(int id, String name, float x, float y, Image imgName){
        this.setId(id);
        this.setName(name);
        this.setX(x);
        this.setY(y);
        this.setImgName(imgName);
        this.identifier = name;
    }

    public void drawVisibleOvals(Graphics gc, int i, float[] delta) {

    }
    
    //ovdbg @Override
    public String getIdentifier() {
        return this.identifier;
    }

    //ovdbg @Override
    public JPanel getPanel() {
        return new JPanel();
    }


    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Image getImgName() {
        return imgName;
    }

    public void setImgName(Image imgName) {
        this.imgName = imgName;
    }

    //ovdbg @Override
    public boolean isToShow(int i) {
        return false;
    }

    //ovdbg @Override
    public void draw(Graphics gc, int level, float[] delta) {   
      
    }


    //ovdbg @Override
    public void setActCoord_x(int actCoord_x) {
      this.actCoord_x = actCoord_x;
    }

    //ovdbg @Override
    public void setActCoord_y(int actCoord_y) {
        this.actCoord_y = actCoord_y;
    }

    //ovdbg @Override
    public int getActCoord_x() {
        return this.actCoord_x;
    }

    //ovdbg @Override
    public int getActCoord_y() {
        return this.actCoord_y;
    }

    //ovdbg @Override
    public void init(Painter painter) {
        this.painter = painter;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the hide
     */
    public boolean isHide() {
        return hide;
    }

    /**
     * @param hide the hide to set
     */
    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public boolean isMarked() {
        return marked;
    }


  

}
