/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package techtree.objects;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
import techtree.Painter;

/**
 *
 * @author HorstRabe
 */
public class Object_TwoCoordinates implements I_Object_TwoCoordinates{

    protected String identifier;
    
   private int x1;
   private int y1;
   private int x2;
   private int y2;

    private boolean marked = false;
    protected int actCoord_x;
    protected int actCoord_y;
    protected int actCoord_x2;
    protected int actCoord_y2;
    
	protected Painter painter;

      private  boolean hide;
   
     public Object_TwoCoordinates(int x1, int y1, int x2, int y2){

        this.setX1(x1);
        this.setY1(y1);
        this.setX2(x2);
        this.setY2(y2);
    }
    public Object_TwoCoordinates(){
        
    }
    //ovdbg @Override
    public JPanel getPanel() {
        return new JPanel();
    }


    


    //ovdbg @Override
    public int getX1() {
        return x1;
    }

    //ovdbg @Override
    public void setX1(int x1) {
        this.x1 = x1;
    }

    //ovdbg @Override
    public int getY1() {
        return y1;
    }

    //ovdbg @Override
    public void setY1(int y1) {
        this.y1 = y1;
    }
    
    //ovdbg @Override
    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

  

    //ovdbg @Override
    public boolean isToShow(int i) {
        return false;
    }



    //ovdbg @Override
    public void setActCoord_x(int actCoord_x) {
      this.actCoord_x = actCoord_x;
    }

    //ovdbg @Override
    public void setActCoord_y(int actCoord_y) {
        this.actCoord_y = actCoord_y;
    }

    //ovdbg @Override
    public int getActCoord_x() {
        return this.actCoord_x;
    }

    //ovdbg @Override
    public int getActCoord_y() {
        return this.actCoord_y;
    }

    //ovdbg @Override
    public void init(Painter painter) {
        this.painter = painter;
    }

    //ovdbg @Override
    public int getActCoord_x2() {
        return this.actCoord_x2;
    }

    //ovdbg @Override
    public int getActCoord_y2() {
        return this.actCoord_y2;
    }

    //ovdbg @Override
    public void setActCoord_x2(int actCoord_x2) {
     this.actCoord_x2 = actCoord_x2;
    }

    //ovdbg @Override
    public void setActCoord_y2(int actCoord_y2) {
       this.actCoord_y2 = actCoord_y2;
            
            
    }

    //ovdbg @Override
    public String getIdentifier() {
         return identifier;
    }

    //ovdbg @Override
    public void drawLine(Graphics gc, Object_TwoCoordinates object_twoCoordinates, int level, float[] delta) {
     }

    //ovdbg @Override
    public void draw(Graphics gc, int level, float[] delta) {
     }

    public boolean isHide() {
        return hide;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public boolean isMarked() {
        return marked;
    }


   



}
