/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map;
import techtree.AppletMain;

/**
 *
 * @author Bullet
 */
public class TechRelation extends Object_TwoCoordinates {

    private int reqResearchStatus;
    private int sourceStatus;
    private int reqResearchId;
    private int sourceId;
    private ResDesc sourceResearch;
    private static int STARPICTURE_SIZE = 150;
    private static final int MAXIMG_SIZE = 150;
    private AppletMain main;
    private boolean hide = false;

    public TechRelation(int reqResearchId, int sourceId, HashMap<String, I_Object_OneCoordinate> researches, AppletMain main) {
        this.reqResearchId = reqResearchId;
        this.sourceId = sourceId;
        this.main = main;

        this.identifier = reqResearchId + "  -  " + sourceId;
        ResDesc sourceResearch = null;
        ResDesc requResearch = null;

        for (Map.Entry<String, I_Object_OneCoordinate> o : researches.entrySet()) {
            if (o.getValue() instanceof ResDesc) {
                ResDesc rd = (ResDesc) o.getValue();
                if (rd.getId() == sourceId) {
                    sourceResearch = rd;
                } else if (rd.getId() == reqResearchId) {
                    requResearch = rd;
                }
                if ((sourceResearch != null && requResearch != null)) {
                    break;
                }
            }

        }

        if ((sourceResearch != null && requResearch != null)) {


            this.setX1((int) requResearch.getX());
            this.setY1((int) requResearch.getY());
            this.reqResearchStatus = requResearch.getStatus();
            this.setX2((int) sourceResearch.getX());
            this.setY2((int) sourceResearch.getY());
            this.sourceStatus = sourceResearch.getStatus();

            sourceResearch.setTotalRequRes(sourceResearch.getTotalRequRes() + 1);
            if (requResearch.getStatus() == 3) {
                sourceResearch.setRequResDone(sourceResearch.getRequResDone() + 1);
            }

        }

        if (requResearch == null && sourceResearch != null) {
            this.setX1((int) sourceResearch.getX());
            this.setY1((int) sourceResearch.getY());
        }

        this.sourceResearch = sourceResearch;

        main.getmainPaintCanvas().getTechRelations().add(identifier);
    }

    /**
     * @return the reqResearchId
     */
    public int getReqResearchId() {
        return reqResearchId;
    }

    @Override
    public String getIdentifier(){
        return identifier;
    }

    public void drawLine(Graphics gc, TechRelation techrelation, int level, float[] delta) {
      
        String sourceIdent = main.getmainPaintCanvas().idToName.get(techrelation.getSourceId());
        String reqIdent = main.getmainPaintCanvas().idToName.get(techrelation.getReqResearchId());
        ResDesc rd1= (ResDesc)main.getmainPaintCanvas().getInformationData().get(sourceIdent);
        ResDesc rd2 = (ResDesc)main.getmainPaintCanvas().getInformationData().get(reqIdent);
        int x1 = 0;
        int x2 = 0;
        if(rd1 != null){
            x1 = rd1.width;
        }
        if(rd2 != null){
            x2 = rd2.width;
        }
        if (techrelation.getReqResearchStatus() == 0) {

            gc.setColor(Color.RED);
        } else if (techrelation.getReqResearchStatus() == 1) {
            gc.setColor(Color.getHSBColor(0.051f, 0.973f, 1.000f));
        } else if (techrelation.getReqResearchStatus() == 2) {

            gc.setColor(Color.YELLOW);
        } else if (techrelation.getReqResearchStatus() == 3) {
            gc.setColor(Color.GREEN);
        }


        if(isMarked()){
            gc.setColor(Color.WHITE);
        }
        gc.drawLine((int) ((techrelation.actCoord_x + x2/2)-5),
                (int) (techrelation.actCoord_y),
                (int) ((techrelation.actCoord_x2 + x1/2)-5),
                (int) (techrelation.actCoord_y2 - 10));


        int wx = getZFactorAdjust(STARPICTURE_SIZE * delta[0]);
        int wy = getZFactorAdjust(STARPICTURE_SIZE * delta[1]);
        int size = Math.min(wx, wy);
        ResDesc rd1x = (ResDesc)main.getmainPaintCanvas().getInformationData().get(main.getmainPaintCanvas().idToName.get(this.sourceId));
    
       
    }

    /**
     * @return the sourceId
     */
    public int getSourceId() {
        return sourceId;
    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    /**
     * @return the reqResearchStatus
     */
    public int getReqResearchStatus() {
        return reqResearchStatus;
    }

    /**
     * @return the sourceStatus
     */
    public int getSourceStatus() {
        return sourceStatus;
    }



    /**
     * @return the hide
     */
    public boolean isHide() {
        return hide;
    }

    /**
     * @param hide the hide to set
     */
    public void setHide(boolean hide) {
        this.hide = hide;
    }

}
