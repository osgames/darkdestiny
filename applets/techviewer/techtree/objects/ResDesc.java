package techtree.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.JPanel;

import techtree.AppletMain;
import techtree.Technology;
import techtree.painting.DisplayManagement;
import techtree.panels.research.ResearchInformationPanel;

/**
 * Ein System, f�r die Anzeige
 *
 * @author martin
 *
 */
public class ResDesc extends Object_OneCoordinate {

    private int STARPICTURE_SIZE = 30;
    /**
     * Wie weit sind Planeten vom eigenen System aus zu sehen
     */
    private static final int VISIBLE_RADIUS = 100;
    private static final int MAXIMG_SIZE = 19;
    /**
     * Anzeige ob Feind Freund usw;
     */
    private int status = -1;
    /**
     * Daten f�r farbliche Hinterlegung im Join Modus
     */
    private int totalRequRes;
    private int requResDone;
    private int popPoints = 0;
    private int scorePoints = 0;
    private AppletMain mainApplet;
    private String identifier;
    private int rp;
    private int crp;
    private String description;
    private boolean marked = false;
    private boolean hide = false;
    public int width = 0;

    /**
     * @param y
     * @param x
     * @param name     
     */
    public ResDesc(String sysName, int id, float x, float y, int status, Image imgName, AppletMain mainApplet, int rp, int crp, String description) {
        super(id, sysName, x, y, imgName);
        this.rp = rp;
        this.crp = crp;
        this.description = description;
        this.status = status;
        this.mainApplet = mainApplet;
        width = 0;

        this.identifier = sysName;
        mainApplet.getmainPaintCanvas().getResearches().put(sysName, this);
    }

    public ResDesc(String sysName, int sysId, float x, float y, Image img, AppletMain mainApplet) {
        super(sysId, sysName, x, y, img);
        this.mainApplet = mainApplet;
        this.identifier = sysName;
    }

    //ovdbg @Override
    public boolean isToShow(int i) {
        return true;
    }

    //ovdbg @Override
    public void drawVisibleOvals(Graphics gc, int level, float[] delta) {
    }

    public void updateWidth(Graphics gc) {

        String resName = getName();
        int longestLine = gc.getFontMetrics().stringWidth(resName);
        width = longestLine;
    }
    //ovdbg @Override

    public void draw(Graphics gc, int level, float[] delta) {
        int height = 20;

        ArrayList<Technology> techs = mainApplet.getmainPaintCanvas().getUnlockedTech().get(getId());

        ArrayList<Integer> processed = new ArrayList<Integer>();
        int techCount = 0;
        if (techs != null) {
            for (Technology tech : techs) {
                if (tech.getResearchType() == 1) {
                    continue;
                }
                if (!processed.contains(tech.getResearchType())) {
                    techCount++;
                    processed.add(tech.getResearchType());
                }
            }
        }
        processed = new ArrayList<Integer>();
        gc.setColor(Color.WHITE);
        if (isMarked()) {


            gc.fillRect(this.getActCoord_x() - 22, this.getActCoord_y() - height / 2 - 2, width + 24 + techCount * 20, height + 4);

        }
        if (status == 0) {

            gc.setColor(Color.RED);
            if (this.requResDone == this.totalRequRes) {
                gc.setColor(Color.YELLOW);
            }
        } else if (status == 1) {


            gc.setColor(Color.getHSBColor(0.051f, 0.973f, 1.000f));
        } else if (status == 2) {

            gc.setColor(Color.YELLOW);
        } else if (status == 3) {
            gc.setColor(Color.GREEN);
        }



        if (getRequResDone() == getTotalRequRes()) {

            if (status == 1) {

                gc.setColor(Color.getHSBColor(0.051f, 0.703f, 1.00f));
            } else if (status == 3) {
                gc.setColor(Color.GREEN);
            } else {
                gc.setColor(Color.YELLOW);
            }

        } else if (getRequResDone() != getTotalRequRes()) {

            gc.setColor(Color.RED);

        } else {
            gc.setColor(Color.GREEN);
        }
        if (this.totalRequRes > 0) {
            gc.fillOval((int) ((((this.getActCoord_x() - 10 / 2))) + width / 2) - 5, (int) (this.getActCoord_y() - 10 / 2) - 10, 10, 10);
        }
        //Main
        gc.fillRect(this.getActCoord_x(), this.getActCoord_y() - height / 2, width + techCount * 20, height);
        //Links
        gc.fillRect(this.getActCoord_x() - 20, this.getActCoord_y() - height / 2, 20, 20);
        int count = 0;
        if (techs != null) {
            for (Technology tech : techs) {
                if (tech.getResearchType() == 1) {
                    continue;
                }
                if (!processed.contains(tech.getResearchType())) {
                    String picName = DisplayManagement.getGraphicName(tech.getResearchType(), tech.getDescription());
                    Image img = DisplayManagement.getGraphic(picName);

                    gc.fillRect(this.getActCoord_x() + width + count, this.getActCoord_y() - height / 2, 20, 20);
                    gc.drawImage(img, this.getActCoord_x() + width + count, this.getActCoord_y() - height / 2, null);

                    processed.add(tech.getResearchType());
                    count += 20;
                }
            }
        }
        gc.setColor(Color.BLACK);
        //Punkt
       // gc.fillOval(this.getActCoord_x() - 15, (this.getActCoord_y() - height / 2) + 5, 10, 10);

                    Image img = DisplayManagement.getGraphic("infoT.png");
        gc.drawImage(img, this.getActCoord_x() - 20, (this.getActCoord_y() - height / 2), null);

        gc.setColor(Color.BLACK);
        gc.drawString(this.getName(), this.getActCoord_x(), this.getActCoord_y() + 4);

    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    public int getPopPoints() {
        return popPoints;
    }

    public void setPopPoints(int popPoints) {
        this.popPoints = popPoints;
    }

    public int getScorePoints() {
        return scorePoints;
    }

    public void setScorePoints(int scorePoints) {
        this.scorePoints = scorePoints;
    }

    //ovdbg @Override
    public String getIdentifier() {
        return this.identifier;
    }

    //ovdbg @Override
    public JPanel getPanel() {

        return new ResearchInformationPanel(this, mainApplet);

    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the totalRequRes
     */
    public int getTotalRequRes() {
        return totalRequRes;
    }

    /**
     * @param totalRequRes the totalRequRes to set
     */
    public void setTotalRequRes(int totalRequRes) {
        this.totalRequRes = totalRequRes;
    }

    /**
     * @return the requResDone
     */
    public int getRequResDone() {
        return requResDone;
    }

    /**
     * @param requResDone the requResDone to set
     */
    public void setRequResDone(int requResDone) {
        this.requResDone = requResDone;
    }

    /**
     * @return the rp
     */
    public int getRp() {
        return rp;
    }

    /**
     * @param rp the rp to set
     */
    public void setRp(int rp) {
        this.rp = rp;
    }

    /**
     * @return the crp
     */
    public int getCrp() {
        return crp;
    }

    /**
     * @param crp the crp to set
     */
    public void setCrp(int crp) {
        this.crp = crp;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the marked
     */
    public boolean isMarked() {
        return marked;
    }

    /**
     * @param marked the marked to set
     */
    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    /**
     * @return the hide
     */
    public boolean isHide() {
        return hide;
    }

    /**
     * @param hide the hide to set
     */
    public void setHide(boolean hide) {
        this.hide = hide;
    }
}
