/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package techtree.objects;

import java.awt.Graphics;
import javax.swing.JPanel;
import techtree.Painter;

/**
 *
 * @author Horst
 */
public interface I_Object_OneCoordinate {
    
    abstract String getIdentifier();
    abstract JPanel getPanel();
    
    abstract int getActCoord_x();
    abstract int getActCoord_y();
    abstract void setActCoord_x(int actCoord_x);
    abstract void setActCoord_y(int actCoord_y);
    abstract void init(Painter painter);
    abstract boolean isHide();
    abstract void setHide(boolean hide);
    
    abstract boolean isToShow(int i);
    abstract void draw(Graphics gc, int level, float[] delta);

    abstract void setMarked(boolean marked);
    abstract boolean isMarked();
}
