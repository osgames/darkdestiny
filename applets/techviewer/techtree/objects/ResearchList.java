package techtree.objects;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
import techtree.AppletConstants;

import techtree.AppletMain;
import techtree.panels.research.ResearchListPanel;

/**
 * Ein System, f�r die Anzeige
 *
 * @author martin
 *
 */
public class ResearchList{

    /**
     * Anzeige ob Feind Freund usw;
     */
    /**
     * Daten f�r farbliche Hinterlegung im Join Modus
     */
    private AppletMain main;

    /**
     * @param y
     * @param x
     * @param name     
     */
     public ResearchList(AppletMain main) {
         this.main = main;
    //    mainApplet.getmainPaintCanvas().getResearches().put(sysName,this);
    }



    //ovdbg @Override
    public String getIdentifier() {
        return "researchlist";
    }

    //ovdbg @Override
    public JPanel getPanel() {
        return new ResearchListPanel(main);

    }

}
