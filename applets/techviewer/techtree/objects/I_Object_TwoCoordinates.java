/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree.objects;

import java.awt.Graphics;

/**
 *
 * @author Horst
 */
public interface I_Object_TwoCoordinates extends I_Object_OneCoordinate {

    abstract public int getX1();

    abstract public int getY1();

    abstract public void setX1(int x1);

    abstract public void setY1(int y1);

    abstract public int getX2();

    abstract public int getY2();

    abstract public void setX2(int x2);

    abstract public void setY2(int y2);

    abstract public int getActCoord_x2();

    abstract public int getActCoord_y2();

    abstract public void setActCoord_x2(int actCoord_x2);

    abstract public void setActCoord_y2(int actCoord_y2);
    
        abstract void drawLine(Graphics gc, Object_TwoCoordinates object_twoCoordinates, int level, float[] delta);
       
    
}
