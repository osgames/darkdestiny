/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree.panels.search;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import techtree.AppletMain;
import techtree.Painter;
import techtree.objects.ResDesc;
import techtree.objects.ResearchList;
import techtree.painting.DisplayManagement;

/**
 *
 * @author Eobane
 */
public class SearchPanel extends JPanel implements ActionListener {
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     *
     * @author HorstRabe
     */
    private JLabel searchLabel;
    private JTextField searchTextField = new JTextField();
    private SpringLayout layout;
    private int type;
    private String[] options;
    private JButton searchButton;
    private JButton rootButton;
    private JButton rootButtonHidden;
    private JButton unmark;
    private Painter painter;
    private JButton list;
    private JButton help;
    private Locale l;


    public SearchPanel(AppletMain main, int type, String[] options) {

        this.l = main.l;
        this.painter = main.getmainPaintCanvas();
        layout = new SpringLayout();
        this.setLayout(layout);
        this.type = type;
        this.options = options;

   
        searchTextField.setPreferredSize(new Dimension(200, 18));
        initializeComponents();
        addComponents();
        orderComponents();
        this.setBackground(Color.DARK_GRAY);

        this.setPreferredSize(new Dimension(500, 23));




    }

    public void update(int selectedIndex) {

        searchLabel.setText(options[selectedIndex] + " :");

        this.getSearchTextField().setVisible(true);
        searchLabel.setText(options[selectedIndex] + " :");

        searchLabel.setForeground(Color.WHITE);

        this.type = selectedIndex;
    }

    private void addComponents() {
        this.add(searchLabel);
        this.add(getSearchTextField());
        this.add(searchButton);
        this.add(rootButton);
        this.add(rootButtonHidden);
        this.add(unmark);

        this.getSearchTextField().setVisible(true);

        unmark.addActionListener(this);
        rootButtonHidden.addActionListener(this);
        rootButton.addActionListener(this);
        searchButton.addActionListener(this);
    }

    private void initializeComponents() {

        list = new JButton();
        help = new JButton();
        Image helpImage = DisplayManagement.getGraphic("help.png");
        Image listImage = DisplayManagement.getGraphic("list.png");
        ImageIcon helpIcon = new ImageIcon(helpImage);
        ImageIcon listIcon = new ImageIcon(listImage);
        list.setIcon(listIcon);
        list.setPreferredSize(new Dimension(20, 20));
        help.setIcon(helpIcon);
        help.setPreferredSize(new Dimension(20, 20));

        list.addActionListener(this);
        help.addActionListener(this);

        this.add(list);
        this.add(help);

        searchButton = new JButton();
        rootButton = new JButton();
        unmark = new JButton();
        rootButtonHidden = new JButton();
        Image buttonImage = DisplayManagement.getGraphic("search.png");
        Image unmarkImage = DisplayManagement.getGraphic("cancelP.png");
        Image treeImage = DisplayManagement.getGraphic("tree.png");
        Image treehImage = DisplayManagement.getGraphic("treeh.png");
        ImageIcon treeIcon = new ImageIcon(treeImage);
        ImageIcon treehIcon = new ImageIcon(treehImage);
        ImageIcon searchButtonIcon = new ImageIcon(buttonImage);
        ImageIcon unmarkImageIcon = new ImageIcon(unmarkImage);
        rootButton.setIcon(treeIcon);
        rootButton.setPreferredSize(new Dimension(20, 20));
        rootButtonHidden.setIcon(treehIcon);
        rootButtonHidden.setPreferredSize(new Dimension(20, 20));
        unmark.setIcon(unmarkImageIcon);
        unmark.setPreferredSize(new Dimension(20, 20));
        searchButton.setIcon(searchButtonIcon);
        searchButton.setPreferredSize(new Dimension(20, 20));
        searchLabel = new JLabel(options[type] + " :");
        searchLabel.setForeground(Color.WHITE);
    }

    private void orderComponents() {

        layout.putConstraint(SpringLayout.NORTH, searchLabel, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, searchLabel, 2, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.WEST,getSearchTextField(), 0, SpringLayout.EAST, searchLabel);


        layout.putConstraint(SpringLayout.WEST, searchButton, 5, SpringLayout.EAST,getSearchTextField());
        layout.putConstraint(SpringLayout.NORTH, searchButton, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, rootButton, 5, SpringLayout.EAST, searchButton);
        layout.putConstraint(SpringLayout.NORTH, rootButton, 0, SpringLayout.NORTH, this);


        layout.putConstraint(SpringLayout.WEST, rootButtonHidden, 5, SpringLayout.EAST, rootButton);
        layout.putConstraint(SpringLayout.NORTH, rootButtonHidden, 0, SpringLayout.NORTH, this);


        layout.putConstraint(SpringLayout.WEST, unmark, 5, SpringLayout.EAST, rootButtonHidden);
        layout.putConstraint(SpringLayout.NORTH, unmark, 0, SpringLayout.NORTH, this);


        layout.putConstraint(SpringLayout.WEST, list, 5, SpringLayout.EAST, unmark);
        layout.putConstraint(SpringLayout.NORTH, list, 0, SpringLayout.NORTH, this);


        layout.putConstraint(SpringLayout.WEST, help, 5, SpringLayout.EAST, list);
        layout.putConstraint(SpringLayout.NORTH, help, 0, SpringLayout.NORTH, this);

    }

    public void actionPerformed(ActionEvent e) {

        String name = "";
        ResDesc rd = null;
        name = "";
        try {
            name = getSearchTextField().getText();
            for(Map.Entry<String, ResDesc> entry: painter.getResearches().entrySet()){
                if(entry.getKey().equalsIgnoreCase(name)){
                    rd = entry.getValue();
                }
            }
        } catch (Exception ex) {
            System.out.println("NAN");
        }
        if (e.getSource() == searchButton) {

            if (!name.equals("")) {
                painter.switchToResearchName(name, null);
            }
        } else if (e.getSource() == rootButton) {
            if (!name.equals("")) {
                if (rd != null) {

                    painter.findTreeFor(rd.getIdentifier(), false);
                }
            }
            if (!name.equals("")) {
                painter.switchToResearchName(name, null);
            }
        } else if (e.getSource() == rootButtonHidden) {

            if (!name.equals("")) {
                if (rd != null) {
                    painter.findTreeFor(rd.getIdentifier(), true);
                }
            }
            if (!name.equals("")) {
                painter.switchToResearchName(name, null);
            }
        } else if (e.getSource() == unmark) {
            this.searchTextField.setText("");
            painter.unmark();

        }else if(e.getSource() == list){


                        ResearchList rl = new ResearchList(painter.main);
                        painter.main.updateInformationPanel2(rl);
        }else if(e.getSource() == help){
            painter.main.setShowHelp(true);

        }
    }

    /**
     * @return the searchTextField
     */
    public JTextField getSearchTextField() {
        return searchTextField;
    }
}
