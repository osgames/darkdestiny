/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree.panels.research;

import techtree.panels.*;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import techtree.AppletMain;
import techtree.objects.ResDesc;

/**
 *
 * @author Horst
 */
public class ResearchListPanel extends JPanel implements I_InformationPanel {

    private SpringLayout layout;
    private String identifier;

    public ResearchListPanel(AppletMain mainApplet) {
        this.identifier = "researchlist";
        this.setBackground(Color.BLACK);

        this.setPreferredSize(new Dimension(mainApplet.getmainPaintCanvas().getResearches().size() * 50,
                (int) mainApplet.informationScrollPane.getPreferredSize().getHeight()));
        this.layout = new SpringLayout();
        this.setLayout(layout);
        boolean isFirst = true;
        ResearchListEntry rletmp = null;
        for (Map.Entry<String, ResDesc> entry : mainApplet.getmainPaintCanvas().getResearches().entrySet()) {
            ResearchListEntry rle = new ResearchListEntry(mainApplet, entry.getValue(), this);
            this.add(rle);

            if (isFirst) {


                layout.putConstraint(SpringLayout.WEST, rle, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, rle, 0, SpringLayout.NORTH, this);
                isFirst = false;
            } else {

                layout.putConstraint(SpringLayout.WEST, rle, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, rle, 0, SpringLayout.SOUTH, rletmp);
            }
            rletmp = rle;
        }
        this.setPreferredSize(new Dimension(mainApplet.getmainPaintCanvas().getResearches().size()
                * (int) rletmp.getPreferredSize().getHeight(),
                mainApplet.getmainPaintCanvas().getResearches().size()
                * (int) rletmp.getPreferredSize().getHeight()));

        this.setVisible(true);

    }

    public String getIdentifier() {
        return this.identifier;
    }
}
