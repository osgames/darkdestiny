/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree.panels.research;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import techtree.Technology;
import techtree.painting.DisplayManagement;
import techtree.panels.GraphPanel;

/**
 *
 * @author Bullet
 */
public class TechnologyListEntry extends JPanel {

    private SpringLayout layout;
    private Technology tech;

    public TechnologyListEntry(Technology tech) {
        this.tech = tech;

        this.setBackground(Color.WHITE);
        layout = new SpringLayout();
        JLabel name = new JLabel(tech.getName());
        name.setFont(new Font("Tahoma", 1, 9));
        this.add(name);
        String picName = DisplayManagement.getGraphicName(tech.getResearchType(), tech.getDescription());
       System.out.println("picnaem : " + picName);
        JPanel img = new GraphPanel(picName, 20, 20);
        this.add(img);
        this.setLayout(layout);
        layout.putConstraint(SpringLayout.WEST, img, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, img, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, name, 0, SpringLayout.EAST, img);
        layout.putConstraint(SpringLayout.NORTH, name, 0, SpringLayout.NORTH, this);


        this.setPreferredSize(new Dimension(183,20));
               this.setVisible(true);
       this.validate();
    }
}
