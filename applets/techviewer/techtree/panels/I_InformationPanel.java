/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package techtree.panels;

import javax.swing.JPanel;

/**
 *
 * @author HorstRabe
 */
 public interface I_InformationPanel {

    /**
     * 
     * 
     * @return An unique identifier which identifys the Panel
     */
     
    String getIdentifier();
   
}
