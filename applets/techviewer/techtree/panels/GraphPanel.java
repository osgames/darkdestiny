/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree.panels;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import javax.swing.border.Border;
import techtree.CentredBackgroundBorder;
import techtree.painting.DisplayManagement;

/**
 *
 * @author HorstRabe
 */
public class GraphPanel extends JPanel {

    public GraphPanel(String picName, int width, int height) {
        this.setPreferredSize(new Dimension(width, height));
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic(picName), picName);
        final Border background = new CentredBackgroundBorder(bi);
        this.setBorder(background);

    }
}
