/**
 *
 */
package techtree;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * @author martin
 *
 */
public class SelectedRect implements MouseListener, MouseMotionListener {
    
    float x_start = Float.NaN;
    float y_start = Float.NaN;
    private Painter parent;
    private int tmpx;
    private int tmpy;
    private int x_int;
    private int y_int;
    private boolean active = false;
    /**
     * @param parent
     *
     */
    public SelectedRect(Painter parent) {
        this.parent = parent;
        parent.addMouseListener(this);
        parent.addMouseMotionListener(this);
    }
    
    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mouseMoved(MouseEvent e) {}
    
        /* (non-Javadoc)
         * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
         */
    public void mousePressed(MouseEvent e) {
        x_start = parent.convertToFloatX(e.getX(), e.getY());
        y_start = parent.convertToFloatY(e.getX(), e.getY());
        x_int = e.getX();
        y_int = e.getY();
        active = true;
    }
    
        /* (non-Javadoc)
         * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
         */
    public void mouseReleased(MouseEvent e) {
           
            float tmpx = parent.convertToFloatX(e.getX(), e.getY());
            float tmpy = parent.convertToFloatY(e.getX(), e.getY());
            
                
                
               
        if(Math.abs(x_int - e.getX())> 5 && Math.abs(y_int - e.getY())>5){
          //parent.setSystemSize(tmpx/x_start, tmpy/y_start);
                
                parent.zoomRect(x_start, y_start, tmpx, tmpy); }
                x_start = Float.NaN;
                y_start = Float.NaN;
           
        
        active = false;
    }
    
        /* (non-Javadoc)
         * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
         */
    public void mouseDragged(MouseEvent e) {
        tmpx = e.getX();
        tmpy = e.getY();
        active = true;
        parent.repaint();
    }
    
    public boolean isActive() {
        return active;
    }
    
    public void paint(Painter painter, Graphics g) {

        if(!painter.connectingObjects && !painter.ctrlPressed){
        g.setColor(Color.white);
        int w = Math.abs(tmpx - x_int);
        int h = Math.abs(tmpy - y_int);
        
        g.drawRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w,h);}
    }
    
}


