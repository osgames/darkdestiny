package techtree;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SpringLayout;
import techtree.buttons.BlueButton;
import techtree.painting.DisplayManagement;

/**
 * 
 *
 * @author Morgenstern
 * @author martin
 */
public class Buttons extends Panel{

    /**
     * Eine Serial-Nummer 
     */
    private static final long serialVersionUID = -8458214492780849089L;
    private BlueButton button1 = null;
    private BlueButton button2 = null;
    private BlueButton left = null;
    private BlueButton right = null;
    private BlueButton up = null;
    private BlueButton down = null;
    private Painter ap;
    SpringLayout layout;
    private int yoff;

    /** Creates a new instance of ButtonFunctions */
    public Buttons(AppletMain main) {
        this.setPreferredSize(new Dimension((int) main.getPreferredSize().getWidth(), (int) (40)));
        yoff = (int)main.getPreferredSize().getWidth()/2;
    }

    public void setPainter(Painter ap) {
        this.setPreferredSize(new Dimension((int) ap.main.getPreferredSize().getWidth(), (int) (40)));

        this.ap = ap;
        layout = new SpringLayout();
        this.setLayout(layout);
        this.setBackground(Color.BLACK);
        initialize();
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {



        this.add(getButton1());
        this.add(getButton2());
        this.add(getRight());
        this.add(getLeft());
        this.add(getUp());
        this.add(getDown());

        layout.putConstraint(SpringLayout.WEST, button1,yoff-40 , SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, button1, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, button2, 0, SpringLayout.WEST, button1);
        layout.putConstraint(SpringLayout.NORTH, button2, 0, SpringLayout.SOUTH, getButton1());


        layout.putConstraint(SpringLayout.WEST, left, 15, SpringLayout.EAST, getButton1());
        layout.putConstraint(SpringLayout.NORTH, left, 0, SpringLayout.NORTH, getButton2());

        layout.putConstraint(SpringLayout.WEST, up, 0, SpringLayout.EAST, getLeft());
        layout.putConstraint(SpringLayout.NORTH, up, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, down, 0, SpringLayout.EAST, getLeft());
        layout.putConstraint(SpringLayout.NORTH, down, 0, SpringLayout.NORTH, getLeft());

        layout.putConstraint(SpringLayout.WEST, right, 0, SpringLayout.EAST, getDown());
        layout.putConstraint(SpringLayout.NORTH, right, 0, SpringLayout.NORTH, getLeft());



    }

    public BlueButton getButton1() {
        if (button1 == null) {
            button1 = new BlueButton();
            button1.setName("buttonZoomOut");
            Image buttonImage = DisplayManagement.getGraphic("zoomOut.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            button1.setIcon(sendFleetIcon);
            button1.setPreferredSize(new Dimension(20, 20));
            button1.setMargin(new Insets(0, 0, 0, 0));
            button1.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    ap.zoomOut();
                }
            });
        }
        return button1;
    }

    public BlueButton getButton2() {
        if (button2 == null) {
            button2 = new BlueButton();
            button2.setName("buttonZoomIn");
            Image buttonImage = DisplayManagement.getGraphic("zoomIn.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            button2.setIcon(sendFleetIcon);
            button2.setPreferredSize(new Dimension(20, 20));
            button2.setMargin(new Insets(0, 0, 0, 0));
            button2.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    ap.zoomIn();
                }
            });
        }
        return button2;
    }

    public BlueButton getLeft() {
        if (left == null) {
            left = new BlueButton();
            left.setName("swithLeft");
            Image buttonImage = DisplayManagement.getGraphic("arrleft.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            left.setIcon(sendFleetIcon);
            left.setPreferredSize(new Dimension(20, 20));
            left.setMargin(new Insets(0, 0, 0, 0));
            left.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    ap.moveImage("left");
                }
            });
        }
        return left;
    }

    public BlueButton getRight() {
        if (right == null) {
            right = new BlueButton();
            right.setName("swithright");
            Image buttonImage = DisplayManagement.getGraphic("arrright.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            right.setIcon(sendFleetIcon);
            right.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    ap.moveImage("right");
                }
            });
        }
        return right;
    }

    public BlueButton getUp() {
        if (up == null) {
            up = new BlueButton();
            up.setName("swithup");
            Image buttonImage = DisplayManagement.getGraphic("arrup.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            up.setIcon(sendFleetIcon);
            up.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    ap.moveImage("up");
                }
            });
        }
        return up;
    }

    public BlueButton getDown() {
        if (down == null) {
            down = new BlueButton();
            down.setName("swithdown");
            Image buttonImage = DisplayManagement.getGraphic("arrdown.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            down.setIcon(sendFleetIcon);
            down.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    ap.moveImage("down");
                }
            });
        }
        return down;
    }

    public Painter getPainter() {
        return ap;
    }

}  //  @jve:decl-index=0:visual-constraint="30,10"

