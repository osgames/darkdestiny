package techtree;

/**
 * Hiermit wird konfiguriert, welche Systeme, etc. sichtbar sind
 *
 * @author Morgenstern
 * @author martin
 */
public class HideShowSystems {
	/**
	 * Der Status der alle Systeme beschreibt
	 */
    public static final int SYSTEMSTATUS_ALL = -2;
     
	private static boolean show_PlanetVisibleArea = true;
    private static boolean show_Unknown = true;
    private static boolean[] show_system = {true,true,true,true,true,true,true,true};
    
    public static boolean setShowAble(int sysKind, boolean show){
    	
    	if (sysKind == SYSTEMSTATUS_ALL) //Show All / Hide All
    	{
    		show_Unknown = show;
    		for (int i = 0; i < show_system.length; i++)
    			show_system[i] = show;
    		return true;
    	}
    	//sysKind in eine Nummer konvertieren:
    	if (sysKind == -1)
    	{
    		if (show_Unknown != show)
    		{
    			show_Unknown = show;
    			return true;
    		}
    		return false;
    	}
    	if (show_system[sysKind] != show)
    		{
    		show_system[sysKind] = show;
    			return true;
    		}
   		return false;
    }
    

    public static boolean isDrawPlanetVisibleArea() {
		return show_PlanetVisibleArea;
	}

    public static boolean isSystemToShow(int systemStatus) {
    	if (systemStatus == -1)
    		return show_Unknown;
		return show_system[systemStatus];
	}


	public static boolean isDrawFleet() {
		return false;
	}
    
}
