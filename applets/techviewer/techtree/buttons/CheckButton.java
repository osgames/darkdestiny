/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree.buttons;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;

import techtree.CentredBackgroundBorder;
import techtree.painting.DisplayManagement;

/**
 *
 * @author Eobane
 */
public class CheckButton extends JButton {

    private SpringLayout layout;
    public JCheckBox checkBox;

    public CheckButton(String name) {

        this.setPreferredSize(new Dimension(224, 21));
        JLabel nameLabel = new JLabel(name);
        nameLabel.setForeground(Color.white);

        this.layout = new SpringLayout();
        this.setLayout(layout);

        checkBox = new JCheckBox();
        checkBox.setMargin(new Insets(-1, -1, -1, -1));
        checkBox.setBounds(0, 0, 0, 0);
        checkBox.setBackground(Color.BLACK);
        this.add(checkBox);
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic("checkButton.png"), "checkButton.png");

        final Border background = new CentredBackgroundBorder(bi);
        this.add(nameLabel);
        this.setBorder(background);
        this.repaint();

        layout.putConstraint(SpringLayout.WEST, nameLabel, 3, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, nameLabel, 0, SpringLayout.NORTH, this);


        layout.putConstraint(SpringLayout.EAST, checkBox, -6, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, checkBox, 3, SpringLayout.NORTH, this);


    }





      public static boolean hasAlpha(Image image) {

             // If buffered image, the color model is readily available

             if (image instanceof BufferedImage) {return ((BufferedImage)image).getColorModel().hasAlpha();}



             // Use a pixel grabber to retrieve the image's color model;

             // grabbing a single pixel is usually sufficient

             PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);

             try {pg.grabPixels();} catch (InterruptedException e) {}



             // Get the image's color model

             return pg.getColorModel().hasAlpha();

         }
  

    /**
     * @return the checkBox
     */
    public JCheckBox getCheckBox() {
        return checkBox;
    }

    /**
     * @param checkBox the checkBox to set
     */
    public void setCheckBox(JCheckBox checkBox) {
        this.checkBox = checkBox;
    }
}
