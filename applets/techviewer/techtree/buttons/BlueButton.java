package techtree.buttons;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

/**
 * 
 *
 *
 * @author Stefan
 */
public class BlueButton extends JButton {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 2512144058801786723L;

	/** Creates a new instance of BlueButton */
    public BlueButton() {
        this.setMargin(new Insets(0,0,0,0));
        this.setPreferredSize(new Dimension(20,20));
        this.setBackground(Color.BLACK);
        this.setForeground(Color.WHITE);
    }    
}
