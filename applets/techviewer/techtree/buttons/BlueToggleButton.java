package techtree.buttons;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import techtree.Buttons;
import techtree.HideShowSystems;
import techtree.painting.DisplayManagement;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

/**
 *
 * @author Stefan
 */
public class BlueToggleButton extends JToggleButton {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -2485759902418256130L;
	
	
	private final int idx;
	private final Buttons b;

	/** Creates a new instance of BlueButton */
    public BlueToggleButton(int idx, Buttons b, String tooltip) {
        this.idx = idx;
		this.b = b;
        this.setMargin(new Insets(0,0,0,0));
        this.setPreferredSize(new Dimension(20,20));
		this.setToolTipText(tooltip);
		this.setBackground(Color.BLACK);
        this.setForeground(Color.WHITE);
    	setIcon(new ImageIcon(DisplayManagement.getGraphicForSystemState(idx)));
    	setSelected(true);
    	addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
            	if (HideShowSystems.setShowAble(BlueToggleButton.this.idx, isSelected()))
            		BlueToggleButton.this.b.getPainter().forceRefresh();
            }
        });
    }    
}
