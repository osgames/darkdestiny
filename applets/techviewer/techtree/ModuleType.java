/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package techtree;

/**
 *
 * @author Stefan
 */
public enum ModuleType {
    CHASSIS, WEAPON, SHIELD, ARMOR, ENGINE, REACTOR,
    TRANSPORT_RESSOURCE, TRANSPORT_POPULATION, HANGAR, 
    ENERGY_STORAGE, SPECIAL
}
