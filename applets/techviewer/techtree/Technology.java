/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package techtree;

/**
 *
 * @author Bullet
 */
public class Technology {

    
    private int id;
    private String name;
    private int researchType;
    private String description;
    private int sourceId;

    public Technology(int id, int researchType, String name,  String description, int sourceId){
        this.id = id;
        this.researchType = researchType;
        this.sourceId = sourceId;
        this.name = name;
        this.description = description;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the researchType
     */
    public int getResearchType() {
        return researchType;
    }

    /**
     * @param researchType the researchType to set
     */
    public void setResearchType(int researchType) {
        this.researchType = researchType;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the sourceId
     */
    public int getSourceId() {
        return sourceId;
    }
}
