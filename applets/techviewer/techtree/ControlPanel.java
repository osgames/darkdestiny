/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package techtree;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import techtree.objects.I_Object_OneCoordinate;
import techtree.objects.ResDesc;
import techtree.objects.TechRelation;
import techtree.panels.search.SearchPanel;

/**
 *
 * @author HorstRabe
 */
public class ControlPanel extends JPanel implements ActionListener {

    private JButton printSQL;
    private Painter ap;
    private SpringLayout layout;
    private JComboBox searchOptions;
    private String[] optionsString;
    private DefaultComboBoxModel optionsModel;
    private SearchPanel searchPanel;
    private AppletMain main;

    /** Creates a new instance of ButtonFunctions */
    public ControlPanel(AppletMain main) {
        this.main = main;
        this.setPreferredSize(new Dimension((int) main.getPreferredSize().getWidth(), (int) 40));

    }

    public void setPainter(Painter ap) {
        this.ap = ap;
        initialize();
    }

    public void initialize() {
        this.layout = new SpringLayout();
        this.setLayout(layout);
        this.setBackground(Color.DARK_GRAY);

        printSQL = new JButton("P");
        optionsString = new String[]{ML.getMLStr("lbl_searchforresearch", main.l)};
        optionsModel = new DefaultComboBoxModel(optionsString);
        searchOptions = new javax.swing.JComboBox();
        searchOptions.setModel(optionsModel);

        searchPanel = new SearchPanel(main, searchOptions.getSelectedIndex(), optionsString);

        searchOptions.addActionListener(this);
        searchOptions.setPreferredSize(new Dimension(100, 18));






        this.add(getSearchPanel());

        if (ap.editMode) {
            printSQL.addActionListener(this);
            this.add(printSQL);

        }

        layout.putConstraint(SpringLayout.WEST, getSearchPanel(), 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, getSearchPanel(), 2, SpringLayout.NORTH, this);
        this.setPreferredSize(new Dimension((int) ap.main.getPreferredSize().getWidth(), (int) (getSearchPanel().getPreferredSize().getHeight())));

        layout.putConstraint(SpringLayout.WEST, printSQL, 4, SpringLayout.EAST, getSearchPanel());
        layout.putConstraint(SpringLayout.NORTH, printSQL, 2, SpringLayout.NORTH, this);

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == searchOptions) {
            getSearchPanel().update(searchOptions.getSelectedIndex());
        }
        if (e.getSource() == printSQL) {
            System.out.println("Wanna print sql");
            for (Map.Entry<String, I_Object_OneCoordinate> informationEntity : ap.getInformationData().entrySet()) {
                if (informationEntity.getValue() instanceof ResDesc) {
                    ResDesc rd = (ResDesc) informationEntity.getValue();
                    System.out.println("UPDATE resalignment SET x='" + (int) rd.getX() + "' , y='" + (int) rd.getY() + "' WHERE id=" + rd.getId() + ";");
                }
            }
            for (Map.Entry<String, I_Object_OneCoordinate> informationEntity : ap.getInformationData().entrySet()) {
                if (informationEntity.getValue() instanceof TechRelation) {
                    TechRelation tr = (TechRelation) informationEntity.getValue();
                    //INSERT INTO `techrelation` VALUES ('9', '0', '8', '1');
                    System.out.println("INSERT INTO techrelation VALUES(" + tr.getSourceId() + ",0," + tr.getReqResearchId() + "," + 1 + ");");
                }
            }

            for (Map.Entry<Integer, ArrayList<Technology>> informationEntity : ap.getUnlockedTech().entrySet()) {
                int id = informationEntity.getKey();
                for (Technology t : informationEntity.getValue()) {
                    int sourceId = t.getSourceId();
                    int type = t.getResearchType();
                    System.out.println("INSERT INTO techrelation VALUES(" + sourceId + ",0," + id + "," + type + ");");
                }

            }



        }

    }

    /**
     * @return the searchPanel
     */
    public SearchPanel getSearchPanel() {
        return searchPanel;
    }
}
