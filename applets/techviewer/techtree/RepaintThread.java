/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package techtree;

/**
 *
 * @author Aion
 */
public class RepaintThread extends Thread {
    private boolean stopped = false;
    private Painter painter;

    public RepaintThread(Painter p) {
        this.painter = p;
    }

    @Override
    public void run() {
        while(!stopped) {
            try {
                Thread.sleep(50);
            } catch (Exception e) { }
            painter.forceRefresh();
        }
    }

    public void setStop() {
        stopped = true;
    }
}
