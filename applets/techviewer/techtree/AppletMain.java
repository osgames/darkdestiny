package techtree;

import java.applet.Applet;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import javax.swing.JApplet;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import techtree.objects.I_Object_OneCoordinate;
import techtree.objects.ResearchList;
import techtree.painting.DisplayManagement;
import techtree.panels.Glossar;

/**
 * Im MainApplet befinden sich 4 wichtige Teile
 *  - Painter (Hier wird die Sternenkarte gezeichnet)
 *  - Buttons(Panel) Zum navigieren per Button
 *  - ControlPanel zum ändern der Anzeigeeigenschaften
 *  - InformationPanel  für nähere Details auf der Karte
 *
 *
 * @author Eobane
 */
public class AppletMain extends JApplet implements KeyEventDispatcher {

    private SpringLayout layout;
    public JScrollPane informationScrollPane = new JScrollPane();
    public JPanel informationPanel = new JPanel(new CardLayout());
    private ArrayList<String> identifiers;
    private static final long serialVersionUID = 1L;
    private static Applet instance = null;
    private Painter mainPaintCanvas = null;
    private HashMap<Integer, String> troops;
    public Buttons but = null;
    private ControlPanel controlPanel;
    private boolean showHelp = false;
    private Glossar help;
    protected boolean showInformationPanel = false;
    private String baseUrl = "";
    int helpx = 0;
    int helpy = 0;
    public Locale l = null;
    private Thread rpThread;
    private RepaintThread rt;

    public AppletMain() throws HeadlessException {
        super();
        instance = this;
    }

    public static Applet getApplet() {
        return instance;
    }

    /**
     * This method initializes this
     *
     * @return void
     */
    public void init() {
        Runtime r = Runtime.getRuntime();


        baseUrl = this.getApplet().getCodeBase().getPath();
        layout = new SpringLayout();
        this.setLayout(layout);

        int width = (int) this.getSize().getWidth();
        int height = (int) this.getSize().getHeight();
       //  width = 1500;
      //  height = 1000;

        helpx = (height - 388) / 2 - 100;
        helpy = (width - 341) / 2;


        this.setSize(width, height);
        this.setPreferredSize(new Dimension(width, height));
        this.setBackground(Color.BLACK);

        Runtime rt = Runtime.getRuntime();
        //Buttons to navigate over the map

        but = new Buttons(this);
        controlPanel = new ControlPanel(this);
        AppletManagement.init(this, this.getHeight(), this.getWidth());
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);

        ArrayList<String> images = new ArrayList<String>();

        images.add("cancelP.png");
        images.add("cancel.jpg");
        images.add("zoomOut.png");
        images.add("zoomIn.png");
        images.add("arrright.png");
        images.add("arrleft.png");
        images.add("arrup.png");
        images.add("arrdown.png");


        for (String image : images) {
            try {
                DisplayManagement.bufferGraphic(image);
            } catch (Exception e) {
                System.out.println("Error while loading image: " + image + " : " + e);
            }
        }
        //A List of all invoked and added Panels to the informationScrollPanel
        identifiers = new ArrayList<String>();

        but.setPainter(getmainPaintCanvas());
        //Panel to specify which details to display

        getControlPanel().setPainter(getmainPaintCanvas());
        help = new Glossar(this);
        help.setVisible(false);
        /**
         * @informationScrollPane Detailed Information to following Entitys
         * -Traderoutes
         * -Systems
         * -Fleets(Design/Shipping)
         */
        informationScrollPane = new JScrollPane(informationPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        informationScrollPane.getVerticalScrollBar().setUnitIncrement(15);
        informationScrollPane.setPreferredSize(new Dimension(200, (int) (this.getPreferredSize().getHeight() - getControlPanel().getPreferredSize().getHeight() - but.getPreferredSize().getHeight())));
        //Adding Components to Panel (ControlPanel, Buttons, Informationpanel, Painter)

        addComponents();

        //Order Components using Springlayout
        orderComponents();


        validate();

    }

    public void orderComponents() {



        layout.putConstraint(SpringLayout.WEST, getControlPanel(), 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, getControlPanel(), 0, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, getControlPanel(), 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, but, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, but, 0, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, but, 0, SpringLayout.SOUTH, getControlPanel());

        layout.putConstraint(SpringLayout.WEST, help, helpy, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, help, helpx, SpringLayout.NORTH, this);

        if (showInformationPanel) {

            layout.putConstraint(SpringLayout.EAST, getInformationScrollPane(), 0, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.SOUTH, getInformationScrollPane(), 0, SpringLayout.SOUTH, this);
            layout.putConstraint(SpringLayout.NORTH, getInformationScrollPane(), 0, SpringLayout.SOUTH, but);



            layout.putConstraint(SpringLayout.EAST, getmainPaintCanvas(), 0, SpringLayout.WEST, getInformationScrollPane());
            layout.putConstraint(SpringLayout.SOUTH, getmainPaintCanvas(), 0, SpringLayout.SOUTH, this);
            layout.putConstraint(SpringLayout.WEST, getmainPaintCanvas(), 0, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, getmainPaintCanvas(), 0, SpringLayout.SOUTH, but);
        } else {



            int height = (int) (this.getPreferredSize().getHeight() - getControlPanel().getPreferredSize().getHeight() - but.getPreferredSize().getHeight());
            getmainPaintCanvas().setPreferredSize(new Dimension((int) this.getPreferredSize().getWidth(), height));
            this.remove(getInformationScrollPane());

            layout.putConstraint(SpringLayout.EAST, getmainPaintCanvas(), 0, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.WEST, getmainPaintCanvas(), 0, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, getmainPaintCanvas(), 0, SpringLayout.SOUTH, but);
            layout.putConstraint(SpringLayout.SOUTH, getInformationScrollPane(), 0, SpringLayout.SOUTH, this);

        }


    }

    //ovdbg @Override
    public void start() {
        super.start();
        super.init();



    }

    public void setShowHelp(boolean show) {
        if (help == null) {
            help = new Glossar(this);
        }
        if (!show) {
            help.setVisible(false);
            getmainPaintCanvas().setVisible(true);
        } else {
            help.setVisible(true);
            getmainPaintCanvas().setVisible(false);
        }
        validate();
    }

    // to remove/add the informationPanel
    public void changeShowStatus(boolean selected) {

        this.showInformationPanel = selected;

        System.out.println("Helpx : " + helpx + " helpy : " + helpy);

        layout.putConstraint(SpringLayout.WEST, help, helpy, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, help, helpx, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, getControlPanel(), 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, getControlPanel(), 0, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, getControlPanel(), 0, SpringLayout.NORTH, this);
        getmainPaintCanvas().updatePanel(showInformationPanel);

        if (showInformationPanel) {

            this.add(getInformationScrollPane());
            layout.putConstraint(SpringLayout.EAST, getInformationScrollPane(), 0, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.SOUTH, getInformationScrollPane(), 0, SpringLayout.SOUTH, this);
            layout.putConstraint(SpringLayout.NORTH, getInformationScrollPane(), 0, SpringLayout.SOUTH, but);



            layout.putConstraint(SpringLayout.EAST, getmainPaintCanvas(), 0, SpringLayout.WEST, getInformationScrollPane());
            layout.putConstraint(SpringLayout.SOUTH, getmainPaintCanvas(), 0, SpringLayout.SOUTH, this);
            layout.putConstraint(SpringLayout.WEST, getmainPaintCanvas(), 0, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, getmainPaintCanvas(), 0, SpringLayout.SOUTH, but);
        } else {

            int height = (int) (this.getPreferredSize().getHeight() - getControlPanel().getPreferredSize().getHeight() - but.getPreferredSize().getHeight());
            getmainPaintCanvas().setPreferredSize(new Dimension((int) this.getPreferredSize().getWidth(), height));
            this.remove(getInformationScrollPane());

            layout.putConstraint(SpringLayout.EAST, getmainPaintCanvas(), 0, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.WEST, getmainPaintCanvas(), 0, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, getmainPaintCanvas(), 0, SpringLayout.SOUTH, but);
            layout.putConstraint(SpringLayout.SOUTH, getInformationScrollPane(), 0, SpringLayout.SOUTH, this);

        }
        validate();
    }

    public Painter getmainPaintCanvas() {
        if (mainPaintCanvas == null) {
            mainPaintCanvas = new Painter(this);
            rt = new RepaintThread(mainPaintCanvas);
            rpThread = new Thread(rt);
          /*  if (mainPaintCanvas.editMode) {
                rpThread.start();
            }*/
            mainPaintCanvas.setName("mainPaintCanvas");
        }

        return mainPaintCanvas;
    }

    public void setInformationPanel(JPanel informationPanel) {
        this.informationPanel = informationPanel;
    }

    //Displaying and Adding an InformationPanel about the Entity to the informationPanel
    public void updateInformationPanel(I_Object_OneCoordinate informationEntity) {

        if (!showInformationPanel) {
            this.changeShowStatus(true);
        }

        CardLayout c1 = (CardLayout) informationPanel.getLayout();

        informationScrollPane.getViewport().setView(informationEntity.getPanel());
        if (identifiers.indexOf(informationEntity.getIdentifier()) > -1) {

            c1.show(informationPanel, informationEntity.getIdentifier());
        } else {
            identifiers.add(informationEntity.getIdentifier());
            informationPanel.add(informationEntity.getPanel(), informationEntity.getIdentifier());
            c1.show(informationPanel, informationEntity.getIdentifier());
        }
        informationPanel.revalidate();
        informationPanel.repaint();



    }

    //Displaying and Adding an InformationPanel about the Entity to the informationPanel
    public void updateInformationPanel2(ResearchList informationEntity) {
        System.out.println("inhere");
        if (!showInformationPanel) {
            this.changeShowStatus(true);
        }

        CardLayout c1 = (CardLayout) informationPanel.getLayout();

        informationScrollPane.getViewport().setView(informationEntity.getPanel());
        if (identifiers.indexOf(informationEntity.getIdentifier()) > -1) {

            c1.show(informationPanel, informationEntity.getIdentifier());
        } else {
            identifiers.add(informationEntity.getIdentifier());
            informationPanel.add(informationEntity.getPanel(), informationEntity.getIdentifier());
            c1.show(informationPanel, informationEntity.getIdentifier());
        }
        informationPanel.revalidate();
        informationPanel.repaint();



    }

    private void addComponents() {


        this.add(help);
        this.add(but);
        this.add(getControlPanel());
        //this.add(getInformationScrollPane());
        this.add(getmainPaintCanvas());
    }

    /**
     * @return the informationScrollPane
     */
    public JScrollPane getInformationScrollPane() {
        return informationScrollPane;
    }

    /**
     * @return the troops
     */
    public HashMap<Integer, String> getTroops() {
        return troops;
    }

    /**
     * @param troops the troops to set
     */
    public void setTroops(HashMap<Integer, String> troops) {
        this.troops = troops;
    }

    /**
     * @return the controlPanel
     */
    public ControlPanel getControlPanel() {
        return controlPanel;
    }

    /**
     * @return the l
     */
    public Locale getL() {
        return l;
    }  @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        
        if (e.getKeyCode() == 40 && e.getID() == 401) {
            getmainPaintCanvas().moveImage("down");
            return true;
        } else if (e.getKeyCode() == 39 && e.getID() == 401) {
            getmainPaintCanvas().moveImage("right");
            return true;
        } else if (e.getKeyCode() == 38 && e.getID() == 401) {
            getmainPaintCanvas().moveImage("up");
            return true;
        } else if (e.getKeyCode() == 37 && e.getID() == 401) {
            getmainPaintCanvas().moveImage("left");
            return true;
        } else if (e.getKeyCode() == e.VK_CONTROL && e.getID() == KeyEvent.KEY_PRESSED && !getmainPaintCanvas().ctrlPressed) {
            System.out.println("CTRL");
            getmainPaintCanvas().ctrlPressed = true;
            return true;
        }else if (e.getKeyCode() == e.VK_CONTROL && e.getID() == KeyEvent.KEY_RELEASED && getmainPaintCanvas().ctrlPressed) {
            System.out.println("CTRL Rel");
            getmainPaintCanvas().ctrlPressed = false;
            return true;
        }
        return false;
    }
}  //  @jve:decl-index=0:visual-constraint="10,10"

