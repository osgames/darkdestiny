package techtree;

import java.applet.AppletContext;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import techtree.objects.ResDesc;
import techtree.objects.TechRelation;
import xml.XMLMalFormatedException;
import xml.XMLMemo;

/**
 * Das AppletManagement dient dazu den XML Stream auszulesen  für
 * - Systeme u. Planeten
 * - Flotten
 * - Interne Handelsflotten
 * - Hyperraumscanner
 *
 *
 * @author martin
 *
 */
public class AppletManagement {

    public static AppletMain main;
    @SuppressWarnings("unused")
    private static AppletContext appletContext;
    private static String xmlPath = "F:\\";
    private static String tradeRoutesXmlPath;
    private static XMLMemo landCoding;
    private static boolean joinMode = false;
    private static int marked = 0;
    private static String basePath = "";

    public static void init(AppletMain main, int height, int width) {

        Runtime rt = Runtime.getRuntime();
        AppletManagement.main = main;
        appletContext = main.getAppletContext();
        if (main.getParameter("xml") != null) {
            xmlPath = main.getParameter("xml");
        }
        String source = main.getParameter("source");
        String join = main.getParameter("join");
        String mark = main.getParameter("mark");


        InputStream is;
        if (join != null) {
            System.out.println("Join Mode active");
            joinMode = true;
        } else {
            joinMode = false;
        }

        if (mark != null) {
            marked = Integer.parseInt(mark);
        } else {
            marked = 0;
        }

        if (source == null) {
            // joinMode = true;            
            source = "d:/xml/tech.xml";

            try {
                is = new FileInputStream(source);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        } else {
            URL url;

            // Get base path
            if (joinMode) {
                basePath = source.replace("createJoinMapInfo.jsp", "");
            } else {
                basePath = source.replace("createTechTreeInfo.jsp", "");

            }

            try {
                System.out.println("Looking for " + source);
                url = new URL(source);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return;
            }

            try {
                is = url.openStream();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        XMLMemo in;
        try {
            in = XMLMemo.parse(is);
        } catch (XMLMalFormatedException e) {
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        //Reading all Systems
        XMLMemo constants = in.getElements("Constants").get(0);

        parseConstants(main, constants);

        XMLMemo unlockedTechnologies = in.getElements("UnlockedTechnologies").get(0);

        for (Iterator i = unlockedTechnologies.getElements("UnlockedTechnology").iterator(); i.hasNext();) {
            XMLMemo technology = (XMLMemo) i.next();
            Technology tech = parseTechnology(technology);
            if (tech.getResearchType() != 1) {
            System.out.println("tech : " + tech.getId());
            ArrayList<Technology> ts = main.getmainPaintCanvas().getUnlockedTech().get(tech.getId());
            if (ts == null) {
                ts = new ArrayList<Technology>();
            }
            ts.add(tech);
                System.out.println("putting : " + tech.getId());

                for (Technology t : ts) {
                    System.out.println("=> : " + t.getSourceId());
                }
                main.getmainPaintCanvas().getUnlockedTech().put(tech.getId(), ts);
            }
            //     main.getmainPaintCanvas().
        }
        //Reading all Systems
        XMLMemo allSystems = in.getElements("Researches").get(0);

        for (Iterator i = allSystems.getElements("Research").iterator(); i.hasNext();) {
            XMLMemo res = (XMLMemo) i.next();
            ResDesc rd = parseResearch(res);
            main.getmainPaintCanvas().getInformationData().put(rd.getIdentifier(), rd);
            if(rd.getId() != 999){
            main.getmainPaintCanvas().addObject(rd);
            }
            main.getmainPaintCanvas().idToName.put(rd.getId(), rd.getIdentifier());
            main.getmainPaintCanvas().nameToId.put(rd.getIdentifier(), rd.getId());
        }
        //Reading all Systems
        XMLMemo techRelations = in.getElements("TechRelations").get(0);

        for (Iterator i = techRelations.getElements("TechRelation").iterator(); i.hasNext();) {
            XMLMemo techRelation = (XMLMemo) i.next();
            TechRelation tr = parseTechRelation(techRelation);
            main.getmainPaintCanvas().getInformationData().put(tr.getIdentifier(), tr);
        }


        try {

            is.close();
        } catch (Exception e) {

            System.out.println("Cant close Stream");
        }


        main.getmainPaintCanvas().fitView(height, width);

    }

    private static ResDesc parseResearch(XMLMemo res) {
        String name = res.getAttribute("name");
        int id = Integer.parseInt(res.getAttribute("id"));
        float x = Float.parseFloat(res.getAttribute("x"));
        float y = Float.parseFloat(res.getAttribute("y"));

        String description = res.getAttribute("description");
        int rp = Integer.parseInt(res.getAttribute("rp"));
        int crp = Integer.parseInt(res.getAttribute("crp"));
        int status = 0;
        if (res.getAttribute("status") != null) {
            status = Integer.parseInt(res.getAttribute("status"));
        }
        Image img = null;
        return new ResDesc(name, id, x, y, status, img, main, rp, crp, description);
    }

    private static TechRelation parseTechRelation(XMLMemo res) {
        int reqResearchId = Integer.parseInt(res.getAttribute("reqResearchId"));
        int sourceId = Integer.parseInt(res.getAttribute("sourceId"));
        return new TechRelation(reqResearchId, sourceId, main.getmainPaintCanvas().getInformationData(), main);
    }

    public static boolean isJoinMode() {
        return joinMode;
    }

    public static String getBasePath() {
        return basePath;
    }

    private static void parseConstants(AppletMain main, XMLMemo constants) {
        XMLMemo Language = constants.getElements("Language").get(0);
        String locale = Language.getAttribute("Locale");
        System.out.println("Got languageConstant : " + locale);
        main.l = new Locale(locale);
    }

    private static Technology parseTechnology(XMLMemo technology) {

        int id = Integer.parseInt(technology.getAttribute("resId"));
        int resType = Integer.parseInt(technology.getAttribute("resType"));
        int sourceId = Integer.parseInt(technology.getAttribute("sourceId"));


        String name = technology.getAttribute("name");
        String description = technology.getAttribute("description");

        return new Technology(id, resType, name, description, sourceId);
    }
}
