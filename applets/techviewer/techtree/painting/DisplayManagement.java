package techtree.painting;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.ImageIcon;
import techtree.AppletManagement;
import techtree.ModuleType;

/**
 * @author martin
 *
 */
public class DisplayManagement {

    // Die 100 Lichtjahre die man andere Systeme sieht
    public static final Color planetVisibleColor = Color.WHITE;
    private static Map<String, Image> imageBuffer = new TreeMap<String, Image>();
    private static String picPath = "pics/";

    public static Image getGraphicForSystemState(int i) {

        String picName = getImageName(i);
        if (!imageBuffer.containsKey(picName)) {
            URL file = DisplayManagement.class.getClassLoader().getResource(picPath + picName);
            Image img = AppletManagement.main.getImage(file);
            imageBuffer.put(picName, img);
            return img;
        }
        return imageBuffer.get(picName);
    }

    public static String getGraphicName(int resType, String description) {

        String picName = "";
        switch(resType){
            case(1):
                picName = "research.png";
                break;
            case(0):
                picName = "construction.png";
                break;
            case(2):
                picName = "module.png";
                if(description.endsWith(ModuleType.ARMOR.toString()) || description.endsWith("Panzerung")){
                    picName = "armor.png";
                }else if(description.endsWith(ModuleType.CHASSIS.toString()) || description.endsWith("Chassis")){
                    picName = "chassis.png";
                }else if(description.endsWith(ModuleType.SHIELD.toString()) || description.endsWith("Schild")){
                    picName = "shield.png";
                }else if(description.endsWith(ModuleType.WEAPON.toString()) || description.endsWith("Waffe")){
                    picName = "weapon.png";
                }
                break;
            case(3):
                picName = "groundtroop.png";
                break;
            case(4):
                picName = "improvement.png";
                break;
            case(5):
                picName = "ressource.png";
        }
       return picName;
    }
    public static Image getGraphic(String picName) {

        if (!imageBuffer.containsKey(picName)) {
            URL file = DisplayManagement.class.getClassLoader().getResource(picPath + picName);
            Image img = AppletManagement.main.getImage(file);
            imageBuffer.put(picName, img);
            return img;
        }
        return imageBuffer.get(picName);
    }

    public static void bufferGraphic(String picName) {

        if (!imageBuffer.containsKey(picName)) {
            URL file = DisplayManagement.class.getClassLoader().getResource(picPath + picName);
            BufferedImage img = toBufferedImage(AppletManagement.main.getImage(file), picName);
            imageBuffer.put(picName, img);
        }
    }

    private static String getImageName(int idx) {
        switch (idx) {

            case (-2):
                return "mainstar.gif"; // Hauptsysteme

            case (-1):
                return "graystar.gif";
            case (0):
                return "star_000.gif";
            case (1):
                return "star_001.gif"; //Eigene

            case (2):
                return "star_010.gif"; //Alliierte

            case (3):
                return "star_011.gif";
            case (4):
                return "star_100.gif"; //Feindlich

            case (5):
                return "star_101.gif";
            case (6):
                return "star_110.gif";
            case (7):
                return "star_111.gif";

        }
        return "graystar.gif";
    }

    public static BufferedImage toBufferedImage(Image image, String imageName) {

        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        }


        // This code ensures that all the pixels in the image are loaded

        image = new ImageIcon(image).getImage();



        // Determine if the image has transparent pixels

        boolean hasAlpha = hasAlpha(image);



        // Create a buffered image with a format that's compatible with the screen

        BufferedImage bimage = null;

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

        try {

            // Determine the type of transparency of the new buffered image

            int transparency = Transparency.OPAQUE;

            if (hasAlpha == true) {
                transparency = Transparency.BITMASK;
            }



            // Create the buffered image

            GraphicsDevice gs = ge.getDefaultScreenDevice();

            GraphicsConfiguration gc = gs.getDefaultConfiguration();

            bimage = gc.createCompatibleImage(image.getWidth(null), image.getHeight(null), transparency);

        } catch (HeadlessException e) {
        } //No screen



        if (bimage == null) {

            // Create a buffered image using the default color model

            int type = BufferedImage.TYPE_INT_RGB;

            if (hasAlpha == true) {
                type = BufferedImage.TYPE_INT_ARGB;
            }

            bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);

        }



        // Copy image to buffered image

        Graphics g = bimage.createGraphics();



        // Paint the image onto the buffered image

        g.drawImage(image, 0, 0, null);

        g.dispose();

        imageBuffer.remove(imageName);

        imageBuffer.put(imageName, bimage);
        return bimage;

    }

    public static boolean hasAlpha(Image image) {

        // If buffered image, the color model is readily available

        if (image instanceof BufferedImage) {
            return ((BufferedImage) image).getColorModel().hasAlpha();
        }



        // Use a pixel grabber to retrieve the image's color model;

        // grabbing a single pixel is usually sufficient

        PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);

        try {
            pg.grabPixels();
        } catch (InterruptedException e) {
        }



        // Get the image's color model

        return pg.getColorModel().hasAlpha();

    }
}
