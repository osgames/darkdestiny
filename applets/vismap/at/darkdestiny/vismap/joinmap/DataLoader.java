package at.darkdestiny.vismap.joinmap;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.joinmap.drawable.Galaxy;
import at.darkdestiny.vismap.joinmap.drawable.SystemDesc;
import at.darkdestiny.vismap.logic.AbstractDataLoader;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.xml.XMLMalFormatedException;
import at.darkdestiny.vismap.xml.XMLMemo;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import at.darkdestiny.vismap.joinmap.drawable.Tile;

/**
 * Das DataLoader dient dazu den XML Stream auszulesen für - Systeme u.
 * Planeten - Flotten - Interne Handelsflotten - Hyperraumscanner
 *
 *
 * @author martin
 *
 */
public class DataLoader extends AbstractDataLoader {

    public static AppletMain main;
    private static int marked = 0;
    private static String basePath = "";
    static int width;
    static int height;
    private static LoaderScreen loader;
    private static AppletData appletData;
    private static PainterData painterData;

    public DataLoader(AppletMain main, IAppletData appletData, IPainterData painterData, LoaderScreen l) {
       DataLoader.appletData = (at.darkdestiny.vismap.joinmap.AppletData)appletData;
       DataLoader.painterData = (at.darkdestiny.vismap.joinmap.PainterData)painterData;

        loader = l;

        DataLoader.main = main;

    }

    @Override
    public void init() {
        loadXMLData(main);

    }

    private static SystemDesc parseSystem(XMLMemo sys) {
        String name = sys.getAttribute("name");
        int id = Integer.parseInt(sys.getAttribute("id"));
        int x = Integer.parseInt(sys.getAttribute("x"));
        int y = Integer.parseInt(sys.getAttribute("y"));
        if (sys.getAttribute("sysstatus") != null) {
            int sysstatus = -1;
            try {
                sysstatus = Integer.parseInt(sys.getAttribute("sysstatus"));
            } catch (Exception e) {
            }

            DisplayManagement.setAppletMain(main);
            Image img = DisplayManagement.getGraphicForSystemState(sysstatus);
            if (sysstatus != -1) {
                SystemDesc sysDesc = null;


                sysDesc = new SystemDesc(name, id, x, y, img, main);
                sysDesc.setSystemStatus(sysstatus);

                if (id == marked) {
                    sysDesc.setSystemStatus(2);
                }


                if ((sysstatus == 1)) {
                    XMLMemo score = (XMLMemo) sys.getElements("Score").get(0);

                    sysDesc.setPopPoints(Integer.parseInt(score.getAttribute("popPoints")));
                    sysDesc.setScorePoints(Integer.parseInt(score.getAttribute("statPoints")));
                }

                return sysDesc;
            }
        }

        return new SystemDesc(name, id, x, y, DisplayManagement.getGraphicForSystemState(-1), main);
    }

    public static XMLMemo getXMLfromFile(InputStream is, AppletMain main) {
        String source = main.getParameter("source");

        if (source == null) {
            source = "c:/xml/joinmap.xml";

            loader.addLine("Getting XML file from: " + source);
            try {
                is = new FileInputStream(source);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            URL url;

            // Get base path
            basePath = source.replace("createJoinMapInfo.jsp", "");
            loader.addLine("Getting XML file from: " + basePath);
            try {

                url = new URL(source);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            try {
                long t1 = System.currentTimeMillis();
                loader.addLine("Loading Data - Requesting StarMapInfo - Done in " + (System.currentTimeMillis() - t1) + " ms");
                is = url.openStream();
                loader.addLine("Loading Data - Parsing Inputstream - Done in " + (System.currentTimeMillis() - t1) + " ms");
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        XMLMemo in;
        try {
            long t1 = System.currentTimeMillis();
            //     loader.addLine("Loading Data - Parse to XML - Start");
            in = XMLMemo.parse(is);
            loader.addLine("Loading Data - Parse to XML - Done in " + (System.currentTimeMillis() - t1) + " ms");
        } catch (XMLMalFormatedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        try {

            is.close();
        } catch (Exception e) {
        }
        return in;
    }

    public static void loadXMLData(AppletMain main) {


        InputStream is = null;
        XMLMemo in = getXMLfromFile(is, main);
        XMLMemo constants = in.getElements("Constants").get(0);
        System.currentTimeMillis();
        XMLMemo Language = constants.getElements("Language").get(0);
        parseLanguage(Language);

        try {

            XMLMemo UniverseConstants = constants.getElements("UniverseConstants").get(0);
            AppletConstants.UNIVERSE_SIZE_X = Integer.parseInt(UniverseConstants.getAttribute("width"));
            AppletConstants.UNIVERSE_SIZE_Y = Integer.parseInt(UniverseConstants.getAttribute("height"));
            AppletConstants.MAXXDIFF = AppletConstants.UNIVERSE_SIZE_Y * 2 * 4;
            AppletConstants.MAXYDIFF = AppletConstants.UNIVERSE_SIZE_X * 2 * 4;

        } catch (Exception e) {
            e.printStackTrace();
        }

        XMLMemo tiles = in.getElements("Tiles").get(0);
        parseTiles(tiles);
        XMLMemo galaxys = in.getElements("Galaxys").get(0);
        parseGalaxys(galaxys);
        loadSystems(in);


    }

    private static void parseLanguage(XMLMemo Language) {
        String locale = Language.getAttribute("Locale");
        String language = locale.substring(0, locale.indexOf("_"));
        String country = locale.substring(locale.indexOf("_") + 1, locale.length());
        Locale l = new Locale(language, country);
        appletData.setLocale(l);
    }

    private static void loadSystems(XMLMemo in) {

        //Reading all Systems
        if (in.getElements("Systems") != null && !in.getElements("Systems").isEmpty()) {
            XMLMemo allSystems = in.getElements("Systems").get(0);

            for (Iterator i = allSystems.getElements("System").iterator(); i.hasNext();) {
                XMLMemo sys = (XMLMemo) i.next();
                SystemDesc sd = parseSystem(sys);

                painterData.addDrawObject(AppletConstants.DRAW_LEVEL_SYSTEMS, sd);
                painterData.addDisplayObject(AppletConstants.DRAW_LEVEL_SYSTEMS, sd);
            }
        }

    }

    /**
     * @return the basePath
     */
    public static String getBasePath() {
        return basePath;
    }

    private static void parseTiles(XMLMemo tiles) {
        for (Iterator i = tiles.getElements("Tile").iterator(); i.hasNext();) {
            XMLMemo tile = (XMLMemo) i.next();

            int startX = Integer.parseInt(tile.getAttribute("x").trim());
            int endX = startX + 100;
            int startY = Integer.parseInt(tile.getAttribute("y").trim());
            int endY = startY + 100;
            int systems = Integer.parseInt(tile.getAttribute("systems"));
            int sysCount = 0;
            if (tile.getAttribute("sysCount") != null) {
                sysCount = Integer.parseInt(tile.getAttribute("sysCount"));
            }
            int colSysCount = 0;
            if (tile.getAttribute("colSysCount") != null) {
                colSysCount = Integer.parseInt(tile.getAttribute("colSysCount"));
            }
            int maxSysCount = 0;
            if (tile.getAttribute("maxSysCount") != null) {
                maxSysCount = Integer.parseInt(tile.getAttribute("maxSysCount"));
            }
            int maxSysCountPerTile = 0;
            if (tile.getAttribute("maxSysCountPerTile") != null) {
                maxSysCountPerTile = Integer.parseInt(tile.getAttribute("maxSysCountPerTile"));
            }
            int startSysCount = 0;
            if (tile.getAttribute("startSysCount") != null) {
                startSysCount = Integer.parseInt(tile.getAttribute("startSysCount"));
            }
            int popScore = Integer.parseInt(tile.getAttribute("popScore"));
            int userScore = Integer.parseInt(tile.getAttribute("userScore"));
            int intensity = Integer.parseInt(tile.getAttribute("intensity"));
            Tile t = new Tile(startX, startY, endX, endY, systems, popScore, userScore, intensity, sysCount, colSysCount, maxSysCount, maxSysCountPerTile, startSysCount);

            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_TILES, t);
            painterData.addDisplayObject(AppletConstants.DRAW_LEVEL_TILES, t);
        }
    }

    private static void parseGalaxys(XMLMemo galaxies) {
        for (Iterator i = galaxies.getElements("Galaxy").iterator(); i.hasNext();) {
            XMLMemo galaxy = (XMLMemo) i.next();

            int id = Integer.parseInt(galaxy.getAttribute("id"));
            int originX = Integer.parseInt(galaxy.getAttribute("originX"));
            int originY = Integer.parseInt(galaxy.getAttribute("originY"));
            int height = Integer.parseInt(galaxy.getAttribute("height"));
            int width = Integer.parseInt(galaxy.getAttribute("width"));
            int startX = originX - width / 2;
            int endX = originX + width / 2;
            int startY = originY - height / 2;
            int endY = originY + height / 2;
            boolean playerStart = Boolean.parseBoolean(galaxy.getAttribute("playerStart"));
            int startSystem = Integer.parseInt(galaxy.getAttribute("startSystem"));
            int endSystem = Integer.parseInt(galaxy.getAttribute("endSystem"));
            Galaxy g = new Galaxy(id, startX, startY, endX, endY, startSystem, endSystem, playerStart);

            appletData.addGalaxy(g.getId(), g);
            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_GALAXY, g);
            painterData.addDisplayObject(AppletConstants.DRAW_LEVEL_GALAXY, g);
        }
    }

    @Override
    public PainterData getPainterData() {
        return painterData;
    }

    @Override
    public AppletData getAppletData() {
        return appletData;
    }

    @Override
    public void reload(AppletMain main) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
