package at.darkdestiny.vismap.joinmap;

import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.gui.AppletMain;

import at.darkdestiny.vismap.logic.AppletConstants;
import java.awt.Color;
import java.awt.Graphics;

import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.joinmap.drawable.SystemDesc;
import at.darkdestiny.vismap.joinmap.drawable.SystemDesc_ToolTip;

public class Painter extends MainPainter {

    private static final long serialVersionUID = 4354204667927554366L;

    public SystemDesc_ToolTip showToolTipFor;
    SearchPopup searchPopup = null;
    private boolean grayedOut = false;
    private final boolean showDensity = true;

    /**
     * t
     *
     * @param main
     */
    public Painter(AppletMain main) {

        super(main);
    }

    @Override
    public void paint(Graphics g) {

        super.prePaint(g);

        float[] res = new float[2];
        float[] delta = new float[2];

        convertCoordinates(0, 0, res);
        convertCoordinates(1, 1, delta);
        delta[0] -= res[0];
        delta[1] -= res[1];

        if (dataChanged) {

            if (backBuffer == null) {
                backBuffer = createImage(getWidth(), getHeight());
            }
            Graphics gc = backBuffer.getGraphics();

            gc.setColor(Color.black);
            gc.fillRect(0, 0, this.getWidth(), this.getHeight());

            super.paint(gc);

            dataChanged = false;
        }

        g.drawImage(backBuffer, 0, 0, getWidth(), getHeight(), null);

        if (showToolTipFor != null) {
            showToolTipFor.paint(appletData, g);
        }

        globalRatio = (float) getWidth() / (float) getHeight();

        super.postPaint(g);
        if (grayedOut) {

            g.setColor(new Color(1f, 1f, 1f, 0.5f));
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
        if (mouse.active) {
            mouse.paint(this, g);
        }
        if (!main.isInitialized() && !main.isReloaded()) {

            float factor;
            int width = AppletConstants.UNIVERSE_SIZE_X;
            int height = AppletConstants.UNIVERSE_SIZE_Y;
            if (painterData.getWidth() > painterData.getHeight()) {
                factor = (float) painterData.getWidth() / (float) painterData.getHeight();
                width *= factor;
            } else {
                factor = (float) painterData.getHeight() / (float) painterData.getWidth();
                height *= factor;
            }
            zoomRect(0, 0, width, height);
            main.setInitialized(true);
            main.setReloaded(false);
            dataChanged = true;
            repaint();

        }

    }

    public void switchToSystemId(int systemId) {
        SystemDesc tmp;
        SystemDesc found = null;

        for (I_Object_OneCoordinate entry : painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_SYSTEMS)) {
            if (entry instanceof SystemDesc) {
                tmp = (SystemDesc) entry;
                if (tmp.getId() == systemId) {
                    found = tmp;
                    tmp.setMarked(true);
                } else {
                    tmp.setMarked(false);
                }

            }
        }
        if (found != null) {
            switchToCoordinates(Math.round(found.getX1()), Math.round(found.getY1()));
            showPopUp(false);
        } else {
            searchPopup.setMessage("System# " + systemId + " not found", true);
        }
    }

    public void showPopUp(boolean showPopUp) {
        if (showPopUp && searchPopup == null) {
            searchPopup = new SearchPopup(this, appletData.getLocale());
            searchPopup.pack();
            searchPopup.setLocationRelativeTo(this);
            grayedOut = true;
            forceRefresh();
        } else {
            grayedOut = false;
            searchPopup.setVisible(false);
            searchPopup.dispose();
            searchPopup = null;
            forceRefresh();
        }
    }

    public boolean isShowDensity() {
        return showDensity;
    }
}
