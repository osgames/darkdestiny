/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.joinmap.buttons;

import at.darkdestiny.vismap.buttons.BlueButton;
import at.darkdestiny.vismap.buttons.BlueToggleButton;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.joinmap.Painter;
import at.darkdestiny.vismap.joinmap.PainterData;
import at.darkdestiny.vismap.logic.ReloadThread;
import java.applet.AppletContext;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.ToolTipManager;

/**
 *
 * @author Admin
 */
public class ToolBox extends JPanel implements ActionListener {

    private BlueButton refresh = null;
    private BlueButton help = null;
    private BlueButton search = null;
    private BlueToggleButton density = null;
    private final Painter painter;
    SpringLayout layout = new SpringLayout();
    List<JComponent> buttons = new ArrayList();
    private final AppletMain appletMain;

    public ToolBox(IPainter painter, AppletMain appletMain) {
        this.setLayout(layout);
        ToolTipManager ttm = ToolTipManager.sharedInstance();
        ttm.setInitialDelay(100);
        this.appletMain = appletMain;
        this.painter = (Painter)painter;

        buildPanel();
    }

    private void buildPanel() {


        refresh = new BlueButton("Refresh", "refresh.png", 30, 30, painter);
        refresh.addActionListener(this);

        density = new BlueToggleButton("Density", "refresh.png", true, 30, 30);
        density.addActionListener(this);

        help = new BlueButton("Hilfe", "help.png", 30, 30, painter);
        help.addActionListener(this);


        search = new BlueButton("Suche", "search.png", 30, 30, painter);
        search.addActionListener(this);

        buttons.add(search);
        buttons.add(help);


        if (buttons.size() > 0) {

            for (JComponent button : buttons) {
                this.add(button);
            }
        }


        layout.putConstraint(SpringLayout.WEST, search, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, search, 2, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, refresh, 0, SpringLayout.WEST, search);
        layout.putConstraint(SpringLayout.NORTH, refresh, 20, SpringLayout.SOUTH, search);

        layout.putConstraint(SpringLayout.WEST, help, 0, SpringLayout.WEST, refresh);
        layout.putConstraint(SpringLayout.NORTH, help, 0, SpringLayout.SOUTH, refresh);

        this.setPreferredSize(new Dimension(30, 160));
        this.setOpaque(false);
        this.setVisible(true);
    }

    public IPainter getPainter() {
        return painter;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == search) {
            ((at.darkdestiny.vismap.joinmap.Painter) painter).showPopUp(true);

        } else if (e.getSource() == refresh) {

            appletMain.getLoader().setVisible(true);
            ReloadThread lt = new ReloadThread(appletMain);
            lt.start();
        }else if (e.getSource() == density) {

            ((PainterData)painter.getPainterData()).switchShowTiles();
            painter.forceRefresh();
        } else if (e.getSource() == help) {
            try {
                AppletContext a = appletMain.getAppletContext();
                a.showDocument(new URL("http://www.thedarkdestiny.at/wiki/index.php/Sternenkarte"), "_new");

            } catch (Exception ex) {
                System.out.print("Error while hyperlinking:" + ex);
            }
        }
    }

}
