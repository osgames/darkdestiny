/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.joinmap;

import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.logic.ML;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

/**
 *
 * @author Admin
 */
public class SearchPopup extends JDialog implements ActionListener {

    SpringLayout layout;
    private final String[] options;
    private final JComboBox searchOptions;
    private final JTextField firstValue;
    private final JTextField secondValue;
    private final JButton searchButton;
    private final DefaultComboBoxModel optionsModel;
    private IPainter painter;
    private String message;
    private final JLabel messageLabel;

    public SearchPopup(final Painter p, Locale l) {
        super(new Frame(), "Suche: ");
        message = "";

        this.painter = p;
        options = new String[]{
            ML.getMLStr("lbl_systemid", l),
            ML.getMLStr("lbl_coordinates", l)};
        optionsModel = new DefaultComboBoxModel(options);
        searchOptions = new javax.swing.JComboBox();
        searchOptions.setModel(optionsModel);
        firstValue = new JTextField();
        firstValue.setPreferredSize(new Dimension(150, 20));
        secondValue = new JTextField();
        secondValue.setPreferredSize(new Dimension(70, 20));
        messageLabel = new JLabel(message);
        messageLabel.setForeground(Color.WHITE);
        this.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {

                setVisible(false);
                dispose();
                ((at.darkdestiny.vismap.joinmap.Painter) painter).showPopUp(false);
                p.forceRefresh();
            }
        });

        searchButton = new JButton("Suche");
        searchButton.setPreferredSize(new Dimension(150, 20));

        searchOptions.addActionListener(this);
        searchOptions.setPreferredSize(new Dimension(150, 20));

        layout = new SpringLayout();
        this.getContentPane().setBackground(Color.BLACK);
        this.setLayout(layout);
        this.add(searchOptions);
        this.add(firstValue);
        this.add(searchButton);
        this.add(secondValue);
        this.add(messageLabel);
        secondValue.setVisible(false);
        this.setPreferredSize(new Dimension(470, 80));
        searchButton.addActionListener(this);

        layout.putConstraint(SpringLayout.WEST, searchOptions, 0, SpringLayout.WEST, this.getContentPane());
        layout.putConstraint(SpringLayout.NORTH, searchOptions, 0, SpringLayout.NORTH, this.getContentPane());

        layout.putConstraint(SpringLayout.WEST, firstValue, 0, SpringLayout.EAST, searchOptions);
        layout.putConstraint(SpringLayout.NORTH, firstValue, 0, SpringLayout.NORTH, this.getContentPane());

        layout.putConstraint(SpringLayout.WEST, secondValue, 73, SpringLayout.EAST, searchOptions);
        layout.putConstraint(SpringLayout.NORTH, secondValue, 0, SpringLayout.NORTH, this.getContentPane());

        layout.putConstraint(SpringLayout.EAST, searchButton, 0, SpringLayout.EAST, this.getContentPane());
        layout.putConstraint(SpringLayout.NORTH, searchButton, 0, SpringLayout.NORTH, this.getContentPane());

        layout.putConstraint(SpringLayout.WEST, messageLabel, 0, SpringLayout.WEST, this.getContentPane());
        layout.putConstraint(SpringLayout.SOUTH, messageLabel, 0, SpringLayout.SOUTH, this.getContentPane());

        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == searchOptions) {
            if (searchOptions.getSelectedIndex() == 1) {
                firstValue.setPreferredSize(new Dimension(70, 20));
                secondValue.setVisible(true);
            } else {

                firstValue.setPreferredSize(new Dimension(150, 20));
                secondValue.setVisible(false);
            }
            this.revalidate();
        }
        if (e.getSource() == searchButton) {
            int type = searchOptions.getSelectedIndex();
            switch (type) {
                case (0):

                    int value = -1;
                    try {

                        value = Integer.parseInt(firstValue.getText());

                    } catch (NumberFormatException ex) {
                        System.out.println("NAN");
                    }
                    if (value >= 0) {

                        ((at.darkdestiny.vismap.joinmap.Painter) painter).switchToSystemId(value);
                    }
                    break;

                case (1):

                    value = -1;
                    int value2 = -1;
                    try {

                        value = Integer.parseInt(firstValue.getText());
                        value2 = Integer.parseInt(secondValue.getText());
                        if (value > AppletConstants.UNIVERSE_SIZE_X) {
                            value = AppletConstants.UNIVERSE_SIZE_X;
                        }
                        if (value2 > AppletConstants.UNIVERSE_SIZE_Y) {
                            value2 = AppletConstants.UNIVERSE_SIZE_Y;
                        }
                        if (value < 0) {
                            value = 0;
                        }
                        if (value2 < 0) {
                            value2 = 0;
                        }

                    } catch (NumberFormatException ex) {
                        System.out.println("NAN");
                    }
                    if (value >= 0) {

                        ((at.darkdestiny.vismap.joinmap.Painter) painter).switchToCoordinates(value, value2);
                    }
                    break;
            }
        }
    }

    public void setMessage(String message, boolean error) {
        this.message = message;
        Color c = Color.WHITE;
        if (error) {
            c = Color.RED;
        }
        messageLabel.setForeground(c);
        messageLabel.setText(message);
    }
}
