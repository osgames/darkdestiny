/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.joinmap;

import at.darkdestiny.vismap.dto.AbstractAppletData;
import at.darkdestiny.vismap.joinmap.drawable.Galaxy;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class AppletData extends AbstractAppletData{

    private final Map<Integer, Galaxy> galaxies;

    public AppletData() {
        galaxies = new HashMap<>();
    }

    public void addGalaxy(Integer id, Galaxy galaxy) {
        getGalaxies().put(id, galaxy);
    }

    /**
     * @return the galaxies
     */
    public Map<Integer, Galaxy> getGalaxies() {
        return galaxies;
    }
}
