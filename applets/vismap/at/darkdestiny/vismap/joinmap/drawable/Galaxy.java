/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.joinmap.drawable;

import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Admin
 */
public class Galaxy extends Object_TwoCoordinates{

    int startX;
    int startY;
    int endX;
    int endY;

    int startSystem;
    int endSystem;
    boolean playerStart;

    public Galaxy(int id, int x1, int y1, int x2, int y2, int startSystem, int endSystem, boolean playerStart) {
        super(id, x1, y1, x2, y2);
        this.startSystem = startSystem;

        this.endSystem = endSystem;
        this.playerStart = playerStart;

    }

    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
        if(playerStart){
        gc.setColor(Color.GREEN);
         gc.drawRect(actCoord_x,
                actCoord_y,
                Math.round((getX2() - getX1()) * delta[0]),
                Math.round((getY2() - getY1()) *delta[1]));
        }else{

        gc.setColor(Color.RED);
         gc.drawRect(actCoord_x,
                actCoord_y,
                Math.round((getX2() - getX1()) * delta[0]),
                Math.round((getY2() - getY1()) * delta[1]));
         gc.drawLine(actCoord_x,
                actCoord_y,
                Math.round(actCoord_x2),
                Math.round(actCoord_y2));
         gc.drawLine(actCoord_x,
                actCoord_y + Math.round((getY2() - getY1()) *delta[1]),
                Math.round(actCoord_x + Math.round((getX2() - getX1()) * delta[0])),
                Math.round(actCoord_y));
        }
    }

}
