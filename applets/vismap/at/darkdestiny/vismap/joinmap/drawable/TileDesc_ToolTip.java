/**
 *
 */
package at.darkdestiny.vismap.joinmap.drawable;

import at.darkdestiny.vismap.joinmap.AppletData;
import java.awt.*;

import java.util.ArrayList;

/**
 * @author martin
 *
 */
public class TileDesc_ToolTip {

    private int x;
    private int y;
    private int width;
    private int height;
    private boolean xoff = false;
    private boolean yoff = false;
    Tile tile;
    
    public TileDesc_ToolTip(Tile t, int x, int y, int globalWidth, int globalHeight) {
        this.x = x;
        this.y = y;
        this.tile = t;

    }
        public TileDesc_ToolTip(Tile t, int x, int y) {
        this.x = x;
        this.y = y;
        this.tile = t;
    }

    public void paint(AppletData appletData, Graphics g) {

        int rectWidth = 20;
        int rectHeight = 20;
        Galaxy galaxy = null;
        for(Galaxy galaxyValue : (ArrayList<Galaxy>)appletData.getGalaxies().values()){
            if(x > galaxy.getX1() && x < galaxy.getX2() && y > galaxy.getY1() && y < galaxy.getY2()){
                galaxy = galaxyValue;
                break;
            }
        }
        String sysName = "Tile " + tile.getIdentifier() + "Galaxy " + galaxy.getId();
        int longestLine = g.getFontMetrics().stringWidth(sysName);

        Font normalFont = g.getFont();
        Font boldFont = new Font(normalFont.getFontName(),Font.BOLD,normalFont.getSize());

        g.setFont(normalFont);
        int w = longestLine;
        this.width = w;

        g.setFont(boldFont);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, w+rectWidth, rectHeight);
        g.setColor(Color.black);
        g.drawRect(x, y, w+rectWidth, rectHeight);

        g.drawString(sysName, x+10, y+15);

        g.setFont(normalFont);


    }

    public void setPos(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public boolean isXoff() {
        return xoff;
    }

    public boolean isYoff() {
        return yoff;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
