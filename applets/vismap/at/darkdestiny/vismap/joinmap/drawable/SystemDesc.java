package at.darkdestiny.vismap.joinmap.drawable;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.logic.HideShowSystems;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

/**
 * Ein System, f�r die Anzeige
 *
 * @author martin
 *
 */
public class SystemDesc extends Object_OneCoordinate {

    private final int STARPICTURE_SIZE = 30;
    private long lastUpdate = 0l;
    private boolean marked = false;
    private static final int MAXIMG_SIZE = 19;
    private final int planetCount = 0;
    private int level;
    private int systemStatus = -1;
    private int popPoints = 0;
    private int scorePoints = 0;
    private final String identifier;

    public SystemDesc(String sysName, int sysId, int x, int y, int systemStatus, Image imgName, AppletMain mainApplet, long lastUpdateSystem) {
        super(sysId, sysName, x, y, imgName);
        this.lastUpdate = lastUpdateSystem;
        this.systemStatus = systemStatus;
        this.identifier = AppletConstants.ID_INFORMATIONPANEL_SYSTEM + String.valueOf(sysId);
    }

    public SystemDesc(String sysName, int sysId, int x, int y, Image img, AppletMain mainApplet) {
        super(sysId, sysName, x, y, img);
        this.identifier = AppletConstants.ID_INFORMATIONPANEL_SYSTEM + String.valueOf(sysId);
    }

    public int getPlanetCount() {
        return planetCount;

    }

    public int getSystemStatus() {
        return systemStatus;
    }

    @Override
    public boolean isToShow(int i) {
        if (i == 0) {
            if (getSystemStatus() == 1 || getSystemStatus() == 3 || getSystemStatus() == 5 || getSystemStatus() == 7) {
                return HideShowSystems.isDrawPlanetVisibleArea();
            }
            return false;
        }
        if (i == 1) {
            return HideShowSystems.isSystemToShow(-1);
        }
        return HideShowSystems.isSystemToShow(getSystemStatus());
    }

    public void draw(IAppletData appletData, Graphics gc, int level, float[] delta) {
        this.level = level;
        draw(appletData, gc, delta);
    }

    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
        if (!HideShowSystems.isSystemToShow(systemStatus)) {
            return;
        }
        switch (level) {

            case 3: //Hier werden die grauen Systeme gezeichnet
            {
                int wx = getZFactorAdjust(STARPICTURE_SIZE * delta[0]);
                int wy = getZFactorAdjust(STARPICTURE_SIZE * delta[1]);
                int size = Math.min(wx, wy);
                gc.drawImage(DisplayManagement.getGraphicForSystemState(-1), this.getActCoord_x1() - size / 2, this.getActCoord_y1() - size / 2, size, size, null);
                break;
            }
            case 9: //Hier werden die Systeme noch mal gezeichnet
            {

                if (isMarked()) {

                    gc.setColor(new Color(0f, 1f, 1f, 0.9f));
                    gc.drawOval(Math.round(this.getActCoord_x1() - getZFactorAdjust(50 * delta[0], 50) / 2),
                            Math.round(this.getActCoord_y1() - getZFactorAdjust(50 * delta[1], 50) / 2),
                            Math.round(getZFactorAdjust(50 * delta[0], 50)),
                            Math.round(getZFactorAdjust(50 * delta[1], 50)));
                }

                int wx = getZFactorAdjust(STARPICTURE_SIZE * delta[0]);
                int wy = getZFactorAdjust(STARPICTURE_SIZE * delta[1]);
                int size = Math.min(wx, wy);
                gc.drawImage(getImgName(), this.getActCoord_x1() - size / 2, this.getActCoord_y1() - size / 2, size, size, null);
                break;
            }

        }
    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    private int getZFactorAdjust(float inValue, int maxSize) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > maxSize) {
            return maxSize;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    public float getPriority() {
        return ((planetCount > 0) ? getId() : -1);
    }

    public int getPopPoints() {
        return popPoints;
    }

    public void setPopPoints(int popPoints) {
        this.popPoints = popPoints;
    }

    public int getScorePoints() {
        return scorePoints;
    }

    public void setScorePoints(int scorePoints) {
        this.scorePoints = scorePoints;
    }

    public void setSystemStatus(int systemStatus) {
        this.systemStatus = systemStatus;
    }

    @Override
    public String getIdentifier() {
        return this.identifier;
    }

    /**
     * @return the lastUpdate
     */
    public long getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param lastUpdate the lastUpdate to set
     */
    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public JPanel getPanel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * @return the marked
     */
    @Override
    public boolean isMarked() {
        return marked;
    }

    /**
     * @param marked the marked to set
     */
    @Override
    public void setMarked(boolean marked) {
        this.marked = marked;
    }
}
