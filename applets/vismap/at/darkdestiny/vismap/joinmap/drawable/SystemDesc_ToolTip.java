/**
 *
 */
package at.darkdestiny.vismap.joinmap.drawable;

import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.*;


/**
 * @author martin
 *
 */
public class SystemDesc_ToolTip {

    private int x;
    private int y;
    public SystemDesc sd;
    private int width;
    private int height;
    /**
     * @param y
     * @param x
     * @param sd
     *
     */
    public SystemDesc_ToolTip(SystemDesc sd, int x, int y, int globalWidth, int globalHeight) {
        this.x = x;
        this.y = y;
        this.sd = sd;


    }
        public SystemDesc_ToolTip(SystemDesc sd, int x, int y) {
        this.x = x;
        this.y = y;
        this.sd = sd;
    }
    public void paint(IAppletData appletData, Graphics g) {

        int rectWidth = 20;
        int rectHeight = 20;
        String sysName = "System "+sd.getName()+" ("+sd.getId()+":x)";
        int longestLine = g.getFontMetrics().stringWidth(sysName);

        Font normalFont = g.getFont();
        Font boldFont = new Font(normalFont.getFontName(),Font.BOLD,normalFont.getSize());



        // Loop through all textLines and determine longest line
        g.setFont(normalFont);
        int w = longestLine;
        this.width = w;

        g.setFont(boldFont);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, w+rectWidth, rectHeight);
        g.setColor(Color.black);
        g.drawRect(x, y, w+rectWidth, rectHeight);

        g.drawString(sysName, x+10, y+15);

        g.setFont(normalFont);


    }

    public void setPos(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
