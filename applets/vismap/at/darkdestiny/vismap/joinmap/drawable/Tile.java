/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.joinmap.drawable;

import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.logic.ML;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Locale;

/**
 *
 * @author Admin
 */
public class Tile extends Object_TwoCoordinates {

    private int startX;
    private int startY;
    private int endX;
    private int endY;
    private final int systems;
    private final int popValue;
    private final int userValue;
    private final int intensity;
    private final int sysCount;
    private final int maxSysCount;
    private int colSysCount;
    private final int maxSysCountPerTile;
    private final int tileWidth;
    private final int tileHeight;
    private final int startSysCount;
    private boolean hoover;
    private static final char[] alphabet = new char[26];

    static {

        char a = 65; //erster Buchstabe (gro�es A)
        for (int i = 0; i < alphabet.length; i++, a++) {
            alphabet[i] = a;
        }
    }

    public Tile(int x1, int y1, int x2, int y2, int systems, int popValue, int userValue, int intensity, int sysCount, int colSysCount, int maxSysCount, int maxSysCountPerTile, int startSysCount) {
        super(0, x1, y1, x2, y2);
        this.systems = systems;
        this.popValue = popValue;
        this.userValue = userValue;
        this.intensity = intensity;
        this.sysCount = sysCount;
        this.maxSysCount = maxSysCount;
        this.colSysCount = colSysCount;
        this.maxSysCountPerTile = maxSysCountPerTile;
        this.tileWidth = y2 - y1;
        this.tileHeight = x2 - x1;
        this.startSysCount = startSysCount;
    }

    public void draw(IAppletData appletData, Graphics gc, float[] delta) {

        int totalDensity = 0;
        boolean drawDetails = true;
        Color yellow;
        if (isHoover()) {
            Graphics2D g2 = (Graphics2D) gc;
            g2.setStroke(new BasicStroke(5));
            if (startSysCount > 0) {
                gc.setColor(Color.GREEN);
            } else {
                gc.setColor(Color.RED);
            }
        } else {
            Graphics2D g2 = (Graphics2D) gc;
            g2.setStroke(new BasicStroke(1));
            gc.setColor(Color.GRAY);
        }

        Graphics2D g2 = (Graphics2D) gc;
        g2.setStroke(new BasicStroke(1));

        gc.drawRect(Math.round(this.actCoord_x),
                Math.round(this.actCoord_y),
                Math.round(getTileWidth() * delta[0]),
                Math.round(getTileHeight() * delta[1]));


        if (getSystems() != 0 && getPopValue() > 0) {
            int popDensityValue = (int) Math.ceil(255d / 10d * getPopValue());


            int scoreDensityValue = scoreDensityValue = (int) Math.ceil(getUserValue() / 1000d);
            if (scoreDensityValue > 255) {
                scoreDensityValue = 255;
            }

            totalDensity = (int) (Math.floor((popDensityValue + scoreDensityValue)));
            totalDensity = getIntensity();

            int sRGBValue = 0;
            sRGBValue += ((int) (totalDensity & 0xFF)) << 24; // alpha
            sRGBValue += ((int) (255 & 0xFF)) << 16; // rot
            sRGBValue += ((int) (255 & 0xFF)) << 8; // gr�n
            sRGBValue += ((int) (0 & 0xFF)); // blau

            yellow = new Color(sRGBValue, true);
            gc.setColor(yellow);
            if (totalDensity != 255) {
                gc.fillRect(Math.round(this.actCoord_x),
                        Math.round(this.actCoord_y),
                        Math.round(getTileWidth() * delta[0]),
                        Math.round(getTileHeight() * delta[1]));
            }
        }
        int offsetX = (int) (5 * delta[0]);
        int offsetCoordinate = (int) (15 * delta[0]);
        gc.setFont(new Font("Tahoma", Font.BOLD, (int) (9 * delta[0])));
        gc.drawString("Quadrant " + getCoordinates(), this.actCoord_x + offsetX, this.actCoord_y + offsetCoordinate);

        if (drawDetails) {
            if (this.getSysCount() > 0) {
                double density = (double) this.getSysCount() / (double) this.getMaxSysCountPerTile();
                if (totalDensity == 255) {

                    gc.setColor(Color.BLACK);
                } else {
                    gc.setColor(Color.WHITE);
                }
                int fontYOffset = (int) (20 * delta[0]);
                int offsetY = fontYOffset + (int) (15 * delta[0]);
                int offsetY2 = fontYOffset + (int) (25 * delta[0]);
                int offsetY3 = fontYOffset + (int) (35 * delta[0]);
                int offsetY4 = fontYOffset + (int) (40 * delta[0]);
                int offsetY5 = fontYOffset + (int) (55 * delta[0]);
                int offsetY6 = fontYOffset + (int) (60 * delta[0]);


                String label = String.valueOf(this.getSysCount()) + " " + ML.getMLStr("lbl_solar_1", appletData.getLocale()) + countToName(getSysCount(), appletData.getLocale());
                gc.drawString(label, this.actCoord_x + offsetX, this.actCoord_y + offsetY);
                if (getStartSysCount() > 0) {
                    gc.setColor(Color.GREEN);
                    label = String.valueOf(this.getStartSysCount()) + " " + ML.getMLStr("lbl_start_1", appletData.getLocale()) + countToName(getStartSysCount(), appletData.getLocale());
                    gc.drawString(label, this.actCoord_x + offsetX, this.actCoord_y + offsetY2);
                }

                int barXOffset = 10;


                int maxBarLength = getTileWidth() - barXOffset * 2;

                gc.setColor(Color.WHITE);
                double barWidth = Math.round((double) maxBarLength * density);
                gc.fillRect(Math.round(this.actCoord_x + barXOffset),
                        Math.round(this.actCoord_y + offsetY4),
                        Math.round((int) barWidth * delta[0]),
                        Math.round(5 * delta[1]));
                gc.setColor(Color.BLACK);

                gc.drawRect(Math.round(this.actCoord_x + barXOffset),
                        Math.round(this.actCoord_y + offsetY4),
                        Math.round((int) maxBarLength * delta[0]),
                        Math.round(5 * delta[1]));
                gc.setColor(Color.WHITE);
                gc.drawString(ML.getMLStr("lbl_stardensity", appletData.getLocale()) + ": " + String.valueOf((int) (density * 100)) + "%", this.actCoord_x + offsetX, this.actCoord_y + offsetY3);

                if (this.getColSysCount() > 0) {
                    gc.setColor(Color.GREEN);
                    density = (double) this.getColSysCount() / (double) this.getSysCount();

                    barWidth = Math.round((double) maxBarLength * density);
                    gc.fillRect(Math.round(this.actCoord_x + barXOffset),
                            Math.round(this.actCoord_y + offsetY6),
                            Math.round((int) barWidth * delta[0]),
                            Math.round(5 * delta[1]));

                gc.setColor(Color.BLACK);

                gc.drawRect(Math.round(this.actCoord_x + barXOffset),
                        Math.round(this.actCoord_y + offsetY6),
                        Math.round((int) maxBarLength * delta[0]),
                        Math.round(5 * delta[1]));
                gc.setColor(Color.GREEN);
                    gc.drawString(ML.getMLStr("lbl_colonisation", appletData.getLocale()) + ": " + String.valueOf((int) (density * 100)) + "%", this.actCoord_x + offsetX, this.actCoord_y + offsetY5);
                }

            }

        }

    }

    public String getCoordinates() {
        return coordinatesToLiteral(startX, startY, this.getX1(), this.getY1(), tileWidth);
    }

    public String coordinatesToLiteral(int startX, int startY, int x, int y, int tileSize) {
        String result = "";
        int xNormalized = x - startX;
        int yNormalized = y - startY;

        int xValue = xNormalized / tileSize;
        int yValue = yNormalized / tileSize;
        result += coordinateToLetter(xValue) + "/" + coordinateToLetter(yValue);

        return result;
    }

    public String coordinateToLetter(int value) {
        String result = "";
        int counter = value;
        while (counter > alphabet.length) {
            result += alphabet[value % alphabet.length];
            counter -= alphabet.length;
        }
        if (counter > 0) {
            result += alphabet[value % alphabet.length];
        }

        return result;
    }

    public String countToName(int systemCount, Locale l) {
        if (systemCount == 1) {
            return ML.getMLStr("lbl_system_2", l);
        } else {
            return ML.getMLStr("lbl_systems_2", l);
        }
    }

    @Override
    public String getIdentifier() {
        return "Tile " + "(" + getX1() + "/" + this.getY1() + ")";
    }

    public void setHoover(boolean b) {
        this.hoover = b;
    }

    /**
     * @return the startX
     */
    public int getStartX() {
        return startX;
    }

    /**
     * @return the startY
     */
    public int getStartY() {
        return startY;
    }

    /**
     * @return the endX
     */
    public int getEndX() {
        return endX;
    }

    /**
     * @return the endY
     */
    public int getEndY() {
        return endY;
    }

    /**
     * @return the systems
     */
    public int getSystems() {
        return systems;
    }

    /**
     * @return the popValue
     */
    public int getPopValue() {
        return popValue;
    }

    /**
     * @return the userValue
     */
    public int getUserValue() {
        return userValue;
    }

    /**
     * @return the intensity
     */
    public int getIntensity() {
        return intensity;
    }

    /**
     * @return the sysCount
     */
    public int getSysCount() {
        return sysCount;
    }

    /**
     * @return the maxSysCount
     */
    public int getMaxSysCount() {
        return maxSysCount;
    }

    /**
     * @return the colSysCount
     */
    public int getColSysCount() {
        return colSysCount;
    }

    /**
     * @param colSysCount the colSysCount to set
     */
    public void setColSysCount(int colSysCount) {
        this.colSysCount = colSysCount;
    }

    /**
     * @return the maxSysCountPerTile
     */
    public int getMaxSysCountPerTile() {
        return maxSysCountPerTile;
    }

    /**
     * @return the tileWidth
     */
    public int getTileWidth() {
        return tileWidth;
    }

    /**
     * @return the tileHeight
     */
    public int getTileHeight() {
        return tileHeight;
    }

    /**
     * @return the startSysCount
     */
    public int getStartSysCount() {
        return startSysCount;
    }

    /**
     * @return the hoover
     */
    public boolean isHoover() {
        return hoover;
    }
}
