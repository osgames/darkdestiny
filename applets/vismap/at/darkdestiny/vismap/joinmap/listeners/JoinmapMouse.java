/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.joinmap.listeners;

import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.joinmap.DataLoader;
import at.darkdestiny.vismap.joinmap.drawable.SystemDesc_ToolTip;
import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AbstractMouse;
import at.darkdestiny.vismap.joinmap.drawable.SystemDesc;
import at.darkdestiny.vismap.joinmap.drawable.Tile;
import at.darkdestiny.vismap.logic.ML;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Admin
 */
public class JoinmapMouse extends AbstractMouse {

    private IPainterData painterData;
    private at.darkdestiny.vismap.joinmap.Painter painter;
    private AppletMain main;
    private IAppletData appletData;
    int lastMousePressedX = 0;
    int lastMousePressedY = 0;
    boolean mouseDragged = false;
    boolean mousePressed = true;
    float x_start = Float.NaN;
    float y_start = Float.NaN;
    private int tmpx;
    private int tmpy;
    private int x_int;
    private int y_int;
    Tile lastTile;
    private static final int TOOLTIP_HEIGHT = 10;
    private static final int TOOLTIP_WIDTH = 10;

    public JoinmapMouse(AppletMain main, IPainter painter, IPainterData painterData) {
        this.painterData = painterData;
        this.painter = (at.darkdestiny.vismap.joinmap.Painter) painter;
        this.main = main;
        this.appletData = (at.darkdestiny.vismap.joinmap.AppletData)main.getAppletData();

    }

    public void mouseWheelMoved(MouseWheelEvent e) {

        int zoom = e.getWheelRotation() * 1;

        painter.mouseWheelChanged(zoom, e);

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        tmpx = e.getX();
        tmpy = e.getY();
        if (Math.abs(tmpx - x_int) > 2 && Math.abs(tmpy - y_int) > 2) {
            active = true;
            painter.forceRefresh();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        SystemDesc sd = findSystem(e.getX(), e.getY());
        if (sd != null) {
            if ((painter.showToolTipFor == null) || (painter.showToolTipFor.sd != sd)) {
                painter.showToolTipFor = new SystemDesc_ToolTip(sd, e.getX(), e.getY(), painterData.getWidth(), painterData.getHeight());
                painter.repaint();
            } else {
                painter.showToolTipFor.setPos(e.getX(), e.getY());
                painter.repaint();
            }

        } else if (painter.showToolTipFor != null) {
            painter.showToolTipFor = null;
            painter.repaint();
        }
        Tile tile = findTiles(e.getX(), e.getY());

        if (tile != null && tile != lastTile) {
            lastTile = tile;
            painter.forceRefresh();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        if ((lastMousePressedX == e.getX()) && (lastMousePressedY == e.getY())) {

            {
                SystemDesc sd = findSystem(e.getX(), e.getY());

                if ((sd != null) && (sd.getSystemStatus() == 1)) {
                    Frame f = new Frame("Frame");
                    Object[] options = {"Ja",
                        "Nein"
                    };
                    int n = JOptionPane.showOptionDialog(f,
                            "M�chten sie " + sd.getName() + " als Startsystem w�hlen?",
                            "Startsystem w�hlen",
                            JOptionPane.YES_NO_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[1]);

                    f.dispose();

                    if (n == 0) {
                        try {
                            AppletMain.getApplet().getAppletContext().showDocument(new URL(DataLoader.getBasePath() + "chooseStartLoc.jsp?systemId=" + sd.getId()));
                        } catch (MalformedURLException mfue) {
                            AppletMain.getApplet().showStatus("URL NOT FOUND! " + mfue.getMessage());
                        }
                    }

                }
            }

            if (lastTile != null) {
                if (lastTile.getStartSysCount() > 0) {
                    Frame f = new Frame("Frame");
                    Object[] options = {ML.getMLStr("but_YES", appletData.getLocale()),
                        ML.getMLStr("but_NO", appletData.getLocale())
                    };
                    int n = JOptionPane.showOptionDialog(f,
                            ML.getMLStr("pop_choosestartquadrant", appletData.getLocale()).replace("%1", lastTile.getCoordinates()),
                            ML.getMLStr("pop_title_choosestartquadrant", appletData.getLocale()),
                            JOptionPane.YES_NO_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[1]);

                    f.dispose();

                    if (n == 0) {
                        try {
                            AppletMain.getApplet().getAppletContext().showDocument(new URL(DataLoader.getBasePath() + "chooseStartLocByTile.jsp?tileCoordinateX=" + lastTile.getX1() + "&tileCoordinateY=" + lastTile.getY1() + "&JSESSIONID=" + appletData.getSessionId()));
                        } catch (MalformedURLException mfue) {
                            AppletMain.getApplet().showStatus("URL NOT FOUND! " + mfue.getMessage());
                        }
                    }
                } else {
                    Frame f = new Frame("Frame");
                    Object[] options = {
                            ML.getMLStr("but_OK", appletData.getLocale())
                    };
                    int n = JOptionPane.showOptionDialog(f,
                            ML.getMLStr("pop_nostartsystem", appletData.getLocale()),
                            ML.getMLStr("pop_nostartsystem", appletData.getLocale()),
                            JOptionPane.CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[0]);

                    f.dispose();
                }
            }
        }
        float tmpx = painter.convertToFloatX(e.getX(), e.getY());
        float tmpy = painter.convertToFloatY(e.getX(), e.getY());


        if (Math.abs(x_int - e.getX()) > 5 && Math.abs(y_int - e.getY()) > 5) {
            painter.zoomRect(x_start, y_start, tmpx, tmpy);
        }
        x_start = Float.NaN;
        y_start = Float.NaN;


        active = false;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mousePressed = true;
        lastMousePressedX = e.getX();
        lastMousePressedY = e.getY();
        x_start = painter.convertToFloatX(e.getX(), e.getY());
        y_start = painter.convertToFloatY(e.getX(), e.getY());
        x_int = e.getX();
        y_int = e.getY();
    }

    protected SystemDesc findSystem(int x, int y) {
        float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 16 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 16);

        SystemDesc res = null;
        float priority = Float.NEGATIVE_INFINITY;
        List<I_Object_OneCoordinate> entries = painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_SYSTEMS);

        if (entries != null) {
            for (I_Object_OneCoordinate obj : entries) {
                Object_OneCoordinate sd = (Object_OneCoordinate) obj;

                float dist = (float) Math.sqrt(Math.pow(x - sd.getActCoord_x1(), 2d) + Math.pow(y - sd.getActCoord_y1(), 2d));

                if ((sd instanceof SystemDesc) && (dist < maxdist)) {

                    SystemDesc sys = (SystemDesc) sd;
                    if (sys.getPriority() > priority) {
                        priority = sys.getPriority();
                        res = sys;
                    }
                }
            }
        }
        return res;
    }

    protected Tile findTiles(int x, int y) {
        List<I_Object_OneCoordinate> entries = painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_TILES);

        Tile result = null;
        if (entries != null) {
            for (I_Object_OneCoordinate obj : entries) {

                Tile t = (Tile) obj;
                Rectangle r = new Rectangle(t.getActCoord_x1(), t.getActCoord_y1(), t.getActCoord_x2() - t.getActCoord_x1(), t.getActCoord_y2() - t.getActCoord_y1());
                if (r.contains(x, y)) {
                    t.setHoover(true);
                    result = t;

                } else {
                    t.setHoover(false);
                }
            }
        }
        return result;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void paint(IPainter painter, Graphics g) {

        int w = Math.abs(tmpx - x_int);
        int h = Math.abs(tmpy - y_int);
        g.setColor(new Color(0.3f, 0.3f, 0.8f, 0.3f));
        g.fillRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);

        g.setColor(new Color(0.3f, 0.3f, 1f));
        g.drawRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);
    }

    @Override
    public void update(AppletMain main, MainPainter aThis, IPainterData painterData) {
        this.painterData = painterData;
        this.painter = (at.darkdestiny.vismap.joinmap.Painter) painter;
        this.main = main;

    }
}