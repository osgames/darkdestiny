/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.joinmap;

import at.darkdestiny.vismap.dto.AbstractPainterData;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class PainterData extends AbstractPainterData {

    private final Map<Integer, SystemDesc> systems;
    boolean showTiles = true;
 
    public void switchShowTiles(){

        this.showTiles = !this.showTiles;
    }
    public PainterData() {
        systems = new HashMap();
    }

 

    public void addSystem(int id, SystemDesc sd) {
        systems.put(id, sd);
    }

 
    public SystemDesc getSystem(int id) {
        return systems.get(id);
    }

}
