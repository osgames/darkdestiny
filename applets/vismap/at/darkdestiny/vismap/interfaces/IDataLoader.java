/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.interfaces;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.gui.AppletMain;

/**
 *
 * @author Admin
 */
public interface IDataLoader {
    public IPainterData getPainterData();
    public IAppletData getAppletData();
    public void init();
    public void reload(AppletMain main);
}
