/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.interfaces;

import java.util.List;
import javax.swing.JComponent;
import javax.swing.SpringLayout;

/**
 *
 * @author Admin
 */
public interface IButtonFactory {
    
    public abstract void setPainter(IPainter painter);
    public abstract List<JComponent> getButtons(SpringLayout layout, JComponent parent);
}
