/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.interfaces;

/**
 *
 * @author HorstRabe
 */
 public interface I_InformationPanel {

    /**
     * 
     * 
     * @return An unique identifier which identifys the Panel
     */
     
    String getIdentifier();
   
}
