package at.darkdestiny.vismap.interfaces;

import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.Graphics;
import javax.swing.JPanel;
import at.darkdestiny.vismap.gui.MainPainter;

/**
 *
 * @author Horst
 */
public interface I_Object_OneCoordinate {

    abstract String getIdentifier();
    abstract JPanel getPanel();

    abstract int getActCoord_x1();
    abstract int getActCoord_y1();
    abstract void setActCoord_x1(int actCoord_x);
    abstract void setActCoord_y1(int actCoord_y);
    abstract void init(MainPainter painter);

    abstract void draw(IAppletData appletData, Graphics gc, float[] delta);

    abstract boolean isHide();
    abstract void setHide(boolean hide);

    abstract void setMarked(boolean marked);
    abstract boolean isMarked();

    public int getId();

    public void setId(int id);
}
