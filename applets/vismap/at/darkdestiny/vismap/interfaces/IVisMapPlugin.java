/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.interfaces;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.logic.AbstractMouse;
import java.util.List;
import javax.swing.JLayeredPane;

/**
 *
 * @author Admin
 */
public interface IVisMapPlugin {

    public IDataLoader getDataLoader(AppletMain main, IAppletData appletData, IPainterData painterData, LoaderScreen l);

    public IPainter getPainter(AppletMain main);

    public List<String> getImages();

    public AbstractMouse getMouse(AppletMain main, IPainter p, IPainterData painterData);
    public AbstractMouse getMouse();
    public void addPanels(JLayeredPane panel, AppletMain appletMain, IPainter painter);
}
