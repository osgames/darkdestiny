/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.interfaces;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import javax.swing.JPanel;

/**
 *
 * @author Admin
 */
public interface IPainter {
    public void revalidate();
    public void repaint();
    public void forceRefresh();
    public void moveImage(String direction, float weight);
    public void setPainterData(IPainterData painterData);
    public void setAppletData(IAppletData appletData);
    public IPainterData getPainterData();
    public JPanel getPanel();
    public EStarmapMode getMode();
    public void setMode(EStarmapMode mode);
    public void zoomIn();
    public void zoomOut();
    public void setDefaultCursor(boolean defaultCursor);
    public void setRasterDetail(int size);
    public int getRasterDetail();
    public void printBondarys();
}
