/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.logic;

import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.IPainter;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;

/**
 *
 * @author Admin
 */
public abstract class AbstractMouse implements MouseWheelListener, MouseMotionListener, MouseListener{

    public MouseEvent mouseEvent;
    public boolean active;
    public abstract void paint(IPainter painter, Graphics g);

    public abstract void update(AppletMain main, MainPainter aThis, IPainterData painterData);


    private boolean ctrlPressed = false;
    private boolean altPressed = false;
    /**
     * @return the ctrlPressed
     */
    public boolean isCtrlPressed() {
        return ctrlPressed;
    }

    /**
     * @param ctrlPressed the ctrlPressed to set
     */
    public void setCtrlPressed(boolean ctrlPressed) {
        this.ctrlPressed = ctrlPressed;
    }

    /**
     * @return the altPressed
     */
    public boolean isAltPressed() {
        return altPressed;
    }

    /**
     * @param altPressed the altPressed to set
     */
    public void setAltPressed(boolean altPressed) {
        this.altPressed = altPressed;
    }

}
