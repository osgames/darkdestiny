/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.logic;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.gui.MainPanel;
import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.enumerations.EVismapType;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.IDataLoader;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.IVisMapPlugin;
import java.awt.Dimension;
import at.darkdestiny.vismap.starmap.panels.system.Utilities;

/**
 *
 * @author Aion
 */
public class LoadThread extends Thread {

    AppletMain am;
    LoaderScreen loader;
    public int timer = 3;
    public boolean error = false;
    IDataLoader dataLoader;
    IPainterData painterData;
    IAppletData appletData;
    IVisMapPlugin plugin;

    public LoadThread(AppletMain am, IVisMapPlugin plugin, EVismapType vismapType, boolean admin) {
        this.am = am;
        loader = am.getLoader();
        //Initialize Data Classes
        if (vismapType.equals(EVismapType.JOINMAP)) {
            this.appletData = new at.darkdestiny.vismap.joinmap.AppletData();
            this.painterData = new at.darkdestiny.vismap.joinmap.PainterData();
        } else if (vismapType.equals(EVismapType.STARMAP)) {
            this.appletData = new at.darkdestiny.vismap.starmap.AppletData();
            this.painterData = new at.darkdestiny.vismap.starmap.PainterData();
        } else if (vismapType.equals(EVismapType.TECHTREE)) {
            this.appletData = new at.darkdestiny.vismap.techtree.AppletData();
            this.painterData = new at.darkdestiny.vismap.techtree.PainterData();
        } else if (vismapType.equals(EVismapType.PLANETLOGVIEWER)) {
            this.appletData = new at.darkdestiny.vismap.planetlogviewer.AppletData();
            this.painterData = new at.darkdestiny.vismap.planetlogviewer.PainterData();
        }
        this.appletData.setAdmin(admin);

        appletData.setVismapType(vismapType);
        this.plugin = plugin;
    }

    @Override
    public void run() {

        loader.addLine("VismapType is: " + appletData.getVismapType().toString());
        dataLoader = plugin.getDataLoader(am, appletData, painterData, loader);
        /* if (appletData.isJoinMode()) {
         dataLoader = new at.viswars.vismap.joinmap.DataLoader(am, appletData, painterData, am.getHeight(), am.getWidth(), loader);
         } else {
         dataLoader = new at.viswars.vismap.starmap.DataLoader(am, appletData, painterData, am.getHeight(), am.getWidth(), loader);
         }*/

        loader.addLine("Using: " + dataLoader.getClass().getName());
        DisplayManagement.setAppletMain(am);

        am.setAppletData(appletData);
        IPainter painter = plugin.getPainter(am);
        if (painter == null) {
            System.exit(1);
        }
        painter.setPainterData(painterData);
        painter.setAppletData(appletData);

        am.mainFrame = new MainPanel(painter, new Dimension((int) am.getPreferredSize().getWidth(), (int) am.getPreferredSize().getHeight()), am);

        am.mainFrame.setPainter(painter);

        dataLoader.init();

        if (!error) {

            loader.addLine("Loading Images - Start");
            long t1 = System.currentTimeMillis();

            for (String image : plugin.getImages()) {
                try {
                    DisplayManagement.bufferGraphic(image);
                } catch (Exception e) {
                    System.out.println("Error while loading image: " + image + " : " + e);
                }
            }
            loader.addLine("Loading Images - Done in " + (System.currentTimeMillis() - t1) + " ms");

            loader.addLine("Initializing Glossar");
            Utilities.setMain(am);

            loader.addLine("Initializing Painter");

            /**
             * @informationScrollPane Detailed Information to following Entitys
             * -Traderoutes -Systems -Fleets(Design/Shipping)
             */
            //Adding Components to Panel (ControlPanel, Buttons, Informationpanel, Painter)
            //  loader.addLine("Adding Components - Start");
            for (int i = timer; i >= 0; i--) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
                loader.addLine("Building UI in : " + i + " seconds");
            }
            //Close LoaderScreen
            am.getLoader().setVisible(false);
            am.initialize();
            am.reValidate();
            am.requestFocus();
        }

    }
}
