/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.logic;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class PlanetToColor {
    
    private static final Map<String, Color> colors;
    
    static{
        colors = new HashMap<String, Color>();
        colors.put("A", new Color(0.549f, 0.498f, 0.141f));
        colors.put("B", new Color(0f, 0.718f, 0.894f));
        colors.put("J", new Color(0.494f, 0f, 0f));
        colors.put("G", new Color(0.992f, 0.992f, 0.004f));
        colors.put("E", new Color(1f, 1f, 1f));
        colors.put("M", new Color(0.086f, 0.314f, 0f));
        colors.put("L", new Color(1f, 1f, 1f));
        colors.put("C", new Color(0.129f, 0.259f, 0.216f));
    }
    
    public static Color getColor(String planetType){
        return colors.get(planetType);
    }
}
