/*
 * AppletConstants.java
 *
 * Created on 19. Dezember 2006, 13:31
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.logic;

import java.awt.Color;

/**
 *
 * @author Morgenstern
 */
public class AppletConstants {

    public static final String FILE_PATH = "C:\\xml\\";

    public static final int SYSTEM_STATUS_000 = 0;
    public static final int SYSTEM_STATUS_001 = 1;
    public static final int System_STATUS_010 = 2;
    public static final int SYSTEM_STATUS_011 = 3;
    public static final int SYSTEM_STATUS_100 = 4;
    public static final int System_STATUS_101 = 5;
    public static final int System_STATUS_110 = 6;
    public static final int System_STATUS_111 = 7;
    public static final int DRAW_LEVEL_SYSTEMS = 50;
    public static final int DRAW_LEVEL_SYSTEMS_OVALS = 30;
    //Joinmap specific
    public static final int DRAW_LEVEL_TILES = 20;
    public static final int DRAW_LEVEL_GALAXY = 10;
    //starmap specific
    public static final int DRAW_LEVEL_TERRITORY = 28;
    public static final int DRAW_LEVEL_TECHRELATION = 250;
    public static final int DRAW_LEVEL_RESEARCH=  300;
    public static final int DRAW_LEVEL_FLEETS_TRIANGLES = 40;
    public static final int DRAW_LEVEL_RANGE_VIEW_ENTRY = 19;
    public static final int DRAW_LEVEL_FLEETS = 80;
    public static final int DRAW_LEVEL_TRADE = 70;
    public static final int DRAW_LEVEL_TRANSMITTER = 75;
    public static final int DRAW_LEVEL_OBSERVATORY = 20;
    public static final int DRAW_LEVEL_HYPERSCANNER = 21;
    public static final String FLEET_IMAGE_ENEMY = "Enemy_Fleet";
    public static final String FLEET_IMAGE_OWN = "Own_Fleet";
    public static final String FLEET_IMAGE_ALLIED = "Allied_Fleet";
    public static int UNIVERSE_SIZE_X = 4000;
    public static int UNIVERSE_SIZE_Y = 4000;
    public static float MAXXDIFF = 2000 * 2 * 4;
    public static float MAXYDIFF = 2000 * 2 * 4;
    public static final Color FLEET_COLOR_ENEMY = Color.RED;
    public static final Color FLEET_COLOR_OWN = Color.GREEN;
    public static final Color FLEET_COLOR_ALLIED = Color.BLUE;
    public static final Color FLEET_COLOR_FLEETFORMATION = Color.YELLOW;
    public static final int RASTER_STANDARD_SIZE = 500;
    public static final int RASTER_MAXIMUM_SIZE = 5000;
    public static final int RASTER_MINIMUM_SIZE = 50;
    public static final int FLEET_ENEMY = 3;
    public static final int FLEET_OWN = 1;
    public static final int FLEET_ALLIED = 2;
    public static final String ID_INFORMATIONPANEL_TRADE = "TRADEROUTE ";
    public static final int ID_INFORMATIONPANEL_SYSTEM = 1;
    public static final String ID_INFORMATIONPANEL_FLEET = "Fleet ";
    public static final String ID_INFORMATIONPANEL_HYPERSCANNER = "Hyperscanner ";
    public static final String LINK_GO_SYSTEM = "main.jsp?page=new/system&systemId=";
    public static final String LINK_GO_PLANET = "selectview.jsp?showPlanet=";
    public static final String LINK_MOVE_FLEET_SYSTEM = "main.jsp?page=sendfleet&systemId=";
    public static final String LINK_MOVE_FLEET_PLANET = "main.jsp?page=sendfleet&planetId=";
    public static final String LINK_TRADEROUTE_CANCEL = "main.jsp?page=trade&type=4&action=1&routeId=";
    public static final int HYPERSCANNER_RANGE = 200;
}
