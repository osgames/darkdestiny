package at.darkdestiny.vismap.logic;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.gui.AppletMain;

/**
 *
 * @author Aion
 */
public class ReloadThread extends Thread{

    AppletMain am;
    LoaderScreen loader;
    public ReloadThread(AppletMain am){
        this.am = am;
        loader = am.getLoader();
        loader.clearText();
    }
    @Override
    public void run(){
        am.reload();
        am.setInitialized(true);
        am.setReloaded(true);
        loader.setVisible(false);
        am.mainFrame.getPainter().forceRefresh();
    }
}
