/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.logic;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author Eobane
 */
public class ML {

    private static Map<String, Map<String, String>> lang = new HashMap();
    private static String bundleName = "build/classes/Bundle";
    private static String defaultLang = "de_de";
    private static boolean init = true;

    private static void init() {
        Map<String, String> ger = new HashMap();
        ger.put("lbl_showraster", "Raster Anzeigen");
        ger.put("lbl_invertraster", "Raster Invertieren");
        ger.put("lbl_showtraderoutes", "Handelsrouten anzeigen");
        ger.put("lbl_showtyperscanner", "Hyperraumscanner anzeigen");
        ger.put("lbl_showfleets", "Flotten anzeigen");
        ger.put("lbl_informationpanel", "Informationpanel");
        ger.put("lbl_systemid", "System Id");
        ger.put("lbl_systemname", "System Name");
        ger.put("lbl_planetid", "Planet Id");
        ger.put("lbl_planetname", "Planet Name");
        ger.put("lbl_coordinates", "Koord.");
        ger.put("lbl_searchfor", "Suche nach");
        ger.put("lbl_noowner", "Kein Besitzer");
        ger.put("but_close", "Schlie�en");
        ger.put("pop_nostartsystem", "In diesem Quadranten ist kein Startsystem vorhanden");
        ger.put("pop_title_nostartsystem", "Kein Startsystem");
        ger.put("but_OK", "OK");
        ger.put("but_YES", "Ja");
        ger.put("but_NO", "Nein");
        ger.put("pop_title_choosestartquadrant", "Startquadrant w�hlen");
        ger.put("pop_choosestartquadrant", "M�chten sie %1 als Startquadrant w�hlen?");
        ger.put("lbl_stardensity", "Sternendichte");
        ger.put("lbl_colonisation", "Kolonisation");
        ger.put("lbl_solar_1", "Sonnen");
        ger.put("lbl_start_1", "Start");
        ger.put("lbl_system_2", "system");
        ger.put("lbl_systems_2", "systems");
        ger.put("lbl_research", "Forschung");
        ger.put("lbl_abbr_researchpoints", "FP");
        ger.put("lbl_abbr_computerresearchpoints", "CFP");
        ger.put("lbl_neededrp", "Forschungspunkte");
        ger.put("but_close", "Schlie�en");
        ger.put("lbl_researches", "Forschungen");
        ger.put("lbl_alreadyresearched", "Bereits Erforscht");
        ger.put("lbl_researchable", "Erforschbar");
        ger.put("lbl_inresearch", "In Forschung");
        ger.put("lbl_notresearchable", "Nicht Erforschbar");
        ger.put("lbl_icons", "Icons");
        ger.put("lbl_searchresearch", "Forschung suchen");
        ger.put("lbl_searchpreresearches", "Vorforschungen suchen");
        ger.put("lbl_searchpreresearches_hideothers1", "Vor-Forschungen suchen");
        ger.put("lbl_searchpreresearches_hideothers2", "andere ausblenden");
        ger.put("lbl_showresearchlist", "Forschungsliste anzeigen");
        ger.put("lbl_resetsearch", "Suche zur�cksetzen");
        ger.put("lbl_help", "Hilfe");
        ger.put("lbl_searchforresearch", "Suche nach Forschung");
        ger.put("lbl_unlockedtech", "Freigeschaltete Technologien");
        ger.put("lbl_armor", "Panzerung");
        ger.put("lbl_weapon", "Waffe (Modul)");
        ger.put("lbl_construction", "Geb�ude");
        ger.put("lbl_shield", "Schild (Modul)");
        ger.put("lbl_module", "Modul");
        ger.put("lbl_groundtroop", "Bodentruppen");
        ger.put("lbl_chassis", "Chassis");
        ger.put("lbl_technologies", "Technologien");
        lang.put("de_DE", ger);

        Map<String, String> eng = new HashMap<String, String>();

        eng.put("lbl_showraster", "Show Raster");
        eng.put("lbl_invertraster", "Invert Raster");
        eng.put("lbl_showtraderoutes", "Show Traderoutes");
        eng.put("lbl_showtyperscanner", "Show Hyperspacescanner");
        eng.put("lbl_showfleets", "Show Fleets");
        eng.put("lbl_informationpanel", "Informationpanel");
        eng.put("lbl_systemid", "System Id");
        eng.put("lbl_systemname", "System Name");
        eng.put("lbl_planetid", "Planet Id");
        eng.put("lbl_planetname", "Planet Name");
        eng.put("lbl_coordinates", "Coordinates");
        eng.put("lbl_searchfor", "Search for");
        eng.put("lbl_noowner", "No Owner");
        eng.put("but_close", "Close");
        eng.put("pop_nostartsystem", "There is no start system in this quadrant");
        eng.put("pop_title_nostartsystem", "No startsystem");
        eng.put("but_OK", "OK");
        eng.put("but_YES", "Yes");
        eng.put("but_NO", "No");
        eng.put("pop_title_choosestartquadrant", "Choose start quadrant");
        eng.put("pop_choosestartquadrant", "Would you like %1 as start quadrant?");
        eng.put("lbl_stardensity", "Stardensity");
        eng.put("lbl_colonisation", "Colonization");
        eng.put("lbl_solar_1", "Solar");
        eng.put("lbl_start_1", "Start");
        eng.put("lbl_system_2", "system");
        eng.put("lbl_systems_2", "systems");eng.put("lbl_research", "Research");
        eng.put("lbl_abbr_researchpoints", "RP");
        eng.put("lbl_abbr_computerresearchpoints", "CRP");
        eng.put("lbl_neededrp", "Researchpoints");
        eng.put("but_close", "Close");
        eng.put("lbl_researches", "Researches");
        eng.put("lbl_alreadyresearched", "Already researched");
        eng.put("lbl_researchable", "Researchable");
        eng.put("lbl_inresearch", "In research");
        eng.put("lbl_notresearchable", "Not researchable");
        eng.put("lbl_icons", "Icons");
        eng.put("lbl_searchresearch", "Search Research");
        eng.put("lbl_searchpreresearches", "Search Pre-Researches");
        eng.put("lbl_searchpreresearches_hideothers1", "Search Pre-Researches");
        eng.put("lbl_searchpreresearches_hideothers2", "hide others");
        eng.put("lbl_showresearchlist", "Show Researchlist");
        eng.put("lbl_resetsearch", "Reset Search");
        eng.put("lbl_help", "Help");
        eng.put("lbl_searchforresearch", "Search for Research");
        eng.put("lbl_unlockedtech", "Unlocked Technologies");
        eng.put("lbl_armor", "Armor");
        eng.put("lbl_weapon", "Weapon (Module)");
        eng.put("lbl_construction", "Construction");
        eng.put("lbl_shield", "Shield (Module)");
        eng.put("lbl_module", "Module");
        eng.put("lbl_groundtroop", "Groundtroops");
        eng.put("lbl_chassis", "Chassis");
        eng.put("lbl_technologies", "Technologies");


        eng.put("but_close", "Close");
        lang.put("en_US", eng);
    }

    public static String getMLStr(String key, Locale locale) {
        if (init) {
            init();
            init = false;
        }
        String data = key;
        try {
            data = lang.get(locale.toString()).get(key);
        } catch (Exception e) {
            return "e:" + data;
        }
        return data;
    }
}
