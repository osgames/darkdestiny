/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.dto;

import at.darkdestiny.vismap.enumerations.EVismapType;
import java.util.Locale;

/**
 *
 * @author Admin
 */
public class AbstractAppletData implements IAppletData{

    private Locale locale;
    private EVismapType vismapType;
    private String sessionId;
    private boolean admin;

    /**
     * @return the locale
     */
    @Override
    public Locale getLocale() {
        return locale;
    }

    /**
     * @param locale the locale to set
     */
    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * @return the vismapType
     */
    @Override
    public EVismapType getVismapType() {
        return vismapType;
    }

    /**
     * @param vismapType the vismapType to set
     */
    @Override
    public void setVismapType(EVismapType vismapType) {
        this.vismapType = vismapType;
    }


    @Override
    public void setSessionId(String sessionId) {
      this.sessionId = sessionId;
    }

    /**
     * @return the sessionId
     */
    @Override
    public String getSessionId() {
        return sessionId;
    }

    @Override
    public boolean isAdmin() {
        return this.admin;
    }

    @Override
    public void setAdmin(boolean isAdmin) {
        this.admin = isAdmin;
    }

}
