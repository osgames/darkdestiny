/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.dto;

import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public abstract class AbstractPainterData implements IPainterData{

    //Objects which can be displayed in the information panel
    protected Map<Integer, List<I_Object_OneCoordinate>> displayObjects;
    //Objects drawed
    protected Map<Integer, List<I_Object_OneCoordinate>> drawObjects;

    protected Map<String, I_Object_OneCoordinate> informationData;


    private int width;
    private int height;
    private boolean rasterInverted = false;
    private boolean showRaster = false;


    public AbstractPainterData() {
        displayObjects = new TreeMap();
        drawObjects = new TreeMap();
        informationData = new HashMap();
    }

    @Override
    public void addInformationData(String key, I_Object_OneCoordinate object) {
        getInformationData().put(key, object);
    }


    @Override
    public void addDisplayObject(Integer drawLevel, I_Object_OneCoordinate object) {
        List<I_Object_OneCoordinate> objects = getDisplayObjects().get(drawLevel);
        if (objects == null) {
            objects = new ArrayList();
        }
        objects.add(object);
        getDisplayObjects().put(drawLevel, objects);
    }


    @Override
    public void addDrawObject(Integer drawLevel, I_Object_OneCoordinate object) {
        List<I_Object_OneCoordinate> objects = getDrawObjects().get(drawLevel);
        if (objects == null) {
            objects = new ArrayList();
        }
        objects.add(object);
        getDrawObjects().put(drawLevel, objects);
    }



    /**
     * @return the displayObjects
     */
    @Override
    public Map<Integer, List<I_Object_OneCoordinate>> getDisplayObjects() {
        return displayObjects;
    }

    /**
     * @return the drawObjects
     */
    @Override
    public Map<Integer, List<I_Object_OneCoordinate>> getDrawObjects() {
        return drawObjects;
    }

    /**
     * @return the informationData
     */
    @Override
    public Map<String, I_Object_OneCoordinate> getInformationData() {
        return informationData;
    }
 /**
     * @return the rasterInverted
     */
    @Override
    public boolean isRasterInverted() {
        return rasterInverted;
    }

    /**
     * @param rasterInverted the rasterInverted to set
     */
    @Override
    public void setRasterInverted(boolean rasterInverted) {
        this.rasterInverted = rasterInverted;
    }

    @Override
    public boolean isShowRaster() {
        return showRaster;
    }

    /**
     * @param showRaster the showRaster to set
     */
    @Override
    public void setShowRaster(boolean showRaster) {
        this.showRaster = showRaster;
    }


    /**
     * @return the width
     */
    @Override
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    @Override
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean isContinueOnObject(I_Object_OneCoordinate ob, MainPainter mainPainter){
        return false;
    }

    @Override
    public boolean isContinueOnObject(I_Object_OneCoordinate ob, MainPainter mainPainter, Object data){
        return false;
    }

}
