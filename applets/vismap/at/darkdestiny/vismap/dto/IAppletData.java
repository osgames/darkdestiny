/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.dto;

import at.darkdestiny.vismap.enumerations.EVismapType;
import java.util.Locale;

/**
 *
 * @author Admin
 */
public interface IAppletData {

    /**
     * @return the locale
     */
    public Locale getLocale();

    /**
     * @param locale the locale to set
     */
    public void setLocale(Locale locale);

    /**
     * @return the vismapType
     */
    public EVismapType getVismapType();

    /**
     * @param vismapType the vismapType to set
     */
    public void setVismapType(EVismapType vismapType);


    public void setSessionId(String sessionId);

    /**
     * @return the sessionId
     */
    public String getSessionId();

    public boolean isAdmin();

    public void setAdmin(boolean isAdmin);
}
