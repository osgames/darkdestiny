/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.dto;

import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public interface IPainterData {


    public void addDisplayObject(Integer drawLevel, I_Object_OneCoordinate object);


    public void addDrawObject(Integer drawLevel, I_Object_OneCoordinate object);


    public void addInformationData(String key, I_Object_OneCoordinate object);

    /**
     * @return the displayObjects
     */
    public Map<Integer, List<I_Object_OneCoordinate>> getDisplayObjects();

    /**
     * @return the drawObjects
     */
    public Map<Integer, List<I_Object_OneCoordinate>> getDrawObjects();

    /**
     * @return the informationData
     */
    public Map<String, I_Object_OneCoordinate> getInformationData();



    /**
     * @return the width
     */
    public int getWidth();

    /**
     * @param width the width to set
     */
    public void setWidth(int width);

    /**
     * @return the height
     */
    public int getHeight();

    /**
     * @param height the height to set
     */
    public void setHeight(int height);

    public boolean isContinueOnObject(I_Object_OneCoordinate ob, MainPainter mainPainter);

    public boolean isContinueOnObject(I_Object_OneCoordinate ob, MainPainter mainPainter, Object data);

     /**
     * @return the rasterInverted
     */
    public boolean isRasterInverted();

    /**
     * @param rasterInverted the rasterInverted to set
     */
    public void setRasterInverted(boolean rasterInverted) ;
    public boolean isShowRaster() ;

    /**
     * @param showRaster the showRaster to set
     */
    public void setShowRaster(boolean showRaster) ;
}
