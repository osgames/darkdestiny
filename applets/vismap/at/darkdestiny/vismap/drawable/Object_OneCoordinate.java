/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.drawable;

import java.awt.Image;
import at.darkdestiny.vismap.logic.HideShowSystems;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;

/**
 *
 * @author HorstRabe
 */
public abstract class Object_OneCoordinate implements I_Object_OneCoordinate {

    protected int id;
    private String name;
    protected int x;
    protected int y;
    private Image imgName;
    private String identifier;
    protected int actCoord_x;
    protected int actCoord_y;
    protected IPainter painter;
    boolean hide = false;
    boolean marked = false;

    public Object_OneCoordinate(int id, String name, int x, int y, Image imgName) {
        this.id = id;
        this.name = name;
        this.x = x;
        this.y = y;
        this.imgName = imgName;
        this.identifier = name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getX1() {
        return x;
    }

    public void setX1(int x) {
        this.x = x;
    }

    public int getY1() {
        return y;
    }

    public void setY1(int y) {
        this.y = y;
    }

    public Image getImgName() {
        return imgName;
    }

    public void setImgName(Image imgName) {
        this.imgName = imgName;
    }

    
    public boolean isToShow(int i) {
        return HideShowSystems.isSystemToShow(i);
    }

   
    @Override
    public void setActCoord_x1(int actCoord_x) {
        this.actCoord_x = actCoord_x;
    }

    @Override
    public void setActCoord_y1(int actCoord_y) {
        this.actCoord_y = actCoord_y;
    }

    @Override
    public int getActCoord_x1() {
        return this.actCoord_x;
    }

    @Override
    public int getActCoord_y1() {
        return this.actCoord_y;
    }


    @Override
   public void init(MainPainter painter) {
        this.painter = painter;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public boolean isHide() {
        return this.hide;

    }

    @Override
    public void setHide(boolean hide) {
        this.hide = hide;
    }

    @Override
    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    @Override
    public boolean isMarked() {
        return this.marked;
    }

    
    @Override
    public String getIdentifier() {
        return identifier;
    }

}
